import sys, os, time
#urllib2 is used to simulate online files as local files
from urllib.request import urlopen
import requests
#ElementTree is used for xml parsing
from lxml import etree as ET
#pandas is used for easy data analysis
import pandas as pd
#numpy is used for better arrays
import numpy as np
#unidecoed is for removing accents
import unidecode as UN
#SQLAlchemy is for database management
from sqlalchemy import create_engine
#PDF processing
from PDF_download import *

def Get_arXiV(email, tool, search_term, name):
    #Sets database name for arXiv
    engine = create_engine('sqlite:///DB/papers_DB_'+ name + '.db')
    db = 'arXiV'
    #previous_call = './' + tool + '_' + name + '_' + db + '.csv'

    #Checks if the process has already been done for some papers in prevous search
    #   if that is the case, stores in a dataFrame
    if engine.dialect.has_table(engine, db):
        print ('Reading the previous call')
        query = 'SELECT * FROM '+ db +';'
        dataFrame_previous = pd.read_sql(query, engine)
        
    #sets the location of the API for the query
    api_query = 'http://export.arxiv.org/api/query?search_query='
    #sets the location of the API for the fetching
    api_fetch = 'http://export.arxiv.org/api/query?id_list='
    
    #creates the query to the DB to get the number of papers matching the
    #   search_term:    
    #   1)Creates the base query
    #   2)Parses the response to get the total number of pages and papers
    query_base = api_query + search_term
    print ('Sending request for ' + query_base)
    
    
    #Populates the arXID list by either None or with the values from the
    #   previous data
    if engine.dialect.has_table(engine, db):
        arXID_i = dataFrame_previous['arXID'].tolist()
        arXID_i = [ str(x) for x in arXID_i ]
    else:
        arXID_i = []
    arXID = []
    
    #all IDs are stored in the XML response of query, we parse the XML
    #   extract the PMID from the it, and add it to a ID_list
    #gets the XML respon
    print('Getting relevant IDs')
    for i in range (10):
        response = urlopen(query_base + '&start=' + str(i*10000) + '&max_results=10000')       
        xml_doc = ET.fromstring(response.read())

        for child in xml_doc:
            begin = child.tag.split('}')[0]+ '}'
            break 
            
        for ID in xml_doc.findall('.//' + begin + 'id'):
            #if the ID is not in the list already, then we add it to the list
            if 'abs' in ID.text:
                temp_ID = ID.text.split('abs/')[1].split('v')[0]
                if temp_ID not in arXID_i:
                    arXID.append(temp_ID)

    #Checks if there was more entry than previously
    if engine.dialect.has_table(engine, db):
        if len(arXID) == 0:
            #print (dataFrame_previous.info())
            print ('No new entry from ArXiV')
            return
        else:
            print ('There are %s new entries' % str(len(arXID)))

    
    #1 - Extracts the informaation
    Date, Journal, Title, Authors, Aff, Abs, MesH,FullT, DOI = \
                            [],[], [], [], [], [], [], [], []
    Fetched = []
    URL = []
    dump_in = 0
    print ('Extracting the data for the set of the request')
    for ID in arXID:
        print ('Fetching ID', ID)
        fetch = api_fetch + ID 

        dump_in = dump_in + 1
        Fetched.append(ID)
        
        begin_sub = ''
        try:
            response = urlopen(fetch)       
            xml_doc = ET.fromstring(response.read())
            for child in xml_doc:
                begin = child.tag.split('}')[0]+ '}'
                for subchild in child:
                    if subchild.tag.split('}')[0]+ '}' != begin:
                        begin_sub = subchild.tag.split('}')[0]+ '}'
                        break
        except:
            xml_doc = ET.fromstring("<root>blabla</root>")

        #Extracts the DOI, not always available
        doi = ''
        for tag in xml_doc.findall('.//'+ begin_sub + 'doi'):
            doi = tag.text
        if doi != '':
            DOI.append(doi)
        else:
            DOI.append(np.nan)
        #Extracts the date from the XML, always available
        date = ''
        for tag in xml_doc.findall('.//'+ begin + 'published'):
            date = tag.text.split('T')[0]
            date = date.split('-')[0] + '-' + date.split('-')[1]
        if date != '':
            Date.append(date)
        else:
            Date.append(np.nan)
        #Extracts the journal name, not always available
        journ = ''
        for tag in xml_doc.findall('.//' + begin_sub + 'journal_ref'):
            journ = tag.text
        if journ != '' and journ != None:
            Journal.append(UN.unidecode(journ))
        else:
            Journal.append(np.nan)
        #Extracts the title of the paper, always available
        title = ''
        for tag in xml_doc.findall('.//'+ begin + 'title'):
            if 'ArXiv' not in tag.text:
                title = tag.text                                 
        if title != '' and title != None:
            Title.append(UN.unidecode(title))
        else:
            Title.append(np.nan)
        #Extract the names of the Authors, not always available
        author = []
        for tag in xml_doc.findall('.//' + begin + 'name'):
            if (tag.text != None) and (tag.text != ''):
                full_name = tag.text.split()[-1] + ' ' + tag.text.split()[0]
                author.append(UN.unidecode(full_name))
        if len(author) != 0:
            Authors.append(";".join(author))
        else:
            Authors.append(np.nan)
        #Extracts the corresponding affiliations not always available
        affiliation = []
        for tag in xml_doc.findall('.//'+ begin_sub + 'affiliation'):
            temp = tag.text.split(';')
            for item in temp:
                if item not in affiliation:
                    try:
                        affiliation.append(UN.unidecode(item))
                    except AttributeError:
                        pass
        if len(affiliation) !=0:
            Aff.append(";".join(affiliation))
        else:
            Aff.append(np.nan)
        #Extracts the full abstract not always available
        abstract = ''
        for tag in xml_doc.findall('.//' + begin + 'summary'):
            abstract = tag.text
        if abstract != '' and abstract != None:
            Abs.append(UN.unidecode(" ".join(abstract.split())))
        else:
            Abs.append(np.nan)
        #Extracts the MeshKeywords, not available in arXiV
        MesH.append(np.nan)
        
        #Extracts the full text, not available by default (arXiV)
        #   so fetching URL from arXiV, then downloading cooresponding PDF and
        #   extracting the text
        
        url_PDF = 'http://arxiv.org/pdf/' + ID + '.pdf'
        try:        
            status, file_size = download_from_URL(url_PDF, './DownloadedPDF/' + name + '/', ID)
                #We have an actual file, so we store it, otherwise, do nothing
            if (file_size > 0):
                FullT.append(convert_PDF(ID, name))
            else:
                FullT.append(np.nan)
            URL.append(url_PDF)
        except: #(the link to pdf was not working for some reason)
            print('ArXiV link to PDF not working')
            URL.append(np.nan)
            FullT.append(np.nan)

        if dump_in >= int(50):
            print ("Dumping some data")
            #DUmps some data to not have to start at beginning if a problem occurs
            dict = {'Date':Date, 'DOI':DOI, 'arXID':Fetched,  'Journal':Journal, \
                'Title':Title, 'Authors':Authors, 'Affiliation':Aff, \
                'Abstract':Abs, 'Keywords': MesH, 'Full text':FullT, 'URL': URL}

            dataFrame = pd.DataFrame(dict)
            #Exports to DB for later use
            dataFrame.to_sql(db, engine, if_exists='append', chunksize = 50)
            #Resets entries
            Date, Journal, Title, Authors, Aff, Abs, MesH,FullT, DOI = [],[], [], [], [], [], [], [], []
            Fetched = []
            URL = []
            dump_in = 0

    #creates a dictionnary and forms a pandas DataFrame for easier data
    #   analysis later
    dict = {'Date':Date, 'DOI':DOI, 'arXID':Fetched,  'Journal':Journal, \
                'Title':Title, 'Authors':Authors, 'Affiliation':Aff, \
                'Abstract':Abs, 'Keywords': MesH, 'Full text':FullT, 'URL': URL}

    #creates or append to the previous dataFrame
    dataFrame = pd.DataFrame(dict)
    #Exports to DB for later use
    dataFrame.to_sql(db, engine, if_exists='append', chunksize  = 50)
    print ('Data acquired from the database')

    return
    
    