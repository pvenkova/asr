# IMPORTANT: 
You NEED to have poppler-utils installed (`sudo apt-get install poppler-utils`) or the processing of PDF files will not be done, hence missing a lot of data!
For topic modelling with LDA_Mallet you need java installed.
# Required packages:
numpy; datetime; pandas; unidecode; sqlalchemy; stop_words; geotext; urllib; lxml; requests; shutil; bs4; itertools; sklearn; nltk; pyLDAvis; gensim; pickle; scipy; spacy

# Example use:
python3 main.py -k "Ebola" -m "your_email@xxx.com" -t "sys_review" -n "Ebola" 

python3 main.py -k "Malawi NOT cichlid NOT tobacco" -m "your_email@xxx.com" -t "sys_review" -n "MalawiProposal" 

python3 main.py -k "HIV AND Malawi" -m "your_email@xxx.com" -t "sys_review" -n "HIVMalawi"

 - `-k` argument stands for keyword. It basically represents the search query to send to the databases.
 - `-m` argument stands for email address. It is required by Pubmed and Pubmed Central API.
 - `-t` argument stands for tool name. It is also required by Pubmed and Pubmed Central API.
 - `-n` argument stands for name. It will be the file name for the local SQLite database which stores the results
 

# Improvements

 - Create a GUI
 - Modify command line version of the topic modelling script to remove the need of editing the script itself or incorporate the topic modelling script to `main.py` to be used directly instead.
 - Try other topic modelling approaches and models (currently uses Latent Dirichlet Allocation - LDA)
 - Add modules for other pre-print servers or open-source database provider. Currently the software is biased towards medical publications.
 - Add OCR for image-based PDF
 - Add HTML web-scraper for HTML only publications
 - Add restrictions to location of keywords to certain parts of the paper rather than everything.
