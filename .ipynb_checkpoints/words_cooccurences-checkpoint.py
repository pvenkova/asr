###### This script analyses full texts from fetched databases by main.py
######      To extract important keywords and the corresponding cooccurences.
###### Usage: python3 words_cooccurences.py -n 'name of the DB without the
######      extension'

###### Required packages
#Removes characters
from tfidf import *
#Re
import re, sys, operator
#Numpy provides more tools for numbers processing
import numpy as np
#pandas is used for easy data analysis
import pandas as pd
#used for arguments parsing
import argparse
#itertools is used for combination of a list
import itertools
#Scikit-learn does the cooccurrences, ways to do simple DL techniques
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation #Non-negative Matrix Factorization
import scipy.sparse as sp
from scipy import io
#pyLDAvis is used for the LDA visualisation
import pyLDAvis
import pyLDAvis.sklearn
import pyLDAvis.gensim
#NLTK for NLP
from nltk.corpus import wordnet as wn
#Stop-words is a list of words of no interests for text analysis in different
#languages
from stop_words import get_stop_words
#SQLalchemy for database
from sqlalchemy import create_engine
#datetime
from datetime import datetime
#Spacy if used for topic modeling
import spacy
spacy.load('en')
from spacy.lang.en import English
parser = English()
from gensim import corpora
import pickle
import gensim


###### This function parses the arguments
######      given by the user at script call
def parse_args():
    #Initialises the parser
    parser = argparse.ArgumentParser(description="Query args",
                                     add_help=True)
    #Adds name argument
    parser.add_argument('--name', '-n', required=True,
                        help='Name of the file to use (without the extension)',
                        action='store', dest='name')
    parser.add_argument('--words', '-wc', required=True,
                        help='Must the cooccurences be done (y/n)',
                        action='store', dest='words')
#     parser.add_argument('--topic', '-tm', required=True,
#                         help='Must the topic modelling be done (y/n)',
#                         action='store', dest='topic')
    #Stores the argument
    args = parser.parse_args()
    #Returns the argument
#     if (args.name and args.words and args.topic):
#         return (args.name, args.words, args.topic)
    if (args.name and args.words):
        return (args.name, args.words)
    else:
        print('Please enter required information.')
        return

# def display_topics(model, feature_names, no_top_words):
#     for topic_idx, topic in enumerate(model.components_):
#         print ("Topic %d:" % (topic_idx))
#         print (" ".join([feature_names[i]
#                         for i in topic.argsort()[:-no_top_words - 1:-1]]))
#     return

###### Main function
def main():
    #Get the stopwords
    en_stop = get_stop_words('en') + get_stop_words('fr')
    ## Gets the DB file
    CSV_file, words_cooc, topic_mod = parse_args() 
    engine = create_engine('sqlite:///DB/'+ CSV_file + '.db')
    ## GEts parameters for the DB
    print('Reading file ' + CSV_file)
    query = 'SELECT * FROM Complete;'
    ## Applies TFIDF to full texts to extract keywords - Takes 12min on 5744 FT/ min on 36917 FT
    ##      and does cooccurencing
    if words_cooc == 'y':
        # Reads the data
        start_time = datetime.now()
        dataFrame = pd.read_sql(query, engine).dropna(subset=['Full text'])
        print(dataFrame.info())
        print('##### Cooccurences')
        #print('Applying TFIDF to full texts')
        #dataFrame['KW_FullT'], all_KW = apply_tfidf(dataFrame['Full text'].tolist())
        #all_papers = len(dataFrame['Full text'].tolist())
        keyw_joined = [str(item).replace(',',' ') for item in dataFrame['KW_FullT'].tolist()]
        ## Removes useless words and punctuation
        print('Preprocessing text')
        keyw_clean=[' '.join(prepare_text(item)) for item in keyw_joined]
        ## Create cooccurences with sklearn
        print('Co-occurrencing')
        count_model = CountVectorizer(ngram_range=(1,1),max_features=1000) # checks per one words (no bigrams), max 500
        X = count_model.fit_transform(keyw_clean)
        Xc = (X.T * X) # this is co-occurrence matrix in sparse csr format
        freq_words = Xc.toarray()
        ## Sets the name of columns/rows
        words = count_model.vocabulary_
        sorted_words = dict(sorted(words.items(), key=operator.itemgetter(1)))
        sorted_words = {v:k for k,v in sorted_words.items()}
        name = list(sorted_words.values())
        ## Makes the data
        dataFrame = pd.DataFrame(data=freq_words, index=name, columns=name)
        dataFrame.to_csv('cooccurence_' + CSV_file + '.csv')
        #Calculates sims
        symSim = [[0. for x in range(len(name))] for y in range(len(name))]
        sim12 = [[0. for x in range(len(name))] for y in range(len(name))]
        for w1 in range(len(name)):           
            for w2 in range(len(name)):
                if freq_words[w1][w1] == 0:
                    sim12[w1][w2] = freq_words[w1][w2]/(freq_words[w1][w1]+1)	    #sim12 ~ 1 in diagonals (construction); = #of papers with word w1 AND w2/#of papers with word w1
                else:
                    sim12[w1][w2] = freq_words[w1][w2]/(freq_words[w1][w1])
                if freq_words[w2][w2] == 0:
                    symSim[w1][w2] = all_papers*sim12[w1][w2]/(freq_words[w2][w2]+1)    #symSim = sim12/#of paper with word w2 * all papers
                else:
                    symSim[w1][w2] = all_papers*sim12[w1][w2]/(freq_words[w2][w2])
        #Stores sims
        df_sim = pd.DataFrame(data=sim12, index=name, columns=name)
        df_sim.to_csv("AsymSim_"+ CSV_file +".csv")
        df_Symsim = pd.DataFrame(data=symSim, index=name, columns=name)
        df_Symsim.to_csv("SymSim_"+ CSV_file +".csv")
        print('Cooccurences done in ', datetime.now()-start_time)


###### Always executed
if __name__ == "__main__":
    main()
