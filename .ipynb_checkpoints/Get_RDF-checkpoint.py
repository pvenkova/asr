
import sys, os
#urllib2 is used to simulate online files as local files
from urllib.request import urlopen
import requests
#ElementTree is used for xml parsing
from lxml import etree as ET
from lxml import objectify
#pandas is used for easy data analysis
import pandas as pd
#numpy is used for better arrays
import numpy as np
#unidecoed is for removing accents
import unidecode as UN
#SQLalchemy for database management
from sqlalchemy import create_engine


#Main program to get RDF
def Get_RDF(tool, name, rdf_file):
    db = 'RDF'
    #Checks if the file exists
    if not os.path.isfile(rdf_file):
        print ('There is no RDF file attach to this search')
        return

    #Reads the CSV
    data = pd.read_csv(rdf_file, index_col = 0)

    #Initialises what needs to be
    Date, Authors, MesH = [], [], []
    PMC_ID, PMID, Aff, FullT = [], [], [], []

    #Extracts the date (YYYY-MM)
    date_list = data['Date'].tolist()
    for dates in date_list:
        date = str(dates).split('-')
        try:
            date = date[0] + '-' + date[1]
        except IndexError:
            if date[0] != 'nan':
                date = date[0] + '-01'
            else:
                date = np.nan
        Date.append(date)
    #Extracts the DOI
    DOI = data['DOI'].tolist()
    #Extracts the journal name
    Journal = data['Publication Title'].tolist()
    #Extracts the title
    Title = data['Title'].tolist()
    #Extracts the authors
    authors = data['Author'].tolist()
    author = []
    for entries in authors:
        author = str(entries).split(';')
        Authors.append(UN.unidecode(";".join(author))) 
    #Extracts the abstract
    Abs = data['Abstract Note'].tolist()
    #Extract keywords (they are in two columns so we need to add them. .add \
    # does not work with strings
    kw = (data['Manual Tags'].map(str) + data['Automatic Tags']).tolist()
    for keys in kw:
        temp = str(keys).replace('nan', '')
        if temp == '':
            temp = np.nan
        else:
            temp = UN.unidecode(";".join(temp.split(';')))
        MesH.append(temp)
    #Extracts the URL just in case
    URL = data['Url'].tolist()
    #PMCID, PMID, Aff and FullT are not given
    for i in range(len(Title)):
        PMC_ID.append(np.nan)
        PMID.append(np.nan)
        Aff.append(np.nan)
        FullT.append(np.nan)
    #creates a dictionnary and forms a pandas DataFrame for easier data
    #   analysis later
    dict = {'Date':Date, 'DOI':DOI, 'PMCID':PMC_ID, 'PMID':PMID, 'Journal':Journal, \
            'Title':Title, 'Authors':Authors, 'Affiliation':Aff, \
            'Abstract':Abs, 'Keywords': MesH, 'Full text':FullT, 'URL':URL}

    #creates or append to the previous dataFrame
    dataFrame = pd.DataFrame(dict)    

    #Exports to DB for later use
    engine = create_engine('sqlite:///DB/papers_DB_'+ name + '.db')
    dataFrame.to_sql(db, engine, if_exists='replace', chunksize = 50)
    print ('Data acquired from Zotero')

    return


