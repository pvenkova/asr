#################################################################################################################################################
#                                                              IMPORT PACKAGES                                                                  #
#################################################################################################################################################
#Stop words
from stop_words import get_stop_words
#SKlearn provides stg tfidf related
from sklearn.feature_extraction.text import TfidfVectorizer
#COllection
import collections
import operator
#Nltk stem is used to process plurals and tenses of words
import nltk
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.corpus import names
# unidecode
import unidecode as UN
#Pandas
import pandas as pd
import numpy as np


#################################################################################################################################################
#                                                          SUBROUTINES                                                                          #
#################################################################################################################################################
def apply_tfidf(col):
    '''
    Description
    --------------
    This function applies TF-IDF to a list of documents

    Takes
    --------------
    col (list): list of texts to use the TF-IDF on
    
    Returns
    --------------
    keyw (string): list of TFIDF keywords separated by semicolumns-comm
    ";".join(all_keyw): list of all TFIDF keywords separated by semicolumns-comm
    
    '''
    keyw = []
    all_keyw = []
    corpus = col
    corpus_nonan = [item for item in corpus if not pd.isnull(item)]
    tf = TfidfVectorizer(tokenizer=prepare_text,
                            stop_words=get_stop_words('en'),
                            min_df=1)
    tfidf_matrix = tf.fit_transform(corpus_nonan)
    feature_names = tf.get_feature_names()
    for item in corpus:
        imp_words = []
        if not pd.isnull(item):
            doc_number = corpus_nonan.index(item)
            feature_index = tfidf_matrix[doc_number,:].nonzero()[1]
            tfidf_scores = zip(feature_index, [tfidf_matrix[doc_number, x] for x in feature_index])
            sorted_words = {}
            for w, s in [(feature_names[i], s) for (i, s) in tfidf_scores]:
                sorted_words[w] = s
            common = collections.Counter(sorted_words).most_common(50)
            for w, s in common:
                imp_words.append(w.lower())
                if w.lower() not in all_keyw:
                    #list of all keywords of all papers
                    all_keyw.append(w.lower())
            imp_words = list(int_filter(imp_words))
        if imp_words != []:
            keyw.append(";".join(imp_words))
        else:
            keyw.append(np.nan)
        all_keyw = list(int_filter(all_keyw))
    return (keyw, ";".join(all_keyw))


def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wn.ADJ,
                "N": wn.NOUN,
                "V": wn.VERB,
                "R": wn.ADV}

    return tag_dict.get(tag, wn.NOUN)

def get_lemma(word):
    '''
    Description
    --------------
    This function lemmatizes (search for synonyms and root) a word

    Takes
    --------------
    word (string)
    
    Returns
    --------------
    The lemmatized word
    
    '''
    wnl = WordNetLemmatizer()
    lem_word = wnl.lemmatize(word, pos = get_wordnet_pos(word))
    return wnl.lemmatize(lem_word, pos = get_wordnet_pos(word))


def prepare_text(text, incl_nouns = False ):
    '''
    Description
    --------------
    This function preprocesses text for NLP purposes

    Takes
    --------------
    text (string)
    incl_nouns (bool, default False)
    
    Returns
    --------------
    tokens (list): list of tokens extracted from the text
    
    '''
    #List of all English nouns
    nouns = [x.name().split('.', 1)[0] for x in wn.all_synsets('n')]
    name_list = names.words('male.txt') + names.words('female.txt')
    name_list = [UN.unidecode(name.lower()) for name in name_list]
    #List of accronyms of interest
    accr = ['HIV', 'AIDS', 'ART', 'CART', 'HAART',
            'hiv', 'aids', 'art', 'cart', 'haart']
    #Get the stopwords - adds the name_list to remove first names
    en_stop = get_stop_words('en') + get_stop_words('fr') + name_list
    #okenize the text
    tokens = text.lower().split(' ') #word_tokenize(UN.unidecode(text.lower()))
    
    first = True
    do_lemma = True
    while first: #It goes twice because after lemmatization, some root fro stopwords might still be around
        #Removes  http and www adresses and all useless stuff poluting PDFs - custom filter
        tokens = [token for token in tokens if (('www' not in token) 
                                            and ('http' not in token)
                                            and ('doi' not in token)
                                            and ('journal' not in token)
                                            and ('licens' not in token)
                                            and ('abstract' not in token)
                                            and ('author' not in token)
                                            and ('open' not in token)
                                            and ('permit' not in token)
                                            and ('article' not in token)
                                            and ('volume' not in token)
                                            and ('book' not in token)
                                            and ('copy' not in token)
                                            and ('credit' not in token)
                                            and ('publi' not in token)
                                            and ('repro' not in token)
                                            and ('accept' not in token)
                                            and ('comply' not in token)
                                            and ('html' not in token)
                                            and ('concl' not in token)
                                            and ('nih' not in token)
                                            and ('ackno' not in token)
                                            and ('intro' not in token)
                                            and ('method' not in token)
                                            and ('background' not in token)
                                            and ('research' not in token)
                                            and ('review' not in token)
                                            and ('correct' not in token)
                                            and ('supplement' not in token)
                                            and ('plos' not in token)
                                            and ('editor' not in token)
                                            and ('edu' not in token)
                                            and ('month' not in token)
                                            and ('day' not in token)
                                            and ('univer' not in token))]
        #Removes stopwords
        tokens = [token for token in tokens if token not in en_stop]
        if do_lemma:
            #Lemmatize
            tokens = [get_lemma(token) for token in tokens]
            do_lemma = False
        else:
            first = False
    #Removes useless characters
    tokens = [remove_char(token) for token in tokens]
    #Keeps only nouns and accronyms of interest 
    if incl_nouns:
        tokens = [token for token in tokens if token in nouns]
    #Keeps only > 4 (missing acronyms?)
    tokens = [token for token in tokens if (len(token) > 4 or token in accr)]
    #Removes integers
    tokens = list(int_filter(tokens))
    #Returns the clean tokens
    return list(set(tokens)) #To avoid duplicates

def remove_char(string):
    '''
    Description
    --------------
    This function removes unwanted characters

    Takes
    --------------
    string (string)
    
    Returns
    --------------
    
    '''
    string =  string.replace('\n',' ')
    return ''.join(e for e in string if (e.isalpha() or e.isspace() or e == '-' or e == "'"))

def int_filter(someList):
    '''
    Description
    --------------
    This function removes integers from a list of token
    appearing as strings

    Takes
    --------------
    someList (list)
    
    Returns
    --------------
    v (list): cleaned list 
    
    '''
    for v in someList:
        try:
            int(v)
            continue # Skip these
        except ValueError:
            yield v # Keep these
