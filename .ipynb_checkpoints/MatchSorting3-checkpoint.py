###### Packages import
import numpy as np
#time is used here for the program's execution time
from datetime import datetime
#pandas is used for easy data analysis
import pandas as pd
#system imports
import sys, os
#Initial pool sorting
from Process_Initial_Pool2 import *
#unidecode is to remove accent
import unidecode as UN
#database management
from sqlalchemy import create_engine
#Stop-words is a list of words of no interests for text analysis in different
#languages
from stop_words import get_stop_words



#################################################################################################################################################
#                                                          SUBROUTINES                                                                          #
#################################################################################################################################################
def proc_ip(name, dataFrame):
    '''
    Description
    --------------
    This function processes the initial pool if needed and calls the other functions

    Takes
    --------------
    name (str): the name of the search
    dataFrame: the extracted data from the online databases

    Returns
    --------------
    (lists): lists of relevant informations
    '''
    ### Sorts basic names
    inputIP_dir = './InitialPools/'+ name + '/'
    file_name_IP = name + '_IP.xml'
    ###
    print('############### Processing Initial Pool')
    df_IP = conversion_ENL(file_name_IP, inputIP_dir) #Returns the ENL XML data
    df_IP_final = pd.DataFrame(data=None, columns=dataFrame.columns)
    index_IP = 0

    for index, row in df_IP.iterrows():
        for index2, row2 in dataFrame.iterrows():
            if (row.DOI == row2.DOI) and (not pd.isnull(row.DOI)) and (not pd.isnull(row2.DOI)):
                df_IP_final.loc[index_IP] = row2
                index_IP += 1
                break
            else:
                if not pd.isnull(row2.Title):
                    if (UN.unidecode(row.Title.lower()) == UN.unidecode(row2.Title.lower())) \
                             or (UN.unidecode(row.Title.lower()) in UN.unidecode(row2.Title.lower())):
                        df_IP_final.loc[index_IP] = row2
                        index_IP += 1
                        break

    KW_initial = df_IP_final['KW_FullText'].tolist() + df_IP_final['KW_Abstract'].tolist() + df_IP_final['Keywords'].tolist() + df_IP_final['Title_Clean'].tolist()
    KW_initial = [KW.split(';') for KW in KW_initial if not pd.isnull(KW)]
    KW_initial = list(set(sum(KW_initial,[])))

    Journal_initial = list(set(df_IP_final['Journal'].tolist()))
    
    Author_initial = df_IP_final['Authors_LN'].tolist()
    Author_initial = [Author.split(';') for Author in Author_initial if not pd.isnull(Author)]
    Author_initial = list(set(sum(Author_initial,[])))
    
    Title_initial = df_IP_final['Title_Clean'].tolist()
    Title_initial = [Title.split(';') for Title in Title_initial if not pd.isnull(Title)]
    Title_initial = list(set(sum(Title_initial,[])))

    Aff_initial = df_IP_final['Aff_Clean'].tolist()
    Aff_initial = [Aff.split(';') for Aff in Aff_initial if not pd.isnull(Aff)]
    Aff_initial = list(set(sum(Aff_initial,[])))
    
    df_IP_final.to_csv(inputIP_dir + 'data.csv')

    return (KW_initial, Journal_initial, Author_initial, Title_initial, Aff_initial)
        
def match_comp(dataFrame, name, engine):
    '''
    Description
    --------------
    This function sort papers (rel/irrel/tocheck) based on naive matching

    Takes
    --------------
    dataFrame (pd.dataframe): The dataframe where all infos are
    name (str): name of the search
    engine (sqlite engine): DB connection engine

    Returns
    --------------
    dataFrame_rel2, dataFrame_irrel, dataFrame_tocheck (pd.Dataframes): corresponding dataframes
    '''
    ### Sorts basic names
    inputIP_dir = './InitialPools/'+ name + '/'
    ### Gets the data from IP
    KW_initial, Journal_initial, Author_initial, Title_initial, Aff_initial = proc_ip(name, dataFrame)
    ### Reads the IP stuff
    if os.path.isfile(inputIP_dir + 'KW_initial_' + name):
        with open(inputIP_dir + 'KW_initial_' + name) as f:
            KW_initial = f.read().splitlines()
    elif len(KW_initial) == 0:
        print('Please provide a short amount of keywords...')
        sys.exit()

    ### Matching
    print('############### Asserting relevance')
    new_entries = 1
    nb_loop = 0
    while (new_entries != 0):
        print('##### New Loop')
        nb_loop += 1
        # New loop so resets the indexes
        # Creates some useful dataframe
        dataFrame_tocheck = pd.DataFrame(data=None, columns=dataFrame.columns)      #Stores papers that must be checked by hand (40-60% match)
        dataFrame_irrel = pd.DataFrame(data=None, columns=dataFrame.columns)        #Stores papers that are irrelevant (<40% match)
        dataFrame_rel1 = pd.DataFrame(data=None, columns=dataFrame.columns)         #Stores papers that are relevant based on the first check (>60%)
        index_irrel = 0
        index_rel1 = 0
        index_tocheck = 0
        new_entries = 0
        print('Performing match')
        for index, row in dataFrame.iterrows():
            #Makes a list out of the data from the databases for Title, Author, KW
            #We don't want the referee's response
            try:
                if 'referee' in row['Title_Clean']:
                    continue
                list_title = row['Title_Clean'].split(';')
            except AttributeError:
                list_title = []
            except TypeError:
                list_title = []
            try:
                list_author = row['Authors_LN'].split(';')
            except AttributeError:
                list_author = []
            try:
                list_aff = row['Aff_Clean'].split(';')
            except AttributeError:
                list_aff = []
            try:
                list_KW = ' '.join(row['Keywords'].split(';')).split() + row['KW_Abstract'].split(';')
            except:
                if ((str(row['Keywords']) != 'nan') and (row['Keywords'] != None)):
                    list_KW = row['Keywords'].split(';')
                elif ((str(row['KW_Abstract']) != 'nan') and (row['KW_Abstract'] != None)):
                    list_KW = row['KW_Abstract'].split(';')
                else:
                    list_KW = []
            try:
                list_KW_FT = row['KW_FullText'].split(';')
            except AttributeError:
                list_KW_FT = []
            except TypeError:
                list_KW_FT = []

                 
            matching = 0
            total_matching = 0
            #Matches Title if there are no NaN and Titles from initial pool were provided
            try:
                if len(list_title)>0 and len(Title_initial)>0:
                    for item in list_title:
                        if item in Title_initial:
                            matching += 1
                total_matching = matching
            except TypeError:
                pass
            #Same for author
            matching = 0
            try:
                if len(list_author)>0 and len(Author_initial)>0: 
                    for item in list_author:
                        if item in Author_initial:
                            matching += 1
                total_matching += matching
            except TypeError:
                pass
            #Same for KW
            matching = 0
            try:
                if len(list_KW)>0 and len(KW_initial)>0:
                    for item in list_KW:
                        if item in KW_initial:
                            matching += 1
                total_matching += matching
            except TypeError:
                pass
            #Same for Affiliations
            matching = 0
            try:
                if len(list_aff)>0 and len(Aff_initial)>0:
                    for item in list_aff:
                        if item in Aff_initial:
                            matching += 1
                total_matching += matching
            except TypeError:
                pass
            #Same for FT
            matching = 0
            try:
                if len(list_KW_FT)>0 and len(KW_initial)>0:
                    for item in list_KW_FT:
                        if item in KW_initial:
                            matching += 1
                total_matching += matching
            except TypeError:
                pass
            #Check how many entries they were
            total_entries = 0
            if (type(list_title) != float) and (len(Title_initial) >0):
                total_entries += len(list_title)
            if (type(list_author) != float) and (len(Author_initial) >0):
                total_entries += len(list_author)
            if (type(list_KW) != float):
                total_entries += len(list_KW)
            if (type(list_KW_FT) != float):
                total_entries += len(list_KW_FT)
            if (type(list_aff) != float) and (len(Aff_initial) >0):
                total_entries += len(list_aff)
            #Checks the percentages matching, then categorise the papers
            try:
                perc_matching = float(total_matching)/float(total_entries) * 100
            except ZeroDivisionError:
                perc_matching = 0.0
            if perc_matching < 60:
                #REJECTED
                dataFrame_irrel.loc[index_irrel] = row
                index_irrel += 1
            elif perc_matching >= 85:
                if not ((len(list_KW) == 0) and (len(list_KW_FT) == 0) and (len(list_author) == 0)): #Only title, affiliation => can't say
                    #ACCEPTED
                    dataFrame_rel1.loc[index_rel1] = row
                    index_rel1 += 1
                    if perc_matching < 98:                  #not in IP, or not already added
                        #Inputs back in IP
                        Title_initial = list(set(Title_initial + list_title))
                        Author_initial = list(set(Author_initial + list_author))
                        KW_initial = list(set(KW_initial + list_KW + list_KW_FT))
                        Aff_initial = list(set(Aff_initial + list_aff))
                        new_entries += 1
                else: #Rejected    
                    dataFrame_irrel.loc[index_irrel] = row
                    index_irrel += 1
            else:
                #CHECK BY HAND
                dataFrame_tocheck.loc[index_tocheck] = row
                index_tocheck += 1
    dataFrame_rel1.to_sql('Relevant_papers', engine, if_exists = 'replace', chunksize = 50)
    dataFrame_irrel.to_sql('Irrelevant_papers', engine, if_exists = 'replace', chunksize = 50)
    dataFrame_tocheck.to_sql('To_check', engine, if_exists = 'replace', chunksize = 50) 
    return (dataFrame_rel1, dataFrame_irrel, dataFrame_tocheck)