#################################################################################################################################################
#                                                              IMPORT PACKAGES                                                                  #
#################################################################################################################################################
#Shutil is used to get the PDF content
import shutil
#BeautifulSoup is used for HTML parsing
from bs4 import BeautifulSoup
#Requests and re are used for online queyiing
import requests, re
#Systems import
import os, string, sys
#o allow system calls
from subprocess import call
#unidecode to remove accents and such
import unidecode as UN
#Pandas & numpy
import pandas as pd
import numpy as np
#This is to decode an URL whch contains % strings
import urllib.parse


#################################################################################################################################################
#                                                          SUBROUTINES                                                                          #
#################################################################################################################################################
def remove_char(string):
    '''
    Description
    --------------
    This function removes unwanted characters

    Takes
    --------------
    string (string)
    
    Returns
    --------------
    
    '''
    string =  string.replace('\n',' ')
    return ''.join(e for e in string if (e.isalpha() or e.isspace() or e == '-' or e == "'"))

def get_URL_from_DOI(doi):
    '''
    Description
    --------------
    This function gets a direct URL to a pdf based on DOI

    Takes
    --------------
    DOI (string): URL to the PDF file
    
    Returns
    --------------
    url_PDF (string/None): URL to the PDF file
    '''
    #Defines header to not be rejected
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}
    #Defines redirection terms (esp. for Elsevier)
    redirections = ['Redirecting', 'redirecting', 'Redirect', 'redirect', 'tag_url']
    #Constructs URL for DOI.org
    url_PDF = None
    url_DOI = 'https://doi.org/' + doi
    try:
        #Send the requests to DOI.org and parses th html response
        response = requests.get(url_DOI, headers = headers)
        url_base = response.url
        html = BeautifulSoup(response.text, features = 'lxml')
        #Check the title
        title = html.title.text
        #if this is a redirecting page, the title should mention it, so we need to access the actual page
        while (title in redirections):
            for link in html.find_all('meta'):
                url_DOI = str(link.get('content'))
                for redirection in redirections:
                    if (redirection in url_DOI):
                        #Gets the actual web page, sends a new request and parses it
                        url_DOI = url_DOI.split(redirection + '=')[-1]
                        if redirection == 'tag_url':
                            url_DOI = url_DOI.split('&title=')[0]
                        url_DOI = urllib.parse.unquote(url_DOI)
                        response = requests.get(url_DOI, headers = headers)
                        url_base = response.url
                        html = BeautifulSoup(response.text, features = 'lxml')
                        #Check the title
                        title = html.title.text
        #Gets the root access of the 
        url_base = url_base.split('/')[0] + '//' + url_base.split('/')[2]
        url_base_noHTTP = url_base.split('/')[2] 
        #Looks for tags with links
        #Checks if there's a link with a pdf file and the doi
        if 'sciencedirect' in url_base: #Elsevier
            url_PDF = 'http://api.elsevier.com/content/article/doi:' + doi + '?view=FULL'
            return url_PDF
        
        for link in html.find_all('meta'):
            current_link = str(link.get('content'))
            #skip adverts and doi.org links
            if ('pubads' in current_link) or ('doi.org' in current_link):
                continue
            if (doi in current_link) and ('file' in current_link):
                if not ((url_base_noHTTP in current_link) or ('http' in current_link)):
                    url_PDF = url_base + current_link
                else:
                    url_PDF = current_link
        #Wasn't a "meta" tag
        if pd.isnull(url_PDF): 
            for link in html.find_all('a'):
                current_link = str(link.get('href'))
                #skip adverts and doi.org links
                if ('pubads' in current_link) or ('doi.org' in current_link):
                    continue
                if (doi in current_link):
                    if not ((url_base_noHTTP in current_link) or ('http' in current_link)):
                        url_PDF = url_base + current_link
                    else:
                        url_PDF = current_link
                    break
                elif ('.pdf' in current_link) and ('doi' in current_link):
                    if not ((url_base_noHTTP in current_link) or ('http' in current_link)):
                        url_PDF = url_base + current_link
                    else:
                        url_PDF = current_link
                elif ('.pdf' in current_link):
                    if not ((url_base_noHTTP in current_link) or ('http' in current_link)):
                        url_PDF = url_base + current_link
                    else:
                        url_PDF = current_link
        #Wasn't a "meta" or "a" tag
        if pd.isnull(url_PDF): 
            for link in html.find_all('link'):
                current_link = str(link.get('href'))
                #skip adverts and doi.org links
                if ('pubads' in current_link) or ('doi.org' in current_link):
                    continue
                if ('.pdf' in current_link):
                    if not ((url_base_noHTTP in current_link) or ('http' in current_link)):
                        url_PDF = url_base + current_link
                    else:
                        url_PDF = current_link           
        #Tests if the site exists
        if not pd.isnull(url_PDF):
            #SOmetimes, just the http is missing
            if not ('http' in url_PDF):
                url_PDF = 'http:' + url_PDF
            print(url_PDF)
            test_url = requests.get(url_PDF, headers = headers).status_code
            if test_url != 200:
                url_PDF = None
                print('No response from the site. Error:', test_URL)
                print('Could not get link from DOI', doi)
    except Exception as e:
        print(e)
        print('Could not get link from DOI', doi, url_PDF)
    if pd.isnull(url_PDF):
        return
    else:
        #Replace all possible '%XXX' codes that might be here
        return(urllib.parse.unquote(url_PDF))

def download_from_URL(url, output_path, file_name):
    '''
    Description
    --------------
    This function downloads PDF from a direct URL

    Takes
    --------------
    url (string): URL to the PDF file
    output_path (string): where to store the PDF
    file_name (string): what will be the file name in the output_path.
    
    Returns
    --------------
    str status (string): status code of the requests
    file_size (int): size in bytes of the downloaded file
    '''
    try:
        if 'elsevier' in url: 
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
                       'X-ELS-APIKey': 'a16f56929eefdd0e8d9b38898f5e0105', 'Accept': 'application/pdf'}
        else:
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}
        page_response = requests.get(url, headers = headers, stream=True)
        status= page_response.status_code
        file_size = 0
        if (status == 200):
            page_response.raw.decode_content = True
            with open(output_path + file_name + '.pdf', 'wb') as f:
                shutil.copyfileobj(page_response.raw, f)
                file_size = int(os.stat(output_path + file_name + '.pdf').st_size)
        return (str(status), file_size)
    except Exception as e:
        print('Failed to get pdf', url)
        print(e, url)
        return ('Failed to get response', -1)

def convert_PDF(file_name, name):
    '''
    Description
    --------------
    This function converts PDF to text to do text processing

    Takes
    --------------
    file_name (string): name of the PDF fle to convert
    name (string): folder root of PDF
    IP (bool) : if it's for processing the IP

    Returns
    --------------
    text (string): the text conversion of the pdf
    '''
  

    call_shell = 'pdftotext ./DownloadedPDF/' + name + '/' + file_name + '.pdf text.txt'
    try:
        #Extract the text from the pdf
        call(call_shell, shell=True)
        with open('text.txt','r',errors='ignore') as file:
            text = file.read()
            text = UN.unidecode(text)
            text = remove_char(text)
            os.remove('text.txt')
            os.remove('./DownloadedPDF/' + name + '/' + file_name + '.pdf') #takes too much space
        return (text)
    except Exception as e:
        print('Failed to get data from PDF - make sure you have poppler-utils installed')
        print(e)
        try:
            os.remove('./DownloadedPDF/' + name + '/' + file_name + '.pdf') #takes too much space
        except:
            pass
        return (None)
