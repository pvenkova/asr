#This program transforms the XML export of ENL files to a
#   more useful type, CSV, which can then be read by pandas
#   to create the pool of initial papers, and extracts the
#   relevant information for processing.
#   ATTENTION: To be the most helpful possible, ENL should
#   register Year, Title, Abstract, Author names and affiliations,
#   DOI, Journal and keywords. The PDF names should also be
#   consistent, ideally with the DOI as a name ('/' characters
#   replaced with '_'), for easier process.


#################################################################################################################################################
#                                                              IMPORT PACKAGES                                                                  #
#################################################################################################################################################

#system imports
import os, errno, sys, re, string
import argparse
#pands is used for data amalysis
import pandas as pd
#numpy extends arrays and adds new functions
import numpy as np
#line processing
from itertools import islice
#ElementTree is used for xml parsing
from lxml import etree as ET
#requests is for fetching
import requests

#################################################################################################################################################
#                                                          SUBROUTINES                                                                          #
#################################################################################################################################################
def check_length(list_tocheck, length):
    '''
    Description
    --------------
    This functioncheck the lenght of a list compared to a value

    Takes
    --------------
    list_tocheck (list): the list to check the length of
    length (int): the value to ne equal to
    
    Returns
    --------------
    '''
    if len(list_tocheck) < length:
        list_tocheck.append(np.nan)
    elif len(list_tocheck) > length:
        print ('Problem here')
        print (list_tocheck, len(list_tocheck))
        sys.exit()
    return

def conversion_ENL(file_name, output_dir):
    '''
    Description
    --------------
    This function converts the ENL file (basically a XML file) to a dataframe

    Takes
    --------------
    file_name (str): the name of the file to extract data from
    output_dir (str): the directory where the file is

    Returns
    --------------
    dataFrame (pd.DataFrame): a dataframe containing all information regarding
                            the papers
    output_dir (str): see input
    '''
    Year, Title, Abs, Authors, DOI, Journal, KW, Aff = [],[],[],[],\
                                                    [],[],[],[]
    #gets the XML response
    xml_doc = ET.parse(output_dir + file_name).getroot()
    #For each record, i.e. entry in the ENL database
    for record in xml_doc.iter('record'):
        #Gets the list of authors, not always provided
        authors = []
        for style in record.findall('contributors'):
            for author in style.iter('author'):
                authors.append(author.text)
        if len(authors) != 0:
            Authors.append(authors)
        else:
            Authors.append(np.nan)
        #Gets the affiliations, not always provided
        affiliation = []
        for style in record.findall('auth-address'):
            for addresses in style.iter('style'):
                if addresses.text != 'Unlisted':
                    affiliation.append(addresses.text)
        if len(affiliation) != 0:           
            Aff.append(affiliation)
        else:
            Aff.append(np.nan)
        #Get the titles & journals. Journal name is not always provided
        journal = ''
        for style in record.findall('titles'):
            for title in style.iter('title'):
            #    for tag in title.iter('style'):
                Title.append(title.text)
            #for title in style.iter('alt-title'):
            #    for tag in title.iter('style'):
            #        journal = tag.text
        for style in record.findall('periodical'):
            for perio in style.findall('full-title'):
                journal = perio.text
        if journal != '':
            Journal.append(journal)
        else:
            Journal.append(np.nan)
        #Gets the date (year only provided)
        date = ''
        for style in record.findall('dates'):
            for year in style.iter('year'):
                #for tag in year.iter('style'):
                date = year.text + '-01'
        if date != '':
            Year.append(date)
        else:
            Year.append(np.nan)
        #Gets the abstract
        abstract = ''
        for style in record.findall('abstract'):
            #for abstr in style.iter('style'):
            abstract = "".join(style.itertext()).replace('\n','')
        if abstract != '':
            Abs.append(abstract)
        else:
            Abs.append(np.nan)
        #Gets the keywords, not always available
        keyw = []
        for style in record.findall('keywords'):
            for key in style.iter('keyword'):
                #for tag in key.iter('style'):
                keyw.append(key.text)
        if len(keyw) != 0:
            KW.append(keyw)
        else:
            KW.append(np.nan)
        #Gets the DOI
        doi = ''
        for style in record.findall('notes'):
            for text in style.iter('style'):
                string = text.text
                try:
                    string = string.split('doi: ')[1]
                    doi = string.split(' ')[0]
                except:
                    pass
        if doi != '':
            DOI.append(doi[:-1])
        else:
            for tag in record.findall('electronic-resource-num'):
                doi = tag.text
            if doi != '':
                DOI.append(doi)
            else:
                DOI.append(np.nan)

    #print(len(Year), len(DOI),len(Journal), len(Title),len(Authors), len(Aff),len(Abs), len(KW))

    #Makes the dataFrame
    dict = {'Date':Year, 'DOI':DOI, 'Journal':Journal, 'Title':Title, 'Author':Authors, \
        'Affiliation':Aff, 'Abstract':Abs, 'Keywords':KW}
    dataFrame = pd.DataFrame(dict)
    return (dataFrame)