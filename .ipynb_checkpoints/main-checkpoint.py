#python3 main.py -k "HIV AND Malawi" -m "amaury.thiabaud@gmail.com" -t "sys_review" -n "HIVMalawi" 
#python3 main.py -k "Malawi NOT cichlid NOT tobacco" -m "amaury.thiabaud@gmail.com" -t "sys_review" -n "MalawiProposal" 

#################################################################################################################################################
#                                                              IMPORT PACKAGES                                                                  #
#################################################################################################################################################

# Numpy is used for mathematical purposes and better arrays
import numpy as np
#time is used here for the program's execution time
from datetime import datetime
import time
#pandas is used for easy data analysis
import pandas as pd
#args2query is used for giving arguments
from args2query import args2query
#system imports
import sys, os, string
#PubMed Central
from Get_PMC import Get_PMC
#PubMed
from Get_PM import Get_PM
#RDF Zotero
from Get_RDF import Get_RDF
#Paperity
from Get_PAP import Get_PAP
#JSTOR
from Get_JSTOR import Get_JSTOR
#arXiV
from Get_arXiV import Get_arXiV
#unidecode is to remove accent
import unidecode as UN
#database management
from sqlalchemy import create_engine
#gotext is used to identify cities
from geotext import GeoText
#Nltk stem is used to process plurals and tenses of words
import nltk
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.corpus import names
#Stop words
from stop_words import get_stop_words

#################################################################################################################################################
#                                                          SUBROUTINES                                                                          #
#################################################################################################################################################
def remove_char(string):
    '''
    Description
    --------------
    This function removes unwanted characters

    Takes
    --------------
    string (string)
    
    Returns
    --------------
    
    '''
    string =  string.replace('\n',' ')
    return ''.join(e for e in string if (e.isalpha() or e.isspace() or e == '-' or e == "'"))

def int_filter(someList):
    '''
    Description
    --------------
    This function removes integers from a list of token
    appearing as strings

    Takes
    --------------
    someList (list)
    
    Returns
    --------------
    v (list): cleaned list 
    
    '''
    for v in someList:
        try:
            int(v)
            continue # Skip these
        except ValueError:
            yield v # Keep these
            
def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wn.ADJ,
                "N": wn.NOUN,
                "V": wn.VERB,
                "R": wn.ADV}

    return tag_dict.get(tag, wn.NOUN)

def get_lemma(word):
    '''
    Description
    --------------
    This function lemmatizes (search for synonyms and root) a word

    Takes
    --------------
    word (string)
    
    Returns
    --------------
    The lemmatized word
    
    '''
    wnl = WordNetLemmatizer()
    lem_word = wnl.lemmatize(word, pos = get_wordnet_pos(word))
    return wnl.lemmatize(lem_word, pos = get_wordnet_pos(word))
            
def prepare_text(text, incl_nouns = False ):
    '''
    Description
    --------------
    This function preprocesses text for NLP purposes

    Takes
    --------------
    text (string)
    incl_nouns (bool, default False)
    
    Returns
    --------------
    tokens (list): list of tokens extracted from the text
    
    '''
    #List of all English nouns
    nouns = [x.name().split('.', 1)[0] for x in wn.all_synsets('n')]
    name_list = names.words('male.txt') + names.words('female.txt')
    name_list = [UN.unidecode(name.lower()) for name in name_list]
    #List of accronyms of interest
    accr = ['HIV', 'AIDS', 'ART', 'CART', 'HAART',
            'hiv', 'aids', 'art', 'cart', 'haart']
    #Get the stopwords - adds the name_list to remove first names
    en_stop = get_stop_words('en') + get_stop_words('fr') + name_list
    #okenize the text
    tokens = text.lower().split() #word_tokenize(UN.unidecode(text.lower()))
    
    first = True
    do_lemma = True
    while first: #It goes twice because after lemmatization, some root fro stopwords might still be around
        #Removes  http and www adresses and all useless stuff poluting PDFs - custom filter
        tokens = [token for token in tokens if (('www' not in token) 
                                            and ('http' not in token)
                                            and ('doi' not in token)
                                            and ('journal' not in token)
                                            and ('licens' not in token)
                                            and ('abstract' not in token)
                                            and ('author' not in token)
                                            and ('open' not in token)
                                            and ('permit' not in token)
                                            and ('article' not in token)
                                            and ('volume' not in token)
                                            and ('book' not in token)
                                            and ('copy' not in token)
                                            and ('credit' not in token)
                                            and ('publi' not in token)
                                            and ('repro' not in token)
                                            and ('accept' not in token)
                                            and ('comply' not in token)
                                            and ('html' not in token)
                                            and ('concl' not in token)
                                            and ('nih' not in token)
                                            and ('ackno' not in token)
                                            and ('intro' not in token)
                                            and ('method' not in token)
                                            and ('background' not in token)
                                            and ('research' not in token)
                                            and ('review' not in token)
                                            and ('correct' not in token)
                                            and ('supplement' not in token)
                                            and ('plos' not in token)
                                            and ('editor' not in token)
                                            and ('edu' not in token)
                                            and ('month' not in token)
                                            and ('day' not in token)
                                            and ('univer' not in token))]
        #Removes stopwords
        tokens = [token for token in tokens if token not in en_stop]
        if do_lemma:
            #Lemmatize
            tokens = [get_lemma(token) for token in tokens]
            do_lemma = False
        else:
            first = False
    #Removes useless characters
    tokens = [remove_char(token) for token in tokens]
    #Keeps only nouns and accronyms of interest 
    if incl_nouns:
        tokens = [token for token in tokens if token in nouns]
    #Keeps only > 4 (missing acronyms?)
    tokens = [token for token in tokens if (len(token) > 4 or token in accr)]
    #Removes integers
    tokens = list(int_filter(tokens))
    #Returns the clean tokens
    return list(set(tokens)) #To avoid duplicates



def get_databases(email, tool, search_term, name):
    '''
    Description
    --------------
    This function calls the different database script to get data
    from them.

    Takes
    --------------
    email (string): address email from user, required by PM/PMC
    tool (string): name of the tool the data is used for, required by PM/PMC
    search_term (string): search terms that are used to identify what should
    be downloaded
    name (string): name of the search

    Returns
    --------------
    '''
    # PMC - h for XXXX papers, some PDF DL & Extraction
    print('##### PUBMED CENTRAL')
    start_time = datetime.now()
    Get_PMC(email, tool, search_term, name)
    print('PUBMED CENTRAL data fetched in ', datetime.now()-start_time)
    # PM - h for XXXX papers, some PDF DL & Extraction
    print('##### PUBMED')
    start_time = datetime.now()
    Get_PM(email, tool, search_term, name)
    print('PUBMED data fetched in ', datetime.now()-start_time)
    # Paperity - 7h for 3500 papers, PDF DL & Exctraction
    print('##### PAPERITY')
    start_time = datetime.now()
    print('!!!! Careful, this API was used March 2018 and was still' \
           + ' in development, so it might have changed !!!!')
    Get_PAP(email, tool, search_term, name)
    print('PAPERITY data fetched in ', datetime.now()-start_time)
    # JSTOR
    start_time = datetime.now()
    print('##### JSTOR Dumps')
    Get_JSTOR(email, tool, search_term, name)
    print('JSTOR data fetched in ', datetime.now()-start_time)
    # arXiV
    start_time = datetime.now()
    print('##### arXiV')
    Get_arXiV(email, tool, search_term, name)
    print('arXiV data fetched in ', datetime.now()-start_time)
    # TODO: social databases; medarXiV
    return

def title_clean(title):
    '''
    Description
    --------------
    This function processes the title with NLP

    Takes
    --------------
    title (string): the title to process

    Returns
    --------------
    prepare_text(title) (list): list of useful tokens from the title
    '''
    if not pd.isnull(title):
        return (";".join(prepare_text(title)))
    else:
        return

#################################################################################################################################################
#                                                            MAIN PROG                                                                          #
#################################################################################################################################################
def main():
    # sets user email acount, tool name, database name,
    #   and search terms for API request
    email, tool, search_term, name = args2query()
    # Sets output for PDF
    # if not os.path.exists(name):
    #    os.makedirs(name)
    if not os.path.exists('./DownloadedPDF/' + name + '/'):
        os.makedirs('./DownloadedPDF/' + name + '/')
    if not os.path.exists('./DB/'):
        os.makedirs('./DB/')
    # Connects to the database:
    engine = create_engine('sqlite:///DB/papers_DB_'+ name + '.db')
    #Fetches the databases    
    print('############### Fetching data from databases')
    get_databases(email, tool, search_term, name)
    try:
        query = 'SELECT * FROM PMC;'
        dataFrame_PMC = pd.read_sql(query, engine)
    except:
        dataFrame_PMC = pd.DataFrame()
        print ('No PMC table in the database')
    try:
        query = 'SELECT * FROM PM;'
        dataFrame_PM = pd.read_sql(query, engine)
    except:
        dataFrame_PM = pd.DataFrame()
        print ('No PM table in the database')
    try:
        query = 'SELECT * FROM RDF;'
        dataFrame_RDF = pd.read_sql(query, engine)
    except:
        dataFrame_RDF = pd.DataFrame()
        print ('No RDF table in the database')
    try:
        query = 'SELECT * FROM PAP;'
        dataFrame_PAP = pd.read_sql(query, engine)
    except:
        dataFrame_PAP = pd.DataFrame()
        print ('No PAPERITY table in the database')
    try:
        query = 'SELECT * FROM JSTOR;'
        dataFrame_JSTOR = pd.read_sql(query, engine)
    except:
        dataFrame_JSTOR = pd.DataFrame()
        print ('No JSTOR table in the database')
    try:
        query = 'SELECT * FROM arXiV;'
        dataFrame_arXiV = pd.read_sql(query, engine)
    except:
        dataFrame_arXiV = pd.DataFrame()
        print ('No arXiV table in the database')
    #Merges all the data
    print('############### Merging data')
    start_time = datetime.now()
    dataFrame = pd.concat([dataFrame_PMC, dataFrame_PM, \
                               dataFrame_RDF, dataFrame_PAP, \
                               dataFrame_JSTOR, dataFrame_arXiV], \
                               ignore_index= True, \
                               sort=False)
    print('Merged done in ', datetime.now()-start_time) #15 min for about 18000 papers
    #This clean is useful for deduplication
    print('############### Reshaping title list')
    start_time = datetime.now()
    dataFrame['Title_Clean'] = dataFrame['Title'].apply(title_clean)
    print('Reshape done in ', datetime.now()-start_time)
    #Checks for duplicates
    print('############### Removing duplicates')
    start_time = datetime.now()
    before_dedup = dataFrame.shape[0]
    dataFrame = dataFrame[dataFrame['PMID'].isnull() | ~dataFrame[dataFrame['PMID'].notnull()].duplicated(subset='PMID',keep='first')] #here we keep first beacuse PMC appears before PM in the dataframe.
    dataFrame = dataFrame[dataFrame['PMCID'].isnull() | ~dataFrame[dataFrame['PMCID'].notnull()].duplicated(subset='PMCID',keep='first')] #here we keep first beacuse PMC appears before PM in the dataframe.
    dataFrame['Title'] = dataFrame['Title'].str.lower()
    dataFrame = dataFrame[dataFrame['Title'].isnull() | ~dataFrame[dataFrame['Title'].notnull()].duplicated(subset='Title',keep='first')] #here we keep first because PMC appears before PM in the dataframe.
    #For unknown DOIs, the selection is based on Title
    df_gpTitle = dataFrame.set_index('Title_Clean').sort_index().reset_index()
    #No easy way to do it with groupby, unfortunately, because groupby requires stupid aggregation
    for i in range(df_gpTitle.shape[0]):
        try:
            if df_gpTitle.loc[i,'Title_Clean'] == df_gpTitle.loc[i+1,'Title_Clean']: #with equal Titles
                #done on i+1, so that the last one will be the most complete, and be kept after duplicate check
                for j in range(df_gpTitle.shape[1]):
                    if not pd.isnull(df_gpTitle.iloc[i,j]):
                        if pd.isnull(df_gpTitle.iloc[i+1,j]):
                            df_gpTitle.iloc[i+1,j] = df_gpTitle.iloc[i,j]                    
        except KeyError:
            pass
    #Checks for duplicates and keep the last one, the most complete, after the previous loop
    df_gpTitle = df_gpTitle.drop_duplicates(subset='Title_Clean', keep='last')
    df_gpTitle = df_gpTitle.drop_duplicates(subset='Title_Clean', keep='last')
    dataFrame = df_gpTitle.copy()
    after_dedup = dataFrame.shape[0]
    print('There were ' + str(before_dedup - after_dedup) + ' duplicates')
    print('Deduplication done in ', datetime.now()-start_time) #1 min for about 18000 entries
    dataFrame = dataFrame.drop(columns = ['Title_Clean'])
    dataFrame.to_sql('Complete', engine, if_exists='replace', chunksize = 10)
   
    return

## Always executed
if __name__ == "__main__":
    main()
