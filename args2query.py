#################################################################################################################################################
#                                                              IMPORT PACKAGES                                                                  #
#################################################################################################################################################

#system imports
import os, errno, sys
#used for arguments parsing
import argparse

#################################################################################################################################################
#                                                          SUBROUTINES                                                                          #
#################################################################################################################################################
def args2query():
    '''
    Description
    --------------
    This function processes the argument from the command line

    Takes
    --------------

    Returns
    --------------
    args.email (string): email address of the user
    args.tool (string): name of the tool the script is used for
    search_term (string): search terms to send to online providers
    args.name (string): name of the output

    TO DO (maybe?)
    --------------
    #1 - Add years
    #2 - Add authors
    #3 - Choice of output
    '''
    #initialises the parser
    parser = argparse.ArgumentParser(description="Query args",
                                     add_help=True)
    #adds required argument 
    parser.add_argument('--keywords', '-k', required=True,
                        help='Search keyword', action='store',
                        dest='keyW')
    parser.add_argument('--email', '-m', required=True,
                        help='EMail adress', action='store',
                        dest='email')
    parser.add_argument('--tool', '-t', required=True,
                        help='Tool for which the query is used',
                        action='store', dest='tool')
    parser.add_argument('--name', '-n', required=True,
                        help='Name to use for outputs (no space)',
                        action='store', dest='name')
    #printing the parsed arguments
    args = parser.parse_args()
    #print ('Searching on the %s database' % args.DB)
    print ('Keywords : %s ' % args.keyW)
    print ('Email address: %s' % args.email)
    print ('Used for: %s' % args.tool)
    #Checking if requred and necessary arguments are parsed
    if (args.keyW and args.name and args.email and args.tool):
        search_term = "+".join(args.keyW.split())
        
        return (args.email, args.tool, search_term, args.name)
    else :
        print('Incomplete search. Please enter required information.')
        return
     

def args2query_dump():
    '''
    Description
    --------------
    This function processes the command lines argument for dumping data from the database

    Takes
    --------------
    Returns
    --------------
    args.table (string): which table of the database to dump
    args.database (string): which database to dump
    output (string): output file without extension
    dedup (y/n): if the entries must be deduplicated or not
    '''
    #initialises the parser
    parser = argparse.ArgumentParser(description="Query args",
                                     add_help=True)
    #adds required argument 
    parser.add_argument('--table_name', '-tb', required=True,
                        help='Table name to extract', action='store',
                        dest='table')
    parser.add_argument('--database_name', '-db', required=True,
                        help='Database name', action='store',
                        dest='database')
    parser.add_argument('--output', '-o', required=False,
                        help='Output name', action='store',
                        dest='output')
    parser.add_argument('--dedup', '-dd', required=False,
                        help='Do you want deduplication (y/n)', action='store',
                        dest='dedup')
    #printing the parsed arguments
    args = parser.parse_args()
    #Checking if requred and necessary arguments are parsed
    if (args.table and args.database):
        if (args.output):
            output = args.output
        else:
            output = 'output'
        if (args.dedup):
            dedup = args.dedup
        else:
            dedup = 'n'   
        return (args.table, args.database, output, dedup)
    else :
        print('Incomplete search. Please enter required information.')
        return
