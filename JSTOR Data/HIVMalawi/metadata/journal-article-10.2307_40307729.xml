<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40307729</article-id>
         <article-id pub-id-type="pub-doi">10.1086/590943</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Emergence of Drug Resistance in HIV Type 1–Infected Patients after Receipt of First-Line Highly Active Antiretroviral Therapy: A Systematic Review of Clinical Trials</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ravindra</given-names>
                  <surname>Gupta</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrew</given-names>
                  <surname>Hill</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anthony W.</given-names>
                  <surname>Sawyer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Deenan</given-names>
                  <surname>Pillay</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">47</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40012519</issue-id>
         <fpage>712</fpage>
         <lpage>722</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40307729"/>
         <abstract>
            <p>Background. Resistance to antiretroviral combination therapy is associated with increased mortality. Understanding the relative risks of emerging resistance to first-line therapy is of importance for both resource-rich and resource-poor settings. Methods. We undertook an overview of clinical trials of adults receiving first-line highly active antiretroviral therapy (HAART), which consisted of dual nucleoside reverse-transcriptase inhibitors (NRTIs) combined with a third agent (either a nonnucleoside reverse-transcriptase inhibitor [NNRTI] or a ritonavir-boosted protease inhibitor [bPI]). The primary outcome measures were incidences of mutations conferring resistance to key drugs (NRTIs, NNRTIs, or bPIs) per trial at week 48. For meta-analysis, inverse-variance weighting was used to create estimates of overall incidences per group, with exact 95% confidence intervals (95% CIs). Results. The study included 20 clinical trials that comprised 30 treatment arms and 7970 patients. Virologic failure at 48 weeks occurred in 4.9% (95% CI, 3.9%-6.1%) of NNRTI recipients, compared with 5.3% (95% CI, 4.4%-6.4%; P = .50) of bPI recipients. Of failures that were successfully genotyped, the M184V mutation in the HIV reverse transcriptase (lamivudine resistance) occurred in 35.3% (95% CI, 29.3%-41.6%) of patients who started NNRTI-based HAART, compared with 21.0% (95% CI, 14.4%-28.8%; P&lt;.001) for those who received a bPI. For the K65R mutation in the HIV reverse transcriptase (multinudeoside resistance), incidences were 5.3% (95% CI, 2.4%-9.9%) and 0.0% (95% CI, 0.0%-3.6%; P = .01), respectively, in patients treated with nonzidovudine-containing regimens. Resistance to the third agent (an NNRTI or PI) occurred in 53% (95% CI, 46%-60%) and 0.9% (95% CI, 0.0%-6.2%; P&lt; .001) of such patients, respectively. Conclusions. Initial therapy with bPI-based regimens resulted in less resistance within and across drug classes. This finding is of particular significance for the developing world, where rates of resistance to NRTIs and NNRTIs at 48 weeks are much higher than has been seen in both cohorts and clinical trials in well-resourced countries.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d708e226a1310">
            <label>1</label>
            <mixed-citation id="d708e233" publication-type="other">
Palella FJ Jr, Delaney KM, Moorman AC, et al. Declining morbidity
and mortality among patients with advanced human immunodefi-
ciency virus infection. HIV Outpatient Study Investigators. N Engl J
Med 1998; 338:853-60.</mixed-citation>
         </ref>
         <ref id="d708e249a1310">
            <label>2</label>
            <mixed-citation id="d708e256" publication-type="other">
Department of Health and Human Services (DHHS). Guidelines for
the use of antiretroviral agents in HIV-infected adults and adolescents.
Available at: http://www.aidsinfo.nih.gov/contentfiles/Adultand
AdolescentGLpdf. Accessed 15 July 2008.</mixed-citation>
         </ref>
         <ref id="d708e272a1310">
            <label>3</label>
            <mixed-citation id="d708e279" publication-type="other">
Hammer SM, Saag MS, Schechter M, et al; International AIDS Soci-
ety–USA panel. Treatment for adult HIV infection: 2006 recommen-
dations of the International AIDS Society–USA panel. JAMA 2006;
296:827-43.</mixed-citation>
         </ref>
         <ref id="d708e295a1310">
            <label>4</label>
            <mixed-citation id="d708e302" publication-type="other">
World Health Organization. Antiretroviral therapy for HIV infection
in adults and adolescents: recommendations for a public health ap-
proach. Available at: http://www.who.int/hiv/pub/guidelines/artadult
guidelines.pdf. Accessed 12 December 2007.</mixed-citation>
         </ref>
         <ref id="d708e319a1310">
            <label>5</label>
            <mixed-citation id="d708e326" publication-type="other">
Bartlett JA, Buda JJ, von Scheele B, et al. Minimizing resistance con-
sequences after virologic failure on initial combination therapy: a sys-
tematic overview. J Acquir Immune Defic Syndr 2006; 41:323-31.</mixed-citation>
         </ref>
         <ref id="d708e339a1310">
            <label>6</label>
            <mixed-citation id="d708e346" publication-type="other">
Kozal MJ, Hullsiek KH, Macarthur RD, Berg-Wolf M, Peng G, Xiang
Y; for the Terry Beirn Community Programs for Clinical Research on
AIDS (CPCRA). The incidence of HIV drug resistance and its impact
on progression of HIV disease among antiretroviral-naive participants
started on three different antiretroviral therapy strategies. HIV Clin
Trials 2007; 8:357-70.</mixed-citation>
         </ref>
         <ref id="d708e369a1310">
            <label>7</label>
            <mixed-citation id="d708e376" publication-type="other">
Hogg RS, Bangsberg DR, Lima VD, et al. Emergence of drug resistance
is associated with an increased risk of death among patients first starting
HAART. PLoS Med 2006;3:e356.</mixed-citation>
         </ref>
         <ref id="d708e389a1310">
            <label>8</label>
            <mixed-citation id="d708e396" publication-type="other">
Phillips AN, Dunn D, Sabin C, et al; UK Collaborative Group on HIV
Drug Resistance; UK CHIC Study Group. Long term probability of
detection of HIV-1 drug resistance after starting antiretroviral therapy
in routine clinical practice. AIDS 2005; 19:487-94.</mixed-citation>
         </ref>
         <ref id="d708e412a1310">
            <label>9</label>
            <mixed-citation id="d708e419" publication-type="other">
Vella S, Palmisano L. The global status of resistance to antiretroviral
drugs. Clin Infect Dis 2005;41(Suppl 4):S239-46.</mixed-citation>
         </ref>
         <ref id="d708e429a1310">
            <label>10</label>
            <mixed-citation id="d708e436" publication-type="other">
von Wyl V, Yerly S, Bóni J, Bürgisser P, Klimkait T, Battegay M; Swiss
HIV Cohort Study. Emergence of HIV-1 drug resistance in previously
untreated patients initiating combination antiretroviral treatment: a
comparison of different regimen types. Arch Intern Med 2007; 167:
1782-90.</mixed-citation>
         </ref>
         <ref id="d708e456a1310">
            <label>11</label>
            <mixed-citation id="d708e463" publication-type="other">
Haubrich R, Riddler SA, DiRienzo AG, et al. A prospective randomized,
phase III trial of NRTI-, PI-and NNRTI-sparing regimens for initial
treatment of HIV-1 infection (ACTG 5142) [abstract THLB0204]. In:
Program and abstracts of the XVI International AIDS Conference (To-
ronto). Stockholm: International AIDS Society, 2006.</mixed-citation>
         </ref>
         <ref id="d708e482a1310">
            <label>12</label>
            <mixed-citation id="d708e489" publication-type="other">
US Food and Drug Administration. Drugs used in the treatment of
HIV infection. January 2008. Available at: http://www.fda.gov/oashi/
aids/virals.html. Accessed 12 December 2007.</mixed-citation>
         </ref>
         <ref id="d708e502a1310">
            <label>13</label>
            <mixed-citation id="d708e509" publication-type="other">
Johnson VA, Brun-Vezinet F, Clotet B, et al. Update of the drug re-
sistance mutations in HIV-1: fall 2006. Top HIV Med 2006; 14:125-30.</mixed-citation>
         </ref>
         <ref id="d708e519a1310">
            <label>14</label>
            <mixed-citation id="d708e526" publication-type="other">
Armitage P, Berry G. Statistical methods in medical research. 3rd ed.
Oxford, UK: Blackwell Scientific Publications, 1994.</mixed-citation>
         </ref>
         <ref id="d708e536a1310">
            <label>15</label>
            <mixed-citation id="d708e543" publication-type="other">
Gallant JE, Staszewski S, Pozniak AL, et al.; 903 Study Group. Efficacy
and safety of tenofovir DF vs stavudine in combination therapy in
antiretroviral-naive patients: a 3-year randomized trial. JAMA 2004;
292:191-201.</mixed-citation>
         </ref>
         <ref id="d708e559a1310">
            <label>16</label>
            <mixed-citation id="d708e566" publication-type="other">
Miller MD, Margot NA, McColl DJ, Tran S, Coakley DF, Cheng AK.
Genotypic and phenotypic characterisation of virological failure
through 48 weeks among treatment naive patients taking tenofovir DF
or stavudine in combination with lamivudine and efavirenz. In: Pro-
gram and abstracts of the 6th International Congress on Drug Therapy
in HIV infection (Glasgow, UK). 2001.</mixed-citation>
         </ref>
         <ref id="d708e590a1310">
            <label>17</label>
            <mixed-citation id="d708e597" publication-type="other">
Miller MD, Margot NA, McColl DJ, Coakley DF, Cheng AK. Char-
acterisation of virological failure through 96 weeks among treatment
naive patients taking tenofovir DF (TDF) or stavudine (d4T) in com-
bination with lamivudine (3TC) and efavirenz (EFV) [poster 554]. In:
Program and abstracts of the 2nd International AIDS Society Confer-
ence on the Pathogenesis and Treatment of HIV infection (Paris).
Stockholm: International AIDS Society, 2003.</mixed-citation>
         </ref>
         <ref id="d708e623a1310">
            <label>18</label>
            <mixed-citation id="d708e630" publication-type="other">
Gallant JE, Dejesus E, Arribas JR; Study 934 Group. Tenofovir DF,
emtricitabine, and efavirenz vs. zidovudine, lamivudine, and efavirenz
for HIV. N Engl J Med 2006; 354:251-60.</mixed-citation>
         </ref>
         <ref id="d708e643a1310">
            <label>19</label>
            <mixed-citation id="d708e650" publication-type="other">
Pozniak AL, Gallant JE, Dejesus E, et al. Tenofovir disoproxil fumarate,
emtricitabine, and efavirenz versus fixed-dose zidovudine/lamivudine
and efavirenz in antiretroviral-naive patients: virologic, immunologic,
and morphologic changes—a 96-week analysis. J Acquir Immune Defic
Syndr 2006; 43:535-40.</mixed-citation>
         </ref>
         <ref id="d708e669a1310">
            <label>20</label>
            <mixed-citation id="d708e676" publication-type="other">
Dejesus E, Herrera G, Teófilo E, et al.; CNA30024 Study Team. Abacavir
versus zidovudine combined with lamivudine and efavirenz, for the
treatment of antiretroviral-naive HIV-infected adults. Clin Infect Dis
2004; 39:1038-46.</mixed-citation>
         </ref>
         <ref id="d708e692a1310">
            <label>21</label>
            <mixed-citation id="d708e699" publication-type="other">
Moyle GJ, Dejesus E, Cahn P, et al.; Ziagen Once-Daily in Antiretroviral
Combination Therapy (CNA30021) Study Team. Abacavir once or
twice daily combined with once-daily lamivudine and efavirenz for the
treatment of antiretroviral-naive HIV-infected adults: results of the
Ziagen Once Daily in Antiretroviral Combination Study. J Acquir Im-
mune Defic Syndr 2005; 38:417-25.</mixed-citation>
         </ref>
         <ref id="d708e722a1310">
            <label>22</label>
            <mixed-citation id="d708e729" publication-type="other">
Dejesus E, McCarty D, Farthing CF, et al.; EPV20001 International
Study Team. Once-daily versus twice-daily lamivudine, in combination
with zidovudine and efavirenz, for the treatment of antiretroviral-naive
adults with HIV infection: a randomized equivalence trial. Clin Infect
Dis 2004; 39:411-8.</mixed-citation>
         </ref>
         <ref id="d708e749a1310">
            <label>23</label>
            <mixed-citation id="d708e756" publication-type="other">
Vavro C, McCarty D, Shortino D, Hetherington S. Genotypic analysis
of HIV-1 from subjects experiencing virologic breakthrough while tak-
ing 3TC QD vs 3TC BID, EFV and ZDV [abstract H-2052]. In: Program
and abstracts of the 42nd Interscience Conference on Antimicrobial
Agents and Chemotherapy (San Diego). Washington, DC: American
Society for Microbiology, 2002.</mixed-citation>
         </ref>
         <ref id="d708e779a1310">
            <label>24</label>
            <mixed-citation id="d708e786" publication-type="other">
van Leth F, Phanuphak P, Ruxrungtham K, et al; 2NN Study team.
Comparison of first-line antiretroviral therapy with regimens including
nevirapine, efavirenz, or both drugs, plus stavudine and lamivudine:
a randomised open-label trial, the 2NN Study. Lancet 2004; 363:
1253-63.</mixed-citation>
         </ref>
         <ref id="d708e805a1310">
            <label>25</label>
            <mixed-citation id="d708e812" publication-type="other">
Hall D, van Leth F, Schere J, et al. Genotypic analysis of reverse tran-
scriptase in treatment-naive HIV-1 patients treated with lamivudine,
stavudine and nevirapine and/or efavirenz [poster 694]. In: Program
and abstracts of the 11th Conference on Retroviruses and Opportu-
nistic Infections (San Francisco). Alexandria, VA: Foundation for Re-
trovirology and Human Health, 2004:694.</mixed-citation>
         </ref>
         <ref id="d708e835a1310">
            <label>26</label>
            <mixed-citation id="d708e842" publication-type="other">
Podzamczer D, Ferrer E, Consiglio E, et al. A randomized clinical trial
comparing nelfinavir or nevirapine associated to zidovudine/lamivu-
dine in HIV-infected naive patients (the Combine Study). Antivir Ther
2002; 7:81-90.</mixed-citation>
         </ref>
         <ref id="d708e858a1310">
            <label>27</label>
            <mixed-citation id="d708e867" publication-type="other">
Ferrer E, Podzamczer D, Arnedo M, et al; Combine Study Team. Ge-
notype and phenotype at baseline and at failure in human immuno-
deficiency virus–infected antiretroviral-naive patients in a randomized
trial comparing zidovudine and lamivudine plus nelfinavir or nevi-
rapine. J Infect Dis 2003; 187:687-90.</mixed-citation>
         </ref>
         <ref id="d708e886a1310">
            <label>28</label>
            <mixed-citation id="d708e893" publication-type="other">
Cameron W, da Silva B, Arribas J, et al. A two-year randomized con-
trolled clinical trial in antiretroviral-naive subjects using lopinavir/ri-
tonavir (LPV/r) monotherapy after initial induction treatment com-
pared to an efavirenz (EFV) 3-drug regimen (Study M03-613) [abstract
THLB0201]. In: Program and abstracts of the XVI International AIDS
Conference (Toronto). Stockholm: International AIDS Society, 2006.</mixed-citation>
         </ref>
         <ref id="d708e917a1310">
            <label>29</label>
            <mixed-citation id="d708e924" publication-type="other">
Riddler SA, Haubrih R, DiRienzo AG, et al. Drug resistance at viro-
logical failure in a randomized, phase III trial of NRTI-, PI-and
NNRTI-sparing regimens for initial treatment of HIV-1 infection
(ACTG 5142) [abstract THLB0204]. In: Program and abstracts of the
XVI International AIDS Conference (Toronto). Stockholm: Interna-
tional AIDS Society, 2006.</mixed-citation>
         </ref>
         <ref id="d708e947a1310">
            <label>30</label>
            <mixed-citation id="d708e954" publication-type="other">
Walmsley S, Ruxruntham K, Slim J, Ward D, Larson P, Raffi F. Sa-
quinavir/r versus lopinavir/r plus emtricitabine/tenofovir as initial ther-
apy in HIV-1 infected patients [abstract PS 1/4]. In: Program and ab-
stracts of the 11th European AIDS Conference (Madrid). 2007.</mixed-citation>
         </ref>
         <ref id="d708e970a1310">
            <label>31</label>
            <mixed-citation id="d708e977" publication-type="other">
Eron J Jr, Yeni P, Gathe J Jr, et al. The KLEAN study of fosamprenavir-
ritonavir versus lopinavir-ritonavir, each in combination with abacavir-
lamivudine, for initial treatment of HIV infection over 48 weeks: a
randomised non-inferiority trial. Lancet 2006; 368:476-82. (Erratum:
Lancet 2006; 368:1238).</mixed-citation>
         </ref>
         <ref id="d708e996a1310">
            <label>32</label>
            <mixed-citation id="d708e1003" publication-type="other">
Yeni P, Eron J, Clotet B, et al. The KLEAN Study: FPV/r BID vs LPV/
r BID +(ABC/3TC) QD in ART naive subjects. 48 week analysis dem-
onstrates rare resistance and non-inferiority [abstract H-1056]. In: Pro-
gram and abstracts of the 46th Interscience Conference on Antimi-
crobial Agents and Chemotherapy (San Francisco). Washington, DC:
American Society for Microbiology, 2006.</mixed-citation>
         </ref>
         <ref id="d708e1026a1310">
            <label>33</label>
            <mixed-citation id="d708e1033" publication-type="other">
Gathe JC Jr, Ive P, Wood R, et al. SOLO: 48-week efficacy and safety
comparison of once-daily fosamprenavir/ritonavir versus twice-daily
nelfinavir in naive HIV-1 -infected patients. AIDS 2004; 18:1529-37.</mixed-citation>
         </ref>
         <ref id="d708e1046a1310">
            <label>34</label>
            <mixed-citation id="d708e1053" publication-type="other">
Gathe JC Jr, Wood R, Sanne I, et al. Long-term (120-week) antiviral
efficacy and tolerability of fosamprenavir/ritonavir once daily in ther-
apy-naive patients with HIV-1 infection: an uncontrolled, open-label,
single-arm follow-on study. Clin Ther 2006; 28:745-54.</mixed-citation>
         </ref>
         <ref id="d708e1070a1310">
            <label>35</label>
            <mixed-citation id="d708e1077" publication-type="other">
MacManus S, Yates PJ, Elston RC, White S, Richards N, Snowden W.
GW433908/ritonavir once daily in antiretroviral therapy-naive HIV-
infected patients: absence of protease resistance at 48 weeks. AIDS
2004; 18:651-5.</mixed-citation>
         </ref>
         <ref id="d708e1093a1310">
            <label>36</label>
            <mixed-citation id="d708e1100" publication-type="other">
Elion R, De Jesus E, Sension M, et al. Once-daily abacavir/lamivudine
(ABC/3TC) and boosted atazanavir (ATV/RTV) in antiretroviral-naive
HIV-1 infected subjects: 48-week results from COL102060 (SHARE)
[abstract WePeB033]. In: Programs and abstracts of the XV Interna-
tional AIDS Society Conference on HIV Pathogenesis and Treatment
(Sydney, Australia). Stockholm: International AIDS Society, 2007.</mixed-citation>
         </ref>
         <ref id="d708e1123a1310">
            <label>37</label>
            <mixed-citation id="d708e1130" publication-type="other">
Smith K, Weinberg W, Dejesus E, et al. Fosamprenavir or atazanavir
boosted with ritonavir given once daily with tenofovir/emtricitabine
in antiretroviral naive HIV-infected patients: ALERT Study Virology
Analysis through 48 Weeks [poster H360]. In: Program and abstracts
of the Interscience Conference on Antimicrobial Agents and Che-
motherapy (Chicago) Washington, DC: American Society for Micro-
biology, 2007.</mixed-citation>
         </ref>
         <ref id="d708e1156a1310">
            <label>38</label>
            <mixed-citation id="d708e1163" publication-type="other">
Malan DR, Krantz E, David N, Wirtz V, Hammond J, McGrath D; 089
Study Group. Efficacy and safety of atazanavir, with or without rito-
navir, as part of once-daily highly active antiretroviral therapy regimens
in antiretroviral-naive patients. J Acquir Immune Defic Syndr 2008;
47:161-7.</mixed-citation>
         </ref>
         <ref id="d708e1182a1310">
            <label>39</label>
            <mixed-citation id="d708e1191" publication-type="other">
Delfraissy JF, Flandre P, Delaugerre C, et al. MONARK Trial (MON-
otherapy AntiRetroviral Kaletra): 48-week analysis of lopinavir/rito-
navir (LPV/r) monotherapy compared to LPV/r + zidovudine/lami-
vudine (AZT/3TC) in antiretroviral-naive patients [late breaker
abstract THLB0202]. In: Program and abstracts of the XVI Interna-
tional AIDS Conference (Toronto). Stockholm: International AIDS So-
ciety, 2006.</mixed-citation>
         </ref>
         <ref id="d708e1217a1310">
            <label>40</label>
            <mixed-citation id="d708e1224" publication-type="other">
Norton M, Delaugerre C, Barot G, et al. Drug resistance outcomes in
a trial comparing lopinavir/ritonavir (LPV/r) monotherapy to LPV/r
+ zidovudine/lamivudine (MONARK Trial) [abstract 74]. In: Program
and abstracts of the 15th HIV Drug Resistance Workshop (Sitges,
Spain). San Diego: University of California, San Diego, 2006.</mixed-citation>
         </ref>
         <ref id="d708e1244a1310">
            <label>41</label>
            <mixed-citation id="d708e1251" publication-type="other">
Walmsley S, Bernstein B, King M, et al.; M98-863 Study Team. Lo-
pinavir-ritonavir versus nelfinavir for the initial treatment of HIV in-
fection. N Engl J Med 2002; 346:2039-46.</mixed-citation>
         </ref>
         <ref id="d708e1264a1310">
            <label>42</label>
            <mixed-citation id="d708e1271" publication-type="other">
Kempf DJ, King MS, Bernstein B, et al. Incidence of resistance in a
double-blind study comparing lopinavir/ritonavir plus stavudine and
lamivudine to nelfinavir plus stavudine and lamivudine. J Infect Dis
2004; 189:51-60.</mixed-citation>
         </ref>
         <ref id="d708e1287a1310">
            <label>43</label>
            <mixed-citation id="d708e1294" publication-type="other">
Johnson MA, Gathe JC Jr, Podzamczer D, et al. A once-daily lopinavir/
ritonavir-based regimen provides noninferior antiviral activity com-
pared with a twice-daily regimen. J Acquir Immune Defic Syndr
2006; 43:153-60.</mixed-citation>
         </ref>
         <ref id="d708e1310a1310">
            <label>44</label>
            <mixed-citation id="d708e1317" publication-type="other">
Molina JM, Gathe J, Lim PL, et al. Comprehensive resistance testing
in antiretroviral naive patients treated with once-daily lopinavir/rito-
navir plus tenofovir and emtricitabine: 48-week results from study 418
[abstract WePeB5701]. In: Program and abstracts of the XV Interna-
tional AIDS Conference (Bangkok). Stockholm: International AIDS
Society, 2004.</mixed-citation>
         </ref>
         <ref id="d708e1340a1310">
            <label>45</label>
            <mixed-citation id="d708e1347" publication-type="other">
De Jesus E, Ortiz R, Khanlou H, et al. Efficacy and safety of darunavir/
ritonavir versus lopinavir/ritonavir in ARV treatment-naive HIV-1-
infected patients at week 48: ARTEMIS (TMC114-C211) [abstract H-
718b]. In: Program and abstracts of the 46th Interscience Conference
on Antimicrobial Agents and Chemotherapy (Chicago). Washington,
DC: American Society for Microbiology, 2006.</mixed-citation>
         </ref>
         <ref id="d708e1370a1310">
            <label>46</label>
            <mixed-citation id="d708e1377" publication-type="other">
Bartlett JA, Johnson J, Herrera G, et al.; Clinically Significant Long-
Term Antiretroviral Sequential Sequencing Study (CLASS) Team. Long-
term results of initial therapy with abacavir and lamivudine combined
with efavirenz, amprenavir/ritonavir, or stavudine. J Acquir Immune
Defic Syndr 2006; 43:284-92.</mixed-citation>
         </ref>
         <ref id="d708e1397a1310">
            <label>47</label>
            <mixed-citation id="d708e1404" publication-type="other">
Lanier R, Irlbeck D, Liao Q, et al. Emergence of resistance-associated
mutations over 96 weeks of therapy in patients initiating ABC/3TC +
d4T, EFV or APV/r [poster H-910]. In: Program and abstracts of the
43rd Interscience Conference on Antimicrobial Agents and Chemo-
therapy. Washington, DC: American Society for Microbiology, 2003.</mixed-citation>
         </ref>
         <ref id="d708e1423a1310">
            <label>48</label>
            <mixed-citation id="d708e1430" publication-type="other">
Metzner KJ, Bonhoeffer S, Fischer M, et al; The Swiss HIV Cohort
Study. Emergence of minor populations of human immunodeficiency
virus type 1 carrying the Ml 84V and L90M mutations in subjects
undergoing structured treatment interruptions. J Infect Dis 2003; 188:
1433-43.</mixed-citation>
         </ref>
         <ref id="d708e1449a1310">
            <label>49</label>
            <mixed-citation id="d708e1456" publication-type="other">
Metzner KJ, Rauch P, Walter H, et al. Detection of minor populations
of drug-resistant HIV-1 in acute seroconverters. AIDS 2005; 19:
1819-25.</mixed-citation>
         </ref>
         <ref id="d708e1469a1310">
            <label>50</label>
            <mixed-citation id="d708e1476" publication-type="other">
Charpentier C, Dwyer DE, Mammano F, Lecossier D, Clavel F, Hance
AJ. Role of minority populations of human immunodeficiency virus
type 1 in the evolution of viral resistance to protease inhibitors. J Virol
2004; 78:4234-47.</mixed-citation>
         </ref>
         <ref id="d708e1492a1310">
            <label>51</label>
            <mixed-citation id="d708e1499" publication-type="other">
World Health Organization, UNAIDS, UNICEF. Towards universal
screening: scaling up priority HIV/AIDS interventions in the health
sector. 2008. Available at: http://www.who.int/hiv/pub/towards
_universal_access_report_2008.pdf. Accessed 15 July 2008.</mixed-citation>
         </ref>
         <ref id="d708e1515a1310">
            <label>52</label>
            <mixed-citation id="d708e1522" publication-type="other">
Ferradini L, Jeannin A, Pinoges L, et al. Scaling up of highly active
antiretroviral therapy in a rural district of Malawi: an effectiveness
assessment. Lancet 2006; 367:1335-42.</mixed-citation>
         </ref>
         <ref id="d708e1536a1310">
            <label>53</label>
            <mixed-citation id="d708e1543" publication-type="other">
Kamya MR, Mayanja-Kizza H, Kambugu A, et al; Academic Alliance
for AIDS Care and Prevention in Africa. Predictors of long-term viral
failure among Ugandan children and adults treated with antiretroviral
therapy. J Acquir Immune Defic Syndr 2007; 46:187-93.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
