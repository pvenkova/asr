<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7hbgkn</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt9qddb5</book-id>
      <subj-group>
         <subject content-type="call-number">RA643.86.A35M675 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">AIDS (Disease)</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Medical anthropology</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Anthropology</subject>
         <subject>Health Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Morality, Hope and Grief</book-title>
         <subtitle>Anthropologies of AIDS in Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Dilger</surname>
               <given-names>Hansjörg</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Luig</surname>
               <given-names>Ute</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>05</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780857457967</isbn>
      <isbn content-type="epub">9781845458294</isbn>
      <isbn content-type="epub">184545829X</isbn>
      <publisher>
         <publisher-name>Berghahn Books</publisher-name>
         <publisher-loc>New York; Oxford</publisher-loc>
      </publisher>
      <edition>NED - New edition, 1</edition>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>Hansjörg Dilger</copyright-holder>
         <copyright-holder>Ute Luig</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt9qddb5"/>
      <abstract abstract-type="short">
         <p>The HIV/AIDS epidemic in sub-Saharan Africa has been addressed and perceived predominantly through the broad perspectives of social and economic theories as well as public health and development discourses. This volume however, focuses on the micro-politics of illness, treatment and death in order to offer innovative insights into the complex processes that shape individual and community responses to AIDS. The contributions describe the dilemmas that families, communities and health professionals face and shed new light on the transformation of social and moral orders in African societies, which have been increasingly marginalised in the context of global modernity.</p>
      </abstract>
      <counts>
         <page-count count="356"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dilger</surname>
                           <given-names>Hansjörg</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Luig</surname>
                           <given-names>Ute</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.5</book-part-id>
                  <title-group>
                     <title>Introduction.</title>
                     <subtitle>Morality, Hope and Grief: Towards an Ethnographic Perspective in HIV/AIDS Research</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dilger</surname>
                           <given-names>Hansjörg</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Over the last couple of years, anthropological studies on HIV/AIDS in sub-Saharan Africa have taken an increasingly broad focus. While anthropological and social science publications during the first years of the epidemic tended to assume an applied and rather narrow approach to the disease by concentrating on risk behaviours, risk groups and prevention – increasingly also on care and support systems later – more recent research has focused on the way HIV/AIDS as a<italic>social reality</italic>has become embedded in specific social and cultural contexts and how these contexts in turn are being modified, transformed and challenged by the presence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.6</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Beyond Bare Life:</title>
                     <subtitle>AIDS, (Bio)Politics, and the Neoliberal Order</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Comaroff</surname>
                           <given-names>Jean</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>To contemplate the shape of late modern history – in Africa or elsewhere – is impossible without the polymorphous presence of HIV/ AIDS, signal pandemic of the global here-and-now. In retrospect, its onset was uncanny: the disease appeared like a memento mori in a world high on the hype of Reaganomics, deregulation and the end of the Cold War. In its wake, even careful observers made medieval associations: ‘AIDS’, wrote Susan Sontag (1989: 122), ‘reinstates something like a premodern experience of illness’, a throwback to an era when sickness was, by its nature, immutable, mysterious and fatal. Such reactions make</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.7</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Spiritual Insecurity and AIDS in South Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ashforth</surname>
                           <given-names>Adam</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>The biological assault upon African populations afflicted by the HIV epidemic, coupled with the attendant medical and educational responses, marks a moment of dramatic social and cultural change in South Africa. The demographic impact of the current levels of infection will be profound. One-fi fth of the South African adult population will soon be dead. Even with treatment, AIDS patients on antiretrovirals are only expected to live an additional four years. Life expectancy at birth is plummeting; infant mortality is rising. The epidemic is fracturing households and communities, placing everincreasing strain on support networks.</p>
                     <p>The ways in which people make</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.8</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>New Hopes and New Dilemmas:</title>
                     <subtitle>Disclosure and Recognition in the Time of Antiretroviral Treatment</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mogensen</surname>
                           <given-names>Hanne O.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>The AIDS epidemic is entering a new era due to the increasing availability of antiretroviral treatment in African countries. An AIDS diagnosis is no longer a death sentence, and in Uganda thousands of people have risen from their deathbed in the past one to two years (2005 and 2006). The predominant feeling among AIDS patients and their relatives in Uganda is presently one of new hopes – but also of new dilemmas. Complex issues of testing, recognition, disclosure and social support are changing form but are not disappearing. In this chapter I will use my ten years of experience with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.9</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Health Workers Entangled:</title>
                     <subtitle>Confidentiality and Certification</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Whyte</surname>
                           <given-names>Susan R.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Whyte</surname>
                           <given-names>Michael A.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kyaddondo</surname>
                           <given-names>David</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>80</fpage>
                  <abstract>
                     <p>It was distribution day at Ekataali hospital in Eastern Uganda. Every other Thursday the NGO running the AIDS project gave out material support to ‘clients’, people who had tested HIV positive at the AIDS Information Centre (AIC) in the hospital. On this particular Thursday in 2002, one of the counsellors at the centre, who was also a midwife at the hospital, had invited us to come along and see how the flour, sugar and soap were given out. As is so often the case, the activities got off to a slow start. While the counsellor was off trying to find</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.10</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>‘My Relatives Are Running Away from Me!’</title>
                     <subtitle>Kinship and Care in the Wake of Structural Adjustment, Privatisation and HIV/AIDS in Tanzania</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dilger</surname>
                           <given-names>Hansjörg</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>102</fpage>
                  <abstract>
                     <p>[Structural adjustment policies in Africa] were originally designed to stabilize developing countries’ economies. In reality, they imposed harsh measures which deepened poverty, undermined food security and self-reliance, and led to resource exploitation, environmental destruction and population displacement. … Prior to the 1980s, the district hospitals, community health centres and other outreach health posts provided medical services and essential drugs free of charge. With reforms, user fees and cost recovery were introduced, and the sale of drugs was liberalized. … By the end of the 1990s, the health systems in most sub-Saharan countries had virtually collapsed. Few people could afford annual</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.11</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>The Social History of an Epidemic:</title>
                     <subtitle>HIV/AIDS in Gwembe Valley, Zambia, 1982–2004</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Colson</surname>
                           <given-names>Elizabeth</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>Much said here will resonate with what has happened and is happening elsewhere in Africa, but I am dealing with the impact of AIDS at a particular place and time: the Gwembe Valley of Zambia’s Southern Province in the years 1982–2004. For data, I draw upon the Gwembe Long-Term Study initiated in 1956, long before AIDS was known (Scudder and Colson 1979, 2002; Cliggett 2002).</p>
                     <p>In the slightly over twenty years since the mid-1980s, when Zambians, including the people of Gwembe Valley, first became aware of a new threat to their existence identified under the acronym AIDS, they have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.12</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Living beyond AIDS in Maasailand:</title>
                     <subtitle>Discourses of Contagion and Cultural Identity</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Talle</surname>
                           <given-names>Aud</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>148</fpage>
                  <abstract>
                     <p>In this chapter I discuss how the pastoral Maasai of East Africa, in discourses and practices, negotiate cultural boundaries between themselves and others while engaging with the HIV/AIDS pandemic.¹ For the Maasai,<italic>biita</italic>² which comes from ‘outside’ and brings with it death and misfortune becomes imaginable and manageable within a larger discursive context of modern living and urban sociality. Within this discourse, ‘Maasai culture’ as represented by the Maasai themselves as well as by others simultaneously facilitates and prevents transmission of HIV. This ambiguity of Maasai traditions within the HIV/AIDS national discourse appears to open up abundant space for creatively</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.13</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Politics of Blame:</title>
                     <subtitle>Clashing Moralities and the AIDS Epidemic in Nso’ (North-West Province, Cameroon)</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Quaranta</surname>
                           <given-names>Ivo</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>173</fpage>
                  <abstract>
                     <p>This chapter is based on ethnographic fieldwork carried out in 1999–2000 in Kumbo, once the capital of the pre-colonial kingdom of Nso’, in the present Bui Division of the Anglophone north-western Grassfields of Cameroon (Quaranta 2006). The work analysed the local processes of social production and cultural interpretation of AIDS, through interviews carried out with medical personnel and directors of the two local hospitals (both belonging to religious institutions), more than forty traditional doctors (<italic>anngaashiv</italic>), the local ruler (HRH Sëm Mbinglo I) and some members of the royal palace (Nto’ Nso’) and secret societies members, as well as with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.14</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>Gossip, Rumour and Scandal:</title>
                     <subtitle>The Circulation of AIDS Narratives in a Climate of Silence and Secrecy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Reid</surname>
                           <given-names>Graeme</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>192</fpage>
                  <abstract>
                     <p>In South Africa AIDS initially affected those on the social margins – a pattern familiar in many parts of the world and typical of the cities of the Western world. Beginning in 1983, gays, drug addicts and sex workers were the first to be infected. Press reports at the onset of the epidemic announced a ‘gay plague’. But this was soon to change. Shortly after the first AIDS cases were diagnosed, migrant workers were subject to screening by mining houses. Large numbers of men from all over the subcontinent were tested for viral antibodies. From the mid 1980s, alarms began</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.15</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>‘We Are Tired of Mourning!’</title>
                     <subtitle>The Economy of Death and Bereavement in a Time of AIDS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Haram</surname>
                           <given-names>Liv</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>219</fpage>
                  <abstract>
                     <p>I shall begin this chapter with an event that occurred in October 2004 when I conducted anthropological fi eldwork, which I have been doing intermittently since 1989, among the Meru who live on the slopes of Mount Meru immediately east of Arusha town in northern Tanzania (Haram 1999, 2004, 2005a, 2005b). Together with my host family and a neighbouring woman, Mama Martha, I was having morning tea when lamenting cries cut through the air: ‘<italic>Ouiii-ouiii-ouuiii-ouii!</italic>’ We all shivered, and with despair, Mama Martha announced what we all feared, namely that yet another person had died. While some left to inquire</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.16</book-part-id>
                  <title-group>
                     <label>Chapter 11</label>
                     <title>Purity Is Danger:</title>
                     <subtitle>Ambiguities of Touch around Sickness and Death in Western Kenya</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Geissler</surname>
                           <given-names>P. Wenzel</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Prince</surname>
                           <given-names>Ruth J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>240</fpage>
                  <abstract>
                     <p>This chapter is based on fieldwork in Uhero, a Luo-speaking village in western Kenya, from 2000 to 2002. During this time, before the advent of free antiretroviral treatment, HIV/AIDS reached epidemic proportions in western Kenya, and bodily suffering and death were widely shared experiences, commonly referred to as ‘the death of today’. The chapter explores illness, death, care, and the dying and dead body through the stories of a married daughter and her elderly father, who died within few months from one another, of chest infections, diarrhoea and physical atrophy. The analytical focus is on touch, defined as direct, physical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.17</book-part-id>
                  <title-group>
                     <label>Chapter 12</label>
                     <title>Diseased and Dangerous:</title>
                     <subtitle>Images of Widows’ Bodies in the Context of the HIV Epidemic in Northern Zambia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Offe</surname>
                           <given-names>Johanna A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>270</fpage>
                  <abstract>
                     <p>In Kasama, the capital of Northern Province of Zambia, Katwishi Chishimba,¹ a forty-year-old widow and teacher at a local secondary school, feels constantly watched by neighbours, friends and relatives since her husband died three years ago. Regarding her appearance, she sees herself in a dilemma: If she gains weight, dresses up in nice clothes and has her hair done, people suspect her of ‘catching’ men, seducing them into dangerous sex. If, however, she loses weight, wears dark fabric, and leaves her hair unbraided, they assume that she is sick and is going to die the same way her husband died.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.18</book-part-id>
                  <title-group>
                     <label>Chapter 13</label>
                     <title>Orphans’ Ties</title>
                     <subtitle>Belonging and Relatedness in Child-Headed Households in Malawi</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wolf</surname>
                           <given-names>Angelika</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>292</fpage>
                  <abstract>
                     <p>Malawi is still one of the countries in southern Africa most severely affected by the HIV/AIDS epidemic. In 2007 almost one million people were living with HIV in the country, the national adult prevalence rate was estimated at 11.9 per cent (UNAIDS 2008). Globally, 15 million children have been orphaned by AIDS, more than 12 million in sub-Saharan Africa alone (UNICEF 2005: 6). By 2010 an estimated 18 million children in sub-Saharan Africa will become orphans due to the epidemic (UNAIDS 2006). In 2005, 14 per cent of all children in Malawi were orphaned; more than one million orphans have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.19</book-part-id>
                  <title-group>
                     <label>Chapter 14</label>
                     <title>The Widow in Blue:</title>
                     <subtitle>Blood and the Morality of Remembering in Botswana’s Time of AIDS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Klaits</surname>
                           <given-names>Frederick</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>312</fpage>
                  <abstract>
                     <p>One of the most immediately visible indications of the scope of the current AIDS pandemic in Botswana is the large number of women dressed in mourning for their deceased husbands. In Old Naledi, a densely populated neighbourhood of Gaborone, many women are wearing black – full-length black dresses, black shoulder throws and black headscarves. The mourning attire of widowers is less conspicuous, consisting of hats and pieces of black cloth pinned to the shirt. The affines of the bereaved spouse provide these clothes out of recognition that his or her ‘blood’ (<italic>madi</italic>) has been rendered ‘hot’ (<italic>mogote</italic>) or ‘dirty’ (<italic>leswe</italic>).</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.20</book-part-id>
                  <title-group>
                     <title>Notes on Contributors</title>
                  </title-group>
                  <fpage>332</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qddb5.21</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>337</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
