<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envihealpers</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100482</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environmental Health Perspectives</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Institute of Environmental Health Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00916765</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41548659</article-id>
         <article-categories>
            <subj-group>
               <subject>Review</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Who Adopts Improved Fuels and Cookstoves? A Systematic Review</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jessica J.</given-names>
                  <surname>Lewis</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Subhrendu K.</given-names>
                  <surname>Pattanayak</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">120</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40074726</issue-id>
         <fpage>637</fpage>
         <lpage>645</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41548659"/>
         <abstract>
            <p>Background: The global focus on improved cookstoves (ICSs) and clean fuels has increased because of their potential for delivering triple dividends: household health, local environmental quality, and regional climate benefits. However, ICS and clean fuel dissemination programs have met with low rates of adoption. Objectives: We reviewed empirical studies on ICSs and fuel choice to describe the literature, examine determinants of fuel and stove choice, and identify knowledge gaps. Methods: We conducted a systematic review of the literature on the adoption of ICSs or cleaner fuels by households in developing countries. Results are synthesized through a simple vote-counting meta-analysis. Results: We identified 32 research studies that reported 146 separate regression analyses of ICS adoption (11 analyses) or fuel choice (135 analyses) from Asia (60%), Africa (27%), and Latin America (19%). Most studies apply multivariate regression methods to consider 7-13 determinants of choice. Income, education, and urban location were positively associated with adoption in most but not all studies. However, the influence of fuel availability and prices, household size and composition, and sex is unclear. Potentially important drivers such as credit, supply-chain strengthening, and social marketing have been ignored. Conclusions: Adoption studies of ICSs or clean energy are scarce, scattered, and of differential quality, even though global distribution programs are quickly expanding. Future research should examine an expanded set of contextual variables to improve implementation of stove programs that can realize the "win-win-win" of health, local environmental quality, and climate associated with these technologies.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d631e196a1310">
            <mixed-citation id="d631e200" publication-type="other">
Adkins E, Eapen S, Kaluwile F, Nair G, Modi V. 2010. Off-grid
energy services for the poor: introducing LED lighting in
the Millennium Villages Project in Malawi. Energy Policy
38:1087-1097.</mixed-citation>
         </ref>
         <ref id="d631e216a1310">
            <mixed-citation id="d631e220" publication-type="other">
Amacher GS, Hyde WF, Joshee BR. 1992. The adoption of
consumption technologies under uncertainty: a case of
improved stoves in Nepal. J Econ Dev 17(2):93-105.</mixed-citation>
         </ref>
         <ref id="d631e233a1310">
            <mixed-citation id="d631e237" publication-type="other">
Amacher GS, Hyde WF, Kanel KR. 1996. Household fuelwood
demand and supply in Nepal's Tarai and mid-hills: choice
between cash outlays and labor opportunity. World Dev
24(11):1725-1736.</mixed-citation>
         </ref>
         <ref id="d631e253a1310">
            <mixed-citation id="d631e257" publication-type="other">
Arthur MFSR, Zahran S, Bucini G. 2010. On the adoption of
electricity as a domestic source by Mozambican house-
holds. Energy Policy 38(11):7235-7249.</mixed-citation>
         </ref>
         <ref id="d631e271a1310">
            <mixed-citation id="d631e275" publication-type="other">
Bailis R, Cowan A, Berrueta V, Masera O. 2009. Arresting the
killer in the kitchen: the promises and pitfalls of commer-
cializing improved cookstoves. World Dev 37(10):1694-1705.</mixed-citation>
         </ref>
         <ref id="d631e288a1310">
            <mixed-citation id="d631e292" publication-type="other">
Baland JM, Bardhan P, Das S, Mookherjee D, Sarkar R. 2010. The
environmental impact of poverty: evidence from firewood
collection in rural Nepal. Econ Dev Cult Change 59(1):23-61.</mixed-citation>
         </ref>
         <ref id="d631e305a1310">
            <mixed-citation id="d631e309" publication-type="other">
Barnes DF, Openshaw K, Smith KR, van der Plas R. 1993. The
design and diffusion of improved cooking stoves. World
Bank Res Obs 8(2):119-141.</mixed-citation>
         </ref>
         <ref id="d631e322a1310">
            <mixed-citation id="d631e326" publication-type="other">
Beach RH, Pattanayak SK, Yang J-C, Murray ВС, Abt RC.
2005. Econometric studies of non-industrial private forest
management: a review and synthesis. Forest Policy Econ
7(3):261-281.</mixed-citation>
         </ref>
         <ref id="d631e342a1310">
            <mixed-citation id="d631e346" publication-type="other">
Bruce N, Perez-Padilla R, Albalak R. 2000. Indoor air pollution
in developing countries: a major environmental and public
health challenge. Bull World Health Organ 78(9):1078-1092.</mixed-citation>
         </ref>
         <ref id="d631e359a1310">
            <mixed-citation id="d631e363" publication-type="other">
Campbell BM, Vermeulen SJ, Mangono JJ, Mabugu R. 2003. The
energy transition in action: urban domestic fuel choices in
a changing Zimbabwe. Energy Policy 31(6):553-562.</mixed-citation>
         </ref>
         <ref id="d631e377a1310">
            <mixed-citation id="d631e381" publication-type="other">
Chalmers 1. 2005. If evidence-informed policy works in practice,
does it matter if it doesn't work in theory? Evidence Policy
1(2):227-242.</mixed-citation>
         </ref>
         <ref id="d631e394a1310">
            <mixed-citation id="d631e398" publication-type="other">
Chambwera M, Folmer H. 2007. Fuel switching in Harare: an
almost ideal demand system approach. Energy Policy
35(4):2538-2548.</mixed-citation>
         </ref>
         <ref id="d631e411a1310">
            <mixed-citation id="d631e415" publication-type="other">
Chaudhuri S, Pfaff ASP. 2003. Fuel-Choice and Indoor Air
Quality: A Household-Level Perspective on Economic
Growth and the Environment. New York:Department of
Economics and School of International and Public Affairs,
Columbia University.</mixed-citation>
         </ref>
         <ref id="d631e434a1310">
            <mixed-citation id="d631e438" publication-type="other">
Chen L, Heerink N, van den Berg M. 2006. Energy consumption
in rural China: a household model for three villages in
Jiangxi Province. Ecol Econ 58(2):407-420.</mixed-citation>
         </ref>
         <ref id="d631e451a1310">
            <mixed-citation id="d631e455" publication-type="other">
Cook T, Cooper H, Corday D, Hartmann H, Hedges L, Light R,
et al. 1992. Meta-Analysis for Explanation: A Casebook.
New York:Russell Sage Foundation.</mixed-citation>
         </ref>
         <ref id="d631e468a1310">
            <mixed-citation id="d631e472" publication-type="other">
Damte A, Koch SF. 2011. Clean Fuel Saving Technology Adoption
in Urban Ethiopia. Department of Economics Working Paper
Series. Pretoria, South Africa:University of Pretoria.</mixed-citation>
         </ref>
         <ref id="d631e486a1310">
            <mixed-citation id="d631e490" publication-type="other">
DeFries R, Pandey D. 2010. Urbanization, the energy ladder and
forest transitions in India's emerging economy. Land Use
Policy 27(2):130-138.</mixed-citation>
         </ref>
         <ref id="d631e503a1310">
            <mixed-citation id="d631e507" publication-type="other">
Edwards JHY, Langpap C. 2005. Startup costs and the decision to
switch from firewood to gas fuel. Land Econ 81(4):570-586.</mixed-citation>
         </ref>
         <ref id="d631e517a1310">
            <mixed-citation id="d631e521" publication-type="other">
El Tayeb Muneer S, Mukhtar Mohamed EW. 2003. Adoption of
biomass improved cookstoves in a patriarchal society: an
example from Sudan. Sci Total Environ 307(1-3):259-266.</mixed-citation>
         </ref>
         <ref id="d631e534a1310">
            <mixed-citation id="d631e538" publication-type="other">
Ezzati M, Bailis R, Kammen DM, Holloway T, Price L, Cifuentes
LA, et al. 2004. Energy management and global health.
Annu Rev Environ Resour 29(1):383-419.</mixed-citation>
         </ref>
         <ref id="d631e551a1310">
            <mixed-citation id="d631e555" publication-type="other">
Farsi M, Filippini M, Pachauri S. 2007. Fuel choices in urban
Indian households. Environ Dev Econ 12(06):757-774.</mixed-citation>
         </ref>
         <ref id="d631e565a1310">
            <mixed-citation id="d631e569" publication-type="other">
Flodgren G, Eccles M, Shepperd S, Scott A, Parmelli E, Beyer F.
2011. An overview of reviews evaluating the effectiveness
of financial incentives in changing healthcare professional
behaviours and patient outcomes. Cochrane Database
Syst Rev (7):CD009255; doi:10.1002/14651858.CD009255
[Online 6 July 2011].</mixed-citation>
         </ref>
         <ref id="d631e593a1310">
            <mixed-citation id="d631e597" publication-type="other">
GACC (Global Alliance for Clean Cookstoves). 2011. Igniting
Change: A Strategy for Universal Adoption of Clean
Cookstoves and Fuels. Washington, DC:GACC.</mixed-citation>
         </ref>
         <ref id="d631e610a1310">
            <mixed-citation id="d631e614" publication-type="other">
Gebreegziabher Z, Mekonnen A, Kassie M, Köhlin G. 2009.
Urban Energy Transition and Technology Adoption:
The Case of Tigrai, Northern Ethiopia. Gothenburg,
Sweden:University of Gothenburg. Available: http://www.
umb.no/statisk/ncde-2009/zenebe_gegreegziabher.pdf
[accessed 26 March 2012].</mixed-citation>
         </ref>
         <ref id="d631e637a1310">
            <mixed-citation id="d631e643" publication-type="other">
Geist HJ, Lambin EF. 2001. What Drives Tropical Deforestation?
A Meta-Analysis of Proximate and Underlying Causes of
Deforestation Based on Subnational Scale Case Study
Evidence. LUCC Report Series No. 4. Louvain-la-Neuve,
Belgium:University of Louvain.</mixed-citation>
         </ref>
         <ref id="d631e662a1310">
            <mixed-citation id="d631e666" publication-type="other">
Gerrard M, Gibbons F, Bushman B. 1996. Relation between
perceived vulnerability to HIV and precautionary sexual
behavior. Psychol Bull 119(3):390-409.</mixed-citation>
         </ref>
         <ref id="d631e679a1310">
            <mixed-citation id="d631e683" publication-type="other">
Glasgow RE, Lichtenstein E, Marcus AC. 2003. Why don't we
see more translation of health promotion research to
practice? Rethinking the efficacy-to-effectiveness transi-
tion. Am J Public Health 93(8):1261-1267.</mixed-citation>
         </ref>
         <ref id="d631e699a1310">
            <mixed-citation id="d631e703" publication-type="other">
Google. 2011. Google Scholar Homepage. Available: http://
scholar.google.com/[accessed 20 June 2011].</mixed-citation>
         </ref>
         <ref id="d631e714a1310">
            <mixed-citation id="d631e718" publication-type="other">
Green LW, Ottoson JM, García C, Hiatt RA. 2009. Diffusion the-
ory and knowledge dissemination, utilization, and integra-
tion in public health. Annu Rev Public Health 30(1):151-174.</mixed-citation>
         </ref>
         <ref id="d631e731a1310">
            <mixed-citation id="d631e735" publication-type="other">
Gundimeda H, Köhlin G. 2008. Fuel demand elasticities for
energy and environmental policies: Indian sample survey
evidence. Energy Econ 30(2):517-546.</mixed-citation>
         </ref>
         <ref id="d631e748a1310">
            <mixed-citation id="d631e752" publication-type="other">
Gupta G, Köhlin G. 2006. Preferences for domestic fuel: analy-
sis with socio-economic factors and rankings in Kolkata,
India. Ecol Econ 57(1):107-121.</mixed-citation>
         </ref>
         <ref id="d631e765a1310">
            <mixed-citation id="d631e769" publication-type="other">
Heltberg R. 2004. Fuel switching: evidence from eight develop-
ing countries. Energy Econ 26(5):869-887.</mixed-citation>
         </ref>
         <ref id="d631e779a1310">
            <mixed-citation id="d631e783" publication-type="other">
Heltberg R. 2005. Factors determining household fuel choice in
Guatemala. Environ Dev Econ 10(03):337-361.</mixed-citation>
         </ref>
         <ref id="d631e793a1310">
            <mixed-citation id="d631e797" publication-type="other">
Hofstad O, Köhlin G, Namaalway F. 2009. How can emissions
from woodfuel be reduced? In: Realising REDD+: National
Strategy and Policy Options (Angelsen A, Brockhaus M,
Kanninen M, Sills E, Sunderlin WD, Wertz-Kanounnikoff S,
eds). Bogor, lndonesia:Center for International Forestry
Research, 237-248.</mixed-citation>
         </ref>
         <ref id="d631e821a1310">
            <mixed-citation id="d631e825" publication-type="other">
Hölzel L, Härter M, Reese С, Kriston L. 2011. Risk factors for
chronic depression—a systematic review. J Affect Disord
129(1-3):1-13.</mixed-citation>
         </ref>
         <ref id="d631e838a1310">
            <mixed-citation id="d631e842" publication-type="other">
Hosier RH, Dowd J. 1987. Household fuel choice in Zimbabwe:
an empirical test of the energy ladder hypothesis. Resour
Energy 9(4):347-361.</mixed-citation>
         </ref>
         <ref id="d631e855a1310">
            <mixed-citation id="d631e859" publication-type="other">
Jack DW. 2006. Household Behavior and Energy Demand:
Evidence from Peru [PhD Dissertation]. Cambridge, MA:
Harvard University.</mixed-citation>
         </ref>
         <ref id="d631e872a1310">
            <mixed-citation id="d631e876" publication-type="other">
Kaul S, Liu Q. 1992. Rural household energy use in China.
Energy 17(4):405-411.</mixed-citation>
         </ref>
         <ref id="d631e886a1310">
            <mixed-citation id="d631e890" publication-type="other">
Kavi Kumar KS, Viswanathan B. 2007. Changing structure of
income indoor air pollution relationship in India. Energy
Policy 35(11):5496-5504.</mixed-citation>
         </ref>
         <ref id="d631e903a1310">
            <mixed-citation id="d631e907" publication-type="other">
Kebede B, Bekele A, Kedir E. 2002. Can the urban poor afford
modern energy? The case of Ethiopia. Energy Policy
30(11-12):1029-1045.</mixed-citation>
         </ref>
         <ref id="d631e921a1310">
            <mixed-citation id="d631e925" publication-type="other">
Kemmler A. 2007. Factors influencing household access to
electricity in India. Energy Sustain Dev 11(4):13-20.</mixed-citation>
         </ref>
         <ref id="d631e935a1310">
            <mixed-citation id="d631e939" publication-type="other">
Khandker SR, Barnes DF, Samad HA. 2010. Energy Poverty
in Rural and Urban India: Are the Energy Poor Also
Income Poor? Policy Research Working Paper No. 5463.
Washington, DC:World Bank.</mixed-citation>
         </ref>
         <ref id="d631e955a1310">
            <mixed-citation id="d631e959" publication-type="other">
Köhlin G, Sills EO, Pattanayak SK, Wilfong C. 2011. Energy,
Gender and Development. Policy Research Working
Paper, No. WPS 5800. Washington, DC:Social Dimensions
of Climate Change Division, World Bank.</mixed-citation>
         </ref>
         <ref id="d631e975a1310">
            <mixed-citation id="d631e979" publication-type="other">
Lamarre-Vincent J. 2011. Household Determinants and
Respiratory Health Impacts of Fuel Switching in Indonesia
[Master's Thesis]. Durham, NC:Duke University.</mixed-citation>
         </ref>
         <ref id="d631e992a1310">
            <mixed-citation id="d631e996" publication-type="other">
Legros G, Havet I, Bruce N, Bonjour S. 2009. The Energy Access
Situation in Developing Countries: A Review Focusing on
the Least Developed Countries and Sub-Saharan Africa.
New York:United Nations Development Programme and
World Health Organization.</mixed-citation>
         </ref>
         <ref id="d631e1015a1310">
            <mixed-citation id="d631e1019" publication-type="other">
Louw K, Conradie B, Howells M, Dekenah M. 2008. Determinants
of electricity demand for newly electrified low-income
African households. Energy Policy 36:2812-2818.</mixed-citation>
         </ref>
         <ref id="d631e1033a1310">
            <mixed-citation id="d631e1037" publication-type="other">
Madon T, Hofman KJ, Kupfer L, Glass RI. 2007. Public health.
Implementation science. Science 318(5857):1728-1729.</mixed-citation>
         </ref>
         <ref id="d631e1047a1310">
            <mixed-citation id="d631e1051" publication-type="other">
Martin WJ, Glass RI, Balbus JM, Collins FS. 2011. Public
health. A major environmental cause of death. Science
334(6053):180-181.</mixed-citation>
         </ref>
         <ref id="d631e1064a1310">
            <mixed-citation id="d631e1068" publication-type="other">
Masera OR, Saatkamp BD, Kammen DM. 2000. From linear
fuel switching to multiple cooking strategies: a critique
and alternative to the energy ladder model. World Dev
28(12):2083-2103.</mixed-citation>
         </ref>
         <ref id="d631e1084a1310">
            <mixed-citation id="d631e1088" publication-type="other">
McEachern M, Hanson S. 2008. Socio-geographic perception
in the diffusion of innovation: solar energy technology in
Sri Lanka. Energy Policy 36(7):2578-2590.</mixed-citation>
         </ref>
         <ref id="d631e1101a1310">
            <mixed-citation id="d631e1107" publication-type="other">
Mitchell A. 2010. Indoor Air Pollution: Technologies to Reduce
Emissions Harmful to Health: Report of a Landscape
Analysis of Evidence and Experience. Washington,
DC:USAID-TRAction.</mixed-citation>
         </ref>
         <ref id="d631e1123a1310">
            <mixed-citation id="d631e1127" publication-type="other">
Moher D, Liberati A, Tetzlaff J, Altman DG, the PRISMA Group.
2009. Preferred reporting items for systematic reviews
and meta-analyses: the PRISMA statement. PloS Med
6(7):e1000097; doi:10.1371/journal.pmed.1000097 [Online
21 July 2009].</mixed-citation>
         </ref>
         <ref id="d631e1147a1310">
            <mixed-citation id="d631e1151" publication-type="other">
Ouedraogo B. 2006. Household energy preferences for cook-
ing in urban Ouagadougou, Burkina Faso. Energy Policy
34(18):3787-3795.</mixed-citation>
         </ref>
         <ref id="d631e1164a1310">
            <mixed-citation id="d631e1168" publication-type="other">
Pandey S, Yadama GN. 1992. Community development pro-
grams in Nepal: a test of diffusion of innovation theory.
Soc Serv Rev 66(4):582-597.</mixed-citation>
         </ref>
         <ref id="d631e1181a1310">
            <mixed-citation id="d631e1185" publication-type="other">
Pattanayak SK, Mercer DE, Sills EO, Yang J-C. 2003. Taking
Stock of Agroforestry Adoption Studies. Agrofor Syst
57(3):173-186.</mixed-citation>
         </ref>
         <ref id="d631e1198a1310">
            <mixed-citation id="d631e1202" publication-type="other">
Pattanayak SK, Pfaff A. 2009. Behavior, environment, and
health in developing countries: evaluation and valuation.
Annu Rev Resour Econ 1:183-217.</mixed-citation>
         </ref>
         <ref id="d631e1215a1310">
            <mixed-citation id="d631e1219" publication-type="other">
Peng W, Hisham Z, Pan J. 2010. Household level fuel switching
in rural Hubei. Energy Sustain Dev 14(3):238-244.</mixed-citation>
         </ref>
         <ref id="d631e1229a1310">
            <mixed-citation id="d631e1233" publication-type="other">
Pine K, Edwards R, Masera O, Schilmann A, Marrón-Mares A,
Riojas-Rodríguez H. 2011. Adoption and use of improved
biomass stoves in Rural Mexico. Energy Sustain Dev
15(2):176-183.</mixed-citation>
         </ref>
         <ref id="d631e1250a1310">
            <mixed-citation id="d631e1254" publication-type="other">
Pohekar SD, Kumar D, Ramachandran M. 2005. Dissemination
of cooking energy alternatives in India—a review. Renew
Sust Energ Rev 9(4):379-393.</mixed-citation>
         </ref>
         <ref id="d631e1267a1310">
            <mixed-citation id="d631e1271" publication-type="other">
Ramanathan V, Carmichael G. 2008. Global and regional climate
changes due to black carbon. Nat Geosci 1(4):221-227.</mixed-citation>
         </ref>
         <ref id="d631e1281a1310">
            <mixed-citation id="d631e1285" publication-type="other">
Ramanathan V, Li F, Ramana MV, Praveen PS, Kim D, Corrigan CE,
et al. 2007. Atmospheric brown clouds: hemispherical
and regional variations in long-range transport, absorp-
tion, and radiative forcing. J Geophys Res 112:D22S21;
doi:10.1029/2006JD008124 [Online 23 October 2007].</mixed-citation>
         </ref>
         <ref id="d631e1304a1310">
            <mixed-citation id="d631e1308" publication-type="other">
Rao MN, Reddy BS. 2007. Variations in energy use by Indian house-
holds: an analysis of micro level data. Energy 32(2):143-153.</mixed-citation>
         </ref>
         <ref id="d631e1318a1310">
            <mixed-citation id="d631e1322" publication-type="other">
Rebane KL, Barham BL. 2011. Knowledge and adoption of
solar home systems in rural Nicaragua. Energy Policy
39(6):3064-3075.</mixed-citation>
         </ref>
         <ref id="d631e1335a1310">
            <mixed-citation id="d631e1339" publication-type="other">
Reddy BS. 1995. A multilogit model for fuel shifts in the domes-
tic sector. Energy 20(9):929-936.</mixed-citation>
         </ref>
         <ref id="d631e1350a1310">
            <mixed-citation id="d631e1354" publication-type="other">
Rehfuess EA, Briggs DJ, Joffe M, Best N. 2010. Bayesian mod-
eling of household solid fuel use: insights towards design-
ing effective interventions to promote fuel switching in
Africa. Environ Res 110(7):725-732.</mixed-citation>
         </ref>
         <ref id="d631e1370a1310">
            <mixed-citation id="d631e1374" publication-type="other">
Rehfuess EA, Mehta S, Prüss-Üstiin A. 2006. Assessing house-
hold solid fuel use: multiple implications for the millennium
development goals. Environ Health Perspect 114:114:373-378.</mixed-citation>
         </ref>
         <ref id="d631e1387a1310">
            <mixed-citation id="d631e1391" publication-type="other">
Ruiz-Mercado I, Masera O, Zamora H, Smith KR. 2011. Adoption
and sustained use of improved cookstoves. Energy Policy
39:7557-7566; doi:10.1016/j.enpol.2011.03.02 [Online 15 April
2011].</mixed-citation>
         </ref>
         <ref id="d631e1407a1310">
            <mixed-citation id="d631e1411" publication-type="other">
Sagar AD, Kartha S. 2007. Bioenergy and sustainable develop-
ment? Ann Rev Environ Resour 32(1):131-167.</mixed-citation>
         </ref>
         <ref id="d631e1421a1310">
            <mixed-citation id="d631e1425" publication-type="other">
ScienceDirect. 2011. ScienceDirect Homepage. Available: http://
www.sciencedirect.com/science/browse/s [accessed
20 June 2011].</mixed-citation>
         </ref>
         <ref id="d631e1438a1310">
            <mixed-citation id="d631e1442" publication-type="other">
Shindell DT, Kuylenstierna JCI, Raes F, Ramanathan V,
Rosenthal E, Terry S, et al. 2011. Integrated Assessment
of Black Carbon and Tropospheric Ozone: Summary for
Decision Makers. Nairobi:United Nations Environment
Programme and World Meteorological Organization.</mixed-citation>
         </ref>
         <ref id="d631e1462a1310">
            <mixed-citation id="d631e1466" publication-type="other">
Sinton JE, Smith KR, Peabody JW, Yaping L, Xiliang Z,
Edwards R, et al. 2004. An assessment of programs to pro-
mote improved household stoves in China. Energy Sustain
Dev 8(3):33-52.</mixed-citation>
         </ref>
         <ref id="d631e1482a1310">
            <mixed-citation id="d631e1486" publication-type="other">
Slaski X, Thurber M. 2009. Research Note: Cookstoves and
Obstacles to Technology Adoption by the Poor. Program
on Energy and Sustainable Development Working Paper
No. 89. Stanford, CA:Program on Energy and Sustainable
Development.</mixed-citation>
         </ref>
         <ref id="d631e1505a1310">
            <mixed-citation id="d631e1509" publication-type="other">
Smith KR, Jerrett M, Anderson HR, Burnett RT, Stone V,
Derwent R, et al. 2009. Public health benefits of strate-
gies to reduce greenhouse-gas emissions: health impli-
cations of short-lived greenhouse pollutants. Lancet
374(9707):2091-2103.</mixed-citation>
         </ref>
         <ref id="d631e1528a1310">
            <mixed-citation id="d631e1534" publication-type="other">
Smith KR, McCracken JP, Weber MW, Hubbard A, Jenny A,
Thompson LM, et al. 2011. Effect of reduction in house-
hold air pollution on childhood pneumonia in Guatemala
(RESPIRE): a randomized controlled trial. Lancet
378(9804):1717-1726.</mixed-citation>
         </ref>
         <ref id="d631e1553a1310">
            <mixed-citation id="d631e1557" publication-type="other">
Smith KR, Mehta S, Feuz MM-. 2004. Indoor air pollution from
household use of solid fuels. In: Comparative Quantification
of Health Risks: Global and Regional Burden of Disease
Attributable to Selected Major Risk Factors (Ezzati M, ed).
Geneva-.World Health Organization, 1435-1494.</mixed-citation>
         </ref>
         <ref id="d631e1576a1310">
            <mixed-citation id="d631e1580" publication-type="other">
Smith-Sivertsen T, Diaz E, Pope D, Lie RT, Diaz A, McCracken J,
et al. 2009. Effect of reducing indoor air pollution on wom-
en's respiratory symptoms and lung function: the RESPIRE
randomized trial, Guatemala. Am J Epidemiol 170(2):211-220.</mixed-citation>
         </ref>
         <ref id="d631e1597a1310">
            <mixed-citation id="d631e1601" publication-type="other">
Sorrell S. 2007. Improving the evidence base for energy policy: the
role of systematic reviews. Energy Policy 35(3):1858-1871.</mixed-citation>
         </ref>
         <ref id="d631e1611a1310">
            <mixed-citation id="d631e1615" publication-type="other">
Thomson Reuters. 2011. ISI Web of Science. New York:Thomson
Reuters.</mixed-citation>
         </ref>
         <ref id="d631e1625a1310">
            <mixed-citation id="d631e1629" publication-type="other">
van der Knaap LM, Leeuw FL, Bogaerts S, Nijssen LTJ. 2008.
Combining Campbell standards and the realist evaluation
approach. Am J Eval 29(1):48-57.</mixed-citation>
         </ref>
         <ref id="d631e1642a1310">
            <mixed-citation id="d631e1646" publication-type="other">
Venkataraman C, Sagar AD, Habib G, Lam N, Smith KR. 2010.
The Indian National Initiative for Advanced Biomass
Cookstoves: the benefits of clean combustion. Energy
Sustain Dev 14(2):63-72.</mixed-citation>
         </ref>
         <ref id="d631e1662a1310">
            <mixed-citation id="d631e1666" publication-type="other">
Walekhwa PN, Mugisha J, Drake L. 2009. Biogas energy from
family-sized digesters in Uganda: critical factors and pol-
icy implications. Energy Policy 37(7):2754-2762.</mixed-citation>
         </ref>
         <ref id="d631e1679a1310">
            <mixed-citation id="d631e1683" publication-type="other">
Wendland KJ, Pattanayak SK, Sills E. 2011. Democracy and
Dictatorship: Comparing Household Innovation across
the Border of Benin and Togo. Raleigh, NC:North Carolina
State University, Department of Forestry and Natural
Resources.</mixed-citation>
         </ref>
         <ref id="d631e1703a1310">
            <mixed-citation id="d631e1707" publication-type="other">
WHO. 2009. Global Health Risks: Mortality and Burden of
Disease Attributable to Major Risks. Geneva:World Health
Organization.</mixed-citation>
         </ref>
         <ref id="d631e1720a1310">
            <mixed-citation id="d631e1724" publication-type="other">
World Bank. 2011. Household Cookstoves, Environment, Health,
and Climate Change: A New Look at an Old Problem.
Washington, DC:World Bank.</mixed-citation>
         </ref>
         <ref id="d631e1737a1310">
            <mixed-citation id="d631e1741" publication-type="other">
Yan HJ. 2010. The Theoretic and Empirical Analysis on the
Compatibility of Sustainable Development Strategies and
Poverty Reduction Policies at Micro Level. Aix-en-Provence,
France:Université de la Méditerranée Aix-Marseille II.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
