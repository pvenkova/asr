<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1r2fkg</book-id>
      <subj-group>
         <subject content-type="call-number">HD73 .I52 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Congresses</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Economics</subject>
      </subj-group>
      <book-title-group>
         <book-title>In Search of Prosperity</book-title>
         <subtitle>Analytic Narratives on Economic Growth</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>EDITED AND WITH AN INTRODUCTION BY</role>
            <name name-style="western">
               <surname>Rodrik</surname>
               <given-names>Dani</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>21</day>
         <month>11</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780691092690</isbn>
      <isbn content-type="ppub">0691092699</isbn>
      <isbn content-type="epub">9781400845897</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2003</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1r2fkg"/>
      <abstract abstract-type="short">
         <p>The economics of growth has come a long way since it regained center stage for economists in the mid-1980s. Here for the first time is a series of country studies guided by that research. The thirteen essays, by leading economists, shed light on some of the most important growth puzzles of our time. How did China grow so rapidly despite the absence of full-fledged private property rights? What happened in India after the early 1980s to more than double its growth rate? How did Botswana and Mauritius avoid the problems that other countries in sub--Saharan Africa succumbed to? How did Indonesia manage to grow over three decades despite weak institutions and distorted microeconomic policies and why did it suffer such a collapse after 1997?</p>
         <p>What emerges from this collective effort is a deeper understanding of the centrality of institutions. Economies that have performed well over the long term owe their success not to geography or trade, but to institutions that have generated market-oriented incentives, protected property rights, and enabled stability. However, these narratives warn against a cookie-cutter approach to institution building.</p>
         <p>The contributors are Daron Acemoglu, Maite Careaga, Gregory Clark, J. Bradford DeLong, Georges de Menil, William Easterly, Ricardo Hausmann, Simon Johnson, Daniel Kaufmann, Massimo Mastruzzi, Ian W. McLean, Lant Pritchett, Yingyi Qian, James A. Robinson, Devesh Roy, Arvind Subramanian, Alan M. Taylor, Jonathan Temple, Barry R. Weingast, Susan Wolcott, and Diego Zavaleta.</p>
      </abstract>
      <counts>
         <page-count count="520"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.4</book-part-id>
                  <title-group>
                     <title>List of Contributors</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction:</title>
                     <subtitle>WHAT DO WE LEARN FROM COUNTRY NARRATIVES?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>RODRIK</surname>
                           <given-names>DANI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The spectacular gap in incomes that separates the world’s rich and poor nations is the central economic fact of our time. Average income in Sierra Leone, which is the poorest country in the world for which we have data, is almost one hundred times lower than that in Luxembourg, the world’s richest country. Nearly two-thirds of the world’s population lives in countries where average income is only one-tenth the U.S. level (fig. 1.1).¹ Since the starting points for all these countries were not so far apart prior to the Industrial Revolution, these disparities must be attributed almost entirely to differences</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Australian Growth:</title>
                     <subtitle>A CALIFORNIA PERSPECTIVE</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MCLEAN</surname>
                           <given-names>IAN W.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>TAYLOR</surname>
                           <given-names>ALAN M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>23</fpage>
                  <abstract>
                     <p>The Australian growth experience includes several challenges to growth economists—both theorists and policymakers. Australia is one of the few examples of an economy that has maintained living standards at or close to world-best levels for more than 150 years. Understanding how a small open economy achieved this despite major domestic shocks and dramatic shifts in international economic conditions should yield insights pertinent to the replication of similar outcomes elsewhere. In addition, Australia’s high per capita income has been sustained in significant measure on the basis of the exploitation of its abundant natural resources. This contradicts one view in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>One Polity, Many Countries:</title>
                     <subtitle>ECONOMIC GROWTH IN INDIA, 1873-2000</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CLARK</surname>
                           <given-names>GREGORY</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>WOLCOTT</surname>
                           <given-names>SUSAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>India is perhaps the most interesting of all economies for those interested in economic growth. It is one of the poorer countries of the world, and has even seen an erosion of its income per capita relative to the economically advanced economies such as the United States since we have the first reasonable data in 1873. But this relative decline has occurred in what has been for most of this period a very favorable institutional environment. Indeed from an economist’s perspective the institutional environment in the colonialist years from 1873 to 1947—secure property rights, free trade, fixed exchange rates,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>An African Success Story:</title>
                     <subtitle>BOTSWANA</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ACEMOGLU</surname>
                           <given-names>DARON</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>JOHNSON</surname>
                           <given-names>SIMON</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROBINSON</surname>
                           <given-names>JAMES A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>80</fpage>
                  <abstract>
                     <p>Despite some success stories in the 1960s and early 1970s, Africa is poor and getting poorer.¹ There is also an almost universally pessimistic consensus about its economic prospects. This consensus started to emerge in recent empirical work on the determinants of growth with Barro’s (1991) discovery of a negative “African Dummy” and was summed up by Easterly and Levine’s (1997) title, “Africa’s Growth Tragedy.” Table 4.1 collects some familiar comparative evidence on Africa’s economic performance. The average sub-Saharan African country is poorer than the average low-income country and getting poorer. Indeed, the average growth rate has been negative since 1965,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>A Toy Collection, a Socialist Star, and a Democratic Dud?</title>
                     <subtitle>GROWTH THEORY, VIETNAM, AND THE PHILIPPINES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PRITCHETT</surname>
                           <given-names>LANT</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>123</fpage>
                  <abstract>
                     <p>Robert Solow often refers affectionately to his famous namesake model as a “parable” (in print) or verbally as a “toy” model. Being a toy does not mean that the model is not serious, but rather that in the interests of clarity it is stripped to its barest essentials. The toy model eliminates complexity to illuminate a few key relationships in the clearest possible way. The sacrifice for this clarity is generality. The Solow model explains the “broad facts about the growth of advanced industrialized economies” (Solow 1970, 3), and he has never maintained that it is applicable to developing countries.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Growing into Trouble:</title>
                     <subtitle>INDONESIA AFTER 1966</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>TEMPLE</surname>
                           <given-names>JONATHAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>152</fpage>
                  <abstract>
                     <p>Few countries have experienced reversals in economic fortune and reputation as dramatic as those of Indonesia. In the early 1960s, one influential commentator described the country as a “chronic dropout” and “the number one failure among the major underdeveloped countries.”¹ By 1996, the picture looked very different: under Suharto, the country had grown rapidly for most of the previous thirty years, living standards had significantly improved, and the incidence of poverty had been sharply reduced. Although its record was by no means one of consistent success, Indonesia was increasingly regarded as yet another East Asian example of sound policy and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>India since Independence:</title>
                     <subtitle>AN ANALYTIC GROWTH NARRATIVE</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>DELONG</surname>
                           <given-names>J. BRADFORD</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>184</fpage>
                  <abstract>
                     <p>How useful is the modern theory of economic growth? Does it provide a satisfactory framework for analyzing the wealth and poverty of nations? This chapter investigates this question by applying modern growth theory to the case of the economic development of India over the past half-century. Whether growth theory turns out to be useful—whether valid, interesting, and nonobvious insights are generated—is left as an exercise to the reader.</p>
                     <p>I should note at the start that this is a hazardous exercise: I know a fair amount about growth theory, but I know relatively little about India. The general rule</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Who Can Explain the Mauritian Miracle?</title>
                     <subtitle>MEADE, ROMER, SACHS, OR RODRIK?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SUBRAMANIAN</surname>
                           <given-names>ARVIND</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROY</surname>
                           <given-names>DEVESH</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>205</fpage>
                  <abstract>
                     <p>In the postwar period, few sub-Saharan African countries have made the transition to high standards of living for their population. The record of sustained economic performance in sub-Saharan Africa (hereafter Africa) is not heartening. It is not that there have not been periods of sustained growth: as table 8.1 shows, sixteen African countries, at various points in time, achieved high rates of growth. Sadly, however, very few such episodes have been long and sustained enough to lead to high levels of income and standards of living.¹ In 1998, only two African countries ranked among the top 50 countries in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Venezuela’s Growth Implosion:</title>
                     <subtitle>A NEOCLASSICAL STORY?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HAUSMANN</surname>
                           <given-names>RICARDO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>244</fpage>
                  <abstract>
                     <p>Once upon a time there was a prosperous village. Life was good and people were happy. One year the crops failed and living conditions deteriorated. When the same thing happened the following year, the town elders met and decided that such a second occurrence could not be a coincidence: it had to be the work of the devil, acting through some witch whose soul he had possessed. The problem had to be dealt with.</p>
                     <p>The elders decided to establish an inquisitorial committee to search for witches and burn them at the stake. They had scientific means of identifying them: since</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>History, Policy, and Performance in Two Transition Economies:</title>
                     <subtitle>POLAND AND ROMANIA</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>DE MENIL</surname>
                           <given-names>GEORGES</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>271</fpage>
                  <abstract>
                     <p>The Growth of the formerly Communist countries of Europe since 1990 is a study in contrasts: A small group of countries—notably Poland, Hungary, and the two parts of Czechoslovakia—recovered early from an initial drop of output (common to all the countries in the region) and achieved sustainable and consistently high rates of growth (4–8 percent) with moderate inflation. In others—in the former Soviet Union, or in southeastern Europe—output continued to fall until the end of the century, or recovered and then fell again. Some of these low-performance countries suffered bouts of high inflation. But the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>How Reform Worked in China</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>QIAN</surname>
                           <given-names>YINGYI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>297</fpage>
                  <abstract>
                     <p>In the last 22 years of the 20th century, China transformed itself from a poor, centrally planned economy to a lower-middle-income, emerging market economy. With total gross domestic product (GDP) growing at an average annual rate of about 9 percent, China’s per capita GDP more than quadrupled during this period. The benefits of growth were also shared by the people on a broad basis: the number of people living in absolute poverty was substantially reduced from over 250 million to about 50 million, a decline from a one-third to a twenty-fifth of its population; and life expectancy increased from 64</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.16</book-part-id>
                  <title-group>
                     <label>CHAPTER 12</label>
                     <title>Sustained Macroeconomic Reforms, Tepid Growth:</title>
                     <subtitle>A GOVERNANCE PUZZLE IN BOLIVIA?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KAUFMANN</surname>
                           <given-names>DANIEL</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>MASTRUZZI</surname>
                           <given-names>MASSIMO</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>ZAVALETA</surname>
                           <given-names>DIEGO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>334</fpage>
                  <abstract>
                     <p>We are cognizant of the limits to large cross-country empirical studies in trying to understand in depth a particular country reality, in ways useful for advice. At the same time, merely relying on a single country account at a particular point in time ignores the historical and comparative cross-country perspective. Worse, an in-depth investigation of a single issue within a country begs the question of whether the particular issue is fundamental for the country’s growth and development relative to other determinants, or not. Further, there are drawbacks in excessive reliance on narrow empirical approaches, or on mere qualitative narrative.</p>
                     <p>Consequently,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.17</book-part-id>
                  <title-group>
                     <label>CHAPTER 13</label>
                     <title>Fiscal Federalism, Good Governance, and Economic Growth in Mexico</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CAREAGA</surname>
                           <given-names>MAITE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>WEINGAST</surname>
                           <given-names>BARRY R.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>399</fpage>
                  <abstract>
                     <p>Good governance and appropriately designed institutions are now recognized as necessary for economic growth. Yet we know too little about this relationship. Why do some governments protect property rights, provide a stable macroeconomy, and have limited taxes and corruption, while others are corrupt kleptocracies that prey on their citizens?</p>
                     <p>In recent years, a new political economy literature has emerged that studies these questions by investigating the interaction of political institutions and economic performance.¹ Proponents of this approach provide a general if abstract answer to the above questions: A country’s political and institutional framework determines whether government officials face incentives for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.18</book-part-id>
                  <title-group>
                     <label>CHAPTER 14</label>
                     <title>The Political Economy of Growth without Development:</title>
                     <subtitle>A CASE STUDY OF PAKISTAN</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>EASTERLY</surname>
                           <given-names>WILLIAM</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>439</fpage>
                  <abstract>
                     <p>Pakistan is an intriguing paradox. It has a well-educated and entrepreneurial Diaspora who thrive as small business owners in industrial economies, skilled workers in Gulf States, and high officials in international organizations. The professional elite within Pakistan is at a similar level to those in the industrialized world. Pakistan benefited from $58 billion of foreign development assistance (in 1995 dollars over 1960–98), 22 adjustment loans from the IMF and the World Bank (not counting the adjustment loan each is making in the years 2000–2001), a lucrative Cold War alliance with the United States, and multiple government development programs.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1r2fkg.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>473</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
