<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30085725</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Herpes Simplex Virus Type 2 Infection as a Risk Factor for Human Immunodeficiency Virus Acquisition in Men Who Have Sex with Men</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Cristina</given-names>
                  <surname>Renzi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>John M.</given-names>
                  <surname>Douglas</surname>
                  <suffix>Jr.</suffix>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Mark</given-names>
                  <surname>Foster</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Cathy W.</given-names>
                  <surname>Critchlow</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Rhoda</given-names>
                  <surname>Ashley-Morrow</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Susan P.</given-names>
                  <surname>Buchbinder</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Beryl A.</given-names>
                  <surname>Koblin</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>David J.</given-names>
                  <surname>McKirnan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Kenneth H.</given-names>
                  <surname>Mayer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Connie L.</given-names>
                  <surname>Celum</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">187</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i30085720</issue-id>
         <fpage>19</fpage>
         <lpage>25</lpage>
         <permissions>
            <copyright-statement>Copyright 2003 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30085725"/>
         <abstract>
            <p>The association of human immunodeficiency virus (HIV) acquisition with herpes simplex virus type 2 (HSV-2) was assessed among men who have sex with men (MSM) in a nested case-control study of 116 case subjects who seroconverted to HIV during follow-up and 342 control subjects who remained HIV seronegative, frequency-matched by follow-up duration and report of HIV-infected sex partner and unprotected anal sex. The baseline HSV-2 seroprevalence was higher among case {46%) than control (34%) subjects (P = .03); the HSV-2 seroincidence was 7% versus 4% (P = .3). Only 15% of HSV-2-infected MSM reported herpes outbreaks in the past year. HIV acquisition was associated with prior HSV-2 infection (odds ratio [OR], 1.8; 95% confidence interval [CI], 1.1-2.9), reporting M2 sex partners (OR, 2.9; 95% CI, 1.4-6.3), and reporting fewer herpes outbreaks in the past year (OR, 0.3; 95% CI, 0.1-0.8). HSV-2 increases the risk of HIV acquisition, independent of recognized herpes lesions and behaviors reflecting potential HIV exposure. HSV-2 suppression with antiviral therapy should be evaluated as an HIV prevention strategy among MSM.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d218e268a1310">
            <label>1</label>
            <mixed-citation id="d218e275" publication-type="other">
Vittinghoff E, Douglas JM Jr, Judson FN, McKirnan D, MacQueen K,
Buchbinder SP. Per-contact risk of human immunodeficiency virus trans-
mission between male sexual partners. Am J Epidemiol 1999; 150:306-11.</mixed-citation>
         </ref>
         <ref id="d218e288a1310">
            <label>2</label>
            <mixed-citation id="d218e295" publication-type="other">
Craib KJP, Meddings DR, Strathdee SA, et al. Rectal gonorrhoea as an
independent risk factor for HIV infection in a cohort of homosexual
men. Genitourin Med 1995;71:150-4.</mixed-citation>
         </ref>
         <ref id="d218e308a1310">
            <label>3</label>
            <mixed-citation id="d218e315" publication-type="other">
Evans BG, Catchpole MA, Heptonstall J, et al. Sexually transmitted
diseases and HIV-1 infection among homosexual men in England and
Wales. BMJ 1993;306:426-8.</mixed-citation>
         </ref>
         <ref id="d218e328a1310">
            <label>4</label>
            <mixed-citation id="d218e335" publication-type="other">
Cohen MS, Hoffman IF, Royce RA, et al. Reduction of concentration
of HIV-1 in semen after treatment of urethritis: implications for pre-
vention of sexual transmission of HIV-1. AIDSCAP Malawi Research
Group. Lancet 1997;349:1868-73.</mixed-citation>
         </ref>
         <ref id="d218e352a1310">
            <label>5</label>
            <mixed-citation id="d218e359" publication-type="other">
Cohen MS. Sexually transmitted diseases enhance HIV transmission:
no longer a hypothesis. Lancet 1998;351:5-7.</mixed-citation>
         </ref>
         <ref id="d218e369a1310">
            <label>6</label>
            <mixed-citation id="d218e376" publication-type="other">
Buchbinder SP, Douglas JM Jr, McKirnan DJ, Judson FN, Katz MH,
MacQueen KM. Feasibility of human immunodeficiency virus vaccine
trials in homosexual men in the United States: risk behavior, seroin-
cidence, and willingness to participate. J Infect Dis 1996; 174:954-61.</mixed-citation>
         </ref>
         <ref id="d218e392a1310">
            <label>7</label>
            <mixed-citation id="d218e399" publication-type="other">
Quinn TC, Wawer MJ, Sewankambo N, et al. Viral load and hetero-
sexual transmission of human immunodeficiency virus type 1. Rakai
Project Study Group. N Engl J Med 2000;342:921-9.</mixed-citation>
         </ref>
         <ref id="d218e412a1310">
            <label>8</label>
            <mixed-citation id="d218e419" publication-type="other">
Kreiss JK, Hopkins SG. The association between circumcision status
and human immunodeficiency virus infection among homosexual
men. J Infect Dis 1993;168:1404-8.</mixed-citation>
         </ref>
         <ref id="d218e432a1310">
            <label>9</label>
            <mixed-citation id="d218e439" publication-type="other">
Buve A, Carael M, Hayes RJ, et al. The multicentre study on factors
determining the differential spread of HIV in four African cities: sum-
mary and conclusions. AIDS 2001; 15:8127-31.</mixed-citation>
         </ref>
         <ref id="d218e452a1310">
            <label>10</label>
            <mixed-citation id="d218e459" publication-type="other">
Wald A, Link K. Risk of human immunodeficiency virus infection in
herpes simplex virus type 2-seropositive persons: a meta-analysis. J
Infect Dis 2002; 185:45-52.</mixed-citation>
         </ref>
         <ref id="d218e473a1310">
            <label>11</label>
            <mixed-citation id="d218e480" publication-type="other">
Schacker T, Zeh J, Hu H-L, Hill E, Corey L. Frequency of symptomatic
and asymptomatic herpes simplex virus type 2 reactivations among
human immunodeficiency virus-infected men. J Infect Dis 1998; 178:
1616-22.</mixed-citation>
         </ref>
         <ref id="d218e496a1310">
            <label>12</label>
            <mixed-citation id="d218e503" publication-type="other">
Krone MR, Wald A, Tabet SR, Paradise M, Corey L, Celum CL. Herpes
simplex virus type 2 shedding in human immunodeficiency virus-
negative men who have sex with men: frequency, patterns, and risk
factors. Clin Infect Dis 2000;30:261-7.</mixed-citation>
         </ref>
         <ref id="d218e519a1310">
            <label>13</label>
            <mixed-citation id="d218e526" publication-type="other">
Holmberg SD, Stewart JA, Gerber AR, et al. Prior herpes simplex virus
type 2 infection as a risk factor for HIV infection. JAMA 1988; 259:
1048-50.</mixed-citation>
         </ref>
         <ref id="d218e539a1310">
            <label>14</label>
            <mixed-citation id="d218e546" publication-type="other">
Keet IP, Lee FK, van Griensven GJ, Lange JM, Nahmias A, Coutinho
RA. Herpes simplex virus type 2 and other genital ulcerative infections
as a risk factor for HIV-1 acquisition. Genitourin Med 1990;66:330-3.</mixed-citation>
         </ref>
         <ref id="d218e559a1310">
            <label>15</label>
            <mixed-citation id="d218e566" publication-type="other">
Kingsley LA, Armstrong J, Rahman A, Ho M, Rinaldo CR Jr. No
association between herpes simplex virus type-2 seropositivity or ano-
genital lesions and HIV seroconversion among homosexual men. J
Acquir Immune Defic Syndr 1990;3:773-9.</mixed-citation>
         </ref>
         <ref id="d218e582a1310">
            <label>16</label>
            <mixed-citation id="d218e589" publication-type="other">
Berglund T, Fredlund H, Giesecke J. Epidemiology of the reemergence
of gonorrhea in Sweden. Sex Transm Dis 2001;28:111-4.</mixed-citation>
         </ref>
         <ref id="d218e600a1310">
            <label>17</label>
            <mixed-citation id="d218e607" publication-type="other">
Stall RD, Hays RB, Waldo CR, Ekstrand M, McFarland W. The gay
'905: a review of research in the 1990s on sexual behavior and HIV
risk among men who have sex with men. AIDS 2000; 14:8101-14.</mixed-citation>
         </ref>
         <ref id="d218e620a1310">
            <label>18</label>
            <mixed-citation id="d218e627" publication-type="other">
Whittington WLH, Collis T, Dithmer-Schreck D, et al. Sexually trans-
mitted diseases and human immunodeficiency virus-discordant part-
nerships among men who have sex with men. Clin Infect Dis 2002;
35:1010-7.</mixed-citation>
         </ref>
         <ref id="d218e643a1310">
            <label>19</label>
            <mixed-citation id="d218e650" publication-type="other">
Katz MH, Schwarcz SK, Kellogg TA, et al. Impact of highly active
antiretroviral treatment on HIV seroincidence among men who have
sex with men: San Francisco. Am J Public Health 2002; 92:388-94.</mixed-citation>
         </ref>
         <ref id="d218e663a1310">
            <label>20</label>
            <mixed-citation id="d218e670" publication-type="other">
Seage GR 3rd, Holte SE, Metzger DS, et al. Are US populations appro-
priate for trials of human immunodeficiency virus vaccine? The HIVNET
Vaccine Preparedness Study. Am J Epidemiol 2001; 153:619-27.</mixed-citation>
         </ref>
         <ref id="d218e683a1310">
            <label>21</label>
            <mixed-citation id="d218e690" publication-type="other">
Buchbinder SP, Heagerty PJ, Mayer KH, et al. Risk factors for HIV
seroconversion in a contemporary cohort of high risk men who have
sex with men (MSM) [abstract 23350]. In: Bridging the gap: conference
record of the 12th World AIDS Conference (Geneva). Stockholm: In-
ternational AIDS Society, 1998:411.</mixed-citation>
         </ref>
         <ref id="d218e709a1310">
            <label>22</label>
            <mixed-citation id="d218e716" publication-type="other">
Ashley RL, Militoni J, Lee F, Nahmias A, Corey L. Comparison of
Western blot (immunoblot) and glycoprotein G-specific immunodot
enzyme assay for detecting antibodies to herpes simplex virus types 1
and 2 in human sera. J Clin Microbiol 1988;26:662-7.</mixed-citation>
         </ref>
         <ref id="d218e733a1310">
            <label>23</label>
            <mixed-citation id="d218e740" publication-type="other">
Gray RH, Wawer MJ, Serwadda D, et al. Serologic HSV-2 associated
with HIV acquisition/transmission in discordant couples and the gen-
eral population: Rakai, Uganda. Int J STD AIDS 2001; 12(Suppl 2):64.</mixed-citation>
         </ref>
         <ref id="d218e753a1310">
            <label>24</label>
            <mixed-citation id="d218e760" publication-type="other">
del Mar Pujades Rodriguez M, Obasi A, Mosha F, et al. Herpes simplex
virus type 2 infection increases HIV incidence: a prospective study in
rural Tanzania. AIDS 2002; 16:451-62.</mixed-citation>
         </ref>
         <ref id="d218e773a1310">
            <label>25</label>
            <mixed-citation id="d218e780" publication-type="other">
Auvert B, Ballard R, Campbell C, et al. HIV infection among youth in
a South African mining town is associated with herpes simplex virus-2
seropositivity and sexual behaviour. AIDS 2001; 15:885-98.</mixed-citation>
         </ref>
         <ref id="d218e793a1310">
            <label>26</label>
            <mixed-citation id="d218e800" publication-type="other">
Lucchetti A, Sanchez JL, Collis TK, et al. Bacterial STDs and HSV-2
acquisition among MSM HIV seroconverters in Peru [abstract 370].
In: Final programme and abstracts of the 1st International AIDS Society
Conference on HIV Pathogenesis and Treatment (Buenos Aires). Stock-
holm: International AIDS Society, 2001:187.</mixed-citation>
         </ref>
         <ref id="d218e819a1310">
            <label>27</label>
            <mixed-citation id="d218e826" publication-type="other">
Reynolds SJ, Risbud AR, Shepherd ME, et al. Recent herpes simplex
virus type 2 infection and the risk of HIV acquisition in Pune, India
[abstract MoOrC1012]. In: Knowledge and commitment for action:
abstract book of the 14th Internationl AIDS Conference (Barcelona,
Spain), vol 1. Stockholm: International AIDS Society, 2002:88.</mixed-citation>
         </ref>
         <ref id="d218e845a1310">
            <label>28</label>
            <mixed-citation id="d218e852" publication-type="other">
Koelle DM, Benedetti J, Langenberg A, Corey L. Asymptomatic re-
activation of herpes simplex virus in women after the first episode of
genital herpes. Ann Intern Med 1992; 116:433-7.</mixed-citation>
         </ref>
         <ref id="d218e866a1310">
            <label>29</label>
            <mixed-citation id="d218e873" publication-type="other">
Fleming DT, McQuillan GM, Johnson RE, et al. Herpes simplex virus
type 2 in the United States, 1976 to 1994. N Engl J Med 1997; 337:
1105-11.</mixed-citation>
         </ref>
         <ref id="d218e886a1310">
            <label>30</label>
            <mixed-citation id="d218e893" publication-type="other">
Wald A, Zeh J, Selke SA, Ashley RL, Corey L. Virologic characteristics
of subclinical and symptomatic genital herpes infections. N Engl J Med
1995;333:770-5.</mixed-citation>
         </ref>
         <ref id="d218e906a1310">
            <label>31</label>
            <mixed-citation id="d218e913" publication-type="other">
Wald A, Zeh J, Selke S, Warren T, Ashley R, Corey L. Genital shedding
of herpes simplex virus among men. J Infect Dis 2002; 186(Suppl 1):
S34-9.</mixed-citation>
         </ref>
         <ref id="d218e926a1310">
            <label>32</label>
            <mixed-citation id="d218e933" publication-type="other">
Wald A, Corey L, Cone R, Hobson A, Davis G, Zeh J. Frequent genital
herpes simplex virus 2 shedding in immunocompetent women: effect
of acyclovir treatment. J Clin Invest 1997;99:1092-7.</mixed-citation>
         </ref>
         <ref id="d218e946a1310">
            <label>33</label>
            <mixed-citation id="d218e953" publication-type="other">
Wald A, Zeh J, Selke SA, et al. Reactivation of genital herpes simplex
virus type 2 infection in asymptomatic seropositive persons. N Engl J
Med 2000;342:844-50.</mixed-citation>
         </ref>
         <ref id="d218e966a1310">
            <label>34</label>
            <mixed-citation id="d218e973" publication-type="other">
Lafferty WE, Downey L, Celum C, Wald A. Herpes simplex virus type
1 as a cause of genital herpes: impact on surveillance and prevention.
J Infect Dis 2000; 181:1454-7.</mixed-citation>
         </ref>
         <ref id="d218e987a1310">
            <label>35</label>
            <mixed-citation id="d218e994" publication-type="other">
Koelle DM, Wald A. Herpes simplex virus: the importance of asymp-
tomatic shedding. J Antimicrob Chemother 2000;45:1-8.</mixed-citation>
         </ref>
         <ref id="d218e1004a1310">
            <label>36</label>
            <mixed-citation id="d218e1011" publication-type="other">
Krone MR, Tabet SR, Paradise MA, Wald A, Corey L, Celum CL. Her-
pes simplex virus shedding among human immunodeficiency virus-
negative men who have sex with men: site and frequency of shedding.
J Infect Dis 1998;178:978-82.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
