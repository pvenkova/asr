<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt234xbb</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt24h91v</book-id>
      <subj-group>
         <subject content-type="lcsh">Policy sciences</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Public administration</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Delivering Policy Reform</book-title>
         <subtitle>Anchoring Significant Reforms in Turbulent Times</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Lindquist</surname>
               <given-names>Evert A.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Vincent</surname>
               <given-names>Sam</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Wanna</surname>
               <given-names>John</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>04</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="ppub">9781921862182</isbn>
      <isbn content-type="epub">9781921862199</isbn>
      <publisher>
         <publisher-name>ANU E Press</publisher-name>
         <publisher-loc>Canberra</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>ANU E Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt24h91v"/>
      <abstract abstract-type="short">
         <p>Predictable and unpredictable challenges continually confront the policy settings and policy frameworks of governments. They provide a constantly changing dynamic within which policy-making operates. Governments at all levels are asking their public services to identify innovative and workable reforms to anticipate and address these challenges. Public service leaders around the world are struggling not only to better anticipate emerging demands but also to address reform backlogs. However, time and time again, major policy reforms can prove tough to implement - especially in turbulent environments - and even tougher to anchor over time. This leads to considerable uncertainty and inefficiency as governments and policy communities try to keep pace with change. Policies that unravel or are dismantled are costly and represent wasted opportunities. They lead to cynicism about the effectiveness of governments and public service advice more generally, making it more difficult to deal with other emerging challenges. This volume of proactive essays on delivering policy reform offers an intriguing blend of strategic policy advice and management insight. It brings together a diverse range of highquality contributors from overseas as well as from Australia and New Zealand - including national political leaders, public service executives, heads of independent agencies, and leading international scholars.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fels</surname>
                           <given-names>Allan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract abstract-type="extract">
                     <p>This volume of essays had its origins in our thinking about the global financial crisis—when, in 2008, the major economic shock emerging out of the mortgage and financial sector in the United States swept around the world with disastrous consequences. High-risk sub-prime lending in the United States might have precipitated the crisis, but its full impacts affected almost every nation and every industry sector as banks collapsed, credit dried up, share markets and property markets fell spectacularly and government budgets took a big hit in deficit and debt levels. The crisis suddenly and unexpectedly posed a major threat to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.4</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.5</book-part-id>
                  <title-group>
                     <label>1.</label>
                     <title>Delivering policy reform:</title>
                     <subtitle>making it happen, making it stick</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lindquist</surname>
                           <given-names>Evert A.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wanna</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Cascading and often unpredictable challenges continually confront the policy settings and policy frameworks of governments. They provide a constantly changing dynamic within which policy making operates. Sometimes these challenges are born of crises, sometimes of more systematic forces such as climate change or environmental threats. Some are the result of social and demographic trends, such as ageing or social inclusion. Sometimes they emerge by stealth, travelling under the radar until they suddenly manifest themselves. Such transformative changes are occurring across many fields simultaneously: technological, economic, international, defence and security related, environmental, social and demographic. They force governments to rethink what</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.6</book-part-id>
                  <title-group>
                     <label>2.</label>
                     <title>‘Don’t waste the crisis’:</title>
                     <subtitle>the agenda for public-policy reforms in a turbulent world</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>de Geus</surname>
                           <given-names>Aart</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>13</fpage>
                  <abstract abstract-type="extract">
                     <p>A number of Organisation for Economic Cooperation and Development (OECD) member governments told us during the early 2000s that effective policy making was not only about ‘where to go’, but also about ‘how to get there’. We heard ministers ask ‘how to reform and to be re-elected’ and ‘how to reform and perform’. Almost all OECD countries face medium and long-term structural challenges in the context of global imbalances, economic recalibration, climate change, population ageing, and so on. On the other hand, these governments have implemented policy reforms in a wide range of domains, with a view to enhancing living</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.7</book-part-id>
                  <title-group>
                     <label>3.</label>
                     <title>Making reforms sustainable:</title>
                     <subtitle>lessons from the American policy reform experience</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Patashnik</surname>
                           <given-names>Eric M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>27</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter concerns my most recent research on making policy reforms sustainable in the US policy context, and will synthesise and build upon themes developed in my 2008 book, <italic>Reforms at Risk: What happens after major policy changes are enacted.</italic> What follows has four interconnected parts. First, I explore why it is important to strive for sustainable reforms. Second, I consider the puzzle of reform: why some reforms ‘stick’ and why others do not. I do this by introducing several concepts and by tapping into several case studies of policy reform in the United States. Third, with several cases in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.8</book-part-id>
                  <title-group>
                     <label>4.</label>
                     <title>How to design and deliver reform that makes a real difference:</title>
                     <subtitle>what recent history has taught us as a nation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kelly</surname>
                           <given-names>Paul</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>43</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter is structured with three objectives in mind. First, it describes the current state of Australia’s political culture, which prizes expediency and timidity over boldness and reform zeal—highlighting the problems this then raises for much-needed further reform efforts. Second, it identifies the contributing factors that made the previous, post-1983 reform era so successful in Australia, drawing out the lessons from that history. And third, it will examine where we ought to go from here in terms of future policy reforms. Undoubtedly, the most speculative part of the chapter will concern the future solutions, simply because they are the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.9</book-part-id>
                  <title-group>
                     <label>5.</label>
                     <title>The ‘new responsibility model’ for New Zealand public-sector CEOs</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>English</surname>
                           <given-names>Bill</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract abstract-type="extract">
                     <p>I approach the topic of public-sector change from a finance minister’s perspective, speculating on how we might embrace further reform over the next 10 years. It is a review that comes from a number of years observing this important topic, starting with my experience as a junior Treasury official when Roger Douglas was New Zealand’s Minister of Finance, developing when I entered Parliament in 1990 when the government I was part of was undertaking a program of dramatic reform, and, most recently, continuing over a number of years spent in opposition, where I had time to ponder and reflect on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.10</book-part-id>
                  <title-group>
                     <label>6.</label>
                     <title>A portent of things to come:</title>
                     <subtitle>lessons from a reforming minister</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tanner</surname>
                           <given-names>Lindsay</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>65</fpage>
                  <abstract abstract-type="extract">
                     <p>This contribution was written during the 2010 election campaign—a period when some commentators were suggesting the age of reform was over. Indeed, the first half of 2010 saw a series of extraordinary events that punctuated what perhaps would otherwise have been a steady period of reform progress in Australia. But these should not be viewed as a portent of things to come.</p>
                     <p>I was directly involved in reform efforts over the past three years in my capacity as Minister for Finance, and indirectly as a shadow minister for a much longer period. Shadow ministers have a bigger influence than</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.11</book-part-id>
                  <title-group>
                     <label>7.</label>
                     <title>The agenda for achieving a world-class public sector:</title>
                     <subtitle>making reforms that matter in the face of challenges</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sedgwick</surname>
                           <given-names>Stephen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>75</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter begins by discussing some key issues surrounding public-sector reform. It will then explore public-sector change at the national level in Australia since the modern reform era began in the mid 1970s, and the implications for us today. My primary focus will be directed towards the comprehensive reform program set out in the landmark publication <italic>Ahead of the Game: Blueprint for the reform of Australian government administration</italic> (Moran 2010), which is in the process of being implemented, subject to the policy priorities of the Gillard government.¹ The chapter will conclude with some thoughts about what tomorrow might bring and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.12</book-part-id>
                  <title-group>
                     <label>8.</label>
                     <title>Collaborative reform:</title>
                     <subtitle>lessons from the COAG Reform Council, 2008–2010</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>O’Loughlin</surname>
                           <given-names>Mary Ann</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>91</fpage>
                  <abstract abstract-type="extract">
                     <p>During the 2007 federal election campaign, Kevin Rudd pledged to reform Commonwealth–state relations if elected—an unusual mandate to request, given that Commonwealth–state relations have rarely garnered much community interest.¹ But Rudd was very experienced in this field, having worked in both the Commonwealth Public Service and the Queensland Public Service, as well as serving as chief of staff to former Queensland Premier Wayne Goss. In the lead-up to the election, Rudd promised to ‘end the blame game’, with a primary focus on health. This message resonated with the community, allowing Rudd to take a reform agenda for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.13</book-part-id>
                  <title-group>
                     <label>9.</label>
                     <title>Entrenching ‘Rogernomics’ in New Zealand:</title>
                     <subtitle>political and academic perspectives</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Boston</surname>
                           <given-names>Jonathan</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Douglas</surname>
                           <given-names>Roger</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>99</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter looks at the case study of public-sector reform in New Zealand in the 1980s and early 1990s, and what underpinned the success in moving this significant reform forward. I will briefly outline the nature of the reforms in question, then propose some explanations as to how these comprehensive, radical, and rapidly introduced reforms were made possible. I will conclude by analysing the outcomes of these reforms, before finally drawing some lessons from the experience. In the second half of the chapter, Sir Roger Douglas will offer his observations from the reform process, as one of its major architects.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.14</book-part-id>
                  <title-group>
                     <label>10.</label>
                     <title>Institutional renewal and reform:</title>
                     <subtitle>the challenge of the Commonwealth of Nations</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kirby</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>109</fpage>
                  <abstract abstract-type="extract">
                     <p>In July 2010, I was appointed to the Eminent Persons Group (EPG) established by the Commonwealth Heads of Government Meeting (CHOGM) held in Trinidad and Tobago in 2009. At that meeting were gathered the leaders of the 53 Commonwealth nations. They decided to establish the EPG in order to investigate, and report on, the essential ingredients of reform to the institutional arrangements of this global family of Commonwealth nations. There was a sense that in this era of institutional reform, there might be lessons applicable to the Commonwealth of Nations for the attainment of desirable identified objectives. At the least,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.15</book-part-id>
                  <title-group>
                     <label>11.</label>
                     <title>Tackling cartels:</title>
                     <subtitle>lessons for making and entrenching reform</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kovacic</surname>
                           <given-names>William E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>123</fpage>
                  <abstract abstract-type="extract">
                     <p>My background is teaching law and my natural habitat is the university, but for nine of the past 10 years I have been tackling national and international cartels and seeing theory meet practice at a federal institution, the Federal Trade Commission. For me, the deepest education in that process has been realising how hard it is not only to get seemingly straightforward things done, but also to make certain that changes or adjustments in a policy stick. This chapter will focus on those experiences.</p>
                     <p>I think often of the work of my historian colleague Ronald Spector, who writes about organisations</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.16</book-part-id>
                  <title-group>
                     <label>12.</label>
                     <title>The overhaul of Australian immigration practices, 2005–2010</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Metcalfe</surname>
                           <given-names>Andrew</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>131</fpage>
                  <abstract abstract-type="extract">
                     <p>Over the past five years, the Department of Immigration and Citizenship (DIAC) has sought to reform and transform our culture and business practices. This endeavour has come about as a direct result of the department’s previous history of maladministration in cases such as Cornelia Rau’s unlawful detention in 2004 and the subsequent <italic>Palmer Report</italic> her case provoked. This chapter will analyse the progress that we are making, characterised by attempts to build stronger visa, migration and citizenship services. It will also explore the challenges we have faced in implementing this organisational change, and our future plans for the department.</p>
                     <p>First,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.17</book-part-id>
                  <title-group>
                     <label>13.</label>
                     <title>Getting integrity reforms adopted internationally</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pope</surname>
                           <given-names>Jeremy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>145</fpage>
                  <abstract abstract-type="extract">
                     <p>Throughout the Cold War, the topic of corruption on the international stage was virtually taboo. Development agencies could not or would not discuss it; international financial institutions closed their eyes to it; representatives of Western governments engaged in it when it suited them; and the private sector saw it simply as an unpleasant but increasingly expensive way to get things done. The silence was deafening. There were no reliable estimates of the extent to which development aid in particular was being siphoned off into bribes and kickbacks, let alone any analysis of the impact on fundamental human rights of what</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.18</book-part-id>
                  <title-group>
                     <label>14.</label>
                     <title>Sustaining water reform in Australia</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Matthews</surname>
                           <given-names>Ken</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>167</fpage>
                  <abstract abstract-type="extract">
                     <p>Australia is the driest inhabited continent in the world, so effective water management is of critical importance. In this chapter, I will analyse the reform process of Australian water management. I will first explore the role of the National Water Commission (NWC)—a somewhat misunderstood body—in the process. I will next discuss the specific needs for reform in this field as identified by the commission, and address how best to approach, build and sustain water-management reform. I will finally present the National Water Commission’s recommendations to the Council of Australian Governments (COAG) on water reform, and our appraisal of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.19</book-part-id>
                  <title-group>
                     <label>15.</label>
                     <title>Up in smoke:</title>
                     <subtitle>combating tobacco through legislative reform</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Moodie</surname>
                           <given-names>Rob</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>181</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter focuses on tobacco control and the reform of tobacco use in Australia—policy areas I have been involved in now for about 12 years. Combating the harm caused by tobacco has been a long battle, involving a series of difficult policy reforms. To comprehend how much progress has been made to date, think back to the famous 1946 Camel cigarettes advertisement, ‘more doctors smoke Camel than any other cigarette’, which resulted from a survey of 113 000 US doctors. Ronald Reagan sent packets of Chesterfields to people for Christmas. General practitioners would even prescribe cigarettes to their patients;</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.20</book-part-id>
                  <title-group>
                     <label>16.</label>
                     <title>Improving road safety:</title>
                     <subtitle>perspectives from Victoria’s Transport Accident Commission</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dore</surname>
                           <given-names>Janet</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>193</fpage>
                  <abstract abstract-type="extract">
                     <p>Victoria became the first jurisdiction in the world to introduce compulsory seatbelts in 1970. Since then the state has become a pioneer in making progress on road trauma and reducing the death rate on the state’s roads. This chapter will outline the challenges involved in getting to this stage, the progress made, and the reforms that are still to be achieved.</p>
                     <p>In the 1960s the road toll in Victoria was more than 1000 deaths per year, and by the 1970s it was still more than 800 deaths. To address this, the Transport Accident Commission (TAC) was established in 1986 and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h91v.21</book-part-id>
                  <title-group>
                     <label>17.</label>
                     <title>Epilogue:</title>
                     <subtitle>rules for reformers</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hart</surname>
                           <given-names>Paul ’t</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>201</fpage>
                  <abstract abstract-type="extract">
                     <p>There is a widespread need for adaptation, change and even ‘paradigm shifts’ in the way societies are governed and how their governments organise themselves. Many contributions to this volume highlight this need. Let us look at some of the main drivers.</p>
                     <p>Citizens, companies and governments everywhere are, first of all, trying to come to terms with the true implications of the information age. The boardroom and street-level consequences of life in the information society are challenging the system-level architecture of governance. Technologically driven possibilities and culturally embedded expectations now demand that governments follow corporations and engage in mass customisation—responsive,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
