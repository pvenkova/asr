<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26332071</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Pragmatic tradition or romantic aspiration? The causes of impulsive marriage and early divorce among women in rural Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Bertrand-Dansereau</surname>
                  <given-names>Anaïs</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Clark</surname>
                  <given-names>Shelley</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>McGill University, Montreal, Canada.</aff>
            <aff id="af2">
               <label>²</label>McGill University, Montreal, Canada.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>7</month>
            <year>2016</year>
            <string-date>JULY - DECEMBER 2016</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>12</month>
            <year>2016</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">35</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26332068</issue-id>
         <fpage>47</fpage>
         <lpage>80</lpage>
         <permissions>
            <copyright-statement>©2016 Anaïs Bertrand-Dansereau &amp; Shelley Clark</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26332071"/>
         <abstract xml:lang="eng">
            <sec>
               <label>BACKGROUND</label>
               <p>Despite increased attention to shifting union-formation processes, there is little consensus as to which is more stable, modern unions or traditional marriages. This is especially relevant in Malawi, where divorce is common.</p>
            </sec>
            <sec>
               <label>OBJECTIVES</label>
               <p>We investigate what individual, family, and relationship characteristics are associated with early divorce, and how unions with these characteristics make sense in the lives of young women.</p>
            </sec>
            <sec>
               <label>METHODS</label>
               <p>We draw on the 2006 wave of the Malawi Longitudinal Study of Families and Health (MLSFH) and on qualitative peer interviews by young people. We first investigate the prevalence of divorce by time since first union and then estimate a logistic discrete-time hazard model to test the association between individual, family, and relationship characteristics and early divorce. Finally, we use a thematic analysis of qualitative data to understand the social context of fragile relationships.</p>
            </sec>
            <sec>
               <label>RESULTS</label>
               <p>The first three years of marriage exhibit the highest rates of divorce. Women who marry someone they have known for a short time and whose relationship is not embedded in family ties are more likely to divorce early. These impulsive marriages reflect characteristics that are borrowed from both modern and traditional cultural repertoires. Their fragility stems from the absence of both family involvement and a strong emotional bond between spouses.</p>
            </sec>
            <sec>
               <label>CONTRIBUTION</label>
               <p>This research bridges the demographic literature on divorce in sub-Saharan Africa with anthropological inquiry into the globalization of romantic courtship and companionate marriage. We show that hybrid impulsive unions are more fragile than either modern or traditional unions.</p>
            </sec>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Abah, A.L. (2009). Popular culture and social change in Africa: The case of the Nigerian video industry. Media, Culture &amp; Society 31(5): 731‒748. doi:10.1177/0163443709339456.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Angotti, N. and Kaler, A. (2013). The more you learn the less you know? Interpretive ambiguity across three modes of qualitative data. Demographic Research 28(33): 951‒980. doi:10.4054/DemRes.2013.28.33.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Archambault, J.S. (2011). Breaking up ‘because of the phone’ and the transformative potential of information in southern Mozambique. New Media &amp; Society 13(3): 444‒456. doi:10.1177/1461444810393906.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Bankole, A., Biddlecom, A., Guiella, G., Singh, S., and Zulu, E. (2007). Sexual behavior, knowledge and information sources of very young adolescents in four sub-Saharan African countries. African Journal of Reproductive Health 11(3): 28‒43. doi:10.2307/25549730.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Behrend, H. (1998). Love à la Hollywood and Bombay in Kenyan studio photography. Paideuma 44: 139‒153.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Boileau, C., Clark, S., Bignami-Van Assche, S., Poulin, M., Reniers, G., and Watkins, S.C. (2009). Sexual and marital trajectories and HIV infection among ever-married women in rural Malawi. Sexually Transmitted Infections 85(Suppl 1): i27‒i33. doi:10.1136/sti.2008.033969.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Brown, L. and Strega, S. (2005). Introduction: transgressive possibilities. In Brown, L. and Strega, S. (eds.). Research as resistance: Critical, indigenous, and anti-oppressive approaches. Toronto: Canadian Scholar’s Press/Women’s Press: 1‒17. doi:10.1016/j.amjcard.2005.06.022.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Chae, S. (2016). Forgotten marriages? Measuring the reliability of marriage histories. Demographic Research 34(19): 525‒562. doi:10.4054/DemRes.2016.34.19.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Chimango, L. (1977). Woman without ankhoswe in Malawi. African Legal Studies 15: 54‒61. doi:10.1080/07329113.1977.10756240.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Clark, S. and Brauner-Otto, S. (2015). Divorce in sub-Saharan Africa: Are unions becoming less stable? Population and Development Review 41(4): 583‒605. doi:10.1111/j.1728-4457.2015.00086.x.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Clark, S., Kabiru, C., and Mathur, R. (2010). Relationship transitions among youth in urban Kenya. Journal of Marriage and Family 72(1): 73‒88. doi:10.1111/j.1741 -3737.2009.00684.x.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Cole, J. and Thomas, L.M. (eds.) (2009). Love in Africa. Chicago: University of Chicago Press. doi:10.7208/chicago/9780226113555.001.0001.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Fair, L. (2009). Making love in the Indian Ocean: Hindi films, Zanzibari audiences, and the construction of romance in the 1950s and 1960s. In Cole, J. and Thomas, L.M. (eds.). Love in Africa. Chicago: University of Chicago Press: 58‒82. doi:10.7208/chicago/9780226113555.003.0003.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Forster, P.G. (1994). Culture, nationalism, and the invention of tradition in Malawi. The Journal of Modern African Studies 32(3): 477‒497. doi:10.1017/S0022278X00 015196.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Freeman, E.K. and Coast, E. (2014). Sex in older age in rural Malawi. Ageing and Society 34(7): 1118‒1141. doi:10.1017/S0144686X12001481.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Goode, W.J. (1993). World changes in divorce patterns. New Haven and London: Yale University Press.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Grant, M.J. and Soler-Hampejsek, E. (2014). HIV risk perceptions, the transition to marriage, and divorce in Southern Malawi. Studies in Family Planning 45(3): 315‒337. doi:10.1111/j.1728-4465.2014.00394.x.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Hirsch, J.S., and Wardlow, H. (2006). Modern loves: The anthropology of romantic courtship and companionate marriage. Ann Arbor: University of Michigan Press. doi:10.3998/mpub.170440.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Jonason, P.K. (2013). Four functions for four relationships: Consensus definitions of university students. Archives of Sexual Behavior 42(8): 1407‒1414. doi:10.1007/s10508-013-0189-7.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Jones, G.W. (1997). Modernization and divorce: Contrasting trends in Islamic Southeast Asia and the West. Population and Development Review 23(1): 95‒114. doi:10.2307/2137462.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Kaler, A. (2006). “When they see money, they think it’s life”: Money, modernity and morality in two sites in rural Malawi. Journal of Southern African Studies 32(2): 335‒349. doi:10.1080/03057070600656333.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Kambalame, J., Chidzalo, E.P., and Chadangalara, J.W.M. (1946). Our African way of life. London: United society for Christian literature and Lutterworth Press.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Klocker, N. (2012). Conducting sensitive research in the present and past tense: Recounting the stories of current and former child domestic workers. Geoforum 43(5): 894‒904. doi:10.1016/j.geoforum.2012.05.012.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Kohler, H.-P., Watkins, S.C., Behrman, J.R., Anglewicz, P., Kohler, I.V., and Thornton, R.L. (2015). Cohort profile: The Malawi longitudinal study of families and health (MLSFH). International Journal of Epidemiology 44(2): 394‒404. doi:10.1093/ije/dyu049.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Mair, L.P. (1951). Marriage and family in the Dedza district of Nyasaland. The Journal of the Royal Anthropological Institute of Great Britain and Ireland 81(1/2): 103‒119. doi:10.2307/2844018.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Manglos, N. (2011). Brokerage in the sacred sphere: Religious leaders as community problem solvers in rural Malawi. Sociological Forum 26(2): 334‒355. doi:10.1111/j.1573-7861.2011.01243.x.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Mhone, C. (1994). Draconian dress act repealed. Southern Africa Political &amp; Economic Monthly 7(3‒4): 20.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Mojola, S.A. (2014). Providing women, kept men: Doing masculinity in the wake of the African HIV/AIDS epidemic. Signs 39(2): 341‒363. doi:10.1086/673086.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Mwambene, L. (2012). Custody disputes under African customary family law in Malawi: Adaptability to change? International Journal of Law, Policy and the Family 26(2): 127‒142. doi:10.1093/lawfam/ebs004.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Okonkwo, A.D. (2010). Gender and sexual risk-taking among selected Nigerian university students. Sexuality &amp; Culture 14(4) 270‒305. doi:10.1007/s12119-010-9074-x.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Padilla, M., Hirsch, J.S., Munoz-Laboy, M., Sember, R.E., and Parker, R.G. (2007). Love and globalization. Nashville: Vanderbilt University Press.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Pattman, R. (2005). “Boys and girls should not be too close”: Sexuality, the identities of African boys and girls and HIV/AIDS education. Sexualities 8(4): 497‒516. doi:10.1177/1363460705056623.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Poulin, M. (2007). Sex, money, and premarital partnerships in southern Malawi. Social Science &amp; Medicine 65(11): 2383‒2393. doi:10.1016/j.socscimed.2007.05.030.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Ranger, T. (1997 [1983]). The invention of tradition in colonial Africa. In Grinker, R.R., Lubkemann, S.C. and Steiner, C. (eds.). Perspectives on Africa: A Reader in Culture, History, and Representation. Oxford: Wiley-Blackwell.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Reniers, G. (2003). Divorce and remarriage in rural Malawi. Demographic Research Special Collection 1(6): 175‒206. doi:10.4054/DemRes.2003.S1.6.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Ryan, L., Kofman, E., and Aaron, P. (2011). Insiders and outsiders: Working with peer researchers in researching Muslim communities. International Journal of Social Research Methodology 14(1): 49‒60. doi:10.1080/13645579.2010.481835.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Sadgrove, J. (2007). “Keeping up appearances”: Sex and religion amongst university students in Uganda. Journal of Religion in Africa 37(1): 116‒144. doi:10.1163/157006607X166618.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Schatz, E. (2005). “Take your mat and go!”: Rural Malawian women’s strategies in the HIV/AIDS era. Culture, Health &amp; Sexuality 7(5): 479‒492. doi:10.1080/1369 1050500151255.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Selikow, T.-A. and Mbulaheni, T. (2013). “I do love him but at the same time I can’t eat love”: Sugar daddy relationships for conspicuous consumption amongst urban university students in South Africa. Agenda 27(2): 86‒98.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Smith, D.J. (2001). Romance, parenthood, and gender in a modern African society. Ethnology 40(2): 129‒151. doi:10.2307/3773927.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Smith, D.J. (2009). Managing men, marriage and modern love: Women’s perspectives on intimacy and male infidelity in southeastern Nigeria. In Cole, J. and Thomas, L.M. (eds.). Love in Africa: University of Chicago Press: 157‒180. doi:10.7208/chicago/9780226113555.003.0007.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Smith, D.J. (2010). Promiscuous girls, good wives, and cheating husbands: Gender inequality, transitions to marriage, and infidelity in southeastern Nigeria. Anthropological Quarterly 83(1): 123‒152. doi:10.1353/anq.0.0118.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Swidler, A. (1986). Culture in action: Symbols and strategies. American Sociological Review 51(2): 273‒286. doi:10.2307/2095521.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Swidler, A. (2003). Talk of love: How culture matters. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Swidler, A., and Watkins, S.A. (2007). Ties of dependence: AIDS and transactional sex in rural Malawi. Studies in Family Planning 38(3): 147‒162. doi:10.1111/j.172 8-4465.2007.00127.x.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Takyi, B.K. (2001). Marital instability in an African society: Exploring the factors that influence divorce processes in Ghana. Sociological Focus 34(1): 77‒96. doi:10.1080/00380237.2001.10571184.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Thomas, L. (2009). Love, sex, and the modern girl in 1930s southern Africa. In Cole, J. and Thomas, L. (eds.). Love in Africa. Chicago: University of Chicago Press: 31‒57. doi:10.7208/chicago/9780226113555.003.0002.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Tilson, D. and Larsen, U. (2000). Divorce in Ethiopia: The impact of early marriage and childlessness. Journal of Biosocial Science 32(3): 355‒372. doi:10.1017/S0021932000003552.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Trinitapoli, J. (2009). Religious teachings and influences on the ABCs of HIV prevention in Malawi. Social Science &amp; Medicine 69(2): 199‒209. doi:10.1016/j.socscimed.2009.04.018.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Trinitapoli, J. (2011). The AIDS-related activities of religious leaders in Malawi. Global Public Health 6(1): 41‒55. doi:10.1080/17441692.2010.486764.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Trinitapoli, J. and Weinreb, A. (2012). Religion and AIDS in Africa. Oxford: Oxford University Press. doi:10.1093/acprof:oso/9780195335941.001.0001. van Pelt, I.S. and Ryen, A. (2015). Self-efficacy in protecting oneself against HIV transmission: A qualitative study of female university students in Malawi. Journal of Comparative Social Work 10(2).</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Wanda, B.P. (1988). Customary family law in Malawi: Adherence to tradition and adaptability to change. The Journal of Legal Pluralism and Unofficial Law 20(27): 117‒134. doi:10.1080/07329113.1988.10756407.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Wardlow, H. and Hirsch, J.S. (2006). Introduction. In Hirsch, J.S. and Wardlow, H. (eds.). Modern loves: The anthropology of romantic courtship &amp; companionate marriage. Ann Arbor: University of Michigan Press.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Watkins, S. and Swidler, A. (2009). Hearsay ethnography: Conversational journals as a method for studying culture in action. Poetics 37(2): 162‒184. doi:10.1016/j.poetic.2009.03.002.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Watkins, S.C. (2004). Navigating the AIDS epidemic in rural Malawi. Population and Development Review 30(4): 673‒705. doi:10.1111/j.1728-4457.2004.00037.x.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Yeatman, S.E. and Trinitapoli, J. (2008). Beyond denomination: The relationship between religion and family planning in rural Malawi. Demographic Research 19(55): 1851‒1882. doi:10.4054/DemRes.2008.19.55.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
