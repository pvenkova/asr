<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jepidcommheal</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000490</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Epidemiology and Community Health (1979-)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>BMJ Publishing Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0143005X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14702738</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23071782</article-id>
         <article-categories>
            <subj-group>
               <subject>Research report</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Child sexual abuse and links to HIV and orphanhood in urban Zimbabwe</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Isolde J</given-names>
                  <surname>Birdthistle</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sian</given-names>
                  <surname>Floyd</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Stewart</given-names>
                  <surname>Mwanasa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Auxillia</given-names>
                  <surname>Nyagadza</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Edmore</given-names>
                  <surname>Gwiza</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Judith R</given-names>
                  <surname>Glynn</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">65</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23071778</issue-id>
         <fpage>1075</fpage>
         <lpage>1082</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 BMJ Publishing Group</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23071782"/>
         <abstract>
            <p>Background: Evidence of a link between sexual violence and HIV is growing; however, studies among children are scarce. The authors sought to characterise child sexual abuse in Harare, Zimbabwe, and explore its links with HIV and orphanhood. Methods: Records for new clients attending a child sexual abuse clinic from July 2004 to June 2005 were computerised and reviewed. Information on characteristics, medical examinations, laboratory tests and perpetrators were summarised. Orphan prevalence was compared with Demographic and Health Survey (DHS) 2005/2006 data for Harare, and a household-based survey in a neighbouring community. Results: Over 1 year, 1194 new clients (90% female) aged 7 weeks to 16 years were assessed, with 93% of boys and 59% of girls classified clinically as prepubertal. 94% of clients reported penetrative sexual abuse, occurring most often in the child's home. Most perpetrators were identified as relatives or neighbours by children under 12 years, and 'boyfriends' by adolescent girls. At presentation, 31/520 (6%) clients tested were HIV-positive. Where recorded, 39 (6%) clients presented within 3 days of abuse, and 36 were given postexposure prophylaxis for HIV (PEP). Among female clients, orphan prevalence was higher than in the DHS (OR=1.7; 1.4 to 2.2) and neighbouring community (OR=1.7; 0.7 to 4.3). Conclusions: High numbers of children in Harare experience penetrative sexual abuse, and most present too late for PEP. More immediate presentation of sexual abuse can help to prevent HIV and recurrent abuse, and assist in examination and prosecution. Orphanhood emerged as a possible risk factor for sexual abuse and an important area for further research.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1412e256a1310">
            <label>1</label>
            <mixed-citation id="d1412e263" publication-type="other">
Pereda N, Guilera G, Foms M, et al. The prevalence of child sexual abuse in
community and student samples: a meta-analysis. Clin Psychol Rev
2009;29:328-38.</mixed-citation>
         </ref>
         <ref id="d1412e276a1310">
            <label>2</label>
            <mixed-citation id="d1412e283" publication-type="other">
Unicef. Analysis of the situation of sexual exploitation of children in the Eastern and
Southern Africa region. 2001.</mixed-citation>
         </ref>
         <ref id="d1412e293a1310">
            <label>3</label>
            <mixed-citation id="d1412e300" publication-type="other">
WHO. Preventing child maltreatment: a guide to taking action and generating
evidence. In: WHO-ISPCAN, ed. Geneva, 2006.</mixed-citation>
         </ref>
         <ref id="d1412e310a1310">
            <label>4</label>
            <mixed-citation id="d1412e317" publication-type="other">
Reza A, Breiding MJ, Gulaid J, etat. Sexual violence and its health consequences for
female children in Swaziland: a cluster survey study. Lancet 2009;373:1966-72.</mixed-citation>
         </ref>
         <ref id="d1412e328a1310">
            <label>5</label>
            <mixed-citation id="d1412e335" publication-type="other">
Madu SN, Peltzer K. Prevalence and patterns of child sexual abuse and victim-
perpetrator relationship among secondary school students in the Northern Province
(South Africa). Archives of Sexual Behaviour 2001,30:311 —21.</mixed-citation>
         </ref>
         <ref id="d1412e348a1310">
            <label>6</label>
            <mixed-citation id="d1412e355" publication-type="other">
McCrann D, Lalor K, Katabaro JK. Childhood sexual abuse among university
students in Tanzania. Child Abuse Neglect 2006,30:1343—51.</mixed-citation>
         </ref>
         <ref id="d1412e365a1310">
            <label>7</label>
            <mixed-citation id="d1412e372" publication-type="other">
Haffejee IE. Sexual abuse of Indian (Asian) children in South Africa: First report in
a community undergoing cultural change. Child Abuse Neglect 1991;15:147-51.</mixed-citation>
         </ref>
         <ref id="d1412e382a1310">
            <label>8</label>
            <mixed-citation id="d1412e389" publication-type="other">
Collings SJ. Child sexual abuse in a sample of South African women students:
Prevalence, characteristics, and long-term effects. South Afr J Psychol
1997;21:153-8.</mixed-citation>
         </ref>
         <ref id="d1412e402a1310">
            <label>9</label>
            <mixed-citation id="d1412e409" publication-type="other">
Brown DW, Riley L, Butchart A, et at. Exposure to physical and sexual violence and
adverse health behaviours in African children: results from the Global School-based
Student Health Survey. Bull World Health Organ 2009:447-55.</mixed-citation>
         </ref>
         <ref id="d1412e422a1310">
            <label>10</label>
            <mixed-citation id="d1412e429" publication-type="other">
Maharaj P, Munthree C. Coerced first sexual intercourse and selected reproductive
health outcomes among young women in KwaZulu-Natal, South Africa. J Biosoc Sei
2007;39:231-44.</mixed-citation>
         </ref>
         <ref id="d1412e443a1310">
            <label>11</label>
            <mixed-citation id="d1412e450" publication-type="other">
Birdthistle IJ, Floyd S, Machingura A, et al. From affected to infected? orphanhood
and HIV risk among female adolescents in urban Zimbabwe. AIDS 2008;22:759-66.</mixed-citation>
         </ref>
         <ref id="d1412e460a1310">
            <label>12</label>
            <mixed-citation id="d1412e467" publication-type="other">
Andersson N, Cockcroft A, Shea B. Gender-based violence and HIV: relevance for
HIV prevention in hyperendemic countries of southern Africa. AIDS 2008;22(Suppl 4):
S73-86.</mixed-citation>
         </ref>
         <ref id="d1412e480a1310">
            <label>13</label>
            <mixed-citation id="d1412e487" publication-type="other">
Dunkle KL, Jewkes RK, Nduna M, etal. Perpetration of partner violence and HIV risk behaviour
among young men in the rural Eastern Cape, South Africa. AIDS 2006;20:2107-14.</mixed-citation>
         </ref>
         <ref id="d1412e497a1310">
            <label>14</label>
            <mixed-citation id="d1412e504" publication-type="other">
Pitche P. Child sexual abuse and sexually transmitted infections in sub-Saharan
Africa. Med Trop (Mars) 2005;65:570-4.</mixed-citation>
         </ref>
         <ref id="d1412e514a1310">
            <label>15</label>
            <mixed-citation id="d1412e521" publication-type="other">
Larsen JV, Chapman JA, Armstrong A. Child sexual abuse in KwaZulu-Natal, South
Africa. Trans R Soc Trop Med Hyg 1998;92:262-4.</mixed-citation>
         </ref>
         <ref id="d1412e531a1310">
            <label>16</label>
            <mixed-citation id="d1412e538" publication-type="other">
Cox S, Andrade G, Lungelow D, et at. The child rape epidemic: Assessing the
incidence at Red Cross Hospital, Cape Town, and establishing the need for a new
national protocol. South Aft Med J 2007,97:950—5.</mixed-citation>
         </ref>
         <ref id="d1412e552a1310">
            <label>17</label>
            <mixed-citation id="d1412e559" publication-type="other">
Meursing K, Vos T, Coutinho 0, et at. Child sexual abuse in Matabeleland,
Zimbabwe. Soc Sei Med 1995:41:1693—704.</mixed-citation>
         </ref>
         <ref id="d1412e569a1310">
            <label>18</label>
            <mixed-citation id="d1412e576" publication-type="other">
Hosegood V, Floyd S, Marston M, et al. The effects of high HIV prevalence on
orphanhood and living arrangements of children in Malawi, Tanzania, and South
Africa. Popul Stud (Camb) 2007;61:327-36.</mixed-citation>
         </ref>
         <ref id="d1412e589a1310">
            <label>19</label>
            <mixed-citation id="d1412e596" publication-type="other">
Watts H, Lopman B, Nyamukapa C, et al. Rising incidence and prevalence of
orphanhood in Manicaland, Zimbabwe, 1998 to 2003. AIDS 2005;19:717-25.</mixed-citation>
         </ref>
         <ref id="d1412e606a1310">
            <label>20</label>
            <mixed-citation id="d1412e613" publication-type="other">
Collings SJ. Childhood sexual abuse in a sample of South African university males:
prevalence and risk factors. South Afr J Psychol 1991;21:153-8.</mixed-citation>
         </ref>
         <ref id="d1412e623a1310">
            <label>21</label>
            <mixed-citation id="d1412e630" publication-type="other">
Birdthistle I, Floyd S, Nyagadza A, et al. Is education the link between orphanhood
and HIV/HSV-2 risk among female adolescents in urban Zimbabwe? Soc Sei Med
2009,68:1810—18.</mixed-citation>
         </ref>
         <ref id="d1412e643a1310">
            <label>22</label>
            <mixed-citation id="d1412e650" publication-type="other">
Gregson S, Nyamukapa CA, Garnett GP, et al. HIV infection and reproductive health
in teenage women orphaned and made vulnerable by AIDS in Zimbabwe. AIDS Care
2005;17:785-94.</mixed-citation>
         </ref>
         <ref id="d1412e664a1310">
            <label>23</label>
            <mixed-citation id="d1412e671" publication-type="other">
Thurman TR, Brown L, Richter L, et al. Sexual risk behavior among South African
adolescents: is orphan status a factor? /A/DS Behav 2006:10:627—35.</mixed-citation>
         </ref>
         <ref id="d1412e681a1310">
            <label>24</label>
            <mixed-citation id="d1412e688" publication-type="other">
Zimbabwe Central Statistics Bureau Ml. Demographic 8 Health Survey 2005/
2006.</mixed-citation>
         </ref>
         <ref id="d1412e698a1310">
            <label>25</label>
            <mixed-citation id="d1412e705" publication-type="other">
Kogan SM. Disclosing unwanted sexual experiences: results from a national sample
of adolescent women. Child Abuse Neglect 2004;28:147-65.</mixed-citation>
         </ref>
         <ref id="d1412e715a1310">
            <label>26</label>
            <mixed-citation id="d1412e722" publication-type="other">
Johnson CF. Child sexual abuse. Lancet 2004;364:462-70.</mixed-citation>
         </ref>
         <ref id="d1412e729a1310">
            <label>27</label>
            <mixed-citation id="d1412e736" publication-type="other">
Woods CR. Sexually transmitted diseases in prepubertal children: mechanisms of
transmission, evaluation of sexually abused children, and exclusion of chronic
perinatal viral infections. Semin Pediatr Infect Dis 2005:16:317—25.</mixed-citation>
         </ref>
         <ref id="d1412e749a1310">
            <label>28</label>
            <mixed-citation id="d1412e756" publication-type="other">
UNGASS. United Nations General Assembly (UNGASS) Report on HIV and AIDS:
Zimbabwe Country Report (January 2006—December 2007). 2007.</mixed-citation>
         </ref>
         <ref id="d1412e767a1310">
            <label>29</label>
            <mixed-citation id="d1412e774" publication-type="other">
Robertson L, Gregson S, Madanhire C, etal. Discrepancies between UN models and
DHS survey estimates of maternal orphan prevalence: insights from analyses of
survey data from Zimbabwe. Sex Transm Infect 2008;84(Suppl 1 ):i57—62.</mixed-citation>
         </ref>
         <ref id="d1412e787a1310">
            <label>30</label>
            <mixed-citation id="d1412e794" publication-type="other">
Jewkes R, Morrell R. Gender &amp; sexuality: emerging perspectives from the
heterosexual epidemic in South Africa &amp; implications for HIV risk and prevention.
J Irt AIDS Soc 2010:13:6.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
