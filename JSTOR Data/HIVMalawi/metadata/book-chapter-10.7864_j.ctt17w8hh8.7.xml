<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt17w8hh8</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>Education</subject>
      </subj-group>
      <book-title-group>
         <book-title>What Works in Girls' Education</book-title>
         <subtitle>Evidence for the World's Best Investment</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>SPERLING</surname>
               <given-names>GENE B.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>WINTHROP</surname>
               <given-names>REBECCA</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib3">
            <role>WITH</role>
            <name name-style="western">
               <surname>KWAUK</surname>
               <given-names>CHRISTINA</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib4">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>YOUSAFZAI</surname>
               <given-names>MALALA</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>17</day>
         <month>10</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9780815728627</isbn>
      <isbn content-type="epub">081572862X</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, DC</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt17w8hh8"/>
      <abstract abstract-type="short">
         <p>Hard-headed evidence on why the returns from investing in girls are so high that no nation or family can afford not to educate their girls.</p>
         <p>Gene Sperling, author of the seminal 2004 report published by the Council on Foreign Relations, and Rebecca Winthrop, director of the Center for Universal Education, have written this definitive book on the importance of girls' education. As Malala Yousafzai expresses in her foreword, the idea that any child could be denied an education due to poverty, custom, the law, or terrorist threats is just wrong and unimaginable. More than 1,000 studies have provided evidence that high-quality girls' education around the world leads to wide-ranging returns:</p>
         <p>Better outcomes in economic areas of growth and incomes</p>
         <p>Reduced rates of infant and maternal mortality</p>
         <p>Reduced rates of child marriage</p>
         <p>Reduced rates of the incidence of HIV/AIDS and malaria</p>
         <p>Increased agricultural productivity</p>
         <p>Increased resilience to natural disasters</p>
         <p>Women's empowerment</p>
         <p>
            <italic>What Works in Girls' Education</italic>is a compelling work for both concerned global citizens, and any academic, expert, nongovernmental organization (NGO) staff member, policymaker, or journalist seeking to dive into the evidence and policies on girls' education.</p>
      </abstract>
      <counts>
         <page-count count="335"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.3</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.4</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Yousafzai</surname>
                           <given-names>Malala</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xvii</fpage>
                  <abstract>
                     <p>My story is not of one girl but of many. It is the story of Malala from Pakistan. It is the story of the sisters I have met from Syria and Nigeria who have been denied an education or been targeted for going to school. It is the story of millions of sisters who I do not know by name, but who continue to struggle for what should rightfully be theirs—a safe, free, quality education that allows them to fulfil their dreams and transform the places in which they live.</p>
                     <p>Every girl, no matter where she lives, no matter</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>As Malala implies in her moving foreword to this book, there should not be a need for a book like this on the evidence for the benefits of girls’ education. For many, the idea that any child could be denied an education due to poverty, custom, the law, or terrorist threats is just wrong and unimaginable. Period. End of story. Indeed, those of us who have worked to make the case for girls’ education with evidence, statistics, and case studies know well that the millions of people around the world who recoiled at hearing of girls being kidnapped in Nigeria,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>The World’s Best Investment:</title>
                     <subtitle>Girls’ Education</subtitle>
                  </title-group>
                  <fpage>15</fpage>
                  <abstract>
                     <p>Educating girls may be the investment for developing countries with the highest return when one considers the exceptionally wide range of crucial areas where educating girls and women brings positive results. Although, as in developed nations, girls’ education brings positive returns for income and economic growth, what has brought girls’ education to the top of developing nations’ policy agendas has been the evidence of high returns in other crucial areas—including improving children’s and women’s survival rates and health, reducing population growth, protecting children’s rights and delaying child marriage, empowering women in the home and in the workplace, and improving</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Glass Half Full:</title>
                     <subtitle>There Has Been Real Progress, But A Girls’ Education Crisis Remains</subtitle>
                  </title-group>
                  <fpage>63</fpage>
                  <abstract>
                     <p>The state of girls’ education today can be summarized as much progress made, but with a burning crisis still remaining. Here, we review how far we have come over the past several decades and the nature of the girls’ education challenge that remains. In closing, we offer insights on the momentum that is building for girls’ education, ranging from grassroots advocacy and civil society engagement to an emerging network within the international community.</p>
                     <p>The story of girls’ education in 2015—the aspirational year of the Education for All initiative and the Millennium Development Goals (MDGs)—is a story of both</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>What Works:</title>
                     <subtitle>A Catalogue of Evidence on Addressing Girls’ Education Needs</subtitle>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>It is now well established that girls’ education is a great investment and that girls want to go to school. Yet, by virtue of their gender, girls still face serious and unique barriers—economic (e.g., direct, indirect, and opportunity costs), cultural, and safety barriers. Many carefully monitored and evaluated interventions have shown, however, that smart policy and thoughtful program design can overcome these barriers.</p>
                     <p>To date, a wide range of policies, approaches, and interventions have been tried, tested, and evaluated to help girls attend and stay in school. The main focus of many of these efforts has been at the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Five Compelling Challenges for the Next Decade</title>
                  </title-group>
                  <fpage>189</fpage>
                  <abstract>
                     <p>Since the Millennium Development Goals were put forth—and even since the first edition of this book was published in 2004—there has been significant and impressive progress on girls’ education. The number of girls not attending primary school has been virtually cut in half, and adolescent girls and women are completing more years of school than ever before. And yet, as we said earlier, the state of girls’ education still should invoke more a sense of a lingering crisis that still needs to be conquered rather than a goal that has been met and deserves congrautlations.</p>
                     <p>As we also</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.10</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>269</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.11</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>301</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17w8hh8.12</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>317</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
