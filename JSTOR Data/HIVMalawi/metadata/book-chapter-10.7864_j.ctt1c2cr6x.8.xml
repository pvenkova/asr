<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1c2cr6x</book-id>
      <subj-group>
         <subject content-type="call-number">JC423</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">New democracies</subject>
         <subj-group>
            <subject content-type="lcsh">Case studies</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">Case studies</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democ ratization</subject>
         <subj-group>
            <subject content-type="lcsh">Case studies</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">India</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">Brazil</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">South Africa.</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">Turkey</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">Indonesia</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Five Rising Democracies</book-title>
         <subtitle>And the Fate of the International Liberal Order</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>PICCONE</surname>
               <given-names>TED</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>23</day>
         <month>02</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9780815725794</isbn>
      <isbn content-type="ppub">0815725795</isbn>
      <isbn content-type="epub">9780815726951</isbn>
      <isbn content-type="epub">0815726953</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1c2cr6x"/>
      <abstract abstract-type="short">
         <p>Shifting power balances in the world are shaking the foundations of the liberal international order and revealing new fault lines at the intersection of human rights and international security. Will these new global trends help or hinder the world’s long struggle for human rights and democracy? The answer depends on the role of five rising democracies—India, Brazil, South Africa, Turkey, and Indonesia—as both examples and supporters of liberal ideas and practices. Ted Piccone analyzes the transitions of these five democracies as their stars rise on the international stage. While they offer important and mainly positive examples of the compatibility of political liberties, economic growth, and human development, their foreign policies swing between interest-based strategic autonomy and a principled concern for democratic progress and human rights. In a multipolar world, the fate of the liberal international order depends on how they reconcile these tendencies.</p>
      </abstract>
      <counts>
         <page-count count="250"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.3</book-part-id>
                  <title-group>
                     <title>Preface and Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.4</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>The Road to the Rise:</title>
                     <subtitle>How Democracy and Development Powered the Five</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>TURN THE CLOCK BACK to 1984. The world was gripped by the nasty Cold War between the United States and the Soviet Union, and their allies and proxies. Wars in Central America raged. Dictators reigned in large swathes of the developing world. Nelson Mandela sat in jail for the twentieth year. Thousands were killed in India in the wake of the assassination of Prime Minister Indira Gandhi and the toxic gas leaks in Bhopal. General Suharto ruled Indonesia, the world’s largest Muslim country, with an iron fist. Brazil’s military junta was entering its twentieth year in power. And the threat</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.5</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>The International Human Rights and Democracy Order:</title>
                     <subtitle>Convergence and Divergence</subtitle>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>AT THE HEART OF the contemporary international liberal order is the idea that the state’s authority should be regulated to protect individual liberties and preserve the social contract to live in peace, prosperity, and security. On this foundation a structure for global governance is built that aims to manage conflicts, promote sustainable development, and protect human dignity. For such an order to work, it must have in place a set of norms, institutions, and instruments to protect and promote human rights and demo cratic systems of governance that, over time, have proven to be the best guarantors of those rights.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.6</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>India:</title>
                     <subtitle>A Reluctant Leader</subtitle>
                  </title-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>TO OUTSIDERS, INDIA’S UNIQUE position as the world’s largest democracy, along with the adoption of Mahatma Gandhi as a power ful global symbol of nonviolence and in de pen dence, has engendered expectations of leadership on democracy and human rights that continue to be disappointed. Inflated hopes for India as a proactive player on the world stage, however, can mislead observers to the real power of its role—as an example of social and economic progress achieved within the framework of liberal democracy.</p>
                     <p>A sprawling subcontinent of 1.2 billion people, 122 languages, hundreds of recognized castes and tribes,¹ and six</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.7</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>Brazil:</title>
                     <subtitle>In Pursuit of Strategic Autonomy</subtitle>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract>
                     <p>BRAZIL HAS LONG SOUGHT a role in world affairs that would mirror its impressive physical attributes: its continental breadth, its status as the world’s fifth largest country by size and population, and its rising place as the world’s sixth largest economy. Yet its own experience of erratic growth, high in equality, and unstable politics has tempered its influence on the world stage and reinforced traditional views of national development as the top priority in both its domestic and international agendas. Since its transition to democracy in 1985, Brazil’s dual strategy of pursuing its national economic interests and a higher profile</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.8</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>South Africa:</title>
                     <subtitle>A Conflicted Mediator</subtitle>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>TO UNDERSTAND THE ORIGINS of democratic South Africa’s foreign policy, one must begin with its polar opposite—the history of its apartheid system. The nature of South Africa’s regime and its role in the world changed so fundamentally in 1994 that the tainted vision of an in de pendent racist state serves mainly as a guide for every thing the new South Africa is not.</p>
                     <p>When South Africa’s white rulers declared the separation of the races as official state doctrine in 1948, the country marked itself as a uniquely illegitimate power whose system of government flew in the face of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.9</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>Turkey:</title>
                     <subtitle>A Questionable Model</subtitle>
                  </title-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>OF THE FIVE RISING democracies, Turkey presents the most complicated case for the proposition that liberalizing states offer a power ful example to others seeking a democratic path to development. Its strong authoritarian legacy from Ottoman and post-Ottoman periods, personified by the presidency of modern Turkey’s founder, Mustafa Kemal Atatürk, runs deep. Today it finds echoes in the behavior of President Tayyip Erdoğan, a strong-willed leader who has engineered Turkey’s impressive rise on the world stage while leading his party toward multiple electoral victories. Despite demonstrable progress toward liberal norms, particularly during his first term as prime minister, Erdoğan’s increasingly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.10</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>Indonesia:</title>
                     <subtitle>A Quiet Player</subtitle>
                  </title-group>
                  <fpage>189</fpage>
                  <abstract>
                     <p>INDONESIA, LIKE INDIA, OFFERS the world a compelling example of a large, diverse, and modernizing society committed to principles of democracy, pluralism, and moderation. After four de cades of revolutionary nationalism under President Sukarno and military-led economic nationalism under President Suharto, Indonesia transformed itself in just ten years into a multiparty democracy with strong economic growth, a remarkably speedy turnaround. As the world’s largest Muslim-majority democracy, its appeal is particularly power ful in an era of profound turbulence within the Islamic community and its relations with the rest of the world. Its influence in constructing a stronger international liberal order,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.11</book-part-id>
                  <title-group>
                     <label>CHAPTER EIGHT</label>
                     <title>Paths to Convergence</title>
                  </title-group>
                  <fpage>219</fpage>
                  <abstract>
                     <p>SINCE THE END OF the Cold War, the global democracy and human rights order has evolved, in fits and starts, toward greater activism on a widening array of issues, from water and sanitation to business and human rights to equality for all, regardless of sexual orientation. The rights of the el derly, people with albinism, people with disabilities, contemporary forms of slavery, climate change and the environment, truth and reconciliation, and rights affected by counterterrorism are some of the many new areas of special concern on the international human rights agenda. The language of rights has also broadened to include</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.12</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>249</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>325</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1c2cr6x.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>351</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
