<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wpghn</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>What Works in Development?</book-title>
         <subtitle>Thinking Big and Thinking Small</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Cohen</surname>
               <given-names>Jessica</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Easterly</surname>
               <given-names>William</given-names>
            </name>
         </contrib>
         <role content-type="editor">EDITORS</role>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>02</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="epub">9780815704195</isbn>
      <isbn content-type="epub">0815704194</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wpghn"/>
      <abstract abstract-type="short">
         <p>
            <italic>What Works in Development?</italic>brings together leading experts to address one of the most basic yet vexing issues in development: what do we really know about what works - and what doesn't -in fighting global poverty?</p>
         <p>The contributors, including many of the world's most respected economic development analysts, focus on the ongoing debate over which paths to development truly maximize results. Should we emphasize a big-picture approach -focusing on the role of institutions, macroeconomic policies, growth strategies, and other country-level factors? Or is a more grassroots approach the way to go, with the focus on particular microeconomic interventions such as conditional cash transfers, bed nets, and other microlevel improvements in service delivery on the ground? The book attempts to find a consensus on which approach is likely to be more effective.</p>
         <p>Contributors include Nana Ashraf (Harvard Business School), Abhijit Banerjee (MIT), Nancy Birdsall (Center for Global Development), Anne Case (Princeton University), Jessica Cohen (Brookings),William Easterly (NYU and Brookings),Alaka Halla (Innovations for Poverty Action), Ricardo Hausman (Harvard University), Simon Johnson (MIT), Peter Klenow (Stanford University), Michael Kremer (Harvard), Ross Levine (Brown University), Sendhil Mullainathan (Harvard), Ben Olken (MIT), Lant Pritchett (Harvard), Martin Ravallion (World Bank), Dani Rodrik (Harvard), Paul Romer (Stanford University), and DavidWeil (Brown).</p>
      </abstract>
      <counts>
         <page-count count="245"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.3</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction:</title>
                     <subtitle>Thinking Big versus Thinking Small</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>COHEN</surname>
                           <given-names>JESSICA</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>EASTERLY</surname>
                           <given-names>WILLIAM</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The starting point for the contributions to this volume, and the conference for which they were prepared, is that there is no consensus on “what works” for growth and development. The ultimate goal of development research—a plausible demonstration of what has worked in the past and what might work in the future—remains elusive. As Martin Ravallion points out in his comment on chapter 2, we are beyond “policy rules” such as the Washington Consensus, and “thinking big” on development and growth is in crisis. The “big” triggers for economic growth have not been shown to work, either because</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.4</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The New Development Economics:</title>
                     <subtitle>We Shall Experiment, but How Shall We Learn?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>RODRIK</surname>
                           <given-names>DANI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>Development economics has long been split between the study of<italic>macro</italic>-development (economic growth, international trade, and fiscal/macro-policies) and<italic>micro</italic>development (microfinance, education, health, and other social programs). Even though the central question animating both branches ostensibly is how to achieve sustainable improvements in living standards in poor countries, their concerns and methods have at times diverged so much that they seem at opposite extremes of the economics discipline.</p>
                     <p>I argue in this chapter that it is now possible to envisage a reunification of the field as these sharp distinctions are eroding in some key respects. Microdevelopment economists have become more interested</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.5</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mullainathan</surname>
                           <given-names>Sendhil</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>48</fpage>
                  <abstract>
                     <p>So, what is new in the new development economics? Dani Rodrik looks at two kinds of newness: randomized evaluations and growth diagnostics. One of his key points (the softer point) is that “we can all get along,” and he finds much similarity between the two approaches. Both are explicitly policy oriented. Both are “experimental.” A caveat is in order here about terms. While some (Rodrik included) feel the growth diagnostics approach is experimental, others (the randomized evaluation proponents) would surely bristle at the use of that word. For Rodrik, “experimental” is about trying out new ideas and looking for policy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.6</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ravallion</surname>
                           <given-names>Martin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>Dani Rodrik deals with two important debates. The first is about a “policy rules” approach to policymaking (famously represented by the so-called Washington Consensus) versus a more pragmatic approach based on experimentation in specific contexts. The second is about experimental methods, particularly randomization.</p>
                     <p>Rodrik takes a pragmatic stance on both debates. He is against “one-size-fits-all” policy rules and favors experimentation, but he is skeptical of the experimental claims by the “randomistas.” In short, he is a nonexperimental experimenter.</p>
                     <p>I do not have any serious disagreement with Rodrik’s arguments regarding either debate. While one might quibble that every economist advising on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Breaking Out of the Pocket:</title>
                     <subtitle>Do Health Interventions Work? Which Ones and in What Sense?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOONE</surname>
                           <given-names>PETER</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>JOHNSON</surname>
                           <given-names>SIMON</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>In the early 1960s, there was a great sense of progress and optimism among the world’s epidemiologists and economists. Both had tied major breakthroughs in their respective fundamental sciences to pressing social problems. And both had created plausible policy “levers” that could be adjusted to improve people’s lives and increase prosperity. Since then, much of this positive view has been further justified, especially in view of the advances in lowering the burden of infectious disease and in achieving sustained and unprecedented growth in a number of countries.</p>
                     <p>Public health has arguably progressed more as intended: new drugs, vaccines, and—most</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.8</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Case</surname>
                           <given-names>Anne</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>84</fpage>
                  <abstract>
                     <p>It would be hard to exaggerate the importance of early-life health. Reducing child mortality improves the health of children at very young ages, which improves both their physical and cognitive development. These benefits spill over to educational attainment, and all subsequently cascade into labor market opportunities, marriage prospects, fertility, and then the health of the next generation. So the health channel is important to improving life chances and reducing the intergenerational transmission of poverty. It is also a channel in which real progress is possible.</p>
                     <p>For Peter Boone and Simon Johnson, the critical question is what is going to be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.9</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Cohen</surname>
                           <given-names>Jessica</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>Peter Boone and Simon Johnson make two important points about improving the value and relevance of randomized trials in child health. Although they focus specifically on child survival, these issues apply to the design of randomized evaluations in health more generally. First, they see a need, especially in the social sciences, for more randomized evaluations that have mortality as an outcome. Second, they feel randomized evaluations should focus on packages of interventions rather than on single interventions such as deworming and bed net distribution.</p>
                     <p>The first question is, should randomized evaluations have broader outcome measures? Most economists would agree that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Pricing and Access:</title>
                     <subtitle>Lessons from Randomized Evaluations in Education and Health</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KREMER</surname>
                           <given-names>MICHAEL</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>HOLLA</surname>
                           <given-names>ALAKA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>91</fpage>
                  <abstract>
                     <p>Over the past ten to fifteen years, randomized evaluations have gone from being a rarity to a standard part of the toolkit of academic development economics. Research is now at a point where, at least for some issues, it is possible to stand back and look beyond the results of a single evaluation to see whether certain common lessons emerge and what the implications may be for models of human capital investment.</p>
                     <p>In this chapter, we review the evidence from randomized evaluations on one issue that has been the subject of extensive and often contentious policy debate: the impact of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.11</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Weil</surname>
                           <given-names>David N.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>120</fpage>
                  <abstract>
                     <p>Michael Kremer and Alaka Holla’s discussion offers a great deal of food for thought about four questions pertinent to randomized controlled trials. First, what are the effects of specific education and health programs on specified outcomes? This relates directly to their central topic. Second, what is the right model of household decisionmaking? This broader question underlies much of what is being said in this volume. Third, what should people do if they have the money or power and want to help the poor in developing countries? And fourth, why are some countries rich and some countries poor?</p>
                     <p>In focusing narrowly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.12</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Romer</surname>
                           <given-names>Paul</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>126</fpage>
                  <abstract>
                     <p>Economists can learn quite a bit about how to contribute to better policy in developing countries by looking at Milton Friedman’s various contributions to monetary policy. In his monetary history of the United States, Friedman (together with Anna Schwartz) argued persuasively that money really does matter at a time when many economists were skeptical. Later, in his presidential address, he identified the key theoretical flaw in the prevailing understanding of the relationship between unemployment and inflation. These contributions (one empirical, the other theoretical) became part of the corpus of scientific knowledge that central bankers learn and rely on when they</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.13</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Policy Irrelevance of the Economics of Education:</title>
                     <subtitle>Is “Normative as Positive” Just Useless, or Worse?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PRITCHETT</surname>
                           <given-names>LANT</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>130</fpage>
                  <abstract>
                     <p>Economists have an excellent positive model of the individual/household demand for schooling—in part because it is a straightforward extension of their positive model of everything else. Utility-maximizing people will invest now by sacrificing time, effort, and money on education/schooling/training to gain benefits in the future. Schooling choices depend on the usual factors of preferences, endowments, technologies, relative prices, and budget constraints. Likewise, economists have an excellent normative theory of schooling policy that is a straightforward extension of their normative model for other policy areas. Normative (welfare or public) economics is devoted to the question “What public sector actions would</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.14</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Olken</surname>
                           <given-names>Benjamin A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>165</fpage>
                  <abstract>
                     <p>In his thought-provoking analysis of what evidence is and how evidence plays a role in policy formulation, Lant Pritchett makes three basic claims:</p>
                     <p>1. That whenever economists do research, they implicitly justify the policy relevance of this research with what Pritchett calls the Normative as Positive (NAP) model of government behavior. This model essentially says that government maximizes social welfare.</p>
                     <p>2. That this is a silly model or, at the very least, an incomplete model of government behavior. Models that approach government actors and their objective functions much more systematically—that is, political economy models—do a much better job</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.15</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Birdsall</surname>
                           <given-names>Nancy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>170</fpage>
                  <abstract>
                     <p>A research program we are developing at the Center for Global Development is in the thinking big category. It could constitute one response to Lant Pritchett’s plea for “a general positive model of schooling policy.” Our program focuses on the problem of aid effectiveness and aims to advance knowledge that will allow recommendations for improving education to be grounded in a theory of policy formation that takes into account why, as Pritchett observes, governments provide (and do not just finance) schooling everywhere in the world.</p>
                     <p>Pritchett wants a theory that explains the government’s interest in shaping the nature of education</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.16</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>The Other Invisible Hand:</title>
                     <subtitle>High Bandwidth Development Policy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HAUSMANN</surname>
                           <given-names>RICARDO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>174</fpage>
                  <abstract>
                     <p>Ever since Adam Smith, economists have been in search of a simple solution to the question of the causes of the wealth of nations and to the challenge of development, but the answer has so far proved elusive. The idea that governments of poor countries need do little to catch up has been a constant refrain in policy circles.¹ According to the Washington Consensus, for example, economic success can be achieved through ten relatively straightforward policies.²</p>
                     <p>Yet most governments in the world fill hundreds of thousands of pages trying to legislate policy and call on hundreds of public bureaucracies to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.17</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ashraf</surname>
                           <given-names>Nava</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>Drawing broadly, from Adam Smith to Arthur Pigo and the history of physics, Ricardo Hausmann argues that economists tend to oversimplify the world. The simple policy prescriptions of Smith, the International Monetary Fund (IMF), or the World Bank hardly match the complexities of economic transaction on the microlevel, says Hausmann. Understanding the development process requires a better understanding of the social structures between the economically active individual and the macromeasures of a given country. Each sector has different, sometimes competing, requirements, and politicians will hardly be able to meet these requirements with incomplete information and misaligned incentives. Thus a system</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.18</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Levine</surname>
                           <given-names>Ross</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>203</fpage>
                  <abstract>
                     <p>The discussion by Ricardo Hausmann is characteristically imaginative, provocative, and substantive. Perhaps because of the complexity of the topic, however, I am not sure whether his points coalesce into a clear, novel message. Consequently, I address only two points that he raises.</p>
                     <p>Baseball provides a useful springboard to discuss this question. Think about coaching a child to hit a baseball. The basic advice would be: eat healthy foods and get plenty of rest to build strong muscles and an alert mind; practice, in order to develop good eye-hand coordination; watch the ball when the pitcher throws it, and hit the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.19</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Big Answers for Big Questions:</title>
                     <subtitle>The Presumption of Growth Policy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BANERJEE</surname>
                           <given-names>ABHIJIT VINAYAK</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>207</fpage>
                  <abstract>
                     <p>Don’t we know that all that matters for reducing poverty is growth, especially after China? And therefore we development economists should focus on the things that make growth happen: macropolicy and creating the right institutional environment? And not bother with the microevidence?</p>
                     <p>No, no, and, as the expression goes, no. Every step of that syllogism is wrong, and, I argue in this essay, each step is probably more obviously wrong than the previous one. But before I come to that, let me make an important clarification: none of what I am about to say denies the fundamental usefulness of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.20</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Klenow</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>222</fpage>
                  <abstract>
                     <p>As Abhijit Banerjee acknowledges in his provocative and compelling chapter, the most important questions in development are often “macro” ones, such as how to boost growth in the fashion of China and India in recent decades. But, he emphasizes, economists do not know very much about how to do this despite lots of research effort. So he suggests abandoning macrodevelopment research for the most part and concentrating on microdevelopment questions such as the effect of randomized interventions in health and education. Banerjee’s position can be summarized as follows.</p>
                     <p>I wholeheartedly agree with the “micro” row in this representation, so need</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.21</book-part-id>
                  <title-group>
                     <title>Comment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Easterly</surname>
                           <given-names>William</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>227</fpage>
                  <abstract>
                     <p>My first reaction to Abhijit Banerjee’s chapter is defensive: “Hey, you are only allowed to make fun of our macrogrowth ethnic group if you yourself are a member of the group!” However, in the end, I agree with Banerjee’s radically skeptical microconclusion about aggregate growth research: “Perhaps making growth happen is ultimately beyond our control. Maybe all that happens is that something goes right for once (privatized agriculture raises incomes in rural China) and then that sparks growth somewhere else in the economy, and so on. Perhaps we will never learn where it will start or what will make it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.22</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>233</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpghn.24</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>247</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
