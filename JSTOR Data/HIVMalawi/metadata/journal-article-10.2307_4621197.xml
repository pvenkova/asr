<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">epidinfe</journal-id>
         <journal-id journal-id-type="jstor">j100820</journal-id>
         <journal-title-group>
            <journal-title>Epidemiology and Infection</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">09502688</issn>
         <issn pub-type="epub">14694409</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4621197</article-id>
         <title-group>
            <article-title>Incidence of Cryptosporidiosis Species in Paediatric Patients in Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>T. D. Morse</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>R. A. B. Nichols</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>A. M.</given-names>
                  <surname>Grimason</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>B. M.</given-names>
                  <surname>Campbell</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>K. C. Tembo</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>H. V.</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>11</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">135</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">8</issue>
         <issue-id>i412402</issue-id>
         <fpage>1307</fpage>
         <lpage>1315</lpage>
         <page-range>1307-1315</page-range>
         <permissions>
            <copyright-statement>Copyright 2007 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4621197"/>
         <abstract>
            <p> We determined the incidence of cryptosporidiosis in children aged &lt;5 years presenting with diarrhoea in an urban and rural hospital-based setting in Malawi. Stools were collected over a 22-month period during both rainy and dry seasons. A range of microscopic methods were used to determine the presence of Cryptosporidium spp. oocysts. Species determination was by polymerase chain reaction-restriction fragment length polymorphism (PCR-RFLP) of oocyst-extracted DNA using 18S rRNA and COWP gene loci. Cryptosporidium spp. oocysts were seen in 5·9 % (50/848) of samples, of which 43 amplified by PCR-RFLP indicated the following species: C. hominis, C. parvum, C. hominis/C. parvum, C. meleagridis and C. andersoni. Seven samples could not be amplified by PCR. Wider species diversity was found in the rural setting, and may be a result of increased malnutrition and zoonotic exposure in this area. Improvements in water, sanitation, household hygiene and animal control are required to reduce the incidence of infection in this population. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1008e205a1310">
            <label>1</label>
            <mixed-citation id="d1008e212" publication-type="other">
WHO. World Health Report - Make Every Child and
Mother Count. Facts and Figures from World Health
Report 2005 (http://www.who.int/whr/en/). Accessed
January 2006.</mixed-citation>
         </ref>
         <ref id="d1008e228a1310">
            <label>2</label>
            <mixed-citation id="d1008e235" publication-type="other">
National Statistical Office (Malawi). Malawi De-
mographic and Health Survey 2000, 2001. Zomba,
Malawi.</mixed-citation>
         </ref>
         <ref id="d1008e248a1310">
            <label>3</label>
            <mixed-citation id="d1008e255" publication-type="journal">
Mwachari C, et al. Chronic diarrhoea among HIV
infected adult patients in Nairobi, Kenya. Journal of
Infection 1998; 37: 48-53.<person-group>
                  <string-name>
                     <surname>Mwachari</surname>
                  </string-name>
               </person-group>
               <fpage>48</fpage>
               <volume>37</volume>
               <source>Journal of Infection</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e290a1310">
            <label>4</label>
            <mixed-citation id="d1008e297" publication-type="journal">
Chunge RN, et al. Mixed infections in childhood
diarrhoea: results of a community study in Kiambu
District, Kenya. East African Medical Journal 1989;
66: 715-723.<person-group>
                  <string-name>
                     <surname>Chunge</surname>
                  </string-name>
               </person-group>
               <fpage>715</fpage>
               <volume>66</volume>
               <source>East African Medical Journal</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e336a1310">
            <label>5</label>
            <mixed-citation id="d1008e343" publication-type="journal">
Amadi B, et al. Intestinal and systemic infection, HIV,
and mortality in Zambian children with persistent
diarrhoea and malnutrition. Journal of Pediatric
Gastroenterology and Nutrition 2001; 32: 550-554.<person-group>
                  <string-name>
                     <surname>Amadi</surname>
                  </string-name>
               </person-group>
               <fpage>550</fpage>
               <volume>32</volume>
               <source>Journal of Pediatric Gastroenterology and Nutrition</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e381a1310">
            <label>6</label>
            <mixed-citation id="d1008e388" publication-type="journal">
Pavone R, et al. Viral gastro-enteritis in children in
Malawi. Annals of Tropical Paediatrics 1990; 10: 15-20.<person-group>
                  <string-name>
                     <surname>Pavone</surname>
                  </string-name>
               </person-group>
               <fpage>15</fpage>
               <volume>10</volume>
               <source>Annals of Tropical Paediatrics</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e420a1310">
            <label>7</label>
            <mixed-citation id="d1008e427" publication-type="journal">
Xiao L, et al. Cryptosporidium Taxonomy: recent
advances and implications for public health. Clinical
Microbiology Reviews 2004; 17: 72-97.<person-group>
                  <string-name>
                     <surname>Xiao</surname>
                  </string-name>
               </person-group>
               <fpage>72</fpage>
               <volume>17</volume>
               <source>Clinical Microbiology Reviews</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e462a1310">
            <label>8</label>
            <mixed-citation id="d1008e469" publication-type="journal">
Ryan UM, et al. Cryptosporidium suis n.sp. (Api-
complexa: Cryptosporidiidae) in pigs (Sus scrofa).
Journal of Parasitology 2004; 90: 769-773.<object-id pub-id-type="jstor">10.2307/3286322</object-id>
               <fpage>769</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1008e488a1310">
            <label>9</label>
            <mixed-citation id="d1008e495" publication-type="journal">
Morgan U, et al. Molecular characterisation of
Cryptosporidium isolates obtained from Human
Immunodeficiency Virus infected individuals living in
Switzerland, Kenya and the United States. Journal of
Clinical Microbiology 2000; 38: 1180-1183.<person-group>
                  <string-name>
                     <surname>Morgan</surname>
                  </string-name>
               </person-group>
               <fpage>1180</fpage>
               <volume>38</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e536a1310">
            <label>10</label>
            <mixed-citation id="d1008e543" publication-type="journal">
Leav BA, et al. Analysis of sequence diversity at the
highly polymorphic Cpgp40/15 locus among Crypto-
sporidium isolates from human immunodeficiency
virus-infected children in South Africa. Infection and
Immunity 2002; 70: 3881-3890.<person-group>
                  <string-name>
                     <surname>Leav</surname>
                  </string-name>
               </person-group>
               <fpage>3881</fpage>
               <volume>70</volume>
               <source>Infection and Immunity</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e585a1310">
            <label>11</label>
            <mixed-citation id="d1008e592" publication-type="journal">
Gatei W, et al. Molecular analysis of the 18S rRNA
gene of Cryptosporidium parasites from patients with
or without human immunodeficiency virus infections
living in Kenya, Malawi, Brazil, the United Kingdom
and Vietnam. Journal of Clinical Microbiology 2003;
41: 1458-1462.<person-group>
                  <string-name>
                     <surname>Gatei</surname>
                  </string-name>
               </person-group>
               <fpage>1458</fpage>
               <volume>41</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e636a1310">
            <label>12</label>
            <mixed-citation id="d1008e643" publication-type="journal">
Peng MM, et al. Molecular epidemiology of crypto-
sporidiosis in children in Malawi. Journal of Eukaryotic
Microbiology 2003; 50 (Suppl.): 557-559.<person-group>
                  <string-name>
                     <surname>Peng</surname>
                  </string-name>
               </person-group>
               <issue>Suppl.</issue>
               <fpage>557</fpage>
               <volume>50</volume>
               <source>Journal of Eukaryotic Microbiology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e681a1310">
            <label>13</label>
            <mixed-citation id="d1008e688" publication-type="journal">
Tumwine JK, et al. Cryptosporidium parvum in children
with diarrhoea in Mulago Hospital, Kampala, Uganda.
American Journal of Tropical Medicine and Hygiene
2003; 68: 710-715.<person-group>
                  <string-name>
                     <surname>Tumwine</surname>
                  </string-name>
               </person-group>
               <fpage>710</fpage>
               <volume>68</volume>
               <source>American Journal of Tropical Medicine and Hygiene</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e726a1310">
            <label>14</label>
            <mixed-citation id="d1008e733" publication-type="journal">
UNAIDS. Malawi. Epidemiological Fact Sheets on
HIV/AIDS and Sexually Transmitted Diseases, 2004
(www.aidsmalawi.org). Accessed January 2006.<person-group>
                  <string-name>
                     <surname>UNAIDS</surname>
                  </string-name>
               </person-group>
               <source>Epidemiological Fact Sheets on HIV/AIDS and Sexually Transmitted Diseases</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e762a1310">
            <label>15</label>
            <mixed-citation id="d1008e769" publication-type="journal">
Smith HV, Corcoran GD. New drugs and treatment
for cryptosporidiosis. Current Opinion in Infectious
Diseases 2004; 17: 557-564.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <fpage>557</fpage>
               <volume>17</volume>
               <source>Current Opinion in Infectious Diseases</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e804a1310">
            <label>16</label>
            <mixed-citation id="d1008e811" publication-type="journal">
Casemore DP, Armstrong M, Sands RL. Laboratory
diagnosis of cryptosporidiosis. Journal of Clinical
Pathology 1985; 38: 1337-1341.<person-group>
                  <string-name>
                     <surname>Casemore</surname>
                  </string-name>
               </person-group>
               <fpage>1337</fpage>
               <volume>38</volume>
               <source>Journal of Clinical Pathology</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e847a1310">
            <label>17</label>
            <mixed-citation id="d1008e854" publication-type="journal">
Grimason AM, et al. Application of DAPI and
immunofluorescence for enhanced identification of
Cryptosporidium oocysts in water samples. Water
Research 1994; 28: 733-736.<person-group>
                  <string-name>
                     <surname>Grimason</surname>
                  </string-name>
               </person-group>
               <fpage>733</fpage>
               <volume>28</volume>
               <source>Water Research</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e892a1310">
            <label>18</label>
            <mixed-citation id="d1008e899" publication-type="journal">
McLaughlin J, et al. Genetic characterisation of
Cryptosporidium strains from 218 patients with diar-
rhoea diagnosed as having sporadic cryptosporidiosis.
Journal of Clinical Microbiology 1999; 37: 3153-3158.<person-group>
                  <string-name>
                     <surname>McLaughlin</surname>
                  </string-name>
               </person-group>
               <fpage>3153</fpage>
               <volume>37</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e937a1310">
            <label>19</label>
            <mixed-citation id="d1008e944" publication-type="journal">
Bukhari Z, Smith HV. Effect of three concentration
techniques on viability of Cryptosporidium parvum
oocysts recovered from bovine faeces. Journal of
Clinical Microbiology 1995; 33: 2592-2595.<person-group>
                  <string-name>
                     <surname>Bukhari</surname>
                  </string-name>
               </person-group>
               <fpage>2592</fpage>
               <volume>33</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e982a1310">
            <label>20</label>
            <mixed-citation id="d1008e989" publication-type="journal">
Nichols RAB, Moore JE, Smith HV. A rapid method for
extracting oocyst DNA from Cryptosporidium-positive
human faeces for outbreak investigations. Journal of
Microbiological Methods 2006; 65: 512-524.<person-group>
                  <string-name>
                     <surname>Nichols</surname>
                  </string-name>
               </person-group>
               <fpage>512</fpage>
               <volume>65</volume>
               <source>Journal of Microbiological Methods</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1027a1310">
            <label>21</label>
            <mixed-citation id="d1008e1034" publication-type="journal">
Nichols RAB, Campbell BM, Smith HV. Molecular
fingerprinting of Cryptosporidium oocysts isolated dur-
ing regulatory monitoring. Applied and Environmental
Microbiology 2006; 72: 5428-5435.<person-group>
                  <string-name>
                     <surname>Nichols</surname>
                  </string-name>
               </person-group>
               <fpage>5428</fpage>
               <volume>72</volume>
               <source>Applied and Environmental Microbiology</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1072a1310">
            <label>22</label>
            <mixed-citation id="d1008e1079" publication-type="journal">
Nichols RAB, Smith HV. Optimization of DNA
extraction and molecular detection of Cryptosporidium
oocysts in natural mineral water sources. Journal of
Food Protection 2004; 67: 524-532.<person-group>
                  <string-name>
                     <surname>Nichols</surname>
                  </string-name>
               </person-group>
               <fpage>524</fpage>
               <volume>67</volume>
               <source>Journal of Food Protection</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1118a1310">
            <label>23</label>
            <mixed-citation id="d1008e1125" publication-type="journal">
Johnson DW, et al. Development of a PCR protocol
for sensitive detection of Cryptosporidium oocysts in
water samples. Applied and Environmental Microbiology
1995; 61: 3849-3855.<person-group>
                  <string-name>
                     <surname>Johnson</surname>
                  </string-name>
               </person-group>
               <fpage>3849</fpage>
               <volume>61</volume>
               <source>Applied and Environmental Microbiology</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1163a1310">
            <label>24</label>
            <mixed-citation id="d1008e1170" publication-type="journal">
Nichols RAB, et al. A sensitive, semi qualitative direct
PCR-RFLP assay for simultaneous detection of five
Cryptosporidium species in treated drinking waters and
mineral waters. Water Supply 2002; 2: 9-15.<person-group>
                  <string-name>
                     <surname>Nichols</surname>
                  </string-name>
               </person-group>
               <fpage>9</fpage>
               <volume>2</volume>
               <source>Water Supply</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1208a1310">
            <label>25</label>
            <mixed-citation id="d1008e1215" publication-type="journal">
Homan W, et al. Characterisation of Cryptosporidium
parvum in human and animal faeces by single tube
nested polymerase chain reaction and restriction
analysis. Parasitology Research 1999; 85: 707-712.<person-group>
                  <string-name>
                     <surname>Homan</surname>
                  </string-name>
               </person-group>
               <fpage>707</fpage>
               <volume>85</volume>
               <source>Parasitology Research</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1253a1310">
            <label>26</label>
            <mixed-citation id="d1008e1260" publication-type="journal">
Xiao L, et al. Phylogenetic analysis of Cryptosporidium
parasites based on the small-subunit rRNA gene locus.
Applied and Environmental Microbiology 1999; 65:
1578-1583.<person-group>
                  <string-name>
                     <surname>Xiao</surname>
                  </string-name>
               </person-group>
               <fpage>1578</fpage>
               <volume>65</volume>
               <source>Applied and Environmental Microbiology</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1298a1310">
            <label>27</label>
            <mixed-citation id="d1008e1305" publication-type="journal">
Baxby D, Blundell N. Sensitive, rapid, simple methods
for detecting Cryptosporidium in faeces. Lancet 1993; 2:
1149.<person-group>
                  <string-name>
                     <surname>Baxby</surname>
                  </string-name>
               </person-group>
               <fpage>1149</fpage>
               <volume>2</volume>
               <source>Lancet</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1340a1310">
            <label>28</label>
            <mixed-citation id="d1008e1347" publication-type="journal">
Varea M, et al. Fuschin fluorescence and autofluor-
escence of Cryptosporidium, Isospora and Cyclospora
oocysts. International Journal of Parasitology 1998; 28:
1881-1883.<person-group>
                  <string-name>
                     <surname>Varea</surname>
                  </string-name>
               </person-group>
               <fpage>1881</fpage>
               <volume>28</volume>
               <source>International Journal of Parasitology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1386a1310">
            <label>29</label>
            <mixed-citation id="d1008e1393" publication-type="journal">
Morgan UM, et al. Comparison of PCR and micro-
scopy for detection of Cryptosporidium parvum in
human fecal specimens: Clinical trial. Journal of
Clinical Microbiology 1998; 36: 995-998.<person-group>
                  <string-name>
                     <surname>Morgan</surname>
                  </string-name>
               </person-group>
               <fpage>995</fpage>
               <volume>36</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1431a1310">
            <label>30</label>
            <mixed-citation id="d1008e1438" publication-type="journal">
Gatei W, et al. Cryptosporidium muris infection in
an HIV-infected adult, Kenya. Emerging Infectious
Diseases 2002; 8: 204-206.<person-group>
                  <string-name>
                     <surname>Gatei</surname>
                  </string-name>
               </person-group>
               <fpage>204</fpage>
               <volume>8</volume>
               <source>Emerging Infectious Diseases</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1473a1310">
            <label>31</label>
            <mixed-citation id="d1008e1480" publication-type="journal">
Guyot K, et al. Molecular characterisation of Crypto-
sporidium isolates obtained from humans in France.
Journal of Clinical Microbiology 2001; 39: 3472-3480.<person-group>
                  <string-name>
                     <surname>Guyot</surname>
                  </string-name>
               </person-group>
               <fpage>3472</fpage>
               <volume>39</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1515a1310">
            <label>32</label>
            <mixed-citation id="d1008e1522" publication-type="journal">
Palmer CJ, et al. Cryptosporidium muris, a rodent
pathogen, recovered from a human in Peru. Emerging
Infectious Diseases 2003; 9: 1174-1176.<person-group>
                  <string-name>
                     <surname>Palmer</surname>
                  </string-name>
               </person-group>
               <fpage>1174</fpage>
               <volume>9</volume>
               <source>Emerging Infectious Diseases</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1557a1310">
            <label>33</label>
            <mixed-citation id="d1008e1564" publication-type="journal">
Amar C, Pedraza-Diaz S, McLauchlin J. Extraction
and genotyping of Cryptosporidium parvum DNA from
fecal smears on glass slides stained conventionally for
direct microscope examination. Journal of Clinical
Microbiology 2001; 39: 401-403.<person-group>
                  <string-name>
                     <surname>Amar</surname>
                  </string-name>
               </person-group>
               <fpage>401</fpage>
               <volume>39</volume>
               <source>Journal of Clinical Microbiology</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1605a1310">
            <label>34</label>
            <mixed-citation id="d1008e1612" publication-type="journal">
Amar CFL, et al. Blinded evaluation of DNA extrac-
tion and genotyping of stained Cryptosporidium on
glass slides. Letters in Applied Microbiology 2002; 35:
486-488.<person-group>
                  <string-name>
                     <surname>Amar</surname>
                  </string-name>
               </person-group>
               <fpage>486</fpage>
               <volume>35</volume>
               <source>Letters in Applied Microbiology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1651a1310">
            <label>35</label>
            <mixed-citation id="d1008e1658" publication-type="journal">
Monteiro L, et al. Detection of Helicobacter pylori
DNA in human faeces by PCR: DNA stability and re-
moval of inhibitors. Journal of Microbiological Methods
2001; 45: 89-94.<person-group>
                  <string-name>
                     <surname>Monteiro</surname>
                  </string-name>
               </person-group>
               <fpage>89</fpage>
               <volume>45</volume>
               <source>Journal of Microbiological Methods</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1696a1310">
            <label>36</label>
            <mixed-citation id="d1008e1703" publication-type="journal">
Pedraza-Diaz S, Amar C, McLauchlin J. The identifi-
cation and characterisation of an unusual genotype
of Cryptosporidium from human faeces as Crypto-
sporidium meleagridis. FEM Microbiology Letters 2000;
189: 189-194.<person-group>
                  <string-name>
                     <surname>Pedraza-Diaz</surname>
                  </string-name>
               </person-group>
               <fpage>189</fpage>
               <volume>189</volume>
               <source>FEM Microbiology Letters</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1744a1310">
            <label>37</label>
            <mixed-citation id="d1008e1753" publication-type="journal">
Pedraza-Diaz S, et al. Cryptosporidium meleagridis
from humans: molecular analysis and description of
affected patients. Journal of Infection 2001: 42: 243-
250.<person-group>
                  <string-name>
                     <surname>Pedraza-Diaz</surname>
                  </string-name>
               </person-group>
               <fpage>243</fpage>
               <volume>42</volume>
               <source>Journal of Infection</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1791a1310">
            <label>38</label>
            <mixed-citation id="d1008e1798" publication-type="journal">
Nchito M, et al. Cryptosporidiosis in urban Zambian
children: and analysis of risk factors. American
Journal of Tropical Medicine and Hygiene 1998; 59:
435-437.<person-group>
                  <string-name>
                     <surname>Nchito</surname>
                  </string-name>
               </person-group>
               <fpage>435</fpage>
               <volume>59</volume>
               <source>American Journal of Tropical Medicine and Hygiene</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1836a1310">
            <label>39</label>
            <mixed-citation id="d1008e1843" publication-type="journal">
Steele AD, Gove E, Meewes PJ. Cryptosporidiosis
in white patients in South Africa. Journal of Infection
1989; 19: 281-285.<person-group>
                  <string-name>
                     <surname>Steele</surname>
                  </string-name>
               </person-group>
               <fpage>281</fpage>
               <volume>19</volume>
               <source>Journal of Infection</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1008e1878a1310">
            <label>40</label>
            <mixed-citation id="d1008e1885" publication-type="journal">
Simwa JM, et al. Cryptosporidiosis and childhood
diarrhoea in a rural community in Kenya. East African
Medical Journal 1989; 8: 520-524.<person-group>
                  <string-name>
                     <surname>Simwa</surname>
                  </string-name>
               </person-group>
               <fpage>520</fpage>
               <volume>8</volume>
               <source>East African Medical Journal</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
