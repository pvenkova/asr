<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jepidcommheal</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000490</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Epidemiology and Community Health (1979-)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>BMJ Publishing Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0143005X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14702738</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43281659</article-id>
         <article-categories>
            <subj-group>
               <subject>Perinatal and Maternal Health</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Time series analysis of maternal mortality in Africa from 1990 to 2005</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Maral</given-names>
                  <surname>DerSarkissian</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Caroline A</given-names>
                  <surname>Thompson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Onyebuchi A</given-names>
                  <surname>Arah</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">67</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40129879</issue-id>
         <fpage>992</fpage>
         <lpage>998</lpage>
         <permissions>
            <copyright-statement>© 2013 BMJ Publishing Group</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43281659"/>
         <abstract>
            <p>Objectives: Most global maternal deaths occur in Africa and Asia. In response, the Millennium Development Goal (MDG-5) calls for a 75% reduction in maternal mortality from 1990 to 2015. To assess the potential for progress in MDG-5 in Africa, we examined the cross-sectional and longitudinal associations of socioeconomic, demographic and population-health factors with maternal mortality rates in Africa. Methods: We used data from global agencies and the published literature to identify socioeconomic, demographic and population-health explanatory factors that could be correlated with maternal mortality in 49 countries of Africa for the years 1990, 1995, 2000 and 2005. We used correlation, negative binomial and mixed Poisson regression models to investigate whether there exist associations between potential explanatory factors and maternal mortality. Results: Some African countries have made substantial progress towards achieving MDG-5 while others have fallen behind. Lower gross domestic product (GDP) and female enrolment in primary schools, but higher HIV prevalence, neonatal mortality rate and total fertility rate, were associated with higher maternal mortality. Conclusions: Maternal mortality rates in African countries appear to be declining. The mean maternal mortality ratios in Africa decreased from 695.82 in 1990 to 562.18 in 2005. Yet some countries are more likely than others to achieve MDG-5. Better socioeconomic, demographic and population health development appear to be conducive to better maternal health in Africa. Sustained efforts on all these fronts will be needed to close the gap in maternal survival and achieve MDG-5 in Africa.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1037e208a1310">
            <label>1</label>
            <mixed-citation id="d1037e215" publication-type="other">
United Nations. United Nations Millennium Declaration. 2000; Resolution.</mixed-citation>
         </ref>
         <ref id="d1037e222a1310">
            <label>2</label>
            <mixed-citation id="d1037e229" publication-type="other">
United Nations. Indicators for monitoring the millennium development goals:
definitions, rationale, concepts, and sources. New York: United Nations, 2003.</mixed-citation>
         </ref>
         <ref id="d1037e239a1310">
            <label>3</label>
            <mixed-citation id="d1037e246" publication-type="other">
World Health Organization. International statistical classification of diseases and
related health problems. 10th revision. Geneva: WHO, 1992.</mixed-citation>
         </ref>
         <ref id="d1037e256a1310">
            <label>4</label>
            <mixed-citation id="d1037e263" publication-type="other">
Ronsmans C, Graham WJ. Maternal mortality: who, when, where, and why. Lancet
2006;368:1189-200.</mixed-citation>
         </ref>
         <ref id="d1037e274a1310">
            <label>5</label>
            <mixed-citation id="d1037e281" publication-type="other">
Buor D, Bream K. An analysis of the determinants of maternal mortality in
sub-Saharan Africa. J Womens Health 2004;13:926-38.</mixed-citation>
         </ref>
         <ref id="d1037e291a1310">
            <label>6</label>
            <mixed-citation id="d1037e298" publication-type="other">
Filippi V, Ronsmans C, Campbell OMR, et al. Maternal health in poor countries: the
broader context and a call for action. Lancet 2006:368:1535-41.</mixed-citation>
         </ref>
         <ref id="d1037e308a1310">
            <label>7</label>
            <mixed-citation id="d1037e315" publication-type="other">
Bicego G, Boerma JT, Ronsmans C. The effect of AIDS on maternal mortality in
Malawi and Zimbabwe. AIDS 2002;16. http://journals.lww.com/aidsonline/Fulltext/
2002/05030/The_effect_of_AIDS_on_maternaLmortality_in_Malawi.19.aspx</mixed-citation>
         </ref>
         <ref id="d1037e328a1310">
            <label>8</label>
            <mixed-citation id="d1037e335" publication-type="other">
Hogan MC, Foreman KJ, Naghavi M, et al. Maternal mortality for 181 countries,
1980-2008: a systematic analysis of progress towards Millennium Development
Goal 5. Lancet 2010;375:1609-23.</mixed-citation>
         </ref>
         <ref id="d1037e348a1310">
            <label>9</label>
            <mixed-citation id="d1037e355" publication-type="other">
World Health Organization. Maternal mortality in 2005: estimates developed by
WHO, UNICEF, UNFPA, and the World Bank. Geneva: WHO Press, 2007.</mixed-citation>
         </ref>
         <ref id="d1037e365a1310">
            <label>10</label>
            <mixed-citation id="d1037e372" publication-type="other">
Hill K, Thomas K, AbouZahr C, et al. Estimates of maternal mortality worldwide
between 1990 and 2005: an assessment of available data. Lancet
2007;370:1311-19.</mixed-citation>
         </ref>
         <ref id="d1037e386a1310">
            <label>11</label>
            <mixed-citation id="d1037e393" publication-type="other">
McCarthy J, Maine D. A framework for analyzing the determinants of maternal
mortality. Stud Farn Plann 1992;23:23-33.</mixed-citation>
         </ref>
         <ref id="d1037e403a1310">
            <label>12</label>
            <mixed-citation id="d1037e410" publication-type="other">
UNICEF (United Nations Children's Fund). Programming for safe motherhood:
guidelines for maternal and neonatal survival. New York: UNICEF, 1999.</mixed-citation>
         </ref>
         <ref id="d1037e420a1310">
            <label>13</label>
            <mixed-citation id="d1037e427" publication-type="other">
Mosley WH, Chen LC. An analytical framework for the study of child survival in
developing countries. 1984. Bull World Health Organ 2003;81:140-5.</mixed-citation>
         </ref>
         <ref id="d1037e437a1310">
            <label>14</label>
            <mixed-citation id="d1037e444" publication-type="other">
Rogo KO, Oucho J, Mwalali P. Maternal Mortality. In: Jamison DT, Feachem RG,
Makgoba MW, et al. eds. Disease and Mortality in Sub-Saharan Africa. 2nd edn.
Washington (DC): World Bank; 2006. Chapter 16.</mixed-citation>
         </ref>
         <ref id="d1037e457a1310">
            <label>15</label>
            <mixed-citation id="d1037e464" publication-type="other">
World Bank. World development report 1993: investing in health. Oxford University
Press, 1993.</mixed-citation>
         </ref>
         <ref id="d1037e474a1310">
            <label>16</label>
            <mixed-citation id="d1037e481" publication-type="other">
Jamison DT, Breman JG, Measham AR, et al. Disease control priorities in developing
countries. World Bank Publications, 2006.</mixed-citation>
         </ref>
         <ref id="d1037e492a1310">
            <label>17</label>
            <mixed-citation id="d1037e499" publication-type="other">
Hill K, El AS, Koenig M, et al. How should we measure maternal mortality in the
developing world? A comparison of household deaths and sibling history
approaches. Bull World Health Organ 2006;84:173-80.</mixed-citation>
         </ref>
         <ref id="d1037e512a1310">
            <label>18</label>
            <mixed-citation id="d1037e519" publication-type="other">
Alvarez JL, Gil R, Hernández V, et al. Factors associated with maternal mortality in
sub-Saharan Africa: an ecological study. BMC Public Health 2009;9:462.</mixed-citation>
         </ref>
         <ref id="d1037e529a1310">
            <label>19</label>
            <mixed-citation id="d1037e536" publication-type="other">
Arah OA. On the relationship between individual and population health. Med
Health Care Philos 2009;12:235-44.</mixed-citation>
         </ref>
         <ref id="d1037e546a1310">
            <label>20</label>
            <mixed-citation id="d1037e553" publication-type="other">
World Health Organization. WHO Statistical Information System (WHOSIS). 2010.
http://www.who.int/whosis/en/</mixed-citation>
         </ref>
         <ref id="d1037e563a1310">
            <label>21</label>
            <mixed-citation id="d1037e570" publication-type="other">
United Nations Department of Economic and Social Affairs Population Division.
World population prospects: the 2008 revision. 2009.</mixed-citation>
         </ref>
         <ref id="d1037e580a1310">
            <label>22</label>
            <mixed-citation id="d1037e587" publication-type="other">
The World Bank Group. World Development Indicators 2009. 2010. http://data.
worldbank.org/</mixed-citation>
         </ref>
         <ref id="d1037e598a1310">
            <label>23</label>
            <mixed-citation id="d1037e605" publication-type="other">
Institute for Health Metrics and Evaluation. Global Health Data Exchange (GHDx).
2010. http://www.healthmetricsandevaluation.org/data</mixed-citation>
         </ref>
         <ref id="d1037e615a1310">
            <label>24</label>
            <mixed-citation id="d1037e622" publication-type="other">
United Nations Statistic Division. National Accounts Main Aggregates Data. 2009.
http://unstats.un.org/unsd/snaama/introduction.asp</mixed-citation>
         </ref>
         <ref id="d1037e632a1310">
            <label>25</label>
            <mixed-citation id="d1037e639" publication-type="other">
Van Lerberghe W, De Brouwere V. Of blind alleys and things that have worked:
history's lessons on reducing maternal mortality. Stud Health Serv Organ Policy
2001;17:7-33.</mixed-citation>
         </ref>
         <ref id="d1037e652a1310">
            <label>26</label>
            <mixed-citation id="d1037e659" publication-type="other">
Trussell J, Pebley AR. The potential impact of changes in fertility on infant, child
and maternal mortality. Stud Fam Plann 1984;15:267-80.</mixed-citation>
         </ref>
         <ref id="d1037e669a1310">
            <label>27</label>
            <mixed-citation id="d1037e676" publication-type="other">
Fortney JA. The importance of family planning in reducing maternal mortality. Stud
Fam Plann 1987;18:109-14.</mixed-citation>
         </ref>
         <ref id="d1037e686a1310">
            <label>28</label>
            <mixed-citation id="d1037e693" publication-type="other">
Victora CG, Aquino EML, Do Carmo Leal M, et al. Maternal and child health in
Brazil: progress and challenges. Lancet 2011;377:1863-76.</mixed-citation>
         </ref>
         <ref id="d1037e704a1310">
            <label>29</label>
            <mixed-citation id="d1037e711" publication-type="other">
LeGrand TK, Phillips JF. The effect of fertility reductions on infant and
child mortality: evidence from Matlab in Rural Bangladesh. Popul Stud
1996;50:51-68.</mixed-citation>
         </ref>
         <ref id="d1037e724a1310">
            <label>30</label>
            <mixed-citation id="d1037e731" publication-type="other">
Okonofua FE. Maternal mortality prevention in Africa—need to focus on access and
quality of care. Afr J Reprod Health 2008;12:9-12.</mixed-citation>
         </ref>
         <ref id="d1037e741a1310">
            <label>31</label>
            <mixed-citation id="d1037e748" publication-type="other">
Metiboba S. Reducing maternal mortality in Nigeria through community
participation: a new paradigm shift. Eur J Soc Sci 2011;26:561-5.</mixed-citation>
         </ref>
         <ref id="d1037e758a1310">
            <label>32</label>
            <mixed-citation id="d1037e765" publication-type="other">
Betran A, Wojdyla D, Posner S, et al. National estimates for maternal mortality: an
analysis based on the WHO systematic review of maternal mortality and morbidity.
BMC Public Health 2005;5:131.</mixed-citation>
         </ref>
         <ref id="d1037e778a1310">
            <label>33</label>
            <mixed-citation id="d1037e785" publication-type="other">
Newby H. Series metadata: maternal mortality ratio per 100,000 live births.
New York: UNICEF, Statistics and Monitoring Section, 2010.</mixed-citation>
         </ref>
         <ref id="d1037e795a1310">
            <label>34</label>
            <mixed-citation id="d1037e802" publication-type="other">
WHO, UNICEF, UNFPA, et al. Trends in maternal mortality: 1990 to 2010.
Geneva: World Health Organization, 2012. http://www.unfpa.org/webdav/
site/global/shared/documents/publications/2012/Trends_in_maternal_mortality_
A4-1.pdf</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
