<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">studfamiplan</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100383</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Studies in Family Planning</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Subscription Services, Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00393665</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284465</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24642165</article-id>
         <title-group>
            <article-title>HIV Status, Gender, and Marriage Dynamics among Adults in Rural Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Philip</given-names>
                  <surname>Anglewicz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Georges</given-names>
                  <surname>Reniers</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">45</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24642162</issue-id>
         <fpage>415</fpage>
         <lpage>428</lpage>
         <permissions>
            <copyright-statement>© 2014 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24642165"/>
         <abstract>
            <p>Awareness of and responses to HIV health risks stemming from relations between sexual partners have been well documented in sub-Saharan Africa, but few studies have estimated the effects of observed HIV status on marriage decisions and outcomes. We study marriage dissolution and remarriage in rural Malawi using longitudinal data with repeated HIV and marital status measurements. Results indicate that HIV-positive individuals face greater risks of union dissolution (via both widowhood and divorce) and lower remarriage rates. Modeling studies suggest that the exclusion of HIV-positive individuals from the marriage or partnership pools will reduce the spread of HIV.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1604e171a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1604e178" publication-type="other">
Between 1998 and 2004 the MLSFH was known as the Malawi Diffusion and Ideational Change Project.</mixed-citation>
            </p>
         </fn>
         <fn id="d1604e185a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1604e192" publication-type="other">
A detailed description of the HIV testing procedure is available in Bignami-Van Assche et al. (2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1604e199a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1604e206" publication-type="other">
Household wealth was measured using a wealth index based on ownership of 14 household durable assets, achieved by principal
component analysis (Filmer and Pritchett 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d1604e216a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1604e223" publication-type="other">
Some individuals seroconverted during the survey interval, which could affect regression coefficients. With an incidence rate
of approximately 0.7 new infections per 100 person-years (Obare et al. 2009), however, this bias will be negligible.</mixed-citation>
            </p>
         </fn>
         <fn id="d1604e234a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1604e241" publication-type="other">
Married men who live in a uxorilocal residence may return to their maternal village upon the cessation of a union and thus will
be less likely to be interviewed in the next survey round. This could explain the lack of a significant statistical relationship.</mixed-citation>
            </p>
         </fn>
         <fn id="d1604e251a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1604e258" publication-type="other">
Caution is necessary, however, because formerly married women may, for example, turn to commercial sex work after marital
dissolution as a means of providing for themselves (Vandenhoudt et al. 2013).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1604e277a1310">
            <mixed-citation id="d1604e281" publication-type="other">
Allison, Paul. 2005. Fixed Effects Regression Methods for Longitudinal Data Using SAS. SAS Institute, Cary, NC.</mixed-citation>
         </ref>
         <ref id="d1604e288a1310">
            <mixed-citation id="d1604e292" publication-type="other">
Anglewicz, Philip 2012. "Migration, marital change and HIV infection in Malawi," Demography 49(1): 239-265.</mixed-citation>
         </ref>
         <ref id="d1604e299a1310">
            <mixed-citation id="d1604e303" publication-type="other">
Anglewicz, Philip, Jimi Adams, Francis Onyango, Susan Watkins, and Hans-Peter Kohler. 2009. "The Malawi Diffu-
sion and Ideational Change Project 2004-06: Data collection, data quality, and analysis of attrition," Demographic
Research 20(21): 503-40.</mixed-citation>
         </ref>
         <ref id="d1604e316a1310">
            <mixed-citation id="d1604e320" publication-type="other">
Anglewicz, Philip and Hans-Peter Kohler. 2009. "Overestimating HIV infection: The construction and accuracy of sub-
jective probabilities of HIV infection in rural Malawi," Demographic Research 20(6): 65-96.</mixed-citation>
         </ref>
         <ref id="d1604e331a1310">
            <mixed-citation id="d1604e335" publication-type="other">
Arpino, Bruno, Elisabetta De Cao, and Franco Peracchi. 2013. "Using panel data for partial identification of human im-
munodeficiency virus prevalence when infection status is missing not at random," Journal of the Royal Statistical
Society: Series A (Statistics in Society) 177(3): 587-606.</mixed-citation>
         </ref>
         <ref id="d1604e348a1310">
            <mixed-citation id="d1604e352" publication-type="other">
Bellan, Steve, Kathryn Fiorella, Dessalegn Melesse, et al. 2013. "Extra-couple HIV transmission in sub-Saharan Africa:
A mathematical modelling study of survey data," The Lancet 381(9877): 1561-1569.</mixed-citation>
         </ref>
         <ref id="d1604e362a1310">
            <mixed-citation id="d1604e366" publication-type="other">
Bignami-Van Assche, Simona, et al. 2004. "Research protocol for collecting STI and biomarker samples in Malawi." SNP
Working Paper No. 7. Philadelphia: University of Pennsylvania, http://www.malawi.pop.upenn.edu.</mixed-citation>
         </ref>
         <ref id="d1604e376a1310">
            <mixed-citation id="d1604e380" publication-type="other">
Bignami-Van Assche, Simona, Georges Reniers, and Alexander A. Weinreb. 2003. "An assessment of the KDICP and
MDICP data quality: Interviewer effects, question reliability and sample attrition," Demographic Research SI(2):
31-76.</mixed-citation>
         </ref>
         <ref id="d1604e393a1310">
            <mixed-citation id="d1604e397" publication-type="other">
Boerma, J. Ties, Mark Urassa, Soori Nnko, et al. 2002. "Sociodemographic context of the AIDS epidemic in a rural area in
Tanzania with a focus on people's mobility and marriage," Sexually Transmitted Infections 78(S1): i97-il05.</mixed-citation>
         </ref>
         <ref id="d1604e407a1310">
            <mixed-citation id="d1604e411" publication-type="other">
Boileau, Catherine, Shelley Clark, Simona Bignami-Van Assche, et al. 2009. "Sexual and marital trajectories and HIV in-
fection among ever-married women in rural Malawi," Sexually Transmitted Infections 85(S1): i27-i33.</mixed-citation>
         </ref>
         <ref id="d1604e422a1310">
            <mixed-citation id="d1604e426" publication-type="other">
Bracher, Michael, Gigi Santow, and Susan Watkins. 2003. "'Moving' and marrying: Modeling HIV infection among
newly-weds in Malawi," Demographic Research Sl(6): 207-246.</mixed-citation>
         </ref>
         <ref id="d1604e436a1310">
            <mixed-citation id="d1604e440" publication-type="other">
Carpenter, L.M., A. Kamali, A. Ruberantwari, S.S. Malamba, and J. A.G. Whitworth. 1999. "Rates of HI V-1 transmission
within marriage in rural Uganda in relation to the HIV sero-status of the partners," AIDS 13(9): 1083-1089.</mixed-citation>
         </ref>
         <ref id="d1604e450a1310">
            <mixed-citation id="d1604e454" publication-type="other">
Clark, Shelley, Judith Bruce, and Annie Dude. 2006. "Protecting young women from HIV/AIDS: The case against child
and adolescent marriage," International Family Planning Perspectives 32(2): 79-88.</mixed-citation>
         </ref>
         <ref id="d1604e464a1310">
            <mixed-citation id="d1604e468" publication-type="other">
Clark, Shelley, Michelle Poulin, and Hans-Peter Kohler. 2009. "Marital aspirations, sexual behaviors, and HIV/AIDS in
rural Malawi "Journal of Marriage and Family 71(2): 396-416.</mixed-citation>
         </ref>
         <ref id="d1604e478a1310">
            <mixed-citation id="d1604e482" publication-type="other">
de Walque, Damien. 2007. "Sero-discordant couples in five African countries: Implications for prevention strategies,"
Population and Development Review 33(3): 501-523.</mixed-citation>
         </ref>
         <ref id="d1604e492a1310">
            <mixed-citation id="d1604e496" publication-type="other">
de Walque, Damien and Rachel Kline. 2012. "The association between remarriage and HIV infection in 13 sub-Saharan
African countries," Studies in Family Planning 43(1): 1-10.</mixed-citation>
         </ref>
         <ref id="d1604e507a1310">
            <mixed-citation id="d1604e511" publication-type="other">
Dunkle, Kristen, Rob Stephenson, Etienne Chomba, et al. 2008. "New heterosexually transmitted HIV infections in
married or cohabiting couples in urban Zambia and Rwanda: An analysis of survey and clinical data," The Lancet
371(9631): 2183-2191.</mixed-citation>
         </ref>
         <ref id="d1604e524a1310">
            <mixed-citation id="d1604e528" publication-type="other">
Filmer, Deon and Lant Pritchett. 2001. "Estimating wealth effects without expenditure data or tears: An application to
educational enrollments in states of India," Demography 38(1): 115-132.</mixed-citation>
         </ref>
         <ref id="d1604e538a1310">
            <mixed-citation id="d1604e542" publication-type="other">
Floyd, Sian, Amelia Crampin, Judith Glynn, et al. 2008. "The long-term social and economic impact of HIV on the spouses
of infected individuals in northern Malawi," Tropical Medicine and International Health 13(4): 520-531.</mixed-citation>
         </ref>
         <ref id="d1604e552a1310">
            <mixed-citation id="d1604e556" publication-type="other">
Glynn, Judith, Michael Carael, Anne Buve, et al. 2003. "HIV risk in relation to marriage in areas with high prevalence of
HIV infection," Journal of Acquired Immune Deficiency Syndromes 33(4): 526-535.</mixed-citation>
         </ref>
         <ref id="d1604e566a1310">
            <mixed-citation id="d1604e570" publication-type="other">
Gregson, Simon, Peter R. Mason, Geoff P. Garnett, et al. 2001. "A rural HIV epidemic in Zimbabwe? Findings from a
population-based survey," International Journal of STD &amp; AIDS 12(3): 189-196.</mixed-citation>
         </ref>
         <ref id="d1604e580a1310">
            <mixed-citation id="d1604e584" publication-type="other">
Helleringer, Stephane and Hans-Peter Kohler. 2007. "Sexual network structure and the spread of HIV in Africa: Evidence
from Likoma Island, Malawi," AIDS 21(17): 2323-2332.</mixed-citation>
         </ref>
         <ref id="d1604e595a1310">
            <mixed-citation id="d1604e599" publication-type="other">
Hsaio, Cheng. 2003. Analysis of Panel Data. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1604e606a1310">
            <mixed-citation id="d1604e610" publication-type="other">
Jahn, Andreas, Sian Floyd, Amelia C. Crampin, et al. 2008. "Population-level effect of HIV on adult mortality and early
evidence of reversal after introduction of antiretroviral therapy in Malawi," The Lancet 371(9624): 1603-1611.</mixed-citation>
         </ref>
         <ref id="d1604e620a1310">
            <mixed-citation id="d1604e624" publication-type="other">
Kohler, Hans-Peter, Susan Watkins, Jere Behrman, et al. 2014. "Cohort profile: The Malawi Longitudinal Study of Families
and Health (MLSFH)," International Journal of Epidemiology. First published online, 16 March 2014. doi: 10.1093/
ije/dyu049.</mixed-citation>
         </ref>
         <ref id="d1604e637a1310">
            <mixed-citation id="d1604e641" publication-type="other">
Lopman, Ben, Constance Nyamukapa, Timothy Hallett, et al. 2009. "Role of widows in the heterosexual transmission of
HIV in Manicaland, Zimbabwe, 1998-2003," Sexually Transmitted Infections 85(S1): i41-i48.</mixed-citation>
         </ref>
         <ref id="d1604e651a1310">
            <mixed-citation id="d1604e655" publication-type="other">
Lowrance, David, Simon Makombe, Anthony Harries, et al. 2008. "A public health approach to rapid scale-up of antiret-
roviral treatment in Malawi during 2004-2006," Journal of Acquired Immune Deficiency Syndromes 49(3): 287-293.</mixed-citation>
         </ref>
         <ref id="d1604e665a1310">
            <mixed-citation id="d1604e669" publication-type="other">
Mackelprang, R.D., R. Bosire, B.L. Guthrie, et al. 2014. "High rates of relationship dissolution among heterosexual HIV-
serodiscordant couples in Kenya," AIDS &amp; Behavior 18(1): 189-193.</mixed-citation>
         </ref>
         <ref id="d1604e680a1310">
            <mixed-citation id="d1604e684" publication-type="other">
National Statistics Office (NSO) [Malawi] and ICF Macro. 2011. Malawi Demographic and Health Survey 2010. Zomba,
Malawi, and Calverton, MD: NSO and ICF Macro.</mixed-citation>
         </ref>
         <ref id="d1604e694a1310">
            <mixed-citation id="d1604e698" publication-type="other">
National Statistics Office (NSO) [Malawi] and ORC Macro. 2005. Malawi Demographic and Health Survey 2004. Calver-
ton, MD: NSO and ORC Macro.</mixed-citation>
         </ref>
         <ref id="d1604e708a1310">
            <mixed-citation id="d1604e712" publication-type="other">
Obare, Francis, Peter Fleming, Philip Anglewicz, et al. 2009. "HIV incidence and learning one's HIV status: Evidence
from repeat population-based voluntary counseling and testing in rural Malawi," Sexually Transmitted Infections
85(2) 139-144.</mixed-citation>
         </ref>
         <ref id="d1604e725a1310">
            <mixed-citation id="d1604e729" publication-type="other">
Pison, Giles. 1986. "La démographie de la polygamie," Population 41(1): 93-122.</mixed-citation>
         </ref>
         <ref id="d1604e736a1310">
            <mixed-citation id="d1604e740" publication-type="other">
Porter, Laura, Lingxin Hao, David Bishai, et al. 2004. "HIV status and union dissolution in sub-Saharan Africa: The case
of Rakai, Uganda," Demography 41(3): 465-482.</mixed-citation>
         </ref>
         <ref id="d1604e750a1310">
            <mixed-citation id="d1604e754" publication-type="other">
Reniers, Georges. 2003. "Divorce and remarriage in rural Malawi," Demographic Research SI: 175-206.</mixed-citation>
         </ref>
         <ref id="d1604e762a1310">
            <mixed-citation id="d1604e766" publication-type="other">
. 2008. "Marital strategies for regulating exposure to HIV," Demography 45(2): 417-438.</mixed-citation>
         </ref>
         <ref id="d1604e773a1310">
            <mixed-citation id="d1604e777" publication-type="other">
Reniers, Georges and Benjamin Armbruster. 2012. "HIV status awareness, partnership dissolution and HIV transmission
in generalized epidemics," PloS One 7(12) e50669.</mixed-citation>
         </ref>
         <ref id="d1604e787a1310">
            <mixed-citation id="d1604e791" publication-type="other">
Reniers, Georges and Stephane Helleringer. 2011. "Serosorting and the evaluation of HIV testing and counseling for HIV
prevention in generalized epidemics," AIDS &amp; Behavior 15(1): 1-8.</mixed-citation>
         </ref>
         <ref id="d1604e801a1310">
            <mixed-citation id="d1604e805" publication-type="other">
Reniers, Georges and Rania Tfaily. 2008. "Polygyny and HIV in Malawi," Demographic Research 19(23): 1811-1830.</mixed-citation>
         </ref>
         <ref id="d1604e812a1310">
            <mixed-citation id="d1604e816" publication-type="other">
Smith, Kirsten and Susan Watkins. 2005. "Perceptions of risk and strategies for prevention: Responses to HIV/AIDS in
rural Malawi," Social Science and Medicine 60(3): 649-660.</mixed-citation>
         </ref>
         <ref id="d1604e826a1310">
            <mixed-citation id="d1604e830" publication-type="other">
UNAIDS. 2013. "UNAIDS report on the global AIDS epidemic 2013." Accessed 23 April 2013. http://www.unaids.org/
en/media/unaids/contentassets/documents/epidemiology/2013/gr2013/UN AIDS_Global_Report_2013_en.pdf.</mixed-citation>
         </ref>
         <ref id="d1604e841a1310">
            <mixed-citation id="d1604e845" publication-type="other">
Vandenhoudt, H.M., L. Langat, J. Menten, et al. 2013. "Prevalence of HIV and other sexually transmitted infections among
female sex workers in Kisumu, Western Kenya, 1997 and 2008," PLoS One 8(1): e54953.</mixed-citation>
         </ref>
         <ref id="d1604e855a1310">
            <mixed-citation id="d1604e859" publication-type="other">
Watkins, Susan Cotts. 2004. "Navigating the AIDS epidemic in rural Malawi," Population and Development Review 30(4):
673-705.</mixed-citation>
         </ref>
         <ref id="d1604e869a1310">
            <mixed-citation id="d1604e873" publication-type="other">
Watkins, Susan, Jere Behrman, Hans-Peter Köhler, and Eliya Zulu. 2003. "Introduction to 'Research on demographic
aspects of HIV/AIDS in rural Africa,' " Demographic Research SI ( 1 ): 1-30.</mixed-citation>
         </ref>
         <ref id="d1604e883a1310">
            <mixed-citation id="d1604e887" publication-type="other">
Welz, Tanya, Victoria Hosegood, Shabbar Jaffar, et al. 2007. "Continued very high prevalence of HIV infection in rural
KwaZulu-Natal, South Africa: A population-based longitudinal study," AIDS 21(11): 1467-1472.</mixed-citation>
         </ref>
         <ref id="d1604e897a1310">
            <mixed-citation id="d1604e901" publication-type="other">
WHO/UNAIDS/UNICEF. 2011. "Global HIV/AIDS Response: Epidemic update and health sector progress towards Uni-
versal Access—Progress report 2011." Accessed 23 April 2013. http://www.who.int/hiv/pub/progress_report2011/
en/index.html.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
