<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt24h1dk</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt18z4g30</book-id>
      <subj-group>
         <subject content-type="call-number">K3260.3.Y26 2016</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Right to health</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights</subject>
         <subj-group>
            <subject content-type="lcsh">Health aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Health services accessibility</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Medical policy</subject>
         <subj-group>
            <subject content-type="lcsh">Moral and ethical aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Public health</subject>
         <subj-group>
            <subject content-type="lcsh">Moral and ethical aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Women</subject>
         <subj-group>
            <subject content-type="lcsh">Medical care</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Poor</subject>
         <subj-group>
            <subject content-type="lcsh">Medical care</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Health Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Power, Suffering, and the Struggle for Dignity</book-title>
         <subtitle>Human Rights Frameworks for Health and Why They Matter</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Yamin</surname>
               <given-names>Alicia Ely</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib2">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Farmer</surname>
               <given-names>Paul</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib3">
            <name name-style="western">
               <surname>Lockwood</surname>
               <given-names>Bert B.</given-names>
               <suffix>Jr.</suffix>
            </name>
            <role>Series Editor</role>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>04</day>
         <month>12</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780812247749</isbn>
      <isbn content-type="epub">9780812292190</isbn>
      <isbn content-type="epub">0812292197</isbn>
      <publisher>
         <publisher-name>University of Pennsylvania Press, Inc.</publisher-name>
         <publisher-loc>PHILADELPHIA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>University of Pennsylvania Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt18z4g30"/>
      <abstract abstract-type="short">
         <p>Directed at a diverse audience of students, legal and public health practitioners, and anyone interested in understanding what human rights-based approaches (HRBAs) to health and development mean and why they matter,<italic>Power, Suffering, and the Struggle for Dignity</italic>provides a solid foundation for comprehending what a human rights framework implies and the potential for social transformation it entails. Applying a human rights framework to health demands that we think about our own suffering and that of others, as well as the fundamental causes of that suffering. What is our agency as human subjects with rights and dignity, and what prevents us from acting in certain circumstances? What roles are played by others in decisions that affect our health? How do we determine whether what we may see as "natural" is actually the result of mutable, human policies and practices?</p>
         <p>Alicia Ely Yamin couples theory with personal examples of HRBAs at work and shows the impact they have had on people's lives and health outcomes. Analyzing the successes of and challenges to using human rights frameworks for health, Yamin charts what can be learned from these experiences, from conceptualization to implementation, setting out explicit assumptions about how we can create social transformation. The ultimate concern of<italic>Power, Suffering, and the Struggle for Dignity</italic>is to promote movement from analysis to action, so that we can begin to use human rights frameworks to effect meaningful social change in global health, and beyond.</p>
      </abstract>
      <counts>
         <page-count count="336"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Farmer</surname>
                           <given-names>Paul</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>My profound admiration and respect for Alicia Ely Yamin and her work in health and human rights goes back many years, beginning well before our work together as editors of the journal<italic>Health and Human Rights</italic>. But when we each penned our first contributions as editors for the journal in 2007, our shared experiences, indignation, and understandings about the inequities in the world became even more evident.</p>
                     <p>Alicia is not just an extraordinary leader in the field but someone who has been in the trenches. She speaks authoritatively because she understands the ways in which cycles of poverty and disease</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.5</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.6</book-part-id>
                  <title-group>
                     <title>Introduction.</title>
                     <subtitle>How Do We Understand Suffering?</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Before I had my two children, I had a miscarriage. I was living in New York City at the time and medically it was not a major event. I required surgery, but I was admitted to the hospital very early in the morning and by that same evening I was released and at home. Of course, emotionally it was deeply, deeply painful. Earlier, I had been invited to go on a human rights fact-finding delegation to the state of Chiapas in southern Mexico that was scheduled for the week after my unexpected miscarriage. I had lived in Mexico for years</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.7</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Dignity and Suffering:</title>
                     <subtitle>Why Human Rights Matter</subtitle>
                  </title-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>I graduated from law school with an Echoing Green Foundation Fellowship to bring human rights cases from Mexico to international tribunals and treaty monitoring bodies.¹ One of the first cases I worked on involved the annihilation of nearly an entire family. Francisco Quijano Santoyo was allegedly engaged in drug trafficking and was involved in a shootout that resulted in the death of a police officer. The next day, antinarcotics agents from the Federal Judicial Police (PJF, according to the acronym in Spanish), the Mexican equivalent of the FBI, surrounded the family’s house in Mexico City and exterminated two of three</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.8</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>The Powerlessness of Extreme Poverty:</title>
                     <subtitle>Human Rights and Social Justice</subtitle>
                  </title-group>
                  <fpage>49</fpage>
                  <abstract>
                     <p>Around the world, it is the poor who suffer the vast majority of civil and political rights violations, including torture, in both public and private spheres. In the years I lived in Mexico, far more of the clients I worked with were like Gabriela, with limited choices and struggling to make ends meet—and not like Rosa, who was decidedly middle class. There was the teenager who was playing soccer with friends, who must have irritated the wrong police officer on the wrong day because he ended up tortured to death in a local jail for simply urinating in public.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.9</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Redefining Health:</title>
                     <subtitle>Challenging Power Relations</subtitle>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Before I decided definitively to pursue a career linking global health with human rights, I spent some months working and living at a small, privately funded health-care center in Haryana, India, a small state that borders Punjab. Delhi’s explosive growth since the 1990s has made the area I was in significantly less rural today. But back then, it took several hours, on several buses, to get to the health center from Delhi, and it was a different world. Indeed, Kabliji Hospital was founded as a rural health center, with the specific goal of providing free care to the poor and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.10</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Health Systems as “Core Social Institutions”</title>
                  </title-group>
                  <fpage>99</fpage>
                  <abstract>
                     <p>The day I arrived in Quibdó, in May 2010, was a typically steamy-hot day, which left us drenched in sweat within minutes of leaving the airport. Quibdó is the capital of Chocó, an impoverished, jungle department of Colombia. It is a place that illustrates how “magical realism” in some Latin American literature often merely records observed reality, a place where one feels that “plagues of insomnia and forgetfulness”¹ could surely rival more mundane public health threats.²</p>
                     <p>The enormous army barracks on the outskirts of town were teeming with young soldiers whose menacing automatic weapons and camouflage-covered bravado contrasted sharply with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>Beyond Charity:</title>
                     <subtitle>The Central Importance of Accountability</subtitle>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>I was privileged to be able to help shape Amnesty International’s “Demand Dignity Campaign,” especially in relation to its work on maternal mortality. Launched in 2009, the Demand Dignity Campaign signaled a watershed in international human rights. Previously, Amnesty had eschewed ESC rights, focusing instead on a narrow slice of CP rights, involving, for example, political prisoners and freedom from torture.¹ As the oldest and by far the largest international human rights organization, Amnesty International’s reticence with respect to ESC rights had ripple effects throughout the field. So when, under the leadership of then-Secretary General Irene Khan, the organization changed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.12</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>Power and Participation</title>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>In 2007, as director of research and investigations at Physicians for Human Rights, I led a field investigation into maternal mortality in Peru.¹ I had lived in Peru for years before and knew the country well. As part of the follow-up to that investigation, I worked with CARE Peru to develop a program to strengthen the capacity of local health promoters in the subnational “department” (state) of Puno, to accompany pregnant women to their antenatal checks and delivery care, and to ensure that their rights were respected. Among other things, the health promoters recorded such things as whether the women</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.13</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Shades of Dignity:</title>
                     <subtitle>Equality and Nondiscrimination</subtitle>
                  </title-group>
                  <fpage>180</fpage>
                  <abstract>
                     <p>I met Nomkhosi’s mother, Zondi, in 2013, in an impoverished, semirural area of KwaZulu-Natal province in South Africa. KwaZulu-Natal is the birthplace of the Inkatha Freedom Party (IFP) and Zondi lived not far from some of the battles and massacres that had taken place in the early 1990s between the IFP and African National Congress (ANC) members. Political violence in the region, referred to as the “unofficial war” between the IFP and ANC, killed an estimated 20,000 people between the mid-1980s and the late 1990s. The violence had a drastic impact on the everyday lives of the people living in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.14</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Our Place in the World:</title>
                     <subtitle>Obligations Beyond Borders</subtitle>
                  </title-group>
                  <fpage>206</fpage>
                  <abstract>
                     <p>I was living in Tanzania when I first toured Temeke Hospital in early 2012. Temeke, which serves a large catchment area in a poor part of Dar es Salaam, has historically been the most dysfunctional of the city’s district hospitals. Unlike the other districts, Temeke, which includes some periurban and semirural areas as well, has few private facilities and, at the time, the hospital was invariably overcrowded. On that first visit, the labor ward had three or four women to a bed and women delivering on the floor with just a<italic>khanga</italic>spread beneath them. The overworked staff appeared numb</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.15</book-part-id>
                  <title-group>
                     <title>Conclusion:</title>
                     <subtitle>Another World Is Possible</subtitle>
                  </title-group>
                  <fpage>229</fpage>
                  <abstract>
                     <p>One of my favorite “stories” by the Argentine fiction writer Jorge Luis Borges is “Pierre Menard: Author of the<italic>Quixote</italic>.” In this very short piece, Borges writes a literary critique of a French author—Pierre Menard—and his version of<italic>Don Quixote</italic>is word for word, line for line, identical to the original Cervantes story. But Borges says the story is not just a crude copy. It is actually infinitely richer than the original version because it has all the advantages of three hundred years of allusion to be read into it: “To compose the<italic>Quixote</italic>at the beginning of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>251</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.17</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>293</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>297</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18z4g30.19</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>313</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
