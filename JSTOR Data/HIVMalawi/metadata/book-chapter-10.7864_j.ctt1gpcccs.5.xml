<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gpcccs</book-id>
      <subj-group>
         <subject content-type="call-number">HC60.O84 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic assistance, American</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">2001–</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Other War</book-title>
         <subtitle>Global Poverty and the Millennium Challenge Account</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Brainard</surname>
               <given-names>Lael</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Graham</surname>
               <given-names>Carol</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib3">
            <name name-style="western">
               <surname>Purvis</surname>
               <given-names>Nigel</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib4">
            <name name-style="western">
               <surname>Radelet</surname>
               <given-names>Steven</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib5">
            <name name-style="western">
               <surname>Smith</surname>
               <given-names>Gayle E.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>13</day>
         <month>05</month>
         <year>2004</year>
      </pub-date>
      <isbn content-type="ppub">9780815711148</isbn>
      <isbn content-type="epub">9780815711193</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2003</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1gpcccs"/>
      <abstract abstract-type="short">
         <p>A Brookings Institution Press and the Center for Global Development publicationThe plight of the poorest around the world has been pushed to the forefront of America's international agenda for the first time in many years by the war on terrorism and the formidable challenges presented by the HIV/AIDS pandemic. In March 2002, President Bush announced the creation of the Millennium Challenge Account (MCA). This bilateral development fund represents an increase of $5 billion per year over current assistance levels and establishes of a new agency to promote growth in reform-oriented developing countries.Amounting to a doubling of U.S. bilateral development aid-the largest increase in decades-the MCA offers a critical chance to deliberately shape the face that the United States presents to people in poor nations around the world. This book makes concrete recommendations on crafting a new blueprint for distributing and delivering aid to make the MCA an effective tool, not only in its own right, but also in transforming U.S. foreign aid and strengthening international aid cooperation more generally.The book tackles head on the tension between foreign policy and development goals that chronically afflicts U.S. foreign assistance; the danger of being dismissed as one more instance of the United States going it alone instead of buttressing international cooperation; and the risk of exacerbating confusion among the myriad overlapping U.S. policies, agencies, and programs targeted at developing nations, particularly USAID.In doing so, The Other War draws important lessons from new international development initiatives, such as the Global Fund to Fight AIDS, TB, and Malaria, the mixed record of previous U.S. aid efforts, trends in the U.S. budget for foreign assistance, the agencies currently involved in administering U.S. development policy, and the importance of the relationship between Congress and the executive branch in determining aid outcomes. The MCA holds the promise of substantially increasing U.S. development assistance and piolicy, and the importance of the relationship between Congress and the executive branch in determining aid outcomes. The MCA holds the promise of substantially increasing U.S. development assistance and pioneering a new era in aid, but the authors caution against creating yet another example of wasted aid that could undermine political support for foreign assistance for decades to come.</p>
      </abstract>
      <counts>
         <page-count count="265"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Talbott</surname>
                           <given-names>Strobe</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Birdsall</surname>
                           <given-names>Nancy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>The plight of the poorest around the world has been pushed to the forefront of America’s international agenda for the first time in many years. The debate triggered by the war on terrorism and its extension to Iraq has focused attention on third world poverty—even though the links between poverty and terrorism are far from clear—and the HIV/AIDS pandemic is making the challenge of development even more formidable.</p>
                     <p>In March 2002, in the context of the UN Conference on Financing for Development, President Bush announced his intention to request an increase of $5 billion a year over current</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In March 2002 President Bush announced his intention to request an increase of $5 billion per year over current assistance levels through the creation of a bilateral development fund, the Millennium Challenge Account. To implement the program, the administration subsequently recommended the creation of an independent agency, the Millennium Challenge Corporation (MCC), to allocate the new funding on the basis of objective selection criteria based on a nation’s commitment to “governing justly, investing in people, and encouraging economic freedom.”¹ The proposed Millennium Challenge Account (MCA) is nearly double the size of existing U.S. bilateral assistance programs that are devoted specifically</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Global Aid Trends and Donor Coordination</title>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>President Bush announced the Millennium Challenge Account as the U. S. contribution to the United Nations Conference on Financing for Development in Monterrey, Mexico, in March 2002. The central purpose of the conference was to coordinate and increase donor efforts to combat global poverty. Yet since that summit—and including the November announcement of the Millennium Challenge Corporation (MCC) as the institutional home for the increased aid funds—coordination with other donors and with international financial institutions and other multilateral development banks has been surprisingly absent from the discussion. This chapter focuses on the question of how the operations of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Who Should Qualify?</title>
                  </title-group>
                  <fpage>28</fpage>
                  <abstract>
                     <p>In his speech announcing the Millennium Challenge Account, President Bush proposed that funds from the new program go to low-income countries that are “ruling justly, investing in their people, and encouraging economic freedom.”¹ The administration subsequently announced the procedure it proposes to follow in determining which countries are meeting these three broad criteria. The proposed methodology is one way to address several key issues central to the MCA, including:</p>
                     <p>—the income levels that will determine the set of eligible countries,</p>
                     <p>—the specific indicators that will be used to show commitment in the three broad areas,</p>
                     <p>—the passing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Policy Framework for the Millennium Challenge Account</title>
                  </title-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>In contrast to the sprawling mission and multiple objectives of the U.S. Agency for International Development (USAID), the Millennium Challenge Account will achieve greatest effectiveness by having a narrowly defined mission and developing a core competence in limited areas related to it. The mission of the MCA should be to support growth and sustainable development and combat poverty in poor nations with a demonstrated record of sound policy, social investment, and good governance, by underwriting meritorious strategies designed and implemented by recipients. This chapter sets out a simple set of criteria to determine the priority program areas for the MCA.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>A New Approach to Aid</title>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>Once countries qualify for the Millennium Challenge Account, how should funds be delivered to ensure that they are as effective as possible in supporting growth, poverty reduction, and human development in recipient countries? This chapter examines the core elements of the policy framework for the MCA, including the responsibility for proposing and designing activities funded by the MCA, the types of activities that should be funded, the question of funding projects versus programs, monitoring and evaluation, and coordination with other donors. Each element of the MCA value chain (sketched in figure 5-1) is discussed in turn.</p>
                     <p>Currently U.S. foreign assistance</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Institutional and Operational Guidelines</title>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>The effectiveness of the Millennium Challenge Account will depend not only on good proposals but also on the institutional model and operational approaches guiding its implementation. This chapter reviews the advantages and disadvantages of various institutional models for the MCA and examines the board structure, staffing arrangements, and operational procedures that would best serve the MCA’s implementation. It is recommended that there be a clearer relationship between the MCC and USAID, that the MCC’s board be expanded, that staff detailed to the MCC be funded by MCA operational costs and not by outside agencies or companies, that new contracting methods</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>U.S. Foreign Assistance and Development Programs</title>
                  </title-group>
                  <fpage>146</fpage>
                  <abstract>
                     <p>Even a smart design and a lot of money will not be enough to make a difference to U.S. foreign assistance, unless the Millennium Challenge Account reflects the lessons from past foreign aid failures and successes and complements other U.S. programs for developing countries. While the Bush administration seems intent on starting from a blank slate in order to avoid the problems of past foreign assistance programs, there has been scant reference to the precise nature of those problems and no discussion about the impact on U.S. Agency for International Development (USAID), which will maintain responsibility for the large majority</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>A New Partnership between Congress and the Administration</title>
                  </title-group>
                  <fpage>170</fpage>
                  <abstract>
                     <p>In this chapter, we turn our attention to the role of Congress. The preceding chapters have highlighted those elements of program design and implementation by the executive branch that will be critical to the success of the Millennium Challenge Account (MCA) and U.S. programs to combat global poverty more broadly. But none of this will be possible without strong support from Congress. The purpose of this chapter is to highlight key aspects of program design that would lay the groundwork for a partnership between Congress and the executive branch in support of a successful MCA.</p>
                     <p>Historian Edward S. Corwin once</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Funding the Millennium Challenge Account</title>
                  </title-group>
                  <fpage>194</fpage>
                  <abstract>
                     <p>Last but not least, we come to the issue of money. This chapter assesses the projected budget for the Millennium Challenge Account. It starts by placing the proposed MCA funding in context by considering broader trends in the U.S. budget for foreign assistance and the allocation between development assistance and other types of foreign aid. The MCA is sizable in relation to current levels of U.S. development assistance and the combined income of the select group of initially eligible countries, but, even with full funding of the MCA, U.S. official aid would be small compared to aid from other donors,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Making a Difference to Global Poverty</title>
                  </title-group>
                  <fpage>211</fpage>
                  <abstract>
                     <p>The United States has a vital national interest in being a good development partner to the many countries around the world struggling with poverty. President Bush’s proposal to create a $5 billion Millennium Challenge Account (MCA) creates a rare and important opportunity to pioneer a more effective partnership. But, welcome as they are, new money and a good idea are not enough. Critical design and implementation decisions require urgent attention for the new fund to succeed on its own terms, as well as to strengthen international cooperation and broader U.S. development policy.<italic>The Other War: Global Poverty and the Millennium</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.14</book-part-id>
                  <title-group>
                     <title>APPENDIX A</title>
                     <subtitle>Environment Assessments: The OPIC Model</subtitle>
                  </title-group>
                  <fpage>231</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.15</book-part-id>
                  <title-group>
                     <title>APPENDIX B</title>
                     <subtitle>Congressional Budgetary Basics</subtitle>
                  </title-group>
                  <fpage>234</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>237</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.17</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>253</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcccs.19</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>266</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
