<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt19rmdqh</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Law</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Defense Institution Building in Africa</book-title>
         <subtitle>An Assessment</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>McNerney</surname>
               <given-names>Michael J.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Johnson</surname>
               <given-names>Stuart E.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib3">
            <name name-style="western">
               <surname>Pezard</surname>
               <given-names>Stephanie</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib4">
            <name name-style="western">
               <surname>Stebbins</surname>
               <given-names>David</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib5">
            <name name-style="western">
               <surname>Miles</surname>
               <given-names>Renanah</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib6">
            <name name-style="western">
               <surname>OʹMahony</surname>
               <given-names>Angela</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib7">
            <name name-style="western">
               <surname>Feng</surname>
               <given-names>Chaoling</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib8">
            <name name-style="western">
               <surname>Oliver</surname>
               <given-names>Tim</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>04</day>
         <month>01</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9780833092403</isbn>
      <isbn content-type="ppub">0833092405</isbn>
      <isbn content-type="epub">9780833093707</isbn>
      <publisher>
         <publisher-name>RAND Corporation</publisher-name>
         <publisher-loc>Santa Monica, Calif.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>RAND Corporation</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7249/j.ctt19rmdqh"/>
      <abstract abstract-type="short">
         <p>This report assesses U.S. efforts in defense institution building (DIB) in Africa and suggests possible improvements to planning and execution. It defines DIB and reviews some best practices from DIB and security sector reform experiences. It also highlights how DIB activities serve U.S. official strategic guidance for Africa.</p>
      </abstract>
      <counts>
         <page-count count="196"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.2</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.3</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.4</book-part-id>
                  <title-group>
                     <title>Figures and Tables</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.5</book-part-id>
                  <title-group>
                     <title>Summary</title>
                  </title-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>Defense institutions play a critical role sustaining military forces and ensuring that those forces are accountable to and supportive of civilian institutions. Any country can find sustaining such institutions a challenge, but the challenge is particularly acute for many African nations, where democratic governance, economic and social well-being, and security, as well as the resources to address these issues, can be limited or simply not exist. Such challenges notwithstanding, the 2014 Quadrennial Defense Review noted that, in Africa, “there is also significant opportunity to develop stronger governance institutions and to help build professional, capable military forces that can partner with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.6</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xxi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.7</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Defense institutions play two crucial roles in a country’s defense sector. First, they sustain a country’s military forces. Without the foundation of strong defense institutions, military forces will suffer from weak authorities and systems necessary for long-term effectiveness and responsiveness (e.g., administrative, legal, personnel, resource management, policy, strategy, logistics, and acquisition). Second, healthy defense institutions ensure military forces are professional, accountable, transparent, and subject to civilian oversight and the rule of law. Maintaining effective and legitimate defense institutions is challenging for all countries, but it can be particularly so in many African countries, where democratic governance, economic and social well-being,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.8</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>DIB Best Practices and Their Relevance to U.S. Strategic Objectives in Africa</title>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>The concepts underlying DIB are not new. As the institutional dimension of broader SSR, DIB efforts have been going on for several decades. The history of DIB shows how such reform efforts picked up pace after the end of the Cold War, and how some major examples of DIB took place in Africa. This DIB experience has resulted in a collection of lessons, as well as best practices, that can inform present efforts. Such efforts directly serve U.S. strategic objectives in Africa, either directly, by building accountable and effective defense forces, or indirectly, by creating the conditions that make it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.9</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>DIB Programs in Africa:</title>
                     <subtitle>Challenges and Responses</subtitle>
                  </title-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>As the previous chapter showed, strengthening defense institutions is, in many ways, the foundation for most other U.S. security objectives in Africa. Our research indicates, however, that DIB is especially challenging in Africa, given the prevalence of fragile states and political sensitivities about sovereignty. In this chapter, we analyze these challenges and discuss ways DoD can strengthen its ability to identify and apply DIB programs in Africa.</p>
                     <p>To understand the challenges of DIB in Africa, we started with three questions. First, what do Office of the Secretary of Defense (OSD) policymakers want? Second, what do planners at AFRICOM need? And</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.10</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>Two Africa Case Studies</title>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>The two cases evaluated here—Liberia and Libya—represent the first type of DIB characterized in Chapter Two: large-scale, sweeping reforms that overhaul defense establishments and replace them with more efficient and accountable ones. Both experienced significant conflicts where institutions required a complete overhaul, and capacity-building efforts had transformational potential.¹ Yet this potential for high gains came with high risks, particularly in a country like Libya, where political and security gains have failed to take hold. As mentioned in Chapter Two, countries emerging from civil war or other conflicts tend to be more likely to accept major reforms; yet, their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.11</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>Findings and Recommendations</title>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>As discussed in Chapter One, our goal for this project was to assess DIB efforts in Africa and provide insights on possible improvements to planning and execution. In this chapter, we gather insights from the three components of our research: our review of DIB-related best practices and official guidance for Africa, our overview of DIB programs in Africa, and our case studies.</p>
                     <p>Although the use of the term DIB is relatively recent, it is closely related to other concepts that have been employed for several decades, such as SSR, defense sector reform, and SSG. Many of the best practices developed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.12</book-part-id>
                  <title-group>
                     <title>APPENDIX A</title>
                     <subtitle>AFRICOM Assessment Process</subtitle>
                  </title-group>
                  <fpage>105</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.13</book-part-id>
                  <title-group>
                     <title>APPENDIX B</title>
                     <subtitle>DoDʹs Regional Centers as DIB Providers</subtitle>
                  </title-group>
                  <fpage>109</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.14</book-part-id>
                  <title-group>
                     <title>APPENDIX C</title>
                     <subtitle>Allied Experience in DIB</subtitle>
                  </title-group>
                  <fpage>119</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.15</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>157</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt19rmdqh.16</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>161</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
