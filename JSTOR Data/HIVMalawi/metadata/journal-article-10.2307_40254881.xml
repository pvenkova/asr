<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40254881</article-id>
         <article-id pub-id-type="pub-doi">10.1086/597617</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Comparison of CD4 Cell Count, Viral Load, and Other Markers for the Prediction of Mortality among HIV-1–Infected Kenyan Pregnant Women</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Elizabeth R.</given-names>
                  <surname>Brown</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Phelgona</given-names>
                  <surname>Otieno</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dorothy A.</given-names>
                  <surname>Mbori-Ngacha</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Carey</given-names>
                  <surname>Farquhar</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Elizabeth M.</given-names>
                  <surname>Obimbo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ruth</given-names>
                  <surname>Nduati</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Julie</given-names>
                  <surname>Overbaugh</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Grace C.</given-names>
                  <surname>John-Stewart</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">199</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">9</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40009938</issue-id>
         <fpage>1292</fpage>
         <lpage>1300</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40254881"/>
         <abstract>
            <p>Background. There are limited data regarding the relative merits of biomarkers as predictors of mortality or time to initiation of antiretroviral therapy (ART). Methods. We evaluated the usefulness of the CD4 cell count, CD4 cell percentage (CD4%), human immunodeficiency virus type 1 (HIV-1) load, total lymphocyte count (TLC), body mass index (BMI), and hemoglobin measured at 32 weeks' gestation as predictors of mortality in a cohort of HIV-1-infected women in Nairobi, Kenya. Sensitivity, specificity, positive predictive value (PPV), and area under the receiver operating characteristic (ROC) curve (AUC) were determined for each biomarker separately, as well as for the CD4 cell count and the HIV-1 load combined. Results. Among 489 women with 10,150 person-months of follow-up, mortality rates at 1 and 2 years postpartum were 2.1% (95% confidence interval [ CI], 0.7%–3.4%) and 5.5% (95% CI, 3.0%–8.0%), respectively. CD4 cell count and CD4% had the highest AUC value (&gt;0.9). BMI, TLC, and hemoglobin were each associated with but poorly predictive of mortality (PPV, &lt; 7%). The HIV-1 load did not predict mortality beyond the CD4 cell count. Conclusions. The CD4 cell count and CD4% measured during pregnancy were both useful predictors of mortality among pregnant women. TLC, BMI, and hemoglobin had a limited predictive value, and the HIV-1 load did not predict mortality any better than did the CD4 cell count alone.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d2661e283a1310">
            <label>1</label>
            <mixed-citation id="d2661e292" publication-type="other">
World Health Organization/Joint United Nations Programme on HIV/
AIDS/United Nations Children's Fund. Towards universal access: scal-
ing up priority HIV/AIDS interventions in the health sector: progress
report, April 2007. Available at: http://www.who.int/hiv/mediacentre/
universal_access_progress_report_en.pdf. Accessed 10 October 2007.</mixed-citation>
         </ref>
         <ref id="d2661e311a1310">
            <label>2</label>
            <mixed-citation id="d2661e318" publication-type="other">
Ekouevi DK, Inwoley A, Tonwe-Gold B, et al. Variation of CD4 count
and percentage during pregnancy and after delivery: implications for
HAART initiation in resource-limited settings. AIDS Res Hum Retrovi-
ruses 2007; 23:1469-73.</mixed-citation>
         </ref>
         <ref id="d2661e334a1310">
            <label>3</label>
            <mixed-citation id="d2661e341" publication-type="other">
Mulcahy F, Wallace E, Woods S, et al. Counts in pregnancy do not
accurately reflect the need for long-term HAART [abstract 704b]. In:
Program and abstracts of the 13th Conference on Retroviruses and
Opportunistic Infections (Denver). 2006. Available at: http://www
.retroconference.org/2006/Abstracts/27587.htm.</mixed-citation>
         </ref>
         <ref id="d2661e360a1310">
            <label>4</label>
            <mixed-citation id="d2661e367" publication-type="other">
Miotti PG, Liomba G, Dallabetta GA, Hoover DR, Chiphangwi JD, Saah
AJ. T lymphocyte subsets during and after pregnancy: analysis in human
immunodeficiency virus type 1-infected and–uninfected Malawian
mothers. J Infect Dis 1992; 165:1116-9.</mixed-citation>
         </ref>
         <ref id="d2661e384a1310">
            <label>5</label>
            <mixed-citation id="d2661e391" publication-type="other">
Temmerman M, Nagelkerke N, Bwayo J, Chomba EN, Ndinya-Achola J,
Piot P. HIV-1 and immunological changes during pregnancy: a compar-
ison between HIV-1-seropositive and HIV-1-seronegative women in
Nairobi, Kenya. AIDS 1995; 9:1057-60.</mixed-citation>
         </ref>
         <ref id="d2661e407a1310">
            <label>6</label>
            <mixed-citation id="d2661e414" publication-type="other">
Van der Paal L, Shafer LA, Mayanja BN, Whitworth JA, Grosskurth H.
Effect of pregnancy on HIV disease progression and survival among
women in rural Uganda. Trop Med Int Health 2007; 12:920-8.</mixed-citation>
         </ref>
         <ref id="d2661e427a1310">
            <label>7</label>
            <mixed-citation id="d2661e434" publication-type="other">
Erikstrup C, Kallestrup P, Zinyama R, et al. Predictors of mortality in a
cohort of HIV-1-infected adults in rural Africa. J Acquir Immune Defic
Syndr 2007; 44:478-83.</mixed-citation>
         </ref>
         <ref id="d2661e447a1310">
            <label>8</label>
            <mixed-citation id="d2661e454" publication-type="other">
O'Brien ME, Kupka R, Msamanga GI, Saathoff E, Hunter DJ, Fawzi
WW. Anemia is an independent predictor of mortality and immuno-
logie progression of disease among women with HIV in Tanzania. J
Acquir Immune Defic Syndr 2005; 40:219-25.</mixed-citation>
         </ref>
         <ref id="d2661e470a1310">
            <label>9</label>
            <mixed-citation id="d2661e477" publication-type="other">
Gupta A, Gupte N, Bhosale R, et al. Low sensitivity of total lymphocyte
count as a surrogate marker to identify antepartum and postpartum
Indian women who require antiretroviral therapy. J Acquir Immune
Defic Syndr 2007; 46:338-42.</mixed-citation>
         </ref>
         <ref id="d2661e493a1310">
            <label>10</label>
            <mixed-citation id="d2661e500" publication-type="other">
Chen RY, Westfall AO, Hardin JM, et al. Complete blood cell count as a
surrogate CD4 cell marker for HIV monitoring in resource-limited set-
tings. J Acquir Immune Defic Syndr 2007; 44:525-30.</mixed-citation>
         </ref>
         <ref id="d2661e514a1310">
            <label>11</label>
            <mixed-citation id="d2661e521" publication-type="other">
Spacek LA, Griswold M, Quinn TC, Moore RD. Total lymphocyte count
and hemoglobin combined in an algorithm to initiate the use of highly
active antiretroviral therapy in resource-limited settings. AIDS 2003; 17:
1311-7.</mixed-citation>
         </ref>
         <ref id="d2661e537a1310">
            <label>12</label>
            <mixed-citation id="d2661e544" publication-type="other">
Otieno P, Brown E, Mbori-Ngacha D, et al. HIV-1 disease progression in
breast-feeding and formula-feeding mothers: a prospective 2-year com-
parison of T cell subsets, HIV-1 RNA levels, and mortality. J Infect Dis
2007; 195:220-9.</mixed-citation>
         </ref>
         <ref id="d2661e560a1310">
            <label>13</label>
            <mixed-citation id="d2661e567" publication-type="other">
Walson JL, Brown ER, Otieno PA, et al. Morbidity among HIV-1-
infected mothers in Kenya: prevalence and correlates of illness during
2-year postpartum follow-up. J Acquir Immune Defic Syndr 2007; 46:
208-15.</mixed-citation>
         </ref>
         <ref id="d2661e583a1310">
            <label>14</label>
            <mixed-citation id="d2661e590" publication-type="other">
Shaffer N, Chuachoowong R, Mock PA, et al. Short-course zidovudine
for perinatal HIV-1 transmission in Bangkok, Thailand: a randomised
controlled trial. Bangkok Collaborative Perinatal HIV Transmission
Study Group. Lancet 1999; 353:773-80.</mixed-citation>
         </ref>
         <ref id="d2661e606a1310">
            <label>15</label>
            <mixed-citation id="d2661e613" publication-type="other">
Kiarie JN, Richardson BA, Mbori-Ngacha D, Nduati RW, John-Stewart
GC. Infant feeding practices of women in a perinatal HIV-1 prevention
study in Nairobi, Kenya. J Acquir Immune Defic Syndr 2004; 35:75-81.</mixed-citation>
         </ref>
         <ref id="d2661e626a1310">
            <label>16</label>
            <mixed-citation id="d2661e633" publication-type="other">
Emery S, Bodrug S, Richardson BA, et al. Evaluation of performance of
the Gen-Probe human immunodeficiency virus type 1 viral load assay
using primary subtype A, C, and D isolates from Kenya. J Clin Microbiol
2000;38:2688-95.</mixed-citation>
         </ref>
         <ref id="d2661e650a1310">
            <label>17</label>
            <mixed-citation id="d2661e657" publication-type="other">
World Health Organization. Antiretroviral therapy for HIV infection in
adults and adolescents: recommendations for a public health approach,
2006 revision. Available at: http://www.who.int/hiv/pub/guidelines/
artadultguidelines.pdf. Accessed 21 November 2007.</mixed-citation>
         </ref>
         <ref id="d2661e673a1310">
            <label>18</label>
            <mixed-citation id="d2661e680" publication-type="other">
Department of Health and Human Services Panel on Antiretroviral Guidelines
for Adults and Adolescents. Guidelines for the use of antiretroviral agents in
HIV-1-infected adults and adolescents. 3 November 2008. Available at http://
www.aidsinfo.nih.gov/ContentFiles/AdultandAdolescentGLpdf. Accessed 22
September 2008.</mixed-citation>
         </ref>
         <ref id="d2661e699a1310">
            <label>19</label>
            <mixed-citation id="d2661e706" publication-type="other">
Centers for Disease Control and Prevention. 1993 revised classification
system for HIV infection and expanded surveillance case definition for
AIDS among adolescents and adults. MMWR Recomm Rep 1992; 41:1-
19.</mixed-citation>
         </ref>
         <ref id="d2661e722a1310">
            <label>20</label>
            <mixed-citation id="d2661e729" publication-type="other">
Dybul M, Fauci AS, Bartlett JG, Kaplan JE, Pau AK. Guidelines for
using antiretroviral agents among HIV-infected adults and adoles-
cents: the panel on clinical practices for treatment of HIV. Ann In-
tern Med 2002; 137:381-433.</mixed-citation>
         </ref>
         <ref id="d2661e745a1310">
            <label>21</label>
            <mixed-citation id="d2661e752" publication-type="other">
Panel on Clinical Practices for Treatment of HIV Infection. Guide-
lines for the use of antiretroviral agents in HIV-1-infected adults and
adolescents. 29 October 2004. Available at: http://aidsinfo.nih.gov/
ContentFiles/AdultandAdolescentGL10292004002.pdf. Accessed 14
October 2008.</mixed-citation>
         </ref>
         <ref id="d2661e771a1310">
            <label>22</label>
            <mixed-citation id="d2661e778" publication-type="other">
Centers for Disease Control and Prevention. Recommendations to pre-
vent and control iron deficiency in the United States. MMWR Recomm
Rep 1998; 47:1-29</mixed-citation>
         </ref>
         <ref id="d2661e792a1310">
            <label>23</label>
            <mixed-citation id="d2661e799" publication-type="other">
Heagerty PJ, Lumley T, Pepe MS. Time-dependent ROC curves for cen-
sored survival data and a diagnostic marker. Biometrics 2000; 56:337-
44.</mixed-citation>
         </ref>
         <ref id="d2661e812a1310">
            <label>24</label>
            <mixed-citation id="d2661e819" publication-type="other">
Mellors JW, Munoz A, Giorgi JV, et al. Plasma viral load and CD4⁺
lymphocytes as prognostic markers of HIV-1 infection. Ann Intern Med
1997;126:946-54.</mixed-citation>
         </ref>
         <ref id="d2661e832a1310">
            <label>25</label>
            <mixed-citation id="d2661e839" publication-type="other">
Rodriguez B, Sethi AK, Cheruvu VK, et al. Predictive value of plasma
HIV RNA level on rate of CD4 T-cell decline in untreated HIV infection.
JAMA 2006;296:1498-1506.</mixed-citation>
         </ref>
         <ref id="d2661e852a1310">
            <label>26</label>
            <mixed-citation id="d2661e859" publication-type="other">
Mellors JW, Margolick JB, Phair JP, et al. Prognostic value of HIV-1
RNA, CD4 cell count, and CD4 cell count slope for progression to AIDS
and death in untreated HIV-1 infection. JAMA 2007; 297:2349-50.</mixed-citation>
         </ref>
         <ref id="d2661e872a1310">
            <label>27</label>
            <mixed-citation id="d2661e879" publication-type="other">
Lepri AC, Katzenstein TL, Ullum H, et al. The relative prognostic value
of plasma HIV RNA levels and CD4 lymphocyte counts in advanced
HIV infection. AIDS 1998; 12:1639-43.</mixed-citation>
         </ref>
         <ref id="d2661e892a1310">
            <label>28</label>
            <mixed-citation id="d2661e899" publication-type="other">
Anastos K, Kalish LA, Hessol N, et al. The relative value of CD4 cell
count and quantitative HIV-1 RNA in predicting survival in HIV-1-
infected women: results of the women's interagency HIV study. AIDS
1999;13:1717-26.</mixed-citation>
         </ref>
         <ref id="d2661e916a1310">
            <label>29</label>
            <mixed-citation id="d2661e923" publication-type="other">
Zachariah R, Teck R, Ascurra O, Humblet P, Harries AD. Targeting CD4
testing to a clinical subgroup of patients could limit unnecessary CD4
measurements, premature antiretroviral treatment and costs in Thyolo
District, Malawi. Trans R Soc Trop Med Hyg 2006;100:24-31.</mixed-citation>
         </ref>
         <ref id="d2661e939a1310">
            <label>30</label>
            <mixed-citation id="d2661e946" publication-type="other">
Pepe MS, Janes H, Longton G, Leisenring W, Newcomb P. Limitations
of the odds ratio in gauging the performance of a diagnostic, prognostic,
or screening marker. Am J Epidemiol 2004; 159:882-90.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
