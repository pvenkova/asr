<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">globenvi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50016234</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Global Environment</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>XL Edizioni</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19733739</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">20537352</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43201493</article-id>
         <article-categories>
            <subj-group>
               <subject>Around the world</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ecological and Poverty Impacts of Zimbabwe's Land Struggles: 1980 to Present</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Vimbai C.</given-names>
                  <surname>Kwashirai</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40125480</issue-id>
         <fpage>222</fpage>
         <lpage>253</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43201493"/>
         <abstract>
            <p>Literature on Zimbabwe's land struggles is dominated by a particular perspective emphasizing the historical rooting of inequitable land distribution and ownership in British colonial rule from 1890 onward. This view is informed by the imperative of redressing a historical injustice wherever British people alienated prime land from, among others, the indigenous Shona, Ndebele and Tonga. The key element in this perspective has been the science of land management, and particularly the protection of wooded areas, the soil and wildlife. The discourse on ecological calamity stresses the harmful consequences of unregulated agriculture, mining and hunting, the threats posed by degradation, and the need to control methods of resource exploitation. This study examines the debates on, and processes of; land reform in Zimbabwe in the independence era (1980-present), exploring the social, economic and political contexts of perceptions of land redistribution and management. Zimbabwe's major land reform programme was carried out between 2000 and 2002. An estimated 300,000 small-holder farmers were provided with land holdings ranging between 5 and 10 hectares. In addition, land was set aside for 54,000 black commercial farmers. Of the original 5,000 white farmers in 2000, only 600 were estimated to have remained in full production on their farms. Much of this period has been characterised by both local and global debates about land reform and environmental problems, generating in their wake politically charged and emotive argumentations about issues such as poverty, deforestation, soil erosion and threats to wildlife.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1131e235a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1131e242" publication-type="other">
Government of Zimbabwe, The Transitional National Development Plan,
Volume 1, 1983, pp. 66-69;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e251" publication-type="other">
Onesmo Zishiri, Director of Agriculture and Land
Resettlement, interview with author, 23-25 June 2003, Chinhoyi, Mashonaland
West Region.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e264a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1131e271" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e278a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1131e285" publication-type="other">
G. Kanyenze, "The Performance of the Zimbabwean Economy, 1980-2000",
in Twenty Years of Independence in Zimbabwe From Liberation to Authoritarianism,
S. Darnolf and L. Laasko (eds), Palgrave, Hampshire 2003, pp. 42-45.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e298a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1131e307" publication-type="other">
Mike Auret, Director, Catholic Commission for Justice and Peace, interview
with author, 10-13 February 2001, Chinhoyi;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e316" publication-type="other">
Bishop Helmot Rector, interview
with author, 10-13 February 2001, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e327a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1131e334" publication-type="other">
Governor Ignatius Chombo, interview with author, 16-17 February 2001,
Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e344a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1131e351" publication-type="other">
Onesmo Zishiri, interview with author, 23-25 June, 2003, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e358a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1131e365" publication-type="other">
Riddell Commission, Commission of Inquiry into Incomes, Prices and Con-
ditions of Service, Harare 1981, pp. 107-110.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e375a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1131e382" publication-type="other">
D. Hulme, A. Shepherd, "Conceptualizing chronic poverty", in World Deve-
lopment, 31,3, 2003, pp. 404-405.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e392a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1131e399" publication-type="other">
Chief James Dandahwa, interview with author, 9-10 May 2003, Hurungwe
District, Mashonaland West.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e409a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1131e416" publication-type="other">
Foster Murakwani, Manager Forestry Commission, interview with author,
26 April 2001, Chinhoyi;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e425" publication-type="other">
Peter Mabhande, War Veteran Chairman, interview
with author, 30 April 2001, Chinhoyi;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e434" publication-type="other">
Grace Chauke, worker, interview with
author, 30 April 2001, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e445a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1131e452" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e459a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1131e466" publication-type="other">
Felicia Munjaidi, Tourism Officer, interview with author, 20 October 2001,
Harare. Tourist arrivals to Zimbabwe fell by 25 per cent in 2001-2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e476a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1131e483" publication-type="other">
The Financial Gazette, 25 November 2006;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e489" publication-type="other">
The Independent, 30 November
2006;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e498" publication-type="other">
The Standard, 5 December 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e505a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1131e512" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e519a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1131e526" publication-type="other">
Peter Mabhande, War Veteran Chairman, interview with author, 30 April
2001;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e535" publication-type="other">
The Financial Gazette, 25 November 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e542a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1131e549" publication-type="other">
Onesmo Zishiri, Director of Agriculture and Land Resettlement, interview
with author, 20-21 September 2004, Chinhoyi, Mashonaland West Region;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e558" publication-type="other">
The
Financial Gazette, 25 November 2004;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e567" publication-type="other">
The Independent, 30 November 2004;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e574" publication-type="other">
The
Standards 5 December 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e585a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1131e592" publication-type="other">
Masipula Sithole, Zimbabwe s Public Eyey Political Essays, Zimbabwe Publis-
hing House, Harare 1998, p. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e602a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1131e609" publication-type="other">
The Financial Gazette, 3 August 2005;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e615" publication-type="other">
The Independent, 23 August 2005;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e621" publication-type="other">
The Standard, 25 August 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e628a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1131e635" publication-type="other">
Forestry Commission Annual Report, December 2000, p. 5;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e641" publication-type="other">
Foster Murakwa-
ni, Manager, Forestry Commission, interview with author, 26 April 2001, Chin-
hoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e654a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1131e661" publication-type="other">
Chief James Dandahwa, interview with author, 9-10 May 2003, Hurungwe
District, Mashonaland West.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e671a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1131e678" publication-type="other">
Forestry Commission Annual Report, December 2004, p. 5;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e684" publication-type="other">
Foster Murakwa-
ni, Manager, Forestry Commission, interview with author, 26 April 2001, Chin-
hoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e697a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1131e704" publication-type="other">
Mutizwa Munyimi, Village Head, interview with author, 10 March 2003,
Makonde Business Centre.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e715a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1131e722" publication-type="other">
Jorum Moyo, Chief Executive Officer, interview with author, 22 March
2003, Magunie Growth Point.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e732a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1131e739" publication-type="other">
The Chronicle, 17 April 2003;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e745" publication-type="other">
The Herald, 4 May 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e752a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1131e759" publication-type="other">
Business Chronicle, 12 March 2003;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e765" publication-type="other">
Business Herald, 12 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e772a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1131e779" publication-type="other">
Foster Murakwani, Manager, Forestry Commission, interview with author,
26 April 2001, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e789a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1131e796" publication-type="other">
Peter Mazeze, Agricultural Extension Service Officer, interview with author,
29 April 2001, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e806a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1131e813" publication-type="other">
Foster Murakwani, Manager Forestry Commission, interview with author,
26 April 2001, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e824a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1131e833" publication-type="other">
Joseph Tasosa, Director of the Zimbabwe National Environment Trust, inter-
view with author, 3 March 2003, Harare;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e842" publication-type="other">
Sábele Moyo, Clever Mangami and Tsitsi
Matope, Forestry Officers, interview with author, 10 March 2003, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e852a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1131e859" publication-type="other">
Joseph Tasosa, Director ot the Zimbabwe National bnvironment Irust, in-
terview with author, 3 March 2003, Harare;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e868" publication-type="other">
The Herald, 23 April 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e875a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1131e882" publication-type="other">
Sábele Moyo, Clever Mangami and Tsitsi Matope, Forestry Officers, inter-
view with author, 10 March 2003, Chinhoyi;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e891" publication-type="other">
The Herald, 26 April 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e898a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1131e905" publication-type="other">
Ellen Chaumba, gold panner, interview with author, 19 January 2008, Si-
yakobvu.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e915a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1131e922" publication-type="other">
Kiyasi Sibanda, gold trader, interview with author, 20 December 2007.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e929a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1131e936" publication-type="other">
Sabele Moyo, Clever Mangami and Tsitsi Matope, Forestry Officers, inter-
view with author, 10 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e947a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1131e954" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e961a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1131e968" publication-type="other">
Sábele Moyo, Clever Mangami and Tsitsi Matope, Forestry Officers, inter-
view with author, 10 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e978a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1131e985" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e992a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1131e999" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e1006a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1131e1013" publication-type="other">
Zimbabwe Independent, 7 May 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e1020a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1131e1027" publication-type="other">
Onesmo Zishiri, interview with author, 23-25 June 2003, Chinhoyi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e1035a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1131e1042" publication-type="other">
Business Herald, 4 April 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e1049a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1131e1056" publication-type="other">
The Chronicle, 6 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e1063a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1131e1070" publication-type="other">
The Chronicle, 6 March 2003;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e1076" publication-type="other">
The Chronicle, 8 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d1131e1083a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1131e1090" publication-type="other">
The Chronicle, 10 March 2003;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1131e1096" publication-type="other">
The Chronicle, 11 March 2003.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
