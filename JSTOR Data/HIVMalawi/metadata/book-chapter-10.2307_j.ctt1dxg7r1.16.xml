<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt13kh3fb</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1dxg7r1</book-id>
      <subj-group>
         <subject content-type="call-number">RC607.A26U58 1993</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">AIDS (Disease)</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Women</subject>
         <subj-group>
            <subject content-type="lcsh">Medical care</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">HIV Infections</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Women’s Health</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Women’s Health Services</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Until the Cure</book-title>
         <subtitle>Caring for Women with HIV</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>Kurth</surname>
               <given-names>Ann</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib2">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Mann</surname>
               <given-names>Jonathan</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>24</day>
         <month>11</month>
         <year>1993</year>
      </pub-date>
      <isbn content-type="ppub">9780300058062</isbn>
      <isbn content-type="epub">9780300162592</isbn>
      <publisher>
         <publisher-name>Yale University Press</publisher-name>
      </publisher>
      <permissions>
         <copyright-year>1993</copyright-year>
         <copyright-holder>Yale University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1dxg7r1"/>
      <abstract abstract-type="short">
         <p>Although the AIDS epidemic has generated worldwide concern, very little attention has been paid to its impact on the increasing numbers of women who have been infected. Women with HIV disease are in many ways a unique group: their clinical symptoms can differ from those of men, and because they are the ones who bear and usually care for children, they have different psychosocial concerns and needs. This book-written by experts in the fields of law, medicine, nursing, public health, social work, ethics, and psychiatry, and enriched by personal accounts from women who have been living with the disease-is an essential guide to the medical and social treatment of women with HIV.The book begins by discussing clinical care for women with HIV, providing information on how the disease affects women and what type of gynecological treatment, reproductive counseling, obstetrical management, and neuropsychiatric considerations are important. Authors examine why women have been excluded from research trials of new therapies and argue that women must be included in future trials. One chapter explores ethical issues, such as the reproductive rights of women with HIV, and another is devoted to legal conflicts surrounding such issues as discrimination, government benefits, and custody rights. The final third of the book deals with ways to deliver support services to HIV-positive women and their families and describes a family-centered, community-based, comprehensive care model. The book concludes with suggestions for programs and policies that will lessen the incidence of HIV.</p>
      </abstract>
      <counts>
         <page-count count="348"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <fpage>xi</fpage>
                  <abstract>
                     <p>AIDS requires us to reconsider our basic assumptions and to question received wisdom in the form of convenient categorizations. One such simplification treats health and human rights as distinct and separate—and often conflicting—concerns; another divides the world into academic thinkers and frontline workers. This book courageously challenges the validity or utility of both oversimplifications.</p>
                     <p>AIDS is the first pandemic of its kind to occur during the post-World War II era of human rights. Accordingly, health officials have had a dual responsibility in confronting AIDS: they must protect public health and respect human rights. This dual role initially was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction:</title>
                     <subtitle>An Overview of Women and HIV Disease</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kurth</surname>
                           <given-names>Ann</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>A virus spread through sexual intercourse and blood will affect women as well as men. But this biological truism, applied to the human immunodeficiency virus (HIV), has been appreciated slowly, and perception of the impact of HIV disease in women has not kept pace with reality.¹ The first cases of what we now know as acquired immunodeficiency syndrome (AIDS)—but which was then being called gay-related immune disorder—were reported in the United States in 1981.² Within months cases in women were reported (Masur et al. 1982; Ellerbrock and Rogers 1990).</p>
                     <p>By the end of the following year, women constituted</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A Primer of Health Care</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Young</surname>
                           <given-names>Mary</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>Although a great deal is known about the management of HIV disease in children and men, relatively little has been published regarding the care and treatment of HIV-infected women. Nevertheless, a consensus is emerging, one that is outlined here, with an understanding that HIV treatment is constantly changing. Comprehensive management, particularly in the later stages of the illness, requires interdisciplinary care and often subspecialty medical consultation. It is important for nonspecialist providers—family-and general-practice physicians, internists, nurse-practitioners, and others—to realize that women and men with HIV disease can benefit from a comprehensive primary-care approach.</p>
                     <p>Who should take the time</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Gynecological Considerations in the Primary-Care Setting</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Denenberg</surname>
                           <given-names>Risa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>Biological differences between women and men are important to health care management. For example, women have a greater distribution of body fat and a unique and complex cyclic hormonal system. They are subject to such organ-specific diseases and conditions as pelvic infections, cervical cancer, and vaginal thrush. They also suffer a higher incidence of certain diseases than men do, including simple urinary tract infections, breast cancer, and human papilloma virus (HPV) infections. Women also experience more complications of such common disorders as gonorrhea and chlamydia. Many of these concerns fall into the general category of gynecology.</p>
                     <p>Recognition of the early</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Reproductive Health and Counseling</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hutchison</surname>
                           <given-names>Margaret</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shannon</surname>
                           <given-names>Maureen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>47</fpage>
                  <abstract>
                     <p>Decisions on whether to become pregnant or to continue a pregnancy involve complex personal issues for all women, particularly in this time of besieged reproductive freedom. The decision-making process is confounded by HIV-positivity, which superimposes the reality of a chronic infectious disease that will considerably diminish the quality and length of most women’s lives.</p>
                     <p>Contraceptive compliance and reproductive decision making are shaped by a multitude of factors—variables that are as important to women with HIV as to those who are uninfected. But for an HIV-positive women, selection of a contraceptive method is influenced by the method’s efficacy in preventing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Obstetrical Management</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mitchell</surname>
                           <given-names>Janet L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fennoy</surname>
                           <given-names>Ilene</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tucker</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lottman</surname>
                           <given-names>Patricia O.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib5" xlink:type="simple">
                        <name name-style="western">
                           <surname>Williams</surname>
                           <given-names>Sterling B.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>66</fpage>
                  <abstract>
                     <p>Nearly 85 percent of all reported cases of AIDS in women occur in those of reproductive age, fifteen to forty-four (CDC 1993). Most infected women in this age group are not yet aware of their infection, and perinatally acquired HIV continues to account for about 1.5 percent of all AIDS cases. For this reason the Centers for Disease Control and Prevention (1985) published recommendations on preventing perinatal transmission of HIV; those recommendations were then endorsed by the American College of Obstetrics and Gynecology (ACOG, 1987). The CDC advised that all women of reproductive age be offered HIV counseling with voluntary</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Neuropsychiatric Aspects of Infection</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sanders</surname>
                           <given-names>Kathy M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>81</fpage>
                  <abstract>
                     <p>Along with the emotional distress of receiving a diagnosis of HIV infection comes significant neuropsychiatric morbidity. Caregivers who treat women with HIV disease or women who are at high risk for HIV infection need to be educated on the wide range of neurologic and psychiatric complications and prepared to treat and support patients emotionally.</p>
                     <p>At the time of AIDS diagnosis, 40 percent of patients will have signs of central nervous system (CNS) involvement: primary HIV infection of the brain, spinal cord, or peripheral nerves; opportunistic infections involving the CNS; and primary CNS lymphoma. Psychological complications in the course of diagnosis</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Trends in Federally Sponsored Clinical Trials</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Korvick</surname>
                           <given-names>Joyce A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>Clinical trials of HIV therapeutics in the United States are conducted by pharmaceutical companies; federally sponsored multicenter networks; and such nonprofit organizations as the American Foundation for AIDS Research. Most federally sponsored clinical trials are funded and directed through the National Institutes of Allergy and Infectious Diseases of the National Institutes of Health (NIAID/NIH). Two multicenter networks-the AIDS Clinical Trial Group (ACTG), initiated in 1986, and the Terry Beim Community Programs on Clinical Research on AIDS (CPCRA), established in 1989-are sponsored by the Division of AIDS of NIAID. I focus here on these two programs, analyzing how they address the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>A Community Advocate's View of Clinical Research</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Long</surname>
                           <given-names>Iris L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>104</fpage>
                  <abstract>
                     <p>A clinical trial may offer a woman new treatment and medical-care options. But a woman who considers participating in an HIV / AIDS clinical trial faces many obstacles—some of them historical and many related to HIV disease.</p>
                     <p>The shortcomings of clinical research on the effects of drugs on women are not limited to BIV illness. A review of drugs used to treat high blood pressure, for example, shows the extent to which clinical research fails to discover the effects of drugs on women. In 1991 a group of female doctors looked at the data from clinical trials of a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Ethical Issues</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Levine</surname>
                           <given-names>Carol</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>Using a simple word association test or a sophisticated literature search, what topics link<italic>ethics, women,</italic>and<italic>HN/AIDS?</italic>Chances are, either method will result in a cluster of issues surrounding HIV counseling and testing, vertical transmission and reproductive rights, and participation in clinical research. The existing literature has focused almost exclusively on ethical choices framed by policy analysts, ethicists, and health care providers<italic>about</italic>HIV-positive women and women at risk of infection, not on ethical dilemmas as the women perceive them. Furthermore, discussions about women and ethics typically view women in isolation from others (except unborn or unconceived fetuses). Because</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Legal Considerations</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zavos</surname>
                           <given-names>Michele A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>Women with HIV disease confront many of the same legal issues faced by other HIV-infected individuals. Men and women both may have problems gaining access to basic health care, maintaining health insurance, obtaining government benefits, and receiving treatment and prophylactic care. They also may encounter discrimination in employment, housing, education, and other areas. Moreover, people with HIV disease, like everyone with a potentially catastrophic illness, need a will, power of attorney, and health care directives to maintain control over their medical decisions and other aspects of their lives.</p>
                     <p>But HIV-positive women also encounter a multitude of legal problems exacerbated or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Epidemiology and Natural History</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Anastos</surname>
                           <given-names>Kathryn</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Vermund</surname>
                           <given-names>Sten H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>144</fpage>
                  <abstract>
                     <p>In mid-1981 the first published reports of a newly recognized acquired immune deficiency syndrome described a disease occurring only in men (CDC 1981; Gottlieb et al. 1981; Masur et al. 1981 ). In early 1982 women in New York City hospitals were diagnosed with the same syndrome (Masur et al. 1982), and by 1986 the officially defined acquired immunodeficiency syndrome was the leading cause of death for New York City women aged twenty-five to forty-four (New York City Department of Health 1989). By 1987 mortality statistics revealed that New Jersey women in the same age group were also more likely</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>The Developing World:</title>
                     <subtitle>Caring for the Caregivers</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Carovano</surname>
                           <given-names>Kathryn</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Schietinger</surname>
                           <given-names>Helen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>165</fpage>
                  <abstract>
                     <p>Everywhere in the world women are suffering and dying from AIDS and bearing witness to death in their families and communities. Throughout the developing and developed world women’s risk of acquiring HIV is increasing. For most women the primary risk of infection comes from their—and their partner’s—sex lives. But the women who are at greatest risk are so mostly because they are women: their risk is the product both of biological factors and of the social context in which they live. Throughout the developing world and in underdeveloped parts of the so-called developed world, the context keeps women</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Working with Communities of Women at Risk—A Chronicle</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gasch</surname>
                           <given-names>Helen</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fullilove</surname>
                           <given-names>Mindy Thompson</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>183</fpage>
                  <abstract>
                     <p>There is a poster that reads, “We all have AIDS.”</p>
                     <p>The bodies of our leaders, friends, and sisters have become home to HIV. Unable to reach into their cells and strip away viral particles, we watch and grieve and daily learn to rejoice in minutes we once took for granted.</p>
                     <p>We—women of color who have been working for years in the fight against AIDS—have AIDS in our world. Our dreams of escaping AIDS are dreams of reinventing the world without this plague—the tragic epidemic is our reality—that marks the contours of our lives. We must become</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Caregiving:</title>
                     <subtitle>A Matriarchal Tradition Continues</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Simpson</surname>
                           <given-names>B. Joyce</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Williams</surname>
                           <given-names>Ann</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>200</fpage>
                  <abstract>
                     <p>New and improved treatments have changed the natural history of HIV infection and AIDS from one of episodic, serious illness requiring tertiary care to chronic illness that frequently can be managed in an outpatient setting. This change has transferred much of the responsibility for patient care from the hospital to the family. In populations affected by HIV, home care is provided predominantly by women. And because AIDS disproportionately affects persons of color, most of these caregivers are minority women.</p>
                     <p>The caregivers we describe in this chapter are found in families that are affected by HIV infection primarily attributable to injection-drug</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.19</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Optimizing the Delivery of Health and Social Services:</title>
                     <subtitle>Case Study of a Model</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sunderland</surname>
                           <given-names>Ann</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Holman</surname>
                           <given-names>Susan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>212</fpage>
                  <abstract>
                     <p>When discussing women with HIV, one is all too often talking about a family with this disease, as partners and offspring also may be infected. One cannot simply address the needs of a woman alone but must also address those of her family (biological family and family of choice). Often, members of the extended family, such as an aunt or uncle, are also infected. The family itself can vary from the traditional nuclear unit to more complex families with children from different relationships and many generations to families separated by immigration or incarceration. Each woman’s situation and needs are unique</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.20</book-part-id>
                  <title-group>
                     <label>16</label>
                     <title>Programs and Policies for Prevention</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wagner</surname>
                           <given-names>Krystn</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Cohen</surname>
                           <given-names>Judith</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>228</fpage>
                  <abstract>
                     <p>Even under the best of circumstances it will likely be several years before an effective and affordable vaccine or medical cure for HIV disease is widely available. Until then, prevention of HIV transmission is the only available means of limiting the further spread of the pandemic. Programs and policies for the prevention of HIV and AIDS will be needed even after the arrival of an AIDS vaccine, particularly if early vaccines are not completely effective. It is, therefore, essential that sufficient resources and attention be devoted to the development of effective prevention programs and messages at the same time that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.21</book-part-id>
                  <title-group>
                     <title>Appendix:</title>
                     <subtitle>Resources: Service Organizations, Materials, and Further Reading</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kurth</surname>
                           <given-names>Ann</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wittig</surname>
                           <given-names>Laura J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.22</book-part-id>
                  <title-group>
                     <title>List of References</title>
                  </title-group>
                  <fpage>277</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.23</book-part-id>
                  <title-group>
                     <title>List of Contributors</title>
                  </title-group>
                  <fpage>315</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.24</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>319</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1dxg7r1.25</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>331</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
