<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">demography</journal-id>
         <journal-id journal-id-type="jstor">j100446</journal-id>
         <journal-title-group>
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Population Association of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00703370</issn>
         <issn pub-type="epub">15337790</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4137241</article-id>
         <title-group>
            <article-title>The Impact of Parental Death on School Outcomes: Longitudinal Evidence from South Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Anne</given-names>
                  <surname>Case</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Cally Ardington</string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">43</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i388644</issue-id>
         <fpage>401</fpage>
         <lpage>420</lpage>
         <page-range>401-420</page-range>
         <permissions>
            <copyright-statement/>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4137241"/>
         <abstract>
            <p>We analyze longitudinal data from a demographic surveillance area (DSA) in KwaZulu-Natal to examine the impact of parental death on children 's outcomes. The results show significant differences in the impact of mothers' and fathers' deaths. The loss of a child's mother is a strong predictor of poor schooling outcomes. Maternal orphans are significantly less likely to be enrolled in school and have completed significantly fewer years of schooling, conditional on age, than children whose mothers are alive. Less money is spent on maternal orphans' educations, on average, conditional on enrollment. Moreover, children whose mothers have died appear to be at an educational disadvantage when compared with non-orphaned children with whom they live. We use the timing of mothers' deaths relative to children's educational shortfalls to argue that mothers' deaths have a causal effect on children's educations. The loss of a child's father is a significant correlate of poor household socioeconomic status. However, the death of a father between waves of the survey has no significant effect on subsequent asset ownership. Evidence from the South African 2001 Census suggests that the estimated effects of maternal deaths on children's outcomes in the Africa Centre DSA reflect the reality for orphans throughout South Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d170e218a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d170e225" publication-type="other">
Ainsworth, Beegle, and Koda (2005),</mixed-citation>
            </p>
         </fn>
         <fn id="d170e232a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d170e239" publication-type="other">
www.statssa.gov.za.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d170e255a1310">
            <mixed-citation id="d170e259" publication-type="journal">
Ainsworth, M., K. Beegle, and G. Koda. 2005. "The Impact of Adult Mortality and Parental Deaths
on Primary Schooling in Northwestern Tanzania." Journal of Development Studies 41:412-39.<person-group>
                  <string-name>
                     <surname>Ainsworth</surname>
                  </string-name>
               </person-group>
               <fpage>412</fpage>
               <volume>41</volume>
               <source>Journal of Development Studies</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d170e291a1310">
            <mixed-citation id="d170e295" publication-type="journal">
Bicego, G., S. Rutstein, and K. Johnson. 2003. "Dimensions of the Emerging Orphan Crisis in Sub-
Saharan Africa." Social Science and Medicine 56:1235-47.<person-group>
                  <string-name>
                     <surname>Bicego</surname>
                  </string-name>
               </person-group>
               <fpage>1235</fpage>
               <volume>56</volume>
               <source>Social Science and Medicine</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d170e327a1310">
            <mixed-citation id="d170e331" publication-type="book">
Case, A. and C. Ardington. 2004. "Socioeconomic Factors." Chap. 8 in Demographic and Health
Events, Monograph Series, Africa Centre for Health and Population Studies.<person-group>
                  <string-name>
                     <surname>Case</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Socioeconomic Factors</comment>
               <source>Demographic and Health Events</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d170e360a1310">
            <mixed-citation id="d170e364" publication-type="journal">
Case, A. and A. Deaton. 1999. "School Inputs and Educational Outcomes in South Africa." Quarterly
Journal of Economics 114:1047-84.<object-id pub-id-type="jstor">10.2307/2586891</object-id>
               <fpage>1047</fpage>
            </mixed-citation>
         </ref>
         <ref id="d170e381a1310">
            <mixed-citation id="d170e385" publication-type="journal">
Case, A., C. Paxson, and J. Ableidinger. 2004. "Orphans in Africa: Parental Death, Poverty, and
School Enrollment." Demography 41:483-508.<object-id pub-id-type="jstor">10.2307/1515189</object-id>
               <fpage>483</fpage>
            </mixed-citation>
         </ref>
         <ref id="d170e401a1310">
            <mixed-citation id="d170e405" publication-type="journal">
Duflo, E. 2003. "Grandmothers and Granddaughters: Old-Age Pensions and Intrahousehold Alloca-
tion in South Africa." World Bank Economic Review 17:1-25.<person-group>
                  <string-name>
                     <surname>Duflo</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>17</volume>
               <source>World Bank Economic Review</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d170e437a1310">
            <mixed-citation id="d170e441" publication-type="book">
Evans, D. and E. Miguel. 2004. "Orphans and Schooling in Africa: A Longitudinal Analysis." Unpub-
lished manuscript. Harvard University.<person-group>
                  <string-name>
                     <surname>Evans</surname>
                  </string-name>
               </person-group>
               <source>Orphans and Schooling in Africa: A Longitudinal Analysis</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d170e466a1310">
            <mixed-citation id="d170e470" publication-type="book">
Gertler, P., S. Martinez, D. Levine, and S. Bertozzi. 2003. "Losing the Presence and Presents of Par-
ents: How Parental Death and Disability Affects Children." Unpublished manuscript. Haas School
of Business, University of California at Berkeley.<person-group>
                  <string-name>
                     <surname>Gertler</surname>
                  </string-name>
               </person-group>
               <source>Losing the Presence and Presents of Parents: How Parental Death and Disability Affects Children</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d170e499a1310">
            <mixed-citation id="d170e503" publication-type="book">
Giese, S., H. Meintjes, R. Croke, and R. Chamberlain. 2003. "Health and Social Services to Address
the Needs of Orphans and Other Vulnerable Children in the Context of HIV/AIDS, Research Re-
port and Recommendations." Children's Institute of the University of Cape Town.<person-group>
                  <string-name>
                     <surname>Giese</surname>
                  </string-name>
               </person-group>
               <source>Health and Social Services to Address the Needs of Orphans and Other Vulnerable Children in the Context of HIV/AIDS, Research Report and Recommendations</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d170e532a1310">
            <mixed-citation id="d170e536" publication-type="book">
Hosegood, V. and I.M. Timæus. 2005. "Household Composition and Dynamics in KwaZulu Natal,
South Africa: Mirroring Social Reality in Longitudinal Data Collection." Pp. 58-77 in African
Households: Censuses and Surveys, edited by E. Van de Walle. Armonk, NY: M.E. Sharpe.<person-group>
                  <string-name>
                     <surname>Hosegood</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Household Composition and Dynamics in KwaZulu Natal, South Africa: Mirroring Social Reality in Longitudinal Data Collection</comment>
               <fpage>58</fpage>
               <source>African Households: Censuses and Surveys</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d170e572a1310">
            <mixed-citation id="d170e576" publication-type="book">
Meintjes, H., D. Budlender, S. Giese, and L. Johnson. 2003. "Children in 'Need of Care' or in Need
of Cash? Questioning Social Security Provisions for Orphans in the Context of the South African
AIDS Pandemic." A Joint Working Paper of the Children's Institute and the Centre for Actuarial
Research, University of Cape Town.<person-group>
                  <string-name>
                     <surname>Meintjes</surname>
                  </string-name>
               </person-group>
               <source>Children in 'Need of Care' or in Need of Cash? Questioning Social Security Provisions for Orphans in the Context of the South African AIDS Pandemic</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d170e608a1310">
            <mixed-citation id="d170e612" publication-type="book">
Subbarao, K. and D. Coury. 2004. Reaching Out to Africa's Orphans, A Framework for Public Action.
Washington DC: World Bank.<person-group>
                  <string-name>
                     <surname>Subbarao</surname>
                  </string-name>
               </person-group>
               <source>Reaching Out to Africa's Orphans, A Framework for Public Action</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d170e637a1310">
            <mixed-citation id="d170e641" publication-type="other">
UNAIDS. 2002. "Report on the Global HIV/AIDS Epidemic." Available on-line at www.unaids.
org/barcelona/presskit/report.html</mixed-citation>
         </ref>
         <ref id="d170e651a1310">
            <mixed-citation id="d170e655" publication-type="other">
UNAIDS/UNICEF/USAID. 2004. "Children on the Brink 2004, A Joint Report of New Orphan Esti-
mates and a Framework for Action." Available on-line at www.unaids.org</mixed-citation>
         </ref>
         <ref id="d170e665a1310">
            <mixed-citation id="d170e669" publication-type="book">
World Bank. 2002. Education and HIV/AIDS, A Window of Hope. Washington DC: World Bank.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Education and HIV/AIDS, A Window of Hope</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d170e691a1310">
            <mixed-citation id="d170e695" publication-type="journal">
Yamano, T. and T.S. Jayne. 2004. "Measuring the Impact of Working-Age Adult Mortality on Small-
Scale Farm Households in Kenya." World Development 32(1):91-119.<person-group>
                  <string-name>
                     <surname>Yamano</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>91</fpage>
               <volume>32</volume>
               <source>World Development</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
