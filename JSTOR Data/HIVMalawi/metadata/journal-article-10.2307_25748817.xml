<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25748817</article-id>
         <title-group>
            <article-title>Exposing malaria in-host diversity and estimating population diversity by capture-recapture using massively parallel pyrosequencing</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jonathan J.</given-names>
                  <surname>Juliano</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kimberly</given-names>
                  <surname>Porter</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Victor</given-names>
                  <surname>Mwapasa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rithy</given-names>
                  <surname>Sem</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>William O.</given-names>
                  <surname>Rogers</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Frédéric</given-names>
                  <surname>Ariey</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Chansuda</given-names>
                  <surname>Wongsrichanalai</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrew</given-names>
                  <surname>Read</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Steven R.</given-names>
                  <surname>Meshnick</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thomas E.</given-names>
                  <surname>Wellems</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>16</day>
            <month>11</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">107</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25748722</issue-id>
         <fpage>20138</fpage>
         <lpage>20143</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25748817"/>
         <abstract>
            <p>Malaria infections commonly contain multiple genetically distinct variants. Mathematical and animal models suggest that interactions among these variants have a profound impact on the emergence of drug resistance. However, methods currently used for quantifying parasite diversity in individual infections are insensitive to low-abundance variants and are not quantitative for variant population sizes. To more completely describe the in-host complexity and ecology of malaria infections, we used massively parallel pyrosequencing to characterize malaria parasite diversity in the infections of a group of patients. By individually sequencing single strands of DNA in a complex mixture, this technique can quantify uncommon variants in mixed infections. The in-host diversity revealed by this method far exceeded that described by currently recommended genotyping methods, with as many as sixfold more variants per infection. In addition, in paired pre- and posttreatment samples, we show a complex milieu of parasites, including variants likely up-selected and down-selected by drug therapy. As with all surveys of diversity, sampling limitations prevent full discovery and differences in sampling effort can confound comparisons among samples, hosts, and populations. Here, we used ecological approaches of species accumulation curves and capture-recapture to estimate the number of variants we failed to detect in the population, and show that these methods enable comparisons of diversity before and after treatment, as well as between malaria populations. The combination of ecological statistics and massively parallel pyrosequencing provides a powerful tool for studying the evolution of drug resistance and the in-host ecology of malaria infections.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d1257e246a1310">
            <label>1</label>
            <mixed-citation id="d1257e253" publication-type="other">
World Health
                Organization (2008) World Malaria Report 2008 (World Health Orga-
nization,
                Geneva).</mixed-citation>
         </ref>
         <ref id="d1257e263a1310">
            <label>2</label>
            <mixed-citation id="d1257e270" publication-type="other">
Bell AS, de Roode
                JC, Sim D, Read AF (2006) Within-host competition in genetically
diverse malaria
                infections: Parasite virulence and competitive success. Evolution 60:
1358-1371.</mixed-citation>
         </ref>
         <ref id="d1257e283a1310">
            <label>3</label>
            <mixed-citation id="d1257e290" publication-type="other">
Liu S, Mu J,
                Jiang H, Su XZ (2008) Effects of Plasmodium falciparum mixed infections
on in vitro
                antimalarial drug tests and genotyping. Am J Trop Med Hyg 79:178-184.</mixed-citation>
         </ref>
         <ref id="d1257e300a1310">
            <label>4</label>
            <mixed-citation id="d1257e307" publication-type="other">
Mideo N, et al.
                (2008) Understanding and predicting strain-specific patterns of
pathogenesis in
                the rodent malaria Plasmodium chabaudi. Am Nat 172:214-238.</mixed-citation>
         </ref>
         <ref id="d1257e318a1310">
            <label>5</label>
            <mixed-citation id="d1257e325" publication-type="other">
Wargo AR, Huijben
                S, de Roode JC, Shepherd J, Read AF (2007) Competitive release
and facilitation
                of drug-resistant parasites after therapeutic chemotherapy in
a rodent malaria
                model. Proc Natl Acad Sci USA 104:19914-19919.</mixed-citation>
         </ref>
         <ref id="d1257e338a1310">
            <label>6</label>
            <mixed-citation id="d1257e345" publication-type="other">
Hastings IM
                (2003) Malaria control and the evolution of drug resistance: an
                intriguing
link. Trends
                Parasitol 19:70-73.</mixed-citation>
         </ref>
         <ref id="d1257e355a1310">
            <label>7</label>
            <mixed-citation id="d1257e362" publication-type="other">
Hastings IM,
                D'Alessandro U (2000) Modelling a predictable disaster: The rise and
spread of
                drug-resistantmalaria. Parasitol Today 16:340-347.</mixed-citation>
         </ref>
         <ref id="d1257e372a1310">
            <label>8</label>
            <mixed-citation id="d1257e379" publication-type="other">
Huijben S, et al.
                (2010) Chemotherapy, within-host ecology and fitness of drug
resistant malaria
                parasites. Evolution, 10.1111/j.1558-5646.2010.01068.x.</mixed-citation>
         </ref>
         <ref id="d1257e389a1310">
            <label>9</label>
            <mixed-citation id="d1257e396" publication-type="other">
Talisuna AO,
                Okello PE, Erhart A, Coosemans M, D'Alessandro U (2007) Intensity of
malaria
                transmission and the spread of Plasmodium falciparum resistant malaria: A
review of
                epidemiologic field evidence. Am J Trop Med Hyg 77(6 suppl) 170-180.</mixed-citation>
         </ref>
         <ref id="d1257e409a1310">
            <label>10</label>
            <mixed-citation id="d1257e416" publication-type="other">
Juliano JJ, et
                al. (2009) Misclassification of drug failure in Plasmodium falciparum
clinical trials
                in southeast Asia. J Infect Dis 200:624-628.</mixed-citation>
         </ref>
         <ref id="d1257e427a1310">
            <label>11</label>
            <mixed-citation id="d1257e434" publication-type="other">
Greenhouse B, et
                al. (2006) Validation of microsatellite markers for use in genotyping
polyclonal
                Plasmodium falciparum infections. Am J Trop Med Hyg 75:836-842.</mixed-citation>
         </ref>
         <ref id="d1257e444a1310">
            <label>12</label>
            <mixed-citation id="d1257e451" publication-type="other">
Kwiek JJ, et al.
                (2007) Estimating true antimalarial efficacy by heteroduplex tracking
assay in patients
                with complex Plasmodium falciparum infections. Antimicrob Agents
Chemother
                51:521-527.</mixed-citation>
         </ref>
         <ref id="d1257e464a1310">
            <label>13</label>
            <mixed-citation id="d1257e471" publication-type="other">
Juliano JJ,
                Gadalla N, Sutherland CJ, Meshnick SR (2010) The perils of PCR: Can we
accurately
                'correct' antimalarial trials? Trends Parasitol 26:119-124.</mixed-citation>
         </ref>
         <ref id="d1257e481a1310">
            <label>14</label>
            <mixed-citation id="d1257e488" publication-type="other">
Juliano JJ,
                Taylor SM, Meshnick SR (2009) Polymerase chain reaction adjustment in
antimalarial
                trials: Molecular malarkey? J Infect Dis 200:5-7.</mixed-citation>
         </ref>
         <ref id="d1257e498a1310">
            <label>15</label>
            <mixed-citation id="d1257e505" publication-type="other">
World Health
                Organization (2008) Methods and Techniques for Clinical Trials on
Antimalarial Drug
                Efficacy: Genotyping to Identify Parasite Populations (World
Health
                Organization, Geneva).</mixed-citation>
         </ref>
         <ref id="d1257e518a1310">
            <label>16</label>
            <mixed-citation id="d1257e525" publication-type="other">
Juliano JJ, Kwiek
                JJ, Cappell K, Mwapasa V, Meshnick SR (2007) Minority-variant pfcrt
K76T mutations
                and chloroquine resistance, Malawi. Emerg Infect Dis 13:872-877.</mixed-citation>
         </ref>
         <ref id="d1257e536a1310">
            <label>17</label>
            <mixed-citation id="d1257e543" publication-type="other">
Hastings IM,
                Nsanzabana C, Smith TA (2010) A comparison of methods to detect and
quantify the
                markers of antimalarial drug resistance. Am J Trop Med Hyg 83:489-495.</mixed-citation>
         </ref>
         <ref id="d1257e553a1310">
            <label>18</label>
            <mixed-citation id="d1257e560" publication-type="other">
Tanabe K, et al.
                (2002) In vitro recombination during PCR of Plasmodium falciparum
DNA: A potential
                pitfall in molecular population genetic analysis. Mol Biochem
Parasitol
                122:211-216.</mixed-citation>
         </ref>
         <ref id="d1257e573a1310">
            <label>19</label>
            <mixed-citation id="d1257e580" publication-type="other">
Ferreira MU, et
                al. (1998) Allelic diversity at the merozoite surface protein-1 locus of
Plasmodium
                falciparum in clinical isolates from the southwestern Brazilian Amazon.
Am J Trop Med Hyg
                59:474-480.</mixed-citation>
         </ref>
         <ref id="d1257e593a1310">
            <label>20</label>
            <mixed-citation id="d1257e600" publication-type="other">
Ngrenngarmlert W,
                et al. (2005) Measuring allelic heterogeneity in Plasmodium
falciparum by a
                heteroduplex tracking assay. Am J Trop Med Hyg 72:694-701.</mixed-citation>
         </ref>
         <ref id="d1257e610a1310">
            <label>21</label>
            <mixed-citation id="d1257e617" publication-type="other">
Felger I, et al.
                (1997) Sequence diversity and molecular evolution of the merozoite
surface antigen 2
                of Plasmodium falciparum. J Mol Evol 45:154-160.</mixed-citation>
         </ref>
         <ref id="d1257e627a1310">
            <label>22</label>
            <mixed-citation id="d1257e634" publication-type="other">
Huse SM, Huber
                JA, Morrison HG, Sogin ML, Welch DM (2007) Accuracy and quality of
massively
                parallel DNA pyrosequencing. Genome Biol 8:R143.</mixed-citation>
         </ref>
         <ref id="d1257e645a1310">
            <label>23</label>
            <mixed-citation id="d1257e652" publication-type="other">
Magurran AE
                (2004) Measuring Biological Diversity (Blackwell Science, Maiden, MA).</mixed-citation>
         </ref>
         <ref id="d1257e659a1310">
            <label>24</label>
            <mixed-citation id="d1257e666" publication-type="other">
Gotelli NJ,
                Colwell RK (2001) Quantifying biodiversity: Procedures and pitfalls in
                the
measurement and
                comparison of species richness. Ecol Lett 4:379-391.</mixed-citation>
         </ref>
         <ref id="d1257e676a1310">
            <label>25</label>
            <mixed-citation id="d1257e683" publication-type="other">
Colwell RK, Mao
                CX, Chang J (2004) Interpolating, extrapolating and comparing
incidence based
                species accumulation curves. Ecology 85:2717-2727.</mixed-citation>
         </ref>
         <ref id="d1257e693a1310">
            <label>26</label>
            <mixed-citation id="d1257e700" publication-type="other">
Chao A (1989)
                Estimating population size for sparse data in capture-recapture
experiments.
                Biometrics 45:427-438.</mixed-citation>
         </ref>
         <ref id="d1257e710a1310">
            <label>27</label>
            <mixed-citation id="d1257e717" publication-type="other">
Snounou G, Beck
                HP (1998) The use of PCR genotyping in the assessment of recrudes-
cence or
                reinfection after antimalarial drug treatment. Parasitol Today
                14:462-467.</mixed-citation>
         </ref>
         <ref id="d1257e727a1310">
            <label>28</label>
            <mixed-citation id="d1257e734" publication-type="other">
Day KP, Koella
                JC, Nee S, Gupta S, Read AF (1992) Population genetics and dynamics
of Plasmodium
                falciparum: An ecological view. Parasitology 104(suppl):S35-S52.</mixed-citation>
         </ref>
         <ref id="d1257e745a1310">
            <label>29</label>
            <mixed-citation id="d1257e752" publication-type="other">
Read AF, Taylor
                LH (2001) The ecology of genetically diverse infections. Science 292:
1099-1102.</mixed-citation>
         </ref>
         <ref id="d1257e762a1310">
            <label>30</label>
            <mixed-citation id="d1257e769" publication-type="other">
Bruce MC, et al.
                (2000) Cross-species interactions between malaria parasites in
humans. Science
                287:845-848.</mixed-citation>
         </ref>
         <ref id="d1257e779a1310">
            <label>31</label>
            <mixed-citation id="d1257e786" publication-type="other">
Daubersies P, et
                al. (1996) Rapid turnover of Plasmodium falciparum populations in
asymptomatic
                individuals living in a high transmission area. Am JTropMedHyg 54:18-26.</mixed-citation>
         </ref>
         <ref id="d1257e796a1310">
            <label>32</label>
            <mixed-citation id="d1257e803" publication-type="other">
Mercereau-Puijalon O (1996) Revisiting host/parasite interactions: molecular
                analysis
of parasites
                collected during longitudinal and cross-sectional surveys in humans.
Parasite Immunol
                18:173-180.</mixed-citation>
         </ref>
         <ref id="d1257e816a1310">
            <label>33</label>
            <mixed-citation id="d1257e823" publication-type="other">
Talisuna AO, et
                al. (2004) Two mutations in dihydrofolate reductase combined with
one in the
                dihydropteroate synthase gene predict sulphadoxine-pyrimethamine
parasitological
                failure in Ugandan children with uncomplicated falciparum malaria.
Infect Genet
                Evol 4:321-327.</mixed-citation>
         </ref>
         <ref id="d1257e839a1310">
            <label>34</label>
            <mixed-citation id="d1257e846" publication-type="other">
Harrington WE,
                et al. (2009) Competitive facilitation of drug-resistant Plasmodium
falciparum
                malaria parasites in pregnant women who receive preventive treatment.
Proc Natl Acad
                Sci USA 106:9027-9032.</mixed-citation>
         </ref>
         <ref id="d1257e860a1310">
            <label>35</label>
            <mixed-citation id="d1257e867" publication-type="other">
Hoffmann C, et
                al. (2007) DNA bar coding and pyrosequencing to identify rare HIV
drug resistance
                mutations. Nucleic Acids Res 35:e91.</mixed-citation>
         </ref>
         <ref id="d1257e877a1310">
            <label>36</label>
            <mixed-citation id="d1257e884" publication-type="other">
Wang C, Mitsuya
                Y, Gharizadeh B, Ronaghi M, Shafer RW (2007) Characterization of
mutation spectra
                with ultra-deep pyrosequencing: Application to HIV-1 drug re-
sistance. Genome
                Res 17:1195-1201.</mixed-citation>
         </ref>
         <ref id="d1257e897a1310">
            <label>37</label>
            <mixed-citation id="d1257e904" publication-type="other">
Rozera G, et al.
                (2009) Massively parallel pyrosequencing highlights minority variants in
                the
HIV-1 env
                quasispecies deriving from lymphomonocyte sub-populations. Retrovirology
                6:15.</mixed-citation>
         </ref>
         <ref id="d1257e914a1310">
            <label>38</label>
            <mixed-citation id="d1257e921" publication-type="other">
Mitsuya Y, et
                al. (2008) Minority human immunodeficiency virus type 1 variants in
antiretroviral-naive persons with reverse transcriptase codon 215 revertant
                mutations.
J Virol
                82:10747-10755.</mixed-citation>
         </ref>
         <ref id="d1257e934a1310">
            <label>39</label>
            <mixed-citation id="d1257e941" publication-type="other">
Archer J, et al.
                (2009) Detection of low-frequency pretherapy chemokine (CXC motif)
receptor 4
                (CXCR4)-using HIV-1 with ultra-deep pyrosequencing. Aids 23:1209-1218.</mixed-citation>
         </ref>
         <ref id="d1257e951a1310">
            <label>40</label>
            <mixed-citation id="d1257e958" publication-type="other">
Rogers WO, et al
                (2009) Failure of artesunate-mefloquine combination therapy for
uncomplicated
                Plasmodium falciparum malaria in southern Cambodia. Malar J 8:10.</mixed-citation>
         </ref>
         <ref id="d1257e969a1310">
            <label>41</label>
            <mixed-citation id="d1257e976" publication-type="other">
Mayengue PI, et
                al. (2004) Submicroscopic Plasmodium falciparum infections and
multiplicity of
                infection in matched peripheral, placental and umbilical cord blood
samples from
                Gabonese women. Trop Med Int Health 9:949-958.</mixed-citation>
         </ref>
         <ref id="d1257e989a1310">
            <label>42</label>
            <mixed-citation id="d1257e996" publication-type="other">
Lahr DJ, Katz LA
                (2009) Reducing the impact of PCR-mediated recombination in
molecular
                evolution and environmental studies using a new-generation high-fidelity
DNA polymerase.
                Biotechniques 47:857-866.</mixed-citation>
         </ref>
         <ref id="d1257e1009a1310">
            <label>43</label>
            <mixed-citation id="d1257e1016" publication-type="other">
Kaneko O, Kimura
                M, Kawamoto F, Ferreira MU, Tanabe K (1997) Plasmodium
falciparum:
                Allelic variation in the merozoite surface protein 1 gene in wild
                isolates
from southern
                Vietnam. Exp Parasitol 86:45-57.</mixed-citation>
         </ref>
         <ref id="d1257e1029a1310">
            <label>44</label>
            <mixed-citation id="d1257e1036" publication-type="other">
Williams R, et
                al. (2006) Amplification of complex gene libraries by emulsion PCR. Nat
Methods
                3:545-550.</mixed-citation>
         </ref>
         <ref id="d1257e1046a1310">
            <label>45</label>
            <mixed-citation id="d1257e1053" publication-type="other">
Schoepflin S, et
                al. (2009) Comparison of Plasmodium falciparum allelic frequency
distribution in
                different endemic settings by high-resolution genotyping. Malar J 8:250.</mixed-citation>
         </ref>
         <ref id="d1257e1063a1310">
            <label>46</label>
            <mixed-citation id="d1257e1070" publication-type="other">
Hook EB, Regal
                RR (1995) Capture-recapture methods in epidemiology: methods and
limitations.
                Epidemiol Rev 17:243-264.</mixed-citation>
         </ref>
         <ref id="d1257e1081a1310">
            <label>47</label>
            <mixed-citation id="d1257e1088" publication-type="other">
Kalilani L, et
                al. (2007) A randomized controlled pilot trial of azithromycin or
artesunate added
                to sulfadoxine-pyrimethamine as treatment for malaria in
pregnant women.
                PLoS ONE 2:e1166.</mixed-citation>
         </ref>
         <ref id="d1257e1101a1310">
            <label>48</label>
            <mixed-citation id="d1257e1108" publication-type="other">
Tsibris AM, et
                al. (2009) Quantitative deep sequencing reveals dynamic HIV-1 escape
and large
                population shifts during CCR5 antagonist therapy in vivo. PLoS
                ONE4:e5683.</mixed-citation>
         </ref>
         <ref id="d1257e1118a1310">
            <label>49</label>
            <mixed-citation id="d1257e1125" publication-type="other">
Keating KA,
                Quinn JF (1998) Estimating species richness: The Michaelis-Menten model
revisited.
                O/'/ros 81:411-416.</mixed-citation>
         </ref>
         <ref id="d1257e1135a1310">
            <label>50</label>
            <mixed-citation id="d1257e1142" publication-type="other">
Wei SG, et al.
                (2010) Comparative performance of species-richness estimators using
data from a
                subtropical forest tree community. Ecol Res 25:93-101.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
