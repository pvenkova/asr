<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.18772/12016128660</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Thinking Freedom in Africa</book-title>
         <subtitle>Toward a theory of emancipatory politics</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>NEOCOSMOS</surname>
               <given-names>MICHAEL</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>12</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781868148660</isbn>
      <isbn content-type="epub">9781868148691</isbn>
      <publisher>
         <publisher-name>Wits University Press</publisher-name>
         <publisher-loc>Johannesburg</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Michael Neocosmos</copyright-holder>
         <copyright-holder>Wits University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.18772/12016128660"/>
      <abstract abstract-type="short">
         <p>Previous ways of conceiving the universal emancipation of humanity have in practice ended in failure. Marxism, anti-colonial nationalism and neo-liberalism all understand the achievement of universal emancipation through a form of state politics. Marxism, which had encapsulated the idea of freedom for most of the twentieth century, was found wanting when it came to thinking emancipation because social interests and identities were understood as simply reflected in political subjectivity which could only lead to statist authoritarianism. Neo-liberalism and anti-colonial nationalism have also both assumed that freedom is realisable through the state, and have been equally authoritarian in their relations to those they have excluded on the African continent and elsewhere. Thinking Freedom in Africa then conceives emancipatory politics beginning from the axiom that people think’. In other words, the idea that anyone is capable of engaging in a collective thought-practice which exceeds social place, interests and identities and which thus begins to think a politics of universal humanity. Using the work of thinkers such as Alain Badiou, Jacques Rancière, Sylvain Lazarus, Frantz Fanon and many others, along with the inventive thought of people themselves in their experiences of struggle, the author proceeds to analyse how Africans themselves – with agency of their own – have thought emancipation during various historical political sequences and to show how emancipation may be thought today in a manner appropriate to twenty-first century conditions and concerns.</p>
      </abstract>
      <counts>
         <page-count count="676"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wamba-dia-Wamba</surname>
                           <given-names>Ernest</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>This is a very important book. It deals with a crucial issue of our times of political crisis, namely: is emancipatory politics still possible today and does it have historical references? Or put differently: can we think anew a politics of universal human emancipation? The Marxist political vision has collapsed; attempts to recalibrate it are in difficulty as they lack historical references. The recourse to neo-liberal ideology has made it difficult to even conceptualise the universality of humanity. In fact, due to deep economic crises such as the aggravation of human inequality, neo-liberalism has given rise to fascistic tendencies in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Neocosmos</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.5</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>xxvii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.6</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Politics is thought, thought is real, people think</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The end of ‘the end of history’ was finally announced on a world scale in February 2011. That announcement took place in North Africa and subsequently in the Middle East. Popular upsurges of extraordinary vitality occurred, which brought back into stark relief what most seemed to have forgotten, namely that people, particularly those from the Global South, are perfectly capable of making history. The fact that this process was initiated on the African continent before it began to reverberate elsewhere is also worthy of note. The mass upsurge here was not of religious inspiration but quite secular, contrary to the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.7</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Theoretical introduction:</title>
                     <subtitle>Understanding historical political sequences</subtitle>
                  </title-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>Africans were integrated into European ‘modernity’ through the slave trade. Yet rather than being its pathetic victims, they were able to think as human beings and to actualise that thought during particular exceptional events. It was not simply that people opposed oppression and that rebellions took place; it was also, and more importantly, that in some cases an excessive subjectivity of freedom came to dominate their thinking. The most important of these was without doubt what has become known as the Haitian Revolution from 1791 to 1804, which was an event of world significance. Its effects would have been even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.8</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>From Saint-Domingue to Haiti:</title>
                     <subtitle>The politics of freedom and equality, 1791–1960</subtitle>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>Popular struggles against slavery by Africans have a long history. One of the earliest statements against slavery on the continent itself dates (as far as can be established) from 1222 and is known as The Hunters’ Oath of the Manden or the Mandé Charter.¹ This affirmation is based on the oral traditions of the Mandinka hunters in the area covering parts of modern Mali, Senegal and Guinea and is said to date back to the reign of King Sunjata of the Mandinka. Statements from the charter read like an 18th-century European human rights document and are replete with the recognition</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.9</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Are those-who-do-not-count capable of reason?</title>
                     <subtitle>On the limits of historical thought</subtitle>
                  </title-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>It is important to note that in the academic study of anti-colonial resistance movements in Africa, not only has political consciousness rarely been central, but when it has indeed been the object of study it has been regularly reduced to its social location as well as interpreted, ‘anthropologised’ and translated into an idiom comprehensible to liberal or Marxist post-Enlightenment historical science. Variously described as ‘religious’, ‘tribal’, ‘ethnic’, ‘traditional’ or ‘pre-capitalist’ in their ideologies, such forms of consciousness have been distinguished from those of ‘modernity’ precisely by relating them to their social foundations. While so-called ‘traditional’, ‘ethnic’ and ‘religious’ expressions of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.10</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>The National Liberation Struggle mode of politics in Africa, 1945–1975</title>
                  </title-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>The truth which the event of Haiti 1804 opened up was that of the political emancipation of colonised African peoples, the idea of independence and the formation of African nations achieved by people themselves through their own efforts. It was indeed with the struggles for African independence in mind that C.L.R. James wrote his<italic>Black Jacobins</italic>(James, 2001: xvi). And it was the idea of the nation that lay at the core of independence and post-independence political subjectivities; in times of struggle it was understood as a pure affirmation, but with the advent of state formation it was to be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>The People’s Power mode of politics in South Africa, 1984–1986</title>
                  </title-group>
                  <fpage>134</fpage>
                  <abstract>
                     <p>Having shown at some length the features of the National Liberation Struggle (NLS) mode of politics, I now wish to assess how it was transcended in South Africa in the 1980s. I will outline the new popularly based subjectivities which saw the light of day in that decade and will argue that the period 1984–6 witnessed an event (in Badiou’s sense of the term) in South Africa. This event gave birth to a new mode of politics for the 21st century in Africa, which can be called the People’s Power mode of politics, one that was revived in 2011</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.12</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>From national emancipation to national chauvinism in South Africa, 1973–2013</title>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>Whereas the previous three chapters have been concerned to show the excessive character of various emancipatory modes of politics and the manner in which this excess interacted with expressive political subjectivity, this chapter and the next have a more analytical purpose. More precisely, they are concerned to examine how the idea of a historical sequence may be used to analyse historical changes in political subjectivities in South Africa (in this chapter) and how popular militancy may be rethought for the 21st century in Africa (in chapter 7). There is therefore a slight shift in the argument to emphasise the analytical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.13</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Rethinking militancy in the current sequence:</title>
                     <subtitle>Beyond politics as agency</subtitle>
                  </title-group>
                  <fpage>189</fpage>
                  <abstract>
                     <p>The loss of emancipatory content in the politics associated with socialism and national liberation, as well as their subsequent collapse into state politics, is by now well known; in fact, this outcome has been so common that it seems to have the status of a ‘law of repetition’, as Achille Mbembe has put it in relation to his reading of Fanon.¹ Today salvation is sometimes sought in social movements of an undifferentiated ‘multitude’ (e.g. Hardt and Negri, 2001; Amin and Sridhar, 2002; Bond, 2004), or in the exercise of citizenship rights by disparate sectors of the population making claims on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.14</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Understanding fidelity to the South African emancipatory event:</title>
                     <subtitle>The Treatment Action Campaign and Abahlali baseMjondolo</subtitle>
                  </title-group>
                  <fpage>222</fpage>
                  <abstract>
                     <p>We now no longer live within the Cold War/Keynesian/social-democratic/developmental-state historical sequence and the subjective politics associated with it. Today, neo-liberal economics and politics have replaced state-led economic transformation with market-led growth along with massive unemployment and poverty levels, while so-called deregulation and privatisation have devastated state social-provisioning infrastructure worldwide. At the same time, the current form of colonialism is not only globalised, but has replaced its ‘civilising mission’ (and ‘development mission’) with a liberalising and ‘democratising mission’ (Wamba-dia-Wamba, 2007; Neocosmos, 2009b). Neo-liberal market capitalism and its attendant political liberal-democratic norms are everywhere hegemonic in thought, although people throughout the world</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.15</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>Theoretical introduction:</title>
                     <subtitle>Social representation, modes of rule and political prescriptions</subtitle>
                  </title-group>
                  <fpage>243</fpage>
                  <abstract>
                     <p>For the discipline of sociology, it is the social that is said to determine consciousness or subjectivity. This is manifested in culture, which is determined both internally (socialisation) and externally (social constraints). In this manner both structure and agency determine consciousness (e.g. as indicated by types of social action in the work of Max Weber). There is no freedom to think outside the social except in utopias, which are precisely disconnected from social reality. The social is thereby naturalised. As a result there is no room for thinking emancipatory politics, as there is no room for excessive thought. This way</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.16</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>Marxism and the politics of representation:</title>
                     <subtitle>The ‘agrarian question’ and the limits of political economy – class, nation and the party-state</subtitle>
                  </title-group>
                  <fpage>263</fpage>
                  <abstract>
                     <p>What has become known as the ‘agrarian question’ was a central concern for Marxism in thinking emancipation in the 20th century, because the relatively small proletariat, which was meant to be the subject of history in overwhelmingly rural countries such as Russia, China, Vietnam or Cuba, of necessity had to convince the masses of the rural poor in the countryside that ‘its’ revolutionary politics were also in the interest of the peasantry. The proletariat was thus seen by theory as ‘leading’ politically (i.e. subjectively) a much broader coalition of social forces than itself. Political activists and militants had therefore to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.17</book-part-id>
                  <title-group>
                     <label>Chapter 11</label>
                     <title>Thinking beyond representation, acting beyond representation:</title>
                     <subtitle>Accounting for worker subjectivities in South Africa</subtitle>
                  </title-group>
                  <fpage>309</fpage>
                  <abstract>
                     <p>My concern throughout this book continues to be the carving out of theoretical space for the thinking of politics in its own terms in Africa. The central issue of intellectual and political concern to be explicitly addressed in this chapter is the problem inherent in what may be called the perspective of ‘classism’. By ‘classism’ I mean, following Lazarus (1996), a thought perspective that sees emancipatory politics as founded on the belief in an already socially constituted working class or proletariat as simultaneously a subject of history and a subject of politics. The fusion of social location or place (identified</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.18</book-part-id>
                  <title-group>
                     <label>Chapter 12</label>
                     <title>Renaming the state in Africa today</title>
                  </title-group>
                  <fpage>358</fpage>
                  <abstract>
                     <p>Even if we were to critique the African state in terms of its interest representation (it always represents the dominant interests anyway), its cultural phenomenology of representations of power,¹ or its reflections of a social essence (‘neo-patrimonial’, ‘belly politics’, etc.),² or in terms of identifying institutionally its mode of rule through its creation of ethnic identities,³ all of which may be said to describe and analyse some important aspects of state power, this would not be sufficient. The fact remains that our main project cannot be reduced to a deconstruction or critique of objective power and its subjective representations. Rather,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.19</book-part-id>
                  <title-group>
                     <label>Chapter 13</label>
                     <title>Domains of state politics and systemic violence:</title>
                     <subtitle>The concept of ‘uncivil society’</subtitle>
                  </title-group>
                  <fpage>400</fpage>
                  <abstract>
                     <p>In order to make an argument for an appropriate name for the state in Africa, one needs to establish precisely how the state currently rules the people,¹ particularly given the absence of a national state project of development, which constituted its main way of ruling during the immediate post-independence period. This investigation will be the object of this chapter. It will show that the state’s ways of ruling today (its modes of rule) are varied, and produce distinct ways of subjectively apprehending political agency. We shall see that the identification of a multiplicity of modes of rule by the African</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.20</book-part-id>
                  <title-group>
                     <label>Chapter 14</label>
                     <title>The domain of civil society and its politics</title>
                  </title-group>
                  <fpage>447</fpage>
                  <abstract>
                     <p>The core argument of this chapter and the next is that human rights discourse and traditional discourse, or what Mamdani (2000) calls ‘culture talk’, are expressions of two different modes of rule which operate within (and largely structure) two distinct domains of state politics: the first within a domain of civil society and the second within a domain of traditional society. In each specific domain expressive state politics differ, requiring somewhat different modes of thought in order to enable an alternative excessive politics. The argument deployed here is a conscious attempt to move away from considering tradition and modernity, citizenship</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.21</book-part-id>
                  <title-group>
                     <label>Chapter 15</label>
                     <title>The domain of traditional society and its politics</title>
                  </title-group>
                  <fpage>473</fpage>
                  <abstract>
                     <p>While in civil society state politics are thought around the concepts of citizenship and human rights, in traditional society it is culture and custom that are the operative terms for thinking politics. Both ‘rights’ and ‘culture’ are, of course, objects of struggle and are never simply given. Tradition here will be understood primarily as a produced subjectivity of state-power relations that demarcate a traditional society. Traditional society is predominantly characterised by a mode of rule that revolves principally around the institution of the chieftaincy, which itself embodies (although not necessarily exclusively) the powers of custom and tradition. In this particular</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.22</book-part-id>
                  <title-group>
                     <label>Chapter 16</label>
                     <title>Towards a politics of solidarity:</title>
                     <subtitle>Feminist contributions</subtitle>
                  </title-group>
                  <fpage>521</fpage>
                  <abstract>
                     <p>To conclude Part 2 of this book, I wish to note that, despite the limitations of human rights discourse, which are sometimes admitted in the liberal literature, it is regularly assumed that these are of unquestioned benefit in transforming ‘tradition’, in enabling the previously ‘rightless’ under tradition to ‘acquire human rights’ and thus to assert their humanity in the face of a presumed ‘state of nature’ which, in the famous Hobbesian formulation, is seen as ‘nasty, brutish and short’. The assumption that the character of liberal democracy today is liberatory and universal relative to tradition, which is seen as invariably</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.23</book-part-id>
                  <title-group>
                     <title>Conclusion:</title>
                     <subtitle>Reclaiming the domain of freedom</subtitle>
                  </title-group>
                  <fpage>532</fpage>
                  <abstract>
                     <p>In her excellent book on May 1968 in France, Kristin Ross (2002) argues that the importance of that particular event lies elsewhere than in the question of whether it was a ‘failed revolution’ or not. Rather, she suggests, ‘the narrative of a desired or failed seizure of power ... is a narrative determined by the logic of the state, the story the state tells to itself. For the state, people in the streets are people always already failing to seize state power’ (p. 74). Focusing on the dimension of power – and thereby restricting the idea of politics to the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.24</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>552</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.18772/12016128660.25</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>593</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
