<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">epidinfe</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100820</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Epidemiology and Infection</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09502688</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694409</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">30221562</article-id>
         <title-group>
            <article-title>Limited Impact of Tuberculosis Control in Hong Kong: Attributable to High Risks of Reactivation Disease</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>E.</given-names>
                  <surname>Vynnycky</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>M. W.</given-names>
                  <surname>Borgdorff</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>C. C.</given-names>
                  <surname>Leung</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>C. M.</given-names>
                  <surname>Tam</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>P. E. M.</given-names>
                  <surname>Fine</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">136</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i30221550</issue-id>
         <fpage>943</fpage>
         <lpage>952</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/30221562"/>
         <abstract>
            <p>Over 50 % of the global burden of tuberculosis occurs in South East Asia and the Western Pacific. Since 1950, notification rates in high-income countries in these settings have declined slowly and have remained over ten-fold greater than those in Western populations. The reasons for the slow decline are poorly understood. Using an age-structured model describing the incidence of Mycobacterium tuberculosis infection and disease applied to notification data from Hong Kong, we illustrate that in Hong Kong, a high prevalence of M. tuberculosis infection among older individuals and a high risk of disease through reactivation (e.g. up to 17-fold greater than that estimated for infected males in the United Kingdom) may explain this slow decline. If this feature of the epidemiology of tuberculosis is widespread, the WHO directly observed treatment short-course (DOTS) strategy may have a smaller impact in Asia in the short term than has been implied by recent predictions, all of which have been based on disease risk estimates derived from Western Europe. As a result, it may be difficult to meet the targets for tuberculosis control, which have been prescribed by the UN Millennium Development Goals.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1118e217a1310">
            <label>1</label>
            <mixed-citation id="d1118e224" publication-type="other">
World Health Organization. Global tuberculosis con-
trol. Surveillance, planning, finance. WHO/CDS/TB/
2003.316, 2003.</mixed-citation>
         </ref>
         <ref id="d1118e237a1310">
            <label>2</label>
            <mixed-citation id="d1118e244" publication-type="other">
Tocque K, et al. Long-term trends in tuberculosis.
Comparison of age-cohort data between Hong Kong
and England and Wales. American Journal of Respir-
atory and Critical Care Medicine 1998; 158: 484-488.</mixed-citation>
         </ref>
         <ref id="d1118e260a1310">
            <label>3</label>
            <mixed-citation id="d1118e267" publication-type="other">
Chest Service of the Department of Health. Annual
Report. 1999.</mixed-citation>
         </ref>
         <ref id="d1118e277a1310">
            <label>4</label>
            <mixed-citation id="d1118e284" publication-type="other">
Medical Research Council. MRC national tuberculin
survey 1949-50. Lancet 1952; 1: 775-785.</mixed-citation>
         </ref>
         <ref id="d1118e295a1310">
            <label>5</label>
            <mixed-citation id="d1118e302" publication-type="other">
Vynnycky E, Fine PEM. The annual risk of infection
with Mycobacterium tuberculosis in England and Wales
since 1901. International Journal of Tuberculosis and
Lung Disease 1997; 1: 389-396.</mixed-citation>
         </ref>
         <ref id="d1118e318a1310">
            <label>6</label>
            <mixed-citation id="d1118e325" publication-type="other">
World Health Organization. Analysis and evaluation
of tuberculosis in the British Crown colony of Hong
Kong, 6 September 1951.</mixed-citation>
         </ref>
         <ref id="d1118e338a1310">
            <label>7</label>
            <mixed-citation id="d1118e345" publication-type="other">
Moodie AS. Tuberculosis in Hong Kong. Tubercle
1963; 44: 334-345.</mixed-citation>
         </ref>
         <ref id="d1118e355a1310">
            <label>8</label>
            <mixed-citation id="d1118e362" publication-type="other">
Sutherland I, Svandova E, Radhakrishna S. The devel-
opment of clinical tuberculosis following infection with
tubercle bacilli. 1. A theoretical model for the develop-
ment of clinical tuberculosis following infection, linking
from data on the risk of tuberculous infection and the
incidence of clinical tuberculosis in the Netherlands.
Tubercle 1982; 63: 255-268.</mixed-citation>
         </ref>
         <ref id="d1118e388a1310">
            <label>9</label>
            <mixed-citation id="d1118e395" publication-type="other">
Vynnycky E, Fine PEM. The natural history of tu-
berculosis: the implications of age-dependent risks of
disease and the role of reinfection. Epidemiology and
Infection 1997; 119: 183-201.</mixed-citation>
         </ref>
         <ref id="d1118e411a1310">
            <label>10</label>
            <mixed-citation id="d1118e418" publication-type="other">
Chan-Yeung M, et aL. Molecular and conventional
epidemiology of tuberculosis in Hong Kong: a
population-based prospective study. Journal of Clinical
Microbiology 2003; 41: 2706-2708.</mixed-citation>
         </ref>
         <ref id="d1118e435a1310">
            <label>11</label>
            <mixed-citation id="d1118e442" publication-type="other">
Dye C, et al. Prospects for worldwide tuberculosis con-
trol under the WHO DOTS strategy. Directly observed
short-course therapy. Lancet 1998; 352: 1886-1891.</mixed-citation>
         </ref>
         <ref id="d1118e455a1310">
            <label>12</label>
            <mixed-citation id="d1118e462" publication-type="other">
UN Millennium Development Goals (http://www.un.
org/millenniumgoals/). Accessed 23 February 2007.</mixed-citation>
         </ref>
         <ref id="d1118e472a1310">
            <label>13</label>
            <mixed-citation id="d1118e479" publication-type="other">
Holm, J. Development from tuberculous infection to
tuberculous disease. KNCV, The Hague, The Nether-
lands, 1969.</mixed-citation>
         </ref>
         <ref id="d1118e492a1310">
            <label>14</label>
            <mixed-citation id="d1118e499" publication-type="other">
Global tuberculosis programme and global programme on
vaccines. Statement on BCG revaccination for the pre-
vention of tuberculosis. Weekly Epidemiological Record
1995; 70: 229-231.</mixed-citation>
         </ref>
         <ref id="d1118e515a1310">
            <label>15</label>
            <mixed-citation id="d1118e522" publication-type="other">
Leung CC, et al. Efficacy of the BCG revaccination
programme in a cohort given BCG vaccination at birth
in Hong Kong. International Journal of Tuberculosis and
Lung Disease 2001; 5: 717-723.</mixed-citation>
         </ref>
         <ref id="d1118e538a1310">
            <label>16</label>
            <mixed-citation id="d1118e545" publication-type="other">
Karonga Prevention Trial Group. Randomised con-
trolled trial of single BCG, repeated BCG, or combined
BCG and killed Mycobacterium leprae vaccine for
prevention of leprosy and tuberculosis in Malawi.
Lancet 1996; 348: 17-24.</mixed-citation>
         </ref>
         <ref id="d1118e565a1310">
            <label>17</label>
            <mixed-citation id="d1118e572" publication-type="other">
Census and Statistics Department, Hong Kong. 2006
Population by Census (http://www.bycensus2006.gov.
hk/data/data2/index.htm). Released 23 February 2007.</mixed-citation>
         </ref>
         <ref id="d1118e585a1310">
            <label>18</label>
            <mixed-citation id="d1118e592" publication-type="other">
Comstock GW, Philip RN. Decline of the tuberculosis
epidemic in Alaska. Public Health Reports 1961; 76:
19-24.</mixed-citation>
         </ref>
         <ref id="d1118e605a1310">
            <label>19</label>
            <mixed-citation id="d1118e612" publication-type="other">
Styblo K, Meijer J, Sutherland I. Tuberculosis
Surveillance Research Unit Report No. 1: the trans-
mission of tubercle bacilli; its trend in a human popu-
lation. Bulletin of the International Union Against
Tuberculosis and Lung Disease 1969; 42: 1-104.</mixed-citation>
         </ref>
         <ref id="d1118e631a1310">
            <label>20</label>
            <mixed-citation id="d1118e638" publication-type="other">
Huong NT, et al. Tuberculosis epidemiology in six
provinces of Vietnam after the introduction of the
DOTS strategy. International Journal of Tuberculosis
and Lung Disease 2006; 10: 963-969.</mixed-citation>
         </ref>
         <ref id="d1118e654a1310">
            <label>21</label>
            <mixed-citation id="d1118e661" publication-type="other">
Leung CC, et al. Smoking and tuberculosis in Hong
Kong. International Journal of Tuberculosis and Lung
Disease 2003; 7: 980-986.</mixed-citation>
         </ref>
         <ref id="d1118e674a1310">
            <label>22</label>
            <mixed-citation id="d1118e681" publication-type="other">
Mackay JM, Barnes GT. Effects of strong government
measures against tobacco in Hong Kong. British
Medical Journal (Clinical Research Edition) 1986; 292:
1435-1437.</mixed-citation>
         </ref>
         <ref id="d1118e698a1310">
            <label>23</label>
            <mixed-citation id="d1118e705" publication-type="other">
Lam TH, et al. Mortality and smoking in Hong Kong:
case-control study of all adult deaths in 1998. British
Medical Journal 2001; 323: 361.</mixed-citation>
         </ref>
         <ref id="d1118e718a1310">
            <label>24</label>
            <mixed-citation id="d1118e725" publication-type="other">
Tuberculosis and Chest Service. Tuberculosis control in
Hong Kong. Annual report 2000 of the Tuberculosis
and Chest Service, Annex 4, 1-7, 2004. Department of
Health of Hong Kong Special Administrative Region,
China.</mixed-citation>
         </ref>
         <ref id="d1118e744a1310">
            <label>25</label>
            <mixed-citation id="d1118e751" publication-type="other">
Chang KC, Leung CC, Tam CM. Tuberculosis risk fac-
tors in a silicotic cohort in Hong Kong. International
Journal of Tuberculosis and Lung Disease 2001; 5:
177-184.</mixed-citation>
         </ref>
         <ref id="d1118e767a1310">
            <label>26</label>
            <mixed-citation id="d1118e774" publication-type="other">
Ho MY, Lo WK. Review of occupational diseases in
1997. Public Health and Epidemiology Bulletin 2004; 7:
36-38.</mixed-citation>
         </ref>
         <ref id="d1118e787a1310">
            <label>27</label>
            <mixed-citation id="d1118e794" publication-type="other">
Glynn JR, et al. Worldwide occurrence of Beijing/W
strains of Mycobacterium tuberculosis: a system-
atic review. Emerging Infectious Diseases 2002; 8: 843-
849.</mixed-citation>
         </ref>
         <ref id="d1118e810a1310">
            <label>28</label>
            <mixed-citation id="d1118e817" publication-type="other">
Chan MY, et al. Seventy percent of the Mycobacterium
tuberculosis isolates in Hong Kong represent the
Beijing genotype. Epidemiology and Infection 2001; 127:
169-171.</mixed-citation>
         </ref>
         <ref id="d1118e834a1310">
            <label>29</label>
            <mixed-citation id="d1118e841" publication-type="other">
Ohmori M, et al. Current epidemiological trend of
tuberculosis in Japan. International Journal of Tubercu-
losis and Lung Disease 2002; 6: 415-423.</mixed-citation>
         </ref>
         <ref id="d1118e854a1310">
            <label>30</label>
            <mixed-citation id="d1118e861" publication-type="other">
Chee CB, James L. The Singapore Tuberculosis Elim-
ination Programme: the first five years. Bulletin of the
World Health Organization 2003; 81: 217-221.</mixed-citation>
         </ref>
         <ref id="d1118e874a1310">
            <label>31</label>
            <mixed-citation id="d1118e881" publication-type="other">
Ministry of Health, Singapore. Communicable Disease
Surveillance Report, 2000.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
