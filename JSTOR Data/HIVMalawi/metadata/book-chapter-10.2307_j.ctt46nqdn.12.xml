<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt24h1dk</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt46nqdn</book-id>
      <subj-group>
         <subject content-type="call-number">JC571.L285 2011</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Evolution of International Human Rights</book-title>
         <subtitle>Visions Seen</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Lauren</surname>
               <given-names>Paul Gordon</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>22</day>
         <month>08</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9780812221381</isbn>
      <isbn content-type="epub">9780812209914</isbn>
      <publisher>
         <publisher-name>University of Pennsylvania Press, Inc.</publisher-name>
         <publisher-loc>Philadelphia</publisher-loc>
      </publisher>
      <edition>3</edition>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>University of Pennsylvania Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt46nqdn"/>
      <abstract abstract-type="short">
         <p>This widely acclaimed and highly regarded book, used extensively by students, scholars, policymakers, and activists, now appears in a new third edition. Focusing on the theme of visions seen by those who dreamed of what might be, Lauren explores the dramatic transformation of a world patterned by centuries of human rights abuses into a global community that now boldly proclaims that the way governments treat their own people is a matter of international concern-and sets the goal of human rights "for all peoples and all nations." He reveals the truly universal nature of this movement, places contemporary events within their broader historical contexts, and explains the relationship between individual cases and larger issues of human rights with insight. This new edition incorporates material from recently declassified documents and the most recent scholarship relating to the creation of the new Human Rights Council and its Universal Periodic Review, the International Criminal Court, the Responsibility to Protect (R2P), terrorism and torture, the impact of globalization and modern technology, and activists in NGOs devoted to human rights. It provides perceptive assessments of the process of change, the power of visions and visionaries, politics and political will, and the evolving meanings of sovereignty, security, and human rights themselves.</p>
      </abstract>
      <counts>
         <page-count count="432"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.4</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Visions and Visionaries</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>There are times when the visions seen of a world of possibilities provide a far better measure of a person’s qualities and contributions than the immediate accomplishments of his or her lifetime. Visionary men and women who possess a capacity to see beyond the confines of what is or what has been, and to creatively dream or imagine what might be, sometimes have an impact that far transcends their own time and place. Indeed, visions of prophets, philosophers, and activists seen centuries ago in distant lands are still capable of capturing our imagination, inspiring our thoughts, and influencing our behavior</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.5</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>My Brother’s and Sister’s Keeper:</title>
                     <subtitle>Visions and the Origins of Human Rights</subtitle>
                  </title-group>
                  <fpage>5</fpage>
                  <abstract abstract-type="extract">
                     <p>The historical origins of powerful visions capable of shaping world events and attitudes like those of international human rights are rarely simple. Instead, they emerge in complicated, interrelated, and sometimes paradoxical ways from the influence of many sources, forces, personalities, and conditions in different times and diverse settings. Sometimes together, sometimes overlapping, and sometimes at cross purposes, they each flow like tributaries into ever larger and mightier rivers. At times they flow gently through the calm meadows of religious meditation, prophetic inspiration, poetic expression, philosophic contemplation, or introspection. On other occasions, as we shall see, they smash through human events</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.6</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>To Protect Humanity and Defend Justice:</title>
                     <subtitle>Early International Efforts</subtitle>
                  </title-group>
                  <fpage>43</fpage>
                  <abstract abstract-type="extract">
                     <p>The power of visions of human rights prior to the nineteenth century could be seen largely in inspiration, in their ability to create and then nurture an ideal of compassion and respect for others simply because they were human brothers and sisters. Their capacity to influence actual behavior, however, was largely confined to specific individuals, locales, regions, or in a very small number of cases, groups within nations. Traditional practices, prejudices, vested interests, and capabilities developed over the centuries all served to obstruct human rights and to confine them to exclusive domestic jurisdiction, far removed from consideration as a legitimate</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.7</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Entering the Twentieth Century:</title>
                     <subtitle>Visions, War, Revolutions, and Peacemaking</subtitle>
                  </title-group>
                  <fpage>79</fpage>
                  <abstract abstract-type="extract">
                     <p>The experience of entering a new century invariably provides rather special opportunities to reflect on the process and meaning of historical continuity and change. In this regard, those with visions of human rights at the beginning of the twentieth century—one that eventually would be called the “People’s Century”¹—had reasons for both worry and hope. They knew that the old forces of resistance, prejudices, vested interests, and national sovereignty with domestic jurisdiction would not simply disappear. At the same time the successes gained during the previous century gave them a newfound confidence, and they hoped that the momentum could</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.8</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Opportunities and Challenges:</title>
                     <subtitle>Visions and Rights Between the Wars</subtitle>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract abstract-type="extract">
                     <p>Those who survived a world war, revolutions, and a peace conference reached different conclusions. The all-too-recent experiences of death, devastation, monumental human suffering, and sense of betrayal certainly provided unmistakable warnings about what the future might hold. If the forces of extreme nationalism, state power over citizens, prejudice, and modern technology in the service of war or persecution were unleashed again, then the prospects for security, peace, justice, and human rights appeared bleak indeed. “I see how peoples are set against one another, and in silence, unknowingly, foolishly, obediently, innocently slay one another,” says a young solider in Erich Maria</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.9</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>A “People’s War”:</title>
                     <subtitle>The Crusade of World War II</subtitle>
                  </title-group>
                  <fpage>137</fpage>
                  <abstract abstract-type="extract">
                     <p>The trauma of World War II shook the world to its very foundations. Never before in human history had any armed conflict resulted in so many deaths, such massive devastation, or so much global upheaval. For six brutal years, this total war extended to most parts of the world, consumed the financial and material resources developed over generations, ignited what many called an international “race war,” fanned hatreds that produced horrifying genocide, mobilized science and the mysteries of the atom into instruments of mass destruction, and exposed entire populations of men, women, and children to horrifying death. Very important, more</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.10</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>A “People’s Peace”:</title>
                     <subtitle>Peace and a Charter with Human Rights</subtitle>
                  </title-group>
                  <fpage>165</fpage>
                  <abstract abstract-type="extract">
                     <p>After enduring so much suffering, surviving so much devastation, and hearing so many promises from their leaders about human rights for such a sustained and intense period of the most calamitous war in history, it is hardly surprising that many of those who sacrificed so much during World War II would make demands upon the peace that would follow. Indeed, although the expression, “human rights,” had been explicitly used ever since the eighteenth century, as we have seen, it was the experience of this war and this genocide that propelled it into widespread popular use. Millions of people had fought</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.11</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Proclaiming a Vision:</title>
                     <subtitle>The Universal Declaration of Human Rights</subtitle>
                  </title-group>
                  <fpage>195</fpage>
                  <abstract abstract-type="extract">
                     <p>Those who negotiated and signed the United Nations Charter opened a veritable floodgate to new possibilities of expanding international human rights as never before in history. Explicit provisions suddenly placed the organization and its members on public record and in international law as supporting universal respect for and observance of human rights—”for all.” They formally pledged themselves to promote these rights without any distinction as to race, sex, language, or religion. Just how these objectives would be met in practice, of course, remained to be seen. There were those with expansive hopes that human rights now would be actively</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.12</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Transforming Visions into Reality:</title>
                     <subtitle>The First Fifty Years of the Universal Declaration</subtitle>
                  </title-group>
                  <fpage>227</fpage>
                  <abstract abstract-type="extract">
                     <p>The many women and men who worked so hard to draft and then secure international approval for the Universal Declaration of Human Rights wanted to make certain that the proclaimed vision would be transformed into reality. They knew that without action the text would remain only wishful thinking or abstract verbiage. In fact, their concerns only intensified when critics and detractors immediately attacked their achievement as “mere” words, “only a declaration,” “solely a statement of ideals” devoid of any force, and “just dreams.”¹ But time and experience would reveal that the Universal Declaration made two extremely significant contributions. The first</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.13</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>The Continuing Evolution</title>
                  </title-group>
                  <fpage>267</fpage>
                  <abstract abstract-type="extract">
                     <p>A unique and compellingly symbolic event occurred in 2000 when nearly all the leaders of the world met in New York City for what they called the Millennium Summit. Their purpose was to articulate and affirm their visions at the dawn of the twenty-first century before them. Based upon the lessons they had learned from the past, they quickly acknowledged the existence of a close and integral relationship between security, peace, justice, and human rights. Using the image created by Nelson Mandela, they looked back at the remarkable distance they had come in the light of centuries of historical experience</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.14</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>Toward the Future</title>
                  </title-group>
                  <fpage>289</fpage>
                  <abstract abstract-type="extract">
                     <p>The evolution of international human rights, as we have seen throughout this book, is a history of the long and persistent struggle for freedom and dignity. It is one inspired by visions of what it means to be truly human and have a sense of responsibility to others. It is a history brought about by visionaries and by those determined men and women willing to make sacrifices and sometimes take considerable risks in confronting vested interests, privilege, prejudice, and the claims of national sovereignty. Moreover, it is one in which revolutions, wars, upheavals, and even atrocities have played often critical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.15</book-part-id>
                  <title-group>
                     <title>The Universal Declaration of Human Rights</title>
                  </title-group>
                  <fpage>317</fpage>
                  <abstract abstract-type="extract">
                     <p>
                        <italic>Whereas</italic> recognition of the inherent dignity and of the equal and inalienable rights of all members of the human family is the foundation of freedom, justice, and peace in the world,</p>
                     <p>
                        <italic>Whereas</italic> disregard and contempt for human rights have resulted in barbarous acts which have outraged the conscience of mankind, and the advent of a world in which human beings shall enjoy freedom of speech and belief and freedom from fear and want has been proclaimed as the highest aspiration of the common people,</p>
                     <p>
                        <italic>Whereas</italic> it is essential, if man is not to be compelled to have recourse, as a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>323</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.17</book-part-id>
                  <title-group>
                     <title>Selected Bibliography</title>
                  </title-group>
                  <fpage>375</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>391</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46nqdn.19</book-part-id>
                  <title-group>
                     <title>About the Author</title>
                  </title-group>
                  <fpage>415</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
