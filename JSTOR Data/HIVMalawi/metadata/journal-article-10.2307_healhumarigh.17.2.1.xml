<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">healhumarigh</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Health and Human Rights</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The President and Fellows of Harvard College</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10790969</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">21504113</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>jstor_product</meta-name>
               <meta-value>archive_collection_journals</meta-value>
            </custom-meta>
         </custom-meta-group>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">healhumarigh.17.2.1</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">healhumarigh.17.2.1</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>Editorial</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Making the Case: What Is the Evidence of Impact of Applying Human Rights-Based Approaches to Health?</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Hunt</surname>
                  <given-names>Paul</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Yamin</surname>
                  <given-names>Alicia Ely</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Bustreo</surname>
                  <given-names>Flavia</given-names>
               </string-name>
            </contrib>
            <aff>Paul Hunt is Professor of Law, Human Rights Centre, University of Essex, UK.</aff>
            <aff>Alicia Ely Yamin is Lecturer on Law and Global Health, Director, JD MPH Program, Policy Director, FXB Center for Health and Human Rights, Harvard T.H. Chan School of Public Health, Harvard University, USA.</aff>
            <aff>Flavia Bustreo is Assistant Director-General, Family, Women's and Children's Health, World Health Organization.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>12</month>
            <year>2015</year>
            <string-date>December 2015</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">17</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">healhumarigh.17.issue-2</issue-id>
         <fpage>1</fpage>
         <lpage>9</lpage>
         <permissions>
            <copyright-statement>Health and Human Rights © 2015 Harvard School of Public Health/François-Xavier Bagnoud Center for Health</copyright-statement>
            <copyright-year>2015</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/healhumarigh.17.2.1"/>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>References</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="book">
               <collab>United Nations Development Group</collab>,<source>
                  <italic>The human rights based approach to development cooperation: Towards a common understanding among UN agencies</italic>
               </source>(:<publisher-name>UNDG</publisher-name>,<year>2003</year>).</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="web">See, for example,<collab>Office of the United Nations High Commissioner for Human Rights</collab>,<source>
                  <italic>Scenario and talking points for High Commissioner on Human Rights event to launch the technical guidance on the application of a human rights based approach to the implementation of policies</italic>
               </source>(<month>September</month>
               <year>2012</year>). Available at<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.ohchr.org/EN/NewsEvents/Pages/DisplayNews.aspx-?NewsID=12559&amp;#x00026;LangID=">http://www.ohchr.org/EN/NewsEvents/Pages/DisplayNews.aspx-?NewsID=12559&amp;LangID=</uri>;<person-group person-group-type="author">
                  <string-name>N. Pillay</string-name>
               </person-group>,<source>
                  <italic>Human rights in the post-2015 agenda</italic>
               </source>(<month>June</month>
               <day>6</day>,<year>2013</year>). Available at<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.ohchr.org/Documents/Issues/MDGs/HCOpen-LetterPost2015.pdf">http://www.ohchr.org/Documents/Issues/MDGs/HCOpen-LetterPost2015.pdf</uri>.</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>A. E. Yamin</string-name>
               </person-group>,<source>
                  <italic>Power, suffering, and the struggle for dignity: Human rights frameworks for health and why they matter</italic>
               </source>(:<publisher-name>University of Pennsylvania Press</publisher-name>,<year>2015</year>).</mixed-citation>
         </ref>
         <ref id="ref4">
            <label>4.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>F. Bustreo</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>P. Hunt</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>S. Gruskin</string-name>
               </person-group>, et al.,<source>
                  <italic>Women's and children's health: Evidence of impact of human rights</italic>
               </source>(:<publisher-name>World Health Organization</publisher-name>,<year>2013</year>), p.<fpage>19</fpage>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <label>5.</label>
            <mixed-citation publication-type="other">Ibid., p.<fpage>89</fpage>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <label>6.</label>
            <mixed-citation publication-type="other">Ibid.</mixed-citation>
         </ref>
         <ref id="ref7">
            <label>7.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>A. E. Yamin</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>F. Lander</string-name>
               </person-group>,<article-title>“Implementing a circle of accountability: A proposed framework for judiciaries in enforcing health-related rights,”</article-title>
               <source>
                  <italic>Journal of Human Rights</italic>
               </source>
               <volume>14</volume>/<issue>3</issue>(<year>2015</year>), pp.<fpage>312</fpage>–<lpage>331</lpage>.</mixed-citation>
         </ref>
         <ref id="ref8">
            <label>8.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>F. Bustreo</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>P. Hunt</string-name>
               </person-group>,<article-title>“The right to health is coming of age: Evidence of impact and the importance of leadership,”</article-title>
               <source>
                  <italic>Journal of Public Health Policy</italic>
               </source>
               <volume>34</volume>/<issue>4</issue>(<year>2013</year>), pp.<fpage>574</fpage>–<lpage>579</lpage>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <label>9.</label>
            <mixed-citation publication-type="web">See, for example,<collab>LawAtlas</collab>,<source>
                  <italic>The policy surveillance portal</italic>
               </source>. Available at<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.lawatlas.org/welcome">http://lawatlas.org/welcome</uri>;<person-group person-group-type="author">
                  <string-name>S. Burris</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>M. Ashe</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>D. Levin</string-name>
               </person-group>, et al.,<article-title>“A transdisciplinary approach to public health law: The emerging practice of legal epidemiology,”</article-title>
               <source>
                  <italic>Annual Review of Public Health</italic>
               </source>(forthcoming).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
