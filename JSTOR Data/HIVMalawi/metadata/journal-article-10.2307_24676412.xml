<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mmwrsurvsumm</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50008370</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Morbidity and Mortality Weekly Report: Surveillance Summaries</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Epidemiology Program Office, Centers for Disease Control and Prevention (CDC), Public Health Service, U.S. Department of Health and Human Services</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">15460738</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15458636</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24676412</article-id>
         <title-group>
            <article-title>Malaria Surveillance — United States, 1994</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>S. Patrick</given-names>
                  <surname>Kachur</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Megan E.</given-names>
                  <surname>Reller</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ann M.</given-names>
                  <surname>Barber</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lawrence M.</given-names>
                  <surname>Barat</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Emilia H.A.</given-names>
                  <surname>Koumans</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Monica E.</given-names>
                  <surname>Parise</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jacqueline</given-names>
                  <surname>Roberts</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Trenton K.</given-names>
                  <surname>Ruebush</surname>
                  <suffix>II</suffix>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jane R.</given-names>
                  <surname>Zucker</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>17</day>
            <month>10</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">SS-5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24676409</issue-id>
         <fpage>i</fpage>
         <lpage>18</lpage>
         <page-range>i-iii, 1-18</page-range>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24676412"/>
         <abstract>
            <p>Problem/Condition: Malaria is caused by infection with one of four species of Plasmodium (i.e., P. falciparum, P. vivax, P. ovale, and P. malariae), which are transmitted by the bite of an infective female Anopheles sp. mosquito. Most malarial infections in the United States occur in persons who have traveled to areas (i.e., other countries) in which disease transmission is ongoing. However, cases are transmitted occasionally through exposure to infected blood products, by congenital transmission, or by local mosquitoborne transmission. Malaria surveillance is conducted to identify episodes of local transmission and to adapt prevention recommendations. Reporting Period Covered: Cases with onset of symptoms during 1994. Description of System: Malaria cases confirmed by blood smear are reported to local and/or state health departments by health-care providers and/or laboratories. Case investigations are conducted by local and/or state health departments, and the reports are transmitted to CDC through the National Malaria Surveillance System (NMSS), which was the source of data for this report. Numbers of cases reported through NMSS may differ from those reported through other passive surveillance systems because of differences in the collection and transmission of data. Results: CDC received reports of 1,014 cases of malaria with onset of symptoms during 1994 among persons in the United States or one of its territories. This number represented a 20% decrease from the 1,275 cases reported for 1993. P. vivax, P. falciparum, P. malariae, and P. ovale accounted for 44%, 44%, 4%, and 3% of cases, respectively. More than one species was present in five persons (&lt;1% of the total number of patients). The infecting species was not determined in 50 (5%) cases. The number of reported malaria cases in U.S. military personnel decreased by 86% (i.e., from 278 cases in 1993 to 38 cases in 1994). Of the U.S. civilians who acquired malaria during travel to foreign countries, 18% had followed a chemoprophylactic drug regimen recommended by CDC for the area to which they had traveled. Five persons became infected while in the United States; the infection was transmitted to two of these persons through transfusion of infected blood products. The remaining three cases, which occurred in Houston, Texas, were probably locally acquired mosquitoborne infections. Four deaths were attributed to malaria. Interpretation: The 20% decrease in the number of malaria cases from 1993 to 1994 resulted primarily from an 86% decrease in cases among U.S. military personnel after withdrawal from Somalia. Because most malaria cases acquired in Somalia during 1993 resulted from infection with P.vivax, there was a proportionately greater decrease during 1994 in the number of cases caused by P. vivax relative to those caused by P. falciparum. Actions Taken: Additional information was obtained concerning the four fatal cases and the five cases acquired in the United States. Malaria prevention guidelines were updated and distributed to health-care providers. Persons traveling to a geographic area in which malaria is endemic should take the recommended chemoprophylactic regimen and should use protective measures to prevent mosquito bites. Persons who have a fever or influenza-like illness after returning from a malarious area should seek medical care; medical evaluation should include a blood smear examination for malaria. Malarial infections can be fatal if not promptly diagnosed and treated. Recommendations concerning prevention and treatment of malaria can be obtained from CDC.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1177e307a1310">
            <label>1</label>
            <mixed-citation id="d1177e314" publication-type="other">
Pan American Health Organization. Report for registration of malaria eradication from United
States of America. Washington, DC: Pan American Health Organization, 1969.</mixed-citation>
         </ref>
         <ref id="d1177e324a1310">
            <label>2</label>
            <mixed-citation id="d1177e331" publication-type="other">
Zucker JR. Changing patterns of autochthonous malaria transmission in the United States:
a review of recent outbreaks. Emerging Infectious Diseases 1996;2:37-43.</mixed-citation>
         </ref>
         <ref id="d1177e341a1310">
            <label>3</label>
            <mixed-citation id="d1177e348" publication-type="other">
Lackritz EM, Lobel HO, Howell J, Bloland P, Campbell CC. Imported Plasmodium falciparum
malaria in American travelers to Africa: implications for prevention strategies. JAMA
1991;265:383-5.</mixed-citation>
         </ref>
         <ref id="d1177e361a1310">
            <label>4</label>
            <mixed-citation id="d1177e368" publication-type="other">
World Health Organization. Terminology of malaria and of malaria eradication. Geneva, Swit-
zerland: World Health Organization, 1963:32.</mixed-citation>
         </ref>
         <ref id="d1177e379a1310">
            <label>5</label>
            <mixed-citation id="d1177e386" publication-type="other">
Barat LM, Zucker JR, Barber AM, et al. Malaria surveillance—United States, 1993. In: CDC sur-
veillance summaries. MMWR 1997;46(No. SS-2):27-47.</mixed-citation>
         </ref>
         <ref id="d1177e396a1310">
            <label>6</label>
            <mixed-citation id="d1177e403" publication-type="other">
CDC. Health information for international travel, 1995. Atlanta: US Department of Health and
Human Services, Public Health Service, CDC, 1995; DHHS publication no. (CDQ95-8280.</mixed-citation>
         </ref>
         <ref id="d1177e413a1310">
            <label>7</label>
            <mixed-citation id="d1177e420" publication-type="other">
CDC. Local transmission of Plasmodium vivax malaria—Houston, Texas, 1994. MMWR
1995;44:295, 301-3.</mixed-citation>
         </ref>
         <ref id="d1177e430a1310">
            <label>8</label>
            <mixed-citation id="d1177e437" publication-type="other">
Greenberg AE, Lobel HO. Mortality from Plasmodium falciparum malaria in travelers from
the United States, 1959 to 1987. Ann Intern Med 1990;113:326-7.</mixed-citation>
         </ref>
         <ref id="d1177e447a1310">
            <label>9</label>
            <mixed-citation id="d1177e454" publication-type="other">
Zucker JR, Campbell CC. Malaria: principles of prevention and treatment. Infect Dis Clin North
Am 1993;7:547-67.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
