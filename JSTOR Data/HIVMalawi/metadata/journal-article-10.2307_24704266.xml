<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">evolution</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100004</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society for the Study of Evolution</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00143820</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15585646</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24704266</article-id>
         <title-group>
            <article-title>SWS2 visual pigment evolution as a test of historically contingent patterns of plumage color evolution in warblers</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Natasha I.</given-names>
                  <surname>Bloch</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>James M.</given-names>
                  <surname>Morrow</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Belinda S. W.</given-names>
                  <surname>Chang</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Trevor D.</given-names>
                  <surname>Price</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2015</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">69</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24704260</issue-id>
         <fpage>341</fpage>
         <lpage>356</lpage>
         <permissions>
            <copyright-statement>Copyrightc 2015 Society for the Study of Evolution</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24704266"/>
         <abstract>
            <p>Distantly related clades that occupy similar environments may differ due to the lasting imprint of their ancestors—historical contingency. The New World warblers (Parulidae) and Old World warblers (Phylloscopidae) are ecologically similar clades that differ strikingly in plumage coloration. We studied genetic and functional evolution of the short-wavelength-sensitive visual pigments (SWS2 and SWS1) to ask if altered color perception could contribute to the plumage color differences between clades. We show SWS2 is short-wavelength shifted in birds that occupy open environments, such as finches, compared to those in closed environments, including warblers. Phylogenetic reconstructions indicate New World warblers were derived from a finch-like form that colonized from the Old World 15–20 Ma. During this process, the SWS2 gene accumulated six substitutions in branches leading to New World warblers, inviting the hypothesis that passage through a finch-like ancestor resulted in SWS2 evolution. In fact, we show spectral tuning remained similar across warblers as well as the finch ancestor. Results reject the hypothesis of historical contingency based on opsin spectral tuning, but point to evolution of other aspects of visual pigment function. Using the approach outlined here, historical contingency becomes a generally testable theory in systems where genotype and phenotype can be connected.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d141e283a1310">
            <mixed-citation id="d141e287" publication-type="other">
Alvarado-Cârdenas, L. O., and E. Marti'nez-Meyer. 2013. To converge or not
to converge in environmental space: testing for similar environments
between analogous succulent plants of North America and Africa. Ann.
Bot. 111:1125-1138.</mixed-citation>
         </ref>
         <ref id="d141e303a1310">
            <mixed-citation id="d141e307" publication-type="other">
Andersson, M. B. 1994. Sexual selection. Princeton Univ. Press, Princeton,
NJ.</mixed-citation>
         </ref>
         <ref id="d141e317a1310">
            <mixed-citation id="d141e321" publication-type="other">
Arendt, J., and D. Reznick. 2008. Convergence and parallelism reconsidered:
what have we learned about the genetics of adaptation? Trends Ecol.
Evol. 23:26-32.</mixed-citation>
         </ref>
         <ref id="d141e334a1310">
            <mixed-citation id="d141e338" publication-type="other">
Arikawa, K., M. Wakakuwa, X. Qiu, M. Kurasawa, and D. Stavenga. 2005.
Sexual dimorphism of short-wavelength photoreceptors in the small
white butterfly, Pieris rapae crucivora. J. Neurosci. 25:5935-5942.</mixed-citation>
         </ref>
         <ref id="d141e352a1310">
            <mixed-citation id="d141e356" publication-type="other">
Asenjo, A. B., J. Rim, and D. D. Oprian. 1994. Molecular determinants of
human red/green color discrimination. Neuron 12:1131-1138.</mixed-citation>
         </ref>
         <ref id="d141e366a1310">
            <mixed-citation id="d141e370" publication-type="other">
Barker, F. K., A. Cibois, P. Schikler, J. Feinstein, and J. Cracraft. 2004.
Phylogeny and diversification of the largest avian radiation. Proc. Natl.
Acad. Sei. USA 101:11040-11045.</mixed-citation>
         </ref>
         <ref id="d141e383a1310">
            <mixed-citation id="d141e387" publication-type="other">
Begin, M. T., and P. Handford. 1987. Comparative study of retinal oil droplets
in grebes and coots. Can. J. Zool. 65:2105-2110.</mixed-citation>
         </ref>
         <ref id="d141e397a1310">
            <mixed-citation id="d141e401" publication-type="other">
Bickelmann, C., J. M. Morrow, J. Müller, and B. S. Chang. 2012. Functional
characterization of the rod visual pigment of the echidna (Tachyglossus
aculeatus), a basal mammal. Visual Neurosci. 29:211-217.</mixed-citation>
         </ref>
         <ref id="d141e414a1310">
            <mixed-citation id="d141e418" publication-type="other">
Bloch, N. I. 2014. Evolution of visual pigments in passerine birds: from
opsin genes to visual pigment function. PhD Thesis, University of
Chicago. Available at: http://search.proquest.com/pagepdf/1560685356.
UMI 3628060. Accessed October 2014.</mixed-citation>
         </ref>
         <ref id="d141e434a1310">
            <mixed-citation id="d141e438" publication-type="other">
Boughman, J. W. 2002. How sensory drive can promote speciation. Trends
Ecol. Evol. 17:571-577.</mixed-citation>
         </ref>
         <ref id="d141e449a1310">
            <mixed-citation id="d141e453" publication-type="other">
Bowmaker, J. K. 2008. Evolution of vertebrate visual pigments. Vision Res.
48:2022-2041.</mixed-citation>
         </ref>
         <ref id="d141e463a1310">
            <mixed-citation id="d141e467" publication-type="other">
Bowmaker, J. K., J. K. Kovach, A. V. Whitmore, and E. R. Loew. 1993. Visual
pigments and oil droplets in genetically manipulated and carotenoid
deprived quail: a microspectrophotometric study. Vision Res. 33:571—
578.</mixed-citation>
         </ref>
         <ref id="d141e483a1310">
            <mixed-citation id="d141e487" publication-type="other">
Bowmaker, J. K., L. Heath, S. Wilkie, and D. Hunt. 1997. Visual pigments
and oil droplets from six classes of photoreceptor in the retinas of birds.
Vision Res. 37:2183-2194.</mixed-citation>
         </ref>
         <ref id="d141e500a1310">
            <mixed-citation id="d141e504" publication-type="other">
Briscoe, A. D., S. M. Bybee, G. D. Bernard, F. Yuan, M. P. Sison-Mangus, R.
D. Reed, A. D. Warren, J. Llorente-Bousquets, and C.-C. Chiao. 2010.
Positive selection of a duplicated UV-sensitive visual pigment coincides
with wing pigment evolution in Heliconius butterflies. Proc. Natl. Acad.
Sei. USA 107:3628-3633.</mixed-citation>
         </ref>
         <ref id="d141e523a1310">
            <mixed-citation id="d141e529" publication-type="other">
Castoe, T. A., A. P. J. de Koning, H.-M. Kim, W. Gu, B. P. Noonan, G. Naylor,
Z. J. Jiang, C. L. Parkinson, and D. D. Pollock. 2009. Evidence for an
ancient adaptive episode of convergent molecular evolution. Proc. Natl.
Acad. Sei. USA 106:8986-8991.</mixed-citation>
         </ref>
         <ref id="d141e545a1310">
            <mixed-citation id="d141e549" publication-type="other">
Chan, T., M. Lee, and T. P. Sakmar. 1992. Introduction of hydroxyl-bearing
amino acids causes bathochromic spectral shifts in rhodopsin. Amino
acid substitutions responsible for red-green color pigment spectral tun-
ing. J. Biol. Chem. 267:9478-9480.</mixed-citation>
         </ref>
         <ref id="d141e566a1310">
            <mixed-citation id="d141e570" publication-type="other">
Chang, B. S. 2003. Ancestral gene reconstruction and synthesis of ancient
rhodopsins in the laboratory. Integr. Comp. Biol. 43:500-507.</mixed-citation>
         </ref>
         <ref id="d141e580a1310">
            <mixed-citation id="d141e584" publication-type="other">
Chang, B. S., K. Jonsson, M. Kazmi, M. Donoghue, and T. Sakmar. 2002.
Recreating a functional ancestral archosaur visual pigment. Mol. Biol.
Evol. 19:1483-1489.</mixed-citation>
         </ref>
         <ref id="d141e597a1310">
            <mixed-citation id="d141e601" publication-type="other">
Conte, G. L., M. E. Arnegard, C. L. Peichel, and D. Schlüter. 2012. The prob-
ability of genetic parallelism and convergence in natural populations.
Proc. R. Soc. Lond. B 279:5039-5047.</mixed-citation>
         </ref>
         <ref id="d141e614a1310">
            <mixed-citation id="d141e618" publication-type="other">
Cowing, J. A., S. Poopalasundaram, S. E. Wilkie, J. K. Bowmaker, and D. M.
Hunt. 2002. Spectral tuning and evolution of short wave-sensitive cone
pigments in cottoid fish from Lake Baikal. Biochemistry 41:6019-6025.</mixed-citation>
         </ref>
         <ref id="d141e631a1310">
            <mixed-citation id="d141e635" publication-type="other">
Coyle, B. J., N. S. Hart, K. L. Carleton, and G. Borgia. 2012. Limited variation
in visual sensitivity among bowerbird species suggests that there is no
link between spectral tuning and variation in display colouration. J. Exp.
Biol. 215:1090-1105.</mixed-citation>
         </ref>
         <ref id="d141e651a1310">
            <mixed-citation id="d141e655" publication-type="other">
Crook, J. H. 1964. The evolution of social organisation and visual communi-
cation in the weaver birds (Ploceinae). Behav. Suppl. 10:1-178.</mixed-citation>
         </ref>
         <ref id="d141e666a1310">
            <mixed-citation id="d141e670" publication-type="other">
Endler, J. 1993. The color of light in the forest and its implications. Ecol.
Monogr. 63:2-27.</mixed-citation>
         </ref>
         <ref id="d141e680a1310">
            <mixed-citation id="d141e684" publication-type="other">
Fain, M. G., and P. Houde. 2004. Parallel radiations in the primary clades of
birds. Evolution 58:2558-2573.</mixed-citation>
         </ref>
         <ref id="d141e694a1310">
            <mixed-citation id="d141e698" publication-type="other">
Foster, D. H. 2011. Color constancy. Vision Res. 51:674—700.</mixed-citation>
         </ref>
         <ref id="d141e705a1310">
            <mixed-citation id="d141e709" publication-type="other">
Fotiadis, D., Y. Liang, S. Filipek, D. A. Saperstein, and A. Engel. 2003.
Atomic-force microscopy: rhodopsin dimers in native disc membranes.
Nature 421:127-128.</mixed-citation>
         </ref>
         <ref id="d141e722a1310">
            <mixed-citation id="d141e726" publication-type="other">
Ghosh-Harihar, M., and T. D. Price. 2014. A test for community saturation
along the Himalayan bird diversity gradient, based on within-species
geographical variation. J. Anim. Ecol. 83:628-638.</mixed-citation>
         </ref>
         <ref id="d141e739a1310">
            <mixed-citation id="d141e743" publication-type="other">
Goldsmith, T., J. S. Collins, and S. Licht. 1984. The cone oil droplets of avian
retinas. Vision Res. 24:1661-1671.</mixed-citation>
         </ref>
         <ref id="d141e754a1310">
            <mixed-citation id="d141e758" publication-type="other">
Gomez, D., and M. Thery. 2007. Simultaneous crypsis and conspicuousness
in color patterns: comparative analysis of a neotropical rainforest bird
community. Am. Nat. 169:S42-S61.</mixed-citation>
         </ref>
         <ref id="d141e771a1310">
            <mixed-citation id="d141e775" publication-type="other">
Gould, S. J. 2002. The structure of evolutionary theory. Harvard Univ. Press,
Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="d141e785a1310">
            <mixed-citation id="d141e789" publication-type="other">
Govardovskii, V. I., N. Fyhrquist, T. Reuter, D. G. Kuzmin, and K. Donner.
2000. In search of the visual pigment template. Visual Neurosci 17:509-
528.</mixed-citation>
         </ref>
         <ref id="d141e802a1310">
            <mixed-citation id="d141e806" publication-type="other">
Haldane, J. B. S. 1927. A mathematical theory of natural and artificial se-
lection, Part V: selection and mutation. Math. Proc. Camb. Phil. Soc.
23:838-844.</mixed-citation>
         </ref>
         <ref id="d141e819a1310">
            <mixed-citation id="d141e823" publication-type="other">
Hart, N. S., and D. M. Hunt. 2007. Avian visual pigments: characteristics,
spectral tuning, and evolution. Am. Nat. 169:S7-S26.</mixed-citation>
         </ref>
         <ref id="d141e833a1310">
            <mixed-citation id="d141e837" publication-type="other">
Hart, N. S., J. C. Partridge, A. T. D. Bennett, and I. C. Cuthill. 2000. Visual
pigments, cone oil droplets and ocular media in four species of estrildid
finch. J. Comp. Physiol. A 186:681-694.</mixed-citation>
         </ref>
         <ref id="d141e851a1310">
            <mixed-citation id="d141e855" publication-type="other">
Hart, N. S., T. J. Lisney, and S. P. Collin. 2006. Cone photoreceptor oil
droplet pigmentation is affected by ambient light intensity. J. Exp. Biol.
209:4776-4787.</mixed-citation>
         </ref>
         <ref id="d141e868a1310">
            <mixed-citation id="d141e872" publication-type="other">
He, J., and S. K. Shevell. 1995. Variation in color matching and discrimination
among deuteranomalous trichromats—theoretical implications of small
differences in photopigments. Vision Res. 35:2579-2588.</mixed-citation>
         </ref>
         <ref id="d141e885a1310">
            <mixed-citation id="d141e889" publication-type="other">
Hildebrand, P. W., P. Scheerer, J. H. Park, H.-W. Choe, R. Piechnick, O. P.
Ernst, K. P. Hofmann, and M. Heck. 2009. A ligand channel through the
G protein coupled receptor opsin. PLoS ONE 4:e4382.</mixed-citation>
         </ref>
         <ref id="d141e902a1310">
            <mixed-citation id="d141e906" publication-type="other">
Hofmann, C. M,, N. J. Marshall, K. Abdilleh, Z. Patel, U. E. Siebeck, and K.
L. Carleton. 2012. Opsin evolution in damselfish: convergence, reversal,
and parallel evolution across tuning sites. J. Mol. Evol. 75:79-91.</mixed-citation>
         </ref>
         <ref id="d141e919a1310">
            <mixed-citation id="d141e923" publication-type="other">
Horth, L. 2007. Sensory genes and mate choice: evidence that duplications,
mutations, and adaptive evolution alter variation in mating cue genes
and their receptors. Genomics 90:159-175.</mixed-citation>
         </ref>
         <ref id="d141e936a1310">
            <mixed-citation id="d141e940" publication-type="other">
Hunt, D. M., L. S. Carvalho, J. A. Cowing, and W. L. Davies. 2009. Evolution
and spectral tuning of visual pigments in birds and mammals. Philos.
Trans. R. Soc. Lond. B 364:2941-2955.</mixed-citation>
         </ref>
         <ref id="d141e954a1310">
            <mixed-citation id="d141e958" publication-type="other">
Jetz, W., G. H. Thomas, J. B. Joy, K. Hartmann, and A. O. Mooers. 2012.
The global diversity of birds in space and time. Nature 491:444—
448.</mixed-citation>
         </ref>
         <ref id="d141e971a1310">
            <mixed-citation id="d141e975" publication-type="other">
Knott, B., M. L. Berg, E. R. Morgan, K. L. Buchanan, J. K. Bowmaker, and
A. T. D. Bennett. 2010. Avian retinal oil droplets: dietary manipulation
of colour vision? Proc. R. Soc. Lond. B 277:953-962.</mixed-citation>
         </ref>
         <ref id="d141e988a1310">
            <mixed-citation id="d141e992" publication-type="other">
Liang, Y., D. Fotiadis, S. Filipek, D. A. Saperstein, K. Palczewski, and
A. Engel. 2003. Organization of the G protein-coupled receptors
rhodopsin and opsin in native membranes. J. Biol. Chem. 278:21655-
21662.</mixed-citation>
         </ref>
         <ref id="d141e1008a1310">
            <mixed-citation id="d141e1012" publication-type="other">
Liu, Y., J. A. Cotton, B. Shen, X. Han, S. J. Rossiter, and S. Zhang. 2010.
Convergent sequence evolution between echolocating bats and dolphins.
Curr. Biol. 20:R53-R54.</mixed-citation>
         </ref>
         <ref id="d141e1025a1310">
            <mixed-citation id="d141e1029" publication-type="other">
Losos, J. B. 2010. Adaptive radiation, ecological opportunity, and evolution-
ary determinism. American Society of Naturalists E. O. Wilson award
address. Am. Nat. 175:623-639.</mixed-citation>
         </ref>
         <ref id="d141e1042a1310">
            <mixed-citation id="d141e1046" publication-type="other">
Losos, J. B„ and R. E. Ricklefs. 2009. Adaptation and diversification on
islands. Nature 457:830-836.</mixed-citation>
         </ref>
         <ref id="d141e1057a1310">
            <mixed-citation id="d141e1061" publication-type="other">
Lovette, I. J.. J. L. Pérez-Emân, J. P. Sullivan, R. C. Banks, I. Fiorentino,
S. Cördoba-Cördoba, M. Echeverry-Galvis, F. K. Barker, K. J. Burns,
J. Klicka, et al. 2010. A comprehensive multilocus phylogeny for the
wood-warblers and a revised classification of the Parulidae (Aves). Mol.
Phylogenet. Evol. 57:753-770.</mixed-citation>
         </ref>
         <ref id="d141e1080a1310">
            <mixed-citation id="d141e1084" publication-type="other">
Lovette, I., and W. Hochachka. 2006. Simultaneous effects of phylogenetic
niche conservatism and competition on avian community structure. Ecol-
ogy 87:S14—S28.</mixed-citation>
         </ref>
         <ref id="d141e1097a1310">
            <mixed-citation id="d141e1101" publication-type="other">
Lythgoe, J. N. 1979. The ecology of vision. Oxford Univ. Press, Oxford, U.K.</mixed-citation>
         </ref>
         <ref id="d141e1108a1310">
            <mixed-citation id="d141e1112" publication-type="other">
. 1984. Visual pigments and environmental light. Vision Res. 24:1539-
1550.</mixed-citation>
         </ref>
         <ref id="d141e1122a1310">
            <mixed-citation id="d141e1126" publication-type="other">
Maddison, W. P., and D. R. Maddison. 2001. Mesquite: a modular system for
evolutionary analysis. Available at: http://mesquiteproject.org. Accessed
July 1,2014.</mixed-citation>
         </ref>
         <ref id="d141e1139a1310">
            <mixed-citation id="d141e1143" publication-type="other">
Mahler, D. L., T. Ingram, L. J. Revell, and J. B. Losos. 2013. Exceptional con-
vergence on the macroevolutionary landscape in island lizard radiations.
Science 341:292-295.</mixed-citation>
         </ref>
         <ref id="d141e1157a1310">
            <mixed-citation id="d141e1161" publication-type="other">
Manceau, M., V. S. Domingues, C. R. Linnen, E. B. Rosenblum, and H. E.
Hoekstra. 2010. Convergence in pigmentation at multiple levels: muta-
tions, genes and function. Phil. Trans. R. Soc. Lond. B 365:2439-2450.</mixed-citation>
         </ref>
         <ref id="d141e1174a1310">
            <mixed-citation id="d141e1178" publication-type="other">
Molday, R. S., and D. MacKenzie. 1983. Monoclonal antibodies to rhodopsin:
characterization, cross-reactivity, and application as structural probes.
Biochemistry 22:653-660.</mixed-citation>
         </ref>
         <ref id="d141e1191a1310">
            <mixed-citation id="d141e1195" publication-type="other">
Mollon, J. 1992. Colour vision. Worlds of difference. Nature 356:378-379.</mixed-citation>
         </ref>
         <ref id="d141e1202a1310">
            <mixed-citation id="d141e1206" publication-type="other">
Morrow, J. M., and B. S. Chang. 2010. The plD4-hrGFP II expression vector:
a tool for expressing and purifying visual pigments and other G protein-
coupled receptors. Plasmid 64:162-169.</mixed-citation>
         </ref>
         <ref id="d141e1219a1310">
            <mixed-citation id="d141e1223" publication-type="other">
Morrow, J. M., S. Lazic, and B. S. Chang. 2011. A novel rhodopsin-like gene
expressed in zebrafish retina. Visual Neurosci. 28:325-335.</mixed-citation>
         </ref>
         <ref id="d141e1233a1310">
            <mixed-citation id="d141e1237" publication-type="other">
Nielsen, R., and Z. Yang. 1998. Likelihood models for detecting positively
selected amino acid sites and applications to the HIV-1 envelope gene.
Genetics 148:929-936.</mixed-citation>
         </ref>
         <ref id="d141e1251a1310">
            <mixed-citation id="d141e1255" publication-type="other">
Ord, T. J. 2012. Historical contingency and behavioural divergence in territo-
rial Anolis lizards. J. Evol. Biol. 25:2047-2055.</mixed-citation>
         </ref>
         <ref id="d141e1265a1310">
            <mixed-citation id="d141e1269" publication-type="other">
Orme, D., R. Freckleton, G. Thomas, T. Petzoldt, S. Fritz, N. Isaac, and W.
Pearse. 2013. Caper: comparative analyses of phylogenetics and evo-
lution in R. Available at: http://cran.r-project.org/web/packages/caper/.
Accessed July 1, 2014.</mixed-citation>
         </ref>
         <ref id="d141e1285a1310">
            <mixed-citation id="d141e1289" publication-type="other">
Odeen, A., S. Pruett-Jones, A. C. Driskell, J. K. Armenta, and O. Hâstad.
2012. Multiple shifts between violet and ultraviolet vision in a family of
passerine birds with associated changes in plumage coloration. Proc. R.
Soc. Lond. B 279:1269-1276.</mixed-citation>
         </ref>
         <ref id="d141e1305a1310">
            <mixed-citation id="d141e1309" publication-type="other">
Odeen, A., O. Hâstad, and P. Alstrom. 2011. Evolution of ultraviolet vision in
the largest avian radiation - the passerines. BMC Evol. Biol. 11:313.</mixed-citation>
         </ref>
         <ref id="d141e1319a1310">
            <mixed-citation id="d141e1323" publication-type="other">
Palczewski, K., T. Kumasaka, T. Hori, and C. A. Behnke. 2000. Crystal
structure of rhodopsin: a G protein-coupled receptor. Science 289:739-
745.</mixed-citation>
         </ref>
         <ref id="d141e1336a1310">
            <mixed-citation id="d141e1340" publication-type="other">
Parker, J., G. Tsagkogeorga, J. A. Cotton, Y. Liu, P. Provero, E. Stupka, and
S. J. Rossiter. 2013. Genome-wide signatures of convergent evolution
in echolocating mammals. Nature 502:228-231.</mixed-citation>
         </ref>
         <ref id="d141e1354a1310">
            <mixed-citation id="d141e1358" publication-type="other">
Partridge, J. C., and M. E. Cummings. 1999. Adaptations of visual pigments
to the aquatic environment. Pp. 251-284 in S. N. Archer, M. B. A.
Djamgoz, E. R. Loew, J. C. Partridge, and S. Valerga, eds. Adaptive
mechanisms in the ecology of vision. Kluwer Academic Publishers,
Boston.</mixed-citation>
         </ref>
         <ref id="d141e1377a1310">
            <mixed-citation id="d141e1381" publication-type="other">
Pearce, T. 2012. Convergence and parallelism in evolution: a neo-Gouldian
account. Brit. J. Philos. Sei. 63:429^148.</mixed-citation>
         </ref>
         <ref id="d141e1391a1310">
            <mixed-citation id="d141e1395" publication-type="other">
Piechnick, R., E. Ritter, P. W. Hildebrand, O. P. Ernst, P. Scheerer, K. P.
Hofmann, and M. Heck. 2012. Effect of channel mutations on the uptake
and release of the retinal ligand in opsin. Proc. Natl. Acad. Sei. USA
109:5247-5252.</mixed-citation>
         </ref>
         <ref id="d141e1411a1310">
            <mixed-citation id="d141e1415" publication-type="other">
Price, T. D., and M. Pavelka. 1996. Evolution of a colour pattern: history,
development, and selection. J. Evol. Biol. 9:451 —470.</mixed-citation>
         </ref>
         <ref id="d141e1425a1310">
            <mixed-citation id="d141e1429" publication-type="other">
Price, T. D., I. Lovette, E. Bermingham, H. Gibbs, and A. Richman. 2000.
The imprint of history on communities of North American and Asian
warblers. Am. Nat. 156:354-367.</mixed-citation>
         </ref>
         <ref id="d141e1442a1310">
            <mixed-citation id="d141e1446" publication-type="other">
Price, T. D., D. M. Hooper, C. D. Buchanan, U. S. Johansson, D. T. Tietze,
P. Alström, U. Olsson, M. Ghosh-Harihar, F. Ishtiaq, S. K. Gupta, et al.
2014. Niche filling slows the diversification of Himalayan songbirds.
Nature 509:222-225.</mixed-citation>
         </ref>
         <ref id="d141e1463a1310">
            <mixed-citation id="d141e1467" publication-type="other">
Prunier, R., K. E. Holsinger, and J. E. Carlson. 2012. The effect of historical
legacy on adaptation: do closely related species respond to the environ-
ment in the same way? J. Evol. Biol. 25:1636-1649.</mixed-citation>
         </ref>
         <ref id="d141e1480a1310">
            <mixed-citation id="d141e1484" publication-type="other">
Regan, B. C., C. Julliot, B. Simmen, F. Viénot, P. Charles-Dominique, and
J. D. Mollon. 2001. Fruits, foliage and the evolution of primate colour
vision. Phil. Trans. R. Soc. Lond. B 356:229-283.</mixed-citation>
         </ref>
         <ref id="d141e1497a1310">
            <mixed-citation id="d141e1501" publication-type="other">
Rice, W. R„ and S. D. Gaines. 1994. Extending nondirectional heterogeneity
tests to evaluate simply ordered alternative hypotheses. Proc. Natl. Acad.
Sei. USA 91:225-226.</mixed-citation>
         </ref>
         <ref id="d141e1514a1310">
            <mixed-citation id="d141e1518" publication-type="other">
Rosenblum, E. B., C. E. Parent, and E. E. Brandt. 2014. The molecular basis
of phenotypic convergence. Annu. Rev. Ecol. Evol. Syst. 45:203-226.</mixed-citation>
         </ref>
         <ref id="d141e1528a1310">
            <mixed-citation id="d141e1532" publication-type="other">
Sanocki, E., S. K. Shevell, and J. Winderickx. 1994. Serine/Alanine amino acid
polymorphism of the L-cone photopigment assessed by dual Rayleigh-
type color matches. Vision Res. 34:377—382.</mixed-citation>
         </ref>
         <ref id="d141e1545a1310">
            <mixed-citation id="d141e1549" publication-type="other">
Schlüter, D. 1986. Tests for similarity and convergence of finch communities.
Ecology 67:1073-1085.</mixed-citation>
         </ref>
         <ref id="d141e1560a1310">
            <mixed-citation id="d141e1564" publication-type="other">
Schlüter, D., T. D. Price, A. O. Mooers, and D. Ludwig. 1997. Likelihood of
ancestor states in adaptive radiation. Evolution 51:1699-1711.</mixed-citation>
         </ref>
         <ref id="d141e1574a1310">
            <mixed-citation id="d141e1578" publication-type="other">
Seehausen, O., Y. Terai, I. S. Magalhaes, K. L. Carleton, H. D. J. Mrosso, R.
Miyagi, I. Van Der Sluijs, M. V. Schneider, M. E. Maan, H. Tachida,
et al. 2008. Speciation through sensory drive in cichlid fish. Nature
455:620-626.</mixed-citation>
         </ref>
         <ref id="d141e1594a1310">
            <mixed-citation id="d141e1598" publication-type="other">
Shyue, S. K., S. Boissinot, H. Schneider, I. Sampaio, M. P. Schneider,
C. R. Abee, L. Williams, D. Hewett-Emmett, H. G. Sperling, J. A.
Cowing, et al. 1998. Molecular genetics of spectral tuning in New World
monkey color vision. J. Mol. Evol. 46:697-702.</mixed-citation>
         </ref>
         <ref id="d141e1614a1310">
            <mixed-citation id="d141e1618" publication-type="other">
Starace, D. M., and B. E. Knox. 1998. Cloning and expression of a Xenopus
short wavelength cone pigment. Exp. Eye Res. 67:209-220.</mixed-citation>
         </ref>
         <ref id="d141e1628a1310">
            <mixed-citation id="d141e1632" publication-type="other">
Stoddard, M. C., and R. O. Pram. 2008. Evolution of avian plumage color in a
tetrahedral color space: a phylogenetic analysis of new world buntings.
Am. Nat. 171:755-776.</mixed-citation>
         </ref>
         <ref id="d141e1645a1310">
            <mixed-citation id="d141e1649" publication-type="other">
Sugawara, T., Y. Terai, H. Imai, G. Turner, S. Koblmuller, C. Sturmbauer,
Y. Shichida, and N. Okada. 2005. Parallelism of amino acid changes at
the RH1 affecting spectral sensitivity among deep-water cichlids from
Lakes Tanganyika and Malawi. Proc. Natl. Acad. Sei. USA 102:5448-
5453.</mixed-citation>
         </ref>
         <ref id="d141e1669a1310">
            <mixed-citation id="d141e1673" publication-type="other">
Sugawara, T., H. Imai, M. Nikaido, Y. Imamoto, and N. Okada. 2010. Verte-
brate rhodopsin adaptation to dim light via rapid Meta-II intermediate
formation. Mol. Biol. Evol. 27:506-519.</mixed-citation>
         </ref>
         <ref id="d141e1686a1310">
            <mixed-citation id="d141e1690" publication-type="other">
Takahashi, Y., and T. G. Ebrey. 2003. Molecular basis of spectral tuning in the
newt short wavelength sensitive visual pigment. Biochemistry 42:6025-
6034.</mixed-citation>
         </ref>
         <ref id="d141e1703a1310">
            <mixed-citation id="d141e1707" publication-type="other">
Takenaka, N., and S. Yokoyama. 2007. Mechanisms of spectral tuning in the
RH2 pigments of Tokay gecko and American chameleon. Gene 399:26-
32.</mixed-citation>
         </ref>
         <ref id="d141e1720a1310">
            <mixed-citation id="d141e1724" publication-type="other">
Yang, Z. 2005. Bayes empirical Bayes inference of amino acid sites under
positive selection. Mol. Biol. Evol. 22:1107-1118.</mixed-citation>
         </ref>
         <ref id="d141e1734a1310">
            <mixed-citation id="d141e1738" publication-type="other">
. 2007. PAML 4: phylogenetic analysis by maximum likelihood. Mol.
Biol. Evol. 24:1586-1591.</mixed-citation>
         </ref>
         <ref id="d141e1748a1310">
            <mixed-citation id="d141e1752" publication-type="other">
Yang, Z., and J. Bielawski. 2000. Statistical methods for detecting molecular
adaptation. Trends Ecol. Evol. 15:496-503.</mixed-citation>
         </ref>
         <ref id="d141e1763a1310">
            <mixed-citation id="d141e1767" publication-type="other">
Yang, Z., R. Nielsen, N. Goldman, and A. M. Pedersen. 2000. Codon-
substitution models for heterogeneous selection pressure at amino acid
sites. Genetics 155:431-449.</mixed-citation>
         </ref>
         <ref id="d141e1780a1310">
            <mixed-citation id="d141e1784" publication-type="other">
Yokoyama, S. 2000. Molecular evolution of vertebrate visual pigments. Prog.
Retin. Eye Res. 19:385^119.</mixed-citation>
         </ref>
         <ref id="d141e1794a1310">
            <mixed-citation id="d141e1798" publication-type="other">
. 2003. The spectral tuning in the short wavelength-sensitive type 2
pigments. Gene 306:91-98.</mixed-citation>
         </ref>
         <ref id="d141e1808a1310">
            <mixed-citation id="d141e1812" publication-type="other">
. 2008. Evolution of dim-light and color vision pigments. Annu. Rev.
Genomics Hum. Genet. 9:259-282.</mixed-citation>
         </ref>
         <ref id="d141e1822a1310">
            <mixed-citation id="d141e1826" publication-type="other">
Yokoyama, S., N. S. Blow, and F. B. Radlwimmer. 2000. Molecular evolution
of color vision of zebra finch. Gene 259:17-24.</mixed-citation>
         </ref>
         <ref id="d141e1836a1310">
            <mixed-citation id="d141e1840" publication-type="other">
Yokoyama, S., N. Takenaka, and N. Blow. 2007. A novel spectral tuning in
the short wavelength-sensitive (SWS1 and SWS2) pigments of bluefin
killifish (Lucania goodei). Gene 396:196-202.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
