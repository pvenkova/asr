<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26348231</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Anticipatory child fostering and household economic security in Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Bachan</surname>
                  <given-names>Lauren K.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>The Pennsylvania State University. 211 Oswald Tower. University Park, PA 16802, USA.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2014</year>
            <string-date>JANUARY - JUNE 2014</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">30</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26348191</issue-id>
         <fpage>1157</fpage>
         <lpage>1188</lpage>
         <permissions>
            <copyright-statement>© 2014 Lauren K. Bachan</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26348231"/>
         <abstract xml:lang="eng">
            <sec>
               <label>BACKGROUND</label>
               <p>While there is a rich literature on the practice of child fostering in sub-Saharan Africa, little is known about how fostering impacts receiving households, as few studies consider household conditions both before and after fostering. Despite the fact that circumstances surrounding fostering vary, the literature’s key distinction of fostering is often drawn along the simple line of whether or not a household is fostering a child. This paper argues that anticipation of fostering responsibilities, in particular, is a useful dimension to distinguish fostering experiences for receiving households.</p>
            </sec>
            <sec>
               <label>OBJECTIVE</label>
               <p>This paper examines the relationship between receiving a foster child and subsequent changes in household wealth. Particular emphasis is placed on how these changes are conditioned by differing levels of anticipation of the fostering event.</p>
            </sec>
            <sec>
               <label>METHODS</label>
               <p>This study uses data from Tsogolo la Thanzi (TLT), a longitudinal survey in Balaka, Malawi. Using data from 1754 TLT respondents, fixed effects pooled time-series models are estimated to assess whether and how receiving a foster child changes household wealth.</p>
            </sec>
            <sec>
               <label>RESULTS</label>
               <p>This paper demonstrates the heterogeneity of fostering experiences for receiving households. The results show that households that anticipate fostering responsibilities experience a greater increase in household wealth than both households that do not foster and those that are surprised by fostering.</p>
            </sec>
            <sec>
               <label>CONCLUSION</label>
               <p>Households that anticipate fostering responsibilities exhibit the greatest increase in household wealth. While fostering households that do not anticipate fostering responsibilities may not experience these gains, there is no evidence to indicate that such households are negatively impacted relative to households that do not foster. This finding suggests that additional childcare responsibilities may not be as detrimental to African households as some researchers have feared.</p>
            </sec>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Ainsworth, M. (1996). Economic Aspects of Child Fostering in Cote d’Ivoire. Research in Population Economics 8: 25–62.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Ainsworth, M. and Filmer, D. (2006). Inequalities in Children’s Schooling: AIDS, Orphanhood, Poverty, and Gender. World Development 34(6): 1099–1128. doi:10.1016/j.worlddev.2005.11.007.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Akresh, R. (2005). Risk, Network Quality, and Family Structure: Child Fostering Decisions in Burkina Faso. Illinois: University of Illinois at Urbana-Champaign. (IZA Discussion Paper 1471).</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Akresh, R. (2009). Flexibility of Household Structure. Journal of Human Resources 44(4): 976–997. doi:10.1353/jhr.2009.0011.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Allison, P.D. (1994). Using Panel Data to Estimate the Effects of Events. Sociological Methods &amp; Research 23(2): 174–199. doi:10.1177/0049124194023002002.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Allison, P.D. (2009). Fixed Effects Regression Models. Los Angeles, CA: Sage Publications, Inc.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Andvig, J.C., Canagarajah, S., and Kielland, S. (1999). Child labor in Africa: The issues. Unpublished mimeo. World Bank, Social Protection Sector .</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Bandawe, C.R. and Louw, J. (1997). The experience of family foster care in Malawi: A preliminary investigation. Child Welfare 76(4): 535–47.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Barnett, T. and Blaikie, P. (1992). AIDS in Africa: Its Present and Future Impact. New York: Guilford Press.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Becker, G.S. (1991). A Treatise on the Family. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Beegle, K., Filmer, D., Stokes, A., and Tiererova, L. (2010a). Orphanhood and the living arrangements of children in sub-Saharan Africa. World Development 38(12): 1727–1746. doi:10.1016/j.worlddev.2010.06.015.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Beegle, K., Weerdt, J.D., and Dercon, S. (2010b). Orphanhood and human capital destruction: Is there persistence into adulthood? Demography 47(1): 163–180. doi:10.1353/dem.0.0094.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Bicego, G., Rutstein, S., and Johnson, K. (2003). Dimensions of the emerging orphan crisis in sub-Saharan Africa. Social Science &amp; Medicine 56(6): 1235–1247. doi:10.1016/S0277-9536(02)00125-9.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Bingenheimer, J.B. (2007). Wealth, wealth indices and HIV risk in east africa. International Family Planning Perspectives 33(2): 83–84. doi:10.1363/3308307.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Bledsoe, C. (1990). The politics of children: Fosterage and the social management of fertility among the Mende of Sierra Leonne. In: Handwerker, W. (ed.). Births and Power: Social Change and the Politics of Reproduction. Boulder, CO:Westview Press: 81–100.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Bledsoe, C. and Isiugo-Abanihe, U. (1989). Strategies of child-fosterage among Mende grannies in Sierra Leone. In: Lesthaeghe, R.J. (ed.). Reproduction and Social Organization in sub-Saharan Africa. Berkley, CA: University of California Press: 443–475.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Bongaarts, J. (2001). Household size and composition in the developing world in the 1990s. Population Studies 55(3): 263–279. doi:10.1080/00324720127697.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Caldwell, J. (1976). Toward a restatement of demographic transition theory. Population and Development Review 2(3/4): 321–366. doi:10.2307/1971615.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Caldwell, J. (1983). Direct economic costs and benefits of children. In: Bulatao, R.A. and Lee, R.D. (eds.). Determinates of Fertility in Developing Countries. New York: Academic Press: 458–493, vol. 1.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Caldwell, J. (1997). The impact of the African AIDS epidemic. Health Transition Review 7(Supplement 2): 169–188.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Case, A., Paxson, C., and Ableidinger, J. (2004). Orphans in Africa: Parental death, poverty, and school enrollment. Demography 41(3): 483–508. doi:10.1353/dem.2004.0019.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Cluver, L., Gardner, F., and Operario, D. (2007). Psychological distress amongst AIDSOrphaned children in urban South Africa. Journal of Child Psychology and Psychiatry 48(8): 755–763. doi:10.1111/j.1469-7610.2007.01757.x.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Cluver, L. and Orkin, M. (2009). Cumulative risk and AIDS-Orphanhood: interactions of stigma, bullying and poverty on child mental health in South Africa. Social Science &amp; Medicine 69(8): 1186–1193. doi:10.1016/j.socscimed.2009.07.033.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Dahl, B. (2009). The “Failures of culture”: Christianity, kinship, and moral discourses about orphans during Botswana’s AIDS crisis. Africa Today 56(1): 23–43. doi:10.2979/AFT.2009.56.1.22.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Deininger, K., Garcia, M., and Subbarao, K. (2003). AIDS-Induced orphanhood as a systemic shock: Magnitude, impact, and program interventions in Africa. World Development 31(7): 1201–1220. doi:10.1016/S0305-750X(03)00061-5.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Delavande, A. and Kohler, H.P. (2009). Subjective expectations in the context of HIV/AIDS in Malawi. Demographic Research 20: 817–874. doi:10.4054/DemRes.2009.20.31.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Drah, B. (2012). Orphans in Sub-Saharan Africa: The Crisis, the Interventions, and the Anthropologist. Africa Today 59(2): 2–21. doi:10.2979/africatoday.59.2.3.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Eloundou-Enyegue, P. and Shapiro, D. (2004). Buffering inequalities: The safety net of extended families in Cameroon. Ithaca, NY: Cornell Univ., Cornell Food and Nutrition Policy Program. (SAGA Working Paper).</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Filmer, D. and Pritchett, L.H. (2001). Estimating wealth effects without expenditure data–or tears: An application to educational enrollments in states of India. Demography 38(1): 115–132.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Goody, E.N. (1982). Parenthood and Social Reproduction: Fostering and Occupational Roles in West Africa. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Grant, M.J. (2008). Children’s school participation and HIV/AIDS in rural Malawi: The role of parental knowledge and perceptions. Demographic Research 19: 1603–1634. doi:10.4054/DemRes.2008.19.45.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Grant, M.J. and Yeatman, S. (2012). The relationship between orphanhood and child fostering in sub-Saharan Africa, 1990s–2000s. Population Studies 1–17. doi:10.1080/00324728.2012.681682.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Gregson, S., Mushati, P., and Nyamukapa, C. (2007). Adult mortality and erosion of household viability in AIDS-Afflicted towns, estates, and villages in eastern Zimbabwe. JAIDS Journal of Acquired Immune Deficiency Syndromes 44: 188–195. doi:10.1097/01.qai.0000247230.68246.13.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Hosegood, V., Floyd, S., Marston, M., Hill, C., McGrath, N., Isingo, R., Crampin, A., and Zaba, B. (2007a). The effects of high HIV prevalence on orphanhood and living arrangements of children in Malawi, Tanzania, and South Africa. Population Studies 61(3): 327–336. doi:10.1080/00324720701524292.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Hosegood, V., Preston-Whyte, E., Busza, J., Moitse, S., and Timaeus, I.M.(2007b). Revealing the full extent of households experiences of HIV and AIDS in rural South Africa. Social Science &amp; Medicine 65(6): 1249–1259. doi:10.1016/j.socscimed.2007.05.002.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Howard, B., Phillips, C., Matinhure, N., Goodman, K., McCurdy, S., and Johnson, C. (2006). Barriers and incentives to orphan care in a time of AIDS and economic crisis: A cross-sectional survey of caregivers in rural Zimbabwe. BMC Public Health 6(1):27. doi:10.1186/1471-2458-6-27.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Howe, L.D., Hargreaves, J.R., and Huttly, S.R. (2008). Issues in the construction of wealth indices for the measurement of socio-economic position in low-income countries. Emerging Themes in Epidemiology 5: 3. doi:10.1186/1742-7622-5-3.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Isiugo-Abanihe, U.C. (1985). Child fosterage in West Africa. Population and Development Review 11(1): 53–73. doi:10.2307/1973378.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Johnson, D.R. (1995). Alternative methods for the quantitative analysis of panel data in family research: Pooled time-series models. Journal of Marriage and Family 57(4): 1065–1077. doi:10.2307/353423.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Johnson-Hanks, J.A. (2006). Uncertain Honor: Modern Motherhood in an African Crisis. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Klaits, F. (2010). Death in a Church of Life: Moral Passion during Botswana’s Time of AIDS. Berkeley, CA:University of California Press, 1st ed.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Lloyd, C. and Desai, S. (1992). Children’s living arrangements in developing countries. Population Research and Policy Review 11(3): 193–216. doi:10.1007/BF00124937.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Madhavan, S. (2004). Fosterage patterns in the age of AIDS: continuity and change. Social Science &amp; Medicine 58(7): 1443–1454. doi:10.1016/S0277-9536(03)00341-1.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">McDaniel, A. and Zulu, E. (1996). Mothers, fathers, and children: Regional patterns in child-parent residence in sub-Saharan Africa. African Population Studies 11(1): 1–28.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Monasch, R. and Boerma, T.J. (2004). Orphanhood and childcare patterns in sub-Saharan Africa: An analysis of national surveys from 40 countries. AIDS 18: S55–S65. doi:10.1097/00002030-200406002-00007.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Munthali, A. (2002). Adaptive strategies and coping mechanisms of families and communities affected by HIV/AIDS in Malawi. Geneva: United Nations Research Institution for Social Development.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">National Statistical Office and ICF Macro (2011). Malawi demographic and health survey 2010. Zomba, Malawi and Calverton, MD.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Ntozi, J. (1995). High Fertility in Rural Uganda: The Role of Socioeconomic and Biological Factors. Kampala: Fountain Publishers.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Nyambedha, E.O., Wandibba, S., and Aagaard-Hansen, J. (2003). Changing patterns of orphan care due to the HIV epidemic in Western Kenya. Social Science &amp; Medicine 57(2): 301–311. doi:10.1016/S0277-9536(02)00359-3.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Rutstein, S. and Johnson, K. (2004). DHS wealth index. Calverton, MD: ORC Macro.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Smith, K.P. and Watkins, S.C. (2005). Perceptions of risk and strategies for prevention: Responses to HIV/AIDS in rural Malawi. Social Science &amp; Medicine 60(3): 649–660. doi:10.1016/j.socscimed.2004.06.009.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Subbarao, K., Mattimore, A., and Plangemann, K. (2001). Social protection of Africa’s orphans and other vulnerable children: Issues and good practice program options. Washington, D.C.: The World Bank. (Africa Region Human Development Working Paper Series).</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Trinitapoli, J. and Weinreb, A. (2012). Religion and AIDS in Africa. New York: Oxford University Press. doi:10.1093/acprof:oso/9780195335941.001.0001.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Trinitapoli, J. and Yeatman, S. (2011). Uncertainty and fertility in a generalized AIDS epidemic. American Sociological Review 76(6): 935–954. doi:10.1177/0003122411427672.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">UNAIDS (2007). Report on the global AIDS epidemic. United Nations.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">UNAIDS (2010). Report on the global AIDS epidemic. United Nations.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Urassa, M., Boerma, J.T., Ng’weshemi, J.Z.L., Isingo, R., Schapink, D., and Kumogola, Y. (1997). Orphanhood, child fostering and the AIDS epidemic in rural Tanzania. Health Transition Review 7(Supplement 2): 141–153.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Van de Walle, E. (2006). African Households: Censuses And Surveys. New York: M.E. Sharpe Inc.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Watkins, S.C. (2004). Navigating the AIDS epidemic in rural Malawi. Population and Development Review 30(4): 673–705. doi:10.1111/j.1728-4457.2004.00037.x.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Weinreb, A., Gerland, P., and Fleming, P. (2008). Hotspots and coldspots: Household and village-level variation in orphanhood prevalence in rural Malawi. Demographic Research 19(32): 1217–1217. doi:10.4054/DemRes.2008.19.32.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
