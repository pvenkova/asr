<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt18fs72d</book-id>
      <subj-group>
         <subject content-type="call-number">HB1437 .H39 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Mortality</subject>
         <subj-group>
            <subject content-type="lcsh">Soviet Union</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Mortality</subject>
         <subj-group>
            <subject content-type="lcsh">Russia (Federation)</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Life expectancy</subject>
         <subj-group>
            <subject content-type="lcsh">Soviet Union</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Life expectancy</subject>
         <subj-group>
            <subject content-type="lcsh">Russia (Federation)</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Soviet Union</subject>
         <subj-group>
            <subject content-type="lcsh">Statistics, Vital</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Soviet Union</subject>
         <subj-group>
            <subject content-type="lcsh">Population policy</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Population Studies</subject>
      </subj-group>
      <book-title-group>
         <book-title>A Century of State Murder?</book-title>
         <subtitle>Death and Policy in Twentieth Century Russia</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Haynes</surname>
               <given-names>Michael</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Husan</surname>
               <given-names>Rumy</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745319308</isbn>
      <isbn content-type="epub">9781849641579</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>LONDON; STERLING, VIRGINIA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2003</copyright-year>
         <copyright-holder>Michael Haynes</copyright-holder>
         <copyright-holder>Rumy Husan</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt18fs72d"/>
      <abstract abstract-type="short">
         <p>Russia has one of the lowest rates of adult life expectancy in the world. Average life expectancy for a man in America is 74; in Russia, it is just 59. Birth rates and population levels have also plummeted. These excess levels of mortality affect all countries that formed the former Soviet bloc. Running into many millions, they raise obvious comparisons with the earlier period of forced transition under Stalin. This book seeks to put the recent history of the transition into a longer term perspective by identifying, explaining and comparing the pattern of change in Russia in the last century. It offers a sharp challenge to the conventional wisdom and benign interpretations offered in the west of what has happened since 1991. Through a careful survey of the available primary and secondary sources, Mike Haynes and Rumy Husan have produced the first and most complete and accurate account of Russian demographic crisis from the Revolution to the present. 'Combining exhaustive demographic inquiry with incisive social and political analysis, the authors record the successive phases of the trauma that Russia has endured in the 20th century, placing each in its historical context and ideological setting. A vivid and chilling account of some of the most terrible events of modern history.' Noam Chomsky 'The claim that economics and related disciplines are value-free objective sciences is now thoroughly discredited. Yet the need for humane and dispassionate scholarship in these disciplines has never been more needed. Michael Haynes and Rumy Husan have provided just this with their fine volume, A Century of State Murder? By carefully examining the available statistics, their meaning and limitations, and the conventional wisdom about Russia and the USSR of the past 100 years, i.e. before and after the fall of “communism", they have produced a detailed, balanced and clearly written account of the dreadful history of Russian demography in the regimes of this period. All of them stand condemned, for their arrogance, harshness and ignorance, as do the irresponsible advisors from the West who advocated ill-conceived institutional changes based on mistaken applications of mainstream economic theory to inappropriate situations.' G.C. Harcourt, Jesus College, Cambridge University</p>
      </abstract>
      <counts>
         <page-count count="288"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.3</book-part-id>
                  <title-group>
                     <title>List of Tables</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.4</book-part-id>
                  <title-group>
                     <title>List of Figures</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.5</book-part-id>
                  <title-group>
                     <title>Preface and Acknowledgements</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.6</book-part-id>
                  <title-group>
                     <title>The USSR in the Late Stalin Era</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.7</book-part-id>
                  <title-group>
                     <title>The Four Great Mortality Crises in Twentieth-Century USSR-Russia</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.8</book-part-id>
                  <title-group>
                     <title>Glossary and Abbreviations</title>
                  </title-group>
                  <fpage>xiv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.9</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Demography – the Social Mirror?</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Death is never fair but it comes to all of us. ‘To every thing there is season, and a time to every purpose under heaven,’ said the Old Testament author of Ecclesiastes, ‘A time to be born, and a time to die; a time to plant and a time to pluck up that which is planted ….’ But it is not this simple. Part of the unfairness of death is that too often it comes out of season, people are plucked away from us before their time. Perhaps its cause is the apparent randomness of the car crash, the glancing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.10</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Revolt Against Class Society 1890–1928</title>
                  </title-group>
                  <fpage>26</fpage>
                  <abstract>
                     <p>‘We all depart, we shall all die, monarchs and rulers, judges and potentates, rich and poor and every mortal being’, say the words of the Orthodox funeral service. In the grave, ‘man is bare bones, food for worms and stench … What have we become? What is a poor person, what a rich? What a master, what a free? Are not all ashes?’¹ In Russia before 1914 death was commonplace. With a population of some 170 million the high birth rate meant that over 5 million new lives were created each year but over 3 million were taken away, most</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.11</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Stalin, Mass Repression and Death 1929–53</title>
                  </title-group>
                  <fpage>62</fpage>
                  <abstract>
                     <p>In the years after 1928–29 the USSR was driven forward in a massive drive to industrialise. When Stalin died in 1953, this was far from complete. But a mass urban base was created and with it a heavy industrial base supporting a more modern army. This was what Stalin’s propaganda called a ‘revolution from above’. Its price was enormous coercion and loss of life. ‘Build, build, export, shoot, build … industrialisation is directed like a march through a conquered territory …’, wrote Victor Serge in the mid-1930s.¹ But the pattern of ‘normal death’ also reveals a society where, despite</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.12</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Policy, Inequalities and Death in the USSR 1953–85</title>
                  </title-group>
                  <fpage>90</fpage>
                  <abstract>
                     <p>The years between Stalin’s death and the collapse of the USSR in 1991 saw a succession of less extreme regimes under which conditions improved for the mass of the population. ‘Destalinisation’ led to a huge reduction in the level of state violence – but not its disappearance. Some of the benefits of modernisation were now allowed to filter down to the mass of the population and there were dramatic falls in the death rate and the infant mortality rate. But this relaxation and improvement was constrained by the self-interest of the wider social group on which the system rested. This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.13</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The End of Perestroika and the Transition Crisis of the 1990s</title>
                  </title-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>The latter period of Brezhnev’s reign became popularly designated as the ‘years of stagnation’. Yet, after his death in 1982, the Soviet Union’s economy continued to stagnate, first under the geriatric leaderships of Andropov (1982–84) and Chernenko (1984–85), and then, from March 1985, under the more youthful leadership of 54-year-old Mikhail Gorbachev. The deaths of three successive leaders in 1982–85 seemed to suggest that the failings of the system as a whole had become focused on the failing bodies of its leaders. Season tickets were to be issued to the laying-in-state in the Kremlin’s Hall of Columns,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.14</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>‘Normal’ Deaths During the First Decade of Transition</title>
                  </title-group>
                  <fpage>144</fpage>
                  <abstract>
                     <p>Very rarely has a sense of triumphalism turned so quickly to despair and demoralisation as it did in Russia during the first years of transition. When the transition began, perhaps the vast majority of the population were swept along with a genuine sense of optimism, and supported the system change that would see the casting aside of the authoritarian command economy and its replacement with the market system. But once the severity of the new system was exposed, a degree of resistance followed. Nonetheless, detestation of the previous regime and system did not lead to (and has not led to)</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.15</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Yeltsin, Putin and ‘Abnormal’ Deaths 1992–2002</title>
                  </title-group>
                  <fpage>176</fpage>
                  <abstract>
                     <p>Post-transition Russia has been fortunate in not suffering significant numbers of abnormal deaths that have emanated from serious disasters such as famines or Chernobyl-type explosions. But as this chapter will show, Russia has incurred very large numbers of other types of abnormal deaths – the vast majority resulting from actions of the state. The fall of the Soviet state gave a huge impetus to realising important rights and freedoms. People in Russia, like its former satellites in Central Europe, wished to become a ‘normal market economy’ or, as they would see it, just a ‘normal society’. A normal society would</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.16</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>202</fpage>
                  <abstract>
                     <p>People with handwritten signs are commonplace in Moscow. Sometimes they are asking for work, sometimes they are begging. You can easily pass them by. But sometimes an image remains. In September 2002, a girl in her teens was standing in a Moscow Metro station with a sign which read ‘<italic>Pomagaete, umiraet Mama</italic>’ – ‘Help me, my mother is dying’. An hour later she had gone, replaced by a cripple. Good pitches on the Moscow Metro have to be shared.</p>
                     <p>There has been enough said in the first part of this book to show why the system before 1991 never deserved</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.17</book-part-id>
                  <title-group>
                     <title>Appendix:</title>
                     <subtitle>Basic Data on the Prison Camp System under Stalin</subtitle>
                  </title-group>
                  <fpage>214</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.18</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>216</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.19</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>240</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs72d.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="editor" id="contrib1" xlink:type="simple">
                        <role>Compiled by</role>
                        <name name-style="western">
                           <surname>Carlton</surname>
                           <given-names>Sue</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
