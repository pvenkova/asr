<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">poprespolrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000506</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population Research and Policy Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01675923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737829</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40231001</article-id>
         <article-id pub-id-type="pub-doi">10.1007/s11113-008-9106-5</article-id>
         <title-group>
            <article-title>The Effects of Questionnaire Translation on Demographic Data and Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alexander A.</given-names>
                  <surname>Weinreb</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mariano</given-names>
                  <surname>Sana</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">28</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40008495</issue-id>
         <fpage>429</fpage>
         <lpage>454</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 Springer Science+Business Media B.V</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40231001"/>
         <abstract>
            <p>The collection of demographic data in developing and, increasingly, developed countries often requires the translation of a survey instrument. This article addresses the implications for data and analysis of two of the most common modes of translation. The first, the officially sanctioned--though not empirically verified--method, involves the pre-fieldwork production of a standardized translation of the template questionnaire into all or most languages in which interviews are expected to be conducted. The second, rarely acknowledged in the literature but quite common in the field, occurs where there is a mismatch between the language of the questionnaire available to the interviewer and the language in which the actual interview is conducted. In this case, it is up to the interviewer to translate from the language of the questionnaire to the language of the interview. Using the 1998 Kenya DHS, in which 23% of interviews were translated in this non-standardized manner, we explore the effects of the two translation modes on three indicators of measurement error and on estimated multivariate relations. In general we find that the effects of non-standardized translation on univariate statistics--including higher-order variance structures--are rather moderate. The effects become magnified, however, when multivariate analysis is used. This suggests that the advantages of--and also costs associated with--standardized translation depend on the ultimate purposes of data collection.</p>
         </abstract>
         <kwd-group>
            <kwd>Questionnaire translation</kwd>
            <kwd>Standardized interview</kwd>
            <kwd>Data collection</kwd>
            <kwd>Field methods</kwd>
            <kwd>Developing countries</kwd>
            <kwd>Kenya</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d718e181a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d718e188" publication-type="other">
(Fowler and Mangione 1990).</mixed-citation>
            </p>
         </fn>
         <fn id="d718e195a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d718e202" publication-type="other">
http://kenya.pop.upenn.edu</mixed-citation>
            </p>
         </fn>
         <fn id="d718e209a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d718e216" publication-type="other">
Snijders and Bosker (1999).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d718e222" publication-type="other">
Snijders and Bosker 2003(1999), p. 56</mixed-citation>
            </p>
         </fn>
         <fn id="d718e229a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d718e236" publication-type="other">
Coast et al. 2007</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d718e252a1310">
            <mixed-citation id="d718e256" publication-type="other">
Aquilino, W. S. (1994). Interview mode effects in surveys of drug and alcohol use. Public Opinion
Quarterly, 58, 210-240.</mixed-citation>
         </ref>
         <ref id="d718e266a1310">
            <mixed-citation id="d718e270" publication-type="other">
Axinn, W. G., Fricke, T. E., &amp; Thornton, A. (1991). The microdemographic community-study approach:
Improving survey data by integrating the ethnographic method. Sociological Methods &amp; Research,
20, 187-217. doi:10.1177/0049124191020002001.</mixed-citation>
         </ref>
         <ref id="d718e283a1310">
            <mixed-citation id="d718e287" publication-type="other">
Babbie, E. (2006). The practice of social research (11th ed.). Belmont, CA: Wadsworth Publishing.</mixed-citation>
         </ref>
         <ref id="d718e294a1310">
            <mixed-citation id="d718e298" publication-type="other">
Becker, S., Feyistan, K., &amp; Makinwa-Adebusoye, P. (1995). The effect of sex of interviewers on the
quality of data in a Nigerian family planning questionnaire. Studies in Family Planning, 26, 233-
240. doi: 10.2307/2137848.</mixed-citation>
         </ref>
         <ref id="d718e312a1310">
            <mixed-citation id="d718e316" publication-type="other">
Bernard, H. R. (2000). Social research methods: Qualitative and quantitative approaches. Thousand
Oaks, CA: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d718e326a1310">
            <mixed-citation id="d718e330" publication-type="other">
Chen, A., Liu, Z., &amp; Ennis, C. D. (1997). Universality and uniqueness of teacher educational value
orientations: A cross-cultural comparison between USA and China. Journal of Research and
Development in Education, 30, 135-143.</mixed-citation>
         </ref>
         <ref id="d718e343a1310">
            <mixed-citation id="d718e347" publication-type="other">
Coast, E.E., Randall, S., Leone, T., &amp; Bishop, B. (2007). The commodity chain of the household: From
survey design to policy planning. Presented at the 2007 meetings of the Union of African Population
Science (UAPS), Arusha, Tanzania, December 10-14.</mixed-citation>
         </ref>
         <ref id="d718e360a1310">
            <mixed-citation id="d718e364" publication-type="other">
Converse, J. M., &amp; Presser, S. (1986). Survey questions: Handcrafting the standardized questionnaire.
Thousand Oaks, CA: Sage Publications, Sage Series of Quantitative Applications in the Social
Sciences, No. 63.</mixed-citation>
         </ref>
         <ref id="d718e377a1310">
            <mixed-citation id="d718e381" publication-type="other">
Cordell, D. D., Gregory, J. W., &amp; Piché, V. (1996). Hoe and wage: A social history of a circular
migration system in West Africa. Boulder, CO: Westview Press.</mixed-citation>
         </ref>
         <ref id="d718e391a1310">
            <mixed-citation id="d718e395" publication-type="other">
Creswell, J. W. (2002). Research design: Qualitative, quantitative, and mixed methods approaches (2nd
ed.). Thousand Oaks, CA: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d718e406a1310">
            <mixed-citation id="d718e410" publication-type="other">
Delavande, A., &amp; Kohler, H.-P.(2007). Subjective expectations in the context of HIV/AIDS in Malawi.
University of Pennsylvania, Population Aging Research Center, PARC Working Paper 07-06.</mixed-citation>
         </ref>
         <ref id="d718e420a1310">
            <mixed-citation id="d718e424" publication-type="other">
Dijkstra, W. (1987). Interviewing style and respondent behavior: an experimental study of the survey-
interview. Sociological Methods &amp; Research, 16, 309-334. doi:10.1177/0049124187016002006.</mixed-citation>
         </ref>
         <ref id="d718e434a1310">
            <mixed-citation id="d718e438" publication-type="other">
Fowler, F. J., &amp; Mangione, T. W. (1990). Standardized survey interviewing: minimizing interviewer-
related error. Newbury Park, CA: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d718e448a1310">
            <mixed-citation id="d718e452" publication-type="other">
Groves, R. M. (1989). Survey errors and survey costs. New York: John Wiley &amp; Sons.</mixed-citation>
         </ref>
         <ref id="d718e459a1310">
            <mixed-citation id="d718e463" publication-type="other">
Helleringer, S., &amp; Kohler, H.-P. (2005). Social networks, risk perceptions and changing attitudes towards
HIV/AIDS: New evidence from a longitudinal study using fixed-effect estimation. Population
Studies, 59(3), 265-282. doi: 10. 1080/00324720500212230.</mixed-citation>
         </ref>
         <ref id="d718e476a1310">
            <mixed-citation id="d718e480" publication-type="other">
Maynard, D. W., &amp; Schaeffer, N. C. (2002). Standardization and its discontents. In D. W. Maynard, H.
Houtkoop-Steenstra, N. C. Schaeffer &amp; H. van der Zouwen (Eds.), Standardization and tacit
knowledge: Interaction and practice in the survey interview (pp. 3-45). New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d718e494a1310">
            <mixed-citation id="d718e498" publication-type="other">
Mensch, B. S., Hewett, P. C., &amp; Erulkar, A. S. (2003). The reporting of sensitive behavior among
adolescents: a methodological experiment in Kenya. Demography, 40, 247-268. doi:10. 1353/dem.
2003.0017.</mixed-citation>
         </ref>
         <ref id="d718e511a1310">
            <mixed-citation id="d718e515" publication-type="other">
Mitchell, R. E. (1965). Survey materials collected in the developing countries: Sampling, measurement
and interviewing obstacles in international comparisons. International Social Science Journal, 17,
665-685.</mixed-citation>
         </ref>
         <ref id="d718e528a1310">
            <mixed-citation id="d718e532" publication-type="other">
Overton, J., &amp; van Dierman, P. (2003). Using quantitative techniques. In R. Scheyvens &amp; D. Storey
(Eds.), Development fieldwork: A practical guide (pp. 37-56). Thousand Oaks, CA: Sage
Publications.</mixed-citation>
         </ref>
         <ref id="d718e545a1310">
            <mixed-citation id="d718e549" publication-type="other">
Plummer, M. L., Wight, D., Ross, D., Balira, R., et al. (2004). Asking semi-literate adolescents about
sexual behaviour: the validity of assisted self-completion questionnaire (ASCQ) data in rural
Tanzania. Tropical Medicine &amp; International Health, 9, 737-754. doi: 10.1111/j.1365-3156.
2004.01254.x.</mixed-citation>
         </ref>
         <ref id="d718e565a1310">
            <mixed-citation id="d718e569" publication-type="other">
Sana, M, &amp; Weinreb, A. A. (2008). Insiders, outsiders, and the editing of inconsistent survey data.
Sociological Methods &amp; Research, 36, 515-541. doi: 10.1177/0049124107313857.</mixed-citation>
         </ref>
         <ref id="d718e579a1310">
            <mixed-citation id="d718e583" publication-type="other">
Sapsford, R., &amp; Jupp, V. (2006). Data collection and analysis (2nd ed.). Thousand Oaks, CA: Sage
Publications.</mixed-citation>
         </ref>
         <ref id="d718e594a1310">
            <mixed-citation id="d718e598" publication-type="other">
Schaeffer, N. C, &amp; Presser, S. (2003). The science of asking questions. Annual Review of Sociology, 29,
65-88. doi: 10. 1146/annurev.soc.29.110702.110112.</mixed-citation>
         </ref>
         <ref id="d718e608a1310">
            <mixed-citation id="d718e612" publication-type="other">
Schober, M, &amp; Conrad, F. G. (1997). Does conversational interviewing reduce survey measurement
error? Public Opinion Quarterly, 61, 576-602. doi: 10. 1086/297818.</mixed-citation>
         </ref>
         <ref id="d718e622a1310">
            <mixed-citation id="d718e626" publication-type="other">
Snijders, T.A.B., &amp; Bosker, R.J. (2003(1999)). Multilevel analysis: an introduction to basic and
advanced multilevel modeling. Sage Publications.</mixed-citation>
         </ref>
         <ref id="d718e636a1310">
            <mixed-citation id="d718e640" publication-type="other">
Suchman, L., &amp; Jordan, B. (1990). Interactional troubles in face-to-face survey interviews. Journal of the
American Statistical Association, 85, 232-253. doi: 10.2307/2289550.</mixed-citation>
         </ref>
         <ref id="d718e650a1310">
            <mixed-citation id="d718e654" publication-type="other">
Ware, H. (1977). Language problems in demographic field work in Africa: The case of the Cameroon
Fertility Survey. London: International Statistical Institute &amp; World Fertility Survey, Scientific
Report No. 2.</mixed-citation>
         </ref>
         <ref id="d718e667a1310">
            <mixed-citation id="d718e671" publication-type="other">
Weinreb, A. A. (2006). The limitations of stranger-interviewers in rural Kenya. American Sociological
Review, 71(6), 1014-1039.</mixed-citation>
         </ref>
         <ref id="d718e682a1310">
            <mixed-citation id="d718e686" publication-type="other">
Zeller, R. A., &amp; Carmines, E. G. (1980). Measurement in the social sciences: The link between theory and
data. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
