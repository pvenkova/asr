<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">waterlines</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50017770</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Waterlines</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Practical Action Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02628104</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17563488</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24687553</article-id>
         <title-group>
            <article-title>Moving from efficacy to effectiveness: using behavioural economics to improve the impact of WASH interventions</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>AIDAN</given-names>
                  <surname>COVILLE</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>VICTOR</given-names>
                  <surname>OROZCO</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">33</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24687548</issue-id>
         <fpage>26</fpage>
         <lpage>34</lpage>
         <permissions>
            <copyright-statement>Copyright © Practical Action Publishing 2014</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24687553"/>
         <abstract>
            <p>Biological plausibility and randomized controlled efficacy trials justify the importance of clean and accessible water, improved sanitation, and hygiene in reducing morbidity and mortality rates. However, most health impacts can only take place if people use the improved services and practise hygienic behaviour. Despite considerable efforts in increasing access to water and sanitation services and promoting hygiene practices, few people chlorinate their water, wash their hands, and connect to sanitation systems, limiting the potential health impacts of development programmes. This paper explores cognitive and behavioural constraints identified in the behavioural economics literature and how interventions have successfully accounted for these constraints in their design to increase demand for services and positive habit formation. We then provide stylized examples of using behavioural economics solutions when framing information in hygiene campaigns, using new technologies to remind individuals to wash their hands at critical junctures, automating water purification processes, and designing 'smart' sanitation subsidies as practical opportunities for practitioners to incorporate these insights into project design to achieve greater impact.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d197e126a1310">
            <mixed-citation id="d197e130" publication-type="other">
Ashraf, N., Karlan, D. and Yin, W. (2006) 'Tying Odysseus to the mast: evidence from a
commitment savings product in the Philippines', The Quarterly Journal of Economics 121(2):
635-72 &lt;http://dx.doi.Org/10.1162/qjec.2006.121.2.635&gt;.</mixed-citation>
         </ref>
         <ref id="d197e143a1310">
            <mixed-citation id="d197e147" publication-type="other">
Banerjee, A.V., Duflo, E., Glennerster, R. and Kothari, D. (2010) 'Improving immunisation
coverage in niral India: clustered randomised controlled evaluation of immunisation
campaigns with and without incentives', British Medical Journal 340: c2220 &lt;http://dx.doi.
org/10.1136/bmj.c2220&gt;.</mixed-citation>
         </ref>
         <ref id="d197e163a1310">
            <mixed-citation id="d197e167" publication-type="other">
Brune, L., Gine, X., Goldberg, J. and Yang, D. (2011) Commitments to Save: A Field Experiment in
Rural Malawi, World Bank Policy Research Working Paper no. 5748, August 2011, Washington,
DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d197e180a1310">
            <mixed-citation id="d197e184" publication-type="other">
Cameron, L., Olivia, S. and Shah, M. (2013) Impact Evaluation of a Large-scale Rural Sanitation
Project in Indonesia, Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d197e195a1310">
            <mixed-citation id="d197e199" publication-type="other">
Chase, C. and Do, Q.T. (2012) Handwashing Behavior Change at Scale: Evidence from a Randomized
Evaluation in Vietnam, World Bank Policy Research Working Paper no. 6207, Washington, DC:
World Bank.</mixed-citation>
         </ref>
         <ref id="d197e212a1310">
            <mixed-citation id="d197e216" publication-type="other">
Chetty, R., Friedman, J.N., Leth-Petersen, S., Nielsen, T. and Olsen, T. (2012) Active vs. Passive
Decisions and Crowdout in Retirement Savings Accounts: Evidence from Denmark (No. wl8565),
Cambridge, MA: National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d197e229a1310">
            <mixed-citation id="d197e233" publication-type="other">
Choi, J.J., Laibson, D., Madrian, B.C. and Metrick, A. (2004) 'For better or for worse: default
effects and 401 (k) savings behavior', in D.A. Wise (ed.), Perspectives on the Economics of Aging,
pp. 81-126, Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d197e246a1310">
            <mixed-citation id="d197e250" publication-type="other">
Clapp, J.D. and McDonnell, A.L. (2000) 'The relationship of perceptions of alcohol promotion
and peer drinking norms to alcohol problems reported by college students', journal of College
Student Development 41(1): 19-26.</mixed-citation>
         </ref>
         <ref id="d197e263a1310">
            <mixed-citation id="d197e267" publication-type="other">
Clasen, T.F., Bostoen, K., Schmidt, W.P., Boisson, S., Fung, I.C.H., Jenkins, M.W. and Cairncross, S.
(2010) 'Interventions to improve disposal of human excreta for preventing diarrhoea', Cochrane
Database of Systematic Reviews 6 &lt;http://dx.doi.org/10.1002/14651858.CD007180.pub2&gt;.</mixed-citation>
         </ref>
         <ref id="d197e280a1310">
            <mixed-citation id="d197e284" publication-type="other">
Datta, S. and Mullainathan, S. (2012) Behavioral Design: A New Approach to Development Policy,
CGD Policy Paper 016, Washington DC: Center for Global Development.</mixed-citation>
         </ref>
         <ref id="d197e295a1310">
            <mixed-citation id="d197e299" publication-type="other">
DellaVigna, S. (2007) Psychology and Economics: Evidence from the Field (No. wl3420), Cambridge,
MA: National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d197e309a1310">
            <mixed-citation id="d197e313" publication-type="other">
Devoto, F., Duflo, E., Dupas, P., Pariente, W. and Pons, V. (2011) Happiness on Tap: Piped Water
Adoption in Urban Morocco (No. wl6933), Cambridge, MA: National Bureau of Economic
Research.</mixed-citation>
         </ref>
         <ref id="d197e326a1310">
            <mixed-citation id="d197e330" publication-type="other">
Gächter, S., Orzen, H., Renner, E. and Starmer, C. (2009) 'Are experimental economists prone
to framing effects? A natural field experiment', Journal of Economic Behavior &amp; Organization
70(3): 443-46 &lt;http://dx.doi.Org/10.1016/j.jebo.2007.ll.003&gt;.</mixed-citation>
         </ref>
         <ref id="d197e343a1310">
            <mixed-citation id="d197e347" publication-type="other">
Galiani, S., Gertler, P. and Orsola-Vidal, A. (2012) Promoting Handwashing Behavior in Peru: The
Effect of Large-scale Mass-media and Community Level Interventions, World Bank Policy Research
Working Paper, 6257, Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d197e360a1310">
            <mixed-citation id="d197e364" publication-type="other">
Hammer, J.S. and Spears, D. (2013) Village Sanitation and Children's Human Capital: Evidence
from a Randomized Experiment by the Maharashtra Government, World Bank Policy Research
Working Paper, 6580, Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d197e377a1310">
            <mixed-citation id="d197e381" publication-type="other">
Kahneman, K. and Tversky, A. (1979) 'Prospect theory: an analysis of decision under risk',
Ecotiometrica 47(2): 263-91.</mixed-citation>
         </ref>
         <ref id="d197e392a1310">
            <mixed-citation id="d197e396" publication-type="other">
Karlan, D., Ratan, A.L. and Zinman, J. (2013) Savings by and for the Poor: A Research Review and
Agenda (No. 1027), working paper.</mixed-citation>
         </ref>
         <ref id="d197e406a1310">
            <mixed-citation id="d197e410" publication-type="other">
Kremer, M., Miguel, E., Mullainathan, S., Null, C. and Zwane, A. (2009) Making Water Safe:
Price, Persuasion, Peers, Promoters, or Product Design? Working paper.</mixed-citation>
         </ref>
         <ref id="d197e420a1310">
            <mixed-citation id="d197e424" publication-type="other">
Kremer, M., Miguel, E., Mullainathan, S., Null, C. and Zwane, A.P. (2011) Social Engineering:
Evidence from a Suite of Take-up Experiments in Kenya. Working Paper.</mixed-citation>
         </ref>
         <ref id="d197e434a1310">
            <mixed-citation id="d197e438" publication-type="other">
Luby, S.P., Agboatwalla, M., Painter, J., Altaf, A., Billhimer, W.L. and Hoekstra, R.M. (2004)
'Effect of intensive handwashing promotion on childhood diarrhea in high-risk communities
in Pakistan', Journal of the American Medical Association 291(21): 2547-54 &lt;http://dx.doi.
org/10.1001/jama.291.21.2547&gt;.</mixed-citation>
         </ref>
         <ref id="d197e454a1310">
            <mixed-citation id="d197e458" publication-type="other">
Luoto, J., Levine, D. and Albert, J. (2011) Information and persuasion: achieving safe water
behaviors in Kenya, RAND Working Paper Series No. WR-885 [online], SSRN &lt;http://ssrn.com/
abstract=1980292&gt; or &lt;http://dx.doi.org/10.2139/ssrn.1980292&gt; [accessed 2 December 2013].</mixed-citation>
         </ref>
         <ref id="d197e471a1310">
            <mixed-citation id="d197e475" publication-type="other">
Montgomery, M.A., Desai, M.M. and Elimelech, M. (2010) 'Assessment of latrine use and quality
and association with risk of trachoma in rural Tanzania', Transactions of the Royal Society of
Tropical Medicine and Hygiene 104(4): 283-89 &lt;http://dx.doi.Org/10.1016/j.trstmh.2009.10.009&gt;.</mixed-citation>
         </ref>
         <ref id="d197e489a1310">
            <mixed-citation id="d197e493" publication-type="other">
Pop-Eleches, C., Thirumurthy, H., Habyarimana, J., Graff Zivin, J., Goldstein, M., de Walque,
D. and Bangsberg, D. (2011) 'Mobile phone technologies improve adherence to antiretro-
viral treatment in resource-limited settings: a randomized controlled trial of text message
reminders', Aids 25: 825-34 &lt;http://dx.doi.org/10.1097/QAD.0b013e32834380cl&gt;.</mixed-citation>
         </ref>
         <ref id="d197e509a1310">
            <mixed-citation id="d197e513" publication-type="other">
Schultz, P.W., Nolan, J.M., Cialdini, R.B., Goldstein, N.J. and Griskevicius, V. (2007) 'The
constmctive, destmctive, and reconstructive power of social norms', Psychological Science 18(5):
429-34 &lt;http://dx.doi.Org/10.llll/j.1467-9280.2007.01917.x&gt;.</mixed-citation>
         </ref>
         <ref id="d197e526a1310">
            <mixed-citation id="d197e530" publication-type="other">
Thaler, R.H. (1989) 'Anomalies: inter-industry wage differentials', The Journal of Economic
Perspectives 3(2): 181-93.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
