<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">populationenviro</journal-id>
         <journal-id journal-id-type="jstor">j50000178</journal-id>
         <journal-title-group>
            <journal-title>Population and Environment</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn pub-type="ppub">01990039</issn>
         <issn pub-type="epub">15737810</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40212352</article-id>
         <article-id pub-id-type="pub-doi">10.1007/s11111-008-0065-x</article-id>
         <title-group>
            <article-title>AIDS and Kitchen Gardens: Insights from a Village in Western Kenya</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Laura L.</given-names>
                  <surname>Murphy</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3/5</issue>
         <issue-id>i40007216</issue-id>
         <fpage>133</fpage>
         <lpage>161</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Springer Science+Business Media, LLC</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/40212352"/>
         <abstract>
            <p>In rural Africa, indigenous farming and natural resource management Systems exemplified by kitchen gardens are being reshaped by the HIV/AIDS epidemic and its negative impacts (illness, Stigma and mortality, and economic costs) and positive opportunities (organizational responses to the epidemic). Subtle changes in crops and farm techniques can be traced to these diverse influences of HIV+ infection, illness, mortality, widowhood, fester child care, and AIDS support groups, as well as the organizations, ideas, and flow of funding from outside. These findings draw on original field data: a village census, in-depth interviews with gardeners, and group discussions in a village in Bungoma District (in 2005 and 2007). This part of western Kenya is a typical small-farm zone that has faced a moderate HIV/AIDS epidemic since the 1990s, following decades of demographic, environmental, technological, and institutional changes. Implications of this case study for further research on HIV/AIDS and on micro-level population-environment change suggest that households are useful but imperfect analytical units and are best seen as part of complex social networks, shaping connections to markets. These important "mediating institutions" link AIDS as a demographic and economic force with environmental outcomes in cultivated landscapes.</p>
         </abstract>
         <kwd-group>
            <kwd>Africa</kwd>
            <kwd>Community-based technology</kwd>
            <kwd>Gardens</kwd>
            <kwd>HIV/AIDS</kwd>
            <kwd>Livelihoods</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d19e174a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d19e181" publication-type="other">
ACE-Africa (www.ace-africa.org)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d19e187" publication-type="other">
(ACE-Africa 2004).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d19e203a1310">
            <mixed-citation id="d19e207" publication-type="other">
ACE-Africa. (2004). Training manual for agriculture and nutrition for HIV/AIDS management.
Supporting infected and affected. Compiled by Action in the Community Environment (ACE)-
Africa, Ministry of Agriculture, Ministry of Health. Mimeograph, Bungoma, Kenya.</mixed-citation>
         </ref>
         <ref id="d19e220a1310">
            <mixed-citation id="d19e224" publication-type="other">
ActionAid. (2005). Food security and HIV and AIDS in Southern Africa: Case studies and implications
for future policy. Action Aid International--Southern African Partnership Programme. Johannes-
burg. (Available for download from http://www.sarpn.org).</mixed-citation>
         </ref>
         <ref id="d19e237a1310">
            <mixed-citation id="d19e241" publication-type="other">
Allison, E. H., &amp; Seeley, J. A. (2004). HIV and AIDS among fisherfolk: A threat to 'responsible
fisheries'? Fish and Fisheries, 5(3), 215-234.</mixed-citation>
         </ref>
         <ref id="d19e251a1310">
            <mixed-citation id="d19e255" publication-type="other">
Barany, M., Hammett, A. L., Stadler, K. M., &amp; Kengni, E. (2004a). Non timber forest products in the food
security and nutrition of smallholders afflicted by HIV/AIDS in Sub-Saharan Africa. Forests, Trees,
and Livelihoods, 14, 3-18.</mixed-citation>
         </ref>
         <ref id="d19e269a1310">
            <mixed-citation id="d19e273" publication-type="other">
Barany, M., Sitoe, A., Kayambazinthu, D., &amp; Anyonge, C. H. (2004b). HIV/AIDS and the Miombo
Woodlands of Mozambique &amp; Malawi: An exploratory study. Food and Agriculture Organization
(FAO), Department of Forestry. Presentation prepared for the Annual Meeting of Conservation
International, May 2004, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d19e289a1310">
            <mixed-citation id="d19e293" publication-type="other">
Barnett, T., &amp; Whiteside, A. (2003). AIDS in the 21st century. Disease and globalization. Palgrave
Macmillan (revised edition).</mixed-citation>
         </ref>
         <ref id="d19e303a1310">
            <mixed-citation id="d19e307" publication-type="other">
Baylies, C. (2002). The impact of AIDS on rural households in Africa: A shock like any other?
Development and Change, 33(4), 611-632.</mixed-citation>
         </ref>
         <ref id="d19e317a1310">
            <mixed-citation id="d19e321" publication-type="other">
Bishop-Sambrook, C., &amp; Tanzern, N. (2004). The susceptibility and vulnerability of small-scale fishing
communities to HIV/AIDS in Uganda. FAO and GTZ. http://www.fao.org/hivaids.</mixed-citation>
         </ref>
         <ref id="d19e331a1310">
            <mixed-citation id="d19e335" publication-type="other">
Blaikie, P., &amp; Brookfield, H. (1987). Land degradation and society. Routledge Press.</mixed-citation>
         </ref>
         <ref id="d19e342a1310">
            <mixed-citation id="d19e346" publication-type="other">
BOSTID. (1996). Lost crops of Africa (Vol. 1). Washington DC: National Academy Press.</mixed-citation>
         </ref>
         <ref id="d19e354a1310">
            <mixed-citation id="d19e358" publication-type="other">
Bryceson, D (2000). Rural Africa at the crossroads: Livelihood practises and policies. Natural Resources
Perspectives, ODI, No. 52, April 2000. (http://www.odi.org.uk).</mixed-citation>
         </ref>
         <ref id="d19e368a1310">
            <mixed-citation id="d19e372" publication-type="other">
Bryceson, D. F., &amp; Fonseca, J. (2006). Risking death for survival: Peasant responses to hunger and HIV/
AIDS in Malawi. World Development, 34(9), 1654-1666.</mixed-citation>
         </ref>
         <ref id="d19e382a1310">
            <mixed-citation id="d19e386" publication-type="other">
Cleaver, K., &amp; Schreiber, G. A. (1994). Reversing the spiral: The population, agriculture and
environment nexus in Sub-Saharan Africa. Washington DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d19e396a1310">
            <mixed-citation id="d19e400" publication-type="other">
Conelly, W. T., &amp; Chaiken, M. (2000). Agro-diversity and food security under conditions of extreme
population pressure in Western Kenya. Human Ecology, 28(1).</mixed-citation>
         </ref>
         <ref id="d19e410a1310">
            <mixed-citation id="d19e414" publication-type="other">
DAI/USAID. (2005). AIDS brief for sectoral planners and managers. Community-based natural resource
management. Washington DC.</mixed-citation>
         </ref>
         <ref id="d19e424a1310">
            <mixed-citation id="d19e428" publication-type="other">
De Sherbinin, A. (2006). Rural household micro-demographics, livelihoods and the environment.
Background paper. Population-Environment Research Network Cyberseminar. 10-24 April 2006.
http://www.populationenvironmentresearch.org.</mixed-citation>
         </ref>
         <ref id="d19e442a1310">
            <mixed-citation id="d19e446" publication-type="other">
De Waal, A. (2006). AIDS and power. Why there is no political crisis yet. London: Zed Books.</mixed-citation>
         </ref>
         <ref id="d19e453a1310">
            <mixed-citation id="d19e457" publication-type="other">
De Waal, A., &amp; Whiteside, A. (2003). New Variant Famine." Lancet. October 11, 2003.</mixed-citation>
         </ref>
         <ref id="d19e464a1310">
            <mixed-citation id="d19e468" publication-type="other">
DFID. (2003). The impact of HIV/AIDS on rural livelihoods in Kenya: Workshop on the impact of HIV/
AIDS on land rights. Nairobi: Ministry of Lands and Settlement.</mixed-citation>
         </ref>
         <ref id="d19e478a1310">
            <mixed-citation id="d19e482" publication-type="other">
DHS. (2004). Kenya demographic and health survey. Columbia, MD: Macro.</mixed-citation>
         </ref>
         <ref id="d19e489a1310">
            <mixed-citation id="d19e493" publication-type="other">
Drimie, S. (2002). The impact of HIV/AIDS on rural households and land issues in Southern and Eastern
Africa: A background paper prepared for the Food and Agricultural Organization, Sub-Regional
Office for Southern and Eastern Africa, FAO.</mixed-citation>
         </ref>
         <ref id="d19e506a1310">
            <mixed-citation id="d19e510" publication-type="other">
Dominguez, C., Jones, R., &amp; Waterhouse, R. (2004). The impact of HIV/AIDS on seed security in
Southern Mozambique. Unpublished monograph, International Crops Research Institute for the
Semi-Arid Tropics (ICRISAT).</mixed-citation>
         </ref>
         <ref id="d19e524a1310">
            <mixed-citation id="d19e528" publication-type="other">
Donovan, C., &amp; Mather, D. (2004) Collection and analysis of cross-sectional household survey data on
rural morbidity and mortality: Lessons learned from initial surveys. Monograph. Department of
Agricultural Economics, Michigan State University, East Lansing, Michigan. Online at &lt;
http://www.aec.msu.edu/agecon/fs2/ &gt;.</mixed-citation>
         </ref>
         <ref id="d19e544a1310">
            <mixed-citation id="d19e548" publication-type="other">
Dwasi, J. (2002). Presentation at African biodiversity conservation Group, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="d19e555a1310">
            <mixed-citation id="d19e559" publication-type="other">
Ellis, F., &amp; Freeman, H. (2004). Rural livelihoods and poverty reduction strategies in four African
countries. Journal of Development Studies, 40(4), 1-30.</mixed-citation>
         </ref>
         <ref id="d19e569a1310">
            <mixed-citation id="d19e573" publication-type="other">
Fairhead, J., &amp; Leach, M. (1996). Misreading the African landscape. Society and ecology in a forest-
Savanna mosaic. Cambridge.</mixed-citation>
         </ref>
         <ref id="d19e583a1310">
            <mixed-citation id="d19e587" publication-type="other">
FAO. (1995). The effects of HIV/AIDS on farming systems in eastern Africa. Rome: Food and Agriculture
Organisation. Online at http://www.fao.org/hivaids.</mixed-citation>
         </ref>
         <ref id="d19e597a1310">
            <mixed-citation id="d19e601" publication-type="other">
FAO. (2004). HIV/AIDS, gender inequality and rural livelihoods. The impact of HIV/AIDS on rural
livelihoods in Northern Province, Zambia, FAO, pp. 1-107.</mixed-citation>
         </ref>
         <ref id="d19e612a1310">
            <mixed-citation id="d19e616" publication-type="other">
FASAZ. (2003). Interlinkages between HIV/AIDS, agriculture production and food security, Southern
Province, Zambia. Baseline Survey Report. Farming Systems Association of Zambia (FASAZ).
http://www.fao.org/hivaids.</mixed-citation>
         </ref>
         <ref id="d19e629a1310">
            <mixed-citation id="d19e633" publication-type="other">
Faust, K., Entwistle, B., Rindfuss, R. R, Walsh, S. J., &amp; Sawangdee, Y. (1999). Spatial arrangements of
social and economic networks in Nang Rong. Social Networks, 21, 311-337.</mixed-citation>
         </ref>
         <ref id="d19e643a1310">
            <mixed-citation id="d19e647" publication-type="other">
Ferguson, J. (1992). The anti-politics machine: "Development," depoliticization, and bureaucratic power
in Lesotho. University of Minnesota Press.</mixed-citation>
         </ref>
         <ref id="d19e657a1310">
            <mixed-citation id="d19e661" publication-type="other">
Francis, E. (2000). Making a living: Changing livelihoods in rural Africa. Routledge Press.</mixed-citation>
         </ref>
         <ref id="d19e668a1310">
            <mixed-citation id="d19e672" publication-type="other">
Fox, M., Rosen, S., MacLeod, W., Wasunna, M., Bi, M., Foglia, G., &amp; Simon, J. (2004). The impact of
HIV/AIDS on labour productivity in Kenya. Tropical Medicine and International Health, 9(3), 318-
324.</mixed-citation>
         </ref>
         <ref id="d19e685a1310">
            <mixed-citation id="d19e689" publication-type="other">
Future Harvest. (2005). With time running out, scientists attempt rescue of African vegetable crops.
Future Harvest website: http://www.futureharvest.org/earth/leafy_futures.html (accessed Nov. 20,
2005).</mixed-citation>
         </ref>
         <ref id="d19e703a1310">
            <mixed-citation id="d19e707" publication-type="other">
Gari, J. (2004). Agrobiodiversity strategies to combat food insecurity and HIV/AIDS impact on rural
Africa. Advancing grassroots responses for nutrition, health and sustainable livelihoods. FAO
Population and Development Service, Rome. http://www.fao.org/hivaids.</mixed-citation>
         </ref>
         <ref id="d19e720a1310">
            <mixed-citation id="d19e724" publication-type="other">
Gillespie, S., &amp; Kandiyala, S. (2005). HTV/AIDS and food and nutrition security: From evidence to
action, Food policy review 7, IFPRI, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d19e734a1310">
            <mixed-citation id="d19e738" publication-type="other">
GOK. (2002). District development plan, Bungoma 2002-8. Nairobi, Kenya, Government Printers.</mixed-citation>
         </ref>
         <ref id="d19e745a1310">
            <mixed-citation id="d19e749" publication-type="other">
GOK. (2006). National HIV/AIDS situation. Nairobi, Kenya, Government of Kenya.</mixed-citation>
         </ref>
         <ref id="d19e756a1310">
            <mixed-citation id="d19e760" publication-type="other">
Gori, F. (2005). Fish frame project in Lake Victoria Beaches. Internal study report for "crops, cellphones
and T-cells project". Homa Bay, Kenya.</mixed-citation>
         </ref>
         <ref id="d19e770a1310">
            <mixed-citation id="d19e774" publication-type="other">
HSRC. (2003). Mitigation of HIV/AIDS impacts through agricultural and rural development, success
stories and future actions. Human Services Research Council (HSRC) Workshop Report, May 27-
29, Pretoria, South Africa. Available online at http://www.sarpn.or.za (workshops).</mixed-citation>
         </ref>
         <ref id="d19e788a1310">
            <mixed-citation id="d19e792" publication-type="other">
Hunter, L., &amp; Twine W. (2005). Adult mortality and household dietary use of the local environment:
Qualitative evidence from the Agincourt Field site in rural South Africa. Working Paper, Research
Program on Environment and Behavior. Institute of Behavioral Science (IBS), University of
Colorado.</mixed-citation>
         </ref>
         <ref id="d19e808a1310">
            <mixed-citation id="d19e812" publication-type="other">
Hunter, L., Twine, W., &amp; Johnson, A. (2005). Population dynamics and the environment: examining the
natural resource context of the African HIV/AIDS Pandemic. Working Paper, Research Program on
Environment and Behavior. Institute of Behavioral Science (IBS), University of Colorado, July
2005.</mixed-citation>
         </ref>
         <ref id="d19e828a1310">
            <mixed-citation id="d19e832" publication-type="other">
Iliffe, J. (2006). The African AIDS epidemic: A history. Ohio University Press.</mixed-citation>
         </ref>
         <ref id="d19e839a1310">
            <mixed-citation id="d19e843" publication-type="other">
Lado, C. (2004). Sustainable environmental resource utilization: A case study of farmers' ethnobotanical
knowledge and rural change in Bungoma District, Kenya. Applied Geography, 24, 281-302.</mixed-citation>
         </ref>
         <ref id="d19e853a1310">
            <mixed-citation id="d19e857" publication-type="other">
Liu, J., Daily, G., Ehrlich, P., &amp; Luck, G. (2003). Effects of household dynamics on resource consumption
and biodiversity. Nature, 421, 530-533.</mixed-citation>
         </ref>
         <ref id="d19e867a1310">
            <mixed-citation id="d19e871" publication-type="other">
Loevinsohn, M., &amp; Gillespie, S. (2004). HIV/AIDS, food security, and rural livelihoods. Understanding
and Responding. IFPRI FCND Discussion Paper no. 157. http://www.ifpri.org.</mixed-citation>
         </ref>
         <ref id="d19e882a1310">
            <mixed-citation id="d19e886" publication-type="other">
Mather, D., Donovan, C., Jayne, T., Weber, M., Mazhangara, E., Bailey, L., Yoo, K., Yamano, T., &amp;
Mghenyi, E. (2004). A cross-country analysis of household responses to adult morality in rural Sub-
Saharan Africa: Implications for HIV/AIDS mitigation and rural development policies. Working
Paper, Department of Agricultural Economics, Michigan State University, East Lansing, Michigan.
Online at &lt;http://www.aec.msu.edu/agecon/fs2/&gt;.</mixed-citation>
         </ref>
         <ref id="d19e905a1310">
            <mixed-citation id="d19e909" publication-type="other">
Mauambeta, D. (2003). Conservation in crisis: HIV/AIDS impacts and conservation-based mitigation
measures. Workshop Presentation at the 40th Anniversary Symposium. College of Africa Wildlife
Management, Mweka, Tanzania, Dec 10-12, 2003.</mixed-citation>
         </ref>
         <ref id="d19e922a1310">
            <mixed-citation id="d19e926" publication-type="other">
Maundu, P. (1999). The status of traditional vegetable utilization in Kenya. In Traditional African
Vegetables. Proceedings of the IPGRI International Workshop on Genetic Resources of Traditional
Vegetables in Africa. Nairobi, Kenya, August 1995.</mixed-citation>
         </ref>
         <ref id="d19e939a1310">
            <mixed-citation id="d19e943" publication-type="other">
Murphy, L., Harvey, P., &amp; Silvestre, E. (2005). How do we know what we know about the impact of
AIDS on food and livelihood insecurity? A review of empirical research from rural sub Saharan
Africa. Human Organization, 64(3), 265-275.</mixed-citation>
         </ref>
         <ref id="d19e956a1310">
            <mixed-citation id="d19e960" publication-type="other">
Murphy, L., Kassam, A., &amp; Kesekwa, D. (2006). Innovation and adaptation in home gardens in AIDS-
affected rural Kenya. Unpublished project manuscript.</mixed-citation>
         </ref>
         <ref id="d19e970a1310">
            <mixed-citation id="d19e974" publication-type="other">
Mutangadura, G., Mukurazita, D., &amp; Jackson, H. (1999). A review of household and community
responses to the HIV/AIDS epidemic in the rural areas of sub-Saharan Africa. UNAIDS Best
Practice Collection. Online at &lt;http://www.unaids.org&gt;.</mixed-citation>
         </ref>
         <ref id="d19e988a1310">
            <mixed-citation id="d19e992" publication-type="other">
Nordin, S. (2005). Permaculture network of Malawi. Personal communication with Stacia Nordin, World
Food Program, Malawi, November 2005.</mixed-citation>
         </ref>
         <ref id="d19e1002a1310">
            <mixed-citation id="d19e1006" publication-type="other">
Ogden, J., &amp; Nyblade L. (2005). Common at its core. HIV/AIDS-related stigma across contexts.
Washington DC: International Center for Research on Women (ICRW).</mixed-citation>
         </ref>
         <ref id="d19e1016a1310">
            <mixed-citation id="d19e1020" publication-type="other">
Ogden, J., Esim, S., &amp; Grown, C. (2006). Expanding the care continuum for HIV/AIDS: Bringing carers
into focus. Health Policy and Planning, 21(5), 333-342.</mixed-citation>
         </ref>
         <ref id="d19e1030a1310">
            <mixed-citation id="d19e1034" publication-type="other">
Page, S. (2002). Impact of AIDS on natural resource management in Malawi. Draft report, COMPASS
project, Malawi.</mixed-citation>
         </ref>
         <ref id="d19e1044a1310">
            <mixed-citation id="d19e1048" publication-type="other">
Slater, R., &amp; Wiggins, S. (2004). Responding to HIV/AIDS in agriculture and related activities. ODI
Natural Resource Perspectives Paper, http://www.odi.org.uk.</mixed-citation>
         </ref>
         <ref id="d19e1058a1310">
            <mixed-citation id="d19e1062" publication-type="other">
Steiner, G., et al. (2004). Mitigating the impact of HIV/AIDS by labour-saving technologies. African
Conservation Tillage Network, http://www.act.org.za.</mixed-citation>
         </ref>
         <ref id="d19e1073a1310">
            <mixed-citation id="d19e1077" publication-type="other">
Stonich, S. (1992). I am destroying the land. Westview: Boulder CO.</mixed-citation>
         </ref>
         <ref id="d19e1084a1310">
            <mixed-citation id="d19e1088" publication-type="other">
Tiffen, M., Mortimore M., &amp; Gichuki F. (1994). More people less erosion: Environmental recovery in
Kenya. Chichester, UK: John Wiley &amp; Sons.</mixed-citation>
         </ref>
         <ref id="d19e1098a1310">
            <mixed-citation id="d19e1102" publication-type="other">
UNAIDS. (2007). AIDS epidemic update. United Nations Program on AIDS and World Health
Organization, December 2007. Available at http://www.unaids.org.</mixed-citation>
         </ref>
         <ref id="d19e1112a1310">
            <mixed-citation id="d19e1116" publication-type="other">
Wagner, G. (1947). The Bantu of Western Kenya, with special reference to Vugusu and Logoli (Vols. I,
II). London: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d19e1126a1310">
            <mixed-citation id="d19e1130" publication-type="other">
Waterhouse, R. (2004). The impact of HIV on farmers' knowledge of seed. Case study of Chokwe
District, Gaza Province, Mozambique. ICRISAT Report, Maputo, Mozambique.</mixed-citation>
         </ref>
         <ref id="d19e1140a1310">
            <mixed-citation id="d19e1144" publication-type="other">
White, J., &amp; Robinson, E. (2000). HIV/AIDS and rural livelihoods in sub-Saharan Africa. Report for the
Natural Resources Institute, University of Greenwich, pp. 1-68.</mixed-citation>
         </ref>
         <ref id="d19e1155a1310">
            <mixed-citation id="d19e1159" publication-type="other">
Williamson, J., Foster, G., &amp; Levine, C. (2006). Generation at risk: The global impact of HIV/AIDS on
orphans and vulnerable children. Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d19e1169a1310">
            <mixed-citation id="d19e1173" publication-type="other">
Yamano, T., &amp; Jayne, T. (2004). Measuring the impacts of working age adult mortality on small-scale
farm households in Kenya. World Development, 32(1), 91-119.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
