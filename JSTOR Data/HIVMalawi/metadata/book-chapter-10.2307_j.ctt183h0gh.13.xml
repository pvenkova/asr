<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt183h0gh</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Hope Amidst Despair</book-title>
         <subtitle>HIV/AIDS-Affected Children in Sub-Saharan Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Grannis</surname>
               <given-names>Susanna W.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745331539</isbn>
      <isbn content-type="epub">9781849646970</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>Susanna W. Grannis</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt183h0gh"/>
      <abstract abstract-type="short">
         <p>Of the 16 million children to have been orphaned by AIDS worldwide, almost 15 million live in sub-Saharan Africa. Hope Amidst Despair focuses on these children and those who are made vulnerable by HIV/AIDS. Of the millions affected, many live in deep poverty, experience little schooling, have unmet health and psychological issues and bear the burden of stigma. Their plight is often ignored and, as a result, they lead lives of isolation and exclusion that threaten their futures. The book gives voice to HIV/AIDS orphans, allowing them to tell their stories and explain the challenges they face. Susanna Grannis, founder of CHABHA (Children Affected by HIV/AIDS), shows through first-hand experience and research how young community leaders can, with help, effectively promote children's wellbeing and independence. Readers learn of the complexities and possibilities involved in positive development through the analysis of data on children from five different countries in sub-Saharan Africa. This will be an essential title for HIV/AIDS campaigners, students of development studies, policy makers, donors, and anyone concerned about the welfare of children in developing countries.</p>
      </abstract>
      <counts>
         <page-count count="200"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.3</book-part-id>
                  <title-group>
                     <title>Tables and Figures</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.5</book-part-id>
                  <title-group>
                     <title>Acronyms and Abbreviations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>For some years, I have been visiting and working with grassroots organizations in sub-Saharan Africa that support children orphaned and affected by HIV/AIDS. Through the children I’ve gotten to know, those I have interviewed, leaders of community-based organizations the children belong to, the tiny staff of a non-profit organization that supports these organizations, and lots of reading, I have learned about the despair that comes to children when their parents die and the hopes that emerge with appropriate care.</p>
                     <p>HIV/AIDS is a terrible scourge. Because of HIV/AIDS and uneven economic and political development, much of sub-Saharan Africa seems stuck in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Framing the Issues</title>
                  </title-group>
                  <fpage>6</fpage>
                  <abstract>
                     <p>The experience of two girls illustrates what Mandela means by<italic>ubuntu</italic>.</p>
                     <p>A year earlier, we had seen these two sisters at their home under very different circumstances. The leaders of an association of children affected by HIV/AIDS (human immunodeficiency virus/acquired immune deficiency syndrome) had discovered them and encouraged their participation in association activities such as the workshop described above. Genevieve (age 12) and Elise (age 13) (not their names; throughout, children’s names and identifying characteristics have been changed to protect their privacy) lived in a one-room mud and stick, wattle house, about 10 to 12 feet, on a hump of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>One Continent, Five Countries, and Five Different Epidemics</title>
                  </title-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>Marianthe, infected with HIV, aged 13, and a double orphan, created a chronicle of her life in drawing and words; Figure 2.1 is a detail of the whole. She said that since she had been taking antiretrovirals she is no longer sick everyday, and she shows the virus leaving her body in the figure above. They (HIVs) are the almost human-like markings around the sketched prone figure. You also see a friend who has come to visit and is dismayed by Marianthe’s sickness. Although her understanding of the virus is faulty, it portrays Marianthe’s belief that the virus is leaving</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Girls and Women:</title>
                     <subtitle>Special Vulnerabilities</subtitle>
                  </title-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>Back in 2003 at a workshop for Rwandan teachers and students, my husband and I talked with adolescents and adults about the dangers of HIV/AIDS.</p>
                     <p>In sub-Saharan Africa there is serious gender inequality in the HIV/AIDS numbers, with many more girls and women infected than boys and men. As I write, in late 2010, there is increasing attention to the problem. Since the pandemic has grown so large, this inequity means continued vulnerability and, ultimately, many deaths of mothers. Gender issues, particularly differences in sexual behavior, are profound and can easily be misunderstood. They are difficult to talk about, particularly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Life Sustainer:</title>
                     <subtitle>ARV Treatment</subtitle>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>HIV/AIDS is experienced within the context of family, community, and culture. When an HIV<sup>+</sup>person begins their regimen of life-sustaining antiretroviral drugs, there is usually a strong, positive effect as he or she gains strength, goes back to work or school, and is functioning as a family member. Children have their mother or father again. Thirteen-year-old Jeanne shows us the family dimension in her picture and story in Figure 4.1.</p>
                     <p>Jeanne has presented a vivid representation of HIV in the family – the devastation of the diagnosis, the rejection by a neighbor, probably because of stigma, a child taking responsibility, a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Prevention:</title>
                     <subtitle>The Long-term Goal</subtitle>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>“Everyday more than 7,000 individuals become newly infected with HIV – more than twice as many as the number of people who start on antiretroviral therapy each day.”¹ It stands to reason that treatment alone cannot solve the long-term problem of HIV/AIDS in sub-Saharan Africa as long as this trend continues. Moreover, with the leveling or reduction in donor money for ARVs that appeared in 2010, the only positive way forward is to significantly reduce new infections. Even though there were fewer new infections in sub-Saharan Africa in 2009 compared with 2001, 1.8 million compared with 2.2 million, we are still</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Poverty and Children’s Wellbeing</title>
                  </title-group>
                  <fpage>82</fpage>
                  <abstract>
                     <p>The relationship between HIV/AIDS and poverty goes in two directions. In one direction, poverty impels HIV. Impoverished people are more likely than rich to engage in risky sexual behaviors, such as sex for money, and to become infected. In the other, HIV also causes poverty. The income of families with one or more infected persons drops, especially without successful treatment.¹ These two relationships particularly affect children and youth. Affected youth engage in risky behaviors. Many affected children, especially those whose parents have died from AIDS, live in poverty and experience clusters of problems such as ill health, lack of education,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Education:</title>
                     <subtitle>A Basic Human Right</subtitle>
                  </title-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>African children know that they have to be in school and succeed there to achieve their hoped-for futures. Sadly, too many have to drop out because their parents are sick and can’t support the family, and there simply isn’t enough money. Others drop out because they have to farm or work, and care for siblings and/or sick adults. Children who have dropped out are very eager to return to school, for they know their future depends on being educated. Over and over the children I have talked with who have been out of school say, “I want to continue my</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>I Feel It in My Heart</title>
                  </title-group>
                  <fpage>110</fpage>
                  <abstract>
                     <p>Children’s feelings of sadness and grief, fear, worry, and the weight of heavy responsibilities emerge in tears, pictures, and in words – like the child who said, “I feel it in my heart.” These feelings stay with children and young people for years if not addressed. Given the range of essentials missing in the lives of affected and vulnerable children, it will not be a surprise to read that these children do not usually have access to psychological services that might mitigate some of the long-term effects of grief. Children who do receive help appear to respond well to activities that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Supporting Children</title>
                  </title-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>The experiences of affected children in distress are discussed in Chapters 3 through 8, and listed in the first column in Table 9.1 below. We turn in this chapter to the support itself: service providers, services, and outcomes. The service providers are those who interact directly with children and families. They can be either people who know the children well, or strangers. Factors important to children include how well the providers know them, where services take place, how long the services will last, and of course, the extent of the services provided. As always, the availability and amount of funding</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.16</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>A Matter of Money and Intention</title>
                  </title-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>By highlighting two community-based programs, I demonstrated that increasing the capacity of unemployed young adults to support children in need improves their own situations, too. When we remember that only about 15 per cent of HIV/AIDS affected children receive some kind of external help, though, it is clear there is tremendous need for more community-based programs including those implemented by young adults. Two important ingredients are money and intention. Getting money to children is not a simple matter. It requires that local community leaders be empowered with enough financial support to make a difference for impoverished children. This chapter is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.17</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Hope and/or Despair</title>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>No “magic bullet” can turn vulnerable HIV/AIDS-affected children’s lives around: HIV/AIDS creates too many interrelated complicated difficulties in the lives of the children it touches. Here, in this final chapter, I highlight the<italic>hopes</italic>for vulnerable HIV/AIDS affected children’s improved wellbeing. It would be wrong, though, to leave it at that, for there are many, serious reasons for<italic>despair</italic>, also included here. I end the chapter by commenting on paths to structural<italic>changes</italic>that could shift the balance toward hope and away from despair.</p>
                     <p>About the<italic>hopes</italic>for children affected, let us first remember the hope that comes from biomedical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.18</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>163</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183h0gh.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>180</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
