<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">humanecology</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101456</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Human Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03007839</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15729915</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23280264</article-id>
         <title-group>
            <article-title>Travelling Companions: Emerging Diseases of People, Animals and Plants Along the Malawi-Mozambique Border</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeffery W.</given-names>
                  <surname>Bentley</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mike</given-names>
                  <surname>Robson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bright B.</given-names>
                  <surname>Sibale</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Edwin</given-names>
                  <surname>Nkhulungo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Yolice</given-names>
                  <surname>Tembo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Fransisca</given-names>
                  <surname>Munthali</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23280258</issue-id>
         <fpage>557</fpage>
         <lpage>569</lpage>
         <permissions>
            <copyright-statement>© 2012 Springer Science+Business Media, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23280264"/>
         <abstract>
            <p>Humans, animals and plants suffer from similar types of diseases (e.g., fungal, viral etc.). These can "emerge" as new diseases by expanding their geographical range or by jumping species (from plants to plants, or from animals to humans). Emerging diseases place an additional burden on developing countries which are often struggling to manage the diseases they already have. New diseases spread through weather, insects or other vectors, or by the movement of people, animals or goods. This study examines the role of cross-border travel in the spread of diseases. A survey of travelers and of residents along the Malawi-Mozambique border found that most cross it frequently and that they rarely travel empty-handed, often taking plants and animals with them. People also cross borders seeking medical attention. Attempting to limit travel would hamper an already struggling economy, where many people make a living by producing, processing or transporting plants and animals for food. Cross border travel per se may pose slight danger for the spread of diseases, if governments can collaborate on sharing information about the status of diseases within their border.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d687e296a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d687e303" publication-type="other">
Rugalema and Mathieson 2009</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e309" publication-type="other">
Rugalema et al. 2009</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d687e315" publication-type="other">
Bentley et al. 2009</mixed-citation>
            </p>
         </fn>
         <fn id="d687e322a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d687e329" publication-type="other">
Pakenham 1991</mixed-citation>
            </p>
         </fn>
         <fn id="d687e336a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d687e343" publication-type="other">
Tembo
2007</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d687e362a1310">
            <mixed-citation id="d687e366" publication-type="other">
Anderson, P. K., Cunningham, A. A., Patel, N. G., Morales, F. J.,
Epstein, P. R., and Daszak, P. (2004). Emerging Infectious
Diseases of Plants: Pathogen Pollution, Climate Change and
Agrotechnology Drivers. Trends in Ecology &amp; Evolution 19:
535–544.</mixed-citation>
         </ref>
         <ref id="d687e385a1310">
            <mixed-citation id="d687e389" publication-type="other">
Bentley, J. W., Boa, E. R., Kelly, P., Harun-Ar-Rashid, M., Rahman, A.
K. M., Kabeere, F., and Herbas, J. (2009). Ethnopathology: Local
Knowledge of Plant Health Problems in Bangladesh, Uganda and
Bolivia. Plant Pathology 58: 773–781.</mixed-citation>
         </ref>
         <ref id="d687e405a1310">
            <mixed-citation id="d687e409" publication-type="other">
Chikungwa, P. (2008). Foot and Mouth Disease, Malawi, http://
www.oie.int/wahis/public.php?page=single_report&amp;pop=
l&amp;reportid=7580.</mixed-citation>
         </ref>
         <ref id="d687e422a1310">
            <mixed-citation id="d687e426" publication-type="other">
Daszak, P. A., Cunningham, A., and Hyatt, A. D. (2000). Emerging
Infectious Diseases of Wildlife: Threats to Biodiversity and
Human Health. Science 287: 443–449.</mixed-citation>
         </ref>
         <ref id="d687e440a1310">
            <mixed-citation id="d687e444" publication-type="other">
FAO (2008). Prevention of Foot and Mouth Disease in Endemic/High
Risk Areas. http://www.fao-ectad-gaborone.org/en/IMG/pdf/
Workshop_report_FMD_Management_in_endemic_areas.pdf.</mixed-citation>
         </ref>
         <ref id="d687e457a1310">
            <mixed-citation id="d687e461" publication-type="other">
Farrell, G. (2000). Dispersal, Phenology and Predicted Abundance of
the Larger Grain Borer in Different Environments. African Crop
Science Journal 8(3): 337–343.</mixed-citation>
         </ref>
         <ref id="d687e474a1310">
            <mixed-citation id="d687e478" publication-type="other">
Gaffga, N. H., Tauxe, R. V., and Mintz, E. D. (2007). Cholera: A New
Homeland in Africa? The American Journal of Tropical Medicine
and Hygiene 77: 705–713.</mixed-citation>
         </ref>
         <ref id="d687e491a1310">
            <mixed-citation id="d687e495" publication-type="other">
Government of Malawi (2001). Malawi Growth and Development
Strategy. Government of Malawi, Lilongwe.</mixed-citation>
         </ref>
         <ref id="d687e505a1310">
            <mixed-citation id="d687e509" publication-type="other">
Government of Malawi (2008). Ministry of Agriculture and Food
Security, Draft Agricultural Development Program. Government
of Malawi, Lilongwe.</mixed-citation>
         </ref>
         <ref id="d687e522a1310">
            <mixed-citation id="d687e526" publication-type="other">
Government of Malawi (2009). Update on the Disease Outbreak in
Neno District. Press release, page 42 of The Daily Times
(Lilongwe) 23 November 2009.</mixed-citation>
         </ref>
         <ref id="d687e540a1310">
            <mixed-citation id="d687e544" publication-type="other">
Guardian, The (2009). Cholera Scares Machinga, Blantyre. The
Guardian (Malawi). 1 December 2009, 2.</mixed-citation>
         </ref>
         <ref id="d687e554a1310">
            <mixed-citation id="d687e558" publication-type="other">
Joy, D. A., Feng, X., Mu, J., Furuya, T., Chotivanich, K., Krettli, A. U.,
Ho, M„ Wang, A., White, N. J., Suh, E„ Beerli, P., and Su, X.
(2003). Early Origin and Recent Expansion of Plasmodium falci-
parum. Science 300: 318–321.</mixed-citation>
         </ref>
         <ref id="d687e574a1310">
            <mixed-citation id="d687e578" publication-type="other">
Lederberg, J., Shope, R. E., and Oaks, S. C. (eds.) (1992). Emerging
Infections: Microbial Threats to Health in the United States.
National Academy Press, Washington DC.</mixed-citation>
         </ref>
         <ref id="d687e591a1310">
            <mixed-citation id="d687e595" publication-type="other">
Stories on Malawi (2008). Foot and Mouth Disease Outbreak Hits
Malawi, http://storiesonmalawi.blogspot.com/2008_10_13_
archive.html.</mixed-citation>
         </ref>
         <ref id="d687e608a1310">
            <mixed-citation id="d687e612" publication-type="other">
Matita, G. B. (2003). Foot and Mouth Disease in Malawi: Suspected
Outbreak, http://www.promedmail.org/pls/otn/f?
p=2400:1202:5108239622596470078::NO::F2400_
P1202_CHECK_DISPLAY,F2400_P1202_PUB_MAIL_ID:
X,21595.</mixed-citation>
         </ref>
         <ref id="d687e631a1310">
            <mixed-citation id="d687e635" publication-type="other">
Munthali, S. C. M. (1992). The Larger Grain Borer, Prostephanus
truncatus, in Malawi; current status. Plant Protection Workshop,
Lilongwe, Malawi, 1–5 June 1992.</mixed-citation>
         </ref>
         <ref id="d687e649a1310">
            <mixed-citation id="d687e653" publication-type="other">
NSO (National Statistical Office) (2005a). Integrated Household
Survey 2004-2005. National Statistical Office. Government of
Malawi, Zomba.</mixed-citation>
         </ref>
         <ref id="d687e666a1310">
            <mixed-citation id="d687e670" publication-type="other">
NSO (National Statistical Office) (2005b). Malawi Demographic and
Health Survey 2004. National Statistical Office. Government of
Malawi, Zomba.</mixed-citation>
         </ref>
         <ref id="d687e683a1310">
            <mixed-citation id="d687e687" publication-type="other">
NSO (National Statistical Office) (2007). Welfare Monitoring Survey
2006. National Statistical Office. Government of Malawi, Zomba.</mixed-citation>
         </ref>
         <ref id="d687e697a1310">
            <mixed-citation id="d687e701" publication-type="other">
NSO (National Statistical Office) (2008). 2008 Population and
Housing Census. National Statistical Office. Government of
Malawi, Zomba.</mixed-citation>
         </ref>
         <ref id="d687e714a1310">
            <mixed-citation id="d687e718" publication-type="other">
Pakenham, T. (1991). The Scramble for Africa. Abacus, London.</mixed-citation>
         </ref>
         <ref id="d687e725a1310">
            <mixed-citation id="d687e729" publication-type="other">
Rugalema, G., and Mathieson, K. (2009). Disease, Vulnerability
and Livelihoods on the Tanzania-Uganda Interface Ecosystem
to the West of Lake Victoria. Tanzania country report. FAO,
Rome.</mixed-citation>
         </ref>
         <ref id="d687e746a1310">
            <mixed-citation id="d687e750" publication-type="other">
Rugalema, G., Muir, G., Mathieson, K., Measures, E., Oehler, F., and
Stloukal, L. (2009). Emerging and Re-Emerging Diseases of
Agricultural Importance: Why Local Perspectives Matter. Food
Security 1: 441–455.</mixed-citation>
         </ref>
         <ref id="d687e766a1310">
            <mixed-citation id="d687e770" publication-type="other">
Sumption, K., Pinto, J., Lubroth, J., Morzaria, S., Murray, T., De La
Rocque, S., and Njeumi, F. (2007). Foot-and-Mouth Disease
Situation Worldwide and Major Epidemiological Events in
2005-2006. FAO EMPRES (Emergency Prevention System)
Focus-On 1: 1–11.</mixed-citation>
         </ref>
         <ref id="d687e789a1310">
            <mixed-citation id="d687e793" publication-type="other">
Tembo, Y. L. B. (2007). Incidence and Impact of Chilo partellus
Swinhoe (Lepidoptera: Crambidae), the Cereal Stemborer and
Its Natural Enemies on Maize (Zea mays L.) in Shire Valley
Agricultural Development Division in Malawi. Thesis,
University of Malawi, pp. 118.</mixed-citation>
         </ref>
         <ref id="d687e812a1310">
            <mixed-citation id="d687e816" publication-type="other">
Thresh, J. M., and Cooter, R. J. (2005). Strategies for Controlling Cassava
Mosaic Virus Disease in Africa. Plant Pathology 54: 587–614.</mixed-citation>
         </ref>
         <ref id="d687e826a1310">
            <mixed-citation id="d687e830" publication-type="other">
Wang, G., Van Dam, A. P., Schwartz, I., and Dankert, J. (1999).
Molecular Typing of Borrelia burgdorferi Sensu Lato:
Taxonomic, Epidemiological, and Clinical Implications. Clinical
Microbiology Reviews 12(4): 633–653.</mixed-citation>
         </ref>
         <ref id="d687e846a1310">
            <mixed-citation id="d687e850" publication-type="other">
Wittmayer, J. M., and Büscher, B. (2010). Conserving Conflict?
Transfrontier Conservation, Development Discourses and Local
Conflict Between South Africa and Lesotho. Human Ecology 38:
763–773.</mixed-citation>
         </ref>
         <ref id="d687e867a1310">
            <mixed-citation id="d687e871" publication-type="other">
Wolfe, N., Dunavan, C. P., and Diamond, J. (2007). Origins of Major
Human Infectious Diseases. Nature 447: 279–283.</mixed-citation>
         </ref>
         <ref id="d687e881a1310">
            <mixed-citation id="d687e885" publication-type="other">
Woolhouse, M. E. J., Haydon, D. T., and Antia, R. (2005). Emerging
Pathogens: The Epidemiology and Evolution of Species Jumps.
Trends in Ecology &amp; Evolution 20: 238–244.</mixed-citation>
         </ref>
         <ref id="d687e898a1310">
            <mixed-citation id="d687e902" publication-type="other">
Zetter, R. (1996). Indigenous NGOs and Refugee Assistance: Some
Lessons from Malawi and Mozambique. Development in Practice
6: 37–49.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
