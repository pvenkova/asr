<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt5vj67t</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt5vkgp3</book-id>
      <subj-group>
         <subject content-type="call-number">HQ784.W3C487 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Children and war</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Child soldiers</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Child Soldiers in the Age of Fractured States</book-title>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">EDITED BY</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>GATES</surname>
               <given-names>SCOTT</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>REICH</surname>
               <given-names>SIMON</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>31</day>
         <month>01</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780822960294</isbn>
      <isbn content-type="epub">9780822973591</isbn>
      <publisher>
         <publisher-name>University of Pittsburgh Press</publisher-name>
         <publisher-loc>Pittsburgh, Pa</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>University of Pittsburgh Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt5vkgp3"/>
      <abstract abstract-type="short">
         <p>Current global estimates of children engaged in warfare range from 200,000 to 300,000. Children's roles in conflict range from armed and active participants to spies, cooks, messengers, and sex slaves.<italic>Child Soldiers in the Age of Fractured States</italic>examines the factors that contribute to the use of children in war, the effects of war upon children, and the perpetual cycle of warfare that engulfs many of the world's poorest nations.The contributors seek to eliminate myths of historic or culture-based violence, and instead look to common traits of chronic poverty and vulnerable populations. Individual essays examine topics such as: the legal and ethical aspects of child soldiering; internal UN debates over enforcement of child protection policies; economic factors; increased access to small arms; displaced populations; resource endowments; forced government conscription; rebel-enforced quota systems; motivational techniques employed in recruiting children; and the role of girls in conflict.The contributors also offer viable policies to reduce the recruitment of child soldiers such as the protection of refugee camps by outside forces, "naming and shaming," and criminal prosecution by international tribunals. Finally, they focus on ways to reintegrate former child soldiers into civil society in the aftermath of war.</p>
      </abstract>
      <counts>
         <page-count count="352"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.3</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.4</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gates</surname>
                           <given-names>Scott</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Reich</surname>
                           <given-names>Simon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>ALTHOUGH THIS VOLUME FOCUSES ON CHILD SOLDIERS, it is not limited to children brandishing a gun. It also examines the roles of children, many of whom are preadolescent, linked to armed groups with a variety of functions. As such, child soldiers work as spies, cooks, porters, messengers, sex slaves, and, indeed, as both armed and unarmed combatants who serve in government armies, militias, and nonstate groups.¹ The authors in this collection analyze the phenomenon of child soldiers, examine what has been done to address it, and explore what remedies exist, if any.</p>
                     <p>Dating from the signing of the United Nations</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>METHODOLOGICAL PROBLEMS IN THE STUDY OF CHILD SOLDIERS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ames</surname>
                           <given-names>Barry</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>14</fpage>
                  <abstract>
                     <p>CHILD SOLDIERS MAY BE THE MOST DEPRESSING TOPIC in the field of international conflict, and the elimination of child soldiering is a completely laudable goal. But the methodological principles promoting good scholarship are still crucial. A more precise understanding of why children join violent conflicts, why irregular armies recruit and abduct children, why children leave irregular armies, and what problems they face after leaving will help craft policies to reduce child soldiering.</p>
                     <p>Much of the literature on child soldiers comes from advocacy groups. Not only would it be unrealistic to expect such advocates to jump into complex statistical analyses; it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>AN ETHICAL PERSPECTIVE ON CHILD SOLDIERS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McMahan</surname>
                           <given-names>Jeff</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>MORAL PHILOSOPHY OBVIOUSLY HAS NOTHING TO SAY about the urgent practical problem of preventing unscrupulous recruiters from forcing children to fight their unjust wars for them. That is a question of political and legal policy on which philosophers have no special competence to pronounce. But the problem of child soldiers does have a normative dimension and raises questions that philosophers are specially qualified to address. Do conditions of ignorance and duress in which child soldiers normally act ever make their action morally permissible, even if the war in which they are fighting is unjust and even if they commit war</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>THE EVOLUTION OF THE UNITED NATIONS’ PROTECTION AGENDA FOR CHILDREN:</title>
                     <subtitle>Applying International Standards</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chikuhwa</surname>
                           <given-names>Tonderai W.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>THE HORRORS THAT ARE BEING VISITED ON CHILDREN in the context of war is a blight on the conscience of humankind. Today, in so many of the conflicts around the globe, children are being brutalized in unimaginable ways. Not only are civilians, particularly children, women, and the elderly, increasingly the primary targets and victims of atrocities, but children are also becoming some of the worst perpetrators of brutalities against their own families and communities. They are being forced to give expression to the hatreds of adults. Ironically, at the same time as the situation on the ground deteriorates for children,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>NO PLACE TO HIDE:</title>
                     <subtitle>Refugees, Displaced Persons, and Child Soldier Recruits</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Achvarina</surname>
                           <given-names>Vera</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Reich</surname>
                           <given-names>Simon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>REPORTS OF TALIBAN RECRUITMENT OF CHILDREN as insurgents and possible suicide bombers surfaced in the U.S. media in August 2005 (CNN 2005). Estimates at the time suggested that the insurgent forces in Afghanistan may have comprised up to 8,000 children (IRIN 2003a). To many in the West, this was a surprising revelation, but it should not have been. Children participated in the Israeli-Palestinian conflict and had been used as soldiers by the Taliban against Soviet forces in the 1980s.¹ Many of the current adult insurgents in Afghanistan came from the ranks of these former child soldiers (Center for Defense Information</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>RECRUITING CHILDREN FOR ARMED CONFLICT</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Andvig</surname>
                           <given-names>Jens Christopher</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gates</surname>
                           <given-names>Scott</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>DATA ON CHILD RECRUITMENT IN SUB-SAHARAN AFRICA collected by Achvarina and Reich (2006) and Becker’s comparison of Sri Lanka, Nepal, and Burma in this volume demonstrate that the proportion of child soldiers varies considerably from one group to another. A wide variety of case studies from around the world also suggests that the welfare of the children employed by violent groups varies across organizations. In this chapter we focus on this variance and examine the patterns of recruitment across different kinds of violent organizations. We also seek to answer the general puzzle of why a group would recruit children to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>THE ENABLERS OF WAR:</title>
                     <subtitle>Causal Factors behind the Child Soldier Phenomenon</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Singer</surname>
                           <given-names>P. W.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>93</fpage>
                  <abstract>
                     <p>WHILE WARFARE HAS LONG BEEN THE DOMAIN OF ADULTS, juveniles have been present in armies in a number of instances in the past. For example, young pages armed the knights of the Middle Ages and drummer boys marched before Napoleonic armies. Child soldiers even fought in the U.S. Civil War, most notably when a unit of 247 Virginia Military Institute cadets fought with the Confederate army in the battle of New Market in 1864. More recently, U.S. forces fought against small numbers of underage Hitler Jugend (Hitler Youth) in the closing weeks of World War II.</p>
                     <p>However, these were the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>CHILD RECRUITMENT IN BURMA, SRI LANKA, AND NEPAL</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Becker</surname>
                           <given-names>Jo</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>108</fpage>
                  <abstract>
                     <p>THE NUMBER OF CHILD SOLDIERS IN ASIA IS SECOND ONLY to that in Africa. Although precise figures are impossible to establish, the number of child soldiers in the region is likely to exceed 75,000. Child soldiers have participated in several of the region’s ongoing armed conflicts, including those in Afghanistan, Burma, the Philippines, and Sri Lanka, and the recently ended conflict in Nepal.</p>
                     <p>As in other regions, myriad factors contribute to the recruitment and participation of children in Asia’s armed conflicts. These include poverty, displacement, and a lack of schooling or work opportunities, separation from family, or an abusive family</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>ORGANIZING MINORS:</title>
                     <subtitle>The Case of Colombia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sanín</surname>
                           <given-names>Francisco Gutiérrez</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>PRACTITIONERS AND RESEARCHERS HAVE THOROUGHLY examined an ample set of push factors that cause minors to join nonstate armed organizations in Colombia, ranging from poverty to the provision of weapons. Pull factors and the interaction between push and pull, however, also play a crucial role.</p>
                     <p>Prima facie, recruiting minors does not seem such a good business for an irregular force. Indeed, having children in an armed group may provide several strategic advantages. There are clear disadvantages though. Children can be undisciplined; their bodies and psychology are not prepared for the sustained hardships of war; and they do not stand a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>WAR, DISPLACEMENT, AND THE RECRUITMENT OF CHILD SOLDIERS IN THE DEMOCRATIC REPUBLIC OF CONGO</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lischer</surname>
                           <given-names>Sarah Kenyon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>DISPLACEMENT IS ONE OF THE MOST COMMON PRODUCTS of violent conflict. When weighing the risks posed by civil war, many people decide that their best chance of safety lies elsewhere. Sometimes the chaos and destruction of war arrives so suddenly that families become separated—children flee from school, farmers leave their fields, mothers escape their homes. In other situations, soldiers force an evacuation at gunpoint and whole villages are exiled en masse. Displacement does not necessarily lead to safety, however. Refugees and internally displaced persons (IDPs) may become targets of attack or simply languish indefinitely in squalid, disease-ridden camps.¹ For</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>DISAGGREGATING THE CAUSAL FACTORS UNIQUE TO CHILD SOLDIERING:</title>
                     <subtitle>The Case of Liberia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pugel</surname>
                           <given-names>James B.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>160</fpage>
                  <abstract>
                     <p>WITH A CONTEMPORARY INTERNATIONAL FOCUS ON THE ISSUE of employing child soldiers in inter- and intrastate conflicts, a new area of research for academics has opened to identify and quantify the causal factors attributable to the practice. While the practice of employing children in combat, both in direct actions and supporting logistical roles, is not new to the world stage, the increased usage and blatant disregard of moral and human rights have been so pervasive in recent years that numerous statutes and protocols have been set by international bodies in order to curb miscreant behaviors and bring some semblance of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.16</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>GIRLS IN ARMED FORCES AND GROUPS IN ANGOLA:</title>
                     <subtitle>Implications for Ethical Research and Reintegration</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wessells</surname>
                           <given-names>Michael G.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>183</fpage>
                  <abstract>
                     <p>ONE OF THE MOST SIGNIFICANT VIOLATIONS OF HUMAN RIGHTS is the recruitment of children, defined under international law as people under eighteen years of age, into armed forces such as national armies or armed groups such as the opposition groups that fight government forces in more than twenty countries (CSUCS 2004a, 13). This violation of children’s rights takes an enormous toll on children and societies. Although the physical damage to children garners the most attention, extensive harm arises also from the interaction of physical, psychological, social, and spiritual factors (Wessells 2006, 126–153; Williamson and Robinson 2006). This damage to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.17</book-part-id>
                  <title-group>
                     <label>CHAPTER 12</label>
                     <title>NATIONAL POLICIES TO PREVENT THE RECRUITMENT OF CHILD SOLDIERS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Vargas-Barón</surname>
                           <given-names>Emily</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>203</fpage>
                  <abstract>
                     <p>CHILD SOLDIERS LIVE IN A GRIM WORLD OF VIOLENCE AND deprivation that largely exists outside of international and national laws. Because the actions required to prevent child soldier recruitment are diverse, prevention policies should include a variety of intersectoral and integrated approaches, especially for education and training, in the broadest and most flexible sense.¹</p>
                     <p>The recruitment of child soldiers has been condemned by the international community through the adoption of several international conventions, protocols, and UN resolutions for human rights, fundamental freedoms, and international security, including those for educational and children’s rights. Of special note is the groundbreaking Optional Protocol</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.18</book-part-id>
                  <title-group>
                     <label>CHAPTER 13</label>
                     <title>WISE INVESTMENTS IN FUTURE NEIGHBORS:</title>
                     <subtitle>Recruitment Deterrence, Human Agency, and Education</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McClure</surname>
                           <given-names>Maureen W.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Retamal</surname>
                           <given-names>Gonzalo</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>223</fpage>
                  <abstract>
                     <p>HUMAN SECURITY IS EMERGING AS A SOPHISTICATED and compelling strategy to address the extreme problems of children in contemporary wars. The child soldier is increasingly seen as an icon of new wars—transformed from a young person into a weapon (Kaldor 1999). Whether as members of local militias or as suicide bombers, child soldiers are children growing up among failed adults in failed communities. Some not only fail to learn to read or write, they also fail to learn the humanity they need to be successful neighbors and parents.</p>
                     <p>Turning children into weapons is an act of generational destruction.¹ Failed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.19</book-part-id>
                  <title-group>
                     <label>CHAPTER 14</label>
                     <title>ENDING THE SCOURGE OF CHILD SOLDIERING:</title>
                     <subtitle>An Indirect Approach</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mack</surname>
                           <given-names>Andrew</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>242</fpage>
                  <abstract>
                     <p>PETER SINGER, THE MOST-CITED CONTEMPORARY ANALYST writing on child soldiers, has claimed that the practice of child soldiering has dramatically increased because the global norm against the use of children in war—what he calls “the single greatest taboo of all”—has dramatically eroded. In “the chaos and callousness of modern-day warfare,” he argues, this norm “has seemingly broken down” (Singer 2006, 4).</p>
                     <p>Although accessing good data in this field is a major challenge, what evidence we do have suggests that the claim that child soldiering has increased because a taboo against the use of children in war has broken</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.20</book-part-id>
                  <title-group>
                     <title>CONCLUSION</title>
                     <subtitle>Children and Human Security</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gates</surname>
                           <given-names>Scott</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Reich</surname>
                           <given-names>Simon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>247</fpage>
                  <abstract>
                     <p>CHILDREN HAVE ALWAYS BEEN PART OF WAR, YET THEY constitute an understudied dimension of human security. In this regard, our volume contributes to the shift in focus away from traditional notions of national defense toward what former UN Secretary-General Kofi Annan described as “the protection of communities and individuals from internal violence” (Human Security Center 2005). Focusing on children in armed conflict clearly enlightens us about an important aspect of human security. Given the inherent vulnerability of children, such attention is warranted.</p>
                     <p>The definition of human security is vigorously debated; there is no consensus. The narrow definition, to which this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.21</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.22</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>273</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.23</book-part-id>
                  <title-group>
                     <title>List of Contributors</title>
                  </title-group>
                  <fpage>295</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt5vkgp3.24</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>297</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
