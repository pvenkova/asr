<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id journal-id-type="jstor">j100641</journal-id>
         <journal-title-group>
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn pub-type="ppub">03057070</issn>
         <issn pub-type="epub">14653893</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25064973</article-id>
         <title-group>
            <article-title>Strategic Alliances and Mergers of Financial Exchanges: The Case of the SADC</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Charles C.</given-names>
                  <surname>Okeahalam</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i25064967</issue-id>
         <fpage>75</fpage>
         <lpage>93</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 Journal of Southern African Studies</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25064973"/>
         <abstract>
            <p>Over the last three years there has been increased speculation in Europe and elsewhere as to the relative merits of merging a number of national and international financial exchanges. In some respects, the development of financial markets is an important aspect of economic development, and several countries in the Southern Africa Development Community region have a financial exchange. However, the vast majority of these exchanges have a small number of listed securities and low levels of capitalisation and liquidity. The design, size, scope, institutional and regulatory framework of a financial exchange determines its relative costs and benefits. Seen in this light, without the appropriate scale, liquidity, social and technological infrastructure, it is unlikely that a financial exchange will be able to meet its strategic objectives efficiently. This article discusses the economic case for establishing a regional financial exchange for the Southern African Development Community. It suggests that the most economically efficient and least costly way of accomplishing this is for the national exchanges in the SADC region to merge. The article concludes by suggesting a number of enabling policy proposals.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d2021e114a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d2021e121" publication-type="other">
C. C. Okeahalam, 'An Analysis of the Price-Concentration Relationship in the Botswana Commercial Banking
Industry', Journal of African Finance and Economic Development, 3 (1998), pp. 65-84</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e130" publication-type="other">
C. C. Okeahalam,
'Costs and Efficiency in Botswana Commercial Banking', Journal for Studies in Economics and Econometrics,
23 (1999), pp. 53-72.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e143a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d2021e150" publication-type="other">
G. J. Stigler, 'Public Regulation of the Securities Markets', Journal of Business, 37 (1964),
pp. 117-142</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e159" publication-type="other">
H. Demsetz, 'The Cost of Transacting', Quarterly Journal of
Economics, 82 (1968), pp. 33-53</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e169a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d2021e176" publication-type="other">
A. Madhavan, 'Market Microstructure: A Survey', Journal of Financial
Markets, 3 (2000), pp. 205-258.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e186a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d2021e193" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e201a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d2021e208" publication-type="other">
A. Singh, 'Should Africa Promote Stock Market Capitalism?', Journal of International Development,
11 (1999), pp. 343-365.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e218a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d2021e225" publication-type="other">
C. Detken and P. Hartmann, 'The Euro and
International Capital Markets', International Finance, 3, 1 (2000), pp. 53-94.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e235a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d2021e242" publication-type="other">
M. Pagano, 'Trading Volume and Asset Liquidity', Quarterly Journal of Economics, 104 (1989), pp. 255-274</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e248" publication-type="other">
D. Easley and M. O'Hara, 'Time and the Process of Security Price Adjustment', Journal of Finance, 47, 2
(1992), pp. 577-605.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e258a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d2021e265" publication-type="other">
R. von Rosen, 'Clearing
Up Europe's Exchanges', Financial Times, 9 February 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e275a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d2021e282" publication-type="other">
C. Pirrong, 'The Organisation of Financial Exchange Markets: Theory and Evidence', Journal of Financial
Markets, 2 (1999), pp. 329-357.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e292a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d2021e299" publication-type="other">
K. J. Cohen, S. F. Maier, R. A. Schwartz and D. K. Whitcom, The Microstructure of
Securities Markets (Englewood Cliffs, NJ, Prentice Hall, 1986).</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e310a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d2021e317" publication-type="other">
'Focus on SADC Trade Protocol', Enterprise Magazine (2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e324a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d2021e331" publication-type="other">
J. Michie, 'The Internationalisation of the
Innovation Process', International Journal of the Economics of Business, 5, 3 (1998), pp. 261-277</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e341a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d2021e348" publication-type="other">
J. S. Howe and K. Keim, 'The Stock Price Impacts of Overseas Listings', Financial Management,
16 (1987), pp. 51-56.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e357" publication-type="other">
G. Alexander, C. S. Eun and S. Janakiramanan, 'International Listings and Stock
Returns: Some Empirical Evidence', Journal of Financial and Quantitative Analysis, 23 (1988), pp. 135-151</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e367a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d2021e374" publication-type="other">
Carmody makes the same point and emphasises the significance of this trend by stating that 'The globalisation
strategies of South African conglomerates have gone further than those of other transnationals worldwide.'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e383" publication-type="other">
P. Carmody, 'Between Globalisation and (Post) Apartheid: the Political Economy of Restructuring in South
Africa', Journal of Southern African Studies, 28, 2 (2002), pp. 255-275.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e393a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d2021e400" publication-type="other">
Cohen, Maier, Schwartz and Whitcom, The
Microstructure of Securities Markets.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e409" publication-type="other">
K. R. Jefferis and C. C. Okeahalam, 'International Stock Market
Linkages in Southern Africa', South African Journal of Accounting Research, 13, 2 (1999), pp. 27-52</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e418" publication-type="other">
K. R.
Jefferis and C. C. Okeahalam, 'The Impact of Economic Fundamentals on Stock Markets in Southern Africa',
Development Southern Africa, 17, 1 (2000), pp. 23-51</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e431" publication-type="other">
C. C. Okeahalam and K. R. Jefferis, 'An Analysis of the
Differential Information Hypothesis on the Botswana and Zimbabwe Stock Markets', The Investment Analysts
Journal, 49 (1999), pp. 39-40</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e443" publication-type="other">
C. C. Okeahalam and K. R. Jefferis, 'An Event Study of the Botswana,
Zimbabwe and Johannesburg Stock Markets', South African Journal of Business Management, 30, 4 (2000), pp.
131-140).</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e456a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d2021e463" publication-type="other">
H. Bessimbinder, 'Trade Execution Costs on NASDAQ and the NYSE: a Post-Reform Comparison', Journal
of Financial and Quantitative Analysis, 34 (1999), pp. 387-407</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e472" publication-type="other">
H. Bessimbinder and H. M. Kaufman,
'A Comparison of Trade Execution Costs for NYSE and NASDAQ Listed Stocks', Journal of Financial and
Quantitative Analysis, 32 (1997), pp. 287-310.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e486a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d2021e493" publication-type="other">
P. Clyde, P. Schultz and
M. Zaman, 'Trading Costs and Exchange De-listings: the Case of Firms that Voluntarily Switch from the
American Stock Exchange to the NASDAQ', Journal of Finance, 52 (1998), pp. 2,103-2,112.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e506a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d2021e513" publication-type="other">
J. H. Harris and P. H. Schultz, 'The Trading Profits of SOES Bandits', Journal of Financial Economics, 50 (1998),
pp. 39-62.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e523a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d2021e530" publication-type="other">
P. Young and T. Heys, Capital Market Revolution: The Future of
Markets in an Online World (Harlow, Pearson Education/Financial Times Prentice Hall, 1999).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e539" publication-type="other">
John Langton, Chief Executive of the International Securities Market Association (ISMA),
'Notice to Stock Exchange Members: Dining Room Closed. Silicon Chips Being in Basement. All Welcome',
International Finance, 3, 1 (1999), pp. 161-166</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e552a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d2021e559" publication-type="other">
H. Bessimbinder and H. M. Kaufman, 'A Comparison of Trade
Execution Costs for NYSE and NASDAQ Listed Stocks, Journal of Financial and Quantitative Analysis, 32
(1997), pp. 287-310</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e571" publication-type="other">
W. Christie and P. Shultz, 'Why Do NASDAQ Market Makers Avoid Odd Eighth
Quotes?', Journal of Finance, 49 (1994), pp. 1,813-1,840.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e581a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d2021e588" publication-type="other">
A. A. Kirilenko,
'On the Endogeneity of Trading Arrangements', Journal of Financial Markets, 3 (2000), pp. 287-314.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e598a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d2021e605" publication-type="other">
Christie and Schultz,
'Why Do NASDAQ Market Makers Avoid Odd Eighth Quotes?', pp. 1,813-1,840.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e616a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d2021e623" publication-type="other">
G. Gaudet and S. W. Salant, 'Towards a Theory of Horizontal Mergers', in G. Norman, and M. la Manna
(eds), The New Industrial Economics (Aldershot, UK, Edward Elgar Press, 1992).</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e633a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d2021e640" publication-type="other">
E. J.
Kane, 'Incentives for Banking Mega-Mergers: What Motives Might Regulators Infer from Event Study
Evidence?', Journal of Money, Credit and Banking, 32, 3 (2000), pp. 671-701.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e653a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d2021e660" publication-type="other">
B. Bais, T. Foucault and F. Salanie, 'Floor Dealer
Markets and Limit Order Markets', Journal of Financial Markets, 1, 4 (1998), pp. 253-284.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e670a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d2021e677" publication-type="other">
D. Simon, 'Trading Spaces: Imagining and Positioning the "New" South Africa within the Regional and
Global Economies', International Affairs, 77, 2 (2001), pp. 377-405.</mixed-citation>
            </p>
         </fn>
         <fn id="d2021e687a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d2021e694" publication-type="other">
Business Day, 29 August 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2021e700" publication-type="other">
The Sunday Times, 31 August 2003.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
