<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1283gf</book-id>
      <subj-group>
         <subject content-type="call-number">LE3.B88M33 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">University of Victoria (B.C.)</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">University of Victoria (B.C.)</subject>
         <subj-group>
            <subject content-type="lcsh">Presidents</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">University cooperation</subject>
         <subj-group>
            <subject content-type="lcsh">British Columbia</subject>
            <subj-group>
               <subject content-type="lcsh">Victoria</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Education</subject>
      </subj-group>
      <book-title-group>
         <book-title>Reaching Outward and Upward</book-title>
         <subtitle>The University of Victoria, 1963-2013</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>MacPHERSON</surname>
               <given-names>IAN</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>06</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780773540323</isbn>
      <isbn content-type="epub">9780773587397</isbn>
      <publisher>
         <publisher-name>MQUP</publisher-name>
         <publisher-loc>Montreal; Kingston; London; Ithaca</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>McGill-Queen’s University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1283gf"/>
      <abstract abstract-type="short">
         <p>As the year 2013 and the fiftieth anniversary of the University of Victoria approaches, Ian MacPherson offers a comprehensive history of one of Canada's most progressive and visually beautiful campuses. A reflection on the people, history, and legacy of UVic - once known as Victoria College, a satellite of McGill University - Reaching Outward and Upward brings five decades of learning to life. From its beginnings in 1963, serving a mere handful of students in a hastily developed site, UVic has grown to become one of Canada's leading universities serving over 20,000 students on one of Canada's most stunning university campuses. Ian MacPherson examines how this transformation took place despite some difficult phases and all the challenges that accompany institutional transitions - the development of new faculties, growing student numbers, struggles over funding, equity issues, and computerisation. He looks at the university's development during the presidencies of Howard Petch (1975-1990), David Strong (1990-2000), and David Turpin (2000-present), and suggests that new ways of knowing changed established disciplines and created new alliances among students and faculty and led to the creation of the numerous research centres for which Uvic is well-known. A visually rich book, including pictures, quotations, and sidebars, Reaching Outward and Upward is above all a story of the communities - on-campus, off-campus, local, national, international, physical, and electronic - that together form the University of Victoria.</p>
      </abstract>
      <counts>
         <page-count count="184"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.3</book-part-id>
                  <title-group>
                     <title>PRESIDENT’S PREFACE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Turpin</surname>
                           <given-names>David H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.4</book-part-id>
                  <title-group>
                     <title>FOREWORD AND ACKNOWLEDGMENTS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MacPherson</surname>
                           <given-names>Ian</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xiii</fpage>
                  <abstract>
                     <p>It has been an honour and a pleasure to work with others in developing this book celebrating the fiftieth anniversary of the University of Victoria (UVic). The book acknowledges, with respect and appreciation, the remarkable contribution made by its predecessor institution, Victoria College, from 1903 to 1963. It emphasizes the roles that communities – on and off campus – have played and are playing. It explores some of the many ways of knowing associated with UVic over the last fifty years.</p>
                     <p>It is an historical essay, not a history, as historians usually think about their work. It does not pretend to tell</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.5</book-part-id>
                  <title-group>
                     <title>PROLOGUE</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>People have been studying, teaching, and learning in Gordon Head, where the University of Victoria now stands, for millennia. For the Coast and Straits Salish peoples, transmitting knowledge was central to their existence – understanding intimately the world around them was essential for the good life they enjoyed.</p>
                     <p>Despite the efforts of residential schools to obliterate that understanding, it has been transmitted across generations. It is concerned with knowledge about local places, land, plants, forest animals, and water life, the kinds of understandings that can only come from a sustained, intimate, and respectful connection to the land. It provides reflections on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>ORIGINS</title>
                  </title-group>
                  <fpage>5</fpage>
                  <abstract>
                     <p>It was not an overwhelmingly obvious place for a university but it was promising. The Legislative Buildings had been completed in 1897, six years earlier, replacing the “Birdcages,” the highly unusual buildings that had served legislators, public servants, and the city of Victoria for some forty years. In that year, 1903, the Canadian Pacific Railway started regular sailings between the city and Vancouver on the<italic>Princess Victoria</italic>, destined to become a coastal icon. So too would be the hotel the CPR commissioned Francis Rattenbury to design to front the James Bay tidal flats. When completed in 1908, it would be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>BEGINNINGS</title>
                  </title-group>
                  <fpage>11</fpage>
                  <abstract>
                     <p>When first built, they were the pride of many Canadian towns and cities. They were the high schools and collegiate institutes constructed in virtually every significant Canadian community from 1890 onward. In an age when only 3 per cent of the population went to universities, they were the main final educational destiny for many young people in the expanding middle class. They were usually large and impressive, often with classical columns and motifs, monuments to the importance and solemnity of knowledge, and conspicuous legacies of the civic leaders of the times.</p>
                     <p>In 1902, therefore, Victorians were proud when a new</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>THE COLLEGE TRADITIONS</title>
                  </title-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>Academic institutions are most obviously concrete places – classrooms, libraries, laboratories, gymnasia, cafeterias, bike racks, and parking spots. They should also be places for beauty, where art, music, literature, and architecture – the gifts of talented people – are preserved, cultivated, and appreciated. Above all, though, they are places of the mind: locations for enquiry, analysis, and discussion; settings for relationships, experiences, and memory; environments for comprehension, growth, and wonderment. Their nature as concrete places is relatively easily understood. The artistic and intellectual impact is more difficult to evaluate.</p>
                     <p>Understanding the physical side of Victoria College’s history means following somewhat undignified quests for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>CONTEXTS</title>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>During the 1950s and 1960s, Canadians began to expect more from their universities. In 1951, the Massey Royal Commission on the Arts, Letters, and Sciences warned that inadequate financial investments and materialistic priorities were undermining the fundamental purposes of Canadian universities. It appealed for more support from governments and from the public. The appeal was heard. Federal as well as provincial governments increased their support for universities significantly.</p>
                     <p>The need for increased university support became clearer, too, in October 1957, when the Soviet Union launched<italic>Sputnik</italic>¹, the first Earth-orbiting artificial satellite. As it rose so did alarm in the United</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>STABILIZING YEARS, 1963–1975</title>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>In 1963, the year the University of Victoria opened, a young American poet/songwriter wrote the song “The times they are a-changin’.” It became something of an anthem for the times. It is not known whether Bob Dylan ever visited Gordon Head, but his song applied to the lands on which UVic rose. At first glance, the most obvious changes were physical. Chris Petter, now head of Special Collections in the UVic library, was a student that year. He recalls, “there wasn’t much there. It wasn’t much more than a former army camp with a few buildings on it. It was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>EXPANDING, 1975–1990</title>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>One day in late 1976, he strode into the vice-president’s office and announced, a touch of elation in his voice, “George, I think we have turned the corner.”¹ The speaker was Howard Petch. He had become president of the University of Victoria on 1 July 1975. The vice-president was George Pedersen.</p>
                     <p>Petch’s roots lay in rural Ontario. He carried with him the rigorous determination, sense of duty, and flexible entrepreneurship that can be found in those roots – what some might call the positive attributes of Old Ontario. After military service, he had gone west to UBC, where he had gained</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>PROMOTING, 1990–2000</title>
                  </title-group>
                  <fpage>83</fpage>
                  <abstract>
                     <p>David Strong’s presidential installation process in October 1990 was unusual. Previously, UVic installations had generally been low-key affairs. This one, however, featured a special convocation, carefully planned to impress people on campus, in the community, and in the university world generally. There were two main reasons for the difference. Strong had a mandate from the Board of Governors, emphasized during the search process, to promote UVic aggressively so it would receive the recognition it deserved. The second was that he came from Memorial University of Newfoundland, which, despite its relative youth,¹ took ceremonies more seriously – perhaps because of closer ties</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>CHANGING CONTOURS, 2000–2013</title>
                  </title-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>It was, of course, an honour to have been asked to consider the presidency of another university. But as David Turpin sat in his Queen’s University office – and even though he had grown up on Vancouver Island – he, like others in the Canadian university world, had only a general impression of Canada’s farthest west university. He probed, therefore, into the data comparing UVic with other Canadian universities on such aspects as research profiles, quality of students, and faculty awards. He was impressed by what he saw and asked, “Why wouldn’t I be interested? It was a place that really had</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>BALANCING</title>
                  </title-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>UVic has changed remarkably in fifty years. So too have Canadian universities generally, especially recently. Since 1995 undergraduate enrolments in Canada’s post-secondary institutions have nearly doubled, reaching some 870,000 (733,000 undergraduate, 136,000 graduate). Government support for university operating costs has more than doubled, from $5.4 billion to $11.1 billion. The number of faculty has increased by some 6,000 to 40,000. Federal government research funding (from which UVic has particularly benefitted) has grown from $1.2 billion to $4.2 billion.¹</p>
                     <p>UVic growth and development, therefore, while largely because of what people involved with it have chosen to do and have been able</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.15</book-part-id>
                  <title-group>
                     <title>APPENDICES</title>
                  </title-group>
                  <fpage>171</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.16</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>179</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1283gf.17</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>183</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
