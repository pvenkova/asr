<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30110226</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Impact of Placental Malaria on Gestational Age and Birth Weight</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>C.</given-names>
                  <surname>Menendez</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>J.</given-names>
                  <surname>Ordi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>M. R.</given-names>
                  <surname>Ismail</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>P. J.</given-names>
                  <surname>Ventura</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>J. J.</given-names>
                  <surname>Aponte</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>E.</given-names>
                  <surname>Kahigwa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>F.</given-names>
                  <surname>Font</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>P. L.</given-names>
                  <surname>Alonso</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">181</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id>i30110196</issue-id>
         <fpage>1740</fpage>
         <lpage>1745</lpage>
         <permissions>
            <copyright-statement>Copyright 2000 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30110226"/>
         <abstract>
            <p>Maternal malaria is associated with reduced birth weight, which is thought to be effected through placental insufficiency, which leads to intrauterine growth retardation (IUGR). The impact of malaria on preterm delivery is unclear. The effects of placental malaria-related changes on birth weight and gestational age were studied in 1177 mothers (and their newborns) from Tanzania. Evidence of malaria infection was found in 75.5% of placental samples. Only massive mononuclear intervillous inflammatory infiltration (MMI) was associated with increased risk of low birth weight (odds ratio [OR], 4.0). Maternal parasitized red blood cells and perivillous fibrin deposition both were associated independently with increased risk of premature delivery (OR, 3.2; OR, 2.1, respectively). MMI is an important mechanism in the pathogenesis of IUGR in malaria-infected placentas. This study also shows that placental malaria causes prematurity even in high-transmission areas. The impact of maternal malaria on infant mortality may be greater than was thought previously.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1170e244a1310">
            <label>1</label>
            <mixed-citation id="d1170e251" publication-type="other">
World Health Organization. WHO expert committee on malaria, 18th report
[tech rep 735]. Geneva: World Health Organ Tech Rep Ser 1986.</mixed-citation>
         </ref>
         <ref id="d1170e261a1310">
            <label>2</label>
            <mixed-citation id="d1170e268" publication-type="other">
McGregor IA, Wilson ME, Billewicz WZ. Malaria infection of the placenta
in The Gambia, West Africa: its incidence and relationship to stillbirth,
birth weight and placental weight. Trans R Soc Trop Med Hyg 1983; 77:
232-44.</mixed-citation>
         </ref>
         <ref id="d1170e284a1310">
            <label>3</label>
            <mixed-citation id="d1170e291" publication-type="other">
Bray RS, Sinden RE. The sequestration of Plasmodium falciparum infected
erythrocytes in the placenta. Trans R Soc Trop Med Hyg 1979;73:716-9.</mixed-citation>
         </ref>
         <ref id="d1170e301a1310">
            <label>4</label>
            <mixed-citation id="d1170e308" publication-type="other">
McGregor IA. Thoughts on malaria in pregnancy with consideration of some
factors which influence remedial strategies. Parassitologia 1987; 29:153-63.</mixed-citation>
         </ref>
         <ref id="d1170e319a1310">
            <label>5</label>
            <mixed-citation id="d1170e326" publication-type="other">
Nosten F, ter Kuile F, Maelankirri L, Decludt B, White NJ. Malaria during
pregnancy in an area of unstable endemicity. Trans R Soc Trop Med Hyg
1991;85:424-9.</mixed-citation>
         </ref>
         <ref id="d1170e339a1310">
            <label>6</label>
            <mixed-citation id="d1170e346" publication-type="other">
Meuris S, Piko BB, Eerens P, Vanbellinghen AM, Dramaix M, Hennart P.
Gestational malaria: assessment of its consequences on fetal growth. Am
J Trop Med Hyg 1993;48:603-9.</mixed-citation>
         </ref>
         <ref id="d1170e359a1310">
            <label>7</label>
            <mixed-citation id="d1170e366" publication-type="other">
Bruce-Chwatt LJ. Malaria in African infants and children in southern Ni-
geria. Ann Trop Med Parasitol 1952;46:173-7.</mixed-citation>
         </ref>
         <ref id="d1170e376a1310">
            <label>8</label>
            <mixed-citation id="d1170e383" publication-type="other">
Walter PR, Garin Y, Blot P. Placental pathologic changes in malaria: a his-
tologic and ultrastructural study. Am J Pathol 1982; 109:330-42.</mixed-citation>
         </ref>
         <ref id="d1170e393a1310">
            <label>9</label>
            <mixed-citation id="d1170e400" publication-type="other">
Galbraith RM, Fox H, Hsi B, Galbraith GMP, Bray RS, Faulk WP. The
human materno-fetal relationship in malaria. II. Histological, ultrastruc-
tural and immunopathological studies of the placenta. Trans R Soc Trop
Med Hyg 1980;74:61-72.</mixed-citation>
         </ref>
         <ref id="d1170e416a1310">
            <label>10</label>
            <mixed-citation id="d1170e423" publication-type="other">
Watkinson M, Rushton DI. Plasmodial pigmentation of placenta and out-
come of pregnancy in West African mothers. Br Med J 1983;287:251-4.</mixed-citation>
         </ref>
         <ref id="d1170e434a1310">
            <label>11</label>
            <mixed-citation id="d1170e441" publication-type="other">
Wickramasuriya GAW. Clinical features of malaria in pregnancy. In: Wick-
ramasuriya GAW, ed. Malaria and ankylostomiasis in the pregnant
woman. London: Oxford University Press, 1937:1-90.</mixed-citation>
         </ref>
         <ref id="d1170e454a1310">
            <label>12</label>
            <mixed-citation id="d1170e461" publication-type="other">
Nosten F, ter Kuile F, Maelankirri L, et al. Mefloquine prophylaxis prevents
malaria during pregnancy: a double blind placebo-controlled study. J In-
fect Dis 1994;169:595-603.</mixed-citation>
         </ref>
         <ref id="d1170e474a1310">
            <label>13</label>
            <mixed-citation id="d1170e481" publication-type="other">
Brabin BJ, Ginny M, Sapau J, Galme K, Paino J. Consequences of maternal
anemia on outcome of pregnancy in a malaria endemic area in Papua
New Guinea. Ann Trop Med Parasitol 1990;84:11-24.</mixed-citation>
         </ref>
         <ref id="d1170e494a1310">
            <label>14</label>
            <mixed-citation id="d1170e501" publication-type="other">
Watkinson M, Rushton DI, Lunn PG. Placental malaria and foetoplacental
function: low plasma oestradiols associated with malarial pigmentation
of the placenta. Trans R Soc Trop Med Hyg 1985;79:448-50.</mixed-citation>
         </ref>
         <ref id="d1170e514a1310">
            <label>15</label>
            <mixed-citation id="d1170e521" publication-type="other">
Steketee RW, Wirima JJ, Hightower AW, Slutsker L, Heymann DL, Breman
JG. The effect of malaria and malaria prevention in pregnancy on off-
spring birthweight, prematurity, and intrauterine growth retardation in
rural Malawi. Am J Trop Med Hyg 1996; 55:33-41.</mixed-citation>
         </ref>
         <ref id="d1170e537a1310">
            <label>16</label>
            <mixed-citation id="d1170e544" publication-type="other">
D'Alessandro U, Langerock P, Bennet S, Francis N, Cham K, Greenwood
BM. The impact of a national bed net programme on the outcome of
pregnancy in primigravidae in The Gambia. Trans R Soc Trop Med Hyg
1996;90:487-92.</mixed-citation>
         </ref>
         <ref id="d1170e561a1310">
            <label>17</label>
            <mixed-citation id="d1170e568" publication-type="other">
Jilly P. Anemia in parturient women, with special reference to malaria in-
fection of the placenta. Ann Trop Med Parasitol 1969;63:109-16.</mixed-citation>
         </ref>
         <ref id="d1170e578a1310">
            <label>18</label>
            <mixed-citation id="d1170e585" publication-type="other">
Archibald HM. The influence of malaria infection of the placenta on the
incidence of prematurity. Bull World Health Organ 1956;15:842-5.</mixed-citation>
         </ref>
         <ref id="d1170e595a1310">
            <label>19</label>
            <mixed-citation id="d1170e602" publication-type="other">
Menendez C, Kahigwa E, Hirt R, et al. Randomised placebo-controlled trial
of iron supplementation and malaria chemoprophylaxis for prevention of
severe anemia and malaria in Tanzanian infants. Lancet 1997; 350:844-50.</mixed-citation>
         </ref>
         <ref id="d1170e615a1310">
            <label>20</label>
            <mixed-citation id="d1170e622" publication-type="other">
Smith T, Charlwood JD, Kihonda J, et al. Absence of seasonal variation in
malaria parasitaemia in an area of intense seasonal transmission. Acta
Trop 1993;54:55-72.</mixed-citation>
         </ref>
         <ref id="d1170e635a1310">
            <label>21</label>
            <mixed-citation id="d1170e644" publication-type="other">
Hatz C, Abdulla S, Mull R, et al. Efficacy and safety of CGP 56697 (arte-
mether and benflumetol) compared with chloroquine to treat acute fal-
ciparum malaria in Tanzanian children aged 1-5 years. Trop Med Int
Health 1998;3:498-504.</mixed-citation>
         </ref>
         <ref id="d1170e660a1310">
            <label>22</label>
            <mixed-citation id="d1170e667" publication-type="other">
Dubowitz LMS, Dubowitz V, Goldberg C. Clinical assessment of gestational
age in the newborn infant. J Pediatr 1970;77:1-10.</mixed-citation>
         </ref>
         <ref id="d1170e678a1310">
            <label>23</label>
            <mixed-citation id="d1170e685" publication-type="other">
Ismail MR, Ordi J, Menendez C, et al. Placental pathology in malaria: an
histological, immunohistochemical and quantitative study. Hum Pathol
2000;31:85-93.</mixed-citation>
         </ref>
         <ref id="d1170e698a1310">
            <label>24</label>
            <mixed-citation id="d1170e705" publication-type="other">
Garner PA, Dubowitz L, Baea M, Lai D, Dubowitz M, Heywood P. Birth-
weight and gestation of village deliveries in Papua New Guinea. J Trop
Pediatr 1994;40:37-40.</mixed-citation>
         </ref>
         <ref id="d1170e718a1310">
            <label>25</label>
            <mixed-citation id="d1170e725" publication-type="other">
Ordi J, Ismail MR, Ventura PJ, et al. Massive chronic intervellositis of the
placenta associated with malaria infection. Am J Surg Pathol 1998; 22:
1006-11.</mixed-citation>
         </ref>
         <ref id="d1170e738a1310">
            <label>26</label>
            <mixed-citation id="d1170e745" publication-type="other">
Steinborn A, Geisse M, Kaufmann M. Expression of cytokine receptors in
the placenta in term and preterm labour. Placenta 1998; 19:165-70.</mixed-citation>
         </ref>
         <ref id="d1170e755a1310">
            <label>27</label>
            <mixed-citation id="d1170e764" publication-type="other">
Stallmach T, Hebisch G, Joller-Jemelka HI, Orban P, Schwaller J, Engelmann
M. Cytokine production and visualized effects in the feto-maternal unit:
quantitative and topographic data on cytokines during intrauterine dis-
ease. Lab Invest 1995;73:384-92.</mixed-citation>
         </ref>
         <ref id="d1170e780a1310">
            <label>28</label>
            <mixed-citation id="d1170e787" publication-type="other">
Fried M, Muga RO, Misore A, Duffy PE. Malaria elicits type 1 cytokines
in the human placenta: IFN-γ and TNF-α associated with pregnancy
outcomes. J Immunol 1998;160:2523-30.</mixed-citation>
         </ref>
         <ref id="d1170e801a1310">
            <label>29</label>
            <mixed-citation id="d1170e808" publication-type="other">
Vassiliadis S, Tsoukatos D, Athanasakis I. Interferon-induced class II ex-
pression at the spongiotrophoblastic zone of the murine placenta is linked
to fetal rejection and developmental abnormalities. Acta Physiol Scand
1994;151:485-95.</mixed-citation>
         </ref>
         <ref id="d1170e824a1310">
            <label>30</label>
            <mixed-citation id="d1170e831" publication-type="other">
Brabin BJ. An analysis of malaria in pregnancy in Africa. Bull World Health
Organ 1983;61:1005-16.</mixed-citation>
         </ref>
         <ref id="d1170e841a1310">
            <label>31</label>
            <mixed-citation id="d1170e848" publication-type="other">
Gazin PP, Compaore MP, Hutin Y, Molez JF. Placental infections with Plas-
modium in an endemic zone: risk factors. Bull Soc Pathol Exot 1994; 87:
97-100.</mixed-citation>
         </ref>
         <ref id="d1170e861a1310">
            <label>32</label>
            <mixed-citation id="d1170e868" publication-type="other">
Barros FC, Huttly SRA, Victora CG, Kirkwood BR, Vaughan JP. Com-
parison of the causes and consequences of prematurity and intrauterine
growth retardation: longitudinal study in southern Brazil. Pediatrics
1992;90:238-44.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
