<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envicons</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020266</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environmental Conservation</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CAMBRIDGE UNIVERSITY PRESS</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03768929</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694387</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44519998</article-id>
         <title-group>
            <article-title>The livelihood impacts of the Namibian community based natural resource management programme: a meta-synthesis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>HELEN</given-names>
                  <surname>SUICH</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">37</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40189782</issue-id>
         <fpage>45</fpage>
         <lpage>53</lpage>
         <permissions>
            <copyright-statement>© Foundation for Environmental Conservation 2010</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44519998"/>
         <abstract>
            <p>Community based natural resource management (CBNRM) programmes aim to achieve the joint objectives of biodiversity conservation and improved rural livelihoods by providing incentives to sustainably manage relevant resources. Since 1998, more than 50 natural resource management institutions, known as conservancies, have been established in order to manage wildlife resources, on communal lands in Namibia. The national programme is often cited as a CBNRM success; however, despite its rapid spread, there are few systematically collected or analysed household-level data which demonstrate the long-term ecological, social and economic impacts of Namibian programme. A meta-synthesis was undertaken to determine the range of positive and negative livelihood impacts resulting from CBNRM programme activities in two key regions, and the factors affecting how these impacts have been felt by households or individuals. Impacts were categorized according to any changes in access to and/or returns from the five key assets of the sustainable livelihoods framework, namely financial, human, natural, physical and social assets. Positive and negative impacts were felt on financial, human, natural and social assets; only positive impacts were identified as affecting physical assets. Individual-and household-level impacts differed depending on the specific activities implemented locally and, according to the duration, frequency and timing of the impacts, the circumstances and preferences of households and their access to particular activities and consequent impacts. If a greater understanding of the extent and importance of different impacts is to be gained in the future, more rigorous and comprehensive data collection and analysis will need to be undertaken. Analyses will need to consider the whole range of activities implemented, both the benefits and costs associated with these different activities, and will also need to provide contextual information to allow the relative importance of impacts resulting from CBNRM activities to be better understood.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d4112e141a1310">
            <mixed-citation id="d4112e145" publication-type="other">
Adams, W. (2004) Against Extinction. The Story of Conservation.
London, UK: Earthscan.</mixed-citation>
         </ref>
         <ref id="d4112e155a1310">
            <mixed-citation id="d4112e159" publication-type="other">
Adams, W. &amp; Hulme, D. (2001) Conservation and community.
Changing narratives, policies and practices in African
conservation. In: African Wildlife and Livelihoods. The Promise
and Performance of Community Conservation, ed. D. Hulme &amp; M.
Murphree, pp. 9-23. London, UK: James Currey.</mixed-citation>
         </ref>
         <ref id="d4112e178a1310">
            <mixed-citation id="d4112e182" publication-type="other">
Agra wal, A. &amp; Gibson, C.C. (1999) Enchantment and
disenchantment: the role of community in natural resource
conservation. World Development 27(4): 629-649.</mixed-citation>
         </ref>
         <ref id="d4112e195a1310">
            <mixed-citation id="d4112e199" publication-type="other">
Attree, P. (2006) The social costs of child poverty: a systematic review
of the qualitative evidence. Children and Society 20(1): 54-66.</mixed-citation>
         </ref>
         <ref id="d4112e210a1310">
            <mixed-citation id="d4112e214" publication-type="other">
Bair, C. &amp; Haworth, J. (2005) Doctoral student attrition and
persistence: a meta-synthesis of research. In: Higher Education:
Handbook of Theory and Research, ed. J.C. Smart, pp. 481-534.
Dordrecht: Kluwer Academic Press.</mixed-citation>
         </ref>
         <ref id="d4112e230a1310">
            <mixed-citation id="d4112e234" publication-type="other">
Bandyopadhyay, S., Humavindu, M., Shyamsundar, P. &amp; Wang,
L. (2004) Do households gain from community based natural
resource management? An evaluation of community conservancies
in Namibia. Policy Research Working Paper, World Bank,
Washington DC, USA.</mixed-citation>
         </ref>
         <ref id="d4112e253a1310">
            <mixed-citation id="d4112e257" publication-type="other">
Blaikie, P. (2006) Is small really beautiful? Community-based
natural resource management in Malawi and Botswana. World
Development 34(11): 1942-1957.</mixed-citation>
         </ref>
         <ref id="d4112e270a1310">
            <mixed-citation id="d4112e274" publication-type="other">
Bond, I. (2001) CAMPFIRE and the incentives for institutional
change. In: African Wildlife and Livelihoods. The Promise and
Performance of Community Conservation, ed. D. Hulme &amp; M.
Murphree, pp. 227-243, Oxford, UK: Heinemann.</mixed-citation>
         </ref>
         <ref id="d4112e290a1310">
            <mixed-citation id="d4112e294" publication-type="other">
Bondas, T. &amp; Hall, E.O.C. (2007) Challenges in approaching
meta-synthesis research. Qualitative Health Research 17(1):
113-121.</mixed-citation>
         </ref>
         <ref id="d4112e307a1310">
            <mixed-citation id="d4112e311" publication-type="other">
Boudreaux, K. (2007) Community based natural resource
management and poverty alleviation in Namibia: a case study.
Mercatus Policy Series Policy Comment No. 10, George Mason
University, Fairfax, VA, USA.</mixed-citation>
         </ref>
         <ref id="d4112e328a1310">
            <mixed-citation id="d4112e332" publication-type="other">
Carney, D., ed. (1998) Sustainable Rural Livelihoods: What
Contribution Can We Make? London, UK: DFID.</mixed-citation>
         </ref>
         <ref id="d4112e342a1310">
            <mixed-citation id="d4112e348" publication-type="other">
Carney, D., Drinkwater, M., Rusinów, T., Neefjes, K., Wanmali,
S. &amp; Singh, N. (1999) Livelihood Approaches Compared. A Brief
Comparison of the Livelihoods Approaches of the UK Department for
International Development, CARE, Oxfam and the United Nations
Development Programme. London, UK: DFID.</mixed-citation>
         </ref>
         <ref id="d4112e367a1310">
            <mixed-citation id="d4112e371" publication-type="other">
Davidson, A., ed. (2008) Case Studies on Community Based Tourism
Enterprises. Windhoek, Namibia: WWF-LIFE.</mixed-citation>
         </ref>
         <ref id="d4112e381a1310">
            <mixed-citation id="d4112e385" publication-type="other">
DFID (1999) Sustainable Livelihood Guidance Sheets. London, UK:
DFID.</mixed-citation>
         </ref>
         <ref id="d4112e395a1310">
            <mixed-citation id="d4112e399" publication-type="other">
Emerton, L. (1998) The nature of benefits and the benefits of
nature: why wildlife conservation has not economically benefited
communities in Africa. Community Conservation in Africa
Working Paper No.5/1998, IDPM, Manchester, UK.</mixed-citation>
         </ref>
         <ref id="d4112e415a1310">
            <mixed-citation id="d4112e419" publication-type="other">
Ferraro, P.J. (2009) Counterfactual thinking and impact evaluation
in environmental policy. New Directions for Evaluation 2009(122):
75-84.</mixed-citation>
         </ref>
         <ref id="d4112e433a1310">
            <mixed-citation id="d4112e437" publication-type="other">
Ferraro, P. &amp; Pattanayak, S. (2006) Money for nothing? A call
for empirical evaluation of biodiversity conservation investments.
PLoS Biology 4(4): 482-488.</mixed-citation>
         </ref>
         <ref id="d4112e450a1310">
            <mixed-citation id="d4112e454" publication-type="other">
Finfgeld, D.L. (2003) Meta-synthesis: the state of the art -so far.
Qualitative Health Research 13(7): 893-904.</mixed-citation>
         </ref>
         <ref id="d4112e464a1310">
            <mixed-citation id="d4112e468" publication-type="other">
Ghimire, K. &amp; Pimbert, M., eds (1997) Social Change and
Conservation: Environmental Politics and Impacts of National Parks
and Protected Areas. London, UK: Earthscan.</mixed-citation>
         </ref>
         <ref id="d4112e481a1310">
            <mixed-citation id="d4112e485" publication-type="other">
Hulme, D. &amp; Murphree, M. (1999) Communities, wildlife and the
'new conservation' in Africa. Journal of International Development
11(2): 277-85.</mixed-citation>
         </ref>
         <ref id="d4112e498a1310">
            <mixed-citation id="d4112e502" publication-type="other">
Hulme, D. &amp; Murphree, M. (2001) Community conservation in
Africa. An introduction. In: African Wildlife and Livelihoods. The
Promise and Performance of Community Conservation, ed. D. Hulme
&amp; M. Murphree, pp. 1-8, London, UK: James Currey.</mixed-citation>
         </ref>
         <ref id="d4112e518a1310">
            <mixed-citation id="d4112e522" publication-type="other">
IIED (1994) Whose Eden? An Overview of Community Approaches to
Wildlife Management. London, UK: IIED.</mixed-citation>
         </ref>
         <ref id="d4112e533a1310">
            <mixed-citation id="d4112e537" publication-type="other">
Jacobsohn, M. &amp; Owen-Smith, G. (2003) Integrating conservation
and development. A Namibian case study. Nomadic Peoples 7(1):
92-109.</mixed-citation>
         </ref>
         <ref id="d4112e550a1310">
            <mixed-citation id="d4112e554" publication-type="other">
Jones, B. (2006) Case studies on successful southern African
NRM initiatives and their impacts on poverty and governance.
Country study: Namibia. Promoting integrated natural resource
management as a means to combat desertification: the Living
In A Finite Environment (LIFE) project and the #Khoadi
//Hoas Conservancy, Namibia. Paper prepared for the
United States Agency for International Development and the
IUCN Transboundary Protected Areas Research Initiative
[www document]. URL http://www.irgltd.com/Resources/
Publications/Africa/Namibia%20case%20studies%20phase%
20 1 %20final%20report%20 v2.pdf</mixed-citation>
         </ref>
         <ref id="d4112e592a1310">
            <mixed-citation id="d4112e596" publication-type="other">
Jones, B. &amp; Murphree, M.W. (2004) Community-based natural
resource management as a conservation mechanism: lessons and
directions. In: Parks in Transition. Biodiversity, Rural Development
and the Bottom Line, ed. B. Child, pp. 63-103. London, UK:
Earthscan.</mixed-citation>
         </ref>
         <ref id="d4112e615a1310">
            <mixed-citation id="d4112e619" publication-type="other">
Jones, B. &amp; Weaver, L.C. (2009) CBNRM in Namibia: Growth,
trends, lessons and constraints. In: Evolution and Innovation in
Wildlife Conservation. Game Parks and Transfrontier Conservation
Areas, ed. H. Suich, B. Child &amp; A. Spenceley, pp. 223-242.
London, UK: Earthscan.</mixed-citation>
         </ref>
         <ref id="d4112e638a1310">
            <mixed-citation id="d4112e642" publication-type="other">
Kiss, A., ed. (1990) Living with wildlife. Wildlife resource
management with local participation in Africa. World Bank
Technical Paper. World Bank, Washington DC, USA.</mixed-citation>
         </ref>
         <ref id="d4112e655a1310">
            <mixed-citation id="d4112e659" publication-type="other">
Levine, S. (2007) Trends in human development and human
poverty in Namibia. Background paper to the Namibian
Human Development Report, UNDP, Windhoek, Namibia,
[www document] URL http://www.sarpn.org.za/documents/
d0002886ZHD_report_Namibia_UNDP_Oct2007.pdf</mixed-citation>
         </ref>
         <ref id="d4112e679a1310">
            <mixed-citation id="d4112e683" publication-type="other">
Marshall, G. (2005) Economies for Collaborative Environmental
Management. Renegotiating the Commons. London, UK: Earthscan.</mixed-citation>
         </ref>
         <ref id="d4112e693a1310">
            <mixed-citation id="d4112e697" publication-type="other">
Mendelsohn, J., Jarvis, A., Roberts, C. &amp; Robertson, T. (2002) Atlas
of Namibia. Cape Town, South Africa: David Phillip.</mixed-citation>
         </ref>
         <ref id="d4112e707a1310">
            <mixed-citation id="d4112e711" publication-type="other">
Mosimane, A. &amp; Muranda, N. (2007) Torra Conservancy's
contribution to poverty alleviation. MRC Research Report,
University of Namibia/Multidisciplinary Research Centre,
Windhoek, Namibia.</mixed-citation>
         </ref>
         <ref id="d4112e727a1310">
            <mixed-citation id="d4112e731" publication-type="other">
Mulonga, S. and Murphy, C. (2003) Spending the money: The
experience of conservancy benefit distribution in Namibia up
to mid-2003. Research Discussion Paper No. 63 Windhoek:
DEA/MET.</mixed-citation>
         </ref>
         <ref id="d4112e747a1310">
            <mixed-citation id="d4112e751" publication-type="other">
Murphree, M. (1993) Communities as Resource Management
Institutions. Gatekeeper Series No. 36. London, UK: IIED.</mixed-citation>
         </ref>
         <ref id="d4112e761a1310">
            <mixed-citation id="d4112e765" publication-type="other">
NACSO (2007) Namibia's Communal Conservancies. A Review of
Progress and Challenges in 2007. Windhoek, Namibia: NACSO.</mixed-citation>
         </ref>
         <ref id="d4112e776a1310">
            <mixed-citation id="d4112e780" publication-type="other">
Neumann, R.P. (2005) Model, panacea, or exception? Contextualiz-
ing CAMPFIRE and related programs in Africa. In: Communities
and Conservation. Histories and Politics of Community Based Natural
Resource Management, ed. J.P. Brosius, A.L. Tsing &amp; C. Zerner,
pp. 177-193, Walnut Creek, CA, USA: Alta Mira Press.</mixed-citation>
         </ref>
         <ref id="d4112e799a1310">
            <mixed-citation id="d4112e803" publication-type="other">
Nott, K. &amp; Curtis, B. (2005) Aromatic resins from Commiphora trees.
Roan News. Wildlife Society of Namibia, Windhoek, Namibia.</mixed-citation>
         </ref>
         <ref id="d4112e813a1310">
            <mixed-citation id="d4112e817" publication-type="other">
Ogbaharya, D.G. (2006) A capability theory of CBNRM: the case of
Namibia's communal conservancy programme. Paper prepared
for the International Conference of the Human Development
and Capability Association, the International Conference of the
Human Development and Capability Association on 'Freedom and
Justice', Groningen, The Netherlands, August 29-September 1,
2006 [www document]. URL http://www.equatorinitiative.org/
index.php?option=com_docman&amp;task=doc_download&amp;gid=
423&amp;Itemid=398&amp;lang=en</mixed-citation>
         </ref>
         <ref id="d4112e849a1310">
            <mixed-citation id="d4112e853" publication-type="other">
Pagiola, S., Arcenas, A. &amp; Platais, G. (2005) Can payments for
environmental services help reduce poverty? An exploration of
the issues and the evidence to date from Latin America. World
Development 33(2): 237-253.</mixed-citation>
         </ref>
         <ref id="d4112e869a1310">
            <mixed-citation id="d4112e873" publication-type="other">
Pagiola, S., Rios, A.R. &amp; Arcenas, A. (2008) Can the poor participate
in payments for environmental services? Lessons from the
Silvopastoral Project in Nicaragua. Environment and Development
Economics 13(3): 299-325.</mixed-citation>
         </ref>
         <ref id="d4112e889a1310">
            <mixed-citation id="d4112e893" publication-type="other">
Shackleton, S., Shackleton, C., Netshiluvhi, T., Geach, B., Ballance,
A. &amp; Fairbanks, D. (2002) Use patterns and value of savannah
resources in three rural villages in South Africa. Economic Botany
56(2): 130-146.</mixed-citation>
         </ref>
         <ref id="d4112e910a1310">
            <mixed-citation id="d4112e914" publication-type="other">
Suich, H. (2003) Summary of partial results from the socio-
economic household survey regarding community based
natural resource management and livelihoods in Caprivi and
Kunene. WILD Working Paper No. 12. WILD, Windhoek,
Namibia.</mixed-citation>
         </ref>
         <ref id="d4112e933a1310">
            <mixed-citation id="d4112e937" publication-type="other">
Thorne, S., Jensen, L., Kearney, M.H., Noblit, G. &amp;
Sandelowski, M. (2004) Qualitative meta-synthesis: reflections
on methodological orientation and ideological agenda. Qualitative
Health Research 14(10): 1342-1365.</mixed-citation>
         </ref>
         <ref id="d4112e953a1310">
            <mixed-citation id="d4112e957" publication-type="other">
Vaughan, C., Mulonga, S. and Katjiua, J.B. (2003b) Cash from
conservation: Torra community tastes the benefits. A short
survey and review of the Torra Conservancy cash payouts to
individual registered members. WILD Working Paper No. 15.
WILD, Windhoek, Namibia.</mixed-citation>
         </ref>
         <ref id="d4112e976a1310">
            <mixed-citation id="d4112e980" publication-type="other">
Walsh, D. &amp; Downe, S. (2005) Meta-synthesis method for qualitative
research: a literature review. Journal of Advanced Nursing 50(2):
204-211.</mixed-citation>
         </ref>
         <ref id="d4112e993a1310">
            <mixed-citation id="d4112e997" publication-type="other">
Wunder, S. (2008) Payments for environmental services and the poor:
concepts and preliminary evidence. Environment and Development
Economics 13(3): 279-297.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
