<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id journal-id-type="jstor">j100641</journal-id>
         <journal-title-group>
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Carfax Publishing, Taylor and Francis Ltd.</publisher-name>
         </publisher>
         <issn pub-type="ppub">03057070</issn>
         <issn pub-type="epub">14653893</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">823351</article-id>
         <title-group>
            <article-title>Funerals and the Public Space of Sentiment in Botswana</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Deborah</given-names>
                  <surname>Durham</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Frederick</given-names>
                  <surname>Klaits</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">28</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i233897</issue-id>
         <fpage>777</fpage>
         <lpage>795</lpage>
         <page-range>777-795</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 Taylor &amp; Francis Limited</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/823351"/>
         <abstract>
            <p>In Botswana, funerals are key to the exercise of civil conduct. Funerals constitute distinctive public spaces that focus local attention on how particular persons' sentiments influence the well-being of others. By managing the social impact of sentiments of sorrow, love, jealousy, anger and resignation, all those who attend funerals ideally maintain a footing of civility, preventing recognised differences from causing permanent disruptions in social relations. In the context of death, people shape forms of community and difference - along lines of ethnicity, class, religion, gender and kinship - through the mutuality of their emotions. Funerals thus give rise to a public space and a civil discourse based on sentiment, as distinct from the bureaucratic and rationalising practices of official nationalism. This article is based on the authors' respective fieldwork in a Herero minority community in Mahalapye, and with an Apostolic church in Gaborone.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d239e132a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d239e141" publication-type="journal">
D. Durham, 'Love and Jealousy in the Space of Death', Ethnos, 67 (2002), p. 155-180.<person-group>
                     <string-name>
                        <surname>Durham</surname>
                     </string-name>
                  </person-group>
                  <fpage>155</fpage>
                  <volume>67</volume>
                  <source>Ethnos</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e170a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d239e177" publication-type="book">
J. Habermas, The Structural Transformation of the Public Sphere (Cambridge, MA, MIT Press, 1989)<person-group>
                     <string-name>
                        <surname>Habermas</surname>
                     </string-name>
                  </person-group>
                  <source>The Structural Transformation of the Public Sphere</source>
                  <year>1989</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e198" publication-type="book">
J.
Habermas, 'Further Reflections on the Public Sphere', in C. Calhoun (ed), Habermas and the Public Sphere
(Cambridge, MA, MIT Press, 1992), pp. 421-461.<person-group>
                     <string-name>
                        <surname>Habermas</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Further Reflections on the Public Sphere</comment>
                  <fpage>421</fpage>
                  <source>Habermas and the Public Sphere</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e232" publication-type="compound">
C. Calhoun, 'Introduction: Habermas and the Public
Sphere' and 'Concluding Remarks', in Calhoun (ed), Habermas and the Public Sphere, pp. 1-48 and 462-479
(especially pp. 473-475)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e245" publication-type="journal">
P. Probst, 'Mchape' '95, or The Sudden Fame of Billy Goodson Chisupe: Healing,
Social Memory and the Enigma of the Public Sphere in Post-Banda Malawi', Africa, 60 (1999), pp. 108-137.<object-id pub-id-type="doi">10.2307/1161079</object-id>
                  <fpage>108</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e261a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d239e268" publication-type="book">
I. M. Young, 'Communication and the Other: Beyond Deliberative Democracy', in S. Benhabib (ed), Democracy
and Difference: Contesting the Boundaries of the Political (Princeton, Princeton University Press, 1996), p. 129.<person-group>
                     <string-name>
                        <surname>Young</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Communication and the Other: Beyond Deliberative Democracy</comment>
                  <fpage>129</fpage>
                  <source>Democracy and Difference: Contesting the Boundaries of the Political</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e300a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d239e307" publication-type="book">
C. Lutz, Unnatural Emotions: Everyday Sentiments on a Western Atoll and their Challenge to Western Theory
(Chicago, University of Chicago Press, 1988)<person-group>
                     <string-name>
                        <surname>Lutz</surname>
                     </string-name>
                  </person-group>
                  <source>Unnatural Emotions: Everyday Sentiments on a Western Atoll and their Challenge to Western Theory</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e331" publication-type="journal">
Durham, 'Love
and Jealousy'  </mixed-citation>
            </p>
         </fn>
         <fn id="d239e344a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d239e351" publication-type="book">
M. Karlström, 'Civil Society and its Presuppositions: Lessons from Uganda', in J. L. and J. Comaroff (eds),
Civil Society and the Political Imagination in Africa (Chicago, University of Chicago Press, 1999), pp. 108-109.<person-group>
                     <string-name>
                        <surname>Karlström</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Civil Society and its Presuppositions: Lessons from Uganda</comment>
                  <fpage>108</fpage>
                  <source>Civil Society and the Political Imagination in Africa</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e383a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d239e390" publication-type="book">
N. Munn, The Fame of Gawa (Cambridge, Cambridge University Press, 1986)<person-group>
                     <string-name>
                        <surname>Munn</surname>
                     </string-name>
                  </person-group>
                  <source>The Fame of Gawa</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e411" publication-type="book">
A. Weiner, Women of Value, Men
of Renown (Austin, TX, University of Texas Press, 1976)<person-group>
                     <string-name>
                        <surname>Weiner</surname>
                     </string-name>
                  </person-group>
                  <source>Women of Value, Men of Renown</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e435" publication-type="book">
F. H. Damon and R. Wagner (eds), Death Rituals and
Life in the Societies of the Kula Ring (DeKalb, IL, Northern Illinois University Press, 1989).<person-group>
                     <string-name>
                        <surname>Damon</surname>
                     </string-name>
                  </person-group>
                  <source>Death Rituals and Life in the Societies of the Kula Ring</source>
                  <year>1989</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e460a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d239e467" publication-type="book">
M. Fortes, Oedipus and Job in West African Religion (Cambridge, Cambridge University Press, 1959).<person-group>
                     <string-name>
                        <surname>Fortes</surname>
                     </string-name>
                  </person-group>
                  <source>Oedipus and Job in West African Religion</source>
                  <year>1959</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e488" publication-type="book">
D. W. Cohen and E. S. A. Odhiambo, Burying SM: the Politics of Knowledge
and the Sociology of Power in Africa (Portsmouth, NH, Heinemann, 1992)<person-group>
                     <string-name>
                        <surname>Cohen</surname>
                     </string-name>
                  </person-group>
                  <source>Burying SM: the Politics of Knowledge and the Sociology of Power in Africa</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e512" publication-type="journal">
P. Geschiere and F. Nyamnjoh,
'Capitalism and Autochthony: the Seesaw of Mobility and Belonging', Public Culture, 12,2 (2000), pp. 423-452<person-group>
                     <string-name>
                        <surname>Geschiere</surname>
                     </string-name>
                  </person-group>
                  <issue>2</issue>
                  <fpage>423</fpage>
                  <volume>12</volume>
                  <source>Public Culture</source>
                  <year>2000</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e547" publication-type="book">
J. Goody, Death, Property and the Ancestors (Stanford, CA, Stanford University Press, 1962)<person-group>
                     <string-name>
                        <surname>Goody</surname>
                     </string-name>
                  </person-group>
                  <source>Death, Property and the Ancestors</source>
                  <year>1962</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e568" publication-type="journal">
I. Cunnison,
'Perpetual Kinship: a Political Institution of the Luapula Peoples', Rhodes Livingstone Journal (1956).<person-group>
                     <string-name>
                        <surname>Cunnison</surname>
                     </string-name>
                  </person-group>
                  <source>Rhodes Livingstone Journal</source>
                  <year>1956</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e593a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d239e600" publication-type="book">
B. Anderson, Imagined Communities: Reflections on the Origin and Spread of Nationalism, revised edition
(London, Verso, 1991).<person-group>
                     <string-name>
                        <surname>Anderson</surname>
                     </string-name>
                  </person-group>
                  <source>Imagined Communities: Reflections on the Origin and Spread of Nationalism</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e624" publication-type="book">
G. Feeley-Hamick, A Green Estate: Restoring
Independence in Madagascar (Washington, DC, Smithsonian Institution Press, 1991)<person-group>
                     <string-name>
                        <surname>Feeley-Hamick</surname>
                     </string-name>
                  </person-group>
                  <source>A Green Estate: Restoring Independence in Madagascar</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e648" publication-type="book">
D. Lan, Guns and Rain:
Guerrillas and Spirit Mediums in Zimbabwe (London and Berkeley, James Currey and University of California
Press, 1985)<person-group>
                     <string-name>
                        <surname>Lan</surname>
                     </string-name>
                  </person-group>
                  <source>Guns and Rain: Guerrillas and Spirit Mediums in Zimbabwe</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e677" publication-type="book">
J. B. Peires, The Dead Will Arise: Nongqawuse and the Great Xhosa Cattle-Killing Movement of
1856-7 (Bloomington, IN, Indiana University Press, 1989).<person-group>
                     <string-name>
                        <surname>Peires</surname>
                     </string-name>
                  </person-group>
                  <source>The Dead Will Arise: Nongqawuse and the Great Xhosa Cattle-Killing Movement of 1856-7</source>
                  <year>1989</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e701" publication-type="book">
J. R. Gillis
(ed), Commemorations: The Politics of National Identity (Princeton, NJ, Princeton University Press, 1994)<person-group>
                     <string-name>
                        <surname>Gillis</surname>
                     </string-name>
                  </person-group>
                  <source>Commemorations: The Politics of National Identity</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e725" publication-type="book">
R.
Werbner, 'Smoke from the Barrel of a Gun: Postwars of the Dead, Memory and Reinscription in Zimbabwe', in
R. Werbner (ed), Memory and the Postcolony (London, Zed Books, 1996).<person-group>
                     <string-name>
                        <surname>Werbner</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Smoke from the Barrel of a Gun: Postwars of the Dead, Memory and Reinscription in Zimbabwe</comment>
                  <source>Memory and the Postcolony</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e757a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d239e764" publication-type="journal">
S. Grant, 'Death and Burial in Mochudi: a Study in Changing Traditions', Botswana Notes and Records, 19(1987),
pp. 137-146<person-group>
                     <string-name>
                        <surname>Grant</surname>
                     </string-name>
                  </person-group>
                  <fpage>137</fpage>
                  <volume>19</volume>
                  <source>Botswana Notes and Records</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e795" publication-type="book">
K. Mogapi, Ngwao ya Setswana, 3rd edn (Gaborone, Mmampodi Publishers, 1998), pp. 411-412.<person-group>
                     <string-name>
                        <surname>Mogapi</surname>
                     </string-name>
                  </person-group>
                  <edition>3</edition>
                  <fpage>411</fpage>
                  <source>Ngwao ya Setswana</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e824a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d239e831" publication-type="other">
D. Durham, 'Images of Culture: Being Herero in a Liberal Democracy' (PhD thesis, University of Chicago,
1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e840" publication-type="journal">
'The Predicament of Dress: Polyvalency and the Ironies of a Cultural Identity', American Ethnologist, 26,
2 (1999), pp. 389-41 1<object-id pub-id-type="jstor">10.2307/647292</object-id>
                  <fpage>389</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e855" publication-type="book">
'Uncertain Citizens: the New Intercalary Subject in Postcolonial Botswana', in R. Werbner
(ed), Postcolonial Subjectivities in Africa (London, Zed Books, 2002, pp. 139-170)<comment content-type="section">Uncertain Citizens: the New Intercalary Subject in Postcolonial Botswana</comment>
                  <fpage>139</fpage>
                  <source>Postcolonial Subjectivities in Africa</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e879a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d239e886" publication-type="book">
J. van Nostrand, Old Naledi: The Village Becomes a Town (Toronto, James Lorimer, 1982).<person-group>
                     <string-name>
                        <surname>van Nostrand</surname>
                     </string-name>
                  </person-group>
                  <source>Old Naledi: The Village Becomes a Town</source>
                  <year>1982</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e908a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d239e915" publication-type="other">
F. Klaits, 'Housing the Spirit, Hearing the Voice: Care and Kinship in an Apostolic Church during Botswana's
Time of AIDS' (PhD thesis, Johns Hopkins University, 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d239e925a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d239e932" publication-type="journal">
D. Durham, 'Love and Jealousy'  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e940" publication-type="book">
A. Ashforth, Madumo, A Man
Bewitched (Chicago, University of Chicago Press, 2000)<person-group>
                     <string-name>
                        <surname>Ashforth</surname>
                     </string-name>
                  </person-group>
                  <source>Madumo, A Man Bewitched</source>
                  <year>2000</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e964" publication-type="book">
B. Weiss, The Making and Unmaking of the Haya Lived
World (Durham, NC, Duke University Press, 1996), p. 216<person-group>
                     <string-name>
                        <surname>Weiss</surname>
                     </string-name>
                  </person-group>
                  <fpage>216</fpage>
                  <source>The Making and Unmaking of the Haya Lived World</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e993" publication-type="book">
P. Geschiere, The Modernity of Witchcraft
(Charlottesville, VA, University Press of Virginia, 1997).<person-group>
                     <string-name>
                        <surname>Geschiere</surname>
                     </string-name>
                  </person-group>
                  <source>The Modernity of Witchcraft</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1018a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d239e1025" publication-type="journal">
S. Grant, 'Death and Burial in Mochudi'  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1033" publication-type="journal">
B. Ingstad, 'The Cultural Construction of AIDS and its Consequences
for Prevention in Botswana', Medical Anthropology Quarterly, 4 (1988), pp. 28-40<person-group>
                     <string-name>
                        <surname>Ingstad</surname>
                     </string-name>
                  </person-group>
                  <fpage>28</fpage>
                  <volume>4</volume>
                  <source>Medical Anthropology Quarterly</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1064" publication-type="journal">
B. A. Pauw, 'Widows and
Ritual Danger in Sotho and Tswana Communities', African Studies, 49 (1990), pp. 75-100.<person-group>
                     <string-name>
                        <surname>Pauw</surname>
                     </string-name>
                  </person-group>
                  <fpage>75</fpage>
                  <volume>49</volume>
                  <source>African Studies</source>
                  <year>1990</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1096" publication-type="journal">
I. Schapera, 'Kgatla Notions of Ritual Impurity', African Studies, 38 (1979), pp. 3-15.<person-group>
                     <string-name>
                        <surname>Schapera</surname>
                     </string-name>
                  </person-group>
                  <fpage>3</fpage>
                  <volume>38</volume>
                  <source>African Studies</source>
                  <year>1979</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1125a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d239e1132" publication-type="book">
2 Samuel 2:15-23<comment content-type="section">2 Samuel 2:15-23</comment>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1142a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d239e1149" publication-type="book">
N. Parsons, W. Henderson and T. Tlou, Seretse Khama: 1921-1980 (Braamfontein, Macmillan,
1995).<person-group>
                     <string-name>
                        <surname>Parsons</surname>
                     </string-name>
                  </person-group>
                  <source>Seretse Khama: 1921-1980</source>
                  <year>1995</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1175a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d239e1182" publication-type="journal">
Durham, 'The Predicament of Dress'.  </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1191a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d239e1198" publication-type="journal">
D. Durham, 'Soliciting
Gifts and Negotiating Agency: the Spirit of Asking in Botswana', Journal of the Royal Anthropological Institute,
1 (1995), pp. 111-128.<object-id pub-id-type="doi">10.2307/3034231</object-id>
                  <fpage>111</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1217a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d239e1224" publication-type="journal">
Durham, 'Love and Jealousy'.  </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1233a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d239e1240" publication-type="book">
D. Durham, 'Passports and Persons: the Insurrection of Subjugated Knowledges in Southern
Africa', in C. Crais (ed), Passes, Passports, and the State: Rethinking Political History in Southern Africa
(Portsmouth, NH, Heinemann, forthcoming)<person-group>
                     <string-name>
                        <surname>Durham</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Passports and Persons: the Insurrection of Subjugated Knowledges in Southern Africa</comment>
                  <source>Passes, Passports, and the State: Rethinking Political History in Southern Africa</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1268" publication-type="journal">
W. Werner, "'Playing Soldiers": The Truppenspieler Movement
Among the Herero of Namibia, 1915 to ca. 1945', Journal of Southern African Studies, 16, 3 (1990), pp. 476-502.<object-id pub-id-type="jstor">10.2307/2636891</object-id>
                  <fpage>476</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1283" publication-type="journal">
K. Little, 'The Role of Voluntary Associations in West African Urbanisation', American
Anthropologist, 59 (1957), pp. 579-596.<object-id pub-id-type="jstor">10.2307/666096</object-id>
                  <fpage>579</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1299a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d239e1306" publication-type="book">
C. G. Mbock (ed), Cameroun: Pluralisme Culturel et Convivialité (Paris, Edition Nouvelle du Sud, 1996).<person-group>
                     <string-name>
                        <surname>Mbock</surname>
                     </string-name>
                  </person-group>
                  <source>Cameroun: Pluralisme Culturel et Convivialité</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1328a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d239e1335" publication-type="book">
A. R. Radcliffe-Brown, The Andaman Islanders (Glencoe, IL, Free Press, 1922), pp. 246-254<person-group>
                     <string-name>
                        <surname>Radcliffe-Brown</surname>
                     </string-name>
                  </person-group>
                  <fpage>246</fpage>
                  <source>The Andaman Islanders</source>
                  <year>1922</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1359" publication-type="book">
M. Halbwachs,
The Collective Memory (New York, Harper and Row, 1980; originally published as La Memoire Collective, Presses
Universitaires de France, 1950)<person-group>
                     <string-name>
                        <surname>Halbwachs</surname>
                     </string-name>
                  </person-group>
                  <source>The Collective Memory</source>
                  <year>1980</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1387" publication-type="journal">
A. Schutz, 'Making Music Together: a Study in Social Relationship', Social
Research, 18 (1951), pp. 76-97<person-group>
                     <string-name>
                        <surname>Schutz</surname>
                     </string-name>
                  </person-group>
                  <fpage>76</fpage>
                  <volume>18</volume>
                  <source>Social Research</source>
                  <year>1951</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1419" publication-type="book">
M. Weber, The Rational and Social Foundation of Music (Carbondale, IL,
Southern Illinois University Press, 1958)<person-group>
                     <string-name>
                        <surname>Weber</surname>
                     </string-name>
                  </person-group>
                  <source>The Rational and Social Foundation of Music</source>
                  <year>1958</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1443" publication-type="book">
S. Feld, Sound and Sentiment (Philadelphia, University of Pennsylvania
Press, 1982).<person-group>
                     <string-name>
                        <surname>Feld</surname>
                     </string-name>
                  </person-group>
                  <source>Sound and Sentiment</source>
                  <year>1982</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1469a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d239e1476" publication-type="journal">
K. Alnaes,
'Living with the Past: the Songs of Herero in Botswana', Africa, 59 (1989), pp. 267-299.<object-id pub-id-type="doi">10.2307/1160229</object-id>
                  <fpage>267</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1492a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d239e1499" publication-type="journal">
F. Klaits, 'Making a Good Death: AIDS and Social Belonging in an Independent
Church in Gaborone', Botswana Notes and Records, 30 (1998), pp. 101-1 19.<person-group>
                     <string-name>
                        <surname>Klaits</surname>
                     </string-name>
                  </person-group>
                  <fpage>101</fpage>
                  <volume>30</volume>
                  <source>Botswana Notes and Records</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1531a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d239e1538" publication-type="journal">
J. P. Kiernan, 'The
Canticles of Zion: Song as Word and Action in Zulu Zionist Discourse', Journal of Religion in Africa, 20 (1990),
pp. 188-204<object-id pub-id-type="doi">10.2307/1581368</object-id>
                  <fpage>188</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1556" publication-type="book">
C. A. Muller, Rituals of Fertility and the Sacrifice of Desire: Nazarite Women's Performance in
South Africa (Chicago, University of Chicago Press, 1999).<person-group>
                     <string-name>
                        <surname>Muller</surname>
                     </string-name>
                  </person-group>
                  <source>Rituals of Fertility and the Sacrifice of Desire: Nazarite Women's Performance in South Africa</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1581a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d239e1588" publication-type="book">
J. L. Comaroff and S. Roberts, Rules and Processes: the Cultural Logic of Dispute in an African Context
(Chicago, University of Chicago Press, 1981).<person-group>
                     <string-name>
                        <surname>Comaroff</surname>
                     </string-name>
                  </person-group>
                  <source>Rules and Processes: the Cultural Logic of Dispute in an African Context</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1613a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d239e1620" publication-type="book">
N. Elias, The Civilizing Process: the Development of Manners, trans. E. Jephcott (New York, Urizon Books, 1978).<person-group>
                     <string-name>
                        <surname>Elias</surname>
                     </string-name>
                  </person-group>
                  <source>The Civilizing Process: the Development of Manners</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1642a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d239e1649" publication-type="journal">
M. Lambek and J. Solway, 'Just Anger: Scenarios of Indignation in Botswana and Madagascar', Ethnos, 66 (2001),
pp. 49-72.<person-group>
                     <string-name>
                        <surname>Lambek</surname>
                     </string-name>
                  </person-group>
                  <fpage>49</fpage>
                  <volume>66</volume>
                  <source>Ethnos</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1682a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d239e1689" publication-type="journal">
C.
Piot, 'Secrecy, Ambiguity and the Everyday in Kabre Culture',American Anthropologist, 95 (1993), pp. 353-370<object-id pub-id-type="jstor">10.2307/679845</object-id>
                  <fpage>353</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1704" publication-type="journal">
E. Bercovitch, 'The Agent in the Gift: Hidden Exchange in Inner New Guinea', Cultural Anthropology, 9
(1994), pp. 498-536.<object-id pub-id-type="jstor">10.2307/656386</object-id>
                  <fpage>498</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1720a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d239e1727" publication-type="book">
J. Holm, 'Botswana: a Paternalistic Democracy', in L. Diamond, J. Linz and S. Lipset (eds),
Democracy in Developing Countries, vol. 2 (Africa) (Boulder, Lynne Rienner, 1988)<person-group>
                     <string-name>
                        <surname>Holm</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Botswana: a Paternalistic Democracy</comment>
                  <source>Democracy in Developing Countries, vol. 2 (Africa)</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d239e1755" publication-type="journal">
K. Good, 'Authoritarian
Liberalism: a Defining Characteristic of Botswana', Journal of Contemporary African Studies, 14 (1996),
pp. 29-5 1.<person-group>
                     <string-name>
                        <surname>Good</surname>
                     </string-name>
                  </person-group>
                  <fpage>29</fpage>
                  <volume>14</volume>
                  <source>Journal of Contemporary African Studies</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d239e1790a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d239e1797" publication-type="book">
Liah Greenfeld distinguishes between 'civic' and 'ethnic' nationalism in Nationalism: Five Roads to Modernity
(Cambridge, Harvard University Press, 1992).<person-group>
                     <string-name>
                        <surname>Greenfeld</surname>
                     </string-name>
                  </person-group>
                  <source>Nationalism: Five Roads to Modernity</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
