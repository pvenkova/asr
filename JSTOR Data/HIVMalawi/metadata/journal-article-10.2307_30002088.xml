<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">irisstudinteaffa</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000059</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Irish Studies in International Affairs</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Royal Irish Academy</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03321460</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">20090072</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">30002088</article-id>
         <article-categories>
            <subj-group>
               <subject>Annual Reviews</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ireland's Foreign Aid in 2003</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Helen</given-names>
                  <surname>O'Neill</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i30002074</issue-id>
         <fpage>229</fpage>
         <lpage>253</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 Royal Irish Academy</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/30002088"/>
         <abstract>
            <p>This paper reviews Ireland's official development assistance (ODA) programme during 2003. It opens with an overview of events at global and European levels, including international conferences and negotiations, and follows with an overview of official aid flows from DAC donors, indicating an increase in real terms of 3.9% compared with 2002. Irish official aid also grew, but by much less than would be necessary to keep it on track for reaching the UN target of 0.7% of gross national income (GNI) by the end of 2007. The paper provides data on total ODA for selected years since 1994 and a detailed breakdown of bilateral and multilateral aid expenditures for 2001, 2002 and 2003, comparing budgets with outturns under each of the main expenditure headings. Budgets for 2004 are also presented. The DAC produced a peer review of the Irish aid programme during 2003, and its main findings and recommendations are analysed in the paper. The government's proposal to decentralise DCI to Limerick is also examined.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d43670e150a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d43670e157" publication-type="other">
Bertie Ahern, TD, at the 58th General Assembly of the United Nations,
New York, 25 September 2003, available at http://www.taoiseach.gov.ie/index.asp?doclD= 1610
(27 August 2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e170a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d43670e177" publication-type="other">
Tom Kitt, TD, at Dublin City University,
26 November 2003, available at http://foreignaffairs.gov.ie/information/display.asp?ID= 1375 (27
August 2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e190a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d43670e197" publication-type="other">
Rubens Ricupero, Secretary-General of UNCTAD, at the opening session of the Trade
and Development Board, Geneva, 6 October 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e207a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d43670e214" publication-type="other">
Brian Cowen, TD, minister for foreign affairs, at the EU-Africa meeting held in
Farmleigh House, Dublin, on 1 April 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e225a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d43670e232" publication-type="other">
African Recovery, vol. 16, Number 4, February 2003, 10</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e239a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d43670e246" publication-type="other">
Tom Kitt, TD, minister of state for development cooperation, at the Donor Conference
on Reconstruction of Iraq, Madrid, 23-24 October 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e256a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d43670e263" publication-type="other">
Bertie Ahern, TD, at the launch of the UNDP Human Development
Report, in Government Buildings, Dublin, 8 July 2003, available at http://www.taoiseach.gov.ie/
index.asp?doclD=1526 (27 August 2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e276a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d43670e283" publication-type="other">
OECD/DAC, News release, 16 April 2004, available at http://www.oecd.org/document/38/0,2340,
en_2649_33721_31280678_1_1_1_1 _.html (16 August 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e293a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d43670e300" publication-type="other">
Commission Staff Working Document 'Follow-up to the International Conference on Financing
for Development Monterrey (2002): Monitoring the Barcelona Commitments'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43670e309" publication-type="other">
SEC (2003)
569, 15 May 2003, document 9379/03 (Presse 138), 20</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e319a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d43670e326" publication-type="other">
Commission of the European Communities, Communication from the Commission to the Council
and the European Parliament, 'Translating the Monterrey Consensus into practice: the contribution by
the European Union', SEC (2004) 246, Brussels, 5 March 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e340a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d43670e347" publication-type="other">
Commission of the European Communities, 'Translating the Monterrey Consensus into practice'.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e354a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d43670e361" publication-type="other">
Bertie Ahern, TD, 'Globalisation, Partnership and Investment in People:
Ireland's Experience', delivered at the World Bank, Washington, DC, 13 March 2003, reprinted in
Department of Finance, Annual Report: Participation in the International Monetary Fund and the
World Bank, Dublin, March 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e377a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d43670e384" publication-type="other">
OECD/DAC, DAC Peer Review: Ireland, Paris, 2003, 11.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e391a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d43670e398" publication-type="other">
OECD/DAC (2003), Ireland, 60.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e405a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d43670e412" publication-type="other">
Working Group on the Accountability of Secretaries General
and Accounting Officers (the Mullarkey Report), July 2002</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e422a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d43670e429" publication-type="other">
OECD/DAC (2003), Ireland, 55.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e437a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d43670e444" publication-type="other">
Development Cooperation Ireland, Annual Report 2003 (Dublin, 2004), 10</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e451a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d43670e458" publication-type="other">
OECD/DAC (2003), Ireland, 15.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e465a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d43670e472" publication-type="other">
OECD/DAC (2003), Ireland, 52.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e479a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d43670e486" publication-type="other">
OECD/DAC (2003), Ireland, 52.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e493a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d43670e500" publication-type="other">
Government of Ireland, Challenges and opportunities abroad: White Paper on foreign policy
(Dublin, 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e510a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d43670e517" publication-type="other">
OECD/DAC (2003), Ireland, 53.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e525a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d43670e532" publication-type="other">
OECD/DAC (2003), Ireland, 53.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e539a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d43670e546" publication-type="other">
Communicated in private correspondence with this writer on 27 July 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e553a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d43670e560" publication-type="other">
Helen O'Neill, 'Ireland's foreign aid in 2002', Irish Studies in International Affairs 14 (2003),
267-92: 272.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e570a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d43670e577" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2002', 276-7.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e584a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d43670e591" publication-type="other">
DAC in 1972 (OECD/DAC, Twenty-five years of
development cooperation, 171; emphasis in original)</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e601a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d43670e608" publication-type="other">
Bertie Ahem, TD, entitled 'Globalisation, Partnership and Investment
in People: Ireland's Experience', delivered at the World Bank, Washington, DC, 13 March 2003,
reprinted in Department of Finance, Annual Report: Participation in the International Monetary Fund
and the World Bank, Dublin 2003, no page number given</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e625a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d43670e632" publication-type="other">
Department of Finance, Annual Report: Participation in the International Monetary Fund and the
World Bank, 12.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e642a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d43670e649" publication-type="other">
Tom Kitt, TD, in DCI: Annual Report 2002, (Dublin,
November 2003), 3</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e659a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d43670e666" publication-type="other">
DT, 'Address by the taoiseach, Bertie Ahern, TD, to the United Nations Millennium Summit, New
York, 6 September 2000'.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e676a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d43670e683" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2002', 277.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e690a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d43670e697" publication-type="other">
Bertie Ahern, TD, to the general
debate at the 58th General Assembly of the UN, New York, 25 September 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e707a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d43670e714" publication-type="other">
OECD/DAC, Development Cooperation Review Series: Ireland, (Paris, OECD/DAC 1999, No.
35, 19).</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e725a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d43670e732" publication-type="other">
OECD/DAC (2003), Ireland, 12.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e739a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d43670e746" publication-type="other">
OECD/DAC (2003), Ireland, 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e753a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d43670e760" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2002', 273-274.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e767a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d43670e774" publication-type="other">
Statement on behalf of the Irish government to the United Nations during the debate on the Second
Development Decade at the 25th General Assembly, September 1970.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e784a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d43670e791" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2001',
275</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43670e800" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2002, 274</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e807a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d43670e814" publication-type="other">
OECD/DAC (2003), Ireland, 23.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e822a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d43670e829" publication-type="other">
DFA, Report of the Ireland Aid Review Committee (Dublin, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e836a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d43670e843" publication-type="other">
DCI, Annual report 2002, 3 (emphasis added).</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e850a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d43670e857" publication-type="other">
UNDP, Human Development Report 2004, New York, 2004, Table 1, Human Development Index,
139-42.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e867a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d43670e874" publication-type="other">
OECD/DAC (2003), Ireland, 29.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e881a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d43670e888" publication-type="other">
OECD/DAC (2003), Ireland, 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e895a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d43670e902" publication-type="other">
OECD/DAC (2003), Ireland, 31.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e910a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d43670e917" publication-type="other">
OECD/DAC (1999), Ireland, 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e924a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d43670e931" publication-type="other">
DFA, Report of the Ireland Aid Review Committee, 6.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e938a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d43670e945" publication-type="other">
OECD/DAC (2003), Ireland, 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e952a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d43670e959" publication-type="other">
OECD/DAC (2003), Ireland, 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e966a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d43670e973" publication-type="other">
Bertie Ahern, TD, to the UNGASS on HIV/AIDS, New York, 25
June 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e983a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d43670e990" publication-type="other">
Tom Kitt, TD, to the
Comhlámh seminar on 'Development work in the 21st century---changing contexts and relationships',
5 December 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d43670e1004a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d43670e1011" publication-type="other">
DFA (Ireland Aid), Report of the Ireland Aid Review Committee (Dublin 2002), 58, 60.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
