<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40599112</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Treatment of Active Tuberculosis in HIV-Coinfected Patients: A Systematic Review and Meta-Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Faiz A.</given-names>
                  <surname>Khan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jessica</given-names>
                  <surname>Minion</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Madhukar</given-names>
                  <surname>Pai</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sarah</given-names>
                  <surname>Royce</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>William</given-names>
                  <surname>Burman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anthony D.</given-names>
                  <surname>Harries</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dick</given-names>
                  <surname>Menzies</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">50</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">9</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40026176</issue-id>
         <fpage>1288</fpage>
         <lpage>1299</lpage>
         <permissions>
            <copyright-statement>© 2010 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40599112"/>
         <abstract>
            <p>Background.Patients with human immunodeficiency virus (HIV) infection and tuberculosis have an increased risk of death, treatment failure, and relapse. Methods. A systematic review and meta-analysis of randomized, controlled trials and cohort studies was conducted to evaluate the impact of duration and dosing schedule of rifamycin and use of antiretroviral therapy in the treatment of active tuberculosis in HIV-positive patients. In included studies, the initial tuberculosis diagnosis, failure, and/or relapse were microbiologically confirmed, and patients received standardized rifampin-or rifabutincontaining regimens. Pooled cumulative incidence of treatment failure, death during treatment, and relapse were calculated using random-effects models. Multivariable meta-regression was performed using negative binomial regression. Results.After screening 5158 citations, 6 randomized trials and 21 cohort studies were included. Relapse was more common with regimens using 2 months rifamycin (adjusted risk ratio, 3.6; 95% confidence interval, 1.1-11.7) than with regimens using rifamycin for at least 8 months. Compared with daily therapy in the initial phase (n = 3352 patients from 35 study arms), thrice-weekly therapy (n = 211 patients from 5 study arms) was associated with higher rates of failure (adjusted risk ratio, 4.0; 95% confidence interval, 1.5-10.4) and relapse [adjusted risk ratio, 4.8; 95% confidence interval, 1.8-12.8). There were trends toward higher relapse rates if rifamycins were used for only 6 months, compared with ≥8 months, or if antiretroviral therapy was not used. Conclusions. This review raises serious concerns regarding current recommendations for treatment of HIVtuberculosis coinfection. The data suggest that at least 8 months duration of rifamycin therapy, initial daily dosing, and concurrent antiretroviral therapy might be associated with better outcomes, but adequately powered randomized trials are urgently needed to confirm this.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>&lt;bold&gt;References&lt;/bold&gt;</title>
         <ref id="d348e271a1310">
            <label>1</label>
            <mixed-citation id="d348e278" publication-type="other">
WHO. Global Tuberculosis Control: Epidemiology, strategy, financing.
WHO/HTM/TB/2009.411.</mixed-citation>
         </ref>
         <ref id="d348e288a1310">
            <label>2</label>
            <mixed-citation id="d348e295" publication-type="other">
Korenromp EL, Scano F, Williams BG, Dye C, Nunn P. Effects of human
immunodeficiency virus infection on recurrence of tuberculosis after
rifampin-based treatment: an analytical review. Clin Infect Dis 2003;
37(1):101-112.</mixed-citation>
         </ref>
         <ref id="d348e311a1310">
            <label>3</label>
            <mixed-citation id="d348e318" publication-type="other">
Mallory KF, Churchyard GJ, Kleinschmidt I, De Cock KM, Corbett
EL. The impact of HIV infection on recurrence of tuberculosis in South
African gold miners. Int J Tuberc Lung Dis 2000;4(5):455-462.</mixed-citation>
         </ref>
         <ref id="d348e331a1310">
            <label>4</label>
            <mixed-citation id="d348e338" publication-type="other">
Dlodlo RA, Fujiwara PI, Enarson DA. Should tuberculosis treatment
and control be addressed differently in HIV-infected and -uninfected
individuals? Eur Respir J 2005;25(4):751-757.</mixed-citation>
         </ref>
         <ref id="d348e352a1310">
            <label>5</label>
            <mixed-citation id="d348e359" publication-type="other">
El-Sadr WM, Perlman DC, Denning E, Matts JP, Cohn DL. A review
of efficacy studies of 6-month short-course therapy for tuberculosis
among patients infected with human immunodeficiency virus: differ-
ences in study outcomes. Clin Infect Dis 2001;32(4):623-632.</mixed-citation>
         </ref>
         <ref id="d348e375a1310">
            <label>6</label>
            <mixed-citation id="d348e382" publication-type="other">
Pulido F, Pena JM, Rubio R, et al. Relapse of tuberculosis after treat-
ment in human immunodeficiency virus-infected patients. Arch Intern
Med 1997;157(2):227-232.</mixed-citation>
         </ref>
         <ref id="d348e395a1310">
            <label>7</label>
            <mixed-citation id="d348e402" publication-type="other">
El Sadr WM, Perlman DC, Matts JP, et al. Evaluation of an intensive
intermittent-induction regimen and duration of short-course treat-
ment for human immunodeficiency virus-related pulmonary tuber-
culosis. Clin Infect Dis 1998; 26(5):1148-1158.</mixed-citation>
         </ref>
         <ref id="d348e418a1310">
            <label>8</label>
            <mixed-citation id="d348e425" publication-type="other">
Perriens J, St Louis ME, Mukadi YB, et al. Pulmonary tuberculosis in
HIV-infected patients in Zaire. A controlled trial of treatment for either
6 or 12 months. N Enel J Med 1995;332(12):779-784.</mixed-citation>
         </ref>
         <ref id="d348e438a1310">
            <label>9</label>
            <mixed-citation id="d348e445" publication-type="other">
Harries AD, Chimzizi RB, Nyirenda TE, van Gorkom J, Salaniponi
FM. Preventing recurrent tuberculosis in high HIV-prevalent areas in
sub-Saharan Africa: what are the options for tuberculosis control pro-
grammes? Int J Tuberc Lung Dis 2003;7(7):616-622.</mixed-citation>
         </ref>
         <ref id="d348e461a1310">
            <label>10</label>
            <mixed-citation id="d348e468" publication-type="other">
WHO. Treatment of tuberculosis: guidelines for national treatment
programmes. WHO/CDS/TB. 2004;2003.313(3rd edition).</mixed-citation>
         </ref>
         <ref id="d348e479a1310">
            <label>11</label>
            <mixed-citation id="d348e486" publication-type="other">
Centers for Disease Control and Prevention. Treatment of tuberculosis,
American Thoracic Society, CDC, and Infectious Diseases Society of
America. MMWR Recomm Rep 2003; 52 (RR-11).</mixed-citation>
         </ref>
         <ref id="d348e499a1310">
            <label>12</label>
            <mixed-citation id="d348e506" publication-type="other">
Sterne JA, Hernán MA, Ledergerber B, et al. Long-term effectiveness
of potent antiretroviral therapy in preventing AIDS and death: a pro-
spective cohort study. Lancet 2005;366(9483):378-384.</mixed-citation>
         </ref>
         <ref id="d348e519a1310">
            <label>13</label>
            <mixed-citation id="d348e526" publication-type="other">
Badri M, Wilson D, Wood R. Effect of highly active antiretroviral
therapy on incidence of tuberculosis in South Africa: a cohort study.
Lancet 2002; 359(9323) :2059-2064.</mixed-citation>
         </ref>
         <ref id="d348e539a1310">
            <label>14</label>
            <mixed-citation id="d348e546" publication-type="other">
Lawn SD, Badri M, Wood R. Tuberculosis among HIV-infected patients
receiving HAART: long term incidence and risk factors in a South
African cohort. AIDS 2005;19(18):2109-2116.</mixed-citation>
         </ref>
         <ref id="d348e559a1310">
            <label>15</label>
            <mixed-citation id="d348e566" publication-type="other">
Lawn SD, Myer L, Bekker LG, Wood R. Burden of tuberculosis in an
antiretroviral treatment programme in sub-Saharan Africa: impact on
treatment outcomes and implications for tuberculosis control. AIDS
2006;20(12):1605-1612.</mixed-citation>
         </ref>
         <ref id="d348e582a1310">
            <label>16</label>
            <mixed-citation id="d348e589" publication-type="other">
Moore D, Liechty C, Ekwaru P, et al. Prevalence, incidence and mor-
tality associated with tuberculosis in HIV-infected patients initiating
antiretroviral therapy in rural Uganda. AIDS 2007;21(6):713-719.</mixed-citation>
         </ref>
         <ref id="d348e603a1310">
            <label>17</label>
            <mixed-citation id="d348e610" publication-type="other">
Robinson KA, Dickersin K. Development of a highly sensitive search
strategy for the retrieval of reports of controlled trials using PubMed.
Int J Epidemiol 2002;31(1):150-153.</mixed-citation>
         </ref>
         <ref id="d348e623a1310">
            <label>18</label>
            <mixed-citation id="d348e630" publication-type="other">
WHO/IUATLD Global Project on Anti-tuberculosis Drug Resistance
Surveillance. Anti-Tuberculosis Drug Resistance in the World: Report
No. 4. 2008;WHO/HTM/TB/2008.394.</mixed-citation>
         </ref>
         <ref id="d348e643a1310">
            <label>19</label>
            <mixed-citation id="d348e650" publication-type="other">
Hamza T, van Houwelingen H, Stijnen T. The binomial distribution
of meta-analysis was preferred to model within-study variability. J Clin
Epidemiol 2008;61(1):41-51.</mixed-citation>
         </ref>
         <ref id="d348e663a1310">
            <label>20</label>
            <mixed-citation id="d348e670" publication-type="other">
Higgins JP, Thompson SG, Deeks JJ, Altman DG. Measuring incon-
sistency in meta-analyses. BMJ 2003;327(7414):557-560.</mixed-citation>
         </ref>
         <ref id="d348e680a1310">
            <label>21</label>
            <mixed-citation id="d348e687" publication-type="other">
Glasziou PP, Sanders SL. Investigating causes of heterogeneity in sys-
tematic reviews. Stat Med 2002;21(11):1503-1511.</mixed-citation>
         </ref>
         <ref id="d348e697a1310">
            <label>22</label>
            <mixed-citation id="d348e704" publication-type="other">
Thompson S, Sharp S. Explaining heterogeneity in meta-analysis: a
comparison of methods. Stat Med 1999; 18:2693-2708.</mixed-citation>
         </ref>
         <ref id="d348e715a1310">
            <label>23</label>
            <mixed-citation id="d348e722" publication-type="other">
Durban Immunotherapy Group. Immunotherapy with Mycobacterium
vaccae in patients with newly diagnosed pulmonary tuberculosis: a
randomised controlled trial. Lancet 1999; 354(9173):116-119.</mixed-citation>
         </ref>
         <ref id="d348e735a1310">
            <label>24</label>
            <mixed-citation id="d348e742" publication-type="other">
Ackah AN, Coulibaly D, Digbeu H, et al. Response to treatment, mor-
tality, and CD4 lymphocyte counts in HIV-infected persons with tu-
berculosis in Abidjan, Cote d'Ivoire. Lancet 1995;345(8950):607-610.</mixed-citation>
         </ref>
         <ref id="d348e755a1310">
            <label>25</label>
            <mixed-citation id="d348e764" publication-type="other">
Akksilp S, Karnkawinpong O, Wattanaamornkiat W, et al. Antiretro-
viral therapy during tuberculosis treatment and marked reduction in
death rate of HIV-infected patients, Thailand. Emerg Infect Dis
2007;13(7):1001-1007.</mixed-citation>
         </ref>
         <ref id="d348e780a1310">
            <label>26</label>
            <mixed-citation id="d348e787" publication-type="other">
Colmenero JD, Garcia-Ordonez MA, Sebastian MD, et al. Compliance,
efficacy and tolerability of the therapeutic regimen recommended by
National Consensus on Tuberculosis [in Spanish] . Enferm Infecc Mi-
crobiol Clin 1997;15(3):129-133.</mixed-citation>
         </ref>
         <ref id="d348e803a1310">
            <label>27</label>
            <mixed-citation id="d348e810" publication-type="other">
Daniel OJ, Alausa OK. Treatment outcome of TB/HIV positive and
TB/HIV negative patients on directly observed treatment, short course
(DOTS) in Sagamu, Nigeria. Niger J Med 2006;15(3):222-226.</mixed-citation>
         </ref>
         <ref id="d348e823a1310">
            <label>28</label>
            <mixed-citation id="d348e830" publication-type="other">
Domoua K, N'Dhatz M, Coulibaly G, et al. Efficacy of a short 6 month
therapeutic course for HIV infected tuberculosis patients in Abidjan,
Cote dTvoire [in French]. Bull Soc Pathol Exot 1998;91(4):312-314.</mixed-citation>
         </ref>
         <ref id="d348e844a1310">
            <label>29</label>
            <mixed-citation id="d348e851" publication-type="other">
Glynn JR, Warndorff DK, Fine PEM, Munthali MM, Sichone W, Pon-
nighaus JM. Measurement and determinants of tuberculosis outcome
in Karonga District, Malawi. Bull World Health Organ 1998;76(3):
295-305.</mixed-citation>
         </ref>
         <ref id="d348e867a1310">
            <label>30</label>
            <mixed-citation id="d348e874" publication-type="other">
Harries AD, Nyangulu DS, Kang'ombe C, et al. Treatment outcome
of an unselected cohort of tuberculosis patients in relation to human
immunodeficiency virus serostatus in Zomba Hospital, Malawi. Trans
R Soc Trop Med Hyg 1998;92(3):343-347.</mixed-citation>
         </ref>
         <ref id="d348e890a1310">
            <label>31</label>
            <mixed-citation id="d348e897" publication-type="other">
Ige OM, Sogaolu OM, Odaibo GN, Olaleye OD. Evaluation of modified
short course chemotherapy in active pulmonary tuberculosis patients
with human immunodeficiency virus infection in University College Hos-
pital, Ibadan, Nigeria -a preliminary report. Afr J Med Med Sci 2004;
33(3):259-262.</mixed-citation>
         </ref>
         <ref id="d348e916a1310">
            <label>32</label>
            <mixed-citation id="d348e923" publication-type="other">
Jindani A, Nunn AJ, Enarson DA. Two 8-month regimens of che-
motherapy for treatment of newly diagnosed pulmonary tuberculo-
sis: international multicentre randomised trial. Lancet 2004; 364(9441):
1244-1251.</mixed-citation>
         </ref>
         <ref id="d348e939a1310">
            <label>33</label>
            <mixed-citation id="d348e946" publication-type="other">
Johnson JL, Okwera A, Nsubuga P, et al. Efficacy of an unsupervised
8-month rifampicin-containing regimen for the treatment of pulmo-
nary tuberculosis in HIV-infected adults. Uganda-Case Western Re-
serve University Research Collaboration. Int J Tuberc Lung Dis 2000;
4(11):1032-1040.</mixed-citation>
         </ref>
         <ref id="d348e965a1310">
            <label>34</label>
            <mixed-citation id="d348e972" publication-type="other">
Joloba ML, Johnson JL, Namale A, et al. Quantitative sputum bacilla-
ry load during rifampin-containing short course chemotherapy in hu-
man immunodeficiency virus-infected and non-infected adults with
pulmonary tuberculosis. Int J Tuberc Lung Dis 2000;4(6) :528-536.</mixed-citation>
         </ref>
         <ref id="d348e989a1310">
            <label>35</label>
            <mixed-citation id="d348e996" publication-type="other">
Kassim S, Sassan Morokro M, Ackah A, et al. Two-year follow-up of
persons with HIV-1-and HIV-2-associated pulmonary tuberculosis
treated with short-course chemotherapy in West Africa. AIDS 1995;
9(10):1185-1191.</mixed-citation>
         </ref>
         <ref id="d348e1012a1310">
            <label>36</label>
            <mixed-citation id="d348e1019" publication-type="other">
Kelly PM, Cumming RG, Kaldor JM. HIV and tuberculosis in rural
sub-Saharan Africa: a cohort study with two year follow-up. Trans R
Soc Trop Med Hyg 1999;93(3):287-293.</mixed-citation>
         </ref>
         <ref id="d348e1032a1310">
            <label>37</label>
            <mixed-citation id="d348e1039" publication-type="other">
Kennedy N, Berger L, Curram J, et al. Randomized controlled trial of
a drug regimen that includes ciprofloxacin for the treatment of pul-
monary tuberculosis. Clin Infect Dis 1996;22(5):827-833.</mixed-citation>
         </ref>
         <ref id="d348e1052a1310">
            <label>38</label>
            <mixed-citation id="d348e1059" publication-type="other">
Klautau GB, Kuschnaroff TM. Clinical forms and outcome of tuber-
culosis in HIV-infected patients in a tertiary hospital in Sao Paulo-
Brazil. Braz J Infect Dis 2005; 9(6):464-478.</mixed-citation>
         </ref>
         <ref id="d348e1072a1310">
            <label>39</label>
            <mixed-citation id="d348e1079" publication-type="other">
Mwaungulu FB, Floyd S, Crampin AC, et al. Cotrimoxazole prophylaxis
reduces mortality in human immunodeficiency virus-positive tuber-
culosis patients in Karonga District, Malawi. Bull World Health Organ
2004;82(5):354-363.</mixed-citation>
         </ref>
         <ref id="d348e1095a1310">
            <label>40</label>
            <mixed-citation id="d348e1102" publication-type="other">
Noeske J, Nguenko PN. Impact of resistance to anti-tuberculosis drugs
on treatment outcome using World Health Organization standard reg-
imens. Trans R Soc Trop Med Hys 2002;96(4):429-433.</mixed-citation>
         </ref>
         <ref id="d348e1116a1310">
            <label>41</label>
            <mixed-citation id="d348e1123" publication-type="other">
Okwera A, Johnson JL, Luzze H, et al. Comparison of intermittent
ethambutol with rifampicin-based regimens in HIV-infected adults
with PTB, Kampala. Int J Tuberc Lung Dis 2006;10(1):39-44.</mixed-citation>
         </ref>
         <ref id="d348e1136a1310">
            <label>42</label>
            <mixed-citation id="d348e1143" publication-type="other">
Ribera E, Azuaje C, Lopez RM, et al. Once-daily regimen of saquinavir,
ritonavir, didanosine, and lamivudine in HIV-infected patients with
standard tuberculosis therapy (TBQD Study). J Acquir Immune Defic
Syndr 2005;40(3):317-323.</mixed-citation>
         </ref>
         <ref id="d348e1159a1310">
            <label>43</label>
            <mixed-citation id="d348e1168" publication-type="other">
Schwander S, Rusch Gerdes S, Mateega A, et al. A pilot study of anti-
tuberculosis combinations comparing rifabutin with rifampicin in the
treatment of HIV-1 associated tuberculosis. A single-blind randomized
evaluation in Ugandan patients with HIV-1 infection and pulmonary
tuberculosis. Tuber Lung Dis 1995;76(3):210-218.</mixed-citation>
         </ref>
         <ref id="d348e1187a1310">
            <label>44</label>
            <mixed-citation id="d348e1194" publication-type="other">
Sonnenberg P, Murray J, Glynn JR, Shearer S, Kambashi B, Godfrey-
Faussett P. HIV-1 and recurrence, relapse, and reinfection of tuber-
culosis after cure: a cohort study in South African mineworkers. Lancet
2001;358(9294):1687-1693.</mixed-citation>
         </ref>
         <ref id="d348e1210a1310">
            <label>45</label>
            <mixed-citation id="d348e1217" publication-type="other">
Swaminathan S, Deivanayagam CN, Rajasekaran S, et al. Long term
follow up of HIV-infected patients with tuberculosis treated with 6-
month intermittent short course chemotherapy. Nati Med J India
2008;21(1):3-8.</mixed-citation>
         </ref>
         <ref id="d348e1233a1310">
            <label>46</label>
            <mixed-citation id="d348e1240" publication-type="other">
Zachariah R, Spielmann MPL, Chinji C, et al. Voluntary counselling,
HIV testing and adjunctive cotrimoxazole reduces mortality in tuber-
culosis patients in Thyolo, Malawi. AIDS 2003;17(7):1053-1061.</mixed-citation>
         </ref>
         <ref id="d348e1254a1310">
            <label>47</label>
            <mixed-citation id="d348e1261" publication-type="other">
Murray J, Sonnenberg P, Shearer SC, Godfrey-Faussett P. Human im-
munodeficiency virus and the outcome of treatment for new and re-
current pulmonary tuberculosis in African patients. Am J Respir Crit
Care Med 1999;159(3):733-740.</mixed-citation>
         </ref>
         <ref id="d348e1277a1310">
            <label>48</label>
            <mixed-citation id="d348e1284" publication-type="other">
van Oosterhout JJG, Kumwenda JJ, Beadsworth M, et al. Nevirapine-
based antiretroviral therapy started early in the course of tuberculosis
treatment in adult Malawians. Antivir Ther 2007;12(4):515-521.</mixed-citation>
         </ref>
         <ref id="d348e1297a1310">
            <label>49</label>
            <mixed-citation id="d348e1304" publication-type="other">
Schwander SK, Dietrich M, Mugyenyi P, et al. Clinical course of human
immunodeficiency virus type 1 associated pulmonary tuberculosis dur-
ing short-course antituberculosis therapy. East Afr Med J 1997;74(9):
543-548.</mixed-citation>
         </ref>
         <ref id="d348e1320a1310">
            <label>50</label>
            <mixed-citation id="d348e1327" publication-type="other">
Nsubuga P, Johnson JL, Okwera A, Mugerwa RD, Ellner JJ, Whalen
CC. Gender and HIV-associated pulmonary tuberculosis: presentation
and outcome at one year after beginning antituberculosis treatment in
Uganda. BMC Pulm Med 2002;2:4.</mixed-citation>
         </ref>
         <ref id="d348e1343a1310">
            <label>51</label>
            <mixed-citation id="d348e1350" publication-type="other">
Chaisson RE, Clermont HC, Holt EA, et al. Six-month supervised
intermittent tuberculosis therapy in Haitian patients with and without
HIV infection. Am J Respir Crit Care Med 1996;154(4 Pt 1):1034-108.</mixed-citation>
         </ref>
         <ref id="d348e1363a1310">
            <label>52</label>
            <mixed-citation id="d348e1370" publication-type="other">
Eholie SP, Domoua K, Kakou A, et al. Outcome of patients with tu-
berculosis and AIDS in Abidjan (Ivory Coast). Med Mai Infect 1999;
29(11):697-704.</mixed-citation>
         </ref>
         <ref id="d348e1384a1310">
            <label>53</label>
            <mixed-citation id="d348e1391" publication-type="other">
Fitzgerald DW, Desvarieux M, Severe P, Joseph P, Johnson WD Jr, Pape
JW. Effect of post-treatment isoniazid on prevention of recurrent tu-
berculosis in HIV-1 -infected individuals: a randomised trial. Lancet
2000;356(9240):1470-1474.</mixed-citation>
         </ref>
         <ref id="d348e1407a1310">
            <label>54</label>
            <mixed-citation id="d348e1414" publication-type="other">
Mukadi YD, Wiktor SZ, Coulibaly IM, et al. Impact of HIV infection
on the development, clinical presentation, and outcome of tuberculosis
among children in Abidjan, Cote dTvoire. AIDS 1997; 11(9):1151-
1158.</mixed-citation>
         </ref>
         <ref id="d348e1430a1310">
            <label>55</label>
            <mixed-citation id="d348e1437" publication-type="other">
Olle-Goig JE, Alvarez J. Treatment of tuberculosis in a rural area of
Haiti: directly observed and non-observed regimens. The experience
of Hopital Albert Schweitzer. Int J Tuberc Lung Dis 2001;5(2):137-141.</mixed-citation>
         </ref>
         <ref id="d348e1450a1310">
            <label>56</label>
            <mixed-citation id="d348e1457" publication-type="other">
Patel A, Patel K, Patel J, Shah N, Patel B, Rani S. Safety and antiret-
roviral effectiveness of concomitant use of rifampicin and efavirenz for
antiretroviral-naive patients in India who are coinfected with tuber-
culosis and HIV-1. J Acquir Immune Defic Syndr 2004; 37(1):1166-
1169.</mixed-citation>
         </ref>
         <ref id="d348e1476a1310">
            <label>57</label>
            <mixed-citation id="d348e1483" publication-type="other">
Rodger AJ, Toole M, Lalnuntluangi B, Muana V, Deutschmann P.
DOTS-based tuberculosis treatment and control during civil conflict
and an HIV epidemic, Churachandpur District, India. Bull World
Health Organ 2002;80(6):451-456.</mixed-citation>
         </ref>
         <ref id="d348e1499a1310">
            <label>58</label>
            <mixed-citation id="d348e1506" publication-type="other">
Rothman KJ, Greenland S. Modern epidemiology. 2nd edition. Lip-
pincott Williams &amp; Wilkins, 1998.</mixed-citation>
         </ref>
         <ref id="d348e1517a1310">
            <label>59</label>
            <mixed-citation id="d348e1524" publication-type="other">
Driver CR, Munsiff SS, Li J, Kundamal N, Osahan SS. Relapse in
persons treated for drug-susceptible tuberculosis in a population with
high coinfection with human immunodeficiency virus in New York
City. Clin Infect Dis 2001;33(10):1762-1769.</mixed-citation>
         </ref>
         <ref id="d348e1540a1310">
            <label>60</label>
            <mixed-citation id="d348e1547" publication-type="other">
Theuer CP, Hopewell PC, Elias D, Schecter GF, Rutherford GW, Chais-
son RE. Human immunodeficiency virus infection in tuberculosis pa-
tients. J Infect Dis 1990;162(1):8-12.</mixed-citation>
         </ref>
         <ref id="d348e1560a1310">
            <label>61</label>
            <mixed-citation id="d348e1569" publication-type="other">
Vernon A, Burman W, Benator D, Khan A, Bozeman L. Acquired
rifamycin monoresistance in patients with HIV-related tuberculosis
treated with once-weekly rifapentine and isoniazid. Tuberculosis Trials
Consortium. Lancet 1999; 353(9167): 1843-1847.</mixed-citation>
         </ref>
         <ref id="d348e1585a1310">
            <label>62</label>
            <mixed-citation id="d348e1592" publication-type="other">
Hawken M, Nunn P, Gathua S, et al. Increased recurrence of tuber-
culosis in HIV-1 -infected patients in Kenya. Lancet 1993;342(8867):
332-337.</mixed-citation>
         </ref>
         <ref id="d348e1605a1310">
            <label>63</label>
            <mixed-citation id="d348e1612" publication-type="other">
Li J, Munsiff SS, Driver CR, Sackoff J. Relapse and acquired rifampin
resistance in HIV-infected patients with tuberculosis treated with rif-
ampin-or rifabutin-based regimens in New York City, 1997-2000. Clin
Infect Dis 2006;41(1):83-91.</mixed-citation>
         </ref>
         <ref id="d348e1628a1310">
            <label>64</label>
            <mixed-citation id="d348e1635" publication-type="other">
Nahid P, Gonzalez LC, Rudoy I, et al. Treatment outcomes of patients
with HIV and tuberculosis. Am J Respir Crit Care Med 2007; 175(11):
1199-1206.</mixed-citation>
         </ref>
         <ref id="d348e1649a1310">
            <label>65</label>
            <mixed-citation id="d348e1656" publication-type="other">
Walters E, Cotton MF, Rabie H, Schaaf HS, Walters LO, Marais BJ.
Clinical presentation and outcome of tuberculosis in human immu-
nodeficiency virus infected children on anti-retroviral therapy. BMC
Pediatr 2008;8:1.</mixed-citation>
         </ref>
         <ref id="d348e1672a1310">
            <label>66</label>
            <mixed-citation id="d348e1679" publication-type="other">
Golub JE, Saraceni V, Cavalcante SC, et al. The impact of antiretrovi-
ral therapy and isoniazid preventive therapy on tuberculosis incidence
in HIV-infected patients in Rio de Janeiro, Brazil. AIDS 2007; 21(11):
1441-1448.</mixed-citation>
         </ref>
         <ref id="d348e1695a1310">
            <label>67</label>
            <mixed-citation id="d348e1702" publication-type="other">
Golub JE, Durovni B, King BS, et al. Recurrent tuberculosis in HIV-
infected patients in Rio de Janeiro, Brazil. AIDS 2008; 22(18):2527-
2533.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
