<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">demography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100446</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00703370</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15337790</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41237733</article-id>
         <article-categories>
            <subj-group>
               <subject>SURVIVAL: RISK AND PROTECTION</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Is There an Urban Advantage in Child Survival in Sub-Saharan Africa? Evidence From 18 Countries in the 1990s</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Philippe</given-names>
                  <surname>Bocquier</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nyovani Janet</given-names>
                  <surname>Madise</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eliya Msiyaphazi</given-names>
                  <surname>Zulu</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40055871</issue-id>
         <fpage>531</fpage>
         <lpage>558</lpage>
         <permissions>
            <copyright-statement>© 2011 Population Association of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41237733"/>
         <abstract>
            <p>Evidence of higher child mortality of rural-to-urban migrants compared with urban nonmigrants is growing. However, less attention has been paid to comparing the situation of the same families before and after they migrate with the situation of urban-to-rural migrants. We use DHS data from 18 African countries to compare child mortality rates of six groups based on their mothers' migration status: rural nonmigrants; urban nonmigrants; rural-to-urban migrants before and after they migrate; and urban-to-rural migrants before and after they migrate. The results show that rural-to-urban migrants had, on average, lower child mortality before they migrated than rural nonmigrants, and that their mortality levels dropped further after they arrived in urban areas. We found no systematic evidence of higher child mortality for rural-to-urban migrants compared with urban nonmigrants. Urban-torural migrants had higher mortality in the urban areas, and their move to rural areas appeared advantageous because they experienced lower or similar child mortality after living in rural areas. After we control for known demographic and socioeconomic correlates of under-5 mortality, the urban advantage is greatly reduced and sometimes reversed. The results suggest that it may not be necessarily the place of residence that matters for child survival but, rather, access to services and economic opportunities.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1454e231a1310">
            <mixed-citation id="d1454e235" publication-type="other">
African Population and Health Research Center (APHRC). (2002). Health and livelihood needs of
residents of informal settlements in Nairobi City (APHRC Occasional Study Report No. 1). Nairobi,
Kenya: APHRC.</mixed-citation>
         </ref>
         <ref id="d1454e248a1310">
            <mixed-citation id="d1454e252" publication-type="other">
Brockerhoff, M. (1990). Rural-urban migration and child survival in Senegal. Demography, 27, 601-616.</mixed-citation>
         </ref>
         <ref id="d1454e259a1310">
            <mixed-citation id="d1454e263" publication-type="other">
Brockerhoff, M. (1995). Survival in big cities: The disadvantages of migrants. Social Science and
Medicine, 40, 1371-1383.</mixed-citation>
         </ref>
         <ref id="d1454e273a1310">
            <mixed-citation id="d1454e277" publication-type="other">
Brockerhoff, M., &amp; Yang, X. (1994). Impact of migration on fertility in Sub-Saharan Africa. Social
Biology, 4/(1-2), 19-43.</mixed-citation>
         </ref>
         <ref id="d1454e288a1310">
            <mixed-citation id="d1454e292" publication-type="other">
Chattopadhyay, A., White, M. J., &amp; Debpuur, C. (2006). Migrant fertility in Ghana: Selection versus
adaptation and disruption as causal mechanisms. Population Studies, 60, 189-203.</mixed-citation>
         </ref>
         <ref id="d1454e302a1310">
            <mixed-citation id="d1454e306" publication-type="other">
Coast, E. (2006). Local understandings of, and responses to, HIV: Rural-urban migrants in Tanzania.
Social Science and Medicine, 63, 1000-1010.</mixed-citation>
         </ref>
         <ref id="d1454e316a1310">
            <mixed-citation id="d1454e320" publication-type="other">
Dyson, T. (2003). HIV/AIDS and urbanization. Population and Development Review, 29, 427-442.</mixed-citation>
         </ref>
         <ref id="d1454e327a1310">
            <mixed-citation id="d1454e331" publication-type="other">
Fotso, J.-C. (2006). Child health inequities in developing countries: Differences across urban and rural
areas. International journal for Equity in Health, 5, 9. doi: 10. 1186/1475-9276-5-9</mixed-citation>
         </ref>
         <ref id="d1454e341a1310">
            <mixed-citation id="d1454e345" publication-type="other">
Fotso, J.-C. A., Ezeh, C, Madise, N. J., &amp; Ciera, J. (2007). Progress towards the child mortality
millennium development goal in urban Sub-Saharan Africa: The dynamics of population growth,
immunization, and access to clean water. BMC Public Health, 7, 218.</mixed-citation>
         </ref>
         <ref id="d1454e358a1310">
            <mixed-citation id="d1454e362" publication-type="other">
Gould, W. T. S. (1998). African mortality and the new "urban penalty." Health &amp; Place, 4, 171-181.</mixed-citation>
         </ref>
         <ref id="d1454e370a1310">
            <mixed-citation id="d1454e374" publication-type="other">
Harpham, T. (2009). Urban health in developing countries: What do we know and where do we go?
Health &amp; Place, 15, 107-116.</mixed-citation>
         </ref>
         <ref id="d1454e384a1310">
            <mixed-citation id="d1454e388" publication-type="other">
Kiros, G., &amp; White, M. J. (2004). Migration, community context, and child immunization in Ethiopia.
Social Science &amp; Medicine, 59, 2603-2616.</mixed-citation>
         </ref>
         <ref id="d1454e398a1310">
            <mixed-citation id="d1454e402" publication-type="other">
Konseiga, A., Zulu, E., Bocquier, P., Muindi, K., Beguy, D., &amp; Yé, Y. (2009). Assessing the effect of
mother's migration on childhood mortality in the informal settlements of Nairobi. In M. Collinson, K.
Adazu, M. White, &amp; S. Findley (Eds.), The dynamics of migration, health and livelihoods: INDEPTH
network perspectives. Farnham, UK: Ashgate.</mixed-citation>
         </ref>
         <ref id="d1454e418a1310">
            <mixed-citation id="d1454e422" publication-type="other">
Madise, N. J., Banda, E. M., &amp; Benaya, К. W. (2003). Infant mortality in Zambia: Socioeconomic and
demographic determinants. Social Biology, 50, 148-166.</mixed-citation>
         </ref>
         <ref id="d1454e432a1310">
            <mixed-citation id="d1454e436" publication-type="other">
McDaniel, A., &amp; Zulu, E. M. (1996). Mothers, fathers and children: Regional patterns in child-parental
living arrangements in Sub-Saharan Africa. African Population Studies, 11, 1-28.</mixed-citation>
         </ref>
         <ref id="d1454e446a1310">
            <mixed-citation id="d1454e450" publication-type="other">
McKinney, B. J. (1993). Impact of rural-urban migration on migrant fertility in Senegal (DHS Working
Papers No. 6). Columbia: Macro International.</mixed-citation>
         </ref>
         <ref id="d1454e461a1310">
            <mixed-citation id="d1454e465" publication-type="other">
Montgomery, M. R. (2009). Urban poverty and health in developing countries. Population Bulletin, 64, 1-
20.</mixed-citation>
         </ref>
         <ref id="d1454e475a1310">
            <mixed-citation id="d1454e479" publication-type="other">
Montgomery, M., &amp; Hewett, P. С (2005). Urban poverty and health in developing countries: Household
and neighborhood effects. Demography, 42, 397-425.</mixed-citation>
         </ref>
         <ref id="d1454e489a1310">
            <mixed-citation id="d1454e493" publication-type="other">
National Research Council. (2003). Panel on urban population dynamics. In M. R. Montgomery, K. btren,
B. Cohen, &amp; H. E. Reed (Eds.), Cities transformed: Demographic change and its implications in the
developing world. Washington, DC: National Academies Press.</mixed-citation>
         </ref>
         <ref id="d1454e506a1310">
            <mixed-citation id="d1454e510" publication-type="other">
Ndugwa, R. P., &amp; Zulu, E. M. (2008). Child morbidity and care-seeking in Nairobi slum settlements:
The role of environmental and Socio-economic factors. Journal of Child Health Care, 12, 314-
328.</mixed-citation>
         </ref>
         <ref id="d1454e523a1310">
            <mixed-citation id="d1454e527" publication-type="other">
Nicoli, A., Timaueus, I., Kigadye, R. M., Walraven, G., &amp; Killewo, J. (1994). The impact of HIV-1
infection on mortality in children under 5 years of age in Sub-Saharan Africa: A demographic and
epidemiologie analysis. AIDS, 8, 995-1005.</mixed-citation>
         </ref>
         <ref id="d1454e540a1310">
            <mixed-citation id="d1454e544" publication-type="other">
Ssengonzi, R., De Jong, G. F., &amp; Stokes, C. S. (2002). lhe ettect ot temale migration on intant ana cnua
survival in Uganda. Population Research and Policy Review, 21, 403-43 1 .</mixed-citation>
         </ref>
         <ref id="d1454e555a1310">
            <mixed-citation id="d1454e559" publication-type="other">
Stephenson, R., Mathews, Z., &amp; McDonald, J. W. (2003). The impact of rural-urban migration on under-two
mortality in India. Journal of Biosocial Science, 35, 15-31.</mixed-citation>
         </ref>
         <ref id="d1454e569a1310">
            <mixed-citation id="d1454e573" publication-type="other">
United Nations Human Settlements Programme (UN-Habitat). (2003). The challenges of slums: Global
report on human settlements. London: Earthscan Publications.</mixed-citation>
         </ref>
         <ref id="d1454e583a1310">
            <mixed-citation id="d1454e587" publication-type="other">
United Nations Population Division. (2006). World urbanization prospects: The 2005 revision (Working
Paper No. ESA/P/WP/200). New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d1454e597a1310">
            <mixed-citation id="d1454e601" publication-type="other">
Van de Poel, E., O'Donnell, O. A., &amp; Van Doorslaer, E. (2007). Are urban children really healthier?
Evidence from 47 developing countries. Social Science &amp; Medicine. 65. 1986-2003.</mixed-citation>
         </ref>
         <ref id="d1454e611a1310">
            <mixed-citation id="d1454e615" publication-type="other">
Walker, N., Schwartländer, В., &amp; Bryce, J. (2002). Meeting international goals in child survival and HIV/
AIDS. Lancet, 360, 284-289.</mixed-citation>
         </ref>
         <ref id="d1454e625a1310">
            <mixed-citation id="d1454e629" publication-type="other">
Williamson, N., &amp; Galley, C. (1995). Urban-rural differentials in infant mortality in Victorian England.
Population Studies. 49. 401-420.</mixed-citation>
         </ref>
         <ref id="d1454e640a1310">
            <mixed-citation id="d1454e644" publication-type="other">
Winter, J. M. (1979). Infant mortality, maternal mortality and public health in Britain in the 1930s. Journal
of European Economic History, 8, 439-486.</mixed-citation>
         </ref>
         <ref id="d1454e654a1310">
            <mixed-citation id="d1454e658" publication-type="other">
Woods, R., &amp; Hinde, P. R. (1987). Mortality in Victorian England: Models and patterns. Journal of
Interdisciplinary History, 18, 27-54.</mixed-citation>
         </ref>
         <ref id="d1454e668a1310">
            <mixed-citation id="d1454e672" publication-type="other">
World Bank. (2004). African development indicators 2004. Washington, DC: World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
