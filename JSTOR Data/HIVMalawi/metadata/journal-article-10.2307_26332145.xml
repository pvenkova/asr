<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26332145</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Orphan status, school attendance, and their relationship to household head in Nigeria</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Kazeem</surname>
                  <given-names>Aramide</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Jensen</surname>
                  <given-names>Leif</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Western Kentucky University, USA.</aff>
            <aff id="af2">
               <label>²</label>Pennsylvania State University, USA.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2017</year>
            <string-date>JANUARY - JUNE 2017</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2017</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">36</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26332123</issue-id>
         <fpage>659</fpage>
         <lpage>690</lpage>
         <permissions>
            <copyright-statement>© 2017 Aramide Kazeem &amp; Leif Jensen</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26332145"/>
         <abstract xml:lang="eng">
            <sec>
               <label>BACKGROUND</label>
               <p>This study addresses the important issue of whether extended family networks can meet the educational needs of orphans in Nigeria. The theory behind this paper is based on Hamilton’s rule, which holds that individuals are less altruistic toward those with whom they have distant kinship ties.</p>
            </sec>
            <sec>
               <label>OBJECTIVE</label>
               <p>Our objective is to determine whether orphans experience an educational advantage if they reside in households headed by blood relatives rather than non-relatives, paying attention to age and household income differences.</p>
            </sec>
            <sec>
               <label>METHODS</label>
               <p>We use logistic regression to estimate models of children’s school attendance based on data from the 2010 Nigeria Education Data Survey (NEDS). The analyses examine the associations of paternal (father died) and maternal/double orphans (mother or both parents died) and child’s relation to the household head with school attendance. It also investigates how the pattern of relationships differs by age of children and household income.</p>
            </sec>
            <sec>
               <label>RESULTS</label>
               <p>The results indicate that paternal and maternal/double orphans who are distantly related to their household heads have lower chances of attending school than those who have close biological ties, specifically when they reside in poor households. This finding is consistent with Hamilton’s rule.</p>
            </sec>
            <sec>
               <label>CONCLUSIONS</label>
               <p>Our analysis suggests that orphanhood is problematic for those more distantly related to their guardians and in poor households. Since the disadvantages of orphanhood carry on into later life, ameliorative policies and programs need to be attentive to the double disadvantages faced by children in such circumstances.</p>
            </sec>
            <sec>
               <label>CONTRIBUTION</label>
               <p>This study contributes to the literature by showing that while close kinship ties to household head produce educational advantage for orphans, the willingness of household heads to display altruism regardless of degree of kinship is highly dependent on their economic resources.</p>
            </sec>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Abebe, T. and Aase, A. (2007). Children, AIDS and the politics of orphan care in Ethiopia: The extended family revisited. Social Science and Medicine 64(10): 2058–2069.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Ainsworth, M. and Filmer, D. (2002). Poverty, AIDS and children’s schooling: A targeting dilemma. Washington, D.C: World Bank (Policy research working paper 2885).</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Ainsworth, M., Beegle, K., and Koda, G. (2005). The impact of adult mortality and parental deaths on primary schooling in north-Western Tanzania. Journal of Development Studies 41(3): 412–439. doi:10.1080/0022038042000313318.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Alber, E. (2003). Denying biological parenthood: Fosterage in northern Benin. Ethnos 68(4): 487–506. doi:10.1080/0014184032000160532.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Alvard, M.S. (2003). Kinship, lineage, and evolutionary perspective on cooperative hunting groups in Indonesia. Human Nature 14(2): 129–163. doi:10.1007/s12110-003-1001-5.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Anarfi, J.K. (1992). Report on a pilot study on the coping strategies of households with Aids sufferers. Legon: University of Ghana.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Anderson, K.G. (2005). Relatedness and investment in children in South Africa. Human Nature 16(1): 1–31. doi:10.1007/s12110-005-1005-4.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Asiamah, S, Kraybill, D., and Thompson, S. (2005). Does orphan status affect primary school attendance? An analysis of household survey data from Uganda. Paper presented at the American Agricultural Economics Association Annual Meeting, July 24‒27, Providence, RI</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Bennell, P. (2005). The impact of the AIDS epidemic on the schooling of orphans and other directly affected children in sub-Saharan Africa. Journal of Development Studies 41(3): 467–488. doi:10.1080/0022038042000313336.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Bicego, G., Rutstein, S., and Johnson, K. (2003). Dimensions of the emerging orphan crisis in sub-Saharan Africa. Social Science and Medicine 56(6): 1235–1247.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Borgerhoff Mulder, M. (2007). Hamilton’s rule and kin competition: The Kipsigis case. Evolution and Human Behavior 28(5): 299–312. doi:10.1016/j.evolhumbehav. 2007.05.009.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Brown, J. (2009). Child fosterage and the developmental markers of ovambo children in Namibia: A look at gender and kinship. Childhood in Africa 1(1): 4–10.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Buchmann, C. (2000). Family structure, parental perceptions, and child labor in Kenya: What factors determine who is enrolled in school? Social Forces 78(4): 1349-1378. doi:10.2307/3006177.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Caldwell, J.C. (1982). Theory of fertility decline. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Case, A., Paxson, C., and Ableidinger, J. (2004). Orphans in Africa: Parental death, poverty, and school enrollment. Demography 41(3): 483–508. doi:10.1353/dem. 2004.0019.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Chirwa, W.C. (2002). Social exclusion and inclusion: Challenges to orphans care in Malawi. Nordic Journal of African Studies 11(2): 93–103.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Dahl, B. (2009). The ‘failures of culture’: Christianity, kinship, and moral discourses about orphans during Botswana’s AIDS crisis. Africa Today 56(1): 23–43. doi:10.2979/AFT.2009.56.1.22.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Daly, M. and Wilson, M. (1987). The Darwinian psychology of discriminative parental solicitude. In: Leger, D. (ed.). Nebraska Symposium on Motivation. Nebraska: University of Nebraska Press: 91–144.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Eke, B. (2004). Intergenerational impact of the AIDS pandemic in Nigeria. Journal of Intergenerational Relationships 2(3–4): 39–52. doi:10.1300/J194v02n0304.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Ekong, S.C. (1986). Industrialization and kinship: A comparative study of some Nigerian ethnic group. Kinship and Development 17(2): 197‒206.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Eloundou-Enyegue, P.M and Stokes, C.S. (2002). Will economic crises in Africa weaken rural-urban ties? Insights from child fosterage trends in Cameroon. Rural Sociology 67(2): 278–298. doi:10.1111/j.1549-0831.2002.tb00104.x.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Evans, K.E. and Miguel, E. (2007). Orphans and schooling in Africa: A longitudinal analysis. Demography 44(1): 35–57. doi:10.1353/dem.2007.0002.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Foster, G. and Williamson, J. (2000). A review of current literature of the impact of HIV/AIDS on children in sub-Saharan Africa. AIDS 14(suppl.3): S275–S284.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Fuller, B., Singer, J.D., and Keiley, M. (1995). Why do daughters leave school in Southern Africa? Family economy and mothers commitments. Social Forces 74 (2): 657–681. doi:10.2307/2580496.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Funkquist, A., Eriksson, B., and Muula, A.S. (2007). The vulnerability of orphans in Thyolo District, Southern Malawi. Tanzania Health Research Bulletin 9(2): 102–109. doi:10.4314/thrb.v9i2.14311.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Gage, A. (2005). The interrelationship between fosterage, schooling and children’s labor force participation in Ghana. Population Research and Policy Review 24(5): 431–466. doi:10.1007/s11113-005-4290-z.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Gurven, M. (2004). To give or not to give: An evolutionary ecology of human food transfers. Behavioral and Brain Sciences 27(4): 543–583.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Guo, Y., Xiaoming, L., and Sherr, L. (2012). The impact of HIV/AIDS on children’s educational outcome: A critical review of global literature. AIDS Care: Psychological and Socio-Medical Aspects of AIDS/HIV 24(8): 993–1012. doi:10.1080/09540121.2012.668170.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Hamilton, W.D. (1963). The evolution of altruistic behavior. American Naturalist 97(896): 354–356. doi:10.1086/497114.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Huber, U. and Gould, B. (2002). Primary school attendance in Tanzania: How far is it affected by orphanhood? Paper presented at the Annual Conference of the British Society for Population Studies, University of Newcastle, 9‒11 September.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Hunter, S.S. (1990). Orphans as a window on the AIDS epidemic in sub-Saharan Africa: Initial results and implications of a study in Uganda. Social Science &amp; Medicine 31(6): 681–690. doi:10.1016/0277-9536(90)90250-V.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Isiugo-Abanihe, U.C. (1984). Prevalence and determinants of child fosterage in West Africa: Relevance to demography. Pennsylvania: Population Studies Center (African Demography Working Paper Series. WP-1984-012).</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Karaca-Mandi, P., Norton, E.C., and Dowd, B. (2012). Interaction terms in nonlinear models. Health Services Research 47(1): 255–273. doi:10.1111/j.1475-6773. 2011.01314.x.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Kidman, R., Hanley, J.A., Foster G., Subramanian, S.V., and Heyman, J. (2012). Educational disparities in AIDS-affected communities: Does orphanhood confer unique vulnerability? Journal of Development Studies 48(4): 531–548. doi:10.1080/00220388.2011.604412.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Kobiane, J.F., Calves, A., and Marcoux, R. (2005). Parental death and children’s schooling in Burkina Faso. Comparative Education Review 49(4): 468–489.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Lloyd, C.B. and Blanc, A.K. (1996). Children’s schooling in sub-Saharan Africa: The role of fathers, mothers, and others. Population and Development Review 22(2): 265–298. doi:10.2307/2137435.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Madhaven, S. (2004). Fosterage patterns in the age of AIDS: Continuity and change. Social Science &amp; Medicine 58(7): 1443–1454.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Monasch, R. and Boerma, J.T. (2004). Orphanhood and childcare patterns in sub-Saharan Africa: An analysis of national surveys from 40 Countries. AIDS 18(2): S55–S64. doi:10.1097/00002030-200406002-00007.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Menard, S.W. (1995). Applied Logistic Regression Analysis. Thousand Oaks: Sage.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Norton, E.C., Wang, H., and Ai, C. (2004). Computing interaction effects and standard errors in logit and probit models. Stata Journal 4(2): 154–167.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Nyambedha, E.O., Wandibba, S., and Aagaard-Hansen, J. (2003). Changing patterns of orphan care due to the HIV epidemic in western Kenya. Social Science &amp; Medicine 57(2): 301–311.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Nyamukapa, C. and Gregson, S. (2005). Extended family’s and women’s roles in safeguarding orphans’ education in AIDS-afflicted rural Zimbabwe. Social Science &amp; Medicine 60(10): 2155–2167.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Oke, E.A. (1986). Kinship interaction in Nigeria in relation to societal modernization: A pragmatic approach. Journal of Comparative Family Studies 17(2): 185–196.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Ombuya, B.D., Yambo, J.M., and Mboya, O.T. (2012). Effects of orphanhood on girlchild’s access and retention in secondary school education: A case of Rongo District, Kenya. International Journal of Academic Research in Progressive Education and Development 1(4): 114–133.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Oni, J.B. (1995). Fostered children’s perception of their health care and illness treatment in Ekiti Yoruba households, Nigeria. Health Transition Review 5(1): 21‒34.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Parker, E.M. and Short, S.E. (2009). Grandmother coresidence, maternal orphans, and school enrollment in sub-Saharan Africa. Journal of Family Issues 30(6): 813-836.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Parikh, A., DeSilva, M.B., Cakwe, M., Quinlan, T., Simon, J.L., Skalicky, A., and Zhuwau, T. (2007). Exploring the Cinderella myth: Intrahousehold differences in child wellbeing between orphans and non-orphans in Amajuba District, South Africa. AIDS 21(7): S95–S103. doi:10.1097/01.aids.0000300540.12849.86.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Runhare, T. and Gordon, R (2004). Comprehensive review of gender issues in education sector. Zimbabwe: UNICEF/MoSESC (Report presented for UNICEF/MoSESC 49(1): 38–40).</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Scelza, B.A. and Silk, J.B. (2014). Fosterage as a system of dispersed cooperative breeding: Evidence from the Himba. Human Nature 25(4): 448–464. doi:10.1007/s12110-014-9211-6.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Tanga, P.T. (2013). The impact of the declining extended family support system on the education of orphans in Lesotho. African Journal of AIDS Research 12(3): 173-183. doi:10.2989/16085906.2013.863217.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Thomas, K.J.A. (2010). Family contexts and schooling disruption among orphans in post-genocide Rwanda. Population Research and Policy Review 29(6): 819–842. doi:10.1007/s11113-009-9167-0.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Treiman, D.J. (2009). Quantitative data analysis: Doing social research to test ideas. San Francisco: Jossey-Bass.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">UNAIDS (2014). Federal Republic of Nigeria. Global AIDS response. Country progress report. Nigeria: UNAIDS.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">UNICEF (2009). Promoting quality education for orphans and vulnerable children: A sourcebook of programmer experiences in Eastern and Southern Africa. New York: UNICEF.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">UNICEF (2006). Africa’s orphaned and vulnerable generations: Children affected by AIDS. New York: UNICEF.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">West, A.S., Pen, I., and Griffin, S.A. (2002). Cooperation and competition between relatives. Science 296(5565): 72–75. doi:10.1126/science.1065507.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">West, A.S., Murray, G.M., Machado, A.C., Griffin, A.A., and Edward, A.H. (2001). Testing Hamilton’s Rule with competition between relatives. Nature 409(6819): 510–512. doi:10.1038/35054057.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Yamano, T., Shimamura, Y., and Sserunuuma, D. (2006). Living arrangement and schooling of orphaned children and adolescents in Uganda. Economic Development and Cultural Change 54(4): 833–856. doi10.1086/503586.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Yamano, T. and Jayne, T.S. (2005). Working-age adult mortality and primary school attendance in rural Kenya. Economic Development and Cultural Change 53(3): 619–654.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
