<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">popufrenedit</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100329</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population (French Edition)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Institut national d'études démographiques</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00324663</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19577966</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43855788</article-id>
         <title-group>
            <article-title>Taille des fratries et taille des familles dans les données d'enquêtes utilisées pour estimer la mortalité</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bruno</given-names>
                  <surname>Masquelier</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">69</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40158319</issue-id>
         <fpage>249</fpage>
         <lpage>268</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43855788"/>
         <abstract>
            <p>Les données d'enquêtes recueillies sur la survie des frères et sœurs constituent une source incontournable pour estimer la mortalité des adultes dans les pays où l'état civil reste incomplet. Cet article évalue la qualité de ces données en comparant la taille des fratries déclarées dans les enquêtes démographiques et de santé avec le nombre moyen d'enfants nés vivants des femmes de la génération précédente. Cette comparaison, menée au niveau agrégé, suggère qu'une proportion élevée de frères et soeurs sont omis; les tailles de fratries sont inférieures de 15 % environ aux tailles attendues sur la base des enfants nés vivants. Ces omissions sont plus fréquentes en Afrique subsaharienne que dans les autres régions en développement et leur ampleur augmente légèrement avec l'âge des enquêtées. La mortalité aux âges adultes déduite de ces données n'est pas pour autant sous-estimée, car les omissions semblent surtout concerner des frères et soeurs décédés dans l'enfance. Survey data on sibling survival provide a crucial source of information for estimating adult mortality in countries where vital records are incomplete. This article assesses the quality of these data by comparing sibship sizes reported in Demographic and Health Surveys with women's mean number of liveborn children in the previous generation. This comparison, conducted at aggregate level, suggests that a high proportion of siblings are omitted, since the sibship sizes are 15% lower, on average, than would be expected on the basis of number of liveborn children. Such omissions are more frequent in sub-Saharan Africa than in other developing regions, and their extent increases slightly with the respondents' age. Adult mortality deduced from these data is not underestimated, however, since omissions appear to mainly concern siblings who died in childhood. Los datos de encuesta recogidos sobre la supervivencia de los hermanos y hermanas constituyen una fuente necesaria para estimar la mortalidad de los adultos en los países con un estado civil incompleto. Este artículo evalúa la calidad de esos datos comparando el tamaño de las fratrías declaradas en las encuestas demográficas y de salud con el número medio de hijos nacidos vivos de mujeres de la generación precedente. Esta comparación, hecha a nivel agregado, sugiere que una proporción importante de hermanos y hermanas es omitida; los tamaños de las fratrías son un 15% inferiores à los que cabría esperar sobre la base de los niños nacidos vivos. Estas omisiones son más frecuentes en África subsahariana que en las otras regiones en desarrollo y su amplitud aumenta ligeramente con la edad de las mujeres encuestadas. Sin embargo, la mortalidad a la edad adulta deducida de estos datos no está subestimada pues las omisiones parecen afectar sobre todo a los hermanos y hermanas muertos durante la infancia.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>fre</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d736e172a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d736e179" publication-type="other">
Minneapolis, University of Minnesota, 2013.</mixed-citation>
            </p>
         </fn>
         <fn id="d736e186a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d736e193" publication-type="other">
El-Badry
(1961)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>Références</title>
         <ref id="d736e212a1310">
            <mixed-citation id="d736e216" publication-type="other">
Bendavid Eran, Holmes Charles, Bhattacharya Jay, Miller Grant, 2012, « HIV
development assistance and adult mortality in Africa », JAMA, 307(19),
p. 2060-2067.</mixed-citation>
         </ref>
         <ref id="d736e229a1310">
            <mixed-citation id="d736e233" publication-type="other">
Bongaarts John, 1999, « The fertility impact of changes in the timing of childbearing
in the developing world », Population Studies, 53(3), p. 277-289.</mixed-citation>
         </ref>
         <ref id="d736e243a1310">
            <mixed-citation id="d736e247" publication-type="other">
Brass William, 1996, « Demographic data analysis in less developed countries: 1946-
1996 », Population Studies, 50(3), p. 451-467.</mixed-citation>
         </ref>
         <ref id="d736e257a1310">
            <mixed-citation id="d736e261" publication-type="other">
Dyson Tim, Murphy Mike, 1985, « The onset of fertility transition », Population and
Development Review, 11(3), 399-440.</mixed-citation>
         </ref>
         <ref id="d736e272a1310">
            <mixed-citation id="d736e276" publication-type="other">
El-badry Mohamed A., 1961, « Failure of enumerators to make entries of zero: Errors
in recording childless cases in population censuses », Journal of the American Statistical
Association, 56(296), p. 909-924.</mixed-citation>
         </ref>
         <ref id="d736e289a1310">
            <mixed-citation id="d736e293" publication-type="other">
Faraway Julian J., 2006, Extending the Linear Model with R, Taylor &amp; Francis, 301 p.</mixed-citation>
         </ref>
         <ref id="d736e300a1310">
            <mixed-citation id="d736e304" publication-type="other">
Feeney Griffith, 1991, « Child survivorship estimation: Methods and data analysis »,
Asian and Pacific Population Forum, 5(2-5), p. 51-55, p. 76-87.</mixed-citation>
         </ref>
         <ref id="d736e314a1310">
            <mixed-citation id="d736e318" publication-type="other">
Gakidou Emmanuela, KING Gary, 2006, « Death by survey: Estimating adult mor-
tality without selection bias from sibling survival data », Demography, 43(3),
p. 569-585.</mixed-citation>
         </ref>
         <ref id="d736e331a1310">
            <mixed-citation id="d736e335" publication-type="other">
Gakidou Emmanuela, Hogan Margaret, LOPEZ Alan D., 2004, « Adult mortality:
Time for a reappraisal », International Journal of Epidemiology, 33(4), p. 710-717.</mixed-citation>
         </ref>
         <ref id="d736e345a1310">
            <mixed-citation id="d736e349" publication-type="other">
Garenne Michel, 2002, « Sex ratios at birth in African populations: A review of
survey data », Human Biology, 74(6), p. 889-900.</mixed-citation>
         </ref>
         <ref id="d736e360a1310">
            <mixed-citation id="d736e364" publication-type="other">
Gelman Andrew, HILL Jennifer 2007, Data Analysis Using Regression and Multilevel/
Hierarchical Models, Cambridge University Press, 625 p.</mixed-citation>
         </ref>
         <ref id="d736e374a1310">
            <mixed-citation id="d736e378" publication-type="other">
Hagopian Amy, Flaxman Abraham D., TAKARO Tim K., ESA Al Shatari
Sahar, RAJARATNAM Julie et al. , 2013, « Mortality in Iraq associated with the 2003-
2011 war and occupation: Findings from a national cluster sample survey by the
University Collaborative Iraq Mortality Study », PLoS Medicine, 10(10), el001533.</mixed-citation>
         </ref>
         <ref id="d736e394a1310">
            <mixed-citation id="d736e398" publication-type="other">
Helleringer Stéphane, Pison Gilles, Kante Almamy M., Duthé Géraldine,
Andro Armelle, 2014, « Reporting errors in siblings' survival histories and their
impact on adult mortality estimates: Results from a record linkage study in Senegal »,
Demography, 51(2), p. 387-411.</mixed-citation>
         </ref>
         <ref id="d736e414a1310">
            <mixed-citation id="d736e418" publication-type="other">
Helleringer Stéphane, Düthe Géraldine, Kanté Almamy M., Andro Armelle,
SOKHNA Cheikh et al., 2013, « Misclassification of pregnancy-related deaths in adult
mortality surveys: Case study in Senegal », Tropical Medicine &amp; International Health,
18(1), p. 27-34.</mixed-citation>
         </ref>
         <ref id="d736e434a1310">
            <mixed-citation id="d736e438" publication-type="other">
Hill Kenneth, Trussell James, 1977, « Further developments in indirect mortality
estimation », Population Studies, 31(2), p. 313-334.</mixed-citation>
         </ref>
         <ref id="d736e448a1310">
            <mixed-citation id="d736e452" publication-type="other">
Masquelier Bruno, 2013, « Adult mortality from sibling survival data: A reappraisal
of selection biases », Demography, 50(1), p. 207-228.</mixed-citation>
         </ref>
         <ref id="d736e463a1310">
            <mixed-citation id="d736e467" publication-type="other">
Masquelier Bruno, Reniers Georges, Pison Gilles, 2014, « Divergences in trends
in child and adult mortality in sub-Saharan Africa: Survey evidence on the survival
of children and siblings », Population Studies, 68(2), p. 161-177.</mixed-citation>
         </ref>
         <ref id="d736e480a1310">
            <mixed-citation id="d736e484" publication-type="other">
Merdad Leena, Hill Kenneth, Graham Wendy, 2013, « Improving the measure-
ment of maternal mortality: The sisterhood method revisited », PLoS ONE, 8, e59834.</mixed-citation>
         </ref>
         <ref id="d736e494a1310">
            <mixed-citation id="d736e498" publication-type="other">
Nations Unies, 1983, Manuel X : Techniques indirectes d'estimation démographique,
Population Division, Department of International Economic and Social Affairs, United
Nations, New York.</mixed-citation>
         </ref>
         <ref id="d736e511a1310">
            <mixed-citation id="d736e515" publication-type="other">
Nations Unies, 1997, Demographic Yearbook 1997 - Historical supplement, Department
of International Economic and Social Affairs, United Nations, New York.</mixed-citation>
         </ref>
         <ref id="d736e525a1310">
            <mixed-citation id="d736e529" publication-type="other">
Nations Unies, 2009, Demographic Yearbook 2007, Department of International
Economic and Social Affairs, United Nations, New York.</mixed-citation>
         </ref>
         <ref id="d736e539a1310">
            <mixed-citation id="d736e543" publication-type="other">
Obermeyer Ziad, Rajaratn am Julie, Park Chang H., Gakidou Emmanuela,
Mocan Margaret C. et al., 2010, « Measuring adult mortality using sibling survival:
A new analytical method and new results for 44 countries, 1974-2006 », PLoS Medicine,
7(4), el000260.</mixed-citation>
         </ref>
         <ref id="d736e560a1310">
            <mixed-citation id="d736e564" publication-type="other">
Pinheiro José, Bates Douglas, DebRoy Saikat, Sarkar Deepayan, R Core Team,
2013, nlme: Linear and Nonlinear Mixed Effects Models, R package version 3.1-109.</mixed-citation>
         </ref>
         <ref id="d736e574a1310">
            <mixed-citation id="d736e578" publication-type="other">
Pison Gilles, 1986, « La démographie de la polygamie », Population, 41(1), p. 93-122.</mixed-citation>
         </ref>
         <ref id="d736e585a1310">
            <mixed-citation id="d736e589" publication-type="other">
Preston Samuel H., 1976, « Family sizes of children and family sizes of women »,
Demography, 13(1), p. 105-114.</mixed-citation>
         </ref>
         <ref id="d736e599a1310">
            <mixed-citation id="d736e603" publication-type="other">
Reniers Georges, Masquelier Bruno, Gerland Patrick, 2011, « Adult Mortality
in Africa », in Rogers Richard G., Crimmins Eileen M. (eds.), International Handbook
of Adult Mortality, Springer, p. 151-170.</mixed-citation>
         </ref>
         <ref id="d736e616a1310">
            <mixed-citation id="d736e620" publication-type="other">
Rutenberg Naomi, Sullivan Jeremiah M., 1991, « Direct and indirect estimates
of maternal mortality from the sisterhood method », Demographic and Health Surveys
World Conference Proceedings, August 5-7, Washington, DC. Volume III. Calverton,
Maryland USA, IRD/Macro International Inc. p. 1669-1696.</mixed-citation>
         </ref>
         <ref id="d736e636a1310">
            <mixed-citation id="d736e640" publication-type="other">
Stanton Cynthia, Noureddine Abderrahim, Hill Kenneth, 2000, « An assess-
ment of DHS maternal mortality indicators », Studies in Family Planning, 31(2),
p. 111-123.</mixed-citation>
         </ref>
         <ref id="d736e654a1310">
            <mixed-citation id="d736e658" publication-type="other">
Tlmaeus Ian M., Jasseh Momodou, 2004, « Adult mortality in Sub-Saharan Africa:
Evidence from demographic and health survey », Demography, 41(4), p. 757-772.</mixed-citation>
         </ref>
         <ref id="d736e668a1310">
            <mixed-citation id="d736e672" publication-type="other">
Tlmaeus Ian M., Zaba Basia, Ali Mohamed, 2001, « Estimation of adult mortality
from data on adult siblings », in Zaba Basia, Blacker John (eds.), Brass Tacks: Essays
in Medical Demography, Athlone Press, London, p. 43-66.</mixed-citation>
         </ref>
         <ref id="d736e685a1310">
            <mixed-citation id="d736e689" publication-type="other">
Wang Haidong, Dwyer-Lindgren Laura, Lofgren Katherine T., Rajaratnam
Julie, MARCUS Jacob et ah, 2012, « Age-specific and sex-specific mortality in
187 countries, 1970-2010: A systematic analysis for the Global Burden of Disease
Study 2010 », Lancet, 380(9859), p. 2071-2094.</mixed-citation>
         </ref>
         <ref id="d736e705a1310">
            <mixed-citation id="d736e709" publication-type="other">
Wllmoth John R., Mlzoguchi Nobuko, Oestergaard Mikkel, Say Laie, Mathers
Colin et ah, 2012, « A new method for deriving global estimates of maternal morta-
lity », Statistics, Politics and Policy, 3(2), p. 1-38.</mixed-citation>
         </ref>
         <ref id="d736e722a1310">
            <mixed-citation id="d736e726" publication-type="other">
Zaba Basia, David Patricia H., 1996, « Fertility and the distribution of child mortality
risk among women: An illustrative analysis », Population Studies, 50(2), p. 263-278.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
