<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">studfamiplan</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100383</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Studies in Family Planning</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00393665</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284465</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3181161</article-id>
         <article-categories>
            <subj-group>
               <subject>In Focus: HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Assessing the Potential of Condom Use to Prevent the Spread of HIV: A Microsimulation Study</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael</given-names>
                  <surname>Bracher</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gigi</given-names>
                  <surname>Santow</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Susan Cotts</given-names>
                  <surname>Watkins</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i359328</issue-id>
         <fpage>48</fpage>
         <lpage>64</lpage>
         <page-range>48-64</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3181161"/>
         <abstract>
            <p>In this study, a microsimulation model is used to assess the potential impact of condom use on women's lifetime risk of acquiring HIV in rural southern Malawi. The model draws on survey data for information on sexual activity, marriage and divorce, and on the biomedical literature for input parameters governing the transmission and spread of HIV and other sexually transmitted diseases (STDs). We show that lifetime risk could be as high as 42 percent with no condom use and as low as 8 percent if everyone consistently uses condoms with nonmarital partners. Next, we examine the impact of more realistic, intermediate strategies of condom use, varying men's propensity to use a condom with nonmarital partners, varying the per-coitus probability of condom use, varying probabilities of slippage or breakage, and finally, examining the effect of condom use in the presence of STD symptoms. We demonstrate profound effects of consistent condom use and of condom use prompted by symptomatic STDs.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d715e291a1310">
            <label>5</label>
            <mixed-citation id="d715e298" publication-type="other">
Coale and Demeny
1983</mixed-citation>
         </ref>
         <ref id="d715e308a1310">
            <label>8</label>
            <mixed-citation id="d715e315" publication-type="other">
Zaba and Blacker 1997</mixed-citation>
         </ref>
         <ref id="d715e322a1310">
            <label>10</label>
            <mixed-citation id="d715e329" publication-type="other">
Bracher et al. (2003a)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d715e345a1310">
            <mixed-citation id="d715e349" publication-type="book">
Auvert, Bertran. 1991. "The Auvert approach: A stochastic model for
the heterosexual spread of the human immunodeficiency virus."
In The AIDS Epidemic and Its Demographic Consequences. New York:
United Nations document ST/ESA/SER.A/119. Pp. 77-83<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Auvert</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Auvert approach: A stochastic model for the heterosexual spread of the human immunodeficiency virus</comment>
               <fpage>77</fpage>
               <source>The AIDS Epidemic and Its Demographic Consequences</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d715e387a1310">
            <mixed-citation id="d715e391" publication-type="book">
Bongaarts, John and Robert G. Potter. 1983. Fertility, Biology, and Be-
havior: An Analysis of the Proximate Determinants. New York: Aca-
demic Press<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bongaarts</surname>
                  </string-name>
               </person-group>
               <source>Fertility, Biology, and Behavior: An Analysis of the Proximate Determinants</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d715e420a1310">
            <mixed-citation id="d715e424" publication-type="book">
Bracher, Michael, Gigi Santow, and Susan Cotts Watkins. 2003a.
"'Moving' and marrying: Modelling HIV infection among newly-
weds in Malawi." Demographic Research, Special Collection 1, So-
cial Interactions and HIV/AIDS in Rural Africa, S1-7. &lt;http:/ /
www. demographic-research.org /special/ / 7&gt;. Accessed 2 Feb-
ruary 2004<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bracher</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">'Moving' and marrying: Modelling HIV infection among newly-weds in Malawi</comment>
               <fpage>S1</fpage>
               <source>Demographic Research</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d715e468a1310">
            <mixed-citation id="d715e472" publication-type="book">
.2003b. "A microsimulation study of the effects of divorce and
remarriage on lifetime risks of HIV/AIDS in rural Malawi." Pa-
per presented to the annual meeting of the Population Associa-
tion of America, Minneapolis, 1-3 May<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bracher</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A microsimulation study of the effects of divorce and remarriage on lifetime risks of HIV/AIDS in rural Malawi</comment>
               <source>annual meeting of the Population Association of America, Minneapolis, 1-3 May</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d715e508a1310">
            <mixed-citation id="d715e512" publication-type="book">
Chimbiri, Agnes. 2003. "The condom is an 'intruder' in marriage: Evi-
dence from rural Malawi." Paper presented at the International
Union for the Scientific Study of Population seminar, Taking Stock
of the Condom in the Era of HIV/AIDS, Gaborone, Botswana, 13-
17 July. &lt;http://www.iussp.org/members/restricted/publications /
Gaborone03 / 5-rep-chimbiri03.pdf&gt;. Accessed 2 February 2004<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Chimbiri</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The condom is an 'intruder' in marriage: Evidence from rural Malawi</comment>
               <source>International Union for the Scientific Study of Population seminar, Taking Stock of the Condom in the Era of HIV/AIDS, Gaborone, Botswana, 1317 July</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d715e553a1310">
            <mixed-citation id="d715e557" publication-type="book">
Coale, Ansley J. and Paul Demeny. 1983. Regional Model Life Tables
and Stable Populations. New York: Academic Press<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Coale</surname>
                  </string-name>
               </person-group>
               <source>Regional Model Life Tables and Stable Populations</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d715e582a1310">
            <mixed-citation id="d715e586" publication-type="book">
Farr, Gaston. 2000. "Design and manufacture of male non-latex con-
doms for prevention of pregnancy and STIs." In Condoms. Ed.
Adrian Mindel. London: BMJ Books. Pp. 175-189<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Farr</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Design and manufacture of male non-latex condoms for prevention of pregnancy and STIs</comment>
               <fpage>175</fpage>
               <source>Condoms</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d715e621a1310">
            <mixed-citation id="d715e625" publication-type="book">
Hatcher, Robert A., James Trussell, Felicia Stewart et al. 1998. Con-
traceptive Technology. Seventeenth revised edition. New York: Ar-
dent Media<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hatcher</surname>
                  </string-name>
               </person-group>
               <edition>17</edition>
               <source>Contraceptive Technology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d715e657a1310">
            <mixed-citation id="d715e661" publication-type="journal">
Hearst, Norman and Sanny Chen. 2004. "Condom promotion for
AIDS prevention in the developing world: Is it working?" Stud-
ies in Family Planning35(1): 39-47<object-id pub-id-type="jstor">10.2307/3181160</object-id>
               <fpage>39</fpage>
            </mixed-citation>
         </ref>
         <ref id="d715e680a1310">
            <mixed-citation id="d715e684" publication-type="journal">
Kaler, Amy. 2004. "The moral lens of population control: Condoms
and controversies in southern Malawi." Studies in Family Plan-
ning35(2) (forthcoming)<object-id pub-id-type="jstor">10.2307/3181138</object-id>
            </mixed-citation>
         </ref>
         <ref id="d715e701a1310">
            <mixed-citation id="d715e705" publication-type="book">
Miller, James N. 1989. "Cellular and molecular approaches to the de-
velopment of a vaccine for syphilis: Current status and prospects
for the future." In Vaccines for Sexually Transmitted Diseases. A.
Meheus and R.E. Spier. London: Butterworths. Pp. 105-106<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Miller</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cellular and molecular approaches to the development of a vaccine for syphilis: Current status and prospects for the future</comment>
               <fpage>105</fpage>
               <source>Vaccines for Sexually Transmitted Diseases</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d715e743a1310">
            <mixed-citation id="d715e747" publication-type="book">
Mindel, Adrian and Claudia Estcourt. 2000. "Condoms for the pre-
vention of sexually transmitted infections." In Condoms. Ed.
Adrian Mindel. London: BMJ Books. Pp. 62-84<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mindel</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Condoms for the prevention of sexually transmitted infections</comment>
               <fpage>62</fpage>
               <source>Condoms</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d715e782a1310">
            <mixed-citation id="d715e786" publication-type="book">
National Statistical Office, Malawi, and ORC Macro. 1997. Malawi
Knowledge, Attitudes and Practices in Health Survey 1996. Zomba,
Malawi and Calverton, MD: National Statistical Office, Malawi,
and ORC Macro<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>National Statistical Office, Malawi, and ORC Macro</surname>
                  </string-name>
               </person-group>
               <source>Malawi Knowledge, Attitudes and Practices in Health Survey 1996</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d715e818a1310">
            <mixed-citation id="d715e822" publication-type="book">
Niruthisard, Somchai, Iqbal Shah, Ina Warriner, and Mags Beksinka.
2003. "The latex versus the non-latex male condom: Perspectives
from contrasting populations." Paper presented at the Interna-
tional Union for the Scientific Study of Population seminar, Tak-
ing Stock of the Condom in the Era of HIV/AIDS, Gaborone,
Botswana, 13-17 July. &lt;http: / /www.iussp.org/members/re-
stricted / publications / Gaborone03 / 5-rep-niruthisard03.pdf&gt;.
Accessed 2 February 2004<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Niruthisard</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The latex versus the non-latex male condom: Perspectives from contrasting populations</comment>
               <source>International Union for the Scientific Study of Population seminar, Taking Stock of the Condom in the Era of HIV/AIDS, Gaborone, Botswana, 13-17 July</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d715e870a1310">
            <mixed-citation id="d715e874" publication-type="journal">
Røttingen, John-Arne, D. William Cameron, and Geoffrey P. Garnett.
2001. "A systematic review of the epidemiologic interactions be-
tween classic sexually transmitted diseases and HIV: How much
really is known?" Sexually Transmitted Diseases28(10): 579-597<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Røttingen</surname>
                  </string-name>
               </person-group>
               <issue>10</issue>
               <fpage>579</fpage>
               <volume>28</volume>
               <source>Sexually Transmitted Diseases</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d715e915a1310">
            <mixed-citation id="d715e919" publication-type="book">
Santow, Gigi. 2001. "Microsimulation in demographic research." In
International Encyclopedia of the Social and Behavioral Sciences. Vol-
ume 14. Eds. N.J. Smelser and P.B. Baltes. Amsterdam: Pergamon.
Pp. 9780-9,785<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Santow</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Microsimulation in demographic research</comment>
               <fpage>9780</fpage>
               <volume>14</volume>
               <source>International Encyclopedia of the Social and Behavioral Sciences</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d715e961a1310">
            <mixed-citation id="d715e965" publication-type="book">
Smith, Kirsten and Susan Cotts Watkins. 2002. "Perception of risk in
rural Malawi." Paper presented at the Population Studies Cen-
ter, University of Pennsylvania, 30 September<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Perception of risk in rural Malawi</comment>
               <source>Population Studies Center, University of Pennsylvania, 30 September</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d715e997a1310">
            <mixed-citation id="d715e1001" publication-type="book">
Spencer, Brenda and John Gerofi. 2000. "Can we tell them how to do
it?" In Condoms. Ed. Adrian Mindel. London: BMJ Books. Pp. 207-
219<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Spencer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Can we tell them how to do it?</comment>
               <fpage>207</fpage>
               <source>Condoms</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d715e1036a1310">
            <mixed-citation id="d715e1040" publication-type="book">
Stamm, Lola V. 1999. "Biology of Treponema pallidum." In Sexually
Transmitted Diseases. Third edition. Eds. King K. Holmes, Per-
Anders Mardh, P. Frederick Sparling et al. New York: McGraw-
Hill. Pp. 467-472<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Stamm</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <comment content-type="section">Biology of Treponema pallidum</comment>
               <fpage>467</fpage>
               <source>Sexually Transmitted Diseases</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d715e1081a1310">
            <mixed-citation id="d715e1085" publication-type="book">
Swartz, Morton N., Bernadine P. Healy, and Daniel M. Musher. 1999.
"Late syphilis." In Sexually Transmitted Diseases. Third edition.
Eds. King K. Holmes, Per-Anders Mardh, P. Frederick Sparling
et al. New York: McGraw-Hill. Pp. 487-509<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swartz</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <comment content-type="section">Late syphilis</comment>
               <fpage>487</fpage>
               <source>Sexually Transmitted Diseases</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d715e1126a1310">
            <mixed-citation id="d715e1130" publication-type="book">
Tawfik, Linda and Susan Cotts Watkins. 2003. "Sex in Geneva, sex in
Lilongwe and sex in Balaka." Paper presented to the annual meet-
ing of the Population Association of America, Minneapolis, 1-3 May<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Tawfik</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Sex in Geneva, sex in Lilongwe and sex in Balaka</comment>
               <source>annual meeting of the Population Association of America, Minneapolis, 1-3 May</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d715e1162a1310">
            <mixed-citation id="d715e1166" publication-type="journal">
Trussell, James and Kathryn Kost. 1987. "Contraceptive failure in the
United States: A critical review of the literature." Studies in Fam-
ily Planning18(5): 237-283<object-id pub-id-type="doi">10.2307/1966856</object-id>
               <fpage>237</fpage>
            </mixed-citation>
         </ref>
         <ref id="d715e1186a1310">
            <mixed-citation id="d715e1190" publication-type="journal">
van der Ploeg, Catharina P.B., Carina van Vliet, Sake J. de Vlas, Jecko-
niah 0. Ndinya-Achola, Lieve Fransen, Gerrit J. van Oortmarssen,
and J. Dik F. Habbema. 1998. "STDSIM: A microsimulation model
for decision support in STD control." Interfaces28(3): 84-100<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>van der Ploeg</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>84</fpage>
               <volume>28</volume>
               <source>Interfaces</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d715e1231a1310">
            <mixed-citation id="d715e1235" publication-type="journal">
van Vliet, Carina, Elisabeth I. Meester, Eline L. Korenromp, Burton
Singer, Roel Bakker, and J. Dik F. Habbema. 2001. "Focusing strat-
egies of condom use against HIV in different behavioural settings:
An evaluation based on a simulation model." Bulletin of the World
Health Organization79(5): 442-454<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>van Vliet</surname>
                  </string-name>
               </person-group>
               <issue>5</issue>
               <fpage>442</fpage>
               <volume>79</volume>
               <source>Bulletin of the World Health Organization</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d715e1279a1310">
            <mixed-citation id="d715e1283" publication-type="journal">
Zaba, Basia and John Blacker. 1997. "HIV prevalence and lifetime risk of
dying of AIDS." Health Transition Review7 (Supplement 2): 45-62<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Zaba</surname>
                  </string-name>
               </person-group>
               <issue>Supplement 2</issue>
               <fpage>45</fpage>
               <volume>7</volume>
               <source>Health Transition Review</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
