<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30086283</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Antiretroviral Concentrations in Breast-Feeding Infants of Women in Botswana Receiving Antiretroviral Treatment</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Roger L.</given-names>
                  <surname>Shapiro</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Diane T.</given-names>
                  <surname>Holland</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Edmund</given-names>
                  <surname>Capparelli</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Shahin</given-names>
                  <surname>Lockman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Ibou</given-names>
                  <surname>Thior</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Carolyn</given-names>
                  <surname>Wester</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Lisa</given-names>
                  <surname>Stevens</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Trevor</given-names>
                  <surname>Peter</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Max</given-names>
                  <surname>Essex</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>James D.</given-names>
                  <surname>Connor</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Mark</given-names>
                  <surname>Mirochnick</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">192</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id>i30086279</issue-id>
         <fpage>720</fpage>
         <lpage>727</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30086283"/>
         <abstract>
            <p>Background. The magnitude of initial antiretroviral (ARV) exposure from breast milk is unknown. Methods. We measured concentrations of nevirapine, lamivudine, and zidovudine in serum and whole breast milk from human immunodeficiency virus type 1 (HIV-1)-infected women in Botswana receiving ARV treatment and serum from their uninfected, breast-feeding infants. Results. Twenty mother-infant pairs were enrolled. Maternal serum concentrations of nevirapine were high (median, 9534 ng/ml at a median of 4 h after nevirapine ingestion). Median breast-milk concentrations of nevirapine, lamivudine, and zidovudine were 0.67, 3.34, and 3.21 times, respectively, those in maternal serum. The median infant serum concentration of nevirapine was 971 ng/mL, at least 40 times the 50% inhibitory concentration and similar to peak concentrations after a single 2-mg/kg dose of nevirapine. The median infant serum concentration of lamivudine was 28 ng/mL, and the median infant serum concentration of zidovudine was 123 ng/mL, but infants were also receiving zidovudine prophylaxis. Conclusions. HIV-1 inhibitory concentrations of nevirapine are achieved in breast-feeding infants of mothers receiving these ARVs, exposing infants to the potential for beneficial and adverse effects of nevirapine ingestion. Further study is needed to understand the impact of material ARV treatment on breast-feeding HIV-1 transmission, infant toxity, and HIV-1 resistance mutations among infected infants.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d500e286a1310">
            <label>1</label>
            <mixed-citation id="d500e293" publication-type="other">
The Breastfeeding and HIV International Transmission Study Group.
Late postnatal transmission of HIV-1 in breast-fed children: an indi-
vidual patient data meta-analysis. J Infect Dis 2004; 189:2154-66.</mixed-citation>
         </ref>
         <ref id="d500e306a1310">
            <label>2</label>
            <mixed-citation id="d500e313" publication-type="other">
De Cock KM, Fowler MG, Mercier E, et al. Prevention of mother-to-
child HIV transmission in resource-poor countries. JAMA 2000; 283:
1175-82.</mixed-citation>
         </ref>
         <ref id="d500e326a1310">
            <label>3</label>
            <mixed-citation id="d500e333" publication-type="other">
World Health Organization (WHO), WHO Collaborative Study Team.
Effect of breastfeeding on infant and child mortality due to infectious
diseases in less developed countries: a pooled analysis. Lancet 2000;
355:451-55.</mixed-citation>
         </ref>
         <ref id="d500e349a1310">
            <label>4</label>
            <mixed-citation id="d500e356" publication-type="other">
Vyankandondera J, Luchters S, Hassink E, et al. Reducing risk of HIV-
1 transmission from mother to infant through breastfeeding using
antiretroviral prophylaxis in infants (SIMBA study) [abstract LB7]. In:
Program and abstracts of the 2nd IAS Conference on HIV Pathogenesis
and Treatment (Paris). Geneva, International AIDS Society, 2003.</mixed-citation>
         </ref>
         <ref id="d500e376a1310">
            <label>5</label>
            <mixed-citation id="d500e383" publication-type="other">
Gaillard P, Fowler MG, Dabis F, et al. Use of antiretroviral drugs to
prevent HIV-1 transmission through breast-feeding: from animal stud-
ies to randomized clinical trials. J Acquir Immune Defic Syndr 2004;35:
178-87.</mixed-citation>
         </ref>
         <ref id="d500e399a1310">
            <label>6</label>
            <mixed-citation id="d500e406" publication-type="other">
Musoke P, Guay LA, Bagenda D, et al. A phase I/II study of the safety
and pharmacokinetics of nevirapine in HIV-1-infected pregnant Ugan-
dan women and their neonates (HIVNET 006). AIDS 1999; 13:479-86.</mixed-citation>
         </ref>
         <ref id="d500e419a1310">
            <label>7</label>
            <mixed-citation id="d500e426" publication-type="other">
Parkin NT, Hellmann NS, Whitcomb JM, Kiss L, Chappey C, Petro-
poulos CJ. Natural variation of drug susceptibility in wild-type human
immunodeficiency virus type 1. Antimicrob Agents Chemother 2004;
48:437-43.</mixed-citation>
         </ref>
         <ref id="d500e442a1310">
            <label>8</label>
            <mixed-citation id="d500e449" publication-type="other">
Mirochnick M, Fenton T, Gagnier P, et al. Pharmacokinetics of nevi-
rapine in human immunodeficiency virus type 1-infected pregnant
women and their neonates. J Infect Dis 1998; 178:368-74.</mixed-citation>
         </ref>
         <ref id="d500e462a1310">
            <label>9</label>
            <mixed-citation id="d500e469" publication-type="other">
GlaxoSmithKline. Epivir (lamivudine) package insert. Research Tri-
angle Park, NC: GlaxoSmithKline, 2002.</mixed-citation>
         </ref>
         <ref id="d500e479a1310">
            <label>10</label>
            <mixed-citation id="d500e486" publication-type="other">
Guay L, Musoke P, Fleming T, et al. Intrapartum and neonatal single-
dose nevirapine compared with zidovudine for prevention of mother-
to-child transmission of HIV-1 in Kampala, Uganda: HIVNET 012
randomised trial. Lancet 1999;354:795-802.</mixed-citation>
         </ref>
         <ref id="d500e503a1310">
            <label>11</label>
            <mixed-citation id="d500e510" publication-type="other">
Ruprecht RM, Sharpe AH, Jaenisch R, Trites D. Analysis of 3-azido-
3-deoxythymidine levels in tissues and milk by isocratic high-perfor-
mance liquid chromatography. J Chromatogr 1990;528:371-83.</mixed-citation>
         </ref>
         <ref id="d500e523a1310">
            <label>12</label>
            <mixed-citation id="d500e532" publication-type="other">
Acosta EP, King JR. Methods for integration of pharmacokinetic and
phenotypic information in the treatment of infection with human im-
munodeficiency virus. Clin Infect Dis 2003; 36:373-7.</mixed-citation>
         </ref>
         <ref id="d500e545a1310">
            <label>13</label>
            <mixed-citation id="d500e552" publication-type="other">
Hosseinipour M, Corbett A, Kanyama C, Mshali I, et al. Pharmaco-
kinetic comparison of generic and trade formulations of lamivudine,
stavudine, and nevirapine in 12 HIV-infected Malawian Subjects [ab-
stract 631.]. In: Program and abstracts of the 12th Conference on
Retroviruses and Opportunistic Infections (Boston). Alexandria, VA:
Foundation for Retrovirology and Human Health, 2005:288.</mixed-citation>
         </ref>
         <ref id="d500e575a1310">
            <label>14</label>
            <mixed-citation id="d500e582" publication-type="other">
Aweeka F, Lizak P, Frenkel L, et al. Steady state nevirapine pharma-
cokinetics during second and third trimester pregnancy and postpar-
tum: PACTG 1022 [abstract 932]. In: Program and abstracts of the
11th Conference on Retroviruses and Opportunistic Infections (San
Francisco). Alexandria, VA: Foundation for Retrovirology and Human
Health, 2004.</mixed-citation>
         </ref>
         <ref id="d500e605a1310">
            <label>15</label>
            <mixed-citation id="d500e612" publication-type="other">
Wolf E, Ruemmelein N, Hoffmann C, et al. Week 2-nevirapine (NVP)
plasma levels are associated with NVP-related toxicity [abstract
TuPeB4640]. In: Program and abstracts of the XV International AIDS
Conference (Bangkok). Geneva: International AIDS Society, 2004.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
