<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4462094</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Adverse and Beneficial Secondary Effects of Mass Treatment with Azithromycin to Eliminate Blindness Due to Trachoma in Nepal</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>A. M. Fry</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>H. C. Jha</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>T. M. Lietman</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>J. S. P. Chaudhary</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>R. C. Bhatta</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J.</given-names>
                  <surname>Elliott</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>T. Hyde</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>A.</given-names>
                  <surname>Schuchat</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>B. Gaynor</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>S. F. Dowell</string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>8</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i401628</issue-id>
         <fpage>395</fpage>
         <lpage>402</lpage>
         <page-range>395-402</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4462094"/>
         <abstract>
            <p>Mass administration of azithromycin to eliminate blindness due to trachoma has raised concerns regarding the emergence of antimicrobial resistance. During 2000, we compared the antimicrobial resistance of nasopharyngeal pneumococcal isolates recovered from and the prevalence of impetigo, respiratory symptoms, and diarrhea among 458 children in Nepal before and after mass administration of azithromycin. No azithromycin-resistant pneumococci were isolated except from 4.3% of children who had received azithromycin during 2 previous mass treatments (P&lt;.001). There were decrease in the prevalence of impetigo (from 14% to 6% of subjects; adjusted odds ratio [OR], 0.41; 95% confidence interval [CI], 0.21-0.80) and diarrhea (from 32% to 11%; adjusted OR, 0.26; 95% CI, 0.14-0.43) 10 days after azithromycin treatment. The absence of macrolide-resistant isolates after 1 mass treatment with azithromycin is encouraging, although the recovery of azithromycin-resistant isolates after 2 mass treatments suggests the need for resistance monitoring when multiple rounds of antimicrobial treatment are given.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d330e208a1310">
            <label>1</label>
            <mixed-citation id="d330e215" publication-type="other">
World Health Organization. 51st World Health Assembly. Resolution
51.11. 1998. Available at: http://who.int/pbd/trachoma/wha51-e.htm.
Accessed 25 June 2002.</mixed-citation>
         </ref>
         <ref id="d330e228a1310">
            <label>2</label>
            <mixed-citation id="d330e235" publication-type="journal">
Bailey RL, Whittle HC, Mabey D. Randomised controlled trial of single-
dose azithromycin in treatment of trachoma. Lancet1993; 342:453-6.<person-group>
                  <string-name>
                     <surname>Bailey</surname>
                  </string-name>
               </person-group>
               <fpage>453</fpage>
               <volume>342</volume>
               <source>Lancet</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d330e267a1310">
            <label>3</label>
            <mixed-citation id="d330e274" publication-type="journal">
Tabbara KF, al-Omar O, Choudhury AH, al-Faisal Z. Single-dose azith-
romycin in the treatment of trachoma: a randomised, controled study.
Ophthalmology1996; 103:842-6.<person-group>
                  <string-name>
                     <surname>Tabbara</surname>
                  </string-name>
               </person-group>
               <fpage>842</fpage>
               <volume>103</volume>
               <source>Ophthalmology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d330e309a1310">
            <label>4</label>
            <mixed-citation id="d330e316" publication-type="journal">
Dawson CR, Sallam S, Sheta A, et al. A comparison of oral azithromycin
with topical oxytetracycline/polymyxin for the treatment of trachoma
in children. Clin Infect Dis1997;24:363-8.<person-group>
                  <string-name>
                     <surname>Dawson</surname>
                  </string-name>
               </person-group>
               <fpage>363</fpage>
               <volume>24</volume>
               <source>Clin Infect Dis</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d330e352a1310">
            <label>5</label>
            <mixed-citation id="d330e359" publication-type="journal">
Schachter J, Mabey DCW, Dawson CW, et al. Azithromycin in control
of trachoma. Lancet1999; 354:630-5.<person-group>
                  <string-name>
                     <surname>Schachter</surname>
                  </string-name>
               </person-group>
               <fpage>630</fpage>
               <volume>354</volume>
               <source>Lancet</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d330e391a1310">
            <label>6</label>
            <mixed-citation id="d330e398" publication-type="journal">
Leach AJ, Mayo M, Gratten M, et al. A prospective study of the impact
of community-based azithromycin treatment of trachoma on carriage
and resistance of Streptococcus pneumoniae. Clin Infect Dis1997;24:
356-62.<person-group>
                  <string-name>
                     <surname>Leach</surname>
                  </string-name>
               </person-group>
               <fpage>356</fpage>
               <volume>24</volume>
               <source>Clin Infect Dis</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d330e436a1310">
            <label>7</label>
            <mixed-citation id="d330e443" publication-type="journal">
Chern KC, Cevallos V, Dhami HL, et al. alterations in the conjunctival
bacterial flora following a single dose of azithromycin in a trachoma
endemic area. Br J Ophthalmol1999;83:1332-5.<person-group>
                  <string-name>
                     <surname>Chern</surname>
                  </string-name>
               </person-group>
               <fpage>1332</fpage>
               <volume>83</volume>
               <source>Br J Ophthalmol</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d330e478a1310">
            <label>8</label>
            <mixed-citation id="d330e485" publication-type="journal">
Hyde TB, Gay K, Stephens RS, et al. Macrolide resistance among in-
vasive Streptococcus pneumoniae isolates. JAMA2001; 286:1857-62.<person-group>
                  <string-name>
                     <surname>Hyde</surname>
                  </string-name>
               </person-group>
               <fpage>1857</fpage>
               <volume>286</volume>
               <source>JAMA</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d330e517a1310">
            <label>9</label>
            <mixed-citation id="d330e524" publication-type="journal">
Seppala H, Klaukka T, Vuopio-Varkila J, et al. The effect of changes
in the consumption of macrolide antibiotics on erythromycin resistance
in group A streptococci in Finland. Finnish study group for antimi-
crobial resistance. N Engl J Med1997;337:441-6.<person-group>
                  <string-name>
                     <surname>Seppala</surname>
                  </string-name>
               </person-group>
               <fpage>441</fpage>
               <volume>337</volume>
               <source>N Engl J Med</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d330e562a1310">
            <label>10</label>
            <mixed-citation id="d330e569" publication-type="journal">
Whitney CG, Farley MM, Hadler J, et al. Increasing prevalence of
multidrug-resistant Streptococcus pneumoniae in the United States. N
Engl J Med2000; 343:1917-24.<person-group>
                  <string-name>
                     <surname>Whitney</surname>
                  </string-name>
               </person-group>
               <fpage>1917</fpage>
               <volume>343</volume>
               <source>N Engl J Med</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d330e605a1310">
            <label>11</label>
            <mixed-citation id="d330e612" publication-type="journal">
World Health Organization. Integrated management of the sick child.
Bull World Health Organ1995; 73:735-40.<person-group>
                  <string-name>
                     <surname>World Health Organization</surname>
                  </string-name>
               </person-group>
               <fpage>735</fpage>
               <volume>73</volume>
               <source>Bull World Health Organ</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d330e644a1310">
            <label>12</label>
            <mixed-citation id="d330e651" publication-type="journal">
Adegbola RA, Bailey R, Secka O, et al. Effect of azithromycin on pha-
ryngeal microflora. Pediatr Infect Dis J1995; 14:335-6.<person-group>
                  <string-name>
                     <surname>Adegbola</surname>
                  </string-name>
               </person-group>
               <fpage>335</fpage>
               <volume>14</volume>
               <source>Pediatr Infect Dis J</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d330e683a1310">
            <label>13</label>
            <mixed-citation id="d330e690" publication-type="journal">
Whitty JM, Glasgow KW, Sadiq ST, et al. Impact of community-based
mass treatment for trachoma with oral azithromycin on general mor-
bidity in Gambia children. Pediatr Infect Dis J1999; 18:955-8.<person-group>
                  <string-name>
                     <surname>Whitty</surname>
                  </string-name>
               </person-group>
               <fpage>955</fpage>
               <volume>18</volume>
               <source>Pediatr Infect Dis J</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d330e725a1310">
            <label>14</label>
            <mixed-citation id="d330e732" publication-type="journal">
Tarlow MJ, Harris J, Kolokathis A. Future indications for macrolides.
Pediatr Infect Dis J1997; 16:457-62.<person-group>
                  <string-name>
                     <surname>Tarlow</surname>
                  </string-name>
               </person-group>
               <fpage>457</fpage>
               <volume>16</volume>
               <source>Pediatr Infect Dis J</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d330e764a1310">
            <label>15</label>
            <mixed-citation id="d330e771" publication-type="journal">
Holms SO, Bhatta RC, Chaudhary JS, et al. Comparison of two azith-
romycin distribution strategies for controlling trachoma in Nepal. Bull
World Health Organ2001; 79:194-200.<person-group>
                  <string-name>
                     <surname>Holms</surname>
                  </string-name>
               </person-group>
               <fpage>194</fpage>
               <volume>79</volume>
               <source>Bull World Health Organ</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d330e806a1310">
            <label>16</label>
            <mixed-citation id="d330e813" publication-type="journal">
O'Brien KL, Dagan R, Yagupsky P, et al. Evaluation of a medium
(STGG) for transport and optimal recovery of Streptococcus pneumon-
iae from nasopharyngeal secretions collected during field studies. J Clin
Microbiol2001; 39:1021-4.<person-group>
                  <string-name>
                     <surname>O'Brien</surname>
                  </string-name>
               </person-group>
               <fpage>1021</fpage>
               <volume>39</volume>
               <source>J Clin Microbiol</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d330e852a1310">
            <label>17</label>
            <mixed-citation id="d330e859" publication-type="book">
National Committee for Clinical Laboratory Standards (NCCLS). Per-
formance standards for antimicrobial susceptibility testing: approved
standard. NCCLS document M100-S9. Wayne, PA: NCCLS, 1999.<person-group>
                  <string-name>
                     <surname>National Committee for Clinical Laboratory Standards (NCCLS)</surname>
                  </string-name>
               </person-group>
               <source>Performance standards for antimicrobial susceptibility testing: approved standard</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d330e888a1310">
            <label>18</label>
            <mixed-citation id="d330e895" publication-type="journal">
Bowman RJ, Van Den C, Goode VM, et al. Operational comparison
of single-dose azithromycin and topical tetracycline for trachoma. In-
vest Ophthalmol Vis Sci2000;41:4074-9.<person-group>
                  <string-name>
                     <surname>Bowman</surname>
                  </string-name>
               </person-group>
               <fpage>4074</fpage>
               <volume>41</volume>
               <source>Invest Ophthalmol Vis Sci</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d330e930a1310">
            <label>19</label>
            <mixed-citation id="d330e937" publication-type="journal">
Thylefors B, Negrel A, Pararajasegarem R, Dadzie K. Global data on
blindness. Bull World Health Organ1995; 73:115-21.<person-group>
                  <string-name>
                     <surname>Thylefors</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>73</volume>
               <source>Bull World Health Organ</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d330e969a1310">
            <label>20</label>
            <mixed-citation id="d330e976" publication-type="journal">
Cheney CP. Acute infectious diarrhea. Med Clin North Am1993; 77:
1169-96.<person-group>
                  <string-name>
                     <surname>Cheney</surname>
                  </string-name>
               </person-group>
               <fpage>1169</fpage>
               <volume>77</volume>
               <source>Med Clin North Am</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1008a1310">
            <label>21</label>
            <mixed-citation id="d330e1015" publication-type="journal">
Thomas RJ, Conwill DE, Morton DE, et al. Penicillin prophylaxis for
streptococcal infections in the United States Navy and Marine Corps
recruitment camps, 1951-1985. Rev Infect Dis1988; 10:125-30.<person-group>
                  <string-name>
                     <surname>Thomas</surname>
                  </string-name>
               </person-group>
               <fpage>125</fpage>
               <volume>10</volume>
               <source>Rev Infect Dis</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1050a1310">
            <label>22</label>
            <mixed-citation id="d330e1057" publication-type="journal">
Gray GC, McPhate DC, Leinonen M, et al. Weekly oral azithromycin
as prophylaxis for agents causing acute respiratory disease. Clin Infect
Dis1998;26:103-10.<person-group>
                  <string-name>
                     <surname>Gray</surname>
                  </string-name>
               </person-group>
               <fpage>103</fpage>
               <volume>26</volume>
               <source>Clin Infect Dis</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1093a1310">
            <label>23</label>
            <mixed-citation id="d330e1100" publication-type="journal">
Schrag SJ, Bealle B, Dowell SF Limiting the spread of resistant pneu-
mococci: biological and epidemiologic evidence for the effectiveness
of alternative interventions. Clin Microbiol Rev2000; 13:588-601.<person-group>
                  <string-name>
                     <surname>Schrag</surname>
                  </string-name>
               </person-group>
               <fpage>588</fpage>
               <volume>13</volume>
               <source>Clin Microbiol Rev</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1135a1310">
            <label>24</label>
            <mixed-citation id="d330e1142" publication-type="journal">
Scheidler VF, Bhatta RC, Miao Y, et al. Antibiotic use patterns in a
trachoma-epidemic region of Nepal. Ophthalmic Epidemiol (in press).<person-group>
                  <string-name>
                     <surname>Scheidler</surname>
                  </string-name>
               </person-group>
               <source>Ophthalmic Epidemiol</source>
            </mixed-citation>
         </ref>
         <ref id="d330e1164a1310">
            <label>25</label>
            <mixed-citation id="d330e1171" publication-type="journal">
Emerson PM, Bailey RL, Mabey DCW. Review of the evidence base
for the "F" and "E" components of the SAFE strategy for trachoma
control. Trop Med Int Health2000; 5:515-27.<person-group>
                  <string-name>
                     <surname>Emerson</surname>
                  </string-name>
               </person-group>
               <fpage>515</fpage>
               <volume>5</volume>
               <source>Trop Med Int Health</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1206a1310">
            <label>26</label>
            <mixed-citation id="d330e1213" publication-type="other">
UNAIDS [United Nations Programme on HIV/AIDS]. Accelerating
access to HIV care, support and treatment: opportunistic infections.
2001. Available at: http://www.unaids.org/acc_access/opportunistic_
infections/index.html. Accessed 25 June 2002.</mixed-citation>
         </ref>
         <ref id="d330e1229a1310">
            <label>27</label>
            <mixed-citation id="d330e1236" publication-type="journal">
Gilles HM. The conquest of "river blindness". Ann Trop Med Parasitol
1991; 85:97-101.<person-group>
                  <string-name>
                     <surname>Gilles</surname>
                  </string-name>
               </person-group>
               <fpage>97</fpage>
               <volume>85</volume>
               <source>Ann Trop Med Parasitol</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1268a1310">
            <label>28</label>
            <mixed-citation id="d330e1275" publication-type="journal">
Burnham GM. Adverse reactions to ivermectin treatment for oncho-
cerciasis. Results of a placebo-controlled, double-blind trial in Malawi.
Trans R Soc Trop Med Hyg1993;87:313-7.<person-group>
                  <string-name>
                     <surname>Burnham</surname>
                  </string-name>
               </person-group>
               <fpage>313</fpage>
               <volume>87</volume>
               <source>Trans R Soc Trop Med Hyg</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d330e1311a1310">
            <label>29</label>
            <mixed-citation id="d330e1318" publication-type="journal">
Pacque M, Greene BM, Taylor HR. Community-based treatment of
onchocerciasis with ivermectin: safety, efficacy, and acceptability of
yearly treatment. J Infect Dis1991; 163:381-5.<person-group>
                  <string-name>
                     <surname>Pacque</surname>
                  </string-name>
               </person-group>
               <fpage>381</fpage>
               <volume>163</volume>
               <source>J Infect Dis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
