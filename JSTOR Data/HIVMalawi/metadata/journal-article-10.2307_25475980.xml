<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">demography</journal-id>
         <journal-id journal-id-type="jstor">j100446</journal-id>
         <journal-title-group>
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Population Association of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00703370</issn>
         <issn pub-type="epub">15337790</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25475980</article-id>
         <title-group>
            <article-title>Marital Strategies for Regulating Exposure to HIV</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Georges</given-names>
                  <surname>Reniers</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">45</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i25475970</issue-id>
         <fpage>417</fpage>
         <lpage>438</lpage>
         <permissions>
            <copyright-statement>Copyright The Population Association of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25475980"/>
         <abstract>
            <p>In a setting where the transmission of HIV occurs primarily through heterosexual contact and where no cure or vaccine is available, behavioral change is imperative for containing the epidemic. Abstinence, faithfulness, and condom use most often receive attention in this regard. In contrast, this article treats marriage as a resource for HIV risk management via mechanisms of positive selection (partner choice) and negative selection (divorce of an adulterous spouse). Retrospective marriage histories and panel data provide the evidence for this study, and results indicate that men and women in Malawi increasingly turned to union-based risk-avoidance strategies during the period that the threat of HIV/AIDS materialized. Although both sexes strategize in a similar fashion, men are better equipped than women to deploy these strategies to their advantage. The article concludes with reflections on the long-term and population-level implications of these coping mechanisms.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d918e134a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d918e141" publication-type="other">
Parker 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e147" publication-type="other">
Schoepf 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e153" publication-type="other">
Boerma and Weir (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d918e160a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d918e167" publication-type="other">
Lillard and Panis
1996</mixed-citation>
            </p>
         </fn>
         <fn id="d918e177a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d918e184" publication-type="other">
Huselid and
Cooper 1992</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e193" publication-type="other">
Caraël et al. 1992</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e199" publication-type="other">
Cleland, Ali, and Capo-Chichi
1999</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e209" publication-type="other">
Orubuloye, Caldwell, and Caldwell 1997</mixed-citation>
            </p>
         </fn>
         <fn id="d918e216a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d918e223" publication-type="other">
NSO and ORC Macro 2005</mixed-citation>
            </p>
         </fn>
         <fn id="d918e231a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d918e238" publication-type="other">
Gregson et al. 1998</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e244" publication-type="other">
Macintyre, Brown, and Sosler 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e250" publication-type="other">
Watkins 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d918e257a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d918e264" publication-type="other">
Watkins et al. 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e270" publication-type="other">
http://www.malawi.pop.upenn.edu/.</mixed-citation>
            </p>
         </fn>
         <fn id="d918e277a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d918e284" publication-type="other">
Douglas 1950</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e290" publication-type="other">
Kaler 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e296" publication-type="other">
Mitchell 1956</mixed-citation>
            </p>
         </fn>
         <fn id="d918e303a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d918e312" publication-type="other">
UNAIDS/WHO 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e318" publication-type="other">
Crampin et
al. 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e328" publication-type="other">
Decosas et al. 1995</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e334" publication-type="other">
Crampin et al. 2002</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e340" publication-type="other">
Porter and Zaba 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d918e347a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d918e354" publication-type="other">
Van de Walle and Meekers 1994</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e360" publication-type="other">
NSO and ORC Macro 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d918e367a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d918e374" publication-type="other">
NSO and ORC Macro 2005</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e380" publication-type="other">
Doctor and Weinreb
(2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d918e391a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d918e398" publication-type="other">
Luke 2002</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e404" publication-type="other">
Malungo 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d918e411a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d918e418" publication-type="other">
Allen 2006</mixed-citation>
            </p>
         </fn>
         <fn id="d918e425a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d918e432" publication-type="other">
Glynn et al. (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e438" publication-type="other">
Bongaarts (2007)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e444" publication-type="other">
Bongaarts 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d918e451a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d918e458" publication-type="other">
Esen 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d918e464" publication-type="other">
Wendo 2004</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d918e480a1310">
            <mixed-citation id="d918e484" publication-type="other">
Alderman, H., J. Behrman, H.-P. Kohler, J. Maluccio, and S. Watkins. 2001. "Attrition in Longitu-
dinal Household Survey Data." Demographic Research, Volume 5, article 4:79-124. Available
online at http://www.demographic-research.Org/volumes/vol5/4/5-4.pdf.</mixed-citation>
         </ref>
         <ref id="d918e497a1310">
            <mixed-citation id="d918e501" publication-type="other">
Allen, T. 2006. "AIDS and Evidence: Interrogating Some Ugandan Myths." Journal of Biosocial
Science 38:7-28.</mixed-citation>
         </ref>
         <ref id="d918e511a1310">
            <mixed-citation id="d918e515" publication-type="other">
Bignami-Van Assche, S., G. Reniers, and A.A. Weinreb. 2003. "An Assessment of the KDICP and
MDICP Data Quality." Demographic Research, Special collection 1, article 2:29-76. Available
online at http://www.demographic-research.Org/special/1/2/s1-2.pdf.</mixed-citation>
         </ref>
         <ref id="d918e528a1310">
            <mixed-citation id="d918e532" publication-type="other">
Bignami-Van Assche, S., K. Smith, G. Reniers, P. Anglewicz, R. Thornton, L.W. Chao, A.A. Weinreb,
S. Watkins, and I. Hoffman. 2004. "Protocol for Biomarker Testing in the 2004 Malawi Diffusion
and Ideational Change Project." Social Networks Project Working Paper 6. Population Studies
Center, University of Pennsylvania, Philadelphia.</mixed-citation>
         </ref>
         <ref id="d918e549a1310">
            <mixed-citation id="d918e553" publication-type="other">
Bloom, S.S., C. Banda, G. Songolo, S. Mulendema, A.E. Cunningham, and J.T. Boerma. 2000.
"Looking for Change in Response to the AIDS Epidemic: Trends in AIDS Knowledge and Sexual
Behavior in Zambia, 1990 Through 1998." Journal of Acquired Immune Deficiency Syndromes
25:77-85.</mixed-citation>
         </ref>
         <ref id="d918e569a1310">
            <mixed-citation id="d918e573" publication-type="other">
Boerma, J.T. and S.S. Weir. 2005. "Integrating Demographic and Epidemiological Approaches to Re-
search on HIV/AIDS: The Proximate-Determinants Framework." Journal of Infectious Diseases
191(Suppl. 1):61-67.</mixed-citation>
         </ref>
         <ref id="d918e586a1310">
            <mixed-citation id="d918e590" publication-type="other">
Bongaarts, J. 2007. "Late Marriage and the HIV Epidemic in Sub-Saharan Africa." Population Stud-
ies 61:73-83.</mixed-citation>
         </ref>
         <ref id="d918e600a1310">
            <mixed-citation id="d918e604" publication-type="other">
Booth, A. and D.R. Johnson. 1994. "Declining Health and Marital Quality." Journal of Marriage and
the Family 56:218-23.</mixed-citation>
         </ref>
         <ref id="d918e614a1310">
            <mixed-citation id="d918e618" publication-type="other">
Bracher, M., G. Santow, and S. Watkins. 2003. "A Microsimulation Study of the Effects of Marriage,
Divorce and Remarriage on Lifetime Risks of HIV/AIDS in Rural Malawi." Presented at annual
meeting of the Population Association of America, May 1-3, Minneapolis, MN.</mixed-citation>
         </ref>
         <ref id="d918e631a1310">
            <mixed-citation id="d918e635" publication-type="other">
Caldwell, J.C. 2000. "Rethinking the African AIDS Epidemic." Population and Development Review
26:117-35.</mixed-citation>
         </ref>
         <ref id="d918e646a1310">
            <mixed-citation id="d918e650" publication-type="other">
Caldwell, J.C, P. Caldwell, J. Anarfi, K. Awusabo-Asare, J. Ntozi, I.O. Orubuloye, J. Marck,
W. Cosford, R. Colombo, and E. Hollings, eds. 1999. Resistances to Behavioural Change to
Reduce HIV/AIDS Infection in Predominantly Heterosexual Epidemics in Third World Countries.
Canberra, Australia: Australian National University, Health Transition Centre.</mixed-citation>
         </ref>
         <ref id="d918e666a1310">
            <mixed-citation id="d918e670" publication-type="other">
Caraël, M., J. Cleland, J.-C. Deheneffe, L. Adeokun, and T. Dyson. 1992. "Research on Sexual
Behavior That Transmits HIV: The GPA/WHO Collaborative Surveys—Preliminary Findings."
Pp. 65-87 in Sexual Behaviour and Networking: Anthropological and Socio-Cultural Studies on
the Transmission of HIV, edited by T. Dyson. Liege: S. Derouaux-Ordina.</mixed-citation>
         </ref>
         <ref id="d918e686a1310">
            <mixed-citation id="d918e690" publication-type="other">
Carpenter, L.M., A. Kamali, A. Ruberantwari, S.S. Malamba, and J.A.G. Whitworth. 1999. "Rates of
HIV-1 Transmission Within Marriage in Rural Uganda in Relation to the HIV Sero-Status of the
Partners." AIDS 13:1083-89.</mixed-citation>
         </ref>
         <ref id="d918e703a1310">
            <mixed-citation id="d918e707" publication-type="other">
Clark, S. 2004. "Early Marriage and HIV Risks in Sub-Saharan Africa." Studies in Family Planning
35:149-60.</mixed-citation>
         </ref>
         <ref id="d918e717a1310">
            <mixed-citation id="d918e721" publication-type="other">
Cleland, J.G., M.M. AH, and V. Capo-Chichi. 1999. "Post-Partum Sexual Abstinence in West Africa:
Implications for AIDS-Control and Family Planning Programmes." AIDS 13:125-31.</mixed-citation>
         </ref>
         <ref id="d918e731a1310">
            <mixed-citation id="d918e735" publication-type="other">
Clogg, C.C., E. Petkova, and A. Haritou. 1995. "Statistical Methods for Comparing Regression Coef-
ficients Between Models." American Journal of Sociology 100:1261-93.</mixed-citation>
         </ref>
         <ref id="d918e746a1310">
            <mixed-citation id="d918e750" publication-type="other">
Crampin, A.C., S. Floyd, J.R. Glynn, F. Sibande, D. Mulawa, A. Nyondo, P. Broadbent, L. Bliss,
B. Ngwira, and P.E. Fine. 2002. "Long-Term Follow-up of HIV-Positive and HIV-Negative Indi-
viduals in Rural Malawi." AIDS 16:1545-50.</mixed-citation>
         </ref>
         <ref id="d918e763a1310">
            <mixed-citation id="d918e767" publication-type="other">
Crampin, A.C., J.R. Glynn, B.M. Ngwira, F.D. Mwaungulu, J.M. Ponnighaus, D.K. Warndorff,
and P.E. Fine. 2003. "Trends and Measurement of HIV Prevalence in Northern Malawi." AIDS
17:1817-25.</mixed-citation>
         </ref>
         <ref id="d918e780a1310">
            <mixed-citation id="d918e784" publication-type="other">
Decosas, J., F. Kane, J.K. Anarfi, K.D. Sodji, and H.U. Wagner. 1995. "Migration and AIDS." Lancet
346:826-28.</mixed-citation>
         </ref>
         <ref id="d918e794a1310">
            <mixed-citation id="d918e798" publication-type="other">
Doctor, H.V. and A.A. Weinreb. 2003. "Estimation of AIDS Adult Mortality by Verbal Autopsy in
Rural Malawi." AIDS 17:2509-13.</mixed-citation>
         </ref>
         <ref id="d918e808a1310">
            <mixed-citation id="d918e812" publication-type="other">
Donovan, B. 2000. "The Repertoire of Human Efforts to Avoid Sexually Transmissible Diseases: Past
and Present. Part 1: Strategies Used Before or Instead of Sex." Sexually Transmitted Infections
76:7-12.</mixed-citation>
         </ref>
         <ref id="d918e825a1310">
            <mixed-citation id="d918e829" publication-type="other">
Douglas, M.T. 1950. Peoples of the Lake Nyasa Region. London: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d918e837a1310">
            <mixed-citation id="d918e841" publication-type="other">
Eaton, L., A.J. Flisher, and L.E. Aaro. 2003. "Unsafe Sexual Behaviour in South African Youth."
Social Science and Medicine 56:149-65.</mixed-citation>
         </ref>
         <ref id="d918e851a1310">
            <mixed-citation id="d918e855" publication-type="other">
Esen, U.I. 2004. "African Women, Bride Price, and AIDS." Lancet 363:1734.</mixed-citation>
         </ref>
         <ref id="d918e862a1310">
            <mixed-citation id="d918e866" publication-type="other">
Farr, W. 1858. "The Influence of Marriage on the Mortality of the French People." Pp. 504-13
in Transactions of the National Association for the Promotion of Social Sciences, edited by
G.W. Hastings. London: John W. Parker and Son.</mixed-citation>
         </ref>
         <ref id="d918e879a1310">
            <mixed-citation id="d918e883" publication-type="other">
Fu, H. and N. Goldman. 1996. "Incorporating Health Into Models of Marriage Choice: Demographic
and Sociological Perspectives." Journal of Marriage and the Family 58:740-58.</mixed-citation>
         </ref>
         <ref id="d918e893a1310">
            <mixed-citation id="d918e897" publication-type="other">
Fylkesnes, K., R.M. Musonda, M. Sichone, Z. Ndhlovu, F. Tembo, and M. Monze. 2001. "Declining
HIV Prevalence and Risk Behaviours in Zambia: Evidence From Surveillance and Population-
Based Surveys." AIDS 15:907-16.</mixed-citation>
         </ref>
         <ref id="d918e910a1310">
            <mixed-citation id="d918e914" publication-type="other">
Glynn, J.R., M. Caraël, A. Buvé, R.M. Musonda, and M. Kahindo. 2003. "HIV Risk in Relation to
Marriage in Areas With High Prevalence of HIV Infection." Journal of Acquired Immune Defi-
ciency Syndromes 33:526-35.</mixed-citation>
         </ref>
         <ref id="d918e928a1310">
            <mixed-citation id="d918e932" publication-type="other">
Goldman, N. 1993a. "Marriage Selection and Mortality Patterns: Inferences and Fallacies." Demog-
raphy 30:189-208.</mixed-citation>
         </ref>
         <ref id="d918e942a1310">
            <mixed-citation id="d918e946" publication-type="other">
-. 1993b. "The Perils of Single Life in Contemporary Japan." Journal of Marriage and the
Family 55:191-204.</mixed-citation>
         </ref>
         <ref id="d918e956a1310">
            <mixed-citation id="d918e960" publication-type="other">
Green, E.C. 2003. Rethinking AIDS Prevention: Learning From Successes in Developing Countries.
Westport, CT: Praeger.</mixed-citation>
         </ref>
         <ref id="d918e970a1310">
            <mixed-citation id="d918e974" publication-type="other">
Gregory, R., R. Isingo, M. Marston, M. Urassa, J. Changalucha, M. Ndege, Y. Kumuloga, and B. Zaba.
2007. "HIV and Marital Outcomes: Dissolution and Remarriage in Kisesa, Tanzania." Presented at
the annual meeting of the Population Association of America, March 29-31, New York.</mixed-citation>
         </ref>
         <ref id="d918e987a1310">
            <mixed-citation id="d918e991" publication-type="other">
Gregson, S., T. Zhuwau, R.M. Anderson, and S.K. Chandiwana. 1998. "Is There Evidence for
Behaviour Change in Response to AIDS in Rural Zimbabwe?" Social Science and Medicine
46:321-30.</mixed-citation>
         </ref>
         <ref id="d918e1004a1310">
            <mixed-citation id="d918e1008" publication-type="other">
Heise, L.L. and C. Elias. 1995. "Transforming AIDS Prevention to Meet Women's Needs: A Focus
on Developing Countries." Social Science and Medicine 40:931-43.</mixed-citation>
         </ref>
         <ref id="d918e1019a1310">
            <mixed-citation id="d918e1023" publication-type="other">
Huselid, R.F. and M.L. Cooper. 1992. "Gender Roles as Mediators of Sex Differences in Adolescent
Alcohol Use and Abuse." Journal of Health and Social Behavior 33:348-62.</mixed-citation>
         </ref>
         <ref id="d918e1033a1310">
            <mixed-citation id="d918e1037" publication-type="other">
Kaler, A. 2001. "'Many Divorces and Many Spinsters': Marriage as an Invented Tradition in Southern
Malawi, 1946-1999." Journal of Family History 26:529-56.</mixed-citation>
         </ref>
         <ref id="d918e1047a1310">
            <mixed-citation id="d918e1051" publication-type="other">
-. 2004. "AIDS-Talk in Everyday Life: The Presence of HIV/AIDS in Men's Informal Conver-
sation in Southern Malawi." Social Science and Medicine 59:285-97.</mixed-citation>
         </ref>
         <ref id="d918e1061a1310">
            <mixed-citation id="d918e1065" publication-type="other">
Kamali, A., L.M. Carpenter, J.A. Whitworth, R. Pool, A. Ruberantwari, and A. Ojwiya. 2000. "Seven-
Year Trends in HIV-1 Infection Rates, and Changes in Sexual Behaviour, Among Adults in Rural
Uganda." AIDS 14:427-34.</mixed-citation>
         </ref>
         <ref id="d918e1078a1310">
            <mixed-citation id="d918e1082" publication-type="other">
Lillard, LA. and C.W. Panis. 1996. "Marital Status and Mortality: The Role of Health." Demography
33:313-27.</mixed-citation>
         </ref>
         <ref id="d918e1092a1310">
            <mixed-citation id="d918e1096" publication-type="other">
Lillard, LA. and L.J. Waite. 1995. "Til Death Do Us Part: Marital Disruption and Mortality." Ameri-
can Journal of Sociology 100:1131-56.</mixed-citation>
         </ref>
         <ref id="d918e1107a1310">
            <mixed-citation id="d918e1111" publication-type="other">
Locoh, T. and M.-P. Thiriat. 1995. "Divorce et Remariage des Femmes en Afrique de l'Ouest. Le
Cas du Togo" [Divorce and remarriage of women in West Africa. The case of Togo]. Population
50(l):61-93.</mixed-citation>
         </ref>
         <ref id="d918e1124a1310">
            <mixed-citation id="d918e1128" publication-type="other">
Luke, N. 2002. "Widows and 'Professional Inheritors': Understanding AIDS Risk Perceptions in
Kenya." Presented at the annual meeting of the Population Association of America, May 8-11,
Atlanta, GA.</mixed-citation>
         </ref>
         <ref id="d918e1141a1310">
            <mixed-citation id="d918e1145" publication-type="other">
Macintyre, K., L. Brown, and S. Sosler. 2001. "Tt's Not What You Know, But Who You Knew':
Examining the Relationship Between Behavior Change and AIDS Mortality in Africa." AIDS
Education and Prevention 13:160-74.</mixed-citation>
         </ref>
         <ref id="d918e1158a1310">
            <mixed-citation id="d918e1162" publication-type="other">
Malungo, J.R.S. 2001. "Sexual Cleansing (Kusalazya) and Levirate Marriage (Kunjilila mung'andd)
in the Era of AIDS: Changes in Perceptions and Practices in Zambia." Social Science and Medi-
cine 53:371-82.</mixed-citation>
         </ref>
         <ref id="d918e1175a1310">
            <mixed-citation id="d918e1179" publication-type="other">
Mitchell, C.J. 1956. The Yao Village; a Study in the Social Structure of a Nyasaland Tribe. Manches-
ter: Manchester University Press.</mixed-citation>
         </ref>
         <ref id="d918e1189a1310">
            <mixed-citation id="d918e1193" publication-type="other">
Mukiza-Gapere, J. and J.P. Ntozi. 1995. "Impact of AIDS on Marriage Patterns, Customs and Prac-
tices in Uganda." Health Transition Review 5(Suppl.):201-208.</mixed-citation>
         </ref>
         <ref id="d918e1204a1310">
            <mixed-citation id="d918e1208" publication-type="other">
Murray, J.E. 2000. "Marital Protection and Marital Selection: Evidence From a Historical-Prospective
Sample of American Men." Demography 37:511-21.</mixed-citation>
         </ref>
         <ref id="d918e1218a1310">
            <mixed-citation id="d918e1222" publication-type="other">
Mwaluko, G., M. Urassa, R. Isingo, B. Zaba, and J.T. Boerma. 2003. "Trends in HIV and Sexual
Behaviour in a Longitudinal Study in a Rural Population in Tanzania, 1994-2000." AIDS
17:2645-51.</mixed-citation>
         </ref>
         <ref id="d918e1235a1310">
            <mixed-citation id="d918e1239" publication-type="other">
NSO and ORC Macro. 1994. Malawi Demographic and Health Survey 1992. Zomba, Malawi and
Cal verton, MD: National Statistical Office and ORC Macro.</mixed-citation>
         </ref>
         <ref id="d918e1249a1310">
            <mixed-citation id="d918e1253" publication-type="other">
-. 2001. Malawi Demographic and Health Survey 2000. Zomba, Malawi and Calverton, MD:
National Statistical Office and ORC Macro.</mixed-citation>
         </ref>
         <ref id="d918e1263a1310">
            <mixed-citation id="d918e1267" publication-type="other">
-. 2005. Malawi Demographic and Health Survey 2004. Zomba, Malawi and Calverton, MD:
National Statistical Office and ORC Macro.</mixed-citation>
         </ref>
         <ref id="d918e1277a1310">
            <mixed-citation id="d918e1281" publication-type="other">
Ogbu, J.U. 1978. "African Bridewealth and Women's Status." American Ethnologist 5:241-62.</mixed-citation>
         </ref>
         <ref id="d918e1289a1310">
            <mixed-citation id="d918e1293" publication-type="other">
Orubuloye, O., J.C. Caldwell, and P. Caldwell. 1997. "Men's Sexual Behaviour in Urban and Ru-
ral Southwest Nigeria: Its Cultural, Social and Attitudinal Context." Health Transition Review
7(Suppl.):315-28.</mixed-citation>
         </ref>
         <ref id="d918e1306a1310">
            <mixed-citation id="d918e1310" publication-type="other">
Parker, R. 2001. "Sexuality, Culture and Power in HIV/AIDS Research." Annual Review of Anthro-
pology 30:183-79.</mixed-citation>
         </ref>
         <ref id="d918e1320a1310">
            <mixed-citation id="d918e1324" publication-type="other">
Phiri, K.M. 1983. "Some Changes in the Matrilineal Family System Among the Chewa of Malawi
Since the Nineteenth Century." Journal of African History 24:257-74.</mixed-citation>
         </ref>
         <ref id="d918e1334a1310">
            <mixed-citation id="d918e1338" publication-type="other">
Porter, L., L. Hao, D. Bishai, D. Serwadda, M.J. Wawer, T. Lutalo, and R. Gray. 2004. "HIV Sta-
tus and Union Dissolution in Sub-Saharan Africa: The Case of Rakai, Uganda." Demography
41:465-82.</mixed-citation>
         </ref>
         <ref id="d918e1351a1310">
            <mixed-citation id="d918e1355" publication-type="other">
Porter, K. and B. Zaba. 2004. "The Empirical Evidence for the Impact of HIV on Adult Mortality in
the Developing World: Data From Serological Studies." AIDS 18(Suppl. 2):S9-S17.</mixed-citation>
         </ref>
         <ref id="d918e1365a1310">
            <mixed-citation id="d918e1369" publication-type="other">
Poulin, M. 2007. "Sex, Money and Premarital Partnerships in Southern Malawi." Social Science and
Medicine 65:2383-93.</mixed-citation>
         </ref>
         <ref id="d918e1380a1310">
            <mixed-citation id="d918e1384" publication-type="other">
Reniers, G. 2003. "Divorce and Remarriage in Rural Malawi." Demographic Research, Special col-
lection 1, article 6:175-206. Available online at http://www.demographic-research.0rg/special/l/6/
Sl-6.pdf.</mixed-citation>
         </ref>
         <ref id="d918e1397a1310">
            <mixed-citation id="d918e1401" publication-type="other">
Schatz, E. 2002. Numbers and Narratives: Making Sense of Gender and Context in Rural Malawi.
Unpublished doctoral dissertation. Graduate Group in Demography, University of Pennsylvania,
Philadelphia.</mixed-citation>
         </ref>
         <ref id="d918e1414a1310">
            <mixed-citation id="d918e1418" publication-type="other">
-. 2005. "'Take Your Mat and Go!': Rural Malawian Women's Strategies in the HIV/AIDS
Era." Culture, Health and Sexuality 7:479-92.</mixed-citation>
         </ref>
         <ref id="d918e1428a1310">
            <mixed-citation id="d918e1432" publication-type="other">
Schoepf, B.G. 2001. "International AIDS Research in Anthropology. Taking a Critical Perspective on
the Crisis." Annual Review of Anthropology 30:335-61.</mixed-citation>
         </ref>
         <ref id="d918e1442a1310">
            <mixed-citation id="d918e1446" publication-type="other">
Smith, K.P. and S.C. Watkins. 2005. "Perceptions of Risk and Strategies for Prevention: Responses
to HIV/AIDS in Rural Malawi." Social Science and Medicine 60:649-60.</mixed-citation>
         </ref>
         <ref id="d918e1456a1310">
            <mixed-citation id="d918e1460" publication-type="other">
Stoneburner, R.L. and D. Low-Beer. 2004. "Population-Level HIV Declines and Behavioral Risk
Avoidance in Uganda." Science 304:714-18.</mixed-citation>
         </ref>
         <ref id="d918e1471a1310">
            <mixed-citation id="d918e1475" publication-type="other">
Swidler, A. and S. Watkins. 2005. "Hearsay Ethnography." Presented at the annual meeting of the
Population Association of America, March 31-April 2, Philadelphia, PA.</mixed-citation>
         </ref>
         <ref id="d918e1485a1310">
            <mixed-citation id="d918e1489" publication-type="other">
Trinitapoli, J. 2007. The Role of Religious Organizations in the HIV Crisis of Sub-Saharan Africa.
Unpublished doctoral dissertation. Department of Sociology, University of Texas-Austin.</mixed-citation>
         </ref>
         <ref id="d918e1499a1310">
            <mixed-citation id="d918e1503" publication-type="other">
Ulin, P.R. 1992. "African Women and AIDS: Negotiating Behavioral Change." Social Science and
Medicine 34:63-73.</mixed-citation>
         </ref>
         <ref id="d918e1513a1310">
            <mixed-citation id="d918e1517" publication-type="other">
Umberson, D. 1987. "Family Status and Health Behaviors: Social Control as a Dimension of Social
Integration." Journal of Health and Social Behavior 28:306-19.</mixed-citation>
         </ref>
         <ref id="d918e1527a1310">
            <mixed-citation id="d918e1531" publication-type="other">
UNAIDS. 2004. Report on the Global AIDS Epidemic. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d918e1538a1310">
            <mixed-citation id="d918e1542" publication-type="other">
UNAIDS/WHO. 2004. "Epidemiological Fact Sheet on HIV/AIDS and Sexually Transmitted Infec-
tions: Malawi (2004 Update)." Geneva: UNAIDS/WHO.</mixed-citation>
         </ref>
         <ref id="d918e1553a1310">
            <mixed-citation id="d918e1557" publication-type="other">
UNDP. 2004. "Human Development Report 2004: Cultural Liberty in Today's Diverse World." New
York: United Nations Development Programme.</mixed-citation>
         </ref>
         <ref id="d918e1567a1310">
            <mixed-citation id="d918e1571" publication-type="other">
United Nations. 2007. "World Population Prospects: The 2007 Revision. Population Database." New
York: United Nations, Population Division. Available online at http://esa.un.org/unpp/.</mixed-citation>
         </ref>
         <ref id="d918e1581a1310">
            <mixed-citation id="d918e1585" publication-type="other">
Van de Walle, E. and D. Meekers. 1994. "Marriage Drinks and Kola Nuts." Pp. 57-73 in Nuptiality
in Sub-Saharan Africa: Contemporary Anthropological and Demographic Perspectives, edited by
C. Bledsoe and G. Pison. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d918e1598a1310">
            <mixed-citation id="d918e1602" publication-type="other">
Waite, L.J. 1995. "Does Marriage Matter?" Demography 32:483-507.</mixed-citation>
         </ref>
         <ref id="d918e1609a1310">
            <mixed-citation id="d918e1613" publication-type="other">
Waldron, I., M.E. Hughes, and T.L. Brooks. 1996. "Marriage Protection and Marriage Selection—
Prospective Evidence for Reciprocal Effects of Marital Status and Health." Social Science and
Medicine 43:113-23.</mixed-citation>
         </ref>
         <ref id="d918e1626a1310">
            <mixed-citation id="d918e1630" publication-type="other">
Watkins, S. 2004. "Navigating AIDS in Rural Malawi." Population and Development Review
30:673-705.</mixed-citation>
         </ref>
         <ref id="d918e1641a1310">
            <mixed-citation id="d918e1645" publication-type="other">
Watkins, S., E.M. Zulu, H.-P. Kohler, and J. Behrman, eds. 2003. "Social Interactions and HIV/AIDS
in Rural Africa." Demographic Research, Special collection 1. Available online at http://www
.demographic-research.org/special/.</mixed-citation>
         </ref>
         <ref id="d918e1658a1310">
            <mixed-citation id="d918e1662" publication-type="other">
Wendo, C. 2004. "African Women Denounce Bride Price. Campaigners Claim Payment for Wives
Damages Sexual Health and Contributes to AIDS Spread." Lancet 363:716.</mixed-citation>
         </ref>
         <ref id="d918e1672a1310">
            <mixed-citation id="d918e1676" publication-type="other">
Wyke, S. and G. Ford. 1992. "Competing Explanations for Associations Between Marital Status and
Health." Social Science and Medicine 34:523-32.</mixed-citation>
         </ref>
         <ref id="d918e1686a1310">
            <mixed-citation id="d918e1690" publication-type="other">
Zulu, E.M. 1996. Social and Cultural Factors Affecting Reproductive Behavior in Malawi. Un-
published doctoral dissertation. Graduate Group in Demography, University of Pennsylvania,
Philadelphia.</mixed-citation>
         </ref>
         <ref id="d918e1703a1310">
            <mixed-citation id="d918e1707" publication-type="other">
Zulu, E.M. and G. Chepngeno. 2003. "Spousal Communication About the Risk of Contracting HIV/
AIDS in Rural Malawi." Demographic Research, Special collection 1, article 8:245-77. Available
online at http://www.demographic-research.org/special/1/8/S1-8.pdf.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
