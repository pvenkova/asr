<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26349551</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Masculine sex ratios, population age structure and the potential spread of HIV in China</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Merli</surname>
                  <given-names>M. Giovanna</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Hertog</surname>
                  <given-names>Sara</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Sanford School of Public Policy and Department of Sociology, Duke University.</aff>
            <aff id="af2">
               <label>²</label>United Nations Population Division. The views expressed in this paper do not necessarily reflect those of the United Nations.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2010</year>
            <string-date>JANUARY - JUNE 2010</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">22</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26349548</issue-id>
         <fpage>63</fpage>
         <lpage>94</lpage>
         <permissions>
            <copyright-statement>© 2010 M. Giovanna Merli &amp; Sara Hertog</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26349551"/>
         <abstract xml:lang="eng">
            <p>There is much speculation regarding the contribution of China’s changing demography to the spread of HIV/AIDS. We employ a bio-behavioral macrosimulation model of the heterosexual spread of HIV/AIDS to evaluate the roles that China’s unique demographic conditions -- (1) masculine sex ratios at birth and (2) a population age structure that reflects rapid fertility decline since the 1970’s -- play in altering the market for sexual partners, thereby potentially fueling an increase in behaviors associated with greater risk of HIV infection. We first simulate the relative contributions of the sex ratio at birth and the population age structure to the oversupply of males in the market for sexual partners and show that the sex ratio at birth only aggravates the severe oversupply of males which is primarily a consequence of the population age structure. We then examine the potential consequences of this demographic distortion for the spread of HIV infection and show that, to the extent that males adapt to the dearth of suitable female partners by seeking unprotected sexual contacts with female sex workers, the impact of the oversupply of males in the sexual partnership market on the spread of HIV will be severe.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Ahmed, S., Lutalo, T., Wawer, M., Serwadda, D., Sewankambo, N., Nalugoda, F., Makumbi, F., Wabwire-Mangen, F., Kiwanuka, N., Kigozi, G., Kiddugavu, M., and Gray, R. (2001). HIV incidence and sexually transmitted disease prevalence associated with condom use: A population study in Rakai, Uganda. AIDS 15(16): 2171-2179. doi:10.1097/00002030-200111090-00013.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Ambroziak, J. and Levy, J.A. (1999). Epidemiology, natural history, and pathogenesis of HIV infection. In: Holmes, K.K., Mårdh, P.A., Sparling P., and Wiesner P.J. (eds.). Sexually transmitted diseases. Third edition. New York: McGraw-Hill: 251-258.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Anderson, R. (1992). The transmission dynamics of sexual transmitted diseases: The behavioral component. In: Dyson, T. (ed.). Sex behavior and networking: Anthropological and socio-cultural studies on the transmission of HIV. Liège: IUSSP: 23-47.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Aral, S.O., Over, M., Manhart, L., and Holmes, K.K. (2006). Sexually transmitted infections. In: Jamison, D.T., Breman, J.G., Measham, A.R., Alleyne, G., Claeson, M., Evans, D.B., Jha, P., Mills, A., and Musgrove, P. (eds.). Disease control priorities in developing countries. New York: World Bank and Oxford University Press: 311-330.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Banister, J. (2004). Shortage of girls in China today. Journal of Population Research 21(1): 19-45. doi:10.1007/BF03032209.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Banister, J. and Hill, K. (2004). Mortality in China, 1964-2000. Population Studies 58(1): 55-75. doi:10.1080/0032472032000183753.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Bhrolcháin, M.N. (2001) Flexibility in the marriage market. Population - An English Selection 13(2): 9-47.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Bracher, M., Santow, G., and Watkins, S. (2003). Moving and marrying: Modeling HIV infection among newly-weds in Malawi. Demographic Research SC1(7): 207-246. doi:10.4054/DemRes.2003.S1.7.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Buvé, A., Lagarde E., Caraël, M., Rutenberg, N., Ferry, B., Glynn, J.R., Laourou, M., Akam, E., Chege, J., and Sukwa, T. (2001). Interpreting sexual behaviour data: validity issues in the multicentre study on factors determining the differential spread of HIV in four African cities. AIDS 15(Suppl 4): 117-126. doi:10.1097/00002030-200108004-00013.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Cai, Y. and Lavely, W. (2003). China’s missing girls: Numerical estimates and effects on population growth. The China Review 3(2): 13-29.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Chau, P.H., Yip, P.S.F., and Cui, J. (2003). Reconstructing the incidence of human immunodeficiency virus (HIV) in Hong Kong by using data from HIV positive tests and diagnoses of acquired immune deficiency syndrome. Applied Statistics 52 (2): 237–248. doi:10.1111/1467-9876.00401.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Churat, R., Manglani, M., Sharma, R., and Shah, N.K. (2000). Clinical spectrum of HIV infection. Indian Pediatrics 37: 831-836.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">CNN World News. (2007). China. Too many boys? January 31, 2007.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Coale, A. and Banister, J. (1994). Five decades of missing girls in China. Demography 31(3): 459-479. doi:10.2307/2061752.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Courtwright, D. (1991). Disease, death and disorder on the American frontier. Journal of the History of Medicine and Allied Sciences 46(4): 457-492. doi:10.1093/jhmas/46.4.457.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Curtis, S.L. and Sutherland, E.G. (2004). Measuring sexual behaviour in the era of HIV/AIDS: The experience of Demographic and Health Surveys and similar enquiries. Sexually Transmitted Infections 80(Suppl II): ii22-ii27. doi:10.1136/sti.2004.011650.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Ding, Y., Detels, R., Zhao, Z., Zhu, Y., Zhu, G., Zhang, B., Shen, T., and Xue, X. (2005). HIV infection and sexually transmitted diseases in female commercial sex workers in China. Journal of Acquired Immune Deficiency Syndrome 38(3): 314-319.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Downs, A.M. and De Vincenzi, I. (1996). Probability of heterosexual transmission of HIV: Relationship to the number of unprotected sexual contacts. Journal of Acquired Immune Deficiency Syndrome and Human Retrovirology 11: 388-395.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Edlund, L. and Korn, E. (2002). A theory of prostitution. The Journal of Political Economy 110(1): 181-214. doi:10.1086/324390.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Farrer, J. (2002). Opening up: Youth sex culture and market reform in Shanghai. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Fleming, D.T. and Wasserheit, J.N. (1999). From epidemiological synergy to public health policy and practice: The contribution of other sexually transmitted diseases to sexual transmission of HIV infection. Sexually Transmitted Infections 75(1): 3-17. doi:10.1136/sti.75.1.3.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Garnett, G. and Anderson, R.M. (1993). Factors controlling the spread of HIV in heterosexual communities in developing countries: Patterns of mixing between different age and sexual activity classes. Philosophical Transactions: Biological Sciences 342(1300): 137-159. doi:10.1098/rstb.1993.0143.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Gill, B., Huang, Y., and Lu, X. (2007). Demography of HIV/AIDS in China. Washington, DC: Center for Strategic and International Studies.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Goldman, N., Westoff, C.F., and Hammerslough, C. (1984). Demography of the marriage market in the United States. Population Index 50(1): 5-25. doi:10.2307/2736903.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Goodkind, D. (2004). China’s missing children: The 2000 census underreporting surprise. Population Studies 58(3):281-295.doi:10.1080/0032472042000272348.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Goodkind, D. (2006). Marriage squeeze in China: Historical legacies, surprising findings. Paper presented at the 2006 Annual Meeting of the Population Association of America, Los Angeles, March 30-April 1, 2006.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Goodkind, D. (Forthcoming). Child underreporting, fertility, and sex ratio imbalance in China. Demography.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Goodkind, D. and West, L. (2007). China’s sex ratio at birth: Reported ratios, actual ratios, and expected trends [unpublished manuscript]. Washington, DC: International Programs Center U.S. Census Bureau.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Gu, B. and Roy, K. (1995). Sex ratio at birth in China, with reference to other areas in East Asia: What we know. Asia-Pacific Population Journal 10: 17–42.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Hershatter, G. (1997). Dangerous Pleasures. Berkeley, CA: University of California Press.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Hertog, S. (2007). Heterosexual behavior patterns and the spread of HIV/AIDS: The interacting effects of rate of partner change and sexual mixing. Sexually Transmitted Diseases 34(10): 820-828. doi:10.1097/OLQ.0b013e31805ba84c.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Hesketh, T. and Zhu, W. (2006). Abnormal sex ratios in human populations: Causes and consequences. Proceedings of the National Academy of Sciences 103(36): 13271-13275. doi:10.1073/pnas.0602203103.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Hong, Y. and Li, X. (2008). Behavioral studies of female sex workers in China: A literature review and recommendation for future research. AIDS and Behavior 12(4): 623-636. doi:10.1007/s10461-007-9287-7.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Horizon Market Research and Futures Group Europe. (2002). 2001 behavioural surveillance survey in Yunnan and Sichuan: Sex workers report. Beijing: Horizon Market Research.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Huang, Y., Henderson, G.E., Pan, S., and Cohen, M.S. (2004). HIV/AIDS risk among brothel-based female sex workers in China: Assessing the terms, content, and knowledge of sex work. Sexually Transmitted Diseases 31(11): 695–700. doi:10.1097/01.olq.0000143107.06988.ea.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Hudson, V.M. and den Boer, A.M. (2004). Bare branches: The security implications of Asia’s surplus male population. Boston: MIT Press.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Kerr, C. (2005). Sexual transmission propels China’s HIV epidemic. The Lancet Infectious Diseases 5(8): 474. doi:10.1016/S1473-3099(05)70180-8.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Leclerc, P.M. and Garenne, M. (2007). Inconsistencies in age profile of HIV prevalence: A dynamic model applied to Zambia. Demographic Research 16(5): 121-140. doi:10.4054/DemRes.2007.16.5.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Li, X., Fang, X., Lin, D., Mao, R., Wang, J., Cottrell, L., Harris, C., and Stanton, B. (2004). HIV/STI risk behaviors and perceptions among rural-to-urban migrants in China. AIDS Education and Prevention 16(6): 538-556. doi:10.1521/aeap.16.6.538.53787.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Liang, Z. (2007) Personal communication via e-mail, July 17, 2007.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Liang, Z. and Ma, Z. (2004). China’s floating population: New evidence from the 2000 Census. Population and Development Review 30(3): 467-488. doi:10.1111/j.1728-4457.2004.00024.x.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Lim, L.L. (ed.) (1998). The Sex Sector. The Economic and Social Basis of Prostitution in Southeast Asia. Geneva: International Labour Office.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Lin, L., Jia, M., Ma, Y., Yang, L., Chen, Z., Ho, D.D., Jiang, Y., and Zhang, L. (2008). The changing face of HIV in China. Nature 455: 609-611. doi:10.1038/455609a.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Liu, D., Liu, D., Ng, M.L., Zhou, L.P., and Haeberle, E.J. (1997). Sexual Behaviour in Modern China: Report on the Nationwide Survey of 20,000 Men and Women. New York: Continuum 1997.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Mastro, T.D. and De Vincenzi, I. (1996). Probabilities of sexual HIV-1 transmission. AIDS 10(Suppl. A): 75-82.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Mensch, B., Hewett, P.C., and Erulkar, A.S. (2003). The reporting of sensitive behaviour by adolescents: A methodological experiment in Kenya. Demography 40(2): 247-268. doi:10.1353/dem.2003.0017.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Merli, M.G. (1998). Underreporting of births and infant deaths in rural China: Evidence from field research in one county of Northern China. The China Quarterly 155: 637-655. doi:10.1017/S0305741000050025.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Merli, M.G. and Raftery, A. (2000). Are births underreported in rural China? Manipulation of statistical records in response to China’s population policies. Demography 37(1): 109-126. doi:10.2307/2648100.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Merli, M.G., Hertog, S., Wang, B., and Li, J. (2006). Modeling the spread of HIV/AIDS in China: The role of sexual transmission. Population Studies 60(1): 1-22. doi:10.1080/00324720500436060.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Merli, M.G., Qian, Z., and Smith, H.L. (2004). Adaptations of a political bureaucracy to economic and institutional change under socialism: The Chinese state family planning system. Politics and Society 32(2): 231-256.doi:10.1177/0032329204263073.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Merli, M.G., DeWaard, J., Tian, F., and Hertog, S. (2009). Migration and gender in China's HIV/AIDS epidemic. In: Tucker, J.D. and Poston, D.L. (eds.). Gender policy and HIV in China: Catalyzing policy change. New York: Springer: 27-53. doi:10.1007/978-1-4020-9900-7_3.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Merli, M.G., Neely, W.W., Gao, E., Tu, X., Chen, X., Tian, F., Gu, W., and Yang, Y. (2008). Sampling female sex workers in Shanghai using Respondent Driven Sampling. Paper presented at the Symposium on Health, Equity and Development, Shanghai Academy of Social Sciences, Shanghai, December 19-20, 2008.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">MOH, UNAIDS and WHO. People’s Republic of China Ministry of Health, Joint UN Program on HIV/AIDS and World Health Organization (WHO). (2006). 2005 update on the HIV/AIDS epidemic and response in China. Beijing: National Center for AIDS/STD Prevention and Control.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Morris, M. (1997). Sexual networks and HIV. AIDS 1997 11(Suppl. A): 209-216.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Over, M. (1998). The effects of societal variables on urban rates of HIV infection in Developing Countries: An exploratory analysis. In: Ainsworth, M., Fransen, L., and Over, M. (eds.) Confronting AIDS: Evidence from the Developing World. Luxembourg: European Commission: 39-51.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Palloni, A. (1996). Demography of HIV/AIDS. Population Index 62(4): 601-652. doi:10.2307/3646371.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Palloni, A. and Lamas, L. (1991). The Palloni approach: A duration-dependent model of the spread of HIV/AIDS in Africa. In: The AIDS Epidemic and its Demographic Consequences. Proceedings of the United Nations/World Health Organization Workshop on Modelling the Demographic Impact of the AIDS Epidemic in Pattern II Countries: Progress to Date and Policies for the Future. New York, December 13-15, 1989. New York, New York, United Nations, Department of International Economic and Social Affairs (ST/ESA/SER.A/119): 109-118.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Parish, W., Laumann, E.O., Cohen, M.S., Pan, S., Zheng, H., Hoffman, I., Wang, T., and Ng, K.H. (2003). Population-based study of Clamydial infection in China: A hidden epidemic. JAMA 289(10): 1265-1273. doi:10.1001/jama.289.10.1265.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Parish, W.L., Mojola, S., and Laumann, E.O. (2007). Sexual behavior in China: Trends and comparisons. Population and Development Review 33(4): 729-756. doi:10.1111/j.1728-4457.2007.00195.x.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">PCO (Population Census Office under the State Council and Department of Population, Social, Science and Technology Statistics, National Bureau of Statistics of the People’s Republic of China) (2002). Tabulations of the 2000 census of China. Beijing: China Statistics Press.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Pilcher, C.D., Tien, X.C., Eron, J.J., Vernazza, P.L., Leu, S.Y., Stewart, P.W., Goh, L.E., and Cohen, M.S. (2004). Brief but efficient: Acute HIV infection and the sexual transmission of HIV. The Journal of Infectious Diseases 189(10): 1785-1792. doi:10.1086/386333.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Retherford, R.D., Choe, M.K. Chen J., Li X., and Cui H. (2005). How far has fertility in China really declined?. Population and Development Review 31(1): 57-84. doi:10.1111/j.1728-4457.2005.00052.x.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Rojanapithayakorn, W. and Hanenberg, R. (1996). The 100% condom program in Thailand. AIDS 10(1): 1-7. doi:10.1097/00002030-199601000-00001.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Saracco, A., Musicco, M., Nicolosi, A., Angarano, G., Arici, C., Gavazzeni, G., Costigliola, P., Gafa, S., Gervasoni, C., Luzzati, R., Peccinino, F., Puppo, F., Salassa, B., Sinicco, A., Stellini, R., Terelli, U., Turbessi, G., Vigevani, G.M., Visco, G., Zerboni, R., and Lazzarin, A. (1993). Male-to-female sexual transmission of HIV: longitudinal study of 343 steady partners of infected men. Journal of Acquired Immune Deficiency Syndrome 6(5): 497-502.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Schafer, S. (2003). Not just another pretty face. Newsweek October 13, 2003.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Scharping, T. (2005). Chinese fertility trends 1979-2000: A comparative analysis of birth numbers and school data. Population Research 29(4): 2-15 (in Chinese).</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Scharping, T. (2007). The politics of numbers: Fertility statistics in recent decades. In: Zhao, Z. and Guo, F. (eds.) Transition and challenge: China’s population at the beginning of the 21st century. London: Oxford University Press: 34-52.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Seidlin, M., Vogler, M., Lee, E., Lee, Y.S., and Dubin, N. (1993). Heterosexual transmission of HIV in a cohort of couples in New York City. AIDS 7(9): 1247-54. doi:10.1097/00002030-199309000-00015.</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Sigley, G. and Jeffreys, E. (1999). On ‘sex’ and ‘sexuality’ in China: A conversation with Pan Suiming. Bulletin of Concerned Asian Scholars 31(1): 50-58.</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">State Council AIDS Working Committee Office, UN Theme Group on AIDS in China. (2007). A joint assessment of HIV/AIDS prevention, treatment and care in China. Beijing.</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Thompson, R. (1974). Seventeenth-century English and colonial sex ratios: A postscript. Population Studies 28(1): 153-165. doi:10.2307/2173799.</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Tucker, J.D., Henderson, G.E., Wang, T.F., Huang, Y.Y., Parish, W., Pan, S.M., Chen, X.S., and Cohen, M.S. (2005). Surplus men, sex work, and the spread of HIV in China. AIDS 19(6): 539-547. doi:10.1097/01.aids.0000163929.84154.87.</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Tuljapurkar, S., Li, N., and Feldman, M. (1995). High sex ratios in China’s future. Science 267(5199): 874-876. doi:10.1126/science.7846529.</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">UNAIDS. (2004). Thailand: Epidemiological Fact Sheets. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">UNAIDS. (2008). China: Epidemiological Fact Sheets. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="ref76">
            <mixed-citation publication-type="other">United Nations. (2009). World Population Prospects 2008. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="ref77">
            <mixed-citation publication-type="other">van den Hoek, A., Fu, Y., Dukers, N.H.T.M., Chen, Z., Feng, J., Zhang, L., and Zhang, X. (2001). High prevalence of syphilis and other sexually transmitted diseases among sex workers in China: Potential for fast spread of HIV. AIDS 15(6): 753-759. doi:10.1097/00002030-200104130-00011.</mixed-citation>
         </ref>
         <ref id="ref78">
            <mixed-citation publication-type="other">Wang, B., Li, X., McGuire, J., Kamali, V., Fang, X., and Stanton, B. (2009). Understanding the dynamics of condom use among female sex workers in China. Sexually Transmitted Diseases 36(3): 134-140. doi:10.1097/OLQ.0b013e318191721a.</mixed-citation>
         </ref>
         <ref id="ref79">
            <mixed-citation publication-type="other">Wu, Z., Rou, K., and Cui, H. (2004). The HIV/AIDS epidemic in China: History, current strategies and future challenges. AIDS Education and Prevention 16(Suppl A): 7-17. doi:10.1521/aeap.16.3.5.7.35521.</mixed-citation>
         </ref>
         <ref id="ref80">
            <mixed-citation publication-type="other">Xia, G. and Yang, X. (2005). Risky sexual behaviour among female entertainment workers in China: Implications for HIV/STD prevention intervention. AIDS Education and Prevention 17(2): 143-156. doi:10.1521/aeap.17.3.143.62904.</mixed-citation>
         </ref>
         <ref id="ref81">
            <mixed-citation publication-type="other">Yang, X. (2005). Does where we live matter? Community characteristics and HIV/STD prevalence in southwestern China. International Journal of STD and AIDS 16(1): 31-37. doi:10.1258/0956462052932610.</mixed-citation>
         </ref>
         <ref id="ref82">
            <mixed-citation publication-type="other">Yang, X., Derlega, V.J., and Luo, H. (2005). Migration, behavior change, and HIV/STD risks in China. Paper presented at the IUSSP XXV International Population Conference, Tours, France, July 18-23, 2005.</mixed-citation>
         </ref>
         <ref id="ref83">
            <mixed-citation publication-type="other">Yuan, J., Xu, Y., Jiang, T., Wang, M., Li, Y., Liu, K., Qu, S., Bai, Y., Wang, L., Bollinger, L., Walker, N., He, J., and Ionita, G. (2002). The Socioeconomic Impact of HIV/AIDS in China [electronic resource]. http://www.51condom.com/english/resources/Ourreports/SocEc%20Impact%20study.pdf.</mixed-citation>
         </ref>
         <ref id="ref84">
            <mixed-citation publication-type="other">Zeng, Y., Tu, P., Gu, B., Xu, Y., Li B., and Li, Y. (1993). Causes and implications of the recent increase in the reported sex ratio at birth in China. Population and Development Review 19(2): 283-302. doi:10.2307/2938438.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
