<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt13x1n5s</book-id>
      <subj-group>
         <subject content-type="call-number">HM545.G56 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Globalization</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Globalization</subject>
         <subj-group>
            <subject content-type="lcsh">Health aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Urban anthropology</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Demographic anthropology</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Anthropology</subject>
         <subject>Population Studies</subject>
      </subj-group>
      <book-title-group>
         <book-title>Globalization</book-title>
         <subtitle>The Crucial Phase</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>EDITED BY</role>
            <name name-style="western">
               <surname>Spooner</surname>
               <given-names>Brian</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>04</day>
         <month>03</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9781934536780</isbn>
      <isbn content-type="epub">9781934536797</isbn>
      <isbn content-type="epub">1934536792</isbn>
      <publisher>
         <publisher-name>University of Pennsylvania Press, Inc.</publisher-name>
         <publisher-loc>Philadelphia</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>University of Pennsylvania Museum of Archaeology and Anthropology</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt13x1n5s"/>
      <abstract abstract-type="short">
         <p>Throughout human history, the rate of world population growth overall has been outpaced by the rate of urban population growth. Right now, more the half the world's population lives in cities, and that proportion will only increase in the next fifty years. Rapid urban growth accelerates the exchange of ideas, the expansion of social networks, and the diversity of human interactions that accompany globalization. The present century is therefore the crucial phase, when the world's increasing interconnectedness may give rise to innovation and collaboration or intensify conflict and environmental disaster.</p>
         <p>Bringing together scholars of anthropology and social science as well as law and medicine,<italic>Globalization: The Crucial Phase</italic>presents a holistic and comprehensive understanding of the way the world is changing. The contributors reveal the changing scale of social, economic, and financial diversity, examine the impact of globalization on the environment, health, and nutrition; and consider the initiatives to address the social problems and opportunities that arise from global migration. Collectively, these diverse interdisciplinary perspectives provide an introduction to vital research and policy initiatives in a period that will bring great challenges but also great potential.</p>
         <p>Contributors: Nancy Biller, Christina Catanese, Robert J. Collins, Megan Doherty, Zhengxia Dou, Richard J. Estes, James Ferguson, David Galligan, Mauro Guilln, Cameron Hu, John D. Keenan, Alan Kelly, Janet M. Monge, Marjorie Muecke, Neal Nathanson, Sarah Paoletti, Adriana Petryna, Alan Ruby, Theodore G. Schurr, Brian Spooner, Joseph S. Sun, Zhiguo Wu, Huiquan Zhou.</p>
      </abstract>
      <counts>
         <page-count count="392"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>I</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>V</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.3</book-part-id>
                  <title-group>
                     <title>Figures</title>
                  </title-group>
                  <fpage>VII</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.4</book-part-id>
                  <title-group>
                     <title>Tables</title>
                  </title-group>
                  <fpage>X</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.5</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>XI</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.6</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>XV</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Globalization via World Urbanization:</title>
                     <subtitle>The Crucial Phase</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Spooner</surname>
                           <given-names>Brian</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Globalization is on everyone’s lips, but poorly grasped, and inadequately explained. Like many other terms that are common in everyday conversation, such as democracy or capitalism, its currency trumps our ability to give it an exact meaning. Since it was not coined as a scientific term, we cannot restrict its use to the objectives of social scientists. Definitions in the literature vary with the special interests of each writer. But it flourishes in general usage because it captures the accelerating rate of change we see all around us, much of which extends our day-to-day experience beyond the local and national</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Development Trends Among the World’s Socially Least Developed Countries (SLDCs):</title>
                     <subtitle>Reasons for Guarded Optimism</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Estes</surname>
                           <given-names>Richard J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>23</fpage>
                  <abstract>
                     <p>Following decades of steady social decline many of the world’s<italic>socially least developed countries</italic>(hereafter SLDCs) are beginning to show evidence of significant progress in meeting at least the basic material and social needs of their growing populations (AfDB 2011, 2013; Collier 2007; ESCAP 2012; UNDP 2011; UNICEF 2012).¹ This progress occurred during a period of a worldwide economic recession (2007–2009) and, hence, at a time when most development specialists expected that the social, political, and economic conditions of the SLDCs would worsen rather than improve. This expectation was well-founded given the decades-long declines in development that took place</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Human Genetic Diversity in a Global Context</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Schurr</surname>
                           <given-names>Theodore G.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>When speaking about globalization in a social science context, one immediately thinks of the circulation of capital, culture, and ideas around the world. This process is leading to changes in social forms, cultural modes, and political and economic mechanisms across the globe. Such changes are linked to the increasing urbanization of the world’s population, which, in turn, brings more people into contact and promotes interactions between them. The range of experiences resulting from this exposure to new people and ideas presents real challenges for understanding events that are happening around us and the meaning of these changes.</p>
                     <p>Globalization today can</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Global Financial and Economic Crisis:</title>
                     <subtitle>A Drama in Three Acts</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Guillén</surname>
                           <given-names>Mauro F.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Suárez</surname>
                           <given-names>Sandra L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>115</fpage>
                  <abstract>
                     <p>The global financial and economic crisis that started in 2007 is a rather unique event. It was triggered by the implosion of the U.S. real estate market, whose effects were magnified by the securitization of mortgages and other financial assets, and by the high degree of leverage of financial companies. The crisis spread very quickly to other developed and developing markets through a variety of channels, including finance, trade, and the breakdown of trust and confidence more generally. As a result of its global reach, the crisis has meant the loss of tens of millions of jobs worldwide, trillions of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>“Where Everything Is Political”:</title>
                     <subtitle>Architecture Against Politics in Global Dubai</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>Cameron Hu</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>The eastern coast of the Arabian Peninsula is in rapid metamorphosis. Since the onset of the new millennium dredging ships, cranes, bulldozers, and excavators have augmented its continuous line with terraforms, artificial islands, and peninsulas of sand harvested from the sea floor. Cast in the shape of flora and geographical figures, the terraforms reproduce their namesakes on a massive scale. Dubai’s Palm Jumeirah, a residential complex in the approximate shape of a palm tree, occupies some 25 square kilometers off the city’s shoreline. The World, a private archipelago configured after a map of the globe, covers nearly 54 square kilometers,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>If There Is Food, We Will Eat:</title>
                     <subtitle>An evolutionary and global perspective on human diet and nutrition</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Monge</surname>
                           <given-names>Janet M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>173</fpage>
                  <abstract>
                     <p>The diversity of diet across evolutionary time and over the expanse of the globe today highlights the range of tolerated and consumed foods that characterize humans as a species. The range, as it is oftentimes quoted, includes the full scope from almost exclusive animal food consumption to diets virtually devoid of any animal sources of nourishment (except for the incidental ingestion of insect residua). This diversity underscores the issues and problems in defining what constitutes an “optimal” diet for all humans. Indeed it appears that there is no optimal diet, although there are dietary guidelines that minimize a host of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Aspects of Animal Production in Global Food Supply</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Galligan</surname>
                           <given-names>David</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ferguson</surname>
                           <given-names>James</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kelly</surname>
                           <given-names>Alan</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <string-name>Zhengxia Dou</string-name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib5" xlink:type="simple">
                        <string-name>Zhiguo Wu</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>195</fpage>
                  <abstract>
                     <p>Population growth, emerging affluence especially in the developing world, changing dietary patterns (fast foods), technological advances, and changing societal concerns on food production are inevitable and are challenging the way we produce food animal products. The increasing global demand for animal products primarily in the developing world, along with rapid urbanization, increasing energy costs, decreasing availability of arable land and water place additional constraints on current and traditional animal production systems. Animal products (milk, meat, eggs), while containing high quality protein and, thus, highly desirable, are resource intensive to produce relative to plant based food. However, there are many complex</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Issues in Global Health</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Biller</surname>
                           <given-names>Nancy</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nathanson</surname>
                           <given-names>Neal</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>211</fpage>
                  <abstract>
                     <p>Global health is a vast territory, with many definitions (Cohen 2006). In contrast to established academic disciplines, global health is best construed as an umbrella that draws upon many disciplines, both in the traditional health sciences but also in the social sciences, and includes fields as disparate as business, ethics, and education. Furthermore, most of the important issues in global health are highly controversial and many of the acknowledged authorities tend to be champions of a specific viewpoint. In other words, buyer beware.</p>
                     <p>In this short overview, we will focus on a few of the salient issues, recognizing that numerous</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Global Oral Health</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Collins</surname>
                           <given-names>Robert J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>227</fpage>
                  <abstract>
                     <p>The traditional dental diseases are dental caries (tooth decay) and periodontal disease (gum disease); however, in the past four decades, maintenance and restoration of “dental” health has been expanded beyond consideration of just the teeth and supporting structures to “oral health” which includes soft and hard lesions of all kinds in the oral cavity and the methods to diagnose, treat and prevent these conditions. Oral health varies in countries around the globe although within any one country, including the U.S., disparities in oral health are invariably associated with poverty, education, and other socio-economic variables.</p>
                     <p>Other conditions of public health concern</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.16</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Cities:</title>
                     <subtitle>Threats and Opportunities for Women’s Health</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Muecke</surname>
                           <given-names>Marjorie</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>235</fpage>
                  <abstract>
                     <p>In this chapter, I will present literature-based rationales for scholarly and applied concern with the health of girls and women in cities worldwide. I will first explicate two key terms: urban/cities, and women’s health/gender disparities in health to indicate the urgency of examining them together from a global perspective. In the final section of the chapter, I will present efforts by University of Pennsylvania School of Nursing faculty to improve women’s health in cities around the world.</p>
                     <p>In 1950, 30% of the world’s human population lived in cities. By 2000, that figure had mushroomed to 47%, and the trajectory of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.17</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Pharmaceuticals and the Competitive Logic of Global Clinical Trials</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Petryna</surname>
                           <given-names>Adriana</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>255</fpage>
                  <abstract>
                     <p>In more than seventy percent of all medical consultations in the United States, doctors will prescribe, continue, or provide a medication to a patient. On average, two drugs or “drug mentions” have been documented per medical visit.¹ Antidepressants are the most highly prescribed drugs, followed by high-blood pressure medicines, anti-cholesterol agents, anti-arthritics, and anti-asthmatics. Our pill-taking life, “mediated by proton-pump inhibitors, serotonin boosters and other drugs that have become permanent additives to many human bloodstreams” (Gorman 2004), is by and large taken for granted. Sustaining this way of life is a complex globalized system of drug testing and drug development.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.18</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Preparing Lawyers for Practice in an Era of Global Urbanization:</title>
                     <subtitle>A Proposal for Transnational Clinical Partnerships</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Paoletti</surname>
                           <given-names>Sarah</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>279</fpage>
                  <abstract>
                     <p>It has become axiomatic that we live in a globalized world, and there are few aspects of life untouched by both globalization and the related phenomenon of world urbanization. That is as true in the practice of law, as it is in any other discipline: lawyers increasingly are called upon to operate within multiple legal systems, on behalf of or in partnership with clients and allies, and in opposition to adversaries from multiple countries and cultures. To be effective, lawyers must be able to navigate the legal and logistical complexities inherent to transnational practice with skill and facility.</p>
                     <p>The legal</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.19</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Towards a Comprehensive Response to Victims of Sex Trafficking</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>Huiquan Zhou</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>319</fpage>
                  <abstract>
                     <p>Trafficking in human beings for economic or sexual purposes is considered a modern day form of slavery. Trafficking may take the form of voluntary participation on the part of adults, which later turns into exploitation (International Labor Office 2008). In the case of children, participation in trafficking may be (1) entirely involuntary (e.g., kidnapping, selling of new-born babies [Vermot-Mangold 2007]), (2) partially voluntary (e.g., if children are convinced by their caregivers that being trafficked for labor or sexual purposes will increase the economic well-being of the family as a whole [Bastia 2005; Taylor 2005]), or (3) voluntary (e.g., a juvenile</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.20</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Globalization and the University:</title>
                     <subtitle>A Relationship Worth Studying?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ruby</surname>
                           <given-names>Alan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>343</fpage>
                  <abstract>
                     <p>The purpose of this chapter is to show that globalization affects universities and that there is a lot about the phenomena of globalization and its interaction with institutions of higher learning that merits study, not least of which is the way those institutions can contribute to and shape globalization. I begin with a definition of globalization and how it affects some of the constituent parts and populations of institutions of higher education. Then I examine those in more detail before going on to suggest three ways in which scholars and researchers might add to our knowledge about globalization. I will</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x1n5s.21</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Academically Based Global Service Learning</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sun</surname>
                           <given-names>Joseph S.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Keenan</surname>
                           <given-names>John D.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Doherty</surname>
                           <given-names>Megan</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Catanese</surname>
                           <given-names>Christina</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>359</fpage>
                  <abstract>
                     <p>Many of today’s students want a practical hands-on experience both cross-culturally and technically as part of their education. Educational institutions want to prepare students for global engagement. Communities around the world are seeking financial and technical assistance to improve their access to adequate health care, improved water and sanitation, and digital technology. An experienced local Non-governmental organization (NGO) wants to see community development projects completed in a manner that will insure long-term sustainability and may not have the finances or the technical staff to complete it on their own.</p>
                     <p>One obvious way to satisfy all of these needs is to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
