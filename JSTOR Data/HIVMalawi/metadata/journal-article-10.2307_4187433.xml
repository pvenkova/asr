<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">africatoday</journal-id>
         <journal-id journal-id-type="jstor">africatoday</journal-id>
         <journal-id journal-id-type="jstor">j101301</journal-id>
         <journal-title-group>
            <journal-title>Africa Today</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Indiana University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00019887</issn>
         <issn pub-type="epub">15271978</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4187433</article-id>
         <title-group>
            <article-title>Contesting Migrancy: The Foreign Labor Debate in Post-1994 South Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Jonathan</given-names>
                  <surname>Crush</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Clarence Tshitereke</string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i389073</issue-id>
         <fpage>49</fpage>
         <lpage>70</lpage>
         <page-range>49-70</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 Africa Today Consultants, Inc.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4187433"/>
         <abstract>
            <p>In the minds of its victims and critics, South African mine migrancy epitomized everything that was worst about capitalism and apartheid. The system and its associated institutions remain very much intact but do not look particularly comfortable in the new political and human rights-based dispensation of postapartheid South Africa. Historically, the mines enjoyed privileged access to foreign labor, a "right" denied by the state to most other employers. The continued exclusion of other employers from accessing similar rights has led, perhaps inevitably, to greatly increased usage of undocumented or "illegal" foreign labor by South African employers in other sectors. Logically, the state has two options: open up the system to all or close it down once and for all. To date, it has done neither, as we show. This paper traces the postapartheid South African policy debate on regional migration and foreign contract labor to demonstrate how the mining industry has headed off any challenge to the status quo. The result is that the prospects for a fundamental transformation of the contract labor system remain as elusive as ever.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d204e130a1310">
            <label>3</label>
            <mixed-citation id="d204e137" publication-type="other">
Nattrass (1999).</mixed-citation>
         </ref>
         <ref id="d204e144a1310">
            <label>4</label>
            <mixed-citation id="d204e151" publication-type="other">
Jeeves and Crush (1997).</mixed-citation>
         </ref>
         <ref id="d204e158a1310">
            <label>9</label>
            <mixed-citation id="d204e165" publication-type="other">
Liebenberg 1998</mixed-citation>
         </ref>
         <ref id="d204e172a1310">
            <label>11</label>
            <mixed-citation id="d204e179" publication-type="other">
Santho and Sejanamane (1991)</mixed-citation>
            <mixed-citation id="d204e185" publication-type="other">
Nattrass (1995)</mixed-citation>
            <mixed-citation id="d204e191" publication-type="other">
Standing, Sender, and Weeks
(1996: 288-329).</mixed-citation>
            <mixed-citation id="d204e201" publication-type="other">
Cooper (1996)</mixed-citation>
            <mixed-citation id="d204e207" publication-type="other">
Keet (1995).</mixed-citation>
         </ref>
         <ref id="d204e215a1310">
            <label>12</label>
            <mixed-citation id="d204e222" publication-type="other">
Crush and Veriava (1998).</mixed-citation>
         </ref>
         <ref id="d204e229a1310">
            <label>13</label>
            <mixed-citation id="d204e236" publication-type="other">
Liebenberg 1997</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References Cited</title>
         <ref id="d204e252a1310">
            <mixed-citation id="d204e256" publication-type="book">
Bernstein, Ann and Myron Weiner, eds. 1999. Migration and Refugee Policies: An Overview. New York:
Pinter.<person-group>
                  <string-name>
                     <surname>Bernstein</surname>
                  </string-name>
               </person-group>
               <source>Migration and Refugee Policies: An Overview</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d204e281a1310">
            <mixed-citation id="d204e285" publication-type="book">
Böhning, Roger. 1996. Employing Foreign Workers. Geneva: International Labor Office.<person-group>
                  <string-name>
                     <surname>Böhning</surname>
                  </string-name>
               </person-group>
               <source>Employing Foreign Workers</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d204e307a1310">
            <mixed-citation id="d204e311" publication-type="other">
Chamber of Mines. 1997a. Chamber Comment on the Draft Green Paper on International Migration.
Johannesburg (www.bullion.org.za/bulza/polcy/migrate.htm).</mixed-citation>
         </ref>
         <ref id="d204e321a1310">
            <mixed-citation id="d204e325" publication-type="other">
—. 1997b. Inter-Government Agreements and Migrant Mineworkers. Johannesburg (www.
bullion.org.za/bulza/policy/migrate.htm).</mixed-citation>
         </ref>
         <ref id="d204e336a1310">
            <mixed-citation id="d204e342" publication-type="book">
Chirwa, Wiseman. 1995. Malawian Migrant Labor and the Politics of HIV/AIDS, 1985 to 1993. In Cross-
ing Boundaries: Mine Migrancy in a Democratic South Africa, edited by Jonathan Crush and
Wilmot James. Cape Town: Idasa.<person-group>
                  <string-name>
                     <surname>Chirwa</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Malawian Migrant Labor and the Politics of HIV/AIDS, 1985 to 1993</comment>
               <source>Crossing Boundaries: Mine Migrancy in a Democratic South Africa</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e374a1310">
            <mixed-citation id="d204e378" publication-type="book">
Cooper, Carole. 1995. South Africa's Policy on Migration. Background paper for the Labour Market
Commission. Johannesburg: Centre for Applied Legal Studies, University of Witwatersrand.<person-group>
                  <string-name>
                     <surname>Cooper</surname>
                  </string-name>
               </person-group>
               <source>Background paper for the Labour Market Commission</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e403a1310">
            <mixed-citation id="d204e407" publication-type="book">
.1996.Background paper for the Labor Market Commission on South Africa's Policy on Migra-
tion. Johannesburg: Centre for Applied Legal Studies, University of Witwatersrand.<person-group>
                  <string-name>
                     <surname>Cooper</surname>
                  </string-name>
               </person-group>
               <source>Background paper for the Labor Market Commission on South Africa's Policy on Migration</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d204e432a1310">
            <mixed-citation id="d204e436" publication-type="other">
COSATU 1997. COSATU Submission on the Draft Green Paper on International Migration to the
Department of Home Affairs. Johannesburg (www.cosatu.org.za/docs/migrat.htm).</mixed-citation>
         </ref>
         <ref id="d204e446a1310">
            <mixed-citation id="d204e450" publication-type="other">
—. 2000. Submission on the White Paper on International Migration to the Department of Home
Affairs. Johannesburg (www.queensu.ca/samp/comments/cosatu.htm).</mixed-citation>
         </ref>
         <ref id="d204e460a1310">
            <mixed-citation id="d204e464" publication-type="book">
Crush, Jonathan. 1995. Cheap Gold: Mine Labor in Southern Africa. In The Cambridge Survey of World
Migration, edited by Robin Cohen. Cambridge: Cambridge University Press:172-77.<person-group>
                  <string-name>
                     <surname>Crush</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cheap Gold: Mine Labor in Southern Africa</comment>
               <source>The Cambridge Survey of World Migration</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e494a1310">
            <mixed-citation id="d204e498" publication-type="journal">
1999. The Discourse and Dimensions of Irregularity in Postapartheid South Africa. Interna-
tional Migration37:125-151.<person-group>
                  <string-name>
                     <surname>Crush</surname>
                  </string-name>
               </person-group>
               <fpage>125</fpage>
               <volume>37</volume>
               <source>International Migration</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d204e530a1310">
            <mixed-citation id="d204e534" publication-type="book">
2000.Migrations Past: An Historical Overview of Cross-Border Movement in Southern
Africa. In On Borders: Perspectives on International Migration in Southern Africa, edited by
David McDonald. New York and Cape Town: St Martin's Press and Southern African Migra-
tion Project.<person-group>
                  <string-name>
                     <surname>Crush</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Migrations Past: An Historical Overview of Cross-Border Movement in Southern Africa</comment>
               <source>On Borders: Perspectives on International Migration in Southern Africa</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d204e569a1310">
            <mixed-citation id="d204e573" publication-type="book">
Crush, Jonathan, Alan Jeeves, and David Yudelman, 1991. South Africa's Labor Empire: A History of
Black Migrancy to the Gold Mines. Boulder: Westview Press.<person-group>
                  <string-name>
                     <surname>Crush</surname>
                  </string-name>
               </person-group>
               <source>South Africa's Labor Empire: A History of Black Migrancy to the Gold Mines</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d204e598a1310">
            <mixed-citation id="d204e602" publication-type="book">
Crush, Jonathan and Wilmot James, eds. 1995. Crossing Boundaries: Mine Migrancy in a Democratic
South Africa. Cape Town and Ottawa: Idasa and IDRC.<person-group>
                  <string-name>
                     <surname>Crush</surname>
                  </string-name>
               </person-group>
               <source>Crossing Boundaries: Mine Migrancy in a Democratic South Africa</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e627a1310">
            <mixed-citation id="d204e631" publication-type="book">
Crush, Jonathan and Faranaaz Veriava, eds. 1998. Transforming South African Migration and Immi-
gration Policy. Papers presented to the Green Paper Task Team on International Migration,
Pretoria.<person-group>
                  <string-name>
                     <surname>Crush</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Transforming South African Migration and Immigration Policy</comment>
               <source>Green Paper Task Team on International Migration, Pretoria</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d204e663a1310">
            <mixed-citation id="d204e667" publication-type="journal">
Crush, Jonathan, Theresa Ulicki, Teke Tseane, and Elizabeth Jansen Van Veuren. 2001. Undermining
Labor: The Social Impact of Sub-Contracting on the South African Gold Mines. Journal of
Southern African Studies27.<object-id pub-id-type="jstor">10.2307/823288</object-id>
            </mixed-citation>
         </ref>
         <ref id="d204e684a1310">
            <mixed-citation id="d204e688" publication-type="journal">
Davies, Robert and Judith Head. 1995.The Future of Mine Migrancy in the Context of Broader Trends
in Migration in Southern Africa. Journal of Southern African Studies21:439-50.<object-id pub-id-type="jstor">10.2307/2637253</object-id>
               <fpage>439</fpage>
            </mixed-citation>
         </ref>
         <ref id="d204e704a1310">
            <mixed-citation id="d204e708" publication-type="journal">
de Vletter, Fion. 1987. Foreign Labor on the South African Gold Mines: New Insights on an Old Prob-
lem. International Labor Review126:199-218.<person-group>
                  <string-name>
                     <surname>de Vletter</surname>
                  </string-name>
               </person-group>
               <fpage>199</fpage>
               <volume>126</volume>
               <source>International Labor Review</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d204e740a1310">
            <mixed-citation id="d204e744" publication-type="book">
.1999.Attitudes to the Miners' Amnesty in Mozambique. In The New South Africans?Immigra-
tion Amnesties and their Aftermath, edited by Jonathan Crush and Vincent Williams. Cape
Town: Idasa.<person-group>
                  <string-name>
                     <surname>de Vletter</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Attitudes to the Miners' Amnesty in Mozambique</comment>
               <source>The New South Africans?Immigration Amnesties and their Aftermath</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d204e776a1310">
            <mixed-citation id="d204e780" publication-type="book">
Green, Thuso and John Gay. 1999. Attitudes to the Miners' Amnesty in Lesotho. In The New South
Africans? Immigration Amnesties and their Aftermath, edited by Jonathan Crush and Vincent
Williams. Cape Town: Idasa.<person-group>
                  <string-name>
                     <surname>Green</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Attitudes to the Miners' Amnesty in Lesotho</comment>
               <source>The New South Africans? Immigration Amnesties and their Aftermath</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d204e812a1310">
            <mixed-citation id="d204e816" publication-type="book">
James, Wilmot. 1992. Our Precious Metal: African Labor in South Africa's Gold Industry, 1970-1990. Cape
Town: David Philip.<person-group>
                  <string-name>
                     <surname>James</surname>
                  </string-name>
               </person-group>
               <source>Our Precious Metal: African Labor in South Africa's Gold Industry, 1970-1990</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d204e841a1310">
            <mixed-citation id="d204e845" publication-type="book">
Jeeves, Alan. 1985. Migrant Labor in South Africa's Mining Economy: The Struggle for the Gold Mines'
Labor Supply. Kingston and Montreal: McGill-Queen's Press.<person-group>
                  <string-name>
                     <surname>Jeeves</surname>
                  </string-name>
               </person-group>
               <source>Migrant Labor in South Africa's Mining Economy: The Struggle for the Gold Mines' Labor Supply</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d204e871a1310">
            <mixed-citation id="d204e875" publication-type="book">
.1995.Migrant Labor and the State under Apartheid, 1948-1999. In The Cambridge Survey of
World Migration, edited by Robin Cohen. Cambridge: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Jeeves</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Migrant Labor and the State under Apartheid, 1948-1999</comment>
               <source>The Cambridge Survey of World Migration</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e904a1310">
            <mixed-citation id="d204e908" publication-type="book">
Jeeves, Alan and Jonathan Crush, eds. 1997. White Farms, Black Labor: The State and Agrarian Change
in Southern Africa, 1910-1950. New York: Heinemann.<person-group>
                  <string-name>
                     <surname>Jeeves</surname>
                  </string-name>
               </person-group>
               <source>White Farms, Black Labor: The State and Agrarian Change in Southern Africa, 1910-1950</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d204e933a1310">
            <mixed-citation id="d204e937" publication-type="book">
Johnstone, Rick. 1976. Class, Race and Gold: A Study of Class Relations and Racial Discrimination in South
Africa.London: Routledge and Kegan Paul.<person-group>
                  <string-name>
                     <surname>Johnstone</surname>
                  </string-name>
               </person-group>
               <source>Class, Race and Gold: A Study of Class Relations and Racial Discrimination in South Africa</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d204e962a1310">
            <mixed-citation id="d204e966" publication-type="journal">
.1989. Rand and Kolyma: Afro-Siberian Hamlet. South African Sociological Review1:1-45.<person-group>
                  <string-name>
                     <surname>Johnstone</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>1</volume>
               <source>South African Sociological Review</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d204e995a1310">
            <mixed-citation id="d204e999" publication-type="book">
Keet, Dot. 1995. The Emergence and Implications of an Integrated Southern African Labor Market.
Background paper for the Labor Market Commission, Centre for Southern African Studies,
University of Western Cape.<person-group>
                  <string-name>
                     <surname>Keet</surname>
                  </string-name>
               </person-group>
               <source>The Emergence and Implications of an Integrated Southern African Labor Market</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1028a1310">
            <mixed-citation id="d204e1032" publication-type="book">
Labor Market Commission (LMC). 1996. Restructuring the South African Labor Market: Report of the
Presidential Commission to Investigate Labor Market Policy. Pretoria: Department of Labor.<person-group>
                  <string-name>
                     <surname>Labor Market Commission (LMC)</surname>
                  </string-name>
               </person-group>
               <source>Restructuring the South African Labor Market: Report of the Presidential Commission to Investigate Labor Market Policy</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1058a1310">
            <mixed-citation id="d204e1062" publication-type="book">
Lang, John. 1986. Bullion Johannesburg: Men, Mines and the Challenge of Conflict. Johannesburg:
Jonathan Ball.<person-group>
                  <string-name>
                     <surname>Lang</surname>
                  </string-name>
               </person-group>
               <source>Bullion Johannesburg: Men, Mines and the Challenge of Conflict</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1087a1310">
            <mixed-citation id="d204e1091" publication-type="book">
Liebenberg, Johann. 1997. Presentation to Session on Regional Integration and Migration. Confer-
ence on the Green Paper on International Migration, Cape Town.<person-group>
                  <string-name>
                     <surname>Liebenberg</surname>
                  </string-name>
               </person-group>
               <source>Presentation to Session on Regional Integration and Migration. Conference on the Green Paper on International Migration, Cape Town</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1116a1310">
            <mixed-citation id="d204e1120" publication-type="book">
.1998. Interview with C. Tshitereke, Public Policy Consultant, Chamber of Mines, 23 October.
Cape Town.<person-group>
                  <string-name>
                     <surname>Liebenberg</surname>
                  </string-name>
               </person-group>
               <source>Public Policy Consultant, Chamber of Mines</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1145a1310">
            <mixed-citation id="d204e1149" publication-type="book">
Lincoln, D. 2000. Southward Migrants in the Far North: Zimbabwean Farmworkers in Northern Prov-
ince. In Borderline Farming: Foreign Migrants in South African Commercial Agriculture. Migra-
tion Policy Series, No. 16. Cape Town and Kingston: Southern African Migration Project.<person-group>
                  <string-name>
                     <surname>Lincoln</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Southward Migrants in the Far North: Zimbabwean Farmworkers in Northern Province</comment>
               <source>Borderline Farming: Foreign Migrants in South African Commercial Agriculture</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1181a1310">
            <mixed-citation id="d204e1185" publication-type="other">
Mandoro, L. 1998. Interview with C. Tshitereke, Labor Commissioner, Government of Lesotho. Maseru.</mixed-citation>
         </ref>
         <ref id="d204e1192a1310">
            <mixed-citation id="d204e1196" publication-type="book">
Mather, Charles and Freddie Mathebula. 2000. "The Farmers Prefer Us": Mozambican Farmwork-
ers in the Mpumalanga Lowveld. In Borderline Farming: Foreign Migrants in South African
Commercial Agriculture. Migration Policy Series, No. 16. Cape Town and Kingston: Idasa and
Southern African Migration Project.<person-group>
                  <string-name>
                     <surname>Mather</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">"The Farmers Prefer Us": Mozambican Farmworkers in the Mpumalanga Lowveld</comment>
               <source>Borderline Farming: Foreign Migrants in South African Commercial Agriculture</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1232a1310">
            <mixed-citation id="d204e1236" publication-type="book">
McDonald, David A., ed. 2000. On Borders: Perspectives on International Migration in Southern Africa.
New York and Cape Town: St Martin's Press and Southern African Migration Project.<person-group>
                  <string-name>
                     <surname>McDonald</surname>
                  </string-name>
               </person-group>
               <source>On Borders: Perspectives on International Migration in Southern Africa</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1261a1310">
            <mixed-citation id="d204e1265" publication-type="book">
Molapo, Matsheliso Palesa. 1995. Job Stress, Health and Perceptions of Migrant Mineworkers. In
Crossing Boundaries: Mine Migrancy in a Democratic South Africa, edited by Jonathan Crush
and Wilmot James. Cape Town: Idasa.<person-group>
                  <string-name>
                     <surname>Molapo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Job Stress, Health and Perceptions of Migrant Mineworkers</comment>
               <source>Crossing Boundaries: Mine Migrancy in a Democratic South Africa</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1297a1310">
            <mixed-citation id="d204e1301" publication-type="other">
Molefe, Molefe. 1998. Interview with C. Tshitereke, NUM Lawyer, 10 October. Johannesburg.</mixed-citation>
         </ref>
         <ref id="d204e1308a1310">
            <mixed-citation id="d204e1312" publication-type="book">
Moodie, Dunbar. 1994. Going for Gold: Men, Mines and Migration. Berkeley: University of California
Press.<person-group>
                  <string-name>
                     <surname>Moodie</surname>
                  </string-name>
               </person-group>
               <source>Going for Gold: Men, Mines and Migration</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1337a1310">
            <mixed-citation id="d204e1341" publication-type="other">
National Union of Mineworkers (NUM). 1995a.MiningandMineralsPolicy.Johannesburg: NUM (www.
num.org.za/news/mineral.htm).</mixed-citation>
         </ref>
         <ref id="d204e1351a1310">
            <mixed-citation id="d204e1355" publication-type="book">
.1995b. Access to Jobs by Non-South African Nationals. Johannesburg: NUM.<person-group>
                  <string-name>
                     <surname>National Union of Mineworkers (NUM)</surname>
                  </string-name>
               </person-group>
               <source>Access to Jobs by Non-South African Nationals</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1378a1310">
            <mixed-citation id="d204e1382" publication-type="book">
.1995c. 1st, 2nd and 3rd Submissions by the National Union of Mineworkers. Submission to the
National Labor Market Commission. Johannesburg: NUM.<person-group>
                  <string-name>
                     <surname>National Union of Mineworkers (NUM)</surname>
                  </string-name>
               </person-group>
               <source>1st, 2nd and 3rd Submissions by the National Union of Mineworkers</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1407a1310">
            <mixed-citation id="d204e1411" publication-type="book">
Nattrass, Nicoli. 1995. South African Gold-mining: The 1990s and Beyond. In Crossing Boundaries:
Mine Migrancy in a Democratic South Africa, edited by Jonathan Crush and Wilmot James.
Cape Town: Idasa.<person-group>
                  <string-name>
                     <surname>Nattrass</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">South African Gold-mining: The 1990s and Beyond</comment>
               <source>Crossing Boundaries: Mine Migrancy in a Democratic South Africa</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1443a1310">
            <mixed-citation id="d204e1447" publication-type="journal">
.1999. The Truth and Reconciliation Commission on Business and Apartheid: A Critical Evalu-
ation. African Affairs98:373-91.<object-id pub-id-type="jstor">10.2307/723525</object-id>
               <fpage>373</fpage>
            </mixed-citation>
         </ref>
         <ref id="d204e1463a1310">
            <mixed-citation id="d204e1467" publication-type="book">
Packard, Randall and David Coetzee. 1995. White Plague, Black Labor Revisited: TB and the Mining
Industry. In Crossing Boundaries: Mine Migrancy in a Democratic South Africa edited by Jona-
than Crush and Wilmot James. Cape Town: Idasa.<person-group>
                  <string-name>
                     <surname>Packard</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">White Plague, Black Labor Revisited: TB and the Mining Industry</comment>
               <source>Crossing Boundaries: Mine Migrancy in a Democratic South Africa</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1499a1310">
            <mixed-citation id="d204e1503" publication-type="other">
Philip, Kate. 1999. Interview with C. Tshitereke, NUM, 4 February. Johannesburg.</mixed-citation>
         </ref>
         <ref id="d204e1510a1310">
            <mixed-citation id="d204e1514" publication-type="other">
Pretorius, Leon. 1998. Controlling the Shadows: Employer Sanctions and Undocumented Immigra-
tion in South Africa. Master's Thesis, University of Western Cape.</mixed-citation>
         </ref>
         <ref id="d204e1525a1310">
            <mixed-citation id="d204e1529" publication-type="book">
Reitzes, Maxine. 1997. Strangers Truer Than Fiction: The Social and Economic Impact of Migrants on the
Johannesburg Inner City. Research Report, No. 60. Johannesburg: Centre for Policy Studies.<person-group>
                  <string-name>
                     <surname>Reitzes</surname>
                  </string-name>
               </person-group>
               <source>Strangers Truer Than Fiction: The Social and Economic Impact of Migrants on the Johannesburg Inner City</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1554a1310">
            <mixed-citation id="d204e1558" publication-type="book">
1998a. Foreign Workers in the South African Construction Industry: A Case Study. Unpub-
lished report. Cape Town: Southern African Migration Project.<person-group>
                  <string-name>
                     <surname>Reitzes</surname>
                  </string-name>
               </person-group>
               <source>Foreign Workers in the South African Construction Industry: A Case Study</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1583a1310">
            <mixed-citation id="d204e1587" publication-type="book">
1998b. The Socio-Economic Impact of Cross-Border Migrants in Gauteng and North-West
Province. Unpublished report. Cape Town: Southern African Migration Project.<person-group>
                  <string-name>
                     <surname>Reitzes</surname>
                  </string-name>
               </person-group>
               <source>The Socio-Economic Impact of Cross-Border Migrants in Gauteng and North-West Province</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1612a1310">
            <mixed-citation id="d204e1616" publication-type="book">
Republic of South Africa. 1997. Draft Green Paper on International Migration. Government Gazette
383(18033), Notice 849 of 1997. Pretoria: Government Printer.<person-group>
                  <string-name>
                     <surname>Republic of South Africa</surname>
                  </string-name>
               </person-group>
               <source>Draft Green Paper on International Migration</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1641a1310">
            <mixed-citation id="d204e1645" publication-type="book">
1999.White Paper on International Migration. Government Gazette 406(19920), Notice 529
of 1999. Pretoria: Government Printer.<person-group>
                  <string-name>
                     <surname>Republic of South Africa</surname>
                  </string-name>
               </person-group>
               <source>White Paper on International Migration</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1670a1310">
            <mixed-citation id="d204e1674" publication-type="book">
Richardson, Peter. 1982. Chinese Mine Labor in the Transvaal. London: Macmillan.<person-group>
                  <string-name>
                     <surname>Richardson</surname>
                  </string-name>
               </person-group>
               <source>Chinese Mine Labor in the Transvaal</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1697a1310">
            <mixed-citation id="d204e1701" publication-type="journal">
Rogerson, Christian. 1999. International Migrants in the South African Construction Industry: The
Case of Johannesburg. Africa Insight29:40-51.<person-group>
                  <string-name>
                     <surname>Rogerson</surname>
                  </string-name>
               </person-group>
               <fpage>40</fpage>
               <volume>29</volume>
               <source>Africa Insight</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1733a1310">
            <mixed-citation id="d204e1737" publication-type="journal">
Rogerson, Christian and Jayne Rogerson. 2000. Dealing in Scarce Skills: Employer Responses to the
Brain Drain in South Africa. Africa InsightAugust 2000: 31-40.<person-group>
                  <string-name>
                     <surname>Rogerson</surname>
                  </string-name>
               </person-group>
               <issue>August</issue>
               <fpage>31</fpage>
               <source>Africa Insight</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1769a1310">
            <mixed-citation id="d204e1773" publication-type="other">
Rowett, Roger. 1998. Interview with C. Tshitereke, Managing Director of TEBA, 2 October. Johan-
nesburg.</mixed-citation>
         </ref>
         <ref id="d204e1783a1310">
            <mixed-citation id="d204e1787" publication-type="other">
SAMP. 2000. The South African White Paper on International Migration: An Analysis and Critique.
SAMP Migration Policy Briefs, No.1. Available at www.queensu.ca/samp/Publications.html
#SAMP.</mixed-citation>
         </ref>
         <ref id="d204e1800a1310">
            <mixed-citation id="d204e1804" publication-type="book">
Santho, Sehoai and Mafa Sejanamane, eds. 1991. Southern Africa after Apartheid: Prospects for the
Inner Periphery in the 1990s. Harare: SAPES Trust.<person-group>
                  <string-name>
                     <surname>Santho</surname>
                  </string-name>
               </person-group>
               <source>Southern Africa after Apartheid: Prospects for the Inner Periphery in the 1990s</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1829a1310">
            <mixed-citation id="d204e1833" publication-type="book">
Seidman,GayandJonnySteinberg. 1995.Gold-mining's Labor Markets: Legacies of the Past, Challenges
of the Future. Labor Studies Research Report, No. 6. Johannesburg: Sociology of Work Unit,
University of Witwatersrand.<person-group>
                  <string-name>
                     <surname>Seidman</surname>
                  </string-name>
               </person-group>
               <source>Gold-mining's Labor Markets: Legacies of the Past, Challenges of the Future</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1863a1310">
            <mixed-citation id="d204e1867" publication-type="book">
Standing, Guy, John Sender, and John Weeks. 1996. Restructuring the Labor Market: The South African
Challenge. Geneva: International Labor Office.<person-group>
                  <string-name>
                     <surname>Standing</surname>
                  </string-name>
               </person-group>
               <source>Restructuring the Labor Market: The South African Challenge</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1892a1310">
            <mixed-citation id="d204e1896" publication-type="other">
Taimo, Pedro. 1998. Interview with C. Tshitereke, Labor Commissioner, Government of Mozambique,
Johannesburg.</mixed-citation>
         </ref>
         <ref id="d204e1906a1310">
            <mixed-citation id="d204e1910" publication-type="other">
Truth and Reconciliation Commission (TRC), Final Report, 29 October 1998, Volume 4, Chapter 2, Sec-
tion 23. (www.polity.org.za/govdocs/commission/1998/trc/4chapt2.htm).</mixed-citation>
         </ref>
         <ref id="d204e1920a1310">
            <mixed-citation id="d204e1924" publication-type="journal">
Ulicki, Theresa and Jonathan Crush. 2000. Gender, Farmwork and Women's Migration from Lesotho
to the New South Africa. Canadian Journal of African Studies34:64-79.<object-id pub-id-type="doi">10.2307/486106</object-id>
               <fpage>64</fpage>
            </mixed-citation>
         </ref>
         <ref id="d204e1940a1310">
            <mixed-citation id="d204e1944" publication-type="book">
Wilson, Francis. 1972. Labor in the South African Gold Mines, 1911-1969. Cambridge: Cambridge Uni-
versity Press.<person-group>
                  <string-name>
                     <surname>Wilson</surname>
                  </string-name>
               </person-group>
               <source>Labor in the South African Gold Mines, 1911-1969</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
