<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">canajafristudrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100115</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Canadian Journal of African Studies / Revue Canadienne des Études Africaines</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Canadian Association of African Studies</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00083968</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23236812</article-id>
         <title-group>
            <article-title>Examining the Interface between HIV/AIDS, Religion and Gender in Sub-Saharan Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sarah A.</given-names>
                  <surname>Pugh</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">44</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23236802</issue-id>
         <fpage>624</fpage>
         <lpage>643</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 Canadian Association of African Studies / l'Association canadienne des études africaines</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23236812"/>
         <abstract>
            <p>Malgré la centralité des questions d'égalité des sexes à la compréhension de la propagation continue du VIH/sida en Afrique subsaharienne, peu d'attention a été accordée à l'intersection entre sexe et religion dans le contexte de la pandémie. La plupart des explorations traitent de sexe et de VIH/sida, de sexe et de religion, ou de VIH/sida et de religion, mais rarement de l'interaction entre les trois. Compte tenu de l'essor de la religion (en particulier de l'islam et de confessions chrétiennes rénovatrices) et de son emprise croissante dans de nombreuses régions de l'Afrique subsaharienne, le rôle potentiel de la religion tant dans la réduction que la propagation du VIH/sida doit faire l'objet d'une attention accrue. Tout en reconnaissant le rôle positif que jouent et peuvent jouer les communautés confessionnelles en matière d'éducation, de prévention, de traitement et de soins et d'appui dans le domaine du VIH/sida, cet article se concentre avant tout sur la manière dont enseignements confessionnels et pratiques religieuses au sein de l'Afrique subsaharienne peuvent, par inadvertance, contribuer à la fois à la propagation générale du VIH/sida et à la vulnérabilité différentielle au virus des femmes et des filles, des hommes et des garçons. Plus précisément, cet article examine la manière dont certains enseignements confessionnels peuvent renforcer les stéréotypes sexuels et la subordination des femmes aux exigences sexuelles masculines, affecter l'accès au préservatif et son utilisation, circonscrire l'efficacité des programmes d'éducation sur le VIH/sida et contribuer à un climat de stigmatisation et de discrimination contre les femmes vivant avec le VIH/sida dans la région en particulier. While issues of gender are critical to an understanding of the continuing spread of HIV/AIDS in sub-Saharan Africa, little attention has been paid to the interface between gender and religion in the context of the pandemic. Most explorations deal with gender and HIV/AIDS, gender and religion, or HIV/AIDS and religion, but seldom the interplay between the three. Given the increasing stronghold and growth of religion (in particular Islam and renewalist Christian faiths) in many parts of sub-Saharan Africa, the potential impact of religion in either curtailing or advancing the spread of HIV/AIDS needs closer attention. While recognizing the positive roles that faith communities can and do play in terms of HIV/AIDS education, prevention, treatment, care and support, this article focuses primarily on how certain religious teachings and practice within sub-Saharan Africa may also inadvertently contribute to both the general spread of HIV/AIDS and the differential vulnerability of women and girls, boys and men to the virus. Specifically, this article examines the ways in which some faith-based teachings may reinforce gendered stereotypes and female subordination to male sexual demands, impact condom accessibility and usage, circumscribe the effectiveness of HIV/AIDS education programs, and contribute to a climate of stigma and discrimination, especially against women living with HIV/AIDS in the region.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Bibliography</title>
         <ref id="d617e114a1310">
            <mixed-citation id="d617e118" publication-type="other">
Adogame, Aie. 2007. "HIV/AIDS Support and African Pentecostalism:
The Case of the Redeemed Christian Church of God (RCCG)."
Journal of Health Psychology 12, no.3: 475-84.</mixed-citation>
         </ref>
         <ref id="d617e131a1310">
            <mixed-citation id="d617e135" publication-type="other">
Beckerleg, Susan. 1995. "'Brown Sugar' or Friday Prayers: Youth Choices
and Community Building in Coastal Kenya." African Affairs
94, no.374: 23-38.</mixed-citation>
         </ref>
         <ref id="d617e148a1310">
            <mixed-citation id="d617e152" publication-type="other">
Caldwell, John C. 2000. "Rethinking the AIDS Epidemic." Population
and Development Review26, no.l: 117-35.</mixed-citation>
         </ref>
         <ref id="d617e162a1310">
            <mixed-citation id="d617e166" publication-type="other">
Commonwealth Secretariat. 2002. Gender Mainstreaming in HIV/AIDS:
Taking a Multisectoral Approach. London.</mixed-citation>
         </ref>
         <ref id="d617e177a1310">
            <mixed-citation id="d617e181" publication-type="other">
Dube, Musa W. and Musimbi Kanyoro, eds. 2004. Grant Me Justice!
HIV/AIDS and Gender Readings of the Bible. Pietermartizburg,
South Africa: Cluster Publications.</mixed-citation>
         </ref>
         <ref id="d617e194a1310">
            <mixed-citation id="d617e198" publication-type="other">
Engelke, Matthew. 2003. "The Book, the Church and the
'Incomprehensible Paradox': Christianity in African History."
Journal of Southern African Studies 29, no.l: 297-306.</mixed-citation>
         </ref>
         <ref id="d617e211a1310">
            <mixed-citation id="d617e215" publication-type="other">
Epstein, Helen. 2007. "God and the Fight Against AIDS." In The
Invisible Cure, edited by Helen Epstein. New York: Douglas &amp;.
Mclntyre, 2007.</mixed-citation>
         </ref>
         <ref id="d617e228a1310">
            <mixed-citation id="d617e232" publication-type="other">
Fiedrich, Marc. 2004. "'I Told Them Not To Love One Another!' Gender,
Christianity and the Role of Adult Education in the Ugandan
Response to HIV/AIDS." Transformation 54: 17-41.</mixed-citation>
         </ref>
         <ref id="d617e245a1310">
            <mixed-citation id="d617e249" publication-type="other">
Garner, Robert C. 2000. "Safe Sects? Dynamic Religion and AIDS in
South Africa." The Journal of Modern African Studies 38, no.l: 41-69.</mixed-citation>
         </ref>
         <ref id="d617e259a1310">
            <mixed-citation id="d617e263" publication-type="other">
Gatrad, A. Rashid and Aziz Skeikh. 2004. "Risk Factors for HtV/AIDS in
Muslim Communities." Diversity in Health and Social Care 1: 65-69.</mixed-citation>
         </ref>
         <ref id="d617e274a1310">
            <mixed-citation id="d617e278" publication-type="other">
Gupta, G.R., Justin O. Parkhill and Jessica Ogden. 2008. "Structural
Approaches to HIV Prevention." The Lance 372, no.9640: 764-65.</mixed-citation>
         </ref>
         <ref id="d617e288a1310">
            <mixed-citation id="d617e292" publication-type="other">
Kaler, Amy. 2004. "The Moral Lens of Population Control: Condoms and
Controversies in Southern Malawi." Studies in Family Planning 35,
no.2: 105-15.</mixed-citation>
         </ref>
         <ref id="d617e305a1310">
            <mixed-citation id="d617e309" publication-type="other">
Kamaara, Eunice. 1999. "Reproductive and Sexual Health Problems of
Adolescent Girls in Kenya: A Challenge to the Church."
Reproductive Health Matters 7, no. 14: 130-33.</mixed-citation>
         </ref>
         <ref id="d617e322a1310">
            <mixed-citation id="d617e326" publication-type="other">
Khan, Salar M., ed. 2005. Islamic Shariah Guidance on AIDS. New
Delhi: IFA Publications.</mixed-citation>
         </ref>
         <ref id="d617e336a1310">
            <mixed-citation id="d617e340" publication-type="other">
Lagarde, Emmanuel et al. 2000. "Religion and Protective Behaviours
toward AIDS in Rural Senegal." AIDS 14: 2027-33.</mixed-citation>
         </ref>
         <ref id="d617e350a1310">
            <mixed-citation id="d617e354" publication-type="other">
Mate, Rekopantswe. 2002. "Wombs as God's Laboratories: Pentecostal
Discourses of Femininity in Zimbabwe." Africa 72, no.4: 549.</mixed-citation>
         </ref>
         <ref id="d617e365a1310">
            <mixed-citation id="d617e369" publication-type="other">
Maxwell, David. 1998. "Delivered from the Spirit of Poverty?:
Pentecostalism, Prosperity and Modernity in Zimbabwe." Journal of
Religion in Africa 28: 350-73.</mixed-citation>
         </ref>
         <ref id="d617e382a1310">
            <mixed-citation id="d617e386" publication-type="other">
—. 1999. "Historicizing Christian Independency: The Southern African
Pentecostal Movement c. 1908-1960." Journal of African History
40, no.2: 243-64.</mixed-citation>
         </ref>
         <ref id="d617e399a1310">
            <mixed-citation id="d617e403" publication-type="other">
—. 2000. ""Catch the Cockerel before Dawn': Pentecostalism and
Politics in Post-Colonial Zimbabwe." Africa 70, no.2: 249-77.</mixed-citation>
         </ref>
         <ref id="d617e413a1310">
            <mixed-citation id="d617e417" publication-type="other">
Müller, Julian and Sunette Pienaar. 2004. "Stories about Care: Women in
a Historically Disadvantaged Community Infected and/or Affected
by HIV/AIDS." HTS Theological Studies 60, no.3: 1029-47.</mixed-citation>
         </ref>
         <ref id="d617e430a1310">
            <mixed-citation id="d617e434" publication-type="other">
de Paoli, M.M., R. Manongi, and K.I. Klepp. 2004. "Factors Influencing
Acceptability of Voluntary Counselling and HIV-Testing among
Pregnant Women in Northern Tanzania." AIDS Care 16, no.4: 411-25.</mixed-citation>
         </ref>
         <ref id="d617e447a1310">
            <mixed-citation id="d617e451" publication-type="other">
Pew Form on Religious and Public Life. 2006. "Spirit and Power: A 10-
Country Survey of Petecostals" &lt;http://pewforum.org/surveys/
pentecostal/&gt;.</mixed-citation>
         </ref>
         <ref id="d617e465a1310">
            <mixed-citation id="d617e469" publication-type="other">
Prince, Bridgette, Sarah Pugh, and Sharon Kleintjies, eds. 2007. Skills-
building for Gender Mainstreaming in HIV/AIDS Research and
Practice: Seminar Proceedings. Cape Town: Human Sciences
Research Council Press.</mixed-citation>
         </ref>
         <ref id="d617e485a1310">
            <mixed-citation id="d617e489" publication-type="other">
Schoffeleers, Matthew. 1999. "The AIDS Pandemic, the Prophet Billy
Chisupe, and the Democratization Process in Malawi." Journal of
Religion in Africa 29, no.4: 406-40.</mixed-citation>
         </ref>
         <ref id="d617e502a1310">
            <mixed-citation id="d617e506" publication-type="other">
Surur, Feiruz and Mirgissa Kaba. 2000. "The Role of Religious Leaders in
HIV/AIDS Prevention, Control, and Patient Care and Support: A Pilot
Project in Jimma Zone." Northeast African Studies 7, no.2: 59-80.</mixed-citation>
         </ref>
         <ref id="d617e519a1310">
            <mixed-citation id="d617e523" publication-type="other">
Watkins, Susan Cotts. 2004. "Navigating the AIDS Epidemic in Rural
Malawi." Population and Development Review30, no.4: 673-705.</mixed-citation>
         </ref>
         <ref id="d617e533a1310">
            <mixed-citation id="d617e537" publication-type="other">
Willms, Dennis, Maria-Innes Arratia and Patrick Makondesa. 2004.
"Malawi Faith Communities Responding to HIV/AIDS: Preliminary
Findings of a Knowledge Translation and Participatory-Action Research
(PAR) Project." African Journal of AIDS Research 3, no.l: 23-32.</mixed-citation>
         </ref>
         <ref id="d617e553a1310">
            <mixed-citation id="d617e557" publication-type="other">
Yamba, C. Bawa. 1997. "Cosmologies in Turmoil: Witchfinding and AIDS
in Chiawa, Zambia." Africa 67, no.2: 200-23.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
