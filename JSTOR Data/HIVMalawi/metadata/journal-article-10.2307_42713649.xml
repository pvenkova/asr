<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">polietra</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009350</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Politique étrangère</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Institut Français des Relations Internationales</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0032342X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19588992</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42713649</article-id>
         <article-categories>
            <subj-group>
               <subject>repères</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Sida, un enjeu global de sécurité</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Stefan</given-names>
                  <surname>Elbe</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dominique</given-names>
                  <surname>David</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">70</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40102042</issue-id>
         <fpage>163</fpage>
         <lpage>175</lpage>
         <permissions>
            <copyright-statement>© Ifri / Armand Colin</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42713649"/>
         <abstract>
            <p>La pandémie de sida est une tragédie de sécurité humaine : touchant la sécurité alimentaire, l'emploi, l'éducation et les services publics, elle a des effets globaux sur la stabilité des États. Les armées sont particulièrement touchées par le virus. Elles peuvent être des vecteurs de propagation, et donc de déstabilisation internationale, modifiant les conditions mêmes des conflits, ou des opérations de maintien de la paix. Mais elles peuvent aussi devenir des acteurs majeurs de la lutte contre la pandémie. This article argues that the global AIDS pandemic is no longer solely a health issue, but also has emerging human, national, and international security dimensions that must be acknowledged by scholars and international policy-makers. In order to substantiate this argument, this article specifically analyses these three dimensions in greater detail, and outlines the broad policy implications that follow from such an analysis. It is important to recognize these security dimensions of the AIDS pandemic, the article concludes, (i) in order to arrive at a more comprehensive understanding of the nature and extent of the contemporary pandemic ; (ii) in order for the level of the international response to become commensurate with the extent of the global challenge posed by the AIDS pandemic ; and (iii) because the security sector, as a high-risk group and vector of the virus, can make a responsible contribution to international efforts to reduce the transmission of HIV/AIDS.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>fre</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d773e146a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d773e153" publication-type="other">
AIDS Epidemic Update, Genève, décembre 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e160a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d773e167" publication-type="other">
Estimations de Onusida, AIDS Epidemie Update, Genève, décembre 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e174a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d773e181" publication-type="other">
A. Hwang, « AIDS over Asia: AIDS Has Arrived in India and in China », The Guardian, 16
janvier 2001 ;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d773e190" publication-type="other">
J. Gittings, « War on Prejudice as China Awakes to HIV Nightmare », The Guardian,
3 novembre 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e200a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d773e207" publication-type="other">
L. Brown et al., Beyond Malthus: Nineteen Dimensions of the Population Challenge, New York,
W.W.Norton, 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e218a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d773e225" publication-type="other">
D. Gordon, remarques devant le United States Institute of Peace, Plague upon Plague: AIDS and
Violent Conflict in Africa, Washington, DC.USIP, « Current Issues Briefing Panel », 8 mai 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e235a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d773e242" publication-type="other">
International Crisis Group (ICG), HIV/AIDS as a Security Issue, Washington, DC/Bruxelles, juin 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e249a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d773e256" publication-type="other">
L. Heinecken, « AIDS: the New Security Frontier », Conflicts Trends, vol. 3, n° 4, 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e263a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d773e270" publication-type="other">
National Intelligence Council, National Intelligence Estimate : the Global Infectious Disease –
Threat and its Implications for the United States, Washington, DC, janvier 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e280a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d773e287" publication-type="other">
L. Heinecken, « Living in Terror: the Looming Security Threat to Southern Africa », African Security
Review, vol. 10, n°4, 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e297a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d773e304" publication-type="other">
Sur tous ces points, voir S. Elbe, Strategic Implications of HIV/AIDS, Oxford, Oxford University
Press, « Adelphi Paper n°357 ».</mixed-citation>
            </p>
         </fn>
         <fn id="d773e315a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d773e322" publication-type="other">
C. Allen, « Ending Endemic Violence: Limits to Conflict Resolution in Africa », Review of African
Political Economy, vol. 26, n°81, septembre 1999.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e332a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d773e339" publication-type="other">
K. Annan, Review of the Problem of HIV/AIDS in All its Aspects, New York, ONU, A/55/779,
16 février 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e349a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d773e356" publication-type="other">
Select Committee on International Development, HIV/AIDS : the Impact on Social and Economic
Development, Londres, House of Commons, 2 mars 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e366a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d773e373" publication-type="other">
National Intelligence Council, Global Trends 2015 : A Dialogue About the Future With Nongovern-
ment Experts, Washington, DC, Central Intelligence Agency, décembre 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e383a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d773e390" publication-type="other">
R. Tenthani, SADC Police Chiefs Warned to Regard AIDS As Security Issue, Dakar, Panafrican
News Agency, 31 juillet 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e400a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d773e407" publication-type="other">
M. Schneider et M. Moodie, The Destabilizing Impacts of HIV/AIDS, Washington, DC, CSIS, mai 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e415a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d773e422" publication-type="other">
Onusida, AIDS as a Security Issue, Genève, Onusida, 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e429a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d773e436" publication-type="other">
U. Kristoffersson, HIV/AIDS as a Human Security Issue, disponible sur &lt;www.unaids.org&gt;.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e443a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d773e450" publication-type="other">
M. Schonteich, « Age and AIDS: South Africa's Crime Time Bomb? », African Security Review, 8,
n°4, 1999.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e460a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d773e467" publication-type="other">
J. S. Kassalow, Why Health Is Important to US Foreign Policy, New York/Washington, DC, Council
on Foreign Relations, avril 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e477a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d773e484" publication-type="other">
J. Large, « Disintegration Conflicts and the Restructuring of the Masculinity », Gender and Deve-
lopment, vol. 5, n°2, juin 1997.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e494a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d773e501" publication-type="other">
A. Whiteside et C. Sunter, AIDS: the Challenge for South Africa, Le Cap (Afrique du Sud), Human,
Rousseau and Tafelberg, 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e512a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d773e519" publication-type="other">
J. Astill, « War inject AIDS into Sierra Leone », The Guardian, 21 mai 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e526a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d773e533" publication-type="other">
Office américain de la comptabilité générale (GAO), UN Peacekeeping : United Nations Face
Challenges in Responding to the Impact of HIV/AIDS on Peacekeeping Operations, Washington, DC,
GAO, décembre 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e546a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d773e553" publication-type="other">
Ch. Beyrer, War in the Blood: Sex, Politics and AIDS in Southeast Asia, Londres, Zed Books, 1998.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e560a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d773e567" publication-type="other">
J. Mendelson Forman et M. Carballo, « A Policy Critique of HIV/AIDS and Demobilisation »,
Conflict, Security and Development, vol. 1, n°2, 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e577a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d773e584" publication-type="other">
A. Adefofalu, « HIV/AIDS as an Occupational Hazard to Soldiers - ECOMOG Experience »,

              Communication au 3
              
              congrès des services médicaux des forces armées et de police d'Afrique,
            
Pretoria (Afrique du Sud), 24-28 octobre 1999.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e600a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d773e607" publication-type="other">
M Carballo et J. Cilloniz, HIV/AIDS and Security, Genève, International Center for Migration and
Health, 2002, p. 21.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e618a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d773e625" publication-type="other">
Ministère britannique de la Défense, The Future Strategic Context for Defence, Londres, Ministère
de la Défense, 7 février 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e635a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d773e642" publication-type="other">
G. Mills, « AIDS and the South Africain Miltary : Timeworn Cliché or Timebomb ? », in M. Lange
(dir.), HIV/AIDS : A Threat to the African renaissance?, Johannesburg (Afrique du Sud), Konrad
Adenauer Stiftung, 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d773e655a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d773e662" publication-type="other">
T. Deen, « UN Focuses on Links Between AIDS and Peacekeeping », Inter Press Service, 17 juillet 2000.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
