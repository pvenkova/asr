<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/elbe14868</book-id>
      <subj-group>
         <subject content-type="call-number">JZ6005.E43 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Security, International</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">National security</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">AIDS (Disease)</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Virus Alert</book-title>
         <subtitle>Security, Governmentality, and the AIDS Pandemic</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>ELBE</surname>
               <given-names>STEFAN</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>21</day>
         <month>07</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="epub">9780231520058</isbn>
      <isbn content-type="epub">0231520050</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>New York</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/elbe14868"/>
      <abstract abstract-type="short">
         <p>Bound up with the human cost of HIV/AIDS is the critical issue of its impact on national and international security, yet attempts to assess the pandemic's complex risk fail to recognize the political dangers of construing the disease as a security threat. The securitization of HIV/AIDS not only affects the discussion of the disease in international policy debates, but also transforms the very nature and function of security within global politics.</p>
         <p>In his analysis of the security implications of HIV/AIDS, Stefan Elbe addresses three concerns: the empirical evidence that justifies framing HIV/AIDS as a security issue, the meaning of the term "security" when used in relation to the disease, and the political consequences of responding to the AIDS pandemic in the language of security. His book exposes the dangers that accompany efforts to manage the global spread of HIV/AIDS through the policy frameworks of national security, human security, and risk management. Beyond developing strategies for mitigating these dangers, Elbe's research reveals that, in construing the AIDS pandemic as a threat, policymakers and international institutions also implicitly seek to integrate current security practices within a particular rationalization of political rule. Elbe identifies this transformation as the "governmentalization" of security and, by drawing on the recently translated work of Michel Foucault, develops a framework for analyzing its key elements and consequences.</p>
      </abstract>
      <counts>
         <page-count count="224"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.3</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>VIRUSES, HEALTH, AND INTERNATIONAL SECURITY</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Viruses are back on the international agenda—and not just as a matter of low politics. Infectious disease actually became the subject of international diplomacy as early as 1851, when delegates of the first International Sanitary Conference gathered in Paris to consider joint responses to the cholera epidemics that had overrun the European continent in the first half of the nineteenth century. During the twentieth century, though, the pertinence of controlling potentially pandemic microbes was gradually overshadowed by the more pressing concerns of avoiding the specter of renewed wars and the ever-present potential for a nuclear confrontation. The twentieth century’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A NOBLE LIE?</title>
                     <subtitle>Examining the Evidence on AIDS and Security</subtitle>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract abstract-type="extract">
                     <p>Is the designation of HIV/AIDS as a security threat justified by the empirical evidence? The answer to this question depends largely upon which security framework is used as a reference point. The international debate on HIV/AIDS and security has been marked not just by recourse to one overarching security framework but by the intense contestation between at least three different security frameworks: human security, national security, and international security. Each of these competing frameworks posits distinct empirical relationships between HIV/AIDS and security and gives rise to quite different political priorities in relation to the AIDS pandemic. This chapter undertakes a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>SECURITY IN THE ERA OF GOVERNMENTALITY:</title>
                     <subtitle>AIDS and the Rise of Health Security</subtitle>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract abstract-type="extract">
                     <p>What kind of transformation in the function of security is the securitization of HIV/AIDS implicitly encouraging? This chapter argues that the instrumental use of security arguments as a strategy for scaling up international AIDS initiatives is a contemporary manifestation of the <italic>governmentalization of security</italic>. Put differently, the securitization of HIV/AIDS is a site in contemporary world politics where practices of security are being absorbed within a broader rationalization of political rule that has been unfolding in the West since the eighteenth century and that was described by Michel Foucault as the era of “governmentality.” During this era of governmentality political</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>NATIONAL SECURITY:</title>
                     <subtitle>Sovereignty, Medicine, and the Securitization of AIDS</subtitle>
                  </title-group>
                  <fpage>86</fpage>
                  <abstract abstract-type="extract">
                     <p>That the securitization of HIV/AIDS draws upon a <italic>sovereign</italic> economy of power can be seen in those arguments claiming that the AIDS pandemic is a threat to <italic>national</italic> security. This is because the national security framework invoked in such arguments is itself a legacy of the much older form of sovereign power; it is a security framework principally concerned with the survival and power of the sovereign and, by modern extension, of the state. When political actors claim that HIV/AIDS is a threat to national security, they are thus implicitly marshaling a sovereign economy of power in response to the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>HUMAN SECURITY:</title>
                     <subtitle>Discipline, Healthy Bodies, and the Global Curing Machine</subtitle>
                  </title-group>
                  <fpage>108</fpage>
                  <abstract abstract-type="extract">
                     <p>The securitization of HIV/AIDS not only draws upon sovereign power, but also mobilizes a <italic>disciplinary</italic> economy of power. This can be seen in arguments claiming that the AIDS pandemic is also a threat to <italic>human</italic> security. Over the past decade many of those who remain uneasy about using the language of national security in relation to HIV/AIDS have nevertheless been quite willing to openly discuss HIV/AIDS as a threat to human security—the seemingly “softer” option for analyzing the AIDS-security nexus. Yet what is this human security framework at its core if not the marrying of a disciplinary economy of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>RISK AND SECURITY:</title>
                     <subtitle>Government, Military Risk Groups, and Population Triage</subtitle>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract abstract-type="extract">
                     <p>The securitization of HIV/AIDS does, finally, also contain newer strategies for governmental management. This can be seen in arguments that portray the armed forces and the United Nations peacekeepers as “risk groups” with respect to HIV/AIDS. The very idea of a “risk group” is a quintessentially governmental notion. It is a category generated by statistically determining the overall distribution of HIV/AIDS within the population. Such a statistical analysis reveals that there are groups within the population who have a higher prevalence of HIV/AIDS compared to the population as a whole—that is, risk groups. Identification of these segments of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>THE POWER OF AIDS:</title>
                     <subtitle>Responding to the Governmentalization of Security</subtitle>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract abstract-type="extract">
                     <p>Tracing the concurrent operation of sovereign, disciplinary, and governmental forms of power in the securitization of HIV/AIDS has unearthed a complex set of political dangers. How should these competing benefits and drawbacks involved in discussing HIV/AIDS as a security issue be evaluated in the end? Moreover, can Michel Foucault’s own cursory reflections on the rise of the era of governmentality help us develop an adequate response to this question? There are at least three possible ways of responding to these complexities. The first (and perhaps easiest) position would be to simply reject the securitization of HIV/AIDS altogether. Such a response</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.11</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>177</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">elbe14868.12</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>201</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
