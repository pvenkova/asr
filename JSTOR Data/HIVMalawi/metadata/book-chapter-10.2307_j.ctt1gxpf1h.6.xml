<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gxpf1h</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Economics</subject>
      </subj-group>
      <book-title-group>
         <book-title>Development Without Aid</book-title>
         <subtitle>The Decline of Development Aid and the Rise of the Diaspora</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>PHILLIPS</surname>
               <given-names>DAVID A.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>04</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9780857286239</isbn>
      <isbn content-type="epub">9780857283023</isbn>
      <publisher>
         <publisher-name>Anthem Press</publisher-name>
         <publisher-loc>LONDON; NEW YORK; DELHI</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>David A. Phillips</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1gxpf1h"/>
      <abstract abstract-type="short">
         <p>Development Without Aid' provides a critique of foreign aid as a resource that is unable to provide the dynamism to propel the poorest countries out of poverty. It examines the rapid growth of the worlds diasporas as an alternative dynamic with potential to supersede foreign aid and drive a reassertion of sovereignty by poor states, especially in Africa, over their own development.</p>
      </abstract>
      <counts>
         <page-count count="234"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.3</book-part-id>
                  <title-group>
                     <title>LIST OF ACRONYMS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.5</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>INTRODUCTION:</title>
                     <subtitle>MOTIVATION AND PERSPECTIVE</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This book is ultimately a personal reflection rather than an academic treatise, and so, while as far as possible remaining objective and supported by evidence, it is in the end as much advocacy as argument, at times taking the risk of verging on the polemical. It aims to open up perspectives as much as analyze facts. It is about the development of poor countries, not the principles of how societies and economies develop but the narrower canvas of development assistance to poor countries, its effectiveness and whether there is a different way to achieve its objectives. It aims, in fact,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.6</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>WHAT IS FOREIGN AID, WHO DOES IT, WHY AND HOW MUCH IS THERE?</title>
                  </title-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>Official development assistance (ODA) is formally defined by the Organisation for Economic Co-operation and Development (OECD) Development Assistance Committee as follows: financial flows, technical assistance and commodities that are (1) designed to promote economic development and welfare as their main objective (excluding aid for military or other non-development purposes); and (2) are provided as either grants or subsidized loans or conversion of past loans to grants, and the costs that donors incur to administer their own programs.³ To qualify as aid the funding from government and government agencies for the economic development of poorer countries has to have a “grant</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.7</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>HOW FAR HAS DEVELOPMENT AID BEEN EFFECTIVE?</title>
                  </title-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>The bare facts about foreign aid give few grounds for expecting much in the way of achievement. The effectiveness of aid has indeed been a continuing and controversial issue for 50 years and an entire literary genre is devoted to it. As mentioned earlier, the first decade of the 2000s saw unprecedented energy aimed at assessing its value. The Paris conference of 2005 singled out improving effectiveness as the principal objective of aid reform, followed up with further declarations in Ghana in 2008 and Busan in December 2011 which have in turn led to extensive evaluation efforts. Major international conferences</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.8</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>WHY HAS DEVELOPMENT AID DONE SO LITTLE?</title>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>A recent study of 6,000 World Bank projects evaluated since the early 1980s found that: “even after accounting for a wide range of micro and macro variables, much of the variation in project performance remains unexplained.”³ The study was able to account for only about 12 percent of the variation in measured project outcomes, which largely reflected as-yet-unmeasured factors at both the country and project levels. The inability to successfully explain the variation in project outcomes is closely related to the inability to explain outcomes per se. This chapter considers the reasons for this inability.</p>
                     <p>Some aid commentators assert a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.9</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>CHANGING THE DYNAMICS OF DEVELOPMENT</title>
                  </title-group>
                  <fpage>103</fpage>
                  <abstract>
                     <p>In Chapter 1, I suggested that what was needed to bring the one billion poorest of the poor on to a growth path was radical change. This statement was not a call for revolution but rather to draw a stark contrast with the incrementalism and low expectations associated with the aid industry over 50 years, so low in some cases that one might well think that there was a hidden agenda, to do nothing but appear to be doing something. In the case of some donor nations that see aid simply as an exercise in political influence this surely applies.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.10</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>“NEW AID”:</title>
                     <subtitle>NEW WAYS TO PROMOTE AND FINANCE DEVELOPMENT?</subtitle>
                  </title-group>
                  <fpage>141</fpage>
                  <abstract>
                     <p>Before moving towards a conclusion we can turn our attention once again to the efforts of the aid donors following the “New Start” endorsed by the Paris and other international conferences in the first decade of the 2000s. Does this New Start in the end at least have the potential to make foreign aid effective?</p>
                     <p>At the start of this book it was suggested that aid might be reclassified into two more categories – alienative and non-alienative. Assistance in general can have negative effects but certain types of assistance tend to be the more negative, especially within the soft category of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.11</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>ANOTHER PATHWAY OUT OF POVERTY?</title>
                  </title-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>The fundamental issue that I have tried to investigate in this book is how far the people of the poorest and most aid-dependent countries can begin to move beyond their need for foreign aid and, thereby, assume or resume sovereignty over their development process. One pathway to this objective is for poor economies to incorporate diaspora resources as a complement to expanding their own. Taking account of this, one plausible solution for development assistance might be a “re-emphasis.” This might mean, for example, retaining the current direction of assistance but shifting a little bit. New Aid could be repackaged to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.12</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>EXIT STRATEGY – REPLACING FOREIGN ASSISTANCE</title>
                  </title-group>
                  <fpage>167</fpage>
                  <abstract>
                     <p>The experience of 50 years of aid shows that the donor countries in collaboration with the recipients must define far more clearly exactly where and how they can add value. They started to do something like that during the first decade of the 2000s at Monterrey, Rome, Paris, Accra and Busan; but after nearly 10 years of New Aid, the achievement has been small and the full implications of important changes in the dynamics of the international order have apparently not yet been absorbed.</p>
                     <p>One key implication of the new dynamic is that aid resources and diaspora resources are not</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.13</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>POSTSCRIPT</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bauer</surname>
                           <given-names>Peter T.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>177</fpage>
                  <abstract>
                     <p>The country of my childhood, now Malawi, has received US$13 billion in aid since independence (US$22 billion in today’s prices), averaging well over 20 percent of its gross national income over the past 20 years. It is being assisted by about 40 official multilateral and bilateral aid agencies and approximately 200 foreign non-government organizations. In an average year it hosts several hundred donor missions. It faces similar or worse problems than it did at independence half a century ago: income levels well below the average for the poorest countries, emphasis on the same (and inappropriate) agricultural products (e.g. tobacco), a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.14</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>181</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxpf1h.15</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>213</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
