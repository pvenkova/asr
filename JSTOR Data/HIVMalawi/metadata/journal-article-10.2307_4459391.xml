<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4459391</article-id>
         <title-group>
            <article-title>Use of Rifabutin in the Treatment of Pulmonary Tuberculosis</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Carlo Grassi</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Vittoria Peona</string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">22</volume>
         <issue-id>i400671</issue-id>
         <fpage>S50</fpage>
         <lpage>S54</lpage>
         <page-range>S50-S54</page-range>
         <permissions>
            <copyright-statement>Copyright 1996 The University of Chicago</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4459391"/>
         <abstract>
            <p> This article reviews recent studies conducted outside the United States assessing the efficacy and safety of rifabutin in the treatment of tuberculosis (TB) in HIV-infected patients, in patients with newly diagnosed TB, and in patients with multidrug-resistant TB. A 6-month pilot study of 50 Ugandan patients with TB associated with HIV infection showed that rifabutin and rifampin were similarly effective with regard to conversion of sputum-smear findings (sputum conversion) and in bringing about clinical and radiologic improvement. Compared with rifampin, rifabutin showed potential for reducing the time to sputum conversion for these patients. Multicenter studies in five countries compared two rifabutin dosages (150 mg/d and 300 mg/d) with rifampin as part of a combination regimen for treatment of newly diagnosed TB in 935 patients. Rifabutin compared favorably with rifampin in sputum conversion; administration of 150 mg/d of rifabutin yielded good results and the fewest adverse effects. The use of rifabutin by 270 patients in five countries who had multidrug-resistant TB (∼90% of isolates tested were resistant to rifampin and isoniazid) was assessed in another study. For the majority of these patients, signs and symptoms diminished; one-third had bacteriologic conversions. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d708e135a1310">
            <label>1</label>
            <mixed-citation id="d708e142" publication-type="journal">
Brudney K, Dobkin J. Resurgent tuberculosis in New York City: human
immunodeficiency virus, homelessness and the decline of tuberculosis
control programs. Am Rev Respir Dis1991; 144:745-9.<person-group>
                  <string-name>
                     <surname>Brudney</surname>
                  </string-name>
               </person-group>
               <fpage>745</fpage>
               <volume>144</volume>
               <source>Am Rev Respir Dis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d708e177a1310">
            <label>2</label>
            <mixed-citation id="d708e184" publication-type="journal">
Di Perri G, Cruciani M, Danzi MC, et al. Nosocomial epidemic of active
tuberculosis among HIV-infected patients. Lancet1989; 2:1502-4.<person-group>
                  <string-name>
                     <surname>Di Perri</surname>
                  </string-name>
               </person-group>
               <fpage>1502</fpage>
               <volume>2</volume>
               <source>Lancet</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d708e216a1310">
            <label>3</label>
            <mixed-citation id="d708e223" publication-type="book">
Klaudt K, ed. TB: a global emergency. WHO report on the TB epidemic
[WHO/TB/94.177]. Geneva: World Health Organization, 1994.<person-group>
                  <string-name>
                     <surname>Klaudt</surname>
                  </string-name>
               </person-group>
               <source>TB: a global emergency</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d708e248a1310">
            <label>4</label>
            <mixed-citation id="d708e255" publication-type="journal">
Barnes PF, Bloch AB, Davidson PT, et al. Tuberculosis in patients with
human immunodeficiency virus infection. N Engl J Med1991;
324:1644-50.<person-group>
                  <string-name>
                     <surname>Barnes</surname>
                  </string-name>
               </person-group>
               <fpage>1644</fpage>
               <volume>324</volume>
               <source>N Engl J Med</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d708e291a1310">
            <label>5</label>
            <mixed-citation id="d708e298" publication-type="journal">
Davey RT Jr. Mycobacterial disease in HIV infection: recent therapeutic
advances. In: Lane HC, moderator. Recent advances in the management
of AIDS-related opportunistic infections. Ann Intern Med1994;
120:945-55.<person-group>
                  <string-name>
                     <surname>Davey</surname>
                  </string-name>
               </person-group>
               <fpage>945</fpage>
               <volume>120</volume>
               <source>Ann Intern Med</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d708e336a1310">
            <label>6</label>
            <mixed-citation id="d708e343" publication-type="journal">
Hamburg MA, Frieden TR. Tuberculosis transmission in the 1990s. N
Engl J Med1994;330:1750-1.<person-group>
                  <string-name>
                     <surname>Hamburg</surname>
                  </string-name>
               </person-group>
               <fpage>1750</fpage>
               <volume>330</volume>
               <source>N Engl J Med</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d708e375a1310">
            <label>7</label>
            <mixed-citation id="d708e382" publication-type="journal">
Bloch AB, Cauthen GM, Onorato IM, et al. Nationwide survey of drug
resistant tuberculosis in the United States. JAMA1994;271:665-71.<person-group>
                  <string-name>
                     <surname>Bloch</surname>
                  </string-name>
               </person-group>
               <fpage>665</fpage>
               <volume>271</volume>
               <source>JAMA</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d708e414a1310">
            <label>8</label>
            <mixed-citation id="d708e421" publication-type="journal">
Davidson PT, Le HQ. Drug treatment of tuberculosis- 1992. Drugs
1992;43:651-73.<person-group>
                  <string-name>
                     <surname>Davidson</surname>
                  </string-name>
               </person-group>
               <fpage>651</fpage>
               <volume>43</volume>
               <source>Drugs</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d708e453a1310">
            <label>9</label>
            <mixed-citation id="d708e460" publication-type="journal">
Combs DL, O'Brien RJ, Geter LJ. USPHS tuberculosis short course che-
motherapy trial 21: effectiveness, toxicity and acceptability. The report
of the final results. Ann Intern Med1990; 112:397-406.<person-group>
                  <string-name>
                     <surname>Combs</surname>
                  </string-name>
               </person-group>
               <fpage>397</fpage>
               <volume>112</volume>
               <source>Ann Intern Med</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d708e495a1310">
            <label>10</label>
            <mixed-citation id="d708e502" publication-type="journal">
Hong Kong Chest Service/British Medical Research Council. Controlled
trial of 2, 4, and 6 months of pyrazinamide in 6-month-three-times-
weekly regimens for smear-positive pulmonary tuberculosis, including
an assessment of a combined preparation of isoniazid, rifampicin and
pyrazinamide. Am Rev Respir Dis1991; 143:700-6.<person-group>
                  <string-name>
                     <surname>Hong Kong Chest Service/British Medical Research Council</surname>
                  </string-name>
               </person-group>
               <fpage>700</fpage>
               <volume>143</volume>
               <source>Am Rev Respir Dis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d708e544a1310">
            <label>11</label>
            <mixed-citation id="d708e551" publication-type="journal">
Mitchison DA, Nunn AJ. Influence of initial drug resistance on the re-
sponse to short course chemotherapy of pulmonary tuberculosis. Am
Rev Respir Dis1986; 133:423-30.<person-group>
                  <string-name>
                     <surname>Mitchison</surname>
                  </string-name>
               </person-group>
               <fpage>423</fpage>
               <volume>133</volume>
               <source>Am Rev Respir Dis</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d708e586a1310">
            <label>12</label>
            <mixed-citation id="d708e593" publication-type="journal">
Iseman MD, Madsen LA. Drug-resistant tuberculosis. Clin Chest Med
1989; 10:341-53.<person-group>
                  <string-name>
                     <surname>Iseman</surname>
                  </string-name>
               </person-group>
               <fpage>341</fpage>
               <volume>10</volume>
               <source>Clin Chest Med</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d708e625a1310">
            <label>13</label>
            <mixed-citation id="d708e632" publication-type="journal">
Della Bruna C, Schioppacassi G, Ungheri D, Jabes D, Morvillo E, Sanfili-
ppo A. LM 427, a new spiropiperidylrifamycin: in vitro and in vivo
studies. J Antibiot (Tokyo)1983;36:1502-6.<person-group>
                  <string-name>
                     <surname>Della Bruna</surname>
                  </string-name>
               </person-group>
               <fpage>1502</fpage>
               <volume>36</volume>
               <source>J Antibiot (Tokyo)</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d708e667a1310">
            <label>14</label>
            <mixed-citation id="d708e674" publication-type="journal">
Heifets LB, Iseman MD, Lindholm-Levy PJ. Determination of MICs of
conventional and experimental drugs in liquid medium by the radiomet-
ric method against Mycobacterium avium complex. Drugs Exp Clin Res
1987; 13:529-38.<person-group>
                  <string-name>
                     <surname>Heifets</surname>
                  </string-name>
               </person-group>
               <fpage>529</fpage>
               <volume>13</volume>
               <source>Drugs Exp Clin Res</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d708e712a1310">
            <label>15</label>
            <mixed-citation id="d708e719" publication-type="journal">
Woodley CL, Kilburn JO. In vitro susceptibility of Mycobacterium avium
complex and Mycobacterium tuberculosis strains to a spiro-piperidyl
rifamycin. Am Rev Respir Dis1982; 126:586-7.<person-group>
                  <string-name>
                     <surname>Woodley</surname>
                  </string-name>
               </person-group>
               <fpage>586</fpage>
               <volume>126</volume>
               <source>Am Rev Respir Dis</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d708e754a1310">
            <label>16</label>
            <mixed-citation id="d708e761" publication-type="journal">
Mitchison DA, Ellard GA, Grosset J. New antibacterial drugs for the
treatment of mycobacterial disease in man. Br Med Bull1988;44:
757-74.<person-group>
                  <string-name>
                     <surname>Mitchison</surname>
                  </string-name>
               </person-group>
               <fpage>757</fpage>
               <volume>44</volume>
               <source>Br Med Bull</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d708e797a1310">
            <label>17</label>
            <mixed-citation id="d708e804" publication-type="journal">
Schwander S, Riisch-Gerdes S, Mateega A, et al. A pilot study of antituber-
culous combinations comparing rifabutin with rifampin in the treatment
of HIV-associated tuberculosis: a single, blinded, randomized evaluation
in Ugandan patients with HIV-1 infection and pulmonary tuberculosis.
Tuber Lung Dis1995; 76:210-8.<person-group>
                  <string-name>
                     <surname>Schwander</surname>
                  </string-name>
               </person-group>
               <fpage>210</fpage>
               <volume>76</volume>
               <source>Tuber Lung Dis</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d708e845a1310">
            <label>18</label>
            <mixed-citation id="d708e852" publication-type="journal">
Gonzales-Montaner LJ, Natal S, Yongchalyud P, et al. Rifabutin for the
treatment of newly diagnosed pulmonary tuberculosis: a multinational,
randomized, comparative study versus rifampin. Tuber Lung Dis
1994; 75:341-7.<person-group>
                  <string-name>
                     <surname>Gonzales-Montaner</surname>
                  </string-name>
               </person-group>
               <fpage>341</fpage>
               <volume>75</volume>
               <source>Tuber Lung Dis</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d708e890a1310">
            <label>19</label>
            <mixed-citation id="d708e897" publication-type="journal">
Felten MK. Preliminary experience with rifabutine (ansamycin LM 427)
in patients with newly diagnosed pulmonary tuberculosis (PTB) and
chronic disease [abstract A 136]. Joint Annual Meeting of the American
Lung Association and the American Thoracic Society (New Orleans).
Am Rev Respir Dis1987; 135(4): part 2.<person-group>
                  <string-name>
                     <surname>Felten</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <volume>135</volume>
               <source>Am Rev Respir Dis</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d708e938a1310">
            <label>20</label>
            <mixed-citation id="d708e945" publication-type="book">
Rey R, Ramos A, Munoz L, et al. Estudio clinico controlado con rifabutina
en el tratamiento de la tuberculosis pulmonar inicial [abstract]. Primo
Congreso Luso-espanol de Pneumologia (Lisbon), 21-24 May 1989.<person-group>
                  <string-name>
                     <surname>Rey</surname>
                  </string-name>
               </person-group>
               <source>Estudio clinico controlado con rifabutina en el tratamiento de la tuberculosis pulmonar inicial [abstract]</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d708e974a1310">
            <label>21</label>
            <mixed-citation id="d708e981" publication-type="book">
Rey-Duran R, Boulahbal F, Gonzales-Montaner LJ, et al. Role of rifabutin
in the treatment of chronic drug-resistant pulmonary tuberculosis. In:
Program and abstracts of the 7th International Conference on AIDS:
Satellite Symposium on the Therapeutic Approach to Mycobacterial
Infections in AIDS. Florence, Italy: Istituto Superiore di Sanita,
1991:33-5.<person-group>
                  <string-name>
                     <surname>Rey-Duran</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Role of rifabutin in the treatment of chronic drug-resistant pulmonary tuberculosis</comment>
               <fpage>33</fpage>
               <source>Program and abstracts of the 7th International Conference on AIDS: Satellite Symposium on the Therapeutic Approach to Mycobacterial Infections in AIDS</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
