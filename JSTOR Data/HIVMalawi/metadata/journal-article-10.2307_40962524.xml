<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">biometrics</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100101</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Biometrics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley-Blackwell</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0006341X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15410420</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40962524</article-id>
         <article-categories>
            <subj-group>
               <subject>BIOMETRIC PRACTICE</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Bayesian Estimation of the Time-Varying Sensitivity of a Diagnostic Test with Application to Mother-to-Child Transmission of HIV</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Elizabeth R.</given-names>
                  <surname>Brown</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">66</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40043115</issue-id>
         <fpage>1266</fpage>
         <lpage>1274</lpage>
         <permissions>
            <copyright-statement>© The International Biometric Society, 2010</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40962524"/>
         <abstract>
            <p>We present a Bayesian model to estimate the time-varying sensitivity of a diagnostic assay when the assay is given repeatedly over time, disease status is changing, and the gold standard is only partially observed. The model relies on parametric assumptions for the distribution of the latent time of disease onset and the time-varying sensitivity. Additionally, we illustrate the incorporation of historical data for constructing prior distributions. We apply the new methods to data collected in a study of mother-to-child transmission of HIV and include a covariate for sensitivity to assess whether two different assays have different sensitivity profiles.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d968e180a1310">
            <mixed-citation id="d968e184" publication-type="other">
Albert, P. S. and Dodd, L. E. (2004). A cautionary note on the robust-
ness of latent class models for estimating diagnostic error without
a gold standard. Biometrics 60, 427-435.</mixed-citation>
         </ref>
         <ref id="d968e197a1310">
            <mixed-citation id="d968e201" publication-type="other">
Albert, P. S., McShane, L. M., and Shih, J. H. (2001). Latent class
modeling approaches for assessing diagnostic error without a gold
standard: With applications to p53 immunohistochemical assays
in bladder tumors. Biometrics 57, 610-619.</mixed-citation>
         </ref>
         <ref id="d968e217a1310">
            <mixed-citation id="d968e221" publication-type="other">
Balasubramanian, R. and Lagakos, S. W. (2001). Estimation of the
timing of perinatal transmission of HIV. Biometrics 57, 1048-
1058.</mixed-citation>
         </ref>
         <ref id="d968e234a1310">
            <mixed-citation id="d968e238" publication-type="other">
Balasubramanian, R. and Lagakos, S. W. (2003). Estimation of a failure
time distribution based on imperfect diagnostic tests. Biometrika
90, 171-182.</mixed-citation>
         </ref>
         <ref id="d968e252a1310">
            <mixed-citation id="d968e256" publication-type="other">
Dendukuri, N. and Joseph, L. (2001). Bayesian approaches to model-
ing the conditional dependence between multiple diagnostic tests.
Biometrics 57, 158-167.</mixed-citation>
         </ref>
         <ref id="d968e269a1310">
            <mixed-citation id="d968e273" publication-type="other">
Dunn, D. T., Simonds, R. J., Bulterys, M., Kalish, L. A., Moye, J.,
de Maria, A., Kind, C, Rudin, C, Denamur, E., Krivine, A.,
Loveday, C, and Newell, M. L. (2000). Interventions to prevent
vertical transmission of HIV-1: Effect on viral detection rate in
early infant samples. AIDS 14, 1421-1428.</mixed-citation>
         </ref>
         <ref id="d968e292a1310">
            <mixed-citation id="d968e296" publication-type="other">
Espeland, M. A., Platt, O. S., and Gallagher, D. (1989). Joint estima-
tion of incidence and diagnostic error rates from irregular longi-
tudinal data. Journal of the American Statistical Association 84,
972-979.</mixed-citation>
         </ref>
         <ref id="d968e312a1310">
            <mixed-citation id="d968e316" publication-type="other">
Gelfand, A. E. and Smith, A. F. M. (1990). Sampling-based approaches
to calculating marginal densities. Journal of the American Sta-
tistical Association 85, 398-409.</mixed-citation>
         </ref>
         <ref id="d968e329a1310">
            <mixed-citation id="d968e333" publication-type="other">
Guay, L., Musoke, P., Fleming, T., Bagenda, D., Allen, M., Nakabiito,
C, Sherman, J., Bakaki, P., Ducar, C, Deseyve, M., Emel, L.,
Mirochnick, M., Fowler, M., Mofenson, L., Miotti, P., Dransfield,
K., Bray, D., Mmiro, F., and Jackson, J. (1999). Intrapartum
and neonatal single-dose nevirapine compared with zidovudine for
prevention of mother-to-child transmission of HIV-1 in Kampala,
Uganda: HIVNET 012 randomised trial. The Lancet 354, 795-
802.</mixed-citation>
         </ref>
         <ref id="d968e362a1310">
            <mixed-citation id="d968e366" publication-type="other">
Gupte, N., Brookmeyer, R, Bollinger, R, and Gray, G. (2007). Mod-
eling maternal-infant HIV transmission in the presence of breast-
feeding with an imperfect test. Biometrics 63, 1189-1197.</mixed-citation>
         </ref>
         <ref id="d968e380a1310">
            <mixed-citation id="d968e384" publication-type="other">
Hui, S. L. and Zhou, X. H. (1998). Evaluation of diagnostic tests with-
out gold standards. Statistical Methods in Medical Research 7,
354-370.</mixed-citation>
         </ref>
         <ref id="d968e397a1310">
            <mixed-citation id="d968e403" publication-type="other">
Jackson, J. B., Musoke, P., Fleming, T., Guay, L. A., Bagenda, D.,
Allen, M., Nakabiito, C, Sherman, J., Bakaki, P., Owor, M.,
Ducar, C, Deseyve, M., Mwatha, A., Emel, L., Duefield, C,
Mirochnick, M., Fowler, M. G., Mofenson, L., Miotti, P., Gigliotti,
M., Bray, D., and Mmiro, F. (2003). Intrapartum and neonatal
single-dose nevirapine compared with zidovudine for prevention
of mother-to-child transmission of HIV-1 in Kampala, Uganda:
18-month follow-up of the HIVNET 012 randomised trial. The
Lancet 362, 859-868.</mixed-citation>
         </ref>
         <ref id="d968e435a1310">
            <mixed-citation id="d968e439" publication-type="other">
Nduati, R., John, G., Mbori-Ngacha, D., Richardson, B., Overbaugh,
J., Mwatha, A., Ndinya-Achola, J., Bwayo, J., Onyango, F. E.,
Hughes, J., and Kreiss, J. (2000). Effect of breastfeeding and for-
mula feeding on transmission of HIV-1: A randomized clinical
trial. Journal of the American Medical Association 283, 1167-
1174.</mixed-citation>
         </ref>
         <ref id="d968e462a1310">
            <mixed-citation id="d968e466" publication-type="other">
Neal, R. M. (2003). Slice sampling. Annals of Statistics 31, 705-767.</mixed-citation>
         </ref>
         <ref id="d968e473a1310">
            <mixed-citation id="d968e477" publication-type="other">
Neal, R. M. (2005). The short-cut metropolis method. Technical Report
0506, Department of Statistics, University of Toronto.</mixed-citation>
         </ref>
         <ref id="d968e487a1310">
            <mixed-citation id="d968e491" publication-type="other">
Nesheim, S., Palumbo, P., Sullivan, K., Lee, F., Vink, P., Abrams, E.,
and Bulterys, M. (2003). Quantitative RNA testing for diagnosis
of HIV-infected infants. Journal of Acquired Immune Deficiency
Syndromes 32, 192-195.</mixed-citation>
         </ref>
         <ref id="d968e508a1310">
            <mixed-citation id="d968e512" publication-type="other">
Perelson, A. and Nelson, P. (1999). Mathematical analysis of HIV-1
dynamics in vivo. SIAM REVIEW 41, 3-44.</mixed-citation>
         </ref>
         <ref id="d968e522a1310">
            <mixed-citation id="d968e526" publication-type="other">
R Development Core Team. (2006). R: A Language and Environment
for Statistical Computing. R Foundation for Statistical Comput-
ing, Vienna, Austria. ISBN 3-900051-07-0.</mixed-citation>
         </ref>
         <ref id="d968e539a1310">
            <mixed-citation id="d968e543" publication-type="other">
Rouet, F., Sakarovitch, C, Msellati, P., Elenga, N., Montcho, C, Viho,
I., Blanche, S., Rouzioux, C, Dabis, F., and Leroy, V. (2003).
Pediatrie viral human immunodeficiency virus type 1 RNA levels,
timing of infection, and disease progression in African HIV-1-
infected children. Pediatrics 112, e289.</mixed-citation>
         </ref>
         <ref id="d968e562a1310">
            <mixed-citation id="d968e566" publication-type="other">
Simonds, R. J., Brown, T. M., Thea, D. M., Orloff, S. L., Steketee,
R. W., Lee, F. K., Palumbo, P. E., and Kalish, M. L. (1998).
Sensitivity and specificity of a qualitative RNA detection as-
say to diagnose HIV infection in young infants. AIDS 12, 1545-
1549.</mixed-citation>
         </ref>
         <ref id="d968e585a1310">
            <mixed-citation id="d968e591" publication-type="other">
Taha, T. E., Brown, E. R., Hoffman, I., Fawzi, W., Read, J. S., Sinkala,
M., Martinson, F., Kafulafula, G., Msamanga, G., Valentine, M.,
Emel, L., Mwatha, A., Adeniyi-Jones, S., Goldenberg, R., and the
HPTN024 Team. (2006). A phase III clinical trial of antibiotics
to reduce chorioamnonitis-related perinatal HIV-1 transmission.
AIDS 20, 1313-1321.</mixed-citation>
         </ref>
         <ref id="d968e614a1310">
            <mixed-citation id="d968e618" publication-type="other">
Taha, T. E., Hoover, D. R., Kumwenda, N. I., Fiscus, S. A., Kafulafula,
G., Nkhoma, C, Chen, S., Piwowar, E., Broadhead, R. L., Jack-
son, J. B., and Miotti, P. G. (2007). Late postnatal transmission
of HIV-1 and associated factors. Journal of Infectious Diseases
196, 10-14.</mixed-citation>
         </ref>
         <ref id="d968e638a1310">
            <mixed-citation id="d968e642" publication-type="other">
Taha, T. E., Kumwenda, N. L, Hoover, D. R., Fiscus, S. A., Kafulafula,
G., Nkhoma, C, Nour, S., Chen, S., Liomba, G., Miotti, P. G.,
and Broadhead, R. L. (2004). Nevirapine and zidovudine at birth
to reduce perinatal transmission of HIV in an African setting -
A randomized controlled trial. Journal of the American Medical
Association 292, 202-209.</mixed-citation>
         </ref>
         <ref id="d968e665a1310">
            <mixed-citation id="d968e669" publication-type="other">
The Breastfeeding and HIV International Transmission Study Group.
(2004). Late postnatal transmission of HIV-1 in breast-fed chil-
dren: An individual patient data meta-analysis. Journal of Infec-
tious Diseases 189, 2154-2166.</mixed-citation>
         </ref>
         <ref id="d968e685a1310">
            <mixed-citation id="d968e689" publication-type="other">
Thistle, P., Spitzer, R. F., Glazier, R. H., Pilon, R., Arbess, G.,
Simor, A., Boyle, E., Chitsike, L, Chipato, T., Gottesman, M.,
and Silverman, M. (2007). A randomized, double-blind, placebo-
controlled trial of combined nevirapine and zidovudine compared
with nevirapine alone in the prevention of perinatal transmis-
sion of HIV in Zimbabwe. Clinical Infectious Diseases 44, 111-
119.</mixed-citation>
         </ref>
         <ref id="d968e715a1310">
            <mixed-citation id="d968e719" publication-type="other">
Young, N. L., Shaffer, N., Chaowanachan, T., Chotpitayasunondh, T.,
Vanparapar, N., Mock, P. A., Waranawat, N., Chokephaibulkit,
K., Chuachoowong, R., Wasinrapee, P., Mastro, T. D., and Si-
monds, R. J. (2000). Early diagnosis of HIV-1-infected infants
in Thailand using RNA and DNA PCR assays sensitive to non-B
subtypes. Journal of Acquired Immune Deficiency Syndromes 24,
401.</mixed-citation>
         </ref>
         <ref id="d968e745a1310">
            <mixed-citation id="d968e749" publication-type="other">
Zhang, P. and Lagakos, S. W. (2008). Analysis of time to a silent
event whose occurrence is monitored with error, with applica-
tion to mother-to-child HIV transmission. Statistics in Medicine
27, 4637-4646.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
