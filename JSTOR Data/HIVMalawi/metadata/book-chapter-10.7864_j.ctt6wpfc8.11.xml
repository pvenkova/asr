<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wpfc8</book-id>
      <subj-group>
         <subject content-type="call-number">HC59.72.P6T66 2007</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Poverty</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Political violence</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic assistance, American</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Public-private sector cooperation</subject>
         <subj-group>
            <subject content-type="lcsh">Congresses</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Peace building</subject>
         <subj-group>
            <subject content-type="lcsh">Congresses</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Too Poor for Peace?</book-title>
         <subtitle>Global Poverty, Conflict, and Security in the 21st Century</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>BRAINARD</surname>
               <given-names>LAEL</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>CHOLLET</surname>
               <given-names>DEREK</given-names>
            </name>
         </contrib>
         <role content-type="editor">Editors</role>
      </contrib-group>
      <pub-date>
         <day>31</day>
         <month>12</month>
         <year>2007</year>
      </pub-date>
      <isbn content-type="epub">9780815713760</isbn>
      <isbn content-type="epub">0815713762</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2007</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wpfc8"/>
      <abstract abstract-type="short">
         <p>Extreme poverty exhausts institutions, depletes resources, weakens leadership, and ultimately contributes to rising insecurity and conflict. Just as poverty begets insecurity, however, the reverse is also true. As the destabilizing effects of conflict settle in, civil institutions are undermined and poverty proliferates. Breaking this nexus between poverty and conflict is one of the biggest challenges of the twenty-first century. The authors of this compelling book -some of the most experienced practitioners from around the world -investigate the complex and dynamic relationship between poverty and insecurity, exploring possible agents for change. They bring the latest lessons and intellectual framework to bear in an examination of African leadership, the private sector, and American foreign aid as vehicles for improving economic conditions and security. Contributors include Colin Kahl (University of Minnesota),Vinca LaFleur (Vinca LaFleur Communications), Edward Miguel (University of California, Berkeley), Jane Nelson (Harvard University and Brookings), Anthony Nyong (University of Jos and the International Development Research Centre, Nairobi), Susan Rice (Brookings), Robert Rotberg (Harvard University and the World Peace Foundation), Marc Sommers (Tufts University), Hendrik Urdal (International Peace Research Institute), and Jennifer Windsor (Freedom House).</p>
      </abstract>
      <counts>
         <page-count count="175"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Talbott</surname>
                           <given-names>Strobe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>While the world is experiencing an unprecedented period of peace among major powers, vast numbers of people are perishing in civil wars and from other ravages associated with failed states and corrupt, incompetent, if not murderous political systems. Whether sparked by natural resource scarcities, inadequate employment opportunities for growing numbers of youths, or decrepit and corrupt institutions, intrastate conflict thrives in areas of poverty, leading to a vicious and deadly cycle between poverty and insecurity.<italic>Too Poor for Peace? Global Poverty, Conflict, and Security in the 21st Century</italic>explores the many facets of the tangled web between poverty and insecurity,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Tangled Web:</title>
                     <subtitle>The Poverty-Insecurity Nexus</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Brainard</surname>
                           <given-names>Lael</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chollet</surname>
                           <given-names>Derek</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>LaFleur</surname>
                           <given-names>Vinca</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The fight against global poverty is commonly—and appropriately—framed as a moral imperative. Stark images of suffering weigh on Western consciences, as images of hungry children in Niger, AIDS orphans in Tanzania, tsunami victims in Indonesia, and refugees in Darfur are beamed into our living rooms in real time. In today’s increasingly interconnected world, the “haves” cannot ignore the suffering of the “have-nots.” Whether or not we choose to care, we cannot pretend that we do not see.</p>
                     <p>Yet the effort to end poverty is about much more than extending a helping hand to those in need. In a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Poverty Breeds Insecurity</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rice</surname>
                           <given-names>Susan E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>Few American leaders today evince much interest in poverty—either domestic or international. Contrast our current obsession with flag burning, the estate tax, immigration, or gay marriage with the animating themes of the 1960s. Then, John and Robert Kennedy, Lyndon Johnson, Martin Luther King Jr., and many others summoned our national energy to wage a “War on Poverty” and build a “Great Society.” Our media brought us searing images of destitution from Appalachia to the Mississippi Delta to the South Bronx. Our president insisted in global forums that “political sovereignty is but a mockery without the means of meeting poverty</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Poverty and Violence:</title>
                     <subtitle>An Overview of Recent Research and Implications for Foreign Aid</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Miguel</surname>
                           <given-names>Edward</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>50</fpage>
                  <abstract>
                     <p>Dozens of countries around the world have suffered civil conflicts in the past few decades, with the highest concentration in sub-Saharan Africa. The humanitarian consequences have been staggering: at least 3 million civilian deaths in the Democratic Republic of the Congo’s (the former Zaire) civil war, and millions of other deaths in Sudan, Rwanda, Sierra Leone, Angola, Somalia, Uganda, Mozambique, and Liberia, among others. And civil conflict is not just an African problem, as continuing violence in the Middle East and elsewhere (Colombia, Nepal, and so on) demonstrates.</p>
                     <p>The direct humanitarian consequences of war for survivors are enormous in physical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Demography, Environment, and Civil Strife</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kahl</surname>
                           <given-names>Colin H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>60</fpage>
                  <abstract>
                     <p>At both the global and local levels, natural resource depletion and environmental degradation result from the interactions among extreme wealth, population pressures, and extreme poverty. The material-intensive and pollution-laden consumption habits and production activities of high-income countries are responsible for most of the world’s greenhouse gases, solid and hazardous waste, and other environmental pollution. High-income countries also generate a disproportionate amount of the global demand for fossil fuels, nonfuel minerals, grain, meat, fish, tropical hardwoods, and products from endangered species.¹</p>
                     <p>Poverty and inequality within developing countries, especially those with rapidly growing populations, also places burdens on the environment. Impoverished individuals</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Resource and Environmental Security</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nyong</surname>
                           <given-names>Anthony</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Major contemporary economic and social issues are intimately linked with the quest for global poverty reduction, particularly poverty in developing countries. Today, people around the world, particularly in the developing world, are struggling to survive in the face of a multitude of environmental problems—the overuse of natural resources, the degradation of the ecosystem, and extreme climatic events such as floods, droughts, and hurricanes. These problems play an important role in increasing human vulnerability, undermining livelihoods and human well-being, threatening environmental security, and potentially generating or exacerbating conflict.¹ The past two decades have witnessed the intensification of the debate concerning</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>The Demographics of Political Violence:</title>
                     <subtitle>Youth Bulges, Insecurity, and Conflict</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Urdal</surname>
                           <given-names>Henrik</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>90</fpage>
                  <abstract>
                     <p>Youth often play a prominent role in political violence, and the existence of a “youth bulge” has historically been associated with times of political crisis.¹ Generally, it has been observed that young males are the main protagonists of criminal as well as political violence.</p>
                     <p>The question is whether countries with youthful age structures, or “youth bulges,”² are more likely to experience internal armed conflict, terrorism, and riots. The issue has received increasing attention during the past decade following the more general debate over security implications of population pressure and resource scarcity. In “The Coming Anarchy,” Robert Kaplan argues that anarchy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Embracing the Margins:</title>
                     <subtitle>Working with Youth amid War and Insecurity</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sommers</surname>
                           <given-names>Marc</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>ʺRonaldoʺ was a four-year veteran of Liberia’s civil war. He was first abducted in 2000 by Charles Taylor’s army at the age of twelve years. His captors took him to a military camp, where he found many of his friends already there. They immediately warned him that “you have to be brave to survive.” His subsequent bravery caught his general’s eye, and Ronaldo soon became the general’s houseboy and prize trainee. Ronaldo escaped but was later recaptured, eventually returning to his role in the general’s service.</p>
                     <p>Once, when the general left their upcountry military base for consultations with Taylor in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The Role of Leadership in Overcoming Poverty and Achieving Security in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rotberg</surname>
                           <given-names>Robert I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>Today there is a widespread recognition in sub-Saharan Africa that the continent’s earlier leaders, and some of those still in charge, erred by attempting to repeal the fundamental laws of economics, by grabbing wealth for themselves and their clients, and by pretending that their antidemocratic instincts were culturally sanctioned—that Africans preferred autocracy to real democratic practice. There are still a few throwbacks; several contemporary heads of state continue to be mired in the old ways. But whereas Africans several decades ago accepted misrule, nowadays Africans revile the excesses of past and present leaders and expect their heads of state</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Operating in Insecure Environments</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nelson</surname>
                           <given-names>Jane</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>128</fpage>
                  <abstract>
                     <p>Conditions of severe political, economic, and physical insecurity, often underpinned by weak governance and high levels of poverty and inequality, continue to blight the lives of millions of people around the world. The poor are particularly vulnerable to the ravages and costs of conflict, natural and economic disasters, repression, corruption, market distortions and externalities, weak legal and regulatory frameworks, and inadequate public institutions.²</p>
                     <p>Yet operating in insecure environments is not only a challenge for low-income households and communities. It is increasingly a challenge for donor agencies; for companies, including both indigenous enterprises and foreign investors; and for nongovernmental organizations (NGOs),</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Breaking the Poverty-Insecurity Nexus:</title>
                     <subtitle>Is Democracy the Answer?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Windsor</surname>
                           <given-names>Jennifer L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>Do poor people have to make a choice between political freedom and freedom from insecurity or want? This question continues to be asked in some academic and policy circles, despite the evidence and lessons learned from the past two decades that all point to the fact that lasting success in all three goals requires an integrated approach.</p>
                     <p>Although democracy is not a panacea for all ills, it is clearly associated with and essential to achieving lasting success in reducing both poverty and insecurity. The challenge for U.S. policymakers is to further refine strategies, processes, and organizational structures to strengthen the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.14</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>163</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>167</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpfc8.16</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>177</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
