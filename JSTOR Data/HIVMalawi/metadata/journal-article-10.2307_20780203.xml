<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20780203</article-id>
         <article-categories>
            <subj-group>
               <subject>MAJOR ARTICLES AND BRIEF REPORTS</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Fetal Immune Activation to Malaria Antigens Enhances Susceptibility to In Vitro HIV Infection in Cord Blood Mononuclear Cells</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kevin</given-names>
                  <surname>Steiner</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Latoya</given-names>
                  <surname>Myrie</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Indu</given-names>
                  <surname>Malhotra</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter</given-names>
                  <surname>Mungai</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eric</given-names>
                  <surname>Muchiri</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Arlene</given-names>
                  <surname>Dent</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Christopher L.</given-names>
                  <surname>King</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>15</day>
            <month>9</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">202</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20780189</issue-id>
         <fpage>899</fpage>
         <lpage>907</lpage>
         <permissions>
            <copyright-statement>© 2010 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20780203"/>
         <abstract>
            <p>Mother-to-child-transmission (MTCT) of human immunodeficiency virus (HIV) remains a significant cause of new HIV infections in many countries. To examine whether fetal immune activation as a consequence of prenatal exposure to parasitic antigens increases the risk of MTCT, cord blood mononuclear cells (CBMCs) from Kenyan and North American newborns were examined for relative susceptibility to HIV infection in vitro. Kenyan CBMCs were 3-fold more likely to be infected with HIV than were North American CBMCs (P = .03). Kenyan CBMCs with recall responses to malaria antigens demonstrated enhanced susceptibility to HIV when compared with Kenyan CBMCs lacking recall responses to malaria (P = .03). CD4⁺ T cells from malaria-sensitized newborns expressed higher levels of CD25 and human leukocyte antigen DR ex vivo, which is consistent with increased immune activation. CD4⁺ T cells were the primary reservoir of infection at day 4 after virus exposure. Thus, prenatal exposure and in utero priming to malaria may increase the risk of MTCT.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1098e268a1310">
            <label>1</label>
            <mixed-citation id="d1098e275" publication-type="other">
Abu-Raddad L,
                Patnaik , Küblin J. Dual infection with HIV and
malaria fuels the
                spread of both diseases in sub-Saharan Africa. Science
2006;
                314(5805):1603-1606.</mixed-citation>
         </ref>
         <ref id="d1098e288a1310">
            <label>2</label>
            <mixed-citation id="d1098e295" publication-type="other">
UNAIDS. AIDS
                Epidemic Update: 2007. Geneva, Switzerland: UNAIDS,
2007.</mixed-citation>
         </ref>
         <ref id="d1098e305a1310">
            <label>3</label>
            <mixed-citation id="d1098e312" publication-type="other">
Dreyfuss ML,
                Msamanga Gl, Spiegelman D, Hunter DJ, Urassá EJ, Hertz-
mark E, Fawzi WW.
                Determinants of low birth weight among HIV-
infected pregnant
                women in Tanzania. Am J Clin Nutr 2001; 74(6):814--
826.</mixed-citation>
         </ref>
         <ref id="d1098e328a1310">
            <label>4</label>
            <mixed-citation id="d1098e335" publication-type="other">
Zijenah L, Mbizvo
                M, Kasule J, et al. Mortality in the first 2 years among
infants born to
                human immunodeficiency virus-infected women in Ha-
rare, Zimbabwe. J
                Infect Dis 1998; 178(1):109-113.</mixed-citation>
         </ref>
         <ref id="d1098e349a1310">
            <label>5</label>
            <mixed-citation id="d1098e356" publication-type="other">
Taha TET,
                Dallabetta GA, Canner JK, et al. The effect of human im-
munodeficiency
                virus infection on birthweight, and infant and child
mortality in
                urban Malawi. Int J Epidemiol 1995;24(5):1022-1029.</mixed-citation>
         </ref>
         <ref id="d1098e369a1310">
            <label>6</label>
            <mixed-citation id="d1098e376" publication-type="other">
Parise ME, Ayisi
                JG, Nahlen BL, et al. Efficacy of sulfadoxine-pyri-
methamine for
                prevention of placental malaria in an area of Kenya
with a high
                prevalence of malaria and human immunodeficiency virus
infection. Am J
                Trop Med Hyg 1998;59(5):813-822.</mixed-citation>
         </ref>
         <ref id="d1098e392a1310">
            <label>7</label>
            <mixed-citation id="d1098e399" publication-type="other">
Matteelli A,
                Caligaris S, Castelli F, Carosi G. The placenta and malaria.
Ann Trop Med
                Parasitol 1997;91(7):803-810.</mixed-citation>
         </ref>
         <ref id="d1098e409a1310">
            <label>8</label>
            <mixed-citation id="d1098e416" publication-type="other">
Bulmer J, Rasheed
                F, Morrison L, Fancis N, Greenwood B. Placental
malaria. II. A
                semi-quantitative investigation of the pathological fea-
tures.
                Histopathology 1993;22(3):219-225.</mixed-citation>
         </ref>
         <ref id="d1098e429a1310">
            <label>9</label>
            <mixed-citation id="d1098e438" publication-type="other">
Inion I,
                Mwanyumba F, Gaillard , et al. Placental malaria and perinatal
transmission of
                human immunodeficiency virus type 1. J Infect Dis
2003;
                188(11):1675-1678.</mixed-citation>
         </ref>
         <ref id="d1098e451a1310">
            <label>10</label>
            <mixed-citation id="d1098e458" publication-type="other">
van Eijk A, Ayisi
                J, ter Kuile F, et al. HIV increases the risk of malaria
in women of all
                gravidities in Kisumu, Kenya. AIDS 2003; 17(4):595-
603.</mixed-citation>
         </ref>
         <ref id="d1098e472a1310">
            <label>11</label>
            <mixed-citation id="d1098e479" publication-type="other">
Ladner J, Leroy V,
                Simonon A, et al. HIV infection, malaria, and
pregnancy: A
                prospective cohort study in Kigali, Rwanda. Am J Trop
Med Hyg 2002;
                66(1 ):56-60.</mixed-citation>
         </ref>
         <ref id="d1098e492a1310">
            <label>12</label>
            <mixed-citation id="d1098e499" publication-type="other">
ter Kuile F,
                Parise M, Verhoeíf F, et al. The burden of co-infection
with human
                immunodeficiency virus type 1 and malaria in pregnant
women in
                sub-saharan Africa. Am J Trop Med Hyg 2004; 71(2 Suppl):
41-54.</mixed-citation>
         </ref>
         <ref id="d1098e515a1310">
            <label>13</label>
            <mixed-citation id="d1098e522" publication-type="other">
Brahmbhatt H,
                Sullivan D, Kigozi G, et al. Association of HIV and
malaria with
                mother-to-child transmission, birth outcomes, and child
mortality. J
                Acquir Immune Defic Syndr 2008; 47(4):472-476.</mixed-citation>
         </ref>
         <ref id="d1098e535a1310">
            <label>14</label>
            <mixed-citation id="d1098e542" publication-type="other">
Ticconi C,
                Mapfumo M, Dorrucci M, et al. Effect of maternal HIV
and malaria
                infection on pregnancy and perinatal outcome in Zim-
babwe. J Acquir
                Immune Defic Syndr 2003;34(3):289-294.</mixed-citation>
         </ref>
         <ref id="d1098e555a1310">
            <label>15</label>
            <mixed-citation id="d1098e562" publication-type="other">
Ayisi JG, van
                Eijk AM, ter Kuile FO, et al. The effect of dual infection
with HIV and
                malaria on pregnancy outcome in western Kenya. AIDS
2003;17(4):585-594.</mixed-citation>
         </ref>
         <ref id="d1098e575a1310">
            <label>16</label>
            <mixed-citation id="d1098e582" publication-type="other">
Mwapasa V,
                Rogerson S, Molyneux M, et al. The effect of Plasmodium
falciparum
                malaria on peripheral and placental HIV-1 RNA concen-
trations in
                pregnant Malawian women. AIDS 2004; 18(7):1051-1059.</mixed-citation>
         </ref>
         <ref id="d1098e596a1310">
            <label>17</label>
            <mixed-citation id="d1098e603" publication-type="other">
Küblin JG,
                Patnaik P, Jere CS, et al. Effect of Plasmodium falciparum
malaria on
                concentration of HIV-1-RNA in the blood of adults in rural
Malawi: a
                prospective cohort study. Lancet 2005; 365(9455):233-240.</mixed-citation>
         </ref>
         <ref id="d1098e616a1310">
            <label>18</label>
            <mixed-citation id="d1098e623" publication-type="other">
Fawzi W,
                Msamanga G, Renjifo B, et al. Predictors of intrauterine and
intrapartum
                transmission of HIV-1 among Tanzánián women. AIDS
2001;
                15(9):1157-1165.</mixed-citation>
         </ref>
         <ref id="d1098e636a1310">
            <label>19</label>
            <mixed-citation id="d1098e643" publication-type="other">
Mofenson LM,
                Lambert JS, Stiehm ER, et al. Risk factors for perinatal
transmission of
                human immunodeficiency virus type 1 in women
treated with
                zidovudine. N Engl J Med 1999; 341 (6):385-393.</mixed-citation>
         </ref>
         <ref id="d1098e656a1310">
            <label>20</label>
            <mixed-citation id="d1098e663" publication-type="other">
Ned R, Moore J,
                Chaisavaneeyakorn S, Udhayakumar V. Modulation
of immune
                responses during HIV-malaria co-infection in pregnancy.
Trends Parasitol
                2005;21(6):284-291.</mixed-citation>
         </ref>
         <ref id="d1098e676a1310">
            <label>21</label>
            <mixed-citation id="d1098e683" publication-type="other">
Brahmbhatt H,
                Kigozi G, Wabwire-Mangen F, et al. The effects of pla-
cental malaria
                on mother-to-child HIV transmission in Rakai, Uganda.
AIDS 2003;
                17(17):2539-2541.</mixed-citation>
         </ref>
         <ref id="d1098e696a1310">
            <label>22</label>
            <mixed-citation id="d1098e703" publication-type="other">
Ayouba A,
                Nerrienet E, Menu E, et al. Mother-to-child transmission
of human
                immunodeficiency virus type 1 in relation to the season in
Yaounde,
                Cameroon. Am J Trop Med Hyg 2003;69(4):447-449.</mixed-citation>
         </ref>
         <ref id="d1098e717a1310">
            <label>23</label>
            <mixed-citation id="d1098e724" publication-type="other">
Ayisi J, van
                Eijk A, Newman R, et al. Maternal malaria and perinatal
HIV
                transmission, western Kenya. Emerg Infect Dis 2004; 10(4):643-652.</mixed-citation>
         </ref>
         <ref id="d1098e734a1310">
            <label>24</label>
            <mixed-citation id="d1098e741" publication-type="other">
Zack J, Arrigo
                S, Weitsman S, Go A, Haislip A, Chen I. HIV-1 entry
into quiescent
                primary lymphocytes: molecular analysis reveals a labile,
latent viral
                structure. Cell 1990;61(2):213-222.</mixed-citation>
         </ref>
         <ref id="d1098e754a1310">
            <label>25</label>
            <mixed-citation id="d1098e761" publication-type="other">
Zack J, Haislip
                A, Krogstad P, Chen I. Incompletely reverse-transcribed
human
                immunodeficiency virus type 1 genomes in quiescent cells can
function as
                intermediates in the retro viral life cycle. J Virol 1992; 66(3):
1717-1725.</mixed-citation>
         </ref>
         <ref id="d1098e777a1310">
            <label>26</label>
            <mixed-citation id="d1098e784" publication-type="other">
Malhotra I, Ouma
                J, Wamachi A, et al. In utero exposure to helminth
and
                mycobacterial antigens generates cytokine responses similar to that
observed in
                adults. J Clin Invest 1997;99(7):1759-1766.</mixed-citation>
         </ref>
         <ref id="d1098e797a1310">
            <label>27</label>
            <mixed-citation id="d1098e804" publication-type="other">
Ismaili J, van
                der Sande M, Holland M, et al. Plasmodium falciparum
infection of the
                placenta affects newborn immune responses. Clin Exp
Immunol 2003;
                133(3):414-421.</mixed-citation>
         </ref>
         <ref id="d1098e817a1310">
            <label>28</label>
            <mixed-citation id="d1098e824" publication-type="other">
Malhotra I,
                Mungai P, Muchiri E, et al. Distinct Thl- and Th2-Type
prenatal
                cytokine responses to Plasmodium falciparum erythrocyte in-
vasion ligands.
                Infect Immun 2005; 73(6):3462-3470.</mixed-citation>
         </ref>
         <ref id="d1098e838a1310">
            <label>29</label>
            <mixed-citation id="d1098e845" publication-type="other">
Xi G, Leke R,
                Thuita L, et al. Congenital exposure to Plasmodium
falciparum
                antigens: prevalence and antigenic specificity of in utero-
produced
                antimalarial Immunoglobulin M antibodies. Infect Immun
2003;71(3):1242-1246.</mixed-citation>
         </ref>
         <ref id="d1098e861a1310">
            <label>30</label>
            <mixed-citation id="d1098e868" publication-type="other">
Malhotra I,
                Mungai P, Muchiri E, Kwiek J, Meshnick S, King C. Um-
bilical
                cord-blood infections with Plasmodium falciparum Malaria Are
Acquired
                Antenatally in Kenya. J Infect Dis 2006; 194(2):176-183.</mixed-citation>
         </ref>
         <ref id="d1098e881a1310">
            <label>31</label>
            <mixed-citation id="d1098e888" publication-type="other">
Gallagher M,
                Malhotra I, Mungai P, et al. The effects of maternal hel-
minth and malaria
                infections on mother-to-child HIV transmission.
AIDS
                2005;19(16):1849-1855.</mixed-citation>
         </ref>
         <ref id="d1098e901a1310">
            <label>32</label>
            <mixed-citation id="d1098e908" publication-type="other">
Malhotra I, Dent A,
                Mungai P, et al. Can prenatal malaria exposure
produce an immune
                tolerant phenotype?: a prospective birth cohort
study in Kenya.
                PLoS Med 2009;6(7):el000116.</mixed-citation>
         </ref>
         <ref id="d1098e921a1310">
            <label>33</label>
            <mixed-citation id="d1098e928" publication-type="other">
Malhotra I,
                Wamachi AN, Mungai PL, et al. Fine specificity of neonatal
lymphocytes to an
                abundant malaria blood-stage antigen: epitope map-
ping of
                Plasmodium falciparum MSP 133. J Immunol 2008; 180(5):3383-
3390.</mixed-citation>
         </ref>
         <ref id="d1098e944a1310">
            <label>34</label>
            <mixed-citation id="d1098e951" publication-type="other">
Ostrowski M, Chun
                T, Cheseboro B, Stanley S, Tremblay M. Unit 12.5:
detection assays
                for HIV proteins. In: Current protocols in immunology.
New York, NY:
                John Wiley &amp; Sons, 2007.</mixed-citation>
         </ref>
         <ref id="d1098e965a1310">
            <label>35</label>
            <mixed-citation id="d1098e972" publication-type="other">
Battke F, Symons
                S, Nieselt K. Mayday- integrative analytics for ex-
pression data.
                BMC Bioinformatics 2010; 11(1):121.</mixed-citation>
         </ref>
         <ref id="d1098e982a1310">
            <label>36</label>
            <mixed-citation id="d1098e989" publication-type="other">
May K, Grube M,
                Malhotra I, et al. Antibody-dependent transplacental
transfer of
                malaria blood-stage antigen using a human ex vivo placental
perfusion model.
                PLoS ONE 2009;4(1 l):e7986.</mixed-citation>
         </ref>
         <ref id="d1098e1002a1310">
            <label>37</label>
            <mixed-citation id="d1098e1009" publication-type="other">
Wu L, Paxton WA,
                Kassam N, et al. CCR5 levels and expression pattern
correlate with
                infectability by macrophage-tropic HIV-1 in vitro. J Exp
Med
                1997;185(9):1681-1692.</mixed-citation>
         </ref>
         <ref id="d1098e1022a1310">
            <label>38</label>
            <mixed-citation id="d1098e1029" publication-type="other">
Paxton WA, Liu R,
                Kang S, et al. Reduced HIV-1 infectability of CD4+
lymphocytes from
                exposed-uninfected individuals: association with low
expression of
                CCR5 and high production of [beta]-chemokines. Vi-
rology
                1998;244(l):66-73.</mixed-citation>
         </ref>
         <ref id="d1098e1045a1310">
            <label>39</label>
            <mixed-citation id="d1098e1052" publication-type="other">
Lawn S, Butera S,
                Folks T. Contribution of immune activation to the
pathogenesis and
                transmission of human immunodeficiency virus type
1 infection. Clin
                Microbiol Rev 2001; 14(4):753-777.</mixed-citation>
         </ref>
         <ref id="d1098e1065a1310">
            <label>40</label>
            <mixed-citation id="d1098e1072" publication-type="other">
Chuachoowong R,
                Shaffer N, Siriwasin W, et al. Short-course antenatal
zidovudine reduces
                both cervicovaginal human immunodeficiency vi-
rus type 1 RNA
                levels and risk of perinatal transmission. J Infect Dis
2000;181(1):99-106.</mixed-citation>
         </ref>
         <ref id="d1098e1089a1310">
            <label>41</label>
            <mixed-citation id="d1098e1096" publication-type="other">
John G, Nduati R,
                Mbori-Ngacha D, et al. Correlates of mother-to-
child human
                immunodeficiency virus type 1 (HIV-1) transmission:
association with
                maternal plasma HIV-1 RNA load, genital HIV-1 DNA
shedding, and
                breast infections. J Infect Dis 2001; 183(2):206-212.</mixed-citation>
         </ref>
         <ref id="d1098e1112a1310">
            <label>42</label>
            <mixed-citation id="d1098e1119" publication-type="other">
Tuomala R,
                O'Driscoll P, Bremer J, et al. Cell-associated genital tract
virus and
                vertical transmission of human immunodeficiency virus type
1 in
                antiretroviral-experienced women. J Infect Dis 2003; 187(3):375-
384.</mixed-citation>
         </ref>
         <ref id="d1098e1135a1310">
            <label>43</label>
            <mixed-citation id="d1098e1142" publication-type="other">
Van de Perre P.
                Mother-to-child transmission of HIV-1: the 'all mu-
cosal'
                hypothesis as a predominant mechanism of transmission. AIDS
1999;
                13(9):1133-1138.</mixed-citation>
         </ref>
         <ref id="d1098e1155a1310">
            <label>44</label>
            <mixed-citation id="d1098e1162" publication-type="other">
Gaillard ,
                Verhofstede C, Mwanyumba F, et al. Exposure to HIV-1
during delivery
                and mother-to-child transmission. AIDS 2000; 14(15):
2341-2348.</mixed-citation>
         </ref>
         <ref id="d1098e1175a1310">
            <label>45</label>
            <mixed-citation id="d1098e1182" publication-type="other">
Pope M, Haase
                AT. Transmission, acute HIV-1 infection and the quest
for strategies
                to prevent infection. Nat Med 2003; 9(7):847-852.</mixed-citation>
         </ref>
         <ref id="d1098e1192a1310">
            <label>46</label>
            <mixed-citation id="d1098e1199" publication-type="other">
Schacker T,
                Little S, Connick E, et al. Rapid accumulation of human
immunodeficiency
                virus (HIV) in lymphatic tissue reservoirs during
acute and early
                HIV infection: implications for timing of antiretroviral
therapy. J
                Infect Dis 2000; 181(l):354-357.</mixed-citation>
         </ref>
         <ref id="d1098e1216a1310">
            <label>47</label>
            <mixed-citation id="d1098e1223" publication-type="other">
Schacker T,
                Little S, Connick E, et al. Productive infection of cells
in lymphoid
                tissues during primary and early human immunodefi-
ciency virus
                infection. J Infect Dis 2001; 183(4):555-562.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
