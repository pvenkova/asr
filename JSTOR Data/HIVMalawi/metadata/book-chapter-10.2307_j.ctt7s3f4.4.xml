<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7s3f4</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
         <subject>Philosophy</subject>
      </subj-group>
      <book-title-group>
         <book-title>Exploitation and Developing Countries</book-title>
         <subtitle>The Ethics of Clinical Research</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Hawkins</surname>
               <given-names>Jennifer S.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Emanuel</surname>
               <given-names>Ezekiel J.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>04</day>
         <month>08</month>
         <year>2008</year>
      </pub-date>
      <isbn content-type="ppub">9780691126760</isbn>
      <isbn content-type="ppub">0691126763</isbn>
      <isbn content-type="epub">9781400837328</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>Princeton; Oxford</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2008</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt7s3f4"/>
      <abstract abstract-type="short">
         <p>When is clinical research in developing countries exploitation? Exploitation is a concept in ordinary moral thought that has not often been analyzed outside the Marxist tradition. Yet it is commonly used to describe interactions that seem morally suspect in some way. A case in point is clinical research sponsored by developed countries and carried out in developing countries, with participants who are poor and sick, and lack education. Such individuals seem vulnerable to abuse. But does this, by itself, make such research exploitative?</p>
         <p>
            <italic>Exploitation and Developing Countries</italic>is an attempt by philosophers and bioethicists to reflect on the meaning of exploitation, to ask whether and when clinical research in developing countries counts as exploitative, and to consider what can be done to minimize the possibility of exploitation in such circumstances. These reflections should interest clinical researchers, since locating the line between appropriate and inappropriate use of subjects--the line between exploitation and fair use--is the central question at the heart of research ethics. Reflection on this rich and important moral concept should also interest normative moral philosophers of a non-Marxist bent.</p>
         <p>In addition to the editors, the contributors are Richard J. Arneson, Alisa L. Carse, Margaret Olivia Little, Thomas Pogge, Andrew W. Siegel, and Alan Wertheimer.</p>
      </abstract>
      <counts>
         <page-count count="336"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>[i]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>[vii]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.3</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Why Exploitation?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HAWKINS</surname>
                           <given-names>JENNIFER S.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>EMANUEL</surname>
                           <given-names>EZEKIEL J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The first effective intervention that could dramatically reduce the risk that an HIV-infected woman would pass the virus to her baby was discovered in 1994.¹ The intervention was tested in the AIDS Clinical Trial Group (ACTG) Study 076 and came to be known in the medical community as “the 076 regimen.” This regimen was both costly and complicated, involving large quantities of AZT (the trade name for the drug zidovudine) administered in an elaborate schedule. A pregnant HIV-infected woman receiving the 076 regimen was required to begin treatment in the second trimester of pregnancy, so that she would receive the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Research Ethics, Developing Countries, and Exploitation:</title>
                     <subtitle>A Primer</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HAWKINS</surname>
                           <given-names>JENNIFER S.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>As the AZT trials demonstrated, questions about the ethical conduct of clinical research in developing countries are anything but simple. Informed judgments about the cases in this book, and about future directions for policy, require familiarity with both the details of clinical research and significant moral distinctions. This chapter aims to provide some background material relevant to understanding the complexities of the current debates.</p>
                     <p>The first section provides a basic overview of the fundamental concepts of research ethics as they relate to developing country research—informed consent, randomized controlled trials (RCTs), standard of care, and clinical equipoise. It is intended</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Case Studies:</title>
                     <subtitle>The Havrix Trial and the Surfaxin Trial</subtitle>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>Havrix is an inactivated hepatitis A vaccine that was tested in 1990 among schoolchildren from Kamphaeng Phet province in northern Thailand.¹ The study was a collaboration of Walter Reed Army Institute of Research (U.S. government), SmithKline Beecham Biologicals, and Thailand’s Ministry of Public Health. Initially, there was a randomized, double-blind Phase II study involving 300 children, primarily family members of physicians and nurses at the Kamphaeng Phet provincial hospital. After demonstration of safety and a hepatitis A–neutralizing antibody response, a randomized, double-blind Phase III trial with a hepatitis B vaccine control involving 40,000 children aged one to sixteen was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Exploitation in Clinical Research</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WERTHEIMER</surname>
                           <given-names>ALAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>63</fpage>
                  <abstract>
                     <p>Many bioethicists have claimed that clinical research in underdeveloped societies is vulnerable to the charge of exploitation. Here are some typical statements:</p>
                     <p>1. “Unless the interventions being tested will actually be made available to the impoverished populations that are being used as research subjects, developed countries are simply exploiting them in order to quickly use the knowledge gained from the clinical trials for the developed countries’ own benefit.”¹</p>
                     <p>2. “. . . the placebo-controlled trials are exploitative of poor people, who are being manipulated into serving the interests of those who live in wealthy nations. . .”²</p>
                     <p>3. “. .</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Testing Our Drugs on the Poor Abroad</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>POGGE</surname>
                           <given-names>THOMAS</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>105</fpage>
                  <abstract>
                     <p>Determining whether U.S. companies and some of the persons involved in them are acting ethically when conducting the research described in the Havrix trial and the Surfaxin trial requires reflection on the moral objections that could be raised against what they did. Given the wide range of possible moral objections, it would be folly to try to discuss them all in the space of this chapter. I concentrate, then, on a kind of moral objection that strikes me as especially interesting, plausible, and important. I try to work out whether such objections are valid and, if so, what significance they</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Broadly Utilitarian Theories of Exploitation and Multinational Clinical Research</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ARNESON</surname>
                           <given-names>RICHARD J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>142</fpage>
                  <abstract>
                     <p>Confronted by fine-grained deontological concepts such as exploitation, the consequentialist is a bull in a china shop. Here are many subtly different varieties of exquisite porcelain dishes, but the distinctions do not matter to the bull, and his hooves smash the plates indiscriminately. For many, this situation exhibits the deep flaws of consequentialism. I disagree, and want to defend the bull’s perspective.</p>
                     <p>In the pejorative and morally interesting sense, exploiting a person is using her to one’s advantage in a way that is morally wrongful and that specifically wrongs her. In other words, the one who exploits takes advantage of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Kantian Ethics, Exploitation, and Multinational Clinical Trials</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SIEGEL</surname>
                           <given-names>ANDREW W.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>175</fpage>
                  <abstract>
                     <p>In the recent profusion of literature on the ethics of multinational clinical trials, exploitation has been cast as the central iniquity that rules and guidelines for international research must address. The fundamental concern is with investigators reaping benefits from conditions of social and economic deprivation in developing countries while disregarding the health needs of persons in those countries. This concern is heightened when private industry conducts clinical trials in developing countries, as the salient feature of deprivation within the corporate<italic>Weltansicht</italic>is the opportunity for profit, not the tragic character of existing and perishing in poverty.</p>
                     <p>Although there has been</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Exploitation and the Enterprise of Medical Research</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CARSE</surname>
                           <given-names>ALISA L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>LITTLE</surname>
                           <given-names>MARGARET OLIVIA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>206</fpage>
                  <abstract>
                     <p>According to some, exploitation is a moral red herring. To be sure, it will be said, we know that coercion is wrong. So, too, manipulation, deception, and fraud: all of these undermine the voluntariness of agreement. But why, it will be asked, is there anything morally amiss when genuine consent has been achieved? A seamstress may agree to modest wages, a person may trade sex for money, but just so long as their agreements are based on an undistorted appreciation of potential burdens and benefits, it is morally permissible to pay the wage and take the sex. One might deplore</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Exploitation and Placebo Controls</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HAWKINS</surname>
                           <given-names>JENNIFER S.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>246</fpage>
                  <abstract>
                     <p>Nicole is a young, HIV-positive mother in Côte d’Ivoire, where virtually no treatment is available for the deadly disease HIV causes. In the late 1990s, Nicole was one of thousands of HIV-positive pregnant women from developing countries who participated in clinical trials to see if low-dose AZT could reduce the rate of transmission from mother to child.¹ In 1998, when Nicole was interviewed after the trial by a reporter, she still did not know which arm of the trial (AZT or placebo) she had been in, and she did not know the HIV status of her baby.²</p>
                     <p>The reporter asked</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Addressing Exploitation:</title>
                     <subtitle>Reasonable Availability versus Fair Benefits</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>EMANUEL</surname>
                           <given-names>EZEKIEL J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>286</fpage>
                  <abstract>
                     <p>Over the last decade, clinical research conducted by sponsors and researchers from developed countries in developing countries has been the subject of significant controversy.¹ Debate about perinatal HIV transmission studies in Southeast Asia and Africa, sponsored by the National Institutes of Health (NIH) and Centers for Disease Control (CDC), inflamed this controversy and focused it on the standard of care, that is, whether treatments tested in developing countries should be compared with treatments provided locally or the worldwide best interventions.² Debate about research in developing countries has expanded to include concerns about informed consent. Less discussed but potentially even more</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7s3f4.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>315</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
