<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23212722</article-id>
         <title-group>
            <article-title>Procedures for Collection of Induced Sputum Specimens From Children</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lindsay R.</given-names>
                  <surname>Grant</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Laura L.</given-names>
                  <surname>Hammitt</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David R.</given-names>
                  <surname>Murdoch</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Katherine L.</given-names>
                  <surname>O'Brien</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>J. Anthony</given-names>
                  <surname>Scott</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">54</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23212712</issue-id>
         <fpage>S140</fpage>
         <lpage>S145</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/cid/cir1069"
                   xlink:title="an external site"/>
         <abstract>
            <p>In most settings, sputum is not routinely collected for microbiological diagnosis from children with lower respiratory disease. To evaluate whether it is feasible and diagnostically useful to collect sputum in the Pneumonia Etiology Research for Child Health (PERCH) study, we reviewed the literature on induced sputum procedures. Protocols for induced sputum in children were collated from published reports and experts on respiratory disease and reviewed by an external advisory group for recommendation in the PERCH study. The advisory group compared 6 protocols: 4 followed a nebulization technique using hypertonic saline, and 2 followed a chest or abdomen massage technique. Grading systems for specimen quality were evaluated. Collecting sputum from children with lower respiratory tract illness is feasible and is performed around the world. An external advisory group recommended that sputum be collected from children hospitalized with severe and very severe pneumonia who participate in the PERCH study provided no contraindications exist. PERCH selected the nebulization technique using hypertonic saline.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1898e166a1310">
            <label>1</label>
            <mixed-citation id="d1898e173" publication-type="other">
Levine OS. Overview of the Pneumonia Etiology Research for Child
Health project. Clin Infect Dis 2012; 54(Suppl 2): S93-101.</mixed-citation>
         </ref>
         <ref id="d1898e183a1310">
            <label>2</label>
            <mixed-citation id="d1898e190" publication-type="other">
The PERCH Core Team. Introduction. Clin Infect Dis 2012;
54(Suppl 2): S87-8.</mixed-citation>
         </ref>
         <ref id="d1898e200a1310">
            <label>3</label>
            <mixed-citation id="d1898e207" publication-type="other">
Neill AM, Martin IR, Weir R, et al. Community acquired pneumonia:
aetiology and usefulness of severity criteria on admission. Thorax
1996; 51:1010-16.</mixed-citation>
         </ref>
         <ref id="d1898e220a1310">
            <label>4</label>
            <mixed-citation id="d1898e227" publication-type="other">
Laing R, Slater W, Coles C, et al. Community-acquired pneumonia
in Christchurch and Waikato 1999-2000: microbiology and epide-
miology. N Z Med J 2001; 114:488-92.</mixed-citation>
         </ref>
         <ref id="d1898e241a1310">
            <label>5</label>
            <mixed-citation id="d1898e248" publication-type="other">
Anevlavis S, Petroglou N, Tzavaras A, et al. A prospective study of
the diagnostic utility of sputum Gram stain in pneumonia. J Infect
2009; 59:83-9.</mixed-citation>
         </ref>
         <ref id="d1898e261a1310">
            <label>6</label>
            <mixed-citation id="d1898e268" publication-type="other">
Millar EV, Watt JP, Bronsdon MA, et al. Indirect effect of 7-valent
pneumococcal conjugate vaccine on pneumococcal colonization among
unvaccinated household members. Clin Infect Dis 2008; 47:989-96.</mixed-citation>
         </ref>
         <ref id="d1898e281a1310">
            <label>7</label>
            <mixed-citation id="d1898e288" publication-type="other">
Abdullahi O, Nyiro J, Lewa P, Slack M, Scott JA. The descriptive epi-
demiology of Streptococcus pneumoniae and Haemophilus influenzae
nasopharyngeal carriage in children and adults in Kilifi district, Kenya.
Pediatr Infect Dis J 2008; 27:59-64.</mixed-citation>
         </ref>
         <ref id="d1898e304a1310">
            <label>8</label>
            <mixed-citation id="d1898e311" publication-type="other">
Masur H, Gill VJ, Ognibene FP, Shelhamer J, Godwin C, Kovacs JA.
Diagnosis of Pneumocystis pneumonia by induced sputum technique
in patients without the acquired immunodeficiency syndrome. Ann
Intern Med 1988; 109:755-6.</mixed-citation>
         </ref>
         <ref id="d1898e327a1310">
            <label>9</label>
            <mixed-citation id="d1898e334" publication-type="other">
Mofenson LM, Brady MT, Danner SP, et al. Centers for Disease
Control and Prevention, National Institutes of Health, HIV Medicine
Association of the Infectious Disease Society of America, Pediatric
Infectious Diseases Society, American Academy of Pediatrics. Guide-
lines for the prevention and treatment of opportunistic infections
among HIV-exposed and HIV-infected children: recommendations
from CDC, the National Institutes of Health, the HIV Medicine As-
sociation of the Infectious Diseases Society of America, the Pediatric
Infectious Diseases Society, and the American Academy of Pediatrics.
MMWR Recomm Rep 2009; 58:1-166.</mixed-citation>
         </ref>
         <ref id="d1898e369a1310">
            <label>10</label>
            <mixed-citation id="d1898e376" publication-type="other">
Zar HJ, Dechaboon A, Hanslo D, Apolles P, Magnus KG, Hussey G.
Pneumocystis jiroveci pneumonia in South African children infected
with human immunodeficiency virus. Pediatr Infect Dis J 2000; 19:
603-7.</mixed-citation>
         </ref>
         <ref id="d1898e393a1310">
            <label>11</label>
            <mixed-citation id="d1898e400" publication-type="other">
Zar HJ, Tannenbaum E, Apolles P, Roux P, Hanslo D, Hussey G.
Sputum induction for the diagnosis of pulmonary tuberculosis in
infants and young children in an urban setting in South Africa. Arch
Dis Child 2000; 82:305-8.</mixed-citation>
         </ref>
         <ref id="d1898e416a1310">
            <label>12</label>
            <mixed-citation id="d1898e423" publication-type="other">
Mor SM, Tumwine JK, Ndeezi G, et al. Respiratory cryptosporidiosis
in HIV-seronegative children in Uganda: potential for respiratory
transmission. Clin Infect Dis 2010; 50:1366-72.</mixed-citation>
         </ref>
         <ref id="d1898e436a1310">
            <label>13</label>
            <mixed-citation id="d1898e443" publication-type="other">
Zar HJ, Apolles P, Argent A, et al. The etiology and outcome of
pneumonia in human immunodeficiency virus-infected children
admitted to intensive care in a developing country. Pediatr Crit Care
Med 2001; 2:108-12.</mixed-citation>
         </ref>
         <ref id="d1898e459a1310">
            <label>14</label>
            <mixed-citation id="d1898e466" publication-type="other">
Kiwanuka J, Graham SM, Coulter JB, et al. Diagnosis of pulmonary
tuberculosis in children in an HIV-endemic area, Malawi. Ann Trop
Paediatr 2001; 21:5-14.</mixed-citation>
         </ref>
         <ref id="d1898e479a1310">
            <label>15</label>
            <mixed-citation id="d1898e486" publication-type="other">
Shata AM, Coulter JB, Parry CM, Ching'ani G, Broadhead RL, Hart CA.
Sputum induction for the diagnosis of tuberculosis. Arch Dis Child
1996; 74:535-7.</mixed-citation>
         </ref>
         <ref id="d1898e499a1310">
            <label>16</label>
            <mixed-citation id="d1898e506" publication-type="other">
Al-Saleh S, Dell SD, Grasemann H, et al. Sputum induction in routine
clinical care of children with cystic fibrosis. J Pediatr 2010; 157:1006-11.</mixed-citation>
         </ref>
         <ref id="d1898e517a1310">
            <label>17</label>
            <mixed-citation id="d1898e524" publication-type="other">
Jones PD, Hankin R, Simpson J, Gibson PG, Henry RL. The toler-
ability, safety, and success of sputum induction and combined
hypertonic saline challenge in children. Am J Respir Crit Care Med
2001; 164:1146-9.</mixed-citation>
         </ref>
         <ref id="d1898e540a1310">
            <label>18</label>
            <mixed-citation id="d1898e547" publication-type="other">
Lex C, Payne DN, Zacharasiewicz A, et al. Sputum induction in
children with difficult asthma: safety, feasibility, and inflammatory
cell pattern. Pediatr Pulmonol 2005; 39:318-24.</mixed-citation>
         </ref>
         <ref id="d1898e560a1310">
            <label>19</label>
            <mixed-citation id="d1898e567" publication-type="other">
Covar RA, Spahn JD, Martin RJ, et al. Safety and application of induced
sputum analysis in childhood asthma. J Allergy Clin Immunol 2004;
114:575-82.</mixed-citation>
         </ref>
         <ref id="d1898e580a1310">
            <label>20</label>
            <mixed-citation id="d1898e587" publication-type="other">
Gibson PG, Henry RL, Thomas P. Noninvasive assessment of airway
inflammation in children: induced sputum, exhaled nitric oxide, and
breath condensate. Eur Respir J 2000; 16:1008-15.</mixed-citation>
         </ref>
         <ref id="d1898e600a1310">
            <label>21</label>
            <mixed-citation id="d1898e607" publication-type="other">
Gilani Z, Kwong YD, Levine OS, et al. A landscape analysis of recent
and ongoing childhood pneumonia etiology studies. Clin Infect Dis
2012; This issue.</mixed-citation>
         </ref>
         <ref id="d1898e620a1310">
            <label>22</label>
            <mixed-citation id="d1898e627" publication-type="other">
Lahti E, Peltola V, Waris M, et al. Induced sputum in the diagnosis of
community-acquired pneumonia. Thorax 2009; 64:252-7.</mixed-citation>
         </ref>
         <ref id="d1898e638a1310">
            <label>23</label>
            <mixed-citation id="d1898e645" publication-type="other">
Ballieux S, Lopes D. La Bronchiolite du nourrisson. La Kinésithérapie
respiratoire par augmentation du flux expiratoire: une evidence? [Bron-
cholitis respiratory kinesitherapy by increasing expiratory flow: proof?]
Kinésithérapie scientifique 2008; 484:5-17.</mixed-citation>
         </ref>
         <ref id="d1898e661a1310">
            <label>24</label>
            <mixed-citation id="d1898e668" publication-type="other">
Von Essen SG, Robbins RA, Spurzem JR, Thompson AB,
McGranaghan SS, Rennard SI. Bronchoscopy with bronchoalveolar
lavage causes neutrophil recruitment to the lower respiratory tract.
Am Rev Respir Dis 1991; 144:848-54.</mixed-citation>
         </ref>
         <ref id="d1898e684a1310">
            <label>25</label>
            <mixed-citation id="d1898e691" publication-type="other">
Mermond S, Zurawski V, D'Ortenzio E, et al. Lower respira-
tory infections among hospitalized children in New Caledonia
and relevance of induced sputum analysis. Clin Infect Dis 2012;
54(Suppl 2): Si80-89.</mixed-citation>
         </ref>
         <ref id="d1898e707a1310">
            <label>26</label>
            <mixed-citation id="d1898e714" publication-type="other">
Hammitt L, Kazungu S, Morpeth SC, et al. A preliminary study of
pneumonia etiology among hospitalized children in Kenya. Clin Infect
Dis 2012; 54(Suppl 2): S190-9.</mixed-citation>
         </ref>
         <ref id="d1898e727a1310">
            <label>27</label>
            <mixed-citation id="d1898e734" publication-type="other">
Bartlett RC, ed. Medical microbiology: Quality cost and clinical rele-
vance. New York: John Wiley &amp; Soms, 1974.</mixed-citation>
         </ref>
         <ref id="d1898e744a1310">
            <label>28</label>
            <mixed-citation id="d1898e751" publication-type="other">
Murray PR, Washington JA II. Microscopic and bacteriologic analysis
of expectorated sputum. Mayo Clin Proc 1975; 50:339—44.</mixed-citation>
         </ref>
         <ref id="d1898e762a1310">
            <label>29</label>
            <mixed-citation id="d1898e769" publication-type="other">
Wong LK, Barry AL, Horgan SM. Comparison of six different criteria
for judging the acceptability of sputum specimens. J Clin Microbiol
1982; 16:627-31.</mixed-citation>
         </ref>
         <ref id="d1898e782a1310">
            <label>30</label>
            <mixed-citation id="d1898e789" publication-type="other">
Honkinen M, Lahti E, Òsterback R, Ruuskanen O, Waris M. Viruses and
bacteria in sputum samples of children with community-acquired pneu-
monia. Clin Microbiol Infect 2011; doi: 10.1111/j.1469-0691.2011.03603.x.</mixed-citation>
         </ref>
         <ref id="d1898e802a1310">
            <label>31</label>
            <mixed-citation id="d1898e809" publication-type="other">
Hammitt LL, O'Brien KL, Murdoch DR. Specimen collection for
pneumonia etiology investigations in children. Clin Infect Dis 2012;
54(Suppl 2): SI32-9.</mixed-citation>
         </ref>
         <ref id="d1898e822a1310">
            <label>32</label>
            <mixed-citation id="d1898e829" publication-type="other">
Musher DM, Montoya R, Wanahita A. Diagnostic value of microscopic
examination of Gram-stained sputum and sputum cultures in patients
with bacteremic pneumococcal pneumonia. Clin Infect Dis 2004;
39:165-9.</mixed-citation>
         </ref>
         <ref id="d1898e845a1310">
            <label>33</label>
            <mixed-citation id="d1898e852" publication-type="other">
Iriso R, Mudido PM, Karamagi C, Whalen C. The diagnosis of child-
hood tuberculosis in an HIV-endemic setting and the use of induced
sputum. Int J Tuberc Lung Dis 2005; 9:716-26.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
