<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">philtranmathphys</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100915</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Philosophical Transactions: Mathematical, Physical and Engineering Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">1364503X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41148901</article-id>
         <title-group>
            <article-title>Global health and climate change: moving from denial and catastrophic fatalism to positive action</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anthony</given-names>
                  <surname>Costello</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mark</given-names>
                  <surname>Maslin</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Hugh</given-names>
                  <surname>Montgomery</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anne M.</given-names>
                  <surname>Johnson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Paul</given-names>
                  <surname>Ekins</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>13</day>
            <month>5</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">369</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1942</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40050960</issue-id>
         <fpage>1866</fpage>
         <lpage>1882</lpage>
         <permissions>
            <copyright-statement>COPYRIGHT © 2011 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41148901"/>
         <abstract>
            <p>The health effects of climate change have had relatively little attention from climate scientists and governments. Climate change will be a major threat to population health in the current century through its potential effects on communicable disease, heat stress, food and water security, extreme weather events, vulnerable shelter and population migration. This paper addresses three health-sector strategies to manage the health effects of climate change— promotion of mitigation, tackling the pathways that lead to ill-health and strengthening health systems. Mitigation of greenhouse gas (GHG) emissions is affordable, and low-carbon technologies are available now or will be in the near future. Pathways to ill-health can be managed through better information, poverty reduction, technological innovation, social and cultural change and greater coordination of national and international institutions. Strengthening health systems requires increased investment in order to provide effective public health responses to climate-induced threats to health, equitable treatment of illness, promotion of lowcarbon lifestyles and renewable energy solutions within health facilities. Mitigation and adaptation strategies will produce substantial benefits for health, such as reductions in obesity and heart disease, diabetes, stress and depression, pneumonia and asthma, as well as potential cost savings within the health sector. The case for mitigating climate change by reducing GHGs is overwhelming. The need to build population resilience to the global health threat from already unavoidable climate change is real and urgent. Action must not be delayed by contrarians, nor by catastrophic fatalists who say it is all too late.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d105e191a1310">
            <label>1</label>
            <mixed-citation id="d105e198" publication-type="other">
McMichael, A. J., Neira, M., Bertollini, R., Campbell-Lendrum, D. к Hales, S. 2009 Climate
change: a time of need and opportunity for the health sector. Lancet 374, 2123-2125. (doi:10.
1016/S0140-6736(09)62031-6)</mixed-citation>
         </ref>
         <ref id="d105e211a1310">
            <label>2</label>
            <mixed-citation id="d105e218" publication-type="other">
Costello, A. et al 2009 Managing the health effects of climate change: Lancet and University
College London Institute for Global Health Commission. Lancet 373, 1693-1733. (doi:10.1016/
S0140-6736(09)60935-1)</mixed-citation>
         </ref>
         <ref id="d105e231a1310">
            <label>3</label>
            <mixed-citation id="d105e238" publication-type="other">

              Maslin, M. A. 2008 Global warming: a very short introduction, p. 192, 2
              
              edn. Oxford, UK:
            
Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d105e251a1310">
            <label>4</label>
            <mixed-citation id="d105e258" publication-type="other">
Gleick, P. H. et al. 2010 Climate change and the integrity of science. Science 328, 689-690.
(doi:10.1126/science.328.5979.689)</mixed-citation>
         </ref>
         <ref id="d105e269a1310">
            <label>5</label>
            <mixed-citation id="d105e276" publication-type="other">
IPCC. 2007 Climate change 2007: impacts, adaptation, and vulnerability. In Contribution of
Working Group II to the Fourth Assessment Report of the Intergovernmental Panel on Climate
Chanae feds M. L. Parrv et al). Cambridge, UK: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d105e289a1310">
            <label>6</label>
            <mixed-citation id="d105e296" publication-type="other">
Turley, С et al. 2010 The societal challenge of ocean acidification. Mar. Pollut. Bull 60, 787-
792. (doi:10.1016/j.marpolbul.2010.05.006)</mixed-citation>
         </ref>
         <ref id="d105e306a1310">
            <label>7</label>
            <mixed-citation id="d105e313" publication-type="other">
Sherwood, S. C. к Huber, M. 2010 An adaptability limit to climate change due to heat stress.
Proc. NatlAcad. Sci. USA 107, 9552-9555. (doi:10.1073/pnas.0913352107)</mixed-citation>
         </ref>
         <ref id="d105e323a1310">
            <label>8</label>
            <mixed-citation id="d105e330" publication-type="other">
Hiller brand, R. к Ghil, M. 2008 Anthropogenic climate change: scientific uncertainties and
moral dilemmas. Physica D 237, 2132-2138. (doi:10.1016/j.physd.2008.02.015)</mixed-citation>
         </ref>
         <ref id="d105e340a1310">
            <label>9</label>
            <mixed-citation id="d105e347" publication-type="other">
Lewis, S. L., Brando, P. M., Phillips, O. L., van der Heijden, G. M. F. к Nepstad, D. 2011 The
2010 Amazon drought. Science 331, 554. (doi: 10. 1126/science. 1200807)</mixed-citation>
         </ref>
         <ref id="d105e357a1310">
            <label>10</label>
            <mixed-citation id="d105e364" publication-type="other">
Cuvier, G. 1823 Recherches sur les ossemens fossiles, où l'on rétablit les caractères de plusieurs
animaux, dont les révolutions du globe ont détruit les espèces. Paris, France: Dufour et
D'Ocagne.</mixed-citation>
         </ref>
         <ref id="d105e378a1310">
            <label>11</label>
            <mixed-citation id="d105e385" publication-type="other">
Gould, S. J. к Eldredge, N. 1977 Punctuated equilibria: the tempo and mode of evolution
reconsidered. Paleobiology 3, 115-151.</mixed-citation>
         </ref>
         <ref id="d105e395a1310">
            <label>12</label>
            <mixed-citation id="d105e402" publication-type="other">
Costello, А. &amp; Maslin, M. 2008 Apocalypse now? Lancet 372, 105-106. (doi:10.1016/S0140-
6736(08)61017-X)</mixed-citation>
         </ref>
         <ref id="d105e412a1310">
            <label>13</label>
            <mixed-citation id="d105e419" publication-type="other">
Loh, J., Green, R. E., Ricketts, T., Lamoreux, J., Jenkins, M., Kapos, V. &amp;: Randers, J. 2005
The living planet index: using species population time series to track trends in biodiversity.
Phil Trans. R. Soc. В 360, 289-295. (doi:10.1098/rstb.2004.1584)</mixed-citation>
         </ref>
         <ref id="d105e432a1310">
            <label>14</label>
            <mixed-citation id="d105e439" publication-type="other">
Thomas, C. D. et al 2004 Extinction risk from climate change. Nature 427, 145-148. (doi: 10.
1038/nature02121)</mixed-citation>
         </ref>
         <ref id="d105e449a1310">
            <label>15</label>
            <mixed-citation id="d105e456" publication-type="other">
Botkin, D. B. et al 2007 Forecasting the effects of global warming on biodiversity. BioScience
57, 227-236. (doi:10.1641/B570306)</mixed-citation>
         </ref>
         <ref id="d105e466a1310">
            <label>16</label>
            <mixed-citation id="d105e473" publication-type="other">
Pearson, R. G. et al 2006 Model-based uncertainty in species' range prediction. J. Biogeogr.
33, 1704-1711. (doi:10.1111/j.l365-2699.2006.01460.x)</mixed-citation>
         </ref>
         <ref id="d105e484a1310">
            <label>17</label>
            <mixed-citation id="d105e491" publication-type="other">
Lovelock, J. 2009 The vanishing face of Gaia: a final warning. London, UK: Allen Lane.</mixed-citation>
         </ref>
         <ref id="d105e498a1310">
            <label>18</label>
            <mixed-citation id="d105e505" publication-type="other">
Lovelock, J. 2006 The Earth is about to catch a morbid fever that may last as long as 100000
years. The Independent, 16 January 2006.</mixed-citation>
         </ref>
         <ref id="d105e515a1310">
            <label>19</label>
            <mixed-citation id="d105e522" publication-type="other">
McGuire, W. 2008 Seven years to save the planet: the questions and the answers, p. 28, new
edn. London, UK: Weidenfeld &amp; Nicolson.</mixed-citation>
         </ref>
         <ref id="d105e532a1310">
            <label>20</label>
            <mixed-citation id="d105e539" publication-type="other">
Maslin, M. A., Mahli, Y., Phillips, O. &amp; Cowling, S. 2005 New views on an old forest: assessing
the longevity, resilience and future of the Amazon rainforest. Trans. Inst. Br. Geogr. 30,
477-499. (doi:10.1111/j. 1475-5661. 2005.00181.x)</mixed-citation>
         </ref>
         <ref id="d105e552a1310">
            <label>21</label>
            <mixed-citation id="d105e559" publication-type="other">
Dunkley Jones, T., Ridgwell, A., Lunt, D. J., Maslin, M. A., Schmidt, D. N. к Valdes, P. J. 2010
A Paleocene perspective on climate sensitivity and methane hydrate instability. Phil. Trans.
R. Soc. A 368, 2395-2415. (doi:10.1098/rsta.2010.0053)</mixed-citation>
         </ref>
         <ref id="d105e572a1310">
            <label>22</label>
            <mixed-citation id="d105e579" publication-type="other">
Maslin, M. A., Owen, M., Betts, R., Day, S., Dunkley Jones, Т. к Ridgwell, A. 2010 Gas
hydrates: past and future geohazard? Phil. Trans. R. Soc. A 368, 2369-2393. (doi:10.1098/rsta.
2010.0065)</mixed-citation>
         </ref>
         <ref id="d105e593a1310">
            <label>23</label>
            <mixed-citation id="d105e600" publication-type="other">
Stern, N. 2008 The economics of climate change: the Stern Review, p. 120. Cambridge, UK:
Cambridge Universitv Press.</mixed-citation>
         </ref>
         <ref id="d105e610a1310">
            <label>24</label>
            <mixed-citation id="d105e617" publication-type="other">
IEA (International Energy Agency). 2009 World energy outlook 2009. Paris, France: IEA.</mixed-citation>
         </ref>
         <ref id="d105e624a1310">
            <label>25</label>
            <mixed-citation id="d105e631" publication-type="other">
IEA (International Energy Agency). 2010 Energy technology perspectives. Paris, France: IEA.</mixed-citation>
         </ref>
         <ref id="d105e638a1310">
            <label>26</label>
            <mixed-citation id="d105e645" publication-type="other">
Barker, T., Qureshi, M. S. к Köhler, J. 2006 The costs of greenhouse gas mitigation with
induced technological change: a meta-analysis of estimates in the literature. Tyndall Centre for
Climate Change Research, Working Paper No. 89. Tyndall Centre, University of East Anglia,
Norwich, UK.</mixed-citation>
         </ref>
         <ref id="d105e661a1310">
            <label>27</label>
            <mixed-citation id="d105e668" publication-type="other">
Ekins, P. к Speck, S. (eds) 2011 Environmental tax reform: a policy for green growth. Oxford,
UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d105e678a1310">
            <label>28</label>
            <mixed-citation id="d105e685" publication-type="other">
Müller, В. 2010 The reformed financial mechanism of the UNFCCC: post Copenhagen
architecture and governance. European Capacity Briefing Initiative. See http://www.oxf ord
climatepolicv.orff/publications/documents/ecbiRFM2final.pdf (accessed 30 July 2010).</mixed-citation>
         </ref>
         <ref id="d105e699a1310">
            <label>29</label>
            <mixed-citation id="d105e706" publication-type="other">
Armstrong, L. 2011 Towards a sustainable energy future: realities and opportunities. Phil.
Trans. R. Soc. A 369, 1857-1865. (doi: 10. 1098/rsta.2011.0008)</mixed-citation>
         </ref>
         <ref id="d105e716a1310">
            <label>30</label>
            <mixed-citation id="d105e723" publication-type="other">
Jones, J. D. G. 2011 Why genetically modified crops? Phil. Trans. R. Soc. A 369, 1807-1816.
(doi:10.1098/rsta.2010.0345)</mixed-citation>
         </ref>
         <ref id="d105e733a1310">
            <label>31</label>
            <mixed-citation id="d105e740" publication-type="other">
Peter, L. M. 2011 Towards sustainable photovolt aies: the search for new materials. Phil. Trans.
R. Soc. A 369, 1840-1856. (doi:10.1098/rsta.2010.0348)</mixed-citation>
         </ref>
         <ref id="d105e750a1310">
            <label>32</label>
            <mixed-citation id="d105e757" publication-type="other">
Haines, A. et al. 2009 Public health benefits of strategies to reduce greenhouse-gas emissions:
overview and implications for policy makers. Lancet 374, 2104-2114. (doi:10.1016/S0140-
6736(09)61759-1)</mixed-citation>
         </ref>
         <ref id="d105e770a1310">
            <label>33</label>
            <mixed-citation id="d105e777" publication-type="other">
Haines, A., Wilkinson, P., Tonne, С. &amp; Roberts, I. 2009 Aligning climate change and public
health policies. Lancet 374, 2035-2038. (doi:10.1016/S0140-6736(09)61667-6)</mixed-citation>
         </ref>
         <ref id="d105e787a1310">
            <label>34</label>
            <mixed-citation id="d105e794" publication-type="other">
Friel, S. et al 2009 Public health benefits of strategies to reduce greenhouse-gas emissions: food
and agriculture. Lancet 374, 2016-2025. (doi:10.1016/S0140-6736(09)61753-0)</mixed-citation>
         </ref>
         <ref id="d105e805a1310">
            <label>35</label>
            <mixed-citation id="d105e812" publication-type="other">
Woodcock, J. et al. 2009 Public health benefits of strategies to reduce greenhouse-gas emissions:
urban land transport. Lancet 374, 1930-1943. (doi:10.1016/S0140-6736(09)61714-1)</mixed-citation>
         </ref>
         <ref id="d105e822a1310">
            <label>36</label>
            <mixed-citation id="d105e829" publication-type="other">
Wilkinson, P. et al 2009 Public health benefits of strategies to reduce greenhouse-gas emissions:
household energy. Lancet 374, 1917-1929. (doi:10.1016/S0140-6736(09)61713-X)</mixed-citation>
         </ref>
         <ref id="d105e839a1310">
            <label>37</label>
            <mixed-citation id="d105e846" publication-type="other">
Kjellstrom, T., Holmer, I. k Lemke, В. 2009 Workplace heat stress, health and productivity -
an increasing challenge for low and middle-income countries during climate change. Glob. Health
Action 2. (doi:10.3402/ffha.v2i0.2047)</mixed-citation>
         </ref>
         <ref id="d105e859a1310">
            <label>38</label>
            <mixed-citation id="d105e866" publication-type="other">
D'Ippoliti, D. et al. 2010 The impact of heat waves on mortality in 9 European cities: results
from the EuroHEAT project. Environ. Health 9, 37. (doi:10.1186/1476-069X-9-37)</mixed-citation>
         </ref>
         <ref id="d105e876a1310">
            <label>39</label>
            <mixed-citation id="d105e883" publication-type="other">
Snow, R. W. к Marsh, K. 2010 Malaria in Africa: progress and prospects in the decade since
the Abuja Declaration. Lancet 376, 137-139. (doi:10.1016/S0140-6736(10)60577-6)</mixed-citation>
         </ref>
         <ref id="d105e893a1310">
            <label>40</label>
            <mixed-citation id="d105e900" publication-type="other">
Gething, P. W., Smith, D. L., Patii, A. P., Tatem, A. J., Snow, R. W. к Hay, S. I. 2010 Climate
change and the global malaria recession. Nature 465, 342-345. (doi:10.1038/nature09098)</mixed-citation>
         </ref>
         <ref id="d105e911a1310">
            <label>41</label>
            <mixed-citation id="d105e918" publication-type="other">
Jones, K. E., Patel, N. G., Levy, M. A., Storeygard, A., Balk, D., Gittleman, J. L. к Daszak,
P. 2008 Global trends in emerging infectious diseases. Nature 451, 990-993. (doi: 10. 1038/
nature06536)</mixed-citation>
         </ref>
         <ref id="d105e931a1310">
            <label>42</label>
            <mixed-citation id="d105e938" publication-type="other">
Schmidt, К. А. к Osterfeld, R. S. 2001 Biodiversity and the dilution effect in disease ecology.
Ecology 82, 609-619. (doi:10.1890/0012-9658(2001)082[0609:BATDEI]2.0.CO;2)</mixed-citation>
         </ref>
         <ref id="d105e948a1310">
            <label>43</label>
            <mixed-citation id="d105e955" publication-type="other">
Suzán, G., Mareé, E., Giermakowski, J. T., Mills, J. N., Ceballos, G., Ostfeld, R. S., Armién, В.,
Pascale, J. M. &amp; Yates, T. L. 2009 Experimental evidence for reduced rodent diversity causing
increased hantavirus prevalence. PLoS ONE 4, e5461. (doi: 10. 1371 /journal. pone. 0005461)</mixed-citation>
         </ref>
         <ref id="d105e968a1310">
            <label>44</label>
            <mixed-citation id="d105e975" publication-type="other">
Avert. 2009 Global HIV and AIDS estimates. See http://www.avert.org/worldstats.htm
(accessed 25 October 2010).</mixed-citation>
         </ref>
         <ref id="d105e985a1310">
            <label>45</label>
            <mixed-citation id="d105e992" publication-type="other">
FAO Terrastat. 2010 Land resource potential and constraints statistics at country and regional
level. See http: //www.fao.ore/aff/ad/aecll/terrastat /#terrastatdb/.</mixed-citation>
         </ref>
         <ref id="d105e1002a1310">
            <label>46</label>
            <mixed-citation id="d105e1009" publication-type="other">
Battisti, D. S. &amp; Nay lor, R. L. 2009 Historical warnings of future food insecurity with
unprecedented seasonal heat. Science 323. 240-244. fdoi: 10. 1126 /science. 1164363)</mixed-citation>
         </ref>
         <ref id="d105e1020a1310">
            <label>47</label>
            <mixed-citation id="d105e1027" publication-type="other">
Lobell, D. В., Burke, M. В., Tebaldi, C., Mastrandrea, M. D., Falcon, W. P. к Naylor, R. L. 2008
Prioritizing climate change adaptation needs for food security in 2030. Science 319, 607-610.
(doi: 10. 1 m/science. 1152339)</mixed-citation>
         </ref>
         <ref id="d105e1040a1310">
            <label>48</label>
            <mixed-citation id="d105e1047" publication-type="other">
Sen, A. 1982 Poverty and famines: an essay on entitlements and deprivation. Oxford, UK:
Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d105e1057a1310">
            <label>49</label>
            <mixed-citation id="d105e1064" publication-type="other">
India Environment Portal. 2009 State of Environment Report. See http://www.mdia
environmentportal.org.in/content/state-environment-report-india-2009 (accessed 23 August
2010).</mixed-citation>
         </ref>
         <ref id="d105e1077a1310">
            <label>50</label>
            <mixed-citation id="d105e1084" publication-type="other">
Dreze, J. 2010 The task of making: the PDS work. The Hindu 8 Julv 2010.</mixed-citation>
         </ref>
         <ref id="d105e1091a1310">
            <label>51</label>
            <mixed-citation id="d105e1098" publication-type="other">
Conway, G. к Waage, J. 2010 Science and innovation for development. UK Collaborative on
Development Sciences.</mixed-citation>
         </ref>
         <ref id="d105e1108a1310">
            <label>52</label>
            <mixed-citation id="d105e1115" publication-type="other">
Costello, A. 2010 Harnessing science for global health. Lancet 375, 1599-1600. (doi: 10. 1016/
S0140-6736(10)60688-5)</mixed-citation>
         </ref>
         <ref id="d105e1126a1310">
            <label>53</label>
            <mixed-citation id="d105e1133" publication-type="other">
Economist. 2009 Science &amp;; Technology. Cheaper desalination. A fresh way to take the salt out
of seawater. Economist, 29 October 2009.</mixed-citation>
         </ref>
         <ref id="d105e1143a1310">
            <label>54</label>
            <mixed-citation id="d105e1150" publication-type="other">
Godfray, H. С J. et al 2010 The future of the global food system Phil. Trans. R. Soc. В 365,
2769-2777. (doi:10.1098/rstb.2010.0180)</mixed-citation>
         </ref>
         <ref id="d105e1160a1310">
            <label>55</label>
            <mixed-citation id="d105e1167" publication-type="other">
Crane, B. p. &amp; Dusenberry, J. 2004 Power and politics m international funding for reproductive
health: the US Global Gag Rule. Reprod. Health Matters 12, 128-137. (doi:10.1016/S0968-
8080(04)24140-4)</mixed-citation>
         </ref>
         <ref id="d105e1180a1310">
            <label>56</label>
            <mixed-citation id="d105e1187" publication-type="other">
Greco, G., Powell-Jackson, T., Borghi, J. &amp; Mills, A. 2008 Countdown to 2015: assessment of
donor assistance to maternal, newborn, and child health between 2003 and 2006. Lancet 371,
1268-1275. fdoi:10.1016/S0140-6736f08)60561-9)</mixed-citation>
         </ref>
         <ref id="d105e1200a1310">
            <label>57</label>
            <mixed-citation id="d105e1207" publication-type="other">
Wadhams, Ň. 2010 Progress in Rwanda's drive to slow population growth. Lancet 376, 81-82.
doi:10.1016/S0140-6736(10)61063-X)</mixed-citation>
         </ref>
         <ref id="d105e1217a1310">
            <label>58</label>
            <mixed-citation id="d105e1224" publication-type="other">
Lutz, W. &amp; Samir, К. С 2010 Dimensions of global population projections: what do we
know about future population trends and structures? Phil. Trans. R. Soc. В 365, 2779-2791.
ídoi:10.1098/rstb.2010.0133)</mixed-citation>
         </ref>
         <ref id="d105e1238a1310">
            <label>59</label>
            <mixed-citation id="d105e1245" publication-type="other">
Strzepek, К. &amp; Boehlert, В. 2010 Competition for water for the food system. Phil. Trans. R.
Soc. В 365, 2927-2940. (doi:10.1098/rstb.2010.0152)</mixed-citation>
         </ref>
         <ref id="d105e1255a1310">
            <label>60</label>
            <mixed-citation id="d105e1262" publication-type="other">
Economist. 2010 Security and the environment. Climate wars. Does a warming world really
mean that more conflict is inevitable? Economist, 8 July 2010.</mixed-citation>
         </ref>
         <ref id="d105e1272a1310">
            <label>61</label>
            <mixed-citation id="d105e1279" publication-type="other">
Emanuel, К. 2005 Increasing destructiveness of tropical cyclones over the past 30 years. Nature
436, 686-688. (doi:10.1038/nature03906)</mixed-citation>
         </ref>
         <ref id="d105e1289a1310">
            <label>62</label>
            <mixed-citation id="d105e1296" publication-type="other">
Webster, P. J., Holland, G. J., Curry, J. А. к Chang, H. R. 2005 Changes in tropical cyclone
number, duration, and intensity in a warming environment. Science 309, 1844-1846. (doi:
10. 1126 /science. 1116448)</mixed-citation>
         </ref>
         <ref id="d105e1309a1310">
            <label>63</label>
            <mixed-citation id="d105e1316" publication-type="other">
Dasgupta, S., Huq, M., Khan, Z. H., Ahmed, M. M. Z., Mukherjee, N., Khan, M. F. к Pandey,
K. D. 2010 Vulnerability of Bangladesh to cyclones in a changing climate: potential damages
and adaptation cost. World Bank Policy Research Working Paper No. 5280. Available at SSRN.
See httn: //ssrn.com /abstract=1596490</mixed-citation>
         </ref>
         <ref id="d105e1332a1310">
            <label>64</label>
            <mixed-citation id="d105e1339" publication-type="other">
Rahmstorf, S. 2010 A new view on sea level rise. Has the IPCC underestimated the risk of sea
level rise? Nat. Rev. Climate Chanae 4. (doi: 10. 1038 /climate.2010.29)</mixed-citation>
         </ref>
         <ref id="d105e1350a1310">
            <label>65</label>
            <mixed-citation id="d105e1357" publication-type="other">
Dwyer, G. S. к Chandler, M. A. 2009 Mid-Pliocene sea level and continental ice volume based
on coupled benthic Mg/Ca palaeotemperatures and oxygen isotopes. Phil. Trans. R. Soc. A
367, 157-168. (doi:10.1098/rsta.2008.0222)</mixed-citation>
         </ref>
         <ref id="d105e1370a1310">
            <label>66</label>
            <mixed-citation id="d105e1377" publication-type="other">
Marmot, M., Friel, S., Bell, R., Houweling, T. A. J. к Taylor, S. (on behalf of the Commission
on Social Determinants of Health). 2008 Closing the gap in a generation: health equity
through action on the social determinants of health. Lancet 372, 1661-1669. (doi:10.1016/
S0140-6736(08)61690-6)</mixed-citation>
         </ref>
         <ref id="d105e1393a1310">
            <label>67</label>
            <mixed-citation id="d105e1400" publication-type="other">
Department of Health. 2010 Climate Change Plan. See http://www.dh.gov.uk/prod_consum_
dh/groups/dh diffitalassets/@dh/@en/@ps/documents/digitalasset/dh 114995.pdf.</mixed-citation>
         </ref>
         <ref id="d105e1410a1310">
            <label>68</label>
            <mixed-citation id="d105e1417" publication-type="other">
NHS Sustainable Development Unit. 2009 Fit for the Future. See http://www.sdu.nhs.uk/
publications-resources/4/Fit-for-the-Future-/.</mixed-citation>
         </ref>
         <ref id="d105e1427a1310">
            <label>69</label>
            <mixed-citation id="d105e1434" publication-type="other">
Griffiths, J., Rao, M., Adshead, F. &amp; Thorpe, A. (eds) 2009 The health practitioner's guide to
climate change diagnosis and cure. London, UK: Earthscan.</mixed-citation>
         </ref>
         <ref id="d105e1444a1310">
            <label>70</label>
            <mixed-citation id="d105e1451" publication-type="other">
Rajaratnam, J. K., Marcus, J. R., Flaxman, A. D., Wang, H., Levin-Rector, A., Dwyer, L.,
Costa, M., Lopez, A. D. &amp;; Murray, C. J. 2010 Neonatal, postneonatal, childhood, and under-5
mortality for 187 countries, 1970-2010: a systematic analysis of progress towards Millennium
Development Goal 4. Lancet 375, 1988-2008. (doi: 10. 1016/S0140-6736(10)60703-9)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
