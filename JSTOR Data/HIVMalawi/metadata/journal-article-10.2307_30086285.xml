<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30086285</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Low CD4 T Cell Counts before HIV-1 Seroconversion Do Not Affect Disease Progression in Ethiopian Factory Workers</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Yared</given-names>
                  <surname>Mekonnen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Ronald B.</given-names>
                  <surname>Geskus</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Jan C. M.</given-names>
                  <surname>Hendriks</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Tsehaynesh</given-names>
                  <surname>Messele</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Jose</given-names>
                  <surname>Borghans</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Frank</given-names>
                  <surname>Miedema</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Dawit</given-names>
                  <surname>Wolday</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Roel A.</given-names>
                  <surname>Coutinho</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Nicole H. T. M.</given-names>
                  <surname>Dukers</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">192</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id>i30086279</issue-id>
         <fpage>739</fpage>
         <lpage>748</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30086285"/>
         <abstract>
            <p>Background. Human immunodeficiency virus type 1 (HIV-1)-ininfected Ethiopians have lower CD4 T cell counts than do other populations in Africa and industrialized countries. We studied whether this unique immunological profile results in shorter survival times in HIV-1-infected Ethiopians. Methods. Data from an open cohort study of 149 HIV-1-infected factory workers in Ethiopia for 1997-2002 were used. To estimate survival times,a continuous-time Markov model was designed on the basis of CD4 T cell counts and World Health Organization clinical staging. By use of a random-effects model, decline in CD4 T cell counts was compared between HIV-1 infected Ethiopian and Dutch individuals. Results. Median survival times were in the range of 9.1-13.7 years, depending on the approach used. This range is similar to that for populations in industrialized countries before the advent of antiretroviral therapy. Ethiopians had a lower annual decline in CD4 T cell counts than did Dutch individuals, which remained when groups with similar CD4 T cell count categories were compared. Moreover, the slower decline in CD4 T cell counts was not due merely to lower HIV-1 RNA loads or an absence of syncytium-inducing/X4 HIV-1 subtype C strains in Ethiopians. Conclusions. Low baseline CD4 T cell counts do not imply shorter survival times in Ethiopians than in other populations, presumably because of a slower decline in CD4 T cell counts.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d479e284a1310">
            <label>1</label>
            <mixed-citation id="d479e291" publication-type="other">
French N, Mujugira A, Nakiyingi J, Mulder D, Janoff EN, Gilks CR
Immunological and clinical stages in HIV-1-infected Ugandan adults
are comparable and provide no evidence of rapid progression but poor
survival with advanced diseases. J Acquir Immune Defic Syndr 1999;
22:509-16.</mixed-citation>
         </ref>
         <ref id="d479e310a1310">
            <label>2</label>
            <mixed-citation id="d479e317" publication-type="other">
Collaborative Group on AIDS Incubation and HIV-1 survival including
the CASCADE EU Concerted Action. Time from HIV-1 seroconversion
to AIDS and death before widespread use of a highly active antiret-
roviral therapy: a collaborative re-analysis. Lancet 2000;355:1131-7.</mixed-citation>
         </ref>
         <ref id="d479e333a1310">
            <label>3</label>
            <mixed-citation id="d479e340" publication-type="other">
Bentwich Z, Kalinkovich A, Weisman Z. Immune activation is a dom-
inant factor in the pathogenesis of African AIDS. Immunol Today 1995;
16:187-91.</mixed-citation>
         </ref>
         <ref id="d479e353a1310">
            <label>4</label>
            <mixed-citation id="d479e360" publication-type="other">
Hazenberg MD, Otto SA, van Benthem BH, et al. Persistent immune
activation in HIV-1 infection is associated with progression to AIDS.
AIDS 2003; 17:1-8.</mixed-citation>
         </ref>
         <ref id="d479e374a1310">
            <label>5</label>
            <mixed-citation id="d479e381" publication-type="other">
Hazenberg MD, Hamann D, Schuitemaker H, Miedema F. T cell de-
pletion in HIV-1 infection: how CD4+ T cells go out of stock. Nat
Immunol 2000; 1:285-9.</mixed-citation>
         </ref>
         <ref id="d479e394a1310">
            <label>6</label>
            <mixed-citation id="d479e401" publication-type="other">
Anzala OA, Nagelkerke NJ, Bwayo JJ, et al. Rapid progression of dis-
ease in Africa sex workers with human immunodeficiency virus type
1 infection. I Infect Dis 1995;171:686-9.</mixed-citation>
         </ref>
         <ref id="d479e414a1310">
            <label>7</label>
            <mixed-citation id="d479e421" publication-type="other">
N'Galy B, Ryder RW, Bila K, et al. Human immunodeficiency virus
infection among employees in an African hospital. N Engl J Med 1988;
319:1123-7.</mixed-citation>
         </ref>
         <ref id="d479e434a1310">
            <label>8</label>
            <mixed-citation id="d479e441" publication-type="other">
Whittle H, Egboga A, Todd J, et al. Clinical and laboratory predictors
of survival in Gambian patients with symptomatic HIV-1 or HIV-2
infection. AIDS 1992;6:685-9.</mixed-citation>
         </ref>
         <ref id="d479e454a1310">
            <label>9</label>
            <mixed-citation id="d479e461" publication-type="other">
Mann JM, Bila K, Colebunders RL, et al. Natural history of human
immunodeficiency virus infection in Zaire. Lancet 1986; 2:707-9.</mixed-citation>
         </ref>
         <ref id="d479e471a1310">
            <label>10</label>
            <mixed-citation id="d479e478" publication-type="other">
Crampin AC, Floyd S, Glynn JR, et al. Long-term follow-up of HIV-
1-infected and HIV-1-uninfected individuals in rural Malawi. AIDS
2002;16:1545-50.</mixed-citation>
         </ref>
         <ref id="d479e492a1310">
            <label>11</label>
            <mixed-citation id="d479e499" publication-type="other">
Morgan D, Mahe C, Mayanja B, Okongo JM, Lubega R, Whitworth
JA. HIV-1 infection in rural Africa: is there a difference in median
time to AIDS and survival compared with that in industrialized coun-
tries? AIDS 2002; 16:597-603.</mixed-citation>
         </ref>
         <ref id="d479e515a1310">
            <label>12</label>
            <mixed-citation id="d479e522" publication-type="other">
Morgan D, Whitworth J. The natural history of HIV-1 infection in
Africa. Nat Med 2001;7:143-5.</mixed-citation>
         </ref>
         <ref id="d479e532a1310">
            <label>13</label>
            <mixed-citation id="d479e539" publication-type="other">
Urassa W, Bakari M, Sandstrom E, et al. Rate of decline of absolute
number and percentages of CD4 lymphocytes among HIV-1-infected
adults in Dar es Salaam, Tanzania. AIDS 2004; 18:433-8.</mixed-citation>
         </ref>
         <ref id="d479e552a1310">
            <label>14</label>
            <mixed-citation id="d479e559" publication-type="other">
Phillips AN, Pezzotti P, Lepri AC, Rezza G, the Italian Seroconversion
Study. CD4 lymphocyte count as a determinant of the time for HIV-
1 seroconversion to AIDS and death from AIDS: evidence from the
Italian Seroconversion Study. AIDS 1994;8:1299-305.</mixed-citation>
         </ref>
         <ref id="d479e575a1310">
            <label>15</label>
            <mixed-citation id="d479e582" publication-type="other">
Longini IM Jr, Clark WS, Gardner LI, Brundage JF. The dynamics of
CD4+ T-lymphocyte decline in HIV-1-infected individuals: a Markov
modeling approach. J Acquir Immune Defic Syndr 1991;4:1141-7.</mixed-citation>
         </ref>
         <ref id="d479e595a1310">
            <label>16</label>
            <mixed-citation id="d479e604" publication-type="other">
1997 revised guidelines for performing CD4-t- T cell determinations in
persons infected with human immunodeficiency virus (HIV). Centers
for Disease Control and Prevention. MMWR Recomm Rep 1997; 46:
1-29.</mixed-citation>
         </ref>
         <ref id="d479e621a1310">
            <label>17</label>
            <mixed-citation id="d479e628" publication-type="other">
Lang W, Perkins H, Anderson RE, Royce R, Jewell N, Winkelstein W
Jr. Patterns of T-lymphocyte changes with human immunodeficiency
virus infection: from seroconversion to the development of AIDS. J
Acquir Immune Defic Syndr 1989;2:63-9.</mixed-citation>
         </ref>
         <ref id="d479e644a1310">
            <label>18</label>
            <mixed-citation id="d479e651" publication-type="other">
Phillips AN, Lee CA, Elford J, et al. Serial CD4 lymphocyte counts and
the development of AIDS. Lancet 1991;337:389-92.</mixed-citation>
         </ref>
         <ref id="d479e661a1310">
            <label>19</label>
            <mixed-citation id="d479e668" publication-type="other">
Hendriks JC, Satten GA, van Ameijden EJ, et al. The incubation period
to AIDS in injecting drug users estimated from prevalent cohort data,
accounting for death prior to an AIDS diagnosis. AIDS 1998; 12:
1537-44.</mixed-citation>
         </ref>
         <ref id="d479e684a1310">
            <label>20</label>
            <mixed-citation id="d479e691" publication-type="other">
Wolday D, Tsegaye A, Messele T. Low absolute CD4 counts in Ethio-
pians. Ethiop Med J 2002;40(Suppl l):ll-6.</mixed-citation>
         </ref>
         <ref id="d479e701a1310">
            <label>21</label>
            <mixed-citation id="d479e708" publication-type="other">
Pollack S, Fuad B, Etzioni A. CD4 T-lymhocytopenia without oppor-
tunistic infection in HIV-1-uninfected Ethiopian immigrants to Israel.
Lancet 1993; 342:50.</mixed-citation>
         </ref>
         <ref id="d479e721a1310">
            <label>22</label>
            <mixed-citation id="d479e728" publication-type="other">
Kalinkovich A, Weisman Z, Burstein R, Bentwich Z. Standard values
of T-lymphocyte subsets in Africa. J Acquir Immune Defic Syndr Hum
Retrovirol 1998;17:183-5.</mixed-citation>
         </ref>
         <ref id="d479e742a1310">
            <label>23</label>
            <mixed-citation id="d479e749" publication-type="other">
Kassa E, Rinke de Wit TF, Hailu E, et al. Evaluation of the World
Health Organization staging system for HIV-1 infection and disease in
Ethiopia: association between clinical stages and laboratory markers.
AIDS 1999;13:381-9.</mixed-citation>
         </ref>
         <ref id="d479e765a1310">
            <label>24</label>
            <mixed-citation id="d479e772" publication-type="other">
Messele T, Abdulkadir M, Fontanel AL, et al. Reduced naive and in-
creased activated CD4 and CDS cells in healthy adult Ethiopians com-
pared with their Dutch counterparts. Clin Exp Immunol 1999; 115:
443-50.</mixed-citation>
         </ref>
         <ref id="d479e788a1310">
            <label>25</label>
            <mixed-citation id="d479e795" publication-type="other">
Rinke de Wit TF, Tsegaye A, Wolday D, et al. Primary HIV-1 subtype
C infection in Ethiopia. J Acquir Immune Defic Syndr 2002; 30:463-70.</mixed-citation>
         </ref>
         <ref id="d479e805a1310">
            <label>26</label>
            <mixed-citation id="d479e812" publication-type="other">
Hazenberg MD, Otto SA, Cohen Stuart JW, et al. Increased cell division
but not thymic dysfunction rapidly affects the T cell receptor excision
circle content of the naive T cell population in HIV-1 infection. Nat
Med 2000; 6: 1036-42.</mixed-citation>
         </ref>
         <ref id="d479e828a1310">
            <label>27</label>
            <mixed-citation id="d479e835" publication-type="other">
Tugume SB, Piwowar EM, Lutalo T, et al. Hematological reference ranges
among healthy Ugandans. Clin Diagn Lab Immunol 1995;2:233-5.</mixed-citation>
         </ref>
         <ref id="d479e845a1310">
            <label>28</label>
            <mixed-citation id="d479e852" publication-type="other">
Levin A, Brubaker G, Shao JS, et al. Determination of T-lymphocyte
subsets on site in rural Tanzania: results in HIV-1-infected and non-
infected individuals. Int J STD AIDS 1996;7:288-91.</mixed-citation>
         </ref>
         <ref id="d479e866a1310">
            <label>29</label>
            <mixed-citation id="d479e873" publication-type="other">
Zekeng L, Sadjo A, Meli J, et al. T-lymphocyte subsets values among
healthy Cameroonians. J Acquir Immune Defic Syndr Hum Retrovirol
1997; 14:82-3.</mixed-citation>
         </ref>
         <ref id="d479e886a1310">
            <label>30</label>
            <mixed-citation id="d479e893" publication-type="other">
Embree J, Bwayo J, Nagelkerke N, et al. Lymphocyte subsets in human
immunodeficiency virus type 1-infected and uninfected children in
Nairobi. Pediatr Infect Dis J 2001;20:397-403.</mixed-citation>
         </ref>
         <ref id="d479e906a1310">
            <label>31</label>
            <mixed-citation id="d479e913" publication-type="other">
Mekonnen Y, Sanders E, Aklilu M, et al. Evidence of changes in sexu-
al behaviors among male factory workers in Ethiopia. AIDS 2003; 17:
223-31.</mixed-citation>
         </ref>
         <ref id="d479e926a1310">
            <label>32</label>
            <mixed-citation id="d479e933" publication-type="other">
Hendriks JC, Satten GA, Longini IM, et al. Use of immunological
markers and continuous-time Markov models to estimate progression
of HIV-1 infection in homosexual men. AIDS 1996; 10:649-56.</mixed-citation>
         </ref>
         <ref id="d479e946a1310">
            <label>33</label>
            <mixed-citation id="d479e953" publication-type="other">
The WHO International Collaborating Group for the Study of the
WHO Staging System. Proposed "World Health Organization staging
system for HIV-1 infection and disease"
: preliminary testing by an
international collaborative cross-sectional study. AIDS 1993;7:711-8.</mixed-citation>
         </ref>
         <ref id="d479e972a1310">
            <label>34</label>
            <mixed-citation id="d479e979" publication-type="other">
Sypsa V, Touloumi G, Kenward M, Karafoulidou A, Hatzakis A. Com-
parison of smoothing techniques for CD4 data in a Markov model
with states defined by CD4: an example on the estimation of the HIV-
1 incubation time distribution. Stat Med 2001;20:3667-76.</mixed-citation>
         </ref>
         <ref id="d479e996a1310">
            <label>35</label>
            <mixed-citation id="d479e1003" publication-type="other">
Satten GA, Longini, IM. Markov chains with measurement error: es-
timating the "true" course of a marker of the progression of human
immunodeficiency virus disease (with discussion). Appl Stat 1996; 45:
275-309.</mixed-citation>
         </ref>
         <ref id="d479e1019a1310">
            <label>36</label>
            <mixed-citation id="d479e1026" publication-type="other">
Ihaka R, Gentleman R. R: a language for data analysis and graph-
ics. J Comp Graph Stat 1996;5:299-314. Available at: http:ZZwww.r
-project.org. Accessed 18 July 2005.</mixed-citation>
         </ref>
         <ref id="d479e1039a1310">
            <label>37</label>
            <mixed-citation id="d479e1046" publication-type="other">
Taylor JMG, Cumberland WG, Sy JR A stochastic model for analysis
of longitudinal AIDS data. J Am Stat Assoc 1994; 89:727-36.</mixed-citation>
         </ref>
         <ref id="d479e1056a1310">
            <label>38</label>
            <mixed-citation id="d479e1063" publication-type="other">
Multicohort Analysis Project Workshop. Immunological markers of
AIDS progression: consistency across five HIV-1-infected cohorts, part
1. AIDS 1994;7:911-21.</mixed-citation>
         </ref>
         <ref id="d479e1076a1310">
            <label>39</label>
            <mixed-citation id="d479e1083" publication-type="other">
Pehrson P, Lindback S, Lidman C, Gaines H, Giesecke J. Longer surviv-
al after HIV-1 infection for injecting drug users than for homosexual
men: implications for immunology. AIDS 1997; 11:1007-12.</mixed-citation>
         </ref>
         <ref id="d479e1096a1310">
            <label>40</label>
            <mixed-citation id="d479e1103" publication-type="other">
Koblin BA, van Benthem BH, Buchbinder SP, et al. Long-term after
infection with human immunodeficiency virus type 1 (HIV-1) among
homosexual men in hepatitis B vaccine trial cohorts in Amsterdam,
New York City, and San Francisco, 1987-1995. Am J Epidemiol 1999;
150:1026-30.</mixed-citation>
         </ref>
         <ref id="d479e1123a1310">
            <label>41</label>
            <mixed-citation id="d479e1130" publication-type="other">
Mekonnen Y, Dukers NH, Sanders E, et al. Simple markers for initiating
antiretroviral therapy among HIV-1-infected Ethiopians. AIDS 2003;
17:815-9.</mixed-citation>
         </ref>
         <ref id="d479e1143a1310">
            <label>42</label>
            <mixed-citation id="d479e1152" publication-type="other">
Schechter MT, Le N, Craib KJ, Le TN, O'Shaughnessy MV, Montaner
JS. Use of the Markov model to estimate the waiting times in a modified
WHO staging system for HIV-1 infection. J Acquir Immune Defic Syn-
dr Hum Retrovirol 1995; 8:474-9.</mixed-citation>
         </ref>
         <ref id="d479e1168a1310">
            <label>43</label>
            <mixed-citation id="d479e1175" publication-type="other">
Longini IM, Clark WS. Statistical analysis of the stages of HIV-1 in-
fection using Markov model. Stat Med 1989;8:831-43.</mixed-citation>
         </ref>
         <ref id="d479e1185a1310">
            <label>44</label>
            <mixed-citation id="d479e1192" publication-type="other">
Munoz A, Carey V, Saah AJ, et al. Predictors of decline in CD4 lym-
phocytes in a cohort of homosexual men infected with human im-
munodeficiency virus. J Acquir Immune Defic Syndr 1988; 1:396-404.</mixed-citation>
         </ref>
         <ref id="d479e1205a1310">
            <label>45</label>
            <mixed-citation id="d479e1212" publication-type="other">
Easterbrook PJ, Emami J, Gazzard B. Rate of CD4 cell decline and pre-
diction of survival in zidovudine treated patients. AIDS 1993; 7:959-67.</mixed-citation>
         </ref>
         <ref id="d479e1222a1310">
            <label>46</label>
            <mixed-citation id="d479e1229" publication-type="other">
Abebe A, Demissie D, Goudsmit J, et al. HIV-1 subtype C syncy-
tium- and non-syncytium-Ethiopian patients with AIDS. AIDS 1999;
13:1305-11.</mixed-citation>
         </ref>
         <ref id="d479e1243a1310">
            <label>47</label>
            <mixed-citation id="d479e1250" publication-type="other">
Fagnoni FF, Vescovini R, Passed G, et al. Shortage of circulating naive
CD8+ T cells provides new insights on immunodeficiency in aging.
Blood 2000;95:2860-8.</mixed-citation>
         </ref>
         <ref id="d479e1263a1310">
            <label>48</label>
            <mixed-citation id="d479e1270" publication-type="other">
Silvestri G, Sodora DL, Koup RA, et al. Nonpathogenic SIV infection of
sooty mangabeys is characterized by limited bystander immunopathology
despite chronic high-level viraemia. Immunity 2003; 18:441-52.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
