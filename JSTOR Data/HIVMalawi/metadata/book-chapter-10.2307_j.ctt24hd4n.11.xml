<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt24hd4n</book-id>
      <subj-group>
         <subject content-type="lcsh">Decolonization</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Whites</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zimbabwe</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
            <subj-group>
               <subject content-type="lcsh">1980–</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zimbabwe</subject>
         <subj-group>
            <subject content-type="lcsh">Race relations</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Pioneers, Settlers, Aliens, Exiles</book-title>
         <subtitle>The decolonisation of white identity in Zimbabwe</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Fisher</surname>
               <given-names>J. L.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>31</day>
         <month>03</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9781921666148</isbn>
      <isbn content-type="epub">9781921666155</isbn>
      <publisher>
         <publisher-name>ANU E Press</publisher-name>
         <publisher-loc>Canberra</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>ANU E Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt24hd4n"/>
      <abstract abstract-type="short">
         <p>What did the future hold for Rhodesia's white population at the end of a bloody armed conflict fought against settler colonialism? Would there be a place for them in newly independent Zimbabwe? Pioneers, Settlers, Aliens, Exiles sets out the terms offered by Robert Mugabe in 1980 to whites who opted to stay in the country they thought of as their home. The book traces over the next two decades their changing relationship with the country when the post-colonial government revised its symbolic and geographical landscape and reworked codes of membership. Particular attention is paid to colonial memories and white interpellation in the official account of the nation's rebirth and indigene discourses, in view of which their attachment to the place shifted and weakened. As the book describes the whites' trajectory from privileged citizens to persons of disputed membership and contested belonging, it provides valuable background information with regard to the land and governance crises that engulfed Zimbabwe at the start of the twenty-first century.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.3</book-part-id>
                  <title-group>
                     <title>[Map]</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.4</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.6</book-part-id>
                  <title-group>
                     <label>1.</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This chapter provides a brief history of Rhodesia as a white settler state. It introduces the Rhodesians, details early bonds developing between them and the territory and their nascent interest in pioneer history before saying something about what sorts of people they thought they were and the society they hoped to create. An account of the key legislative pillars that institutionalised racism and mapped the identities of white and black, settler and native, into the landscape follows. Then the process of data collection is discussed, as well as white engagement with Zimbabwe’s public culture, for they have heard a lot</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.7</book-part-id>
                  <title-group>
                     <label>2.</label>
                     <title>Zimbabwe’s discourse of national reconciliation</title>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>Rhodesia’s war concluded with the Lancaster House Constitutional Conference held in London between August and December 1979. Elections¹ the following February brought Robert Mugabe into office as Prime Minister and leader of the Zimbabwe African National Union Patriotic Front (ZANU PF). A policy of national reconciliation was part of the political settlement and, in light of this, plans for the future direction of the nation were established. Elsewhere new governments intent on employing a policy of national reconciliation ‘as the official normalisation of a previously abnormal condition’ have confronted the dilemma of ‘how to remember the unjust or criminal social</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.8</book-part-id>
                  <title-group>
                     <label>3.</label>
                     <title>Re-inscribing the national landscape</title>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>With majority rule, the question arose of what about Rhodesia’s colonial era was to be remembered. This issue brings with it a struggle over historicity that has in part been waged over the decolonisation of Zimbabwe’s national landscape. Radcliffe and Westwood (1996:28) make the point that there is a diversity of sites where correlative imaginaries between a people and a place can be produced. In this regard, states actively ‘distribute space’ (Driver 1992:150), setting material and representational boundaries that are formative of identities. These ‘imaginative geographies’ (Said 1993:6, 271) are contained, for instance, in ‘the concrete and precise character’ (Driver</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.9</book-part-id>
                  <title-group>
                     <label>4.</label>
                     <title>Zimbabwe’s narrative of national rebirth</title>
                  </title-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>This chapter examines how the new regime has drawn on the memory of the liberation war in order to constitute nationhood and create its own authoritative code of membership. That nationhood is realised not uncommonly through war (Hobsbawm and Ranger 1983:279) was something that Anderson (1990:129–31) understood when he attributed our attachment and willingness to die for the idea as an outcome of the nation’s depiction in terms associated with kinship and home. Zimbabwe’s war memory provides, as it were, a classificatory scheme, the wherewithal to think about who belongs, and how, to the Zimbabwean nation, for war is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.10</book-part-id>
                  <title-group>
                     <label>5.</label>
                     <title>Decolonising settler citizenship</title>
                  </title-group>
                  <fpage>103</fpage>
                  <abstract>
                     <p>The academic literature suggests citizenship produces a connection between individuals, the State and the community in which they live, and establishes a relationship containing the element of a common destiny, of stakeholders who have an investment in a shared future (Kaplan 1993:250; Kratochwil 1994:487). Stasiulis and Yuval-Davis (1995:19) believe this to be particularly important in settler and post-colonial societies where myths of common origin are divisive or contentious—a case put for Zimbabwe in the previous chapter. To realise these objectives, a state’s citizenship ideals need to be inclusive, capable of drawing people in on the basis of their birth</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.11</book-part-id>
                  <title-group>
                     <label>6.</label>
                     <title>The mobilisation of indigeneity</title>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>Representations of the indigene and, more particularly, the process of indigenisation became prominent during Zimbabwe’s second decade of independence. According to Horowitz (1991:4), the introduction of new terms designating categories of people invariably reflects aspirations of an improved collective status or a different conflict alignment. Changes in language therefore indicate the setting or shifting of borders and reveal other, alternative configurations regarding who properly belongs within them and how these people are connected to the land. The ways in which the subject position of the indigene have been constructed, represented and mobilised (Brah 1996:191) and, at the same time, the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.12</book-part-id>
                  <title-group>
                     <label>7.</label>
                     <title>The loss of certainty</title>
                  </title-group>
                  <fpage>173</fpage>
                  <abstract>
                     <p>The affective relations of national identity and changes to the white sense of belonging since independence brought with it the renegotiation of subjectivity are discussed in this chapter. Ethnographic evidence indicates that a prior sense of being properly located and at home can give way or dissipate in the light of civil war, the reworking of national narratives and widespread emigration (Borneman 1992; Loizos 1981:130–2; Mamdani 1973). Accordingly, Gupta (1992:76) nominates that the structures of feeling that bind space, time and memory in the production of location should be studied in order to establish how certain spaces become, or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.13</book-part-id>
                  <title-group>
                     <label>8.</label>
                     <title>Zimbabwe’s governance and land reform crises–a postscript</title>
                  </title-group>
                  <fpage>201</fpage>
                  <abstract>
                     <p>In a bid to disrupt and confuse the work of the people’s National Constitutional Assembly (NCA), Mugabe’s government set up its own Constitutional Commission of Inquiry to draw up a new constitution in April 1999. The Commission’s work was presented to the public as bringing the final break with colonialism. Just months before the 2000 general election, ZANU PF’s draft document was put to a referendum and rejected. This represented the first electoral defeat for ZANU PF in 20 years. The proposed constitution would have, among other things, increased executive powers, and so strengthened Mugabe’s grip on power and protected</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.14</book-part-id>
                  <title-group>
                     <label>9.</label>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>221</fpage>
                  <abstract>
                     <p>Rhodesian nationalism was an assertion of belonging in and of a place and constituted the grounds for the whites’ claim to a homeland. The settlers were able to depict themselves as being ‘at home’ because the black majority—distanced and produced as others—was ‘not home’ in Rhodesia. Instead, they were located elsewhere, in the peripheral spaces of the Tribal Trust Lands, urban townships and a few elite, but separate suburbs. Racism, as the highest expression of the colonial system, thus established fundamental and immutable distinctions between the colonist and the colonised. With the transfer of power, the order of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.15</book-part-id>
                  <title-group>
                     <title>Appendix</title>
                  </title-group>
                  <fpage>231</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hd4n.16</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
