<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7zdv4</book-id>
      <subj-group>
         <subject content-type="call-number">HV696.F6C515 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food relief</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food relief</subject>
         <subj-group>
            <subject content-type="lcsh">International cooperation</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Hunger in the Balance</book-title>
         <subtitle>The New Politics of International Food Aid</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Clapp</surname>
               <given-names>Jennifer</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>03</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780801450396</isbn>
      <isbn content-type="epub">9780801463938</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>ITHACA; LONDON</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.ctt7zdv4"/>
      <abstract abstract-type="short">
         <p>Food aid has become a contentious issue in recent decades, with sharp disagreements over genetically modified crops, agricultural subsidies, and ways of guaranteeing food security in the face of successive global food crises. In<italic>Hunger in the Balance</italic>, Jennifer Clapp provides a timely and comprehensive account of the contemporary politics of food aid, explaining the origins and outcomes of recent clashes between donor nations-and between donors and recipients.</p>
         <p>She identifies fundamental disputes between donors over "tied" food aid, which requires that food be sourced in the donor country, versus "untied" aid, which provides cash to purchase food closer to the source of hunger. These debates have been especially intense between the major food aid donors, particularly the European Union and the United States. Similarly, the EU's rejection of GMO agricultural imports has raised concerns among recipients about accepting GMO foodstuffs from the United States. For the several hundred million people who at present have little choice but to rely on food aid for their daily survival, Clapp concludes, the consequences of these political differences are profound.</p>
      </abstract>
      <counts>
         <page-count count="216"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.5</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>FOOD AID POLITICS:</title>
                     <subtitle>The Old and the New</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>More than 900 million people on the planet are undernourished. Food aid has long been one of many responses to global hunger. It is by no means the primary response or the best approach to ensuring food security. Nevertheless, some 150–200 million people depend on food aid every year. In situations of acute food shortages, particularly when crises hit people who are already suffering from chronic undernourishment, such aid can mean the difference between life and death. And food intervention in the first two years of a child’s life can make enormous improvements in both long-term health and quality</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>PAST AND PRESENT FOOD ASSISTANCE TRENDS</title>
                  </title-group>
                  <fpage>15</fpage>
                  <abstract abstract-type="extract">
                     <p>In order to fully grasp the current landscape of food aid politics, it is important to first look back briefly to its origins and to trace its evolution over the years.¹ The international political and economic context in which food aid operates today is very different from the context in which it first emerged as a form of international development assistance. In its early days, food aid policy was driven largely by forces no longer relevant in the current context: sizable grain surpluses needing to be disposed of, which determined the largest donors, and geopolitical considerations of the Cold War,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>DONOR POLICIES ON THE QUESTION OF TYING</title>
                  </title-group>
                  <fpage>46</fpage>
                  <abstract abstract-type="extract">
                     <p>The idea of untying food aid has been discussed by donor countries since at least the 1970s. It gained significant momentum in international policy circles starting in the mid-1990s, after the European Union adopted the policy in 1996. Agencies such as the FAO and the OECD have taken up the issue in extensive reports directed at their membership since 2005, and from 2007 the WFP began to strongly endorse the idea. Some donors, such as Canada and Australia, eventually followed the EU on this idea by untying their own food aid programs. Others, however, have not. Japan has moved in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>U.S. DEBATES ON TIED FOOD AID</title>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract abstract-type="extract">
                     <p>The United States has maintained a nearly 100 percent tied food aid policy for most of the history of its food aid programs. It was not until 2008 that U.S. legislation even opened the door, however slightly, to the possibility of allocating a very small portion of the aid in the form of cash for local and regional purchases in developing countries. The United States is an important case to examine in some depth because the debate over the tying of food aid and food aid monetization practices became especially politicized at the domestic level in 2005–8.</p>
                     <p>This chapter</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>THE GMO CONTROVERSY</title>
                  </title-group>
                  <fpage>94</fpage>
                  <abstract abstract-type="extract">
                     <p>Differences between donor countries regarding tying and untying of food aid that emerged starting in the mid-1990s have resulted in renewed tensions at the international level. Heated political debates erupted in several new arenas in which food aid had not previously been so politicized. The first of these new clashes occurred in 2002–3, when food aid became caught up in the wider international debate over the trade in genetically modified organisms. U.S.-sourced food aid containing GMOs that was sent to southern Africa in the context of drought and looming famine was an initial catalyst to a wider debate over</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>FOOD AID AT THE WTO</title>
                  </title-group>
                  <fpage>118</fpage>
                  <abstract abstract-type="extract">
                     <p>Clashes among donors and between donors and recipients in the context of the Doha Round of trade negotiations of the World Trade Organization after 2001 showed that in the international trade arena, food aid had become highly politicized. This chapter examines the debates in the Doha Round concerning the issue of food aid. In negotiations on the WTO Doha Round Agreement on Agriculture, the European Union pressed the idea of imposing trade disciplines, or strict rules, on such aid. It asserted that tied food aid, monetization, and concessional sales—all practiced by the United States—had the same overall effect</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>THE 2007–2008 FOOD CRISIS AND THE GLOBAL GOVERNANCE OF FOOD AID</title>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract abstract-type="extract">
                     <p>Even before the WTO draft text on food aid was settled in late 2008, the landscape of the global food economy had shifted dramatically with the sharp rise in food prices over the 2007–8 period. This new reality had a profound impact on food aid practice and politics. International attention began to focus on how to reform global governance mechanisms to promote food security in the world’s poorest countries. Much of this discussion at the international level has centered on funding initiatives through the Group of Eight (G-8) major industrial economies and Group of Twenty (G-20) leading economies to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>CONCLUSION:</title>
                     <subtitle>Prospects for the Future of Food Aid Politics</subtitle>
                  </title-group>
                  <fpage>161</fpage>
                  <abstract abstract-type="extract">
                     <p>Through its various transformations over the decades, a constant feature of food aid is that it continues to be highly political, regardless of the conditions under which it is given and the form it takes. From its early days as a surplus disposal regime with foreign policy payoffs for donors to its more recent transformation toward a humanitarian assistance regime that provides food assistance in a variety of forms, its provision has continued to prompt debate, albeit in different ways.</p>
                     <p>In the early 1990s international relations scholars indicated that the shift toward a recipient-oriented and development-focused food aid regime was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.14</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>173</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zdv4.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>195</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
