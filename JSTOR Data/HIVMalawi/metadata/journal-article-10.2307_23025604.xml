<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">vienyearpopurese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50003009</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Vienna Yearbook of Population Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Verlag der Österreichischen Akademie der Wissenschaften</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">17284414</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17285305</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23025604</article-id>
         <title-group>
            <article-title>Reconstruction of populations by age, sex and level of educational attainment for 120 countries for 1970-2000</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Wolfgang</given-names>
                  <surname>Lutz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anne</given-names>
                  <surname>Goujon</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Samir</given-names>
                  <surname>K.C.</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Warren</given-names>
                  <surname>Sanderson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23025591</issue-id>
         <fpage>193</fpage>
         <lpage>235</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23025604"/>
         <abstract>
            <p>Using demographic multi-state methods for back projecting the populations of 120 countries by age, sex and level of educational attainment from 2000 to 1970 (covering 93 percent of the 2000 world population), this paper presents an ambitious effort to reconstruct human capital data which are essential for empirically studying the aggregate level returns to education. Unlike earlier reconstruction efforts, this new dataset jointly produced at the International Institute for Applied Systems Analysis (IIASA) and the Vienna Institute of Demography (VID) gives the full educational attainment distributions for four categories (no education, primary, secondary and tertiary education) by five-year age groups and with definitions that are strictly comparable across time. Based on empirical distributions of educational attainment by age and sex for the year 2000, the method moves backward along cohort lines while explicitly considering the fact that men and women with different education have different levels of mortality. The resulting dataset will allow new estimates on the impact of age-specific human capital growth on economic growth and first results show—unlike earlier studies—a consistently positive effect.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1142e281a1310">
            <mixed-citation id="d1142e285" publication-type="other">
Alachkar, A. and W. J. Serow. 1988. "The socioeconomic determinants of mortality: An
international comparison." Genus 44(3-4): 131-151.</mixed-citation>
         </ref>
         <ref id="d1142e295a1310">
            <mixed-citation id="d1142e299" publication-type="other">
Andersen, O. 1991. "Occupational impacts on mortality declines in the Nordic countries."
In: W. Lutz, (ed.) Future demographic trends in Europe and North America. What
can we assume today? London: Academic Press, pp. 41-54.</mixed-citation>
         </ref>
         <ref id="d1142e312a1310">
            <mixed-citation id="d1142e316" publication-type="other">
Barro, R. J. and J. W. Lee. 1993. "International comparisons of educational attainment."
Journal of Monetary Economics 32(3): 363-394.</mixed-citation>
         </ref>
         <ref id="d1142e326a1310">
            <mixed-citation id="d1142e330" publication-type="other">
Barro, R. J. and J. W. Lee. 1996. "International measures of schooling years and
schooling quality." American Economic Review 86(2): 218-223.</mixed-citation>
         </ref>
         <ref id="d1142e341a1310">
            <mixed-citation id="d1142e345" publication-type="other">
Barro, R. J. and J. W. Lee. 2001. "International data on educational attainment: Updates
and implications." Oxford Economic Papers 53(3): 541-563.</mixed-citation>
         </ref>
         <ref id="d1142e355a1310">
            <mixed-citation id="d1142e359" publication-type="other">
Batljan, I. (Work in progress). New challenges for health human resources planning -
projected changes in educational composition may result in even faster increase in
number of older people than previously thought in Sweden.</mixed-citation>
         </ref>
         <ref id="d1142e372a1310">
            <mixed-citation id="d1142e376" publication-type="other">
Bloom, D. E. 2006. Measuring global educational progress. Cambridge, MA: American
Academy of Arts and Sciences.</mixed-citation>
         </ref>
         <ref id="d1142e386a1310">
            <mixed-citation id="d1142e390" publication-type="other">
Cohen, D. and M. Soto. 2007. "Growth and human capital: Good data, good results."
Journal of Economic Growth 12(1): 51-76.</mixed-citation>
         </ref>
         <ref id="d1142e400a1310">
            <mixed-citation id="d1142e404" publication-type="other">
Cohen, J., D. E. Bloom, and M. B. Malin (eds.) 2007. Educating all Children: A global
agenda. Cambridge, MA: American Academy of Arts and Sciences.</mixed-citation>
         </ref>
         <ref id="d1142e414a1310">
            <mixed-citation id="d1142e418" publication-type="other">
Crespo Cuaresma, J. and W. Lutz. 2007. "Human Capital, Age Structure and Economic
Growth: Evidence from a New Dataset" IIASA Interim Report IR-07-011. Laxenburg,
Austria: International Institute for Applied Systems Analysis.</mixed-citation>
         </ref>
         <ref id="d1142e432a1310">
            <mixed-citation id="d1142e436" publication-type="other">
De Angelis, D., W. R. Gilks, and N. E. Day. 1998. "Bayesian projection of the acquired
immune deficiency syndrome epidemic." Journal of the Royal Statistical Society,
Series C (AppliedStatistics) 47(4): 449-498.</mixed-citation>
         </ref>
         <ref id="d1142e449a1310">
            <mixed-citation id="d1142e453" publication-type="other">
De la Fuente, A. and R. Domenech. 2006. "Human capital in growth regressions: How
much difference does data quality make? Journal of the European Economic
Association 4: 1-36.</mixed-citation>
         </ref>
         <ref id="d1142e466a1310">
            <mixed-citation id="d1142e470" publication-type="other">
Doblhammer, G. 1997. Socioeconomic differentials in Austrian Adult mortality. A study
based on linked census and deaths records for the years 1981/1982. Doctoral Thesis.
Vienna: Sozial- und Wirtschaftswissenschaftliche Fakultat, Universitat Wien.</mixed-citation>
         </ref>
         <ref id="d1142e483a1310">
            <mixed-citation id="d1142e487" publication-type="other">
Duleep, H. O. 1989. "Measuring socioeconomic mortality differentials over time."
Demography 26(2): 345-351.</mixed-citation>
         </ref>
         <ref id="d1142e497a1310">
            <mixed-citation id="d1142e501" publication-type="other">
Elo, I. T. and S. H. Preston. 1996. "Educational differentials in mortality: United States,
1979-85." Social Science and Medicine 42(1): 47-57.</mixed-citation>
         </ref>
         <ref id="d1142e511a1310">
            <mixed-citation id="d1142e515" publication-type="other">
Feldman, J. J., D. M. Makuc, J. C. Kleinman, and J. Cornoni-Huntley. 1989. "National
trends in educational differentials in mortality." American Journal of Epidemiology
129(5): 919-933.</mixed-citation>
         </ref>
         <ref id="d1142e529a1310">
            <mixed-citation id="d1142e533" publication-type="other">
Figoli, M. 2006. "Educational attainment in Brazil: An analysis of the rates between 1970
and 2000." IIASA Interim Report IR-06-005. Laxenburg, Austria: International
Institute for Applied Systems Analysis.</mixed-citation>
         </ref>
         <ref id="d1142e546a1310">
            <mixed-citation id="d1142e550" publication-type="other">
Fotso, J.-C. 2006. "Malawi's future human capital: Is the country on track to meeting the
MDGs on education?" IIASA Interim Report IR-06-020. Laxenburg, Austria:
International Institute for Applied Systems Analysis.</mixed-citation>
         </ref>
         <ref id="d1142e563a1310">
            <mixed-citation id="d1142e567" publication-type="other">
Huisman, M., A. E. Kunst, O. Andersen, M. Bopp, J.-K. Borgan, C. Borrell, G. Costa, P.
Deboosere, G. Desplanques, A. Donkin, S. Gadeyne, C. Minder, E. Regidor, T.
Spadea, T. Valkonen, and J. P. Mackenbach. 2004. "Socioeconomic inequalities in
mortality among elderly people in 11 European populations." Journal of
Epidemiology and Community Health 58(6): 468-475.</mixed-citation>
         </ref>
         <ref id="d1142e586a1310">
            <mixed-citation id="d1142e590" publication-type="other">
Kitagawa, E. and P. Hauser. 1973. Differential mortality in the United States: A study in
socioeconomic epidemiology. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d1142e600a1310">
            <mixed-citation id="d1142e604" publication-type="other">
Kyriacou, G. 1991. "Level and growth effects of human capital: A cross-country study of
the convergence hypothesis." Working Paper 91-26. New York: C.V. Starr Center for
Applied Economics, Department of Economics, New York University.</mixed-citation>
         </ref>
         <ref id="d1142e617a1310">
            <mixed-citation id="d1142e621" publication-type="other">
Lau, L. J., D. T. Jamison, and F. F. Louat. 1991. "Education and productivity in
developing countries: An aggregate production function approach. Policy Research
Working Papers, WPS 612. Washington, D.C.: The World Bank.</mixed-citation>
         </ref>
         <ref id="d1142e635a1310">
            <mixed-citation id="d1142e639" publication-type="other">
Law, M., M. Lynskey, J. Ross, and W. Hall. 2001. "Back projection estimates of the
number of dependent heroin users in Australia." Addiction 96(3): 433-443.</mixed-citation>
         </ref>
         <ref id="d1142e649a1310">
            <mixed-citation id="d1142e653" publication-type="other">
Lee, R. D. 1978. Econometric studies of topics in demographic history. A dissertation in
economics, Harvard University. New York: Arno Press.</mixed-citation>
         </ref>
         <ref id="d1142e663a1310">
            <mixed-citation id="d1142e667" publication-type="other">
Lee, R. D. 1985. "Inverse projection and back projection: A critical appraisal, and
comparative results for England, 1539 to 1871." Population Studies 39: 233-248.</mixed-citation>
         </ref>
         <ref id="d1142e677a1310">
            <mixed-citation id="d1142e681" publication-type="other">
Lutz, W. and S. Scherbov. 2004. "Probabilistic population projections for India with
explicit consideration of the education-fertility link." International Statistical Review
72(1): 81-92.</mixed-citation>
         </ref>
         <ref id="d1142e694a1310">
            <mixed-citation id="d1142e698" publication-type="other">
Lutz, W., A. Goujon, and G. Doblhammer-Reiter. 1999. "Demographic dimensions in
forecasting: Adding education to age and sex." In: W. Lutz, J. W. Vaupel, and D. A.
Ahlburg (eds.) Frontiers of population forecasting. A supplement to Population and
Development Review 24( 1998): 42-58.</mixed-citation>
         </ref>
         <ref id="d1142e714a1310">
            <mixed-citation id="d1142e718" publication-type="other">
Nehru V., E. Swanson, and A. Dubey. 1995. "A new database on human capital stock in
developing and industrial countries: Sources, methodology and results." Journal of
Development Economics 46: 379-401.</mixed-citation>
         </ref>
         <ref id="d1142e732a1310">
            <mixed-citation id="d1142e736" publication-type="other">
Pamuk, E. R. 1985. "Social class inequality in mortality from 1921 to 1972 in England
and Wales." Population Studies 39: 17-31.</mixed-citation>
         </ref>
         <ref id="d1142e746a1310">
            <mixed-citation id="d1142e750" publication-type="other">
Pappas, G., S. Queen, W. Hadden, and G. Fisher. 1993. "The increasing disparity in
mortality between socioeconomic groups in the United States, 1960 and 1986." New
England Journal of Medicine 329(2): 103-109.</mixed-citation>
         </ref>
         <ref id="d1142e763a1310">
            <mixed-citation id="d1142e767" publication-type="other">
Preston, S. H., M. R. Haines, and E. Pamuk. 1981. "Effects of industrialization and
urbanization on mortality in developed countries." In: International Union for the
Scientific Study of Population. International Population Conference, Manila, 1981:
Solicited Papers, Vol. 2. Liege: Ordina Editions, pp. 233-54.</mixed-citation>
         </ref>
         <ref id="d1142e783a1310">
            <mixed-citation id="d1142e787" publication-type="other">
Sanderson, W. 2005. Education forecasts and backcasts: Lessons from the summer of
2005. Unpublished Note.</mixed-citation>
         </ref>
         <ref id="d1142e797a1310">
            <mixed-citation id="d1142e801" publication-type="other">
UN. 2005. World population prospects: The 2004 revision. New York: United Nations,
Department of Economic and Social Affairs, Population Division.</mixed-citation>
         </ref>
         <ref id="d1142e811a1310">
            <mixed-citation id="d1142e815" publication-type="other">
Woubalem, Z. 2006. "Estimates of excess adult deaths due to HIV/AIDS in Kenya."
IIASA Interim Report IR-06-013. Laxenburg, Austria: International Institute for
Applied Systems Analysis.</mixed-citation>
         </ref>
         <ref id="d1142e829a1310">
            <mixed-citation id="d1142e833" publication-type="other">
Wrigley, E. A. and R. S. Schofield. 1982. The population history of England, 1541-1871:
A reconstruction. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
