<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">revinterstud</journal-id>
         <journal-id journal-id-type="jstor">j50000149</journal-id>
         <journal-title-group>
            <journal-title>Review of International Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">02602105</issn>
         <issn pub-type="epub">14699044</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40072072</article-id>
         <article-id pub-id-type="pub-doi">10.1017/S0260210505006303</article-id>
         <title-group>
            <article-title>Domestic Threats, Regional Solutions? The Challenge of Security Integration in Southern Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Anne</given-names>
                  <surname>Hammerstad</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i40003058</issue-id>
         <fpage>69</fpage>
         <lpage>87</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 British International Studies Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/40072072"/>
         <abstract>
            <p>The article discusses the salience of different theories of regional security integration through the prism of the experience of the Southern African Development Community (SADC). It tracks the region's progress from a hostile security complex to a nascent security community and asks what strategy for security integration should be employed to continue this positive trend. Although Southern African leaders seem to prefer a collective security strategy à la NATO, the common security approach of the OSCE is more appropriate: most of the region's security threats are domestic and lack of capacity warrants an incremental, decentralised process focused on the weakest SADC members. The current state-centric approach, which tends to conflate the security needs of regimes with those of the population as a whole, will not further the cause of building a security community in Southern Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1452e119a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1452e126" publication-type="other">
Barry Buzan and Ole Waever, Regions and Powers: The Structure of International Security
(Cambridge: Cambridge University Press, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e136a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1452e143" publication-type="other">
Emanuel Adler and Michael Barnett (eds.), Security Communities (Cambridge: Cambridge University
Press, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e153a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1452e160" publication-type="other">
Andrew Hurrell, 'Regionalism in Theoretical Perspective', in Louise Fawcett and Andrew Hurrell
(eds.), Regionalism in World Politics: Regional Organization and International Order (Oxford: Oxford
University Press, 1995), pp. 38-9.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e173a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1452e180" publication-type="other">
Buzan and Waever,
Regions and Powers, p. 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e191a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1452e198" publication-type="other">
Musifiky Mwanasali, 'From the Organization of African Unity to the African Union', in Mwesiga
Baregu and Christopher Landsberg (eds.), From Cape to Cairo: Southern Africa's Evolving Security
Challenges (Boulder, CO: Lynne Rienner, 2003), p. 210.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e211a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1452e218" publication-type="other">
Ernesto Hernandez-Cata, The Africa Competitiveness Report 2004 (Geneva: World Economic Forum,
2004), Table l,p. 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e228a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1452e235" publication-type="other">
Neuma Grobbelar, 'Every Continent Needs an America': The Experience of South African Firms Doing
Business in Mozambiaue, Business in Africa Report No. 2 (Johannesburg: SAIIA, June 2004), p. 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e245a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1452e252" publication-type="other">
Hurrell, 'Regionalism in
Theoretical Perspective', p. 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e262a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1452e269" publication-type="other">
Thomas Pedersen, 'Cooperative Hegemony: Power, Ideas and Institutions in Regional Integration' in
Review of International Studies, 28: 4 (2002), p. 677.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e279a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1452e286" publication-type="other">
SADC Secretariat, 22nd SADC Summit Anniversary Brochure, lst-Srd October 2002 (Gaborone:
SADC Secretariat, October 2002), pp. 39-63.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e297a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1452e304" publication-type="other">
Sophie Chauvin and Guillaume Gaulier, 'Prospects for Increasing Trade among SADC
Countries' in Dirk Hanshom et al. (eds.), Monitoring Regional Integration in Southern Africa
Yearbook 2002 (Windhoek: Gamsberg Macmillan, 2002), pp. 29 &amp; 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e317a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1452e324" publication-type="other">
Statement by the Acting Executive Secretary of SADC at the Official
Launch of the SADC Information 21 Project (Gaborone: SADC, 13 November 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e334a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1452e341" publication-type="other">
Buzan and Waever, Regions and Powers, p. 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e348a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1452e355" publication-type="other">
Barry Buzan, People States and Fear: An Agenda for International Security Studies in the Post-Cold
War Era, 2nd edn. (Hemel Hempstead: Harvester Wheatsheaf, 1991), pp. 188 and 190.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e365a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1452e372" publication-type="other">
Buzan and Waever, Regions and Powers, p. 234.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e379a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1452e386" publication-type="other">
Ken Booth and Peter Vale, 'Critical Security Studies and Regional Insecurity: The Case of Southern
Africa', in Keith Krause and Michael C. Williams (eds.), Critical Security Studies: Concepts and
Cases (London: UCL Press, 1997), p. 338.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e400a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1452e407" publication-type="other">
Emanuel Adler and Michael Barnett, 'A Framework for the Study of Security Communities' in Adler
and Barnett, Security Communities, p. 30.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e417a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1452e424" publication-type="other">
Mohammed Ayoob, The Third World Security Predicament: State Making, Regional Conflict and the
International System (London: Lynne Rienner, 1995);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1452e433" publication-type="other">
Edward Azar and Chung-in Moon (eds.),
National Security in the Third World: the Management of Internal and External Threats (Aldershot,
UK: Elgar, 1988).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e446a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1452e453" publication-type="other">
Adler and Barnett, 'A Framework', p. 48.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e460a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1452e467" publication-type="other">
Robert Jervis, 'Security Regimes' in Stephen Krasner (ed.), International Regimes (Ithaca, NY:
Cornell University Press, 1983).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e477a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1452e484" publication-type="other">
Stefan Elbe, Strategic Implications of HIV/ AIDS, IISS Adelphi Paper 357 (Oxford: Oxford University
Press, July 2003);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1452e493" publication-type="other">
Jacqui Ala, 'Aids as a New Security Threat', in Baregu and Landsberg, From Cape
to Congo.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e503a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1452e510" publication-type="other">
Laurie Nathan, ' "Organ
Failure": A Review of the SADC Organ on Politics, Defence and Security', in L. Laakso (ed.),
Regional Integration for Conflict Prevention and Peace Building in Africa: Europe, SADC and Ecowas
(Helsinki: Department of Political Science, University of Helsinki, 2002);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1452e525" publication-type="other">
Willie Breytenbach, 'Failure
of Security Co-operation in SADC: The Suspension of the Organ for Politics, Defence and Security',
in South African Journal of International affairs, 7: 1 (2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e539a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1452e546" publication-type="other">
Protocol, Article 2, 'Objectives', paragraph 2(h).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e553a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1452e560" publication-type="other">
Ibid., 'Preamble', and article 11, paragraph 1(a)</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e567a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1452e574" publication-type="other">
Ibid., Article 2, 'Objectives', paragraphs 2(a) and (g).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e581a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1452e588" publication-type="other">
SADC, Final Communiqe of the 23rd session of the Interstate Defence and Security
Committee (ISDSC) of the Southern African Development Community (SADC) Organ on Politics,
Defence and Security Cooperation (Luanda, Angola: 7-9 August 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e601a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1452e608" publication-type="other">
SADC, Draft SADC Mutual Defence Pact, Article 6(1) (Maputo: 23 August 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e615a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1452e622" publication-type="other">
SADC, 'Mutual Defence Pact to Strengthen Military Cooperation', in SADC
Today, 6: 4 (2003), p. 10.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e633a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1452e640" publication-type="other">
Protocol, Article 2, 'Objectives', paragraphs 2 (e) and (f).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e647a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1452e654" publication-type="other">
Joint Standing Committee on Defence, Department Briefing: SADC Organ on Politics, Defence and
Security Co-operation and the SADC Mutual Defence Pact, minutes from Committee meeting (Cape
Town: The Parliamentary Monitorine Group, 14 November 2003), at &lt;www.pmg.0rg.za&gt;.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e667a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1452e674" publication-type="other">
NATO, The North Atlantic Treaty (Washington DC, 4 April 1949), article 5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e681a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1452e688" publication-type="other">
Bjørn Møller, 'NATO, the EU and the OSCE: Role Models for Africa?' in Shannon Field (ed.),
Peace in Africa: Towards a Collaborative Security Regime (Johannesburg: Institute for Global
Dialogue, 2004), p. 126.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e701a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1452e708" publication-type="other">
NATO, The Alliance's Strategic Concept: Approved by the Heads of State and Government
participating in the meeting of the North Atlantic Council in Washington DC on 23rd and 24th April
1999 (Brussels: Press Release NAC-S(99)65, 24 April 1999), Part I, paragraph 6.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e721a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1452e728" publication-type="other">
Derek da Cunha, Asia-Pacific Security: The Strengths and Weaknesses of ASEAN and the
ASEAN Regional Forum', paper delivered at the SAIIA workshop Creating the Foundation for SADC
Regional Security Integration: Developing Shared Values and Ideas (Johannesburg: SAIIA, 4 June 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e742a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1452e749" publication-type="other">
François Heisbourg, 'A World Without Alliances', in The World in 2004 (London: The Economist,
November 2003);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1452e758" publication-type="other">
Møller, 'NATO, the EU and the OSCE', p. 133.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e765a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1452e772" publication-type="other">
United Nations
Development Programme, The Human Development Report (Oxford: Oxford University Press, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e782a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1452e789" publication-type="other">
OSCE, 'Older and Wiser: Election Observation Benefits from Lessons Learned', in OSCE Newsletter,
10:3, p. 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e799a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1452e806" publication-type="other">
SADC, SADC Statement on the Elections in Zimbabwe (Blantyre: 16 March 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e813a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1452e820" publication-type="other">
SADC Parlimentary Forum Election Observer Mission to Malawi Elections 2004, Mission
Statement (22 May 2004);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1452e829" publication-type="other">
SADC Organ, Preliminary Statement by the SADC Organ on Politics,
Defence and Security Co-operation on the Parliamentary and Presidential Elections of the Republic of
Malawi Held on 20th May 2004 (Lilongwe: 21 May 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e842a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1452e849" publication-type="other">
the ODIHR website at &lt;http://www.osce.ore/odihr/ &gt;.</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e857a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1452e864" publication-type="other">
OSCE, What is the OSCE?, information leaflet (Vienna: OSCE Secretariat, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1452e871a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1452e878" publication-type="other">
Adler and Barnett, Security Communities, p. 50.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
