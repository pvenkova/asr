<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt17kk70z</book-id>
      <subj-group>
         <subject content-type="call-number">RA390.A357W35 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Catholic Church</subject>
         <subj-group>
            <subject content-type="lcsh">Missions</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Missions, Medical</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Medical personnel</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Women missionaries</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Medical care</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
         <subject>Sociology</subject>
         <subject>History</subject>
         <subject>Religion</subject>
      </subj-group>
      <book-title-group>
         <book-title>Into Africa</book-title>
         <subtitle>A Transnational History of Catholic Medical Missions and Social Change</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>WALL</surname>
               <given-names>BARBRA MANN</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>23</day>
         <month>09</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780813566221</isbn>
      <isbn content-type="epub">9780813566238</isbn>
      <isbn content-type="epub">0813566231</isbn>
      <publisher>
         <publisher-name>Rutgers University Press</publisher-name>
         <publisher-loc>NEW BRUNSWICK, NEW JERSEY; LONDON</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Barbra Mann Wall</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt17kk70z"/>
      <abstract abstract-type="short">
         <p>The most dramatic growth of Christianity in the late twentieth century has occurred in Africa, where Catholic missions have played major roles. But these missions did more than simply convert Africans. Catholic sisters became heavily involved in the Church's health services and eventually in relief and social justice efforts. In<italic>Into Africa</italic>, Barbra Mann Wall offers a transnational history that reveals how Catholic medical and nursing sisters established relationships between local and international groups, sparking an exchange of ideas that crossed national, religious, gender, and political boundaries.Both a nurse and a historian, Wall explores this intersection of religion, medicine, gender, race, and politics in sub-Saharan Africa, focusing on the years following World War II, a period when European colonial rule was ending and Africans were building new governments, health care institutions, and education systems. She focuses specifically on hospitals, clinics, and schools of nursing in Ghana and Uganda run by the Medical Mission Sisters of Philadelphia; in Nigeria and Uganda by the Irish Medical Missionaries of Mary; in Tanzania by the Maryknoll Sisters of New York; and in Nigeria by a local Nigerian congregation. Wall shows how, although initially somewhat ethnocentric, the sisters gradually developed a deeper understanding of the diverse populations they served. In the process, their medical and nursing work intersected with critical social, political, and cultural debates that continue in Africa today: debates about the role of women in their local societies, the relationship of women to the nursing and medical professions and to the Catholic Church, the obligations countries have to provide care for their citizens, and the role of women in human rights.A groundbreaking contribution to the study of globalization and medicine,<italic>Into Africa</italic>highlights the importance of transnational partnerships, using the stories of these nuns to enhance the understanding of medical mission work and global change.</p>
      </abstract>
      <counts>
         <page-count count="240"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.3</book-part-id>
                  <title-group>
                     <title>List of Figures</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.4</book-part-id>
                  <title-group>
                     <title>List of Tables</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.5</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.6</book-part-id>
                  <title-group>
                     <title>ABBREVIATIONS</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Medical Missions in Context</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Following World War II, national identities and international alliances shifted rapidly, and many colonized nations found courage and opportunity to move toward independence. In sub-Saharan Africa, as the voices of independence grew louder, British and other colonizers came under international criticism and increasingly were unable to sustain their colonies. Within a decade, they began moving out in droves. Consequently, indigenous populations struggled to realign their countries and begin the arduous process of self-rule. Catholic women’s religious congregations, whose work in Africa had been hampered by the war, now saw themselves playing a supportive role in this transition process by expanding</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Nursing, Medicine, and Mission in Ghana</title>
                  </title-group>
                  <fpage>32</fpage>
                  <abstract>
                     <p>In 1955 the government of the Gold Coast (later, Ghana) under Prime Minister Kwame Nkrumah endorsed the Report of the Commission of Enquiry into the Health Needs of the Gold Coast that noted that nurses were “the greatest contributory factor to improvement of the health services. … It cannot be said too often that until the number of nurses is adequate the extension of facilities in the curative field and to some extent in the preventive field will be of little or no value.” It also endorsed “enlisting the aid of missionary societies and other voluntary agencies in the provision</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Shifting Mission in Rural Tanzania</title>
                  </title-group>
                  <fpage>64</fpage>
                  <abstract>
                     <p>In 2012 Luis Gomes Sambo, regional director of the Africa Region of the World Health Organization (WHO), wrote, “Most countries in the Region inherited a colonial, European model of health care that was primarily intended for colonial administrators and expatriates, with separate or second class provision made—if at all—for Africans.”¹ This statement was written from an international viewpoint, yet when one considers experiences in health care at the local level, a different picture can be seen. Small health care centers administered by Catholic sister nurses and physicians in Tanganyika (later, Tanzania) after World War II illustrate a transnational</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Catholic Medical Missions and Transnational Engagement in Nigeria</title>
                  </title-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>“The Catholic Church has long been heavily involved in services of education and health, and increasingly in development and relief … [owing] much of its standing to its international character, and to the human and financial resources it can therefore command. These resources mean that Catholic involvement in many areas can rival or even surpass the government’s. Certainly no other denomination can match it.”¹ This statement by Paul Gifford serves as an opening to an analysis of how a Christian group such as the Catholic Church exerted tremendous influence in health care and, in the process, transformed itself to include</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Transnational Collaboration in Primary Health Care</title>
                  </title-group>
                  <fpage>120</fpage>
                  <abstract>
                     <p>As Catholic missions were transformed in terms of personnel and practices, they also became more open to other cultural practices. In 1962, Sister Doctor Margaret Mary Nolan, a Medical Missionary of Mary (MMM) who worked in Nigeria, noted: “The native doctors have many useful, powerful leaves and herbs at their disposal. In many cases native treatment will save a life, through controlling haemorrhage, or relieving pain and preventing shock. The ill effects seen from these raw drugs, we presume, arise simply because the dosages have never been studied. All this leaves plenty of scope for research workers.”¹ As well, Sister</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Appraising Women Religious and Their Mission Work</title>
                  </title-group>
                  <fpage>152</fpage>
                  <abstract>
                     <p>Disagreements on the nature of mission legacies continue to be central to historians today. This book has taken a transnational approach to assert that Catholic medical and nursing missions had a significant influence on religion, medicine, nursing, and education in sub-Saharan African societies. Clearly, power imbalances existed, since American and European sisters established and initially administered the institutions. As a result of new theologies, changing social and political conditions, and encounters with African populations, however, sisters underwent a major change in the way they thought about and carried out their work. As they engaged in cross-national flows of knowledge, their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.13</book-part-id>
                  <title-group>
                     <title>NOTE ON SOURCES</title>
                  </title-group>
                  <fpage>171</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.14</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>175</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.15</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>219</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk70z.16</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>231</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
