<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27794254</article-id>
         <article-categories>
            <subj-group>
               <subject>GLOBAL</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Rotavirus Vaccination: Cost-Effectiveness and Impact on Child Mortality in Developing Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Deborah</given-names>
                  <surname>Atherly</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert</given-names>
                  <surname>Dreibelbis</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Umesh D.</given-names>
                  <surname>Parashar</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Carol</given-names>
                  <surname>Levin</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John</given-names>
                  <surname>Wecker</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Richard D.</given-names>
                  <surname>Rheingans</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">200</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27794249</issue-id>
         <fpage>S28</fpage>
         <lpage>S38</lpage>
         <permissions>
            <copyright-statement>© 2009 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1086/605033"
                   xlink:title="an external site"/>
         <abstract>
            <p>Background. Rotavirus is the leading cause of severe gastroenteritis in children &lt;5 years of age and is responsible for &gt;500,000 deaths annually; ∼85% of this burden is in low-income countries eligible for financial support from the GAVI Alliance. We projected the uptake, health impact, and cost-effectiveness of introducing rotavirus vaccination in GAVI-eligible countries to help policy makers in prioritizing resources to gain the greatest health improvements for their constituencies. Methods. A demand forecast model was used to predict adoption of rotavirus vaccine in the poorest countries in the world. We then modeled health outcomes and direct costs of a hypothetical birth cohort in the target population for scenarios with and without a rotavirus vaccine with use of data on health outcomes of rotavirus infection, vaccine effectiveness, and immunization rates. Results. Vaccination would prevent 2.4 million rotavirus deaths and &gt;82 million disability-adjusted life-years (DALYs) in 64 of the 72 GAVI-eligible countries introducing vaccine from 2007 through 2025. The cost per DALY averted decreases over time, from a high of US$450 per DALY averted in the first year to a sustained low of $30 per DALY during 2017–2025, with a cumulative figure of $43 per DALY averted during 2008–2025. By applying the baseline scenario with an initial vaccine price of $7 per dose for a 2-dose vaccine, with a gradual decrease beginning in 2012 and stabilizing at $1.25 per dose by 2017, vaccination was very cost-effective in all GAVI-eligible countries with use of each country's gross domestic product per DALY averted as a threshold. Conclusions. Introduction of rotavirus vaccines into the world's poorest countries is very cost-effective and is projected to substantially reduce childhood mortality.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d5634e250a1310">
            <label>1</label>
            <mixed-citation id="d5634e257" publication-type="other">
World Health Organization. Global and national estimates of deaths
under age five attributable to rotavirus infection: 2004. Geneva: World
Health Organization, 2006.</mixed-citation>
         </ref>
         <ref id="d5634e270a1310">
            <label>2</label>
            <mixed-citation id="d5634e277" publication-type="other">
GAVI Alliance. Accelerated development and introduction of priority
new vaccines: the case of pneumococcal and rotavirus vaccines. Paris:
McKinsey &amp; Company, 2002.</mixed-citation>
         </ref>
         <ref id="d5634e290a1310">
            <label>3</label>
            <mixed-citation id="d5634e297" publication-type="other">
Kane MA, Brooks A. New immunization initiatives and progress toward
the global control of hepatitis B. Curr Opin Infect Dis 2002; 15:465–9.</mixed-citation>
         </ref>
         <ref id="d5634e307a1310">
            <label>4</label>
            <mixed-citation id="d5634e314" publication-type="other">
Mahoney RT, Maynard JE. The introduction of new vaccines into de-
veloping countries. Vaccine 1999; 17:646–52.</mixed-citation>
         </ref>
         <ref id="d5634e325a1310">
            <label>5</label>
            <mixed-citation id="d5634e332" publication-type="other">
Newall AT, Beutels , Macartney , Wood J, Maclntyre CR. The cost-
effectiveness of rotavirus vaccination in Australia. Vaccine 2007; 25:
8851–60.</mixed-citation>
         </ref>
         <ref id="d5634e345a1310">
            <label>6</label>
            <mixed-citation id="d5634e352" publication-type="other">
Carlin JB, Jackson T, Lane L, Bishop RF, Barnes GL. Cost effectiveness
of rotavirus vaccination in Australia. Aust J Public Health 1999;23:
611–6.</mixed-citation>
         </ref>
         <ref id="d5634e365a1310">
            <label>7</label>
            <mixed-citation id="d5634e372" publication-type="other">
Tucker AW, Haddix AC, Bresee JS, Holman RC, Parashar UD, Glass
RI. Cost-effectiveness analysis of a rotavirus immunization program
for the United States. JAMA 1998;279:1371–6.</mixed-citation>
         </ref>
         <ref id="d5634e385a1310">
            <label>8</label>
            <mixed-citation id="d5634e392" publication-type="other">
Fischer TK, Anh DD, Antii L, et al. Health care costs of diarrheal
disease and estimates of the cost-effectiveness of rotavirus vaccination
in Vietnam. J Infect Dis 2005; 192:1720–6.</mixed-citation>
         </ref>
         <ref id="d5634e405a1310">
            <label>9</label>
            <mixed-citation id="d5634e412" publication-type="other">
Constenla D, O'Ryan M, Navarrete MS, Antii L, Rheingans RD. Po-
tential cost effectiveness of a rotavirus vaccine in Chile [in Spanish].
Revista Medica de Chile 2006; 134:679–88.</mixed-citation>
         </ref>
         <ref id="d5634e425a1310">
            <label>10</label>
            <mixed-citation id="d5634e432" publication-type="other">
Podewils LJ, Antil L, Hummelman E, Bresee J, Parashar UD, Rheingans
R. Projected cost-effectiveness of rotavirus vaccination for children in
Asia. J Infect Dis 2005; 192:1.</mixed-citation>
         </ref>
         <ref id="d5634e446a1310">
            <label>11</label>
            <mixed-citation id="d5634e453" publication-type="other">
Widdowson MA, Bresee JS, Gentsch JR, Glass RI. Rotavirus disease
and its prevention. Curr Opin Gastroenterol 2005;21:26–31.</mixed-citation>
         </ref>
         <ref id="d5634e463a1310">
            <label>12</label>
            <mixed-citation id="d5634e470" publication-type="other">
Isakbaeva ET, Musabaev E, Antil L, et al. Rotavirus disease in Uzbek-
istan: cost-effectiveness of a new vaccine. Vaccine 2007; 25:373–80.</mixed-citation>
         </ref>
         <ref id="d5634e480a1310">
            <label>13</label>
            <mixed-citation id="d5634e487" publication-type="other">
Lorgelly PK, Joshi D, Iturriza Gomara M, Gray J, Mugford M. Exploring
the cost effectiveness of an immunization programme for rotavirus
gastroenteritis in the United Kingdom. Epidemiol Infect 2008; 136:
44–55.</mixed-citation>
         </ref>
         <ref id="d5634e503a1310">
            <label>14</label>
            <mixed-citation id="d5634e510" publication-type="other">
Rheingans RD, Constenla D, Antil L, Innis BL, Breuer T. Potential
cost-effectiveness of vaccination for rotavirus gastroenteritis in eight
Latin American and Caribbean countries. Rev Pan Am Salud Publica
2007;21:205–16.</mixed-citation>
         </ref>
         <ref id="d5634e526a1310">
            <label>15</label>
            <mixed-citation id="d5634e533" publication-type="other">
Parashar UD, Hummelman EG, Bresee JS, Miller MA, Glass RI. Global
illness and deaths caused by rotavirus disease in children. Emerg Infect
Dis 2003;9:565–72.</mixed-citation>
         </ref>
         <ref id="d5634e546a1310">
            <label>16</label>
            <mixed-citation id="d5634e553" publication-type="other">
Mrukowicz JZ, Krobicka B, Duplaga M, et al. Epidemiology and impact
of rotavirus diarrhoea in Poland. Acta Paediatr Suppl 1999; 88:53–60.</mixed-citation>
         </ref>
         <ref id="d5634e564a1310">
            <label>17</label>
            <mixed-citation id="d5634e571" publication-type="other">
Sz cs G, Uj M, Mihály I, Deák J. Burden of human rotavirus-associated
hospitalizations in three geographic regions of Hungary. Acta Paediatr
Suppl 1999;88:61–5.</mixed-citation>
         </ref>
         <ref id="d5634e584a1310">
            <label>18</label>
            <mixed-citation id="d5634e591" publication-type="other">
Ehrenkranz , Lanata CF, Penny ME, Salazar-Lindo E, Glass RI. Ro-
tavirus diarrhea disease burden in Peru: the need for a rotavirus vaccine
and its potential cost savings. Rev Panam Salud Publica 2001; 10:240–8.</mixed-citation>
         </ref>
         <ref id="d5634e604a1310">
            <label>19</label>
            <mixed-citation id="d5634e611" publication-type="other">
Chan PK, Tarn JS, Nelson EA, et al. Rotavirus infection in Hong Kong:
epidemiology and estimates of disease burden. Epidemiol Infect
1998;120:321–5.</mixed-citation>
         </ref>
         <ref id="d5634e624a1310">
            <label>20</label>
            <mixed-citation id="d5634e631" publication-type="other">
Fang ZY, Yang H, Zhang J, et al. Child rotavirus infection in association
with acute gastroenteritis in two Chinese sentinel hospitals. Pediatr Int
2000;42:401–5.</mixed-citation>
         </ref>
         <ref id="d5634e644a1310">
            <label>21</label>
            <mixed-citation id="d5634e651" publication-type="other">
Ardern-Holmes SL, Lennon D, Pinnock R, et al. Trends in hospitali-
zation and mortality from rotavirus disease in New Zealand infants.
Pediatr Infect Dis J 1999;18:614–9.</mixed-citation>
         </ref>
         <ref id="d5634e664a1310">
            <label>22</label>
            <mixed-citation id="d5634e671" publication-type="other">
Nelson EA, Tarn JS, Glass RI, Parashar UD, Fok TE Incidence of
rotavirus diarrhea and intussusception in Hong Kong using standard-
ized hospital discharge data. Pediatr Infect Dis J 2002;21:701–3.</mixed-citation>
         </ref>
         <ref id="d5634e685a1310">
            <label>23</label>
            <mixed-citation id="d5634e694" publication-type="other">
Berner R, Schumacher RF, Forster J. Survey on rotavirus infections in
a German pediatrie hospital. Eur J Clin Microbiol Infect Dis 1997; 16:
479–81.</mixed-citation>
         </ref>
         <ref id="d5634e707a1310">
            <label>24</label>
            <mixed-citation id="d5634e714" publication-type="other">
Lynch M, O'Halloran F, Whyte D, Fanning S, Cryan B, Glass RI.
Rotavirus in Ireland: national estimates of disease burden, 1997 to
1998. Pediatr Infect Dis J 2001;20:693–8.</mixed-citation>
         </ref>
         <ref id="d5634e727a1310">
            <label>25</label>
            <mixed-citation id="d5634e734" publication-type="other">
Noel JS, Parker SP, Choules K, Phillips AD, Walker-Smith J, Cubitt
WD. Impact of rotavirus infection on a paediatric hospital in the east
end of London. J Clin Pathol 1994;47:67–70.</mixed-citation>
         </ref>
         <ref id="d5634e747a1310">
            <label>26</label>
            <mixed-citation id="d5634e754" publication-type="other">
O'Mahony J, Christie G, Morgan JG, Hill C. Rotavirus gastroenteritis
among paediatric patients at Tralee general hospital. Ir Med J 2000;
93:274–7.</mixed-citation>
         </ref>
         <ref id="d5634e767a1310">
            <label>27</label>
            <mixed-citation id="d5634e774" publication-type="other">
Vesikari T, Rautanen T, Von Bonsdorff CH. Rotavirus gastroenteritis
in Finland: burden of disease and epidemiological features. Acta Pae-
diatr 1999;88:24–30.</mixed-citation>
         </ref>
         <ref id="d5634e787a1310">
            <label>28</label>
            <mixed-citation id="d5634e794" publication-type="other">
Bittencourt SA, Leal Mdo C, Santos MO. Hospitalization due of in-
fectious diarrhea in Rio de Janeiro State. Cad Saude Publica 2002; 18:
747–54.</mixed-citation>
         </ref>
         <ref id="d5634e808a1310">
            <label>29</label>
            <mixed-citation id="d5634e815" publication-type="other">
Gómez JA, Nates S, De Castagnaro NR, Espul C, Borsa A, Glass RI.
Anticipating rotavirus vaccines: review of epidemiologie studies of ro-
tavirus diarrhea in Argentina. Rev Panam Salud Publica 1998; 3:69–78.</mixed-citation>
         </ref>
         <ref id="d5634e828a1310">
            <label>30</label>
            <mixed-citation id="d5634e835" publication-type="other">
Effler PV, Holman RC, Parashar UD, Glass RI. Diarrhea-associated
hospitalizations among children in Hawaii. Hawaii Med J 2000; 59:
362–5.</mixed-citation>
         </ref>
         <ref id="d5634e848a1310">
            <label>31</label>
            <mixed-citation id="d5634e855" publication-type="other">
Ford-Jones EL, Wang E, Petric M, Corey P, Moineddin R, Fearon M.
Hospitalization for community-acquired, rotavirus-associated diarrhea:
a prospective, longitudinal, population-based study during the seasonal
outbreak. The Greater Toronto Area/Peel Region PRESI Study Group.
Pediatrie Rotavirus Epidemiology Study for Immunization. Arch Pe-
diatr Adolesc Med 2000; 154:578–85.</mixed-citation>
         </ref>
         <ref id="d5634e878a1310">
            <label>32</label>
            <mixed-citation id="d5634e885" publication-type="other">
Parashar UD, Holman RC, Clarke MJ, Bresee JS, Glass RI. Hospital-
izations associated with rotavirus diarrhea in the United States, 1993
through 1995: surveillance based on the new ICD-9-CM rotavirus-
specific diagnostic code. J Infect Dis 1998;177:13–7.</mixed-citation>
         </ref>
         <ref id="d5634e901a1310">
            <label>33</label>
            <mixed-citation id="d5634e908" publication-type="other">
Parashar UD, Holman RC, Bresee JS, et al. Epidemiology of diarrheal
disease among children enrolled in four West Coast health maintenance
organizations. Vaccine Safety Datalink Team. Pediatr Infect Dis J
1998;17:605–11.</mixed-citation>
         </ref>
         <ref id="d5634e924a1310">
            <label>34</label>
            <mixed-citation id="d5634e931" publication-type="other">
Buigues RP, Duval B, Rochette L, et al. Hospitalizations for diarrhea
in Quebec children from 1985 to 1998: estimates of rotavirus-associated
diarrhea. Can J Infect Dis 2002; 13:239–44.</mixed-citation>
         </ref>
         <ref id="d5634e945a1310">
            <label>35</label>
            <mixed-citation id="d5634e952" publication-type="other">
Berner R, Schumacher RF, Hameister S, Forster J. Occurrence and
impact of community-acquired and nosocomial rotavirus infections—
a hospital-based study over 10 y. Acta Paediatr 1999;88:48–52.</mixed-citation>
         </ref>
         <ref id="d5634e965a1310">
            <label>36</label>
            <mixed-citation id="d5634e972" publication-type="other">
Murray CJL, Lopez AD. The global burden of disease: a comprehensive
assessment of mortality and disability from diseases, injuries and risk
factors in 1990 and projected to 2020. Vol. 1. Cambridge: Harvard
University Press, 1996.</mixed-citation>
         </ref>
         <ref id="d5634e988a1310">
            <label>37</label>
            <mixed-citation id="d5634e995" publication-type="other">
Ruiz-Palacios GM, Perez-Schael I, Velazquez FR, et al.; Human Ro-
tavirus Vaccine Study Group. Safety and efficacy of an attenuated vac-
cine against severe rotavirus gastroenteritis. Engl J Med 2006; 354:
11–22.</mixed-citation>
         </ref>
         <ref id="d5634e1011a1310">
            <label>38</label>
            <mixed-citation id="d5634e1018" publication-type="other">
De Vos B, Vesikari T, Linhares AC, et al. A rotavirus vaccine for pro-
phylaxis of infants against rotavirus gastroenteritis. Pediatr Infect Dis
J 2004;23(Suppl):Sl79–82.</mixed-citation>
         </ref>
         <ref id="d5634e1031a1310">
            <label>39</label>
            <mixed-citation id="d5634e1038" publication-type="other">
Vesikari T, Karvonen A, Puustinen L, et al. Efficacy of RIX4414 live
attenuated human rotavirus vaccine in Finnish infants. Pediatr Infect
Dis J 2004;23:937–43.</mixed-citation>
         </ref>
         <ref id="d5634e1051a1310">
            <label>40</label>
            <mixed-citation id="d5634e1058" publication-type="other">
United Nations Children's Fund. The state of the world's children 2004.
2002. Available at: http://www.unicef.org/sowc04/index.html. Accessed
24 May 2004.</mixed-citation>
         </ref>
         <ref id="d5634e1072a1310">
            <label>41</label>
            <mixed-citation id="d5634e1079" publication-type="other">
Murray C, Lopez A. The global burden of disease: a comprehensive
assessment of mortality and disability from diseases, injuries, and risk
factors in 1990 and projected to 2020. In: Global burden of disease
and injury series. Vol. 1, xxxii. Boston: School of Public Health, Har-
vard University, 1996:990.</mixed-citation>
         </ref>
         <ref id="d5634e1098a1310">
            <label>42</label>
            <mixed-citation id="d5634e1107" publication-type="other">
Villa S, Guiscafre H, Martinez H, Munoz O, Gutierrez G. Seasonal
diarrhoeal mortality among Mexican children. Bull WHO 1999; 77:
375–80.</mixed-citation>
         </ref>
         <ref id="d5634e1120a1310">
            <label>43</label>
            <mixed-citation id="d5634e1127" publication-type="other">
Gomez JA, Sordo ME, Gentile A. Epidemiologie patterns of diarrheal
disease in Argentina: estimation of rotavirus disease burden. Pediatr
Infect Dis J 2002;21:843–50.</mixed-citation>
         </ref>
         <ref id="d5634e1140a1310">
            <label>44</label>
            <mixed-citation id="d5634e1147" publication-type="other">
Velazquez FR, Matson DO, Guerrero ML, et al. Serum antibody as a
marker of protection against natural rotavirus infection and disease. J
Infect Dis 2000; 182:1602–9.</mixed-citation>
         </ref>
         <ref id="d5634e1160a1310">
            <label>45</label>
            <mixed-citation id="d5634e1167" publication-type="other">
Gonzalez FS, Sordo ME, Rowensztein G, et al. Rotavirus diarrhea:
impact in a pediatrie hospital of Buenos Aires. Medicina 1999; 59:
321–6.</mixed-citation>
         </ref>
         <ref id="d5634e1180a1310">
            <label>46</label>
            <mixed-citation id="d5634e1187" publication-type="other">
Cardoso DD, Soares CM, Dias e Souza MB, et al. Epidemiological
features of rotavirus infection in Goiania, Goias, Brazil, from 1986 to
2000. Memorias do Instituto Oswaldo Cruz 2003; 98:25–9.</mixed-citation>
         </ref>
         <ref id="d5634e1201a1310">
            <label>47</label>
            <mixed-citation id="d5634e1208" publication-type="other">
Bok , Castagnaro NC, Diaz NE, et al. Rotavirus laboratory network:
results after one year of observation [in Spanish]. Rev Arg Microbiol
1999;31:1–12.</mixed-citation>
         </ref>
         <ref id="d5634e1221a1310">
            <label>48</label>
            <mixed-citation id="d5634e1228" publication-type="other">
Bok K, Castagnaro N, Borsa A, et al. Surveillance for rotavirus in
Argentina. J Med Virol 2001;65:190–8.</mixed-citation>
         </ref>
         <ref id="d5634e1238a1310">
            <label>49</label>
            <mixed-citation id="d5634e1245" publication-type="other">
Barraza P, Avendano LF, Spencer E, Calderón A, Prenzel I, Duarte E.
Hospital infection caused by rotaviruses in infants, Santiago, Chile.
Boletín de la Oficina Sanitaria Panamericana 1986; 101:328–38.</mixed-citation>
         </ref>
         <ref id="d5634e1258a1310">
            <label>50</label>
            <mixed-citation id="d5634e1265" publication-type="other">
Urrestarazu MI, Liprandi F, De Suarez EP, Gonzalez R, Perez-Schael
I. Etiological, clinical, and sociodemographic characteristics of acute
diarrhea in Venezuela [in Spanish]. Rev Panam Salud Publica 1999;
6:149–56.</mixed-citation>
         </ref>
         <ref id="d5634e1281a1310">
            <label>51</label>
            <mixed-citation id="d5634e1288" publication-type="other">
Perez-Schael I, Gonzalez R, Fernandez R, et al. Epidemiological features
of rotavirus infection in Caracas, Venezuela: implications for rotavirus
immunization programs. J Med Virol 1999; 59:520–6.</mixed-citation>
         </ref>
         <ref id="d5634e1301a1310">
            <label>52</label>
            <mixed-citation id="d5634e1308" publication-type="other">
Hsu VP, Abdul Rahman HB, Wong SL, et al. Estimates of the rotavirus
disease burden in Malaysia. J Infect Dis 2005; 19(Suppl l):S80–6.</mixed-citation>
         </ref>
         <ref id="d5634e1319a1310">
            <label>53</label>
            <mixed-citation id="d5634e1326" publication-type="other">
Nelson EA, Tarn JS, Bresee JS, et al. Estimates of rotavirus disease
burden in Hong Kong: hospital-based surveillance. J Infect Dis
2005;192(Suppl l):S71–9.</mixed-citation>
         </ref>
         <ref id="d5634e1339a1310">
            <label>54</label>
            <mixed-citation id="d5634e1346" publication-type="other">
Chen KT, Chen PY, Tang RB, et al. Sentinel hospital surveillance for
rotavirus diarrhea in Taiwan, 2001-2003. J Infect Dis 2005; 192(Suppl
l):S44–8.</mixed-citation>
         </ref>
         <ref id="d5634e1359a1310">
            <label>55</label>
            <mixed-citation id="d5634e1366" publication-type="other">
Jiraphongsa C, Bresee JS, Pongsuwanna Y, et al.; Rotavirus Surveillance
Project Thailand Study Group. Epidemiology and burden of rotavirus
diarrhea in Thailand: results of the sentinel surveillance. J Infect Dis
2005;192(Suppl l):S87–93.</mixed-citation>
         </ref>
         <ref id="d5634e1382a1310">
            <label>56</label>
            <mixed-citation id="d5634e1389" publication-type="other">
Cama RI, Parashar UD, Taylor DN, et al. Enteropathogens and other
factors associated with severe disease in children with acute watery
diarrhea in Lima, Peru. J Infect Dis 1999; 179:1139–44.</mixed-citation>
         </ref>
         <ref id="d5634e1402a1310">
            <label>57</label>
            <mixed-citation id="d5634e1409" publication-type="other">
Cunliffe NA, Gondwe JS, Kirkwood CD, et al. Effect of concomitant
HIV infection on presentation and outcome of rotavirus gastroenteritis
in Malawian children. Lancet 2001;358:550–5.</mixed-citation>
         </ref>
         <ref id="d5634e1422a1310">
            <label>58</label>
            <mixed-citation id="d5634e1429" publication-type="other">
Mpabalwani M, Oshitani H, Kasolo F, et al. Rotavirus gastro-enteritis
in hospitalized children with acute diarrhoea in Zambia. Ann Trop
Paediatr 1995;15:39–43.</mixed-citation>
         </ref>
         <ref id="d5634e1443a1310">
            <label>59</label>
            <mixed-citation id="d5634e1450" publication-type="other">
Fang ZY, Wang B, Kilgore PE, et al. Sentinel hospital surveillance for
rotavirus diarrhea in the People's Republic of China, August 2001-July
2003. J Infect Dis 2005; 192(Suppl l)S94–9.</mixed-citation>
         </ref>
         <ref id="d5634e1463a1310">
            <label>60</label>
            <mixed-citation id="d5634e1470" publication-type="other">
Bahl R, Ray P, Subodh S, et al. Incidence of severe rotavirus diarrhea
in Delhi and the G and types of the infecting strains. J Infect Dis
2005; 192(Suppl l):S114–9.</mixed-citation>
         </ref>
         <ref id="d5634e1483a1310">
            <label>61</label>
            <mixed-citation id="d5634e1490" publication-type="other">
De Francisco A, Hall A, Schellenberg JR, Greenwood AM, Greenwood
BM. The pattern of infant and childhood mortality in Upper River
Division, the Gambia. Ann Trop Paediatr 1993; 13:345–52.</mixed-citation>
         </ref>
         <ref id="d5634e1503a1310">
            <label>62</label>
            <mixed-citation id="d5634e1510" publication-type="other">
Jaffar S, Leach A, Greenwood AM, et al. Changes in the pattern of
infant and childhood mortality in upper river division, the Gambia,
from 1989 to 1993. Trop Med Int Health 1997;2:28–37.</mixed-citation>
         </ref>
         <ref id="d5634e1523a1310">
            <label>63</label>
            <mixed-citation id="d5634e1530" publication-type="other">
Shamebo D, Muhe L, Sandstrom A, Freij L, Krantz I, Wall S. The
Butajira Rural Health Project in Ethiopia: a nested case-referent (con-
trol) study of under-5 mortality and its health and behavioural deter-
minants. Ann Trop Paediatr 1994; 14:201–9.</mixed-citation>
         </ref>
         <ref id="d5634e1546a1310">
            <label>64</label>
            <mixed-citation id="d5634e1553" publication-type="other">
Khalique , Sinha SN, Yunus M, Malik A. Early childhood mortality—
a rural study. J R Soc Health 1993; 113:247–9.</mixed-citation>
         </ref>
         <ref id="d5634e1564a1310">
            <label>65</label>
            <mixed-citation id="d5634e1571" publication-type="other">
Hirve S, Ganatra B. A prospective cohort study on the survival ex-
perience of under five children in rural western India. Indian Pediatr
1997;34:995–1001.</mixed-citation>
         </ref>
         <ref id="d5634e1584a1310">
            <label>66</label>
            <mixed-citation id="d5634e1591" publication-type="other">
van Tran D, van Thuan C, Tuan T, Dung PH, Persson L, Grabe M.
Survey on immunization, diarrhoeal disease and mortality in Quang
Ninh Province, Vietnam. J Trop Pediatr 1991;37:280–5.</mixed-citation>
         </ref>
         <ref id="d5634e1604a1310">
            <label>67</label>
            <mixed-citation id="d5634e1611" publication-type="other">
Jinadu MK, Olusi SO, Agun JI, Fabiyi AK. Childhood diarrhoea in
rural Nigeria. I. Studies on prevalence, mortality and socio-environ-
mental factors. J Diarrhoeal Dis Res 1991;9:323–7.</mixed-citation>
         </ref>
         <ref id="d5634e1624a1310">
            <label>68</label>
            <mixed-citation id="d5634e1631" publication-type="other">
Baqui AH, Sabir AA, Begum N, Arifeen SE, Mitra SN, Black RE. Causes
of childhood deaths in Bangladesh: an update. Acta Paediatrica 2001;90:
682–90.</mixed-citation>
         </ref>
         <ref id="d5634e1644a1310">
            <label>69</label>
            <mixed-citation id="d5634e1651" publication-type="other">
Population Division of the Department of Economic and Social Affairs
of the United Nations Secretariat. World population prospects: the 2006
revision. Available at: http://esa.un.org/unpp/. Accessed August 2007.</mixed-citation>
         </ref>
         <ref id="d5634e1664a1310">
            <label>70</label>
            <mixed-citation id="d5634e1671" publication-type="other">
Liddle JL, Burgess MA, Gilbert GL, et al. Rotavirus gastroenteritis:
impact on young children, their families and the health care system.
Med J Aust 1997; 167:304–7.</mixed-citation>
         </ref>
         <ref id="d5634e1685a1310">
            <label>71</label>
            <mixed-citation id="d5634e1692" publication-type="other">
Baltussen R, Adam T, Tan Torres T, et al. Generalized cost effectiveness
analysis: a guide. Geneva: World Health Organization: 2002.</mixed-citation>
         </ref>
         <ref id="d5634e1702a1310">
            <label>72</label>
            <mixed-citation id="d5634e1711" publication-type="other">
World Health Organization. Prices for hospital and health centres.
2008. Available at: http://www.who.int/whosis/en/. Accessed 21 Feb-
ruary 2008.</mixed-citation>
         </ref>
         <ref id="d5634e1724a1310">
            <label>73</label>
            <mixed-citation id="d5634e1731" publication-type="other">
World Health Organization. Review of treatment cost protocol studies.
Geneva: WHO Department of Vaccines and Biologicals, 2001:1-32.</mixed-citation>
         </ref>
         <ref id="d5634e1741a1310">
            <label>74</label>
            <mixed-citation id="d5634e1748" publication-type="other">
Rheingans RD, Constenla D, Antil L, Innis BL, Breuer T. Economic
and health burden of rotavirus gastroenteritis for the 2003 birth cohort
in eight Latin American and Caribbean countries. Rev Panam Salud
Publica 2007;21:192–204.</mixed-citation>
         </ref>
         <ref id="d5634e1764a1310">
            <label>75</label>
            <mixed-citation id="d5634e1771" publication-type="other">
Bureau of Labor Statistics. Consumer price index. 2000-2002. Available
at: http://www.bls.gov/cpi/cpi_dr.htm. Accessed 10 April 2008.</mixed-citation>
         </ref>
         <ref id="d5634e1781a1310">
            <label>76</label>
            <mixed-citation id="d5634e1788" publication-type="other">
World Development Indicators Online. 2004. Available at: http://www
.worldbank.org/data/wdi2004/. 3 May 2004.</mixed-citation>
         </ref>
         <ref id="d5634e1799a1310">
            <label>77</label>
            <mixed-citation id="d5634e1806" publication-type="other">
Wolfson LJ. WHO Immunization Coverage Estimates and Trajectories
(WHO ICE-T). Geneva: World Health Organization, Department of
Immunization, Vaccines, and Biologicals, 2008:5.</mixed-citation>
         </ref>
         <ref id="d5634e1819a1310">
            <label>78</label>
            <mixed-citation id="d5634e1826" publication-type="other">
DHS. Demographic and Health Surveys. 2001-2007, Measures DHS.
Available at: http://www.measuredhs.com. Accessed 21 February 2008.</mixed-citation>
         </ref>
         <ref id="d5634e1836a1310">
            <label>79</label>
            <mixed-citation id="d5634e1843" publication-type="other">
Pan American Health Organization. 2008 PAHO revolving fund vaccine
prices. Immunization Newsletter 2008; 1:1.</mixed-citation>
         </ref>
         <ref id="d5634e1853a1310">
            <label>80</label>
            <mixed-citation id="d5634e1860" publication-type="other">
IMF. World economic outlook database. 2008. Available at: http://
www.imf.org/external/pubs/ft/weo/2008/01/. Accessed 21 April 2008.</mixed-citation>
         </ref>
         <ref id="d5634e1870a1310">
            <label>81</label>
            <mixed-citation id="d5634e1877" publication-type="other">
World Health Organization. Some strategies to reduce risk, in World
Health Report 2002: reducing risk, promoting healthy lives. Geneva:
World Health Organization, 2002:100–44.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
