<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">science</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100000</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Science</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Association for the Advancement of Science</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00368075</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">10959203</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>journal-series-title</meta-name>
               <meta-value>New Series</meta-value>
            </custom-meta>
         </custom-meta-group>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23269274</article-id>
         <article-categories>
            <subj-group>
               <subject>SPECIAL SECTION: Disease Prevention</subject>
               <subj-group>
                  <subject>PERSPECTIVES</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Double Burden of Noncommunicable and Infectious Diseases in Developing Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>I. C.</given-names>
                  <surname>Bygbjerg</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>21</day>
            <month>9</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">337</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6101</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23269236</issue-id>
         <fpage>1499</fpage>
         <lpage>1501</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 American Association for the Advancement of Science</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1126/science.1223466"
                   xlink:title="Science, published by AAAS."/>
         <abstract>
            <p>On top of the unfinished agenda of infectious diseases in low- and middle-income countries, development, industrialization, urbanization, investment, and aging are drivers of an epidemic of noncommunicable diseases (NCDs). Malnutrition and infection in early life increase the risk of chronic NCDs in later life, and in adult life, combinations of major NCDs and infections, such as diabetes and tuberculosis, can interact adversely. Because intervention against either health problem will affect the other, intervening jointly against noncommunicable and infectious diseases, rather than competing for limited funds, is an important policy consideration requiring new thinking and approaches.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References and Notes</title>
         <ref id="d965e144a1310">
            <label>1</label>
            <mixed-citation id="d965e151" publication-type="other">
A. B. Omran, Milbank Q. 49, 509 (1971).</mixed-citation>
         </ref>
         <ref id="d965e158a1310">
            <label>2</label>
            <mixed-citation id="d965e165" publication-type="other">
D. Stuckler, Milbank Q. 86, 273 (2008).</mixed-citation>
         </ref>
         <ref id="d965e172a1310">
            <label>3</label>
            <mixed-citation id="d965e179" publication-type="other">
C. D. Mathers, D. Loncar, Updated projections of global
mortality and burden of disease, 2002-2030. World
Health Organization; available at vvww.who.int/
healthinfo/globaLburden_disease/en/index.html.</mixed-citation>
         </ref>
         <ref id="d965e195a1310">
            <label>4</label>
            <mixed-citation id="d965e202" publication-type="other">
WHO, UNAIDS, Strategies for the Prevention and
Control of Communicable Diseases (WHO/CDS/CPE/SMT/
2001.13).</mixed-citation>
         </ref>
         <ref id="d965e216a1310">
            <label>5</label>
            <mixed-citation id="d965e223" publication-type="other">
D. Stuckler, 5. Basu, M. McKee, PLoS Med. 7, el000241
(2010).</mixed-citation>
         </ref>
         <ref id="d965e233a1310">
            <label>6</label>
            <mixed-citation id="d965e240" publication-type="other">
C. J. L. Murray, A. D. Lopez, Eds., Global Burden of
Disease and Injury Series, vol. I (Harvard School of Public
Health on behalf of the World Health Organization and
the World Bank, Cambridge, 1996).</mixed-citation>
         </ref>
         <ref id="d965e256a1310">
            <label>7</label>
            <mixed-citation id="d965e263" publication-type="other">
WHO, The Double Burden: Emerging Epidemics and
Persistent Problems. The World Health Report 1999
(WHO, Geneva, 1999).</mixed-citation>
         </ref>
         <ref id="d965e276a1310">
            <label>8</label>
            <mixed-citation id="d965e283" publication-type="other">
D. Yach, C. Hawkes, C. L. Gould, K. J. Hofman, ]AMA 291,
2616 (2004).</mixed-citation>
         </ref>
         <ref id="d965e293a1310">
            <label>9</label>
            <mixed-citation id="d965e300" publication-type="other">
WHO, Preventing Chronic Diseases—A Vital Investment,
WHO Global Report (WHO, Geneva, 2005).</mixed-citation>
         </ref>
         <ref id="d965e310a1310">
            <label>10</label>
            <mixed-citation id="d965e317" publication-type="other">
The World Bank, Public Policy and the Challenge of
Chronic Noncommunicable Diseases (The World Bank,
Washington, DC, 2007).</mixed-citation>
         </ref>
         <ref id="d965e331a1310">
            <label>11</label>
            <mixed-citation id="d965e338" publication-type="other">
WHO, United Nations high-level meeting on
noncommunicable disease prevention and control. NCD
summit to shape the international agenda, September
2011; available at www.who.int/nmh/events/
un_ncd_summit2011/en/.</mixed-citation>
         </ref>
         <ref id="d965e357a1310">
            <label>12</label>
            <mixed-citation id="d965e364" publication-type="other">
C. Dye, B. Bourdin Trunz, K. Lonnroth, G. Roglic,
B. G. Williams, PLoS ONE 6, e21161 (2011).</mixed-citation>
         </ref>
         <ref id="d965e374a1310">
            <label>13</label>
            <mixed-citation id="d965e381" publication-type="other">
I. A. McGregor, M. E. Wilson, W. Z. Billewicz, Trans. R.
Soc. Trop. Med. Hyg. 77, 232 (1983).</mixed-citation>
         </ref>
         <ref id="d965e391a1310">
            <label>14</label>
            <mixed-citation id="d965e398" publication-type="other">
I. Philips, B. Wharton, BM] 5589, 407 (1968).</mixed-citation>
         </ref>
         <ref id="d965e405a1310">
            <label>15</label>
            <mixed-citation id="d965e412" publication-type="other">
D. J. P. Barker, C. Osmond, Lancet 327, 1077 (1986).</mixed-citation>
         </ref>
         <ref id="d965e419a1310">
            <label>16</label>
            <mixed-citation id="d965e426" publication-type="other">
C. N. Hales et al„ BM] 303, 1019 (1991).</mixed-citation>
         </ref>
         <ref id="d965e434a1310">
            <label>17</label>
            <mixed-citation id="d965e441" publication-type="other">
C. K. Lutter, R. Lutter, Science 337, 1495 (2012).</mixed-citation>
         </ref>
         <ref id="d965e448a1310">
            <label>18</label>
            <mixed-citation id="d965e455" publication-type="other">
D. L. Christensen, A. Kapur, I. C. Bygbjerg,
Int. J. Gynaecol. Obstet. 115, (suppl. 1), 516 (2011).</mixed-citation>
         </ref>
         <ref id="d965e465a1310">
            <label>19</label>
            <mixed-citation id="d965e472" publication-type="other">
C. ]. Nolan, P. Damm, M. Prentki, Lancet 378, 169 (2011).</mixed-citation>
         </ref>
         <ref id="d965e479a1310">
            <label>20</label>
            <mixed-citation id="d965e486" publication-type="other">
C. Lau,). M. Rogers, M. Desai, M. G. Ross, Obstet.
Gynecol. 117, 978 (2011).</mixed-citation>
         </ref>
         <ref id="d965e496a1310">
            <label>21</label>
            <mixed-citation id="d965e503" publication-type="other">
G. V. Agrasada, U. Ewald, E. Kylberg, J. Gustafsson,
Asia Pac. ]. Clin. Nutr. 20. 62 (2011).</mixed-citation>
         </ref>
         <ref id="d965e513a1310">
            <label>22</label>
            <mixed-citation id="d965e520" publication-type="other">
E. L. Leikin, J. H. Jenkins, G. A. Pomerantz, L. Klein,
Obstet. Gynecol. 69, 570 (1987).</mixed-citation>
         </ref>
         <ref id="d965e531a1310">
            <label>23</label>
            <mixed-citation id="d965e538" publication-type="other">
M. Dartell et al„ Sex. Transm. Dis. 39, 201 (2012).</mixed-citation>
         </ref>
         <ref id="d965e545a1310">
            <label>24</label>
            <mixed-citation id="d965e552" publication-type="other">
WHO, Guidelines for Implementing Collaborative TB and
HIV Programme Activities (WHO/CDS/TB/2003.319).</mixed-citation>
         </ref>
         <ref id="d965e562a1310">
            <label>25</label>
            <mixed-citation id="d965e569" publication-type="other">
WHO, Malaria and HIV/AIDS Interactions and
Implications-, available at www.who.int/malaria/
publications/atoz/9241593350/en/i ndex.html.</mixed-citation>
         </ref>
         <ref id="d965e582a1310">
            <label>26</label>
            <mixed-citation id="d965e589" publication-type="other">
C. Y„ PLoS Med. 15, el52 (2008).</mixed-citation>
         </ref>
         <ref id="d965e596a1310">
            <label>27</label>
            <mixed-citation id="d965e603" publication-type="other">
K. E. Dooley, R. E. Chaisson, Lancet Infect. Dis. 9,737 (2009).</mixed-citation>
         </ref>
         <ref id="d965e610a1310">
            <label>28</label>
            <mixed-citation id="d965e617" publication-type="other">
A. D. Harries et ai, Int. ]. Tuberc. Lung Dis. 15, 1436, i
(2011).</mixed-citation>
         </ref>
         <ref id="d965e628a1310">
            <label>29</label>
            <mixed-citation id="d965e635" publication-type="other">
WHO, Collaborative Framework for Care and Control of
Tuberculosis and Diabetes (WH0/HTM/TB/2011.15);
available at www.who.int/diabetes/publications/
tb_diabetes2011/en/index.html.</mixed-citation>
         </ref>
         <ref id="d965e651a1310">
            <label>30</label>
            <mixed-citation id="d965e658" publication-type="other">
T. J. Allain et al„ Trop. Med. Int. Health 16, 1077 (2011).</mixed-citation>
         </ref>
         <ref id="d965e665a1310">
            <label>31</label>
            <mixed-citation id="d965e672" publication-type="other">
M. Uplekar, M. C. Raviglione, Bull. World Health Organ.
85, 413 (2007).</mixed-citation>
         </ref>
         <ref id="d965e682a1310">
            <label>32</label>
            <mixed-citation id="d965e689" publication-type="other">
L. Li et al„ Trop. Med. Int. Health., published online
25 July 2012 (10.1111/j.l3652012.03068.x).</mixed-citation>
         </ref>
         <ref id="d965e699a1310">
            <label>33</label>
            <mixed-citation id="d965e706" publication-type="other">
I. C. Bygbjerg, Ugeskr. Laeger 174, 1516 (2012)
(Challenges and solutions in global health (Danish)].</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
