<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">lawsocietyreview</journal-id>
         <journal-id journal-id-type="jstor">j100264</journal-id>
         <journal-title-group>
            <journal-title>Law &amp; Society Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn pub-type="ppub">00239216</issn>
         <issn pub-type="epub">15405893</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">29734170</article-id>
         <title-group>
            <article-title>On the Edge of the Law: Women's Property Rights and Dispute Resolution in Kisii, Kenya</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Elin</given-names>
                  <surname>Henrysson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Sandra F.</given-names>
                  <surname>Joireman</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">43</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i29734167</issue-id>
         <fpage>39</fpage>
         <lpage>60</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 The Law and Society Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/29734170"/>
         <abstract>
            <p>Scholars have argued that economic efficiency requires a clear definition of the rights of ownership, contract, and transfer of land. Ambiguity in the definition or enforcement of any of these rights leads to an increase in transaction costs in the exchange and transfer of land as well as a residual uncertainty after any land contract. In Kenya, government efforts at establishing clearly defined property rights and adjudication mechanisms have been plagued by the existence of alternative processes for the adjudication of disputes. Customary dispute resolution has been praised as an inexpensive alternative to official judicial processes in a legally pluralistic environment. However, our research demonstrates that customary processes may also carry a monetary cost that puts them beyond the means of many citizens. This article compares the costs and processes of the formal and informal methods of property rights adjudication for women in the Kisii region of Kenya. The research results suggest that women have weak property rights overall, they have limited access to formal dispute resolution systems because of costs involved, and even the informal systems of conflict resolution are beyond the means of many citizens.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1534e164a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1534e171" publication-type="other">
Gordon (1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d1534e178a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1534e185" publication-type="other">
United Nations Development Programme 2006</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1534e191" publication-type="other">
United Nations Devel¬
opment Programme 2006:71</mixed-citation>
            </p>
         </fn>
         <fn id="d1534e201a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1534e208" publication-type="other">
Nyosia focus group, Kisii Central, 19 Oct. 2006</mixed-citation>
            </p>
         </fn>
         <fn id="d1534e215a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1534e222" publication-type="other">
Obotaka focus group, Kisii Central, 29 Sept. 2006</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1534e238a1310">
            <mixed-citation id="d1534e242" publication-type="other">
Acemoglu, Daron, et al. (2004) "Institutions as the Fundamental Cause of Long-Run
Growth." National Bureau of Economic Research Working Paper Series No.
10481, Cambridge, MA, May.</mixed-citation>
         </ref>
         <ref id="d1534e255a1310">
            <mixed-citation id="d1534e259" publication-type="other">
Benjaminsen, Tor A., &amp; Christian Lund (2002) "Formalisation and Informalisation of
Land and Water Rights in Africa: An Introduction," 14 European J. of Development
Research 1.</mixed-citation>
         </ref>
         <ref id="d1534e272a1310">
            <mixed-citation id="d1534e276" publication-type="other">
Berry, Sara (1992) No Condition Is Permanent. Madison, WI: Univ. of Wisconsin Press.</mixed-citation>
         </ref>
         <ref id="d1534e283a1310">
            <mixed-citation id="d1534e287" publication-type="other">
Central Bureau of Statistics (2001) "1999 Population and Housing Census," Central
Bureau of Statistics and Ministry of Finance and Planning, http.VAvww.cbs.go.ke/.</mixed-citation>
         </ref>
         <ref id="d1534e298a1310">
            <mixed-citation id="d1534e302" publication-type="other">
Chanock, Martin (1991) "Paradigms, Policies and Property: A Review of the Customary
Law of Land Tenure," in K. Mann &amp; R. Roberts, eds., Law in Colonial Africa.
Portsmouth, NH: Heinemann Educational Books.</mixed-citation>
         </ref>
         <ref id="d1534e315a1310">
            <mixed-citation id="d1534e319" publication-type="other">
-(1998) Law, Custom, and Social Order: The Colonial Experience in Malawi and Zambia,
2d ed. Portsmouth, NH: Heinemann.</mixed-citation>
         </ref>
         <ref id="d1534e329a1310">
            <mixed-citation id="d1534e333" publication-type="other">
Chimhowu, Admos, &amp; Phil W'oodhouse (2006) "Customary vs Private Property Rights?
Dynamics and Trajectories of Vernacular Land Markets in Sub-Saharan .Africa," 6
J. of Agrarian Change 346-71.</mixed-citation>
         </ref>
         <ref id="d1534e346a1310">
            <mixed-citation id="d1534e350" publication-type="other">
Connolly, Brynna (2005) "Non-State Justice Systems and the State: Proposals for a
Recognition Typology," 38 Connecticut Law Rev 239.</mixed-citation>
         </ref>
         <ref id="d1534e360a1310">
            <mixed-citation id="d1534e364" publication-type="other">
De Soto, Hernando (2000) The Mystery of Capital. New York: Basic Books,</mixed-citation>
         </ref>
         <ref id="d1534e371a1310">
            <mixed-citation id="d1534e375" publication-type="other">
de Sousa Santos, Boaventura (2006) "The Heterogeneous State and Legal Pluralism in
Mozambique," 40 Law &amp; Society Rev. 39-75.</mixed-citation>
         </ref>
         <ref id="d1534e386a1310">
            <mixed-citation id="d1534e390" publication-type="other">
Fitzpatrick, Daniel (2006) "Evolution and Chaos in Property Rights Systems: The Third
World Tragedy of Contested Access," 115 Yale Law J. 996-1048.</mixed-citation>
         </ref>
         <ref id="d1534e400a1310">
            <mixed-citation id="d1534e404" publication-type="other">
Fukuyama, Francis (2004) State-Building: Governance and World Order in the 21st Century.
Ithaca, NY: Cornell Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e414a1310">
            <mixed-citation id="d1534e418" publication-type="other">
Ghai, Y P., &amp; J. P. W. B. McAuslan (1970) Public Law and Political Change in Kenya. New
York: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e428a1310">
            <mixed-citation id="d1534e432" publication-type="other">
Global Health Reporting (2006) "Regional/Global HIV/AIDS Statistics 2006," 10 April,
http://www.Global Health Reporting.org.</mixed-citation>
         </ref>
         <ref id="d1534e442a1310">
            <mixed-citation id="d1534e446" publication-type="other">
Gluckman, Max (1955) The Judicial Process among the Barotse of Northern Rhodesia.
Manchester, United Kingdom: Manchester Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e456a1310">
            <mixed-citation id="d1534e460" publication-type="other">
- (1965) The Ideas in Barotse Jurisprudence. New Haven, CT: Yale Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e468a1310">
            <mixed-citation id="d1534e472" publication-type="other">
Gordon, April (1995) "Gender, Ethnicity and Class in Kenya: 'Burying Otieno' Revis¬
ited," 20 Signs 883-912.</mixed-citation>
         </ref>
         <ref id="d1534e482a1310">
            <mixed-citation id="d1534e486" publication-type="other">
Guyatt, Helen L., et al. (2002) "Too Poor to Pay: Charging for Insecticide-Treated
Bednets in Highland Kenya," 7 Tropical Medicine csf International Health 846-50.</mixed-citation>
         </ref>
         <ref id="d1534e496a1310">
            <mixed-citation id="d1534e500" publication-type="other">
Griffiths, Anne (1998) "Reconfiguring Law: An Ethnographic Perspective from
Botswana," 23 Law &amp; Social Inquiry 587-620.</mixed-citation>
         </ref>
         <ref id="d1534e510a1310">
            <mixed-citation id="d1534e514" publication-type="other">
Hakannson, N. Thomas (1994) "The Detachability of Women: Gender and Kinship in
the Processes of Socio-Economic Change Among the Gusii of Kenya," 21 American
Ethnologist 515-3 8.</mixed-citation>
         </ref>
         <ref id="d1534e527a1310">
            <mixed-citation id="d1534e531" publication-type="other">
Ikdahl, Ingunn, et al. (2005) "Human Rights, Formalization and Women's Land Rights
in Southern and Eastern .Africa," in Studies in Women's Law. Oslo: Institute of
Women's Law, University of Oslo.</mixed-citation>
         </ref>
         <ref id="d1534e544a1310">
            <mixed-citation id="d1534e548" publication-type="other">
Joireman, Sandra F. (2007) "Enforcing New' Property Rights in Sub-Saharan Africa: The
Ugandan Constitution and the 1998 Land Act," 39 Comparative Politics 463-80.</mixed-citation>
         </ref>
         <ref id="d1534e559a1310">
            <mixed-citation id="d1534e563" publication-type="other">
-(2008) "The Mystery of Capital Formation in Sub-Saharan Africa: Women,
Property Rights and Customary Law," 36 World Development 1233-46.</mixed-citation>
         </ref>
         <ref id="d1534e573a1310">
            <mixed-citation id="d1534e577" publication-type="other">
Kane, Minneh, et al. (2005) "Reassessing Customary Law Systems as a Vehicle for Pro¬
viding Equitable Access to Justice for the Poor," World Bank 2005, http://siteres
ources.worldbank.org/INTRANETSOCIALDEVELOPMENT/Resources/Kane.rev.
pdf (accessed 29 March 2007).</mixed-citation>
         </ref>
         <ref id="d1534e593a1310">
            <mixed-citation id="d1534e597" publication-type="other">
Kasanga, Kasim (2002) "Land Tenure, Resource Access and Decentralization in Ghana,"
in C. Toulmin et al., eds., The Dynamics of Resource Tenure in West Africa. Portsmouth,
NH: Heinemann.</mixed-citation>
         </ref>
         <ref id="d1534e610a1310">
            <mixed-citation id="d1534e614" publication-type="other">
Lastarria-Cornhiel, Susana (1997) "Impact of Privatization on Gender and Property
Rights in Africa," 25 World Development 1317-33.</mixed-citation>
         </ref>
         <ref id="d1534e624a1310">
            <mixed-citation id="d1534e628" publication-type="other">
Libecap, Gary (2003) "Contracting for Property Rights," in T. Anderson, &amp;
F. McChesney, eds., Property Rights: Cooperation, Conflict and Law. Princeton,
NJ: Princeton Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e641a1310">
            <mixed-citation id="d1534e645" publication-type="other">
Manji, Ambreena (1999) "Imagining Women's 'Legal World': Towards a Feminist The¬
ory of Legal Pluralism in Africa," 8 Social ö5 Legal Studies 435-55.</mixed-citation>
         </ref>
         <ref id="d1534e656a1310">
            <mixed-citation id="d1534e660" publication-type="other">
Meinzen-Dick, Ruth S., &amp; Rajendra Pradhan (2002) Legal Pluralism and Dynamic Property
Rights. Washington, DC: International Food Policy Research Institute.</mixed-citation>
         </ref>
         <ref id="d1534e670a1310">
            <mixed-citation id="d1534e674" publication-type="other">
Nadel, S. F. (1947) The Nuba. London: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e681a1310">
            <mixed-citation id="d1534e685" publication-type="other">
Norton, Seth W. (2000) "The Cost of Diversity: Endogenous Property Rights and
Growth," 11 Constitutional Political Economy 319-37.</mixed-citation>
         </ref>
         <ref id="d1534e695a1310">
            <mixed-citation id="d1534e699" publication-type="other">
Nyambara, Pius S. (2001) "Immigrants, 'Traditional' Leaders and the Rhodesian State:
the Power of 'Communal' Land Tenure and the Politics of Land Acquisition in
Gokwe, Zimbabwe, 1963-1979," 27/. of Southern African Studies 771-91.</mixed-citation>
         </ref>
         <ref id="d1534e712a1310">
            <mixed-citation id="d1534e716" publication-type="other">
Nyambu-Musembi, Celestine (2003) Review of Experience in Engaging with "Non-State"
Justice Systems in East Africa. Sussex, United Kingdom: Institute of Development
Studies, Sussex University.</mixed-citation>
         </ref>
         <ref id="d1534e729a1310">
            <mixed-citation id="d1534e733" publication-type="other">
Okuro, Samwel Ong'wen (2002) "The Land Question in Kenya: The Place of Land
Tribunals in the Land Reform Process in Kombewa Division." Paper presented at
The Codesria Tenth General Assembly, Kampala-Uganda, 8-12 Dec, http://
www.codesria.org/Archives/galO/papers_galO_12/6plenary_Okuro.htm.</mixed-citation>
         </ref>
         <ref id="d1534e750a1310">
            <mixed-citation id="d1534e754" publication-type="other">
Penal Reform International (2000) Access to Justice in Sub-Saharan Africa: The Role of
Traditional and Informal Justice Systems. London: Astron Printers.</mixed-citation>
         </ref>
         <ref id="d1534e764a1310">
            <mixed-citation id="d1534e768" publication-type="other">
Peters, Pauline E. (2004) "Inequality and Social Conflict Over Land in Africa," 4J. of
Agrarian Change 269-314.</mixed-citation>
         </ref>
         <ref id="d1534e778a1310">
            <mixed-citation id="d1534e782" publication-type="other">
Ranger, Terence (1983) "The Invention of Tradition in Colonial Africa," in T. Ranger &amp;
E. Hobsbaw'm, eds., The Invention of Tradition. New York: Cambridge Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e792a1310">
            <mixed-citation id="d1534e796" publication-type="other">
Ribot, Jesse C. (1999) "Decentralisation, Participation and Accountability in Sahelian
Forestry: Legal Instruments of Political-Administrative Control," 69 Africa 23.</mixed-citation>
         </ref>
         <ref id="d1534e806a1310">
            <mixed-citation id="d1534e810" publication-type="other">
Rose, Laurel L. (2002) "Women's Strategies for Customary Land Access in Swaziland
and Malawi: A Comparative Study," 49 Africa Today 123-49.</mixed-citation>
         </ref>
         <ref id="d1534e820a1310">
            <mixed-citation id="d1534e824" publication-type="other">
Strickland, Richard S. (2004) To Have and to Hold: Women's Property and Inheritance
Rights in the Context of HIV/AIDS in Sub-Saharan Africa," Information brief,
International Center for Research on Women, June, http://wwwr.icrw.org/docs/
2004_info_haveandhold.pdf.</mixed-citation>
         </ref>
         <ref id="d1534e841a1310">
            <mixed-citation id="d1534e845" publication-type="other">
Toulmin, Camila, et al. (2002) The Dynamics of Resource Tenure in West Africa. Portsmouth,
NH: Heinemann.</mixed-citation>
         </ref>
         <ref id="d1534e855a1310">
            <mixed-citation id="d1534e859" publication-type="other">
United Nations Development Programme (2006) "Kenya National Human Develop¬
ment Report 2006," Newr York, UNDP, http://wwrw.ke.undp.org/06nhdrreport.pdf.</mixed-citation>
         </ref>
         <ref id="d1534e869a1310">
            <mixed-citation id="d1534e873" publication-type="other">
Verma, Ritu (2001) Gender, Land and Livelihoods in East Africa: Through Farmers Eyes.
Ottawa: International Development Research Centre.</mixed-citation>
         </ref>
         <ref id="d1534e883a1310">
            <mixed-citation id="d1534e887" publication-type="other">
Waithaka, Michael, et al. (2000) "A Participatory Rapid Appraisal of Farming Systems in
Western Kenya," Report of a PRA of Dairy and Crop Activities in W'estern Kenya,
Smallholder Dairy Project, September, http://www.smallholderdairy.org/publica
tions/Collaborative%20R&amp;D%20reports/Wal/Pages%20from%20Wraithaka%20et%
20al-2000-PRA%20Western%20Kenya%20cov-9.pdf.</mixed-citation>
         </ref>
         <ref id="d1534e906a1310">
            <mixed-citation id="d1534e910" publication-type="other">
Wanyeki, L. Muthoni (2003) "Introduction," in L. M. Wanyeki, ed., Women and Land in
Africa: Culture, Religion and Realizing Women s Land Rights. New York: Zed Books
Ltd.</mixed-citation>
         </ref>
         <ref id="d1534e923a1310">
            <mixed-citation id="d1534e927" publication-type="other">
Weimer, David L. (1997) The Political Economy of Property Rights. New York: Cambridge
Univ. Press.</mixed-citation>
         </ref>
         <ref id="d1534e938a1310">
            <mixed-citation id="d1534e942" publication-type="other">
Whitehead, Ann, &amp; Dzodzi Tsikata (2003) "Policy Discourses on Women's Land Rights
in Sub-Saharan Africa: The Implications of the Re-turn to the Customary," 3J. of
Agrarian Change 67-112.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>Statutes Cited</title>
         <ref id="d1534e964a1310">
            <mixed-citation id="d1534e968" publication-type="other">
The Chief's Act, The Government Printer, Nairobi (1988).</mixed-citation>
         </ref>
         <ref id="d1534e975a1310">
            <mixed-citation id="d1534e979" publication-type="other">
The Land Disputes Tribunal Act, Kenya Gazette Supplement, The Government Printer,
Nairobi (1990).</mixed-citation>
         </ref>
         <ref id="d1534e989a1310">
            <mixed-citation id="d1534e993" publication-type="other">
The Law of Succession Act, The Government Printer, Nairobi (1981).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
