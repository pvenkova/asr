<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25764830</article-id>
         <article-categories>
            <subj-group>
               <subject>MAJOR ARTICLES AND BRIEF REPORTS</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Concordance of CCR5 Genotypes that Influence Cell-Mediated Immunity and HIV-1 Disease Progression Rates</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gabriel</given-names>
                  <surname>Catano</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Zoya A.</given-names>
                  <surname>Chykarenko</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrea</given-names>
                  <surname>Mangano</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>J-M</given-names>
                  <surname>Anaya</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Weijing</given-names>
                  <surname>He</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alison</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rosa</given-names>
                  <surname>Bologna</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Luisa</given-names>
                  <surname>Sen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert A.</given-names>
                  <surname>Clark</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrew</given-names>
                  <surname>Lloyd</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ludmila</given-names>
                  <surname>Shostakovich-Koretskaya</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sunil K.</given-names>
                  <surname>Ahuja</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>15</day>
            <month>1</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">203</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25764811</issue-id>
         <fpage>263</fpage>
         <lpage>272</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/infdis/jiq023"
                   xlink:title="an external site"/>
         <abstract>
            <p>We used cutaneous delayed-type hypersensitivity responses, a powerful in vivo measure of cell-mediated immunity, to evaluate the relationships among cell-mediated immunity, AIDS, and polymorphisms in CCR5, the HIV-1 coreceptor. There was high concordance between CCR5 polymorphisms and haplotype pairs that influenced delayed-type hypersensitivity responses in healthy persons and HIV disease progression. In the cohorts examined, CCR5 genotypes containing -2459G/G (HHA/HHA, HHA/HHC, HHC/HHC) or -2459A/A (HHE/HHE) associated with salutary or detrimental delayed-type hypersensitivity and AIDS phenotypes, respectively. Accordingly, the CCR5-Δ32 allele, when paired with non-Δ32-bearing haplotypes that correlate with low (HHA, HHC) versus high (HHE) CCR5 transcriptional activity, associates with disease retardation or acceleration, respectively. Thus, the associations of CCR5-Δ32 heterozygosity partly reflect the effect of the non-Δ32 haplotype in a background of CCR5 haploinsufficiency. The correlations of increased delayed-type hypersensitivity with -2459G/G-containing CCR5 genotypes, reduced CCR5 expression, decreased viral replication, and disease retardation suggest that CCR5 may influence HIV infection and AIDS, at least in part, through effects on cell-mediated immunity.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d190e329a1310">
            <label>1</label>
            <mixed-citation id="d190e336" publication-type="other">
Kobayashi K, Kaneda K, Kasama T. Immunopathogenesis of delayed -
type hypersensitivity. Microsc Res Tech 2001; 53:241-5.</mixed-citation>
         </ref>
         <ref id="d190e346a1310">
            <label>2</label>
            <mixed-citation id="d190e353" publication-type="other">
Dolan MJ, Clerici M, Blatt SP, et al. In vitro T cell function, delayed-
type hypersensitivity skin testing, and CD4+ T cell subset phenotyping
independently predict survival time in patients infected with human
immunodeficiency virus. J Infect Dis 1995; 172:79-87.</mixed-citation>
         </ref>
         <ref id="d190e369a1310">
            <label>3</label>
            <mixed-citation id="d190e376" publication-type="other">
Maas JJ, Roos MT, Keet IP, et al. In vivo delayed-type hypersensitivity
skin test anergy in human immunodeficiency virus type 1 infection is
associated with T cell nonresponsiveness in vitro. J Infect Dis 1998;
178:1024-9.</mixed-citation>
         </ref>
         <ref id="d190e392a1310">
            <label>4</label>
            <mixed-citation id="d190e399" publication-type="other">
Dolan MJ, Kulkarni H, Camargo JF, et al. CCL3L1 and CCR5 influence
cell-mediated immunity and affect HIV-AIDS pathogenesis via viral
entry-independent mechanisms. Nat Immunol 2007; 8:1324-36.</mixed-citation>
         </ref>
         <ref id="d190e413a1310">
            <label>5</label>
            <mixed-citation id="d190e420" publication-type="other">
Anastos K, Shi Q, French AL, et al. Total lymphocyte count, hemo-
globin, and delayed-type hypersensitivity as predictors of death and
AIDS illness in HIV-1-infected women receiving highly active anti-
retroviral therapy. J Acquir Immune Defic Syndr 2004; 35:383-92.</mixed-citation>
         </ref>
         <ref id="d190e436a1310">
            <label>6</label>
            <mixed-citation id="d190e443" publication-type="other">
Valdez H, Smith KY, Landay A, et al. Response to immunization with
recall and neoantigens after prolonged administration of an HIV-1
protease inhibitor-containing regimen. ACTG 375 team. AIDS Clinical
Trials Group. AIDS 2000; 14:11-21.</mixed-citation>
         </ref>
         <ref id="d190e459a1310">
            <label>7</label>
            <mixed-citation id="d190e466" publication-type="other">
Kaslow RA, Dorak T, Tang JJ. Influence of host genetic variation on
susceptibility to HIV type 1 infection. J Infect Dis 2005; 191:S68-77.</mixed-citation>
         </ref>
         <ref id="d190e476a1310">
            <label>8</label>
            <mixed-citation id="d190e483" publication-type="other">
Mummidi S, Bamshad M, Ahuja SS, et al. Evolution of human and
non-human primate CC chemokine receptor 5 gene and mRNA.
Potential roles for haplotype and mRNA diversity, differential
haplotype-specific transcriptional activity, and altered transcription
factor binding to polymorphic nucleotides in the pathogenesis of
HIV-1 and simian immunodeficiency virus. J Biol Chem 2000;
275:18946-61.</mixed-citation>
         </ref>
         <ref id="d190e509a1310">
            <label>9</label>
            <mixed-citation id="d190e516" publication-type="other">
Shieh B, Liau YE, Hsieh PS, Yan YP, Wang ST, Li C. Influence of
nucleotide polymorphisms in the CCR2 gene and the CCR5 promoter
on the expression of cell surface CCR5 and CXCR4. Int Immunol 2000;
12:1311-8.</mixed-citation>
         </ref>
         <ref id="d190e532a1310">
            <label>10</label>
            <mixed-citation id="d190e539" publication-type="other">
Salkowitz JR, Bruse SE, Meyerson H, et al. CCR5 promoter poly-
morphism determines macrophage CCR5 density and magnitude of
HIV-1 propagation in vitro. Clin Immunol 2003; 108:234-40.</mixed-citation>
         </ref>
         <ref id="d190e553a1310">
            <label>11</label>
            <mixed-citation id="d190e560" publication-type="other">
Thomas SM, Tse DB, Ketner DS, et al. CCR5 expression and duration
of high risk sexual activity among HIV-seronegative men who have sex
with men. AIDS 2006; 20:1879-83.</mixed-citation>
         </ref>
         <ref id="d190e573a1310">
            <label>12</label>
            <mixed-citation id="d190e580" publication-type="other">
Kawamura T, Gulden FO, Sugaya M, et al. R5 HIV productively infects
Langerhans cells, and infection levels are regulated by compound CCR5
polymorphisms. Proc Natl Acad Sci U S A 2003; 100:8401-6.</mixed-citation>
         </ref>
         <ref id="d190e593a1310">
            <label>13</label>
            <mixed-citation id="d190e600" publication-type="other">
Martin MP, Dean M, Smith MW, et al. Genetic acceleration of AIDS
progression by a promoter variant of CCR5. Science 1998;
282:1907-11.</mixed-citation>
         </ref>
         <ref id="d190e613a1310">
            <label>14</label>
            <mixed-citation id="d190e620" publication-type="other">
Gonzalez E, Bamshad M, Sato N, et al. Race-specific HIV-1 disease-
modifying effects associated with CCR5 haplotypes. Proc Natl Acad Sci
USA 1999; 96:12004-9.</mixed-citation>
         </ref>
         <ref id="d190e633a1310">
            <label>15</label>
            <mixed-citation id="d190e640" publication-type="other">
Mangano A, Gonzalez E, Dhanda R, et al. Concordance between the
CC chemokine receptor 5 genetic determinants that alter risks of
transmission and disease progression in children exposed perina-
tally to human immunodeficiency virus. J Infect Dis 2001; 183:
1574-85.</mixed-citation>
         </ref>
         <ref id="d190e659a1310">
            <label>16</label>
            <mixed-citation id="d190e666" publication-type="other">
Loetscher P, Uguccioni M, Bordoli L, et al. CCR5 is characteristic of
Thl lymphocytes. Nature 1998; 391:344-5.</mixed-citation>
         </ref>
         <ref id="d190e677a1310">
            <label>17</label>
            <mixed-citation id="d190e684" publication-type="other">
Camargo JF, Quinones MP, Mummidi S, et al. CCR5 expression levels
influence NFAT translocation, IL-2 production, and subsequent sig-
naling events during T lymphocyte activation. J Immunol 2009;
182:171-82.</mixed-citation>
         </ref>
         <ref id="d190e700a1310">
            <label>18</label>
            <mixed-citation id="d190e707" publication-type="other">
Wu L, Paxton WA, Kassam N, et al. CCR5 levels and expression pattern
correlate with infectability by macrophage-tropic HIV-1, in vitro. J Exp
Med 1997; 185:1681-91.</mixed-citation>
         </ref>
         <ref id="d190e720a1310">
            <label>19</label>
            <mixed-citation id="d190e727" publication-type="other">
Paxton WA, Liu R, Kang S, et al. Reduced HIV-1 infectability of CD4+
lymphocytes from exposed-uninfected individuals: association with
low expression of CCR5 and high production of beta-chemokines.
Virology 1998; 244:66-73.</mixed-citation>
         </ref>
         <ref id="d190e743a1310">
            <label>20</label>
            <mixed-citation id="d190e750" publication-type="other">
de Roda Husman AM, Blaak H, Brouwer M, Schuitemaker H. CC
chemokine receptor 5 cell-surface expression in relation to CC che-
mokine receptor 5 genotype and the clinical course of HIV-1 infection.
J Immunol 1999; 163:4597-603.</mixed-citation>
         </ref>
         <ref id="d190e766a1310">
            <label>21</label>
            <mixed-citation id="d190e773" publication-type="other">
Paxton WA, Kang S, Liu R, et al. HIV-1 infectability of CD4+
lymphocytes with relation to beta-chemokines and the CCR5
coreceptor. Immunol Lett 1999; 66:71-5.</mixed-citation>
         </ref>
         <ref id="d190e786a1310">
            <label>22</label>
            <mixed-citation id="d190e793" publication-type="other">
Gonzalez E, Dhanda R, Bamshad M, et al. Global survey of genetic
variation in CCR5, RANTES, and MIP-1 alpha: impact on the epide-
miology of the HIV-1 pandemic. Proc Natl Acad Sci U S A 2001;
98:5199-204.</mixed-citation>
         </ref>
         <ref id="d190e810a1310">
            <label>23</label>
            <mixed-citation id="d190e817" publication-type="other">
Tang J, Shelton B, Makhatadze NJ, et al. Distribution of chemokine
receptor CCR2 and CCR5 genotypes and their relative contribution to
human immunodeficiency virus type 1 (HIV-1) seroconversion, early
HIV-1 RNA concentration in plasma, and later disease progression.
J Virol 2002; 76:662-72.</mixed-citation>
         </ref>
         <ref id="d190e836a1310">
            <label>24</label>
            <mixed-citation id="d190e843" publication-type="other">
Hladik F, Liu H, Speelmon E, et al. Combined effect of CCR5-Delta32
heterozygosity and the CCR5 promoter polymorphism -2459 A/G on
CCR5 expression and resistance to human immunodeficiency virus
type 1 transmission. J Virol 2005; 79:11677-84.</mixed-citation>
         </ref>
         <ref id="d190e859a1310">
            <label>25</label>
            <mixed-citation id="d190e866" publication-type="other">
Smith A, Vollmer-Conna U, Geczy A, et al. Does genotype mask the
relationship between psychological factors and immune function?
Brain Behav Immun 2005; 19:147-52.</mixed-citation>
         </ref>
         <ref id="d190e879a1310">
            <label>26</label>
            <mixed-citation id="d190e886" publication-type="other">
Shostakovich-Koretskaya L, Catano G, Chykarenko ZA, et al. Com-
binatorial content of CCL3L and CCL4L gene copy numbers influence
HIV-AIDS susceptibility in Ukrainian children. AIDS 2009; 23:
679-88.</mixed-citation>
         </ref>
         <ref id="d190e902a1310">
            <label>27</label>
            <mixed-citation id="d190e909" publication-type="other">
Ahn SH, Kim do Y, Chang HY, et al. Association of genetic variations
in CCR5 and its ligand, RANTES with clearance of hepatitis B virus in
Korea. J Med Virol 2006; 78:1564-71.</mixed-citation>
         </ref>
         <ref id="d190e922a1310">
            <label>28</label>
            <mixed-citation id="d190e929" publication-type="other">
Thio CL, Astemborski J, Bashirova A, et al. Genetic protection against
hepatitis B virus conferred by CCR5Delta32: Evidence that CCR5
contributes to viral persistence. J Virol 2007; 81:441-5.</mixed-citation>
         </ref>
         <ref id="d190e943a1310">
            <label>29</label>
            <mixed-citation id="d190e950" publication-type="other">
Konishi I, Horiike N, Hiasa Y, Michitaka K, Onji M. CCR5 promoter
polymorphism influences the interferon response of patients with
chronic hepatitis C in Japan. Intervirology 2004; 47:114-20.</mixed-citation>
         </ref>
         <ref id="d190e963a1310">
            <label>30</label>
            <mixed-citation id="d190e970" publication-type="other">
Mokubo A, Tanaka Y, Nakajima K, et al. Chemotactic cytokine re-
ceptor 5 (CCR5) gene promoter polymorphism (59029A/G) is asso-
ciated with diabetic nephropathy in Japanese patients with type 2
diabetes: a 10-year longitudinal study. Diabetes Res Clin Pract 2006;
73:89-94.</mixed-citation>
         </ref>
         <ref id="d190e989a1310">
            <label>31</label>
            <mixed-citation id="d190e996" publication-type="other">
Cha RH, Yang SH, Kim HS, et al. Genetic interactions between the
donor and the recipient for susceptibility to acute rejection in kidney
transplantation: polymorphisms of CCR5. Nephrol Dial Transpl 2009;
24:2919-25.</mixed-citation>
         </ref>
         <ref id="d190e1012a1310">
            <label>32</label>
            <mixed-citation id="d190e1019" publication-type="other">
McDermott DH, Zimmerman PA, Guignard F, Kleeberger CA, Leit-
man SF, Murphy PM. CCR5 promoter polymorphism and HIV-1
disease progression. Multicenter AIDS Cohort Study (MACS). Lancet
1998; 352:866-70.</mixed-citation>
         </ref>
         <ref id="d190e1035a1310">
            <label>33</label>
            <mixed-citation id="d190e1042" publication-type="other">
Sellebjerg F, Kristiansen TB, Wittenhagen P, et al. Chemokine receptor
CCR5 in interferon-treated multiple sclerosis. Acta Neurol Scand 2007;
115:413-8.</mixed-citation>
         </ref>
         <ref id="d190e1055a1310">
            <label>34</label>
            <mixed-citation id="d190e1062" publication-type="other">
Pedersen BR, Kamwendo D, Blood M, et al. CCR5 haplotypes
and mother-to-child HIV transmission in Malawi. LoS One 2007;
2:e838.</mixed-citation>
         </ref>
         <ref id="d190e1076a1310">
            <label>35</label>
            <mixed-citation id="d190e1083" publication-type="other">
Panzer U, Schneider A, Steinmetz OM, et al. The chemokine receptor 5
Delta32 mutation is associated with increased renal survival in patients
with IgA nephropathy. Kidney Int 2005; 67:75-81.</mixed-citation>
         </ref>
         <ref id="d190e1096a1310">
            <label>36</label>
            <mixed-citation id="d190e1103" publication-type="other">
Reynes J, Baillat V, Portales P, Clot J, Corbeau P. Low CD4+ T-cell
surface CCR5 density as a cause of resistance to in vivo HIV-1 in-
fection. J Acquir Immune Defic Syndr 2003; 34:114-6.</mixed-citation>
         </ref>
         <ref id="d190e1116a1310">
            <label>37</label>
            <mixed-citation id="d190e1125" publication-type="other">
Gervaix A, Nicolas J, Portales P, et al. Response to treatment and
disease progression linked to CD4+ T cell surface CC chemokine re-
ceptor 5 density in human immunodeficiency virus type 1 vertical
infection. J Infect Dis 2002; 185:1055-61.</mixed-citation>
         </ref>
         <ref id="d190e1141a1310">
            <label>38</label>
            <mixed-citation id="d190e1148" publication-type="other">
Vincent T, Portales P, Baillat V, et al. The immunological response to
highly active antiretroviral therapy is linked to CD4+ T-cell surface
CCR5 density. J Acquir Immune Dene Syndr 2006; 43:377-8.</mixed-citation>
         </ref>
         <ref id="d190e1161a1310">
            <label>39</label>
            <mixed-citation id="d190e1168" publication-type="other">
Reynes J, Baillat V, Portales P, Clot J, Corbeau P. Relationship between
CCR5 density and viral load after discontinuation of antiretroviral
therapy. JAMA 2004; 291:46.</mixed-citation>
         </ref>
         <ref id="d190e1181a1310">
            <label>40</label>
            <mixed-citation id="d190e1188" publication-type="other">
Heredia A, Gilliam B, DeVico A, et al. CCR5 density levels on primary
CD4 T cells impact the replication and Enfuvirtide susceptibility of R5
HIV-1. AIDS 2007; 21:1317-22.</mixed-citation>
         </ref>
         <ref id="d190e1202a1310">
            <label>41</label>
            <mixed-citation id="d190e1209" publication-type="other">
Choudhry V, Zhang MY, Harris I, et al. Increased efficacy of HIV-1
neutralization by antibodies at low CCR5 surface concentration. Bio-
chem Biophys Res Commun 2006; 348:1107-15.</mixed-citation>
         </ref>
         <ref id="d190e1222a1310">
            <label>42</label>
            <mixed-citation id="d190e1229" publication-type="other">
Pandrea I, Apetrei C, Gordon S, et al. Paucity of CD4+CCR5+ T cells
is a typical feature of natural SIV hosts. Blood 2007; 109:1069-76.</mixed-citation>
         </ref>
         <ref id="d190e1239a1310">
            <label>43</label>
            <mixed-citation id="d190e1246" publication-type="other">
Schroder C, Pierson RN 3rd, Nguyen BN, et al. CCR5 blockade
modulates inflammation and alloimmunity in primates. J Immunol
2007; 179:2289-99.</mixed-citation>
         </ref>
         <ref id="d190e1259a1310">
            <label>44</label>
            <mixed-citation id="d190e1266" publication-type="other">
Fischereder M, Luckow B, Hocher B, et al. CC chemokine receptor 5
and renal-transplant survival. Lancet 2001; 357:1758-61.</mixed-citation>
         </ref>
         <ref id="d190e1276a1310">
            <label>45</label>
            <mixed-citation id="d190e1283" publication-type="other">
Smith KY, Kumar S, Pulvirenti JJ, Gianesin M, Kessler HA, Landay A.
CCR5 and CXCR4 expression after highly active antiretroviral therapy
(HAART). J Acquir Immune Dene Syndr 2002; 30:458-60.</mixed-citation>
         </ref>
         <ref id="d190e1296a1310">
            <label>46</label>
            <mixed-citation id="d190e1303" publication-type="other">
Koning FA, Otto SA, Hazenberg MD, et al. Low-level CD4+ T cell
activation is associated with low susceptibility to HIV-1 infection. J
Immunol 2005; 175:6117-22.</mixed-citation>
         </ref>
         <ref id="d190e1317a1310">
            <label>47</label>
            <mixed-citation id="d190e1324" publication-type="other">
van Asten L, Danisman F, Otto SA, et al. Pre-seroconversion immune
status predicts the rate of CD4 T cell decline following HIV infection.
Aids 2004; 18:1885-93.</mixed-citation>
         </ref>
         <ref id="d190e1337a1310">
            <label>48</label>
            <mixed-citation id="d190e1344" publication-type="other">
Hazenberg MD, Otto SA, van Benthem BH, et al. Persistent immune
activation in HIV-1 infection is associated with progression to AIDS.
AIDS 2003; 17:1881-8.</mixed-citation>
         </ref>
         <ref id="d190e1357a1310">
            <label>49</label>
            <mixed-citation id="d190e1364" publication-type="other">
Miyazawa M, Lopalco L, Mazzotta F, Lo Caputo S, Veas F, Clerici M.
The 'immunologic advantage' of HIV-exposed seronegative in-
dividuals. AIDS 2009; 23:161-75.</mixed-citation>
         </ref>
         <ref id="d190e1377a1310">
            <label>50</label>
            <mixed-citation id="d190e1384" publication-type="other">
Begaud E, Chartier L, Marechal V, et al. Reduced CD4 T cell activation
and in vitro susceptibility to HIV-1 infection in exposed uninfected
Central Africans. Retro virology 2006; 3:35.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
