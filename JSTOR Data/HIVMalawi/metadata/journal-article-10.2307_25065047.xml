<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id journal-id-type="jstor">j100641</journal-id>
         <journal-title-group>
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn pub-type="ppub">03057070</issn>
         <issn pub-type="epub">14653893</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25065047</article-id>
         <title-group>
            <article-title>'Guards and Guns': Towards Privatised Militarism in Post-Apartheid South Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Jacklyn</given-names>
                  <surname>Cock</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i25065039</issue-id>
         <fpage>791</fpage>
         <lpage>803</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 The Editorial Board of the Journal of Southern African Studies</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25065047"/>
         <abstract>
            <p>This article argues that contemporary South Africa is marked by the coexistence of both old and new forms of militarism. A shallow and uneven process of state demilitarisation was underway between 1990 to 1998 in the form of reductions in military expenditure, weapons holdings, force levels, employment in arms production and base closures. However, this has had contradictory consequences including providing an impetus to a 'privatised militarism' that is evident in three related processes: new forms of violence, the growth of private security firms and the proliferation of small arms. Since 1998 a process of re-militarisation is evident in the use of the military in foreign policy and a re-armament programme. Both trends illustrate how a restructured, but not transformed, post-apartheid army represents a powerful block of military interests.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d57e114a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d57e121" publication-type="other">
A. Appadurai, 'Deep Democracy: Urban Governmentality and the Horizon of Polities', Public Culture, 14, 1
(2002), p. 24.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e131a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d57e138" publication-type="other">
C Tilly, 'War and the International System 1900-1992' (unpublished paper, New York, 1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e144" publication-type="other">
V. Berghahn,
Militarism: The History of an International Debate (Cambridge, Cambridge University Press, 1981)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e153" publication-type="other">
M. Shaw,
Post-military Society (London, Blackwell, 1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e163" publication-type="other">
E. Hutchful, 'Demilitarising the Political Process in Africa:
Some Basic Issues', African Security Review, 16, 2 (1997), pp. 3-11.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e173a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d57e180" publication-type="other">
L. Merryfinch, 'Militarism', in W. Chapkis (ed.), Loaded Questions: Women in the Military (Amsterdam,
Transnational Institute, 1981), p. 9.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e190a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d57e197" publication-type="other">
Shaw, Post-military Society, p. 14.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e205a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d57e212" publication-type="other">
M. Mann, 'The Roots and Contradictions of Modern Militarism', New Left Review, 167 (March 1987), p. 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e219a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d57e226" publication-type="other">
E.P. Thompson, 'Notes on Exterminism, the Last Stage of Civilization', in E.P. Thompson et al., Exterminism
and the Cold War (London, Verso, 1982), p. 21.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e236a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d57e243" publication-type="other">
Ibid., p. 22.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e250a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d57e257" publication-type="other">
J. Cock and L. Nathan (eds), War and Society. The Militarization of South Africa (Cape Town, David Philip,
1989)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e266" publication-type="other">
K. Grundy, The Militarisation of South African Politics (Oxford, Oxford University Press, 1988).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e273a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d57e280" publication-type="other">
Cock and Nathan, War and Society</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e286" publication-type="other">
G. Cawthra and B. M0ller, Defensive Restructuring of the Armed Forces in
Southern Africa (Dartmouth, Macmillan, 1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e295" publication-type="other">
P. Frankel, Pretoria's Praetorians; Civil Military Relations in
South Africa (Oxford, Oxford University Press, 1984)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e305" publication-type="other">
G. Cawthra, Brutal Force: The Apartheid War Machine
(London, International Defence and Aid Fund, 1988).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e315a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d57e322" publication-type="other">
J. Cock, 'The Role of Violence in Current State Security Strategies', in M. Swilling (ed.), Views on the South
African State (Pretoria, Human Sciences Research Council, 1990), pp. 85-105.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e333a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d57e340" publication-type="other">
H. Marais, South Africa. Limits to Change: The Political Economy of Transformation (London, Zed Books,
1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e350a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d57e357" publication-type="other">
Department of Defence, Annual Report (Pretoria, Department of Defence, 2004), p. 12.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e364a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d57e371" publication-type="other">
R. Luckham, 'The Military, Militarism and Democratization in Africa', in E. Hutchful and A. Bathily (eds), The
Military and Militarism in Africa (Dakar, Codesria, 1998), p. 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e381a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d57e388" publication-type="other">
'How Does the Arms Deal Measure Up To Our Needs?', Sunday Independent, 27 May 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e395a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d57e402" publication-type="other">
'Defence Needs are Social Needs', The Star, 24 July 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e409a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d57e416" publication-type="other">
1. Liebenberg, 'The Integration of the Military in Post-liberation South Africa', Armed Forces and Society, 24, 1
(1996), pp. 105-32</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e425" publication-type="other">
Cawthra and M0ller, Defensive Restructuring; G. Kynoch, 'The Transformation of the South
African Military', Journal of Modern African Studies, 34, 3 (1996), pp. 441-57.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e436a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d57e443" publication-type="other">
SANDF, Sipiwe Nyanda, cited in The Sunday Independent, 14 July 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e450a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d57e457" publication-type="other">
Nyanda cited in 'We are an Integrated Force', The Sowetan, 24 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e464a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d57e471" publication-type="other">
Briefing from the SANDF to the Defence Portfolio Committee, 27 May 2003. Parliamentary Monitoring Group
available at http://www.pmg.org.za (accessed 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e481a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d57e488" publication-type="other">
'Arms Deal Cripples SANDF', Mail &amp;amp; Guardian, 20 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e495a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d57e502" publication-type="other">
Boshoff, 'Defence Needs are Social Needs'.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e509a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d57e516" publication-type="other">
Surgeon-General, Lt.-General Van Rensberg to the Defence Portfolio Committee, 18 March
2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e527a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d57e534" publication-type="other">
Minister of Defence, Joe Modise, cited in Mail and Guardian, 8 November 1996.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e541a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d57e548" publication-type="other">
Pan African Congress Deputy Secretary-General, Wonder Masombuka, cited in The Sunday Times, 14 January
2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e558a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d57e565" publication-type="other">
Interview with Rocky Williams, Johannesburg, 4 May 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e571" publication-type="other">
J. Cock, 'Towards a Common Society: the
Integration of Soldiers and Armies in a Future South Africa' (unpublished paper, Johannesburg, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e581a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d57e588" publication-type="other">
SANDF to the Defence Portfolio Committee, 4 March 2003. Parliamentary Monitoring Group.
Available at http://www.pmg.org.za (accessed 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e598a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d57e605" publication-type="other">
World Bank, Demobilisation and Reintegration of Military Personnel in Africa: The Evidence From Seven
Country Case Studies (Washington DC, The World Bank, 1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e614" publication-type="other">
N. Ball, 'The International Development
Community's Response to Demobilisation', in Bonn International Centre for Conversion (BICC), Converting
Defense Resources to Human Development, Report No. 12 (Bonn, BICC, 1998), pp. 21-7</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e626" publication-type="other">
N. Ball,
'Demobilising and Reintegrating Soldiers: Lessons from Africa', in K. Kumar (ed.), Rebuilding Societies After
Civil War: Critical Roles for International Assistance (Boulder, CO, Lynne Rienner, 1997), pp. 85-105.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e639a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d57e646" publication-type="other">
R. Preston, War-Affected Namibians: Demobilising and Integrating Fighters (Windhoek, University of Namibia,
1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e655" publication-type="other">
M. Musemwa, 'The Ambiguities of Democracy: The Demobilization of the Zimbabwean Ex-Combatants
and the Ordeal of Rehabilitation, 1980-1993', Transformation, 26 (1995), pp. 28-34.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e666a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d57e673" publication-type="other">
K. Kingma, 'Demobilisation, Reintegration and Peacebuilding in Southern Africa', in P. Batchelor and
K. Kingma (eds), Demilitarisation and Peace-building in Southern Africa. Volume 1: Concepts and Processes
(Aldershot, Ashgate, 2004), pp. 133-62</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e685" publication-type="other">
A. Vines, 'Disarmament in Mozambique', Journal of Southern African
Studies, 24, 1 (March 1998), pp. 35-47</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e694" publication-type="other">
J. Cilliers (ed.), Dismissed. Demobilisation and Reintegration of Former
Combatants in Africa (Pretoria, Institute for Defence Policy, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e704a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d57e711" publication-type="other">
A. Minaar, cited in the Sowetan, 29 September 1998.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e718a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d57e725" publication-type="other">
Interviews, Johannesburg, 12 June 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e732a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d57e739" publication-type="other">
Cock, 'Towards a Common Society', p. 25</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e745" publication-type="other">
J. Cock, 'The Social Integration of Demobilised Soldiers in
Contemporary South Africa', African Defence Review, 2, 12 (1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e755a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d57e762" publication-type="other">
I. Liebenberg and M. Roelfs, 'Demobilisation and its Aftermath: Economic Reinsertion of South Africa's
Demobilized Military Personnel', Institute for Security Studies Monograph Series, 61 (Johannesburg, 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e772a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d57e779" publication-type="other">
S. Gear, Wishing Us Away. Challenges Facing Ex-Combatants in the 'New' South Africa (Johannesburg, Centre
for the Study of Violence and Reconciliation, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e790a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d57e797" publication-type="other">
Centre for Conflict Resolution (CCR), 'Soldiers of Misfortune' (unpublished paper, Cape Town, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e804a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d57e811" publication-type="other">
Ibid, p. 39.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e818a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d57e825" publication-type="other">
Ibid, p. 51.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e832a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d57e839" publication-type="other">
Mail and Guardian, 8 September 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e846a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d57e853" publication-type="other">
CCR, 'Soldiers of Misfortune', p. 49.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e860a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d57e867" publication-type="other">
Ibid, p. 50.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e875a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d57e882" publication-type="other">
Ibid., p. 53.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e889a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d57e896" publication-type="other">
Business Report, 30 May 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e903a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d57e910" publication-type="other">
The Star, 24 June 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e917a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d57e924" publication-type="other">
A. Vines, Peace Postponed: Angola since the Lusaka Protocol (London, Catholic Institute for International
Relations, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e934a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d57e941" publication-type="other">
Interview, Pretoria, 5 August 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e948a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d57e955" publication-type="other">
R. Chetty (ed.), Firearm Use and Distribution in South Africa (Pretoria, National Crime Prevention Centre,
2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e966a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d57e973" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e980a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d57e987" publication-type="other">
Ibid</mixed-citation>
            </p>
         </fn>
         <fn id="d57e994a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d57e1001" publication-type="other">
Interview with G. Simpson, Johannesburg, 3 August 1999.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1008a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d57e1015" publication-type="other">
Minister of Police, cited in The Star, 2 April 1998.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1022a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d57e1029" publication-type="other">
Interview with Sheena Duncan, head of the investigation into the Central Firearms Register, Johannesburg,
3 November 2000.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1039a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d57e1046" publication-type="other">
The Star, 17 June 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1054a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d57e1061" publication-type="other">
Interviews, Johannesburg, 7 September 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1068a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d57e1075" publication-type="other">
The Saturday Star, 7 June 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1082a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d57e1089" publication-type="other">
Sunday Times, 1 June 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1096a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d57e1103" publication-type="other">
C. Gould and G. Lamb, Hide and Seek. Taking Account of Small Arms in Southern Africa (Pretoria, Institute for
Security Studies, 2004), p. 185.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1113a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d57e1120" publication-type="other">
CCR, 'Soldiers of Misfortune', p. 42.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1127a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d57e1134" publication-type="other">
S. Hall, Policing the Crisis. Mugging, the State and Law and Order (London, Macmillan, 1978), p. 157.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1142a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d57e1149" publication-type="other">
E. Hellman, 'Non-Europeans in the Army', Race Relations, 10, 2 (1943), pp. 45-53.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1156a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d57e1163" publication-type="other">
Interviews, Johannesburg, 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1170a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d57e1177" publication-type="other">
R. Williams, Beyond 2000 (Harmondsworth, Penguin, 1983), p. 45.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1184a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d57e1191" publication-type="other">
R.T. Naylor, 'The Structure and Operation of the Modern Arms Black Market', pp. 44-57, in J. Boutwell, M.
Klare and L. Reed (eds), Lethal Commerce: The Global Trade in Small Arms and Light Weapons (Cambridge,
MA, Committee on International Security and the American Academy of Arts and Sciences, 1995), p. 35.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1204a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d57e1211" publication-type="other">
South African Labour Bulletin, 'The Challenges of Growth and Poverty', pp. 37-41</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d57e1217" publication-type="other">
South African Labour
Bulletin, 27, 5 (2003), p. 39.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1227a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d57e1234" publication-type="other">
Boshoff, 'Defence Needs are Social Needs'.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1242a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d57e1249" publication-type="other">
'We'll Kill You, Rebels Warn SA Soldiers', The Star, 28 May 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1256a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d57e1263" publication-type="other">
'SA Combat Troops Prepare for Congo', Sunday Times, 4 August 2002, p. 6.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1270a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d57e1277" publication-type="other">
'SA to Send Extra Troops to DRC, Sunday Times, 15 June 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1284a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d57e1291" publication-type="other">
'A Cause For Concern', Mail and Guardian, 26 July 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1298a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d57e1305" publication-type="other">
'ANC Downplays Arms Deal Offsets', Mail and Guardian, 16 August 2002, p. 9.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1312a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d57e1319" publication-type="other">
'The Thorny Issues at the Heart of the Arms Bill', Sunday Independent, 21 July 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1327a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d57e1334" publication-type="other">
'How Does the Arms Deal Measure Up To Our Needs?', Sunday Independent, 27 May 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1341a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d57e1348" publication-type="other">
J. Cock, 'Butterfly Wings' and 'Green Shoots': The Impact of Peace Organizations in Southern Africa',
pp. 182-212, in Batchelor and Kingma (eds), Volume 2: National and Regional Experiences (Aldershot,
Ashgate, 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1361a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d57e1368" publication-type="other">
Terry Crawford-Brown, cited in Mail and Guardian, 26 July 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1375a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d57e1382" publication-type="other">
'Arms Deal Cripples SANDF', Mail and Guardian, 20 March 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1389a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d57e1396" publication-type="other">
'SWAPO MPs Call for More Spending on the Military', Business Day, 11 April 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1403a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d57e1410" publication-type="other">
L. Nathan, 'Organ Failure: A Review of the S ADC Organ on Politics, Defence and Security' (unpublished paper,
Cape Town, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1421a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d57e1428" publication-type="other">
Interview with T. Motumi, Pretoria, 1 April 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1435a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d57e1442" publication-type="other">
'AU's Peace Force Plan Shot Down As Too Expensive', The Star, 27 May 2003.</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1449a1310">
            <label>80</label>
            <p>
               <mixed-citation id="d57e1456" publication-type="other">
Department of Defence, Annual Reports 1997-2004 (Pretoria, Department of Defence, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d57e1463a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d57e1470" publication-type="other">
R. Bahro, From Red to Green (London, Verso, 1982).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
