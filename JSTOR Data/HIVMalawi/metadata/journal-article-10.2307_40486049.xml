<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">philtranbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100835</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Philosophical Transactions: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628436</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40486049</article-id>
         <article-id pub-id-type="pub-doi">10.1098/rstb.2009.0067</article-id>
         <article-categories>
            <subj-group>
               <subject>Reviews</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Neglected and Endemic Zoonoses</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ian</given-names>
                  <surname>Maudlin</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mark Charles</given-names>
                  <surname>Eisler</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Susan Christina</given-names>
                  <surname>Welburn</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>27</day>
            <month>9</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">364</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1530</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40021303</issue-id>
         <fpage>2777</fpage>
         <lpage>2787</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40486049"/>
         <abstract>
            <p>Endemic zoonoses are found throughout the developing world, wherever people live in close proximity to their animals, affecting not only the health of poor people but often also their livelihoods through the health of their livestock. Unlike newly emerging zoonoses that attract the attention of the developed world, these endemic zoonoses are by comparison neglected. This is, in part, a consequence of under-reporting, resulting in underestimation of their global burden, which in turn artificially downgrades their importance in the eyes of administrators and funding agencies. The development of cheap and effective vaccines is no guarantee that these endemic diseases will be eliminated in the near future. However, simply increasing awareness about their causes and how they may be prevented—often with very simple technologies—could reduce the incidence of many endemic zoonoses. Sustainable control of zoonoses is reliant on surveillance, but, as with other public-sector animal health services, this is rarely implemented in the developing world, not least because of the lack of sufficiently cheap diagnostics. Public–private partnerships have already provided advocacy for human disease control and could be equally effective in addressing endemic zoonoses.</p>
         </abstract>
         <kwd-group>
            <kwd>endemic zoonoses</kwd>
            <kwd>poverty</kwd>
            <kwd>burden of disease</kwd>
            <kwd>under-reporting</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1055e176a1310">
            <mixed-citation id="d1055e180" publication-type="other">
Allan, J. C, Wilkins, P. P., Tsang, V. C. &amp; Craig, P. S. 2003
Immunodiagnostic tools for taeniasis. Acta Trop. 87,
87-103.</mixed-citation>
         </ref>
         <ref id="d1055e193a1310">
            <mixed-citation id="d1055e197" publication-type="other">
Bern, C., Garcia, H. H., bvans, C, Gonzalez, A. b., Veras-
tegui, M., Tsang, V. C. &amp; Gilman, R. H. 1999 Magnitude
of the disease burden from neurocysticercosis in a devel-
oping country. Clin. Infect. Dis. 29, 1203-1209. (doi:10.
1086/313470)</mixed-citation>
         </ref>
         <ref id="d1055e216a1310">
            <mixed-citation id="d1055e220" publication-type="other">
Bern, C, Joshi, A. B., Jha, S. N., Das, M. L., Hightower, A.,
Thakur, G. D. &amp; Bista, M. B. 2000 Factors associated
with visceral leishmaniasis in Nepal: bed-net use is
strongly protective. Am. J. Trop. Med. Hyg. 63, 184-188.</mixed-citation>
         </ref>
         <ref id="d1055e236a1310">
            <mixed-citation id="d1055e240" publication-type="other">
Bern, C, Maguire, J. H. &amp; Alvar, J. 2008 Complexities of
assessing the disease burden attributable to leishmaniasis.
PLoSNegl. Trop. Dis. 2, e313. (doi:10.1371/journal.pntd.
0000313)</mixed-citation>
         </ref>
         <ref id="d1055e257a1310">
            <mixed-citation id="d1055e261" publication-type="other">
Beutels, P., Scuffham, P. &amp; Maclntyre, C. 2008 Funding of
drugs: do vaccines warrant a different approach? Lancet
Infect. Dis. 8, 727-733. (doi:10.1016/S1473-3099(08)
70258-5)</mixed-citation>
         </ref>
         <ref id="d1055e277a1310">
            <mixed-citation id="d1055e281" publication-type="other">
Boutayeb, A. 2007 Developing countries and neglected dis-
eases: challenges and perspectives. Int. J. Equity Health 6,
20. (doi:10.1186/1475-9276-6-20)</mixed-citation>
         </ref>
         <ref id="d1055e294a1310">
            <mixed-citation id="d1055e298" publication-type="other">
Briggs, D. &amp; Hanlon, A. 2007 World Rabies Day: focusing
attention on a neglected disease. Vet. Rec. 161, 288-289.</mixed-citation>
         </ref>
         <ref id="d1055e308a1310">
            <mixed-citation id="d1055e312" publication-type="other">
Budke, C. M., Deplazes, P. &amp; Torgerson, P. R. 2006 Global
socioeconomic impact of cystic echinococcosis. Emerg.
Infect. Dis. 12, 296-303.</mixed-citation>
         </ref>
         <ref id="d1055e325a1310">
            <mixed-citation id="d1055e329" publication-type="other">
Canning, D. 2006 Priority setting and the 'neglected' tropi-
cal diseases. Trans. R. Soc. Trop. Med. Hyg. 100, 499-504.
(doi:10.1016/j.trstmh.2006.02.001)</mixed-citation>
         </ref>
         <ref id="d1055e342a1310">
            <mixed-citation id="d1055e346" publication-type="other">
Cleaveland, S., Shaw, D. J., Mfinanga, S. G., Shirima, G.,
Kazwala, R. R., Eblate, E. &amp; Sharp, M. 2007 Mycobacter-
ium bovis in rural Tanzania: risk factors for infection in
human and cattle populations. Tuberculosis (Edinb.) 87,
30-43. (doi:10.1016/j.tube.2006.03.001)</mixed-citation>
         </ref>
         <ref id="d1055e366a1310">
            <mixed-citation id="d1055e370" publication-type="other">
Cosivi, O., Meslin, F. X., Daborn, C. J. &amp; Grange, J. M.
1995 Epidemiology of Mycobacterium bovis infection in
animals and humans, with particular reference to
Africa. Rev. Sci. Tech. 14, 733-746.</mixed-citation>
         </ref>
         <ref id="d1055e386a1310">
            <mixed-citation id="d1055e390" publication-type="other">
Craig, P. S. &amp; Larrieu, E. 2006 Control of cystic echino-
coccosis/hydatidosis: 1863-2002. Adv. Parasitol. 61,
443-508. (doi:10.1016/S0065-308X(05)61011-1)</mixed-citation>
         </ref>
         <ref id="d1055e403a1310">
            <mixed-citation id="d1055e407" publication-type="other">
Craig, r. 5. et al. 2007 Prevention and control of cystic echi-
nococcosis. Lancet Infect. Dis. 7, 385-394. (doi:10.1016/
S1473-3099(07)70134-2)</mixed-citation>
         </ref>
         <ref id="d1055e420a1310">
            <mixed-citation id="d1055e424" publication-type="other">
Dantas-Torres, F. 2008 Canine vector-borne diseases
in Brazil. Parasit. Vectors 1, 25. (doi:10.1186/1756-3305-
1-25)</mixed-citation>
         </ref>
         <ref id="d1055e437a1310">
            <mixed-citation id="d1055e441" publication-type="other">
de la Rua-Domenech, R. 2006 Human Mycobacterium bovis
infection in the United Kingdom: incidence, risks, control
measures and review of the zoonotic aspects of bovine
tuberculosis. Tuberculosis (Edinb.) 86, 77-109. (doi:10.
1016/j.tube.2005.05.002)</mixed-citation>
         </ref>
         <ref id="d1055e460a1310">
            <mixed-citation id="d1055e464" publication-type="other">
Dias, J. C, Silveira, A. C. &amp; Schofield, C. J. 2002 The
impact of Chagas disease control in Latin America: a
review. Mem. Inst. Oswaldo Cruz 97, 603-612.(doi:10.
1590/S0074-02762002000500002)</mixed-citation>
         </ref>
         <ref id="d1055e481a1310">
            <mixed-citation id="d1055e485" publication-type="other">
Easterly, W. 2001 The elusive quest for growth. Cambridge,
MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1055e495a1310">
            <mixed-citation id="d1055e499" publication-type="other">
Easterly, W 2006 The White Man's Burden. New York, NY:
Penguin Press.</mixed-citation>
         </ref>
         <ref id="d1055e509a1310">
            <mixed-citation id="d1055e513" publication-type="other">
Ensennk, M. 2007 Entomology. Welcome to Ethiopia's fly
factory. Science 317, 310-313. (doi:10.1126/science.
317.5836.310)</mixed-citation>
         </ref>
         <ref id="d1055e526a1310">
            <mixed-citation id="d1055e530" publication-type="other">
European Parliament 2005 Resolution on major and
neglected diseases in developing countries. 2005/
2047 (INI), Strasbourg. Adopted 8 September 2005; see
http://www.europarl.eu.int/omk/sipade3?PUBREF=-//EP//
TEXT+TA+P6-TA-2005-0341+0+DOC+XML+V0//
EN&amp;L=EN&amp;LEVEL=O&amp;NAV=S&amp;LSTDOC=Y&amp;
LSTDOC=N.</mixed-citation>
         </ref>
         <ref id="d1055e556a1310">
            <mixed-citation id="d1055e560" publication-type="other">
Feachem, R. &amp; Sabot, O. 2008 A new global malaria eradica-
tion strategy. Lancet 371, 1633-1635. (doi:10.1016/
S0140-6736(08)60424-9)</mixed-citation>
         </ref>
         <ref id="d1055e573a1310">
            <mixed-citation id="d1055e577" publication-type="other">
Fenwick, A. 2006 New initiatives against Africa's worms.
Trans. R. Soc. Trop. Med. Hyg. 100, 200-207. (doi:10.
1016/j.trstmh.2005.03.014)</mixed-citation>
         </ref>
         <ref id="d1055e591a1310">
            <mixed-citation id="d1055e595" publication-type="other">
Ferroglio, E., Poggi, M. &amp; Trisciuoglio, A. 2008 Evaluation
of 65% permethrin spot-on and deltamethrin-
impregnated collars for canine Leishmania infantum
infection prevention. Zoonoses Public Health 55, 145-148.
(doi:10.1111/j.1863-2378.2007.01092.x)</mixed-citation>
         </ref>
         <ref id="d1055e614a1310">
            <mixed-citation id="d1055e618" publication-type="other">
Fèvre, E. M., Coleman, P. G., Odiit, M., Magona, J. W.,
Welburn, S. C. &amp; Woolhouse, M. E. J. 2001 The origins
of a new Trypanosoma brucei rhodesiense sleeping sickness
outbreak in eastern Uganda. Lancet 358, 625-628.
(doi:10.1016/S0140-6736(01)05778-6)</mixed-citation>
         </ref>
         <ref id="d1055e637a1310">
            <mixed-citation id="d1055e641" publication-type="other">
Fèvre, E. M., Wissmann, B. V, Welburn, S. C. &amp; Lutumba,
P. 2008 The burden of human African tripanosomiasis.
PLoS Negl Trop. Dis. 2, e333. (doi:10.1371/journal.
pntd.0000333)</mixed-citation>
         </ref>
         <ref id="d1055e657a1310">
            <mixed-citation id="d1055e661" publication-type="other">
Foglia Manzillo, V, Oliva, G., Pagano, A., Manna, L.,
Maroli, M. &amp; Gradoni, L. 2006 Deltamethrin-
impregnated collars for the control of canine leishmaniasis:
evaluation of the protective effect and influence on the
clinical outcome of Leishmania infection in kennelled
stray dogs. Vet. Parasitol. 142, 142-145. (doi:10.1016/
j.vetpar.2006.06.029)</mixed-citation>
         </ref>
         <ref id="d1055e687a1310">
            <mixed-citation id="d1055e691" publication-type="other">
Franco, M., Mulder, M., Gilman, R. &amp; Smits, H. 2007
Human brucellosis. Lancet Infect. Dis. 7, 775-786.
(doi:10.1016/S1473-3099(07)70286-4)</mixed-citation>
         </ref>
         <ref id="d1055e704a1310">
            <mixed-citation id="d1055e708" publication-type="other">
Freuling, C, Selhorst, T, Bätza, H. J. &amp; Müller, T. 2008 The
financial challenge of keeping a large region rabies-free -
the EU example. Dev. Biol. 131, 273-282.</mixed-citation>
         </ref>
         <ref id="d1055e722a1310">
            <mixed-citation id="d1055e726" publication-type="other">
Garcia, H. H., Parkhouse, R. M., Gilman, R. H.,
Montenegro, T., Bernal, T., Martinez, S. M., Gonzalez,
A. E., Tsang, V. C. &amp; Harrison, L. J. (Cysticercosis Work-
ing Group in Peru) 2000 Serum antigen detection in the
diagnosis, treatment, and follow-up of neurocysticercosis
patients. Trans. R. Soc. Trop. Med. Hyg. 94, 673-676.
(doi:10.1016/S0035-9203(00)90228-1)</mixed-citation>
         </ref>
         <ref id="d1055e752a1310">
            <mixed-citation id="d1055e756" publication-type="other">
Giffoni, J. H., de Almeida, C. E., dos Santos, S. O., Ortega,
V. S. &amp; de Barros, A. T. 2002 Evaluation of 65%
permethrin spot-on for prevention of canine visceral leish-
maniasis: effect on disease prevalence and the vectors
(Diptera: Psychodidae) in a hyperendemic area. Vet.
Ther. 3, 485-492.</mixed-citation>
         </ref>
         <ref id="d1055e779a1310">
            <mixed-citation id="d1055e783" publication-type="other">
Godfroid, J. &amp; Käsbohrer, A. 2002 Brucellosis in the
European Union and Norway at the turn of the twenty-
first century. Vet. Microbiol. 90, 135-145. (doi:10.1016/
S0378-1135(02)00217-1)</mixed-citation>
         </ref>
         <ref id="d1055e799a1310">
            <mixed-citation id="d1055e803" publication-type="other">
Hargrove, J. W. 2003 Tsetse eradication: sufficiency, neces-
sity and desirability. Research Report, DFID Animal
Health Programme, Centre for Tropical Veterinary Medi-
cine. Edinburgh, UK: University of Edinburgh. See http://
www.sun.ac.za/sacema/publications/TsetseEradication.html.</mixed-citation>
         </ref>
         <ref id="d1055e822a1310">
            <mixed-citation id="d1055e826" publication-type="other">
Hotez, P. J., Molyneux, D. H., Fenwick, A., Kumaresan, J.,
Sachs, S. E., Sach, J. D. &amp; Savioli, L. 2007 Control of
neglected tropical diseases. N. Engl. J. Med. 357, 1018-
1027. (doi:10.1056/NEJMra064142)</mixed-citation>
         </ref>
         <ref id="d1055e842a1310">
            <mixed-citation id="d1055e846" publication-type="other">
House, J. A. &amp; Mariner, J. C. 1996 Stabilization of rinder-
pest vaccine by modification of the lyophilization process.
Dev. Biol. Stand 87, 235-244.</mixed-citation>
         </ref>
         <ref id="d1055e860a1310">
            <mixed-citation id="d1055e864" publication-type="other">
Jenkins, M. C. 2001 Advances and prospects for subunit
vaccines against protozoa of veterinary importance. Vet. Para-
sitol 22, 291-310. (doi:10.1016/S0304-4017(01)00557-X)</mixed-citation>
         </ref>
         <ref id="d1055e877a1310">
            <mixed-citation id="d1055e881" publication-type="other">
Joshi, A. B., Banjara, M. R., Pokhrel, S., Jimba, M.,
Singhasivanon, P. &amp; Ashford, R. W. 2006 Elimination of
visceral leishmaniasis in Nepal: pipe-dreams and
possibilities. Kathmandu Univ. Med. J. 4, 488-496.</mixed-citation>
         </ref>
         <ref id="d1055e897a1310">
            <mixed-citation id="d1055e901" publication-type="other">
Joshi, A., Narain, J. P., Prasittisuk, C., Bhatia, R., Hashim,
G., Jorge, A., Banjara, M. &amp; Kroeger, A. 2008 Can visc-
eral leishmaniasis be eliminated from Asia? J. Vector Borne
Dis. 45, 105-111.</mixed-citation>
         </ref>
         <ref id="d1055e917a1310">
            <mixed-citation id="d1055e921" publication-type="other">
Kedzierski, L., Zhu, Y. &amp; Handman, E. 2006 Leishmania
vaccines: progress and problems. Parasitology 133, S87-
S112. (doi:10.1017/S0031182006001831)</mixed-citation>
         </ref>
         <ref id="d1055e934a1310">
            <mixed-citation id="d1055e938" publication-type="other">
Kgori, P. M., Modo, S. &amp; Torr, S. J. 2006 The use of aerial
spraying to eliminate tsetse from the Okavango Delta of
Botswana. Acta Trop. 99, 184-199. (doi:10.1016/
j.actatropica.2006.07.007)</mixed-citation>
         </ref>
         <ref id="d1055e954a1310">
            <mixed-citation id="d1055e958" publication-type="other">
King, C. H. &amp; Bertino, A. M. 2008 Asymmetries of poverty:
why global burden of disease valuations underestimate
the burden of neglected tropical diseases. PLoS Negl.
Trop. Dis. 2, e209. (doi:10.1371/journal.pntd.0000209)</mixed-citation>
         </ref>
         <ref id="d1055e975a1310">
            <mixed-citation id="d1055e979" publication-type="other">
Knobel, D. L., Cleaveland, S., Coleman, P. G., Fèvre, E. M.,
Meltzer, M. I., Miranda, M. E., Shaw, A., Zinsstag, J. &amp;
Meslin, F. X. 2005 Re-evaluating the burden of rabies
in Africa and Asia. Bull. World Health Organ. 83,
360-368.</mixed-citation>
         </ref>
         <ref id="d1055e998a1310">
            <mixed-citation id="d1055e1002" publication-type="other">
Lammie, P. J., Fenwick, A. &amp; Utzinger, J. 2006 A blueprint
for success: integration of neglected tropical disease
control programmes. Trends Parasitol. 22, 313-321.
(doi:10.1016/j.pt.2006.05.009)</mixed-citation>
         </ref>
         <ref id="d1055e1018a1310">
            <mixed-citation id="d1055e1022" publication-type="other">
Lightowlers, M. W et al 2003 Vaccination against cestode
parasites: anti-helminth vaccines that work and why. Vet.
Parasitol. 115, 83-123. (doi:10.1016/S0304-4017(03)
00202-4)</mixed-citation>
         </ref>
         <ref id="d1055e1038a1310">
            <mixed-citation id="d1055e1042" publication-type="other">
Lukes, J. et al 2007 Evolutionary and geographical history of
the Leishmania donovani complex with a revision of
current taxonomy. Proc. Natl Acad. Sci. USA 104,
9375-9380. (doi:10.1073/pnas.0703678104)</mixed-citation>
         </ref>
         <ref id="d1055e1058a1310">
            <mixed-citation id="d1055e1062" publication-type="other">
Mafojane, N. A., Appleton, C. C, Krecek, R. C, Michael,
L. M. &amp; Willingham, A. L. 2003 The current status of
neurocysticercosis in Eastern and Southern Africa. Acta
Trop. 87, 25-33.</mixed-citation>
         </ref>
         <ref id="d1055e1078a1310">
            <mixed-citation id="d1055e1082" publication-type="other">
Maguire, J. H. 2006 Chagas' disease—Can we stop the
deaths? N Engl J. Med. 355, 760-761. (doi:10.1056/
NEJMp068130)</mixed-citation>
         </ref>
         <ref id="d1055e1096a1310">
            <mixed-citation id="d1055e1100" publication-type="other">
Makita, K., Fèvre, E. M., Waiswa, C, Kaboyo, W, De Clare
Bronsvoort, B. M., Eisler, M. C. &amp; Welburn, S. C. 2008
Human brucellosis in urban and peri-urban areas of
Kampala, Uganda. Ann. NY Acad. Sci. 1149, 309-311.</mixed-citation>
         </ref>
         <ref id="d1055e1116a1310">
            <mixed-citation id="d1055e1120" publication-type="other">
Mathers, C. D., Ezzati, M. &amp; Lopez, A. D. 2007 Measuring
the burden of neglected tropical diseases: the global
burden of disease framework. PLoS Negl Trop. Dis. 1,
e114. (doi:10.1371/journal.pntd.0000114)</mixed-citation>
         </ref>
         <ref id="d1055e1136a1310">
            <mixed-citation id="d1055e1140" publication-type="other">
Maudlin, I. 2006 African trypanosomiasis. Ann. Trop. Med.
Parasitol 100, 679-701. (doi:10.1179/136485906X
112211)</mixed-citation>
         </ref>
         <ref id="d1055e1153a1310">
            <mixed-citation id="d1055e1157" publication-type="other">
May, R. M. 2007 Parasites, people and policy: infectious dis-
eases and the millennium development goals. Trends Ecol.
Evol 22, 497-503. (doi:10.1016/j.tree.2007.08.009)</mixed-citation>
         </ref>
         <ref id="d1055e1170a1310">
            <mixed-citation id="d1055e1174" publication-type="other">
McDermott, J. J. &amp; Arimib, S. M. 2002 Brucellosis in sub-
Saharan Africa: epidemiology, control and impact. Vet.
Microbiol. 90, 111-134. (doi:10.1016/S0378-1135(02)
00249-3)</mixed-citation>
         </ref>
         <ref id="d1055e1190a1310">
            <mixed-citation id="d1055e1194" publication-type="other">
Meltzer, M. I. &amp; Rupprecht, C. E. 1998 A review of the
economics of the prevention and control of rabies. Part
2: Rabies in dogs, livestock and wildlife. Pharmacoeco-
nomics 14, 481-498. (doi:10.2165/00019053-1998140
50-00003)</mixed-citation>
         </ref>
         <ref id="d1055e1214a1310">
            <mixed-citation id="d1055e1218" publication-type="other">
Molyneux, D. H. 2008 Combating the 'other diseases' of
MDG 6: changing the paradigm to achieve equity and
poverty reduction? Trans. R. Soc. Trop. Med. Hyg. 102,
509-519. (doi:10.1016/j.trstmh.2008.02.024)</mixed-citation>
         </ref>
         <ref id="d1055e1234a1310">
            <mixed-citation id="d1055e1238" publication-type="other">
Molyneux, D. H., Hopkins, D. R. &amp; Zagaria, N. 2004
Disease eradication, elimination and control: the need
for accurate and consistent usage. Trends Parasitol 20,
347-351. (doi:10.1016/j.pt.2004.06.004)</mixed-citation>
         </ref>
         <ref id="d1055e1254a1310">
            <mixed-citation id="d1055e1258" publication-type="other">
Molyneux, D. H., Hotez, P. J. &amp; Fenwick, A. 2005 'Rapid-
impact interventions': how a policy of integrated control
for Africa's neglected tropical diseases could benefit the
poor. PLoS Med. 2, e336. (doi:10.1371/journal.pmed.
0020336)</mixed-citation>
         </ref>
         <ref id="d1055e1277a1310">
            <mixed-citation id="d1055e1283" publication-type="other">
Moreira, E. D., Souza, V. M. M., Sreenivasan, M.,
Nascimento, E. G. &amp; Pontes-de-Carvalho, L. 2004
Assessment of an optimized dog-culling program in the
dynamics of canine Leishmania transmission. Vet. Parasito!.
122, 245-252. (doi:10.1016/j.vetpar.2004.05.019)</mixed-citation>
         </ref>
         <ref id="d1055e1302a1310">
            <mixed-citation id="d1055e1306" publication-type="other">
Noazin, S. et al 2008 First generation leishmaniasis
vaccines: a review of field efficacy trials. Vaccine 26,
6759-6767. (doi:10.1016/j.vaccine.2008.09.085)</mixed-citation>
         </ref>
         <ref id="d1055e1319a1310">
            <mixed-citation id="d1055e1323" publication-type="other">
Nunes, C. M., Lima, V. M., Paula, H. B., Perri, S. H.,
Andrade, A. M., Dias, F. E. &amp; Burattini, M. N. 2008
Dog culling and replacement in an area endemic for
visceral leishmaniasis in Brazil. Vet. Parasitai. 153,
19-23. (doi:10.1016/j.vetpar.2008.01.005)</mixed-citation>
         </ref>
         <ref id="d1055e1343a1310">
            <mixed-citation id="d1055e1347" publication-type="other">
Odiit, M., Coleman, P. G., Liu, W. C, McDermott, J. J.,
Fèvre, E. M., Welburn, S. C. &amp; Woolhouse, M. E. 2005
Quantifying the level of under-detection of Trypanosoma
brucei rhodesiense sleeping sickness cases. Trop. Med. Int.
Health 10, 840-849. (doi:10.1111/j.1365-3156.2005.
01470.x)</mixed-citation>
         </ref>
         <ref id="d1055e1370a1310">
            <mixed-citation id="d1055e1374" publication-type="other">
Ollila, E. 2005 Global health priorities—priorities of the
wealthy? Globalization and Health 1, 6. (doi:10.1186/
1744-8603-1-6)</mixed-citation>
         </ref>
         <ref id="d1055e1387a1310">
            <mixed-citation id="d1055e1391" publication-type="other">
Pawlowski, Z., Allan, J. &amp; Sarti, E. 2005 Control of Taenia
solium taeniasis/cysticercosis: from research towards
implementation. Int. J. Parasitol. 35, 1221-1232.
(doi:10.1016/j.ijpara.2005.07.015)</mixed-citation>
         </ref>
         <ref id="d1055e1407a1310">
            <mixed-citation id="d1055e1411" publication-type="other">
Phiri, I. K. et al. 2003 The emergence of Taenia solium
cysticercosis in Eastern and Southern Africa as a serious
agricultural problem and public health risk. Acta Trop.
87, 13-23.</mixed-citation>
         </ref>
         <ref id="d1055e1427a1310">
            <mixed-citation id="d1055e1431" publication-type="other">
Picozzi, K., Fèvre, E. M., Odiit, M., Carrington, M., Eisler,
M. C, Maudlin, I. &amp; Welburn, S. C. 2005 Sleeping sick-
ness in Uganda: a thin line between two fatal diseases.
Brit Med. J. 331, 1238-1241. (doi:10.1136/bmj.331.
7527.1238)</mixed-citation>
         </ref>
         <ref id="d1055e1450a1310">
            <mixed-citation id="d1055e1454" publication-type="other">
Reithinger, R. 2008 Leishmaniases' burden of disease:
ways forward for getting from speculation to reality.
PLoS Negl. Trop. Dis. 2, e285. (doi:10.1371/journal.
pntd.0000285)</mixed-citation>
         </ref>
         <ref id="d1055e1471a1310">
            <mixed-citation id="d1055e1475" publication-type="other">
Reithinger, R., Coleman, P. G., Alexander, B., Vieira, E. P.,
Assis, G. &amp; Davies, C. R. 2004 Are insecticide-
impregnated dog collars a feasible alternative to dog
culling as a strategy for controlling canine visceral
leishmaniasis in Brazil? Int. J. Parasitol. 34, 55-62.
(doi:10.1016/j.ijpara.2003.09.006)</mixed-citation>
         </ref>
         <ref id="d1055e1498a1310">
            <mixed-citation id="d1055e1502" publication-type="other">
Sachs, J. 2005 The end of poverty: economic possibilities for our
time. New York, NY: Penguin Books.</mixed-citation>
         </ref>
         <ref id="d1055e1512a1310">
            <mixed-citation id="d1055e1516" publication-type="other">
Sarti, E. et al 1997 Development and evaluation of
a health education intervention against Taenia solium in
a rural community in Mexico. Am. J. Trop. Med. Hyg.
56, 127-132.</mixed-citation>
         </ref>
         <ref id="d1055e1532a1310">
            <mixed-citation id="d1055e1536" publication-type="other">
Shiffman, J. 2006 Donor funding priorities for communic-
able disease control in the developing world. Health
Policy Plann. 21, 411-420. (doi:10.1093/heapol/czl028)</mixed-citation>
         </ref>
         <ref id="d1055e1549a1310">
            <mixed-citation id="d1055e1553" publication-type="other">
Singer, B. H. &amp; Ryff, C. D. 2007 Neglected tropical diseases,
neglected data sources, and neglected issues. PLoS Negl.
Trop. Dis. 1, e104. (doi:10.1371/journal.pntd.0000104)</mixed-citation>
         </ref>
         <ref id="d1055e1566a1310">
            <mixed-citation id="d1055e1570" publication-type="other">
Singh, G., Prabhakar, S., Ito, A. &amp; Cho, S. Y. 2002 Taenia
solium taeniasis and cysticercosis in Asia. In Taenia
solium cysticercosis: from basic to clinical science, (eds G.
Singh &amp; S. Prabhakar), pp. 111-128. Cambridge, UK:
CABI.</mixed-citation>
         </ref>
         <ref id="d1055e1590a1310">
            <mixed-citation id="d1055e1594" publication-type="other">
Smith, G. C, Thulke, H. H., Fooks, A. R., Artois, M.,
Macdonald, D. W., Eisinger, D. &amp; Seihorst, T. 2008
What is the future of wildlife rabies control in Europe?
Dev. Biol. 131, 283-289.</mixed-citation>
         </ref>
         <ref id="d1055e1610a1310">
            <mixed-citation id="d1055e1614" publication-type="other">
Stuart, K., Brun, R., Croft, S., Fairlamb, A., Gürtler, R. E.,
McKerrow, J., Reed, S. &amp; Tarleton, R. 2008 Kinetoplas-
tids: related protozoan pathogens, different diseases.
J. Clin. Invest. 118, 1301-1310. (doi:10.1172/JCI33945)</mixed-citation>
         </ref>
         <ref id="d1055e1630a1310">
            <mixed-citation id="d1055e1634" publication-type="other">
Tarleton, R. L., Reithinger, R., Urbina, J. A., Kitron, U. &amp;
Gürtler, R. E. 2007 The challenges of Chagas disease-
grim outlook or glimmer of hope? PLoS Med. 4, e332.
(doi:10.1371/journal.pmed.0040332)</mixed-citation>
         </ref>
         <ref id="d1055e1650a1310">
            <mixed-citation id="d1055e1654" publication-type="other">
Torr, S. J., Maudlin, I. &amp; Vale, G. A. 2007 Less is more:
restricted application of insecticide to cattle to improve
the cost and efficacy of tsetse control. Med. Vet. Entomol
21, 53-64. (doi:10.1111/j.l365-2915.2006.00657.x)</mixed-citation>
         </ref>
         <ref id="d1055e1670a1310">
            <mixed-citation id="d1055e1674" publication-type="other">
Vreysen, M. J. et al. 2000 Glossina austeni (Diptera: Glossini-
dae) eradicated on the island of Unguja, Zanzibar, using
the sterile insect technique.J. Econ. Entomol. 93, 123-135.</mixed-citation>
         </ref>
         <ref id="d1055e1687a1310">
            <mixed-citation id="d1055e1691" publication-type="other">
Weiss, R. A. &amp; McLean, A. R. 2004 What have we learnt
from SARS? Phil Trans. R. Soc. Lond. B 359, 1137-
1140. (doi:10.1098/rstb.2004.1487)</mixed-citation>
         </ref>
         <ref id="d1055e1705a1310">
            <mixed-citation id="d1055e1709" publication-type="other">
Welburn, S. C, Picozzi, K., Fèvre, E. M., Coleman, P. G.,
Odiit, M., Carrington, M. &amp; Maudlin, I. 2001 Identifi-
cation of human-infective trypanosomes in animal reservoir
of sleeping sickness in Uganda by means of serum-
resistance-associated (SRA) gene. Lancet 358, 2017-2019.
(doi:10.1016/SO140-6736(01)07096-9)</mixed-citation>
         </ref>
         <ref id="d1055e1732a1310">
            <mixed-citation id="d1055e1736" publication-type="other">
Welburn, S. C, Coleman, P. G., Maudlin, I., Fèvre, E. M.,
Odiit, M. &amp; Eisler, M. C. 2006 Crisis, what crisis? Con-
trol of Rhodesian sleeping sickness. Trends Parasitol. 22,
123-128. (doi:10.1016/j.pt.2006.01.011)</mixed-citation>
         </ref>
         <ref id="d1055e1752a1310">
            <mixed-citation id="d1055e1756" publication-type="other">
WHO 1998 World survey of Rabies No. 32 for the year 1996.
Document EMC/ZDI/98.4. Geneva, Switzerland: WHO.</mixed-citation>
         </ref>
         <ref id="d1055e1766a1310">
            <mixed-citation id="d1055e1770" publication-type="other">
WHO 2003 Control of neurocysticercosis. Fifty-sixth World
Health Assembly. A56/10. Geneva, Switzerland: WHO.</mixed-citation>
         </ref>
         <ref id="d1055e1780a1310">
            <mixed-citation id="d1055e1784" publication-type="other">
WHO 2004 In The world health report 2004. Changing history.
Geneva, Switzerland: WHO.</mixed-citation>
         </ref>
         <ref id="d1055e1794a1310">
            <mixed-citation id="d1055e1798" publication-type="other">
WHO 2006 The control of neglected zoonotic diseases: a
route to poverty alleviation. Report of a joint WHO/
DFID-AHP Meeting, 20-21 September 2005, WHO
Headquarters Geneva, with the participation of FAO
and OIE, p. 62. Geneva, Switzerland: WHO.</mixed-citation>
         </ref>
         <ref id="d1055e1818a1310">
            <mixed-citation id="d1055e1822" publication-type="other">
Woolhouse, M. E. &amp; Gowtage-Sequeria, S. 2005 Host range
and emerging and re-emerging pathogens. Emerg. Infect.
Dis. 11, 1842-1847.</mixed-citation>
         </ref>
         <ref id="d1055e1835a1310">
            <mixed-citation id="d1055e1839" publication-type="other">
Zinsstag, J., Schelling, E., Wyss, K. &amp; Mahamat, M. B. 2005
Potential of cooperation between human and animal
health to strengthen health systems. Lancet 366, 2142-
2145. (doi:10.1016/S0140-6736(05)67731-8)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
