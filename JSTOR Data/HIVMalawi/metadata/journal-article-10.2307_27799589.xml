<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27799589</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Elevations in Mortality Associated with Weaning Persist into the Second Year of Life among Uninfected Children Born to HIV-Infected Mothers</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Louise</given-names>
                  <surname>Kuhn</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Moses</given-names>
                  <surname>Sinkala</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Katherine</given-names>
                  <surname>Semrau</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Chipepo</given-names>
                  <surname>Kankasa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Prisca</given-names>
                  <surname>Kasonde</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mwiya</given-names>
                  <surname>Mwiya</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Chih-Chi</given-names>
                  <surname>Hu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Wei-Yann</given-names>
                  <surname>Tsai</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Donald M.</given-names>
                  <surname>Thea</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Grace M.</given-names>
                  <surname>Aldrovand</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">50</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27799566</issue-id>
         <fpage>437</fpage>
         <lpage>444</lpage>
         <permissions>
            <copyright-statement>© 2009 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27799589"/>
         <abstract>
            <p>Background. Early weaning has been recommended to reduce postnatal human immunodeficiency virus (HIV) transmission. We evaluated the safety of stopping breast-feeding at different ages for mortality of uninfected children born to HIV-infected mothers. Methods. During a trial of early weaning, 958 HIV-infected mothers and their infants were recruited and followed up from birth to 24 months postpartum in Lusaka, Zambia. One-half of the cohort was randomized to wean abruptly at 4 months, and the other half of the cohort was randomized to continue breast-feeding. We examined associations between uninfected child mortality and actual breast-feeding duration and investigated possible confounding and effect modification. Results. The mortality rate among 749 uninfected children was 9.4% by 12 months of age and 13.6% by 24 months of age. Weaning during the interval encouraged by the protocol (4–5 months of age) was associated with a 2.03-fold increased risk of mortality (95% confidence interval [CI], 1.13–3.65), weaning at 6–11 months of age was associated with a 3.54-fold increase (95% CI, 1.68–7.46), and weaning at 12–18 months of age was associated with a 4.22-fold increase (95% CI, 1.59–11.24). Significant effect modification was detected, such that risks associated with weaning were stronger among infants born to mothers with higher CD4 + cell counts (&gt;350 cells/μL). Conclusion. Shortening the normal duration of breast-feeding for uninfected children born to HIV-infected mothers living in low-resource settings is associated with significant increases in mortality extending into the second year of life. Intensive nutritional and counseling interventions reduce but do not eliminate this excess mortality.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1145e295a1310">
            <label>1</label>
            <mixed-citation id="d1145e302" publication-type="other">
Ekpini ER, Wiktor SZ, Satten GA, et al. Late postnatal mother-to-child
transmission of HIV-1 in Abidjan, Cote d'Ivoire. Lancet 1997; 349:
1054–1059.</mixed-citation>
         </ref>
         <ref id="d1145e315a1310">
            <label>2</label>
            <mixed-citation id="d1145e322" publication-type="other">
Miotti PG, Taha TE, Kumwenda NI, et al. HIV transmission through
breast feeding: a study in Malawi. JAMA 1999; 282:744–749.</mixed-citation>
         </ref>
         <ref id="d1145e332a1310">
            <label>3</label>
            <mixed-citation id="d1145e339" publication-type="other">
Fawzi W, Msamanga G, Spiegelman D, et al. Transmission of HIV-1
through breastfeeding among women in Dar es Salaam, Tanzania.
JAIDS 2002; 31:331–338.</mixed-citation>
         </ref>
         <ref id="d1145e352a1310">
            <label>4</label>
            <mixed-citation id="d1145e359" publication-type="other">
Breastfeeding and HIV International Transmission Study Group. Late
postnatal transmission of HIV-1 in breast-fed children: an individual
patient data meta-analysis. J Infect Dis 2004; 189:2154-2166.</mixed-citation>
         </ref>
         <ref id="d1145e373a1310">
            <label>5</label>
            <mixed-citation id="d1145e380" publication-type="other">
Taha TE, Kumwenda NI, Hoover DR, et al. The impact of breastfeeding
on the health of HIV-positive mothers and their children in sub-Sa-
haran Africa. Bull WHO 2006; 84:546–554.</mixed-citation>
         </ref>
         <ref id="d1145e393a1310">
            <label>6</label>
            <mixed-citation id="d1145e400" publication-type="other">
Bahl R, Frost C, Kirkwood BR, et al. Infant feeding patterns and risks
of death and hospitalization in the first half of infancy: multicentre
cohort study. Bull WHO 2005; 83:418–426.</mixed-citation>
         </ref>
         <ref id="d1145e413a1310">
            <label>7</label>
            <mixed-citation id="d1145e420" publication-type="other">
WHO Collaborative Study Team on the Role of Breastfeeding on the
Prevention of Infant Mortality. Effect of breastfeeding on infant and
child mortality due to infectious diseases in less developed countries:
a pooled analysis. Lancet 2000; 355:451–455.</mixed-citation>
         </ref>
         <ref id="d1145e436a1310">
            <label>8</label>
            <mixed-citation id="d1145e443" publication-type="other">
Kuhn L, Aldrovandi GM, Sinkala M, et al. Effects of early, abrupt
cessation of breastfeeding on HIV-free survival of children in Zambia.
Engl J Med 2008; 359:130–141.</mixed-citation>
         </ref>
         <ref id="d1145e456a1310">
            <label>9</label>
            <mixed-citation id="d1145e463" publication-type="other">
Kuhn L, Sinkala M, Kankasa C, et al. High uptake of exclusive breast-
feeding and reduced early post-natal HIV transmission. PLOS ONE
2007; 2(12).:-el363. doi:10.1371/journal.pone.0001363.</mixed-citation>
         </ref>
         <ref id="d1145e476a1310">
            <label>10</label>
            <mixed-citation id="d1145e483" publication-type="other">
Stringer JS, Zulu I, Levy J, et al. Rapid scale-up of antiretroviral therapy
at primary care sites in Zambia: feasibility and early outcomes. JAMA
2006; 296:782–793.</mixed-citation>
         </ref>
         <ref id="d1145e497a1310">
            <label>11</label>
            <mixed-citation id="d1145e504" publication-type="other">
Walter J, Mwiya M, Scott N, et al. Reduction in preterm delivery and
neonatal mortality after the introduction of antenatal cotrimoxazole
prophylaxis among HIV-infected women with low CD4 cell counts. J
Infect Dis 2006; 194:1510–1518.</mixed-citation>
         </ref>
         <ref id="d1145e520a1310">
            <label>12</label>
            <mixed-citation id="d1145e527" publication-type="other">
Lawrence R, Lawrence R. Breastfeeding: a guide for the medical pro-
fessional. St Louis, Missouri: Mosby, 1999.</mixed-citation>
         </ref>
         <ref id="d1145e537a1310">
            <label>13</label>
            <mixed-citation id="d1145e544" publication-type="other">
Kagaayi J, Gray RH, Brahmbhatt H, et al. Survival of infants born to
HIV-positive mothers by feeding modality in Rakai, Uganda. PLOS
ONE 2008; 3:e3877-doi:10.1371/journal.pone.0003877.</mixed-citation>
         </ref>
         <ref id="d1145e557a1310">
            <label>14</label>
            <mixed-citation id="d1145e564" publication-type="other">
Quigley MA, Kelly YJ, Sacker A. Breastfeeding and hospitalization for
diarrheal and respiratory infection in the United Kingdom Millennium
Cohort Study. Pediatrics 2007; 119:e837-e842.</mixed-citation>
         </ref>
         <ref id="d1145e577a1310">
            <label>15</label>
            <mixed-citation id="d1145e584" publication-type="other">
Goldman AS. The immune system of human milk: antimicrobial, an-
tiinflammatory and immunomodulating properties. Pediatr Infect Dis
J 1993; 12:664–671.</mixed-citation>
         </ref>
         <ref id="d1145e597a1310">
            <label>16</label>
            <mixed-citation id="d1145e604" publication-type="other">
Labbok MH, Clark D, Goldman AS. Breastfeeding: maintaining an ir-
replaceable immunological resource. Nat Rev Immunol 2004; 4:565–572.</mixed-citation>
         </ref>
         <ref id="d1145e615a1310">
            <label>17</label>
            <mixed-citation id="d1145e622" publication-type="other">
Shapiro RL, Lockman S, Kim S, et al. Infant morbidity, mortality, and
breast milk immunologie profiles among breast-feeding HIV-infected
and HIV-uninfected women in Botswana. J Infect Dis 2007; 196:562–565.</mixed-citation>
         </ref>
         <ref id="d1145e635a1310">
            <label>18</label>
            <mixed-citation id="d1145e642" publication-type="other">
World Health Organization. Planning guide for national implemen-
tation of the global strategy for infant and young child feeding, http:
//www.whoint/nutrition/publications/Planning_guide.pdf. 2006. Ac-
cessed 22 December 2009.</mixed-citation>
         </ref>
         <ref id="d1145e658a1310">
            <label>19</label>
            <mixed-citation id="d1145e665" publication-type="other">
Kuhn L, Aldrovandi GM, Sinkala M, et al. Differential effects of early
weaning for HIV-free survival of children born to HIV-infected moth-
ers by severity of maternal disease. PLOS ONE 2009; 4:e6059-doi:
10.1371/journal.pone.0006059.</mixed-citation>
         </ref>
         <ref id="d1145e681a1310">
            <label>20</label>
            <mixed-citation id="d1145e688" publication-type="other">
Six Week Extended-Dose Nevirapine (SWEN) Study Team. Extended-
dose nevirapine to 6 weeks of age for infants to prevent HIV trans-
mission via breastfeeding in Ethiopia, India, and Uganda: an analysis
of three randomised controlled trials. Lancet 2008;372:300–313.</mixed-citation>
         </ref>
         <ref id="d1145e704a1310">
            <label>21</label>
            <mixed-citation id="d1145e711" publication-type="other">
Kumwenda NI, Hoover DR, Mofenson LM, et al. Extended antiret-
roviral prophylaxis to reduce breast-milk HIV-1 transmission. Engl
J Med 2008;359:119–129.</mixed-citation>
         </ref>
         <ref id="d1145e724a1310">
            <label>22</label>
            <mixed-citation id="d1145e731" publication-type="other">
Tonwe-Gold , Ekouevi DK, Viho I, et al. Antiretroviral treatment and
prevention of peripartum and postnatal HIV transmission in West
Africa: evaluation of a two-tiered approach. PLOS Medicine 2007; 4:
e257.</mixed-citation>
         </ref>
         <ref id="d1145e748a1310">
            <label>23</label>
            <mixed-citation id="d1145e755" publication-type="other">
Palombi L, Marazzi MC, Voetberg A, Magid MA. Treatment acceler-
ation program and the experience of the DREAM program in preven-
tion of mother-to-child transmission of HIV. AIDS 2007; 21 (Suppl 4):
S65–S71.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
