<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt3fhwkz</book-id>
      <subj-group>
         <subject content-type="call-number">DS740.5.A34S55 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">China</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
               <subj-group>
                  <subject content-type="lcsh">Africa</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
               <subj-group>
                  <subject content-type="lcsh">China</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">China</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
               <subj-group>
                  <subject content-type="lcsh">Africa</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
               <subj-group>
                  <subject content-type="lcsh">China</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>China and Africa</book-title>
         <subtitle>A Century of Engagement</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>SHINN</surname>
               <given-names>DAVID H.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>EISENMAN</surname>
               <given-names>JOSHUA</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>10</day>
         <month>07</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780812244199</isbn>
      <isbn content-type="epub">9780812208009</isbn>
      <publisher>
         <publisher-name>University of Pennsylvania Press, Inc.</publisher-name>
         <publisher-loc>PHILADELPHIA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>University of Pennsylvania Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt3fhwkz"/>
      <abstract abstract-type="short">
         <p>The People's Republic of China once limited its involvement in African affairs to building an occasional railroad or port, supporting African liberation movements, and loudly proclaiming socialist solidarity with the downtrodden of the continent. Now Chinese diplomats and Chinese companies, both state-owned and private, along with an influx of Chinese workers, have spread throughout Africa. This shift is one of the most important geopolitical phenomena of our time.<italic>China and Africa: A Century of Engagement</italic>presents a comprehensive view of the relationship between this powerful Asian nation and the countries of Africa. This book, the first of its kind to be published since the 1970s, examines all facets of China's relationship with each of the fifty-four African nations. It reviews the history of China's relations with the continent, looking back past the establishment of the People's Republic of China in 1949. It looks at a broad range of areas that define this relationship-politics, trade, investment, foreign aid, military, security, and culture-providing a significant historical backdrop for each. David H. Shinn and Joshua Eisenman's study combines careful observation, meticulous data analysis, and detailed understanding gained through diplomatic experience and extensive travel in China and Africa.<italic>China and Africa</italic>demonstrates that while China's connection to Africa is different from that of Western nations, it is no less complex. Africans and Chinese are still developing their perceptions of each other, and these changing views have both positive and negative dimensions.</p>
      </abstract>
      <counts>
         <page-count count="544"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.3</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Yu</surname>
                           <given-names>George T.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract abstract-type="extract">
                     <p>“Let China sleep, for when she awakes, she will shake the world,” was Napoleon’s response to those who asked about the quiescent China of the eighteenth and nineteenth centuries. Today, China has indeed awakened, and she is shaking the world. As Mao Zedong declared in October 1949, upon the founding of the People’s Republic of China, “China has stood up!”</p>
                     <p>One measurement of how “China has stood up” is its emergence as a global economic and political power, extending its presence to nearly all corners of the world. As the second-largest global economy and the world’s factory floor, Chinese merchandise</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.4</book-part-id>
                  <title-group>
                     <title>[Map]</title>
                  </title-group>
                  <fpage>xvi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Political leaders in Beijing regularly refer to China as the world’s “largest developing country” and Africa as “the continent with the most developing countries.” Yet China is hardly a typical developing country and Africa is hardly a cohesive political entity. China’s growing economic and political clout over the last decade is a fact, but its remarkable rise remains incomplete and the global implications uncertain. Meanwhile, rising commodity prices have given African elites newfound wealth and in many countries “stability” has become the motto of autocrats. Although China’s influence on global affairs is growing, its actions to safeguard its interests are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A Historical Overview of China-Africa Relations</title>
                  </title-group>
                  <fpage>17</fpage>
                  <abstract abstract-type="extract">
                     <p>Trade was the first link between Africa and China. Chinese scholar Gao Jinyuan noted that Queen Cleopatra of Egypt, who reigned between 51 and 30 BCE, reportedly wore silks that likely came from China. In about 166 CE the Han emperor received gifts, some of which originated in northeast Africa, from the Roman emperor, who ruled Egypt at the time.¹ Former Chinese ambassador to Kenya, An Yongu, stated that Chinese goods dating to the Han Dynasty (206 BCE–220 CE) have been found in Africa.² British scholar Basil Davidson agreed that Chinese products reached the Red Sea, Mediterranean, and even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Political Relations</title>
                  </title-group>
                  <fpage>56</fpage>
                  <abstract abstract-type="extract">
                     <p>Political ties are, of course, inextricably linked to the China-Africa government-to-government relationships described in Chapter 2. This chapter, however, traces the development of party-to-party relations, which in the early years of the PRC, were dominated by numerous united front, solidarity, and friendship organizations.</p>
                     <p>Party-to-party contacts were established during the 1950s and 1960s, but conducted through less formal subordinate CPC-led groups known as “mass organizations” (<italic>qunzhong zuzhi</italic>), “united front groups” (<italic>tongzhan tuanti</italic>), and “people’s organizations” (<italic>mingjian tuanti</italic>). Before the establishment of the CPC’s International Department (CPC-ID) in 1977, the international liaison offices of these organizations were charged with maintenance of CPC</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Trade Relations</title>
                  </title-group>
                  <fpage>99</fpage>
                  <abstract abstract-type="extract">
                     <p>Driven by Chinese resource purchases and African demand for affordable consumer products, trade is now the largest feature of the China-Africa economic relationship. China-Africa trade deals concluded before China’s economic reform and opening up in the late 1970s were politically expedient, but rarely amounted to much trade. Although trade with Africa was briefly, and marginally, important to China at the height of its international isolation during the active period of the Cultural Revolution, 1967–1971, it did not become a sizable portion of Africa’s total world trade until the second half of the decade 2005–2010.</p>
                     <p>Together, China’s increasing demand</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Investment and Assistance</title>
                  </title-group>
                  <fpage>128</fpage>
                  <abstract abstract-type="extract">
                     <p>Chinese overseas investment is a relatively new phenomenon. Beginning in 1979 and continuing until 1985, only state-owned corporations and provincial and municipal international economic and technological cooperation enterprises could invest outside China. Beijing had a strict approval system for outward investment projects. China liberalized the system between 1986 and 1991, allowing more enterprises to apply for permission to establish subsidiaries in other countries. From 1992 to 1998, there was a major expansion in overseas investment by local and provincial enterprises. In the early 1990s, China identified Africa as a key market for development and began establishing trade and investment centers</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Military and Security Ties and Peacekeeping Missions</title>
                  </title-group>
                  <fpage>162</fpage>
                  <abstract abstract-type="extract">
                     <p>China’s security relationship with select African countries dates back to the 1950s and now extends in some fashion to all fifty African countries that recognize Beijing. It began with Chinese support for African independence movements and several revolutionary groups that opposed conservative African governments. This was part of Mao Zedong and Vice Premier and Defense Minister Lin Biao’s doctrine of revolutionary warfare and promoting wars of national liberation around the world.¹ As the African countries gained independence and China’s policy became more pragmatic and less ideological, it shifted to arms transfers, contracts for military construction, increased training, and high level</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Media, Education, and Cultural Relations and Ties with Chinese Communities in Africa</title>
                  </title-group>
                  <fpage>194</fpage>
                  <abstract abstract-type="extract">
                     <p>The state-controlled media are China’s most effective conduit for information collection and distribution in Africa. Xinhua News Agency (Xinhua), China Radio International (CRI), and China Central Television’s (CCTV) coverage of China-Africa relations have grown apace with China’s engagement on the continent. At the same time, China’s universities have become the choice for increasing numbers of elite African students. They, along with vocational training and Confucius Institutes in African countries, have spread knowledge about China and Chinese language to African youth. Since the 1950s, youth conferences, film festivals, and delegations of artists and writers have been part of Sino-African cultural exchanges.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>China’s Relations with North Africa and the Sahel</title>
                  </title-group>
                  <fpage>228</fpage>
                  <abstract abstract-type="extract">
                     <p>In the mid-1950s, the PRC began its African diplomatic offensive in North Africa for very practical reasons. It was the region of Africa that had the largest number of independent states; Beijing believed it could persuade several to recognize the PRC. China began with Egypt, and followed success there two years later in Morocco and with the provisional government in Algeria, which was in a war for independence with France. Although conservative Tunisia did not recognize Taiwan, it required more persuasion. The Kingdom of Libya established relations with Taiwan in 1959 and the socialist government that seized power in 1969</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>China’s Relations with East Africa, the Horn, and the Indian Ocean Islands</title>
                  </title-group>
                  <fpage>249</fpage>
                  <abstract abstract-type="extract">
                     <p>The nine countries of East Africa and the Horn have had diplomatic relations only with the PRC; they never recognized Taiwan. Sudan was the first to establish relations, albeit three years after independence. Somalia, Tanzania, Uganda, Kenya, and Eritrea recognized Beijing the year they became independent. Djibouti delayed for two years. Ethiopia, under imperial rule for about 2,000 years and never colonized, waited longest before establishing ties in 1970. South Sudan became independent in 2011 and immediately recognized Beijing. Of the nine, China initially had the closest and most intensive links with Tanzania. Somalia ties were also strong, especially as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>China’s Relations with West and Central Africa</title>
                  </title-group>
                  <fpage>284</fpage>
                  <abstract abstract-type="extract">
                     <p>West and Central Africa were the principal diplomatic battlegrounds between Beijing and Taipei. The different political leanings among governments in this region were strong. These countries also experienced a higher frequency of regime change than those in other African regions, which increased the possibility for them to switch recognition between the PRC and Taiwan. During the 1960s, when it considered opposition groups closer to its views, China interfered in the internal affairs of some independent African governments in the region. On several occasions, China armed and trained opposition groups, angering the affected governments.</p>
                     <p>Of the twenty-one countries in this region,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>China’s Relations with Southern Africa</title>
                  </title-group>
                  <fpage>323</fpage>
                  <abstract abstract-type="extract">
                     <p>Five countries (Zambia, Malawi, Botswana, Lesotho, and Swaziland) of the ten in southern Africa achieved independence peacefully in the 1960s. Four (Zimbabwe, Namibia, Mozambique, and Angola) underwent wars of national liberation while South Africa experienced considerable violence as it moved from white minority rule to majority black control. Mozambique and Angola became independent in 1975, Zimbabwe in 1980, Namibia in 1990; South Africa formally implemented majority rule in 1994. These experiences in achieving independence or majority rule had a significant impact on China’s relationship with each country.</p>
                     <p>Of the five countries that had a peaceful transition to independence, only Zambia</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Conclusion:</title>
                     <subtitle>Looking Forward</subtitle>
                  </title-group>
                  <fpage>362</fpage>
                  <abstract abstract-type="extract">
                     <p>In this volume we have examined China-Africa relations with a historical, topical, and geographical approach and through the eyes of the Africanist, Sinologist, and policymaker. China-Africa relations encompass a broad, multilayered set of fifty-four bilateral, political, economic, military, and social relationships, and we have investigated each of them. Bilateral relations require two interlocutors, yet China’s size and resources allow it to initiate most of its interaction with African countries. China tends to determine the level of interaction, the types of activities, and the terms of agreements. African states want more international respect and acceptance of their continent as an emerging</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.17</book-part-id>
                  <title-group>
                     <title>APPENDIX 1.</title>
                     <subtitle>ESTABLISHMENT OF PRC RELATIONS WITH AFRICAN COUNTRIES</subtitle>
                  </title-group>
                  <fpage>377</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.18</book-part-id>
                  <title-group>
                     <title>APPENDIX 2.</title>
                     <subtitle>TRADE BETWEEN AFRICA AND CHINA, 1938–2010</subtitle>
                  </title-group>
                  <fpage>381</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.19</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>383</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.20</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>499</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhwkz.21</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>525</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
