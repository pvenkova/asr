<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">humanbiology</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50003611</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Human Biology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wayne State University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00187143</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15346617</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23265987</article-id>
         <title-group>
            <article-title>Resource Availability, Mortality, and Fertility: A Path Analytic Approach to Global Life-History Variation</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>MARK A.</given-names>
                  <surname>CAUDELL</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>ROBERT J.</given-names>
                  <surname>QUINLAN</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">84</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23265984</issue-id>
         <fpage>101</fpage>
         <lpage>125</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 Wayne State University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23265987"/>
         <abstract>
            <p>Humans exhibit considerable diversity in timing and rate of reproduction. Life-history theory (LHT) suggests that ecological cues of resource richness and survival probabilities shape human phenotypes across populations. Populations experiencing high extrinsic mortality due to uncertainty in resources should exhibit faster life histories. Here we use a path analytic (PA) approach informed by LHT to model the multiple pathways between resources, mortality rates, and reproductive behavior in 191 countries. Resources that account for the most variance in population mortality rates are predicted to explain the most variance in total fertility rates. Results indicate that resources (e.g., calories, sanitation, education, and health-care expenditures) influence fertility rates in paths through communicable and noncommunicable diseases. Paths acting through communicable disease are more strongly associated with fertility than are paths through noncommunicable diseases. These results suggest that a PA approach may help disaggregate extrinsic and intrinsic mortality factors in cross-cultural analyses. Such knowledge may be useful in developing targeted policies to decrease teenage pregnancy, total fertility rates, and thus issues associated with overpopulation.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d435e272a1310">
            <mixed-citation id="d435e276" publication-type="other">
Anderson, K. G. 2010. Life expectancy and the timing of life history events in developing countries.
Human Nature. 21 (2): 103—123.</mixed-citation>
         </ref>
         <ref id="d435e286a1310">
            <mixed-citation id="d435e290" publication-type="other">
Borgerhoff Mulder, M. 1992. Reproductive decisions. In Evolutionary Ecology and Human Behavior,
E. A. Smith and B. Winterhalder, eds. New York: Aldine de Gruyter, 339—374.</mixed-citation>
         </ref>
         <ref id="d435e300a1310">
            <mixed-citation id="d435e304" publication-type="other">
Brown, T. A. 2006. Confirmatory Factor Analysis for Applied Research. New York: Guilford Press.</mixed-citation>
         </ref>
         <ref id="d435e311a1310">
            <mixed-citation id="d435e315" publication-type="other">
Bulled, N. L., and R. Sosis 2010. Examining the relationship between life expectancy, reproduction,
and educational attainment. Human Nature. 21(3):269-289.</mixed-citation>
         </ref>
         <ref id="d435e326a1310">
            <mixed-citation id="d435e330" publication-type="other">
Byrne, B. M. 2012. Structural Equation Modeling with Mplus. New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d435e337a1310">
            <mixed-citation id="d435e341" publication-type="other">
Chisholm, J. S. 1993. Death, hope, and sex: Life-history theory and the development of reproductive
strategies. Curr. Anthropol. 34(1): 1-24.</mixed-citation>
         </ref>
         <ref id="d435e351a1310">
            <mixed-citation id="d435e355" publication-type="other">
Chisholm, J.S.I 999. Death, Hope, and Sex: Steps to an Evolutionary Ecology of Mind and Morality.
New York: Cambridge</mixed-citation>
         </ref>
         <ref id="d435e365a1310">
            <mixed-citation id="d435e369" publication-type="other">
Chisholm, J. S., and D. A. Coall. 2008. Not by bread alone: The role of psychosocial stress in age
at first reproduction and health inequalities. In Evolutionary Medicine and Health, W.
Trevathan, E. O. Smith, and J. J. Mckenna, eds. New York: Oxford University Press,
134-148.</mixed-citation>
         </ref>
         <ref id="d435e385a1310">
            <mixed-citation id="d435e389" publication-type="other">
Chisholm, J. S., J. A. Quinlivan, R. W. Petersen, and D. A. Coall, 2005. Early stress predicts age at
menarche and first birth, adult attachment, and expected lifespan. Human Nature.
16(3):233-265.</mixed-citation>
         </ref>
         <ref id="d435e402a1310">
            <mixed-citation id="d435e406" publication-type="other">
Ellis, B. J., A. J. Figueredo, B. H. Brumbach, and G. L. Schlomer. 2009. Fundamental dimensions of
environmental risk. Human Nature. 20(2):204-268.</mixed-citation>
         </ref>
         <ref id="d435e417a1310">
            <mixed-citation id="d435e421" publication-type="other">
Gant, L„ K. M. Heath, G. G. Ejikeme, C. Snell, and K. Briar-Lawson. 2009. Early motherhood, high
mortality, and HIV/AIDS rates in Sub-Saharan Africa. Social Work in Public Health
24(l/2):39-46.</mixed-citation>
         </ref>
         <ref id="d435e434a1310">
            <mixed-citation id="d435e438" publication-type="other">
Harpending, H., P. Draper, and R. Pennington. 1990. Cultural evolution, parental care and
mortality. In Disease in Populations in Transition: Anthropolgoical and Epidemiological
Perspectives, A. C. Swedlund and G. J. Armelgos, eds., New York: Bergin &amp; Garvey,
251-265.</mixed-citation>
         </ref>
         <ref id="d435e454a1310">
            <mixed-citation id="d435e458" publication-type="other">
Hill, K., and H. Kaplan. 1999. Life history traits in humans: Theory and empirical studies. Annu. Rev.
Anthropol. 397-430.</mixed-citation>
         </ref>
         <ref id="d435e468a1310">
            <mixed-citation id="d435e472" publication-type="other">
Hu, L., and P. Bentler. 1999. Cutoff criteria for fit indexes in covariance structure analysis:
Conventional criteria versus new alternatives. Structural Equation Modeling: A Multidisci-
plinary J., 6( 1): 1 — 55.</mixed-citation>
         </ref>
         <ref id="d435e485a1310">
            <mixed-citation id="d435e489" publication-type="other">
Kaplan, H., K. Hill, J. Lancaster, and A. M. Hurtado. 2000. A theory of human life history evolution:
Diet, intelligence, and longevity. Evolutionary Anthropology /ssues News and Reviews.
9(4): 156—185.</mixed-citation>
         </ref>
         <ref id="d435e502a1310">
            <mixed-citation id="d435e506" publication-type="other">
Kline, R. B. 2010. Principles and Practice of Structural Equation Modeling. New York: The Guilford
Press.</mixed-citation>
         </ref>
         <ref id="d435e517a1310">
            <mixed-citation id="d435e521" publication-type="other">
Leowski, J. 1986. Mortality from acute respiratory infections in children under 5 years of age: Global
estimates. World Health Statistics Quarterly 39(2): 138-144.</mixed-citation>
         </ref>
         <ref id="d435e531a1310">
            <mixed-citation id="d435e535" publication-type="other">
Lopez, A. D., C. D. Mathers, M. Ezzati, D. T. Jamison, and C. J. L. Murray. 2006. Global and
regional burden of disease and risk factors, 2001: systematic analysis of population health
data. The Lancet. 367(9524): 1747-1757.</mixed-citation>
         </ref>
         <ref id="d435e548a1310">
            <mixed-citation id="d435e552" publication-type="other">
Low, B. S„ A. Hazel, N. Parker, and K. B. Welch. 2008. Influences on women's reproductive lives:
Unexpected ecological underpinnings. Cross-Cultural Research. 42(3 ):201 — 219.</mixed-citation>
         </ref>
         <ref id="d435e562a1310">
            <mixed-citation id="d435e566" publication-type="other">
MacCallum, R. C„ M. W. Browne, and H. M. Sugawara. 1996. Power analysis and determination of
sample size for covariance structure modeling. Psychological Methods. 1 (2): 130— 149.</mixed-citation>
         </ref>
         <ref id="d435e576a1310">
            <mixed-citation id="d435e580" publication-type="other">
Muthen, B., and L. Muthen. 2010. Mplus User's Guide (version 6.0). Los Angeles, CA: Muthen &amp;
Muthén.</mixed-citation>
         </ref>
         <ref id="d435e590a1310">
            <mixed-citation id="d435e594" publication-type="other">
Narayan, K.M.V., M.K. Ali, and J.P. Koplan. 2010. Global noncommunicable disease—where wolds
meet. New England Journal of Medicine. 353(13): 1 196-1198.</mixed-citation>
         </ref>
         <ref id="d435e605a1310">
            <mixed-citation id="d435e609" publication-type="other">
Nettle, D. 2010. Dying young and living fast: Variation in life history across English neighborhoods.
Behavioral Ecology. 21(2):387.</mixed-citation>
         </ref>
         <ref id="d435e619a1310">
            <mixed-citation id="d435e623" publication-type="other">
Nettle, D., D. A. Coall, and T. E. Dickins. 2011. Early-life conditions and age at first pregnancy
in British women. Proceedings of the Royal Society B: Biological Sciences.
278(1712): 1721.</mixed-citation>
         </ref>
         <ref id="d435e636a1310">
            <mixed-citation id="d435e640" publication-type="other">
Placek, C., and R. J. Quinlan. 2011. Environmental Risk and Adolescent Fertility in Africa and the
Caribbean. Anthropology: Washington State University. Pullman.</mixed-citation>
         </ref>
         <ref id="d435e650a1310">
            <mixed-citation id="d435e654" publication-type="other">
Promislow, D. E. L., and P. H. Harvey. 1990. Living fast and dying young: A comparative analysis
of life-history variation among mammals. J. Zool. 220(3):417-437.</mixed-citation>
         </ref>
         <ref id="d435e664a1310">
            <mixed-citation id="d435e668" publication-type="other">
Quinlan, R. J. 2007. Human parental effort and environmental risk. Proceedings. Biological
Sciences/The Royal Society. 274( 1606): 121—125.</mixed-citation>
         </ref>
         <ref id="d435e678a1310">
            <mixed-citation id="d435e682" publication-type="other">
Quinlan, R. J. 2010. Extrinsic mortality effects on reproductive strategies in a Caribbean community.
Human Nature. 21(2): 124—139.</mixed-citation>
         </ref>
         <ref id="d435e693a1310">
            <mixed-citation id="d435e697" publication-type="other">
Roff, D. A. 2002. Life History Evolution. Sunderland: Sinauer.</mixed-citation>
         </ref>
         <ref id="d435e704a1310">
            <mixed-citation id="d435e708" publication-type="other">
Sachs, J., and P. Malaney. 2002. The economic and social burden of malaria. Nature.
415(6872):680-685.</mixed-citation>
         </ref>
         <ref id="d435e718a1310">
            <mixed-citation id="d435e722" publication-type="other">
StataCorp. (2009). Stata statistical software: Release 11.</mixed-citation>
         </ref>
         <ref id="d435e729a1310">
            <mixed-citation id="d435e733" publication-type="other">
Stearns, S. C. 1989. Trade-offs in life-history evolution. Functional Ecology. 3(3):259-268.</mixed-citation>
         </ref>
         <ref id="d435e740a1310">
            <mixed-citation id="d435e744" publication-type="other">
Steams, S. C. 1992. The Evolution of Life Histories. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d435e751a1310">
            <mixed-citation id="d435e755" publication-type="other">
Walker, R., M. Gurven, K. Hill, A. Migliano, N. Chagnon, R. D. E. Souza, and T. Yamauchi. 2006.
Growth rates and life histories in twenty-two small-scale societies. Am. J. Hum. Biol. 311:
295-311.</mixed-citation>
         </ref>
         <ref id="d435e769a1310">
            <mixed-citation id="d435e773" publication-type="other">
Watson, J. T., M. Gayer, and M. A. Connolly. 2007. Epidemics after natural disasters. Emerging
Infectious Diseases. 13(1): 1 -</mixed-citation>
         </ref>
         <ref id="d435e783a1310">
            <mixed-citation id="d435e787" publication-type="other">
Wilson, M., and M. Daly. 1997. Life expectancy, economic inequality, homicide, and reproductive
timing in Chicago neighbourhoods. Br. Med. J. 314(7089): 1271-1274.</mixed-citation>
         </ref>
         <ref id="d435e797a1310">
            <mixed-citation id="d435e801" publication-type="other">
World Health Organization. 2009. World Health Statistics. Retrieved from www.who.int/whosis/
whostat/2009/en/index.html</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
