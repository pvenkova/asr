<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26332029</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Counting Souls</article-title>
            <subtitle>Towards an historical demography of Africa</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Walters</surname>
                  <given-names>Sarah</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>London School of Hygiene &amp; Tropical Medicine, U.K.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2016</year>
            <string-date>JANUARY - JUNE 2016</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2016</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">34</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26332026</issue-id>
         <fpage>63</fpage>
         <lpage>108</lpage>
         <permissions>
            <copyright-statement>© 2016 Sarah Walters</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26332029"/>
         <abstract xml:lang="eng">
            <sec>
               <label>BACKGROUND</label>
               <p>Little is known about even the relatively recent demographic history of Africa, because of the lack of data. Elsewhere, historical demographic trends have been reconstructed by applying family reconstitution to church records. Such data also exist throughout Africa from the late 19th century. For the Counting Souls Project, nearly one million records from the oldest Catholic parishes in East and Central Africa have been digitised. These data are currently being processed into a relational database. The aim of this paper is to describe their potential for demographic reconstruction in the region, and to outline how their provenance defines the analytical approach.</p>
            </sec>
            <sec>
               <label>RESULTS</label>
               <p>Empirically, religion is correlated with population patterns in contemporary Africa, and, historically, reproduction and family formation were central to Christian mission in the region. Measuring change using sources created by agents of change raises questions of epistemology, causation, and selection bias. This paper describes how these concerns are balanced by missionary determination to follow the intimate lives of their parishioners, to monitor their ‘souls’, and to measure their morality, fidelity, and faith. This intimate recording means that the African parish registers, together with related sources such as missionary diaries and letters and oral histories, describe qualitatively and quantitatively what happens to individual agency (reproductive decision-making) when the moral hegemony shifts (via evangelisation and colonisation), and how the two interact in a reciprocal process of change.</p>
            </sec>
            <sec>
               <label>CONCLUSION</label>
               <p>Reconstructing long-term demographic trends using parish registers in Africa is therefore more than simply generating rates and testing their reliability. It is a bigger description of how ‘decision rules’ are structured and re-structured, unpicking the cognitive seam between individual and culture by exploring dynamic micro-interactions between reproduction, honour, hope, and modernity over the long term. With such a mixed-methods approach, parish registers offer real potential for historical demography in Africa.</p>
            </sec>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Allen, D.R. (2000). Learning the facts of life: past and present experiences in a rural Tanzanian community. Africa Today 47(3-4): 3–27. doi:10.1353/at.2000.0059.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Austen, R.A. (1968). Northwest Tanzania under German and British rule: colonial policy and tribal politics, 1889–1939. New Haven, CT: Yale University Press.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Bernardi, L. and Hutter, I. (2007). The anthropological demography of Europe. Demographic Research 17(18): 541–566. doi:10.4054/DemRes.2007.17.18.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Blacker, J.G. (1962). Population growth and differential fertility in Zanzibar protectorate. Population Studies 15(3): 258–266. doi:10.2307/2172728.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Boucher, C. (2002). Digging our roots: the Chamare Museum frescoes. Mua mission: KuNgoni Centre of Culture and Art.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Brass, W., Coale, A.J., Demeny, P., Heissel, D.F., Lorimer, F., Romaniuk, A., and Van de Walle, E. (1968). The demography of tropical Africa. Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Breckenridge, K. and Szreter, S. (2012). Registration and recognition: documenting the person in world history. Oxford: Published for the British Academy by Oxford University Press. doi:10.5871/bacad/9780197265314.001.0001.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Breschi, M., Manfredini, M., and Fornasin, A. (2011). Demographic responses to short-term stress in a 19th century Tuscan population: the case of household out-migration. Demographic Research 25(15): 491–512. doi:10.4054/DemRes. 2011.25.15.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Brown, W. and Brown, B. (1969). White Father Archives in Tanzania. 12(2): 211–212. doi:10.2307/523165.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Caldwell, J.C. (1987). The cultural context of high fertility in sub-Saharan Africa. Population and Development Review 13(3): 409–437. doi:10.2307/1973133.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Caldwell, J.C., Orubuloye, I.O., and Caldwell, P. (1992). Fertility decline in Africa: a new type of transition? Population and Development Review 18(2): 211–242. doi:10.2307/1973678.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Ceillier, J.-C. (2008). Histoire des missionnaires d'Afrique (Pères blancs): de la fondation par Mgr Lavigerie à la mort du fondateur (1868−1892). Paris: Karthala.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Clark, S. and Colson, E. (1995). Ten thousand Tonga: a longitudinal anthropological study from southern Zambia, 1956–1991. Population Studies 49(1): 91–109. doi:10.1080/0032472031000148266.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Clement, D. (1977). The Research Committee at Bujora. Tanganyika Notes and Records 75: 81–82.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Colwell, S. (2000). Vision and revision: demography, maternal and child health development, and the representation of native women in colonial Tanganyika. [PhD Thesis]. Urbana-Champaign, IL: University of Illinois.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Cory, H. (1953). Sukuma law and custom. London: Published for the International African Institute by Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Crampin, A.C., Dube, A., Mboma, S., Price, A., Chihana, M., Jahn, A., Baschieri, A., Molesworth, A., Mwaiyeghele, E., Branson, K., Floyd, S., McGrath, N., Fine, P.E., French, N., Glynn, J.R., and Zaba, B. (2012). Profile: the Karonga Health and Demographic Surveillance System. International Journal of Epidemiology 41(3): 676–685. doi:10.1093/ije/dys088.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Del Panta, L., Rettaroli, R., and Rosental, P.-A. (2006). Methods of historical demography. In: Caselli, G., Vallin, J. and Wunsch, G. (eds.). Demography: analysis and synthesis: a treatise in population. Oxford: Elsevier: 597–618.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Doyle, S. (2012). Before HIV: sexuality, fertility and mortality in East Africa, 1900-1980. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Faupel, J.F. (1962). African holocaust: the story of the Uganda martyrs. London: G. Chapman.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Feltz, G. (1990). Catholic missions, mentalities and quantitative history in Burundi 1990–1962. In: Fetter, B. (ed.). Demography from scanty evidence. Boulder, CO: Lynne Rienner.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Fetter, B. (1989). Colonial microenvironments and the mortality of educated young men in Northern Malawi, 1897−1927. Canadian Journal of African Studies 23: 399–415. doi:10.2307/485185.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Fleury, M. and Henry, L. (1956). Des registres paroissiaux à l'histoire de la population: manuel de dépouillement et d'exploitation de l'état civil ancien. Paris: L'institut National D'etudes Demographiques.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Garvey, B. (1994). Bembaland church: religious and social change in South Central Africa, 1891–1964. Leiden: E.J. Brill.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Good, C.M. (2004). The steamer parish: the rise and fall of missionary medicine on an African frontier. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Grondin, E. (1995). Kisa cha wanawake wanne wa Kimasai: miongoni mwa Wairaqw. Peramiho: Peramiho Printing Press.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Guzowski, P. (2012). An inventory of census micro-data from the territory of Western Ukraine (Red Ruthenia, Eastern Galicia) before 1914. MOSAIC Working Papers. WP2012-003. Rostock: Max Planck Institute for Demographic Research: 62.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Hansen, K.T. (1992). African encounters with domesticity. New Brunswick, NJ: Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Hastings, A. (1979). A history of African Christianity, 1950–1975. Cambridge: Cambridge University Press. doi:10.1017/CBO9780511563171.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Hastings, A. (1996). The Church in Africa, 1450–1950. Oxford: Clarendon Press. doi:10.1093/0198263996.001.0001.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Hinfelaar, H.F. (2004). History of the Catholic Church in Zambia. Lusaka: Bookworld Publishers.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Hinfelaar, M. and Macola, G. (2004). A first guide to non-governmental archives in Zambia [unpublished manuscript]. Lusaka: National Archives of Zambia.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Hinfelaar, M. and Macola, G. (n.d.). The White Fathers’ Archive in Zambia [unpublished manuscript]. Lusaka: Faith and Encounter Centre (FENZA).</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Hodgson, D.L. (2005). The church of women: gendered encounters between Maasai and missionaries. Bloomington, IN: Indiana University Press.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Hunt, N.R. (1988). “Le bébé en brousse”: European women, African birth spacing and colonial intervention in breast-feeding in the Belgian Congo. International Journal of African Historical Studies 21(3): 401–432. doi:10.2307/219448.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Hunt, N.R. (1991). Noise over camouflaged polygamy, colonial morality taxation, and a woman-naming crisis in Belgian Africa. Journal of African History 32(3): 471–494. doi:10.1017/S0021853700031558.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Hunt, N.R. (1999). A colonial lexicon of birth ritual, medicalization, and mobility in the Congo. Durham, NC: Duke University Press. doi:10.1215/9780822381365.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Iliffe, J. (1979). A modern history of Tanganyika. Cambridge: Cambridge University Press. doi:10.1017/CBO9780511584114.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Iliffe, J. (1995). Africans: the history of a continent. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Iliffe, J. (n.d.). Review of Cross and Flag in Africa, by Aylward Shorter [electronic resource]. Rome: Site international des Missionnaires d’Afrique. http://www.mafrome.org/livre_shorter.htm.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Johnson-Hanks, J. (2002). On the limits of life stages in ethnography: toward a theory of vital conjunctures. American Anthropologist 104(3): 865–880. doi:10.1525/aa.2002.104.3.865.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Johnson-Hanks, J. (2004). Uncertainty and the second space: modern birth timing and the dilemma of education. European Journal of Population 20: 351–373. doi:10.1007/s10680-004-4095-5.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Johnson-Hanks, J. (2006). Uncertain honor: modern motherhood in an African crisis. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Johnson-Hanks, J. (2015). Populations are composed one event at a time. In: Kreager, P., Winney, B., Uliajaszek, S., and Capelli, C. (eds.). Population in the human sciences: concepts, models, evidence. Oxford: Oxford University Press. doi:10.1093/acprof:oso/9780199688203.003.0009.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Kachipila, H. (2006). The revival of “Nyau” and changing gender relations in early colonial central Malawi. Journal of Religion in Africa 36(3/4): 319–345. doi:10.1163/157006606778941959.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Katzenellenbogen, J., Yach, D., and Dorrington, R.E. (1993). Mortality in a rural South African mission, 1837–1909: an historical cohort study using church records. International Journal of Epidemiology 22(6): 965–975. doi:10.1093/ije/22. 6.965.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Kjekshus, H. (1996). Ecology control and economic development in East African history: the case of Tanganyika 1850–1950. London: James Currey.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Koponen, J. (1996). Population: a dependent variable. In: Maddox, G., Giblin, J.L., and Kimambo, I.N. (eds.). Custodians of the land: ecology and culture in the history of Tanzania. Ohio, OH: Ohio University Press.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Kuczynski, R.R. (1949). Demographic survey of the British Colonial Empire, Vol. II. London: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Larsson, B. (1991). Conversion to greater freedom? Women, church and social change in North-Western Tanzania under colonial rule. Stockholm: Almqvist and Wiksell.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Laslett, P. (1977). Family life and illicit love in earlier generations: essays in historical sociology. Cambridge: Cambridge University Press. doi:10.1017/CBO9780511 522659.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Lesthaeghe, R. (1989). Production and reproduction in sub-Saharan Africa: an overview of organizing principles. In: Lesthaeghe, R. (ed.). Reproduction and social organization in sub-Saharan Africa. Berkeley, CA: University of California Press: 13–59.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Linden, I. and Linden, J. (1974). Catholics, peasants and Chewa resistance in Nyasaland, 1889–1939. London: Heinemann.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Mahapatra, P., Shibuya, K., Lopez, A.D., Coullare, F., Notzon, F.C., Rao, C., and Szreter, S. (2007). Civil registration systems and vital statistics: successes and missed opportunities. Lancet 370(9599): 1653–1663. doi:10.1016/S0140- 6736(07)61308-7.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Manfredini, M. and Breschi, M. (2008a). Marriage and the kin network: evidence from a 19th century Italian community. In: Bengtsson, T.M., Geraldine P. (eds.).</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Kinship and Demographic Behavior in the Past. Amsterdam: Springer: 15–36. doi:10.1007/978-1-4020-6733-4_2.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Manfredini, M. and Breschi, M. (2008b). Socioeconomic structure and differential fertility by wealth in a mid-nineteenth century Tuscan community. Annales de démographie historique 115: 799–828.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">McCracken, J. (2008). Politics and Christianity in Malawi, 1875-1940: the impact of the Livingstonia Mission in the Northern Province. Zomba: Kachere Series.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Moultrie, T.A., Sayi, T.S., and Timæus, I.M. (2012). Birth intervals, postponement, and fertility decline in Africa: a new type of transition? Population Studies 66(3): 241–258. doi:10.1080/00324728.2012.701660.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Newton, G.H. (2005). Creating a customisable name matchinig algorithm for historical computing by refactoring. [MSc Thesis]. Cambridge: Anglia Ruskin University.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Nhonoli, A.M.M. (1954). An enquiry into the infant mortality rate in rural areas of Unyamwezi. The East African Medical Journal 31: 1–12.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Nolan, F. (2008). Les débuts de la mission de Bukumbi au sud du lac Victoria (dans la Tanzanie actuelle) 1883–1912. Histoire, Monde et Cultures Religieuses 4(8): 11–38. doi:10.3917/hmc.008.0011.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Nolan, F. (2012). The White Fathers in colonial Africa (1919-1939). Nairobi: Paulines Publications.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Notkola, V. and Siiskonen, H. (2000). Fertility, mortality and migration in SubSaharan Africa: the case of Ovamboland in North Namibia, 1925−90. Basingstoke: Macmillan. doi:10.1057/9780333981344.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Notkola, V., Timæus, I., and Siiskonen, H. (2000). Mortality transition in the Ovamboland region of Namibia, 1930−1990. Population Studies 54: 153–167. doi:10.1080/713779086.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Olusanya, P.O. (1969a). Modernisation and the level of fertility in Western Nigeria.</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Paper presented at International Union for Scientific Study of Population (IUSSP) International Population Conference, London, 1969.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Olusanya, P.O. (1969b). Rural-urban fertility differentials in Western Nigeria. Population Studies 23(3): 363–378. doi:10.2307/2172876.</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Őri, P. and Pakot, L. (2011). Census and census-like material preserved in the archives of Hungary, Slovakia and Transylvania (Romania), 18–19th centuries. MPIDR Working Papers. WP2011-020. Rostock: Max Planck Institute for Demographic Research: 57.</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">Philips, L. (2000). The Double Metaphone Search Algorithm. C/C++ User’s Journal 18(6).</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Raum, O.F. (1972). The Chaga of north-eastern Tanzania. In Molnos, A. (ed.). Cultural source materials for population planning in East Africa. Vol. 2. Innovations and communication. Nairobi: East African Publishing House: 68–80.</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Renault, F. (1992). Le Cardinal Lavigerie. Paris: Fayard.</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Romaniuk, A. (1980). Increase in natural fertility during the early stages of modernization: evidence from an African case study, Zaire. Population Studies 34(2): 293–310. doi:10.2307/2175188.</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">Ruggles, S. (1992). Migration, marriage, and mortality: correcting sources of bias in English family reconstitutions. Population Studies 46(3): 507–522. doi:10.1080/0032472031000146486.</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">Saucier, J.-F. (1972). Correlates of the long post-partum taboo: a cross-cultural study. Current Anthropology 13: 238–249. doi:10.1086/201273.</mixed-citation>
         </ref>
         <ref id="ref76">
            <mixed-citation publication-type="other">Sayer, A. (2000). Moral economy and political economy. Studies in Political Economy 61: 79–104.</mixed-citation>
         </ref>
         <ref id="ref77">
            <mixed-citation publication-type="other">Scheper-Hughes, N. (1985). Culture, scarcity, and maternal thinking: maternal detachment and infant survival in a Brazilian shantytown. Ethos 13(4): 291–317. doi:10.1525/eth.1985.13.4.02a00010.</mixed-citation>
         </ref>
         <ref id="ref78">
            <mixed-citation publication-type="other">Scheper-Hughes, N. (1992). Death without weeping: the violence of everyday life in Brazil. Berkeley, CA: University of California Press.</mixed-citation>
         </ref>
         <ref id="ref79">
            <mixed-citation publication-type="other">Schoenmaeckers, R. (1981). The child-spacing tradition and the postpartum taboo in tropical Africa: anthropological evidence. In Page, H.J. and Lesthaeghe, R. (eds.). Child-spacing in tropical Africa: traditions and change. New York, NY: Academic Press: 25–71.</mixed-citation>
         </ref>
         <ref id="ref80">
            <mixed-citation publication-type="other">Shemeikka, R., Notkola, V., and Siiskonen, H. (2005). Fertility decline in North- Central Namibia: an assessment of fertility in the period 1960-2000 based on parish registers. Demographic Research 13(4): 83–116. doi:10.4054/DemRes. 2005.13.4.</mixed-citation>
         </ref>
         <ref id="ref81">
            <mixed-citation publication-type="other">Shorter, A. (2006). The cross and flag in Africa: the “white fathers” during the colonial scramble (1892–1914). Maryknoll, NY: Orbis Books.</mixed-citation>
         </ref>
         <ref id="ref82">
            <mixed-citation publication-type="other">Siiskonen, H., Taskinen, A., and Notkola, V. (2005). Parish registers: a challenge for African historical demography. History in Africa 32: 385–402. doi:10.1353/hia. 2005.0024.</mixed-citation>
         </ref>
         <ref id="ref83">
            <mixed-citation publication-type="other">Smythe, K.R. (2006). Fipa families: reproduction and Catholic evangelization in Nkansi, Ufipa, 1880–1960. Portsmouth, NH: Heinemann.</mixed-citation>
         </ref>
         <ref id="ref84">
            <mixed-citation publication-type="other">St-Arneault, S. (2007). KuNgoni: when water falls sand becomes crystal. Mua mission: KuNgoni Centre of Culture and Art.</mixed-citation>
         </ref>
         <ref id="ref85">
            <mixed-citation publication-type="other">Steegstra, M. (2005). Dipo and the politics of culture in Ghana. Accra: Woeli Publishing Services.</mixed-citation>
         </ref>
         <ref id="ref86">
            <mixed-citation publication-type="other">Summers, C. (1991). Intimate colonialism − the imperial production of reproduction in Uganda, 1907–1925. Signs 16(4): 787–807. doi:10.1086/494703.</mixed-citation>
         </ref>
         <ref id="ref87">
            <mixed-citation publication-type="other">Sundkler, B. and Steed, C. (2000). A history of the church in Africa. Cambridge: Cambridge University Press. doi:10.1017/CBO9780511497377.</mixed-citation>
         </ref>
         <ref id="ref88">
            <mixed-citation publication-type="other">Swidler, A. (2013). African affirmations: The religion of modernity and the modernity of religion. International Sociology 28(6): 680–696. doi:10.1177/02685809 13508568.</mixed-citation>
         </ref>
         <ref id="ref89">
            <mixed-citation publication-type="other">Szreter, S., Sholkamy, H., and Dharmalingam, A. (2004). Categories and contexts: anthropological and historical studies in critical demography. Oxford: Oxford University Press. doi:10.1093/0199270570.001.0001.</mixed-citation>
         </ref>
         <ref id="ref90">
            <mixed-citation publication-type="other">Szreter, S. and Breckenridge, K. (2012). Recognition and registration: the infrastructure of personhood in world history. In: Breckenridge, K. and Szreter, S. (eds.). Registration and recognition: documenting the person in world history. Oxford: Published for the British Academy by Oxford University Press. doi:10.5871/bacad/9780197265314.003.0001.</mixed-citation>
         </ref>
         <ref id="ref91">
            <mixed-citation publication-type="other">Tanner, R.E.S. (1955). The sexual mores of the Basukuma. International Journal of Sexology 8: 238–241.</mixed-citation>
         </ref>
         <ref id="ref92">
            <mixed-citation publication-type="other">Thomas, L.M. (2003). Politics of the womb: women, reproduction, and the state in Kenya. Berkeley, CA: University of California Press. doi:10.1093/0198293 488.003.0005.</mixed-citation>
         </ref>
         <ref id="ref93">
            <mixed-citation publication-type="other">Thornton, J. (1977). An eighteenth century baptismal register and the demographic history of Manguenzo. In: Fyfe, C. and McMaster, D. (eds.). African historical demography (Volume 1, proceedings of a seminar held in the Centre of African Studies, University of Edinburgh 29th and 30th April 1977). Edinburgh: Centre of African Studies, University of Edinburgh: 405–415.</mixed-citation>
         </ref>
         <ref id="ref94">
            <mixed-citation publication-type="other">UNICEF (2013). Every child’s birth right: inequities and trends in birth registration. New York, NY: UNICEF.</mixed-citation>
         </ref>
         <ref id="ref95">
            <mixed-citation publication-type="other">van Breugel, J.W.M. and Ott, M. (2001). Chewa traditional religion. Blantyre: Christian Literature Association in Malawi.</mixed-citation>
         </ref>
         <ref id="ref96">
            <mixed-citation publication-type="other">Varkevisser, C. (1973). The Sukuma of northern Tanzania. In: Molnos, A. (ed.). Cultural source materials for population planning in East Africa. Vol. 3. Beliefs and practices. Nairobi: East African Publishing House: 234–248.</mixed-citation>
         </ref>
         <ref id="ref97">
            <mixed-citation publication-type="other">Vaughan, M. (1983). Which family?: Problems in the reconstruction of the history of the family as an economic and cultural unit. Journal of African History 24(2): 275–283. doi:10.1017/S0021853700021988.</mixed-citation>
         </ref>
         <ref id="ref98">
            <mixed-citation publication-type="other">Vaughan, M. (1991). Curing their ills: colonial power and African illness. Cambridge: Polity.</mixed-citation>
         </ref>
         <ref id="ref99">
            <mixed-citation publication-type="other">Waliggo, J.M. (1976). The Catholic Church in the Buddu Province of Buganda, 1879−1925. [PhD Thesis]. Cambridge: University of Cambridge.</mixed-citation>
         </ref>
         <ref id="ref100">
            <mixed-citation publication-type="other">Walters, S. (2008). Fertility, mortality and marriage in northwest Tanzania, 1920−1970: a demographic study using parish registers. [PhD Thesis]. Cambridge: University of Cambridge.</mixed-citation>
         </ref>
         <ref id="ref101">
            <mixed-citation publication-type="other">Walters, S., Helleringer, S., and Masquelier, B. (2013). Report on the Likoma Island Anglican parish registers [unpublished manuscript]. London: London School of Hygiene and Tropical Medicine.</mixed-citation>
         </ref>
         <ref id="ref102">
            <mixed-citation publication-type="other">Watkins, S. and Warriner, I. (2003). How Do We Know We Need to Control for Selectivity? Demographic Research Special Collection 1(4): 109–142. doi:10.4054/DemRes.2003.S1.4.</mixed-citation>
         </ref>
         <ref id="ref103">
            <mixed-citation publication-type="other">Willigan, J.D. and Lynch, K.A. (1982). Sources and methods of historical demography. New York, NY: Academic Press. doi:10.1016/B978-0-12-757020-4.50024-9.</mixed-citation>
         </ref>
         <ref id="ref104">
            <mixed-citation publication-type="other">Wilson, M. (1957). Rituals of kinship among the Nyakyusa. London: Published for the International African Institute by Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref105">
            <mixed-citation publication-type="other">Wrigley, E.A. (1966). Family reconstitution. In: Eversley, D.E.C., Laslett, P. and Wrigley, E.A. (eds.). An introduction to English historical demography. New York, NY: Weidenfeld and Nicolson.</mixed-citation>
         </ref>
         <ref id="ref106">
            <mixed-citation publication-type="other">Wrigley, E.A. and Schofield, R.S. (1973). Nominal record linkage by computer and the logic of family reconstitution. In: Wrigley, E.A. (ed.). Identifying people in the past. London: Arnold. doi:10.2307/2173919.</mixed-citation>
         </ref>
         <ref id="ref107">
            <mixed-citation publication-type="other">Wrigley, E.A. (1977). Births and baptisms: the use of Anglican baptism registers as a source of information about the numbers of births in England before the beginning of civil registration. Population Studies 31(2): 281–312.</mixed-citation>
         </ref>
         <ref id="ref108">
            <mixed-citation publication-type="other">Wrigley, E.A. (1983). English population history from family reconstitution: summary results 1600−1799. Population Studies 37(2): 157–184. doi:10.2307/2173980.</mixed-citation>
         </ref>
         <ref id="ref109">
            <mixed-citation publication-type="other">Wrigley, E.A. and Schofield, R.S. (1989). The population history of England 1541–1871: a reconstruction. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref110">
            <mixed-citation publication-type="other">Wrigley, E.A. (1994). The effect of migration on the estimation of marriage age in family reconstitution studies. Population Studies 48(1): 81–97.</mixed-citation>
         </ref>
         <ref id="ref111">
            <mixed-citation publication-type="other">Wrigley, E.A., Davies, R.S., Oeppen, J.E., and Schofield, R.S. (1997). English population history from family reconstitution, 1580–1837. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref112">
            <mixed-citation publication-type="other">Yeatman, S.E. and Trinitapoli, J. (2008). Beyond denomination: the relationship between religion and family planning in rural Malawi. Demographic Research 19(55): 1851–1882. doi:10.4054/DemRes.2008.19.55.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
