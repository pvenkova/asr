<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30078067</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>Parasites</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Trichomoniasis in Men and HIV Infection: Data from 2 Outpatient Clinics at Lilongwe Central Hospital, Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Matthew A.</given-names>
                  <surname>Price</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>William C.</given-names>
                  <surname>Miller</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>S. Cornelia</given-names>
                  <surname>Kaydos-Daniels</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Irving F.</given-names>
                  <surname>Hoffman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>David</given-names>
                  <surname>Chilongozi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Francis E.</given-names>
                  <surname>Martinson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>David</given-names>
                  <surname>Namakhwa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Jimmy</given-names>
                  <surname>Malanda</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Myron</given-names>
                  <surname>Cohen</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>10</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">190</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">8</issue>
         <issue-id>i30078055</issue-id>
         <fpage>1448</fpage>
         <lpage>1455</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30078067"/>
         <abstract>
            <p>Background. Little is known about the epidemiologic profile of trichomoniasis in men and its relationship to human immunodeficiency virus (HIV) infection. Among men presenting for care for symptomatic sexually transmitted infections (STIs) in Malawi, trichomoniasis is not considered for first-line treatment. Methods. We conducted a cross-sectional survey of 1187 men attending either a dermatology or STI outpatient clinic in the capital of Malawi. Men were interviewed, and the etiologies of the STIs were determined. Results. At the STI clinic (n = 756 men), we identified 150 men (20%) with Trichomonas vaginalis infection, 358 men (47%) with HIV infection, and 335 men (44%) with Neisseria gonorrhoeae infection. At the dermatology clinic (n = 431 men), we identified 54 (13%), 118 (27%), and 2 (0.5%) men, respectively. At both clinics, a lower education level and reporting never having used a condom were predictive of T. vaginalis infection. Only at the dermatology clinic was older age associated with infection, and only at the STI clinic were marital, genital ulcer disease, and HIV-infection status associated with T. vaginalis infection. At the STI clinic, urethral symptoms attributable to trichomoniasis were more severe among HIV-positive men than among HIV-negative men. Conclusions. Given its high prevalence and the increased risk for HIV transmission, T. vaginalis infection should be reconsidered for inclusion in the Malawi STI-treatment regimen for men.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d827e272a1310">
            <label>1</label>
            <mixed-citation id="d827e279" publication-type="other">
Gerbase AC, Rowley JT, Heymann DH, Berkley SF, Piot P. Global prev-
alence and incidence estimates of selected curable STDs. Sex Transm
Infect 1998;74(Suppl l):S12-6.</mixed-citation>
         </ref>
         <ref id="d827e292a1310">
            <label>2</label>
            <mixed-citation id="d827e299" publication-type="other">
Laga M, Alary M, Nzila N, et al. Condom promotion, sexually trans-
mitted diseases treatment, and declining incidence of HIV-1 infection
in female Zairian sex workers. Lancet 1994; 344:246-8.</mixed-citation>
         </ref>
         <ref id="d827e312a1310">
            <label>3</label>
            <mixed-citation id="d827e319" publication-type="other">
Pepin J, Sobela F, Deslandes S, et al. Etiology of urethral discharge in
West Africa: the role of Mycoplasma genitalium and Trichomonas vag-
inalis. Bull World Health Organ 2001;79:118-26.</mixed-citation>
         </ref>
         <ref id="d827e332a1310">
            <label>4</label>
            <mixed-citation id="d827e339" publication-type="other">
Morency P, Dubois MJ, Gresenguet G, et al. Aetiology of urethral
discharge in Bangui, Central African Republic. Sex Transm Infect 2001;
77:125-9.</mixed-citation>
         </ref>
         <ref id="d827e353a1310">
            <label>5</label>
            <mixed-citation id="d827e360" publication-type="other">
Hobbs MM, Kazembe P, Reed AW, et al. Trichomonas vaginalis as a
cause of urethritis in Malawian men. Sex Transm Dis 1999;26:381-7.</mixed-citation>
         </ref>
         <ref id="d827e370a1310">
            <label>6</label>
            <mixed-citation id="d827e377" publication-type="other">
Watson-Jones D, Mugeye K, Mayaud P, et al. High prevalence of trich-
omoniasis in rural men in Mwanza, Tanzania: results from a population
based study. Sex Transm Infect 2000; 76:355-62.</mixed-citation>
         </ref>
         <ref id="d827e390a1310">
            <label>7</label>
            <mixed-citation id="d827e397" publication-type="other">
Hook EW III. Trichomonas vaginalis: no longer a minor STD. Sex
Transm Dis 1999;26:388-9.</mixed-citation>
         </ref>
         <ref id="d827e407a1310">
            <label>8</label>
            <mixed-citation id="d827e414" publication-type="other">
Kreiger JN. Consider diagnosis and treatment of trichomoniasis in men.
Sex Transm Dis 2000;27:241-2.</mixed-citation>
         </ref>
         <ref id="d827e424a1310">
            <label>9</label>
            <mixed-citation id="d827e431" publication-type="other">
Krieger JN, Jenny C, Verdon M, et al. Clinical manifestations of tricho-
moniasis in men. Ann Intern Med 1993; 118:844-9.</mixed-citation>
         </ref>
         <ref id="d827e441a1310">
            <label>10</label>
            <mixed-citation id="d827e448" publication-type="other">
Jackson DJ, Rakwar JP, Chohan B, et al. Urethral infection in a work-
place population of East African men: evaluation of strategies for
screening and management. J Infect Dis 1997; 175:833-8.</mixed-citation>
         </ref>
         <ref id="d827e462a1310">
            <label>11</label>
            <mixed-citation id="d827e469" publication-type="other">
Sorvillo F, Kerndt P. Trichomonas vaginalis and amplification of HIV-
1 transmission. Lancet 1998;351:213-4.</mixed-citation>
         </ref>
         <ref id="d827e479a1310">
            <label>12</label>
            <mixed-citation id="d827e486" publication-type="other">
Sorvillo F, Smith L, Kerndt P, Ash L. Trichomonas vaginalis, HIV, and
African-Americans. Emerg Infect Dis 2001; 7:927-32.</mixed-citation>
         </ref>
         <ref id="d827e496a1310">
            <label>13</label>
            <mixed-citation id="d827e503" publication-type="other">
Jackson DJ, Rakwar JP, Bwayo JJ, Kreiss JK, Moses S. Urethral Trichomo-
nas vaginalis infection and HIV-1 transmission. Lancet 1997; 350:1076.</mixed-citation>
         </ref>
         <ref id="d827e513a1310">
            <label>14</label>
            <mixed-citation id="d827e520" publication-type="other">
Van der Veen F, Fransen L. Drugs for STD management in developing
countries: choice, procurement, cost, and financing. Sex Transm Infect
1998;74(Suppl l):S166-74.</mixed-citation>
         </ref>
         <ref id="d827e533a1310">
            <label>15</label>
            <mixed-citation id="d827e540" publication-type="other">
Petrin D, Delgaty K, Bhatt R, Garber G. Clinical and microbiological
aspects of Trichomonas vaginalis. Clin Microbiol Rev 1998; 11:300-17.</mixed-citation>
         </ref>
         <ref id="d827e550a1310">
            <label>16</label>
            <mixed-citation id="d827e557" publication-type="other">
Price M, Zimba D, Hoffman I, et al. The addition of treatment for
trichomoniasis to the syndromic management of urethritis in Malawi:
a randomized clinical trial. Sex Transm Dis 2003;30:516-22.</mixed-citation>
         </ref>
         <ref id="d827e571a1310">
            <label>17</label>
            <mixed-citation id="d827e578" publication-type="other">
Kaydos SC, Swygard H, Wise SL, et al. Development and validation of
a PCR-based enzyme-linked immunosorbent assay with urine for use in
clinical research settings to detect Trichomonas vaginalis in women. J Clin
Microbiol 2002;40:89-95. 17,</mixed-citation>
         </ref>
         <ref id="d827e594a1310">
            <label>18</label>
            <mixed-citation id="d827e601" publication-type="other">
Greenland S, Rothman KJ. Concepts of interaction. In: Rothman KJ,
Greenland S, eds. Modern epidemiology. Philadelphia: Lippincott-
Raven, 1998:329-42.</mixed-citation>
         </ref>
         <ref id="d827e614a1310">
            <label>19</label>
            <mixed-citation id="d827e621" publication-type="other">
Cohen MS, Hoffman IF, Royce RA, et al. Reduction of concentration
of HIV-1 in semen after treatment of urethritis: implications for pre-
vention of sexual transmission of HIV-1. Lancet 1997;349:1868-73.</mixed-citation>
         </ref>
         <ref id="d827e634a1310">
            <label>20</label>
            <mixed-citation id="d827e641" publication-type="other">
Latif AS, Mason PR, Marowa E. Urethral trichomoniasis in men. Sex
Transm Dis 1987;14:9-11. 20.</mixed-citation>
         </ref>
         <ref id="d827e651a1310">
            <label>21</label>
            <mixed-citation id="d827e658" publication-type="other">
Anosike JC, Onwuliri CO, Inyang RE, et al. Trichomoniasis amongst
students of a higher institution in Nigeria. Appl Parasitol 1993; 34:19-25.</mixed-citation>
         </ref>
         <ref id="d827e668a1310">
            <label>22</label>
            <mixed-citation id="d827e675" publication-type="other">
Kaydos-Daniels SC, Miller WC, Hoffman I, et al. Validation of a urine-
based PCR-enzyme-linked immunosorbent assay for use in clinical re-
search settings to detect Trichomonas vaginalis in men. J Clin Microbiol
2003;41:318-23.</mixed-citation>
         </ref>
         <ref id="d827e692a1310">
            <label>23</label>
            <mixed-citation id="d827e699" publication-type="other">
Kaydos-Daniels SC, Miller WC, Hoffman I, et al. The use of specimens
from various genitourinary sites in men, to detect Trichomonas vaginalis
infection. J Infect Dis 2004; 189:1926-31.</mixed-citation>
         </ref>
         <ref id="d827e712a1310">
            <label>24</label>
            <mixed-citation id="d827e719" publication-type="other">
Pillay DG, Hoosen AA, Vezi B, Moodley C. Diagnosis of Trichomonas
vaginalis in male urethritis. Trop Geogr Med 1994;46:44-5.</mixed-citation>
         </ref>
         <ref id="d827e729a1310">
            <label>25</label>
            <mixed-citation id="d827e736" publication-type="other">
Rottingen J-A, Cameron DW, Garnett GP. A systematic review of the
epidemiologic interactions between classic sexually transmitted diseases
and HIV: how much is known? Sex Transm Dis 2001;28:579-97.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
