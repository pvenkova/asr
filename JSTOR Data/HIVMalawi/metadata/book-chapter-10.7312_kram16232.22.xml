<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt125vrx</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/kram16232</book-id>
      <subj-group>
         <subject content-type="call-number">TX803.P35K73 2013</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Peanut butter</subject>
         <subj-group>
            <subject content-type="lcsh">United States</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Creamy and Crunchy</book-title>
         <subtitle>An Informal History of Peanut Butter, the All-American Food</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>KRAMPNER</surname>
               <given-names>JON</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>11</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="epub">9780231530934</isbn>
      <isbn content-type="epub">0231530935</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Jon Krampner</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/kram16232"/>
      <abstract abstract-type="short">
         <p>More than Mom's apple pie, peanut butter is the all-American food. With its rich, roasted-peanut aroma and flavor; caramel hue; and gooey, consoling texture, peanut butter is an enduring favorite, found in the pantries of at least 75 percent of American kitchens. Americans eat more than a billion pounds a year. According to the Southern Peanut Growers, a trade group, that's enough to coat the floor of the Grand Canyon (although the association doesn't say to what height).</p>
         <p>Americans spoon it out of the jar, eat it in sandwiches by itself or with its bread-fellow jelly, and devour it with foods ranging from celery and raisins ("ants on a log") to a grilled sandwich with bacon and bananas (the classic "Elvis"). Peanut butter is used to flavor candy, ice cream, cookies, cereal, and other foods. It is a deeply ingrained staple of American childhood. Along with cheeseburgers, fried chicken, chocolate chip cookies (and apple pie), peanut butter is a consummate comfort food.</p>
         <p>In<italic>Creamy and Crunchy</italic>are the stories of Jif, Skippy, Peter Pan; the plight of black peanut farmers; the resurgence of natural or old-fashioned peanut butter; the reasons why Americans like peanut butter better than (almost) anyone else; the five ways that today's product is different from the original; the role of peanut butter in fighting Third World hunger; and the Salmonella outbreaks of 2007 and 2009, which threatened peanut butter's sacred place in the American cupboard. To a surprising extent, the story of peanut butter is the story of twentieth-century America, and Jon Krampner writes its first popular history, rich with anecdotes and facts culled from interviews, research, travels in the peanut-growing regions of the South, personal stories, and recipes.</p>
      </abstract>
      <counts>
         <page-count count="320"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.5</book-part-id>
                  <title-group>
                     <label>One</label>
                     <title>PEANUTS 101</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>
                        <italic>A</italic>bout 80 million years ago, the Coastal Plain began to form along the southeastern and Gulf coasts of what’s now the United States. The sandy loam it left behind, along with the warm southern climate, would prove ideal for growing peanuts.</p>
                     <p>Sandy loam is loose and soft; it looks a little like beach sand but is more nutritious for plants and is found as far inland as the Fall Line, a twenty-mile-wide zone separating the soft sediments of the Coastal Plain from the denser clay soils of the Appalachian foothills. The Fall Line got its name because the junction of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.6</book-part-id>
                  <title-group>
                     <label>Two</label>
                     <title>THE SOCIAL RISE OF THE PEANUT</title>
                  </title-group>
                  <fpage>14</fpage>
                  <abstract>
                     <p>
                        <italic>P</italic>eanuts are uniquely evocative of the South. In early October 2008, as the U.S. and world economies were collapsing, I was in Tifton, Georgia, the heart of the peanut belt. On a cool bright morning, as I walked out of my motel along Interstate 75, the musty, fragrant scent of peanuts lying in windrows outside the city wafted through the air, discernable over the diesel exhalations of big rigs heading north on the interstate. In Southwest Georgia, peanuts are a harbinger of autumn, much as pumpkins are in the Midwest or iridescent leaves are in New England. “Fall is my</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.7</book-part-id>
                  <title-group>
                     <label>Three</label>
                     <title>THE BIRTH OF PEANUT BUTTER</title>
                  </title-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n 1891, following the Battle of Wounded Knee in South Dakota, the Sioux surrendered to the U.S. Army, marking the end of the Indian wars and the closing of the American frontier. Seven years later, the era of American imperialism began with the Spanish-American War. The doings of society’s upper crust in the Gay Nineties were chronicled by Edith Wharton and Henry James, although things weren’t always so gay for ordinary people: the Panic of 1893 led to an economic depression that lasted four years. If they had a little money, though, Americans could try several new comfort foods: the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.8</book-part-id>
                  <title-group>
                     <label>Four</label>
                     <title>PETER PAN:</title>
                     <subtitle>“IMPROVED BY HYDROGENATION”</subtitle>
                  </title-group>
                  <fpage>46</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n 1906 Upton Sinclair wrote<italic>The Jungle</italic>, an exposé of working conditions at Chicago meatpackers such as Swift &amp; Company. But like the other Chicago packers, Swift was expanding to other foods: two years earlier, it had bought H. C. Derby and its subsidiary E. K. Pond.¹ Originally a tripe and pigs’ feet shop, by 1914 Derby was making peanut butter,² and in 1920 it introduced the E. K. Pond brand.³</p>
                     <p>Peanut butter lore holds that Peter Pan, introduced in 1928 (or possibly as early as 1927),⁴ was born by renaming the E. K. Pond brand and hydrogenating it, but that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.9</book-part-id>
                  <title-group>
                     <label>Five</label>
                     <title>HOW PETER PAN LOST ITS GROOVE</title>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>
                        <italic>T</italic>he history of Peter Pan is the most difficult to trace of the three major brands,¹ as Swift and subsequent owners have practiced the dark arts of secrecy in a way designed to confound all but the most adroit corporate Sovietologists. We know that Swift bought Derby Foods and E .K. Pond in 1904, that by 1914 it was making peanut butter, and that Peter Pan dates to 1928. But for many of the fifty-five years it made Peter Pan, Swift kept its name at arm’s length from it.</p>
                     <p>News articles about and advertising for Peter Pan ordinarily refer to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.10</book-part-id>
                  <title-group>
                     <label>Six</label>
                     <title>SKIPPY:</title>
                     <subtitle>“HE MADE HIS FIRST JAR OF PEANUT BUTTER IN HIS GARAGE”</subtitle>
                  </title-group>
                  <fpage>72</fpage>
                  <abstract>
                     <p>
                        <italic>A</italic>lthough Skippy came on the market only five years after Peter Pan, it was a different business climate and a different world. After the stock market crash of October 1929, the Roaring Twenties gave way to the Great Depression, which hit bottom in 1933, the year Skippy was launched. Forty million Americans were plunged into poverty. Businessmen went from owning their own homes to renting rooms to becoming homeless. Okies like John Steinbeck’s Tom Joad saw their farms fall victim to massive dust storms and migrated to California to live hard lives recorded in<italic>The Grapes of Wrath</italic>. Down-on-their-luck people</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.11</book-part-id>
                  <title-group>
                     <label>Seven</label>
                     <title>SKIPPY ON TOP</title>
                  </title-group>
                  <fpage>84</fpage>
                  <abstract>
                     <p>
                        <italic>A</italic>fter World War II, Skippy cemented its leadership status in the peanut butter industry, chalking up $6 to $7.5 million in sales a year between 1947 and 1949.¹ Its sales reputedly exceeded the total of the next three nationally advertised brands combined,² which may have been Peter Pan, Heinz, and Beech-Nut. In the 1950s, Skippy advertised on the TV show<italic>You Asked for It</italic>and in the 1960s on<italic>Dennis the Menace</italic>and<italic>The Flintstones</italic>. Annette Funicello, famed as a Mouseketeer and the costar of agreeably frivolous beach movies with Frankie Avalon, served as a commercial spokesperson. But nothing says</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.12</book-part-id>
                  <title-group>
                     <label>Eight</label>
                     <title>JIF:</title>
                     <subtitle>“BUT IS IT STILL PEANUT BUTTER?”</subtitle>
                  </title-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n World War II, William T. Young served stateside as an officer in the ordnance branch of the U.S. Army, procuring and maintaining military supplies. After demobilization, Young, the son of a Lexington, Kentucky, dry-cleaning shop owner, wanted to start his own business but couldn’t get a bank loan. His father-in-law, a peanut sheller in Blakely, Georgia, offered to lend him $25,000 if he’d do something involving peanuts.¹ Young had studied mechanical engineering at the University of Kentucky and knew what equipment would be needed, so in 1946 he started his own peanut butter company, calling it Big Top.² Twelve</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.13</book-part-id>
                  <title-group>
                     <label>Nine</label>
                     <title>“CHOOSY MOTHERS CHOOSE …”</title>
                  </title-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>
                        <italic>D</italic>uring and after the Peanut Butter Case, Procter &amp; Gamble continued to grind out Jif in its Lexington plant at 767 East Third Street (renamed Winchester Road in the 1980s) and a satellite plant in Portsmouth, Virginia. After Jimmy Carter was elected president in 1976, Billie Jean Gibbons, then executive secretary to the Lexington plant manager, says there was a fuss when peanuts from his family farm came to the plant. “We would get peanuts in burlap bags,” she says. “There were tags on them that said ‘Carter’s’—everyone wanted one, just because he was, or had been, president.”¹</p>
                     <p>While the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.14</book-part-id>
                  <title-group>
                     <label>Ten</label>
                     <title>PEANUT BUTTER GOES INTERNATIONAL</title>
                  </title-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>
                        <italic>A</italic>lthough peanut butter is regarded as a uniquely American food, it has taken root in foreign soil, even if not enough to suit Procter &amp; Gamble.</p>
                     <p>Peanut butter got its start in the 1890s, at the same time as the American empire, which would eventually spread it around the world. Although countries such as Haiti and the Netherlands have their own distinctive peanut butter traditions, it’s mainly found overseas wherever there are American expatriates, the American military, and locals who deal with them. Germany, which is top-heavy with U.S. military bases, is the number-two importer of U.S. peanut butter.¹ Of people</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.15</book-part-id>
                  <title-group>
                     <label>Eleven</label>
                     <title>THE MUSIC OF PEANUT BUTTER</title>
                  </title-group>
                  <fpage>138</fpage>
                  <abstract>
                     <p>
                        <italic>B</italic>ecause peanut butter is so distinctively American, songs about it are woven into American pop culture, as are songs about peanuts. The latter category includes “Goober Peas,” the melodic Civil War–era invocation of camaraderie among Confederate soldiers, and Dizzy Gillespie’s bebop-style “Salt Peanuts.” “The Peanut Vendor” (“El Manisero”), a Cuban song in the rumba style, was popularized during the 1930s and 1940s by American musicians such as Stan Kenton. “Take Me Out to the Ballgame” encourages the listener to “buy me some peanuts and Cracker Jacks,” and even “The Ballad of the Boll Weevil” obliquely evokes peanuts, as it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.16</book-part-id>
                  <title-group>
                     <label>Twelve</label>
                     <title>DEAF SMITH:</title>
                     <subtitle>WHAT’S OLD-FASHIONED IS NEW AGAIN</subtitle>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n the early 1970s, the clean-cut, conservative High Plains residents of Deaf Smith County in the Texas Panhandle were treated to the sight of Volkswagen buses filled with hippies turning off Interstate 40 and heading straight for the county seat of Hereford. The town was named for the breed of cattle first brought to the area in 1898, but these visitors weren’t there for beef: they were looking for a start-up company called Arrowhead Mills and the organic foods it made, notably Deaf Smith peanut butter.</p>
                     <p>“We had hippies from New York and California coming to see the great guru</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.17</book-part-id>
                  <title-group>
                     <label>Thirteen</label>
                     <title>THE RISE AND FALL OF THE FLORUNNER</title>
                  </title-group>
                  <fpage>158</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n July 1970, Robert Choate Jr. told a Senate subcommittee that most breakfast cereals were nutritionally worthless.¹ The observations of Choate, who two years earlier had helped to set up a White House conference on food, nutrition, and health, caused observers to muse that a peanut butter and jelly sandwich might make a better breakfast for children. That December, the U.S. Supreme Court effectively ended a twelve-year battle between the Food and Drug Administration and the peanut butter industry when it declined to reject the FDA standard that peanut butter had to contain at least 90 percent peanuts. A year</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.18</book-part-id>
                  <title-group>
                     <label>Fourteen</label>
                     <title>THE PEANUT BUTTER CRISIS OF 1980</title>
                  </title-group>
                  <fpage>166</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n the fall of 2011, severe drought conditions across the South (coupled with the decision of farmers to plant more cotton) produced an unusually small peanut crop. This caused the price of peanut butter to spike. “Time to open the Strategic National Peanut Butter Reserve,” one wag posted on the Internet. For peanut industry veterans, it brought to mind another dark year in peanut butter annals—1980.</p>
                     <p>In late August of that year, Roger Knapp trudged through the sun-baked peanut fields of southern Georgia. Normally that late in the growing season, peanut plants from different rows would have merged into</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.19</book-part-id>
                  <title-group>
                     <label>Fifteen</label>
                     <title>“YOU MEAN IT’S NOT GOOD FOR ME?”</title>
                  </title-group>
                  <fpage>175</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n the late 1980s, peanut butter’s abiding popularity was demonstrated by the appearance of stores exclusively devoted to selling peanut-butter-related products. There was Peanut Butter Fantasies in Boston’s historic Fanueil Hall, Peanut Butter and Jelly, Inc., and Goin’ Nuts in New York City, and Trombley’s in Woburn, Massachusetts. But as Americans grew increasingly health conscious, there were dark clouds on the horizon for peanut butter, with increasing public worry about aflatoxin, peanut allergies, fat, trans fats, and even choking. People began to say, “You mean it’s not good for me?” and peanut butter sales fell off.</p>
                     <p>
                        <italic>Consumer Reports</italic>had been</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.20</book-part-id>
                  <title-group>
                     <label>Sixteen</label>
                     <title>THE SHORT, HAPPY LIFE OF SORRELLS PICKARD</title>
                  </title-group>
                  <fpage>189</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n Hollywood, film and TV actors often say, “But I really want to direct.” Herb Dow, a successful film editor and executive with an easygoing, no-nonsense manner and the cherubic face of a 1950s TV-sitcom dad, had a different dream: He really wanted to have his own peanut butter company. His dream came true, but not in the way he probably imagined.</p>
                     <p>In the early 1980s, Dow and his wife ate dinner on Friday nights at the Magic Pan, part of a chain of restaurants specializing in crepes, in Woodland Hills in the San Fernando Valley of Los Angeles. “We</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.21</book-part-id>
                  <title-group>
                     <label>Seventeen</label>
                     <title>PEANUT CORPORATION OF AMERICA:</title>
                     <subtitle>“THERE WAS NO RED FLAG”</subtitle>
                  </title-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>
                        <italic>S</italic>hirley Almer owned a bowling alley in Wadena, Minnesota. A strong, resilient woman, the seventy-two-year-old Almer had beaten lung cancer and a brain tumor, fighting back to regain the use of her limbs and speech (figure 17.1). In December 2008 she entered a nursing home in nearby Brainerd to recover from a urinary tract infection.¹ While there, she started buying Christmas presents and was planning to get a puppy.</p>
                     <p>But during what was expected to be a quick and easy convalescence, she began complaining of stomach cramps and had diarrhea.² She went into a downward spiral, and on the day</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.22</book-part-id>
                  <title-group>
                     <label>Eighteen</label>
                     <title>PEANUT BUTTER SAVES THE WORLD</title>
                  </title-group>
                  <fpage>210</fpage>
                  <abstract>
                     <p>
                        <italic>I</italic>n the case of the Peanut Corporation of America, peanut butter took life. But for starving children in sub-Saharan Africa and elsewhere in the Third World, it represents a second chance at life.</p>
                     <p>Peanut-butter-based pastes, known as ready-to-use therapeutic foods (RUTFs), consist of peanut butter with milk and sugar powders and a bit of vegetable oil, enriched with vitamins and minerals. A three-ounce serving of Plumpy’Nut, the best-known of these pastes, has 500 calories and a lot of protein in addition to the vitamins and minerals.</p>
                     <p>Third World children starve because their mothers can’t produce enough milk for them or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.23</book-part-id>
                  <title-group>
                     <label>Nineteen</label>
                     <title>WHERE ARE THE PEANUT BUTTERS OF YESTERYEAR?</title>
                  </title-group>
                  <fpage>220</fpage>
                  <abstract>
                     <p>
                        <italic>T</italic>oday Jif, Skippy, and Peter Pan dominate a highly concentrated peanut butter market, with Smart Balance and Planters rounding out the top tier. Many brands have fallen by the wayside in the more than 100 years since peanut butter was first developed, including regional stalwarts Dr. Schindler’s (Baltimore and Washington, D.C.), Robb Ross (Sioux City, Iowa), Toner’s Radiant Roast (Denver), Meadors Old Timey (South Carolina), and even the namesake brand of peanut butter pioneer George Bayle (St. Louis).</p>
                     <p>Nine hundred million dollars’ worth of peanut butter was sold in 2008,¹ with Americans eating 1.1 billion pounds of peanut butter in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.24</book-part-id>
                  <title-group>
                     <title>Appendix One</title>
                     <subtitle>AUTHOR’S RECOMMENDATIONS</subtitle>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.25</book-part-id>
                  <title-group>
                     <title>Appendix Two</title>
                     <subtitle>PEANUT BUTTER TIME LINE</subtitle>
                  </title-group>
                  <fpage>237</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.26</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.27</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>283</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">kram16232.28</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>299</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
