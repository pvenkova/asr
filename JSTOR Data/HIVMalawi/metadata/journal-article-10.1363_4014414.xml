<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">intsexrephea</journal-id>
         <journal-title-group>
            <journal-title content-type="full">International Perspectives on Sexual and Reproductive Health</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Guttmacher Institute</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19440391</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19440405</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">intsexrephea.40.3.144</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.1363/4014414</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>ARTICLES</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Contraceptive Method Skew and Shifts in Method Mix In Low- and Middle-Income Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Bertrand</surname>
                  <given-names>Jane T.</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Sullivan</surname>
                  <given-names>Tara M.</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Knowles</surname>
                  <given-names>Ellen A.</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Zeeshan</surname>
                  <given-names>Muhammad F.</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Shelton</surname>
                  <given-names>James D.</given-names>
               </string-name>
            </contrib>
            <bio>
               <p>Jane Bertrand is professor and chair, and at the time this research was conducted, Ellen A. Knowles was research assistant—both with the Department of Global Health Systems and Development, Tulane University School of Public Health and Tropical Medicine, New Orleans, LA, USA. Tara M. Sullivan is project director, Knowledge for Health Project, Center for Communication Programs, Johns Hopkins Bloomberg School of Public Health, Baltimore, MD, USA. At the time this research was conducted, Muhammad F. Zeeshan was doctoral student, The Ohio State University College of Public Health, Columbus, OH, USA. James D. Shelton is an independent consultant.</p>
               <p>
                  <bold>Author contact:</bold>
                  <email xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="mailto:bertrand@tulane.edu">bertrand@tulane.edu</email>
               </p>
            </bio>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>9</month>
            <year>2014</year>
            <string-date>September 2014</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">intsexrephea.40.issue-3</issue-id>
         <fpage>144</fpage>
         <lpage>153</lpage>
         <permissions>
            <copyright-statement>© 2014 Guttmacher Institute</copyright-statement>
            <copyright-year>2014</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/10.1363/4014414"/>
         <abstract>
            <p>
               <bold>CONTEXT:</bold>Method mix—the percentage distribution of contraceptive users in a given country, by method—is one measure that reflects the availability of a range of contraceptive methods. A skewed method mix—one in which 50% or more of contraceptive users rely on a single method—could be cause for concern as a sign of insufficiency of alternative methods or provider bias. Shifts in method mix are important to individual countries, donors and scholars studying contraceptive dynamics.</p>
            <p>
               <bold>METHODS:</bold>To determine current patterns and recent changes in method mix, we examined 109 low- and middle-income countries. A variety of statistical methods were used to test four factors as correlates of skewed method mix: geographic region, family planning program effort index, modern contraceptive prevalence rate and human development index. An assessment of changes in reliance on female and male sterilization, the IUD, the implant and the injectable was conducted for countries with available data.</p>
            <p>
               <bold>RESULTS:</bold>Of the 109 countries included in this analysis, 30% had a skewed method mix—a modest decrease from 35% in a 2006 analysis. Only geographic region showed any correlation with method skew, but it was only marginally significant. The proportion of users relying on female sterilization, male sterilization or the IUD decreased in far more countries than it increased; the pattern was reversed for the injectable.</p>
            <p>
               <bold>CONCLUSION:</bold>Method mix skew is not a definitive indicator of lack of contraceptive choice or provider bias; it may instead reflect cultural preferences. In countries with a skewed method mix, investigation is warranted to identify the cause.</p>
         </abstract>
         <trans-abstract xml:lang="es">
            <p>
               <bold>RESUMEN</bold>
            </p>
            <p>
               <bold>Contexto:</bold>La mezcla de métodos—la distribución porcentual de usuarios de anticonceptivos por método en un determinado país—es una medida que refleja la disponibilidad de un conjunto diverso de métodos anticonceptivos. Una mezcla sesgada de métodos—una mezcla en la que el 50% o más de los usuarios de anticonceptivos dependen de un solo método—podría ser causa de preocupación en tanto signo de insuficiencia de métodos alternativos o de sesgo del proveedor. Los cambios en la mezcla de métodos son importantes para los países individuales, donantes y académicos que estudian la dinámica anticonceptiva.</p>
            <p>
               <bold>Métodos:</bold>Para determinar los patrones actuales y cambios recientes en la mezcla de métodos, se examinaron 109 países de bajos y medianos ingresos. Se utilizó una variedad de métodos estadísticos para probar cuatro factores como correlatos de la mezcla sesgada de métodos: región geográfica, índice de esfuerzo del programa de planificación familiar, tasa de prevalencia de uso de anticonceptivos modernos e índice de desarrollo humano. Se condujo un análisis de cambios en la dependencia en la esterilización femenina y masculina, DIU, implantes e inyectables para los países con datos disponibles.</p>
            <p>
               <bold>Resultados:</bold>De los 109 países incluidos en este análisis, el 30% tuvo una mezcla sesgada de métodos—una disminución modesta respecto al 35% obtenido en un análisis realizado en 2006. Solamente una región geográfica mostró alguna correlación con el sesgo de métodos, pero solo tuvo una significancia marginal. La proporción de usuarios que dependían de la esterilización femenina, la esterilización masculina o el DIU disminuyó en muchos más países que en los que aumentó; el patrón fue inverso en el caso de los inyectables.</p>
            <p>
               <bold>Conclusión:</bold>El sesgo en la mezcla de métodos no es un indicador definitivo de la falta de opciones anticonceptivas o del sesgo del proveedor; pero puede que refleje preferencias culturales. En países con una mezcla sesgada de métodos, se justifica la investigación para determinar la causa.</p>
         </trans-abstract>
         <trans-abstract xml:lang="fr">
            <p>
               <bold>RÉSUMÉ</bold>
            </p>
            <p>
               <bold>Contexte:</bold>L'éventail de méthodes—représentant les pourcentages de répartition, par méthode, des utilisatrices de la contraception dans un pays donné—est une mesure qui reflète la disponibilité d'une gamme de méthodes contraceptives. Une distribution asymétrique—dans laquelle 50% ou plus des utilisatrices pratiquent une même méthode—pourrait être préoccupante en ce qu'elle pourrait être signe d'insuffisance d'autres méthodes ou de biais des prestataires. Les variations de l'éventail comptent pour les pays individuels, les donateurs et les chercheurs qui étudient la dynamique contraceptive.</p>
            <p>
               <bold>Méthodes:</bold>Afin de déterminer les tendances actuelles et l'évolution récente de l'éventail de méthodes, 109 pays à revenu faible à intermédiaire ont été examinés. Différentes méthodes statistiques ont été utilisées pour tester quatre facteurs de corrélation d'éventail asymétrique: la région géographique, l'indice d'effort programmatique de planification familiale, le taux de prévalence de la contraception moderne et l'indice de développement humain. Les changements survenus au niveau du recours à la stérilisation féminine et masculine, au stérilet, aux implants et aux injectables ont été évalués pour les pays disposant de données pertinentes.</p>
            <p>
               <bold>Résultats:</bold>Trente pour cent des 109 pays soumis à l'analyse présentent un éventail de méthodes asymétrique—en légère baisse par rapport aux 35% observés en 2006. Seule la région géographique est en corrélation avec l'asymétrie, mais dans une mesure marginalement significative seulement. La proportion du recours à la stérilisation féminine, la stérilisation masculine ou au stérilet diminue dans un bien plus grand nombre de pays que celui des pays où elle augmente. La tendance est inverse pour l'injectable.</p>
            <p>
               <bold>Conclusion:</bold>L'asymétrie de l'éventail de méthodes n'est pas un indicateur définitif de manque de choix contraceptif ou de biais des prestataires; elle peut aussi refléter les préférences culturelles. Dans les pays présentant un éventail asymétrique, la recherche se justifie pour en identifier la cause.</p>
         </trans-abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Cleland J</string-name>
               </person-group>et al.,<article-title>Family planning: the unfinished agenda</article-title>,<source>
                  <italic>Lancet</italic>
               </source>,<year>2006</year>,<volume>368</volume>(<issue>9549</issue>):<fpage>1810</fpage>–<lpage>1827</lpage>.</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Bongaarts J</string-name>
               </person-group>et al.,<source>
                  <italic>Family Planning Programs for the 21st Century: Rationale and Design</italic>
               </source>,:<publisher-name>Population Council</publisher-name>,<year>2012</year>.</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="web">
               <collab>London Summit on Family Planning</collab>,<source>
                  <italic>Summaries of Commitments</italic>
               </source>,<year>2013</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.londonfamilyplanningsummit.co.uk/COMMITMENTS_090712.pdf">http://www.londonfamilyplanningsummit.co.uk/COMMITMENTS_090712.pdf</uri>&gt;, accessed Sept. 2, 2013.</mixed-citation>
         </ref>
         <ref id="ref4">
            <label>4.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hardee K</string-name>
               </person-group>et al.,<source>
                  <italic>Voluntary Family Planning Programs that Respect, Protect, and Fulfill Human Rights: A Conceptual Framework</italic>
               </source>,:<publisher-name>Futures Group</publisher-name>,<year>2013</year>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <label>5.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Kim YM</string-name>
               </person-group>et al.,<article-title>Promoting informed choice: evaluating a decisionmaking tool for family planning clients and providers in Mexico</article-title>,<source>
                  <italic>International Family Planning Perspectives</italic>
               </source>,<year>2005</year>,<volume>31</volume>(<issue>4</issue>):<fpage>162</fpage>–<lpage>171</lpage>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <label>6.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Jain AK</string-name>
               </person-group>et al.,<article-title>Evaluation of an intervention to improve quality of care in family planning programme in the Philippines</article-title>,<source>
                  <italic>Journal of Biosocial Science</italic>
               </source>,<year>2012</year>,<volume>44</volume>(<issue>1</issue>):<fpage>27</fpage>–<lpage>41</lpage>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <label>7.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>RamaRao S</string-name>
               </person-group>et al,<article-title>The link between quality of care and contraceptive use</article-title>,<source>
                  <italic>International Family Planning Perspectives</italic>
               </source>,<year>2003</year>,<volume>29</volume>(<issue>2</issue>):<fpage>76</fpage>–<lpage>83</lpage>.</mixed-citation>
         </ref>
         <ref id="ref8">
            <label>8.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Bessinger RE</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Bertrand JT</string-name>
               </person-group>,<article-title>Monitoring quality of care in family planning programs: a comparison of observations and client exit interviews</article-title>,<source>
                  <italic>International Family Planning Perspectives</italic>
               </source>,<year>2001</year>,<volume>27</volume>(<issue>2</issue>):<fpage>63</fpage>–<lpage>70</lpage>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <label>9.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Blanc AK</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Curtis SL</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Croft TN</string-name>
               </person-group>,<article-title>Monitoring contraceptive continuation: links to fertility outcomes and quality of care</article-title>,<source>
                  <italic>Studies in Family Planning</italic>
               </source>,<year>2002</year>,<volume>33</volume>(<issue>2</issue>):<fpage>127</fpage>–<lpage>140</lpage>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <label>10.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Bruce J</string-name>
               </person-group>,<article-title>Fundamental elements of the quality of care: a simple framework</article-title>,<source>
                  <italic>Studies in Family Planning</italic>
               </source>,<year>1990</year>,<volume>21</volume>(<issue>2</issue>):<fpage>61</fpage>–<lpage>91</lpage>.</mixed-citation>
         </ref>
         <ref id="ref11">
            <label>11.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Sullivan TM</string-name>
               </person-group>et al.,<article-title>Skewed contraceptive method mix: why it happens, why it matters</article-title>,<source>
                  <italic>Journal of Biosocial Science</italic>
               </source>,<year>2006</year>,<volume>38</volume>(<issue>4</issue>):<fpage>501</fpage>–<lpage>521</lpage>.</mixed-citation>
         </ref>
         <ref id="ref12">
            <label>12.</label>
            <mixed-citation publication-type="web">
               <collab>Reproductive Health Supplies Coalition</collab>,<source>
                  <italic>Contraceptive Implants: Product Brief Caucus on New and Underused Reproductive Health Technologies</italic>
               </source>,<year>2012</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.rhsupplies.org/fileadmin/user_upload/Caucus_on_New_RH_technologies/rhsc-brief-contraceptiveimplants_A4.pdf">http://www.rhsupplies.org/fileadmin/user_upload/Caucus_on_New_RH_technologies/rhsc-brief-contraceptiveimplants_A4.pdf</uri>&gt;, accessed Sept. 2, 2013.</mixed-citation>
         </ref>
         <ref id="ref13">
            <label>13.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Barnes J</string-name>
               </person-group>et al.,<source>
                  <italic>Total Market Initiatives for Reproductive Health</italic>
               </source>,:<publisher-name>Abt Associates</publisher-name>,<year>2012</year>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <label>14.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Sutherland EG</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Otterness C</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Janowitz B</string-name>
               </person-group>,<article-title>What happens to contraceptive use after injectables are introduced? An analysis of 13 countries</article-title>,<source>
                  <italic>International Perspectives on Sexual and Reproductive Health</italic>
               </source>,<year>2011</year>,<volume>37</volume>(<issue>4</issue>):<fpage>202</fpage>–<lpage>208</lpage>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <label>15.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Darroch JE</string-name>
               </person-group>,<article-title>Trends in contraceptive use</article-title>,<source>
                  <italic>Contraception</italic>
               </source>,<year>2013</year>,<volume>87</volume>(<issue>3</issue>):<fpage>259</fpage>–<lpage>263</lpage>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <label>16.</label>
            <mixed-citation publication-type="web">
               <collab>World Bank</collab>,<source>
                  <italic>Data: Country and Lending Groups</italic>
               </source>,<year>2012</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.data.worldbank.org/about/country-classifications/country-and-lendinggroups">http://data.worldbank.org/about/country-classifications/country-and-lendinggroups</uri>&gt;, accessed Aug. 14, 2014.</mixed-citation>
         </ref>
         <ref id="ref17">
            <label>17.</label>
            <mixed-citation publication-type="web">
               <collab>World Bank</collab>,<source>
                  <italic>Data: Population Size</italic>
               </source>,<year>2011</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.data.worldbank.org">http://data.worldbank.org</uri>&gt;, accessed Aug. 14, 2014.</mixed-citation>
         </ref>
         <ref id="ref18">
            <label>18.</label>
            <mixed-citation publication-type="web">
               <collab>United Nations, Department of Economic and Social Affairs, Population Division</collab>,<source>
                  <italic>World Contraceptive Use 2012</italic>
               </source>,<year>2012</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.un.org/esa/population/publications/WCU2012/MainFrame.html">http://www.un.org/esa/population/publications/WCU2012/MainFrame.html</uri>&gt;, accessed Aug. 14, 2014.</mixed-citation>
         </ref>
         <ref id="ref19">
            <label>19.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Fabic MS</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Choi Y</string-name>
               </person-group>,<article-title>Assessing the quality of data regarding use of the lactational amenorrhea method</article-title>,<source>
                  <italic>Studies in Family Planning</italic>
               </source>,<year>2013</year>,<volume>44</volume>(<issue>2</issue>):<fpage>205</fpage>–<lpage>221</lpage>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <label>20.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Arévalo M</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Jennings V</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Sinai I</string-name>
               </person-group>,<article-title>Efficacy of a new method of family planning: the Standard Days Method</article-title>,<source>
                  <italic>Contraception</italic>
               </source>,<year>2002</year>,<volume>65</volume>(<issue>5</issue>):<fpage>333</fpage>–<lpage>338</lpage>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <label>21.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Alkema L</string-name>
               </person-group>et al.,<article-title>National, regional, and global rates and trends in contraceptive prevalence and unmet need for family planning between 1990 and 2015: a systematic and comprehensive analysis</article-title>,<source>
                  <italic>Lancet</italic>
               </source>,<year>2013</year>,<volume>381</volume>(<issue>9878</issue>):<fpage>1642</fpage>–<lpage>1652</lpage>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <label>22.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Westoff CF</string-name>
               </person-group>et al.,<article-title>Contraception-abortion connections in Armenia</article-title>,<source>
                  <italic>DHS Analytical Studies</italic>
               </source>,:<publisher-name>ORC Macro</publisher-name>,<year>2002</year>, No. 6.</mixed-citation>
         </ref>
         <ref id="ref23">
            <label>23.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Janevic T</string-name>
               </person-group>et al.,<article-title>Individual and community level socioeconomic inequalities in contraceptive use in 10 newly independent states: a multilevel cross-sectional analysis</article-title>,<source>
                  <italic>International Journal for Equity in Health</italic>
               </source>,<year>2012</year>,<volume>11</volume>(<issue>1</issue>):<fpage>69</fpage>.</mixed-citation>
         </ref>
         <ref id="ref24">
            <label>24.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Kragelund Nielsen K</string-name>
               </person-group>et al.,<article-title>Key barriers to the use of modern contraceptives among women in Albania: a qualitative study</article-title>,<source>
                  <italic>Reproductive Health Matters</italic>
               </source>,<year>2012</year>,<volume>20</volume>(<issue>40</issue>):<fpage>158</fpage>–<lpage>165</lpage>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <label>25.</label>
            <mixed-citation publication-type="book">
               <collab>Health Policy Project</collab>,<source>
                  <italic>Achieving the MDGs: the Contribution of Family Planning: Mauritania</italic>
               </source>,:<publisher-name>Futures Group</publisher-name>,<year>2011</year>.</mixed-citation>
         </ref>
         <ref id="ref26">
            <label>26.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Potts M</string-name>
               </person-group>et al., Niger: too little, too late,<source>
                  <italic>International Perspectives on Sexual and Reproductive Health</italic>
               </source>,<year>2011</year>,<volume>37</volume>(<issue>2</issue>):<fpage>95</fpage>–<lpage>101</lpage>.</mixed-citation>
         </ref>
         <ref id="ref27">
            <label>27.</label>
            <mixed-citation publication-type="book">
               <collab>Guttmacher Institute</collab>,<article-title>Facts on barriers to contraceptive use in the Philippines</article-title>,<source>
                  <italic>In Brief</italic>
               </source>,:<publisher-name>Guttmacher Institute</publisher-name>,<year>2010</year>.</mixed-citation>
         </ref>
         <ref id="ref28">
            <label>28.</label>
            <mixed-citation publication-type="book">
               <collab>USAID Africa Bureau</collab>et al.,<source>
                  <italic>Three Successful Sub-Saharan Africa Family Planning Programs: Ethiopia, Malawi, Rwanda</italic>
               </source>,:<publisher-name>USAID</publisher-name>,<year>2012</year>.</mixed-citation>
         </ref>
         <ref id="ref29">
            <label>29.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Van Lith LM</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Yahner M</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Bakamjianc L</string-name>
               </person-group>,<article-title>Women's growing desire to limit births in sub-Saharan Africa: meeting the challenge</article-title>,<source>
                  <italic>Global Health: Science and Practice</italic>
               </source>,<year>2013</year>,<volume>1</volume>(<issue>1</issue>):<fpage>97</fpage>–<lpage>107</lpage>.</mixed-citation>
         </ref>
         <ref id="ref30">
            <label>30.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hobstetter M</string-name>
               </person-group>et al.,<source>
                  <italic>Separated by Borders, United in Need: An Assessment of Reproductive Health on the Thailand-Burma Border</italic>
               </source>,:<publisher-name>Ibis Reproductive Health</publisher-name>,<year>2012</year>.</mixed-citation>
         </ref>
         <ref id="ref31">
            <label>31.</label>
            <mixed-citation publication-type="book">
               <collab>Taw NP</collab>,<source>
                  <italic>Country Report on 2007 Fertility and Reproductive Health Survey</italic>
               </source>,:<publisher-name>Union of Myanmar, Ministry of Immigration and Population, Department of Population and United Nations Population Fund (UNFPA)</publisher-name>,<year>2009</year>.</mixed-citation>
         </ref>
         <ref id="ref32">
            <label>32.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Thompson S</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Mercer MA</string-name>
               </person-group>,<source>
                  <italic>Integrating Child Spacing with Maternal Care in Timor-Leste</italic>
               </source>,:<publisher-name>Health Alliance International (HAI)</publisher-name>,<year>2009</year>.</mixed-citation>
         </ref>
         <ref id="ref33">
            <label>33.</label>
            <mixed-citation publication-type="book">
               <collab>EngenderHealth</collab>,<source>
                  <italic>Contraceptive Sterilization: Global Issues and Trends</italic>
               </source>,:<publisher-name>EngenderHealth</publisher-name>,<year>2002</year>.</mixed-citation>
         </ref>
         <ref id="ref34">
            <label>34.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Singh A</string-name>
               </person-group>et al.,<article-title>Sterilization regret among married women in India: implications for the Indian national family planning program</article-title>,<source>
                  <italic>International Perspectives on Sexual and Reproductive Health</italic>
               </source>,<year>2012</year>,<volume>38</volume>(<issue>4</issue>):<fpage>187</fpage>–<lpage>195</lpage>.</mixed-citation>
         </ref>
         <ref id="ref35">
            <label>35.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Potter JE</string-name>
               </person-group>,<article-title>The persistence of outmoded contraceptive regimes: the cases of Mexico and Brazil</article-title>,<source>
                  <italic>Population and Development Review</italic>
               </source>,<year>1999</year>,<volume>25</volume>(<issue>4</issue>):<fpage>703</fpage>–<lpage>739</lpage>.</mixed-citation>
         </ref>
         <ref id="ref36">
            <label>36.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Kraft JM</string-name>
               </person-group>et al.,<article-title>Use of dual protection in Botswana</article-title>,<source>
                  <italic>Studies in Family Planning</italic>
               </source>,<year>2009</year>,<volume>40</volume>(<issue>4</issue>):<fpage>319</fpage>–<lpage>328</lpage>.</mixed-citation>
         </ref>
         <ref id="ref37">
            <label>37.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Aveyard R</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Alfred A</string-name>
               </person-group>,<source>
                  <italic>Understanding Knowledge, Attitudes, Beliefs, and Practices Around Reproductive, Maternal, Neonatal, and Child Health in South Sudan</italic>
               </source>,:<publisher-name>BBC Media Action</publisher-name>,<year>2013</year>.</mixed-citation>
         </ref>
         <ref id="ref38">
            <label>38.</label>
            <mixed-citation publication-type="web">
               <collab>Government of South Sudan and Extending Service Delivery Project</collab>,<source>
                  <italic>Reproductive Health/Family Planning Service Provision for Returning Populations to South Sudan: Assessment Findings &amp; Recommendations</italic>
               </source>,<year>2006</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.esdproj.org/site/DocServer/Sth_Sudan_Situation_Atssessment_Report_-_FINAL_09-25-06__.pdf?docID=321">http://www.esdproj.org/site/DocServer/Sth_Sudan_Situation_Atssessment_Report_-_FINAL_09-25-06__.pdf?docID=321</uri>&gt;, accessed Aug. 14, 2014.</mixed-citation>
         </ref>
         <ref id="ref39">
            <label>39.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Jacobstein R</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Stanley H</string-name>
               </person-group>,<article-title>Contraceptive implants: providing better choice to meet growing family planning demand</article-title>,<source>
                  <italic>Global Health: Science and Practice</italic>
               </source>,<year>2013</year>,<volume>1</volume>(<issue>1</issue>):<fpage>11</fpage>–<lpage>17</lpage>.</mixed-citation>
         </ref>
         <ref id="ref40">
            <label>40.</label>
            <mixed-citation publication-type="book">
               <collab>Ministry of Health, Kingdom of Morocco and the EVALUATION Project/Tulane University</collab>,<source>
                  <italic>The Dynamics of the Moroccan Family Planning Program</italic>
               </source>,:<publisher-name>The EVALUATION Project</publisher-name>,<year>1998</year>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
