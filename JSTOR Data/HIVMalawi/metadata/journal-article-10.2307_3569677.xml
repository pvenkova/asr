<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">inteaffaroyainst</journal-id>
         <journal-id journal-id-type="jstor">j100185</journal-id>
         <journal-title-group>
            <journal-title>International Affairs (Royal Institute of International Affairs 1944-)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00205850</issn>
         <issn pub-type="epub">14682346</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3569677</article-id>
         <title-group>
            <article-title>The European Contribution to Global Environmental Governance</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>John</given-names>
                  <surname>Vogler</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">81</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i282856</issue-id>
         <fpage>835</fpage>
         <lpage>850</lpage>
         <page-range>835-850</page-range>
         <permissions>
            <copyright-statement>Copyright 2005 The Royal Institute of International Affairs</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3569677"/>
         <abstract>
            <p>The European Union has become an increasingly central player in international environmental politics. Its role, especially as a protagonist to the United States, has been highlighted by the way in which it successfully led the campaign for ratification of the Kyoto Protocol. The 2005 UK presidency has made climate change one of its twin priorities along with African development, and it is with this in mind that the article discusses the way in which the Union can be considered an international environmental actor in its own right and the various contributions that it makes to global environmental governance. While the EU is well known as a trade actor the complexities of its role as an environmental actor, operating under shared competence between the member states and the Community, are less well understood. Despite the inherent difficulties it has been surprisingly effective, although in areas such as climate change there is a need for strong presidential leadership. The EU's most evident field of activity has concerned the many multilateral environmental agreements in which it has come to play a leading role. However, this does not exhaust its contribution to global environmental governance that extends to the dissemination of norms and the incorporation of partners in its accession and neighbourhood policies. Sustainable development is also a key area of internal and external Union endeavour at the WTO and elsewhere, although there are continuing contradictions arising from its agricultural and fisheries policies. Finally, the Union's credibility will rest upon its ability to implement its environmental commitments and this is nowhere more evident than in its new emissions trading system. This is the centrepiece of the EU's commitment to the Kyoto Protocol and it is the need to co-ordinate the Union's diplomacy in the extension of the climate change regime, to include the United States and the developing countries, that the UK presidency must address.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1240e122a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1240e129" publication-type="other">
Tony Blair, speech on the Ioth anniversary of the Prince of Wales Business and Environment
Programme, 14 Sept. 2004, http://www.numberlo.gov.uk, last accessed 26Jan. 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e139a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1240e146" publication-type="other">
Defra, Delivering the essentials of life: Defra'sfive year strategy, Cm. 6411 (London: HMSO, 2004), p. 54.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e153a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1240e160" publication-type="book">
United Nations Economic and Social Council, Implementing Agenda 21: report of the Secretary General,
E/CN.17/2002/PC.2 (New York: United Nations, 2002), p. 233.<person-group>
                     <string-name>
                        <surname>United Nations Economic</surname>
                     </string-name>
                  </person-group>
                  <fpage>233</fpage>
                  <source>Implementing Agenda 21: report of the Secretary General</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e189a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1240e196" publication-type="book">
Joy Hyvarinen and Duncan Brack, Global environmental
institutions: analysis and options for change (London: RIIA, Energy and Environmental Programme, 2000).<person-group>
                     <string-name>
                        <surname>Hyvarinen</surname>
                     </string-name>
                  </person-group>
                  <source>Global environmental institutions: analysis and options for change</source>
                  <year>2000</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e222a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1240e229" publication-type="journal">
Matthew Paterson, David Humphreys and Lloyd Pettiford, 'Conceptualizing global
environmental governance: from interstate regimes to counter-hegemonic struggles', Global
Environmental Politics 3: 2, May 2003, pp. 1-10.<person-group>
                     <string-name>
                        <surname>Paterson</surname>
                     </string-name>
                  </person-group>
                  <issue>2</issue>
                  <fpage>1</fpage>
                  <volume>3</volume>
                  <source>Global Environmental Politics</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e267a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1240e274" publication-type="other">
In the Treaty Establishing the European Community (TEC), articles 130(r), (s) and (t).</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e281a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1240e288" publication-type="other">
British Petroleum, Statistical review of world energy 2004, http://www.bp.com/subsection.do, last accessed
10 July 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e298a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1240e305" publication-type="book">
Charlotte Bretherton and John Vogler,
The European Union as a global actor (London: Routledge, 1999).<person-group>
                     <string-name>
                        <surname>Bretherton</surname>
                     </string-name>
                  </person-group>
                  <source>The European Union as a global actor</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e329" publication-type="journal">
John Vogler, 'The European
Union as an actor in international environmental politics', Environmental Politics 8: 3, 1999, pp. 24-48.<person-group>
                     <string-name>
                        <surname>Vogler</surname>
                     </string-name>
                  </person-group>
                  <issue>3</issue>
                  <fpage>24</fpage>
                  <volume>8</volume>
                  <source>Environmental Politics</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e364a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1240e371" publication-type="book">
Vinod K. Aggarwal and Edward A. Fogarty, eds, EU trade
strategies between regionalism and globalism (Basingstoke: Palgrave Macmillan, 2004).<person-group>
                     <string-name>
                        <surname>Aggarwal</surname>
                     </string-name>
                  </person-group>
                  <source>EU trade strategies between regionalism and globalism</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e396a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1240e403" publication-type="journal">
European Commission, An EU-India strategic partnership, Com (2004) 430 final, p. 6.<person-group>
                     <string-name>
                        <surname>Commission</surname>
                     </string-name>
                  </person-group>
                  <fpage>6</fpage>
                  <volume>430</volume>
                  <source>Com</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e433a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1240e440" publication-type="other">
The Directorate-General for the Environment has 550 staff: one of its seven directorates is concerned
with international affairs and 60 staff are classified as being concerned with global environmental issues.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e450a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1240e457" publication-type="other">
European Court of Justice, Commission v. Council 22/70 (the ERTA case) established
that the adoption of internal rules by the Community implied the right to act externally as well.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e467a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1240e474" publication-type="other">
John Vogler, 'The external environmental policy of the European
Union', in Olav Schram Stokke</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e483" publication-type="book">
Oystein B. Thommessen, eds, Yearbook of International Cooperation
on Environment and Development 2003/4 (London: Fridtj of Nansen Institute/Earthscan, 2003), pp. 65-71.<person-group>
                     <string-name>
                        <surname>Thommessen</surname>
                     </string-name>
                  </person-group>
                  <fpage>65</fpage>
                  <source>Yearbook of International Cooperation on Environment and Development</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e512a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1240e519" publication-type="other">
Commission DG Environment, Multilateral Environmental Agreements to which the EC is a Contracting Party
or Signatory, 13 / 10/2003, http://www.europa.eu.int/comm./environment/international_issues/
agreements_en.htm, last accessed 5 July 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e532a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1240e539" publication-type="other">
In 1992</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e545" publication-type="other">
The 1998 presidency was preceded by the signature of the Kyoto
Protocol</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e554" publication-type="other">
John Prescott had been active as a member of the 'troika' in support of
the Luxembourg presidency.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e564" publication-type="book">
Riidiger K. W. Wurzel, The EU presidency: 'honest broker' or driving
seat? An Anglo-German comparison in the environmental policy field (London: Anglo-German Foundation for
the Study of Industrial Society, 2004).<person-group>
                     <string-name>
                        <surname>Wurzel</surname>
                     </string-name>
                  </person-group>
                  <source>The EU presidency: 'honest broker' or driving seat? An Anglo-German comparison in the environmental policy field</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e593a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1240e600" publication-type="other">
However, Council conclusions on mandates for
negotiations have to be approved in advance (by December if the meeting is scheduled to take place
before April in the subsequent year).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e612" publication-type="other">
Preparations will have to be made for a UNEP Governing Council
meeting in February 2006, as well as for CSD 14 in spring 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e623a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1240e630" publication-type="journal">
Jan Pronk, quoted in Earth Negotiations Bulletin, July 2001, p. 34.<person-group>
                     <string-name>
                        <surname>Pronk</surname>
                     </string-name>
                  </person-group>
                  <issue>July</issue>
                  <fpage>34</fpage>
                  <source>Earth Negotiations Bulletin</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e659a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1240e666" publication-type="book">
Richard E. Benedick, Ozone diplomacy: new
directions in safeguarding the planet (Cambridge, MA: Harvard University Press, 1991).<person-group>
                     <string-name>
                        <surname>Benedick</surname>
                     </string-name>
                  </person-group>
                  <source>Ozone diplomacy: new directions in safeguarding the planet</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e690" publication-type="other">
1987 Single European Act.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e697a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1240e704" publication-type="journal">
Michael Grubb and Farmina Yamin, 'Climate
collapse at The Hague: what happened and where do we go from here?', International Affairs 77: 2, April
2001, pp. 261-76;<object-id pub-id-type="jstor">10.2307/3025540</object-id>
                  <fpage>261</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e722" publication-type="book">
Hermann E. Ott, Warning signs from Delhi: troubled waters ahead for global climate policy
(Wuppertal: Institute for Climate Environment and Energy, 2002).<person-group>
                     <string-name>
                        <surname>Ott</surname>
                     </string-name>
                  </person-group>
                  <source>Warning signs from Delhi: troubled waters ahead for global climate policy</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e747a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1240e754" publication-type="book">
Louise van Schaik and Christian Egenhofer, Reform of the EU institutions:
implications for the EU's performance in climate change negotiations, CEPS policy brief no. 40 (Brussels:
Centre for European Policy Studies, Sept. 2003).<person-group>
                     <string-name>
                        <surname>van Schaik</surname>
                     </string-name>
                  </person-group>
                  <source>Reform of the EU institutions: implications for the EU's performance in climate change negotiations</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e783a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1240e790" publication-type="journal">
Ian
Manners, 'Normative power Europe: a contradiction in terms?', Journal of Common Market Studies 40: 2,
2002, pp. 235-58.<person-group>
                     <string-name>
                        <surname>Manners</surname>
                     </string-name>
                  </person-group>
                  <issue>2</issue>
                  <fpage>235</fpage>
                  <volume>40</volume>
                  <source>Journal of Common Market Studies</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e828a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1240e835" publication-type="journal">
European Commission, Ten years after Rio: preparing for the World Summit on Sustainable Development in
2002, Com 2001 53 final, 6.2.2001, p. 13.<person-group>
                     <string-name>
                        <surname>European Commission</surname>
                     </string-name>
                  </person-group>
                  <issue>2</issue>
                  <fpage>13</fpage>
                  <volume>6</volume>
                  <source>Ten years after Rio: preparing for the World Summit on Sustainable Development in 2002</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e871a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1240e878" publication-type="other">
OECD, Environmental review of Poland (Paris: OECD, 2003), http://www.oecd.org, last accessed I Sept.
2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e888a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1240e895" publication-type="journal">
Environmental Politics, 13: I, Spring 2004.<issue>I</issue>
                  <volume>13</volume>
                  <source>Environmental Politics</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e914a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1240e921" publication-type="journal">
Miranda Schreurs, 'Environmental protection in an expanding European
Community: lessons from past accessions', Environmental Politics 13: I, Spring 2004, pp. 27-5 I, p. 49.<person-group>
                     <string-name>
                        <surname>Schreurs</surname>
                     </string-name>
                  </person-group>
                  <issue>I</issue>
                  <fpage>27</fpage>
                  <volume>13</volume>
                  <source>Environmental Politics</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e956a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1240e963" publication-type="book">
Mirjam van Riesen with Tovar Camilo, The EU's
contribution to the Millennium Goals: special focus HIV/AIDS, a report by Alliance 2015 (The Hague: Alliance
2015, 2004).<person-group>
                     <string-name>
                        <surname>van Riesen</surname>
                     </string-name>
                  </person-group>
                  <source>The EU's contribution to the Millennium Goals: special focus HIV/AIDS, a report by Alliance 2015</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e991" publication-type="other">
environmental protection is no. 7.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e997" publication-type="other">
The
extent to which EU political commitments translate into action may be reflected in the 1.3% of the EU
2002 aid budget devoted to environmental projects (p. 9).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1010" publication-type="other">
Evaluation evidence for particular countries is
patchy. In Morocco there has been investment in the water sector;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1019" publication-type="other">
Malawi, 'a positive impact on the
management of natural resources, but less positive impact on environment as a horizontal issue'.
Bangladesh has been provided with environmental protection through the funding of infrastructure
projects. In South Africa and elsewhere the Commission has not undertaken an evaluation (p. 42).</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1035a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1240e1042" publication-type="book">
John McCormick, Environmental policy in the European Union (Basingstoke: Palgrave Macmillan, 2001),
pp. 75-86, counts no fewer than 14 EU environmental policy principles enshrined in the treaties.<person-group>
                     <string-name>
                        <surname>McCormick</surname>
                     </string-name>
                  </person-group>
                  <fpage>75</fpage>
                  <source>Environmental policy in the European Union</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1071a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1240e1078" publication-type="book">
Birnie and Alan Boyle, International law and the environment, 2nd edn (Oxford: Oxford University
Press, 2002), pp. 116-17.<person-group>
                     <string-name>
                        <surname>Boyle</surname>
                     </string-name>
                  </person-group>
                  <edition>2</edition>
                  <fpage>116</fpage>
                  <source>International law and the environment</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1111a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1240e1118" publication-type="book">
Laurence Graff, 'The precautionary principle', in Christopher
Bail, Robert Falkner and Helen Marquard, eds, The Cartagena Protocol on Biosafety: reconciling trade in
biotechnology with environment and development? (London: RIIA/Earthscan, 2002), pp. 410-22.<person-group>
                     <string-name>
                        <surname>Graff</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The precautionary principle</comment>
                  <fpage>410</fpage>
                  <source>The Cartagena Protocol on Biosafety: reconciling trade in biotechnology with environment and development?</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1153a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1240e1160" publication-type="other">
Review of the EU Sustainable Development Strategy: UK
conclusions and recommendations, Oct. 2004, http://www.sustainable-development.gov.uk/wssd2/II.htm,
p. 3, last accessed 26 January 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1173a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1240e1180" publication-type="other">
Vogler, 'The external environmental policy of the European Union', p. 66.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1187a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1240e1194" publication-type="journal">
Simon Lightfoot and Jon Burchill, 'Green hope or
greenwash? The actions of the European Union at the World Summit on Sustainable Development',
Global Environmental Change 14, 2004, pp. 337-44.<person-group>
                     <string-name>
                        <surname>Lightfoot</surname>
                     </string-name>
                  </person-group>
                  <fpage>337</fpage>
                  <volume>14</volume>
                  <source>Global Environmental Change</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1229a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1240e1236" publication-type="other">
The WSSD simply accepted the Doha Declaration on trade so that the critical trade-aid-environment
conjunction did not appear on its agenda.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1246a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1240e1253" publication-type="other">
They are to be found at points 31-33</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1259" publication-type="other">
Doha Ministerial Declaration of 20 Nov. 2001: the
relationship between WTO rules and MEAs (point 3 I);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1268" publication-type="other">
TRIPs and eco-labelling (point 32);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1275" publication-type="book">
Fatoumata Jawara and Aileen Kwa, Behind the scenes at the WTO: the real world of international trade
negotiations (London: Zed, 2003), pp. 101-6.<person-group>
                     <string-name>
                        <surname>Jawara</surname>
                     </string-name>
                  </person-group>
                  <fpage>101</fpage>
                  <source>Behind the scenes at the WTO: the real world of international trade negotiations</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1305a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1240e1312" publication-type="other">
European Commission, MEAs: implementation of the Doha development agenda, public 25.03.2002, http://
europa.eu.int/com/trade/issues/global/environment/docs/097-02-DDA.MEAS.pdf, p. 5, last accessed
Io Jan. 2005.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1324" publication-type="book">
European Commission, DDA [Doha development agenda] paragraph 31(ii), MEAs:
information exchange and observer status: EC submission to the WTO, Brussels, Io Oct. 2002, ref. 440/02-
Rev2.<person-group>
                     <string-name>
                        <surname>European Commission</surname>
                     </string-name>
                  </person-group>
                  <source>DDA [Doha development agenda] paragraph 31(ii), MEAs: information exchange and observer status: EC submission to the WTO</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1353a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1240e1360" publication-type="book">
European Commission DDA paragraph 32( iii), labelling for environmental purposes: EC submission to the
CTE, Brussels, 3 March 2003, ref. I18/03-Revi.<person-group>
                     <string-name>
                        <surname>European Commission</surname>
                     </string-name>
                  </person-group>
                  <source>DDA paragraph 32( iii), labelling for environmental purposes: EC submission to the CTE</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1385a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1240e1392" publication-type="other">
General Affairs Council of April 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1399a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1240e1406" publication-type="book">
Hon. Margaret Beckett to the Agra-Europe Outlook Conference, London 2 March
2004.<person-group>
                     <string-name>
                        <surname>Hon</surname>
                     </string-name>
                  </person-group>
                  <source>Margaret Beckett to the Agra-Europe Outlook Conference</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1431a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1240e1438" publication-type="book">
Presidency Conclusions, European Council, Brussels, 20 and 21 March 2003, p. 54.<person-group>
                     <string-name>
                        <surname>Presidency Conclusions</surname>
                     </string-name>
                  </person-group>
                  <fpage>54</fpage>
                  <source>European Council</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1463a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1240e1470" publication-type="other">
UK Government, Review of the EU Sustainable Development Strategy: UK conclusions and recommendations,
p. 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1481a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1240e1488" publication-type="book">
Claire Godfrey, Stop the dumping! How EU agricultural subsidies are damaging
livelihoods in the developing world, briefing paper no. 3 I (Oxford: Oxfam, Oct. 2002).<person-group>
                     <string-name>
                        <surname>Godfrey</surname>
                     </string-name>
                  </person-group>
                  <source>Stop the dumping! How EU agricultural subsidies are damaging livelihoods in the developing world</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1513a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1240e1520" publication-type="other">
Rt Hon.
Margaret Beckett, 2 March 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1530a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1240e1537" publication-type="book">
Conclusions, 2592nd meeting of the Council of the EU (Agricultural and Fisheries), 21 June, 1057/
04 PECHE 227 ENV 347.<person-group>
                     <string-name>
                        <surname>Conclusions</surname>
                     </string-name>
                  </person-group>
                  <source>2592nd meeting of the Council of the EU</source>
                  <year>1057</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1562a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d1240e1569" publication-type="book">
Christoph Knill and Andrea Lenschow, eds, Implementing EU environmental policy: new directions and
old problems (Manchester: Manchester University Press, 2000).<person-group>
                     <string-name>
                        <surname>Knill</surname>
                     </string-name>
                  </person-group>
                  <source>Implementing EU environmental policy: new directions and old problems</source>
                  <year>2000</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1594a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1240e1601" publication-type="other">
Under Directive 2003/87/EC of 13 Oct. 2003, 'establishing a scheme for greenhouse gas emission
trading within the Community and amending Council Directive 96/61/EC'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1611a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1240e1618" publication-type="journal">
The Times,
15 Feb. 2005.<issue>15 Feb</issue>
                  <source>The Times</source>
                  <year>2005</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1638a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1240e1645" publication-type="book">
Thomas Legge and Christian Egenhofer, CEPS commentary: after Marrakech: the regionalization of the Kyoto
Protocol (Brussels: CEPS, 2001), p. 4.<person-group>
                     <string-name>
                        <surname>Legge</surname>
                     </string-name>
                  </person-group>
                  <fpage>4</fpage>
                  <source>CEPS commentary: after Marrakech: the regionalization of the Kyoto Protocol</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1674a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1240e1681" publication-type="other">
http://www.europa.int/comm/environment/interational_issues/
green_diplomacy_en, last accessed 26 Jan. 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e1691a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1240e1700" publication-type="other">
The agreement was finalized at the first post-enlargement EU-Russia summit in
May 2004.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1709" publication-type="book">
This involved an effective doubling of Russian domestic
natural gas prices to industrial users by 2010 (European Commission Trade 2004, 'Russia-WTO: EU-
Russia deal brings Russia a step closer to WTO membership', Brussels, 21 May 2004<comment content-type="section">This involved an effective doubling of Russian domestic natural gas prices to industrial users by 2010 (European Commission Trade 2004</comment>
                  <source>Russia-WTO: EU-Russia deal brings Russia a step closer to WTO membership</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1732" publication-type="other">
http://
www.europa.eu.int/comm/trade/issues/bilateral/countries/russia/pr2 504_en.htm. While denying
actual linkage, Russian President Putin was prepared to acknowledge that 'The European Union has
made concessions on some points during the negotiations on the WTO.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1747" publication-type="other">
the European Commission's delegation to Moscow, 'Press conference
following the European Union-Russia Summit 21 May 2004', http://www.delrus.cec.eu.int/en/news
584.htm.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e1759" publication-type="other">
On 22 October 2004 Commission President Prodi was able to welcome the Russian Duma's
ratification of the Protocol, which, he said, 'repays my Commission's long and patient efforts to tackle
global warming': European Commission, 'Declaration by President Prodi on the Duma's ratification of
the Kyoto Protocol, Brussels 22 Oct. 2004': http//www.europa. eu.nt/comm./external_relations/
Russia/intro/prodi 2204.htm, last accessed io Jan. 2005.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
