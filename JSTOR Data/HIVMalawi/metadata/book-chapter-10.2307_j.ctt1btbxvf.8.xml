<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7hbh3b</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1btbxvf</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Law</subject>
      </subj-group>
      <book-title-group>
         <book-title>Beyond Prison</book-title>
         <subtitle>The Fight to Reform Prison Systems around the World</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Othmani</surname>
               <given-names>Ahmed</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <role>with</role>
            <name name-style="western">
               <surname>Bessis</surname>
               <given-names>Sophie</given-names>
            </name>
         </contrib>
         <contrib contrib-type="translator" id="contrib3">
            <role>Translated from the French by</role>
            <name name-style="western">
               <surname>Garling</surname>
               <given-names>Marguerite</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>07</month>
         <year>2008</year>
      </pub-date>
      <isbn content-type="epub">9780857450586</isbn>
      <isbn content-type="epub">0857450581</isbn>
      <publisher>
         <publisher-name>Berghahn Books</publisher-name>
         <publisher-loc>New York; Oxford</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2002</copyright-year>
         <copyright-holder>Editions LA DÉCOUVERTE</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1btbxvf"/>
      <abstract abstract-type="short">
         <p>"This is an exceptional personal testimony and story of achievement - Ahmed Othmani tells of his own appalling treatment when in detention and how it informed and inspired a lifetime vocation to struggle for the rights of all prisoners everywhere. As the story demonstrates, Othmani is one of those rare individuals who moved from passion and conviction to effective action - he was responsible for the establishment of one of the world's most reliable and mature human rights organizations, in the field of penal reform, Penal Reform International (PRI). His untimely death in Morocco in 2004 deprived the cause of a passionate advocate, but the work goes on."[From the Preface]</p>
      </abstract>
      <counts>
         <page-count count="144"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Badinter</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>The world of prisons is one that Ahmed Othmani certainly knew well – not only because he spent nearly ten years of his life in Tunisian jails, but also because of his tireless combat, after leaving prison in 1979, to advance human rights around the world, particularly in the context of prison.</p>
                     <p>Ahmed Othmani’s journey is, in many respects, worthy of admiration. Born in 1943 into a semi-nomadic tribe in Tunisia when it was a French protectorate, he found himself very early on confronted with violence and solitude. The violence was that of the French military who, during the armed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.4</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Robinson</surname>
                           <given-names>Mary</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Journey of a Generation</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>I come from southern Tunisia, from the arid lands good only for rearing livestock; colonial agriculture took up the most fertile lands. From the early twentieth century, tribes – including my own – were dispossessed and driven back to the poorest land, which is the arid plains. During my childhood, the years of armed struggle for independence left their mark on me, the most active resistance being in my region, between Gafsa and Sidi Bou Zid. This was the land of the fighters known as<italic>fellaghas</italic>, who included several members of my extended family. Armed groups often stopped at our</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>From Revolution to the Defence of Freedom</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>We must hark back to the years in prison to understand how and why so many<italic>Perspectives</italic>militants changed course politically. During the 1970s, at the same time that our country was undergoing this transformation, the GEAST set off down a path of radicalisation which led to a populist impasse and impoverished its thinking. In the 1960s, it was more a movement of intellectuals, coming up with theories and attempting to analyse society but without any real constituency among the people. By contrast, during the 1970s it became a movement of protest with more popular backing.</p>
                     <p>At the university, the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Crime and Punishment</title>
                  </title-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>The issue of judicial fairness has always been a matter for debate in any organised society. The first rule of good justice lies in the principle of separation of powers. It is imperative that the judiciary should be independent of the executive and the legislature, the latter making laws but not responsible for applying them. Good justice also requires that everyone should be equal before the law. These two elements lie at the heart of the concept of the rule of law. To guarantee the independence and equity of the justice system, it is vital that due process should obey</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Prisons – a World Apart</title>
                  </title-group>
                  <fpage>49</fpage>
                  <abstract>
                     <p>The world prison population is approaching 9 million individuals. This figure – given by the United Nations – is approximate, insofar as we do not know the exact number of detainees in China. But, as each detainee is part of a family, the number of people directly affected by prison is much higher: at least triple the number of detainees. Another feature is that the overwhelming majority of the prison population is male, essentially young adult males in the prime of life. This aggravates the problems caused by promiscuity. Sexuality is a major problem in the prison context, and promiscuity</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Prison – a Caricature of Society</title>
                  </title-group>
                  <fpage>63</fpage>
                  <abstract>
                     <p>Prison is a caricature of society insofar as it reproduces it, but in quite particular circumstances. Prison forms a community governed by power, domination and submission, but also by positive social relations. What brings the world of prison closest to the real world is the depth of its poverty, all the more unbearable by reason of its exceptions. A lot of prisons, in fact, contain a wing for white-collar criminals. The rich enjoy relatively favourable conditions, either because they pay for them or because of their high social standing. They are rarely sentenced to long terms and are generally in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Do Prisoners Have Rights?</title>
                  </title-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>The aim of imprisonment is, of course, above all to protect society; but it is also the duty of legislators and administrators to guarantee the rights of everyone, including those who have broken the law and ended up in prison. The primary right is that of preserving someone’s physical and moral integrity, that’s to say, the right not to be tortured, humiliated or raped, and it is non-negotiable. Under international law, the only right prisoners are deprived of is freedom to move around. In theory, therefore, they may enjoy all the other rights. They should live in decent conditions that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Alternatives to Prison</title>
                  </title-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>How do you prevent crime? Sociologists, political pundits and criminologists will no doubt agree that the best prevention lies in building a society whose members have access to education, health-care and full employment, allowing them to fulfil their essential needs, both material and spiritual. For it is frustration that most often leads to crime. Bad examples should be avoided: we know how harmful the influence of television can be in glorifying violence and brute force. Today’s heroes are often brutal characters. The prevalence of violence in the media is bound to encourage its development.</p>
                     <p>Prevention also means adopting measures on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Reforming Prison</title>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>Reform of the penal system needs to be seen as an integral part of social reform, including as it does judicial reform, with prisons only part of a whole. But multiple points of resistance around the world are hindering many attempts at reform. These come in the first place from law-makers themselves, especially in democratic countries where parliamentarians are first and foremost politicians standing for election.</p>
                     <p>So, to make progress on reforms, the voters and decisionmakers first have to be won over, since political will is a prerequisite for any decision on reform. Moreover, the need to reform generally speaking</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1btbxvf.13</book-part-id>
                  <title-group>
                     <title>Postface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lellouche</surname>
                           <given-names>Simone Othmani</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>Ahmed Othmani was killed in a road accident on 8th December 2004 in Rabat, Morocco, after attending the first day of a conference on ‘Civil Society as a Means to Activate Reform in the Arab World’. This meeting, which brought together NGO representatives from across the Arab world, was due to present its recommendations to the G8 Forum the next day.</p>
                     <p>It was in August 2001 that Ahmed put together this book,<italic>Beyond Prison</italic>, with Sophie Bessis, and it was published in French by<italic>La Découverte</italic>in 2002. Even with the passing of time, it is still worth reading for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
