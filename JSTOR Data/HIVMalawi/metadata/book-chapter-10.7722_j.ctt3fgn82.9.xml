<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt3fgn82</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Language &amp; Literature</subject>
         <subject>Health Sciences</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Breaking the Silence</book-title>
         <subtitle>South African Representations of HIV/AIDS</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>GRÜNKEMEIER</surname>
               <given-names>ELLEN</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>07</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9781847010704</isbn>
      <isbn content-type="epub">9781782041924</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk; Rochester, NY</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Ellen Grünkemeier</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt3fgn82"/>
      <abstract abstract-type="short">
         <p>South Africa is one of the countries in the world most affected by HIV/AIDS, and yet, until recently, the epidemic was barely visible in South African literature. Much can be gained from approaching the South African epidemic through creative texts such as novels, photographs, films, cartoons and murals because they produce and circulate meanings of HIV/AIDS and its various facets such as its 'origin', 'transmission routes' and 'physical manifestations'. Other aspects explored are the denial of HIV/AIDS, its stigmatisation, discriminatory practices, modes of disclosure, access to anti-retroviral medication, as well as the role of alternative treatment. Creative texts, which are open to different and possibly contradictory readings, can serve as a starting point to increase the cultural visibility of the virus and to challenge dominant ideas about the epidemic. The cultural constructions of HIV/AIDS should be carefully examined because the meanings are pervasive and have very 'real' consequences: they play a powerful role both in determining which issues receive attention and in shaping public understanding of the virus. Ellen Grünkemeier is a lecturer and researcher in the English Department at Leibniz University of Hanover, Germany. Her publications include two co-edited volumes on postcolonial literatures and cultures, 'Listening to Africa. Anglophone African Literatures and Cultures' (2012), and 'Postcolonial Studies across the Disciplines' (ASNEL Papers 19, forthcoming).</p>
      </abstract>
      <counts>
         <page-count count="256"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction</title>
                     <subtitle>SETTING THE AGENDA</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>In her photographic series ‘From the Inside’ (2000-2002), the artist Sue Williamson addresses the South African epidemic by engaging with people living with HIV/AIDS who are willing to disclose their status and to pose for portrait shots. She has statements they make about the virus and its effects painted on walls in public places around Cape Town and Johannesburg, which she then also documents as photographs. These are then printed side by side with the portraits. John Masuku, quoted above, is one such subject of the series.¹ Both Masuku’s statement and his participation in the art project demonstrate his openness</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Mapping the Terrain</title>
                     <subtitle>THE SOUTH AFRICAN HIV/AIDS EPIDEMIC</subtitle>
                  </title-group>
                  <fpage>10</fpage>
                  <abstract abstract-type="extract">
                     <p>While HIV/AIDS is usually associated primarily with medicine, it is by no means limited to this field. Reaching out beyond medical concerns, HIV/AIDS cuts across discourses and involves a variety of aspects such as historical, economic, political, social, psychological, ethical and cultural issues. Highlighting the abundance of meanings, connotations and attributions generated around HIV/AIDS, Paula Treichler coined the frequently quoted phrase ‘epidemic of signification’ (Treichler 1999, 11). Bringing together the medically connoted term ‘epidemic’ with ‘signification’, which relates to issues of language and meaning, she outlines the broad context and significance HIV/AIDS has come to acquire. ‘The AIDS epidemic is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>HIV/AIDS as a Taboo Topic</title>
                     <subtitle>A CULTURE OF SILENCE</subtitle>
                  </title-group>
                  <fpage>29</fpage>
                  <abstract abstract-type="extract">
                     <p>HIV/AIDS has long been a taboo topic in South Africa; however, judging by available statistics (as problematic as they might be), the scope of the South African epidemic can hardly be overestimated. According to the UNAIDS <italic>World AIDS Day Report 2011</italic>, South Africa has the highest number of people living with HIV/AIDS in the world; approximately 5.6 million South Africans are HIV-positive (UNAIDS 2011, 7). Comparing the findings of national health surveys from 2002, 2005 and 2008, it becomes evident that the overall HIV prevalence has stabilised at approximately 11% (Shisana et al. 2009, 30).¹ Although variations between men and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Imagery</title>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract abstract-type="extract">
                     <p>While the term symbol has been used differently in various academic disciplines, the diverse definitions usually do share one criterion: a symbol is not a rhetorical figure but an object which represents something else (Cuddon 1999, 885; Peil 1998, 519). Unlike signs regarded as arbitrary social constructions that need to be decoded on the basis of cultural conventions, symbols are understood to be more suggestive (de Saussure 1983, 68). Informed by a structuralist approach, Jürgen Link defines symbol as a motivated sign in which a complex iconic signifier, called ‘pictura’, is mapped onto an abstract signified, referred to as ‘subscriptio’</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Myths</title>
                  </title-group>
                  <fpage>114</fpage>
                  <abstract abstract-type="extract">
                     <p>The apparently ‘sudden’ emergence of a ‘new’ and ‘fatal’ disease has given rise to mythifications that shape the social construction and perception of the pandemic. One of the most prominent examples is the naturalised association of the virus with homosexuality (see Chapter 2). Generally speaking, myth is a cultural construction through language, imbued with meanings, connotations and ideologies. In his text ‘Myth Today’, published in his well-known work <italic>Mythologies</italic> (1957), Roland Barthes explains that myth goes beyond the lexical meaning of a sign and refers to a broader meaning or meta-message. Contrary to the linguistic system in which the connection</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Literary Genres</title>
                  </title-group>
                  <fpage>163</fpage>
                  <abstract abstract-type="extract">
                     <p>In the mid-1990s, the publication of AIDS memoirs reached a peak (Demmer 2007, 295). With few exceptions, however, they were written by white US-American homosexual men who offered insight into the lives of people who were affected by what was then considered a terminal illness. With the introduction of antiretrovirals, these ‘chronicles of dying’ (ibid.) lost in relevance because the number of AIDS-related deaths was in decline in North America and Europe. In South Africa, however, the situation is different; the epidemic has long been a taboo topic and treatment is not as widely available. Exploring their lives with HIV/AIDS,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Afterword</title>
                     <subtitle>MEANINGS MATTER</subtitle>
                  </title-group>
                  <fpage>221</fpage>
                  <abstract abstract-type="extract">
                     <p>Playing off the theme ‘Breaking the Silence’ used for the Durban International AIDS Confence in 2000, the focus here has been on texts that address the virus more or less directly, thereby breaking with the pervasive culture of silence that, in turn, also upholds the secrecy and stigma associated with the infection. Functioning as what Jürgen Link calls interdiscourses, the texts reach beyond the purview of expert groups of medical doctors, biologists, sociologists or statisticians in that they circulate knowledge about HIV/AIDS in society as a whole. In so doing, they raise awareness of the infection among South Africans and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.12</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>224</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fgn82.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>249</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
