<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt17mcsk6</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt17mvh4w</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
         <subject>Anthropology</subject>
      </subj-group>
      <book-title-group>
         <book-title>UNESCO on the Ground</book-title>
         <subtitle>Local Perspectives on Intangible Cultural Heritage</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Foster</surname>
               <given-names>Michael Dylan</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Gilman</surname>
               <given-names>Lisa</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>23</day>
         <month>09</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780253019400</isbn>
      <isbn content-type="epub">9780253019530</isbn>
      <isbn content-type="epub">0253019532</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington; Indianapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Indiana University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt17mvh4w"/>
      <abstract abstract-type="short">
         <p>For nearly 70 years, the United Nations Educational, Scientific and Cultural Organization (UNESCO) has played a crucial role in developing policies and recommendations for dealing with intangible cultural heritage. What has been the effect of such sweeping global policies on those actually affected by them? How connected is UNESCO with what is happening every day, on the ground, in local communities? Drawing upon six communities ranging across three continents-from India, South Korea, Malawi, Japan, Macedonia and China-and focusing on festival, ritual, and dance, this volume illuminates the complexities and challenges faced by those who find themselves drawn, in different ways, into UNESCO's orbit. Some struggle to incorporate UNESCO recognition into their own local understanding of tradition; others cope with the fallout of a failed intangible cultural heritage nomination. By exploring locally, by looking outward from the inside, the essays show how a normative policy such as UNESCO's intangible cultural heritage policy can take on specific associations and inflections. A number of the key questions and themes emerge across the case studies and three accompanying commentaries: issues of terminology; power struggles between local, national and international stakeholders; the value of international recognition; and what forces shape selection processes. With examples from around the world, and a balance of local experiences with broader perspectives, this volume provides a unique comparative approach to timely questions of tradition and change in a rapidly globalizing world.</p>
      </abstract>
      <counts>
         <page-count count="188"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>[i]</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>[v]</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.3</book-part-id>
                  <title-group>
                     <title>UNESCO on the Ground</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Foster</surname>
                           <given-names>Michael Dylan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>These words are invoked by the United Nations Educational, Scientific, and Cultural Organization, commonly known as UNESCO, to define the term<italic>intangible cultural heritage</italic>or, in the current academicbureaucratic vernacular,<italic>ICH</italic>.¹ UNESCO’s language here is openended, if not vague, but clearly includes the sort of expressive culture long studied by folklorists. Significantly, the definition emphasizes recognition of ICH on the<italic>local</italic>level, by the “communities,” “groups,” and “individuals” involved with the practices, representations, expressions, knowledge, and skills under consideration. I note here the importance of the local in this definition, and indeed in much of UNESCO’s ICH discourse, because of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Voices on the Ground:</title>
                     <subtitle>Kutiyattam, UNESCO, and the Heritage of Humanity</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lowthorp</surname>
                           <given-names>Leah</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>Kutiyattam Sanskrit theater of Kerala state was recognized as India’s first UNESCO Masterpiece of the Oral and Intangible Heritage of Humanity in 2001. Looking back a decade later, how has UNESCO recognition impacted both the art and the lives of its artists? Based upon two years of ethnographic research from 2008 to 2010 among Kutiyattam artists in Kerala, India, this essay follows the art’s postrecognition trajectory through its increasing mediatization, institutionalization, and liberalization. Drawing on extended interviews with over fifty Kutiyattam actors, actresses, and drummers, it focuses on reclaiming the voices of affected artists on the ground.</p>
                     <p>Kerala is a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Economic Imperative of UNESCO Recognition:</title>
                     <subtitle>A South Korean Shamanic Ritual</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Yun</surname>
                           <given-names>Kyoim</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>41</fpage>
                  <abstract>
                     <p>This study examines a South Korean shamanic ritual to explore how UNESCO recognition complicates preexisting discourses concerning the interface of cultural and economic values in heritage making and heritage maintenance. Although UNESCO seldom discusses the economic issues associated with heritage status, financial factors are a key concern and a source of tension among local and national stakeholders when valorizing selected local culture for translocal use and recognition. Local perceptions and use of the global project are closely related to the goals and protocols of the 2003 ICH Convention.</p>
                     <p>The designated ritual comes from Cheju (Jeju) Island, located in Korea’s southern</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Demonic or Cultural Treasure?</title>
                     <subtitle>Local Perspectives on Vimbuza, Intangible Cultural Heritage, and UNESCO in Malawi</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gilman</surname>
                           <given-names>Lisa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>Vimbuza, one of two Malawian dance forms inscribed on the UNESCO Representative List of the Intangible Cultural Heritage of Humanity in 2008, is a healing ritual practiced by Tumbuka people in Malawi’s northern region. Vimbuza refers both to the ailments caused by Vimbuza spirits and the rituals used to heal people with these spiritinduced illnesses. Vimbuza healers diagnose and treat spirit-related illnesses in rituals that combine dress, drumming, singing, and movement. It is sometimes called a “traditional dance” and performed for entertainment rather than healing. Drawing from interviews with practitioners, the general public, and cultural sector professionals, this essay explores</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Imagined UNESCOs:</title>
                     <subtitle>Interpreting Intangible Cultural Heritage on a Japanese Island</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Foster</surname>
                           <given-names>Michael Dylan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>Toshidon is a “visiting deity” (<italic>raihōshin</italic>) ritual that takes place every New Year’s Eve on a small island off the southwest coast of Japan. Performed for purposes of education, Toshidon is an event in which groups of men, masked and costumed as demon-deity figures, walk from house to house frightening and disciplining children. In 2009, Toshidon was inscribed on the UNESCO Representative List, a significant occurrence for this relatively isolated community. Based on ongoing fieldwork on the island, this essay explores specific events and discourses that emerged from this recognition. I conclude that the UNESCO inscription becomes a floating signifier</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Macedonia, UNESCO, and Intangible Cultural Heritage:</title>
                     <subtitle>The Challenging Fate of Teškoto</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Silverman</surname>
                           <given-names>Carol</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>93</fpage>
                  <abstract>
                     <p>In Macedonia debates about heritage are played out along the fault lines of ethnic and religious conflict as well as a faltering economy and threats from neighbors about interpretations of history. The country’s 2002 and 2004 failed applications for a UNESCO Masterpiece of Intangible Cultural Heritage and its ongoing submissions of representative lists provide a valuable case study of how rural folklore symbols are selectively adopted into heritage discourse and elevated to iconic status. This essay analyzes Teškoto (the Heavy/Difficult Dance) as featured in two UNESCO Masterpiece applications as well as in village contexts, ensemble performances, an annual staged ritual,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Shifting Actors and Power Relations:</title>
                     <subtitle>Contentious Local Responses to the Safeguarding of Intangible Cultural Heritage in Contemporary China</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>You</surname>
                           <given-names>Ziying</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>113</fpage>
                  <abstract>
                     <p>This essay addresses the contentious local responses to intangible cultural heritage (ICH) protection in a local context. The following ethnographic case study concerns the living tradition of worshipping the ancient sage-kings Yao and Shun in several villages in Hongtong County, Shanxi Province, China. Named as an item of national ICH in 2008, the official title of this local tradition is Hongtong Zouqin Xisu, “the custom of visiting sacred relatives in Hongtong.” I explore the ways local people have responded to the safeguarding of ICH, with a focus on shifting actors and power relations within interconnected communities.</p>
                     <p>Hongtong is a county</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Understanding UNESCO:</title>
                     <subtitle>A Complex Organization with Many Parts and Many Actors</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Seeger</surname>
                           <given-names>Anthony</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>One of the great difficulties in evaluating the effectiveness of UNESCO intangible cultural heritage (ICH) programs at the local level is that the acronym (or “brand,” as Michael Dylan Foster suggests in his essay)<italic>UNESCO</italic>is widely used, but the particular actors to which the term is applied are rarely indicated. UNESCO is not monolithic, and it has a number of distinct components that range from centralized to extremely decentralized. The final text of the 2003 Convention on the Safeguarding of Intangible Cultural Heritage (hereafter just 2003 Convention), was hammered out during months of debate and reflects a complex consensus.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Learning to Live with ICH:</title>
                     <subtitle>Diagnosis and Treatment</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hafstein</surname>
                           <given-names>Valdimar Tr.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>In his<italic>Philosophical Investigations</italic>, Ludwig Wittgenstein (2009, 43) argues that “the meaning of a word is its use in the language.” In other words, Wittgenstein suggests, if we want to learn what a word means or how a sign signifies, we cannot rely on formal definitions; they are a dead end. Furthermore, it is fruitless to examine the external signifier to which our word points, and etymology will not help us grasp its meaning. Rather, the question of meaning is empirical. The only way to discover the meaning of a word is to go into the field, to observe how</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>From Cultural Forms to Policy Objects:</title>
                     <subtitle>Comparison in Scholarship and Policy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Noyes</surname>
                           <given-names>Dorothy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>This welcome edited collection is concerned not only with intangible cultural heritage (ICH), which has commandeered our field’s attention for the past decade, but with comparison, about which we really ought to talk more. Folklorists today rarely launch comparative inquiry top-down, from the etic categories of early genre theory. Nor has a comprehensive practice of bottom-up comparison emerged from the turn to emic categories and the ethnography of communication at the local level. Rather, the editors here have formalized and made explicit a third turn. Comparison today is apt to move sideways to a category of policy, intangible cultural heritage,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17mvh4w.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>177</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
