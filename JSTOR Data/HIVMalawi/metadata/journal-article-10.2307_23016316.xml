<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">intecompbiol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101203</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Integrative and Comparative Biology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">15407063</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15577023</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23016316</article-id>
         <article-categories>
            <subj-group>
               <subject>Bridging the Gap Between Ecoimmunology and Disease Ecology</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>From Host Immunity to Pathogen Invasion: The Effects of Helminth Coinfection on the Dynamics of Microparasites</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Vanessa O.</given-names>
                  <surname>Ezenwa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anna E.</given-names>
                  <surname>Jolles</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23016311</issue-id>
         <fpage>540</fpage>
         <lpage>551</lpage>
         <permissions>
            <copyright-statement>© 2011 The Society for Integrative and Comparative Biology</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:title="an external site"
                   xlink:href="http://dx.doi.org/10.1093/icb/icr058"/>
         <abstract>
            <p>Synopsis Concurrent infections with multiple parasites are ubiquitous in nature. Coinfecting parasites can interact with one another in a variety of ways, including through the host's immune system via mechanisms such as immune trade-offs and immunosuppression. These within-host immune processes mediating interactions among parasites have been described in detail, but how they scale up to determine disease dynamic patterns at the population level is only beginning to be explored. In this review, we use helminth—microparasite coinfection as a model for examining how within-host immunological effects may influence the ecological outcome of microparasitic diseases, with a specific focus on disease invasion. The current literature on coinfection between helminths and major microparasitic diseases includes many studies documenting the effects of helminths on individual host responses to microparasites. In many cases, the observed host responses map directly onto parameters relevant for quantifying disease dynamics; however, there have been few attempts at integrating data on individual-level effects into theoretical models to extrapolate from the individual to the population level. Moreover, there is considerable variability in the particular combination of disease parameters affected by helminths across different microparasite systems. We develop a conceptual framework identifying some potential sources of such variability: Pathogen persistence and severity, and resource availability to hosts. We also generate testable hypotheses regarding diseases and the environmental contexts when the effects of helminths on microparasite dynamics should be most pronounced. Finally, we use a case study of helminth and mycobacterial coinfection in the African buffalo to illustrate both progress and challenges in understanding the population-level consequences of within-host immunological interactions, and conclude with suggestions for future research that will help improve our understanding of the effects of coinfection on dynamics of infectious diseases.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1440e185a1310">
            <mixed-citation id="d1440e189" publication-type="other">
Abu-Raddad LJ, Patnaik P, Kublin JG. 2006. Dual infection
with HIV and malaria fuels the spread of both diseases in
sub-Saharan Africa. Science 314:1603-6.</mixed-citation>
         </ref>
         <ref id="d1440e202a1310">
            <mixed-citation id="d1440e206" publication-type="other">
Bradley JE, Jackson JA. 2008. Measuring immune system
variation to help understand host-pathogen community
dynamics. Parasitol 135:807-23.</mixed-citation>
         </ref>
         <ref id="d1440e219a1310">
            <mixed-citation id="d1440e223" publication-type="other">
Brady MJ, O'Neill SM, Dalton JP, Mills KHG. 1999. Fasciola
hepatica suppresses a protective Thl response against
Bordetella pertussis. Infect Immunol 67:5372-8.</mixed-citation>
         </ref>
         <ref id="d1440e236a1310">
            <mixed-citation id="d1440e240" publication-type="other">
Briand V, Watier L, Le Hesran JY, Garcia A, Cot M. 2005.
Coinfection with Plasmodium falciparum and Schistosoma
haematobium: protective effect of schistosomiasis on malar-
ia in Senegalese children. Am J Trop Med Hyg 72:702-7.</mixed-citation>
         </ref>
         <ref id="d1440e257a1310">
            <mixed-citation id="d1440e261" publication-type="other">
Brown M, Kizza M, Watera C, Quigley MA, Rowland S,
Hughes P, Whitworth JAG, Elliott AM. 2004. Helminth
infection is not associated with faster progression of HIV
disease in coinfected adults in Uganda. J Infect Dis
190:1869-79.</mixed-citation>
         </ref>
         <ref id="d1440e280a1310">
            <mixed-citation id="d1440e284" publication-type="other">
Brown M, Mawa PA, Kaleebu P, Elliott AM. 2006a.
Helminths and HIV infection: epidemiological observations
on immunological hypotheses. Paras Immunol 28:613-23.</mixed-citation>
         </ref>
         <ref id="d1440e297a1310">
            <mixed-citation id="d1440e301" publication-type="other">
Brown M, Miiro G, Nkurunziza P, Watera C, Quigley MA,
Dunne DW, Whitworth JAG, Elliott AM. 2006b.
Schistosoma mansoni, nematode infections, and progression
to active tuberculosis among HIV-1 infected Ugandans. Am
J Trop Med Hyg 74:819-25.</mixed-citation>
         </ref>
         <ref id="d1440e320a1310">
            <mixed-citation id="d1440e324" publication-type="other">
Bruce MC, Donnelly CA, Alpers MP, Galinski MR,
Barnwell JW, Walliker D, Day KP. 2000. Cross-species
interactions between malaria parasites in humans. Science
287:845-8.</mixed-citation>
         </ref>
         <ref id="d1440e340a1310">
            <mixed-citation id="d1440e344" publication-type="other">
Cattadori IM, Albert R, Boag B. 2007. Variation in host
susceptibility and infectiousness generated by co-infection:
the myxoma - Trichostrongylus retortaeformis case in wild
rabbits. I R Soc Interface 4:831-40.</mixed-citation>
         </ref>
         <ref id="d1440e360a1310">
            <mixed-citation id="d1440e364" publication-type="other">
Chandra RK. 1996. Nutrition, immunity and infection: From
basic knowledge of dietary manipulation of immune re-
sponses to practical application of ameliorating suffering
and improving survival. PNAS 93:14304-7.</mixed-citation>
         </ref>
         <ref id="d1440e381a1310">
            <mixed-citation id="d1440e385" publication-type="other">
Chen CC, Louie S, McCormick B, Walker WA, Shi HN. 2005.
Concurrent infection with an intestinal helminth parasite
impairs host resistance to enteric Citrobacter rodentium and
enhances Citrobacter-induced colitis in mice. Infect Immun
73:5468-81.</mixed-citation>
         </ref>
         <ref id="d1440e404a1310">
            <mixed-citation id="d1440e408" publication-type="other">
Co TR, Hirsch CS, Toossi Z, Dietze R, Ribeiro-Rodrigues R.
2007. Intestinal helminth coinfection has a negative impact
on both anti-Mycobacterium tuberculosis immunity and
clinical response to tuberculosis therapy. Clin Exp
Immunol 147:45-52.</mixed-citation>
         </ref>
         <ref id="d1440e427a1310">
            <mixed-citation id="d1440e431" publication-type="other">
Diniz LM, Magalhaes EFL, Pereira FEL, Dietze R, Ribeiro-
Rodrigues R. 2010. Presence of intestinal helminths de-
creases T helper type 1 responses in tuberculoid leprosy
patients and may increase the risk for multi-bacillary lep-
rosy. Clin Exp Immunol 161:142-50.</mixed-citation>
         </ref>
         <ref id="d1440e450a1310">
            <mixed-citation id="d1440e454" publication-type="other">
Edwards MJ, Buchatska O, Ashton M, Montoya M,
Bickle QD, Borrow P. 2005. Reciprocal immunomodulation
in a Schistosome and hepatotropic virus coinfection model.
] Immunol 175:6275-85.</mixed-citation>
         </ref>
         <ref id="d1440e470a1310">
            <mixed-citation id="d1440e474" publication-type="other">
Elias D, Akuffo H, Thors C, Pawlowski A, Britton S. 2005.
Low dose chronic Schistosoma mansoni infection increases
susceptibility to Mycobacterium bovis BCG infection in
mice. Clin Exp Immunol 139:398-404.</mixed-citation>
         </ref>
         <ref id="d1440e490a1310">
            <mixed-citation id="d1440e494" publication-type="other">
Elliott AM, Mawa PA, Joseph S, Namujju PB, Kizza M,
Nakiyingi JS, Watera C, Dunne DW, Whitworth JAG.
2003. Associations between helminth infection and CD4+
T cell count, viral load and cytokine responses in HIV-1
infected Ugandan adults. Trans R Soc Trop Med Hyg
97:103-8.</mixed-citation>
         </ref>
         <ref id="d1440e518a1310">
            <mixed-citation id="d1440e522" publication-type="other">
Else KJ, Finkelman FD. 1998. Intestinal nematode parasites,
cytokines and effector mechanisms. Int J Parasitol
28:1145-58.</mixed-citation>
         </ref>
         <ref id="d1440e535a1310">
            <mixed-citation id="d1440e539" publication-type="other">
Eraud C, Trouve C, Dano S, Chastel O, Faivre B. 2008.
Competition for resources modulates cell-mediated immu-
nity and stress hormone level in nested collared doves
(Streptopelia decaocto). Gen Comp End 155: 542-51.</mixed-citation>
         </ref>
         <ref id="d1440e555a1310">
            <mixed-citation id="d1440e559" publication-type="other">
Erb KJ, Trujillo C, Fugate M, Moll H. 2002. Infection with
the helminth Nippostrongylus brasiliensis does not inter-
fere with efficient elimination of Mycobacterium bovis
BCG from the lungs of mice. Clin Diagn Lab Immunol
9:727-30.</mixed-citation>
         </ref>
         <ref id="d1440e578a1310">
            <mixed-citation id="d1440e582" publication-type="other">
Ezenwa VO, Etienne RS, Luikart G, Beja-Pereira A, Jolles AE.
2010. Hidden consequences of living in a wormy world:
nematode-induced immune suppression facilitates
tuberculosis invasion in African Buffalo. Am Nat
176:613-24.</mixed-citation>
         </ref>
         <ref id="d1440e601a1310">
            <mixed-citation id="d1440e605" publication-type="other">
Eziefula AC, Brown M. 2008. Intestinal nematodes: disease
burden, deworming and the potential importance of co-
infection. Curr Opin Infect Dis 21:516-22.</mixed-citation>
         </ref>
         <ref id="d1440e618a1310">
            <mixed-citation id="d1440e622" publication-type="other">
Fenton A. 2008. Worms and germs: the population dynamic
consequences of microparasite-macroparasite co-infection.
Parasitology 135:1545-60.</mixed-citation>
         </ref>
         <ref id="d1440e636a1310">
            <mixed-citation id="d1440e640" publication-type="other">
Frantz FG, Silva Rosada R, Turato WM, Peres CM, Coelho-
Castelo AAM, Ramos SG, Aronoff DM, Silva CL,
Faccioli LH. 2007. The immune response to toxocariasis
does not modify susceptibility to Mycobacterium tuberculo-
sis infection in BALB/c mice. Am J Trop Med Hyg
77:691-8.</mixed-citation>
         </ref>
         <ref id="d1440e663a1310">
            <mixed-citation id="d1440e667" publication-type="other">
Furze RC, Hussell T, Selkirk ME. 2006. Amelioration of influ-
enza-induced pathology in mice by coinfection with
Trichinella spiralis. Infect Immun 74:1924-32.</mixed-citation>
         </ref>
         <ref id="d1440e680a1310">
            <mixed-citation id="d1440e684" publication-type="other">
Gallagher M, et al. 2005. The effects of maternal helminth and
malaria infections on mother-to-child HIV transmission.
AIDS 19:1849-55.</mixed-citation>
         </ref>
         <ref id="d1440e697a1310">
            <mixed-citation id="d1440e701" publication-type="other">
Gibson LR, Li B, Remold SK. 2010. Treating cofactors can
reverse the expansion of a primary disease epidemic. BMC
Infect Dis 10:248.</mixed-citation>
         </ref>
         <ref id="d1440e714a1310">
            <mixed-citation id="d1440e718" publication-type="other">
Graham AL. 2008. Ecological rules governing helminth-
microparasite coinfection. Proc Natl Acad Sci 105:
566-70.</mixed-citation>
         </ref>
         <ref id="d1440e731a1310">
            <mixed-citation id="d1440e735" publication-type="other">
Graham AL, Cattadori IM, Lloyd-Smith JO, Ferrari MJ,
Bjornstad ON. 2007. Transmission consequences of
coinfection: Cytokines writ large? Trends Parasitol
23:284-91.</mixed-citation>
         </ref>
         <ref id="d1440e752a1310">
            <mixed-citation id="d1440e756" publication-type="other">
Graham AL, Lamb TJ, Read AF, Allen JE. 2005. Malaria-filaria
coinfection in mice makes malarial disease more severe
unless filarial infection achieves patency. J Infect Dis
191:410-21.</mixed-citation>
         </ref>
         <ref id="d1440e772a1310">
            <mixed-citation id="d1440e776" publication-type="other">
Hartgers FC, Yazdanbakhsh M. 2006. Co-infection of hel-
minths and malaria: modulation of the immune responses
to malaria. Paras Immunol 28:497-506.</mixed-citation>
         </ref>
         <ref id="d1440e789a1310">
            <mixed-citation id="d1440e793" publication-type="other">
Hawley DM, Altizer SM. 2011. Disease ecology meets ecolog-
ical immunology: understanding the links between organ-
ismal immunity and infection dynamics in natural
populations. Funct Ecol 25:48-60.</mixed-citation>
         </ref>
         <ref id="d1440e809a1310">
            <mixed-citation id="d1440e813" publication-type="other">
Hosseinipour MC, Napravnik S, Joaki G, Gama S, Mbeye N,
Banda B, Martinson F, Hoffman I, Cohen MS. 2007. HIV
and parasitic infection and the effect of treatment
among adult outpatients in Malawi. J Infect Dis
195:1278-82.</mixed-citation>
         </ref>
         <ref id="d1440e832a1310">
            <mixed-citation id="d1440e836" publication-type="other">
Hotez PJ. 2009. Mass drug administration and integrated con-
trol for the world's high-prevalenceneglected tropical dis-
eases. Clin Pharmacol Therapeut 85:659-64.</mixed-citation>
         </ref>
         <ref id="d1440e849a1310">
            <mixed-citation id="d1440e853" publication-type="other">
Jolles AE, Ezenwa VO, Etienne RS, Turner WC, Olff H. 2008.
Interactions between macroparasites and microparasites
drive infection patterns in free-ranging African buffalo.
Ecology 89:2239-50.</mixed-citation>
         </ref>
         <ref id="d1440e870a1310">
            <mixed-citation id="d1440e874" publication-type="other">
Kamal SM, El Sayed Khalifa K. 2006. Immune modulation by
helminthic infections: worms and viral infections. Paras
Immunol 28:483-96.</mixed-citation>
         </ref>
         <ref id="d1440e887a1310">
            <mixed-citation id="d1440e891" publication-type="other">
Karp CL, Auwaerter PG. 2007. Coinfection with HIV and
tropical infectious diseases. Il.helminthic, fungal, bacterial,
and viral pathogens. Clin Infect Dis 45:1214-20.</mixed-citation>
         </ref>
         <ref id="d1440e904a1310">
            <mixed-citation id="d1440e908" publication-type="other">
Keesing F, et al. 2010. Impacts of biodiversity on the emer-
gence and transmission of infectious diseases. Nature
468:647-52.</mixed-citation>
         </ref>
         <ref id="d1440e921a1310">
            <mixed-citation id="d1440e925" publication-type="other">
Lamb TJ, Graham AL, Le Goff L, Allen JE. 2005. Co-infected
C57BL/6 mice mount appropriately polarized and compart-
mentalized cytokine responses to Litomosoides sigmodontis
and Leishmania major but disease progression is altered.
Paras Immunol 27:317-24.</mixed-citation>
         </ref>
         <ref id="d1440e944a1310">
            <mixed-citation id="d1440e948" publication-type="other">
Legesse M, Erko B, Balcha F. 2004. Increased parasitaemia
and delayed parasite clearance in Schistosoma mansoni
and Plasmodium berghei coinfected mice. Acta Trop
91:161-6.</mixed-citation>
         </ref>
         <ref id="d1440e964a1310">
            <mixed-citation id="d1440e968" publication-type="other">
Lloyd-Smith JO, Poss M, Grenfell BT. 2008. HIV-1/parasite
co-infection and the emergence ofiiew parasite strains.
Parasitology 135:795-806.</mixed-citation>
         </ref>
         <ref id="d1440e982a1310">
            <mixed-citation id="d1440e986" publication-type="other">
Lloyd-Smith JO, Schreiber SJ, Kopp PE, Getz WM. 2005.
Superspreading and the effect of individual variation on
disease emergence. Nature 438:355-9.</mixed-citation>
         </ref>
         <ref id="d1440e999a1310">
            <mixed-citation id="d1440e1003" publication-type="other">
Long KZ, Nanthakumar N. 2004. Energetic and nutritional
regulation of the adaptive immune response and trade-
offs in ecological immunology. Am J Human Biol
16:499-507.</mixed-citation>
         </ref>
         <ref id="d1440e1019a1310">
            <mixed-citation id="d1440e1023" publication-type="other">
Lyke KE, et al. 2005. Association of Schistosoma haematobium
infection with protection against acute Plasmodium falcip-
arum malaria in Malian children. Am J Trop Med Hyg
73:1124-30.</mixed-citation>
         </ref>
         <ref id="d1440e1039a1310">
            <mixed-citation id="d1440e1043" publication-type="other">
Maizels RM, Balic A, Gomez-Escobar N, Nair MD, Taylor M,
Allen JE. 2004. Helminth parasites - masters of regulation.
Immunol Rev 201:89-116.</mixed-citation>
         </ref>
         <ref id="d1440e1056a1310">
            <mixed-citation id="d1440e1060" publication-type="other">
Martin LB, Navara KJ, Weil ZM, Nelson RJ. 2007.
Immunological memory is compromised by food restric-
tion in deer mice Peromyscus maniculatus. Am J Physiol-
Reg Integr Comp Physiol 292:R316-20.</mixed-citation>
         </ref>
         <ref id="d1440e1076a1310">
            <mixed-citation id="d1440e1080" publication-type="other">
May RM, Gupta S, McLean AR. 2001. Infectious disease dy-
namics: what characterizes a successful invader? Phil Trans
R Soc B 356:901-10.</mixed-citation>
         </ref>
         <ref id="d1440e1094a1310">
            <mixed-citation id="d1440e1098" publication-type="other">
Miller CMD, Smith NC, Ikin RJ, Boulter NR, Dalton JP,
Donnelly S. 2009. Immunological interactions between 2
common pathogens, Thl-inducing protozoan Toxoplasma
gondii and the Th2-inducing helminth Fasciola hepatica.
PloS One 4:e5692.</mixed-citation>
         </ref>
         <ref id="d1440e1117a1310">
            <mixed-citation id="d1440e1121" publication-type="other">
Modjarrad K, Zulu I, Redden DT, Njobvu L, Lane HC,
Bentwich Z, Vermund SH. 2005. Treatment of intestinal
helminths does not reduce plasma concentrations of
HIV-1 RNA in coinfected Zambian adults. J Infect Dis
192:1277-83.</mixed-citation>
         </ref>
         <ref id="d1440e1140a1310">
            <mixed-citation id="d1440e1144" publication-type="other">
Moreau E, Chauvin A. 2010. Immunity against helminths:
Interactions with the host and the intercurrent infections.
J Biomed Biotech 2010:42893.</mixed-citation>
         </ref>
         <ref id="d1440e1157a1310">
            <mixed-citation id="d1440e1161" publication-type="other">
Mosmann TR, Sad S. 1996. The expanding universe of T-cell
subsets: Thl, Th2 and more. Immunol Today 17:138—46.</mixed-citation>
         </ref>
         <ref id="d1440e1171a1310">
            <mixed-citation id="d1440e1175" publication-type="other">
Neto LMS, de Vasconcellos Carvalhaes de Oliveira R,
Totino PR, Sant'Anna FM, Coelho VD, Rolla VC,
Zanini GM. 2010. Enteroparasitosis prevalence and parasit-
ism influence in clinical outcomes of tuberculosis patients
with or without HIV co-infection in a reference hospital in
Rio de Janeiro (2000-2006). Braz J Infect Dis 13:427-32.</mixed-citation>
         </ref>
         <ref id="d1440e1198a1310">
            <mixed-citation id="d1440e1202" publication-type="other">
Nmorsi O, Isaac C, Ukwandu NCD. 2009. Schistosoma hae-
matobium and Plasmodium falciparum co-infection with
protection against Plasmodium falciparum malaria in
Nigerian children. As Pac J Trop Med 2:16-20.</mixed-citation>
         </ref>
         <ref id="d1440e1219a1310">
            <mixed-citation id="d1440e1223" publication-type="other">
Pedersen AB, Babayan RK. 2011. Wild immunology. Mol Ecol
20:872-80.</mixed-citation>
         </ref>
         <ref id="d1440e1233a1310">
            <mixed-citation id="d1440e1237" publication-type="other">
Pedersen AB, Fenton A. 2007. Emphasizing the ecology in
parasite community ecology. Trends Ecol Evol 22:133-9.</mixed-citation>
         </ref>
         <ref id="d1440e1247a1310">
            <mixed-citation id="d1440e1251" publication-type="other">
Petney TN, Andrews RH. 1998. Multiparasite communities in
animals and humans: Frequency, structure and pathogenic
significance. Int J Parasitol 28:377-93.</mixed-citation>
         </ref>
         <ref id="d1440e1264a1310">
            <mixed-citation id="d1440e1268" publication-type="other">
Rodriguez-Sosa M, Rivera-Montoya I, Espinoza A, Romero-
Grijalva M, Lopez-Flores R, Gonzales J, Terrazas LI. 2006.
Acute cysticercosis favours rapid and more severe lesions
caused by Leishmania major and Leishmania mexicana in-
fection, a role for alternatively activated macrophages. Cell
Immunol 242:61-71.</mixed-citation>
         </ref>
         <ref id="d1440e1291a1310">
            <mixed-citation id="d1440e1295" publication-type="other">
Sangweme DT, Midzi N, Zinyowera-Mutapuri S, Mduluza T,
Diener-West M, Kumar N. 2010. Impact of Schistosome
infection on Plasmodium falciparum malariometric indices
and immune correlates in school age children in Burma
Valley, Zimbabwe. PloS Negl Trop Dis 4:e882.</mixed-citation>
         </ref>
         <ref id="d1440e1314a1310">
            <mixed-citation id="d1440e1318" publication-type="other">
Sangweme DT, Shiff C, Kumar N. 2009. Plasmodium yoelii:
Adverse outcomes of non-lethal P. yoelii malaria during co-
infection with Schistosoma mansoni in BALB/c mouse
model. Exp Parasitol 122:254-9.</mixed-citation>
         </ref>
         <ref id="d1440e1335a1310">
            <mixed-citation id="d1440e1339" publication-type="other">
Secor WE. 2006. Interactions between schistosomiasis and in-
fection with HIV-1. Paras Immunol 28:597-603.</mixed-citation>
         </ref>
         <ref id="d1440e1349a1310">
            <mixed-citation id="d1440e1353" publication-type="other">
Sokhna C, Le Hesran JY, Mbaye PA, Akiana J, Camara P,
Diop M, Ly A, Druilhe P. 2004. Increase of malaria attacks
among children presenting concomitant infection by
Schistosoma mansoni in Senegal. Malaria J 3:43.</mixed-citation>
         </ref>
         <ref id="d1440e1369a1310">
            <mixed-citation id="d1440e1373" publication-type="other">
Specht S, Fernandez Ruiz D, Dubben B, Deininger S,
Hoerauf A. 2010. Filaria-induced IL-10 suppresses murine
cerebral malaria. Microb Infect 12:635-42.</mixed-citation>
         </ref>
         <ref id="d1440e1386a1310">
            <mixed-citation id="d1440e1390" publication-type="other">
Supali T, et al. 2010. Polyparasitism and its impact on the
immune system. Int J Parasitol 40:1171-6.</mixed-citation>
         </ref>
         <ref id="d1440e1400a1310">
            <mixed-citation id="d1440e1404" publication-type="other">
Telfer S, Lambin X, Birtles R, Beldomenico P, Burthe S,
Paterson S, Begon M. 2010. Species interactions in a para-
site community drive infection risk in a wildlife population.
Science 330:243-6.</mixed-citation>
         </ref>
         <ref id="d1440e1420a1310">
            <mixed-citation id="d1440e1424" publication-type="other">
Tompkins DM, Dunn AM, Smith MJ, Telfer S. 2011. Wildlife
diseases: from individuals to ecosystems. J Animal Ecol
80:19-38.</mixed-citation>
         </ref>
         <ref id="d1440e1438a1310">
            <mixed-citation id="d1440e1442" publication-type="other">
van Riet E, Hartgers FC, Yazdanbakhsh M. 2007. Chronic
helminth infections induce immunomodulation: Conse-
quences and mechanisms. Immunobiology 212:475-90.</mixed-citation>
         </ref>
         <ref id="d1440e1455a1310">
            <mixed-citation id="d1440e1459" publication-type="other">
Waknine-Grinberg JH, Gold D, Ohayon A, Flescher E,
Heyfets A, Doenhoff MJ, Schramm G, Haas H,
Golenser J. 2010. Schistosoma mansoni infection reduces
the incidence of murine cerebral malaria. Malaria J 9:5.</mixed-citation>
         </ref>
         <ref id="d1440e1475a1310">
            <mixed-citation id="d1440e1479" publication-type="other">
Walson JL, et al. 2008. Albendazole treatment of HIV-1 and
helminth co-infection: a randomized, double-blind, pla-
cebo-controlled trial. AIDS 22:1601-9.</mixed-citation>
         </ref>
         <ref id="d1440e1492a1310">
            <mixed-citation id="d1440e1496" publication-type="other">
Wearing HJ, Rohani P. 2006. Ecological and immunological
determinants of dengue epidemics. Proc Natl Acad Sci USA
103:11802-7.</mixed-citation>
         </ref>
         <ref id="d1440e1509a1310">
            <mixed-citation id="d1440e1513" publication-type="other">
Wolday D, Mayaan S, Mariam ZG, Berhe N, Seboxa T,
Britton S, Galai N, Landay A, Bentwich Z. 2002.
Treatment of intestinal worms is associated with decreased
HIV plasma viral load. JAIDS 31:56-62.</mixed-citation>
         </ref>
         <ref id="d1440e1529a1310">
            <mixed-citation id="d1440e1533" publication-type="other">
Wolfe ND, Dunavan CP, Diamond J. 2007. Origins of major
human infectious diseases. Nature 447:279-83.</mixed-citation>
         </ref>
         <ref id="d1440e1544a1310">
            <mixed-citation id="d1440e1548" publication-type="other">
Yoshida A, Maruyama H, Kumagai T, Amano T, Kobayashi F,
Zhang MX, Himeno K, Ohta N. 2000. Schistosoma mansoni
infection cancels the susceptibility to Plasmodium chahaudi
through induction of type 1 immune responses in A/J mice.
Int Immunol 12:1117-25.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
