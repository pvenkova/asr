<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt58dksc</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt5vjpqv</book-id>
      <subj-group>
         <subject content-type="call-number">BJ1533.R42B37 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Respect for persons</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Bioethics</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Medical ethics</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
         <subject>Philosophy</subject>
      </subj-group>
      <book-title-group>
         <book-title>Human Dignity, Human Rights, and Responsibility</book-title>
         <subtitle>The New Language of Global Bioethics and Biolaw</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Barilan</surname>
               <given-names>Yechiel Michael</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>14</day>
         <month>09</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780262525978</isbn>
      <isbn content-type="epub">9780262305815</isbn>
      <isbn content-type="epub">026230581X</isbn>
      <publisher>
         <publisher-name>The MIT Press</publisher-name>
         <publisher-loc>Cambridge, Massachusetts; London, England</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>Massachusetts Institute of Technology</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt5vjpqv"/>
      <abstract abstract-type="short">
         <p>"Human dignity" has been enshrined in international agreements and national constitutions as a fundamental human right. The World Medical Association calls on physicians to respect human dignity and to discharge their duties with dignity. And yet human dignity is a term--like love, hope, and justice--that is intuitively grasped but never clearly defined. Some ethicists and bioethicists dismiss it; other thinkers point to its use in the service of particular ideologies. In this book, Michael Barilan offers an urgently needed, nonideological, and thorough conceptual clarification of human dignity and human rights, relating these ideas to current issues in ethics, law, and bioethics. Combining social history, history of ideas, moral theology, applied ethics, and political theory, Barilan tells the story of human dignity as a background moral ethos to human rights. After setting the problem in its scholarly context, he offers a hermeneutics of the formative texts on Imago Dei; provides a philosophical explication of the value of human dignity and of vulnerability; presents a comprehensive theory of human rights from a natural, humanist perspective; explores issues of moral status; and examines the value of responsibility as a link between virtue ethics and human dignity and rights. Barilan accompanies his theoretical claim with numerous practical illustrations, linking his theory to such issues in bioethics as end-of-life care, cloning, abortion, torture, treatment of the mentally incapacitated, the right to health care, the human organ market, disability and notions of difference, and privacy, highlighting many relevant legal aspects in constitutional and humanitarian law.</p>
      </abstract>
      <counts>
         <page-count count="368"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.3</book-part-id>
                  <title-group>
                     <title>Series Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Caplan</surname>
                           <given-names>Arthur</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
                  <abstract>
                     <p>Glenn McGee and I developed the Basic Bioethics series and collaborated as series coeditors from 1998 to 2008. In fall 2008 and spring 2009 the series was reconstituted, with a new Editorial Board, under my sole editorship. I am pleased to present the thirty-second book in the series.</p>
                     <p>The Basic Bioethics series makes innovative works in bioethics available to a broad audience and introduces seminal scholarly manuscripts, state-of-the-art reference works, and textbooks. Topics engaged include the philosophy of medicine, advancing genetics and biotechnology, end-of-life care, health and social policy, and the empirical study of biomedical life. Interdisciplinary work is encouraged.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This book narrates the story of “human dignity” as a background moral ethos to human rights. It offers a philosophical theory of human dignity and human rights, along with an exploration of what I refer to herein as the strongest moral status—that of beneficiaries of rights.</p>
                     <p>In comparison to numerous publications on these topics, this book is special for combining history with applied ethics, for dealing comprehensively both with human dignity and human rights, and for distinguishing human rights from broader issues of justice, good, and virtue.</p>
                     <p>This first chapter presents the problem and the philosophical definitions and methods</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Hermeneutics and the History of Human Dignity</title>
                  </title-group>
                  <fpage>23</fpage>
                  <abstract>
                     <p>This chapter is divided into four main parts. The first proffers a bird’s-eye view of a few milestones in the maturation of the instrument of human rights. The second part consists of the full text of the key passages from the Hebrew Bible (i.e., the creation of Adam, the covenant with Noah, and Psalm 8). Because self-awareness and acknowledgment of one’s finite and limited perspectives are important lessons of critical theory, in the third section of this chapter I present my own hermeneutics of the sources. In the fourth section I embark on a review of the evolution of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Reconstructing Human Dignity as a Moral Value</title>
                  </title-group>
                  <fpage>93</fpage>
                  <abstract>
                     <p>In this chapter I explicate a philosophical theory of human dignity and offer a descriptive definition of the term. Such a definition lies in between a rigidly logical specification and a looser fanning out of the relevant “language games” and their conceptual ecology. The latter refers to the manner in which the components within the descriptive definition bear on each other and how they interact with other philosophical concepts.</p>
                     <p>Following the historical review of the previous chapter, we may broadly conceptualize the normative value of human dignity as comprised of three categories: the formal elements, the substantial elements, and motivation.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Human Rights or Natural Moral Rights</title>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>James Griffin recently observed that “we do not yet have a clear enough idea of what human rights are” (Griffin 2008, 1). But a rough sketch does exist. Human rights address urgent and very important moral concerns that are practically or potentially relevant to all humans. Whenever possible, respect for human rights must be enforced, even at the expense of other interests and values. Human rights matters are mainly dealt with by social institutions, national and international law. Human rights violations are intolerable regardless of the social context or excuses given. The temporary breach of a human right might be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Moral Status</title>
                  </title-group>
                  <fpage>207</fpage>
                  <abstract>
                     <p>The purpose of this chapter is to examine who might be a beneficiary of moral rights and under what conditions. Ordinarily, authors construct their answers to these questions on metaphysical and theological considerations, such as intelligence, capacity to suffer, and ensoulment. My strategy is different. I argue that the very structure of rights entails conditions on the kinds of creatures that can benefit from rights in a coherent manner and on the kinds of environments that sustain the practice of rights. In order to find these conditions, we have to explore difficult cases involving conflicts that challenge the paradigm of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Responsibility beyond Human Rights</title>
                  </title-group>
                  <fpage>261</fpage>
                  <abstract>
                     <p>This chapter is dedicated to examining dignity-related values that are not governable by human rights. One group of such values pertains to relationships that involve claims on the body and person of others. These are relationships of intimacy, such as between parents and children, romantic domestic partners, and caregivers and receivers of care, especially in contexts of significant dependence and exposure of privacy. Another group of values aims at the preservation and promotion of the conditions that sustain the ethos of human dignity and the instrument of rights (to be discussed in the last section of this chapter). As this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>A Synthetic Summary</title>
                  </title-group>
                  <fpage>295</fpage>
                  <abstract>
                     <p>In this book I have surveyed the historical development of “human dignity” as a normative value. Following this survey and insights from philosophy, anthropology, and psychology I explicated a philosophical theory of human dignity. Then a theory of human rights was established on two parallel modes of reasoning. The first is the notion of moral rights as the strongest enforceable protection of the most basic values of those who deserve the strongest moral status. No argument has been marshaled for acceptance of the notion of the strongest moral status and its given definition; neither have I argued against the possibility</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.12</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>303</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>335</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vjpqv.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>351</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
