<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100641</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03057070</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14653893</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40283235</article-id>
         <article-id pub-id-type="pub-doi">10.1080/03057070902919876</article-id>
         <article-categories>
            <subj-group>
               <subject>Liberation Struggles and Experiences of Exile</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The African National Congress of South Africa in Zambia: The Culture of Exile and the Changing Relationship with Home, 1964-1990</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Hugh</given-names>
                  <surname>Macmillan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40011239</issue-id>
         <fpage>303</fpage>
         <lpage>329</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 The Editorial Board of the Journal of Sourthern African Studies</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/03057070902919876"
                   xlink:title="an external site"/>
         <abstract>
            <p>Liberal and other critics of the ANC in government in South Africa frequently refer to the malign influence of 'exile' on the culture of the party, citing alleged secrecy, paranoia and lack of internal democracy, as the inevitable consequences of the years spent abroad - but they do this without much knowledge of the real experience of exile. This article focuses on the ANC in Zambia, specifically Lusaka, and seeks to examine the history, geography and culture of exile in that place, to provide the missing dimensions of time and space, and to trace the changing relationship between the movement and its two main 'homes' - Zambia and South Africa. The article examines the changing status of the ANC in Zambia from one among many Zambiabased liberation movements in the 1960s and 1970s to a predominant position in the 1980s, as its exile population increased, and it developed the bureaucratic structures of a governmentin-waiting. The ANC's headquarters in Zambia gained in importance as its members were pushed out of other front-line states in the 1980s. Meanwhile, Lusaka became, paradoxically, the destination of an increasing flow of emissaries from the burgeoning internal democratic movement, and from other interest groups. The article's major theses are that the culture of exile of the ANC in Zambia was more typical of its underlying culture than that in any other place and that this reflected the relative openness of Zambia itself It is also argued that lessons learned by the ANC in Zambia about the one-party state, and about economic management, had a significant influence on the its own policies during the transition to democracy, and in government, after the return of the ANC to South Africa in 1990.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1488e128a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1488e135" publication-type="other">
The Economist, 22 April 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e142a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1488e149" publication-type="other">
P.N. O'Malley, Shades of Difference: Mac Maharaj and the Struggle for South Africa (London and New York,
Viking Penguin, 2007), p. 491.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e159a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1488e166" publication-type="other">
L. Nkosi, Home and Exile (London, Longman, 1965).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e173a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1488e180" publication-type="other">
R. Kasrils, Armed and Dangerous: My Underground Struggle Against Apartheid (Oxford, Heinemann, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e188a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1488e195" publication-type="other">
ANC Operational Strategy, 1975-86'
(D.Phil dissertation, Oxford University, 1993), p. 17.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e205a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1488e212" publication-type="other">
Ibid., p. 439.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e219a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1488e226" publication-type="other">
H. Bernstein, The Rift, the Exile Experience of South Africans (Jonathan Cape, London, 1994).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e232" publication-type="other">
S. Morrow, B. Maaba and L. Pulumani, Education in Exile, SOMAFCO, the ANC School in Tanzania, 1978 to
1992 (Cape Town, HSRC Press, 2004);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e241" publication-type="other">
R. Suttner, 'Culture(s) of the African National Congress of South
Africa: Imprint of Exile Experiences', Journal of Contemporary African Studies, 21, 2 (2003), pp. 188-99;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e251" publication-type="other">
S. Hassim, 'Nationalism, Feminism: Autonomy and the ANC in Exile', Journal of Southern African Studies, 30,
4 (2004), pp. 433-56</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e260" publication-type="other">
T. Lodge, 'The State of Exile: the African National Congress of South Africa,
1976-86', in P. Frankel, N. Pines and M. Swilling, The State, Resistance and Change (London, Croom Helm,
1988);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e272" publication-type="other">
E. Maloka, The South African Communist Party in Exile, 1963-90 (Pretoria, Africa Institute of South
Africa. 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e282a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1488e291" publication-type="other">
S. Ellis and T. Sechaba, Comrades Against Apartheid, the ANC and the South African Communist Party in Exile
(London, James Currey, 1992);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e300" publication-type="other">
S. M. Davis, Apartheid's Rebels, Inside South Africa 's Hidden War (New Haven
and London, Yale University Press, 1987);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e310" publication-type="other">
T. Sellström, Sweden and National Liberation in Southern Africa,
2 Volumes (Uppsala, Nordic Africa Institute, 1999);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e319" publication-type="other">
T. Sellström (ed.), Liberation in Southern Africa: Regional
and Swedish Voices: Interviews from Angola, Mozambique, South Africa, the Frontline and Sweden (Uppsala,
Nordic Africa Institute, 1999);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e331" publication-type="other">
V. Shubin, ANC: A View from Moscow (Bellville, Mayibuye Books, 1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e338a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1488e345" publication-type="other">
Lewis Nkosi's 'Refugee Woman', Sechaba (December 1983), p. 32.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e352a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1488e359" publication-type="other">
'Opening Statement of Comrade President Oliver Tambo', Mayibuye, 5/6 (1985), p. 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e367a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1488e374" publication-type="other">
R.A. Simons, All My Life and All My Strength (Johannesburg, STE Publishers, 2004), pp. 302-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e381a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1488e388" publication-type="other">
L. Callinicos, Oliver Tambo: Beyond the Engeli Mountains (Cape Town, David Philip, 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e395a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1488e402" publication-type="other">
B. Turok, Nothing But the Truth (Johannesburg, Jonathan Ball, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e409a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1488e416" publication-type="other">
M. Gevisser, Thabo Mbeki: the Dream Deferred (Johannesburg, Jonathan Ball, 2007), p. 290 and pp. 386, 411,
578.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e426a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1488e433" publication-type="other">
Kasrils, Armed and Dangerous, pp. 181-2.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e440a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1488e447" publication-type="other">
C. Braam, Operation Vula (Johannesburg, Jacana Books, 2004), pp. 179-80.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e455a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1488e462" publication-type="other">
Callinicos, Oliver Tambo, p. 316.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e469a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1488e476" publication-type="other">
ANC Archives, Fort Hare, Lusaka Mission, 1/120/152,</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e483a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1488e490" publication-type="other">
Sellstrom (ed.), National Liberation, interview with Anders Johansson, p. 299.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e497a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1488e504" publication-type="other">
N. Gordimer, A Sport of Nature (London, Jonathan Cape, 1987), p. 232.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e511a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1488e518" publication-type="other">
Braam, Oneration Vula. p. 181.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e525a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1488e532" publication-type="other">
Bernstein, The Rift, Interview with Thomas Nkobi, p. 17.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e540a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1488e547" publication-type="other">
K.D. Kaunda, Kaunda on Violence (London, Collins, 1980), p. 172.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e554a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1488e561" publication-type="other">
R. Bernstein, Memory against Forgetting (London, Viking, 1994), pp. 356-60.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e567" publication-type="other">
S. Clingman, Bram Fischer, Afrikaner
Revolutionary (Cape Town, David Philip, 1998), p. 372.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e577a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1488e584" publication-type="other">
Simons, All My Life, pp. 303-4, 310;</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e591a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1488e598" publication-type="other">
'With the Lid Off, Mail &amp; Guardian, 6 December 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e605a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1488e612" publication-type="other">
Chris Hani, 'The Wankie Campaign', pp. 34-7,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e618" publication-type="other">
Sibeko, Freedom in Our Lifetime, pp. 87- 101.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e624" publication-type="other">
Howard Barrell, MK: the ANC's Armed Struggle
(Harmondsworth, Penguin Books, 1990), Joe Modise reached Zambia in 1966 (p. 20).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e634" publication-type="other">
N. van Driel,
"The ANC's First Armed Military Operation', www.sahistory.ore.za</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e644a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1488e651" publication-type="other">
S. Zukas, Into Exile and Back (Lusaka, Bookworld Publishers, 2003), p. 137.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e659a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1488e666" publication-type="other">
A. Sibeko (Zola Zembe), with Joyce Leeson, Freedom in Our Lifetime (Durban, Indicator Press, 1996),
pp. 74-7, 87-90.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e676a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1488e683" publication-type="other">
Zambia National Archives, Lusaka (hereafter ZNA), MF1/173, 'Pan Africanist Congress, 1965-8 ,</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e690a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1488e697" publication-type="other">
Sellström, Liberation in Southern Africa, interview with Kenneth Kaunda, p. 240.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e704a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1488e711" publication-type="other">
C. Leys and J. Saul, 'Liberation without
Democracy? The SWAPO crisis of 1976', Journal of Southern African Studies, 20, 2 (1994), pp. 123-47;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e720" publication-type="other">
L. White, The Assassination of Herbert Chitepo (Bloomington, University of Indiana Press, 2003);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e726" publication-type="other">
J. Marcum, The Angolan Revolution (Cambridge, MA, MIT Press, two volumes, 1969 &amp; 1978).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e733a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1488e740" publication-type="other">
journal Ikwezi (A Journal of South African and Southern African
Political Analysis) in June 1978 (pp. 3-6, 73-5)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e749" publication-type="other">
SADET, The Road to Democracy in South
Africa (Cape Town, Zebra Press, Cape Town, 2004), Volume One, pp. 536-7,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e758" publication-type="other">
(Notes of interview with Alfred
Sipetho Willie and Gardner Sijake, Cape Town, 5 April 2008.)</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e768a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1488e775" publication-type="other">
'Southern Africa: a Betrayal', Black Dwarf,
26 November 1969.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e786a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1488e793" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e800a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1488e807" publication-type="other">
Alfred Sipetho Willie and Gardner Sijake, Cape Town, 5 April 2008.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e814a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1488e821" publication-type="other">
ANC archives, Fort Hare, Lusaka Mission, 1/14/55, Tambo's report to NEC meeting, 27-31 August 1971;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e827" publication-type="other">
Tambo papers, Box 4, notebook, undated [1969].</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e834a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1488e841" publication-type="other">
'Zambian
government wants to remove our men from Zambia to Tanzania because of misbehaviour. As a result the
government says that O.R. must return to Zambia as soon as possible'. Manifesto on Southern Africa, Lusaka,
14th-16th April, 1969 (Lusaka, Government Printer, 1969), p. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e857a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1488e864" publication-type="other">
Fort Hare, Tambo papers, Box 46,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e870" publication-type="other">
Lilanda Special Committee, 1970-1.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e877a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1488e884" publication-type="other">
ANC commission of enquiry into 'the incidents at Roma
Township' sat in 1970-71,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e893" publication-type="other">
South Africa (African
Nationalists), In Defence of the African Image and Heritage (Dar es Salaam, February 1976), pp. 44-5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e904a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1488e911" publication-type="other">
Lusaka Mission 1/14/55,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e917" publication-type="other">
Lusaka, 27-31 August 1971;</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e924a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d1488e931" publication-type="other">
Callinicos, Oliver Tambo, p. 353,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e937" publication-type="other">
T. Bonga [and others]' (London, 27 December 1975), ff. 12-24,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e943" publication-type="other">
In Defence of the African Image and Heritage, pp. 1-48.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e950a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1488e957" publication-type="other">
Bernstein, The Rift, p. xxi.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e964a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1488e971" publication-type="other">
'Sixtieth Anniversary...Lusaka Meeting', Sechaba, April 1972, pp. 6-8.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e977" publication-type="other">
Anglin and
Shaw, Zambia's Foreign Policy, and K. Eriksen (pseudonym), 'Zambia: Class Formation and Detente', Review
of African Political Economy, 9 (May- August 1978), pp. 4-26.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e990a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1488e997" publication-type="other">
Eriksen, 'Zambia', p. 8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1004a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1488e1011" publication-type="other">
'Comrade Boy Mvemve (J.D.) Murdered', Sechaba, May 1974, pp. 2-3;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1017" publication-type="other">
'J. D. Died Like a Soldier', Sechaba,
June 1974, pp. 26-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1028a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1488e1035" publication-type="other">
Sechaba, June 1974, p. 28.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1042a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d1488e1049" publication-type="other">
Sellstrom, Sweden and the Liberation of Southern Africa, p. 583;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1055" publication-type="other">
Shubin, A View from Moscow, p. 109.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1062a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d1488e1069" publication-type="other">
Sellstrom, Sweden and the Liberation of Southern Africa, p. 592.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1076a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d1488e1083" publication-type="other">
Anglin and Shaw, Zambia's Foreign Policy, p. 243;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1089" publication-type="other">
Shubin, A View from Moscow, p. 168.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1096a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d1488e1103" publication-type="other">
Shubin, A View From Moscow, pp. 165-6, 178.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1110a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d1488e1117" publication-type="other">
Sellstrom, Sweden and the Liberation of Southern Africa, p. 409.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1125a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d1488e1132" publication-type="other">
'Duma Nokwe: Honourable Son of Africa', Sechaba, 12, 2 (1978), p. 35.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1139a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d1488e1146" publication-type="other">
'Duma Nokwe: Honourable Son of Africa', pp. 31-9;</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1153a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d1488e1160" publication-type="other">
Shubin, A View from Moscow, pp. 342-5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1167a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d1488e1174" publication-type="other">
University of the Witwatersrand, Cullen Library, Lewin papers, Simons to Lewin, 5 September 1980.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1181a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d1488e1188" publication-type="other">
Ikwezi, June 1978, p. 6.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1195a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d1488e1202" publication-type="other">
Rhodes House Library, Oxford: Barrell papers, interview with Ivan Pillay, 1989;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1208" publication-type="other">
Gevisser, Thabo Mbeki, p. 290;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1214" publication-type="other">
O'Malley, Shades of Difference, p. 215.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1222a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d1488e1229" publication-type="other">
Davis, Apartheid's Rebels, p. 49;</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1236a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d1488e1243" publication-type="other">
Kasrils, Armed and Dangerous, p. 182.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1250a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d1488e1257" publication-type="other">
Sellström, Sweden and the Liberation of Southern Africa, 711-12;</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1264a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d1488e1271" publication-type="other">
Z. Maharaj, Dancing to a Different Rhythm, A Memoir (Cape Town, Zebra Press, 2006), pp. 136-7, 149,</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1278a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d1488e1285" publication-type="other">
Ibid., pp. 149-50;</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1292a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d1488e1299" publication-type="other">
University of the Witwatersrand, Cullen Library, Lewin papers, Simons to Lewin, 5 September 1980.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1307a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d1488e1314" publication-type="other">
O'Malley, Shades of Difference, p. 220; Maharaj, Dancing to a Different Rhythm, p. 149.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1320" publication-type="other">
www.anc.org.za/history/documents/misc/trc2</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1326" publication-type="other">
D. Beresford, 'Poison in the Ranks of the
ANC, Weekly Guardian, 15 September 1991.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1336a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d1488e1343" publication-type="other">
Kasrils, Armed and Dangerous, p. 253,</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1350a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d1488e1357" publication-type="other">
S. Ellis, 'Mbokodo: Security in ANC Camps,
1961-90', African Affairs, 93 (1994), pp. 279-98.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1367a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d1488e1376" publication-type="other">
Maloka, The SACP in Exile, p. 56,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1382" publication-type="other">
SACP quarterly (Umsebenzi, 2nd quarter, 1990, p. 8)</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1389a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d1488e1396" publication-type="other">
Jack Simons's article 'Our Freedom Charter' (Sechaba, June 1985, pp. 7-10)</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1403a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d1488e1410" publication-type="other">
O'Malley, Shades of Difference, pp. 246, 259; Maloka, The SACP in Exile, p. 44.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1416" publication-type="other">
Mark
Gevisser (Thabo Mbeki, pp. 467-8)</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1427a1310">
            <label>80</label>
            <p>
               <mixed-citation id="d1488e1434" publication-type="other">
P. Jordan, 'Crisis of Conscience in the SACP: a Critical Review of Slovo' s "Has Socialism Failed?"', Southern
African Political and Economic Monthly, June 1990, p. 34.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1444a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d1488e1451" publication-type="other">
Shubin, A View From Moscow, p. 215;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1457" publication-type="other">
Swedish Kroner 68,000,000 - more than US$12,000,000
(Sweden and the Liberation of Southern Africa, p. 705).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1467a1310">
            <label>82</label>
            <p>
               <mixed-citation id="d1488e1474" publication-type="other">
Barrell, 'Conscripts to their Age';</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1480" publication-type="other">
Sellström, Sweden and the Liberation of Southern Africa, p. 412.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1487a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d1488e1494" publication-type="other">
H.W. van der Merwe, Peacemaking in South Africa: a Life in Conflict Resolution (Cape Town, Tafelberg,
2000), pp. 139-47;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1503" publication-type="other">
A. Sampson, Black and Gold (London, Hodder &amp; Stoughton) pp. 253-6;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1509" publication-type="other">
Alex Boraine in Sellstrom (ed.), National Liberation, p. 108.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1516a1310">
            <label>86</label>
            <p>
               <mixed-citation id="d1488e1523" publication-type="other">
A. Sparks, Tomorrow is Another Country (Sandton, Struik, 1994), pp. 29-31.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1530a1310">
            <label>87</label>
            <p>
               <mixed-citation id="d1488e1537" publication-type="other">
Comrade Mzala's (James Nxumalo) article 'AIDS -
Misinformation and Racism' (Sechaba, October 1988, pp. 22-30).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1546" publication-type="other">
HIV
virus (pp. 27, 30).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1557a1310">
            <label>88</label>
            <p>
               <mixed-citation id="d1488e1564" publication-type="other">
B. Pityana, in Sellstrom, National Liberation in Southern Africa, pp. 188-9;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1570" publication-type="other">
Father Cas (Paulsen) and M. Lapsley in Bernstein, The Rift, pp. 186 &amp; 374.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1577a1310">
            <label>89</label>
            <p>
               <mixed-citation id="d1488e1584" publication-type="other">
M. Macmillan, 'The South African National Congress and the Churches ,
The Month, June 1989.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1594a1310">
            <label>90</label>
            <p>
               <mixed-citation id="d1488e1601" publication-type="other">
Personal knowledge and see the Commonwealth Group of Eminent Person, Misson to South Africa:
The Commonwealth Report (London, Penguin, 1986).</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1611a1310">
            <label>92</label>
            <p>
               <mixed-citation id="d1488e1618" publication-type="other">
Rhodes House Library, Oxford: Barrell papers: interviews with Sue Rabkin; and interview with Ivan Pillay,
2002,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1627" publication-type="other">
O'Malley, Shades of Difference, pp. 246-7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1634a1310">
            <label>93</label>
            <p>
               <mixed-citation id="d1488e1641" publication-type="other">
Braam, Operation Vula; O'Malley, Shades of Difference, 239-313.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1648a1310">
            <label>95</label>
            <p>
               <mixed-citation id="d1488e1655" publication-type="other">
Simons All My Life and All My Strength, pp. 344-7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1663a1310">
            <label>96</label>
            <p>
               <mixed-citation id="d1488e1670" publication-type="other">
K. Musonda, 'Confusing Foreign Policy over East Europe', Times of Zambia, 9 January 1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1677a1310">
            <label>97</label>
            <p>
               <mixed-citation id="d1488e1684" publication-type="other">
Zambia Daily Mail, 18 January 1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1691a1310">
            <label>98</label>
            <p>
               <mixed-citation id="d1488e1698" publication-type="other">
J. Slovo, Has Socialism Failed? (Umsebenzi Discussion pamphlet, Inkululeko Publications, London, no date
[1990], p. 19).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1707" publication-type="other">
African Communist (Second Quarter, 1990).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1713" publication-type="other">
South Africa' (ANC, Lusaka, no date [1988]),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1720" publication-type="other">
Jack Simons and Kader Asmal, in December 1988.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1727a1310">
            <label>99</label>
            <p>
               <mixed-citation id="d1488e1734" publication-type="other">
'Hasty Changes Out - Kaunda', Zambia Daily Mail, 15 March 1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1741a1310">
            <label>100</label>
            <p>
               <mixed-citation id="d1488e1748" publication-type="other">
A.M. Lewanika and D. Chitala (eds), The Hour Has Come! Proceedings of the National Conference on the
Multi-Party Option (Lusaka, Zambia Research Foundation, 1990) and Zukas, Into Exile and Back, pp. 176-89.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1757" publication-type="other">
Lewanika (The Hour Has Come!, p. 61),</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1764a1310">
            <label>101</label>
            <p>
               <mixed-citation id="d1488e1771" publication-type="other">
Rhodes House Library, Oxford: Barrell papers, interviews with Ivan Pillay, July 1989.</mixed-citation>
            </p>
         </fn>
         <fn id="d1488e1779a1310">
            <label>102</label>
            <p>
               <mixed-citation id="d1488e1786" publication-type="other">
Joe Slovo, 'Nudging the balance from "free" to "plan"', Review/Economy supplement, Weekly Mail, 30 March-
4 April 1990, p. 4.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1488e1795" publication-type="other">
(Gevisser,
Thabo Mbeki, p. 540)</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
