<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">demography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100446</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00703370</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15337790</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23252681</article-id>
         <article-categories>
            <subj-group>
               <subject>REPRODUCTIVE BEHAVIOR</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Impact of HIV Testing on Subjective Expectations and Risky Behavior in Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Adeline</given-names>
                  <surname>Delavande</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Hans-Peter</given-names>
                  <surname>Kohler</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">49</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23252460</issue-id>
         <fpage>1011</fpage>
         <lpage>1036</lpage>
         <permissions>
            <copyright-statement>© 2012 Population Association of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23252681"/>
         <abstract>
            <p>We investigate the causal impact of learning HIV status on HIV/AIDS-related expectations and sexual behavior in the medium run. Our analyses document several unexpected results about the effect of learning one's own, or one's spouse's, HIV status. For example, receiving an HIV-negative test result implies higher subjective expectations about being HIV-positive after two years, and individuals tend to have larger prediction errors about their HIV status after learning their HIV status. If individuals in HIV-negative couples also learn the status of their spouse, these effects disappear. In terms of behavioral outcomes, our analyses document that HIV-positive individuals who learned their status reported having fewer partners and using condoms more often than those who did not learn their status. Among married respondents in HIV-negative couples, learning only one's own status increases risky behavior, while learning both statuses decreases risky behavior. In addition, individuals in serodiscordant couples who learned both statuses are more likely to report some condom use. Overall, our analyses suggest that ensuring that each spouse learns the HIV status of the other, either through couple's testing or through spousal communication, may be beneficial in high-prevalence environments.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1072e219a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1072e226" publication-type="other">
http://www.malawi.pop.upenn.edu/</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1072e232" publication-type="other">
Watkins et al. 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1072e238" publication-type="other">
Anglewicz et al. 2009</mixed-citation>
            </p>
         </fn>
         <fn id="d1072e245a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1072e252" publication-type="other">
Delavande et al. (2011)</mixed-citation>
            </p>
         </fn>
         <fn id="d1072e259a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1072e266" publication-type="other">
Wooldridge (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1072e273a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1072e280" publication-type="other">
Wooldridge (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1072e288a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1072e295" publication-type="other">
Sherr et al. (2007)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1072e311a1310">
            <mixed-citation id="d1072e315" publication-type="other">
Allen, S., Karita, E., Chomba, E., Roth, D. L., Telfair, J., Zulu, I.,... Haworth, A. (2007). Promotion of
couples' voluntary counselling and testing for HIV through influential networks in two African capital
cities. BMC Public Health, 7, 349.</mixed-citation>
         </ref>
         <ref id="d1072e328a1310">
            <mixed-citation id="d1072e332" publication-type="other">
Allen, S., Meinzen-Derr, J., Kautzman, M., Zulu, I., Trask, S., Fideli, U.,... Haworth, A. (2003). Sexual
behavior of HIV discordant couples after HIV counseling and testing. AIDS, 17, 733-740.</mixed-citation>
         </ref>
         <ref id="d1072e342a1310">
            <mixed-citation id="d1072e346" publication-type="other">
Anglewicz, P., Adams, J., Obare-Onyango, F., Kohler, H.-P., &amp; Watkins, S. (2009). The Malawi diffusion
and ideational change project 2004-06: Data collection, data quality and analyses of attrition.
Demographic Research, 20{article 21), 503-540. doi:10.4054/DemRes.2009.20.21</mixed-citation>
         </ref>
         <ref id="d1072e359a1310">
            <mixed-citation id="d1072e363" publication-type="other">
Boozer, M. A., &amp; Philipson, T. J. (2000). The impact of public testing for human immunodeficiency virus.
Journal of Human Resources, 35, 419 446.</mixed-citation>
         </ref>
         <ref id="d1072e374a1310">
            <mixed-citation id="d1072e378" publication-type="other">
Chimbiri, A. (2007). The condom is an Intruder in Marriage: Evidence from rural Malawi. Social Science &amp;
Medicine, 64, 1102-1115.</mixed-citation>
         </ref>
         <ref id="d1072e388a1310">
            <mixed-citation id="d1072e392" publication-type="other">
Corbett, E. L., Makamureb, B., Cheunga, Y. B., Dauyab, E„ Matambob, R., Bandasonb, T.,... Hayes, R. J.
(2007). HIV incidence during a cluster-randomized trial of two strategies providing voluntary counselling and
testing at the workplace, Zimbabwe. AIDS, 21, 483-489.</mixed-citation>
         </ref>
         <ref id="d1072e405a1310">
            <mixed-citation id="d1072e409" publication-type="other">
Delavande, A., Gine, X., &amp; McKenzie, D. (2011). Measuring subjective expectations in developing
countries: A critical review and new evidence. Journal of Development Economics, 94, 151-163.</mixed-citation>
         </ref>
         <ref id="d1072e419a1310">
            <mixed-citation id="d1072e423" publication-type="other">
Delavande, A., &amp; Kohler, H.-P. (2009). Subjective expectations in the context of HIV/AIDS in Malawi.
Demographic Research, 20(article 31), 817-875. doi:10.4054/DemRes.2009.20.31</mixed-citation>
         </ref>
         <ref id="d1072e433a1310">
            <mixed-citation id="d1072e437" publication-type="other">
Denison, J., O'Reilly, K., Schmid, G., Kennedy, C., &amp; Sweat, M. (2008). HIV voluntary counseling and
testing and behavioral risk reduction in developing countries: A meta-analysis, 1990-2005. AIDS and
Behavior, 12, 363-373.</mixed-citation>
         </ref>
         <ref id="d1072e450a1310">
            <mixed-citation id="d1072e454" publication-type="other">
De Paula, A., Shapira, G., &amp; Todd, P. E. (2010). How beliefs about HIV status affect risky
behaviors: Evidence from Malawi. Unpublished manuscript, Department of Economics,
University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="d1072e468a1310">
            <mixed-citation id="d1072e472" publication-type="other">
Desgrees-du-Lou, A., &amp; Ome-Gliemann, J. (2008). Couple-centred testing and counselling for HIV serodis-
cordant heterosexual couples in sub-Saharan Africa. Reproductive Health Matters, 16, 151-161.</mixed-citation>
         </ref>
         <ref id="d1072e482a1310">
            <mixed-citation id="d1072e486" publication-type="other">
Goldstein, M., Zivin, J. G., Habyarimana, J., Pop-Eleches, C., &amp; Thirumurthyk, H. (2008). Health worker
absence, HIV testing and behavioral change: Evidence from western Kenya. Unpublished Working
Paper, Department of Economics, Columbia University.</mixed-citation>
         </ref>
         <ref id="d1072e499a1310">
            <mixed-citation id="d1072e503" publication-type="other">
Gong, E. (2010). HIV testing &amp; risky behavior: The effect of being surprised by your HIV status.
Unpublished manuscript, Department of Agricultural and Resource Economics, University of
California, Berkeley.</mixed-citation>
         </ref>
         <ref id="d1072e516a1310">
            <mixed-citation id="d1072e520" publication-type="other">
Granich, R. M., Gilks, C. F., Dye, C., De Cock, K, &amp; Williams, B. G. (2009). Universal voluntary HIV
testing with immediate antiretroviral therapy as a strategy for elimination of HIV transmission: A
mathematical model. The Lancet, 373, 48-57.</mixed-citation>
         </ref>
         <ref id="d1072e533a1310">
            <mixed-citation id="d1072e537" publication-type="other">
Manski, C. F. (2004). Measuring expectations. Econometrica, 72, 1329-1376.</mixed-citation>
         </ref>
         <ref id="d1072e544a1310">
            <mixed-citation id="d1072e548" publication-type="other">
Matovu, J., Gray, R., Makumbi, F., Waver, M., Serwadda, D., Kigozi, G.,... Nalugoda, F. (2005).
Voluntary HIV counseling and testing acceptance, sexual risk behavior and HIV incidence in Rakai,
Uganda. AIDS, 19, 503-511.</mixed-citation>
         </ref>
         <ref id="d1072e562a1310">
            <mixed-citation id="d1072e566" publication-type="other">
Meijer, E., &amp; Wansbeek, T. (2007). The sample selection model from a method of moments perspective.
Econometric Reviews, 26, 25-51.</mixed-citation>
         </ref>
         <ref id="d1072e576a1310">
            <mixed-citation id="d1072e580" publication-type="other">
Obare, F., Fleming, R, Anglewicz, R, Thornton, R., Martinson, F., Kapatuka, A.,... Kohler, H.-R
(2009). Acceptance of repeat population-based voluntary counseling and testing for HIV in
rural Malawi. Sexually Transmitted Infections, 85, 139-144. Advance online publication,
doi: 10.1136/sti.2008.030320</mixed-citation>
         </ref>
         <ref id="d1072e596a1310">
            <mixed-citation id="d1072e600" publication-type="other">
Santow, G., Bracher, M., &amp; Watkins, S. (2008). Implications for behavioural change in rural Malawi of
popular understandings of the epidemiology of AIDS (Working Paper CCPR-2008-045). Los Angles:
California Center for Population Research, UCLA.</mixed-citation>
         </ref>
         <ref id="d1072e613a1310">
            <mixed-citation id="d1072e617" publication-type="other">
Sherr, L., Lopman, G., Kakowa, M., Dube, S., Chawira, G., Nyamukapa, C Gregson, S. (2007).
Voluntary counseling and testing: Uptake, impact on sexual behaviour, and HIV incidence in a rural
Zimbabwean cohort. AIDS, 21, 851-860.</mixed-citation>
         </ref>
         <ref id="d1072e630a1310">
            <mixed-citation id="d1072e634" publication-type="other">
Thornton, R. (2008). The demand for and impact of learning HIV status: Evidence from a field experiment.
American Economic Review, 98, 1829-1863.</mixed-citation>
         </ref>
         <ref id="d1072e644a1310">
            <mixed-citation id="d1072e648" publication-type="other">
UNAIDS. (2001). The impact of voluntary counselling and testing. A global review of the benefits and
challenges. Retrieved from http://www.unaids.org</mixed-citation>
         </ref>
         <ref id="d1072e659a1310">
            <mixed-citation id="d1072e663" publication-type="other">
UNAIDS. (2006). Report on the global HIV/AIDS epidemic. New York: World Health Organization and
UNAIDS. Retrieved from http://www.unaids.org</mixed-citation>
         </ref>
         <ref id="d1072e673a1310">
            <mixed-citation id="d1072e677" publication-type="other">
UNAIDS. (2008). Report on the global HIV/AIDS epidemic. New York: World Health Organization and
UNAIDS. Retrieved from http://www.unaids.org</mixed-citation>
         </ref>
         <ref id="d1072e687a1310">
            <mixed-citation id="d1072e691" publication-type="other">
The Voluntary HIV-1 Counseling and Testing Efficacy Study Group. (2000). Efficacy of voluntary HIV-1
counselling and testing in individuals and couples in Kenya, Tanzania, and Trinidad: A randomised
trial.Lancet, 356, 103-112.</mixed-citation>
         </ref>
         <ref id="d1072e704a1310">
            <mixed-citation id="d1072e708" publication-type="other">
Watkins, S., Behrman, J. R., Kohler, H.-R, &amp; Zulu, E. M. (2003). Introduction to "Research on demo-
graphic aspects of HIV/AIDS in rural Africa". Demographic Research Special Collection, 1(1), 1-30.
doi: 10.4054/DemRes.2003.S1.1</mixed-citation>
         </ref>
         <ref id="d1072e721a1310">
            <mixed-citation id="d1072e725" publication-type="other">
Weinhardt, L. S., Carey, M. P., Johnson, B. T., &amp; Bickham, N. L. (1999). Effects of HIV counseling and
testing on sexual risk behavior: A meta-analytic review of published research, 1985-1997. American
Journal of Public Health, 89, 1397-1405.</mixed-citation>
         </ref>
         <ref id="d1072e738a1310">
            <mixed-citation id="d1072e742" publication-type="other">
Wooldridge, J. (2002). Econometric analysis of cross section and panel data. Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
