<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">studfamiplan</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100383</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Studies in Family Planning</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley-Blackwell</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00393665</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284465</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25593946</article-id>
         <title-group>
            <article-title>Are Female Orphans at Risk for Early Marriage, Early Sexual Debut, and Teen Pregnancy? Evidence from Sub-Saharan Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Tia</given-names>
                  <surname>Palermo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Amber</given-names>
                  <surname>Peterman</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25593943</issue-id>
         <fpage>101</fpage>
         <lpage>112</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25593946"/>
         <abstract>
            <p>Female orphans are widely cited as being at risk for early marriage, early childbearing, and risky sexual behavior; however, to date no studies have examined these linkages using population-level data across multiple countries. This study draws from recent Demographic and Health Surveys from ten sub-Saharan African countries to examine the relationship between orphanhood status and measures of early marriage, early sexual debut, and teen pregnancy among adolescent girls aged 15 to 17. Results indicate that, overall, little association is found between orphanhood and early marriage or teen pregnancy, whereas evidence from seven countries supports associations between orphanhood and early sexual debut. Findings are sensitive to the use of multivariate models, type of orphan, and country setting. Orphanhood status alone may not be a sufficient targeting mechanism for addressing these outcomes in many countries; a broader, multidimensional targeting scheme including orphan type, schooling, and poverty measures would be more robust in identifying and aiding young women at risk.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d704e126a1310">
            <label>1</label>
            <mixed-citation id="d704e133" publication-type="other">
Nour 2006</mixed-citation>
         </ref>
         <ref id="d704e140a1310">
            <label>3</label>
            <mixed-citation id="d704e147" publication-type="other">
Dekker and Hoogeveen 2002</mixed-citation>
         </ref>
         <ref id="d704e154a1310">
            <label>4</label>
            <mixed-citation id="d704e161" publication-type="other">
Gregson and his colleagues (2005)</mixed-citation>
         </ref>
         <ref id="d704e168a1310">
            <label>5</label>
            <mixed-citation id="d704e175" publication-type="other">
&amp;amp;lt;http://www.measuredhs.com&amp;amp;gt;.</mixed-citation>
         </ref>
         <ref id="d704e183a1310">
            <label>10</label>
            <mixed-citation id="d704e190" publication-type="other">
Ai and Norton
(2003).</mixed-citation>
         </ref>
         <ref id="d704e200a1310">
            <label>11</label>
            <mixed-citation id="d704e207" publication-type="other">
&amp;amp;lt;http://hdr.
undp.org/en/humandev/hdi/&amp;amp;gt;.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d704e226a1310">
            <mixed-citation id="d704e230" publication-type="other">
Ai, Chunrong and Edward C. Norton. 2003. "Interaction terms in logit
and probit models." Economic Letters 80(1): 123-129.</mixed-citation>
         </ref>
         <ref id="d704e240a1310">
            <mixed-citation id="d704e244" publication-type="other">
Ainsworth, Martha and Innocent Semali. 2000. "The Impact of Adult Deaths
on Children's Health in Northwestern Tanzania." World Bank Policy
Research Working Paper No. 2266. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d704e257a1310">
            <mixed-citation id="d704e261" publication-type="other">
Antwine, Benjamin, Elizabeth Cantor-Graae, and Francis Bajunirwe.
2005. "Psychological distress among AIDS orphans in rural Ugan-
da." Social Science &amp;amp; Medicine 61(3): 555-564.</mixed-citation>
         </ref>
         <ref id="d704e274a1310">
            <mixed-citation id="d704e278" publication-type="other">
Beegle, Kathleen and Sofya Krutikova. 2007. "Adult Mortality and
Children's Transition into Marriage." World Bank Policy Research
Working Paper No. 4139: Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d704e292a1310">
            <mixed-citation id="d704e296" publication-type="other">
Beegle, Kathleen, Joachim De Weerdt, and Stefan Dercon. 2006. "Or-
phanhood and the long-run impact on children." American Journal
of Agricultural Economics 88(5): 1,266-1,272.</mixed-citation>
         </ref>
         <ref id="d704e309a1310">
            <mixed-citation id="d704e313" publication-type="other">
Bicego, George, Shea Rutstein, and Kiersten Johnson. 2003. "Dimensions
of the emerging orphan crisis in sub-Saharan Africa." Social Science
&amp;amp; Medicine 56(6): 1,235-1,247.</mixed-citation>
         </ref>
         <ref id="d704e326a1310">
            <mixed-citation id="d704e330" publication-type="other">
Bishai, David, El Daw Suliman, Heena Brahmbhatt, et. al. 2003. "Does
biological relatedness affect survival?" Demographic Research 8(9):
261-278.</mixed-citation>
         </ref>
         <ref id="d704e343a1310">
            <mixed-citation id="d704e347" publication-type="other">
Boerma, J.T., M. Urassa, S. Nnko, et al. 2002. "Sociodemographic con-
text of the AIDS epidemic in a rural area of Tanzania with a focus
on people's mobility and marriage." Sexually Transmitted Infections
78(Supplement 1): i97-il05.</mixed-citation>
         </ref>
         <ref id="d704e363a1310">
            <mixed-citation id="d704e367" publication-type="other">
Bracher, Michael, Gigi Santow, and Susan Cotts Watkins. 2003. "Mov-
ing and marrying: Modelling HIV infection among newly-weds
in Malawi." Demographic Research. (Special Collection 1, Article 7):
207-246.</mixed-citation>
         </ref>
         <ref id="d704e383a1310">
            <mixed-citation id="d704e387" publication-type="other">
Bruce, Judith and Shelley Clark. 2004. "The implications of early mar-
riage for HIV/AIDS policy." Brief based on background paper pre-
pared for the WHO/UNFPA/Population Council Technical Consul-
tation on Married Adolescents. New York: Population Council.</mixed-citation>
         </ref>
         <ref id="d704e404a1310">
            <mixed-citation id="d704e408" publication-type="other">
Case, Anne and Cally Ardington. 2006. "The impact of parental death
on school outcomes: Longitudinal evidence from South Africa."
Demography 43(3): 401-120.</mixed-citation>
         </ref>
         <ref id="d704e421a1310">
            <mixed-citation id="d704e425" publication-type="other">
Case, Anne, Christina H. Paxson, and Joseph Ableidinger 2004. "Or-
phans in Africa: Parental death, poverty, and school enrollment."
Demography 41(3): 483-508.</mixed-citation>
         </ref>
         <ref id="d704e438a1310">
            <mixed-citation id="d704e442" publication-type="other">
Castle, Sarah. 2004. "Rural children's attitudes to people with HIV/ AIDS
in Mali: The causes of stigma." Culture, Health &amp;amp; Sexuality 6(1): 1-18.</mixed-citation>
         </ref>
         <ref id="d704e452a1310">
            <mixed-citation id="d704e456" publication-type="other">
Clark, Shelley. 2004. "Early marriage and HIV risks in sub-Saharan Af-
rica." Studies in Family Planning 35(3): 149-160.</mixed-citation>
         </ref>
         <ref id="d704e466a1310">
            <mixed-citation id="d704e470" publication-type="other">
Cluver, Lucie and Frances Gardner. 2007. "Risk and protective factors
for psychological well-being of children orphaned by AIDS in Cape
Town: A qualitative study of children and caregivers' perspec-
tives." AIDS Care 19(3): 318-325.</mixed-citation>
         </ref>
         <ref id="d704e486a1310">
            <mixed-citation id="d704e490" publication-type="other">
Crampin, Amelia C, Sian Floyd, Judith R. Glynn, et al. 2003. "The long-
term impact of HIV and orphanhood on the mortality and physical
well-being of children in rural Malawi." AIDS 17(3): 389-397.</mixed-citation>
         </ref>
         <ref id="d704e504a1310">
            <mixed-citation id="d704e508" publication-type="other">
Dekker, Marleen and Hans Hoogeveen. 2002. "Bridewealth and house-
hold security in rural Zimbabwe." Journal of African Economies 11(1):
114-145.</mixed-citation>
         </ref>
         <ref id="d704e521a1310">
            <mixed-citation id="d704e525" publication-type="other">
Gilborn, Laelia Zoe. 2002. "The effects of HIV infection and AIDS on
children in Africa." Western Journal of Medicine 176(1): 12-14.</mixed-citation>
         </ref>
         <ref id="d704e535a1310">
            <mixed-citation id="d704e539" publication-type="other">
Gilborn, Laelia Zoe, Rebecca Nyonyintono, Robert Kabumbuli, and Ga-
briel Jagwe-Wadda. 2001. Making a Difference for Children Affected by
AIDS: Baseline Findings from Operations Research in Uganda. Horizons
Report. New York: Population Council.</mixed-citation>
         </ref>
         <ref id="d704e555a1310">
            <mixed-citation id="d704e559" publication-type="other">
Gregson, Simon, Constance A. Nyamukapa, G.P. Garnett, et al. 2005. "HIV
infection and reproductive health in teenage women orphaned and
made vulnerable by AIDS in Zimbabwe." AIDS Care 17(7): 785-794.</mixed-citation>
         </ref>
         <ref id="d704e572a1310">
            <mixed-citation id="d704e576" publication-type="other">
Hallman, Kelly. 2006. "Orphanhood, poverty, and HIV risk behav-
iors among adolescents in KwaZulu-Natal, South Africa." Paper
presented at the Annual Meeting of the Population Association of
America, Los Angeles, 30 March-1 April.</mixed-citation>
         </ref>
         <ref id="d704e592a1310">
            <mixed-citation id="d704e596" publication-type="other">
Hosegood, Victoria, Sian Floyd, Milly Marston, et al. 2007. "The effects
of high HIV prevalence on orphanhood and living arrangements of
children in Malawi, Tanzania, and South Africa." Population Stud-
ies 61(3): 327-336.</mixed-citation>
         </ref>
         <ref id="d704e613a1310">
            <mixed-citation id="d704e617" publication-type="other">
Hosegood, Victoria, Anna-Maria Vanneste, and Ian M. Timaeus. 2004.
"Levels and causes of adult mortality in rural South Africa: The
impact of AIDS." AIDS 18(4): 1-19.</mixed-citation>
         </ref>
         <ref id="d704e630a1310">
            <mixed-citation id="d704e634" publication-type="other">
Hunter, Susan S. 1990. "Orphans as a window on the AIDS epidemic
in sub-Saharan Africa: Initial results and implications of a study in
Uganda." Social Science &amp;amp; Medicine 31(6): 681-690.</mixed-citation>
         </ref>
         <ref id="d704e647a1310">
            <mixed-citation id="d704e651" publication-type="other">
Institut National de la Statistique et de 1'Analyse Economique (INSAE)
[Benin] and Macro International. 2007. Enquete Demographiaue et de
Sante (EDSB-III)-Benin 2006. Cotonou, Benin and Calverton, MD:
INSAE and Macro International.</mixed-citation>
         </ref>
         <ref id="d704e667a1310">
            <mixed-citation id="d704e671" publication-type="other">
Jahn, A., F. Mwaungulu, V. Msiska, et al. 2005. "What is the true death toll
of AIDS in rural Malawi? Population estimates from the Karonga Con-
tinuous Registration System." Paper presented at the Annual Meeting
of the National AIDS Commission, Lilongwe, Malawi, 18-20 April.</mixed-citation>
         </ref>
         <ref id="d704e687a1310">
            <mixed-citation id="d704e691" publication-type="other">
Jain, Saranga and Kathleen Kurz. 2007. New Insights on Preventing Child
Marriage: A Global Analysis of Factors and Programs. Washington, DC:
International Center for Research on Women.</mixed-citation>
         </ref>
         <ref id="d704e704a1310">
            <mixed-citation id="d704e708" publication-type="other">
Jensen, Robert and Rebecca Thornton. 2003. "Early female marriage in
the developing world." In Gender, Development, and Marriage. Ed.
Caroline Sweetman. Oxford: Oxfam. Pp. 9-19.</mixed-citation>
         </ref>
         <ref id="d704e722a1310">
            <mixed-citation id="d704e726" publication-type="other">
Juma, Milka, Ian Askew, and Alan Ferguson. 2007. "Situation analysis
of the sexual and reproductive health and HIV risks and prevention
needs of older orphaned and vulnerable children in Nyanza Prov-
ince, Kenya." Nairobi: Population Council and Constella Futures.</mixed-citation>
         </ref>
         <ref id="d704e742a1310">
            <mixed-citation id="d704e746" publication-type="other">
Lindblade, Kim A., Frank Odhiambo, Daniel H. Rosen, and Kevin M.
DeCock. 2003. "Health and nutritional status of orphans &amp;amp;lt;6 years
old cared for by relatives in western Kenya." Tropical Medicine &amp;amp;
International Health 8(1): 67-72.</mixed-citation>
         </ref>
         <ref id="d704e762a1310">
            <mixed-citation id="d704e766" publication-type="other">
Makame, V., C. Ani, and S. Grantham-McGregor. 2002. "Psychological
well being of orphans in Dar Es Salaam, Tanzania." Acta Paediatrica
91(4): 459^65.</mixed-citation>
         </ref>
         <ref id="d704e779a1310">
            <mixed-citation id="d704e783" publication-type="other">
Mathur, Sanyukta, Margaret Greene, and Anju Malhotra. 2003. Too
Young to Wed: The Lives, Rights and Health of Young Married Girls.
Washington, DC: International Center for Research on Women.</mixed-citation>
         </ref>
         <ref id="d704e796a1310">
            <mixed-citation id="d704e800" publication-type="other">
Miller, Candace Marie, Sofia Gruskin, S.V. Subramanian, and Jody Hey-
mann. 2007. "Emerging health disparities in Botswana: Examining
the situation of orphans during the AIDS epidemic." Social Science
&amp;amp; Medicine 64(12): 2,476-2,486.</mixed-citation>
         </ref>
         <ref id="d704e816a1310">
            <mixed-citation id="d704e820" publication-type="other">
Ministry of Health and Social Welfare (MOHSW) [Lesotho], Bureau of Sta-
tistics (BOS) [Lesotho], and ORC Macro. 2005. Lesotho Demographic and
Health Survey 2004. Calverton, MD: MOHSW, BOS, and ORC Macro.</mixed-citation>
         </ref>
         <ref id="d704e834a1310">
            <mixed-citation id="d704e838" publication-type="other">
Morreira, S.M. and C. Rudd. 2000. Child Sexual Abuse: Research Con-
ducted at the Family Support Clinic, Harare Central Hospital. Report
for UNAIDS. Unpublished.</mixed-citation>
         </ref>
         <ref id="d704e851a1310">
            <mixed-citation id="d704e855" publication-type="other">
Muula, Adamson S., Humphreys Misiri, Lumbani Munthali, et al. 2003.
"The living situations of orphans in periurban Blantyre, Malawi."
Letter. South African Medical Journal 93(12): 920-921.</mixed-citation>
         </ref>
         <ref id="d704e868a1310">
            <mixed-citation id="d704e872" publication-type="other">
Nour, Nawal M. 2006. "Health consequences of child marriage in Af-
rica." Emerging Infectious Diseases 12(11): 1,644-1,650.</mixed-citation>
         </ref>
         <ref id="d704e882a1310">
            <mixed-citation id="d704e886" publication-type="other">
Oleke, Christopher, Astrid Blystad, Karen Marie Moland, Ole Bjorn
Rekdal, and Kristian Heggenhougen. 2006. "The varying vulner-
ability of African orphans: The case of Langi, Northern Uganda."
Childhood 13(2): 267-284.</mixed-citation>
         </ref>
         <ref id="d704e902a1310">
            <mixed-citation id="d704e906" publication-type="other">
Population Council. 2007. "Transitions to adulthood: Child marriage /
married adolescents." &amp;amp;lt;http://www.popcouncil.org/ta/mar.
html.&amp;amp;gt; Accessed 22 December 2007.</mixed-citation>
         </ref>
         <ref id="d704e919a1310">
            <mixed-citation id="d704e923" publication-type="other">
Parikh, Anokhia, Mary Bachman DeSilva, Mandisa Cakwe, et al. 2007.
"Exploring the Cinderella myth: Intrahousehold differences in child
wellbeing between orphans and non-orphans in Amajuba District,
South Africa." AIDS 21(Supplement 7): S95-S103.</mixed-citation>
         </ref>
         <ref id="d704e940a1310">
            <mixed-citation id="d704e944" publication-type="other">
Rau, Bill. 2003. "HIV/AIDS and Child Labour: A State-of-the-Art Re-
view with Recommendations for Action." Synthesis Report, Paper
No. 6. Geneva: International Labour Organization, International
Programme on the Elimination of Child Labour.</mixed-citation>
         </ref>
         <ref id="d704e960a1310">
            <mixed-citation id="d704e964" publication-type="other">
Rotheram-Borus, Mary Jane, Martha Lee, Noelle Leonard, et al. 2003.
"Four-year behavioral outcomes of an intervention for parents living
with HIV and their adolescent children." AIDS 17(8): 1,217-1,225.</mixed-citation>
         </ref>
         <ref id="d704e977a1310">
            <mixed-citation id="d704e981" publication-type="other">
Thurman, Tonya R., Lisanne Brown, Linda Richter, Pranitha Maharaj,
and Robert Magnani. 2006. "Sexual risk behavior among South
African adolescents: Is orphan status a factor?" AIDS and Behavior
10(6): 627-635.</mixed-citation>
         </ref>
         <ref id="d704e997a1310">
            <mixed-citation id="d704e1001" publication-type="other">
Timaeus, Ian M. and Tarda Boler. 2007. "Father figures: The progress at
school of orphans in South Africa." AIDS 21(Supplement 7): S83-S93.</mixed-citation>
         </ref>
         <ref id="d704e1011a1310">
            <mixed-citation id="d704e1015" publication-type="other">
UNAIDS and WHO. 2007. AIDS Epidemic Update. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d704e1022a1310">
            <mixed-citation id="d704e1026" publication-type="other">
UNAIDS, UNICEF, and USAID. 2004. Children on the Brink 2004. Ge-
neva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d704e1037a1310">
            <mixed-citation id="d704e1041" publication-type="other">
UNICEF. 2005. Early Marriage: A Harmful Traditional Practice: A Statistical
Exploration. UNICEF: New York.</mixed-citation>
         </ref>
         <ref id="d704e1051a1310">
            <mixed-citation id="d704e1055" publication-type="other">
Williamson, John, Adrienne Cox, and Beverly Johnston. 2004. Conduct-
ing a Situation Analysis of Orphans and Vulnerable Children Affected
by HIV/AIDS: A Framework and Resource Guide. &amp;amp;lt;http: / /pdf.usaid.
gov/pdf_docs/PN ACX649.pdf &amp;amp;gt;. Accessed 22 December 2007.</mixed-citation>
         </ref>
         <ref id="d704e1071a1310">
            <mixed-citation id="d704e1075" publication-type="other">
Wood, Kate, Elaine Chase and Peter Aggleton. 2006. " Telling the truth
is the best thing': Teenage orphans' experiences of parental AIDS-
related illness and bereavement in Zimbabwe." Social Science &amp;amp;
Medicine 63(7): 1,923-1,933.</mixed-citation>
         </ref>
         <ref id="d704e1091a1310">
            <mixed-citation id="d704e1095" publication-type="other">
Yamano, Takashi and T.S. Jayne. 2004. "Measuring the impacts of work-
ing-age adult mortality on small-scale farm households in Kenya."
World Development 32(1): 91-119.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
