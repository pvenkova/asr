<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/piot16626</book-id>
      <subj-group>
         <subject content-type="nlm">Acquired Immunodeficiency Syndrome</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Government Programs</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Human Rights</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Politics</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Socioeconomic Factors</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">World Health</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>AIDS Between Science and Politics</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="translator" id="contrib1">
            <role>TRANSLATED BY</role>
            <name name-style="western">
               <surname>GAREY</surname>
               <given-names>LAURENCE</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>05</day>
         <month>05</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9780231538770</isbn>
      <isbn content-type="epub">0231538774</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/piot16626"/>
      <abstract abstract-type="short">
         <p>Peter Piot, founding executive director of the Joint United Nations Programme on HIV/AIDS (UNAIDS), recounts his experience as a clinician, scientist, and activist fighting the disease from its earliest manifestation to today. The AIDS pandemic was not only catastrophic to the health of millions worldwide but also fractured international relations, global access to new technologies, and public health policies in nations across the globe. As he struggled to get ahead of the disease, Piot found science does little good when it operates independently of politics and economics, and politics is worthless if it rejects scientific evidence and respect for human rights.</p>
         <p>Piot describes how the epidemic altered global attitudes toward sexuality, the character of the doctor-patient relationship, the influence of civil society in international relations, and traditional partisan divides. AIDS thrust health into national and international politics where, he argues, it rightly belongs. The global reaction to AIDS over the past decade is the positive result of this partnership, showing what can be achieved when science, politics, and policy converge on the ground. Yet it remains a fragile achievement, and Piot warns against complacency and the consequences of reduced investments. He refuses to accept a world in which high levels of HIV infection are the norm. Instead, he explains how to continue to reduce the incidence of the disease to minute levels through both prevention and treatment, until a vaccine is discovered.</p>
      </abstract>
      <counts>
         <page-count count="216"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.3</book-part-id>
                  <title-group>
                     <title>TRANSLATOR’S NOTE</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This book is based on ten lectures in the “Knowledge against Poverty” series at the Collège de France in Paris from 2009 to 2010. I had the unique opportunity to reflect on my experience as scientist, clinician, founding executive director of the Joint United Nations Programme on HIV/AIDS (UNAIDS), and activist in the struggle against AIDS since the epidemic began. This experience convinced me that without a political or economic connection science brings little to people, but also that without scientific evidence and respect for human rights, politics is ineffective and can even be harmful.² This present book represents the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>A HETEROGENEOUS AND STILL-EVOLVING EPIDEMIC</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>AIDS was first described in 1981, which makes it a recent historic phenomenon. Following its discovery in North America and then in Western Europe, around 1985 the epicenter was considered to be in Central Africa where the HIV infection rate was 4 or 5 percent in countries such as Zaire, Rwanda, Burundi, Uganda, and Zambia. Southern Africa, which would later experience the highest HIV infection rates in the world, was hardly affected at the time. Ten years later, in 1995, the virus had spread throughout the world with an important focus in Southeast Asia, mainly connected with commercial sex. At</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>HYPERENDEMIC HIV IN SOUTHERN AFRICA</title>
                  </title-group>
                  <fpage>28</fpage>
                  <abstract>
                     <p>Southern Africa is experiencing an exceptionally severe AIDS epidemic, with adult HIV prevalence rates of up to 27.4 percent in 2013 in Swaziland. The region accounts for over one third of all people living with HIV in the world, and the nine countries with the highest prevalence in the world are in Southern Africa. Given these sobering statistics, as well as a continued high spread of HIV, the situation can be characterized as “hyperendemic.” Nowhere else in the world is the impact of AIDS on multiple aspects of people’s lives and on societies greater.</p>
                     <p>The thirteenth International AIDS Conference in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>AIDS AS AN INTERNATIONAL POLITICAL ISSUE</title>
                  </title-group>
                  <fpage>46</fpage>
                  <abstract>
                     <p>AIDS has been about politics from the beginning. Politics in its original and noble sense: not party politics but that of societal choices, debates, and engagement in the AIDS response. This chapter will discuss the transnational dimension of the AIDS response, which was a new development in health, and may be a precursor of other global solidarity movements.</p>
                     <p>AIDS is obviously an infectious disease, but since its discovery in 1981 it has been perceived as distinct from other diseases, largely because of its connection with sex and drugs. However, its image is not only a matter of modes of transmission,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>A NEW TYPE OF TRANSNATIONAL CIVIL SOCIETY MOVEMENT</title>
                  </title-group>
                  <fpage>74</fpage>
                  <abstract>
                     <p>Since the fall of the Berlin wall and the major financial crisis, the weakening of states has become a “catch-all” theme.¹ Nations would no longer necessarily be the framework for public action. The economic crisis originating with the banking collapse in the United States has demonstrated the fragility of nations, often powerless to exercise a profound reform of the international financial system. Multinationals, international institutions, towns, regions, and civil society groups have become major actors in social and political life. For many reasons—colonization, maritime commercial exchange, development aid, infectious disease control, and epidemics—aspects of public health have historically</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>THE RIGHT TO TREATMENT</title>
                  </title-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>From the start of the AIDS epidemic finding an effective treatment, if not a cure, was a priority, given the nearly 100 percent mortality within ten years of infection with HIV. Let us not forget that in the early 1980s there were no effective treatments against viruses on the market, except acyclovir for herpes simplex and, partly, amantadine for influenza. Enormous public and private resources were invested in research for an effective HIV treatment. It shows that fast and well-coordinated efforts can lead to concrete results. Unfortunately the search for a vaccine has not yet been as successful. With the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>COMBINATION PREVENTION</title>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract>
                     <p>The search for simple solutions to stop the exponential spread of the HIV epidemic, as with the search for a vaccine, began early. However, a better understanding of the biology, transmission dynamics, and behavioral and structural determinants, combined with results of community and clinical trials, strongly suggest that only a<italic>combination</italic>of behavioral, biological, structural, and antiretroviral approaches can further reduce new HIV infections to low endemic levels—that is until an effective and affordable vaccine becomes widely available.¹ Just as for other public health activities, HIV prevention must be based as far as possible on scientific evidence. However, in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>THE ECONOMICS OF AIDS</title>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>As for any health issue, HIV infection has major economic dimensions. In addition to its social and public health aspects, the economic impact of AIDS, as well as the costs of controlling it, are important arguments to put it on national and international political agendas.</p>
                     <p>In general the poor are more affected by disease than the rich. It is the case for infant mortality, cardiovascular disease, diabetes, tuberculosis, and maternal mortality across the world. As is often the case, AIDS is different. The association between economic status and the disease seems more complex, probably because sex is the main route</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>PROMINENCE OF HUMAN RIGHTS</title>
                  </title-group>
                  <fpage>147</fpage>
                  <abstract>
                     <p>In 1987, from what was still an epidemic of unknown extent, Jonathan Mann, then-director of the WHO Special Programme on AIDS,¹ observed that for analytic purposes it was useful to consider AIDS as three distinct yet intertwined global epidemics. The first was the epidemic of HIV infection itself. The second, inexorably following the first but with a delay of several years, was the epidemic of AIDS. Unlike most infectious diseases, such as measles or yellow fever, which develop days or weeks after infection, AIDS may not occur until years or even decades after the initial infection. Finally, the third epidemic,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>THE LONG-TERM VIEW</title>
                  </title-group>
                  <fpage>160</fpage>
                  <abstract>
                     <p>When AIDS emerged in 1981, and throughout the 1980s, we implicitly believed that it would be a short-lived crisis. The question of when it would end was rarely discussed and for more than two decades there was essentially no consideration that HIV infection might be a long-term epidemic. For two decades a vaccine was promised in the next five years. Everything we have learned about the virus, what drives its spread and how to control it, tell us that HIV will be with us for a long time—decades, if not generations, depending on when an effective vaccine will be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.15</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>173</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">piot16626.16</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>185</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
