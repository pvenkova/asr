<body xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <p>Exposure to bloodborne pathogens is the most serious occupational health risk faced by healthcare workers (HCWs) worldwide.<sup>1</sup> Until the 1980s, however, exposure to blood was considered “part of doing business,” typically caused by HCW carelessness.<sup>2</sup> It was not until the epidemic of acquired immunodeficiency syndrome, and the first case of occupationally acquired human immunodeficiency virus (HIV) infection in 1984,<sup>3</sup> that occupational blood exposures moved to the forefront of concern.</p>
      <p>Globally, the cost of disease from occupationally acquired hepatitis B, hepatitis C, and HIV infection is high. Of 35 million HCWs worldwide, the World Health Organization estimates that approximately 3 million experience percutaneous injuries each year. Of those injured HCWs, 70,000 are likely to be infected with hepatitis B virus as a result of exposure, 15,000 with HCV, and 1,000 with HIV.<sup>4</sup> Although the majority of these occupational infections occur in developing countries where the prevalence rates of bloodborne pathogens are high, most documented infections are reported from industrialized countries, where surveillance is most commonly conducted.<sup>1</sup>
      </p>
      <p>Important progress has been made over the last decade in reducing HCWs’ exposure risk in the United States and other industrialized countries. Here, the use of personal protective equipment (PPE), including gloves, fluid‐resistant gowns, face masks and goggles, which has been shown to reduce exposure to blood and body fluids,<sup>5</sup>
         <sup>‐</sup>
         <sup>6</sup> is widespread. Vaccination against hepatitis B has significantly reduced the incidence of occupationally acquired hepatitis B virus infection.<sup>7</sup> In the United States, needlestick injuries have decreased markedly among HCWs following passage of the Needlestick Safety and Prevention Act in 2000, which mandated the implementation of safety‐engineered needles and other sharp devices.<sup>8</sup>
         <sup/>
         <sup>‐</sup>
         <sup>10</sup>
      </p>
      <p>Despite this progress, the operating room continues to rank as one of the highest‐risk hospital settings for percutaneous injury and for blood and body fluid (BBF) exposure.<sup>11</sup> As an occupational group, surgeons sustain more exposures than other physician specialty groups,<sup>12</sup> yet in the United States they have been among the slowest to adopt safety‐engineered devices.<sup>13</sup> In recent years, US surgeons have been the focus of increased efforts to bring about higher levels of compliance with safety mandates.</p>
      <p>Similar efforts are long overdue on behalf of surgeons practicing in sub‐Saharan African countries, where the prevalence of bloodborne pathogens is among the highest in the world and where healthcare resources are scarce. Reducing surgeons’ exposure risk should be a top priority in international efforts to protect Africa’s healthcare workforce. Our study seeks to address this important yet often overlooked group by documenting the frequency and circumstances of blood exposures in a group of African surgeons, and collecting data regarding their access to and use of protective equipment and preventive strategies for reducing occupational blood exposure.</p>
      <sec id="sc1">
         <title>Methods</title>
         <p>
            <italic>Participants. </italic>Survey forms were included with conference materials at the 2006 Pan‐African Association of Surgeons meeting held in Blantyre, Malawi. Several announcements were made during conference plenary sessions that encouraged practicing surgeons to complete and return the surveys. Returned surveys were deposited in a drop‐box located in the exhibit hall. Responses were anonymous.</p>
         <p>
            <italic>Survey items. </italic>The questionnaire solicited descriptive information from respondents (ie, country of practice, years in practice, annual number of surgical procedures performed, and surgical specialty), as well as information about the use of PPE and injury‐reducing practices and devices; hepatitis B vaccination status; and access to postexposure prophylaxis for HIV. Respondents described in detail their most recent sharp‐device injury sustained during surgery, including the procedure being performed, the device that caused the injury, circumstances of the exposure, and extent of blood contamination of the device. They also described their most recent blood or body fluid exposure, including the procedure being performed, type of body fluid involved, location of fluid contact, and PPE worn at time of exposure.</p>
      </sec>
      <sec id="sc2">
         <title>Results</title>
         <p>
            <italic>Demographic data. </italic>Of 110 surgeons who practiced in Africa, 84 returned completed surveys (76% response rate); respondents were from the following 14 sub‐Saharan African countries: Cameroon, Ethiopia, Ghana, Kenya, Madagascar, Malawi, Mozambique, Nigeria, Rwanda, South Africa, Sudan, Tanzania, Uganda, and Zambia. There was greater representation of surgeons from southeastern Africa. Overall, the mean number of years in practice of respondents was 16.8. The most common specialties among respondents were general surgery (reported by 46% of respondents) and orthopedics (reported by 37%).</p>
         <p>
            <italic>Use of protective practices and devices. </italic>Thirty‐nine percent of respondents had been fully vaccinated against hepatitis B virus. Of the remaining 61%, a total of 18% had not completed the 3‐dose vaccination series, and 43% had not begun the series (2% of those reported having antibodies). Most (89%) of the respondents indicated that they had access to HIV postexposure prophylaxis.</p>
         <p>More than one‐third (40%) of surgeons reported using the hands‐free technique for passing sharp instruments<sup>14</sup> (Figure); 31% used blunt suture needles in their practice. Use of tissue adhesives—an alternative to suturing that requires no sharp device—was reported by fewer respondents.<fig-group content-type="figgrp" id="fg1" position="float" seq="1">
               <label>Figure. </label>
               <caption>
                  <p>Use of safer devices and work practices to reduce bloodborne pathogen exposure risk among surgeons in sub‐Saharan Africa. Respondents could choose more than 1 safety practice or device.</p>
               </caption>
               <fig position="float" fig-type="figure">
                  <graphic xlink:href="fg1.tiff"
                           rotation="0"
                           alt-version="no"
                           mimetype="image"
                           position="float"
                           xlink:type="simple"/>
               </fig>
            </fig-group>
         </p>
         <p>
            <italic>Percutaneous injuries. </italic>Respondents estimated the number of percutaneous injuries, other sharp‐device injuries, and/or cuts sustained during the previous year. A total of 91% reported 1 or more percutaneous injuries (range, 0 to more than 20), and there was a mean (±SD) of <inline-formula>
               <tex-math notation="latex">
\documentclass{aastex}
\usepackage{amsbsy}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{mathrsfs}
\usepackage{pifont}
\usepackage{stmaryrd}
\usepackage{textcomp}
\usepackage{portland,xspace}
\usepackage{amsmath,amsxtra}
\usepackage[OT2,OT1]{fontenc}
\newcommand\cyr{
\renewcommand\rmdefault{wncyr}
\renewcommand\sfdefault{wncyss}
\renewcommand\encodingdefault{OT2}
\normalfont
\selectfont}
\DeclareTextFontCommand{\textcyr}{\cyr}
\pagestyle{empty}
\DeclareMathSizes{10}{9}{7}{6}
\begin{document}
\landscape
$3.1\pm 3.3$
\end{document}</tex-math>
            </inline-formula> percutaneous injuries per surgeon per year. For 61% of the incidents, the surgeons’ hands were in contact with the operative site when the injury occurred, a circumstance under which there is potential for both surgeon‐to‐patient blood contact and patient‐to‐surgeon blood contact. The device most frequently responsible for the injury was a suture needle (accounting for 77% of percutaneous injuries), and they most often caused injury during the act of suturing.</p>
         <p>
            <italic>Blood and body fluid exposures. </italic>Respondents estimated the number of BBF exposures to non‐intact skin or mucous membranes that they sustained during the previous year; 80% sustained 1 or more BBF exposures (range, 0‐17), with a mean (±SD) of <inline-formula>
               <tex-math notation="latex">
\documentclass{aastex}
\usepackage{amsbsy}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{mathrsfs}
\usepackage{pifont}
\usepackage{stmaryrd}
\usepackage{textcomp}
\usepackage{portland,xspace}
\usepackage{amsmath,amsxtra}
\usepackage[OT2,OT1]{fontenc}
\newcommand\cyr{
\renewcommand\rmdefault{wncyr}
\renewcommand\sfdefault{wncyss}
\renewcommand\encodingdefault{OT2}
\normalfont
\selectfont}
\DeclareTextFontCommand{\textcyr}{\cyr}
\pagestyle{empty}
\DeclareMathSizes{10}{9}{7}{6}
\begin{document}
\landscape
$4.2\pm 4.6$
\end{document}</tex-math>
            </inline-formula> exposures per surgeon per year. The body site most frequently exposed was the eyes (49% of exposures), followed by the hands (16%), torso (11%), and nose and/or mouth (9%). In 65% of incidents, the surgeon was exposed to blood, and in another 23% the surgeon was exposed to blood‐tainted body fluid. BBF exposures typically involved fluid making contact with an exposed area that was not protected by a barrier garment.</p>
         <p>In reporting the PPE typically worn during surgery, 82% of respondents indicated that they typically wore a gown; however, only 35% reported that they were wearing a gown at the time of their most recent BBF exposure, and of those, more than half were wearing cotton gowns. Only 29% of surgeons reported wearing eye protection, and of those who sustained BBF exposures to the eyes, 81% were not wearing goggles at the time of exposure. Slightly less than one‐fifth of eye exposures occurred while surgeons were wearing eyeglasses, which are not considered to be protective.</p>
      </sec>
      <sec id="sc3">
         <title>Discussion</title>
         <p>The rate of hepatitis B vaccination was alarmingly low. This finding was surprising, because many African surgeons receive training at their country’s expense. In addition to the importance of protecting the health of surgeons, countries have an economic interest in protecting their investment in the surgeons’ training. Hepatitis B vaccination should be required and provided to all HCWs who have direct contact with patients. Vaccination should take place during their training to achieve high levels of immunity early in their careers and remove worker discretion.</p>
         <p>HIV postexposure prophylaxis was more widely available than we expected. This is likely the result of internationally subsidized programs, and the long‐term availability of these antiretrovirals is uncertain. Therefore, primary prevention of blood exposure should remain a priority for protecting HCWs—not only to protect them from HIV but, like a universal vaccine, to protect them from all bloodborne pathogens, known and unknown.</p>
         <p>One of the most surprising findings was that one‐third of respondents reported using blunt suture needles—a higher percentage than reported in some industrialized countries; for example, in the United States.<sup>13</sup> This result suggests a high level of risk awareness, but it also indicates that a majority of African surgeons unnecessarily remain at risk of injury.</p>
         <p>Survey results also indicate that fluid‐resistant barrier garments are less frequently worn than cotton garments. Cotton gowns, which absorb fluid and hold it next to the skin, may be more harmful than protective, especially if the skin is not intact.<sup>15</sup> A fluid‐proof barrier, such as a plastic apron, could provide inexpensive frontal protection if worn in combination with a cotton gown. Fluid‐resistant gowns would be even more protective.</p>
         <p>Eye protection was significantly underused, despite the known possibility for the transmission of bloodborne pathogens through the eyes.<sup>16</sup>
            <sup/>
            <sup>‐</sup>
            <sup>18</sup> Standard eyeglasses provide inadequate protection, as confirmed by survey responses describing a common scenario in which blood that splashed on the forehead then dripped down into the eyes. Protective eyewear should have a seal above the eye to prevent this common occurrence.<sup>19</sup>
         </p>
         <p>The scarcity of adequate and safe surgical supplies is a major obstacle to African surgeons’ safety. Purchasing safety‐engineered devices and protective products may create additional expenses, and developing these items at cost‐neutral prices should be a goal of medical‐product manufacturers.</p>
         <p>Respondents’ frequent use of the hands‐free passing technique, using a “neutral zone,”<sup>14</sup> reflects the fact that they are motivated to minimize risk. This is a no‐cost practice that, if widely adopted, could reduce the risk of injury for all surgical personnel, not just surgeons. Opportunities to educate African surgical personnel about the use of this technique should be actively pursued. One possible way of achieving this goal would be to offer an open‐access, Internet‐based training program.</p>
         <p>Surgeons in sub‐Saharan Africa are keenly aware of the infection risks they face, and this survey identifies simple yet high‐yield opportunities to improve their protection against occupation exposures. HCWs are not infinitely replaceable, and they have a basic right to protection from preventable occupational diseases. Hospital administrators, national and international policy‐makers, and HCWs must work collaboratively to realize this right.</p>
      </sec>
   </body>