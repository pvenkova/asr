<body xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <p>The Comparative and International Education Society presented three awards at the society’s meeting at Stanford University on March 25, 2005. These awards represent outstanding accomplishments on the part of the recipients.</p>
      <sec id="sc1">
         <title>Gail P. Kelly Award for the Outstanding Dissertation in Comparative Education</title>
         <p>The Gail P. Kelly Award is presented to a comparative education dissertation that deals with issues of social justice and equity while, at the same time, exemplifying academic excellence, originality, and methodological, theoretical, and empirical rigor. The Kelly Award was presented for the first time in 1994. This year, Kelly Award committee members evaluated 13 dissertations that had been defended between July 2003 and August 2004.</p>
         <p>The 2005 Kelly Award was presented to Nancy Kendall, for her dissertation titled <italic>Global Policy in Practice: The “Successful Failure” of Free Primary Education in Malawi</italic>. Professor Kendall graduated from Stanford University in 2004 and currently teaches at Florida State University. Of her dissertation, one committee member commented:<disp-quote>
               <p>I found this to be an excellent, sophisticated study. … The unintended consequences of free primary education and, concomitantly, the particular alignment of state actors and donor organizations in approaching educational reform in Malawi have complications for many developing countries in Africa and elsewhere. This was an ambitious topic and it is presented lucidly by the author, in a solid theoretical framework that helps the reader to make sense of the “story” and of the contradictions inherent in a great deal of development work and educational reform.</p>
            </disp-quote>It is “difficult to imagine a setting more in need of truly progressive education reform,” another reviewer concluded, “and this study contributes to that direction.”</p>
         <p>Chris Bjork chaired the Kelly Award committee, which also included Helen Boyle, Peter Demerath, Abigail Harris, and Frances Vavrus.</p>
      </sec>
      <sec id="sc2">
         <title>George F. Bereday Award</title>
         <p>The George F. Bereday Award has been presented since 1981 for an outstanding article published in the <italic>Comparative Education Review</italic> during the previous year. This year’s Bereday committee members reviewed the 15 articles published in 2004 for their importance in shaping the field, analytic merit, policy implications, concern for theoretical constructs, and implications for future research.</p>
         <p>The Bereday Award for <italic>Comparative Education Review</italic> was presented to Jackie Kirk, for her article titled “Impossible Fictions: The Lived Experiences of Women Teachers in Karachi” (vol. 48, no. 4). The committee found Kirk’s article a “lively and engaging piece of writing,” a “valid exploration of the complex topic of teachers’ subjectivities in post‐colonial context,” and a “very good, clear and well‐researched [discussion] regarding women teachers in Pakistan.” Applying Valerie Walkerdine’s construct of “impossible fiction,” Kirk<disp-quote>
               <p>focuses on a topic that has been generally neglected by scholars and policymakers in our field. Kirk’s study on women teachers’ subjectivities challenges taken‐for‐granted gender assumptions (e.g., women teachers are best teaching girls), emphasizes the importance of including women in Education for All policy discussions, and makes a strong case for the need for more qualitative research on women teachers in developing country contexts.</p>
            </disp-quote>This study further distinguishes itself, one committee member concluded, in “underscor[ing] the value of feminist theory and research for improving education policy.”</p>
         <p>Chair Sangeeta Kamat was joined on this year’s Bereday Award committee by Birgit Brock‐Utne, Gustavo Fischman, Peter Mayo, and Heinrich Mintrop.</p>
      </sec>
      <sec id="sc3">
         <title>Joyce Cain Award for Distinguished Research on African Descendants</title>
         <p>The Joyce Cain Award, first presented in 2001, recognizes a comparative education article that explores themes related to people of African descent while demonstrating excellence and rigor in research and writing. This year the Cain Award committee considered eight nominated articles published in 2003 and 2004.</p>
         <p>The 2005 Cain Award was given to Comparative and International Education Society member Parfait Eloundou‐Enyegue, for his article (coauthored with Julie Davanzo) titled “Economic Downturns and Schooling Inequality, Cameroon 1987–1995,” <italic>Population Studies</italic> 57 (2003): 183–97. Cain Award committee chair Edith Mukudi praised Professor Eloundou‐Enyegue’s article as “a very impressive and well‐formed study.” Its</p>
         <p>
            <disp-quote>
               <p>literature review and presentation and analysis of data were informative, clear, and compelling. The methodology selected—working with large databases and longitudinal analysis—added to the quality of the study. Employing both quantitative and qualitative tools was the other added advantage in terms of showing both the depth and breadth of the problem of educational access in … Cameroon. Combining both economic and social variables in the analysis illuminated the multiple factors that determine educational development and outcomes. The authors concluded with important and appropriate implications for educational policy and practice in Cameroon. … The committee is delighted to be able to award the Joyce Cain Award to an article of such high research caliber and with such important policy implications.</p>
            </disp-quote>
         </p>
         <p>Joining Professor Mukudi on the Cain Award committee were M. Christopher Brown and Thomas Clayton.</p>
      </sec>
   </body>