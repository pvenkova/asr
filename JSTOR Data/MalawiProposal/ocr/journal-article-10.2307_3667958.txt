<plain_text><page sequence="1">- 507 - TABLE DES MATIERES DU VOLUME 55 INHOUD VAN DEEL 55 Ayobangira F. X. - Cf. Troupin G. &amp; - (299). Bamps P. - Red&amp;couverte d'Ancistrocarpus comperei Wilczek (Tilia- ceae) au Zaire et description complkmentaire de l'espbce ...... 500-501 Bienfait A., Waterkeyn L. &amp; Ermin L. - Importance des verrues foliaires silicifiees dans la systematique des Selaginella. Observations en microscopie electronique i balayage (MEB) ............ 73-81 Bridson D.M. &amp; Robbrecht E. - Further notes on the tribe Pavetteae (Rubiaceae) .......................... 83-115 Compere P. - Combinaisons nouvelles dans les Cyanophycees .. 487-489 Coudijzer J. - Cf. Goetghebeur P. &amp; - (207-259). Dechamps R., Mosango M. &amp; Robbrecht E. - Etudes syst6matiques sur les Hymenocardiaceae d'Afrique: la morphologie du pollen et l'anatomie du bois . ...................... 473-485 Demaret F. - Rudolf Wilczek (1903-1984) .......... 3-12 Demaret F. &amp; Empain A. - Bryum pseudotriquetrum (Hedw.) Gaert., Meyer &amp; Scherb. et sa variete binum (Schreb.) Lilj ..... 275-290 Dowsett-Lemaire F. - The forest vegetation of the Nyika Plateau (Malawi-Zambia): ecological and phenological studies ....... 301-392 Empain A. - Cf. Demaret F. &amp; - (275-290). Ermin L. - Cf. Bienfait A., Waterkeyn L. &amp; - (73-81). Geerinck D. - Recensio: Duvigneaud P., L'ecosysteme Foret . . 300 Geerinck D. - Orchidaceae nouvelles du Zaire ......... 501-502 Goetghebeur P. &amp; Coudijzer J. - Studies in Cyperaceae 5. The genus Bulbostylis in Central Africa ......... .......... 207-259 Heinemann P. - Hymenagarici novi .............. 492-494 Heinemann P. &amp; Rammeloo J. - Paxillaceae novae ...... 494-495 Lawalree A. - Esp&amp;ces nouvelles d'Afrique centrale du genre Thesium L. sous-section Fimbriata A.W. Hill (Santalaceae) ......... 17-25 Lawalree A. - Trois Lactuca (Asteraceae) nouveaux du Zai're .. 271-273 Lebrun J.-P. - La v&amp;ritable identite du Vernonia garambaensis (Asteraceae) ...........................498-499 Lejoly J. &amp; Lisowski S. - Le genre Calycolobus Willd. (Convolvula- ceae) en Afrique tropicale ................... . . 27-60 Lejoly J. &amp; Lisowski S. - Ipomoea robbrechtii, Convolvulacte nouvelle du Haut-Shaba (Zaire) .................. 61-62 Lejoly J. &amp; Lisowski S. - Ipomoea petitiana Lejoly &amp; Lisowski, Convolvulacee nouvelle du Haut-Shaba (Zaire) .......... 497-498 Leonard J. - Combinaisons nouvelles dans le genre Vaccaria Wolf (Caryophyllaceae) ........................297-298 Leonard J. - Note sur Astragalus ammophilus Karelin &amp; Kir. non M. Bieb. ex Besser (Papilionacee asiatique) ............ 489-491 Leonard J. - Note sur Prosopis cineraria (L.) Druce et P. koelziana Burkart (Mimosacees asiatiques) ................. 491-492 Leonard J. &amp; Mosango M. - Sur la repartition geographique d'Hymenocardia ripicola J. Leonard (Hymenocardiacee africaine) . . 469-471 Liben L. - Pierre Staner (1901-1984) ............. 13-16 Lisowski S. - Trois nouveaux taxons d'Helichrysum (Asteraceae) du Zaire ............................. 63-68 Lisowski S. - Cf. Lejoly J. &amp; - (27-60), (61-62), (497-498). Lubini A. - La for&amp;t marecageuse B Mitragyna stipulosa et Pycnan- thus marchalianus dans la region de Kisangani (Haut-Zaire) . . . . 393-420</page><page sequence="2">- 508 - Maquet P. - Note sur deux Asteraceae du Rwanda . ..... 69-72 Mosango M. - Cf. Leonard J. &amp; - (469-471). Mosango M. - Cf. Dechamps, - &amp; Robbrecht E. (473-485). Nyakabwa M. - Etude du groupement herbac6 secondaire a Paspa- lum virgatum dans la ville de Kisangani (Zaire) .......... 461-468 Ousted S. - A taxonomic revision of the genus Uebelinia Hochst. (Caryophyllaceae)................ .........421-459 Pichi Sermolli R.E.G. - A contribution to the knowledge of the Pteridophyta of Rwanda, Burundi, and Kivu (Zaire) - II ..... 123-206 Rammeloo J. - Cf. Heinemann P. &amp; - (494-495). Robbrecht E. - Two new species of Tricalysia (Rubiaceae) . .. 496 Robbrecht E. - Cf. Bridson D.M. &amp; - (83-115). Robbrecht E. - Cf. Dechamps R., Mosango M. &amp; - (473-485). Schiman-Czeika H. - Zwei neue Acanthophyllum-Arten (Caryophyl- laceae) aus der A. squarrosum-Verwandtschaft .......... 117-121 Schotsman H. D. - Les Callitriche L. de l'Afrique intertropicale continentale II. Description de deux nouvelles espbces ....... 291-296 Symoens J.-J. - Cf. Triest L. &amp; - (261-269). Triest L. &amp; Symoens J.-J. - Isozyme patterns and taxonomic position of a Central African population of Najas marina L. subsp. armata (Lindb. f.) Horn af Rantzien . .... ............. 261-269 Troupin G. &amp; Ayobangira F. X. - Combinaisons nouvelles parmi les Labiatae d'Afrique centrale ........ .......... 299 Waterkeyn L. - Cf. Bienfait A., - &amp; Ermin L. (73-81). Taxa, combinationes nominaque nova in vol. 55 (1985) publicata 503-505 DATES DE PUBLICATION PUBLICATIEDATA 55 (1/2): 1-300 (30-6-1985) 55 (3/4): 301-508 (31-12-1985)</page></plain_text>