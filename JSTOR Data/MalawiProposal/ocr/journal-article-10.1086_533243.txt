<body xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <sec sec-type="index_only">
         <title/>
         <p>
 [[START 03X0761G]]
 Review Essays 
 Hegemony and Culture in Historical Anthropology: 
 A Review Essay on Jean and John L. Comaroff's 
 Of Revelation and Revolution 
 SALLY ENGLE MERRY 
 IN THE RECENT RAPPROCHEMENT BETWEEN ANTHROPOLOGY AND HISTORY, one of the 
 most important projects is Jean and John L. Comaroff's study of the colonial 
 mission in South Africa. In this large, two-volume work spanning almost 200 years 
 and over 700 pages, the Comaroffs tell the history of the colonial encounter on the 
 South African frontier and detail its contemporary consequences.1 More a history 
 of culture than a history of persons or events, this work presents the story of the 
 changes in cultural meanings and practices taking place over time in a region of 
 radical political, economic, and religious transformation. Although it begins from 
 the role of the mission, the scope of the project extends into an exploration of 
 changing conceptions of agriculture, money, healing, fashion, architecture, house 
 furnishings, law, property, and subjectivity. This work contributes to the effort to 
 expand the terrain of data and methods of historical analysis and to re-theorizing 
 and historicizing culture. 
 By focusing on the way the unnoticed and undiscussed features of social practice 
 and meaning contribute to maintaining or changing relations of power, these two 
 volumes contribute to the move within history to expand the subjects relevant to 
 historical analysis and the ways of analyzing them. The volumes are less an 
 examination of events, persons, and narratives than of the cultural interstices of the 
 historical process. They track the exercise of power in the unexamined domains of 
 everyday social life. They have made an enormous impact on the field of 
 anthropology, particularly on the critical issue of the reformulation of the notion of 
 culture, the core concept of the discipline. Rethinking what culture means has 
 consumed the field for the last two decades, and these two books have spurred the 
 effort.2 Within anthropology, they have been widely praised for breaking new 
 ground theoretically as well as criticized for failing to present an adequate temporal 
 I am grateful to John L. Comaroff and Jane Collier for comments on an earlier draft of this essay. 
 1 Jean Comaroff and John L. Comaroff, Of Revelation and Revolution, Vol. 1: Christianity, 
 Colonialism, and Consciousness in South Africa, Vol. 2: The Dialectics of Modernity on a South African 
 Frontier (Chicago, 1991, 1997). 
 2 See, for example, James Clifford and George Marcus, eds., Writing Culture (Berkeley, Calif., 
 1986); Adam Kuper, Culture: The Anthropologists'Account (Cambridge, Mass., 1999). 
  
  
 460 
  
  
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Hegemony and Culture in Historical Anthropology 
  
  
 and narrative account of historical change.3 As I will argue, however, this critique 
 fails to appreciate the contribution a focus on hegemony makes to understanding 
 historical change and the historicity of culture. 
 This review will consider two questions: First, how has this work expanded the 
 approaches and materials available for historical investigation? The focus on 
 hegemony offers an approach to understanding power within everyday social life as 
 it is embedded in cultural understandings and practices. Second, how has this work 
 contributed to the re-theorization of that continually vexing yet anthropologically 
 central concept, culture? In recent years, this term has taken on new significance in 
 related fields, such as cultural studies, and has become a critical feature of identity 
 politics. Yet, as everyday discourse has rendered the term more fixed and bounded, 
 anthropological theory has developed a more flexible, contested, and unbounded 
 conception. 
 Volume 1 was published in 1991 and Volume 2 in 1997. In many ways, their 
 innovations in method and theory have been taken up in subsequent years in both 
 history and anthropology. When assessing their impact, it is important to remember 
 that many of the ideas that seemed novel when they were first published are now 
 taken for granted and that the turn to this way of understanding history and culture 
 has expanded during the 1990s. 
 THIS WORK IS NOT A HISTORY OF SOUTH AFRICA, nor does it claim to be one. It is a 
 history of the making of a new hegemony on the South African frontier out of the 
 cultural repertoires of the Southern Tswana, the Nonconformist missionaries, and, 
 in the background, the settlers, the mining companies, the British imperial 
 government, and Britain itself. It is a complement to existing histories, not a 
 replacement for them. The subject of investigation is not actors, events, and 
 narratives but the creation of unrecognized and unarticulated cultural patterns, 
 modes of thinking, and physical representations that reflect or justify particular 
 relations of power. The empirical basis is not only texts such as letters, reports, 
 novels, and poetry but also buildings, clocks, village arrangements, rituals, and a 
 whole series of materializations of everyday life. These are the forms of evidence 
 typically considered within anthropology and cultural studies, with its emphasis on 
 the power of discourses and representations. The Comaroffs' study examines the 
 unspoken, everyday practices and categories that control thought and action 
 without being noticed. Thus this work needs to be read in conjunction with more 
 conventional historical accounts. 
 The core analytical concept on which both volumes rest seems to be that of 
 hegemony. The Comaroffs carefully define this concept in Volume 1, and the 
 meaning of this term governs the choice of data and the shape of both volumes. 
 Hegemony refers to the unspoken, taken-for-granted assumptions about person 
 and action that shape social life even though they are not brought to consciousness. 
 There is thus an implicit and unrecognized quality to those meanings and practices 
 3 Donald L. Donham, "Essay: Thinking Temporally or Modernizing Anthropology," American 
 Anthropologist 103 (2001): 134-50, quote p. 138. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 461 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Sally Engle Merry 
  
  
 that fall under the rubric of hegemony. The Comaroffs define hegemony as "that 
 order of signs and practices, relations and distinctions, images and epistemolo- 
 gies-drawn from a historically situated cultural field-that come to be taken-for- 
 granted as the natural and received shape of the world and everything that inhabits 
 it." Once such signs and meanings become recognized and explicitly articulated, 
 they fall into the domain of ideology and are more susceptible to contestation.4 
 What is hegemony and what is ideology is not fixed; the boundary is fluid. Ideas and 
 practices move across it constantly, both at the level of individual consciousness and 
 in more collective cultural practice. An individual may have experiences that make 
 him or her aware of hegemonic relations of power, which then immediately become 
 contested. Such transformations in consciousness contribute to practices of resis- 
 tance. Indeed, perhaps the very act of reading these books shifts hegemonic 
 understandings to ideological ones, opening them to challenge. Such a shift seems 
 compatible with the intent of the authors, whose goals include making hegemonic 
 understandings visible and unmasking the processes of their making and unmaking 
 over time. 
 The focus on hegemony is designed to uncover the way such cultural under- 
 standings shape relations of power, making domination seem natural and suppress- 
 ing forms of resistance. Exposing the implicit and unstated aspects of cultural life 
 in order to defamiliarize them is an approach with deep roots in anthropology, 
 apparent in such classic works as Horace Miner's study of the "Nacirema" (aka 
 Americans).5 In the 1950s, Miner described this culture's fixation with bathrooms, 
 an insight reinforced a half-century later by the proclivity to build expensive new 
 houses with more bathrooms than bedrooms. What the Comaroffs add to the 
 anthropological process of revealing the apparently natural as cultural is their focus 
 on power. They draw on a Marxist tradition through Antonio Gramsci and 
 Raymond Williams as well as the Birmingham school and Michel Foucault. They 
 claim that, in understanding historical change, it is critical to see how people come 
 to think of themselves and their everyday social relationships in new ways so that 
 transformation occurs not only by force but also by persuasion. Their concern is not 
 how or why changes happen but how they are understood by variously positioned 
 actors. Political economy remains crucial, but culture, symbolism, and ideology are 
 also fundamental to the domain of power; thus, for example, the Christian mission 
 acts in conjunction with material forces rather than separate from them.6 
 As Foucault has demonstrated in his studies of the genealogies of major modern 
 institutions such as the prison and the mental institution, power exists in the 
 unrecognized corners of social life as well as in more explicit and conventionally 
 understood locations. These hidden domains are of central concern to the 
 Comaroffs. There has been increasing attention to these locations and their 
 implications for power relations in historical as well as anthropological research 
 over the past two decades, inspired in part by Foucault's work. On the South 
 African frontier, where settler interests in lands and mines were conspicuous, this 
 approach highlights less obvious areas such as the Christian mission or architectural 
 4 Comaroff and Comaroff, Of Revelation and Revolution, quote p. 1: 23; see also 1: 24. 
 5 Horace Miner, "Body Ritual among the Nacirema," American Anthropologist 58 (1956): 503-07. 
 6 Comaroff and Comaroff, Of Revelation and Revolution, 1: 8. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 462 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Hegemony and Culture in Historical Anthropology 
  
  
 styles of villages. Indeed, as the Comaroffs note at one point, the surprising feature 
 of the missionaries is that they did so much with so few resources.7 In my research 
 on missionaries in Hawai'i, I was similarly surprised at the extent of missionary 
 influence despite the lack of support for the mission from the sending community 
 as well as the poverty and sense of isolation and difficulty the missionaries felt.8 In 
 hindsight, their transformative power seems enormous, but at the time, they felt 
 minimally supported for a project in which their strongest ally was God. Dressed in 
 hand-me-down clothes, living in grass houses, desperate to get slates to equip 
 schools, the missionaries in Hawai'i, as in South Africa, did not experience 
 themselves as the potent force for creating a new hegemony that they now appear. 
 The Comaroffs' central approach is to explore power where it does not seem to 
 reside. They examine power in the domain of culture, in understandings of the self, 
 the community, and the moral order. For those who expect a story of the alienation 
 of land, the imposition of new regimes of labor, and the expansion of empire 
 through conquest, the analysis of such cultural domains might seem to miss the 
 point. However, they take the political and economic framework as given, recog- 
 nizing and acknowledging it while exploring another dimension of power, that 
 residing in the sphere of cultural meaning and practice. They examine how these 
 changes became thinkable and reasonable to variously positioned actors relying on 
 particular cultural categories of thought and action. "Nakedness," for example, 
 meant not nude nobility to the missionaries but "degeneracy and disorder, the wild 
 and the wanton, dirt and contagion," fueling their insistence that the Tswana wear 
 clothes.9 
 It is not easy to study the unspoken and unrecognized facets of culture in the 
 past, of course. To claim that one has uncovered the hegemonic and exposed it for 
 the reader involves substantial methodological and empirical difficulties, of which 
 the Comaroffs are well aware. One must examine implicit meanings, since as soon 
 as they become explicit, they move into the domain of ideology. While still 
 important, they then become openly subject to contestation and debate and lose 
 some of their power to naturalize social relationships and inequalities. Analysis 
 depends on interpretation, since by definition hegemonic beliefs and practices do 
 not speak directly. Such an interpretive practice is inevitably subject to competing 
 ways of analyzing the same material. Yet examining these domains of meaning and 
 practice provides a productive approach to understanding historical change. 
 Nevertheless, the reader might wonder if, in the midst of all this contestation 
 over signs and meanings, there is something missing. Ideas and cultural practices 
 rather than institutional structures emerge as powerful. The life stories of particular 
 people whose visions and decisions shaped events are told only in passing. The 
 larger changes in political economy, such as the pressure on land by settlers and the 
 rise of a mining economy with a pattern of migratory wage labor, are rhetorically 
 present: there are frequent references to capitalism and the British state, to the 
 mineral revolution, to the consequences of land alienation and the emiseration of 
 the peasantry. But they are not the central concerns of the volumes. To some extent, 
 7 Comaroff and Comaroff, Of Revelation and Revolution, 1: 9. 
 8 Sally Engle Merry, Colonizing Hawai'i: The Cultural Power of Law (Princeton, N.J., 2000). 
 9 Comaroff and Comaroff, Of Revelation and Revolution, 2: 224. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 463 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Sally Engle Merry 
  
  
 this is a product of their focus on the early nineteenth century-on the period 
 before colonial control was established and capitalism produced land alienation and 
 migratory wage labor. The Comaroffs are interested in how the meanings and 
 practices brought by the mission prefigured these subsequent economic and 
 political changes. 
 But it is also because the Comaroffs are exploring the cultural domain of power: 
 hegemony rather than political economy. It is ironic that scholars with a strong 
 Marxist background have written books that appear somewhat bereft of analysis of 
 the political economy of the frontier, but I suspect that this occurred because the 
 changing political economy was part of the taken-for-granted world from which 
 they wrote. It is not that political economy is absent. There are two chapters in 
 Volume 2 that focus on changes in agriculture and in conceptions of wealth and 
 money, and the Comaroffs always seek to join material and ideological dimensions 
 of power. But, as in cultural studies scholarship, those domains of behavior subject 
 to analysis are more often representations and meanings than statistics on land 
 ownership or migration patterns. They are asking how domination was made 
 thinkable: by what categories of person and action the social relations of the 
 frontier in the transition to industrial capitalism and the apartheid state were 
 understood and came to seem justifiable to various actors. Clearly, race and gender 
 are key categories, but so are those of the saved, the sinner, and the productive 
 worker. How those relations were imagined and how the developing inequalities of 
 the frontier were justified and normalized is the central concern of these books. In 
 this sense, the focus of analysis is less about what happened than what kinds of 
 meanings were circulating and how those meanings shaped events. Donald Donham 
 complains that there is too little agency in the accounts of the Tswana,10 but the 
 relative under-emphasis on individual actors seems reasonable given the authors' 
 concern with uncovering hegemony in the making. Agency does emerge at the 
 ruptures and breaks in consciousness of various groups and of individuals. 
 Is there sufficient focus on the inevitable incompleteness of hegemony and on 
 forms of resistance? Theories of hegemony acknowledge the continual emergence 
 of counter-hegemonies and resistance, a point the Comaroffs reiterate. They note 
 that hegemony is always being made and can be unmade, that it is never total, that 
 it is "a process as much as a thing."' Their interest in hegemony and their 
 recognition that it is part of a contested domain of culture requires that there be 
 space for some challenge to that power, some way of thinking about the fractures 
 in hegemony. They include under the category "resistance" organized protest, 
 explicit movements of dissent, gestures of tacit refusal, and "gestures that sullenly 
 and silently contest the forms of an existing hegemony." Contradictions in 
 consciousness and the consciousness of contradictions can be the source of ever 
 more articulate resistance: "That is why the history of colonialism, even in the most 
 remote backwaters of the modern world, is such a drawn out affair, such an intricate 
 fugue of challenge and riposte, mastery and misery."'2 For example, the evangelical 
 message of the missionaries became part of the emerging hegemony, yet it also gave 
 10 Donham, "Essay." 
 11 Comaroff and Comaroff, Of Revelation and Revolution, 1: 25. 
 12 Comaroff and Comaroff, Of Revelation and Revolution, 1: 31, 1: 26. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 464 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Hegemony and Culture in Historical Anthropology 
  
  
 rise to new forms of consciousness that sparked forms of resistance that ultimately 
 became part of black consciousness and the fight against apartheid.13 The Comar- 
 offs' sense of culture as open, contested, and transforming lends itself to theorizing 
 resistance as a form of counter-hegemony within both local and imported cultural 
 understandings. 
 Yet the analysis of resistance plays a relatively small role in Volume 1 and seems 
 virtually to disappear in Volume 2. The new forms of capitalism and Christianity 
 seem readily adopted by the Tswana, albeit to varying degrees. The new Tswana 
 elites, who have the means to resist, seem most eager to adopt Christianity, 
 European styles of housing and furniture, and hats and skirts. Indeed, the 
 hegemony of the idea of civilization, in the British mode, seems relatively 
 uncontested. Yet it is clear that the seeds of the anti-apartheid struggle were laid 
 in these encounters across the colonial frontier. The authors clearly document the 
 emergence of forms of political consciousness, ideas of rights, and Christian 
 concepts of self that were fundamental to the emerging political opposition. The 
 two volumes take for granted the connection between the shifting consciousness on 
 the historical frontier and the resistance movements of the future but do not 
 develop the linkages. 
 The concept of resistance has proved difficult to use, even more than hegemony, 
 since in some ways almost everything can be considered resistance. Like hegemony, 
 resistance is a matter of interpretation. There have been debates about whether 
 resistance requires consciousness, whether it needs to be collective, whether it 
 requires a vision of justice, or whether the concept can be extended to include 
 actions based on an inchoate sense of discontent and non-cooperation. The 
 Comaroffs seem to adopt a relatively broad definition that does not require 
 organized social action or an explicit political program. But this broad definition 
 could include a vast array of behavior from painting graffiti on the walls to refusing 
 to do schoolwork. Without a specific political framework against which resistance 
 takes place, the concept becomes incoherent. It needs to be understood within a 
 particular system of power in which the relatively powerless engage in practices that 
 challenge dominant groups in the name of an alternative vision of social justice. 
 Although the graffiti artist and the balky student could be engaging in resistance 
 under this definition, placing their behavior in a political framework distinguishes 
 non-cooperation from actions that constitute resistance against a system of power. 
 The Comaroffs assume rather than elaborate the particular structures of power 
 within which resistant behavior occurs. Further explication would help differentiate 
 resistance from other forms of uncooperative behavior and emphasize its political 
 significance. They clearly assume that the reader is familiar with the anti-apartheid 
 movement, which they see as the legacy of these earlier social processes. Yet, 
 although they have laid the groundwork for that analysis, they do not draw out these 
 connections. Volume 3, which will focus on schooling, promises to make this 
 connection explicit. 
 13 See Comaroff and Comaroff, Of Revelation and Revolution, 1: 12. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 465 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Sally Engle Merry 
  
  
 IN THE LAST TWO DECADES, there has been great ferment within anthropology, as the 
 concept of culture developed over the first eighty years of the discipline has come 
 under fire. The notion of a bounded, consensual, value-based system of social life 
 that exists in an integrated and relatively static form was very influential until at 
 least the 1950s. Anthropologists did not study problems such as the colonial 
 frontier; instead, they focused on studying "pristine" societies that were untouched 
 by colonial influences or, if they had been influenced, they were treated as if they 
 had not. The forces of change were subtracted in order to produce an imagined 
 primitive, much as the British colonial government of the early twentieth century 
 lionized a past age of Africa in which "traditional" society was homogeneous and 
 unchanged.14 This perspective blended with the sense that those Africans who had 
 learned urban ways were somehow degenerate, less moral and worthy, detached 
 from their "real" culture. Studies of detribalization emphasized the chaotic and 
 degraded lives of Africans living in the towns in the 1940s and 1950s.15 Similarly, 
 Hawaiians living in urban areas and adapting to new ways of life did not warrant 
 anthropological attention. Instead, they were worthy of scholarship only in remote 
 rural areas or where their way of life could be reconstructed. 
 Anthropology is now struggling to think of culture in more flexible ways, as 
 unbounded, changing, contested, and as rooted in practices and habits as well as 
 ideas and values. Not only are there always flows of new ideas, perspectives, and 
 practices, but there are also, within any group, contests over meanings and action. 
 Those in power may use claims to cultural authenticity to force their ideas on 
 others. Subordinated groups may seize other cultural arguments to contest those 
 claims-arguments derived either from contradictions within a society or provided 
 by newcomers or those who have traveled elsewhere. Such processes of reformu- 
 lation, argumentation, and change are fundamental to any social group, although 
 the rate and extent of contestation may vary. 
 This set of ideas was developing in anthropology during the 1980s and 1990s, but 
 the Comaroffs' books offer one of the most thorough and sophisticated exemplars 
 of the new approach and were important to the emergence of this perspective in the 
 1990s. Instead of examining a reconstructed "culture," they focus on the colonial 
 encounter itself, locating their study on the frontier between Tswana life and British 
 imperialism and capitalism. Their focus is the colonization of consciousness and the 
 consciousness of colonization, in which the making of modern South Africa "has 
 involved a long battle for the possession of salient signs and symbols, a bitter, drawn 
 out contest of conscience and consciousness."16 One of the questions that concerns 
 them is why the Tswana went along with the mission enterprise, the transformation 
 in their dress, their habitations, and their ideas of property and propriety under the 
 influence of the civilizing mission. The answer involves thinking of culture in terms 
 of consciousness as well as examining the processes by which new relations of power 
 become accepted and taken for granted. The Comaroffs define culture as "the space 
 14 Martin Chanock, Law, Custom, and Social Order: The Colonial Experience in Malawi and Zambia 
 (Cambridge, 1985). 
 15 For example, Monica Wilson, The Analysis of Social Change (Cambridge, 1968). 
 16 Comaroff and Comaroff, Of Revelation and Revolution, 1: xi, 1: 4. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 466 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Hegemony and Culture in Historical Anthropology 
  
  
 of signifying practice, the semantic ground on which human beings seek to construct 
 and represent themselves and others-and hence, society and history."17 
 Ironically, new theories of culture are developing in anthropology just as the 
 older concepts of culture are becoming more central to popular public discourse. 
 The idea of culture in a more homogeneous sense has been absorbed into identity 
 politics, so every group now has its own "culture" that defines its essence. Social 
 groups are often differentiated on the basis of their distinct "cultures"-of religion, 
 race, ethnicity, sexuality, region, or nation. Women are frequently the carriers of 
 this culture through their dress, deportment, and acquiescence to practices of 
 marriage and family. The concept of culture in common parlance is the homoge- 
 neous, static, consensual, and holistic model of culture from anthropology's past. 
 When governments use the guarding of culture to excuse their failure to protect the 
 human rights of vulnerable populations such as women or racial minorities, for 
 example, they draw on this notion of culture. Some governments, in hearings about 
 their compliance with the UN's Convention on the Elimination of All Forms of 
 Discrimination Against Women (CEDAW), claim that they are unable to achieve 
 gender equality despite their best efforts because women are oppressed by 
 patriarchal "culture."'8 This concept precludes attention to particular legal con- 
 structions of marriage, divorce, inheritance, and custody and conceals a govern- 
 ment's failure to provide schools, health care, and employment opportunities for 
 women. It ignores variations in women's situations based on class, ethnicity, race, 
 region, religion, and many other factors. 
 In another example of the popular usage of culture, indigenous peoples confront 
 resistance to their claims to resources and reparations when they are unable to 
 prove that they possess an authentic "culture."19 If they have experienced cultural 
 change, a normal pattern for all social groups, they are often disqualified from 
 making these claims. Ironically, the concept of culture developed by anthropologists 
 in the early twentieth century to defend the rights of small-scale communities to a 
 way of life at odds with the civilizing project of nineteenth and twentieth-century 
 colonialism and its Christian missionaries has been appropriated for the very 
 different political project of resisting progressive reforms and ignoring indigenous 
 claims to reparations. 
 The entry of a major piece of scholarship on colonialism and historical 
 anthropology into the 1990s debate about what culture is and what role it plays in 
 contemporary political struggles was a welcome contribution. The Comaroffs have 
 made three important innovations. First, they used their detailed ethnography of 
 the Tswana to promote a conception of culture that incorporates historical change 
 and contestation over cultural meanings and practices. Because their work takes a 
 historical perspective, it challenges the principles of consensus and homogeneity 
 inherent in the old anthropological definition of culture. It foregrounds change and 
 17 Comaroff and Comaroff, Of Revelation and Revolution, 1: 21. 
 18 Established by the United Nations in 1979, this convention has been called a bill of rights for 
 women. Its signatory countries number 170 to date. See http://www.un.org/womenwatch/daw/cedaw. It 
 holds hearings every year at which signatory countries present periodic reports on their compliance 
 with the convention. 
 19 James Clifford, The Predicament of Culture: Twentieth-Century Ethnography, Literature, and Art 
 (Cambridge, Mass., 1988). 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 467 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Sally Engle Merry 
  
  
 conflict, although, as they point out, not everything is contested and changing. 
 There are areas of social life that remain relatively untouched. Although many 
 anthropologists have turned to more historical work since the 1980s,20 the 
 Comaroffs have contributed very significantly to this approach by providing a 
 masterful and theoretically sophisticated examination of a single site over one 
 hundred years placed within the longer time span of the eighteenth to twenty-first 
 centuries. 
 Their second innovation is the examination of the colonial frontier, a site of 
 intersection between separate cultural worlds, rather than the places where such 
 worlds originate. They focus on points of dialectic and change rather than stasis, 
 examining the shifts in both the frontier and the metropole. For example, they draw 
 intriguing parallels between the image of the African peasant with his or her chaotic 
 world and that of the British poor in London. Each side reshapes the other, 
 although what constitutes a "side" is also a complicated issue, since each is multiply 
 constituted among a group of actors who have quite different interests and 
 perspectives.21 This approach is perhaps more novel in anthropology than history. 
 They describe this historical process as a "long conversation" between the Southern 
 Tswana and the Nonconformist missionaries, emphasizing the dialectical, iterative 
 character of change.22 Although they detail the imbalance in the interaction, this 
 metaphor deemphasizes its inequalities of power. However, it does usefully 
 highlight the way each side contributes to producing the conversation itself and the 
 possibility of misunderstandings. As I have shown in a study of American 
 colonialism in Hawai'i, each side only partially understood the other and the 
 implications of the new practices and ideas they were adopting from one another, 
 leading to unanticipated and undesired trajectories of social change. This was 
 particularly significant for the Hawaiian king and chiefs who adopted American 
 legal institutions.23 The Comaroffs analyze similar misrecognitions in the "conver- 
 sations" on the South African frontier. 
 Their third innovation is the focus on domains of social life that fall into the 
 category of the taken for granted and the everyday, such as practices of agriculture, 
 housing, dress, health and hygiene, furnishings, domesticity, or healing. Many of 
 these domains have been the subjects of previous historical and anthropological 
 studies, of course; the Comaroffs' innovation was connecting them to changing 
 relations of power and cultural meaning. They examined these domains in terms of 
 the creation of new relationships of power, new ways of imagining and therefore 
 accepting the inequalities produced by the impact of capitalism and colonialism, 
 and new understandings of gendered, racialized, and ethnicized persons. For 
 example, the Comaroffs explore the remaking of Tswana selves through two 
 competing theories of personhood, one based on law and individual property 
 ownership and the other on primordial sovereignty. The first defined persons by 
 citizenship and rights, the second by membership in a "traditional," "customary" 
 20 For example, Eric R. Wolf, Europe and the People without History (Berkeley, Calif., 1982); 
 Marshall Sahlins, Islands of History (Chicago, 1985); June Starr and Jane F. Collier, eds., History and 
 Power: New Directions in the Study of Law (Ithaca, N.Y., 1989). 
 21 Comaroff and Comaroff, Of Revelation and Revolution, 2: 23-24. 
 22 Comaroff and Comaroff, Of Revelation and Revolution, 2: 63. 
 23 Merry, Colonizing Hawai'i. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 468 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Hegemony and Culture in Historical Anthropology 
  
  
 political community.24 Both provide the basis for contemporary political mobiliza- 
 tion, the first as national political actors, the second as members of primordial 
 ethnic groups. The focus on reading relations of power through the minutiae of 
 everyday life, including its artifacts and habits, is one way to deal with the common 
 problem in colonial research that the Europeans committed their experiences and 
 reactions to writing far more extensively than did many of the people they 
 colonized. This has created imbalances in the extent to which we are able to learn 
 the story from all sides of the colonial encounter.25 The Comaroffs are quite aware 
 of this issue and seek, by relying on material as well as textual data, to provide a 
 more balanced account.26 
 Despite their commitment to seeing these interactions as multifaceted and their 
 insistence on the importance of escaping the binary of two cultures in contact,27 
 there is a tendency to slide into a dualistic analysis. The Comaroffs seek to make 
 sociological analyses that recognize the hybridity and complexity of these intersec- 
 tions while examining the representational practices that construct dualities. 
 Sometimes, they emphasize internal variations, as in their analysis of the Tswana 
 response to the new rhetoric of rights: "Different fractions of the population 
 reacted differently toward the conception of property essayed by the civilizing 
 mission, just as they had in respect of other matters social and cultural, spiritual and 
 material. Still, no one could escape its implications." Yet sometimes the text 
 describes the confrontation as a binary between African and European ways of 
 acting. The book juxtaposes, for example, "On one side, a European sensibility 
 according to which, broadly speaking, the more specific the use to which something 
 was put, the more refined the object, the more cultured the practice, the more 
 civilized the person. On the other, the old Tswana penchant for multi-functionality, 
 which associated versatility and plasticity with use-value, social work, and, often, 
 beauty. These two poles-both tendencies, of course, neither of them simple lived 
 realities-charted the terrain of everyday practice."28 The chapter on money 
 contrasts the Tswana logic of value focused on cattle with that of the missionaries 
 based on money.29 It also describes how beads formed a hybrid system between 
 them, but the analysis is largely of a dualistic opposition. Despite the Comaroffs' 
 recognition that the Europeans were a multifaceted group, with tensions between 
 merchants, missionaries, and settlers,30 between Nonconformists and Anglicans, 
 between British and other nationalities, these groups are often not differentiated. 
 Moreover, although the Comaroffs carefully document the increasing stratification 
 taking place among the Tswana, with chiefly families adopting more of the housing 
 style and dress of Europeans while the poor retained their previous modes of dress, 
 they are sometimes treated as one and the same. 
 The tendency to present situations in terms of binary oppositions, despite the 
 Comaroffs' commitment to seeing them in more complex ways, reveals the power of 
 24 Comaroff and Comaroff, Of Revelation and Revolution, 2: 369-70. 
 25 See Greg Dening, The Death of William Gooch: A History's Anthropology (Honolulu, 1995). 
 26 Comaroff and Comaroff, Of Revelation and Revolution, 2: 40-53. 
 27 For instance, Comaroff and Comaroff, Of Revelation and Revolution, 2: 367. 
 28 Comaroff and Comaroff, Of Revelation and Revolution, 2: 384, 2: 270. 
 29 Comaroff and Comaroff, Of Revelation and Revolution, 2: 190. 
 30 See, for instance, Comaroff and Comaroff, Of Revelation and Revolution, 2: 184-85. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 469 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]
 [[START 03X0761G]]
 Sally Engle Merry 
  
  
 the old concept of culture, the ease with which one can slip into thinking of culture 
 in this way. This concept of culture informs the reader, as he or she imports the idea 
 into the text, as much as the sources that are available to a scholar of colonialism. 
 Many who wrote about their experiences on the frontier took such dualisms for 
 granted. The Comaroffs have made enormous progress in articulating another way 
 of thinking about culture, a way of linking it to power through their focus on 
 hegemony and their critique of assumptions of consensus. They constantly seek to 
 move beyond these representational practices as they provide sociological analyses. 
 Nevertheless, it is easy for both writer and reader to slip into the old usage, to 
 organize the complexities of social life along the lines of two holistic cultures in 
 confrontation. 
 The concept of culture that the Comaroffs-along with increasing numbers of 
 anthropologists and historians-are advocating is a major shift from that in 
 common usage and from past practice in both disciplines. It is clear that this is now 
 becoming the dominant theory of culture, but its deployment poses challenges. The 
 Comaroffs have initiated a critically important project in these two volumes, one 
 that has flourished since the 1990s. The disciplines of anthropology and of history 
 have both been enriched by this refashioned understanding of culture. 
 Sally Engle Merry is a professor of anthropology at Wellesley College. Her 
 work in the anthropology of law focuses on law and culture and on law and 
 colonialism. She has written Colonizing Hawai'i: The Cultural Power of Law 
 (2000), The Possibility of Popular Justice: A Case Study of American Community 
 Mediation (co-edited with Neal Milner, 1993), Getting Justice and Getting Even: 
 Legal Consciousness among Working-Class Americans (1990), and Urban Dan- 
 ger: Life in a Neighborhood of Strangers (1981). Merry is currently writing a book 
 on culture and the regulation of violence against women within the interna- 
 tional human rights system. She is past president of the Law and Society 
 Association and the Association for Political and Legal Anthopology. 
  
  
 AMERICAN HISTORICAL REVIEW 
  
  
 470 
  
  
 APRIL 2003 
  
  
 
 
 [[END 03X0761G]]

</p>
      </sec>
   </body>