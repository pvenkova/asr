<plain_text><page sequence="1">Recent literature on bryophytes?111(3) William R. Buck Institute of Systematic Botany, The New York Botanical Garden, Bronx, NY 10458-5126, U.S.A. e-mail: bbuck@nybg.org Bruce Allen Missouri Botanical Garden, P.O. Box 299, St. Louis, MO 63166-0299, U.S.A. e-mail: bruce.allen@mobot.org Ronald A. Pursell Department of Biology, 208 Mueller Laboratory, The Pennsylvania State University, University Park, PA 16802-5301, U.S.A. e-mail: raplO@psu.edu M. .A. .A. Ahrens, M. 2006. Verbreitung, ?kologie und Vergesellschaftung der Laubmoose Rhynchostegiella jacquinii und R. curviseta im Oberrheingebiet. Carolinea 63: 51-70. -. 2006. Zur Entwicklung von Moosen in einigen Dauerfl?chen im Kraichgau (S?dwestdeutschland). Carolinea 63: 29-49. Aicardi, O. (comp.). 2007. Contribution ? l'inventaire de la bryoflore fran?aise. Bulletin de la Soci?t? Botanique du Centre-Ouest n.s. 38: 327-332. Akerstrom, H. 2007. Bl?mossa Leucobryum glaucum i Uppland. Myrinia 17: 73-81. [In Swedish with English abstract] Andersson, M. 2007. Nordish Bryologisk F?rening 40 ?r 2006? ?terbesok i V?stmanland. Myrinia 17: 96-104. [In Swedish with English abstract; on the field trip associated with the 40th anniversary of the Nordic Bryological Society] Anonymous. 2008. Additions to the Wyoming bryophyte flora. Castilleja 27(1): 5. [Reports Sphagnum spp. and Warnstorfia tunarae from Evansia articles] Asakawa, Y. 2007. Scent substances of bryophytes. Bryological Research 9: 198-203. [In Japanese with English abstract] Azuelo, A. G. 2006. [Abstract] Bryophyte flora of Mt. Malindang, Misamis Occidental, Philippines. Page 136. In V. B. Amoroso (ed.), Proceedings of the 2n Symposium on Long-Term Ecological and Biodiversity Research in East Asia Region. Central Mindanao University, Bukidnon, Philippines. Baisheva, E. Z. 2007. The diversity of bryophytes in natural ecosystems: approaches to their studying and conservation. Uspekhi Sovremennoi Biologii 127: 316-333. [In Russian with English abstract] Bakalin, V. A. 2006. Liverworts of Kronotsky State Nature Reserve (Kamchatka Peninsula). Botaniceskij Zhurnal (St. Petersburg) 91: 871-879. [In Russian with English summary; Schistochilopsis laxa and Cephalozia grimsulana new to Kamchatka] Bates, J. W., H. W. Matcham 8c F. Lara. 2007. Dialytrichia fragilifolia (Bryopsida: Pottiaceae) in Berkshire and Caernarvonshire, new to Britain. Journal of Bryology 29: 228-234. Bathon, H. 2007. Das Bl?schenmoss Sphaerocarpos texanus Austin (Marchantiophyta: Sphaerocarpaceae) in Ro?dorf?neufund f?r Hessen. Hessische Floristische Briefe 56: 24-28. Bednarek-Ochyra, H. &amp; R. Ochyra. 2007. New national and regional bryophyte records, 17. 1. Bucklandiella crispipila, Papua New Guinea. Journal of Bryology 29: 277. -&amp;-, 2007. New national and regional bryophyte records, 17. 3. Dicranella hookeri, Peru. Journal of Bryology 29: 277. -&amp;-. 2007. New national and regional bryophyte records, 17. 9. Hygrodicranum falklandicum, Tristan da Cunha. Journal of Bryology 29: 278. The Bryologist 111(3), pp. 526-533 0007-2745/08/$0.95/0 Copyright ?2008 by The American Bryological and Lichenological Society, Inc.</page><page sequence="2">Buck et al.: Recent literature on bryophytes 5^7 -&amp;-. 2007. New national and regional bryophyte records, 17. 13. Pohlia nutans, lies Crozet. Journal of Bryology 29: 279. Benavides Duque, J. C. 2007. Competitive Ability of an Epilithic Moss, Thuidium tomentosum Schimp., under Different Light Treatments in a Subtropical Lower Montane Forest in Puerto Rico, ix + 40 pp. University of Puerto Rico, Mayag?ez, PR. [Master's thesis] Blocked, T. L. (comp.). 2007. New national and regional bryophyte records, 17. Journal of Bryology 29: 277-283. [Individual records listed separately] -. 2007. [Review] Si He (editor). 2007. Moss Flora of China. English Version. Volume 4. Bryaceae-Timmiaceae. Journal of Bryology 29: 284-285. B?ger, K. &amp; T. Wolf. 2007. Dicranum viride (Gr?nes Gabelzahnmoos) im hessischen FFH-Gebiet "J?gersburger Gernsheimer Wald (6217-308)?eine Erwiderrung und Richtigstellung zum Artikel von Werner Manzke zum Umgang mit den Moosen bei der Umsetzung der FFH Richtlinie in Hessen" im Heft 4/2006 dieser Zeitschift. Hessische Floristische Briefe 56: 29-33. Boufford, D. E., Yu Jia, Liang-qian Li, Qing Tian &amp; Zhi-yun Zhang. 2007. Botanical expedition to southern Gansu Province, China, May 2007. Newsletter of Himalayan Botany 40: 5-14. [Buxbaumia minakatae, Neodolichomitra yunnanensis and Pylaisia levieri reported] Buck, W. R., B. Allen &amp; R. A. Pursell. 2008. Recent literature on bryophytes?111(1). The Bryologist 111: 140-152. Cailliau, A. &amp; M. J. Price. 2007. Hornworts in the agricultural fields of Geneva: new findings, the soil diaspore bank and ex situ cultures. Candollea 62: 165-172. Callaghan, D. A. &amp; P. A. Ashton. 2007. Bryophyte clusters and sand dune vegetation communities. Journal of Bryology 29: 213-221. Ceska, A. &amp; O. Ceska. 2008. Carpet burweed invades RV parks. Menziesia 12(4): 4-7. [Includes photographs of Entosthodon fascicularis and Sphaerocarpos texanus, listed as threatened by Soliva sessilis, an invasive Asteraceae] Chaudhary, B. L. &amp; S. Rana. 2007. Terrestrial moss Splachnobryum indicum Hamp. new to Rajasthan, India. Journal of the Indian Botanical Society 86: 48-50. Chudzihska, E. 1998. Heterochromatin diversity in two parental species of the tetraploid liverwort Pellia borealis Lorb. Pages 239-243. In J. Maluszynska (ed.), Plant Cytogenetics. Wydawnictwo Uniwersytetu Slaskiego, Katowice, Poland. Ciosek, M. T. 2003. [Abstract] Sphagno girgensohnii-Piceetum Polak. 1962 in Jegiel Nature Reserve near Lochow. Pages 46-47. In A. Brzeg &amp; M. Lisiewska (eds.), Zr?znicowanie, Dynamika i Przeksztalcenia Roslinnosci Borowej [Differentiation, Dynamics and Transformations of Coniferous Forests]. Sorus, Poznan, Poland. [In Polish] Colacino, C. 2005. Brioflora dell'Albania: situazione attuale e considerazioni in relazione ad una possibile utilizzazione nel biomonitoraggio ambi?ntale. Societ? Italiana di Selvicotura ed Ecologia For?stale Atti 4: 357-364. -. 2005 [2007]. Versione italiana annotata del Glossarium Polyglottum Bryologiae. Delpinoa 47: 57-110. Conte, B., R. Braglia, A. Basile, R. Castaldo Cobianchi 8c C. Forni. 2007. Proteomics and bryophytes: a comparison between different methods of protein extraction to study protein synthesis in the aquatic moss Leptodictyum riparium (Hedw.). Caryologia 60: 102-105. Cullina, W. 2008. Native Ferns, Moss 8c Grasses, xiv -I- 256 pp. Houghton Mifflin Co., Boston, MA. ["Part II: Mosses" on pp. 92-113] Cykowska, B. 2006. Diplophyllum albicans (Hepaticopsida, Scapaniaceae) in the Polish Carpathians. Polish Botanical Journal 51: 93-95. -. 2006. Schistostega pennata (Bryopsida, Schistostegaceae) in the Polish Carpathians (Poland). Polish Botanical Journal 51: 225-227. Dengler, J. 8c S. Boch. 2007. Taxonomy, ecology, and distribution of six remarkable plant taxa (Spermatophyta, Bryophyta, and Lichenes) of the Estonian dry grassland flora. Phyton (Horn) 47: 47-71. [Bryum radiculosum used in study; it is also new to Estonia] Ellis, L. T. 2007. Bryophyte flora of Uganda. 7. Calymperaceae and Octoblepharaceae. Journal of Bryology 29: 259-274. Engel, J. J. 8c D. Glenny. 2008. Studies on Lophocoleaceae XVI. Chiloscyphus anisolobus, an interesting new species from New Zealand. The Bryologist 111: 118-123. Enroth, J. 8c M. Ji. 2007. A new species of Neckera (Neckeraceae, Bryopsida) from Xizang, China. Edinburgh Journal of Botany 64: 295-301. [New: N. serrulatifolia sp. nov.] Evangelista, E., S. Cipollaro 8c C. Colacino. 2005. Brioflora de una cerreta adiacente al Centro Oli di Viggiano (Potenza): osservazioni e consideraziioni preliminari in relazione al biomonigoraggio dell'inquinamento deU'aria. Societ? Italiana di Selvicotura ed Ecologia For?stale Atti 4: 497 499. Farrell, C. A. 2007. International Peat Congress 2008 Post Congress Tour: western peatlands. Peatlands International 2007(2): 15-19. Faubert, J. 2007. Catalogue des bryophytes du Qu?bec et du Labrador. Provancheria 30: 1-138. [4 hornworts, 231 liverworts 8c 657 mosses listed] -. 2008. Les grandes m?connues du monde vert: les bryophytes. Quatre-Temps 32(1): 25-28. [Includes biographical sketch of James Kucyniak (1919-1962)]</page><page sequence="3">528 THE BRYOLOGIST 111(3)12008 Fiedorow, P., I. Odrzykoski 8c Z. Szweykowska-Kulihska. 1998. Phylogenetic studies of liverworts using molecular biology techniques. Pages 244-249. In J. Maluszynska (ed.), Plant Cytogenetics. Wydawnictwo Uniwersytetu Slaskiego, Katowice, Poland. [Pellia used as an example] Fisher, K. M. 2008. Bayesian reconstruction of ancestral expression of the LEA gene families reveals propagule derived desiccation tolerance in resurrection plants. American Journal of Botany 95: 506-515. [Includes use of Physcomitrella and Tortula [Syntrichia] ruralis] Fritz, ?., L. Gustafsson 8c K. Larsson. 2008. Does forest continuity matter in conservation? - A study of epiphytic lichens and bryophytes in beech forests of southern Sweden. Biological Conservation 141: 655-668. Fuertes, E. 2007. Bibliograf?a bot?nica Ib?rica, 2006: Bryophyta. Bot?nica Complutensis 31: 151-152. Fuselier, L. C. 2004. Maintenance of Sexually Dimorphic Patterns of Growth and Reproduction in Marchantia inflexa. x + 200 pp. University of Kentucky, Lexington, KY. [Doctoral dissertation; available online at http://lib.uky. edu/ETD/ukybiol2004d00154/fuselier.pdf] Garcia, C. 8c C. Sergio. 1999. Novas referencias para Portugal de Octodiceras fontanum (B. Pyl.) Lindb. Anuario da Sociedade Broteriana 65: 114-115. -8c-. 1999. Una nova especie para a brioflora portuguesa: Phascum leptophyllum C. M?ll. Anuario da Sociedade Broteriana 65: 115-116. Goldberg, I. 2008. ?rets fund af mosser 2007. In A.-M. C. B?rger, B. V. Petersen, H. Tranberg 8c I. Goldberg, Arets fund 2007. Urt 32(1): 18-20. -8c M. K. Due. 2007. Bryologi i Danmark?fra Thomas Jensen til Bryologkredsen. Myrinia 17: 82-95. [In Swedish with English abstract; on history of Danish bryology] Grundmann, M., S. W. Ansell, S. J. Russell, M. A. Koch 8c J. C. Vogel. 2008. Hotspots of diversity in a clonal world?the Mediterranean moss Pleurochaete squarrosa in central Europe. Molecular Ecology 17: 825-838. H?jkov?, P., V. Pl?sek 8c M. H?jek. 2007. A contribution to the Bulgarian bryoflora. Phytologia Balc?nica 13: 307-310. [Dicranella staphylina and Gymnocolea inflata new to Bulgaria] He, Si 8c Li Zhang. 2007. Neckeropsis moutieri (Neckeraceae), a Southeast Asia species new to China. Journal of Tropical and Subtropical Botany 15: 545-548. [Includes key to the 11 spp. of Neckeropsis in China] Hedderson, T. A. 8c R. H. Zander. 2007. Ludorugbya springbokorum (Pottiaceae) a new moss genus and species from the Western Cape Province of South Africa. Journal of Bryology 29: 222-227. Heden?s, L. 8c P. Elden?s. 2008. Relationships in Scorpidium (Calliergonaceae, Bryophyta), especially between 5. cossonii and S. scorpioides. Taxon 57: 121-130. Herrera-Paniagua, P., C. Delgadillo M., J. L. V. R?os 8c I. Luna-Vega. 2008. Floristics and biogeography of the mosses of the state of Quer?taro, Mexico. The Bryologist 111: 41-56. Heylen, O. 8c M. Hermy. 2008. Age structure and ecological characteristics of some epiphytic liverworts (Frullania dilatata, Metzgeria furcata and Radula complanata). The Bryologist 111: 84-97. Hill, M. 2007. [Review] Jan-Peter Frahm. 2006. Moose?Eine Einf?hrung. Journal of Bryology 29: 285. Hiraoka, T., S. Isono 8c S. Hiraoka. 2007. A revised checklist of bryophytes from Kanagawa, Japan (2007). Natural and Environmental Science Research 20: 101-127. [In Japanese with English abstract; 445 spp. of mosses, 197 spp. of hepatics and 9 spp. of anthocerotes reported] Hofmann, H. 2007. BLAM-Exkursion Meiringen 2006?die moosige Sicht. Meylania 39: 24-31, 1 photo. [Includes photograph of participants] Hood, G. 2007. A short history of the Canadian Sphagnum Peat Moss Association. Peatlands International 2007(2): 13-14. Hooijmaijers, C. A. M. 8c K. S. Gould. 2007. Photoprotective pigments in red and green gametophytes of two New Zealand liverworts. New Zealand Journal of Botany 45: 154-461. Ingerpuu, N. 8c K. Vellak. 2007. Collections of G. C. Girgensohn (1786-1872): lectotypes and rare species. Journal of Bryology 29: 235-240. Isono, S., S. Sasaki 8c K. Kanai. 2007. Liverworts and hornworts of Mt. Ohyama and adjacent areas, Kanagawa, Japan. Natural and Environmental Science Research 20: 63-80. [In Japanese with English abstract] Iwatsuki, Z. 8c H. Kiguchi. 2007. Notes on Bryonorrisia acutifolia. Bryological Research 9: 205-207. [In Japanese] Jankowiak-Siuda, K., A. Pacak, I. Odrzykoski, R. Wyatt 8c Z. Szweykowska-Kulinska. 2008. Organellar inheritance in the allopolyploid moss Plagiomnium curvatulum. Taxon 57: 145-152. Jerez Jaimes, J. H. 2003. Composici?n de Tard?grados en el Musgo Calymperes palisotii Schwaegrichen sobre Seis Forofitos (Arboles) en la Universidad de Puerto Rico, Reciento de Mayag?ez. xiii + 99 pp. Universidad de Puerto Rico, Mayag?ez, PR. [Master's thesis] Jorgensen, P. M. 2007. Blomsterkongen og biskopen. Kongelige Norske Videnskabers Selskab ?rbok 2007: 119-125. [biographical information on Cari Linn? and Johan Ernst Gunnerus] Kalnina, L. 2007. Diversity of mire origin and history in Latvia. Peatlands International 2007(2): 54-56.</page><page sequence="4">Buck et al.: Recent literature on bryophytes 529 Kochjarov?, J., R. Solt?s 8c R. Hrivn?k. 2007. Cinclidotus aquaticus in Slovakia: present state and prognoses. Bryonora 40: 1-6. [In Czech with English abstract and summary] Korneck, D. 2007. Bartramia stricta nicht im Rheinland. Decheniana 160: 154. [Such named material is really Anacolia laevisphaera] Kosovich-Anderson, Y. 2008. Sphagnum: how to recognize it in the field. Castilleja 27(1): 6-7. Kreier, H.-P. 2003. Die Pilz-Assoziationen der Aneuraceae (Marchantiophyta). iii + 34 pp. Eberhard-Karls Universit?t, T?bingen, Germany. [Diplomarbeit] Kucera, J. (ed.). 2007. Interesting bryofloristic records, X. Bryonora 40: 41-43. [In Czech] -, S. Kubesov?, M. H?jek 8c V. Pl?sek. 2007. New bryological literature, XVIII. Bryonora 40: 53-65. Lajczak, A. 2006. Torfowiska Kotliny Orawsko-Nowotarskiej: Rozw?j, Antropogeniczna Degradacja, Renaturyzacja i Wybrane Problemy Ochrony. 147 pp. Instytut Botaniki im. W. Szafera, Polska Akademia Nauk, Krakow, Poland. [In Polish with English summary: Peatbogs of Orawsko Nowotarska Basin: Development, human impact, renaturalisation and selected problems of protection] Lara, F., R. Garilleti 8c V. Mazimpaka. 2007. A peculiar new Orthotrichum species (Orthotrichaceae, Bryopsida) from central Argentina. Botanical Journal of the Linnean Society 155: 477-482. [New: O. spiculatum sp. nov.] -8c-. 2007. New national and regional bryophyte records, 17. 19. Zygodon bartramioides, Argentina. Journal of Bryology 29: 280. -8c-. 2007. New national and regional bryophyte records, 17. 20. Zygodon chilensis, Argentina. Journal of Bryology 29: 280-281. -8c-. 2007. New national and regional bryophyte records, 17. 21. Zygodon hookerivar. leptobolax, Argentina. Journal of Bryology 29: 281. -8c-. 2007. New national and regional bryophyte records, 17. 22. Zygodon jajfuelii, Argentina. Journal of Bryology 29: 281. -8c-. 2007. Nyckel til sl?ktet Orthotrichum i Norden. Myrinia 17: 105-112. [In Swedish with English abstract; translated from Spanish by N. L?nnell; a key to Orthotrichum in the Nordic countries] Li?nard, D., G. Durambur, M.-C. Kiefer-Meyer, F. Nogu?, L. Menu-Bouaouiche, F. Chariot, V. Gomord 8c J.-P. Lassalles. 2008. Water transport by aquaporins in the extant plant Physcomitrellla patens. Plant Physiology 146: 1207-1218. Liu, Yan 8c Tong Cao. 2007. New records of mosses from Zhejiang Province. Journal of East China Normal University (Natural Science) 2007(6): 131-134. Lynch, M. 2008. [Review] Native Ferns Moss 8c Grasses by William Cullina, 2008. Horticulture 105(3): 70. Marstaller, R. 2007. Die epilithische Moosvegetation der Bergstr?rze am Manrod bei Rambach (Nordhessen) und am Dohlenstein bei Kahla (Ostth?ringen)?ein Vergleich. Philippia 13: 93-127. -. 2007. Die Moose und Moosgesellschaften des Naturschutzgebietes "Bischofswiese" in der D?lauer Heide (Stadt Halle, Sachsen-Anhalt). Schlechtendalia 16: 41-59. -. 2007. Die Moosgesellschaftgen des Schlossberges zu Lichtenberg (Landkreis Hof, Oberfranken). Berichte der Bayerischen Botanischen Gesellschaft zur Erforschung der Heimischen Flora 77: 71-92. McDaniel, S. F., J. H. Willis 8c A. J. Shaw. 2007. A linkage map reveals a complex basis for segregation distortion in an interpopulation cross in the moss Ceratodon purpureus. Genetics 176: 2489-2500. Meinunger, L. 8c W. Schr?der. 2007. Verbreitungsatlas der Moose Deutschlands. 3 vols. 1: 1-636; 2: 1-699; 3: 1-709. Regensburgische Botanische Gesellschaft, Regensburg, Germany. [Band 1 is anthocerotes, hepatics and Sphagnum; Band 2 is acrocarps Andreaeaceae to Splachnaceae; Band 3 is Schistostegaceae to Hypnaceae] Merced Alejandro, A. 2004. A Heterochronic Sequence for the Development of Paraphyses in Neckeropsis Schimp. (Bryophyta: Neckeraceae). x + 72 pp. University of Puerto Rico, Mayag?ez, PR. [Master's thesis] Mikul?skov?, E., T. Berka, E. Hola, J. Kosnar, S. Kubesov?, I. Markov?, R. Mudrov? 8c Z. Musil. 2007. Bryophytes recorded during the 20th Autumn Meeting of the Bryological and Lichenological Section in the Sumava National Park (Bohemian Forest). Bryonora 40: 14-27. [In Czech with English abstract and summary] Miller, N. G. 2008. A new biography of Leo Lesquereux: [Review] L. Lesquereux. 2006. Letters written from America 1849-1853. Translated from the French by H. D. Page. The Bryologist 111: 134-136. Mott, J. 2008. Introductory bryophyte meeting in Wayland Wood, Sunday 18th November 2007. Norfolk Natterjack 100: 13-14. [Anomodon viticulosus and Fissidens exilis reported] M?ller, N. 2007. Moose als F?llmaterial bei Blockhausbauten. Meylania 39: 12-14. Nan, Zi 8c Rui-liang Zhu. 2007. Epiphyllous liverworts of the Maoershan Nature Reserve, Guangxi, China. Journal of East China Normal University (Natural Science) 2007(6): 125-130. [In Chinese with English abstract] Narv?ez-Parra, E. X. 2003. Brioflora sobre Taludes del Transecto Mayag?ez-Maricao (Puerto Rico), xii + 115 pp. Universidad de Puerto Rico, Mayag?ez, PR. [Master's thesis]</page><page sequence="5">530 THE BRYOLOGIST 111(3): 2008 Natcheva, R. 2007. Dichelyma falcatum: a new aquatic moss to the bryophyte flora of Bulgaria. Phytologia Balc?nica 13: 311-312. -(comp.). 2007. New bryophyte records in the Balkans: 3. Phytologia Balc?nica 13: 429-432. [Records from Romania and Bulgaria] Neily, P., D. McCurdy, B. Stewart 8c E. Quigley. 2004. Coastal forest communities of the Nova Scotian Eastern Shore Ecodistrict. Nova Scotia Department of Natural Resources Forest Research Report 74: 1-26. [Includes list of bryophytes and lichens on p. 20] Nejfeld, P. 8c A. Stebel. 2007. Vegetation of Matyska and Kopa limestone hills (Kotlina Zywiecka Basin, Western Carpathians) and the proposition of its protection. Part 1. The bryophyte and vascular plant flora. Natura Silesiae Superioris 10: 19-36. [In Polish with English and German summaries] N?meth, Cs. 2007. Data on the distribution of some rare alpine-boreal bryophytes in the V?rtes Mts (Hungary). Studia Bot?nica Hungarica 38: 59-70. Nesbitt, P. 2008. 'Woody Fibre', an inspirational leader and teacher. Botanies 32: 12. [On John Hutton Balfour (1808 1884), botanist and father of Isaac Bailey Balfour, the Indian Ocean plant collector] Nickol, M. 2007. Algen, Moose und ein fr?her Tod: Der Kieler Botaniker Daniel Matthias Heinrich Mohr (1780-1808). Kieler Notizen zur Pflanzenkunde in Schleswig-Holstein und Hamburg 35: 1-8. [Includes bibliography] O'Shea, B. 2007. Meiothecium intextum Mitt. (Bryopsida: Sematophyllaceae): an overlooked taxon from Asia and Oceania, with notes on related taxa. Journal of Bryology 29: 275-276. [New synonymy at species and generic level] Ochyra, R. 2006. [Review] B. Allen. 2005. Maine mosses. Sphagnaceae-Timmiaceae. Fragmenta Floristica et Geobot?nica Polonica 13: 436-437. [In Polish] -. 2006. [Review] E. Fudali. 2005. Bryophyte species diversity and ecology in the parks and cemeteries of selected Polish cities. Fragmenta Floristica et Geobot?nica Polonica 13: 292. [In Polish] -. 2006. [Review] F. Schaumann. 2005. Terricolous bryophyte vegetation of Chilean temperate rain forests. Communities, adaptive strategies and divergence patterns. Fragmenta Floristica et Geobot?nica Polonica 13: 326. [In Polish] -. 2006. [Review] J. Coudreuse, J. Haury, J. Bard?t 8c J.-P. Rebillard. 2005. Les bryophytes aquatiques et supra aquatiques. Cl? d'identification pour la mise en oeuvre de l'Indice Biologique Macrophytique en Rivi?re. Fragmenta Floristica et Geobot?nica Polonica 13: 408. [In Polish] -. 2006. [Review] J. Guerra, M. J. Cano 8c R. M. Ros (red.). 2006. Flora briof?tica ib?rica, volumen III. Pottiales: Pottiaceae, Encalyptales: Encalyptaceae. Fragmenta Floristica et Geobot?nica Polonica 13: 433. [In Polish] -. 2006. [Review] J. P. Gruber. 2001. Die Moosflora der Stadt Salzburg und ihr Wandel im Zeitraum von 130 Jahren. Fragmenta Floristica et Geobot?nica Polonica 13: 252. [In Polish] -. 2006. [Review] L. Beyer 8c M. B?lter (red.). 2002. Geoecology of Antarctic ice-free coastal landscapes. Fragmenta Floristica et Geobot?nica Polonica 13: 300. [In Polish] -. 2006. [Review] L. Ya. Partyka. 2005. Brioflora Kryma. Fragmenta Floristica et Geobot?nica Polonica 13: 434-435. [In Polish] -. 2006. [Review] M. C. Nair, K. P. Rajesh 8c P. V. Madhusoodana. 2005. Bryophytes of Wayanad in Western Ghats. Fragmenta Floristica et Geobot?nica Polonica 13: 438-439. [In Polish] -. 2006. [Review] M. J. Price. 2005. Catalogue of the Hedwig-Schw?grichen herbarium (G). Part 1. Type material and a review of typifications for the Hedwig moss names. Fragmenta Floristica et Geobot?nica Polonica 13: 435-436. [In Polish] -. 2006. [Review] P. C. Wu, M. R. Crosby 8c S. He (red.). 2005. Moss flora of China. English Version. Volume 8. Sematophyllaceae-Polytrichaceae. Fragmenta Floristica et Geobot?nica Polonica 13: 438. [In Polish] -. 2006. [Review] S. Fransen. 2005. Taxonomic revision of the moss genus Bartramia Hedw. sections Bartramia and Vaginella C. M?ll. Fragmenta Floristica et Geobot?nica Polonica 13: 440. [In Polish; a correction of a previously printed review] -. 2006. [Review] S. Munch. 2006. Outstanding mosses and liverworts of Pennsylvania and nearby states. Fragmenta Floristica et Geobot?nica Polonica 13: 350. [In Polish] -. 2006. [Review] T. Hallingb?ck, N. L?nnell, H. Weibull, L. Heden?s 8c P. von Knorring. 2006. Nationalnyckeln till Sveriges flora och fauna. Bladmossor: Sk?ldmossor bl?mossor. Bryophyta: Buxbaumia-Leucobryum. Fragmenta Floristica et Geobot?nica Polonica 13: 437. [In Polish] -. 2006. [Review] W. T. Doyle 8c R. E. Stotler. 2006. Contributions toward a bryoflora of California: III. Keys and annotated species catalogue for liverworts and hornworts. Fragmenta Floristica et Geobot?nica Polonica 13: 260. [In Polish] -. 2007. Bryophyte anomalies. Norfolk Natterjack 99: 2. [On Hookeria in Britain] -8c H. Bednarek-Ochyra. 2007. New national and regional bryophyte records, 17. 2. Dicranella dietrichiae, Norfolk Island. Journal of Bryology 29: 277.</page><page sequence="6">Buck et al: Recent literature on bryophytes 531 -8c-. 2007. New national and regional bryophyte records, 17. 4. Didymodon australasiae, South Georgia. Journal of Bryology 29: 277-278. Offner, K. 2007. Das Moosinventar des Landkreises Rh?n Grabfeld. Berichte der Bayerischen Botanischen Gesellschaft zur Erforschung der Heimischen Flora 77: 33 69. Oliver, M. 2008. Moss. Solidago: The Newsletter of the Finger Lakes Native Plant Society 9(2): 7. [poem] Page, S. 8c C. Banks. 2007. Tropical peatlands: distribution, extent and carbon storage?uncertainties and knowledge gaps. Peatlands International 2007(2): 26-27. Papp, B. 2007. Contribution to the bryophyte flora of eastern Turkey. Studia Bot?nica Hungarica 38: 71-78. -8c P. Erzberger. 2007. Contributions to the bryophyte flora of Montenegro. Studia Bot?nica Hungarica 38: 79-94. -8c-. 2007. Contributions to the bryophyte flora of Western Stara Planina Mts (E Serbia). Studia Bot?nica Hungarica 38: 95-123. [Includes 27 bryophytes new to Serbia and Cynodontium tenellum new to Bulgaria] Parker, R. 2008. [Review] Native Ferns, Moss, 8c Grasses, from Emerald Carpet to Amber Wave: Serene and Sensuous Plants for the Garden by William Cullina. 2008. Solidago: The Newsletter of the Finger Lakes Native Plant Society 9(1): 4-5. Parusel, J. B. 2007. Bazzanio-Piceetum Br. Bl. et Siss. 1939 in Beskid Slaski Mts and Babia G?ra Range (Beskid Zywiecki Mts). Natura Silesiae Superioris 10: 45-51. [In Polish with English and German summaries] Peng, Tao 8c Zhao-Hui Zhang. 2007. Analyses of the contents of elements of five mosses and their substrates in Tongshankou Copper Mine, Hubei Province, China. Journal of Wuhan Botanical Research 25: 596-600. [In Chinese with English abstract] Perry, N. B., E. J. Burgess, L. M. Foster, P. J. Gerard, M. Toyota 8c Y. Asakawa. 2008. Insect antifeedant sesquiterpene acet?is from the liverwort Lepidolaena clavigera. 2. Structures, artifacts, and activity. Journal of Natural Products 71: 258-261. Philippi, G. 2006. Zur Frequenz epiphytischer Moose im Bienwald und Hagenauer Forst (mittleres Oberrheingebiet). Carolinea 63: 71-86. Pistarino, A. 8c G. Forneris. 2007. Le collezioni briologiche del Piemonte e della Valle d'Aosta cons?rvate presso l'Erbario dell'Universit? di Torino. Museo Regionale di Scienze Naturali, Bollettino 24: 191-231. Pl?sek, V. 2007. Confirmation of the occurrence of moss Orthotrichum alpestre Hornsch. ex Bruch 8c Schimp. in the Czech Republic and a newly discovered historical locality in Slovakia. Bryonora 40: 27-30. [In Czech with English abstract and summary] P?cs, T. 8c J. Eggers. 2006. New or little known epiphyllous liverworts, XIII. Cololejeunea arfakiana sp. nov. from West Irian (New Guinea). Polish Botanical Journal 51: 155-158. -?, S. P?cs, S. Min, I. Chuah-Petiot, I. Malombe 8c S. Masinde. 2007. East African bryophytes, XXIV. Records from the dry lands of Kenya, with a description of Didymodon revolutus var. nov. africanas (Pottiaceae). Lindbergia 32: 33-39. Pressel, S., H. W. Matcham 8c J. G. Duckett. 2007. Studies of protonemal [sic] morphogensis. XL Bryum and allied genera: a plethora of propagules. Journal of Bryology 29: 241-258. Price, M. J. 2007. Types catalogue of the Hedwig collection in g. Meylania 39: 8-11. Rakowski, W. 2003. [Abstract] Festuco ovinae-Hypnetum jutlandici?heath developing under the influence of temperate treading of pine forests. Page 54. In A. Brzeg 8c M. Lisiewska (eds.), Zr?znicowanie, Dynamika i Przeksztalcenia Roslinnosci Borowej [Differentiation, Dynamics and Transformations of Coniferous Forests]. Sorus, Poznan, Poland. [In Polish] Rawat, K. K. 8c S. C. Srivastava. 2007. Genus Plagiochila in Eastern Himalaya (India), xiv + 259 pp. Bishen Singh Mahendra Pal Singh, Dehra Dun, India. Renner, M. 2007. Mt Rowe: a chunk of south Westland in the Coromandel. Auckland Botanical Society Journal 62: 158 161. [On hepatics on a mountain in the North Island, New Zealand] Rensing, S. A., D. Lang, A. D. Zimmer, A. Terry, A. Salamov, H. Shapiro, T. Nishiyama, P.-F. Perroud, E. A. Lindquist, Y. Kamisugi, T. Tanahashi, K. Sakakibara, T. Fujita, K. Oishi, T. Shin-I, Y. Kuroki, A. Toyoda, Y. Suzuki, S.-i. Hashimoto, K. Yamaguchi, S. Sugano, Y. Kohara, A. Fujiyama, A. Anterola, A. Aoki, N. Ashton, W. B. Barbazuk, E. Barker, J. L. Bennetzen, R. Blankenship, S. H. Cho, S. K. Dutcher, M. Estelle, J. A. Fawcett, H. Gundlach, K. Hanada, A. Heyl, K. A. Hicks, J. Hughes, M. Lohr, K. Mayer, A. Melkozernov, T. Murata, D. R. Nelson, B. Pils, M. Prigge, B. Reiss, T. Renner, S. Rombauts, P. J. Rushton, A. Sanderfoot, G. Schween, S.-H. Shiu, K. Stueber, F. L. Theodoulou, H. Tu, Y. Van de Peer, J. J. Verrier, E. Waters, A. Wood, L. Yang, D. Cove, A. C. Cuming, M. Hasebe, S. Lucas, B. D. Mishler, R. Reski, I. V. Grigoriev, R. S. Quatrano 8c J. L. Boore. 2008. The Physcomitrella genome reveals evolutionary insights into the conquest of land by plants. Science 319: 64-69. Rice, S. K. 2008. New overview of peatlands: [Review] Hakan Rydin 8c J. K. Jeglum. 2006. The Biology of Peatlands. The Bryologist 111: 133-134. Riese, M., O. Zobell, H. Saedler 8c P. Huijser. 2008. SBP domain transcription factors as possible effectors of</page><page sequence="7">532 THE BRYOLOGIST 111(3): 2008 cryptochrome-mediated blue light signaling in the moss Physcomitrella patens. Planta 227: 505-515. Ros, R. M., J. Mu?oz, O. Werner 8c S. Rams. 2008. New typifications and synonyms in Tortula sect. Pottia (Pottiaceae, Musci). Taxon 57: 279-288. Rozzi, R., J. J. Armesto, B. Goffinet, W. Buck, F. Massardo, J. Silander, M. T. K. Arroyo, S. Russell, C. B. Anderson, L. A. Cavieres 8c J. B. Callicott. 2008. Changing lenses to assess biodiversity: patterns of species richness in sub-Antarctic plants and implications for global conservation. Frontiers in Ecology and the Environment 6(3): 131-137. [Shows how use of vertebrates and vascular plants to define hotspots overlooks significant diversity of non-vascular plants] Sales, F. 1999. O herbario do Departamento de Bot?nica da Universidade de Coimbra (coi). Anuario da Sociedade Broteriana 65: 55-78. Saracino, A., C. Colacino, L. Esposito, B. Curcio 8c S. Cipollaro. 2005. II consorzio faggio-abete bianco del Monte Mont?la di Teggiano (SA): aspetti selvicolturali e primo contributo alia brioflora. Societ? Italiana di Selvicotura ed Ecolog?a For?stale Atti 4: 165-171. Sasaki, S. 2007. Bryophytes of the north side of Mt. Iwato and adjacent area, Izumi, Atami, Shizuoka, Japan. Natural and Environmental Science Research 20: 81-96. [In Japanese with English abstract] Sch?fer-Verwimp, A. 2007. New national and regional bryophyte records, 17. 7. Eurhynchium striatum, Canary Islands. Journal of Bryology 29: 278. -. 2007. New national and regional bryophyte records, 17. 8. Ganguleea angulosa, Indonesia, Bali. Journal of Bryology 29: 278. -8c J. V??a. 2007. New national and regional bryophyte records, 17. 16. Southbya organensis, Peru. Journal of Bryology 29: 279-280. Schiavone, M. M. 8c G. M. Su?rez. 2007. Las Thuidiaceae en el noroeste de Argentina. Bolet?n de la Sociedad Argentina de Bot?nica 42: 211-230. [New: Pelekium mexicanum (Mitt.) comb, nov.] Schneider, A. 2008. A discussion with Dr. William Weber. Aquilegia 32(1): 8-9. [The first part of a serialized interview] Schr?der, C, A. Thiele, Shengzhong Wang, Zhaojun Bu 8c H. Joosten. 2007. Hani-mire?a percolation mire in northeast China. Peatlands International 2007(2): 21-24. Sergio, C. 1999. Hep?ticas novas ou raras para a brioflora de Portugal. Anuario da Sociedade Broteriana 65: 94-96. [Nardia geoscyphus, Pallavicinia lyellii, Radula aquilegia and Riccardia chamaedryfolia reported] -. 1999. Notulae bryoflorae lusitanicae VIL Anuario da Sociedade Broteriana 65: 93-116. [A group of 12 short articles, listed separately here] -. 1999. Novas referencias para alguns musgos raros ou pouco conhecidos da brioflora portuguesa. Anuario da Sociedade Broteriana 65: 96-98. [Amblystegium serpens, Campylopus introflexus, Dicranum tauricum, Phascum cuspidatum, Plagiomnium rostratum, Racomitrium obtusum, Tortula canescens and T. papulosa reported] -, J. I. Cubero 8c N. Marcos-Samaniego. 1999. Novas localidades de Neckera crispa Hedw. para Portugal. Anuario da Sociedade Broteriana 65: 110-111. -8c C. Garcia. 1999. Algumas especies novas para o Baixo Alentejo. Anuario da Sociedade Broteriana 65: 106-110. -8c-. 1999. Novas ?reas para Portugal de Isothecium holtii Kindb. Anuario da Sociedade Broteriana 65: 98-99. -,-, J. Jansen 8c M. Sim-Sim. 1999. Novos dados para a brioflora da Serra da Estrela ou de Portugal. Anuario da Sociedade Broteriana 65: 99-104. -,-8c M. Sim-Sim. 1999. Alguns bri?fitos interessantes e novos para a Serra da Malcata. Anuario da Sociedade Broteriana 65: 111-114. -8c T. Koponen. 1999. Acerca da presen?a de Plagiomnium medium (B. 8c S.) T. Kop. em Portugal. Anuario da Sociedade Broteriana 65: 104-105. -8c J. Mu?oz. 1999. Nova referencia de Grimmia lisae De Not. para Portugal. Anuario da Sociedade Broteriana 65: 93-94. -8c C. Tauleigne Gomes. 1999. Primeiros dados para a brioflora das ilhas das Berlengas depois de 1883. Anuario da Sociedade Broteriana 65: 105-106. Shabbara, H. M. 2007. New national and regional bryophyte records, 17. 17. Syntrichia handelii, Egypt. Journal of Bryology 29: 280. -. 2007. New national and regional bryophyte records, 17. 18. Syntrichia virescens, Egypt. Journal of Bryology 29: 280. Shaw, A. J., I. Holz, C. J. Cox 8c B. Goffinet. 2008. Phylogeny, character evolution, and biogeography of the Gondwanic moss family Hypopterygiaceae (Bryophyta). Systematic Botany 33: 21-30. [New: Hypopterygium hookerianum (Griff.) comb, nov., H. parvifolium (Bosch 8c Sande Lac. in Dozy 8c Molk.) comb, nov] Siebel, H. N., R. J. Bijlsma 8c D. Bal. 2006. Toelichting op de Rode Lijst Mossen. Rapport DK 34: 1-70. [Dutch Red List] Skrzypczak, R. 2007. Barbula amplexifolia (Mitt.) A. Jaeger pr?sent en France (Savoie). Bulletin de la Soci?t? Botanique du Centre-Ouest n.s. 38: 321-326. -. 2007. Note sur Leptophascum leptophyllum (M?ll. Hal.) J. Guerra 8c Cano. Bulletin de la Soci?t? Botanique du Centre-Ouest n.s. 38: 333-336. S?derstr?m, L. 2007. New national and regional bryophyte records, 17. 11. Leiocolea badensis, Bulgaria. Journal of Bryology 29: 279.</page><page sequence="8">Buck et al.: Recent literature on bryophytes 533 Sotiaux, A., H. Stieperaere 8c A. Vanderpoorten. 2007. Bryophyte checklist and European Red List of the Brussels capital region, Flanders and Wallonia (Belgium). Belgian Journal of Botany 140: 174-196. Stebel, A., J. Zarnowiec 8c A. Rusihska. 2007. New national and regional bryophyte records, 17. 23. Zygodon stirtonii, Poland. Journal of Bryology 29: 281. Stotler, R. E. 8c B. J. Crandall-Stotler. 2008. Correct author citations for some upper rank names of liverworts (Marchantiophyta). Taxon 57: 289-292. [New: Treubiidae subcl. nov., Haplomitriidae subcl. nov.] Szweykowski, J. 2006. An annotated checklist of Polish liverworts and hornworts. Biodiversity of Poland 4: 1-114. Tobolski, K. 2000. Przewodnik do Oznaczania Torf?w i Osad?w Jeziornych. 508 pp. Wydawnictwo Naukowe PWN, Warsaw, Poland. [Guide to the determination of peats and lake sediments, including a key to bryophytes in peat (pp. 336-356)] Townsend, C. C. 2007. New national and regional bryophyte records, 17. 5. Drepanocladus aduncus, Tanzania. Journal of Bryology 29: 278. -. 2007. New national and regional bryophyte records, 17. 6. Erpodium coronatum, Tanzania. Journal of Bryology 29: 278. -. 2007. New national and regional bryophyte records, 17. 12. Pohlia cruda, Uganda. Journal of Bryology 29: 279. -. 2007. New national and regional bryophyte records, 17. 14. Pseudoleskeopsis claviramea, Malawi. Journal of Bryology 29: 279. Trzpil-Zwierzyk, B. 8c K. Pi?rek. 2003. [Abstract] Distribution of Leucobryum glaucum (Hedw.) Aongstr. and Leucobryo Pinetum W. Mat. (1962) 1973 in central eastern Poland. Page 56. In A. Brzeg 8c M. Lisiewska (eds.), Zr?znicowanie, Dynamika i Przeksztalcenia Roslinnosci Borowej [Differentiation, Dynamics and Transformations of Coniferous Forests]. Sorus, Poznan, Poland. [In Polish] Urmi, E. 2007. Hans-Peter Senn, 1947-2006. Meylania 39: 6-8. Vana, J. 8c A. Sch?fer-Verwimp. 2007. New national and regional bryophyte records, 17. 10. lungermannia borneensis, Indonesia, Sumatra. Journal of Bryology 29: 278-279. Vanderpoorten, A., N. Devos, B. Goffinet, O. J. Hardy 8c A. J. Shaw. 2008. The barriers to oceanic island radiation in bryophytes: insights from the phylogeography of the moss Grimmia montana. Journal of Biogeography 35: 654-663. Vigo i Bonada, J. 2007. In memoriam: Dra. Creu Casas Sicart (1913-2007). Anales del Jard?n Bot?nico de Madrid 64: 243-244. Wang, Hong, Yan-Cheng Jiang 8c Jun Su. 2008. Anatomy studies of four mosses in Glacier No. 1 of Xinjiang. Bulletin of Botanical Research 28: 25-29. [In Chinese with English abstract and figure captions; on Barbula asperfolia, Tortella humilis, Mnium heterophyllum 8c Hypnum plumaeforme] Watling, R. 2007. Cryptogams in a horticultural setting in Scotland. Sibbaldia 5: 109-114. Wennersten, L. 2007. Frystolerans hos mossor. Myrinia 17: 61 72. [In Swedish with English abstract; on freezing tolerance of bryophytes] Wilson, A. 8c J. West. 2007. Vale Helen Joan Hewson 1938 2007. Australian Systematic Botany Society Newsletter 133: 37-41. [Includes botanical bibliography of this Australian hepaticologist] W?jciak, H. 2007. Porosty, Mszaki, Paprotniki. Wydanie (ed.) II. 368 pp. MULTICO, Warsaw, Poland. [Color guide book to Polish lichens, bryophytes and pteridophytes; bryophytes on pp. 198-299] Wu, Xiu-zhen 8c Hong-xiang Lou. 2007. Chemical ecology of bryophytes. Natural Product Research and Development 19: 1073-1078. [In Chinese with English abstract] Wu, Yu-Huan, Chien Gao 8c Guo-Dong Cheng. 2008. Primary study on Hepaticae from Mt. Qilian. Bulletin of Botanical Research 28: 147-150. [In Chinese with English abstract] Yamada, K., S. Isono 8c T. Hiraoka. 2007. A new locality of Calypogeia asakawana S. Hatt. ex Inoue. Bryological Research 9: 204. [In Japanese, from Honshu] -8c Jong-Suk Song. 2007. Liverworts mainly from Seongnamsa and Eoreumgol of Mt. Kaji, South Korea. Natural and Environmental Science Research 20: 97-99. [In Japanese with English abstract] Yokoyama, H., T. Yamaguchi, N. Nishimura, T. Furuki 8c H. Akiyama. 2007. Checklist of bryophytes known from Yakushima Island, southern Kyushu, Japan. Bryological Research 9: 159-197.</page></plain_text>