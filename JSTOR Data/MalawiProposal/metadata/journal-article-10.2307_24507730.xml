<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">publchoi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000024</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Public Choice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00485829</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737101</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24507730</article-id>
         <title-group>
            <article-title>The devil is in the shadow. Do institutions affect income and productivity or only official income and official productivity?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Axel</given-names>
                  <surname>Dreher</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pierre-Guillaume</given-names>
                  <surname>Méon</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Friedrich</given-names>
                  <surname>Schneider</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">158</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24506407</issue-id>
         <fpage>121</fpage>
         <lpage>141</lpage>
         <permissions>
            <copyright-statement>© 2014 Springer Science+Business Media</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24507730"/>
         <abstract>
            <p>This paper assesses the relationship between institutions, output, and productivity when official output is corrected for the size of the shadow economy. Our results confirm the usual positive impact of institutional quality on official output and total factor productivity, and its negative impact on the size of the underground economy. However, once output is corrected for the shadow economy, the relationship between institutions and output becomes weaker. The impact of institutions on total ("corrected") factor productivity becomes insignificant. Differences in corrected output must then be attributed to differences in factor endowments. These results survive several tests for robustness.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d484e220a1310">
            <label>'</label>
            <p>
               <mixed-citation id="d484e227" publication-type="other">
Dreher and Schneider 2010</mixed-citation>
            </p>
         </fn>
         <fn id="d484e234a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d484e241" publication-type="other">
Friedman et al.
(2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e250" publication-type="other">
Schneider and Enste (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e256" publication-type="other">
Johnson et al. 1997</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e263" publication-type="other">
Dreher et al. 2012</mixed-citation>
            </p>
         </fn>
         <fn id="d484e270a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d484e277" publication-type="other">
Caselli (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e283" publication-type="other">
ILO 1972</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e289" publication-type="other">
Gërxhani 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e296" publication-type="other">
Tanzi (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e302" publication-type="other">
Hillman et al. (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e308" publication-type="other">
Dreher et al. (2012)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e314" publication-type="other">
Pritchett (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e320" publication-type="other">
Caselli and Malhotra (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d484e327a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d484e334" publication-type="other">
Dreher et al. (2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d484e342a1310">
            <label>"</label>
            <p>
               <mixed-citation id="d484e349" publication-type="other">
Castellucci 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d484e356a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d484e363" publication-type="other">
Dreher et al. (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d484e370a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d484e377" publication-type="other">
Caselli (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e383" publication-type="other">
Hall and Jones (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e389" publication-type="other">
Prescott (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e396" publication-type="other">
Collins et al. (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d484e403a1310">
            <label>"</label>
            <p>
               <mixed-citation id="d484e410" publication-type="other">
Dreheret al. (2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d484e417a1310">
            <label>'^</label>
            <p>
               <mixed-citation id="d484e424" publication-type="other">
Dreher et a). (2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d484e431a1310">
            <label>l5</label>
            <p>
               <mixed-citation id="d484e438" publication-type="other">
Caselli (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d484e446a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d484e453" publication-type="other">
Hall and Jones (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d484e459" publication-type="other">
Abu-Qam and Abu-Bader
(2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d484e469a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d484e476" publication-type="other">
Dreher et al. 2012</mixed-citation>
            </p>
         </fn>
         <fn id="d484e483a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d484e490" publication-type="other">
Dreher et al. (2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d484e497a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d484e504" publication-type="other">
Acemoglu et al. (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d484e511a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d484e518" publication-type="other">
Dreher et al. 2012</mixed-citation>
            </p>
         </fn>
         <fn id="d484e525a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d484e532" publication-type="other">
Dreher
et al. (2012)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d484e551a1310">
            <mixed-citation id="d484e555" publication-type="other">
Abu-Qarn. A. S., &amp; Abu-Bader, S. (2007). Sources of growth revisited: evidence from selected MENA coun-
tries. World Development, 35, 752-771.</mixed-citation>
         </ref>
         <ref id="d484e565a1310">
            <mixed-citation id="d484e569" publication-type="other">
Abu-Qarn, A. S., &amp; Abu-Bader, S. (2009). Getting income shares right: a panel data investigation of OECD
countries. Economic Development Quarterly, 23, 254-266.</mixed-citation>
         </ref>
         <ref id="d484e579a1310">
            <mixed-citation id="d484e583" publication-type="other">
Acemoglu, D„ Johnson, S„ &amp; Robinson, J. A. (2001). The colonial origins of comparative development: an
empirical investigation. American Economic Review, 91, 1369-1401.</mixed-citation>
         </ref>
         <ref id="d484e593a1310">
            <mixed-citation id="d484e597" publication-type="other">
Acemoglu, D., Johnson, S., &amp; Robinson, J. A. (2005). Institutions as a fundamental cause of long-run growth.
In P. Aghion &amp; S. Durlauf (Eds), Handbook of economic growth (Vol. 1, pp. 385-472).</mixed-citation>
         </ref>
         <ref id="d484e608a1310">
            <mixed-citation id="d484e612" publication-type="other">
Alesina, Α., Easterly, W.. Devleeschauwer, Α.. Kurlat, S., &amp; Wacziarg. R. (2003). Fractionalization. Journal
of Economic Growth, 8, 155-194.</mixed-citation>
         </ref>
         <ref id="d484e622a1310">
            <mixed-citation id="d484e626" publication-type="other">
Aiyar, S., &amp; Dalgaard, C.-J. (2009). Accounting for productivity: is it OK to assume that the world is Cobb-
Douglas? Journal of Macroeconomics, 31. 290-303.</mixed-citation>
         </ref>
         <ref id="d484e636a1310">
            <mixed-citation id="d484e640" publication-type="other">
Barro, R. J., &amp; Lee, J.-W. (2001). International data on educational attainment: updates and implications.
Oxford Economic Papers, 53, 541 -563.</mixed-citation>
         </ref>
         <ref id="d484e650a1310">
            <mixed-citation id="d484e654" publication-type="other">
Brunetti, Α., &amp; Weder. B. (1998). Investment and institutional uncertainty: a comparative study of different
uncertainty measures. Wellwirtschaftliches Archiv, 134, 513-533.</mixed-citation>
         </ref>
         <ref id="d484e664a1310">
            <mixed-citation id="d484e668" publication-type="other">
Caselli, F. (2005). Accounting for cross-country income differences. In P. Aghion &amp; S. Durlauf (Eds.), Hand-
book of economic growth (pp. 679-742). Amsterdam: Elsevier.</mixed-citation>
         </ref>
         <ref id="d484e678a1310">
            <mixed-citation id="d484e682" publication-type="other">
Caselli, F., &amp; Malhotra, P. (2004). Natural disasters and growth: from thought experiment to natural experi-
ment (Working paper). Harvard University.</mixed-citation>
         </ref>
         <ref id="d484e693a1310">
            <mixed-citation id="d484e697" publication-type="other">
Castellucci. L. (2007). Fighting the shadow economy in Italy: dilemmas and policies effectiveness. Paper
prepared for the Conference on: "Undeclared work, tax evasion and avoidance: a momentum for change
in Belgium and Europe"—Brussels, 20-22 June.</mixed-citation>
         </ref>
         <ref id="d484e710a1310">
            <mixed-citation id="d484e714" publication-type="other">
Cavalcanti Ferreira, P., Issler, J. V., &amp; de Abreu Pessôa. S. (2004). Testing production functions used in
empirical growth studies. Economics Letters, 83, 29-35.</mixed-citation>
         </ref>
         <ref id="d484e724a1310">
            <mixed-citation id="d484e728" publication-type="other">
Chong, Α., &amp; Gradstein, M. (2007). Inequality and informality. Journal iff Public Economics, 91, 159-179.</mixed-citation>
         </ref>
         <ref id="d484e735a1310">
            <mixed-citation id="d484e739" publication-type="other">
Collins, S. M„ Bosworth. B. P., &amp; Rodrik, D. (1996). Economic growth in East Asia: accumulation versus
assimilation. Brookings Papers on Economic Activity, 1996(2), 135-203.</mixed-citation>
         </ref>
         <ref id="d484e749a1310">
            <mixed-citation id="d484e753" publication-type="other">
Dabla-Norris, E„ Gradstein. M., &amp; Inchauste, G. (2008). What causes firms to hide output? The determinants
of informality. Journal of Development Economics, 85, 1-27.</mixed-citation>
         </ref>
         <ref id="d484e763a1310">
            <mixed-citation id="d484e767" publication-type="other">
Dollar, D„ &amp; Kraay, A. (2003). Institutions, trade and growth. Journal of Monetary Economics, 50, 133-162.</mixed-citation>
         </ref>
         <ref id="d484e775a1310">
            <mixed-citation id="d484e779" publication-type="other">
Dreher, Α., Méon, P.-G., &amp; Schneider, F. (2012). The devil is in the shadow—do institutions affect income and
productivity or only official income and official productivity '! (Centre Emile Bernheim working paper
No. 12-019).</mixed-citation>
         </ref>
         <ref id="d484e792a1310">
            <mixed-citation id="d484e796" publication-type="other">
Dreher, Α.. &amp; Schneider, F. (2010). Corruption and the shadow economy: an empirical analysis. Public
Choice, 144, 215-238.</mixed-citation>
         </ref>
         <ref id="d484e806a1310">
            <mixed-citation id="d484e810" publication-type="other">
Easterly, W., &amp; Sewadeh. M. (2001). Global development network growth database. http://www.worldbank.
org/research/growth/GDNdata.htm.</mixed-citation>
         </ref>
         <ref id="d484e820a1310">
            <mixed-citation id="d484e824" publication-type="other">
Friedman. E„ Johnson. S., Kaufmann, D„ &amp; Zoido-Lobaton. P. (2000). Dodging the grabbing hand: the
determinants of unofficial activity in 69 countries. Journal of Public Economics, 76, 459-493.</mixed-citation>
         </ref>
         <ref id="d484e834a1310">
            <mixed-citation id="d484e838" publication-type="other">
Gërxhani, Κ. (2004). The informal sector in developed and less developed countries: a literature survey.
Public Choice, 120, 267-300.</mixed-citation>
         </ref>
         <ref id="d484e848a1310">
            <mixed-citation id="d484e852" publication-type="other">
Hall, R., &amp; Jones, C. I. ( 1999). Why do some countries produce so much more output per worker than others?
Quarterly Journal of Economics, 114. 83-116.</mixed-citation>
         </ref>
         <ref id="d484e863a1310">
            <mixed-citation id="d484e867" publication-type="other">
Henisz, W. J. (2000). The institutional environment for economic growth. Economics and Politics, 12, 1-31.</mixed-citation>
         </ref>
         <ref id="d484e874a1310">
            <mixed-citation id="d484e878" publication-type="other">
Heston. Α., Summers, R„ &amp; Aten, B. (2006). Penn world table version 6.2. Center for International Compar-
isons of Production, Income and Prices at the University of Pennsylvania, September.</mixed-citation>
         </ref>
         <ref id="d484e888a1310">
            <mixed-citation id="d484e892" publication-type="other">
Hillman, A. L., Mitov, L„ &amp; Peters, R. K. (1995). The private sector, state enterprises, and informal eco-
nomic activity. In Z. Bogeti» &amp; A. L. Hillman (Eds.), Financing government in transition, Bulgaria:
the political economy of tax policies, tax bases, and tax evasion (pp. 47-70). Washington: The World
Bank.</mixed-citation>
         </ref>
         <ref id="d484e908a1310">
            <mixed-citation id="d484e912" publication-type="other">
Hindriks, J., Muthoo, Α., &amp; Keen, M. ( 1999). Corruption, extortion and evasion. Journal of Public Economics,
74, 395-430.</mixed-citation>
         </ref>
         <ref id="d484e922a1310">
            <mixed-citation id="d484e926" publication-type="other">
Hirschman. A. O. (1970). Exit, voice and loyalty. Cambridge: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d484e933a1310">
            <mixed-citation id="d484e937" publication-type="other">
International Labor Office (1972). Employment, income and equality: a strategy for increasing productivity
in Kenya. ILO.</mixed-citation>
         </ref>
         <ref id="d484e948a1310">
            <mixed-citation id="d484e952" publication-type="other">
Johnson, S., Kaufmann, D., &amp; Shleifer, A. (1997). The unofficial economy in transition. Brookings Papers on
Economic Activity, 2, 159-221.</mixed-citation>
         </ref>
         <ref id="d484e962a1310">
            <mixed-citation id="d484e966" publication-type="other">
Johnson, S., Kaufmann. D.. &amp; Zoido-Lobatôn, P. (1998a). Regulatory discretion and the unofficial economy.
American Economic Review, 88, 387-392.</mixed-citation>
         </ref>
         <ref id="d484e976a1310">
            <mixed-citation id="d484e980" publication-type="other">
Johnson. S., Kaufmann, D., &amp; Zoido-Lobatôn, P. (1998b). Corruption, public finances and the unofficial
economy (World Bank discussion paper 2169).</mixed-citation>
         </ref>
         <ref id="d484e990a1310">
            <mixed-citation id="d484e994" publication-type="other">
Kaufmann, D.. Kraay, Α., &amp; Mastruzzi, M. (2006). Governance matters V: governance indicators for 1996-
2005 (World Bank working paper No. 4012).</mixed-citation>
         </ref>
         <ref id="d484e1004a1310">
            <mixed-citation id="d484e1008" publication-type="other">
King, R. G., &amp; Levine, R. (1994). Capital fundamentalism, economic development, and economic growth.
Carnegie-Rochester Conference Series on Public Policy, 40, 259-292.</mixed-citation>
         </ref>
         <ref id="d484e1018a1310">
            <mixed-citation id="d484e1022" publication-type="other">
Klenow, P., &amp; Rodriguez-Clare, A. (1997). The neoclassical revival in growth economics: has it gone too
far? In B. S. Bernanke &amp; J. J. Rotemberg (Eds.), NBER macroeconomics annual 1997. Cambridge: MIT
Press.</mixed-citation>
         </ref>
         <ref id="d484e1036a1310">
            <mixed-citation id="d484e1040" publication-type="other">
Knack, P., &amp; Keefer, S. (1995). Institutions and economic performance: cross-country tests using alternative
institutional measures. Economics and Politics, 7, 207-227.</mixed-citation>
         </ref>
         <ref id="d484e1050a1310">
            <mixed-citation id="d484e1054" publication-type="other">
Kneller, R„ &amp; Stevens, P. A. (2003). The specification of the aggregate production function in the presence
of inefficiency. Economics Letters, 81, 223-226.</mixed-citation>
         </ref>
         <ref id="d484e1064a1310">
            <mixed-citation id="d484e1068" publication-type="other">
Lambsdorff, J. Graf. (2003). How corruption affects productivity. Kyklos, 56,457-474.</mixed-citation>
         </ref>
         <ref id="d484e1075a1310">
            <mixed-citation id="d484e1079" publication-type="other">
Mauro, P. (1995). Corruption and growth. Quarterly Journal of Economics, 110, 681-712.</mixed-citation>
         </ref>
         <ref id="d484e1086a1310">
            <mixed-citation id="d484e1090" publication-type="other">
Méon, P.-G., &amp; Weill, L. (2005). Does better governance foster efficiency? An aggregate frontier analysis.
Economics of Governance, 6, 75-90.</mixed-citation>
         </ref>
         <ref id="d484e1100a1310">
            <mixed-citation id="d484e1104" publication-type="other">
Méon, P.-G., Sekkat, K„ &amp; Weill, L. (2009). Institutional reforms now and benefits tomorrow: how soon is
tomorrow? Economics and Politics, 21, 319-357.</mixed-citation>
         </ref>
         <ref id="d484e1115a1310">
            <mixed-citation id="d484e1119" publication-type="other">
North, D. C. ( 1994). Economic performance through time. American Economic Review, 84, 359-368.</mixed-citation>
         </ref>
         <ref id="d484e1126a1310">
            <mixed-citation id="d484e1130" publication-type="other">
Olson, M., Sarna, N., &amp; Swamy, Α. V. (2000). Governance and growth: a simple hypothesis explaining cross-
country differences in productivity growth. Public Choice, 102, 341-364.</mixed-citation>
         </ref>
         <ref id="d484e1140a1310">
            <mixed-citation id="d484e1144" publication-type="other">
Prescott, E. C. (1998). Needed: a theory of total factor productivity. International Economic Review, 39,
525-551.</mixed-citation>
         </ref>
         <ref id="d484e1154a1310">
            <mixed-citation id="d484e1158" publication-type="other">
Pritchett, L. (2000). The tyranny of concepts: CUDIE (Cumulated, Depreciated, Investment Effort) is not
capital. Journal of Economic Growth, 5, 361-384.</mixed-citation>
         </ref>
         <ref id="d484e1168a1310">
            <mixed-citation id="d484e1172" publication-type="other">
Rodrik, D„ Subramanian, Α., &amp; Trebbi, F. (2004). Institutions rule: the primacy of institutions over geography
and integration in economic development. Journal of Economic Growth, 9, 131-165.</mixed-citation>
         </ref>
         <ref id="d484e1182a1310">
            <mixed-citation id="d484e1186" publication-type="other">
Schneider, F. (2005a). Shadow economies of 145 countries all over the world: estimation results of the period
1999-2003 (Discussion paper). University of Linz: Department of Economics.</mixed-citation>
         </ref>
         <ref id="d484e1197a1310">
            <mixed-citation id="d484e1201" publication-type="other">
Schneider, F. (2005b). Shadow economies around the world: what do we really know? European Journal of
Political Economy, 21, 598-642.</mixed-citation>
         </ref>
         <ref id="d484e1211a1310">
            <mixed-citation id="d484e1215" publication-type="other">
Schneider, F. (2007). Shadow economies and corruption all over the world: new estimates for 145 countries.
Economics 2007-9.</mixed-citation>
         </ref>
         <ref id="d484e1225a1310">
            <mixed-citation id="d484e1229" publication-type="other">
Schneider, F„ &amp; Enste, D. H. (2000). Shadow economies: size, causes, and consequences. Journal of Eco-
nomic Literature, 38, 77-114.</mixed-citation>
         </ref>
         <ref id="d484e1239a1310">
            <mixed-citation id="d484e1243" publication-type="other">
Senhadji, A. (2000). Sources of economic growth: an extensive growth accounting exercise. IMF Staff Papers,
47, 129-157.</mixed-citation>
         </ref>
         <ref id="d484e1253a1310">
            <mixed-citation id="d484e1257" publication-type="other">
Staiger, D., &amp; Stock, J. H. (1997). Instrumental variables regression with weak instruments. Econometrica,
65, 557-586.</mixed-citation>
         </ref>
         <ref id="d484e1267a1310">
            <mixed-citation id="d484e1271" publication-type="other">
Tanaka, V. (2010). The 'informal sector' and the political economy of development. Public Choice, 145,
295-317.</mixed-citation>
         </ref>
         <ref id="d484e1282a1310">
            <mixed-citation id="d484e1286" publication-type="other">
Tanzi, V. (1999). Uses and abuses of estimates of the underground economy. Economic Journal, 109, 338—
340.</mixed-citation>
         </ref>
         <ref id="d484e1296a1310">
            <mixed-citation id="d484e1300" publication-type="other">
Teobaldelli, D. (2011). Federalism and the shadow economy. Public Choice, 146,269-289.</mixed-citation>
         </ref>
         <ref id="d484e1307a1310">
            <mixed-citation id="d484e1311" publication-type="other">
World Bank (2006). World development indicators. CD-Rom, Washington, DC.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
