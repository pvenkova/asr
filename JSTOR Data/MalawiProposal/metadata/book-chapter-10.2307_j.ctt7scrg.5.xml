<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7scrg</book-id>
      <subj-group>
         <subject content-type="call-number">JF60.W46 2004</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Legitimacy of governments</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">World politics</subject>
         <subj-group>
            <subject content-type="lcsh">1989–</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>When States Fail</book-title>
         <subtitle>Causes and Consequences</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>EDITED BY</role>
            <name name-style="western">
               <surname>Rotberg</surname>
               <given-names>Robert I.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>28</day>
         <month>07</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780691116723</isbn>
      <isbn content-type="ppub">0691116725</isbn>
      <isbn content-type="epub">9781400835799</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2004</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt7scrg"/>
      <abstract abstract-type="short">
         <p>Since 1990, more than 10 million people have been killed in the civil wars of failed states, and hundreds of millions more have been deprived of fundamental rights. The threat of terrorism has only heightened the problem posed by failed states.<italic>When States Fail</italic>is the first book to examine how and why states decay and what, if anything, can be done to prevent them from collapsing. It defines and categorizes strong, weak, failing, and collapsed nation-states according to political, social, and economic criteria. And it offers a comprehensive recipe for their reconstruction.</p>
         <p>The book comprises fourteen essays by leading scholars and practitioners who help structure this disparate field of research, provide useful empirical descriptions, and offer policy recommendations. Robert Rotberg's substantial opening chapter sets out a theory and taxonomy of state failure. It is followed by two sets of chapters, the first on the nature and correlates of failure, the second on methods of preventing state failure and reconstructing those states that do fail. Economic jump-starting, legal refurbishing, elections, the demobilizing of ex-combatants, and civil society are among the many topics discussed.</p>
         <p>All of the essays are previously unpublished. In addition to Rotberg, the contributors include David Carment, Christopher Clapham, Nat J. Colletta, Jeffrey Herbst, Nelson Kasfir, Michael T. Klare, Markus Kostner, Terrence Lyons, Jens Meierhenrich, Daniel N. Posner, Susan Rose-Ackerman, Donald R. Snodgrass, Nicolas van de Walle, Jennifer A. Widner, and Ingo Wiederhofer.</p>
      </abstract>
      <counts>
         <page-count count="336"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.3</book-part-id>
                  <title-group>
                     <title>List of Maps</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>R. I. R.</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.5</book-part-id>
                  <title-group>
                     <label>One</label>
                     <title>The Failure and Collapse of Nation-States:</title>
                     <subtitle>BREAKDOWN, PREVENTION, AND REPAIR</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROTBERG</surname>
                           <given-names>ROBERT I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Nation-states fail when they are consumed by internal violence and cease delivering positive political goods to their inhabitants. Their governments lose credibility, and the continuing nature of the particular nationstate itself becomes questionable and illegitimate in the hearts and minds of its citizens.</p>
                     <p>The rise and fall of nation-states is not new, but in a modern era when national states constitute the building blocks of world order, the violent disintegration and palpable weakness of selected African, Asian, Oceanic, and Latin American states threaten the very foundation of that system. International organizations and big powers consequently find themselves sucked disconcertingly into</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.6</book-part-id>
                  <title-group>
                     <label>Two</label>
                     <title>Domestic Anarchy, Security Dilemmas, and Violent Predation:</title>
                     <subtitle>CAUSES OF FAILURE</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KASFIR</surname>
                           <given-names>NELSON</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>When do security dilemmas or predation explain the violence that accompanies state failure? Most accounts of civil breakdowns attribute violence to preexisting social grievances. But the dangers and opportunities that emerge in response to the breakdown of state institutions may also help to explain why violence threatens, erupts, or intensifies. When individuals, as well as the groups they join, see their rivals acquiring arms, they are likely to become increasingly suspicious of their intentions. Some of them may also take advantage of the failure of the state to enrich themselves by raiding their weaker neighbors. Their search for safety or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.7</book-part-id>
                  <title-group>
                     <label>Three</label>
                     <title>The Global-Local Politics of State Decay</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CLAPHAM</surname>
                           <given-names>CHRISTOPHER</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>States are not unchanging features of the global political order. Neither the international system of states as we came to know it in the second half of the twentieth century, nor most of the individual states within it have any plausible claim to permanence. States have historically derived from various specific and by no means universally realized conditions, and the global political system has until recent times comprised areas under the control of states, areas regulated by other forms of governance, and areas with no stable governance at all. The idea that the state is a universal form of governance</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.8</book-part-id>
                  <title-group>
                     <label>Four</label>
                     <title>The Economic Correlates of State Failure:</title>
                     <subtitle>TAXES, FOREIGN AID, AND POLICIES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>VAN DE WALLE</surname>
                           <given-names>NICOLAS</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>In this chapter I examine the economic factors that predispose states to fail and/or to fall prey to serious internal conflict. As was made clear by Rotberg in the first chapter of this volume, the general term of state failure covers several distinct modes of political decay. It is possible to distinguish two sets of political-economy dynamics that lead to two distinct types of state failure: a first mode in which struggles over a viable state apparatus fuels civil conflict, and a second one in which protracted state weakness leads to a progressive implosion of the state apparatus. These two</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.9</book-part-id>
                  <title-group>
                     <label>Five</label>
                     <title>The Deadly Connection:</title>
                     <subtitle>PARAMILITARY BANDS, SMALL ARMS DIFFUSION, AND STATE FAILURE</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KLARE</surname>
                           <given-names>MICHAEL T.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>116</fpage>
                  <abstract>
                     <p>State failure usually results from the prolonged interaction of a number of powerful corrosive factors, including economic stagnation, political and ethnic factionalism, pervasive corruption, decaying national infrastructure, and environmental degradation. Typically, these factors operate over a long period of time, eroding civil institutions and undermining the authority of the state. At an early or intermediate stage of decay, it is still possible for an effective leader or leadership group to reverse the process and avert full state collapse; even without such leadership, an ailing state can remain in a weakened condition for many years without slipping into total disarray. But</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.10</book-part-id>
                  <title-group>
                     <label>Six</label>
                     <title>Preventing State Failure</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>CARMENT</surname>
                           <given-names>DAVID</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>Policymakers, and the research community charged with keeping them abreast of current events, too often fail to recognize the preconditions and preliminary events that culminate in situations of ethnic catastrophe and state failure. Had preliminary signals been more inquisitively monitored and the teachings of history relied upon as a dependable guide, policymakers might have been better informed. Foreign policy critics have accused foreign ministries, international organizations, and regional organizations of failing to foresee the occurrence of imminent events, failing to prevent outcomes that in hindsight were avoidable, failing to anticipate the costs of preventable occurrences, and failing to account for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.11</book-part-id>
                  <title-group>
                     <label>Seven</label>
                     <title>Forming States after Failure</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MEIERHENRICH</surname>
                           <given-names>JENS</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>The problem of state failure is one of the greatest political and humanitarian challenges facing the world in the twenty-first century. Failed states adversely affect the well-being of tens of millions of people the world over. They have been associated with civil war, ethnic war, and international war, but also with famine, drought, and other social and environmental disasters. Although the problem of state failure has moved to the forefront of international concerns in recent years, we know very little about what to do in situations where it has occurred.</p>
                     <p>This chapter reflects on the formation of states after failure.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.12</book-part-id>
                  <title-group>
                     <label>Eight</label>
                     <title>Disarmament, Demobilization, and Reintegration:</title>
                     <subtitle>LESSONS AND LIABILITIES IN RECONSTRUCTION</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>COLLETTA</surname>
                           <given-names>NAT J.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>KOSTNER</surname>
                           <given-names>MARKUS</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>WIEDERHOFER</surname>
                           <given-names>INGO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>170</fpage>
                  <abstract>
                     <p>Demilitarization is a precondition for reviving civil society, reducing poverty, and sustaining development in countries emerging from war. The realization of these objectives demands the demobilization of forces and the subsequent reintegration of ex-combatants into productive civilian lives. Full demilitarization often also requires landmine removal and disarmament, the reduction of arms flows, the conversion of fixed assets to civilian use, security sector reform, and civilian disarmament.</p>
                     <p>When and where violent conflict ceases, armed combatants are glaring legacies. Demobilization and Reintegration Programs (DRPs) for combatants thus constitute a vital part of demilitarization and of transitions from war to peace. The success</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.13</book-part-id>
                  <title-group>
                     <label>Nine</label>
                     <title>Establishing the Rule of Law</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROSE-ACKERMAN</surname>
                           <given-names>SUSAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>182</fpage>
                  <abstract>
                     <p>The rule of law has two fundamentally different aspects. The first sets legal limits, both civil and criminal, on private interactions. The second imposes limits on the political regime. By definition, a weak state cannot engage in organized predation, but it can disrupt lives and economic activity by its very weakness and its inability to control violence and the destruction of property. This distinction between private lawlessness and public impunity raises a difficult issue for those seeking to shore up weak states. Policies that strengthen the state may simply permit those in power to act with impunity and may encourage</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.14</book-part-id>
                  <title-group>
                     <label>Ten</label>
                     <title>Building Effective Trust in the Aftermath of Severe Conflict</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WIDNER</surname>
                           <given-names>JENNIFER A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>222</fpage>
                  <abstract>
                     <p>There are at least three broad political challenges in the aftermath of severe conflict. One is to attack the problems that have given rise to war with the aim of reducing the risk of renewed fighting. A second is to foster the attitudes and behavior essential to compromise and cooperation in the political realmand to expanded exchange and investment in economic life; cooperation and investment are vital for renewal. A third challenge is to take the idiosyncratic features of particular cases into account and to create space for experimentation and for learning. We know very little about “what works.” We</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.15</book-part-id>
                  <title-group>
                     <label>Eleven</label>
                     <title>Civil Society and the Reconstruction of Failed States</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>POSNER</surname>
                           <given-names>DANIEL N.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>237</fpage>
                  <abstract>
                     <p>Civil society is said to possess almost magical qualities for improving governmental performance—from promoting good health, reducing crime, and generating economic growth to facilitating political reform and easing the reintegration of ex-combatants after civil wars.¹ Given such (alleged) beneficial powers, one might inquire whether civil society could play a useful role in a context where the state is not simply underperforming but is unable to provide even the most basic services that people reasonably expect from it. This is precisely the situation in failed states. The purpose of this chapter is to explore whether civil society might serve as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.16</book-part-id>
                  <title-group>
                     <label>Twelve</label>
                     <title>Restoring Economic Functioning in Failed States</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SNODGRASS</surname>
                           <given-names>DONALD R.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>256</fpage>
                  <abstract>
                     <p>When nation-states fail, their aggregate production and per capita income decline absolutely, and sustained economic growth becomes impossible until state functioning is restored. This is what happened in Afghanistan, Angola, Burundi, the Congo, Liberia, Sierra Leone, and Somalia. Their economic performance sank. In Angola, GDP per capita (expressed in constant U.S. dollars) fell by 40 percent from 1980 to 1994.¹ Between 1988 and 1995, Burundi suffered a 22 percent drop in GDP per capita. The Congo experienced a long and severe economic decline even before the nation’s ills boiled up into a crisis. GDP per capita fell almost continuously from</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.17</book-part-id>
                  <title-group>
                     <label>Thirteen</label>
                     <title>Transforming the Institutions of War:</title>
                     <subtitle>POSTCONFLICT ELECTIONS AND THE RECONSTRUCTION OF FAILED STATES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LYONS</surname>
                           <given-names>TERRENCE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>269</fpage>
                  <abstract>
                     <p>Failed states often are analyzed as periods of chaos as the social, political, and economic institutions that support order disappear and violence fills the ensuing void. This chapter takes a different view. Rather than existing in an anarchic vacuum, actors engaged in conflict following state-collapse create alternative institutions that allow them to accumulate power and sustain the conflict. These social, political, and economic institutions are based on violence, fear, and predation. Institutions of war, such as militia organizations, black-market networks, and chauvinistic identity groups, develop and even thrive in the context of state failure. The challenge of reconstructing failed states</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.18</book-part-id>
                  <title-group>
                     <label>Fourteen</label>
                     <title>Let Them Fail: State Failure in Theory and Practice:</title>
                     <subtitle>IMPLICATIONS FOR POLICY</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HERBST</surname>
                           <given-names>JEFFREY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>302</fpage>
                  <abstract>
                     <p>The phenomenon of state failure is now, as this volume readily attests, a significant problem in several parts of Africa, and also a threat to some countries in central and southeast Asia. However, the international community—as demonstrated by the reverses in Liberia, Sierra Leone, and Somalia—has had a difficult time conceptualizing and implementing responses to profound state decline. Part of the analytical problem is that the post–World War II state system was extremely stable until the early 1990s, due to the adoption of only one unit (the nation-state) as the organizing principle, and to the extraordinary stability</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.19</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>319</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7scrg.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>323</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
