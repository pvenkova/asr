<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">jpoliecon</journal-id>
         <journal-title-group>
            <journal-title>Journal of Political Economy</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00223808</issn>
         <issn pub-type="epub">1537534X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor-stable">20109703</article-id>
         <article-id pub-id-type="doi">10.1086/527495</article-id>
         <article-id pub-id-type="msid">JPE32293</article-id>
         <title-group>
            <article-title>Corruption, Norms, and Legal Enforcement: Evidence from Diplomatic Parking Tickets<xref ref-type="fn" rid="fn1"/>
            </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Raymond</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Fisman</surname>
               </string-name>
            </contrib>
            <aff id="aff_1">Columbia University and National Bureau of Economic Research</aff>
         </contrib-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Edward</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Miguel</surname>
               </string-name>
            </contrib>
            <aff id="aff_2">University of California, Berkeley and National Bureau of Economic Research</aff>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>12</month>
            <year>2007</year>
            <string-date>December 2007</string-date>
         </pub-date>
         <volume>115</volume>
         <issue>6</issue>
         <issue-id>509552</issue-id>
         <fpage>1020</fpage>
         <lpage>1048</lpage>
         <permissions>
            <copyright-statement>© 2007 by The University of Chicago. All rights reserved.</copyright-statement>
            <copyright-year>2007</copyright-year>
            <copyright-holder>The University of Chicago</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/527495"/>
         <abstract>
            <p>We study cultural norms and legal enforcement in controlling corruption by analyzing the parking behavior of United Nations officials in Manhattan. Until 2002, diplomatic immunity protected UN diplomats from parking enforcement actions, so diplomats’ actions were constrained by cultural norms alone. We find a strong effect of corruption norms: diplomats from high‐corruption countries (on the basis of existing survey‐based indices) accumulated significantly more unpaid parking violations. In 2002, enforcement authorities acquired the right to confiscate diplomatic license plates of violators. Unpaid violations dropped sharply in response. Cultural norms and (particularly in this context) legal enforcement are both important determinants of corruption.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="fn1">
               <p>We thank Stefano Dellvigna, Seema Jayachandran, Dean Yang, Luigi Zingales, and seminar participants at the Harvard Development Economics Lunch, Harvard Behavioral Economics Seminar, Harvard Kennedy School of Government, Harvard Political Economy discussion group, University of Michigan, London School of Economics, University of Southern California, Stockholm University, University of California, Berkeley, Northwestern University, University of Hawaii, Syracuse University, Harvard Business School, World Bank, University of Toronto, two anonymous referees, and Steve Levitt for helpful suggestions. Daniel Hartley, Adam Sacarny, and Sarath Sanga provided superb research assistance. We thank Gillian Sorensen for helpful discussions. We are especially grateful to the New York City Department of Finance for providing us with data on parking violations and National Public Radio for alerting us to the existence of these data. All errors are our own.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
   <back>
      <app-group>
         <app id="apa">
            <label>Data Appendix</label>
            <title/>
            <sec id="sca1">
               <label>A. </label>
               <title>New York City Diplomatic Parking Violation Data</title>
               <p>The New York City Department of Finance supplied listings of all unpaid parking violations of UN missions. The violations covered the period from November 24, 1997, to November 21, 2005. In order to appear in the database, a violation had to go unpaid for at least 100 days. Data were at the level of the violation and included the following entries for each violation:<list list-type="bullet">
                     <list-item>
                        <label>•</label>
                        <p>Summons: unique identification number for the violation</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>License plate number of the violating car</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>The person to whom the violating car was registered, often the mission itself</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Time of violation: included both hour and minute as well as calendar date</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Type of violation, e.g., expired meter, fire hydrant</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Street address of violation</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Initial dollar value of fine issued</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Additional dollar penalty for not having paid the fine on time</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Amount paid toward the fine, generally zero</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Name of country to which the car is registered</p>
                     </list-item>
                  </list>
               </p>
               <p>Data on UN diplomats’ paid parking violations (violations that did not go into arrears) were made available to us in aggregate form by the New York City Department of Finance.</p>
            </sec>
            <sec id="sca2">
               <label>B. </label>
               <title>UN Blue Books</title>
               <p>The United Nations issues its list of mission personnel, or Blue Book, twice yearly. We utilize edition numbers 278 (January 1997) through 294 (October 2005). Documents were retrieved from the UN Official Document System, available at<ext-link ext-link-type="uri"
                            xlink:href="http://documents.un.org/advance.asp"
                            xlink:type="simple">http://documents.un.org/advance.asp</ext-link>. Searching for the symbol ST/SG/SER.A/### with truncation turned off returns the relevant Blue Book (where ### is the Blue Book edition number). Edition 280 (May 1998) was checked by hand to count the mission staff and spouses for each country in the Blue Book, producing the following variables:<list list-type="bullet">
                     <list-item>
                        <label>•</label>
                        <p>Mission: indicator variable indicating whether the country had a UN mission</p>
                     </list-item>
                     <list-item>
                        <label>•</label>
                        <p>Staff: simple count of staff (staff members are always listed with their surnames in bold)</p>
                     </list-item>
                  </list>
               </p>
               <p>To create the longitudinal data set of diplomat parking violations, the Blue Book data were first converted into filtered html format. A program was used to parse the name and country of each diplomat in each Blue Book. Next, diplomat names from the Blue Books were matched to the parking violation data. A name was considered to have “matched” if the country, last name, and first four letters of the first name corresponded across data sets. There was some manual double‐checking of apparent spelling typos and duplicate or otherwise confusing names. Diplomats are considered “present” in New York City in the analysis for the months after their first appearance in a Blue Book and before the first Blue Book in which they no longer appear. Since Blue Books are produced only sporadically, in months between a final Blue Book appearance and the first “nonappearance,” we assume arbitrarily that diplomats left New York at the midpoint in time between these months.</p>
            </sec>
            <sec id="sca3">
               <label>C. </label>
               <title>Diplomatic Vehicles</title>
               <p>Data were provided by Murray Smith, Deputy Director at the U.S. State Department’s Office of Foreign Missions in April 2006. These data report counts of the number of vehicles with diplomatic license plates registered to each mission in early 2006.</p>
            </sec>
         </app>
      </app-group>
      <ref-list content-type="unparsed">
         <title>References</title>
         <ref id="rf1">
            <mixed-citation xlink:type="simple" publication-type="">Agence France–Presse. 2005. “Speedy Diplomats Steer Clear of Fines in France.” March 16.</mixed-citation>
         </ref>
         <ref id="rf2">
            <mixed-citation xlink:type="simple" publication-type="">BBC News. 1998. “So What Are Diplomats Immune To?” October 28.<ext-link ext-link-type="uri"
                         xlink:href="http://news.bbc.co.uk/1/hi/special_report/1998/10/98/e-cyclopedia/196677.stm"
                         xlink:type="simple">http://news.bbc.co.uk/1/hi/special_report/1998/10/98/e‐cyclopedia/196677.stm</ext-link>.</mixed-citation>
         </ref>
         <ref id="rf3">
            <mixed-citation xlink:type="simple" publication-type="">Becker, Gary S. 1968. “Crime and Punishment: An Economic Approach.”<italic>J.P.E.</italic>76 (March/April): 169–217.</mixed-citation>
         </ref>
         <ref id="rf4">
            <mixed-citation xlink:type="simple" publication-type="">Borjas, George J. 2000. “The Economic Progress of Immigrants.” In<italic>Issues in the Economics of Immigration</italic>, edited by George J. Borjas. Chicago: Univ. Chicago Press (for NBER).</mixed-citation>
         </ref>
         <ref id="rf5">
            <mixed-citation xlink:type="simple" publication-type="">Di Tella, Rafael, and Ernesto Schargrodsky. 2003. “The Role of Wages and Auditing during a Crackdown on Corruption in the City of Buenos Aires.”<italic>J. Law and Econ.</italic>46 (April): 269–92.</mixed-citation>
         </ref>
         <ref id="rf6">
            <mixed-citation xlink:type="simple" publication-type="">Fernandez, Raquel, and Alessandra Fogli. 2006. “Fertility: The Role of Culture and Family Experience.”<italic>J. European Econ. Assoc.</italic>4 (April–May): 552–61.</mixed-citation>
         </ref>
         <ref id="rf7">
            <mixed-citation xlink:type="simple" publication-type="">Fisman, Raymond, and Edward Miguel. 2006. “Cultures of Corruption: Evidence from Diplomatic Parking Tickets.” Working Paper no. 12312 (June), NBER, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="rf8">
            <mixed-citation xlink:type="simple" publication-type="">Fries, Jacob H. 2002. “U.S. Revokes License Plates Issued to 185 at 30 Consulates.”<italic>New York Times</italic>, September 7.</mixed-citation>
         </ref>
         <ref id="rf9">
            <mixed-citation xlink:type="simple" publication-type="">Ichino, Andrea, and Giovanni Maggi. 2000. “Work Environment and Individual Background: Explaining Regional Shirking Differentials in a Large Italian Firm.”<italic>Q.J.E.</italic>115 (August): 1057–90.</mixed-citation>
         </ref>
         <ref id="rf10">
            <mixed-citation xlink:type="simple" publication-type="">Kaufmann, Daniel, Aart Kraay, and Massimo Mastruzzi. 2005. “Governance Matters IV: Governance Indicators for 1996–2004.” Policy Research Working Paper no. 3630, World Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="rf11">
            <mixed-citation xlink:type="simple" publication-type="">
               <italic>Korea Times</italic>. 1999. “Illegal Parking by Diplomats on Rise.” November 1.</mixed-citation>
         </ref>
         <ref id="rf12">
            <mixed-citation xlink:type="simple" publication-type="">Kuziemko, Ilyana, and Eric Werker. 2006. “How Much Is a Seat on the Security Council Worth? Foreign Aid and Bribery at the United Nations.”<italic>J.P.E.</italic>114 (October): 905–30.</mixed-citation>
         </ref>
         <ref id="rf13">
            <mixed-citation xlink:type="simple" publication-type="">Lambsdorff, Johann Graf. 2006. “Causes and Consequences of Corruption: What Do We Know from a Cross‐Section of Countries?” In<italic>International Handbook on the Economics of Corruption</italic>, edited by Susan Rose‐Ackerman. Northampton, MA: Elgar.</mixed-citation>
         </ref>
         <ref id="rf14">
            <mixed-citation xlink:type="simple" publication-type="">Ledyard, John O. 1995. “Public Goods: A Survey of Experimental Research.” In<italic>The Handbook of Experimental Economics</italic>, edited by John H. Kagel and Alvin E. Roth. Princeton, NJ: Princeton Univ. Press.</mixed-citation>
         </ref>
         <ref id="rf15">
            <mixed-citation xlink:type="simple" publication-type="">Levitt, Steven D. 1997. “Using Electoral Cycles in Police Hiring to Estimate the Effect of Police on Crime.”<italic>A.E.R.</italic>87 (June): 270–90.</mixed-citation>
         </ref>
         <ref id="rf16">
            <mixed-citation xlink:type="simple" publication-type="">———. 2004. “Understanding Why Crime Fell in the 1990s: Four Factors That Explain the Decline and Six That Do Not.”<italic>J. Econ. Perspectives</italic>18 (Winter): 163–90.</mixed-citation>
         </ref>
         <ref id="rf17">
            <mixed-citation xlink:type="simple" publication-type="">———. 2006. “An Economist Sells Bagels: A Case Study in Profit Maximization.” Working Paper no. 12152 (April), NBER, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="rf18">
            <mixed-citation xlink:type="simple" publication-type="">Mauro, Paolo. 2004. “The Persistence of Corruption and Slow Economic Growth.”<italic>IMF Staff Papers</italic>51 (1): 1–18.</mixed-citation>
         </ref>
         <ref id="rf19">
            <mixed-citation xlink:type="simple" publication-type="">Mayer, Thierry, and Soledad Zignago. 2005. “Market Access in Global and Regional Trade.” Working Paper no. 2005‐02 (January), Centre d'Etudes Prospectives et d'Informations Internationales, Paris.</mixed-citation>
         </ref>
         <ref id="rf20">
            <mixed-citation xlink:type="simple" publication-type="">Olken, Benjamin A. 2006. “Corruption Perceptions vs. Corruption Reality.” Manuscript, Harvard Univ.</mixed-citation>
         </ref>
         <ref id="rf21">
            <mixed-citation xlink:type="simple" publication-type="">Reinikka, Ritva, and Jakob Svensson. 2004. “Local Capture: Evidence from a Central Government Transfer Program in Uganda.”<italic>Q.J.E.</italic>119 (May): 679–705.</mixed-citation>
         </ref>
         <ref id="rf22">
            <mixed-citation xlink:type="simple" publication-type="">Rockwell, John. 2004. “Reverberations: Where Mimes Patrolled the Streets and the Mayor Was Superman.”<italic>New York Times</italic>, July 9.</mixed-citation>
         </ref>
         <ref id="rf23">
            <mixed-citation xlink:type="simple" publication-type="">Schiavo‐Campo, Salvatore, Giulio de Tommaso, and Amit Mukherjee. 1999. “An International Statistical Survey of Government Employment and Wages.” Policy Research Working Paper no. 1806, World Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="rf24">
            <mixed-citation xlink:type="simple" publication-type="">Singleton, Don. 2004. “Bill Socks Scofflaw Diplos.”<italic>New York Daily News</italic>, November 21, p. 28.</mixed-citation>
         </ref>
         <ref id="rf25">
            <mixed-citation xlink:type="simple" publication-type="">Steinhauer, Jennifer. 2002. “Suspension of Hostilities over Diplomats’ Tickets.”<italic>New York Times</italic>, August 23.</mixed-citation>
         </ref>
         <ref id="rf26">
            <mixed-citation xlink:type="simple" publication-type="">Tirole, Jean. 1996. “A Theory of Collective Reputations (with Applications to the Persistence of Corruption and to Firm Quality).”<italic>Rev. Econ. Studies</italic>63 (January): 1–22.</mixed-citation>
         </ref>
         <ref id="rf27">
            <mixed-citation xlink:type="simple" publication-type="">Witzel, Morgen. 2005. “How to Respond When Only Bribe Money Talks.”<italic>Financial Times</italic>, July 11, London ed. 1, p. 14.</mixed-citation>
         </ref>
      </ref-list>
      <sec sec-type="online-material">
         <title>Enhancement: Data</title>
         <list>
            <list-item>
               <p>
                  <inline-supplementary-material mimetype="application"
                                                 mime-subtype="zip"
                                                 xlink:show="new"
                                                 xlink:href="/doi/suppl/10.1086%2F527495/suppl_file/data.zip"
                                                 xlink:type="simple">data.zip</inline-supplementary-material>
               </p>
            </list-item>
         </list>
      </sec>
   </back>
</article>
