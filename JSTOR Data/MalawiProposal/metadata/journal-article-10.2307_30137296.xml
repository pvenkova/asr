<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30137296</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Patterns and Implications of Naturally Acquired Immune Responses to Environmental and Tuberculous Mycobacterial Antigens in Northern Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Gillian F.</given-names>
                  <surname>Black</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Hazel M.</given-names>
                  <surname>Dockrell</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Amelia C.</given-names>
                  <surname>Crampin</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Sian</given-names>
                  <surname>Floyd</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Rosemary E.</given-names>
                  <surname>Weir</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Lyn</given-names>
                  <surname>Bliss</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Lifted</given-names>
                  <surname>Sichali</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Lorren</given-names>
                  <surname>Mwaungulu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Huxley</given-names>
                  <surname>Kanyongoloka</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Bagrey</given-names>
                  <surname>Ngwira</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>David K.</given-names>
                  <surname>Warndorff</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Paul E. M.</given-names>
                  <surname>Fine</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">184</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i30137285</issue-id>
         <fpage>322</fpage>
         <lpage>329</lpage>
         <permissions>
            <copyright-statement>Copyright 2001 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30137296"/>
         <abstract>
            <p>Interferon (IFN)-γ responsiveness to 12 purified protein derivative (PPD) and new tuberculin antigens from 9 species of mycobacteria was assessed, using a whole blood assay, in 616 young adults living in northern Malawi, where Mycobacterium bovis bacille Calmette-Guérin (BCG) vaccination provides no protection against pulmonary tuberculosis. The prevalence of IFN-γ responsiveness was highest for PPDs of M. avium, M. intracellulare, and M. scrofulaceum (the MAIS complex). Correlations between responsiveness paralleled genetic relatedness of the mycobacterial species. A randomized, controlled trial was carried out, to assess the increase in IFN-γ responsiveness to M. tuberculosis PPD that can be attributed to M. bovis BCG vaccination. The BCG-attributable increase in IFN-γ response to M. tuberculosis PPD was greater for individuals with low initial responsiveness to MAIS antigens than for those with high initial responsiveness. Although not statistically significant, the trend is consistent with the hypothesis that prior exposure to environmental mycobacteria interferes with immune responses to BCG vaccination.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1184e303a1310">
            <label>1</label>
            <mixed-citation id="d1184e310" publication-type="other">
Fine PEM. Variation in protection by BCG: implications of and for hetero-
logous immunity. Lancet 1995; 346:1339-15.</mixed-citation>
         </ref>
         <ref id="d1184e320a1310">
            <label>2</label>
            <mixed-citation id="d1184e327" publication-type="other">
Karonga Prevention Trial Group. Randomized controlled trial of single BCG,
repeated BCG, or combined BCG and killed Mycobacterium leprae vac-
cine for prevention of leprosy and tuberculosis in Malawi. Lancet 1996;
348:17-24.</mixed-citation>
         </ref>
         <ref id="d1184e343a1310">
            <label>3</label>
            <mixed-citation id="d1184e350" publication-type="other">
British Thoracic Association. Effectiveness of BCG vaccination in Great
Britain in 1978. Br J Dis Chest 1980;74:215-27.</mixed-citation>
         </ref>
         <ref id="d1184e360a1310">
            <label>4</label>
            <mixed-citation id="d1184e367" publication-type="other">
Sutherland I, Springett VH. Effectiveness of BCG vaccination in England
and Wales in 1983. Tubercle 1987;68:81-92.</mixed-citation>
         </ref>
         <ref id="d1184e378a1310">
            <label>5</label>
            <mixed-citation id="d1184e385" publication-type="other">
Rodrigues LC, Gill N, Smith PG. BCG vaccination in the first year of life
protects children of Indian subcontinent ethnic origin against tuberculosis
in England. J Epidemiol Community Health 1991;45:78-80.</mixed-citation>
         </ref>
         <ref id="d1184e398a1310">
            <label>6</label>
            <mixed-citation id="d1184e405" publication-type="other">
Palmer CE, Long MW. Effects of infection with atypical mycobacteria on
BCG vaccination and tuberculosis. Am Rev Respir Dis 1966;94:553-68.</mixed-citation>
         </ref>
         <ref id="d1184e415a1310">
            <label>7</label>
            <mixed-citation id="d1184e422" publication-type="other">
Orme IM, Collins FM. Efficacy of Mycobacterium bovis BCG vaccination
in mice undergoing prior pulmonary infection with atypical mycobacteria.
Infect Immun 1984;44:28-32.</mixed-citation>
         </ref>
         <ref id="d1184e435a1310">
            <label>8</label>
            <mixed-citation id="d1184e442" publication-type="other">
Brown CA, Brown IN, Swinburne S. The effect of oral Mycobacterium vaccae
on subsequent responses of mice to BCG sensitization. Tubercle 1985; 66:
251-60.</mixed-citation>
         </ref>
         <ref id="d1184e455a1310">
            <label>9</label>
            <mixed-citation id="d1184e462" publication-type="other">
Hernandez-Pando R, Pavon L, Arriaga K, Orozco H, Madrid-Marina V,
Rook G. Pathogenesis of tuberculosis in mice exposed to low and high
doses of an environmental mycobacteria saprophyte before infection. In-
fect Immun 1997;65:3317-27.</mixed-citation>
         </ref>
         <ref id="d1184e478a1310">
            <label>10</label>
            <mixed-citation id="d1184e485" publication-type="other">
Lozes E, Denis O, Drowart A, et al. Cross-reactive immune responses against
Mycobacterium bovis BCG in mice infected with non-tuberculous myco-
bacteria belonging to the MAIS-Group. Scand J Immunol 1997;46:16-26.</mixed-citation>
         </ref>
         <ref id="d1184e499a1310">
            <label>11</label>
            <mixed-citation id="d1184e506" publication-type="other">
Palmer CE, Edwards LB. Identifying the tuberculous infected. JAMA
1968;205:167-9.</mixed-citation>
         </ref>
         <ref id="d1184e516a1310">
            <label>12</label>
            <mixed-citation id="d1184e523" publication-type="other">
Edwards LB, Acquaviva FA, Livesay VT, Cross FW, Palmer CE. An atlas
of sensitivity to tuberculin, PPD-B, and histoplasmin in the United States.
Am Rev Respir Dis 1969;99:1-132.</mixed-citation>
         </ref>
         <ref id="d1184e536a1310">
            <label>13</label>
            <mixed-citation id="d1184e543" publication-type="other">
Wilson ME, Fineberg HV, Colditz GA. Geographic latitude and the efficacy
of bacillus Calmette-Guerin vaccine. Clin Infect Dis 1995;20:982-91.</mixed-citation>
         </ref>
         <ref id="d1184e553a1310">
            <label>14</label>
            <mixed-citation id="d1184e560" publication-type="other">
Fine PEM, Sterne JAC, Ponnighaus JM, Rees RJW Delayed-type hyper-
sensitivity, mycobacterial vaccines and protective immunity. Lancet 1994;
344:1245-9.</mixed-citation>
         </ref>
         <ref id="d1184e573a1310">
            <label>15</label>
            <mixed-citation id="d1184e580" publication-type="other">
Sterne JAC, Ponnighaus JM, Fine PEM, Malema S. Geographic determi-
nants of leprosy in Karonga District, Northern Malawi. Int J Epidemiol
1995;24:1211-22.</mixed-citation>
         </ref>
         <ref id="d1184e593a1310">
            <label>16</label>
            <mixed-citation id="d1184e600" publication-type="other">
Fine PEM, Floyd S, Stanford JL, et al. Environmental mycobacteria in north-
ern Malawi: implications for the epidemiology of tuberculosis and leprosy.
Epidemiol Infect (in press).</mixed-citation>
         </ref>
         <ref id="d1184e614a1310">
            <label>17</label>
            <mixed-citation id="d1184e621" publication-type="other">
Fine PEM, Vynnycky E. The effect of heterologous immunity upon the ap-
parent efficacy of (eg BCG) vaccines. Vaccine 1998; 16:1923-8.</mixed-citation>
         </ref>
         <ref id="d1184e631a1310">
            <label>18</label>
            <mixed-citation id="d1184e638" publication-type="other">
Edwards LB, Comstock GW, Palmer CE. Contributions of northern popu-
lations to the understanding of tuberculin sensitivity. Arch Environ Health
1968;17:507-16.</mixed-citation>
         </ref>
         <ref id="d1184e651a1310">
            <label>19</label>
            <mixed-citation id="d1184e660" publication-type="other">
von Reyn CF, Barber TW, Arbeit RD, et al. Evidence of previous infection
with Mycobacterium avium-Mycobacterium intracellulare complex among
healthy subjects: an international study of dominant mycobacterial skin
test reactions. J Infect Dis 1993;168:1553-8.</mixed-citation>
         </ref>
         <ref id="d1184e676a1310">
            <label>20</label>
            <mixed-citation id="d1184e683" publication-type="other">
Hart PD'A, Sutherland I, Thomas J. The immunity conferred by effective
BCG and vole bacillus vaccines, in relation to individual variations in
tuberculin sensitivity and to technical variations in the vaccines. Tubercle
1967;48:201-10.</mixed-citation>
         </ref>
         <ref id="d1184e699a1310">
            <label>21</label>
            <mixed-citation id="d1184e706" publication-type="other">
Mosmann TR, Cherwinski H, Bond MW, Giedlin MA, Coffman RL. Two
types of murine helper T cell clone. I. Definition according to profiles of
lymphokine activities and secreted proteins. J Immun 1986; 136:2348-57.</mixed-citation>
         </ref>
         <ref id="d1184e719a1310">
            <label>22</label>
            <mixed-citation id="d1184e726" publication-type="other">
Cooper AM, Dalton DK, Stewart TA, Griffin JP, Russell DG, Orme IM.
Disseminated tuberculosis in interferon gamma gene-disrupted mice. J
Exp Med 1993;178:2243-7.</mixed-citation>
         </ref>
         <ref id="d1184e740a1310">
            <label>23</label>
            <mixed-citation id="d1184e747" publication-type="other">
Flynn JL, Chan J, Triebold KJ, Dalton DK, Stewart TA, Bloom BR. An
essential role for interferon gamma in resistance to Mycobacterium tu-
berculosis infection. J Exp Med 1993;178:2249-54.</mixed-citation>
         </ref>
         <ref id="d1184e760a1310">
            <label>24</label>
            <mixed-citation id="d1184e767" publication-type="other">
Jouanguy E, Altare F, Lamhamedi S, et al. Interferon-γ-receptor deficiency
in an infant with fatal bacille Calmette-Guérin infection. N Engl J Med
1996;335:1956-61.</mixed-citation>
         </ref>
         <ref id="d1184e780a1310">
            <label>25</label>
            <mixed-citation id="d1184e787" publication-type="other">
Weir RE, Morgan AR, Britton WJ, Butlin CR, Dockrell HM. Development
of a whole blood assay to measure T cell responses to leprosy: a new tool
for immuno-epidemiological field studies of leprosy immunity. J Immunol
Methods 1994;176:93-101.</mixed-citation>
         </ref>
         <ref id="d1184e803a1310">
            <label>26</label>
            <mixed-citation id="d1184e810" publication-type="other">
Weir RE, Brennan PJ, Butlin CR, Dockrell HM. Use of a whole blood assay
to evaluate in vitro T cell responses to new leprosy skin test antigens in
leprosy patients and healthy subjects. Clin Exp Immunol 1999; 116:263-9.</mixed-citation>
         </ref>
         <ref id="d1184e823a1310">
            <label>27</label>
            <mixed-citation id="d1184e830" publication-type="other">
Elliott AM, Hurst TJ, Balyeku MN, et al. The immune response to Myco-
bacterium tuberculosis in HIV-infected and uninfected adults in Uganda:
application of a whole blood cytokine assay in an epidemiological study.
Int J Tuberc Lung Dis 1999;3:239-47.</mixed-citation>
         </ref>
         <ref id="d1184e846a1310">
            <label>28</label>
            <mixed-citation id="d1184e853" publication-type="other">
Black GF, Fine PEM, Warndorff DK, et al. Relationship between IFN-γ
and skin test responsiveness to Mycobacterium tuberculosis PPD in healthy,
non-BCG-vaccinated young adults in northern Malawi. Int J Tuberc Lung
Dis (in press).</mixed-citation>
         </ref>
         <ref id="d1184e870a1310">
            <label>29</label>
            <mixed-citation id="d1184e877" publication-type="other">
Weir RE, Butlin CR, Neupane KD, Failbus SS, Dockrell HM. Use of a whole
blood assay to monitor the immune response to mycobacterial antigens in
leprosy patients: a predictor for type 1 reaction onset? Lepr Rev 1998; 69:
279-93.</mixed-citation>
         </ref>
         <ref id="d1184e893a1310">
            <label>30</label>
            <mixed-citation id="d1184e900" publication-type="other">
Magnusson M, Bentzon MW Preparation of purified tuberculin RT23. Bull
WHO 1958;19:829-43.</mixed-citation>
         </ref>
         <ref id="d1184e910a1310">
            <label>31</label>
            <mixed-citation id="d1184e917" publication-type="other">
International standard for purified protein derivative of avian tuberculin.
WHO Tech Rep Series 1955; 96:11.</mixed-citation>
         </ref>
         <ref id="d1184e927a1310">
            <label>32</label>
            <mixed-citation id="d1184e934" publication-type="other">
Edwards LB, Palmer CE. Epidemiologic studies of tuberculin sensitivity. 1.
Preliminary results with purified protein derivatives prepared from atypical
acid-fast organisms. Am J Hyg 1958;68:213-31.</mixed-citation>
         </ref>
         <ref id="d1184e947a1310">
            <label>33</label>
            <mixed-citation id="d1184e954" publication-type="other">
Stanford JL, Rook G, Samuel N, et al. Preliminary immunological studies
in search of correlates of protective immunity carried out on some Iranian
leprosy patients and their families. Lepr Rev 1980;51:303-14.</mixed-citation>
         </ref>
         <ref id="d1184e967a1310">
            <label>34</label>
            <mixed-citation id="d1184e974" publication-type="other">
Skjot RL, Oettinger T, Rosenkrands I, et al. Comparative evaluation of low-
molecular-mass proteins from Mycobacterium tuberculosis identifies mem-
bers of the ESAT-6 family as immunodominant T-cell antigens. Infect
Immun 2000;68:214-20.</mixed-citation>
         </ref>
         <ref id="d1184e991a1310">
            <label>35</label>
            <mixed-citation id="d1184e998" publication-type="other">
Pitulle C, Dorsch M, Kazda J, Wolters J, Stackebrandt E. Phylogeny of
rapidly growing members of the genus Mycobacterium. Int J Syst Bacteriol
1992;42:337-43.</mixed-citation>
         </ref>
         <ref id="d1184e1011a1310">
            <label>36</label>
            <mixed-citation id="d1184e1018" publication-type="other">
Fine PEM, Bruce J, Ponnighaus JM, Nkhosa P, Harawa A, Vynnycky E.
Tuberculin sensitivity: conversions and reversions in a rural African pop-
ulation. Int J Tuberc Lung Dis 1999;3:962-75.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
