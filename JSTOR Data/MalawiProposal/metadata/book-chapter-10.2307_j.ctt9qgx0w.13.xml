<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt9qgx0w</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The shame of it</book-title>
         <subtitle>Global perspectives on anti-poverty policies</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Gubrium</surname>
               <given-names>Erika K.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Pellissery</surname>
               <given-names>Sony</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Lødemel</surname>
               <given-names>Ivar</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>11</day>
         <month>12</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9781447308713</isbn>
      <isbn content-type="epub">9781447308720</isbn>
      <publisher>
         <publisher-name>Policy Press</publisher-name>
         <publisher-loc>Clifton, Bristol, UK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Policy Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt9qgx0w"/>
      <abstract abstract-type="short">
         <p>The shame experienced by people living in poverty has long been recognised. Nobel laureate and economist, Amartya Sen, has described shame as the irreducible core of poverty. However, little attention has been paid to the implications of this connection in the making and implementation of anti-poverty policies. This important volume rectifies this critical omission and demonstrates the need to take account of the psychological consequences of poverty for policy to be effective. Drawing on pioneering empirical research in countries as diverse as Britain, Uganda, Norway, Pakistan, India, South Korea and China, it outlines core principles that can aid policy makers in policy development. In so doing, it provides the foundation for a shift in policy learning on a global scale and bridges the traditional distinctions between North and South, and high-, middle- and low-income countries. This will help students, academics and policy makers better understand the reasons for the varying effectiveness of anti-poverty policies.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.3</book-part-id>
                  <title-group>
                     <title>Notes on contributors</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Walker</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.6</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>Resetting the stage</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gubrium</surname>
                           <given-names>Erika K.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In 1971, sociologist Robert Pinker made a frank observation about the field of social policy: ‘We know much more about the sentiments of philosophers and social scientists than those of ordinary people in everyday life’ (1971, p 135). The statement is as relevant today as it was then. Whether focused in the global South on intensive development and reconstruction, or in the global North on moving the long-term unemployed into the labour market, public social policy approaches all too frequently ‘take for granted the subjective perceptions of ordinary people’ (1971, p 136) rather than subjecting them to empirical investigation. This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.7</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>New urban poverty and new welfare provision:</title>
                     <subtitle>China’s dibao system</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Yan</surname>
                           <given-names>Ming</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>As the most populous country and carrying the oldest continuous civilisation, China has undergone drastic political and social changes in modern times. Its dynastic system that had lasted centuries was ended and replaced by the Republic in 1911, ending the Imperial rule of over 2,000 years. Following a series of civil wars, the Sino-Japanese War and revolution, the country regained peace and unity as the Communist Party established the People’s Republic in 1949. For the past six decades, the development of the Chinese economy can broadly be divided into two phases: the planned economy or socialist period until 1979, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.8</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>Thick poverty, thicker society and thin state:</title>
                     <subtitle>policy spaces for human dignity in India</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pellissery</surname>
                           <given-names>Sony</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mathew</surname>
                           <given-names>Leemamol</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>India is home to the largest number of poor people in the world. Of 1.13 billion people, 27.8 per cent live below the conservative income-based poverty line set by the Indian government and can thus be said to live in absolute poverty (Planning Commission, 2012). The face of this deep and persistent poverty is observable in minimal health expenditure, in the fact that more than 50 per cent of the population lives without sanitation facilities, in the presence of undignified ageing, as well as in poor educational standards, malnourished bodies, inferior housing, poor infrastructure resulting in deterioration in the quality</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.9</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Self-sufficiency, social assistance and the shaming of poverty in South Korea</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jo</surname>
                           <given-names>Yongmie Nicola</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Walker</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>South Korea was re-established in 1948 as an independent democratic nation after being annexed by Japan in 1910 for 35 years and experiencing three years of militaristic rule by the US between 1945 and 1948. However, the Korean War (1950-53) subsequently led to its partition into the Republic of Korea, better known as South Korea, and North Korea, the Democratic People’s Republic. Since the end of the war, South Korea has experienced a dramatic transformation in virtually every aspect of its social and economic life. From a country characterised by abject poverty, it has become the world’s 15th largest economy,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.10</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>‘Not good enough’:</title>
                     <subtitle>social assistance and shaming in Norway</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gubrium</surname>
                           <given-names>Erika K.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lødemel</surname>
                           <given-names>Ivar</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>Norway is a thin, stretched country with a population of just over 5 million. It is one of the world’s richest countries, and its residents possess among the lowest variability in living standards (The World Bank, 2011). There is a long-standing consensus among all political parties that the quality of health, education and social services should be equalised as much as possible throughout the nation. Hence, the Norwegian welfare state is built on the ‘citizenship’ principle, with schemes, for the most part, financed through general taxes and a tax system that is redistributive in nature (Kuhnle, 1994a, p 81). This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.11</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>Pakistan:</title>
                     <subtitle>a journey of poverty-induced shame</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Choudhry</surname>
                           <given-names>Sohail</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>A federal parliamentary republic of over 180 million people, Pakistan has the sixth largest population and 27th largest gross domestic product (GDP) purchasing power parity (PPP) in the world (IMF, 2012). However, its multidimensional poverty headcount stands at 49.4 per cent and it ranks 145th on the Human Development Index (OPHI, 2011; UN, 2011). Although the responsibility for policy theoretically rests with the Cabinet and individual ministers, because of a feeble and erratic democracy, it is often senior civil servants who assume the central roles in the conception, framing and delivery of policies.</p>
                     <p>This chapter begins by tracing the evolution</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.12</book-part-id>
                  <title-group>
                     <label>SEVEN</label>
                     <title>Separating the sheep from the goats:</title>
                     <subtitle>tackling poverty in Britain for over four centuries</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Walker</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chase</surname>
                           <given-names>Elaine</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>133</fpage>
                  <abstract>
                     <p>The UK is a comparatively small, highly centralised state that operates a first past the post-electoral system.¹ This, combined with strong party loyalty, typically provides ministers with largely unfettered authority to reform policy while in government. The electoral system has also delivered comparatively long spells of government, often lasting two or three five-year terms. This means that it is not unheard of for substantive policy programmes to be implemented and then radically reversed by opposition parties subsequently entering government with a mission and mandate for reform. This said, the potential for inchoate and reactive policy-making is partially mitigated by a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.13</book-part-id>
                  <title-group>
                     <label>EIGHT</label>
                     <title>‘Food that cannot be eaten’:</title>
                     <subtitle>the shame of Uganda’s anti-poverty policies</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kyomuhendo</surname>
                           <given-names>Grace Bantebya</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mwiine</surname>
                           <given-names>Amon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>Over the past 25 years, the varying approaches encompassed within Uganda’s anti-poverty effort have been touted as a best case model in the developing world (Hickey, 2011). Uganda is a land-locked country lying astride the equator in east central Africa. It is closely linked by economic and colonial history to Kenya in the east, Tanzania in the south, South Sudan and the Democratic Republic of Congo (DRC) to the north and west respectively, and Rwanda in the southwest. Agriculture is the mainstay of the economy, contributing 31 per cent of the gross domestic product (GDP), 85 per cent of exports</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.14</book-part-id>
                  <title-group>
                     <label>NINE</label>
                     <title>Shame and shaming in policy processes</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pellissery</surname>
                           <given-names>Sony</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lødemel</surname>
                           <given-names>Ivar</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gubrium</surname>
                           <given-names>Erika K.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>179</fpage>
                  <abstract>
                     <p>In the first chapter of this volume, Erika Gubrium showed how novel concerns in the current global stage have inspired our research on the relationship between shame and anti-poverty policy. As the volume by Chase and Bantebya (2014) demonstrates, shame is closely linked to poverty regardless of the existence and nature of anti-poverty policies. And where such programmes are in place, an encounter with officialdom is only one of many arenas where a poor person is reminded of his or her inferior status. Still, as the preceding chapters have demonstrated, and as we summarise here, policies and programmes have the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.15</book-part-id>
                  <title-group>
                     <label>TEN</label>
                     <title>Towards global principles for dignity-based anti-poverty policies</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gubrium</surname>
                           <given-names>Erika K.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lødemel</surname>
                           <given-names>Ivar</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>The context in which policy-making and delivery occurs is important when analysing the impact of policy. A brief example illustrates this point. One of the low-income respondents in rural Uganda invited us to her homestead. Her home was a hut made of dried mud with no windows. She hoped to be able to mend the thatch roof before the next rainy season. The empty dirt floor was just big enough for the floor mats that were rolled out at bedtime. Her family of five spent most of the time outside in front of the hut, where they prepared meals, washed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>221</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgx0w.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>232</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
