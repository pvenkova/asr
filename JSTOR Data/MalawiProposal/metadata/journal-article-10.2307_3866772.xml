<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">stafpapeintemone</journal-id>
         <journal-id journal-id-type="jstor">j100190</journal-id>
         <journal-title-group>
            <journal-title>Staff Papers (International Monetary Fund)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Monetary Fund</publisher-name>
         </publisher>
         <issn pub-type="ppub">00208027</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/3866772</article-id>
         <title-group>
            <article-title> Exchange Rate Variability: Alternative Measures and Interpretation (Variabilité du taux de change: différentes manières de la mesurer et interprétation) (Variabilidad de los tipos de cambio: Otras posibles medidas e interpretación de las mismas) </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Anthony</given-names>
                  <surname>Lanyi</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Esther C.</given-names>
                  <surname>Suss</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1982</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i294964</issue-id>
         <fpage>527</fpage>
         <lpage>560</lpage>
         <page-range>527-560</page-range>
         <permissions>
            <copyright-statement/>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3866772"/>
         <abstract>
            <p> Many countries have attempted to choose exchange rate arrangements that limit the variability of their exchange rates in order to avert costs arising from such variability. The paper discusses some of these costs--both those incurred during the short run, when contract prices in foreign trade are fixed, and those incurred in the longer run. Three alternative indices for measuring variability are defined: an EV index, the weighted sum of bilateral exchange rate variabilities; a VEER index, the variability of an effective exchange rate index; and an RVEER index, the variability of an index of effective exchange rates adjusted by relative price levels. It is argued that the EV index is the most relevant for assessing short-run costs, while the VEER and RVEER indices are more relevant for assessing longer-run costs. Each index is calculated for the actual exchange rate developments for 118 countries from 1973 to 1980 and is simulated for six alternative exchange arrangements. By comparing the actual variability index with the simulated values, it is shown that the magnitude of exchange rate variability is influenced by the choice of arrangement and that the same arrangement often does not minimize both measures of variability, so that countries may have to choose, in effect, between minimizing short-run or long-run costs. Most countries, however, could have simultaneously minimized all three measures of variability with a common arrangement. The fact that many countries in this position did not in fact do so indicates that other important considerations affect the choice of exchange arrangement. It is nevertheless notable that most countries choose an exchange arrangement under which exchange rate variability, as defined by one or more of the measures, did not differ significantly from the minimum as defined in this paper. /// Maints pays se sont efforcés de choisir un régime de change susceptible de limiter la variabilité de leur taux de change pour éviter les coûts qui en découlent. Les auteurs examinent certains de ces coûts -- les coûts à court terme lorsque les prix forfaitaires pratiqués dans les échanges internationaux sont fixes et à la fois les coûts à long terme. Ils établissent plusieurs indices possibles permettant de mesurer la variabilité: un indice EV qui correspond à la somme pondérée des variabilités du taux de change bilatéral; un indice VEER, soit la variabilité d'un indice de taux de change effectif; et un indice RVEER, soit la variabilité des taux de change effectifs ajustés en fonction des niveaux des prix relatifs. Il est soutenu que l'indice EV convient mieux pour évaluer les coûts à court terme, tandis que les indices VEER et RVEER conviennent mieux pour évaluer les coûts à long terme. Les auteurs ont calculé chaque indice de l'évolution variable du taux de change dans 118 pays entre 1973 et 1980 et ils ont effectué des simulations pour six régimes de change différents. Une comparaison entre l'indice de variabilité et les valeurs obtenues par simulation indique que le choix du régime de change influe sur l'ampleur de la variabilité du taux de change et que souvent le même régime ne minimise pas les deux mesures de la variabilité; de ce fait, il se peut que les pays doivent choisir entre minimiser les coûts à court terme ou minimiser les coûts à long terme. La plupart des pays, toutefois, auraient pu simultanément minimiser les trois mesures de la variabilité au moyen d'un régime de change commun. Le fait que de nombreux pays dans cette situation n'en ont rien fait indique que d'autres considérations importantes interviennent dans le choix du régime de change. Néanmoins, il est remarquable que la plupart des pays choisissent un régime de change dans le cadre duquel la variabilité du taux de change, définie par une ou plusieurs de ces mesures, ne diffère pas de manière significative du minimum tel qu'il est défini dans la présente étude. /// Muchos países han tratado de escoger sistemas cambiarios que limiten la variabilidad de los tipos de cambio de sus monedas con el fin de evitar los costos que esa variabilidad entraña. En este trabajo se consideran algunos de dichos costos, tanto los incurridos a corto plazo, cuando los precios contratados en el intercambio comercial son fijos, como los incurridos a plazo más largo. Se definen tres índices para medir la variabilidad: a) índice EV, o suma ponderada de la variabilidad de los tipos de cambio bilaterales; b) índice VEER, o variabili- dad del tipo de cambio efectivo, y c) índice RVEER, o variabilidad de los tipos de cambio efectivos ajustada mediante los niveles de precios relativos. Se sostiene que el índice EV es el más adecuado para evaluar los costos a corto plazo, mientras que los índices VEER y RVEER se prestan mejor a la evaluación de los costos a largo plazo. En el trabajo se calcula cada índice en función de la evolución efectiva de los tipos de cambio en 118 países de 1973 a 1980, y se simula la evolución que se habría manifestado con otros seis posibles sistemas cambiarios. Comparando el índice de variabilidad efectiva con los valores simulados, se comprueba que la magnitud de la variabilidad de los tipos de cambio está condicionada por la selección del sistema cambiario y que, frecuentemente, el mismo sistema no minimiza ambas medidas de variabilidad. Así pues, los países tendrían que escoger entre minimizar los costos a corto plazo o a largo plazo. La mayoría de los países, sin embargo, podrían haber minimizado simultáneamente los tres medidas de variabilidad si hubieran establecido un sistema común. El hecho de que muchos países que hubieran podido establecerlo no lo hicieran indica que hay otras consideraciones importantes que influyen a la hora de escoger el sistema cambiario. No obstante, es de observar que la mayoría de los países han escogido un sistema cambiario según el cual la variabilidad del tipo de cambio, en función de uno o más de los índices del estudio, no difirió significativamente del mínimo definido en este trabajo. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1631e161a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1631e168" publication-type="other">
Clark (1973)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e174" publication-type="other">
Dreyer and others (1978)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e180" publication-type="other">
IMF (1979 a; 1979 b)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e187a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1631e194" publication-type="other">
Viner (1943)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e200" publication-type="other">
Meade (1951)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e206" publication-type="other">
Friedman (1953)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e213" publication-type="other">
Lanyi
(1969)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e223a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1631e230" publication-type="other">
Rhomberg (1976)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e237a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1631e244" publication-type="other">
Frankel (1975)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e250" publication-type="other">
Suss (1976)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e256" publication-type="other">
Bigman (1978)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e264a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1631e271" publication-type="other">
Crockett and Nsouli (1975)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e277" publication-type="other">
Suss (1976)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e283" publication-type="other">
Kafka (1978)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e290a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1631e297" publication-type="other">
Page (1977)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e303" publication-type="other">
Magee and Rao (1980)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e310a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1631e317" publication-type="other">
Magee (1973)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e324a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1631e331" publication-type="other">
Clark (1973)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e338a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1631e345" publication-type="other">
Hooper and Kohlhagen (1978)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e352a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1631e359" publication-type="other">
Rhomberg (1976)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e367a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1631e374" publication-type="other">
Feltenstein, Goldstein, and Schadler (1979)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e381a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1631e388" publication-type="other">
Black (1976)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e395a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1631e402" publication-type="other">
Rhomberg (1976)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e408" publication-type="other">
Spitäller (1980)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e414" publication-type="other">
Bélanger (1976)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1631e421" publication-type="other">
Feltenstein, Goldstein, and Schadler (1979)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e428a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1631e435" publication-type="other">
Lipschitz (1979)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e442a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1631e449" publication-type="other">
Ben-Bassat (1980)</mixed-citation>
            </p>
         </fn>
         <fn id="d1631e456a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1631e463" publication-type="other">
Lanyi (1969)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>Bibliography</title>
         <ref id="d1631e479a1310">
            <mixed-citation id="d1631e483" publication-type="journal">
Bélanger, Gerard, "An Indicator of Effective Exchange Rates for Primary Pro¬
ducing Countries/' Staff Papers, Vol. 23 (March1976), pp. 113-36<person-group>
                  <string-name>
                     <surname>Bélanger</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>113</fpage>
               <volume>23</volume>
               <source>Staff Papers</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e518a1310">
            <mixed-citation id="d1631e524" publication-type="journal">
Ben-Bassat, Avraham, "The Optimal Composition of Foreign Exchange
Reserves," Journal of International Economics, Vol. 10 (May1980),
pp. 285-95<person-group>
                  <string-name>
                     <surname>Ben-Bassat</surname>
                  </string-name>
               </person-group>
               <issue>May</issue>
               <fpage>285</fpage>
               <volume>10</volume>
               <source>Journal of International Economics</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e562a1310">
            <mixed-citation id="d1631e566" publication-type="book">
Bigman, David, "Exchange Rate Determination in the Short Run" (un¬
published, International Monetary Fund, May 24, 1978)<person-group>
                  <string-name>
                     <surname>Bigman</surname>
                  </string-name>
               </person-group>
               <source>Exchange Rate Determination in the Short Run</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e591a1310">
            <mixed-citation id="d1631e595" publication-type="book">
Black, Stanley W., Exchange Policies for Less Developed Countries in a World
of Floating Rates, Essays in International Finance, No. 119, International
Finance Section, Princeton University (December 1976)<person-group>
                  <string-name>
                     <surname>Black</surname>
                  </string-name>
               </person-group>
               <source>Exchange Policies for Less Developed Countries in a World of Floating Rates</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e625a1310">
            <mixed-citation id="d1631e629" publication-type="journal">
Clark, Peter B., "Uncertainty, Exchange Risk, and the Level of International
Trade," Western Economic Journal, Vol. 11 (September1973), pp. 302-13<person-group>
                  <string-name>
                     <surname>Clark</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>302</fpage>
               <volume>11</volume>
               <source>Western Economic Journal</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e664a1310">
            <mixed-citation id="d1631e668" publication-type="book">
Crockett, Andrew D., and Saleh M. Nsouli, "Exchange Rate Policies for Devel-
oping Countries" (unpublished, International Monetary Fund, August 1,
1975)<person-group>
                  <string-name>
                     <surname>Crockett</surname>
                  </string-name>
               </person-group>
               <source>Exchange Rate Policies for Developing Countries</source>
               <year>1975</year>
            </mixed-citation>
            <mixed-citation id="d1631e696" publication-type="journal">
Journal of Development
Studies, Vol. 13 (January1977), pp. 125-43<issue>January</issue>
               <fpage>125</fpage>
               <volume>13</volume>
               <source>Journal of Development Studies</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e722a1310">
            <mixed-citation id="d1631e726" publication-type="book">
Dreyer, Jacob S., Gottfried Haberler, and Thomas D. Willett (eds.), Exchange
Rate Flexibility, American Enterprise Institute for Public Policy Research
(Washington, 1978)<person-group>
                  <string-name>
                     <surname>Dreyer</surname>
                  </string-name>
               </person-group>
               <source>Exchange Rate Flexibility</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e755a1310">
            <mixed-citation id="d1631e759" publication-type="journal">
Feltenstein, Andrew, Morris Goldstein, and Susan M. Schadler, "A Multilateral
Exchange Rate Model for Primary Producing Countries," Staff Papers,
Vol. 26 (September1979), pp. 543-82<person-group>
                  <string-name>
                     <surname>Feltenstein</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>543</fpage>
               <volume>26</volume>
               <source>Staff Papers</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e797a1310">
            <mixed-citation id="d1631e801" publication-type="book">
Frankel, Jeffrey, "Pegging to Minimize Short-Run Exchange Uncertainty: The
Exchange Rate Policies of Small Countries in a System of Generalized
Floating" (unpublished, International Monetary Fund, December 12,
1975)<person-group>
                  <string-name>
                     <surname>Frankel</surname>
                  </string-name>
               </person-group>
               <source>Pegging to Minimize Short-Run Exchange Uncertainty: The Exchange Rate Policies of Small Countries in a System of Generalized Floating</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e833a1310">
            <mixed-citation id="d1631e837" publication-type="book">
Friedman, Milton, "The Case for Flexible Exchange Rates," in his Essays in
Positive Economics (University of Chicago Press, 1953), pp. 157-203<person-group>
                  <string-name>
                     <surname>Friedman</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Case for Flexible Exchange Rates</comment>
               <fpage>157</fpage>
               <source>Essays in Positive Economics</source>
               <year>1953</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e870a1310">
            <mixed-citation id="d1631e874" publication-type="journal">
Hooper, Peter, and Steven W. Kohlhagen, "The Effect of Exchange Rate Un¬
certainty on the Prices and Volume of International Trade," Journal of
International Economics, Vol. 8 (November1978), pp. 483-511<person-group>
                  <string-name>
                     <surname>Hooper</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>483</fpage>
               <volume>8</volume>
               <source>Journal of International Economics</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e912a1310">
            <mixed-citation id="d1631e916" publication-type="book">
International Monetary Fund (1979 a), Annual Report of the Executive Board for
the Financial Year Ended April 30, 1979 (Washington, 1979)<person-group>
                  <string-name>
                     <surname>International Monetary Fund</surname>
                  </string-name>
               </person-group>
               <source>Annual Report of the Executive Board for the Financial Year Ended April 30, 1979</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e941a1310">
            <mixed-citation id="d1631e945" publication-type="journal">
— (1979 b), IMF Survey, Vol. 8 (October 15, 1979)<person-group>
                  <string-name>
                     <surname>International Monetary Fund</surname>
                  </string-name>
               </person-group>
               <issue>October 15</issue>
               <volume>8</volume>
               <source>IMF Survey</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e974a1310">
            <mixed-citation id="d1631e978" publication-type="journal">
Kafka, Alexandre, "The New Exchange Rate Regime and the Developing
Countries," Journal of Finance, Papers and Proceedings, Vol. 33 (June
1978), pp. 795-802<person-group>
                  <string-name>
                     <surname>Kafka</surname>
                  </string-name>
               </person-group>
               <issue>June</issue>
               <fpage>795</fpage>
               <volume>33</volume>
               <source>Journal of Finance, Papers and Proceedings</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1016a1310">
            <mixed-citation id="d1631e1020" publication-type="book">
Lanyi, Anthony, The Case for Floating Exchange Rates Reconsidered, Essays in
International Finance, No. 72, International Finance Section, Princeton
University (February 1969)<person-group>
                  <string-name>
                     <surname>Lanyi</surname>
                  </string-name>
               </person-group>
               <source>The Case for Floating Exchange Rates Reconsidered</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1049a1310">
            <mixed-citation id="d1631e1053" publication-type="journal">
Lipschitz, Leslie, "Exchange Rate Policy for a Small Developing Country, and
the Selection of an Appropriate Standard," Staff Papers, Vol. 26 (Septem-
ber1979), pp. 423-49<person-group>
                  <string-name>
                     <surname>Lipschitz</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>423</fpage>
               <volume>26</volume>
               <source>Staff Papers</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1092a1310">
            <mixed-citation id="d1631e1096" publication-type="journal">
—, and V. Sundararajan, "The Optimal Basket in a World of Generalized
Floating," Staff Papers, Vol. 27 (March1980), pp. 80-100<person-group>
                  <string-name>
                     <surname>Lipschitz</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>80</fpage>
               <volume>27</volume>
               <source>Staff Papers</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1131a1310">
            <mixed-citation id="d1631e1135" publication-type="journal">
Magee, Stephen P., "Currency Contracts, Pass-Through, and Devaluation,"
Brookings Papers on Economic Activity: 1 (1973), pp. 303-25<object-id pub-id-type="doi">10.2307/2534091</object-id>
               <fpage>303</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1631e1151a1310">
            <mixed-citation id="d1631e1157" publication-type="journal">
-, and Ramesh K.S. Rao, "Vehicle and Nonvehicle Currencies in Inter-
national Trade," American Economic Review, Papers and Proceedings,
Vol. 70 (May1980), pp. 368-73<object-id pub-id-type="jstor">10.2307/1815500</object-id>
               <fpage>368</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1631e1176a1310">
            <mixed-citation id="d1631e1180" publication-type="book">
Meade, J.E., The Theory of International Economic Policy: Volume One—The
Balance of Payments (Oxford University Press, 1951)<person-group>
                  <string-name>
                     <surname>Meade</surname>
                  </string-name>
               </person-group>
               <source>The Theory of International Economic Policy: Volume One—The Balance of Payments</source>
               <year>1951</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1205a1310">
            <mixed-citation id="d1631e1209" publication-type="journal">
Page, S.A.B., "Currency of Invoicing in Merchandise Trade," National Institute
Economic Review, No. 81 (August 1977), pp. 77-81<person-group>
                  <string-name>
                     <surname>Page</surname>
                  </string-name>
               </person-group>
               <issue>81</issue>
               <fpage>77</fpage>
               <source>National Institute Economic Review</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1241a1310">
            <mixed-citation id="d1631e1245" publication-type="journal">
Rhomberg, Rudolf R., "Indices of Effective Exchange Rates," Staff Papers,
Vol. 23 (March1976), pp. 88-112<person-group>
                  <string-name>
                     <surname>Rhomberg</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>88</fpage>
               <volume>23</volume>
               <source>Staff Papers</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1281a1310">
            <mixed-citation id="d1631e1285" publication-type="journal">
Spitäller, Erich, "Short-Run Effects of Exchange Rate Changes on Terms of
Trade and the Trade Balance," Staff Papers, Vol. 27 (June1980),
pp. 320-48<person-group>
                  <string-name>
                     <surname>Spitäller</surname>
                  </string-name>
               </person-group>
               <issue>June</issue>
               <fpage>320</fpage>
               <volume>27</volume>
               <source>Staff Papers</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1323a1310">
            <mixed-citation id="d1631e1327" publication-type="book">
Suss, Esther C, "The Variability of Exchange Rates Under Alternative Re-
gimes" (unpublished, International Monetary Fund, December 3, 1976)<person-group>
                  <string-name>
                     <surname>Suss</surname>
                  </string-name>
               </person-group>
               <source>The Variability of Exchange Rates Under Alternative Regimes</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1352a1310">
            <mixed-citation id="d1631e1356" publication-type="journal">
Viner, Jacob, "Two Plans for International Monetary Stabilization," Yale Re-
view, Vol. 33 (September1943), pp. 77-107<person-group>
                  <string-name>
                     <surname>Viner</surname>
                  </string-name>
               </person-group>
               <issue>September</issue>
               <fpage>77</fpage>
               <volume>33</volume>
               <source>Yale Review</source>
               <year>1943</year>
            </mixed-citation>
         </ref>
         <ref id="d1631e1391a1310">
            <mixed-citation id="d1631e1395" publication-type="book">
Wihlborg, Clas, Currency Risks in International Financial Markets, Princeton
Studies in International Finance, No. 44, International Finance Section,
Princeton University (December 1978)<person-group>
                  <string-name>
                     <surname>Wihlborg</surname>
                  </string-name>
               </person-group>
               <source>Currency Risks in International Financial Markets</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
