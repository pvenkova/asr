<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id journal-id-type="jstor">j101235</journal-id>
         <journal-title-group>
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>World Bank</publisher-name>
         </publisher>
         <issn pub-type="ppub">02586770</issn>
         <issn pub-type="epub">1564698X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3989922</article-id>
         <title-group>
            <article-title>The Current Account in Developing Countries: A Perspective from the Consumption-Smoothing Approach</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Atish R.</given-names>
                  <surname>Ghosh</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Jonathan D.</given-names>
                  <surname>Ostry</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>1995</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">9</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i382780</issue-id>
         <fpage>305</fpage>
         <lpage>333</lpage>
         <page-range>305-333</page-range>
         <permissions>
            <copyright-statement>Copyright 1995 The International Bank for Reconstruction and Development/The World Bank</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3989922"/>
         <abstract>
            <p>According to the consumption-smoothing view, a high degree of capital mobility implies that agents are able to fully smooth their consumption in the face of shocks. This article develops a framework to test whether, indeed, the current account in developing countries acts as a buffer to smooth consumption in the face of shocks to national cash flow, which is defined as output less investment less government expenditure. Using vector autoregression analysis, we estimate the optimal consumption-smoothing current account with data from a sample of forty-five developing countries. We find that for a majority of the countries, the hypothesis of full consumption smoothing cannot be rejected, suggesting that capital mobility may after all be quite high in this group of countries.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d323e132a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d323e139" publication-type="other">
Ostry and Reinhart (1992).</mixed-citation>
            </p>
         </fn>
         <fn id="d323e146a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d323e153" publication-type="other">
Campbell's (1987)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d323e169a1310">
            <mixed-citation id="d323e173" publication-type="journal">
Campbell, John. 1987. "Does Saving Anticipate Declining Labor Income? An Alternative
Test of the Permanent Income Hypothesis." Econometrica55:1249-73.<object-id pub-id-type="doi">10.2307/1913556</object-id>
               <fpage>1249</fpage>
            </mixed-citation>
         </ref>
         <ref id="d323e189a1310">
            <mixed-citation id="d323e193" publication-type="journal">
Campbell, John, and Robert Shiller. 1987. "Cointegration and Tests of Present Value
Models." Journal of Political Economy95(October):1062-88.<object-id pub-id-type="jstor">10.2307/1833129</object-id>
               <fpage>1062</fpage>
            </mixed-citation>
         </ref>
         <ref id="d323e209a1310">
            <mixed-citation id="d323e213" publication-type="book">
Cooper, Richard N., and Jeffrey D. Sachs. 1985. "Borrowing Abroad: The Debtor's
Perspective." In John T. Cuddington and Gordon Whitford Smith, eds., International
Debt and the Developing Countries. Washington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Cooper</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Borrowing Abroad: The Debtor's Perspective</comment>
               <source>International Debt and the Developing Countries</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d323e245a1310">
            <mixed-citation id="d323e249" publication-type="journal">
Dooley, Michael, Jeffrey Frankel, and Donald J. Mathieson. 1987. "International Capital
Mobility: What Do Saving-Investment Correlations Tell Us?" IMF Staff Paper
34:503-30.<person-group>
                  <string-name>
                     <surname>Dooley</surname>
                  </string-name>
               </person-group>
               <fpage>503</fpage>
               <volume>34</volume>
               <source>IMF Staff Paper</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d323e285a1310">
            <mixed-citation id="d323e289" publication-type="book">
Engel, Robert F., and Byung Sam Yoo. 1987. "Forecasting and Testing in Cointegrated
Systems." University of California Discussion Paper. University of California, Depart-
ment of Economics, San Diego. Processed.<person-group>
                  <string-name>
                     <surname>Engel</surname>
                  </string-name>
               </person-group>
               <source>Forecasting and Testing in Cointegrated Systems</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d323e318a1310">
            <mixed-citation id="d323e322" publication-type="book">
Frenkel, Jacob A., and Assaf Razin. 1987. Fiscal Policies and the World Economy: An
Intertemporal Approach. Cambridge, Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Frenkel</surname>
                  </string-name>
               </person-group>
               <source>Fiscal Policies and the World Economy: An Intertemporal Approach</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d323e347a1310">
            <mixed-citation id="d323e351" publication-type="journal">
Ghosh, Atish R.1990. "International Capital Mobility and Optimal Current Account
Behavior: An Empirical Investigation." John M. Olin Discussion Paper 50. Princeton
University, Woodrow Wilson School of Public and International Affairs, Princeton,
N.J. Processed. Forthcoming in Economic Journal.<person-group>
                  <string-name>
                     <surname>Ghosh</surname>
                  </string-name>
               </person-group>
               <source>Economic Journal</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d323e383a1310">
            <mixed-citation id="d323e387" publication-type="other">
IMF (International Monetary Fund). Various issues. International Financial Statistics.</mixed-citation>
         </ref>
         <ref id="d323e394a1310">
            <mixed-citation id="d323e398" publication-type="journal">
Mathieson, Donald, and Liliana Rojas-Suarez. 1992. "Liberalizing the Capital Account."
IMF Working Paper WP/92/46. Processed. Also published in Finance and Development
29(December):41-43.<person-group>
                  <string-name>
                     <surname>Mathieson</surname>
                  </string-name>
               </person-group>
               <issue>December</issue>
               <fpage>41</fpage>
               <volume>29</volume>
               <source>Finance and Development</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d323e436a1310">
            <mixed-citation id="d323e440" publication-type="journal">
Montiel, Peter J.1994. "Capital Mobility in Developing Countries: Some Measurement
Issues and Empirical Estimates." The World Bank Economic Review8(3):311-50.<person-group>
                  <string-name>
                     <surname>Montiel</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>311</fpage>
               <volume>8</volume>
               <source>The World Bank Economic Review</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d323e476a1310">
            <mixed-citation id="d323e480" publication-type="journal">
Ostry, Jonathan D., and Carmen Reinhart. 1992. "Private Saving and Terms of Trade
Shocks: Evidence from Developing Countries." IMF Staff Papers39 (3,
September):495-517.<person-group>
                  <string-name>
                     <surname>Ostry</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>495</fpage>
               <volume>39</volume>
               <source>IMF Staff Papers</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d323e518a1310">
            <mixed-citation id="d323e522" publication-type="journal">
Otto, Glenn. 1992. "Testing a Present-Value Model of the Current Account: Evidence
from U.S. and Canadian Time Series." Journal of International Money and Finance
11:414-30.<person-group>
                  <string-name>
                     <surname>Otto</surname>
                  </string-name>
               </person-group>
               <fpage>414</fpage>
               <volume>11</volume>
               <source>Journal of International Money and Finance</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d323e557a1310">
            <mixed-citation id="d323e561" publication-type="journal">
Sachs, Jeffrey D.1982. "The Current Account in the Macroeconomic Adjustment Pro-
cess." Scandinavian Journal of Economics84(2):147-64.<object-id pub-id-type="doi">10.2307/3439631</object-id>
               <fpage>147</fpage>
            </mixed-citation>
         </ref>
         <ref id="d323e577a1310">
            <mixed-citation id="d323e581" publication-type="book">
Sargent, Thomas J.1979. Macroeconomic Theory. San Diego: Academic Press.<person-group>
                  <string-name>
                     <surname>Sargent</surname>
                  </string-name>
               </person-group>
               <source>Macroeconomic Theory</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d323e603a1310">
            <mixed-citation id="d323e607" publication-type="journal">
Sheffrin, Steven M., and Wing Thye Woo. 1990. "Present Value Tests of an Intertemporal
Model of the Current Account." Journal of International Economics
29(November):237-53.<person-group>
                  <string-name>
                     <surname>Sheffrin</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>237</fpage>
               <volume>29</volume>
               <source>Journal of International Economics</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d323e645a1310">
            <mixed-citation id="d323e649" publication-type="book">
World Bank. Various years. World Tables. Baltimore: Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Tables</source>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
