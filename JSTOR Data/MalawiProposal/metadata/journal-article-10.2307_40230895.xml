<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">poprespolrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000506</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population Research and Policy Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01675923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737829</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40230895</article-id>
         <title-group>
            <article-title>The Evolution of Population Policies in Kenya and Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Chiweni</given-names>
                  <surname>Chimbwete</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Susan Cotts</given-names>
                  <surname>Watkins</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eliya Msiyaphazi</given-names>
                  <surname>Zulu</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">24</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40008480</issue-id>
         <fpage>85</fpage>
         <lpage>106</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 Springer</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40230895"/>
         <abstract>
            <p>Our case studies of the evolution of population policies in Kenya and Malawi offer insights into the interaction between the global population movement and national governments. The comparison is useful because it permits identifying the common strategies of a global movement, strategies that are likely to be evident elsewhere; it also permits identifying differences in national responses related to particular domestic contexts. We find a common repertory of movement strategies to influence the governments of Kenya and Malawi to adopt a neo-Malthusian population policy and to implement a family planning program. However, these strategies were promoted more or less aggressively depending on the national response and the chronological period. National responses were related to differences in the governments' approaches to nation-building, their willingness to accept foreign influence and the importance they placed on preserving cultural traditions, and to their assessment of benefits they would gain from responding favorably to movement proposals. The data come from written accounts and from interviews with international actors and Kenyan and Malawian elites who participated in the policy development process.</p>
         </abstract>
         <kwd-group>
            <kwd>comparative</kwd>
            <kwd>historical</kwd>
            <kwd>Kenya</kwd>
            <kwd>Malawi</kwd>
            <kwd>national</kwd>
            <kwd>policy</kwd>
            <kwd>population</kwd>
            <kwd>sub-saharan Africa</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1905e166a1310">
            <mixed-citation id="d1905e170" publication-type="other">
African Medical and Research Foundation (1979). Field work report on Kirinyaga Family Life
Training Centre. Nairobi: Health Behaviour and Education Department, African Medical
and Research Foundation.</mixed-citation>
         </ref>
         <ref id="d1905e183a1310">
            <mixed-citation id="d1905e187" publication-type="other">
Ajayi, A. &amp; Kekevole, J. (1998). Kenya's population policy: From apathy to effectiveness, pp.
113-156 in A. Jain (ed.), Do population policies matter? Fertility and politics in Egypt,
India, Kenya and Mexico. New York: The Population Council.</mixed-citation>
         </ref>
         <ref id="d1905e200a1310">
            <mixed-citation id="d1905e204" publication-type="other">
Barrett, D.A. (1995). Reproducing persons as a global concern: The making of an institution.
Unpublished PhD Dissertation. Stanford University.</mixed-citation>
         </ref>
         <ref id="d1905e214a1310">
            <mixed-citation id="d1905e218" publication-type="other">
Barrett, D. &amp; Tsui, A.O. (1999). Policy as symbolic statement: International response to
national population policies, Social Forces 78(1): 213-234.</mixed-citation>
         </ref>
         <ref id="d1905e229a1310">
            <mixed-citation id="d1905e233" publication-type="other">
Bates, R.H. (1981). Markets and states in tropical Africa: The political basis of agricultural
politics. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d1905e243a1310">
            <mixed-citation id="d1905e247" publication-type="other">
Chinyama, R. (1986). Child spacing in Malawi, The Malawian Nurse: Journal of the National
Association of Nurses of Malawi 2(1): 10.</mixed-citation>
         </ref>
         <ref id="d1905e257a1310">
            <mixed-citation id="d1905e261" publication-type="other">
Chipande, G. (1988). The impact of demographic changes on rural development in Malawi, pp.
163-174 in R.D. Lee (ed.), Population, food and rural development. Oxford: Clarendon
Press.</mixed-citation>
         </ref>
         <ref id="d1905e274a1310">
            <mixed-citation id="d1905e278" publication-type="other">
Demographic Unit, (1989). National seminar on population and development in Malawi, 5-9
June 1989. University of Malawi, Chancellor College, Zomba.</mixed-citation>
         </ref>
         <ref id="d1905e288a1310">
            <mixed-citation id="d1905e292" publication-type="other">
Downing, L. (1975). The population policy of Kenya. Unpublished.</mixed-citation>
         </ref>
         <ref id="d1905e299a1310">
            <mixed-citation id="d1905e303" publication-type="other">
Edwards, M. (1970). A short-term study of patient persistance and acceptability of family
planning in three rural clinics in Kericho District, 1968-69. Nairobi: IPPF Regional office.</mixed-citation>
         </ref>
         <ref id="d1905e314a1310">
            <mixed-citation id="d1905e318" publication-type="other">
Ejioju, C.N. (1972). The Kenya Programme: Policy and results, pp. 386-392 in S.H. Ominde
and C.N. Ejiogu (eds.), Population growth and economic development in Africa. London,
Nairobi and Ibadan: Heinemann.</mixed-citation>
         </ref>
         <ref id="d1905e331a1310">
            <mixed-citation id="d1905e335" publication-type="other">
Family Planning Association of Kenya (no date). History: Family Planning Association of
Kenya. Discussion Paper. Nairobi, Family Planning Association of Kenya.</mixed-citation>
         </ref>
         <ref id="d1905e345a1310">
            <mixed-citation id="d1905e349" publication-type="other">
Fisher, S. (1974). Briefing for Mr. Oscar Harkavay, May 15-20, 1973. Unpublished.</mixed-citation>
         </ref>
         <ref id="d1905e356a1310">
            <mixed-citation id="d1905e360" publication-type="other">
Fowler, A. (1995). NGOs and the globalization of social welfare, pp. 51-69 in: J.
Semboja &amp; O. Therkildsen (eds.), Service provision under stress in east Africa: The state,
NGOs &amp; people's organizations in Kenya, Tanzania and Uganda. Copenhagen: Centre for
Development Research.</mixed-citation>
         </ref>
         <ref id="d1905e376a1310">
            <mixed-citation id="d1905e380" publication-type="other">
Gachuhi, J.M. &amp; Kiyanjui, P.E. (1973). Utilization of cultural information for popula-
tion planning in east Africa. Nairobi, Kenya: Institute of African Studies and the Ford
Foundation.</mixed-citation>
         </ref>
         <ref id="d1905e393a1310">
            <mixed-citation id="d1905e397" publication-type="other">
Gwatkin, D.R. (1970). Population and the Ford Foundation in Sub-Sahara Africa. Information
Paper. New York: Ford Foundation.</mixed-citation>
         </ref>
         <ref id="d1905e408a1310">
            <mixed-citation id="d1905e412" publication-type="other">
Harkavy, O. (1973). Visits to Kenya, Cameroon, Nigeria, Ghana, May 15-30, 1973. Unpub-
lished.</mixed-citation>
         </ref>
         <ref id="d1905e422a1310">
            <mixed-citation id="d1905e426" publication-type="other">
Harkavy, O. (1995). Curbing population growth: An insider's perspective on the population
movement. New York: Plenum Press.</mixed-citation>
         </ref>
         <ref id="d1905e436a1310">
            <mixed-citation id="d1905e440" publication-type="other">
Heisel, D.F (1968). Attitudes and practice of contraception in Kenya, Demography 5(1): 632-
641.</mixed-citation>
         </ref>
         <ref id="d1905e450a1310">
            <mixed-citation id="d1905e454" publication-type="other">
Hooker, J.R. (1970). Population review 1970: Malawi, American Universities Field Staff.
Central &amp; Southern Africa Series XV(1).</mixed-citation>
         </ref>
         <ref id="d1905e464a1310">
            <mixed-citation id="d1905e468" publication-type="other">
Kalipeni, E. (1996). Demographic response to environmental pressure in Malawi, Population
and Environment: A Journal of Interdisciplinary Studies 17(4): 285-308.</mixed-citation>
         </ref>
         <ref id="d1905e478a1310">
            <mixed-citation id="d1905e482" publication-type="other">
Luke, N. &amp; Watkins, S.C. (2002). Reactions of national elites to international population
policy: Enthusiasm and realism, Population and Development Review 28(4): 707-733.</mixed-citation>
         </ref>
         <ref id="d1905e493a1310">
            <mixed-citation id="d1905e497" publication-type="other">
Malawi Government (1966). Hansard, 4th Session 1966-67. Zomba: Government Printer.</mixed-citation>
         </ref>
         <ref id="d1905e504a1310">
            <mixed-citation id="d1905e508" publication-type="other">
Malawi Government (1986). The National Health Plan of Malawi. Malawi: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="d1905e515a1310">
            <mixed-citation id="d1905e519" publication-type="other">
Malawi Government (1987). Statement of development policies: 1987-1996. Zomba, Malawi:
Government Printer, for the Department of Economic Planning and Development.</mixed-citation>
         </ref>
         <ref id="d1905e529a1310">
            <mixed-citation id="d1905e533" publication-type="other">
Malawi Government (1994a). The Malawi National Family Planning Strategy 1994-1998.
Lilongwe: National Family Welfare Council of Malawi.</mixed-citation>
         </ref>
         <ref id="d1905e543a1310">
            <mixed-citation id="d1905e547" publication-type="other">
Malawi Government (1994b). Population and development in Malawi: Country position
paper for the International Conference on Population and Development (ICPD), 5-13
September, Cairo, Egypt. Lilongwe, Malawi: ICPD National Committee.</mixed-citation>
         </ref>
         <ref id="d1905e560a1310">
            <mixed-citation id="d1905e564" publication-type="other">
Mhone, G. (1992). The political economy of Malawi: An overview, pp. 1-33 in Malawi at the
crossroads: The post-colonial political economy. Harare: Sapes Books.</mixed-citation>
         </ref>
         <ref id="d1905e575a1310">
            <mixed-citation id="d1905e579" publication-type="other">
Ministry of Health of Malawi (1979). Moyo Magazine, January 1979.</mixed-citation>
         </ref>
         <ref id="d1905e586a1310">
            <mixed-citation id="d1905e590" publication-type="other">
Ministry of Health of Malawi (1981). Fertility and family size preferences and child spacing.
Lilongwe: Ministry of Health.</mixed-citation>
         </ref>
         <ref id="d1905e600a1310">
            <mixed-citation id="d1905e604" publication-type="other">
Ministry of Health of Malawi (1984). Report of the inter- ministerial/sectoral orientation
seminar on child spacing, Salima 11-13 December 1984. Unpublished.</mixed-citation>
         </ref>
         <ref id="d1905e614a1310">
            <mixed-citation id="d1905e618" publication-type="other">
Ministry of Health of Malawi (1992). Child spacing programme 1992-1996. Lilongwe:
Ministry of Health.</mixed-citation>
         </ref>
         <ref id="d1905e628a1310">
            <mixed-citation id="d1905e632" publication-type="other">
Mlia, J.R.N. &amp; Kalipeni, E. (1987). Population growth and national development in Malawi,
Malaysian Journal of Tropical Geography 15: 39-48.</mixed-citation>
         </ref>
         <ref id="d1905e642a1310">
            <mixed-citation id="d1905e646" publication-type="other">
Molnos, A. (1968). Attitudes toward family planning in East Africa: An investigation in
schools around Lake Victoria and in Nairobi, with introductory chapters on the position
of women and the population problem in East Africa. Munich: Weltforum Verlag for
Ifo-Institute of Economic Research, African Studies Center.</mixed-citation>
         </ref>
         <ref id="d1905e663a1310">
            <mixed-citation id="d1905e667" publication-type="other">
Morton, K. (1975). Aid and dependency: British aid to Malawi. Discussion Paper. Croom
Helm and Overseas Development Institute.</mixed-citation>
         </ref>
         <ref id="d1905e677a1310">
            <mixed-citation id="d1905e681" publication-type="other">
Mudoga, F.M. (1972). Kenya: Training a family planning communicator. Unpublished.</mixed-citation>
         </ref>
         <ref id="d1905e688a1310">
            <mixed-citation id="d1905e692" publication-type="other">
Miilwafu, W. &amp; Muula, A. (2001). The first medical school in Malawi. Lilongwe: Sunrise
Publications.</mixed-citation>
         </ref>
         <ref id="d1905e702a1310">
            <mixed-citation id="d1905e706" publication-type="other">
National Family Welfare Council of Malawi (1994). The Malawi national family planning
strategy 1994-1998. Lilongwe: Government Printer.</mixed-citation>
         </ref>
         <ref id="d1905e716a1310">
            <mixed-citation id="d1905e720" publication-type="other">
Ndeti, K., Ndeti, C. &amp; Horsely, K. (1980). Cultural values and population policy in Kenya.
Nairobi: Kenya Literature Bureau.</mixed-citation>
         </ref>
         <ref id="d1905e730a1310">
            <mixed-citation id="d1905e734" publication-type="other">
Krystall, A., Berger, J. &amp; Maleche, A. (1973). Report on evaluation of family planning training
of community development, social workers and others working with families. Nairobi:
FAO, Programmes for Better Family Living.</mixed-citation>
         </ref>
         <ref id="d1905e748a1310">
            <mixed-citation id="d1905e752" publication-type="other">
Radel, D.J. (1973). Elite perceptions of population problems and potential solutions: Research
to guide an elite oriented population education program in Kenya. Discussion Paper.
University of Minnesota.</mixed-citation>
         </ref>
         <ref id="d1905e765a1310">
            <mixed-citation id="d1905e769" publication-type="other">
Republic of Kenya (1984). Population policy guidelines. Sessional Paper No. 4 of 1984.
Nairobi: National Committee on Family Planning and Development.</mixed-citation>
         </ref>
         <ref id="d1905e779a1310">
            <mixed-citation id="d1905e783" publication-type="other">
Rockefeller Family Archives (1965). RG5 John D. Rockefeller 3rd Papers, Series 3 (Office
and Home Files), Box 68, Folder 455, "Population Interests - Kenya".</mixed-citation>
         </ref>
         <ref id="d1905e793a1310">
            <mixed-citation id="d1905e797" publication-type="other">
Rothschild, D. (1969). Ethnic inequalities in Kenya, Journal of Modern African Studies 7(4):
689-711.</mixed-citation>
         </ref>
         <ref id="d1905e807a1310">
            <mixed-citation id="d1905e811" publication-type="other">
Saunders, L. (1965). Kenya: Log notes and reports of Population Council Mission to Kenya.
Unpublished.</mixed-citation>
         </ref>
         <ref id="d1905e821a1310">
            <mixed-citation id="d1905e825" publication-type="other">
Sutton, F.X. (1996). A history of the Ford Foundation's international activities. Unpublished.</mixed-citation>
         </ref>
         <ref id="d1905e833a1310">
            <mixed-citation id="d1905e837" publication-type="other">
United Nations (1989). World population policies. Population Studies No. 11. New York:
Department of International Economic and Social Affairs.</mixed-citation>
         </ref>
         <ref id="d1905e847a1310">
            <mixed-citation id="d1905e851" publication-type="other">
Watkins, S.C. (2000). Local and foreign models of reproduction in Nyanza Province, Kenya,
1930-1998, Population and Development Review 26(4): 725-759.</mixed-citation>
         </ref>
         <ref id="d1905e861a1310">
            <mixed-citation id="d1905e865" publication-type="other">
Watkins, S.C. &amp; Hodgson, D. (1998). From mercantilists to Neo-Malthusians: The interna-
tional population movement and the transformation of population ideology in Kenya.
Paper presented at the Workshop on Social Processes Underlying Fertility Change in
Developing Countries, 29-30 January 1998, National Academy of Sciences.</mixed-citation>
         </ref>
         <ref id="d1905e881a1310">
            <mixed-citation id="d1905e885" publication-type="other">
Weinreb, A. (2001). First politics, then culture: Accounting for ethnic differences in demo-
graphic behavior, Population and Development Review 27(3): 437-467.</mixed-citation>
         </ref>
         <ref id="d1905e895a1310">
            <mixed-citation id="d1905e899" publication-type="other">
Widner, J.A. (1992). The rise of a party-state in Kenya: From "Harambee!" to "Nyayo!"
Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d1905e909a1310">
            <mixed-citation id="d1905e913" publication-type="other">
Wilks, Y. (1970). Family planning or tribal planning, Cambridge Review 8: 10.</mixed-citation>
         </ref>
         <ref id="d1905e921a1310">
            <mixed-citation id="d1905e925" publication-type="other">
World Bank (1986). Population sector review. Washington, D.C.: Population Health and
Nutrition Department, The World Bank.</mixed-citation>
         </ref>
         <ref id="d1905e935a1310">
            <mixed-citation id="d1905e939" publication-type="other">
World Bank (1992). Population and the World Bank: Implications from eight case studies.
Washington, D.C.: Operations Evaluation Department, The World Bank.</mixed-citation>
         </ref>
         <ref id="d1905e949a1310">
            <mixed-citation id="d1905e953" publication-type="other">
World Health Organization (1978). Country profile (health): Republic of Malawi. Lilongwe:
World Health Organization.</mixed-citation>
         </ref>
         <ref id="d1905e963a1310">
            <mixed-citation id="d1905e967" publication-type="other">
Zulu, E.M. (1996). Social and cultural factors affecting reproductive behavior in Malawi. Un-
published PhD Dissertation. University of Pennsylvania, Graduate Group in Demography.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
