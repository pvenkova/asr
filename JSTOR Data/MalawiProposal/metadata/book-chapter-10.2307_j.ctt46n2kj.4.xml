<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt45kbkx</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt46n2kj</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Economics</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The New Presence of China in Africa</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>van Dijk</surname>
               <given-names>Meine Pieter</given-names>
            </name>
            <role>(ed.)</role>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>26</day>
         <month>08</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="ppub">9789089641366</isbn>
      <isbn content-type="epub">9789048510641</isbn>
      <publisher>
         <publisher-name>Amsterdam University Press</publisher-name>
         <publisher-loc>Amsterdam</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Meine Pieter van Dijk</copyright-holder>
         <copyright-holder>Amsterdam University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt46n2kj"/>
      <abstract abstract-type="short">
         <p>This book describes China's growing range of activities in Africa, especially in the sub-Saharan region. The three most important instruments China has at its disposal in Africa are development aid, investments and trade policy. The Chinese government, which believes the Western development aid model has failed, is looking for new forms of aid and development in Africa. China's economic success can partly be ascribed to the huge availability of cheap labour, which is primarily employed in export-oriented industries. China is looking for the required raw materials in Africa, and for new marketplaces. Investments are being made on a large scale in Africa by Chinese state-controlled firms and private companies, particularly in the oil-producing countries (Angola, Nigeria and Sudan) and countries rich in minerals (Zambia). Third, the trade policy China is conducting is analysed in China and compared with that of Europe and the United States. In case studies the specific situation in several African countries is examined. In Zambia the mining industry, construction and agriculture are described. One case study of Sudan deals with the political presence of China in Sudan and the extent to which Chinese arms suppliers contributed to the current crisis in Darfur. The possibility of Chinese diplomacy offering a solution in that conflict is discussed. The conclusion considers whether social responsibility can be expected of the Chinese government and companies and if this is desirable, and to what extent the Chinese model in Africa can act as an example - or not - for the West. This title is available in the OAPEN Library - http://www.oapen.org.</p>
      </abstract>
      <counts>
         <page-count count="224"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>1</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>5</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.3</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction:</title>
                     <subtitle>objectives of and instruments for China’s new presence in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>van Dijk</surname>
                           <given-names>Meine Pieter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>China is fast becoming a major player in Sub-Saharan Africa (Jacoby, 2007: 34). In 2008 it replaced the European Union (EU) and the United States (US) as Africa’s major trading partner. However, we are not just talking about trade; China’s Foreign Direct Investment (FDI) and development aid are also increasing rapidly; and aid, investments and trade mutually reinforce each other in the case of China (Asche and Schüller, 2008).¹ This combined use of aid, investments and trade requires political coordination and China has developed a strategy and different policies with respect to Africa, which includes migration to Africa and buying</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.4</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>China’s opening up, from Shenzhen to Sudan</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>de Beule</surname>
                           <given-names>Filip</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Van den Bulcke</surname>
                           <given-names>Daniël</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>At the end of the 1970s, after years of economic and political isolation, China started an enormous systemic transformation to turn around one of the world’s poorest countries. At the beginning of reform, we now know that China was substantially poorer than Sub-Saharan Africa (Dollar, 2008). At that time, China’s real gross domestic product per capita was behind all Sub-Saharan countries, except Ethiopia and Tanzania (CICUP, 2008).</p>
                     <p>Already in the early 1970s Zhou Enlai devised a plan to modernize China’s agriculture, industry, defense, and science and technology; but it was not until Deng Xiaoping’s resurgence –after Mao Zedong’s death– that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.5</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Chinese aid to Africa, origins, forms and issues</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chaponnière</surname>
                           <given-names>Jean-Raphaël</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>China has been one of the engines of the world economy since the beginning of the present decade. Its appetite for raw materials has driven up commodity prices and thus helped to boost growth in Africa (Goldstein et al., 2006; Broadman, 2006, 2008). Trade between China and Africa increased seven-fold between 2000 and 2007, from ten billion US$ to 70 billion US$, making China the leading supplier of the African continent and its second-largest trading partner after the United States. This spectacular growth has halted the marginalization of Africa in world trade that began in 1980. Africa is of course</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.6</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>China’s investments in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kragelund</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>van Dijk</surname>
                           <given-names>Meine Pieter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>83</fpage>
                  <abstract>
                     <p>The Chinese presence on the African continent takes a multiple of (interwoven) forms ranging from migration, development aid (including tariff exemptions and debt relief), trade, and investments (both state and privately driven). Hence, even though this chapter essentially seeks to unpack Chinese Foreign Direct Investments (FDI) in Africa, it will also mention other aspects of Chinese involvement in the continent as the lines between the forms of intervention are blurred (McCormick, 2008).</p>
                     <p>The recent upsurge in Chinese FDI to Africa is important for several reasons. Firstly, Chinese FDI is much needed. According to Asiedu (2004), Africa needs to fill an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.7</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Competing trade policies with respect to Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>van Dijk</surname>
                           <given-names>Meine Pieter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>European countries have been active in Africa for centuries. In the 1950s while preparing the Treaty of Rome, the beginning of the European Union (EU), it was France that insisted on special treatment for its former colonies. This resulted in the subsequent Yaounde, Lomé and Cotonou conventions between Europe and the so-called ACP (African, Caribbean and Pacific) countries. The original group of French and British former colonies has been extended to almost all countries in Sub-Saharan Africa, a number in the Caribbean and in the Pacific Ocean. Through the agreements, the EU gave non-reciprocal trade advantages to all ACP countries.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.8</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>State-driven Chinese investments in Zambia:</title>
                     <subtitle>Combining strategic interests and profits</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bastholm</surname>
                           <given-names>Anders</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kragelund</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>The meeting in November 2006 of 48 African leaders gathered in Beijing, together with their Chinese hosts, President Hu Jintao and Premier Wen Jiabao, to inaugurate the 3rd Forum on China-Africa Cooperation (FOCAC) marked the zenith of Sino-African cooperation. It followed the dramatically intensified political and economic interactions between China and African countries. Figures on trade, aid and investment are increasing rapidly and political ties are growing stronger, as are personal ties between the political elites of China and Africa. This relationship is of immense significance for Africa’s political and economic development; and naturally, both media and scholarly attention on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.9</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The political impact of the Chinese in Sudan</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>van Dijk</surname>
                           <given-names>Meine Pieter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>141</fpage>
                  <abstract>
                     <p>Sudan is often called the most controversial case in the wave of Chinese engagements with Africa (Large, 2007: 57). In particular, the developments in the Southern and in the Darfur region and the pressure on China in 2008 before the Olympics are important factors in understanding China’s role in Africa. Our ambition is not to give a complete overview of the Chinese presence in Sudan, but we would like to understand the reasons for and the impact of the Chinese involvement in this country.</p>
                     <p>First a short review of the history of the cooperation between China and Sudan will be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.10</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The impact of the Chinese in other African countries and sectors</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>van Dijk</surname>
                           <given-names>Meine Pieter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>So far we have discussed three instruments for China’s presence in Africa in detail (aid, investments and trade policies) and dealt with two case studies (Zambia and Sudan). However, most African countries have experienced an increased Chinese presence and China is active in very different sectors. We cannot analyze in this book each and every African country and all the different sectors in which China is active separately. For that reason we will bring together in this chapter the experiences in a few other African countries where China is present and provide some details for typical sectors in which Chinese</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.11</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Responsible production in Africa:</title>
                     <subtitle>The rise of China as a threat or opportunity?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Knorringa</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>177</fpage>
                  <abstract>
                     <p>This chapter explores likely impacts of the rise of China on opportunities to enhance responsible production in Africa. Responsible production refers to those situations where lead actors in value chains make a deliberate effort to include, throughout their supply chain, labor and environmental standards that<italic>go beyond</italic>the existing minimum legal requirements. In this way, I use responsible production as an umbrella term, encompassing both Corporate Social Responsibility (CSR) initiatives and Fair Trade activities. This chapter is part of a broader program of research that raises two basic questions in order to begin assessing the development relevance of responsible production.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.12</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Conclusions from China’s activities in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>van Dijk</surname>
                           <given-names>Meine Pieter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>In this last chapter a number of conclusions concerning China’s presence in Africa will be drawn. Not everybody may agree with them. This has to do with the delicate nature of the subject and the normative framework used. It is also due to the lack of data on a number of the deals between the Chinese government and African countries and with a number of contradictions. Our point of departure is that China’s new presence in Africa has created new opportunities for African countries (if only because of the higher investments, more trade and aid and higher prices for raw</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.13</book-part-id>
                  <title-group>
                     <title>About the authors</title>
                  </title-group>
                  <fpage>221</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt46n2kj.14</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>223</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
