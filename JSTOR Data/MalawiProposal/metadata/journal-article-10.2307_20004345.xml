<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">area</journal-id>
         <journal-id journal-id-type="jstor">j50000002</journal-id>
         <journal-title-group>
            <journal-title>Area</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing Ltd.</publisher-name>
         </publisher>
         <issn pub-type="ppub">00040894</issn>
         <issn pub-type="epub">14754762</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20004345</article-id>
         <article-categories>
            <subj-group>
               <subject>African Environments</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Climate Variability and Rural Livelihoods: Assessing the Impact of Seasonal Climate Forecasts in Lesotho</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Gina</given-names>
                  <surname>Ziervogel</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Rebecca</given-names>
                  <surname>Calder</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i20004337</issue-id>
         <fpage>403</fpage>
         <lpage>417</lpage>
         <permissions>
            <copyright-statement>Copyright 2003 Royal Geographical Society (With the Institute of British Geographers)</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20004345"/>
         <abstract>
            <p>Climate variability acutely affects rural livelihoods and agricultural productivity, yet it is just one of many stresses that vulnerable rural households have to cope with. A livelihood approach is used to assess the potential role that seasonal climate forecasts might play in increasing adaptive capacity in response to climate variability, using Lesotho as a case study. An examination of the assets and strategies that rural households employ enables a holistic assessment of the impact seasonal forecasts could have on rural livelihoods. This research thereby bridges macro-level variability with local-level impacts and adaptation to provide insight into the dynamics of forecast use and impact among vulnerable groups.</p>
         </abstract>
         <kwd-group>
            <kwd>Lesotho</kwd>
            <kwd>adaptive capacity</kwd>
            <kwd>livelihood approaches</kwd>
            <kwd>seasonal climate forecasts</kwd>
            <kwd>climate variability</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d960e162a1310">
            <label>1</label>
            <mixed-citation id="d960e169" publication-type="other">
CARE International is an aid and development organiza-
tion that is composed of a confederation of 11 separate
member organizations that have programmes in over 60
countries (http://www.care-international.org) Accessed
October 2002.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d960e197a1310">
            <mixed-citation id="d960e201" publication-type="other">
Agrawala S, Broad K and Guston D H 2001 Integrating cli-
mate forecasts and societal decision making: challenges to
an emergent boundary organisation Science, Technology
and Human Values 26 454-77</mixed-citation>
         </ref>
         <ref id="d960e217a1310">
            <mixed-citation id="d960e221" publication-type="other">
Archer E R M 2003 Identifying underserved end-user groups
in the provision of climate information Bulletin of the
American Meteorological Society November 2003</mixed-citation>
         </ref>
         <ref id="d960e234a1310">
            <mixed-citation id="d960e238" publication-type="other">
Basher R, Clark C, Dilley M and Harrison M 2001 Coping
with climate: a way forward A multi-stakeholder review of
Regional Climate Outlook Forums concluded at an inter-
national workshop 16-20 October 2000 International
Research Institute for Climate Prediction, Palisades NY</mixed-citation>
         </ref>
         <ref id="d960e257a1310">
            <mixed-citation id="d960e261" publication-type="other">
Bezuidenhout C N and Singels A 2001 The use of simulation
crop modelling to forecast sugarcane yield Paper pre-
sented at the workshop of the South African Sugar Tech-
nology Association on 'Burn/harvest to crush delays &amp; crop
estimating' Mt Edgecombe, South Africa 8 November</mixed-citation>
         </ref>
         <ref id="d960e281a1310">
            <mixed-citation id="d960e285" publication-type="other">
Blaikie P, Cannon T, Davis I and Wisner B 1994 At risk -
natural hazards. People's vulnerability and disasters
Routledge, London</mixed-citation>
         </ref>
         <ref id="d960e298a1310">
            <mixed-citation id="d960e302" publication-type="other">
Blench R 1999 Seasonal climatic forecasting: who can use it
and how should it be disseminated? Natural resource brief-
ing paper 47 Overseas Development Institute, London</mixed-citation>
         </ref>
         <ref id="d960e315a1310">
            <mixed-citation id="d960e319" publication-type="other">
Bohle H G, Downing T and Wafts J 1994 Climate change and
social vulnerability Global Environmental Change 4 37-48</mixed-citation>
         </ref>
         <ref id="d960e329a1310">
            <mixed-citation id="d960e333" publication-type="other">
Bohn L 2003 Climate forecasts in Swaziland: perspectives
from agribusiness in O'Brien K L and Vogel C eds Coping
with climate variability: the use of seasonal climate fore-
casts in southern Africa Ashgate, Burlington 97-109</mixed-citation>
         </ref>
         <ref id="d960e349a1310">
            <mixed-citation id="d960e353" publication-type="other">
Broad K, Pfaff A P and Glantz M H 2002 Effective and equi-
table dissemination of seasonal-to-interannual climate
forecasts: policy implications from the Peruvian fishery
during El Nino 1997-98 Climatic Change 54 415-38</mixed-citation>
         </ref>
         <ref id="d960e369a1310">
            <mixed-citation id="d960e373" publication-type="other">
Calder R, Mokhameleli S, Mahase M, Mohasi M, Lerotholi M
and Khobotle M 2000 TEAM project monitoring and
evaluation guide CARE Lesotho, Maseru</mixed-citation>
         </ref>
         <ref id="d960e387a1310">
            <mixed-citation id="d960e391" publication-type="other">
CARE South Africa 1999 Household livelihood assessments
Household Livelihood Assessment Workshop Port Eliza-
beth 24-26 August 1999</mixed-citation>
         </ref>
         <ref id="d960e404a1310">
            <mixed-citation id="d960e410" publication-type="other">
Carney D 1998 Approaches to sustainable livelihoods for
the rural poor Poverty Briefing Overseas Development
Institute, London</mixed-citation>
         </ref>
         <ref id="d960e423a1310">
            <mixed-citation id="d960e427" publication-type="other">
Chakela Q K ed 1999 State of environment in Lesotho 1997
National Environment Secretariat, Maseru</mixed-citation>
         </ref>
         <ref id="d960e437a1310">
            <mixed-citation id="d960e441" publication-type="other">
Chambers R 1994 Participatory rural appraisal (PRA):
analysis of experience World Development 22 1253-68</mixed-citation>
         </ref>
         <ref id="d960e451a1310">
            <mixed-citation id="d960e455" publication-type="other">
Cleaver F 2001 Institutions, agency and the limitations of parti-
cipatory approaches to development in Cooke B and Kothari
U eds Participation: the new tyranny? Zed, London 36-55</mixed-citation>
         </ref>
         <ref id="d960e468a1310">
            <mixed-citation id="d960e472" publication-type="other">
Crush J, Ulicki T, Tseane T and Jansen van Veuren E 2001
Undermining labour: the rise of sub-contracting in South
African gold mines Journal of Southern African Studies 27
5-31</mixed-citation>
         </ref>
         <ref id="d960e489a1310">
            <mixed-citation id="d960e493" publication-type="other">
CVAP 2000 Masters of the climate competition (http://www.
cvap.gov.au/mastersoftheclimate/) Accessed August 2002</mixed-citation>
         </ref>
         <ref id="d960e503a1310">
            <mixed-citation id="d960e507" publication-type="other">
Ellis F 2000 Rural livelihoods and diversity in developing
countries Oxford University Press, Oxford</mixed-citation>
         </ref>
         <ref id="d960e517a1310">
            <mixed-citation id="d960e521" publication-type="other">
Francis E 2000 Making a living: changing livelihoods in rural
Africa Routledge, London</mixed-citation>
         </ref>
         <ref id="d960e531a1310">
            <mixed-citation id="d960e535" publication-type="other">
Gay J and Hall D 2000 Poverty and livelihoods in Lesotho,
2000. More than a mapping exercise Sechaba Consultants,
Maseru</mixed-citation>
         </ref>
         <ref id="d960e548a1310">
            <mixed-citation id="d960e552" publication-type="other">
Goddard L, Mason S J, Zebiak S E, Ropelewski C F, Basher R
and Cane M A 2001 Current approaches to seasonal to
interannual climate predictions International Journal of
Climatology 21 1111-52</mixed-citation>
         </ref>
         <ref id="d960e568a1310">
            <mixed-citation id="d960e572" publication-type="other">
Hagmann J, Chuma E, Murwira K and Connolly M 1999
Putting process into practice: operationalising participa-
tory extension AGREN network paper no 94 ODI, London</mixed-citation>
         </ref>
         <ref id="d960e586a1310">
            <mixed-citation id="d960e590" publication-type="other">
Hammer G L, Nicholls N and Mitchell C eds 2000 Applica-
tions of seasonal climate forecasting in agricultural and
natural ecosystems - the Australian experience Atmos-
pheric and Oceanic Sciences Library vol 21 Kluwer
Academic, Dordrecht</mixed-citation>
         </ref>
         <ref id="d960e609a1310">
            <mixed-citation id="d960e613" publication-type="other">
Hudson J and Vogel C 2003 The use of seasonal forecasts by
livestock farmers in South Africa in O'Brien K L and Vogel C
eds Coping with climate variability: the use of seasonal
climate forecasts in southern Africa Ashgate, Burlington</mixed-citation>
         </ref>
         <ref id="d960e629a1310">
            <mixed-citation id="d960e633" publication-type="other">
IPCC 2001 Climate change 2001: synthesis report A contri-
bution of Working Groups I, II and III to the Third Assess-
ment Report of the Intergovernmental Panel on Climate
Change Watson R T and the Core Writing Team eds
Cambridge University Press, Cambridge</mixed-citation>
         </ref>
         <ref id="d960e652a1310">
            <mixed-citation id="d960e656" publication-type="other">
Kelly P M and Adger W N 2000 Theory and practice in
assessing vulnerability to climate change and facilitating
adaptation Climatic Change 47 325-52</mixed-citation>
         </ref>
         <ref id="d960e669a1310">
            <mixed-citation id="d960e673" publication-type="other">
Klopper E 1999 The use of seasonal forecasts in South Africa
during the 1997/98 rainfall season Water SA 25 311-17</mixed-citation>
         </ref>
         <ref id="d960e683a1310">
            <mixed-citation id="d960e687" publication-type="other">
LNVAC (Lesotho National Vulnerability Assessment Committee)
and SADC FANR Vulnerability Assessment Committee 2002
Lesotho emergency food security assessment reportMaseru</mixed-citation>
         </ref>
         <ref id="d960e701a1310">
            <mixed-citation id="d960e705" publication-type="other">
Marshall G R, Parton K A and Hammer G L 1996 Risk atti-
tude, planting conditions and the value of seasonal fore-
casts to a dryland wheat grower Australian Journal of
Agricultural Economics 40 211-33</mixed-citation>
         </ref>
         <ref id="d960e721a1310">
            <mixed-citation id="d960e725" publication-type="other">
Mason S J, Joubert A M, Cosijn C and Crimp S J 1996 Review
of seasonal forecasting techniques and their applicability
to southern Africa Water SA 22 203-9</mixed-citation>
         </ref>
         <ref id="d960e738a1310">
            <mixed-citation id="d960e742" publication-type="other">
Mochebelele M and Winter-Nelson A 2000 Migrant labour
and farm technical efficiency in Lesotho World Develop-
ment 28 143-53</mixed-citation>
         </ref>
         <ref id="d960e755a1310">
            <mixed-citation id="d960e759" publication-type="other">
Mohasi M and Turner S 1999 Land and livelihoods in south-
ern Lesotho Research report no 4 Programme for land and
agrarian studies (PLAAS) and CARE Lesotho, Belville</mixed-citation>
         </ref>
         <ref id="d960e772a1310">
            <mixed-citation id="d960e776" publication-type="other">
Murphy S, Washington R, Downing T E, Martin R V,
Ziervogel G, Preston P, Todd M, Butterfield R and Briden J
2001 Seasonal forecasting for climate hazards: prospects
and responses Natural Hazards 23 171-96</mixed-citation>
         </ref>
         <ref id="d960e792a1310">
            <mixed-citation id="d960e796" publication-type="other">
Nelson N and Wright S 1995 Power and participatory devel-
opment Intermediate Technology Publications, London</mixed-citation>
         </ref>
         <ref id="d960e807a1310">
            <mixed-citation id="d960e811" publication-type="other">
O'Brien K L and Vogel C 2003 A future for forecasts? in
O'Brien K L and Vogel C eds Coping with climate variabil-
ity: the use of seasonal climate forecasts in southern Africa
Ashgate, Burlington 197-211</mixed-citation>
         </ref>
         <ref id="d960e827a1310">
            <mixed-citation id="d960e831" publication-type="other">
O'Brien K L, Sygna L, Nxess L O, Kingamkono R and
Hochobeb B 2000 Is information enough? User responses
to seasonal climate forecasts in Southern Africa Report no
2000-03 CICERO University of Oslo, Norway</mixed-citation>
         </ref>
         <ref id="d960e847a1310">
            <mixed-citation id="d960e851" publication-type="other">
Orlove B and Tosteson J 1999 The application of seasonal to
interannual climate forecasts based on ENSO events:
lessons from Australia, Brazil, Ethiopia, Peru, andZimbabwe
Working paper 99-3 Institute of International Studies,
University of California, Berkeley</mixed-citation>
         </ref>
         <ref id="d960e870a1310">
            <mixed-citation id="d960e874" publication-type="other">
Palmer T N and Anderson D 1994 The prospects for seasonal
forecasting - a review paper Quarterly Journal of the
Meteorological Society 120 755-93</mixed-citation>
         </ref>
         <ref id="d960e887a1310">
            <mixed-citation id="d960e891" publication-type="other">
Patt A 2001 Understanding uncertainty: forecasting seasonal
climate for farmers in Zimbabwe Risk, Decision and Policy
6 105-19</mixed-citation>
         </ref>
         <ref id="d960e904a1310">
            <mixed-citation id="d960e908" publication-type="other">
Patt A and Gwata C 2002 Effective seasonal climate forecast
applications: examining constraints for subsistence farmers
in Zimbabwe Global Environmental Change: Human and
Policy Dimensions 12 185-95</mixed-citation>
         </ref>
         <ref id="d960e925a1310">
            <mixed-citation id="d960e929" publication-type="other">
Peshoane L A 2000 Seasonal rainfall forecast for OND 2001/
JFM 2002 Paper presented at the Lesotho Meteorological
Services National Seasonal Forecast Workshop, Maseru</mixed-citation>
         </ref>
         <ref id="d960e942a1310">
            <mixed-citation id="d960e946" publication-type="other">
Pfaff A, Broad K and Glantz M 1999 Who benefits from
climate forecasts? Nature 397 645-6</mixed-citation>
         </ref>
         <ref id="d960e956a1310">
            <mixed-citation id="d960e960" publication-type="other">
Phillips J G, Makaudze E and Unganai L 2001 Current and
potential use of climate forecasts for resource-poor farmers
in Zimbabwe in Impacts of El Nino and climate variability
on agriculture American Society of Agronomy Special
Publication Series no 63 87-100</mixed-citation>
         </ref>
         <ref id="d960e979a1310">
            <mixed-citation id="d960e983" publication-type="other">
Plant S 2000 The relevance of seasonal climate forecasting to
a rural producer in Hammer G, Nicholls N and Mitchell C
eds Applications of seasonal climate forecasting in agricul-
tural and natural ecosystems - the Australian experience
Kluwer Academic, Dordrecht 23-8</mixed-citation>
         </ref>
         <ref id="d960e1002a1310">
            <mixed-citation id="d960e1006" publication-type="other">
Pulwarty R S and Redmond K T 1997 Climate and salmon
restoration in the Columbia river basin: the role and
usability of seasonal forecasts Bulletin of the American
Meteorological Society 78 381-97</mixed-citation>
         </ref>
         <ref id="d960e1022a1310">
            <mixed-citation id="d960e1026" publication-type="other">
Rayner S and Malone E L eds 1998 Human choice and
climate change vol 1 Batelle Press, Columbus</mixed-citation>
         </ref>
         <ref id="d960e1037a1310">
            <mixed-citation id="d960e1043" publication-type="other">
Roncoli C, Kirshen P, Ingram K and Flitcroft I 2000 Oppor-
tunities and constraints to using seasonal precipitation fore-
casting to improve agricultural production systems and liveli-
hood security in the Sahel-Sudan region: a case study of
Burkina Faso, CFAR-phase 1 IRI Forum on Climate Predic-
tions, Agriculture and Development, New York 26-28 April</mixed-citation>
         </ref>
         <ref id="d960e1066a1310">
            <mixed-citation id="d960e1070" publication-type="other">
SADC FANR Vulnerability Assessment Committee 2003
Towards identifying impacts of HIV/AIDS on food insecurity
in Southern Africa and implications for response findings
from Malawi, Zambia and Zimbabwe Zimbabwe, Harare</mixed-citation>
         </ref>
         <ref id="d960e1086a1310">
            <mixed-citation id="d960e1090" publication-type="other">
Scoones I 1996 Hazards and opportunities: farming liveli-
hoods in dryland Africa, lessons from Zimbabwe Zed
Books, London</mixed-citation>
         </ref>
         <ref id="d960e1103a1310">
            <mixed-citation id="d960e1107" publication-type="other">
Sivakumar M V K, Gommes R and Baier W 2000 Agromete-
orology and sustainable agriculture Agricultural and Forest
Meteorology 103 11-26</mixed-citation>
         </ref>
         <ref id="d960e1120a1310">
            <mixed-citation id="d960e1124" publication-type="other">
Smit B, Burton I, Klein R J T and Wandel J 2000 An anatomy
of adaptation to climate change and variability Climatic
Change 45 223-51</mixed-citation>
         </ref>
         <ref id="d960e1137a1310">
            <mixed-citation id="d960e1141" publication-type="other">
Smith J 2001 Living on the edge of rain: rural livelihoods,
vulnerability and resilience on the southern periphery of
the Kalahari PhD dissertation University of Witwatersrand,
Johannesburg</mixed-citation>
         </ref>
         <ref id="d960e1158a1310">
            <mixed-citation id="d960e1162" publication-type="other">
Stern P and Easterling W eds 1999 Making climate forecasts
matter National Academy Press, Washington DC</mixed-citation>
         </ref>
         <ref id="d960e1172a1310">
            <mixed-citation id="d960e1176" publication-type="other">
Turner S, Calder R, Gay J, Hall D, Iredale J, Mbizule C and
Mohatla M 2001 Livelihoods in Lesotho CARE Lesotho, Maseru</mixed-citation>
         </ref>
         <ref id="d960e1186a1310">
            <mixed-citation id="d960e1190" publication-type="other">
Vogel C 2000 Usable science: an assessment of long-term
seasonal forecasts among farmers in rural areas of South
Africa South African Geographical Journal 82 107-16</mixed-citation>
         </ref>
         <ref id="d960e1203a1310">
            <mixed-citation id="d960e1207" publication-type="other">
Walker S, Mukhala E, van den Berg J and Manley C 2001
Assessment of communication and use of climate outlooks
and development of scenarios to promote food security
in the Free State Province of South Africa Department
of Agrometeorology, University of the Free State,
Bloemfontein</mixed-citation>
         </ref>
         <ref id="d960e1230a1310">
            <mixed-citation id="d960e1234" publication-type="other">
Washington Rand Downing T E 1999 Seasonal forecasting of
African rainfall: prediction, responses and household food
security The GeographicalJournal 165 255-74</mixed-citation>
         </ref>
         <ref id="d960e1247a1310">
            <mixed-citation id="d960e1251" publication-type="other">
Whiteside M 1998 Encouraging sustainable smallholder agri-
culture in southern Africa in the context of agricultural
services reform ODI Natural Resource Perspectives no 36
Overseas Development Institute, London</mixed-citation>
         </ref>
         <ref id="d960e1268a1310">
            <mixed-citation id="d960e1272" publication-type="other">
Yohe G and Tol R 2002 Indicators for social and economic
coping capacity - moving toward a working definition of
adaptive capacity Global Environmental Change 12 25-40</mixed-citation>
         </ref>
         <ref id="d960e1285a1310">
            <mixed-citation id="d960e1289" publication-type="other">
Ziervogel G 2001 Global science, local problems: seasonal
forecast use in a Basotho village Open Meeting of the
Global Environmental Change Research Community, Rio
de Janeiro 6-8 October 2001</mixed-citation>
         </ref>
         <ref id="d960e1305a1310">
            <mixed-citation id="d960e1309" publication-type="other">
Ziervogel G in press Targeting seasonal climate forecasts for
integration into household level decisions: the case of
smallholder farmers in Lesotho The Geographical Journal</mixed-citation>
         </ref>
         <ref id="d960e1322a1310">
            <mixed-citation id="d960e1326" publication-type="other">
Ziervogel G and Downing T E in press Stakeholder networks:
improving seasonal forecasts Climatic Change</mixed-citation>
         </ref>
         <ref id="d960e1336a1310">
            <mixed-citation id="d960e1340" publication-type="other">
Zubair L 2001 97-99 ENSO Episode in Sri Lanka: forecasts,
communication responses and impacts in Sri Lanka, an
indebted and peripheral country Open Meeting of the
Global Environmental Change Research Community, Rio
de Janeiro 6-8 October 2001</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
