<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">annabota</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009530</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Annals of Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03057364</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">10958290</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42796074</article-id>
         <title-group>
            <article-title>Prediction of Desiccation Sensitivity in Seeds of Woody Species: A Probabilistic Model Based on Two Seed Traits and 104 Species</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>M. I.</given-names>
                  <surname>DAWS</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>N. C.</given-names>
                  <surname>GARWOOD</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>H. W.</given-names>
                  <surname>PRITCHARD</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">97</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40106316</issue-id>
         <fpage>667</fpage>
         <lpage>674</lpage>
         <permissions>
            <copyright-statement>© Annals of Botany Company 2006</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42796074"/>
         <abstract>
            <p>• Background and Aims Seed desiccation sensitivity limits the ex situ conservation of up to 47 % of plant species, dependent on habitat. Whilst desirable, empirically determining desiccation tolerance levels in seeds of all species is unrealistic. A probabilistic model for the rapid identification of woody species at high risk of displaying seed desiccation sensitivity is presented. • Methods The model was developed using binary logistic regression on seed trait data [seed mass, moisture content, seed coat ratio (SCR) and rainfall in the month of seed dispersal] for 104 species from 37 families from a semideciduous tropical forest in Panamá. • Key Results For the Panamanian species, only seed mass and SCR were significantly related to the response to desiccation, with the desiccation-sensitive seeds being large and having a relatively low SCR (i.e. thin 'seed' coats). Application of this model to a further 38 species, of known seed storage behaviour, from two additional continents and differing vegetation types (dryland Africa and temperate Europe) correctly predicted the response to desiccation in all cases, and resolved conflicting published data for two species (Acer pseudoplatanus and Azadirachta indica). • Conclusions This model may have application as a decision-making tool in the handling of species of unknown seed storage behaviour in species from three disparate habitats.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d1484e193a1310">
            <mixed-citation id="d1484e197" publication-type="other">
Berjak P, Dini M, Pammenter NW. 1984. Possible mechanisms underlying
the different dehydration responses in recalcitrant and orthodox seeds:
desiccation-associated subcellular changes in propagules of Avicennia
marina. Seed Science and Technology 12: 365-384.</mixed-citation>
         </ref>
         <ref id="d1484e213a1310">
            <mixed-citation id="d1484e217" publication-type="other">
Berjak P, Campbell GK, Farrant JM, Omondi Oloo W, Pammenter NW.
1995. Responses of seeds of Azadirachta indica (neem) to short-term
storage under ambient or chilled conditions. Seed Science and Tech-
nology 23: 779-792.</mixed-citation>
         </ref>
         <ref id="d1484e233a1310">
            <mixed-citation id="d1484e237" publication-type="other">
Black M, Pritchard HW. (eds). 2002. Desiccation and survival in plants:
drying without dying. Wallingford, UK: CAB International.</mixed-citation>
         </ref>
         <ref id="d1484e247a1310">
            <mixed-citation id="d1484e251" publication-type="other">
Curran LM, Webb CO. 2000. Experimental tests of the spatio-temporal
scale of seed prédation in mast-fruiting Dipterocarpaceae. Ecological
Monographs 70: 129-148.</mixed-citation>
         </ref>
         <ref id="d1484e265a1310">
            <mixed-citation id="d1484e269" publication-type="other">
D'Arcy WG. 1987. Flora of Panama: checklist and index. Part I: the
introduction and checklist. Monographs in systematic botany,
Vol. 17. Missouri, USA: Missouri Botanical Garden Press.</mixed-citation>
         </ref>
         <ref id="d1484e282a1310">
            <mixed-citation id="d1484e286" publication-type="other">
Daws MI, Mullins CE, Burslem DFRP, Paton SR, Dalling JW. 2002.
Topographic position affects the water regime in a semideciduous
tropical forest in Panama. Plant and Soil 238: 79-90.</mixed-citation>
         </ref>
         <ref id="d1484e299a1310">
            <mixed-citation id="d1484e303" publication-type="other">
Daws MI, Lydall E, Chmielarz P, Leprince O, Matthews S, Thanos CA,
Pritchard HW. 2004. Developmental heat sum influences recalcitrant
seed traits in Aesculus hippocastanum across Europe. New Phytologist
162: 157-166.</mixed-citation>
         </ref>
         <ref id="d1484e319a1310">
            <mixed-citation id="d1484e323" publication-type="other">
Daws MI, Garwood NC, Pritchard HW. 2005. Traits of recalcitrant seeds
in a semi-deciduous forest in Panamá: some ecological implications.
Functional Ecology 19: 874-885.</mixed-citation>
         </ref>
         <ref id="d1484e336a1310">
            <mixed-citation id="d1484e340" publication-type="other">
Daws MI, Cleland H, Chmielarz P, Gorian F, Leprince O, Mullins CE,
Thanos CA, Vandvik V, Pritchard HW. 2006. Variable desiccation
tolerance in Acer pseudoplatanus seeds in relation to developmental
conditions: a case of phenotypic recalcitrance? Functional Plant
Biology 33: 59-66.</mixed-citation>
         </ref>
         <ref id="d1484e359a1310">
            <mixed-citation id="d1484e363" publication-type="other">
Den Ouden J, Jansen PA, Smit R. 2005. Jays, mice and oaks: prédation and
dispersal of Quercus robur and Q. petraea in North Western Europe. In:
Forget P-M, Lambert JE, Hulme PE, Vander Wall SB, eds. Seed fate:
prédation, dispersal and seedling establishment. Wallingford, UK:
CABI Publishing, 223-240.</mixed-citation>
         </ref>
         <ref id="d1484e383a1310">
            <mixed-citation id="d1484e387" publication-type="other">
Dickie JB, Bowyer JT. 1985. Estimation of provisional seed viability con-
stants for apple (Malus domestica Borkh cv Greensleeves). Annals of
Botany 56: 271-275.</mixed-citation>
         </ref>
         <ref id="d1484e400a1310">
            <mixed-citation id="d1484e404" publication-type="other">
Dickie JB, Pritchard HW. 2002. Systematic and evolutionary aspects of
desiccation tolerance in seeds. In: Black M, Pritchard HW, eds. Desic-
cation and survival in plants: drying without dying. Wallingford, UK:
CAB International, 239-259.</mixed-citation>
         </ref>
         <ref id="d1484e420a1310">
            <mixed-citation id="d1484e424" publication-type="other">
Dickie JB, May K, Morris SVA, Titley SE. 1991. The effects of desiccation
on seed survival in Acer platanoides L. and Acer pseudoplatanus L.
Seed Science Research 1: 149-162.</mixed-citation>
         </ref>
         <ref id="d1484e437a1310">
            <mixed-citation id="d1484e441" publication-type="other">
Dietrich WE, Windsor DM, Dunne T. 1982. Geology, climate and
hydrology of Barro Colorado Island. In: Leigh EG Jr, Rand AS, Wind-
sor DM, eds. The ecology of a tropical forest: seasonal rhythms and
long-term changes. Washington, DC: Smithsonian Institution Press,
21-46.</mixed-citation>
         </ref>
         <ref id="d1484e460a1310">
            <mixed-citation id="d1484e464" publication-type="other">
Dussert S, Chabrillange N, Engelmann F, Anthony F, Louarn J,
Hamon S. 2000. Relationship between seed desiccation sensitivity,
seed water content at maturity and climatic characteristics of native
environments of nine Cojfea L. species. Seed Science Research 10:
293-300.</mixed-citation>
         </ref>
         <ref id="d1484e483a1310">
            <mixed-citation id="d1484e487" publication-type="other">
Ellis RH, Hong TD, Roberts EH. 1990. An intermediate category of
seed storage behaviour. 1. Coffee. Journal of Experimental Botany
4: 1167-1174.</mixed-citation>
         </ref>
         <ref id="d1484e501a1310">
            <mixed-citation id="d1484e505" publication-type="other">
Flynn S, Turner RM, Dickie JB. 2004. Seed Information Database (release
6.0, October 2004) http://www.rbgkew.org.uk/data/sid</mixed-citation>
         </ref>
         <ref id="d1484e515a1310">
            <mixed-citation id="d1484e519" publication-type="other">
Garcia D, Banuelos M J, Houle G. 2002. Differential effects of acorn burial
and litter cover on Quercus rubra recruitment at the limit of its range in
eastern North America. Canadian Journal of Botany-Revue
Canadienne de Botanique 80: 1115-1 120.</mixed-citation>
         </ref>
         <ref id="d1484e535a1310">
            <mixed-citation id="d1484e539" publication-type="other">
Greggains V, Finch-Savage WE, Quick WP, Atherton NM. 2000.
Putative desiccation tolerance mechanisms in orthodox and
recalcitrant seeds of the genus Acer. Seed Science Research 10:
317-327.</mixed-citation>
         </ref>
         <ref id="d1484e555a1310">
            <mixed-citation id="d1484e559" publication-type="other">
Grubb PJ, Burslem DFRP. 1998. Mineral nutrient concentrations as a
function of seed size within seed crops: implications for competition
among seedlings and defence against herbivory. Journal of Tropical
Ecology 14: 177-185.</mixed-citation>
         </ref>
         <ref id="d1484e575a1310">
            <mixed-citation id="d1484e579" publication-type="other">
Hong TD, Ellis RH. 1990. A comparison of maturation drying, germination,
and desiccation tolerance between developing seeds of Acer pseudo-
platanus L. and Acer platanoides L. New Phytologist 116: 589-596.</mixed-citation>
         </ref>
         <ref id="d1484e592a1310">
            <mixed-citation id="d1484e596" publication-type="other">
Hong TD, Ellis RH. 1996. A protocol to determine seed storage behaviour.
IPGRI, Technical Bulletin No. 1. Rome, Italy: International Plant
Genetic Resources Institute.</mixed-citation>
         </ref>
         <ref id="d1484e610a1310">
            <mixed-citation id="d1484e614" publication-type="other">
Hong TD, Ellis RH. 1997. Ex situ biodiversity conservation by seed storage:
multiple-criteria keys to estimate seed storage behaviour. Seed Science
and Technology 25: 157-161.</mixed-citation>
         </ref>
         <ref id="d1484e627a1310">
            <mixed-citation id="d1484e631" publication-type="other">
Hong TD, Ellis RH. 1998. Contrasting seed storage behaviour among dif-
ferent species of Meliaceae. Seed Science and Technology 26: 77-95.</mixed-citation>
         </ref>
         <ref id="d1484e641a1310">
            <mixed-citation id="d1484e645" publication-type="other">
ISTA 1999. International rules for seed testing. Seed Science and
Technology 27 (Supplement).</mixed-citation>
         </ref>
         <ref id="d1484e655a1310">
            <mixed-citation id="d1484e659" publication-type="other">
Leigh EG Jr, Rand AS, Windsor DM. (eds). 1982. The ecology of a
neotropical forest : seasonal rhythms and longer-term changes.
Washington, DC: Smithsonian Institution Press.</mixed-citation>
         </ref>
         <ref id="d1484e672a1310">
            <mixed-citation id="d1484e676" publication-type="other">
Pammenter NW, Berjak P. 1999. A review of recalcitrant seed physiology
in relation to desiccation tolerance mechanisms. Seed Science
Research 9: 13-37.</mixed-citation>
         </ref>
         <ref id="d1484e689a1310">
            <mixed-citation id="d1484e693" publication-type="other">
Poulsen K. 1996. Case study: neem (Azadiracta indica A. Juss.)
seed research. In: Ouedraogo AS, Poulsen K, Stubsgaard F, eds.
Intermediate/recalcitrant tropical forest tree seeds. Rome, Italy:
International Plant Genetic Resources Institute, 14-26.</mixed-citation>
         </ref>
         <ref id="d1484e710a1310">
            <mixed-citation id="d1484e714" publication-type="other">
Pritchard HW. 2004. Classification of seed storage 'types' for ex situ
conservation in relation to temperature and moisture. In: Guerrant
EO, Havens K, Maunder M, eds. Ex situ plant conservation:
supporting species survival in the wild. Washington, DC: Island Press,
139-161.</mixed-citation>
         </ref>
         <ref id="d1484e733a1310">
            <mixed-citation id="d1484e737" publication-type="other">
Pritchard HW, Manger KR. 1998. A calorimetrie perspective on
desiccation stress during preservation procedures with recalcitrant
seeds of Quercus robur L. Cryo-Letters 19 (Supplement 1),
23-30.</mixed-citation>
         </ref>
         <ref id="d1484e753a1310">
            <mixed-citation id="d1484e757" publication-type="other">
Pritchard HW, Wood CB, Hodges S, Vautier HJ. 2004a. 100-seed test for
desiccation tolerance and germination: a case study on 8 tropical palm
species. Seed Science and Technology 32: 393-403.</mixed-citation>
         </ref>
         <ref id="d1484e770a1310">
            <mixed-citation id="d1484e774" publication-type="other">
Pritchard HW, Daws MI, Fletcher B J, Gaméné CS, Msanga HP, Omondi W.
2004¿. Ecological correlates of seed desiccation tolerance in tropical
African dryland trees. American Journal of Botany 91: 863-870.</mixed-citation>
         </ref>
         <ref id="d1484e787a1310">
            <mixed-citation id="d1484e791" publication-type="other">
Sacandé M. 2000. Stress, storage and survival of neem seed. PhD thesis.
The Netherlands: Wageningen University.</mixed-citation>
         </ref>
         <ref id="d1484e801a1310">
            <mixed-citation id="d1484e805" publication-type="other">
Tabachnick BG, Fidell LS. 2001. Using multivariate statistics , 4th edn.
Boston, MD: Allyn and Bacon.</mixed-citation>
         </ref>
         <ref id="d1484e816a1310">
            <mixed-citation id="d1484e820" publication-type="other">
Tompsett PB. 1984. Desiccation studies in relation to the storage of
Araucaria seed. Annals of Applied Biology 105: 581-586.</mixed-citation>
         </ref>
         <ref id="d1484e830a1310">
            <mixed-citation id="d1484e834" publication-type="other">
Tompsett PB. 1987. Desiccation and storage studies on Dipterocarpus
seeds. Annals of Applied Biology 110: 371-379.</mixed-citation>
         </ref>
         <ref id="d1484e844a1310">
            <mixed-citation id="d1484e848" publication-type="other">
Tweddle JC, Dickie JB, Baskin CC, Baskin JM. 2003. Ecological aspects
of seed desiccation sensitivity. Journal of Ecology 91: 294-304.</mixed-citation>
         </ref>
         <ref id="d1484e858a1310">
            <mixed-citation id="d1484e862" publication-type="other">
Wood CB, Pritchard HW, Amritphale D. 2000. Desiccation induced
dormancy in papaya (Carica papaya L.) seeds is alleviated by heat
shock. Seed Science Research 10: 135-145.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
