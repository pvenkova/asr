<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1vwmhb0</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Ecology &amp; Evolutionary Biology</subject>
         <subject>Psychology</subject>
         <subject>Biological Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Mate Choice</book-title>
         <subtitle>The Evolution of Sexual Decision Making from Microbes to Humans</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Rosenthal</surname>
               <given-names>Gil G.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>07</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="epub">9781400885466</isbn>
      <isbn content-type="epub">1400885469</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1vwmhb0"/>
      <abstract abstract-type="short">
         <p>A major new look at the evolution of mating decisions in organisms from protozoans to humans</p>
         <p>The popular consensus on mate choice has long been that females select mates likely to pass good genes to offspring. In<italic>Mate Choice</italic>, Gil Rosenthal overturns much of this conventional wisdom. Providing the first synthesis of the topic in more than three decades, and drawing from a wide range of fields, including animal behavior, evolutionary biology, social psychology, neuroscience, and economics, Rosenthal argues that "good genes" play a relatively minor role in shaping mate choice decisions and demonstrates how mate choice is influenced by genetic factors, environmental effects, and social interactions.</p>
         <p>Looking at diverse organisms, from protozoans to humans, Rosenthal explores how factors beyond the hunt for good genes combine to produce an endless array of preferences among species and individuals. He explains how mating decisions originate from structural constraints on perception and from nonsexual functions, and how single organisms benefit or lose from their choices. Both the origin of species and their fusion through hybridization are strongly influenced by direct selection on preferences in sexual and nonsexual contexts. Rosenthal broadens the traditional scope of mate choice research to encompass not just animal behavior and behavioral ecology but also neurobiology, the social sciences, and other areas.</p>
         <p>Focusing on mate choice mechanisms, rather than the traits they target,<italic>Mate Choice</italic>offers a groundbreaking perspective on the proximate and ultimate forces determining the evolutionary fate of species and populations.</p>
      </abstract>
      <counts>
         <page-count count="648"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.4</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Mate Choice and Mating Preferences:</title>
                     <subtitle>AN OVERVIEW</subtitle>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>Hiking in the eucalyptus woods of northern Australia, we might come upon an odd structure with a promenade of shells and bones leading up to a curving, symmetric arch. We might reasonably speculate that we have stumbled upon an indigenous ceremonial site, or perhaps a contemporary art installation (fig. 1.1a). Diving off Japan’s Okinawa Prefecture, we come upon a similar structure—an “alien crop circle” in the popular media (fig. 1.1b). We are astonished when we discover that the architects were a male great bower bird (<italic>Ptilonorhynchus nuchalis</italic>) and a male pufferfish (<italic>Torquigener</italic>sp.), and that these structures only function</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Measuring Preferences and Choices</title>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>How do we actually study mate choice and mating preferences? If all we are interested in is contemporary sexual selection—how courter traits influence fitness—the answer is relatively straightforward. As Arnold (1983, p. 71) pointed out in an influential early review on sexual selection, “one need not identify the agent of sexual selection (e.g., rival males versus discriminating females) in order to measure its impact.” One need only measure realized mating outcomes (section 2.2). We can simply observe a set of focal choosers in a natural or experimental population, and either determine how they behave differently toward individual courters,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>The First Steps in Mate Choice:</title>
                     <subtitle>PREFERENCE FUNCTIONS AND SENSORY TRANSDUCTION</subtitle>
                  </title-group>
                  <fpage>57</fpage>
                  <abstract>
                     <p>Before a chooser can express a preference for any aspect of a courter’s phenotype, he or she has to be able to detect it, a process called sensation (Levine 2000; fig. 3.1). Sensory biology places an upper bound on the breadth and complexity of preference space—the preference envelope—since anything outside a chooser’s range of sensitivity is invisible, inaudible, and odorless: undetectable.</p>
                     <p>Detection involves a response by the sensory periphery to a stimulus. Receptor cells in the periphery transduce a physical property of the environment (photic energy, molecular displacement, molecular structure) into the neurochemical information that choosers see, hear,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Beyond the Periphery:</title>
                     <subtitle>PERCEPTION, COGNITION, AND MULTIVARIATE PREFERENCES</subtitle>
                  </title-group>
                  <fpage>91</fpage>
                  <abstract>
                     <p>The preceding chapter focused on the very first—and most thoroughly studied—step in a chooser’s interaction with a courter: transducing energy in the environment into electrical and chemical signals within an organism. The way scientists think about mate choice has focused on this kind of univariate process, in which a single property of a courter maps neatly onto a single preference function in a chooser.</p>
                     <p>Yet universally, choosers attend to multiple features of courters when making mating decisions. Even the simplest chemosensory response involves attending to two aspects of the incoming chemosignal—specificity and concentration. Mate-choice interactions are often</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Aesthetics and Evaluation in Mate Choice</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>The previous two chapters focused on how choosers integrate input from multiple courter stimuli. This chapter deals with evaluation, whereby different stimulus combinations elicit different hedonic values: in the context of mate choice, they increase or decrease the probability of a mating decision by a chooser. Recognition, usually invoked in the context of “conspecific mate recognition” or “partner recognition,” is a special case of evaluation in which positive hedonic value is assigned to certain classes of individuals or certain stimulus envelopes (chapter 2). Evaluation means that a given courter will respond positively to some stimuli and negatively to others.</p>
                     <p>Darwin</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>From Preferences to Choices:</title>
                     <subtitle>MATE SAMPLING AND MATING DECISIONS</subtitle>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract>
                     <p>The previous chapters detailed the internal processes that choosers use to evaluate potential mates. These processes can be represented as multivariate preference functions, whereby a chooser’s response is a function of courter trait values. But how do preferences translate into actual mating decisions? Mate choice is by definition a comparative process, in the sense that for mate choice to exist, there have to be differences between courters such that a chooser has to be more likely to mate with some courters relative to others, depending on some difference between them. One can imagine an extreme version of Parker’s (1983) “passive”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Mate Choice During and After Mating</title>
                  </title-group>
                  <fpage>174</fpage>
                  <abstract>
                     <p>In the last chapter, I described the decision rules that choosers use to translate preferences into realized matings. An almost definitional assumption when we think about mate choice is that it is these matings that are the endpoint of chooser decisions and of mating success. As described by Eberhard (2009), this point of view characterized Darwin’s (1871) approach to sexual selection and predominated for the next century through the genesis of sexual selection theory. An influential paper by Parker (1970) pointed out that courters continue to compete after mating has occurred. Parker focused on sperm competition. Just as males compete</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Mutual Mate Choice</title>
                  </title-group>
                  <fpage>201</fpage>
                  <abstract>
                     <p>So far, I have treated mate choice as a one-way process, where choosers unilaterally make decisions about which courters to mate with. I have made the implicit assumption that courters will automatically mate if chosen, and that upon mating they will supply choosers with an excess of gametes upon which to exercise cryptic choice. This construct approximates the situation in highly polygynous systems where males contribute few if any resources to the female besides sperm, and in which there is strong competition for mates among males but not females. Indeed, males in some such systems exert no meaningful mate choice</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Variation in Preferences and Choices:</title>
                     <subtitle>General Considerations</subtitle>
                  </title-group>
                  <fpage>228</fpage>
                  <abstract>
                     <p>The preceding chapters described the mechanisms underlying mating preferences and how they are realized into pre- and postmating choices. From sensory transduction through mate-sampling and evaluation,<italic>any change to any one</italic>of these mechanisms has the potential to change how a given courter stimulus is assessed among species, among individual choosers, and even within the same chooser. It is ultimately this variation—at all of these scales—that makes mate choice interesting. Variation in mating preferences is what makes them important to speciation and diversification, what gives mate choices the power to adapt to changing circumstances, and what drives the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>Variation I:</title>
                     <subtitle>Genetics</subtitle>
                  </title-group>
                  <fpage>263</fpage>
                  <abstract>
                     <p>Mating decisions, and their dependence on external influences, are the product of networks of genes acting to shape phenotypes. Structural and regulatory differences in these genes are the basis for preference evolution, and genetic variation is therefore a central concern of mate-choice research.</p>
                     <p>Despite a rapidly growing number of studies on preference genetics, we are just beginning to gain the understanding necessary to test the assumptions and predictions of theory. We begin by asking what we need to know about the genetics of a given aspect of mate choice—say, peak preference for an acoustic signal. The most basic assumption</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>Variation II:</title>
                     <subtitle>Biotic and Abiotic Environment</subtitle>
                  </title-group>
                  <fpage>288</fpage>
                  <abstract>
                     <p>In this chapter and the next, I focus on how preferences vary within and among choosers as a function of the environment. This chapter focuses on the physical environment and on the biotic community in which choosers live: the resources choosers acquire and the challenges toward which they must allocate those resources. This chapter focuses on how the environment shapes preferences; environmental effects on transmission and perception of courter signals are amply covered elsewhere (Bradbury &amp; Vehrencamp 2011; chapter 3). The effect of the social environment, construed to include closely related species with which mating is possible, is covered in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 12</label>
                     <title>Variation III:</title>
                     <subtitle>Social Environment and Epigenetics</subtitle>
                  </title-group>
                  <fpage>306</fpage>
                  <abstract>
                     <p>Mate choice is by definition a social interaction. In the immediate context of a mating decision, a chooser’s behavior is influenced by the phenotype of the courters she experiences (chapter 6). But preferences are shaped by other individuals in a chooser’s life, as well. The social processes that influence mating preferences are outlined in fig. 12.1. Even before birth, parents impart epigenetic markings, hormones, and resources that can affect preferences. Prior to sexual maturity, choosers interact with parents and other older individuals in ways that can have dispositive effects on preferences. Throughout adult life, experiences with courters and other choosers</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.16</book-part-id>
                  <title-group>
                     <label>CHAPTER 13</label>
                     <title>Origins and Histories of Mating Preferences:</title>
                     <subtitle>CHOOSER BIASES</subtitle>
                  </title-group>
                  <fpage>341</fpage>
                  <abstract>
                     <p>Where do mating preferences come from? In this chapter, I consider how an organism’s biology is recruited to mate choice. The evolution of mate choice has been considered primarily through the lens of how preferences coevolve with the traits they target, which I revisit in chapter 15. But how are preferences expressed in the first place?</p>
                     <p>Preferences can only exist if there is variation in courter traits; otherwise there is no way a preference can be expressed. But a host of chooser characteristics can potentially function to discriminate among courter phenotypes and therefore bias mate-choice outcomes. When there is no</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.17</book-part-id>
                  <title-group>
                     <label>CHAPTER 14</label>
                     <title>Selection on Mate Choice and Mating Preferences</title>
                  </title-group>
                  <fpage>358</fpage>
                  <abstract>
                     <p>The last chapter surveyed the sweep of chooser mechanisms that can serve to discriminate among mates, from the molecular underpinnings of fertilization to feeding behavior. Each of these has the potential to act as a matechoice mechanism, each has the potential to vary among and within choosers, and each has the potential to respond to selection on mating outcomes. What happens, then, when biases start to have consequences in the context of mating? Once a mechanism operates in the context of mate choice, it becomes subject to selection in that context, as long as mating decisions covary with fitness outcomes</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.18</book-part-id>
                  <title-group>
                     <label>CHAPTER 15</label>
                     <title>Dynamic Evolution of Preferences, Strategies, and Traits</title>
                  </title-group>
                  <fpage>388</fpage>
                  <abstract>
                     <p>The last two chapters have shown that preferences and traits don’t have to evolve together. Choosers frequently have mechanisms for trait discrimination that can subsequently be recruited for choice; conversely, courter traits evolve in response to latent chooser biases even when these biases are invariant (chapter 13). And the mechanisms underlying preferences are shaped by myriad forces other than heritable properties of courters (chapter 14).</p>
                     <p>Yet we think about preferences mostly in terms of their interplay with courter traits: that is, how they exert sexual selection on courter traits, and how courter traits exert selection on preferences. Some of this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.19</book-part-id>
                  <title-group>
                     <label>CHAPTER 16</label>
                     <title>Mate Choice, Speciation, and Hybridization</title>
                  </title-group>
                  <fpage>439</fpage>
                  <abstract>
                     <p>For the mid-twentieth-century Modern Evolutionary Synthesis, almost the only use for mate choice—and for that matter, for animal behavior—was as a mechanism to restrict gene flow between species. Dobzhansky (1970), following Fisher (1930), believed that selection against hybridization is the primary force acting on preferences; in fact, his latter-day tome on evolution only mentions mate choice in the context of reproductive isolation. Dobzhansky advocated mate choice as an agent of reinforcement (in a very different sense from reinforcement in the context of learning, chapter 6) whereby species could diverge as a result of selection against hybrids.</p>
                     <p>His contemporary,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.20</book-part-id>
                  <title-group>
                     <label>CHAPTER 17</label>
                     <title>Mate Choice and Human Exceptionalism</title>
                  </title-group>
                  <fpage>473</fpage>
                  <abstract>
                     <p>Mate choice emerges from an intricate network of complex mechanisms that are exquisitely sensitive to environmental and social context. Like other animals, humans attend to a suite of courter traits, from odorant molecules (chapter 3) to higher-order properties like social status (chapter 4). We assign fluctuating valences to the same stimuli (chapter 5), we evaluate mates sequentially and in comparison, and our choices are all too often circumvented or subverted (chapter 6). Both males and females are somewhat promiscuous, and there is the opportunity for mating biases to be expressed both within the reproductive tract and long after mating (chapter</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.21</book-part-id>
                  <title-group>
                     <label>CHAPTER 18</label>
                     <title>Conclusions:</title>
                     <subtitle>A Mate-Choice View of the World</subtitle>
                  </title-group>
                  <fpage>482</fpage>
                  <abstract>
                     <p>Since the dawn of sexual reproduction, since organisms first started swapping genetic material and creating offspring, mate choice has played a critical role in determining what subsequent generations look like and how they behave. There are basic rules described by population- and quantitativegenetic theory that apply to every living thing that has sex. The rules mean that when sex is about producing zygotes, there will be coevolution between the sexes; part of this coevolutionary dynamic will involve mate-choice mechanisms. Despite the ubiquity of coevolution, the contribution of indirect viability benefits to coevolution is much smaller than we commonly think. More</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.22</book-part-id>
                  <title-group>
                     <title>GLOSSARY</title>
                  </title-group>
                  <fpage>493</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.23</book-part-id>
                  <title-group>
                     <title>LITERATURE CITED</title>
                  </title-group>
                  <fpage>505</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.24</book-part-id>
                  <title-group>
                     <title>SUBJECT INDEX</title>
                  </title-group>
                  <fpage>617</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1vwmhb0.25</book-part-id>
                  <title-group>
                     <title>TAXONOMIC INDEX</title>
                  </title-group>
                  <fpage>629</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
