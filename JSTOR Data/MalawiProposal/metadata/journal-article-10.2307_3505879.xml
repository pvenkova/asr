<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jappliedecology</journal-id>
         <journal-id journal-id-type="jstor">j100205</journal-id>
         <journal-title-group>
            <journal-title>Journal of Applied Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Science Ltd.</publisher-name>
         </publisher>
         <issn pub-type="ppub">00218901</issn>
         <issn pub-type="epub">13652664</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3505879</article-id>
         <title-group>
            <article-title>Regulation of Shrub Dynamics by Native Browsing Ungulates on East African Rangeland</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>David J.</given-names>
                  <surname>Augustine</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Samuel J.</given-names>
                  <surname>McNaughton</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">41</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i282622</issue-id>
         <fpage>45</fpage>
         <lpage>58</lpage>
         <page-range>45-58</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 British Ecological Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3505879"/>
         <abstract>
            <p> 1. Herbivores, edaphic features and fire are primary factors regulating the balance between woody and herbaceous vegetation in savannas. Many observational studies have evaluated the potential effects of browsing herbivores on woody plant dynamics in African savannas, but few experimental studies have compared the dynamics of African savannas with and without browsers. 2. A replicated herbivore exclusion experiment was used to assess the role that native ungulates play in regulating woody plant dynamics on commercial rangeland in central Kenya, where the indigenous fauna have been allowed to coexist with cattle. 3. Exclusion of native browsing ungulates for just 3 years showed that they have dramatic effects at every scale from individual twig growth rates to overall rates of woody biomass accumulation in the ecosystem. 4. At the scale of individual Acacia twigs, browsers significantly reduced leaf density, leaf biomass and growth rates of twigs &lt; 50 cm above-ground (within the foraging height of dik-diks Madoqua kirkii), but browsers had no effects on twig leaf density or leaf biomass at a height of 1·0-1·25 m above-ground. 5. Reductions in the growth rate of twigs within the foraging height of dik-diks was associated with a 6-fold reduction in the rate at which shrubs &lt; 0·5-m tall were recruited into the 0·5-1·5 m height class. This reduced recruitment combined with measured rates of shrub mortality in larger height classes shows that browsers reduced the rate of increase in shrub density nearly to zero (7· 1 ± 10· 2 shrubs ha<sup>-1</sup>year<sup>-1</sup>) compared to the rapid rate of increase in the absence of browsers (136· 9 ± 13· 6 shrubs ha<sup>-1</sup>year<sup>-1</sup>). 6. Damage to shrub canopies by elephants Loxodonta africana caused large, significant reductions in cover of A. mellifera and Grewia tenax, but lesser reductions in cover of A. etbaica. For Acacias, elephant damage was focused on shrubs &gt; 2·5 m tall, such that Acacias in intermediate height classes (0·5-2·5 m) experienced minimal browser impacts. Elephants influenced shrubland dynamics by altering shrub height-class distributions, shifting species composition from broad-leaved Grewia tenax to fine-leaved Acacia species, and suppressing woody biomass accumulation; but elephants had little influence on changes in shrub density. 7. Synthesis and applications. Our results suggest that a community of native browsers that includes both small, selective species (e.g. dik-diks) and large, bulk-feeding species (elephants) can provide an important ecosystem service by suppressing shrub encroachment on commercial rangeland. </p>
         </abstract>
         <kwd-group>
            <kwd>Acacia savanna</kwd>
            <kwd>Aepyceros melampus</kwd>
            <kwd>Dik-dik</kwd>
            <kwd>Elephant</kwd>
            <kwd>Laikipia</kwd>
            <kwd>Loxodonta africana</kwd>
            <kwd>Kenya</kwd>
            <kwd>Madoqua kirkii</kwd>
            <kwd>Shrub encroachment</kwd>
            <kwd>Wildlife conservation</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d150e207a1310">
            <mixed-citation id="d150e211" publication-type="journal">
Adamoli, J., Sennhauser, E., Acero, J. &amp; Resica, A. (1990)
Stress and disturbance: vegetation dynamics in the dry
region of Argentina. Journalof Biogeography, 17,491-500.<person-group>
                  <string-name>
                     <surname>Adamoli</surname>
                  </string-name>
               </person-group>
               <fpage>491</fpage>
               <volume>17</volume>
               <source>Journalof Biogeography</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d150e246a1310">
            <mixed-citation id="d150e250" publication-type="book">
Ahn, P.M. &amp; Geiger, L.C. (1987) Kenya Soil Survey - Soils of
Laikipia District. Ministry of Agriculture, National Agri-
cultural Laboratories, Kabete, Kenya.<person-group>
                  <string-name>
                     <surname>Ahn</surname>
                  </string-name>
               </person-group>
               <source>Kenya Soil Survey - Soils of Laikipia District</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d150e279a1310">
            <mixed-citation id="d150e283" publication-type="journal">
Archer, S. (1995) Tree-grass dynamics in a Prosopis-
thornscrub savanna parkland: reconstructing the past and
predicting the future. Ecoscience, 2, 83-99.<person-group>
                  <string-name>
                     <surname>Archer</surname>
                  </string-name>
               </person-group>
               <fpage>83</fpage>
               <volume>2</volume>
               <source>Ecoscience</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d150e318a1310">
            <mixed-citation id="d150e322" publication-type="other">
Augustine, D.J. (2002) Large herbivores and process dynamics
in a managed savanna ecosystem. PhD dissertation, Syracuse
University, Syracuse, NY, USA.</mixed-citation>
         </ref>
         <ref id="d150e336a1310">
            <mixed-citation id="d150e340" publication-type="journal">
Augustine, D.J. (2003a) Long-term, livestock-mediated redis-
tribution of nitrogen and phosphorous in an East African
savanna. Journal of Applied Ecology, 40, 137-148.<object-id pub-id-type="jstor">10.2307/827266</object-id>
               <fpage>137</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e359a1310">
            <mixed-citation id="d150e363" publication-type="journal">
Augustine, D.J. (2003b) Spatial heterogeneity in the her-
baceous layer of a semi-arid savanna ecosystem. Plant
Ecology, 167, 319-332.<person-group>
                  <string-name>
                     <surname>Augustine</surname>
                  </string-name>
               </person-group>
               <fpage>319</fpage>
               <volume>167</volume>
               <source>Plant Ecology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d150e398a1310">
            <mixed-citation id="d150e402" publication-type="journal">
van Auken, O.W (2000) Shrub invasions of North American
semiarid grasslands. Annual Review of Ecology and
Systematics, 31, 197-215.<object-id pub-id-type="jstor">10.2307/221730</object-id>
               <fpage>197</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e421a1310">
            <mixed-citation id="d150e425" publication-type="journal">
Barnes, R. (1983) The elephant problem in Ruaha National
Park, Tanzania. Biological Conservation, 26, 127-148.<person-group>
                  <string-name>
                     <surname>Barnes</surname>
                  </string-name>
               </person-group>
               <fpage>127</fpage>
               <volume>26</volume>
               <source>Biological Conservation</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d150e457a1310">
            <mixed-citation id="d150e461" publication-type="journal">
Belsky, A. (1984) Role of small browsing mammals in pre-
venting woodland regeneration in the Serengeti National
Park, Tanzania. African Journal of Ecology, 22, 271-279.<person-group>
                  <string-name>
                     <surname>Belsky</surname>
                  </string-name>
               </person-group>
               <fpage>271</fpage>
               <volume>22</volume>
               <source>African Journal of Ecology</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d150e496a1310">
            <mixed-citation id="d150e500" publication-type="journal">
Ben-Shahar, R. (1996) Woodland dynamics under the influ-
ence of elephants and fire in northern Botswana. Vegetatio,
123, 153-163.<person-group>
                  <string-name>
                     <surname>Ben-Shahar</surname>
                  </string-name>
               </person-group>
               <fpage>153</fpage>
               <volume>123</volume>
               <source>Vegetatio</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d150e536a1310">
            <mixed-citation id="d150e540" publication-type="journal">
Blackmore, A.C., Mentis, M.T. &amp; Scholes, R.J. (1990) The
origin and extent of nutrient-enriched patches within a
nutrient-poor savanna in South Africa. Journal of Biogeo-
graphy, 17, 463-470.<object-id pub-id-type="doi">10.2307/2845378</object-id>
               <fpage>463</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e563a1310">
            <mixed-citation id="d150e567" publication-type="journal">
Boshe, J.I. (1984) Effects of reduced shrub density and cover
on territory size and structure of Kirk's dikdik in Arusha
National Park, Tanzania. African Journal of Ecology, 22,
107-115.<person-group>
                  <string-name>
                     <surname>Boshe</surname>
                  </string-name>
               </person-group>
               <fpage>107</fpage>
               <volume>22</volume>
               <source>African Journal of Ecology</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d150e605a1310">
            <mixed-citation id="d150e609" publication-type="journal">
Caughley, G. (1976) The elephant problem - an alternative
hypothesis. East African Wildlife Journal, 14, 265-284.<person-group>
                  <string-name>
                     <surname>Caughley</surname>
                  </string-name>
               </person-group>
               <fpage>265</fpage>
               <volume>14</volume>
               <source>East African Wildlife Journal</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d150e641a1310">
            <mixed-citation id="d150e645" publication-type="journal">
Cerling, T.E., Harris, J.M. &amp; Passey, B.H. (2003) Dietary pref-
erences of East African bovidae based on stable isotope
analysis. Journal of Mammology, 84, 456-470.<person-group>
                  <string-name>
                     <surname>Cerling</surname>
                  </string-name>
               </person-group>
               <fpage>456</fpage>
               <volume>84</volume>
               <source>Journal of Mammology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d150e680a1310">
            <mixed-citation id="d150e684" publication-type="journal">
Croze, H. (1974) The Seronera bull problem. II. The trees.
East African Wildlife Journal, 12, 29-48.<person-group>
                  <string-name>
                     <surname>Croze</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>12</volume>
               <source>East African Wildlife Journal</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d150e716a1310">
            <mixed-citation id="d150e720" publication-type="journal">
Deshmukh, I. (1992) Estimation of wood biomass in the
Jubba Valley, southern Somalia, and its application to
East African rangelands. African Journal of Ecology, 30,
127-136.<person-group>
                  <string-name>
                     <surname>Deshmukh</surname>
                  </string-name>
               </person-group>
               <fpage>127</fpage>
               <volume>30</volume>
               <source>African Journal of Ecology</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d150e759a1310">
            <mixed-citation id="d150e763" publication-type="journal">
Dublin, H., Sinclair, A.R.E. &amp; McGlade, J. (1990) Elephants
and fire as causes of multiple stable states in the Serengeti-
Mara woodlands. Journal of Animal Ecology, 59, 1147-
1164.<object-id pub-id-type="doi">10.2307/5037</object-id>
               <fpage>1147</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e786a1310">
            <mixed-citation id="d150e790" publication-type="book">
Epp, H., Herlocker, D. &amp; Peden, D. (1982) The use of large-
scale aerial photography to determine wood biomass in the
arid and semi-arid areas of Kenya. Kenya Rangeland Ecolog-
icalMonitoring Unit. Technical Report Series no. 51, Kenya
Rangeland Ecological Monitoring Unit, Nairobi, Kenya.<person-group>
                  <string-name>
                     <surname>Epp</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The use of largescale aerial photography to determine wood biomass in the arid and semi-arid areas of Kenya</comment>
               <source>Kenya Rangeland EcologicalMonitoring Unit</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d150e828a1310">
            <mixed-citation id="d150e832" publication-type="journal">
Field, C. &amp; Ross, I. (1976) The savanna ecology of Kidepo
Valley National Park. II. Feeding ecology of elephant and
giraffe. East African Wildlife Journal, 14, 35-48.<person-group>
                  <string-name>
                     <surname>Field</surname>
                  </string-name>
               </person-group>
               <fpage>35</fpage>
               <volume>14</volume>
               <source>East African Wildlife Journal</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d150e867a1310">
            <mixed-citation id="d150e871" publication-type="journal">
Gadd, M., Young, T. &amp; Palmer, T. (2001) Effects of simulated
shoot and leaf herbivory on vegetative growth and plant
defense in Acacia drepanolobium. Oikos, 92, 515-521.<object-id pub-id-type="jstor">10.2307/3547168</object-id>
               <fpage>515</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e890a1310">
            <mixed-citation id="d150e894" publication-type="journal">
Gowda, J.H. (1996) Spines of Acacia tortilis: what do they
defend and how? Oikos, 77, 279-284.<object-id pub-id-type="doi">10.2307/3546066</object-id>
               <fpage>279</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e910a1310">
            <mixed-citation id="d150e914" publication-type="journal">
Gowda, J.H. (1997) Physical and chemical response of juve-
nile Acacia tortilis trees to browsing. Functional Ecology,
11, 106-111.<object-id pub-id-type="jstor">10.2307/2390552</object-id>
               <fpage>106</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e934a1310">
            <mixed-citation id="d150e938" publication-type="book">
Hohnel, L.V. (1894) Discovery by Count Teleki of Lakes
Rudolf and Stephanie. Longmans and Green, London.<person-group>
                  <string-name>
                     <surname>Hohnel</surname>
                  </string-name>
               </person-group>
               <source>Discovery by Count Teleki of Lakes Rudolf and Stephanie</source>
               <year>1894</year>
            </mixed-citation>
         </ref>
         <ref id="d150e963a1310">
            <mixed-citation id="d150e967" publication-type="journal">
Jachmann, H. &amp; Bell, R.H.V (1985) Utilization by elephants
of the Brachystegia woodlands of the Kasungu National
Park, Malawi. African Journal of Ecology, 23, 245-258.<person-group>
                  <string-name>
                     <surname>Jachmann</surname>
                  </string-name>
               </person-group>
               <fpage>245</fpage>
               <volume>23</volume>
               <source>African Journal of Ecology</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1002a1310">
            <mixed-citation id="d150e1006" publication-type="journal">
Jachmann, H. &amp; Croes, T. (1991) Effects of browsing by
elephants on the Combretum/Terminalia woodland at the
Nazinga game ranch, Burkina Faso, West Africa. Biological
Conservation, 57, 13-24.<person-group>
                  <string-name>
                     <surname>Jachmann</surname>
                  </string-name>
               </person-group>
               <fpage>13</fpage>
               <volume>57</volume>
               <source>Biological Conservation</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1044a1310">
            <mixed-citation id="d150e1048" publication-type="journal">
Jeltsch, F., Milton, S.J., Dean, WR.J. &amp; Van Rooyen, N.
(1997) Analysing shrub encroachment in the southern
Kalahari: a grid-based modelling approach. Journal of
Applied Ecology, 34, 1497-1508.<object-id pub-id-type="doi">10.2307/2405265</object-id>
               <fpage>1497</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1071a1310">
            <mixed-citation id="d150e1075" publication-type="book">
Keuhl, R.O. (2000) Design of Experiments.: Statistical Prin-
ciples of Research Design and Analysis. Duxbury Press,
Belmont, CA, USA.<person-group>
                  <string-name>
                     <surname>Keuhl</surname>
                  </string-name>
               </person-group>
               <source>Design of Experiments.: Statistical Principles of Research Design and Analysis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1104a1310">
            <mixed-citation id="d150e1108" publication-type="journal">
Klein, D. &amp; Fairall, N. (1986) Comparative foraging beha-
viour and associated energetics of impala and blesbok.
Journal of Applied Ecology, 23, 489-502.<object-id pub-id-type="doi">10.2307/2404031</object-id>
               <fpage>489</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1128a1310">
            <mixed-citation id="d150e1132" publication-type="journal">
Laws, R.M. (1970) Elephants as agents of habitat and land-
scape change in East Africa. Oikos, 21, 1-15.<object-id pub-id-type="doi">10.2307/3543832</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1148a1310">
            <mixed-citation id="d150e1152" publication-type="journal">
Meissner, H.H., Pieterse, E. &amp; Potgieter, J.H.J. (1996) Sea-
sonal food selection and intake by male impala Aepyceros
melampus in two habitats. South African Journal of Wildlife
Research, 26, 56-63.<person-group>
                  <string-name>
                     <surname>Meissner</surname>
                  </string-name>
               </person-group>
               <fpage>56</fpage>
               <volume>26</volume>
               <source>South African Journal of Wildlife Research</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1190a1310">
            <mixed-citation id="d150e1194" publication-type="journal">
Meredith, M. &amp; Stehman, S. (1991) Repeated measures experi-
ments in forestry: focus on analysis of response curves.
Canadian Journal of Forest Research, 21, 957-965.<person-group>
                  <string-name>
                     <surname>Meredith</surname>
                  </string-name>
               </person-group>
               <fpage>957</fpage>
               <volume>21</volume>
               <source>Canadian Journal of Forest Research</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1229a1310">
            <mixed-citation id="d150e1233" publication-type="journal">
Neilson, R.P. (1986) High resolution climatic analysis and
southwest biogeography. Science, 232, 27-34.<object-id pub-id-type="jstor">10.2307/1697033</object-id>
               <fpage>27</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1249a1310">
            <mixed-citation id="d150e1253" publication-type="book">
O'Connor, T.G. (1985) A Synthesis of Field Experiments Con-
cerning the Grass Layer in the Savanna Regions of Southern
Africa. South African National Science Programme, Pretoria.<person-group>
                  <string-name>
                     <surname>O'Connor</surname>
                  </string-name>
               </person-group>
               <source>A Synthesis of Field Experiments Concerning the Grass Layer in the Savanna Regions of Southern Africa</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1282a1310">
            <mixed-citation id="d150e1286" publication-type="journal">
Oba, G. &amp; Post, E. (1999) Browse production and offtake by
free-ranging goats in an arid zone, Kenya. Journal of Arid
Environments, 43, 183-195.<person-group>
                  <string-name>
                     <surname>Oba</surname>
                  </string-name>
               </person-group>
               <fpage>183</fpage>
               <volume>43</volume>
               <source>Journal of Arid Environments</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1322a1310">
            <mixed-citation id="d150e1326" publication-type="book">
Patterson, J.H. (1910) In: The Grip of the Nyika. Macmillan,
London, UK.<person-group>
                  <string-name>
                     <surname>Patterson</surname>
                  </string-name>
               </person-group>
               <source>The Grip of the Nyika</source>
               <year>1910</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1351a1310">
            <mixed-citation id="d150e1355" publication-type="journal">
Pellew, R.A. (1983a) The impact of elephant, giraffe, and fire
upon the Acacia tortilis woodlands of the Serengeti. African
Journal of Ecology, 21, 41-74.<person-group>
                  <string-name>
                     <surname>Pellew</surname>
                  </string-name>
               </person-group>
               <fpage>41</fpage>
               <volume>21</volume>
               <source>African Journal of Ecology</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1390a1310">
            <mixed-citation id="d150e1394" publication-type="journal">
Pellew, R.A. (1983b) The giraffe and its food resource in the
Serengeti. I. Composition, biomass, and production of
available browse. African Journal of Ecology, 21, 241-268.<person-group>
                  <string-name>
                     <surname>Pellew</surname>
                  </string-name>
               </person-group>
               <fpage>241</fpage>
               <volume>21</volume>
               <source>African Journal of Ecology</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1429a1310">
            <mixed-citation id="d150e1433" publication-type="book">
Polhill, R.M. (1972) Flora of Tropical East Africa, Gramineae.
Part 1. Whitefriars Press, London, UK.<person-group>
                  <string-name>
                     <surname>Polhill</surname>
                  </string-name>
               </person-group>
               <source>Flora of Tropical East Africa, Gramineae</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1458a1310">
            <mixed-citation id="d150e1462" publication-type="book">
Polhill, R.M. (1974) Flora of Tropical East Africa, Gramineae.
Part 2. Whitefriars Press, London, UK.<person-group>
                  <string-name>
                     <surname>Polhill</surname>
                  </string-name>
               </person-group>
               <source>Flora of Tropical East Africa, Gramineae</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1487a1310">
            <mixed-citation id="d150e1491" publication-type="book">
Polhill, R.M. (1982) Flora of Tropical East Africa, Gramineae.
Part 3. A. A. Balkema, Rotterdam, The Netherlands.<person-group>
                  <string-name>
                     <surname>Polhill</surname>
                  </string-name>
               </person-group>
               <source>Flora of Tropical East Africa, Gramineae</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1517a1310">
            <mixed-citation id="d150e1521" publication-type="journal">
Prins, H.H.T. &amp; Van Der Juegd, H.P. (1993) Herbivore
population crashes and woodland structure in East Africa.
Journal of Ecology, 81, 305- 314.<object-id pub-id-type="doi">10.2307/2261500</object-id>
               <fpage>305</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1540a1310">
            <mixed-citation id="d150e1544" publication-type="journal">
Randerson, J.T., Chapin, ES. III, Harden, J.W., Neff, J.C. &amp;
Harmon, M.E. (2002) Net ecosystem production: a com-
prehensive measure of net carbon accumulation by ecosystems.
Ecological Applications, 12, 937-947.<object-id pub-id-type="doi">10.2307/3061028</object-id>
               <fpage>937</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1567a1310">
            <mixed-citation id="d150e1571" publication-type="journal">
Reid, R.S. &amp; Ellis, J.E. (1995) Impacts of pastoralists on
woodlands in south Turkana, Kenya: livestock-mediated
tree recruitment. Ecological Applications, 5, 978-992.<object-id pub-id-type="doi">10.2307/2269349</object-id>
               <fpage>978</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1590a1310">
            <mixed-citation id="d150e1594" publication-type="journal">
Roques, K., O'Connor, T. &amp; Watkinson, A. (2001) Dynamics
of shrub encroachment in an African savanna: relative
influences of fire, herbivory, rainfall and density dependence.
Journal of Applied Ecology, 38, 268-280.<object-id pub-id-type="jstor">10.2307/2655796</object-id>
               <fpage>268</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1617a1310">
            <mixed-citation id="d150e1621" publication-type="journal">
Ruess, R.W. &amp; Halter, F.L. (1990) The impact of large
herbivores on the Seronera woodlands, Serengeti
National Park, Tanzania. African Journal of Ecology, 28,
259-275.<person-group>
                  <string-name>
                     <surname>Ruess</surname>
                  </string-name>
               </person-group>
               <fpage>259</fpage>
               <volume>28</volume>
               <source>African Journal of Ecology</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1659a1310">
            <mixed-citation id="d150e1663" publication-type="journal">
Scholes, R.J., Dowty, P.R., Caylor, K., Parsons, D.A.B.,
Frost, PG.H. &amp; Shugart, H.H. (2002) Trends in savanna
structure and composition along an aridity gradient in
the Kalahari. Journal of Vegetation Science, 13, 419-
428.<object-id pub-id-type="jstor">10.2307/3236539</object-id>
               <fpage>419</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1690a1310">
            <mixed-citation id="d150e1694" publication-type="journal">
Stelfox, J.B. (1986) Effects of livestock enclosures (bomas) on
the vegetation of the Athi Plains, Kenya. African Journal of
Ecology, 24, 41-45.<person-group>
                  <string-name>
                     <surname>Stelfox</surname>
                  </string-name>
               </person-group>
               <fpage>41</fpage>
               <volume>24</volume>
               <source>African Journal of Ecology</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1729a1310">
            <mixed-citation id="d150e1733" publication-type="journal">
Thouless, C.R. (1995) Long distance movements of elephants
in northern Kenya. African. Journal of Ecology, 33,
321-334.<person-group>
                  <string-name>
                     <surname>Thouless</surname>
                  </string-name>
               </person-group>
               <fpage>321</fpage>
               <volume>33</volume>
               <source>African. Journal of Ecology</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1768a1310">
            <mixed-citation id="d150e1772" publication-type="journal">
du Toit, J., Bryant, J.P. &amp; Frisby, K. (1990) Regrowth and
palatability of Acacia shoots following pruning by African
savanna browsers. Ecology, 71, 149-154.<object-id pub-id-type="doi">10.2307/1940255</object-id>
               <fpage>149</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1791a1310">
            <mixed-citation id="d150e1795" publication-type="journal">
van Vegten, J.A. (1983) Thornbush invasion in a savanna
ecosystem in eastern Botswana. Vegetatio, 56, 3-7.<person-group>
                  <string-name>
                     <surname>Vegten</surname>
                  </string-name>
               </person-group>
               <fpage>3</fpage>
               <volume>56</volume>
               <source>Vegetatio</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1827a1310">
            <mixed-citation id="d150e1831" publication-type="journal">
van de Vijver, C., Foley, C. &amp; Olff, H. (1999) Changes in the
woody component of an East African savanna during 25
years. Journal of Tropical Ecology, 15, 545-564.<object-id pub-id-type="jstor">10.2307/2560203</object-id>
               <fpage>545</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1850a1310">
            <mixed-citation id="d150e1854" publication-type="journal">
Weltzin, J., Archer, S. &amp; Heitschmidt, R. (1997) Small-mammal
regulation of vegetation structure in a temperate savanna.
Ecology, 78, 751-763.<object-id pub-id-type="doi">10.2307/2266055</object-id>
               <fpage>751</fpage>
            </mixed-citation>
         </ref>
         <ref id="d150e1874a1310">
            <mixed-citation id="d150e1878" publication-type="journal">
Young, T.P. (1987) Increased thorn length in Acacia dre-
panolobium - an induced response to browsing. Oecologia,
71, 436-438.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <fpage>436</fpage>
               <volume>71</volume>
               <source>Oecologia</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1913a1310">
            <mixed-citation id="d150e1917" publication-type="journal">
Young, T.P. &amp; Okello, B.N. (1998) Relaxation of an induced
defense after exclusion of herbivores: spine length in Acacia
drepanolobium. Oecologia, 115, 508-513.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <fpage>508</fpage>
               <volume>115</volume>
               <source>Oecologia</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1952a1310">
            <mixed-citation id="d150e1956" publication-type="journal">
Young, T.P., Okello, B., Kinyua, D. &amp; Palmer, T. (1998)
KLEE: a long-term, multi-species herbivore exclusion
experiment in Laikipia, Kenya. African Journal of Range
and Forage Science, 14, 92-104.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <fpage>92</fpage>
               <volume>14</volume>
               <source>African Journal of Range and Forage Science</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d150e1994a1310">
            <mixed-citation id="d150e1998" publication-type="journal">
Young, T.P., Partridge, N. &amp; Macrae, A. (1995) Long-term
glades in acacia bushland and their edge effects in Laikipia,
Kenya. Ecological Applications, 5, 97-108.<object-id pub-id-type="doi">10.2307/1942055</object-id>
               <fpage>97</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
