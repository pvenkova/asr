<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plansystevol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009192</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant Systematics and Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer-Verlag</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03782697</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16156110</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43558151</article-id>
         <title-group>
            <article-title>Diverse pollination systems of the twin-spurred orchid genus Satyrium in African grasslands</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Steven D.</given-names>
                  <surname>Johnson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Craig I.</given-names>
                  <surname>Peter</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Allan G.</given-names>
                  <surname>Ellis</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Elin</given-names>
                  <surname>Boberg</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Christo</given-names>
                  <surname>Botes</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Timotheüs</given-names>
                  <surname>van der Niet</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">292</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40141466</issue-id>
         <fpage>95</fpage>
         <lpage>103</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43558151"/>
         <abstract>
            <p>The large terrestrial orchid genus Satyrium underwent evolutionary radiations in the Cape floral region and the grasslands of southern and eastern Africa. These radiations were accompanied by tremendous diversification of the unusual twin-spurred flowers that characterize the genus, but pollination data required to interpret these patterns of floral evolution have been lacking for grassland species in the genus. Here we document pollinators, nectar properties, and levels of pollination success for 11 grassland Satyrium species in southern and south-central Africa.Pollinators of these species include bees, beetles, butterflies, hawkmoths, noctuid moths, long-proboscid flies, and sunbirds. Most species appear to be specialized for pollination by one functional pollinator group. Longproboscid fly pollination systems are reported for the first time in Satyrium (in S. macrophyllum and a high-altitude form of S. neglectum). Floral morphology, especially spur length and rostellum structure, differs markedly among plants with different pollinators, while nectar volume, concentration, and sugar composition are fairly uniform across species. Most taxa exhibited high levels of pollination success (&gt; 50% of flowers pollinated), a trend that can be attributed to the presence of nectar in the twin spurs.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d232e214a1310">
            <mixed-citation id="d232e218" publication-type="other">
Ellis AG, Johnson SD (2010) Gender differences in the effects of
floral spur length manipulation on fitness in a hermaphrodite
orchid. Int J Plant Sci 171:1010-1019</mixed-citation>
         </ref>
         <ref id="d232e231a1310">
            <mixed-citation id="d232e235" publication-type="other">
Garside S (1922) The pollination of Satyrium bicallosum Thunb. Ann
Bolus Herb 3:137-154</mixed-citation>
         </ref>
         <ref id="d232e245a1310">
            <mixed-citation id="d232e249" publication-type="other">
Goldblatt P, Manning JC (2000) The long-proboscid fly pollination
system in southern Africa. Ann Missouri Bot Gard 87:146-170</mixed-citation>
         </ref>
         <ref id="d232e259a1310">
            <mixed-citation id="d232e263" publication-type="other">
Goldblatt P, Manning JC, Bernhardt P (1998) Adaptive radiation of
bee-pollinated Gladiolus species (Iridaceae) in southern Africa.
Ann Missouri Bot Gard 85:492-517</mixed-citation>
         </ref>
         <ref id="d232e277a1310">
            <mixed-citation id="d232e281" publication-type="other">
Harder LD, Johnson SD (2005) Adaptive plasticity of floral display
size in animal-pollinated plants. Proc R Soc B Biol Sci
272:2651-2657</mixed-citation>
         </ref>
         <ref id="d232e294a1310">
            <mixed-citation id="d232e298" publication-type="other">
Herrera CM (1990) The adaptedness of the floral phenotype in a relict
endemic, hawkmoth-pollinated violet. 1. Reproductive correlates
of floral variation. Biol J Linn Soc 40:263-274</mixed-citation>
         </ref>
         <ref id="d232e311a1310">
            <mixed-citation id="d232e315" publication-type="other">
Jersakova J, Johnson SD (2007) Protandry promotes male pollination
success in a moth-pollinated orchid. Funct Ecol 21:496-504</mixed-citation>
         </ref>
         <ref id="d232e325a1310">
            <mixed-citation id="d232e329" publication-type="other">
Johnson SD (1994) The pollination of Disa versicolor, Orchidaceae,
by anthophorid bees in South Africa. Lindleyana 9:209-212</mixed-citation>
         </ref>
         <ref id="d232e339a1310">
            <mixed-citation id="d232e343" publication-type="other">
Johnson SD (1996) Bird pollination in South African species of
Satyrium (Orchidaceae). Plant Syst Evol 203:91-98</mixed-citation>
         </ref>
         <ref id="d232e353a1310">
            <mixed-citation id="d232e357" publication-type="other">
Johnson SD (1997a) Insect pollination and floral mechanisms in
South African species of Satyrium (Orchidaceae). Plant Syst
Evol 204:195-206</mixed-citation>
         </ref>
         <ref id="d232e371a1310">
            <mixed-citation id="d232e375" publication-type="other">
Johnson SD (1997b) Pollination ecotypes of Satyrium hallackii
(Orchidaceae) in South Africa. Bot J Linn Soc 123:225-235</mixed-citation>
         </ref>
         <ref id="d232e385a1310">
            <mixed-citation id="d232e389" publication-type="other">
Johnson SD (2000) Batesian mimicry in the non-rewarding orchid
Disa pulchra, and its consequences for pollinator behaviour. Biol
J Linn Soc 71:119-132</mixed-citation>
         </ref>
         <ref id="d232e402a1310">
            <mixed-citation id="d232e406" publication-type="other">
Johnson SD (2006) Pollination by long-proboscid flies in the
endangered African orchid Disa scullyi. S Afr J Bot 72:24-27</mixed-citation>
         </ref>
         <ref id="d232e416a1310">
            <mixed-citation id="d232e420" publication-type="other">
Johnson SD, Nicolson SW (2008) Evolutionary associations between
nectar properties and specificity in bird-pollination systems. Biol
Lett 4:49-52</mixed-citation>
         </ref>
         <ref id="d232e433a1310">
            <mixed-citation id="d232e437" publication-type="other">
Johnson SD, Steiner KE (1995) Long-proboscid fly pollination of two
orchids in the Cape Drakensberg mountains, South Africa. Plant
Syst Evol 195:169-175</mixed-citation>
         </ref>
         <ref id="d232e450a1310">
            <mixed-citation id="d232e454" publication-type="other">
Johnson SD, Steiner KE (1997) Long-tongued fly pollination and
evolution of floral spur length in the Disa draconis complex
(Orchidaceae). Evolution 51:45-53</mixed-citation>
         </ref>
         <ref id="d232e468a1310">
            <mixed-citation id="d232e472" publication-type="other">
Johnson SD, Ellis A, Dotterl S (2007) Specialization for pollination
by beetles and wasps: the role of lollipop hairs and fragrance in
Satyrium microrrhynchum (Orchidaceae). Am J Bot 94:47-55</mixed-citation>
         </ref>
         <ref id="d232e485a1310">
            <mixed-citation id="d232e489" publication-type="other">
Kurzweil H (1996) Floral morphology and ontogeny in subtribe
Satyriinae (Fam Orchidaceae). Flora 191:9-28</mixed-citation>
         </ref>
         <ref id="d232e499a1310">
            <mixed-citation id="d232e503" publication-type="other">
la Croix IF (1991) Orchids of Malawi. A. A. Balkema, Rotterdam</mixed-citation>
         </ref>
         <ref id="d232e510a1310">
            <mixed-citation id="d232e514" publication-type="other">
Larsen MW, Peter C, Johnson SD, Olesen JM (2008) Comparative
biology of pollination systems in the African-Malagasy genus
Brownleea (Brownleeinae : Orchidaceae). Bot J Linn Soc
156:65-78</mixed-citation>
         </ref>
         <ref id="d232e530a1310">
            <mixed-citation id="d232e534" publication-type="other">
Manning J, Snijman D (2002) Hawkmoth pollination in Crinum
variabile (Amaryllidaceae) and the biogeography of sphingoph-
ily in southern African Amaryllidaceae. S Afr J Bot 68:212-216</mixed-citation>
         </ref>
         <ref id="d232e547a1310">
            <mixed-citation id="d232e551" publication-type="other">
Ollerton J, Johnson SD, Cranmer L, Kellie S (2003) The pollination
ecology of an assemblage of grassland asclepiads in South
Africa. Ann Bot 92:807-834</mixed-citation>
         </ref>
         <ref id="d232e565a1310">
            <mixed-citation id="d232e569" publication-type="other">
Peter CI, Johnson SD (2009) Pollination by flower chafer beetles in
Eulophia ensata and Eulophia welwitschii (Orchidaceae). S Afr J
Bot 75:762-770</mixed-citation>
         </ref>
         <ref id="d232e582a1310">
            <mixed-citation id="d232e586" publication-type="other">
Shuttleworth A, Johnson SD (2009) New records of insect pollinators
for South African asclepiads (Apocynaceae: Asclepiadoideae).
S Afr J Bot 75:689-698</mixed-citation>
         </ref>
         <ref id="d232e599a1310">
            <mixed-citation id="d232e603" publication-type="other">
Steiner KE (2010) Twin oil sacs facilitate the evolution of a novel
type of pollination unit (meranthium) in a South African orchid.
Am J Bot 97:311-323</mixed-citation>
         </ref>
         <ref id="d232e616a1310">
            <mixed-citation id="d232e620" publication-type="other">
Van der Niet T, Johnson SD (2009) Patterns of plant speciation in the
Cape floristic region. Mol Phylogenet Evol 51:85-93</mixed-citation>
         </ref>
         <ref id="d232e630a1310">
            <mixed-citation id="d232e634" publication-type="other">
van der Niet T, Linder HP (2008) Dealing with incongruence in the
quest for the species tree: a case study from the orchid genus
Satyrium. Mol Phylogenet Evol 47:154-174</mixed-citation>
         </ref>
         <ref id="d232e647a1310">
            <mixed-citation id="d232e651" publication-type="other">
Van der Niet T, Linder HP, Bytebier B, Bellstedt DU (2005)
Molecular markers reject monophyly of the subgenera of
Satyrium (Orchidaceae). Syst Bot 30:263-274</mixed-citation>
         </ref>
         <ref id="d232e665a1310">
            <mixed-citation id="d232e669" publication-type="other">
Van der Niet T, Zollikofer CPE, Ponce de León MS, Johnson SD,
Linder HP (2010) Three-dimensional geometric morphometries
for studying floral shape variation. Trends Plant Sci 15:423-426</mixed-citation>
         </ref>
         <ref id="d232e682a1310">
            <mixed-citation id="d232e686" publication-type="other">
van Wyk B-E, Whitehead CS, Glen HF, Hardy DS, van Jaarsveld EJ,
Smith GF (1993) Nectar sugar composition in the subfamily
Alooideae (Asphodelaceae). Biochem Syst Ecol 21:249-253</mixed-citation>
         </ref>
         <ref id="d232e699a1310">
            <mixed-citation id="d232e703" publication-type="other">
Vogel S (1954) Blütenbiologische Typen als Elemente der Sippen-
gliederug dargestellt anhand der Flora Südafrikas. Fischer, Jena</mixed-citation>
         </ref>
         <ref id="d232e713a1310">
            <mixed-citation id="d232e717" publication-type="other">
Vogel S (1959) Organographie der Blüten kapländischer Ophrydeen.
Akad Wiss Abh Math Naturwiss 6-7:1-268</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
