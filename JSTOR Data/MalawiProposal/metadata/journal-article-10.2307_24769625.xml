<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">populationenviro</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000178</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population and Environment</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer Science+Business Media</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01990039</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737810</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24769625</article-id>
         <article-categories>
            <subj-group>
               <subject>RESEARCH BRIEFS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Using a gender perspective to explore forest dependence in rural HIV/AIDS-affected Malawian households</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Joleen A.</given-names>
                  <surname>Timko</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert A.</given-names>
                  <surname>Kozak</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24769619</issue-id>
         <fpage>441</fpage>
         <lpage>454</lpage>
         <permissions>
            <copyright-statement>© Springer Science+Business Media 2014</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24769625"/>
         <abstract>
            <p>Throughout Sub-Saharan Africa, forest resources play a crucial role in enabling households to control and adapt to HIV/AIDS; however, little is known about how the disease impacts the gendered use of forest resources. This exploratory study characterizes how the dependence on forest resources changes for female and male respondents in HIV/AIDS-affected households in Malawi through three phases: before HIV was known to be present; during HIV-related morbidity; and after AIDS-related mortality. The results presented for female and male respondents in this paper are strikingly similar, and many respondents report that there are no longer any traditional gender roles for household tasks due to HIV/AIDS. Therefore, we question the thinking around gender-specific forest-related interventions for HIV-affected people. Moreover, given the gendered knowledge base that must surrounded resource use, what do these changes in traditional roles mean for sustainable forest resource use in the future? Further research on knowledge transmission around these resources is warranted.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d50e183a1310">
            <mixed-citation id="d50e187" publication-type="other">
Avasthi, A. (2004). Bush-meat trade breeds new HIV. New Scientist, 183(2459), 8-8.</mixed-citation>
         </ref>
         <ref id="d50e194a1310">
            <mixed-citation id="d50e198" publication-type="other">
Barany, M., Hammett, A. L., Sene, A., &amp; Amichev, B. (2001). Nontimber forest benefits and HIV/AIDS
in Sub-Saharan Africa. Journal of Forestry, 99(12), 26-41.</mixed-citation>
         </ref>
         <ref id="d50e208a1310">
            <mixed-citation id="d50e212" publication-type="other">
Barany, M., Holding-Anyonge, C., Kayambazinthu, D., &amp; Sitoe, A. (2005). Firewood, food, and
medicine: Interactions between forests, vulnerability and rural responses to HIV/AIDS. In
Proceedings of the IFPRI conference: HIV/AIDS and food and nutrition security. Durban, South
Africa, April 2006. Retrieved from http://www.fao.org/forestry/9719-088e4b8cdc0c6bd4b67eaa
81e6427a705.pdf.</mixed-citation>
         </ref>
         <ref id="d50e231a1310">
            <mixed-citation id="d50e235" publication-type="other">
Belcher, B. (2005). Forest product markets, forests and poverty reduction. International Forestry Review,
7(2), 82-89.</mixed-citation>
         </ref>
         <ref id="d50e246a1310">
            <mixed-citation id="d50e250" publication-type="other">
Bishop-Sambrook, C. (2003). Labour saving technologies and practices for farming and household
activities in Eastern and Southern Africa. Labour constraints and the impact of HIV/AIDS on rural
livelihoods in Bondo and Busia Districts Western Kenya. Rome: International Fund for Agricultural
Development/Food and Agriculture Organization of the United Nations. Retrieved from http://www.
ifad.org/genderpf/pdf/kenya.pdf.</mixed-citation>
         </ref>
         <ref id="d50e269a1310">
            <mixed-citation id="d50e273" publication-type="other">
Brown, K„ &amp; Lapuyade, S. (2001). Changing gender relationships and forest use: A case study from
Komassi, Cameroon. In C. Colfer &amp; Y. Byron (Eds.), People managing forests: The links between
human well-being and sustainability (pp. 90-110). Washington: Resources for the Future.</mixed-citation>
         </ref>
         <ref id="d50e286a1310">
            <mixed-citation id="d50e290" publication-type="other">
Colfer, C. J., Sheil, D., &amp; Kishi, M. (2006). Forests and human health: Assessing the evidence. CIFOR
Occasional Paper No. 45. Bogor: CIFOR.</mixed-citation>
         </ref>
         <ref id="d50e300a1310">
            <mixed-citation id="d50e304" publication-type="other">
de Sherbinin, A., VanWey, L. K., McSweeney, K., Aggarwal, R„ Barbieri, A., Henry, S., et al. (2008).
Rural household demographics, livelihoods and the environment. Global Environmental Change,
18, 38-53.</mixed-citation>
         </ref>
         <ref id="d50e317a1310">
            <mixed-citation id="d50e321" publication-type="other">
De Souza, R. M., Heinrich, G., Senefeld, S., Coon, K., Sebanja, P., Ogden, J., et al. (2008). Using
innovation to address HIV, AIDS, and environment links: Intervention case studies from Zimbabwe,
Uganda, and Malawi. Population and Environment, 29, 219-246.</mixed-citation>
         </ref>
         <ref id="d50e334a1310">
            <mixed-citation id="d50e338" publication-type="other">
Drimie, S., &amp; Gandure, S. (2005). The impacts of HIV/AIDS on rural livelihoods in Southern Africa: An
Inventory and Literature Review. Retrieved from http://www.zimrelief.info/files/attachments/doclib/
Impact%20of%20HIV%20&amp;%20AIDS%20on%20Livelihood%20in%20Southern%20Africa.pdf.</mixed-citation>
         </ref>
         <ref id="d50e352a1310">
            <mixed-citation id="d50e356" publication-type="other">
Frank, E., &amp; Unruh, J. (2008). Demarcating forest, containing disease: Land and HIV/AIDS in southern
Zambia. Population and Environment, 29, 108-132.</mixed-citation>
         </ref>
         <ref id="d50e366a1310">
            <mixed-citation id="d50e370" publication-type="other">
Holding-Anyonge, C., Rugalema, G., Kayambazinthu, D., Sitoe, A., &amp; Barany, M. (2006). Fuelwood,
food and medicine: the role of forests in the response to HIV and AIDS in rural areas of southern
Africa. Unasylva, 224(51), 20-23.</mixed-citation>
         </ref>
         <ref id="d50e383a1310">
            <mixed-citation id="d50e387" publication-type="other">
Hunter, L. M., de Souza, R. M., &amp; Twine, W. (2008a). The environmental dimensions of the HIV/AIDS
pandemic: A call for scholarship and evidence-based intervention. Population and Environment,
29(3-5), 103-107.</mixed-citation>
         </ref>
         <ref id="d50e400a1310">
            <mixed-citation id="d50e404" publication-type="other">
Hunter, L. M„ Twine, W„ &amp; Johnson, A. (2008a). Adult mortality and natural resource use in rural South
Africa: Evidence from the Agincourt Health and Demographic Surveillance Site. Institute of
Behavioral Science Research Program on Environment and Behavior Working Paper Series,
EB2005-0004. Boulder: University of Colorado at Boulder.</mixed-citation>
         </ref>
         <ref id="d50e420a1310">
            <mixed-citation id="d50e424" publication-type="other">
Hunter, L. M., Twine, W., &amp; Patterson, L. (2007). "Locusts are now our beef': adult mortality and
household dietary use of local environmental resources in rural South Africa. Scandinavian Journal
of Public Health, J5(3), 165-174.</mixed-citation>
         </ref>
         <ref id="d50e437a1310">
            <mixed-citation id="d50e441" publication-type="other">
Kaschula, S. A. (2008). Wild foods and household food security responses to AIDS: Evidence from South
Africa. Population and Environment, 29(3-5), 162-185.</mixed-citation>
         </ref>
         <ref id="d50e452a1310">
            <mixed-citation id="d50e456" publication-type="other">
Lengkeek, A. (2005). Trees on farm to mitigate the effects of HIV/AIDS in SSA. The Overstory, 152,
1-7.</mixed-citation>
         </ref>
         <ref id="d50e466a1310">
            <mixed-citation id="d50e470" publication-type="other">
Malawi Government. (2007). Forestry HIV and AIDS strategy 2007-2011. Ministry of Energy and Mines,
Malawi: Malawi Government.</mixed-citation>
         </ref>
         <ref id="d50e480a1310">
            <mixed-citation id="d50e484" publication-type="other">
Mather, D., Donovan, C., Jayne, T. S„ &amp; Weber, M. (2005). Using empirical information in the era of
HIV/AIDS to inform mitigation and rural development strategies: Selected results from African
country studies. American Journal of Agricultural Economics, 87(5), 1298-1303.</mixed-citation>
         </ref>
         <ref id="d50e497a1310">
            <mixed-citation id="d50e501" publication-type="other">
Mutangadura, G., Mukurazita, D., &amp; Jackson, H. (1999). A review of household and community
responses to the HIV/AIDS epidemic in the rural areas of Sub-Saharan Africa. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d50e511a1310">
            <mixed-citation id="d50e515" publication-type="other">
National AIDS Commission. (2008). HIV and Syphilis Sero—Survey and National HIV Prevalence and
AIDS Estimates Report for 2007. Malawi: Ministry of Health, Department of Preventive Health
Services.</mixed-citation>
         </ref>
         <ref id="d50e528a1310">
            <mixed-citation id="d50e532" publication-type="other">
Shackleton, C. M., Shackleton, S. E., Buiten, E„ &amp; Bird, N. (2007). The importance of dry woodlands and
forests in rural livelihoods and poverty alleviation in South Africa. Forest Policy and Economics,
9(5), 558-577.</mixed-citation>
         </ref>
         <ref id="d50e546a1310">
            <mixed-citation id="d50e550" publication-type="other">
Slater, R„ &amp; Wiggins, S. (2005). Responding to HIV/AIDS in agriculture and related activities. Natural
Resource Perspectives No. 98. London: Overseas Development Institute.</mixed-citation>
         </ref>
         <ref id="d50e560a1310">
            <mixed-citation id="d50e564" publication-type="other">
Timko, J. A. (2013a). HIV/AIDS and rural households in Malawi: Morbidity, mortality, and changing
dependence on important forest resources. In M. Gislason (Ed.), Ecological health: Society, ecology
and health (pp. 147-171). Bingley, UK: Emerald Group Publishing Limited.</mixed-citation>
         </ref>
         <ref id="d50e577a1310">
            <mixed-citation id="d50e581" publication-type="other">
Timko, J. A. (2013b). Exploring forest-related coping strategies for alleviating the HIV/AIDS burden on
rural Malawian households. International Forestry Review, 15(2), 230-240.</mixed-citation>
         </ref>
         <ref id="d50e591a1310">
            <mixed-citation id="d50e595" publication-type="other">
Timko, J. A., Kozak, R. A., &amp; Innes. J. L. (2010). HIV/AIDS and forests in Sub-Saharan Africa:
Exploring the links between morbidity, mortality, and dependence on biodiversity. Biodiversity,
11( 1&amp;2), 45-48.</mixed-citation>
         </ref>
         <ref id="d50e608a1310">
            <mixed-citation id="d50e612" publication-type="other">
Topouzis, D. N. (2007). HIV/AIDS: A risk to the social and economic sustainability of forestry in Sub-
Saharan Africa? International Forestry Review, 9(1), 558-562.</mixed-citation>
         </ref>
         <ref id="d50e622a1310">
            <mixed-citation id="d50e626" publication-type="other">
Torell, E., Tobey, J., Thaxton, M., Crawford, B., Kalangahec, B., Madulud, N., et al. (2006). Examining
the linkages between AIDS and biodiversity conservation in coastal Tanzania. Ocean and Coastal
Management, 49, 792-811.</mixed-citation>
         </ref>
         <ref id="d50e640a1310">
            <mixed-citation id="d50e644" publication-type="other">
Twine, W., &amp; Hunter, L. (2008). AIDS Mortality and the role of natural resources in household food
security in a rural district of South Africa. HIV, livelihoods, food and nutrition security: Findings
from renewal research (2007-2008), brief 11. Washington: IFPRI.</mixed-citation>
         </ref>
         <ref id="d50e657a1310">
            <mixed-citation id="d50e661" publication-type="other">
Villarreal, M., Holding-Anyonge, C„ Swallow, B„ &amp; Kewsiga, F. (2006). The challenge of HIV/AIDS:
Where does agroforestry fit in? In D. Garrity, A. Okono, M. Grayson, &amp; S. Parrott (Eds.), World
agroforestry into the future. Nairobi: World Agroforestry Centre.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
