<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26269473</article-id>
         <article-categories>
            <subj-group>
               <subject>Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Conservation and Unscripted Development</article-title>
            <subtitle>Proximity to Park Associated with Development and Financial Diversity</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Baird</surname>
                  <given-names>Timothy D.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Virginia Tech</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>3</month>
            <year>2014</year>
            <string-date>Mar 2014</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">19</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26269467</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2014 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26269473"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>Decades of research on the social dynamics of biodiversity conservation has shown that parks and protected areas have added hardship to rural communities throughout much of the developing world. Nonetheless, some recent studies have found evidence of poverty alleviation near protected areas. To build on these conflicting accounts, I use a comparative, mixed-methods design to examine opportunistic, unplanned, i.e., unscripted, development in indigenous communities near Tarangire National Park (TNP) in northern Tanzania. I ask the questions: (1) How is proximity to TNP related to community-level infrastructural development? (2) How has the process of development changed over time? and (3) How is proximity to TNP related to infrastructure-related social outcomes at the household-level? Results from semistructured interviews show that, compared with distant communities, communities near TNP have developed more extensive education and water infrastructure in the past decade by procuring financial support from a greater diversity of external organizations, including wildlife-related organizations. Correspondingly, household survey results show that education measures are positively associated with proximity to TNP, controlling for other factors. These findings support the notion that development can accrue near protected areas in ways that are diverse, uncoordinated, and opportunistic, and correspondingly distinct from heralded community-based conservation, community-based natural resource management, and integrated conservation and development project initiatives.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>Africa</kwd>
            <kwd>conservation</kwd>
            <kwd>development</kwd>
            <kwd>education</kwd>
            <kwd>infrastructure</kwd>
            <kwd>Tanzania</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Abbot, J. I. O., D. H. L. Thomas, A. A. Gardner, S. E. Neba, and M. W. Khen. 2001. Understanding the links between conservation and development in the Bamenda Highlands, Cameroon. World Development 29:1115-1136.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Abrams, R. W., E. D. Anwana, A. Ormsby, D. B. K. Dovie, A. Ajagbe, and A. Abrams. 2009. Integrating top-down with bottom-up conservation policy in Africa. Conservation Biology 23:799-804. http://dx.doi.org/10.1111/j.1523-1739.2009.01285.x</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Adams, W. M., R. Aveling, D. Brockington, B. Dickson, J. Elliott, J. Hutton, D. Roe, B. Vira, and W. Wolmer. 2004. Biodiversity conservation and the eradication of poverty. Science 306:1146-1149. http://dx.doi.org/10.1126/science.1097920</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Agrawal, A., and C. C. Gibson. 1999. Enchantment and disenchantment: the role of community in natural resource conservation. World Development 27:629-649. http://dx.doi.org/10.1016/S0305-750X(98)00161-2</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Agrawal, A., and K. Redford. 2006. Poverty, development, and biodiversity conservation: shooting in the dark? Working paper No. 26. Wildlife Conservation Society, New York, New York, USA. [online] URL: http://siteresources.worldbank.org/INTPOVERTYNET/Resources/Agrawal_Redford_WP26.pdf</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Andam, K. S., P. J. Ferraro, K. R. E. Sims, A. Healy, and M. B. Holland. 2010. Protected areas reduced poverty in Costa Rica and Thailand. Proceedings of the National Academy of Sciences 107:9996-10001. http://dx.doi.org/10.1073/pnas.0914177107</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Angeles, G., D. K. Guilkey, and T. A. Mroz. 2005. The impact of community-level variables on individual-level outcomes. Sociological Methods &amp; Research 34:76-121. http://dx.doi.org/10.1177/0049124104273069</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Armitage, D. 2005. Adaptive capacity and community-based natural resource management. Environmental Management 35:703-715. http://dx.doi.org/10.1007/s00267-004-0076-z</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Baird, T. D., and P. W. Leslie. 2013. Conservation as disturbance: upheaval and livelihood diversification near Tarangire National Park, northern Tanzania. Global Environmental Change 23:1131-1141. http://dx.doi.org/10.1016/j.gloenvcha.2013.05.002</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Baird, T. D., P. W. Leslie, and J. T. McCabe. 2009. The effect of wildlife conservation on local perceptions of risk and behavioral response. Human Ecology 37:463-474. http://dx.doi.org/10.1007/s10745-009-9264-z</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Baker, J., E. J. Milner-Gulland, and N. Leader-Williams. 2012. Park gazettement and integrated conservation and development as factors in community conflict at Bwindi Impenetrable Forest, Uganda. Conservation Biology 26:160-170. http://dx.doi.org/10.1111/j.1523-1739.2011.01777.x</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Bandyopadhyay, S., and G. Tembo. 2010. Household consumption and natural resource management around national parks in Zambia. Journal of Natural Resources Policy Research 2:39-55. http://dx.doi.org/10.1080/19390450903350838</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Barrett, C. B., and P. Arcese. 1995. Are integrated conservation-development projects (ICDPs) sustainable? On the conservation of large mammals in sub-Saharan Africa. World Development 23:1073-1084. http://dx.doi.org/10.1016/0305-750X(95)00031-7</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Barrett, C. B., A. J. Travis, and P. Dasgupta. 2011. On biodiversity conservation and poverty traps. Proceedings of the National Academy of Sciences 108:13907-13912. http://dx.doi.org/10.1073/pnas.1011521108</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Berkes, F. 2004. Rethinking community-based conservation. Conservation Biology 18:621-630. http://dx.doi.org/10.1111/j.1523-1739.2004.00077.x</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Bernard, H. R. 2006. Research methods in anthropology: qualitative and quantitative approaches. Fourth edition. Alta Mira Press, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Blaikie, P. 2006. Is small really beautiful? Community-based natural resource management in Malawi and Botswana. World Development 34:1942-1957. http://dx.doi.org/10.1016/j.worlddev.2005.11.023</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Brandon, K., K. Redford, and S. Sanderson, editors. 1998. Parks in peril: people, politics, and protected areas. Island, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Brandon, K. E., and M. Wells. 1992. Planning for people and parks: design dilemmas. World Development 20:557-570. http://dx.doi.org/10.1016/0305-750X(92)90044-V</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Brechin, S. R., P. R. Wilshusen, C. L. Fortwangler, and P. C. West. 2002. Beyond the square wheel: toward a more comprehensive understanding of biodiversity conservation as social and political process. Society &amp; Natural Resources 15:41-64. http://dx.doi.org/10.1080/089419202317174011</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Brockington, D. 2004. Community conservation, inequality and injustice: myths of power in protected area management. Conservation &amp; Society 2:411-432.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Brockington, D. 2007. Forests, community conservation, and local government performance: the village forest reserves of Tanzania. Society &amp; Natural Resources 20:835-848. http://dx.doi.org/10.1080/08941920701460366</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Brockington, D., R. Duffy, and J. Igoe. 2008. Nature unbound: conservation, capitalism, and the future of protected areas. Earthscan, London, UK.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Brockington, D., and J. Igoe. 2006. Eviction for conservation. A global overview. Conservation &amp; Society 4:424-470.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Brockington, D., J. Igoe, and K. Schmidt-Soltau. 2006. Conservation, human rights, and poverty reduction. Conservation Biology 20:250-252. http://dx.doi.org/10.1111/j.1523-1739.2006.00335.x</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Brosius, J. P., A. L. Tsing, and C. Zerner, editors. 2005. Communities and conservation: histories and politics of community-based natural resource management. Altamira Press, Walnut Creek, California, USA.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Campbell, L. M., and A. Vainio-Mattila. 2003. Participatory development and community-based conservation: opportunities missed for lessons learned? Human Ecology 31:417-437. http://dx.doi.org/10.1023/A:1025071822388</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Cernea, M. M., and K. Schmidt-Soltau. 2006. Poverty risks and national parks: policy issues in conservation and resettlement. World Development 34:1808-1830. http://dx.doi.org/10.1016/j.worlddev.2006.02.008</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Child, B., editor. 2004. Parks in transition: biodiversity, rural development and the bottom line. Earthscan, London, UK.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Child, B., and G. Barnes. 2010. The conceptual evolution and practice of community-based natural resource management in southern Africa: past, present and future. Environmental Conservation 37:283-295. http://dx.doi.org/10.1017/S0376892910000512</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Cooke, A. E. 2007. Subdividing the savanna: the ecology of change in Northern Tanzania. Dissertation. University of North Carolina, Chapel Hill, North Carolina, USA.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Coomes, O. T., B. L. Barham, and Y. Takasaki. 2004. Targeting conservation–development initiatives in tropical forests: insights from analyses of rain forest use and economic reliance among Amazonian peasants. Ecological Economics 51:47-64. http://dx.doi.org/10.1016/j.ecolecon.2004.04.004</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Davis, A. 2011. ‘Ha! What is the benefit of living next to the park?’ Factors limiting in-migration next to Tarangire National Park, Tanzania. Conservation &amp; Society 9:25-34. http://dx.doi.org/10.4103/0972-4923.79184</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Dowie, M. 2009. Conservation refugees. MIT Press, Cambridge, Massachusetts, USA.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Ferraro, P. J., and M. M. Hanauer. 2011. Protecting ecosystems and alleviating poverty with parks and reserves: ‘win-win’ or tradeoffs? Environmental and Resource Economics 48:269-286. http://dx.doi.org/10.1007/s10640-010-9408-z</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Ferraro, P. J., M. M. Hanauer, and K. R. E. Sims. 2011. Conditions associated with protected area success in conservation and poverty reduction. Proceedings of the National Academy of Sciences 108:13913-13918. http://dx.doi.org/10.1073/pnas.1011529108</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Figge, F. 2004. Bio-folio: applying portfolio theory to biodiversity. Biodiversity and Conservation 13:827-849. http://dx.doi.org/10.1023/B:BIOC.0000011729.93889.34</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Fratkin, E. 1997. Pastoralism: governance and development issues. Annual Review of Anthropology 26:235-261. http://dx.doi.org/10.1146/annurev.anthro.26.1.235</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Fratkin, E., and R. Mearns. 2003. Sustainability and pastoral livelihoods: lessons from East African Maasai and Mongolia. Human Organization 62:112-122.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Garnett, S. T., J. Sayer, and J. du Toit. 2007. Improving the effectiveness of interventions to balance conservation and development: a conceptual framework. Ecology and Society 12(1): 2. [online] URL: http://www.ecologyandsociety.org/vol12/iss1/art2/</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Goldman, M. 2003. Partitioned nature, privileged knowledge: community-based conservation in Tanzania. Development and Change 34:833-862. http://dx.doi.org/10.1111/j.1467-7660.2003.00331.x</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Goldman, M. 2011. Strangers in their own land: Maasai and wildlife conservation in northern Tanzania. Conservation &amp; Society 9:65-79. http://dx.doi.org/10.4103/0972-4923.79194</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Greiner, C. 2012. Unexpected consequences: wildlife conservation and territorial conflict in northern Kenya. Human Ecology 40:415-425. http://dx.doi.org/10.1007/s10745-012-9491-6</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Hackel, J. 1999. Community conservation and the future of Africa’s wildlife. Conservation Biology 13:726-734. http://dx.doi.org/10.1046/j.1523-1739.1999.98210.x</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Haley, M., and A. Clayton. 2003. The role of NGOs in environmental policy failures in a developing country: the mismanagement of Jamaica’s coral reefs. Environmental Values 12:29-54. http://dx.doi.org/10.3197/096327103129341216</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Hodgson, D. L. 2005. The church of women. Indiana University Press, Bloomington, Indiana, USA.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Hoffman, D., D. Fay, and L. Joppa. 2011. Human migration to protected area edges in Africa and Latin America: questioning large-scale statistical analysis. Conservation &amp; Society 9:1-7. http://dx.doi.org/10.4103/0972-4923.79177</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Homewood, K., P. Kristjanson, and P. C. Trench, editors. 2009. Staying Maasai?: Livelihoods, conservation and development in East African rangelands. Springer, New York, New York, USA. http://dx.doi.org/10.1007/978-0-387-87492-0</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Homewood, K. M., and W. A. Rodgers. 1991. Maasailand ecology: pastoralist development and wildlife conservation in Ngorongoro, Tanzania. Revised edition. Cambridge University Press, Cambridge, UK. http://dx.doi.org/10.1017/CBO9780511525568</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Igoe, J. 2002. National parks and human ecosystems: the challenge to community conservation. A case study from Simanjiro Tanzania. Pages 77-96 in D. Chatty and M. Colchester, editors. Conservation and mobile indigenous peoples. Berghahn Books, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Igoe, J. 2003. Scaling up civil society: donor money, NGOs and the pastoralist land rights movement in Tanzania. Development and Change 34:863-885. http://dx.doi.org/10.1111/j.1467-7660.2003.00332.x</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Jones, B. T. B., and M. Murphree. 2004. Community-based natural resource management as a conservation mechanism: lessons and direction. Pages 63-103 in B. Child, editor. Parks in transition. Earthscan, London, UK.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Kothari, U., editor. 2005. A radical history of development studies: individuals, institutions and ideologies. Zed Books, London, UK.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Kremen, C., A. M. Merenlender, and D. D. Murphy. 1994. Ecological monitoring: a vital need for integrated conservation and development programs in the tropics. Conservation Biology 8:388-397. http://dx.doi.org/10.1046/j.1523-1739.1994.08020388.x</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Lawton, J. H., and V. K. Brown. 1994. Redundancy in ecosystems. Pages 255-270 in E. D. Schulze and H. A. Mooney, editors. Biodiversity and ecosystem function. Springer, Berlin, Germany. http://dx.doi.org/10.1007/978-3-642-58001-7_12</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Leatherman, C. 2008. The poverty question. Nature Conservancy 58(1):42-51.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Leslie, P., and J. T. McCabe. 2013. Response diversity and resilience in social-ecological systems. Current Anthropology 54:114-143. http://dx.doi.org/10.1086/669563</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Levine, A. 2002. Convergence or convenience? International conservation NGOs and development assistance in Tanzania. World Development 30:1043-1055. http://dx.doi.org/10.1016/S0305-750X(02)00022-0</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Little, P. D., A. A. Aboud, and C. Lenachuru. 2009. Can formal education reduce risks for drought-prone pastoralists?: A case study from Baringo District, Kenya. Human Organization 68:154-165.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Mackey, R. L., and D. J. Currie. 2001. The diversity–disturbance relationship: is it generally strong and peaked? Ecology 82:3479-3492.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Markowitz, H. M. 1991. Foundations of portfolio theory. Journal of Finance 46:469-477. http://dx.doi.org/10.1111/j.1540-6261.1991.tb02669.x</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Martin, J., P. L. Fackler, J. D. Nichols, M. C. Runge, C. L. McIntyre, B. L. Lubow, M. C. McCluskie, and J. A. Schmutz. 2011. An adaptive-management framework for optimal control of hiking near Golden Eagle nests in Denali National Park. Conservation Biology 25:316-323.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Mbaiwa, J. E., A. Stronza, and U. Kreuter. 2011. From collaboration to conservation: insights from the Okavango Delta, Botswana. Society &amp; Natural Resources 24:400-411. http://dx.doi.org/10.1080/08941921003716745</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">McCabe, J. T. 1992. Can conservation and development be coupled among pastoral people? An examination of the Maasai of the Ngorongoro Conservation Area, Tanzania. Human Organization 51:353-366.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">McCabe, J. T., P. W. Leslie, and L. DeLuca. 2010. Adopting cultivation to remain pastoralists: the diversification of Maasai livelihoods in northern Tanzania. Human Ecology 38:321-334. http://dx.doi.org/10.1007/s10745-010-9312-8</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Miller, T. R., T. D. Baird, C. M. Littlefield, G. Kofinas, F. Chapin, III, and C. L. Redman. 2008. Epistemological pluralism: rethinking interdisciplinary research. Ecology and Society 13(2): 46. [online] URL: http://www.ecologyandsociety.org/vol13/iss2/art46/</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Morgan-Brown, T., S. K. Jacobson, K. Wald, and B. Child. 2010. Quantitative assessment of a Tanzanian integrated conservation and development project involving butterfly farming. Conservation Biology 24:563-572. http://dx.doi.org/10.1111/j.1523-1739.2009.01433.x</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Naughton-Treves, L., J. Alix-Garcia, and C. A. Chapman. 2011. Lessons about parks and poverty from a decade of forest loss and economic growth around Kibale National Park, Uganda. Proceedings of the National Academy of Sciences 108:13919-13924. http://dx.doi.org/10.1073/pnas.1013332108</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Naughton-Treves, L., M. B. Holland, and K. Brandon. 2005. The role of protecting areas in conserving biodiversity and sustaining local livelihoods. Annual Review of Environment and Resources 30:219-252. http://dx.doi.org/10.1146/annurev.energy.30.050504.164507</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">Nelson, F., and A. Agrawal. 2008. Patronage or participation? Community-based natural resource management reform in sub-Saharan Africa. Development and Change 39:557-585. http://dx.doi.org/10.1111/j.1467-7660.2008.00496.x</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Nelson, F., C. Foley, L. Foley, A. Leposo, E. Loure, D. Peterson, M. Peterson, T. Peterson, H. Sachedina, and A. Williams. 2010. Payments for ecosystem services as a framework for community-based conservation in northern Tanzania. Conservation Biology 24:78-85. http://dx.doi.org/10.1111/j.1523-1739.2009.01393.x</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Nelson, F., R. Nshala, and W. Rodgers. 2007. The evolution and reform of Tanzanian wildlife management. Conservation &amp; Society 5:232-261.</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Neumann, R. P. 1998. Imposing wilderness: struggles over livelihood and nature preservation in Africa. University of California Press, Berkeley, California, USA.</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">Newmark, W. D., and J. L. Hough. 2000. Conserving wildlife in Africa: integrated conservation and development projects and beyond. BioScience 50:585-592. http://dx.doi.org/10.1641/0006-3568(2000)050[0585:CWIAIC]2.0.CO;2</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">Nkedianye, D., M. Radeny, P. Kristjanson, and M. Herrero. 2009. Assessing returns to land and changing livelihood strategies in Kitengela. Pages 115-149 in K. Homewood, P. Kristjanson, and P. C. Trench, editors. Staying Maasai?: Livelihoods, conservation and development in East African rangelands. Springer, New York, New York, USA. http://dx.doi.org/10.1007/978-0-387-87492-0_4</mixed-citation>
         </ref>
         <ref id="ref76">
            <mixed-citation publication-type="other">Olson, D. M., and E. Dinerstein. 1998. The global 200: a representation approach to conserving the Earth’s most biologically valuable ecoregions. Conservation Biology 12:502–515. http://dx.doi.org/10.1046/j.1523-1739.1998.012003502.x</mixed-citation>
         </ref>
         <ref id="ref77">
            <mixed-citation publication-type="other">Oluoko-Odingo, A. A. 2011. Vulnerability and adaptation to food insecurity and poverty in Kenya. Annals of the Association of American Geographers 101:1-20. http://dx.doi.org/10.1080/00045608.2010.532739</mixed-citation>
         </ref>
         <ref id="ref78">
            <mixed-citation publication-type="other">Ostrom, E. 1990. Governing the commons: the evolution of institutions for collective action. Cambridge University Press, Cambridge, UK. http://dx.doi.org/10.1017/CBO9780511807763</mixed-citation>
         </ref>
         <ref id="ref79">
            <mixed-citation publication-type="other">Peters, J. 1998. Transforming the integrated conservation and development project (ICDP) approach: observations from the Ranomafana National Park Project, Madagascar. Journal of Agricultural and Environmental Ethics 11:17-47. http://dx.doi.org/10.1023/A:1007796628731</mixed-citation>
         </ref>
         <ref id="ref80">
            <mixed-citation publication-type="other">Redford, K. H. 2011. Misreading the conservation landscape. Oryx 45:324-330. http://dx.doi.org/10.1017/S0030605311000019</mixed-citation>
         </ref>
         <ref id="ref81">
            <mixed-citation publication-type="other">Richardson, R. B., A. Fernandez, D. Tschirley, and G. Tembo. 2012. Wildlife conservation in Zambia: impacts on rural household welfare. World Development 40:1068-1081. http://dx.doi.org/10.1016/j.worlddev.2011.09.019</mixed-citation>
         </ref>
         <ref id="ref82">
            <mixed-citation publication-type="other">Sachedina, H. T. 2008. Wildlife is our oil: conservation, livelihoods and NGOs in the Tarangire Ecosystem, Tanzania. Oxford University Press, Oxford, UK.</mixed-citation>
         </ref>
         <ref id="ref83">
            <mixed-citation publication-type="other">Sachedina, H. T. 2010. Disconnected nature: the scaling up of African wildlife foundation and its impacts on biodiversity conservation and local livelihoods. Antipode 42:603-623. http://dx.doi.org/10.1111/j.1467-8330.2010.00765.x</mixed-citation>
         </ref>
         <ref id="ref84">
            <mixed-citation publication-type="other">Sachedina, H., and F. Nelson. 2010. Protected areas and community incentives in savannah ecosystems: a case study of Tanzania’s Maasai Steppe. Oryx 44:390-398. http://dx.doi.org/10.1017/S0030605310000499</mixed-citation>
         </ref>
         <ref id="ref85">
            <mixed-citation publication-type="other">Sellen, D. W. 2003. Nutritional consequences of wealth differentials in East African pastoralists: the case of the Datoga of northern Tanzania. Human Ecology 31(4):529-570. http://dx.doi.org/10.1023/b:huec.0000005513.78183.34</mixed-citation>
         </ref>
         <ref id="ref86">
            <mixed-citation publication-type="other">Serneels, S., M. Herrero, S. BurnSilver, P. C. Trench, K. Cochrane, K. Homewood, P. Kristjanson, F. Nelson, M. Radeny, D. M. Thompson, and M. Y. Said. 2009. Methods in the analysis of Massai livelihoods. Pages 43-67 in K. Homewood, P. Kristjanson, and P. C. Trench, editors. Staying Maasai?: Livelihoods, conservation and development in East African rangelands. Springer, New York, New York, USA. http://dx.doi.org/10.1007/978-0-387-87492-0_2</mixed-citation>
         </ref>
         <ref id="ref87">
            <mixed-citation publication-type="other">Sheil, D., and D. F. R. P. Burslem. 2003. Disturbing hypotheses in tropical forests. Trends in Ecology &amp; Evolution 18:18-26. http://dx.doi.org/10.1016/S0169-5347(02)00005-8</mixed-citation>
         </ref>
         <ref id="ref88">
            <mixed-citation publication-type="other">Sheppard, D. J., A. Moehrenschlager, J. M. McPherson, and J. J. Mason. 2010. Ten years of adaptive community-governed conservation: evaluating biodiversity protection and poverty alleviation in a West African hippopotamus reserve. Environmental Conservation 37:270-282. http://dx.doi.org/10.1017/S037689291000041X</mixed-citation>
         </ref>
         <ref id="ref89">
            <mixed-citation publication-type="other">Sims, K. R. E. 2010. Conservation and development: evidence from Thai protected areas. Journal of Environmental Economics and Management 60:94-114. http://dx.doi.org/10.1016/j.jeem.2010.05.003</mixed-citation>
         </ref>
         <ref id="ref90">
            <mixed-citation publication-type="other">Tallis, H., P. Kareiva, M. Marvier, and A. Chang. 2008. An ecosystem services framework to support both practical conservation and economic development. Proceedings of the National Academy of Sciences 105:9457-9464. http://dx.doi.org/10.1073/pnas.0705797105</mixed-citation>
         </ref>
         <ref id="ref91">
            <mixed-citation publication-type="other">Trench, P. C., S. Kiruswa, F. Nelson, and K. Homewood. 2009. Still “People of Cattle”? Livelihoods, diversification and community conservation in Longido District. Pages 217-256 in K. Homewood, P. Kristjanson, and P. C. Trench, editors. Staying Maasai?: Livelihoods, conservation and development in East African rangelands. Springer, New York, New York, USA. http://dx.doi.org/10.1007/978-0-387-87492-0_6</mixed-citation>
         </ref>
         <ref id="ref92">
            <mixed-citation publication-type="other">Wainwright, C., and W. Wehrmeyer. 1998. Success in integrating conservation and development? A study from Zambia. World Development 26:933-944. http://dx.doi.org/10.1016/S0305-750X(98)00027-8</mixed-citation>
         </ref>
         <ref id="ref93">
            <mixed-citation publication-type="other">Waylen, K. A., A. Fischer, P. J. K. McGowan, S. J. Thirgood, and E. J. Milner-Gulland. 2010. Effect of local cultural context on the success of community-based conservation interventions. Conservation Biology 24:1119-1129. http://dx.doi.org/10.1111/j.1523-1739.2010.01446.x</mixed-citation>
         </ref>
         <ref id="ref94">
            <mixed-citation publication-type="other">Wells, M. P., and T. O. McShane. 2004. Integrating protected area management with local needs and aspirations. AMBIO: A Journal of the Human Environment 33:513-519.</mixed-citation>
         </ref>
         <ref id="ref95">
            <mixed-citation publication-type="other">West, P., and D. Brockington. 2006. An anthropological perspective on some unexpected consequences of protected areas. Conservation Biology 20:609-616. http://dx.doi.org/10.1111/j.1523-1739.2006.00432.x</mixed-citation>
         </ref>
         <ref id="ref96">
            <mixed-citation publication-type="other">West, P., J. Igoe, and D. Brockington. 2006. Parks and peoples: the social impact of protected areas. Annual Review of Anthropology 35:251-277. http://dx.doi.org/10.1146/annurev.anthro.35.081705.123308</mixed-citation>
         </ref>
         <ref id="ref97">
            <mixed-citation publication-type="other">Wilkie, D. S., G. A. Morelli, J. Demmer, M. Starkey, P. Telfer, and M. Steil. 2006. Parks and people: assessing the human welfare effects of establishing protected areas for biodiversity conservation. Conservation Biology 20:247-249. http://dx.doi.org/10.1111/j.1523-1739.2005.00291.x</mixed-citation>
         </ref>
         <ref id="ref98">
            <mixed-citation publication-type="other">Yasuda, A. 2011. The impacts of sport hunting on the livelihoods of local people: a case study of Bénoué National Park, Cameroon. Society &amp; Natural Resources 24:860-869. http://dx.doi.org/10.1080/08941920.2010.486394</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
