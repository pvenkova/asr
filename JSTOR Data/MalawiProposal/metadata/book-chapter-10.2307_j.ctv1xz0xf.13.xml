<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctv1xz0xf</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Champions for children</book-title>
         <subtitle>The lives of modern child care pioneers</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Holman</surname>
               <given-names>Bob</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>06</day>
         <month>03</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="epub">9781447318798</isbn>
      <isbn content-type="epub">144731879X</isbn>
      <publisher>
         <publisher-name>Policy Press</publisher-name>
         <publisher-loc>Bristol</publisher-loc>
      </publisher>
      <edition>REV - Revised, 1</edition>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Bob Holman</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctv1xz0xf"/>
      <abstract abstract-type="short">
         <p>Numerous books have been written about Victorian child care pioneers, but few biographical studies have been published about more recent child care and welfare giants. In the revised edition of this classic book, Bob Holman, a champion for children in his own right, looks at the lives of six inspirational individuals who have made significant contributions to the well-being of disadvantaged children. Each of the six discussed - Eleanor Rathbone, Lady Marjory Allen, Clare Winnicott, John Stroud, Barbara Kahan and Peter Townsend - has been important in establishing present systems of child care and welfare, and in stimulating debate around issues which remain high on policy and practitioner agendas. Champions for children is essential reading for childhood and youth studies, sociology of the family, social work, social welfare, academics and students with an interest in child care and welfare issues.Numerous books have been written about Victorian child care pioneers, but few biographical studies have been published about more recent child care and welfare giants. In the revised edition of this classic book, Bob Holman, a champion for children in his own right, looks at the lives of six inspirational individuals who have made significant contributions to the well-being of disadvantaged children. Each of the six discussed - Eleanor Rathbone, Lady Marjory Allen, Clare Winnicott, John Stroud, Barbara Kahan and Peter Townsend - has been important in establishing present systems of child care and welfare, and in stimulating debate around issues which remain high on policy and practitioner agendas. Champions for children is essential reading for childhood and youth studies, sociology of the family, social work, social welfare, academics and students with an interest in child care and welfare issues.</p>
      </abstract>
      <counts>
         <page-count count="232"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>iv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.5</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>Eleanor Rathbone, 1872-1946</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>She never had children, and never married. She came from a wealthy family and never lived alongside poor families. Yet she devoted much of her life to improving their material conditions by campaigning for children’s allowances to be paid directly to mothers. The woman was Eleanor Rathbone and, just before she died, she contributed in the House of Commons to the enactment of the 1945 Family Allowances Act which, Hilary Land stated, “was probably the most notable personal triumph in legislation since the Act which celebrates the Plimsoll line” (Land, 1990, p104). This chapter will attempt to trace how a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.6</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>Marjory Allen, 1897-1976</title>
                  </title-group>
                  <fpage>23</fpage>
                  <abstract>
                     <p>As Chapter One describes, Eleanor Rathbone was instrumental in the establishment of family allowances, which eventually benefited all children in the United Kingdom. Marjory Allen, on the other hand, became known for her concern with the substantial minority of children who were separated from their own parents.</p>
                     <p>Eleanor Rathbone was born into wealth and what is often called ‘the establishment’. She went to Oxford, had the money and the time to devote herself to social causes, and had the connections to ensure election as an MP. She did not marry, resisting social pressure to rely on an emotional relationship with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.7</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>Barbara Kahan, 1920-2000</title>
                  </title-group>
                  <fpage>49</fpage>
                  <abstract>
                     <p>In his book,<italic>Child welfare in England</italic>1872-1989, Harry Hendrick makes no mention of Barbara Kahan (née Langridge) (Hendrick, 1993). Yet anyone involved in the day-to-day practice of children’s services since 1948 knows the name of Barbara Kahan well. Indeed, they probably know it better than that of any other figure in child care. As a local authority practitioner, senior civil servant, writer and campaigner, she was always to the forefront. As there is no other full account of her life, it is timely to record and review her contribution.</p>
                     <p>Barbara Langridge was born in Horsted Keynes, Sussex. A family</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.8</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>John Stroud, 1923-89</title>
                  </title-group>
                  <fpage>75</fpage>
                  <abstract>
                     <p>In 1962, I started as a child care officer with Hertfordshire Children’s Department. I was informed that I would take over cases from John Stroud, the boys’ welfare officer who had been promoted. “You know who he is, don’t you?”, said my senior officer. To my shame I did not. “He’s the novelist, he wrote<italic>The shorn lamb</italic>”, she informed me, in reverential tones. A few days later, John Stroud came to see me. He came into my office and sat on the table, as the room was so small that it could take only one chair. This was difficult</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.9</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>Clare Winnicott, 1906-84</title>
                  </title-group>
                  <fpage>99</fpage>
                  <abstract>
                     <p>While Marjory Allen spearheaded the campaign which established local authority Children’s Departments, Barbara Kahan was actually one of the children’s officers who successfully organised them. John Stroud spread understanding of the ‘new’ child care to the public, and Clare Winnicott was the leading figure who trained staff for the child care service. Clare wrote just one book and very few articles. Even within these, she says little about herself. There is no published biography about her. Yet she was a charismatic person who made a strong impact on those who met her, and I was one of them. It has</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.10</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>Peter Townsend, 1928-2009</title>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>One of Peter Townsend’s earliest memories is of playing cricket with the comedian Arthur Askey. They met while Peter’s mother was performing in a seaside revue. Peter was to become Britain’s leading poverty researcher and campaigner against child poverty.</p>
                     <p>Peter’s parents were living in Middlesbrough when he was born in April 1928. The marriage soon broke up and Peter saw little of his father for years. By the age of three, Peter was living in London where his mother was pursuing her career as a singer and stage performer. As Peter recorded in an interview with Professor Paul Thompson (now</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.11</book-part-id>
                  <title-group>
                     <label>SEVEN</label>
                     <title>Bob Holman, 1936-:</title>
                     <subtitle>A child care participant living through the changes</subtitle>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>It has long been my intention to record the lives and achievements of notable children’s champions. When I submitted my proposal, I was delighted with the publisher’s enthusiasm. However, there was a sting in the tail. They wanted a final chapter about myself. Their reasoning was that this would bring the story up to date and would touch on ‘child care in the community’ which was not a major theme of the six great figures. I am no child care champ, more a child care chump. But I have been close to most of the figures in this book and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.12</book-part-id>
                  <title-group>
                     <label>EIGHT</label>
                     <title>Past, present and future</title>
                  </title-group>
                  <fpage>191</fpage>
                  <abstract>
                     <p>The champions for children who I have described are all aged over 65 or are deceased. The book might, therefore, be dismissed as irrelevant to contemporary child care. On the contrary, the lives of the six children’s champions are worth preserving in print in recognition of the enormous contribution they have made to the well-being of children. In addition, their practices, values, policies and writings have lessons for the present and the future. But the book is not only about them. It also draws on the experiences of those usually regarded just as recipients of services or as residents of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.13</book-part-id>
                  <title-group>
                     <title>Epilogue</title>
                  </title-group>
                  <fpage>205</fpage>
                  <abstract>
                     <p>
                        <italic>Champions for children</italic>appeared in 2001. With the death of Peter Townsend in 2009 all the champions discussed in this book are now dead. But they are not forgotten. Eleanor Rathbone’s contribution has been articulated in recent debates about child benefit while Frank Field MP continues to draw attention to her distinguished parliamentary career. Marjory Allen’s name is still attached to old and new adventure playgrounds. Barbara Kahan is known to today’s social workers for her careful analysis of what is required from children’s residential care and for her success in drawing attention to child abuse. John Stroud’s novels, particularly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.14</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>241</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>251</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv1xz0xf.16</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>263</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
