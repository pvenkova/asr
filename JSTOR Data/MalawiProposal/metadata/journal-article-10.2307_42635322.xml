<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jecongrowth</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000470</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Economic Growth</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">13814338</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737020</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42635322</article-id>
         <title-group>
            <article-title>Geography, institutions, and the making of comparative development</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Raphael A.</given-names>
                  <surname>Auer</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">18</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40098194</issue-id>
         <fpage>179</fpage>
         <lpage>215</lpage>
         <permissions>
            <copyright-statement>© 2013 Springer Science+Business Media</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s10887-013-9087-z"
                   xlink:title="an external site"/>
         <abstract>
            <p>While the direct impact of geographic endowments on prosperity is present in all countries, in former colonies, geography has also affected colonization policies and, therefore, institutional outcomes. Using non-colonized countries as a control group, I re-examine the theories put forward by La Porta et al. (J Law Econ Org 15(1):222-279, 1999 and Acemoglu et al. (Am Econ Rev 91 (5), 1369-1401, 2001. 1 find strong support for both theories, but also evidence that the authors' estimates of the impact of colonization on institutions and growth are biased, since they confound the effect of the historical determinants of institutions with the direct impact of geographic endowments on development. In a baseline estimation, I find that the approach of Acemoglu et al. (2001) overestimates the importance of institutions for economic growth by 28%, as a country's natural disease environment affected settler mortality during colonization and also has a direct impact on prosperity. The approach of La Porta et al. (1999) underestimates the importance of colonization-imposed legal origin for institutional development by 63 %, as Britain tended to colonize countries that are remote from Europe and thus suffer from low access to international markets.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d358e244a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d358e251" publication-type="other">
Galor and Weil 1999;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e257" publication-type="other">
Galor and Moav 2002;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e263" publication-type="other">
Galor 2005,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e270" publication-type="other">
Glaeser et al. 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e276" publication-type="other">
Putterman
and Weil 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e285" publication-type="other">
Spolaore and Wacziarg 2009;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e291" publication-type="other">
Ashraf and
Galor 2013</mixed-citation>
            </p>
         </fn>
         <fn id="d358e301a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d358e308" publication-type="other">
Nunn and Puga (2010),</mixed-citation>
            </p>
         </fn>
         <fn id="d358e315a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d358e322" publication-type="other">
Kaufmann et al. (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e328" publication-type="other">
Acemoglu et al. (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e334" publication-type="other">
La Porta
et al.'s (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e344" publication-type="other">
La
Porta et al.'s (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e353" publication-type="other">
La Porta et al.'s (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e359" publication-type="other">
Acemoglu et al. (2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d358e366a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d358e373" publication-type="other">
Acemoglu et al. (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d358e381a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d358e388" publication-type="other">
Albouy (2012)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e394" publication-type="other">
Acemoglu et al. (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d358e401a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d358e408" publication-type="other">
Acemoglu and Johnson (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d358e415a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d358e424" publication-type="other">
Feyrer and Sacerdote
2009</mixed-citation>
            </p>
         </fn>
         <fn id="d358e434a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d358e443" publication-type="other">
Bloom
and Canning (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d358e453a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d358e460" publication-type="other">
Segura-Cayuela (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d358e467a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d358e474" publication-type="other">
Feyrer's (2009a)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d358e480" publication-type="other">
Feyrer's (2009b)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d358e496a1310">
            <mixed-citation id="d358e500" publication-type="other">
Acemoglu, D., &amp; Johnson, S. (2005). Unbundling institutions. Journal of Political Economy, 113(5), 949-995.</mixed-citation>
         </ref>
         <ref id="d358e507a1310">
            <mixed-citation id="d358e511" publication-type="other">
Acemoglu, D., Johnson, S., &amp; Robinson, J. A. (2001). The colonial origins of comparative development: An
empirical investigation. The American Economic Review, 91(5), 1369-1401.</mixed-citation>
         </ref>
         <ref id="d358e521a1310">
            <mixed-citation id="d358e525" publication-type="other">
Acemoglu, D., Johnson, S., &amp; Robinson, J. A. (2002). Reversal of fortune: Geography and institutions in the
making of the modern world income distribution. The Quarterly Journal of Economics, 117(4), 1231-1294.</mixed-citation>
         </ref>
         <ref id="d358e535a1310">
            <mixed-citation id="d358e539" publication-type="other">
Acemoglu, D., Johnson, S., &amp; Robinson, J. A. (2003). Disease and development in historical perspective.
Journal of the European Economic Association, Papers and Proceedings, 1(2-3), 397-405.</mixed-citation>
         </ref>
         <ref id="d358e550a1310">
            <mixed-citation id="d358e554" publication-type="other">
Acemoglu, D., Johnson, S., &amp; Robinson, J. A. (2012). The colonial origins of comparative development: An
empirical investigation: Reply. American Economic Review, 102(6), 3077-3110.</mixed-citation>
         </ref>
         <ref id="d358e564a1310">
            <mixed-citation id="d358e568" publication-type="other">
Alesina, A., Devleeschauwer, A., Easterly, W., Kurlat, S., &amp; Wacziarg, R. (2003). Fractionalization. Journal
of Economic Growth, 8(2), 155-94.</mixed-citation>
         </ref>
         <ref id="d358e578a1310">
            <mixed-citation id="d358e582" publication-type="other">
Albouy, D. Y. (2012). The colonial origins of comparative development: An empirical investigation: comment.
American Economic Review, 102(6), 3059-3076.</mixed-citation>
         </ref>
         <ref id="d358e592a1310">
            <mixed-citation id="d358e596" publication-type="other">
Arapovic, D., &amp; Auer, R. A. (2012). A database of the colonial history of modern nations during imperialism.
Mimeo, SNB (available on request).</mixed-citation>
         </ref>
         <ref id="d358e606a1310">
            <mixed-citation id="d358e610" publication-type="other">
Ashraf, Q., &amp; Galor, O. (2013). The out of africa hypothesis, human genetic diversity and comparative economic
development. The American Economic Review, 103(1), 1-46.</mixed-citation>
         </ref>
         <ref id="d358e620a1310">
            <mixed-citation id="d358e624" publication-type="other">
Auer, R. A. (2007). The colonial origins of comparative development: A solution to the debate on settler
mortality rates. Working Papers 2007-09, Swiss National Bank.</mixed-citation>
         </ref>
         <ref id="d358e635a1310">
            <mixed-citation id="d358e639" publication-type="other">
Auer, R. A. (2008). The colonial and geographic origins of comparative development. Working Papers 2008-
08, Swiss National Bank.</mixed-citation>
         </ref>
         <ref id="d358e649a1310">
            <mixed-citation id="d358e653" publication-type="other">
Barro, R. J., &amp; Jong-Wha, L. (2000). International data on educational attainment: Updates and implications.
Cambridge: Harvard University.</mixed-citation>
         </ref>
         <ref id="d358e663a1310">
            <mixed-citation id="d358e667" publication-type="other">
Black, J., &amp; Woodfine, P. (Eds.). (1988). The British navy and the use of naval power in the eighteenth century.
Leicester: Leicester University Press.</mixed-citation>
         </ref>
         <ref id="d358e677a1310">
            <mixed-citation id="d358e681" publication-type="other">
Bloom, D. E. (2005). &amp; Canning, D. Health and economic growth: Reconciling the micro and macro evidence.
Mimeograph, Harvard School of Public Health.</mixed-citation>
         </ref>
         <ref id="d358e691a1310">
            <mixed-citation id="d358e695" publication-type="other">
Bloom, D. E., &amp; Sachs, J. D. (1998). Geography, demography, and economic growth in Africa. Brookings
Papers on Economic Activity, 1998(2), 207-73.</mixed-citation>
         </ref>
         <ref id="d358e705a1310">
            <mixed-citation id="d358e709" publication-type="other">
Conley, T. G., Hansen, C. B., &amp; Rossi, P. E. (2012). Plausibly exogenous. Review of Economics and Statistics,
94(1), 260-272.</mixed-citation>
         </ref>
         <ref id="d358e720a1310">
            <mixed-citation id="d358e724" publication-type="other">
Encyclopedia Britannica. (2010). http://www.britannica.com/EBchecked/topic/126354/colonization, Acce-
ssed on 15, August, 2010</mixed-citation>
         </ref>
         <ref id="d358e734a1310">
            <mixed-citation id="d358e738" publication-type="other">
Dell, M., Jones, B. F., &amp; Olken, B. (2008). Climate shocks and economic growth: Evidence from the last half
century November 2008. NBER Working Paper #14132.</mixed-citation>
         </ref>
         <ref id="d358e748a1310">
            <mixed-citation id="d358e752" publication-type="other">
Diamond, J. M. (1997). Guns, germs and steel: The fate of human societies. New York: W.W. Norton &amp; Co.</mixed-citation>
         </ref>
         <ref id="d358e759a1310">
            <mixed-citation id="d358e763" publication-type="other">
Djankov, S., Glaeser, E., La Porta, R., Lopez-de-Silanes, F., &amp; Shleifer, A. (2003). The new comparative
economics. Journal of Comparative Economics, 31(4), 595-619.</mixed-citation>
         </ref>
         <ref id="d358e773a1310">
            <mixed-citation id="d358e777" publication-type="other">
Dollar, D., &amp; Kraay, A. (2003). Institutions, trade and growth: Revisiting the evidence. Journal of Monetary
Economics, 50(1), 133-162.</mixed-citation>
         </ref>
         <ref id="d358e787a1310">
            <mixed-citation id="d358e791" publication-type="other">
Easterly, W., &amp; Levine, R. (2003). Tropics, germs, and crops: How endowments influence economic develop-
ment. Journal of Monetary Economics, 50(1), 3-39.</mixed-citation>
         </ref>
         <ref id="d358e802a1310">
            <mixed-citation id="d358e806" publication-type="other">
Engerman, S. L., &amp; Sokoloff, K. L. (1997). Factor endowments, institutions, and differential paths of growth
among new world economies. In H. Stephen (Ed.), How Latin America fell behind (pp. 260-304). Stanford,
CA: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d358e819a1310">
            <mixed-citation id="d358e823" publication-type="other">
Frankel, J. A., &amp; Romer, D. (1999). Does trade cause growth? The American Economic Review, 89(3), 379-399.</mixed-citation>
         </ref>
         <ref id="d358e830a1310">
            <mixed-citation id="d358e834" publication-type="other">
Feyrer, J. D., &amp; Sacerdote, B. (2009). Colonialism and modern income-islands as natural expenments. The
Review of Economics and Statistics, 91(2).</mixed-citation>
         </ref>
         <ref id="d358e844a1310">
            <mixed-citation id="d358e848" publication-type="other">
Feyrer, J. D. (2009a). Trade and income–exploiting time series in geography. NBER Working Paper 14910,
April 2009.</mixed-citation>
         </ref>
         <ref id="d358e858a1310">
            <mixed-citation id="d358e862" publication-type="other">
Feyrer, J. D. (2009b) Distance, trade, and income–The 1967 to 1975 closing of the Suez Canal as a natural
experiment. NBER Working Paper 15557, December 2009.</mixed-citation>
         </ref>
         <ref id="d358e872a1310">
            <mixed-citation id="d358e876" publication-type="other">
Gallego, F. A. (2010). Historical origins of schooling: The role of democracy and political decentralization.
The Review of Economics and Statistics, 92(2), 228-243.</mixed-citation>
         </ref>
         <ref id="d358e887a1310">
            <mixed-citation id="d358e891" publication-type="other">
Gallup, J. L., Meilinger, A. D., &amp; Sachs, J. D. (1998). Geography and economic development. Working Paper
No: 6849, National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d358e901a1310">
            <mixed-citation id="d358e905" publication-type="other">
Galor, O. (2005). Unified growth theory: From stagnation to growth. In P. Aghion &amp; S. Durlaut (Eds.),
Handbook of economic growth (pp. 171-293). Amsterdam: North-Holland.</mixed-citation>
         </ref>
         <ref id="d358e915a1310">
            <mixed-citation id="d358e919" publication-type="other">
Galor, O., &amp; Moav, O. (2002). Natural selection and the origin of economic growth (with O. Moav). Quarterly
Journal of Economics, 117, 1133-1192.</mixed-citation>
         </ref>
         <ref id="d358e929a1310">
            <mixed-citation id="d358e933" publication-type="other">
Galor, O., &amp; Mountford, A. (2006). Trade and the great divergence: The family connection. American Economic-
Review, 96, 229-303.</mixed-citation>
         </ref>
         <ref id="d358e943a1310">
            <mixed-citation id="d358e947" publication-type="other">
Galor, O., &amp; Weil, D. N. (1999). From malthusian stagnation to modern growth. American Economic Review ,
89, 150-154.</mixed-citation>
         </ref>
         <ref id="d358e957a1310">
            <mixed-citation id="d358e961" publication-type="other">
Glaeser, E., La Porta, R., Lopez-de-Silanes, F., &amp; Shleifer, A. (2004). Do institutions cause growth? Journal
of Economic Growth, 9(3), 271-304.</mixed-citation>
         </ref>
         <ref id="d358e972a1310">
            <mixed-citation id="d358e976" publication-type="other">
Gennaioli, N., &amp; Rainer, I. (2007). The modern impact of precolonial centralization in Africa. The Journal of
Economic Growth, 12(3), 185-234.</mixed-citation>
         </ref>
         <ref id="d358e986a1310">
            <mixed-citation id="d358e990" publication-type="other">
Hall, R. E., &amp; Jones, C. I. (1999). Why do some countries produce so much more output per worker than
others? The Quarterly Journal of Economics, 114(1), 83-116.</mixed-citation>
         </ref>
         <ref id="d358e1000a1310">
            <mixed-citation id="d358e1004" publication-type="other">
Kaufmann, D., Kraay, A., &amp; Mastruzzi, M. (2005). Governance matters IV: Governance indicators for 1996-
2004. World Bank Policy Research Working Paper Series No. 3630.</mixed-citation>
         </ref>
         <ref id="d358e1014a1310">
            <mixed-citation id="d358e1018" publication-type="other">
Kiszewski, A., Meilinger, A., Spielman, A., Malaney, P., Sachs, S. E., &amp; Sachs, J. D. (2004). A global index of
the stability of malaria transmission. American Journal of Tropical Medicine and Hygiene, 70(5), 486-498.</mixed-citation>
         </ref>
         <ref id="d358e1028a1310">
            <mixed-citation id="d358e1032" publication-type="other">
Knack, S., &amp; Keefer, P. (1995). Institutions and economic performance: Cross-country tests using alternative
measures. Economics and Politics, 7(3), 207-227.</mixed-citation>
         </ref>
         <ref id="d358e1042a1310">
            <mixed-citation id="d358e1046" publication-type="other">
Kohn, M. (2012) In E. N. Zalta (Ed.), "Colonialism". The Stanford encyclopedia of philosophy. http://plato.
stanford.edu/archives/sum2012/entries/colonialism/.</mixed-citation>
         </ref>
         <ref id="d358e1057a1310">
            <mixed-citation id="d358e1061" publication-type="other">
La Porta, R., Lopez-de-Silanes, F., Shleifer, A., &amp; Vishny, R. W. (1997). Legal determinants of external finance.
Journal of Finance, 52(3), 1131-1150.</mixed-citation>
         </ref>
         <ref id="d358e1071a1310">
            <mixed-citation id="d358e1075" publication-type="other">
La Porta, R., Lopez-de-Silanes, F., Shleifer, A., &amp; Vishny, R. W. (1998). Law and finance. The Journal of
Political Economy, 106(6), 1113-1155.</mixed-citation>
         </ref>
         <ref id="d358e1085a1310">
            <mixed-citation id="d358e1089" publication-type="other">
La Porta, R., Lopez-de-Silanes, F., Shleifer, A., &amp; Vishny, R. W. (1999). The quality of government. Journal
of Law, Economics and Organization, 15(1), 222-279.</mixed-citation>
         </ref>
         <ref id="d358e1099a1310">
            <mixed-citation id="d358e1103" publication-type="other">
Lipset, S. M. (1960). Political man: The social bases of politics. Garden City, NY: Doubleday.</mixed-citation>
         </ref>
         <ref id="d358e1110a1310">
            <mixed-citation id="d358e1114" publication-type="other">
Mauro, P. (1995). Corruption and growth. The Quarterly Journal of Economics, 110(3), 681-712.</mixed-citation>
         </ref>
         <ref id="d358e1121a1310">
            <mixed-citation id="d358e1125" publication-type="other">
McArthur, J. W., &amp; Sachs, J. D. (2001). institutions and geography: Comment on Acemoglu, Johnson and
Robinson (2000). NBER Working Paper #8114.</mixed-citation>
         </ref>
         <ref id="d358e1136a1310">
            <mixed-citation id="d358e1140" publication-type="other">
Meilinger, A., Gallup, J., &amp; Sachs, J. D. (2000). Climate, coastal proximity, and development. In G. L. Clark, M.
P. Feldman, &amp; M. S. Gertler (Eds.), Oxford handbook of economic geography. Oxford: Oxford University
Press.</mixed-citation>
         </ref>
         <ref id="d358e1153a1310">
            <mixed-citation id="d358e1157" publication-type="other">
North, D. C. (1981). Structure and change in economic history. New York: W. W. Norton &amp; Co.</mixed-citation>
         </ref>
         <ref id="d358e1164a1310">
            <mixed-citation id="d358e1168" publication-type="other">
Nunn, N., &amp; Puga, D. (2010). Ruggedness: The blessing of bad geography in Africa. Working Paper. Harvard
University.</mixed-citation>
         </ref>
         <ref id="d358e1178a1310">
            <mixed-citation id="d358e1182" publication-type="other">
Padró i Miquel. (2007). The control of politicians in divided societies: The politics of fear. Review of Economic
Studies, 74(4), 1259-1274.</mixed-citation>
         </ref>
         <ref id="d358e1192a1310">
            <mixed-citation id="d358e1196" publication-type="other">
Parker, P. M. (1997). National cultures of the world: A statistical reference, cross cultural statistical encyclo-
pedia of the world (Vol. 4). Westport, CT: Greenwood Press.</mixed-citation>
         </ref>
         <ref id="d358e1206a1310">
            <mixed-citation id="d358e1210" publication-type="other">
Putterman, L., &amp; Weil, D. N. (2010). Post-1500 population flows and the long run determinants of economic
growth and inequality. Quarterly Journal of Economics.</mixed-citation>
         </ref>
         <ref id="d358e1221a1310">
            <mixed-citation id="d358e1225" publication-type="other">
Rodrik, D., Subramanian, A., &amp; Trebbi, F. (2004). Institutions rule: The primacy of institutions over geography
and integration in economic development. Journal of Economic Growth, 9(2), 131-165.</mixed-citation>
         </ref>
         <ref id="d358e1235a1310">
            <mixed-citation id="d358e1239" publication-type="other">
Sachs, J. D. (2003). Institutions don't rule: Direct effects of geography on per capita income. February 2003.
NBER Working Paper #9490.</mixed-citation>
         </ref>
         <ref id="d358e1249a1310">
            <mixed-citation id="d358e1253" publication-type="other">
Segura-Cayuela, R. (2006). Inefficient policies, inefficient institutions and trade. 2006 meeting papers 502,
society for economic dynamics.</mixed-citation>
         </ref>
         <ref id="d358e1263a1310">
            <mixed-citation id="d358e1267" publication-type="other">
Spolaore, E., &amp; Wacziarg, R. (2009). The diffusion of development. Quarterly Journal of Economics, 124(2),
469-530.</mixed-citation>
         </ref>
         <ref id="d358e1277a1310">
            <mixed-citation id="d358e1281" publication-type="other">
Spolaore, E., &amp; Wacziarg, R. (forthcoming). How deep are the roots of economic development?. Journal of
Economic Literature.</mixed-citation>
         </ref>
         <ref id="d358e1291a1310">
            <mixed-citation id="d358e1295" publication-type="other">
Weil, D. N. (2007). Accounting for the effect of health on economic growth. The Quarterly Journal of Eco-
nomics, 122(3), 1265-1306.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
