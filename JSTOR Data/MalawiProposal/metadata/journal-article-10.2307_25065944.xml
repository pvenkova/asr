<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">taxon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100387</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Taxon</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Bureau for Plant Taxonomy and Nomenclature</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00400262</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25065944</article-id>
         <article-categories>
            <subj-group>
               <subject>Rubiaceae Systematics</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Sabiceeae and Virectarieae (Rubiaceae, Ixoroideae): One or Two Tribes? New Tribal and Generic Circumscriptions of Sabiceeae and Biogeography of Sabicea s.l.</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Saleh A.</given-names>
                  <surname>Khan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sylvain G.</given-names>
                  <surname>Razafimandimbison</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Birgitta</given-names>
                  <surname>Bremer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sigrid</given-names>
                  <surname>Liede-Schumann</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">57</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25065940</issue-id>
         <fpage>7</fpage>
         <lpage>23</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 The International Association for Plant Taxonomy</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25065944"/>
         <abstract>
            <p>The results of two recent phylogenetic studies led to the reinstatement of the tribe Sabiceeae, currently classified in the subfamily Ixoroideae s.l. (Rubiaceae) but with conflicting circumscriptions. In the present study, phylogenetic analyses based on nrITS and trnT-F sequence data of 78 taxa are performed to evaluate the different circumscriptions of Sabiceeae, the generic limits within Sabiceeae, and the biogeography of Sabicea. The polyphyly of Sabiceeae sensu Andersson is confirmed, and Pentaloncha and Temnopteryx are shown not to belong to Ixoroideae s.l. but to the subfamily Rubioideae. Our results favour a broad circumscription of Sabiceeae that includes Ecpoma, Hekistocarpa, Pseudosabicea, Sabicea, Schizostigma, Stipularia, Tamridaea and Virectaria. Sabicea sensu Wernham is not monophyletic unless Ecpoma, Pseudosabicea, Schizostigma, and Stipularia are included. We find no support for the monophyly of Stipularia, Sabicea and Pseudosabicea. Therefore, our newly circumscribed Sabiceeae contains only Hekistocarpa, Sabicea s.l. (Ecpoma, Pseudosabicea, Schizostigma, Stipularia), Tamridaea, and Virectaria. Finally, our analyses indicate several dispersal events of Sabicea species between African phytogeographical regions and continental African origins of the Malagasy, São Tomean, Asian, and Neotropical species of Sabicea via perhaps four independent dispersal events.</p>
         </abstract>
         <kwd-group>
            <kwd>Biogeography</kwd>
            <kwd>Nrits</kwd>
            <kwd>Rubiaceae</kwd>
            <kwd>Sabicea</kwd>
            <kwd>Sabiceeae</kwd>
            <kwd>trnT-F</kwd>
            <kwd>Virectarieae</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d827e202a1310">
            <mixed-citation id="d827e206" publication-type="other">
Alejandro, G.D., Razafimandimbison, S.G. &amp;amp; Liede-
Schumann, S. 2005. Polyphyly of Mussaenda inferred
from ITS and trnT-F data and its implication for generic
limits in Mussaendeae (Rubiaceae). Amer. J. Bot. 92:
544-557.</mixed-citation>
         </ref>
         <ref id="d827e225a1310">
            <mixed-citation id="d827e229" publication-type="other">
Andersson, L. 1996. Circumscription of the tribe Isertieae
(Rubiaceae). Opera Bot. Belg. 7: 139-164.</mixed-citation>
         </ref>
         <ref id="d827e239a1310">
            <mixed-citation id="d827e243" publication-type="other">
Andreasen, K., Baldwin, B.G. &amp;amp; Bremer, B. 1999. Phylo-
genetic utility of the nuclear rDNA ITS region in the sub-
family Ixoroideae (Rubiaceae): comparisons with cpDNA
rbcL sequence data. Pl. Syst. Evol. 217: 119-135.</mixed-citation>
         </ref>
         <ref id="d827e259a1310">
            <mixed-citation id="d827e263" publication-type="other">
Andreasen, K. &amp;amp; Bremer, B. 2000. Combined phylogenetic
analysis in the Rubiaceae-Ixoroideae: morphology, nuclear
and chloroplast DNA data. Amer. J. Bot. 87: 1731-1748.</mixed-citation>
         </ref>
         <ref id="d827e277a1310">
            <mixed-citation id="d827e281" publication-type="other">
Arnott, G.A.W. 1839. Descriptions of some new or rare Indian
plants. Ann. Nat. Hist. 3: 20-23.</mixed-citation>
         </ref>
         <ref id="d827e291a1310">
            <mixed-citation id="d827e295" publication-type="other">
Aublet, J.B.C.F. 1775. Sabicea, Pentandria, Monogynia. Pp.
192-196 in: Histoire des plantes de la Guiane Françoise,
vol. 1. P.-F. Didot jeune, London &amp;amp; Paris.</mixed-citation>
         </ref>
         <ref id="d827e308a1310">
            <mixed-citation id="d827e312" publication-type="other">
Baldwin, B.G., Anderson, M.J.S., Porter, J.M., Wojcie-
chowski, M.F., Campbell, C.S. &amp;amp; Donoghue, M.J.
1995. The ITS region of nuclear ribosomal DNA: a valu-
able source of evidence on angiosperm phylogeny. Ann.
Missouri Bot. Gard. 82: 247-277.</mixed-citation>
         </ref>
         <ref id="d827e331a1310">
            <mixed-citation id="d827e335" publication-type="other">
Bayer, R. J., Greber, D.G. &amp;amp; Bagnall, N.H. 2002. Phylogeny of
Australian Gnaphalieae (Asteraceae) based on chloroplast
and nuclear sequences, the trnL intron, trnL/trnF inter-
genic spacer, matK, and ETS. Syst. Bot. 27: 801-814.</mixed-citation>
         </ref>
         <ref id="d827e351a1310">
            <mixed-citation id="d827e355" publication-type="other">
Bremekamp, C.E.B. 1934. Notes on Rubiaceae of Surinam.
Recueil Trav. Bot. Néerl. 31: 248-308.</mixed-citation>
         </ref>
         <ref id="d827e365a1310">
            <mixed-citation id="d827e369" publication-type="other">
Bremekamp, C.E.B. 1952. The African species of Oldenlandia
L sensu Hiern et K. Schumann. Verh. Kon. Ned. Akad.
Wetensch., Afd. Natuurk., Sect. 2., 48: 1-297.</mixed-citation>
         </ref>
         <ref id="d827e383a1310">
            <mixed-citation id="d827e389" publication-type="other">
Bremekamp, C.E.B. 1966. Remarks on the position, the de-
limitation and the subdivision of the Rubiaceae. Acta Bot.
Neerl. 15: 1-33.</mixed-citation>
         </ref>
         <ref id="d827e402a1310">
            <mixed-citation id="d827e406" publication-type="other">
Bremer, B. 1996. Combined and separate analyses of morpho-
logical and molecular data in the plant family Rubiaceae.
Cladistics 12: 21-40.</mixed-citation>
         </ref>
         <ref id="d827e419a1310">
            <mixed-citation id="d827e423" publication-type="other">
Bremer, B., Jansen, R.K., Oxelman, B., Backlund, M.,
Lantz, H. &amp;amp; Kim, K.J. 1999. More characters or more
taxa for a robust phylogeny—case study from the coffee
family (Rubiaceae). Syst. Biol. 48: 413-435.</mixed-citation>
         </ref>
         <ref id="d827e439a1310">
            <mixed-citation id="d827e443" publication-type="other">
Bremer, B. &amp;amp; Manen, J.F. 2000. Phylogeny and classification
of the subfamily Rubioideae (Rubiaceae). PL Syst. Evol.
225: 43-72.</mixed-citation>
         </ref>
         <ref id="d827e456a1310">
            <mixed-citation id="d827e460" publication-type="other">
Bremer, B. &amp;amp; Thulin, M. 1998. Collapse of Isertieae, re-estab-
lishment of Mussaendeae, and a new genus of Sabiceeae
(Rubiaceae): phylogenetic relationships based on rbcL
data. Pl. Syst. Evol. 211: 71-92.</mixed-citation>
         </ref>
         <ref id="d827e476a1310">
            <mixed-citation id="d827e480" publication-type="other">
Darwin, S.P. 1976. The subfamilial, tribal, and subtribal no-
menclature of the Rubiaceae. Taxon 25: 595-610.</mixed-citation>
         </ref>
         <ref id="d827e491a1310">
            <mixed-citation id="d827e495" publication-type="other">
Dessein, S., Andersson, L., Robbrecht, E. &amp;amp; Smets, E. 2001a.
Hekistocarpa (Rubiaceae): a member of an emended tribe
Virectarieae. Pl. Syst. Evol. 229: 59-78.</mixed-citation>
         </ref>
         <ref id="d827e508a1310">
            <mixed-citation id="d827e512" publication-type="other">
Dessein, S., Jansen, S., Huysmans, S., Robbrecht, E. &amp;amp;
Smets, E. 2001b. A morphological and anatomical survey
of Virectaria (African Rubiaceae), with a discussion of its
taxonomic position. Bot. J. Linn. Soc. 137: 1-29.</mixed-citation>
         </ref>
         <ref id="d827e528a1310">
            <mixed-citation id="d827e532" publication-type="other">
Deruelle, B., Moreau, C., Nkoumbou, C., Kambou, R., Lis-
som, J., Njongfang, E., Ghogomu, R.T. &amp;amp; Nono, A. 1991.
The Cameroon Line: a review. Pp. 274-327 in: Kampunzu,
A.B. &amp;amp; Lubala, R.T. (eds.), Magmatism in Extensional
Structural Settings. Springer Verlag, Berlin.</mixed-citation>
         </ref>
         <ref id="d827e551a1310">
            <mixed-citation id="d827e555" publication-type="other">
Good, R. 1923. New Tropical African Rubiaceae. J. Bot., 61:
86.</mixed-citation>
         </ref>
         <ref id="d827e565a1310">
            <mixed-citation id="d827e569" publication-type="other">
Grisebach, A.H.R. 1861. XCIV Rubiaceae. Pp. 316-351 in:
Flora of the British West Indian Islands. L. Reeve &amp;amp; Co,
London.</mixed-citation>
         </ref>
         <ref id="d827e582a1310">
            <mixed-citation id="d827e586" publication-type="other">
Hallé, F. 1961. Contribution à l'étude biologique et taxono-
mique des Mussaendeae (Rubiaceae) d'Afrique tropicale.
Adansonia 1: 266-298.</mixed-citation>
         </ref>
         <ref id="d827e600a1310">
            <mixed-citation id="d827e604" publication-type="other">
Hallé, N. 1963. Délimitation des genres Sabicea Aubl. et Ec-
poma K. Schum. en regard d'un genre nouveau: Pseudo-
sabicea (Mussaendeae-Rubiaceae). Adansonia ser. 2, 3:
168-177.</mixed-citation>
         </ref>
         <ref id="d827e620a1310">
            <mixed-citation id="d827e624" publication-type="other">
Hallé, N. 1966. Famille des Rubiacées (lre partie). Vol. 12 of:
Aubréville, A. (ed.), Flore du Gabon. Muséum National
d'Histoire Naturelle, Laboratoire de Phanérogamie, Pa-
ris.</mixed-citation>
         </ref>
         <ref id="d827e640a1310">
            <mixed-citation id="d827e644" publication-type="other">
Hallé, N. 1970. Famille des Rubiacées (2c partie). Vol. 17 of:
Aubréville, A. &amp;amp; Leroy, J.F. (eds.), Flore du Gabon. Mu-
séum National d'Histoire Naturelle, Laboratoire de Pha-
nérogamie, Paris.</mixed-citation>
         </ref>
         <ref id="d827e660a1310">
            <mixed-citation id="d827e664" publication-type="other">
Hepper, F.N. 1958. Sabicea Aubl. and Stipularia Beauv.
(Rubiaceae-Mussaendeae) in Tropical Africa. Kew Bull.
289-294.</mixed-citation>
         </ref>
         <ref id="d827e677a1310">
            <mixed-citation id="d827e681" publication-type="other">
Hiern, W.P. 1877. Ordo LXX. Rubiaceae. Pp. 33-82 in: Oliver,
D., Dyer, W.T.T, Prain, D. &amp;amp; Hill, A.W. (eds.), Flora of
Tropical Africa, vol. 3. L. Reeve &amp;amp; Co., London.</mixed-citation>
         </ref>
         <ref id="d827e694a1310">
            <mixed-citation id="d827e698" publication-type="other">
Hooker, J.D. 1873a. Ordo LXXXIV, Rubiaceae. Pp. 7-151 in:
Bentham, G. &amp;amp; Hooker, J.D. (eds.), Genera Plantarum ad
exemplaria imprimis in herbariis Kewensibus servanta
defirmata, vol. 2. L. Reeve &amp;amp; Co., London.</mixed-citation>
         </ref>
         <ref id="d827e715a1310">
            <mixed-citation id="d827e719" publication-type="other">
Hooker, J.D. 1873b. Hekistocarpa minutiflora. Icones Planta-
rum 12: 46. William Pamplin, London.</mixed-citation>
         </ref>
         <ref id="d827e729a1310">
            <mixed-citation id="d827e733" publication-type="other">
Huelsenbeck, J.P. &amp;amp; Ronquist, F. 2001. MrBayes: Bayesian
inference of phylogeny. Bioinformatics 17: 754-755.</mixed-citation>
         </ref>
         <ref id="d827e743a1310">
            <mixed-citation id="d827e747" publication-type="other">
Joffroy, G. 2001. Le genre Sabicea (Rubiaceae) à São Tomé
(São Tomé et Príncipe). Syst. Geogr. Pl. 71: 383-390.</mixed-citation>
         </ref>
         <ref id="d827e757a1310">
            <mixed-citation id="d827e761" publication-type="other">
Kiehn, M. 1995. Chromosome survey of the Rubiaceae. Ann.
Missouri Bot. Gard. 82: 398-408.</mixed-citation>
         </ref>
         <ref id="d827e771a1310">
            <mixed-citation id="d827e775" publication-type="other">
Kirkbride, M.C.G. 1979. Review of the Neotropical Isertieae
(Rubiaceae). Brittonia 31: 313-332.</mixed-citation>
         </ref>
         <ref id="d827e785a1310">
            <mixed-citation id="d827e789" publication-type="other">
Kirkbride, M.C.G. 1982. A preliminary phylogeny for Neo-
tropical Rubiaceae. Pl. Syst. Evol. 141: 115-121.</mixed-citation>
         </ref>
         <ref id="d827e800a1310">
            <mixed-citation id="d827e804" publication-type="other">
Lantz, H. &amp;amp; Bremer, B. 2004. Phylogeny inferred from mor-
phology and DNA data: characterizing well-supported
groups in Vanguerieae (Rubiaceae). Bot. J. Linn. Soc.
146: 257-283.</mixed-citation>
         </ref>
         <ref id="d827e820a1310">
            <mixed-citation id="d827e824" publication-type="other">
Liede, S. &amp;amp; Kunze, H. 2002. Cynanchum and the Cynanchi-
nae (Apocynaceae-Asclepiadoideae)—a molecular, ana-
tomical and latex triterpenoid study. Organisms Divers.
Evol. 2: 239-269.</mixed-citation>
         </ref>
         <ref id="d827e840a1310">
            <mixed-citation id="d827e844" publication-type="other">
Meisner, C.F. 1838. Plantarum Vascularium Genera, Secundum
Ordines Naturales Digesta, 1: 162. Libraria Weidmannia,
Lipsiae (Leipzig).</mixed-citation>
         </ref>
         <ref id="d827e857a1310">
            <mixed-citation id="d827e861" publication-type="other">
Meve, U. &amp;amp; Liede, S. 2002. A molecular phylogeny and
generic rearrangement of the stapelioid Ceropegieae
(Apocynaceae-Asclepiadoideae). Pl. Syst. Evol. 234:
171-209.</mixed-citation>
         </ref>
         <ref id="d827e877a1310">
            <mixed-citation id="d827e881" publication-type="other">
Motley, T. J., Wurdack, K.J. &amp;amp; Delprete, P.G. 2005. Molecu-
lar systematics of the Catesbaeeae-Chiococceae complex
(Rubiaceae): flower and fruit evolution and biogeographic
implications. Amer. J. Bot. 92: 316-329.</mixed-citation>
         </ref>
         <ref id="d827e897a1310">
            <mixed-citation id="d827e901" publication-type="other">
Munhá, J., Afonso, R., Caldeira, R. &amp;amp; Mata, J. 2002. Estudo
geológico preliminar da região nordeste da Una de S Tomé
(Folha n°2, Ana Chaves). Garcia de Horta, ser. Geol. 18:
1-8.</mixed-citation>
         </ref>
         <ref id="d827e918a1310">
            <mixed-citation id="d827e922" publication-type="other">
Noyes, R.D. 2006. Intraspecific nuclear ribosomal DNA diver-
gence and reticulation in sexual diploid Erigeron strigosus
(Asteraceae). Amer. J. Bot. 93: 470-479.</mixed-citation>
         </ref>
         <ref id="d827e935a1310">
            <mixed-citation id="d827e939" publication-type="other">
Nylander, J.A.A. 2004. MrModeltest, v2. Program distrib-
uted by the author. Evolutionary Biology Centre, Uppsala
University.</mixed-citation>
         </ref>
         <ref id="d827e952a1310">
            <mixed-citation id="d827e956" publication-type="other">
Page, R.D.M. 1996. TREEVIEW: an application to display
phylogenetic trees on personal computers. Comput. Applic.
Biosci. 12: 357-358.</mixed-citation>
         </ref>
         <ref id="d827e969a1310">
            <mixed-citation id="d827e973" publication-type="other">
Palisot-Beauvois, A.M.F.J. 1810 [1807]. Stipulaire. Stipularia,
Fam. des Rubiacées. P. 26 in: Flore d'Oware et de Bénin,
en Afrique. Imprimerie de Fain et compagnie, Paris.</mixed-citation>
         </ref>
         <ref id="d827e986a1310">
            <mixed-citation id="d827e990" publication-type="other">
Persson, C. 2000. Phylogeny of the Neotropical A liberna group
(Rubiaceae), with emphasis on the genus Alibertia, inferred
from ITS and 5S Ribosomal DNA sequences. Amer. J. Bot.
87: 1018-1028.</mixed-citation>
         </ref>
         <ref id="d827e1006a1310">
            <mixed-citation id="d827e1010" publication-type="other">
Puff, C., Igersheim, A. &amp;amp; Buchner, R. 1998. Character states
and taxonomic position of the monotypic Sri Lankan
Schizostigma (Rubiaceae-Isertieae). Pp. 187-203 in:
Dransfield, J., Coode, M.J.E. &amp;amp; Simpson, DA. (eds.),
Plant Diversity in Malesia III. Royal Botanic Gardens,
Kew, London.</mixed-citation>
         </ref>
         <ref id="d827e1034a1310">
            <mixed-citation id="d827e1038" publication-type="other">
Queiroz, A. de, Donoghue, M.J. &amp;amp; Im, J.K. 1995. Separate
versus combined analysis of phylogenetic evidence. Annu.
Rev. Ecol. Syst. 26: 657-681.</mixed-citation>
         </ref>
         <ref id="d827e1051a1310">
            <mixed-citation id="d827e1055" publication-type="other">
Razafimandimbison, S.G. &amp;amp; Bremer, B. 2001 [2002]. Tribal
delimitation of Naucleeae (Cinchonoideae, Rubiaceae):
inference from molecular and morphological data. Syst.
Geogr. Pl. 71: 515-538.</mixed-citation>
         </ref>
         <ref id="d827e1071a1310">
            <mixed-citation id="d827e1075" publication-type="other">
Razafimandimbison, S.G. &amp;amp; Bremer, B. 2002. Phylogeny and
classification of Naucleeae s.l. (Rubiaceae) inferred from
molecular (ITS, rbcl, and trnT-F) and morphological data.
Amer. J. Bot. 89: 1027-1041.</mixed-citation>
         </ref>
         <ref id="d827e1091a1310">
            <mixed-citation id="d827e1095" publication-type="other">
Razafimandimbison, S.G., Kellogg, E.A. &amp;amp; Bremer, B. 2004.
Recent origin and phylogenetic utility of divergent ITS
putative pseudogenes: a case study from Naucleeae (Ru-
biaceae). Syst. Biol. 53: 177-192.</mixed-citation>
         </ref>
         <ref id="d827e1111a1310">
            <mixed-citation id="d827e1115" publication-type="other">
Razafimandimbison, S.G. &amp;amp; Miller, J.S. 1999. New taxa and
nomenclatural notes on the flora of the Marojejy Massif,
Madagascar. III. Rubiaceae. A new species of Sabicea.
Adansonia ser. 3, 21: 41-45.</mixed-citation>
         </ref>
         <ref id="d827e1131a1310">
            <mixed-citation id="d827e1135" publication-type="other">
Renner, S. 2004. Plant dispersal across the tropical Atlantic
by wind and sea currents. Int. J. Pl. Sci. 165 (Supplement):
S23-S33.</mixed-citation>
         </ref>
         <ref id="d827e1149a1310">
            <mixed-citation id="d827e1153" publication-type="other">
Robbrecht, E. 1988. Tropical woody Rubiaceae. Opera Bo-
tanica Belgica 1. National Botanic Garden of Belgium,
Meise.</mixed-citation>
         </ref>
         <ref id="d827e1166a1310">
            <mixed-citation id="d827e1170" publication-type="other">
Robbrecht, E. 1993. Supplement to the 1988 outline of the clas-
sification of the Rubiaceae, index to genera. Pp. 173-196
in: Robbrecht, E. (ed.), Advances in Rubiaceae Macrosys-
tematics. Opera Botanica Belgica 6. National Botanic Gar-
den of Belgium, Meise.</mixed-citation>
         </ref>
         <ref id="d827e1189a1310">
            <mixed-citation id="d827e1193" publication-type="other">
Robbrecht, E. 1996. Generic distribution patterns in subsa-
naran African Rubiaceae (Angiospermae). J. Biogeogr.
23: 311-328.</mixed-citation>
         </ref>
         <ref id="d827e1206a1310">
            <mixed-citation id="d827e1210" publication-type="other">
Robbrecht, E. &amp;amp; Manen, J.F. 2006. The major evolutionary
lineages of the coffee family (Rubiaceae, angiosperms).
Combined analysis (nrDNA and cpDNA) to infer the posi-
tion of Coptosapelta and Luculia, and supertree construc-
tion based on rbcL, rps16, trnL-trnF and atpB-rbcL data.
A new classification in two subfamilies, Cinchonoideae
and Rubioideae. Syst. Geogr. Pl. 76: 85-146.</mixed-citation>
         </ref>
         <ref id="d827e1236a1310">
            <mixed-citation id="d827e1240" publication-type="other">
Rova, J.H.E., Delprete, P.G., Andersson, L. &amp;amp; Albert, V.A.
2002. A trnL-F cpDNA sequence study of the Condam-
ineeae-Rondeletieae-Sipaneeae complex with implica-
tions on the phylogeny of the Rubiaceae. Amer. J. Bot.
89: 145-159.</mixed-citation>
         </ref>
         <ref id="d827e1259a1310">
            <mixed-citation id="d827e1263" publication-type="other">
Schumann, K. 1896 [1897]. Beiträge zur Flora von Afrika,
XIII, Rubiaceae africanae. Bot. Jahrb. Syst. 23: 412-470.</mixed-citation>
         </ref>
         <ref id="d827e1274a1310">
            <mixed-citation id="d827e1278" publication-type="other">
Simmons, M.P. &amp;amp; Ochoterena, H. 2000. Gaps as characters
in sequence-based phylogenetic analyses. Syst. Biol. 49:
369-381.</mixed-citation>
         </ref>
         <ref id="d827e1291a1310">
            <mixed-citation id="d827e1295" publication-type="other">
Steyermark, J.A. 1962. Pittierothamnus, new genus of Ru-
biaceae. Bol. Soc. Venez. Ci. Nat. 23: 92-95.</mixed-citation>
         </ref>
         <ref id="d827e1305a1310">
            <mixed-citation id="d827e1309" publication-type="other">
Steyermark, J.A. 1972. Rubiaceae. Pp. 227-832 in: Maguire,
B. &amp;amp; Collaborators, The Botany of the Guayana Highland,
Part IX. Mem. New York Bot. Gard. 23: 1-832.</mixed-citation>
         </ref>
         <ref id="d827e1322a1310">
            <mixed-citation id="d827e1326" publication-type="other">
Steyermark, J.A. 1974. Rubiaceae. Pp. 7-539 in: Lasser, T.,
Flora de Venezuela, vol. IX, primera parte. Ministerio de
Agricultura y Cría, Caracas.</mixed-citation>
         </ref>
         <ref id="d827e1339a1310">
            <mixed-citation id="d827e1343" publication-type="other">
Swofford, D.L. 2000. PAUP*: Phylogenetic Analysis Using
Parsimony (*and Other Methods), version 4.0b. Sinauer
Associates, Sunderland, Massachusetts.</mixed-citation>
         </ref>
         <ref id="d827e1356a1310">
            <mixed-citation id="d827e1360" publication-type="other">
Tate, J.A., Aguilar, J.F., Wagstaff, S. J., LaDuke, J.C., Bodo
Slotta, T.A. &amp;amp; Simpson, B.B. 2005. Phylogenetic rela-
tionships within the tribe Malveae (Malvaceae, subfamily
Malvoideae) as inferred from its sequence data. Amer. J.
Bot. 92: 584-602.</mixed-citation>
         </ref>
         <ref id="d827e1380a1310">
            <mixed-citation id="d827e1386" publication-type="other">
Thompson, J.D., Higgins, D.G. &amp;amp; Gibson, T. J. 1994. CLUS-
TAL W: improving the sensitivity of progressive multiple
sequence alignment through sequence weighting, posi-
tions-specific gap penalties and weight matrix choice.
Nucleic Acids Res. 22: 4673-4680.</mixed-citation>
         </ref>
         <ref id="d827e1405a1310">
            <mixed-citation id="d827e1409" publication-type="other">
Verdcourt, B. 1958. Remarks on the classification of the Ru-
biaceae. Bull. Jard. Bot. État Bruxelles 28: 209-314.</mixed-citation>
         </ref>
         <ref id="d827e1419a1310">
            <mixed-citation id="d827e1423" publication-type="other">
Verdcourt, B. 1975. New sectional names and a new tribe
Virectarieae (Rubiaceae). Kew Bull. 30: 366.</mixed-citation>
         </ref>
         <ref id="d827e1433a1310">
            <mixed-citation id="d827e1437" publication-type="other">
Wernham, H.F. 1914. A Monograph of the Genus Sabicea.
British Museum (Natural History), London.</mixed-citation>
         </ref>
         <ref id="d827e1447a1310">
            <mixed-citation id="d827e1451" publication-type="other">
White, F. 1979. The Guineo-Congolian region and its relation-
ships to other phytochoria. Bull. Jard. Bot. Natl. Belg. 49:
11-55.</mixed-citation>
         </ref>
         <ref id="d827e1464a1310">
            <mixed-citation id="d827e1468" publication-type="other">
White F. 1993. The AETFAT chorological classification of
Africa: history, methods and applications. Bull. Jard. Bot.
Nati. Belg. 62: 225-281.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
