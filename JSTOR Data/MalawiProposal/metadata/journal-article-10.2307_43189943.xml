<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">aejmacrecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50001042</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Economic Journal: Macroeconomics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Economic Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19457707</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19457715</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43189943</article-id>
         <title-group>
            <article-title>Government Spending Multipliers in Developing Countries: Evidence from Lending by Official Creditors</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Aart</given-names>
                  <surname>Kraay</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40124860</issue-id>
         <fpage>170</fpage>
         <lpage>208</lpage>
         <permissions>
            <copyright-statement>Copyright © 2014 American Economic Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43189943"/>
         <abstract>
            <p>I use a novel loan-level dataset covering lending by official creditors to developing country governments to construct an instrument for government spending. Loans from official creditors typically finance multiyear public spending projects, with disbursements linked to the stages of project implementation. The identification strategy exploits the long lags between approval and eventual disbursement of these loans to isolate a predetermined component of public spending associated with past loan approval decisions taken before the realization of contemporaneous shocks. In a large sample of 102 developing countries over the period 1970-2010, the one-year spending multiplier is reasonably-precisely estimated to be around 0.4.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d122e253a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d122e260" publication-type="other">
Kraay (2012),</mixed-citation>
            </p>
         </fn>
         <fn id="d122e267a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d122e274" publication-type="other">
Ramey and Shapiro (1998);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e280" publication-type="other">
Hall (2009);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e286" publication-type="other">
Fisher and Peters
(2010);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e296" publication-type="other">
Ramey (201 lb);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e302" publication-type="other">
Barro and Redlick (2011).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e308" publication-type="other">
Nakamura and Steinsson (2014)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e314" publication-type="other">
Giavazzi and McMahon (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d122e321a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d122e328" publication-type="other">
Auerbach and Gorodnichenko (2012a, 2012b)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e334" publication-type="other">
Ilzetzki, Mendoza, and Végh (2013).</mixed-citation>
            </p>
         </fn>
         <fn id="d122e341a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d122e348" publication-type="other">
Ilzetzki, Mendoza, and Vegh
(2013)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e357" publication-type="other">
Blanchard-Perotti (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e363" publication-type="other">
Ilzetzki, Mendoza, and Végh (2010),</mixed-citation>
            </p>
         </fn>
         <fn id="d122e371a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d122e378" publication-type="other">
http://web.worldbank.org/WBSITE/
EXTERNAL/PROJECTS/EXTPOLICIES/EXTOPMANUAL/0.,contentMDK:20064540~menuPK:4564187~
pagePK:64709096~piPK:64709108~theSitePK:502184,00.html.</mixed-citation>
            </p>
         </fn>
         <fn id="d122e391a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d122e398" publication-type="other">
http://siteresources.worldbank.org/DATASTATISTICS/Resources/cirs_manual.doc.</mixed-citation>
            </p>
         </fn>
         <fn id="d122e405a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d122e412" publication-type="other">
Chang, Fernandez-Arias, and Serven 2002;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e418" publication-type="other">
Dikhanov 2004;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e424" publication-type="other">
Dias, Richmond, and Wright 2011</mixed-citation>
            </p>
         </fn>
         <fn id="d122e431a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d122e438" publication-type="other">
Barro and Redlick (2011)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e444" publication-type="other">
Blanchard and Perotti (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d122e451a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d122e458" publication-type="other">
Wooldridge (2001. chapter 6.1.2).</mixed-citation>
            </p>
         </fn>
         <fn id="d122e465a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d122e472" publication-type="other">
Driscoll and Kraay (1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d122e480a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d122e487" publication-type="other">
www.worldbank.org/ida.</mixed-citation>
            </p>
         </fn>
         <fn id="d122e494a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d122e501" publication-type="other">
Kraay (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d122e508a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d122e515" publication-type="other">
Auerbach and Gorodnichenko (2012a)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d122e521" publication-type="other">
Leduc and Wilson (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d122e528a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d122e535" publication-type="other">
Ilzetzki, Mendoza, and Végh (2013)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d122e551a1310">
            <mixed-citation id="d122e555" publication-type="other">
Auerbach, Alan, and Yuriy Gorodnichenko. 2012a. "Fiscal Multipliers in Recessions and Expan-
sions." In Fiscal Policy after the Financial Crisis, edited by Alberto Alesina and Francesco Gia-
vazzi, 63-102. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d122e568a1310">
            <mixed-citation id="d122e572" publication-type="other">
Auerbach, Alan, and Yuriy Gorodnichenko. 2012b. "Measuring the Output Responses to Fiscal Pol-
icy." American Economic Journal: Economic Policy 4(1): 1-27.</mixed-citation>
         </ref>
         <ref id="d122e582a1310">
            <mixed-citation id="d122e586" publication-type="other">
Barro, Robert J. 1981. "Output Effects of Government Purchases." Journal of Political Economy
89 (6): 1086-1121.</mixed-citation>
         </ref>
         <ref id="d122e596a1310">
            <mixed-citation id="d122e600" publication-type="other">
Barro, Robert J., and Charles J. Redlick. 2011. "Macroeconomic Effects from Government Purchases
and Taxes." Quarterly Journal of Economics 126 (1): 51-102.</mixed-citation>
         </ref>
         <ref id="d122e611a1310">
            <mixed-citation id="d122e615" publication-type="other">
Blanchard, Olivier, and Roberto Perotti. 2002. "An Empirical Characterization of the Dynamic Effects
of Changes in Government Spending and Taxes on Output." Quarterly Journal of Economics
117(4): 1329-68.</mixed-citation>
         </ref>
         <ref id="d122e628a1310">
            <mixed-citation id="d122e632" publication-type="other">
Chang, Charles, Eduardo Fernandez-Arias, and Luis Serven. 2002. "Measuring Aid Flows: A New
Approach." Global Economy Journal 3 (2-4): 197-218.</mixed-citation>
         </ref>
         <ref id="d122e642a1310">
            <mixed-citation id="d122e646" publication-type="other">
Chodorow-Reich, Gabriel, Laura Feiveson, Zachary Liscow, and William Gui Woolston. 2012. "Does
State Fiscal Relief during Recessions Raise Employment? Evidence from the American Recovery
and Reinvestment Act." American Economic Journal: Economic Policy 4 (3): 118-45.</mixed-citation>
         </ref>
         <ref id="d122e659a1310">
            <mixed-citation id="d122e663" publication-type="other">
Clemens, Jeffrey, and Stephen Miran. 2012. "Fiscal Policy Multipliers on Subnational Government
Spending." American Economic Journal: Economic Policy 4 (2): 46-68.</mixed-citation>
         </ref>
         <ref id="d122e673a1310">
            <mixed-citation id="d122e677" publication-type="other">
Clements, Benedict, Sanjeev Gupta, and Masahiro Nozaki. 2011. What Happens to Social Spending
in IMF-Supported Programs?" International Monetary Fund (IMF) Staff Discussion Note 11/15.</mixed-citation>
         </ref>
         <ref id="d122e687a1310">
            <mixed-citation id="d122e691" publication-type="other">
Cohen, Lauren, Joshua D. Covali, and Christopher Malloy. 2010. "Do Powerful Politicians Cause Cor-
porate Downsizing?" National Bureau of Economic (NBER) Working Paper 15839.</mixed-citation>
         </ref>
         <ref id="d122e702a1310">
            <mixed-citation id="d122e706" publication-type="other">
Dias, Daniel A., Christine J. Richmond, and Mark L. J. Wright. 2011. "The Stock of External Sover-
eign Debt: Can We Take the Data At 'Face Value'?" National Bureau of Economic (NBER) Work-
ing Paper 17551.</mixed-citation>
         </ref>
         <ref id="d122e719a1310">
            <mixed-citation id="d122e723" publication-type="other">
Dikhanov, Yuri. 2004. "Historical PV of Debt in Developing Countries: 1980-2002." Unpublished.</mixed-citation>
         </ref>
         <ref id="d122e730a1310">
            <mixed-citation id="d122e734" publication-type="other">
Driscoll, John C., and Aart C. Kraay. 1998. "Consistent Covariance Matrix Estimation with Spatially
Dependent Panel Data." Review of Economics and Statistics 80 (4): 549-60.</mixed-citation>
         </ref>
         <ref id="d122e744a1310">
            <mixed-citation id="d122e748" publication-type="other">
Fishback, Price V., and Valentina Kachanovskaya. 2010. "In Search of the Multiplier for Federal
Spending on the States During the Great Depression." National Bureau of Economic (NBER)
Working Paper 16561.</mixed-citation>
         </ref>
         <ref id="d122e761a1310">
            <mixed-citation id="d122e765" publication-type="other">
Fisher, Jonas D. M., and Ryan Peters. 2010. "Using Stock Returns to Identify Government Spending
Shocks." Economic Journal 120 (544): 414-36.</mixed-citation>
         </ref>
         <ref id="d122e775a1310">
            <mixed-citation id="d122e779" publication-type="other">
Giavazzi, Francesco, and Michael McMahon. 2012. "The Household Effects of Government Con-
sumption." National Bureau of Economic (NBER) Working Paper 17837.</mixed-citation>
         </ref>
         <ref id="d122e790a1310">
            <mixed-citation id="d122e794" publication-type="other">
Hadi, Ali S. 1992. "Identifying Multiple Outliers in Multivariate Data." Journal of the Royal Statisti-
cal Society 54 (3): 761-71.</mixed-citation>
         </ref>
         <ref id="d122e804a1310">
            <mixed-citation id="d122e808" publication-type="other">
Hall, Robert E. 2009. "By How Much Does GDP Rise if the Government Buys More Output?" Brook-
ings Papers on Economic Activity 39 (2): 183-250.</mixed-citation>
         </ref>
         <ref id="d122e818a1310">
            <mixed-citation id="d122e822" publication-type="other">
Ilzetzki, Ethan, Enrique Mendoza, and Carlos Végh. 2013. "How Big (Small?) Are Fiscal Multipli-
ers?" Journal of Monetary Economics 60 (2): 239-54.</mixed-citation>
         </ref>
         <ref id="d122e832a1310">
            <mixed-citation id="d122e836" publication-type="other">
Ilzetzki, Ethan, Carmen M. Reinhart, and Kenneth S. Rogoff. 2008. Exchange Rate Arrangements
Entering the 21st Century: Which Anchor Will Hold?" Unpublished.</mixed-citation>
         </ref>
         <ref id="d122e846a1310">
            <mixed-citation id="d122e850" publication-type="other">
Jordà, Oscar. 2005. "Estimation and Inference of Impulse Responses by Local Projections." American
Economic Review 95 (1): 161-82.</mixed-citation>
         </ref>
         <ref id="d122e860a1310">
            <mixed-citation id="d122e864" publication-type="other">
Kraay, Aart. 2012. "How Large Is the Government Spending Multiplier? Evidence from World Bank
Lending." Quarterly Journal of Economics 127 (2): 829-87.</mixed-citation>
         </ref>
         <ref id="d122e875a1310">
            <mixed-citation id="d122e879" publication-type="other">
Kraay, Aart. 2014. "Government Spending Multipliers in Developing Countries: Evidence from Lend-
ing by Official Creditors: Dataset." American Economic Journal: Macroeconomics, http://dx.doi.
org/10. 1257/mac.6.4. 170</mixed-citation>
         </ref>
         <ref id="d122e892a1310">
            <mixed-citation id="d122e896" publication-type="other">
Leduc, Sylvain, and Daniel Wilson. 2012. "Roads to Prosperity or Bridges to Nowhere? Theory and
Evidence on the Impact of Public Infrastructure Investment." In National Bureau of Economic
Research Macroeconomics Annual 2012, edited by Daron Acemoglu, Jonathan Parker, and Michael
Woodford, 89-142. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d122e912a1310">
            <mixed-citation id="d122e916" publication-type="other">
Leeper, Eric M., Todd B. Walker, and Shu-Chun S. Yang. 2010. "Government Investment and Fiscal
Stimulus." Journal of Monetary Economics 57 (8): 1000-1012.</mixed-citation>
         </ref>
         <ref id="d122e926a1310">
            <mixed-citation id="d122e930" publication-type="other">
Moreira, Marcelo J. 2003. "A Conditional Likelihood Ratio Test for Structural Models." Economet-
rica 71 (4): 1027-48.</mixed-citation>
         </ref>
         <ref id="d122e940a1310">
            <mixed-citation id="d122e944" publication-type="other">
Nakamura, Emi, and Jón Steinsson. 2014. "Fiscal Stimulus in a Monetary Union." American Eco-
nomic Review 104 (3): 753-92.</mixed-citation>
         </ref>
         <ref id="d122e954a1310">
            <mixed-citation id="d122e958" publication-type="other">
Ramey, Valerie, 2011a. "Can Government Purchases Stimulate the Economy?" Journal of Economic
Literature 49 (3): 673-85.</mixed-citation>
         </ref>
         <ref id="d122e969a1310">
            <mixed-citation id="d122e973" publication-type="other">
Ramey, Valerie. 2011b. "Identifying Government Spending Shocks: It's all in the Timing." Quarterly
Journal of Economics 126 (1): 1-50.</mixed-citation>
         </ref>
         <ref id="d122e983a1310">
            <mixed-citation id="d122e987" publication-type="other">
Ramey, Valerie A., and Matthew D. Shapiro. 1998. "Costly Capital Reallocation and the Effects of
Government Spending." Carnegie-Rochester Conference Series on Public Policy 48 (1): 145-94.</mixed-citation>
         </ref>
         <ref id="d122e997a1310">
            <mixed-citation id="d122e1001" publication-type="other">
Serrato, Juan Carlos Suárez, and Philippe Wingender. 2011. "Estimating Local Fiscal Multipliers."
Unpublished.</mixed-citation>
         </ref>
         <ref id="d122e1011a1310">
            <mixed-citation id="d122e1015" publication-type="other">
Shoag, Daniel. 2010. "The Impact of Government Spending Shocks: Evidence on the Multiplier from
State Pension Plans." http://www.people.fas.harvard.edu/~shoag/papers_files/shoagJmp.pdf.</mixed-citation>
         </ref>
         <ref id="d122e1025a1310">
            <mixed-citation id="d122e1029" publication-type="other">
Staiger, Douglas, and James H. Stock. 1997. "Instrumental Variables Regression with Weak Instru-
ments." Econometrica 65 (3): 557-86.</mixed-citation>
         </ref>
         <ref id="d122e1039a1310">
            <mixed-citation id="d122e1043" publication-type="other">
Stock, James, and Motohiro Yogo. 2005. "Testing for Weak Instruments in Linear IV Regressions."
In Identification and Inference for Econometric Models: Essays in Honor of Thomas Rothenberg ,
edited by Donald W. K. Andrews and James H. Stock, 80-108. Cambridge: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d122e1060a1310">
            <mixed-citation id="d122e1064" publication-type="other">
Wilson, Daniel. 2012. "Fiscal Spending Jobs Multipliers: Evidence from the 2009 American Recovery
and Reinvestment Act." American Economic Journal : Economic Policy 4 (3): 251-82.</mixed-citation>
         </ref>
         <ref id="d122e1074a1310">
            <mixed-citation id="d122e1078" publication-type="other">
Wooldridge, Jeffrey. 2001. Econometric Analysis of Cross Section and Panel Data. Cambridge: MIT
Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
