<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt9qgsfc</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Population ageing and international development</book-title>
         <subtitle>From generalisation to evidence</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Lloyd-Sherlock</surname>
               <given-names>Peter</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>01</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9781847421920</isbn>
      <isbn content-type="epub">9781847421944</isbn>
      <publisher>
         <publisher-name>Policy Press</publisher-name>
         <publisher-loc>Bristol, UK; Portland, OR, USA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>The Policy Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt9qgsfc"/>
      <abstract abstract-type="short">
         <p>Over the next 40 years the number of people aged 60+ in the world, many of whom live in developing regions, will grow by 1¼ billion. What will old age be like for them? This original book provides an analysis of links between development, population ageing and older people, challenging some widely held misconceptions. It highlights the complexity of international experiences and argues that the effects of population ageing on development are influenced by policy choices. The book will be of interest to a range of academic disciplines, including economics, gerontology, social policy and development studies as well as policy-makers and practitioners concerned with developing countries.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.3</book-part-id>
                  <title-group>
                     <title>List of figures and tables</title>
                  </title-group>
                  <fpage>iv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.4</book-part-id>
                  <title-group>
                     <title>List of abbreviations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.5</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.6</book-part-id>
                  <title-group>
                     <title>Notes on the author</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.7</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>xi</fpage>
                  <abstract>
                     <p>The number of people aged 60 and over is projected to increase by one and a quarter billion between 2010 and 2050, reaching 22% of the world’s total population. Of these, 81% will be living in Asia, Africa, Latin America or the Caribbean (United Nations Population Division, 2008). What will this mean for the world and what will old age mean for these people?</p>
                     <p>In April 2009, as part of a larger study, I interviewed two older women living on the same street in Cape Town, South Africa. Both were aged in their early sixties and both received a basic</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.8</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>International development and population ageing</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Population ageing is now recognised as a major issue for international development; at the same time, processes associated with development strongly influence how later life is experienced around the world. This chapter explores relationships between development, demographic change and population ageing. It begins by briefly outlining what is meant by ‘development’, drawing attention to the concept’s complexity and differences in interpretation. The chapter goes onto explore links between development and population change, identifying different scenarios with particular implications for population ageing. The second half of the chapter reviews current knowledge about how population ageing may feed back into development. This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.9</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>Experiencing later life in contexts of development</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>Chapter One looked at population ageing as an intrinsic part of development and considered how it will influence future development experiences. This chapter examines how changes associated with development affect the lives of older people. These relationships are complex and diverse, since patterns of development are variable and older people are a heterogeneous group. Chapter One showed that countries do not all follow the same pathways of development and modernisation. For example, patterns of fertility transition have been highly variable. At the same time, people will not all be affected by the same changes in the same ways. For example,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.10</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>Older people, pensions and development</title>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>Judging by the outputs of academics, policy makers and NGOs, pensions are by far the most important issue affecting the lives of older people in the developing world. For example, between 1984 and 2004, the World Bank issued over 200 loans and 350 papers on pension policy, but provided no loans or papers for other projects explicitly concerned with older people (Bretton Woods Project, 2006). This focus on pensions has dwarfed the amount of attention paid to issues such as health policy or the care economy. Much of the discussion about pension schemes is complex, full of jargon and deeply</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.11</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Population ageing and health</title>
                  </title-group>
                  <fpage>91</fpage>
                  <abstract>
                     <p>Experiences of health and illness are central to older people’s wellbeing and influence how population ageing affects development. Good health may enable older people to continue in employment, facilitate their social networks and enhance their economic and emotional resilience. An old age characterised by high rates of disease and illness increases the potential economic and social ‘burden’ of older people and reduces their quality of life. While the risk of some diseases increases with old age, overall patterns of health vary markedly across older populations. For example, a 60-year-old woman in Japan can expect on average another 22 years of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.12</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>Later life and social relations:</title>
                     <subtitle>family, migration and care</subtitle>
                  </title-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>Informal networks and social relations are central to the wellbeing of all age groups, but are particularly important for older people and young children. As societies undergo modernisation and development, the nature of these social relations will shift, sometimes abruptly. These changes have profound consequences for the lives of individuals and they also feed back into wider development. For example, China’s dramatic fertility decline since the 1980s has reshaped families and this will have complex and substantial impacts on that country’s future. Chapter Two introduced the idea that modernisation is usually harmful to older people since, among other things, it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.13</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>Ageing and development in South Africa</title>
                  </title-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>This chapter provides the first of three country case studies, which complement the thematic chapters in the first part of the book. It begins with a general review of South Africa’s long-run social and economic development, making particular reference to aspects that shape the lives of older people living there today. Then, the country’s main demographic trends are examined. Population ageing is set in a wider context of fertility transition, as well as shifts in life expectancy, living arrangements and the impact of the AIDS epidemic. Predictably, this reveals large disparities between different racial groups. The chapter then turns to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.14</book-part-id>
                  <title-group>
                     <label>SEVEN</label>
                     <title>Ageing and development in Argentina</title>
                  </title-group>
                  <fpage>171</fpage>
                  <abstract>
                     <p>Argentina is a relatively wealthy country, with an embracing welfare system and an aged population structure. As such, it may offer insights relevant to the future for other developing countries. Despite this, it is widely accepted that Argentina’s economic performance over the past 70 years has been poor and erratic. The causes of this decline are complex, but this chapter will seek to assess whether population ageing has played any role in hindering growth. The chapter will also consider how this gradual decline has affected experiences of ageing and the lives of older people. It follows broadly the same structure</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.15</book-part-id>
                  <title-group>
                     <label>EIGHT</label>
                     <title>Ageing and development in India</title>
                  </title-group>
                  <fpage>201</fpage>
                  <abstract>
                     <p>The selection of India for a case study can be justified purely in terms of its large population, which reached one billion in 2000. Although population ageing in India is less advanced than in countries such as Argentina (7% of Indians were aged 60 or more in 2005), the country’s sheer size means that nearly an eighth of the world’s population aged 60 and over live there. As well as scale, India presents deep-seated and complex patterns of diversity. These can be seen in terms of large development disparities between rural and urban districts and between the different states. They</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.16</book-part-id>
                  <title-group>
                     <label>NINE</label>
                     <title>Conclusions and overview</title>
                  </title-group>
                  <fpage>231</fpage>
                  <abstract>
                     <p>This final chapter begins with some comparisons across the three country case studies. It then highlights some of the main themes to emerge from this book and identifies key priorities for policy makers.</p>
                     <p>The central theme of this book is the danger of generalisation: be it about processes of development, patterns of population change or the lives of older people. The varied experiences of the three study countries highlight this point. Table 9.1 shows that each country experienced a unique trajectory of fertility transition and population ageing. In the case of Argentina, this had set in well before 1960, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.17</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>237</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>277</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qgsfc.19</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>289</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
