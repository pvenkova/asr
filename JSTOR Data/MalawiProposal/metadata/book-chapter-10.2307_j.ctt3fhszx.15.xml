<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt24h1dk</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt3fhszx</book-id>
      <subj-group>
         <subject content-type="call-number">JC571.F628 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Political science</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>A Force Profonde</book-title>
         <subtitle>The Power, Politics, and Promise of Human Rights</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>Kolodziej</surname>
               <given-names>Edward A.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>01</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="ppub">9780812237276</isbn>
      <isbn content-type="epub">9780812202502</isbn>
      <publisher>
         <publisher-name>University of Pennsylvania Press, Inc.</publisher-name>
         <publisher-loc>Philadelphia</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2003</copyright-year>
         <copyright-holder>University of Pennsylvania Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt3fhszx"/>
      <abstract abstract-type="short">
         <p>Presenting detailed portraits by leading authorities of the politics of human rights across the major regions of the globe,<italic>A Force Profonde: The Power, Politics, and Promise of Human Rights</italic>reveals human rights to be a force as powerful as capitalist markets and technological innovation in shaping global governance. Human rights issues mobilize populations regardless of their national, ethnic, cultural, or religious differences. Yet progress in advancing human rights globally, as Edward A. Kolodziej and the other contributors to the volume contend, depends decisively on the local support and the efforts of the diverse and divided peoples of the world-a prerequisite that remains problematic in many parts of the globe.<italic>A Force Profonde</italic>explores conceptions of human rights from Western as well as other major world traditions in an attempt to dispel the notion that tyranny, culture, and religion are the only challenges to human rights. Focusing on regional patterns of conflict, the authors point out that violations often have to do with disputes over class, social status, economic privilege, and personal power. In addition, they contend that conflicts over identity are more prevalent in the West than commonly thought. Sharply conflicting views are to be found between the European Union and the United States over issues like the death penalty. Splits within the West between rival Christian sects and between religious adherents and partisans of secularization are no less profound than those in other regions.</p>
      </abstract>
      <counts>
         <page-count count="352"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.3</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>A<italic>Force Profonde</italic>:</title>
                     <subtitle>The Power, Politics, and Promise of Human Rights</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kolodziej</surname>
                           <given-names>Edward A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Chou En-lai was allegedly asked what he thought were the results of the French Revolution. He supposedly replied: “It’s too early to tell.” Adam Hochschild’s prize-winning study of King Leopold’s plundering of the Belgian Congo’s rubber and ivory at the cost of countless native lives makes the same point. In tracing the careers of the long-forgotten reformers who exposed Leopold’s depredations, Hochschild (1999, 306) linked their moral lineage as revolutionaries to the tradition of “the French Revolution and beyond.” They selflessly dedicated their lives and fortunes to the human rights of people they scarcely knew. Their example continues to be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.4</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Western Perspectives</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Donnelly</surname>
                           <given-names>Jack</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>31</fpage>
                  <abstract abstract-type="extract">
                     <p>To talk intelligently about something as vast and varied as “the West” is virtually impossible, even on the relatively narrow topic of the place of human rights in dominant conceptions of political legitimacy. Politically, the West has been classically embodied in Sparta, Athens, and Rome, both the Republic and the Empire; the France of Louis IX, Francis I, Louis XIV, Robespierre, Napoleon, Louis Napoleon, the Third Republic, the Popular Front, Petain, and de Gaulle; the Germany of Emperor Frederick III, the Great Elector Frederick William, Frederick the Great, Kaiser Wilhelm II, Adolf Hitler, Willy Brandt, and Helmut Kohl; the England</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.5</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Muslim Perspectives</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hoffman</surname>
                           <given-names>Valerie J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>45</fpage>
                  <abstract abstract-type="extract">
                     <p>There is no single Muslim perspective on the topic of human rights. Although virtually all Muslims believe that Islam guarantees human rights, there is a great deal of disagreement on specifics. Many Muslims believe that there is no incompatibility between Western notions of human rights and Islam; some even argue for an Islamic derivation of Western human rights concepts. Islam has also often provided the idiom in which Muslims have expressed their demands for human rights in the face of government oppression. On the other hand, Islamization programs instituted by governments have often involved human rights violations that have been</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.6</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>The Northern Tier</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Weinbaum</surname>
                           <given-names>Marvin G.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>71</fpage>
                  <abstract abstract-type="extract">
                     <p>There is much to distinguish among the three countries of the northern tier of the Middle East. The governments of Turkey, Iran, and Afghanistan draw on different sources for their legitimacy. Despite predominantly Muslim populations, the three societies are varied in the ideologies that guide their leaders and inspire their publics. They have distinctive problems in sustaining domestic order and providing for national security. The very different economic assets of the three also pose separate challenges in providing for the well-being and enlightenment of their citizens. Yet among the features they share is that each confronts formidable issues of human</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.7</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>North Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Arfi</surname>
                           <given-names>Badredine</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>91</fpage>
                  <abstract abstract-type="extract">
                     <p>All the North African states—Morocco, Algeria, Tunisia, and Libya—have been confronted since independence with a lack of popular legitimacy. In the 1980s, domestic political debates about human rights slowly became an integral part of a seemingly never-ending process of state legitimation. The agenda of human rights has thence evolved as a constitutive element of the ongoing contest about state legitimation. The politics of human rights in North Africa has assumed the character of a manipulation of history, culture, and Islam to shape the balance of social—symbolic, economic, and political-institutional—power between the state and societal actors as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.8</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>The Middle East:</title>
                     <subtitle>Israel</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Peleg</surname>
                           <given-names>Ilan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>113</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter is based on two main theoretical constructs. The first is the argument that the long-term stability of a political regime depends, to a large extent, on its ability to generate social legitimacy. The widespread acceptance of the regime’s fundamental values is a key for the ability of the polity to maintain its governance patterns; the increasing rejection of those fundamental values by significant segments of the population is likely to generate revisions in those governance patterns. Put differently, erosion in social legitimacy results in increased instability.</p>
                     <p>The second theoretical claim is that in today’s world human and civil</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.9</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Northeast Asia:</title>
                     <subtitle>China</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Friedman</surname>
                           <given-names>Edward</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>128</fpage>
                  <abstract abstract-type="extract">
                     <p>How China, a permanent member of the United Nations Security Council, acts on human rights is a matter of global import. A nuclear power with ambitions for regional predominance, China is home to some one-fifth of the human race. Since 1978 China has also enjoyed the world’s fastest economic growth, receiving far and away the most Foreign Direct Investment (FDI) of all developing nations.</p>
                     <p>Since 1993 China has thrown its clout against the human rights movement. This chapter clarifies why China has been successful in opposing human rights and identifies political factors that might lead Beijing to support human rights.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.10</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>South Asia</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kochanek</surname>
                           <given-names>Stanley A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>144</fpage>
                  <abstract abstract-type="extract">
                     <p>The problems of democracy, legitimacy, and human rights in South Asia are inextricably intertwined with questions of nationalism, national integration, economic development, globalization, and political, cultural, and social change. South Asia is a highly diverse, relatively self-contained geographic region that accounts for one quarter of the world’s population. South Asia is divided into seven states—India, Pakistan, Bangladesh, Sri Lanka, Nepal, Bhutan, and the Maldives—that vary considerably in size, population, level of development, and power, as seen in Tables 1 and 2.¹ The population of the region is largely rural, religiously, linguistically, and ethnically diverse and is among the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.11</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>Southeast Asia</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Neher</surname>
                           <given-names>Clark D.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>163</fpage>
                  <abstract abstract-type="extract">
                     <p>Human rights have become important to Southeast Asian citizens and their leaders because the Western world, in particular the United States, has argued that modern nations must adhere to certain principles. Western leaders believe that, because the world is increasingly interdependent politically and economically, universal values of human rights must be followed by nations that expect to be viewed as democratic and modern. Modern technology has shrunk the world. A global society has emerged that is led by a “world culture” that is predominantly Western. Since the end of the Cold War, for example, free enterprise has emerged as the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.12</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>The European Union</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Newman</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>182</fpage>
                  <abstract abstract-type="extract">
                     <p>The founding treaty of the European Economic Community, signed by the original six member states in Rome in 1957, did not mention human rights. Forty years later, a new treaty agreed to by the current fifteen member states in Amsterdam, included specific clauses on the subject as conditions of membership for the European Union. And in Cologne in June 1999 they decided that it was time to establish a Charter of Fundamental Rights. The task of elaborating this charter was then entrusted to a convention, and the European Council at Nice adopted a final draft in December 2000.</p>
                     <p>The objectives</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.13</book-part-id>
                  <title-group>
                     <label>Chapter 11</label>
                     <title>Eastern Europe:</title>
                     <subtitle>The Russian Federation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Leff</surname>
                           <given-names>Carol Skalnik</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>198</fpage>
                  <abstract abstract-type="extract">
                     <p>Human rights politics play out within a complex web of domestic and transnational relationships. As Thomas Risse-Kappen (1994) says, “ideas do not float freely”; they are encapsulated in normative regimes that are incorporated into institutional structures and championed by concrete actors embedded in political networks. An examination of these actors and institutional structures is critical to explaining the outcomes of Russian human rights battles after communism; these battles have been fought in a transnational setting in which the domestic and foreign linkages must be specified.</p>
                     <p>Before tackling this task, it is sensible to ask what is meant by a human</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.14</book-part-id>
                  <title-group>
                     <label>Chapter 12</label>
                     <title>Latin America</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wiarda</surname>
                           <given-names>Howard J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>220</fpage>
                  <abstract abstract-type="extract">
                     <p>Latin America has always fit awkwardly into the prevailing paradigms that scholars and policy officials have used to categorize the area (Martz 1966; P. Smith 1995). Try answering these questions unambiguously: Is Latin America Western or, with its large indigenous communities, non-Western? Is it developed or developing, traditional or modern, and as compared to what, the United States or Africa? Is it Third World or, with its recent impressive economic growth, First World, or must we further disaggregate by country or area? Is it authoritarian, democratic, or “illiberal democracy,” in Larry Diamond’s (1999a) phrase? Is it capitalist, mercantilist, or some</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.15</book-part-id>
                  <title-group>
                     <label>Chapter 13</label>
                     <title>Southern Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mattes</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Leysens</surname>
                           <given-names>Anthony</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>238</fpage>
                  <abstract abstract-type="extract">
                     <p>The sweep of democracy’s “third wave” through southern Africa in the 1990s is testimony to the increasing demands for and claims to human rights made by citizens and groups across this region.¹ To be sure, we cannot account for this trend without acknowledging the role of global forces such as the end of the Cold War and the increasing emphases on open markets and good governance by international financial institutions and donors. Few of the forty transitions from authoritarian rule in sub-Saharan Africa would have even commenced, let alone succeeded, without the end of the bipolar world, the powerful exemplars</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.16</book-part-id>
                  <title-group>
                     <label>Chapter 14</label>
                     <title>West Africa:</title>
                     <subtitle>Nigeria</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zartman</surname>
                           <given-names>I. William</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>260</fpage>
                  <abstract abstract-type="extract">
                     <p>The thesis of this chapter is that Africa has undergone—and presumably continues to do so—waves of democratization for its own reasons, perhaps related but not dependent on the putative waves on a global scale (Huntington 1997). In that process, a key state such as Nigeria has an influential role. But that role is part of the wave effect, not its motor. The causes lie elsewhere, in the nature of the waves themselves within African events and within the logic of their own effects. The essay will explore and develop these assertions.</p>
                     <p>The term <italic>democratization</italic> has been used in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.17</book-part-id>
                  <title-group>
                     <label>Chapter 15</label>
                     <title>Whither Human Rights?</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kolodziej</surname>
                           <given-names>Edward A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>277</fpage>
                  <abstract abstract-type="extract">
                     <p>Some brief stocktaking is useful at this point to sum up where we are. Based largely (but not exclusively) on the findings of this volume, I first hazard some observations about where the power, politics, and promise of human rights are today. This will not be easy. The scope of the discussion and the richness, depth, and complexity of the preceding chapters caution against sweeping generalizations that fail to account for different and contradictory patterns of human rights struggles across the regions of the world. This brief survey is viewed from an imaginary point in conceptual space that plots how</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.18</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>293</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.19</book-part-id>
                  <title-group>
                     <title>References Cited</title>
                  </title-group>
                  <fpage>299</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.20</book-part-id>
                  <title-group>
                     <title>List of Contributors</title>
                  </title-group>
                  <fpage>317</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.21</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>319</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt3fhszx.22</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>339</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
