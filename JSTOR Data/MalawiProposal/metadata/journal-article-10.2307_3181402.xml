<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jafricultstud</journal-id>
         <journal-id journal-id-type="jstor">j100921</journal-id>
         <journal-title-group>
            <journal-title>Journal of African Cultural Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Taylor &amp; Francis Ltd</publisher-name>
         </publisher>
         <issn pub-type="ppub">13696815</issn>
         <issn pub-type="epub">14699346</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3181402</article-id>
         <article-categories>
            <subj-group>
               <subject>Part 1: Sources and Methods</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Life and Technology in Everyday Life: Reflections on the Career of Mzee Stefano, Master Smelter in Ufipa, Tanzania</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Marcia</given-names>
                  <surname>Wright</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i359353</issue-id>
         <fpage>17</fpage>
         <lpage>34</lpage>
         <page-range>17-34</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 Journal of African Cultural Studies</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3181402"/>
         <abstract>
            <p> The life history and career of one master iron smelter, Mzee Stefano Malimbo of Ufipa in southwestern Tanzania, serves as the base for a retrieval of social history in the twentieth century. By paying special attention to the persistence of smelting, signaled by the reconstruction of his furnaces in 1936, this article explores the demand for the distinctive, large Fipa hoe in the light of economic activities at a time which, while still economically depressed in a global sense, was locally buoyant. The resulting perspective on everyday life calls for a fuller consideration of the ethnographic record of smelting as observed in its heyday and in subsequent demonstrations. A critical rereading of the documentation for smelting communities indicates that the prevailing culture and beliefs must be understood in the light of the dynamics of change in the social and economic context, the values of the community supporting smelting, and the ritual and performative aspects of the drama of transforming earth into iron. To restore smelting to everyday life is to accept multiple identities. The ethno-archaeologists spearheaded by Peter Schmidt must consequently be challenged on a number of scores, including their erasure of individuals whom they subsume into categories of craftsmen and ritualists rather than regard as men with choices and activities beyond iron-working. Mzee Stefano belonged to the generation that saw Ufipa become overwhelmingly Catholic. Reaching the peak of his life in the 1930s, he was not only a smelter observing the traditional rituals of smelting, he was also an appointed sub-chief, blacksmith, farmer, and Catholic in good standing. The practice of Alltagsgeschiċhte must be rigorously critical of evidence and confront theoretical over-determination as need be if it is to reach its ultimate goal of conveying historically informed empathy. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1240e144a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1240e151" publication-type="other">
Schmidt (1996a: Introduction)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e157" publication-type="other">
Schmidt and Patterson (1997)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e164a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1240e171" publication-type="other">
Lemelle
(1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e180" publication-type="other">
1996a: 17</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e187a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1240e194" publication-type="other">
1997.
Personal communication, 7 August 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e204a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1240e211" publication-type="other">
D.D.Y[onge]., 1 March 1957</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e219a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1240e226" publication-type="other">
Lemelle
(1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e235" publication-type="other">
D. P. Collett (1993)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e242a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1240e249" publication-type="other">
Law (1987: 233-4)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e256a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1240e263" publication-type="other">
Arnoldi et al. (1996: introduction)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e270a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1240e277" publication-type="other">
Beidelman (1986)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e283" publication-type="other">
Baumann (1944)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e290a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1240e297" publication-type="other">
Knight 1974</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e304a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1240e311" publication-type="other">
'Iron Smelting in Ufipa' (1967)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e319a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1240e326" publication-type="other">
Bury 1983</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e333a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1240e340" publication-type="other">
Wise 1958a/b</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e346" publication-type="other">
'Iron Making in Ufipa' (1967)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e353a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1240e360" publication-type="other">
Zambia National Archives (hereafter: ZNA)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e367a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1240e374" publication-type="other">
Herbert (1993)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e381a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1240e388" publication-type="other">
Village Lists,
Inclosure 8 in No. 1. Nyasa-Tanganyika Boundary Report, 1898. Public Record Office, London,
FO 881/7116, p. 11</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e401a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1240e408" publication-type="other">
Bury (1983:
286ff.)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e419a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1240e426" publication-type="other">
Watson (1958)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e433a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1240e440" publication-type="other">
Willis (1981: 27ff)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e447a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1240e454" publication-type="other">
Stefano interview 29 October 1982</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e460" publication-type="other">
Wright (1989)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e467a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1240e474" publication-type="other">
Sumbawanga, 2 November 1982</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e481a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1240e488" publication-type="other">
Stefano, interview with A. Mazombwe, 11 March 1984.</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e495a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1240e502" publication-type="other">
SEC 2/303
Abercorn [Mbala] District Reports and SEC 2/819 District Tour Reports, Abercorn, 1932-33.
ZNA</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e516a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1240e523" publication-type="other">
Kjekshus 1977: 89</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1240e529" publication-type="other">
Killick (1990: ch. 5)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e536a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1240e543" publication-type="other">
Lechaptois (d. 1917)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e550a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1240e557" publication-type="other">
30 September and 2, 10, 12 October 1918. APB</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e564a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1240e571" publication-type="other">
Robert 1949: 5</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e578a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1240e585" publication-type="other">
Andrea and Stefano, 26 Oct. 1982, 2 Nov. 1982</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e592a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1240e599" publication-type="other">
D. D. Yonge, 1 March 1957. Sumbawanga District Book. (TNA)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e607a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1240e614" publication-type="other">
Sutton (1985: 170)</mixed-citation>
            </p>
         </fn>
         <fn id="d1240e621a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1240e628" publication-type="other">
Schmidt (1997: 237)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1240e644a1310">
            <mixed-citation id="d1240e648" publication-type="book">
Arnoldi, Mary Jo, Christraud Geary and Kris Hardin (eds.). 1996. African Material Culture.
Bloomington: Indiana University Press.<person-group>
                  <string-name>
                     <surname>Arnoldi</surname>
                  </string-name>
               </person-group>
               <source>African Material Culture</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e673a1310">
            <mixed-citation id="d1240e677" publication-type="journal">
Barndon, Randi. 1996. Fipa ironworking and its technological style. In Schmidt (1996a), pp. 58-
73.<person-group>
                  <string-name>
                     <surname>Barndon</surname>
                  </string-name>
               </person-group>
               <fpage>58</fpage>
               <source>Fipa ironworking and its technological style</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e706a1310">
            <mixed-citation id="d1240e710" publication-type="journal">
Baumann, Hermann. 1928. The division of work according to sex in African hoe culture. Africa 1
(3): 289-319.<object-id pub-id-type="doi">10.2307/1155633</object-id>
               <fpage>289</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1240e726a1310">
            <mixed-citation id="d1240e730" publication-type="journal">
---. 1944. Zur Morphologie des afrikanischen Ackergerites. Koloniale Vilkerkunde I (Wiener
Beitriige zur Kulturgeschichte und Linguistik) 6: 192-322.<person-group>
                  <string-name>
                     <surname>Baumann</surname>
                  </string-name>
               </person-group>
               <fpage>192</fpage>
               <volume>6</volume>
               <source>Koloniale Vilkerkunde I (Wiener Beitriige zur Kulturgeschichte und Linguistik)</source>
               <year>1944</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e763a1310">
            <mixed-citation id="d1240e767" publication-type="journal">
Beidelman, T. O. 1986. On J. Zwernemann's 'Culture History and African Anthropology'.
Anthropos 81: 661-671.<person-group>
                  <string-name>
                     <surname>Beidelman</surname>
                  </string-name>
               </person-group>
               <fpage>661</fpage>
               <volume>81</volume>
               <source>Anthropos</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e799a1310">
            <mixed-citation id="d1240e803" publication-type="book">
Boserup, Ester. 1970. Woman's Role in Economic Development. New York: St. Martin's Press.<person-group>
                  <string-name>
                     <surname>Boserup</surname>
                  </string-name>
               </person-group>
               <source>Woman's Role in Economic Development</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e825a1310">
            <mixed-citation id="d1240e829" publication-type="other">
Bury, Barbara. 1983. The Human Ecology and Political Economy of Agricultural Production on
the Ufipa Plateau, Tanzania, 1945-1981. Unpublished Ph.D. dissertation, Columbia University.</mixed-citation>
         </ref>
         <ref id="d1240e839a1310">
            <mixed-citation id="d1240e843" publication-type="book">
Collett, D. P. 1993. Metaphors and representations associated with precolonial iron-smelting in
eastern and southern Africa. In The Archaeology of Africa: Food, Metals and Towns, ed. by
Thurstan Shaw, Paul Sinclair, Bassey Andah and Alex Okpoko, pp. 499-511. (One World
Archaeology). New York: Routledge.<person-group>
                  <string-name>
                     <surname>Collett</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Metaphors and representations associated with precolonial iron-smelting in eastern and southern Africa</comment>
               <fpage>499</fpage>
               <source>The Archaeology of Africa: Food, Metals and Towns</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e881a1310">
            <mixed-citation id="d1240e885" publication-type="book">
Feierman, Steven. 1995. Africa in history: the end of universal narratives. In After Colonialism,
Imperial Histories and Postcolonial Displacements, ed. by Gyan Prakash, pp. 40-65. Princeton:
Princeton University Press.<person-group>
                  <string-name>
                     <surname>Feierman</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Africa in history: the end of universal narratives</comment>
               <fpage>40</fpage>
               <source>After Colonialism, Imperial Histories and Postcolonial Displacements</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e920a1310">
            <mixed-citation id="d1240e924" publication-type="journal">
Greig, R. C. H. 1937. Iron smelting in Fipa. Tanganyika Notes and Records 4: 74-81<person-group>
                  <string-name>
                     <surname>Greig</surname>
                  </string-name>
               </person-group>
               <fpage>74</fpage>
               <volume>4</volume>
               <source>Tanganyika Notes and Records</source>
               <year>1937</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e954a1310">
            <mixed-citation id="d1240e958" publication-type="journal">
Hay, Margaret Jean. 1996. Hoes and clothes in a Luo household: changing consumption in a
colonial economy, 1906-1936. In Arnoldi et al. (1996), pp. 243-261.<person-group>
                  <string-name>
                     <surname>Hay</surname>
                  </string-name>
               </person-group>
               <fpage>243</fpage>
               <source>Hoes and clothes in a Luo household: changing consumption in a colonial economy, 1906-1936</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e987a1310">
            <mixed-citation id="d1240e991" publication-type="book">
Herbert, Eugenia. 1993. Iron, Gender and Power: Rituals of Transformation in African Societies.
Bloomington: Indiana University Press.<person-group>
                  <string-name>
                     <surname>Herbert</surname>
                  </string-name>
               </person-group>
               <source>Iron, Gender and Power: Rituals of Transformation in African Societies</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1016a1310">
            <mixed-citation id="d1240e1020" publication-type="book">
Kaijage, Fred. 1983. Labor Conditions in the Tanzanian Mining Industry, 1930-1960. (Working
Papers 83). Boston MA: Boston University African Studies Center.<person-group>
                  <string-name>
                     <surname>Kaijage</surname>
                  </string-name>
               </person-group>
               <source>Labor Conditions in the Tanzanian Mining Industry, 1930-1960</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1045a1310">
            <mixed-citation id="d1240e1049" publication-type="other">
Killick, David. 1990. Technology in its Social Setting: Bloomery Iron-working at Kasungu,
Malawi, 1860-1940. Unpublished Ph.D. dissertation, Yale University.</mixed-citation>
         </ref>
         <ref id="d1240e1059a1310">
            <mixed-citation id="d1240e1063" publication-type="book">
Kjekshus, Helge. 1977. Ecology Control and Economic Development in East African History: the
Case of Tanganyika, 1850-1950. Berkeley: University of California Press.<person-group>
                  <string-name>
                     <surname>Kjekshus</surname>
                  </string-name>
               </person-group>
               <source>Ecology Control and Economic Development in East African History: the Case of Tanganyika, 1850-1950</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1088a1310">
            <mixed-citation id="d1240e1092" publication-type="book">
Knight, C. Gregory. 1974. Ecology and Change: Rural Modernisation in an African Community.
New York: Academic Press.<person-group>
                  <string-name>
                     <surname>Knight</surname>
                  </string-name>
               </person-group>
               <source>Ecology and Change: Rural Modernisation in an African Community</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1118a1310">
            <mixed-citation id="d1240e1122" publication-type="book">
Kriger, Colleen E. 1999. Pride of Men: Ironworking in 19th Century West Central Africa.
Portsmouth: Heinemann.<person-group>
                  <string-name>
                     <surname>Kriger</surname>
                  </string-name>
               </person-group>
               <source>Pride of Men: Ironworking in 19th Century West Central Africa</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1147a1310">
            <mixed-citation id="d1240e1151" publication-type="journal">
Law, John. 1987. On the social explanation of technical change: the case of Portuguese maritime
expansion. Technology and Change 28 (2): 227-252.<person-group>
                  <string-name>
                     <surname>Law</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>227</fpage>
               <volume>28</volume>
               <source>Technology and Change</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1186a1310">
            <mixed-citation id="d1240e1190" publication-type="book">
Lechaptois, Adolph. 1913. Aux Rives du Tanganika. Algiers: Maison Carree.<person-group>
                  <string-name>
                     <surname>Lechaptois</surname>
                  </string-name>
               </person-group>
               <source>Aux Rives du Tanganika</source>
               <year>1913</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1212a1310">
            <mixed-citation id="d1240e1216" publication-type="journal">
Lemelle, Sidney J. 1992. Ritual, resistance and social reproduction: a cultural economy of iron-
smelting in colonial Tanzania 1890-1975. Journal of Historical Sociology 5 (2): 161-182.<person-group>
                  <string-name>
                     <surname>Lemelle</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>161</fpage>
               <volume>5</volume>
               <source>Journal of Historical Sociology</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1251a1310">
            <mixed-citation id="d1240e1255" publication-type="book">
Liidtke, Alf. 1995. What is the history of everyday life and who are its practitioners? In The
History of Everyday Life, ed. by idem, trans. W. Temples. Princeton: Princeton University
Press.<person-group>
                  <string-name>
                     <surname>Liidtke</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">What is the history of everyday life and who are its practitioners?</comment>
               <source>The History of Everyday Life</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1287a1310">
            <mixed-citation id="d1240e1291" publication-type="other">
Mapunda, Bertram. 1995. An Archaeological View of the History and Variation of Ironworking in
Southwestern Tanzania. Unpublished Ph.D. dissertation, University of Florida.</mixed-citation>
         </ref>
         <ref id="d1240e1302a1310">
            <mixed-citation id="d1240e1306" publication-type="book">
Moore, Henrietta and Megan Vaughan. 1994. Cutting Down Trees: Gender, Nutrition and
Agricultural Change in the Northern Province of Zambia, 1890-1990. Portsmouth: Heinemann.<person-group>
                  <string-name>
                     <surname>Moore</surname>
                  </string-name>
               </person-group>
               <source>Cutting Down Trees: Gender, Nutrition and Agricultural Change in the Northern Province of Zambia, 1890-1990</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1331a1310">
            <mixed-citation id="d1240e1335" publication-type="other">
Mullen, Joseph E. 1978. Church and State in the Development of Uwanda, Tanzania, 1920-1975.
Unpublished Ph.D. dissertation, University of Edinburgh.</mixed-citation>
         </ref>
         <ref id="d1240e1345a1310">
            <mixed-citation id="d1240e1349" publication-type="journal">
Popplewell, G. D. 1937. Notes on the Fipa. Tanganyika Notes and Records 3: 99-105.<person-group>
                  <string-name>
                     <surname>Popplewell</surname>
                  </string-name>
               </person-group>
               <fpage>99</fpage>
               <volume>3</volume>
               <source>Tanganyika Notes and Records</source>
               <year>1937</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1378a1310">
            <mixed-citation id="d1240e1382" publication-type="book">
Robert, J. M. 1949. Croyances et coutumes magico-religieuses des Wafipa paj'ens. Tabora:
Tanganyika Mission Press.<person-group>
                  <string-name>
                     <surname>Robert</surname>
                  </string-name>
               </person-group>
               <source>Croyances et coutumes magico-religieuses des Wafipa paj'ens</source>
               <year>1949</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1407a1310">
            <mixed-citation id="d1240e1411" publication-type="book">
Schmidt, Peter (ed.). 1996a. The Culture and Technology of African Iron Production. Gainesville:
University of Florida.<person-group>
                  <string-name>
                     <surname>Schmidt</surname>
                  </string-name>
               </person-group>
               <source>The Culture and Technology of African Iron Production</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1436a1310">
            <mixed-citation id="d1240e1440" publication-type="journal">
Schmidt, Peter. 1996b. Reconfiguring the Barongo: reproductive symbolism and reproduction
among a work association of iron smelters. In Schmidt (1996a), pp. 74-127.<person-group>
                  <string-name>
                     <surname>Schmidt</surname>
                  </string-name>
               </person-group>
               <fpage>74</fpage>
               <source>Reconfiguring the Barongo: reproductive symbolism and reproduction among a work association of iron smelters</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1470a1310">
            <mixed-citation id="d1240e1474" publication-type="book">
---. 1997. Iron Technology in East Africa: Symbolism, Science and Archaeology. Bloomington:
Indiana University Press .<person-group>
                  <string-name>
                     <surname>Schmidt</surname>
                  </string-name>
               </person-group>
               <source>Iron Technology in East Africa: Symbolism, Science and Archaeology</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1499a1310">
            <mixed-citation id="d1240e1503" publication-type="book">
--- and Thomas C. Patterson (eds.). 1997. Making Alternative Histories: the Practice of
Archaeology and History in Non-Western Settings. Santa Fe: School of American Research
Press.<person-group>
                  <string-name>
                     <surname>Schmidt</surname>
                  </string-name>
               </person-group>
               <source>Making Alternative Histories: the Practice of Archaeology and History in Non-Western Settings</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1532a1310">
            <mixed-citation id="d1240e1536" publication-type="book">
Sutton, J. E. G. 1985. Spatial and temporal variables in African iron furnaces. In African Iron
Working Ancient and Traditional, ed. by Randi Haaland and Peter Shinnie, pp. 164-196. Oslo:
Norwegian Universities Press.<person-group>
                  <string-name>
                     <surname>Sutton</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Spatial and temporal variables in African iron furnaces</comment>
               <fpage>164</fpage>
               <source>African Iron Working Ancient and Traditional</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1571a1310">
            <mixed-citation id="d1240e1575" publication-type="book">
Watson, William. 1958. Tribal Cohesion in a Money Economy. Manchester: Manchester
University Press.<person-group>
                  <string-name>
                     <surname>Watson</surname>
                  </string-name>
               </person-group>
               <source>Tribal Cohesion in a Money Economy</source>
               <year>1958</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1600a1310">
            <mixed-citation id="d1240e1604" publication-type="journal">
Wembah-Rashid, J.A.R. 1969. Iron Workers in Ufipa. Bulletin of the International Committee for
Urgent Anthropological and Ethnological Research, 2: 65-72.<person-group>
                  <string-name>
                     <surname>Wembah-Rashid</surname>
                  </string-name>
               </person-group>
               <fpage>65</fpage>
               <volume>2</volume>
               <source>Bulletin of the International Committee for Urgent Anthropological and Ethnological Research</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1636a1310">
            <mixed-citation id="d1240e1640" publication-type="book">
Willis, Roy. 1981. A State in the Making: Myth, History and Social Transformation in Pre-
colonial Ufipa. Bloomington: Indiana University Press.<person-group>
                  <string-name>
                     <surname>Willis</surname>
                  </string-name>
               </person-group>
               <source>A State in the Making: Myth, History and Social Transformation in Precolonial Ufipa</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1666a1310">
            <mixed-citation id="d1240e1670" publication-type="journal">
Wise, Robert. 1958a. Iron smelting in Ufipa. Tanganyika Notes and Records 50: 106-111.<person-group>
                  <string-name>
                     <surname>Wise</surname>
                  </string-name>
               </person-group>
               <fpage>106</fpage>
               <volume>50</volume>
               <source>Tanganyika Notes and Records</source>
               <year>1958</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1699a1310">
            <mixed-citation id="d1240e1703" publication-type="journal">
---. 1958b. Some rituals of iron-making in Ufipa. Tanganyika Notes and Records 51: 232-238.<person-group>
                  <string-name>
                     <surname>Wise</surname>
                  </string-name>
               </person-group>
               <fpage>232</fpage>
               <volume>51</volume>
               <source>Tanganyika Notes and Records</source>
               <year>1958</year>
            </mixed-citation>
         </ref>
         <ref id="d1240e1732a1310">
            <mixed-citation id="d1240e1736" publication-type="journal">
Wright, Marcia. 1985. Iron and regional history: report on a research project in southwestern
Tanzania. African Economic History 14: 147-165.<object-id pub-id-type="doi">10.2307/3601116</object-id>
               <fpage>147</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1240e1752a1310">
            <mixed-citation id="d1240e1756" publication-type="book">
---. 1989. The First Generation of Christians at Mwazye. Seminar paper, New College,
Edinburgh University.<person-group>
                  <string-name>
                     <surname>Wright</surname>
                  </string-name>
               </person-group>
               <source>The First Generation of Christians at Mwazye</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
