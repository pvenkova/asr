<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1ps32fd</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Roald Dahl's Marvellous Medicine</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Solomon</surname>
               <given-names>Tom</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>13</day>
         <month>09</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781781383391</isbn>
      <isbn content-type="epub">9781781383469</isbn>
      <publisher>
         <publisher-name>Liverpool University Press</publisher-name>
         <publisher-loc>Liverpool</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Tom Solomon</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1ps32fd"/>
      <abstract abstract-type="short">
         <p>Most people know Roald Dahl as a famous write of children’s books and adult short stories, but few are aware of his fascination with medicine. Right from his earliest days to the end of his life, Dahl was intrigued by what doctors do, and why they do it. During his lifetime, he and his family suffered some terrible medical tragedies: Dahl nearly died when his fighter plane went down in World War II; his son had severe brain injury in an accident; and his daughter died of measles infection of the brain. But he also had some medical triumphs: he dragged himself back to health after the plane crash, despite a skull fracture, back injuries, and blindness; he was responsible for inventing a medical device (the Wade-Dahl-Till valve) to treat his son's hydrocephalus (water on the brain), and he taught his first wife Patricia to talk again after a devastating stroke. His medical interactions clearly influenced some of his writing – for example the explosive potions in George’s Marvellous Medicine. And sometimes his writing impacted on events in his life – for example the research on neuroanatomy he did for his short story William and Mary later helped him design the valve for treating hydrocephalus. In this unique book, Professor Tom Solomon, who looked after Dahl towards the end of his life, examines Dahl’s fascination with medicine. Taking examples from Dahl’s life, and illustrated with excerpts from his writing, the book uses Dahl’s medical interactions as a starting point to explore some extraordinary areas of medical science. Solomon is an award-winning science communicator, and he effortlessly explains the medical concepts underpinning the stories, in language that everyone can understand. The book is also peppered with anecdotes from Dahl’s late night hospital discussions with Solomon, which give new insights into this remarkable man’s thinking as his life came to an end.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.2</book-part-id>
                  <title-group>
                     <title>About the Author</title>
                  </title-group>
                  <fpage>ii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.3</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Most people know Roald Dahl as a famous writer of children’s books and adult short stories, but few are aware of his fascination with medicine. Right from his earliest days to the end of his life, Dahl was intrigued by what doctors do, and why they do it. During his lifetime, he and his family suffered some terrible medical tragedies: Dahl nearly died when his fighter plane went down in World War II, his son had severe brain injury in an accident and his daughter died from measles infection of the brain. But he also had some medical triumphs: he</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.5</book-part-id>
                  <title-group>
                     <title>Prologue</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>According to the family stories, my great-grandfather Moses Solomon landed in Cardiff as a boy at the end of the nineteenth century, escaping the pogroms of Lithuania. His name was not Solomon when he left his home country, but the immigration officer could not understand his eastern European accent and they chose the new name together. I was visiting Cardiff for the first time in May 2014 for a conference, and with Moses in mind felt I ought to go and look around the docks, though quite what I hoped to find I do not know. As I passed the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.6</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>The Witching Hour</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>In 1990 I was a junior doctor at the John Radcliffe Hospital in Oxford. I had just finished all my training as a medical student, and was starting my first year working on the wards. Roald Dahl was one of the patients. He was in his seventies, and had been in and out of hospital in the preceding few months with anaemia. He was being cared for by my boss, one of the country’s most distinguished haematologists, Professor Sir David Weatherall. Dahl was becoming increasingly frail as the summer progressed, and Weatherall was doing all he could to determine the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.7</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Prodding and Poking</title>
                  </title-group>
                  <fpage>18</fpage>
                  <abstract>
                     <p>Sometime during the 1880s Dahl’s father, Harald, left Norway on a boat, spending a few years in Paris before settling in Cardiff, South Wales. At the time, Cardiff was a thriving coal metropolis where enterprising Norwegians could make their fortune. Dahl’s mother, Sofie Magdalene, also came from Norway, and even after his father had died she stayed in Cardiff for the sake of the children’s education. Dahl’s first school was Llandaff Cathedral School in Cardiff. However, following<italic>The Great Mouse Plot</italic>, in which he left a dead rodent in a sweetie jar to terrify a local shopkeeper, he was moved</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.8</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Into Africa</title>
                  </title-group>
                  <fpage>28</fpage>
                  <abstract>
                     <p>Often junior doctors dread their night-time work. They have already done a full day on the wards, and as soon as the evening jobs are finished, they want to get to bed. However, I looked forward to the evenings. Once everything was under control I would head to ward 5E, the haematology ward, and the side-room where Dahl was in bed.</p>
                     <p>I remember one evening it seemed especially quiet, and so I eased the door open slowly, in case he was asleep. I found him reading, concentrating on a novel by Ed McBain. His solid meaty hands, with well-groomed nails,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.9</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Crash</title>
                  </title-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>Dahl and I chatted every few nights, whenever I was on call, and I found I liked him more and more. He was one of many patients to whom I have become close over the years. I used to keep notes of what I learned along the way in a green notebook, important things patients taught me about medicine and about life. In five or six packed years as a medical student, you discover so much about what is important. By seeing people at their most difficult times, their most vulnerable, you understand how some cope, and even grow, whilst</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.10</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>A Lucky Piece of Cake</title>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>‘A man with congestive cardiac failure, two patients with myocardial infarctions and an old boy who had fallen out of bed and hit his head . . .’ I was telling Dahl a bit of what I had been up to earlier in the evening.</p>
                     <p>‘My writing career was started by a bang on the head, you know.’ Dahl had a mischievous twinkle in his eye. ‘Yes, it was the great bash on the head.’ I suspected he was ribbing me, but I let him go on. ‘. . . The plane coming down in the desert. Mind you, there</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.11</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>Tales</title>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>Over the last twenty-five years, evidence has accumulated to support this idea that mild frontal lobe damage could subtly increase artistic abilities. There are cases of acquired savant syndrome caused by fronto-temporal dementia, due to the gradual loss of neurones from the frontal lobes, rather than the sudden trauma or bleeding with which it was first associated. Functional brain imaging techniques such as SPECT (single photon emission computed tomography) show remarkable similarities between savants with fronto-temporal dementia and those with autism: in both situations there is loss of function in the frontal and temporal lobe of the brain and enhanced</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.12</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Threats and Dangers</title>
                  </title-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>‘How is your article coming along?’ Dahl asked me as I was tapping away on the ward computer one evening.</p>
                     <p>‘Slowly,’ I complained. ‘We have all the data, but the journals are very fussy about how you write it.’</p>
                     <p>‘Where will you send it?’</p>
                     <p>‘We’ll probably try the<italic>Lancet</italic>.’</p>
                     <p>‘Ah yes, it’s a good journal. We published there many years ago . . .’</p>
                     <p>‘Really?’ From the sparkle in his eye I thought he was probably trying to get a rise from me.</p>
                     <p>‘Yes, we invented a valve for hydrocephalus.’ He started fiddling selfconsciously with the pens on the work</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.13</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>‘It’ll Be Good for Them’</title>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>Although it is less than an hour on the train from London, the village of Great Missenden has not changed much since I first visited it twenty-five years ago. From the station I walk down the high street, past banks, shops and pubs, some of which have been there for hundreds of years. There is red brick, lots of Tudor timber, some tasteful blue and green washes. And what is that up on the left? A house with a huge and strange shadow on it. Wait a minute . . . it is not a shadow. It’s a pale outline</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.14</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>A Clue Here</title>
                  </title-group>
                  <fpage>106</fpage>
                  <abstract>
                     <p>Many years later, pouring through the archive at the museum, I found that Dahl<italic>had</italic>in fact been doggedly corresponding with scientists for years after, to try to discover a link between smallpox vaccination and measles encephalitis.</p>
                     <p>‘There must be some clue or clues as to the susceptibility of a person to encephalitis from this vaccination business,’ he wrote to Dr John Adams at the University of California’s Department of Paediatrics in 1966. ‘It is probably staring us in the face. In what way for example is resistance to encephalitis provided when immunity to smallpox is achieved? My daughter was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.15</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>A Shot in the Arm</title>
                  </title-group>
                  <fpage>116</fpage>
                  <abstract>
                     <p>In 1796, physician Edward Jenner inoculated a young boy with pus extracted from a cowpox sore on a milkmaid’s hand. His action not only protected eightyear-old James Phipps from smallpox, but ushered in the era of vaccination. It was a slow start; it would be one hundred years before Pasteur spearheaded the development of vaccines for cholera and anthrax that got things going properly.</p>
                     <p>Cowpox is principally a disease of cattle, but Jenner noticed that milkmaids, who were liable to get the lesions on their hands, seemed not to be affected by smallpox when it ravaged communities. He was not</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.16</book-part-id>
                  <title-group>
                     <label>Chapter 11</label>
                     <title>A Bubble Bursts</title>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>For thirty-nine years, Patricia Neal’s brain had successfully coped with about 750 millilitres of blood pumped up through her cerebral arteries every minute of every day. But on 17 February 1965 things went disastrously wrong. Neal had won the Oscar, the Academy Award for Best Actress in<italic>Hud</italic>, the year before, and was back in Hollywood working on her next movie project,<italic>Seven Women</italic>. She was three months’ pregnant with her fifth child, Lucy, and was at home giving seven-year-old Tessa a bath when suddenly a pain shot through her head. Initially she wondered if she had just overdone it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.17</book-part-id>
                  <title-group>
                     <label>Chapter 12</label>
                     <title>The Mysterious Joy of Language</title>
                  </title-group>
                  <fpage>142</fpage>
                  <abstract>
                     <p>‘It was an extraordinary sight.’ Dahl was recalling Pat’s slow recovery from the stroke. ‘Extraordinary to walk into the intensive care, with all its intravenous drips and tubes and oxygen tents, to see Pat propped up in her bed with the nurse beside her singing songs, and yet she could barely utter a word.’</p>
                     <p>‘It’s an example of localisation of function.’ I drew a sketch on a scrap of paper. ‘The speech centres are here on the left side of the brain . . .’</p>
                     <p>‘That’s Broca’s area.’</p>
                     <p>‘Yes, exactly, and music and singing are on the right.’</p>
                     <p>Early on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.18</book-part-id>
                  <title-group>
                     <label>Chapter 13</label>
                     <title>The Cabbage and the Giant</title>
                  </title-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>Before they left UCLA Medical Centre, Pat’s neurosurgeon, Dr Carton, suggested to Dahl that immediate and intense stimulation might be the best chance for her recovery; they should start speech therapy and mental rehabilitation right away. Not a single day must be lost. This was just what Dahl wanted to hear, that you could actually do something to take control of the outcome. Carton arranged for a speech therapist to visit Pat daily at home. Dahl asked Jean Alexander, the nurse in whom they had such confidence, to also come as often as she could. Plus of course there were</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.19</book-part-id>
                  <title-group>
                     <label>Chapter 14</label>
                     <title>Thousands Across the Country</title>
                  </title-group>
                  <fpage>164</fpage>
                  <abstract>
                     <p>Valerie Eaton Griffith’s rehabilitation work with Pat and Dahl, and the book which described their methods,<italic>A Stroke in the Family</italic>, started a medical revolution in terms of stroke therapy; they were also key catalysts in the development of the Stroke Association of today from its predecessor the Chest and Heart Association.</p>
                     <p>The origins of the Chest and Heart Association can be traced back to the National Association for the Prevention of Consumption and other forms of Tuberculosis, which was founded in 1898. The organisation’s evolution over the next hundred years, shifting according to the public health priorities, is reflected</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.20</book-part-id>
                  <title-group>
                     <label>Chapter 15</label>
                     <title>Rusting to Pieces</title>
                  </title-group>
                  <fpage>177</fpage>
                  <abstract>
                     <p>Over the decades Dahl’s productivity grew and grew, like the giant peach that had started it all. For him there was no magic powder, just the regular couple of hours every morning and afternoon spent in his hut. Whilst the 1970s saw a balance of adult short stories and children’s books, by the 1980s Dahl was writing almost exclusively for children, producing some of his best-loved work, including<italic>The Twits, George’s Marvellous Medicine, The BFG, The Witches</italic>and<italic>Matilda</italic>.</p>
                     <p>‘In my little work hut I enter a dreamlike state,’ he explained to me, ‘sinking back into childhood. I really have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.21</book-part-id>
                  <title-group>
                     <label>Chapter 16</label>
                     <title>The Patient</title>
                  </title-group>
                  <fpage>190</fpage>
                  <abstract>
                     <p>‘Polycythaemia . . . rubra . . . vera . . .’ Dahl let the term roll around his mouth, as if he were savouring a good claret. ‘Yes, I like that. Polycythaemia – busy, grave, important, serious; and yet rubra . . . vera – a touch of colour . . . lightness . . . hope . . .’</p>
                     <p>‘Surely characters from a story,’ I countered. ‘Poly Cythaemia, a nasty ugly aunt with awful breath and crooked teeth; Ruby and Vera, her younger sisters, more pleasant and amenable, who try to make up for Polly’s terrible ways.’</p>
                     <p>It was late</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.22</book-part-id>
                  <title-group>
                     <label>Chapter 17</label>
                     <title>The Last Night</title>
                  </title-group>
                  <fpage>205</fpage>
                  <abstract>
                     <p>It had been another awful day for Dahl: his blood pressure had been low, his temperature up and down, and his bones aching terribly despite the analgesics. When I came on the ward in the early evening, he was more comfortable, and by midnight everything seemed settled. Dahl was asleep, and Liccy and Ophelia were with him, so I decided to go and get some rest, but at one in the morning I was woken by a bleep from the nurses.</p>
                     <p>Could I come down? Roald was in distress.</p>
                     <p>I examined him, and talked with Liccy and Ophelia. Dahl was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.23</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>213</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.24</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>215</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.25</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>221</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.26</book-part-id>
                  <title-group>
                     <title>Photo Credits</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.27</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.28</book-part-id>
                  <title-group>
                     <title>Charity Support</title>
                  </title-group>
                  <fpage>252</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1ps32fd.29</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
