<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt24h1dk</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt5vkdcv</book-id>
      <subj-group>
         <subject content-type="call-number">JC571.C278 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">National human rights institutions</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ombudsman</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights advocacy</subject>
         <subj-group>
            <subject content-type="lcsh">Government policy</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Chains of Justice</book-title>
         <subtitle>The Global Rise of State Institutions for Human Rights</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Cardenas</surname>
               <given-names>Sonia</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>14</day>
         <month>03</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780812245394</isbn>
      <isbn content-type="epub">9780812208931</isbn>
      <isbn content-type="epub">0812208935</isbn>
      <publisher>
         <publisher-name>University of Pennsylvania Press, Inc.</publisher-name>
         <publisher-loc>Philadelphia</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>University of Pennsylvania Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt5vkdcv"/>
      <abstract abstract-type="short">
         <p>National human rights institutions-state agencies charged with protecting and promoting human rights domestically-have proliferated dramatically since the 1990s; today more than a hundred countries have NHRIs, with dozens more seeking to join the global trend. These institutions are found in states of all sizes-from the Maldives and Barbados to South Africa, Mexico, and India; they exist in conflict zones and comparatively stable democracies alike. In<italic>Chains of Justice</italic>, Sonia Cardenas offers a sweeping historical and global account of the emergence of NHRIs, linking their growing prominence to the contradictions and possibilities of the modern state.As human rights norms gained visibility at the end of the twentieth century, states began creating NHRIs based on the idea that if international human rights standards were ever to take root, they had to be firmly implanted within countries-impacting domestic laws and administrative practices and even systems of education. However, this very position within a complex state makes it particularly challenging to assess the design and influence of NHRIs: some observers are inclined to associate NHRIs with ideals of restraint and accountability, whereas others are suspicious of these institutions as "pretenders" in democratic disguise. In her theoretically and politically grounded examination, Cardenas tackles the role of NHRIs, asking how we can understand the global diffusion of these institutions, including why individual states decide to create an NHRI at a particular time while others resist the trend. She explores the influence of these institutions in states seeking mostly to appease international audiences as well as their value in places where respect for human rights is already strong.The most comprehensive account of the NHRI phenomenon to date,<italic>Chains of Justice</italic>analyzes many institutions never studied before and draws from new data released from the Universal Periodic Review Mechanism of the United Nations Human Rights Council. With its global scope and fresh insights into the origins and influence of NHRIs,<italic>Chains of Justice</italic>promises to become a standard reference that will appeal to scholars immersed in the workings of these understudied institutions as well as nonspecialists curious about the role of the state in human rights.</p>
      </abstract>
      <counts>
         <page-count count="480"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>[i]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>[ix]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.3</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Self-Restraining State?</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>High up the embankment of Agra Fort, next to a sweeping view of the Taj Mahal, is a nondescript archway with a marble plaque. The tablet marks the spot of a legendary chain from the seventeenth century. The unusual chain was according to some accounts made of gold, was eighty feet long, and had sixty bells attached to it, linking Agra Fort to a post by the nearby riverbank. It was known simply as the “chain of justice,” and forging it was one of Nuruddin Jahangir’s first acts as leader of the Mughal Empire. The plan was for ordinary people</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.4</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Historical Linkages</title>
                  </title-group>
                  <fpage>16</fpage>
                  <abstract>
                     <p>Just as human rights is a modern and contested discourse, which cannot be read retroactively into every past struggle, not all government bodies that address human rights issues are NHRIs.¹ National human rights institutions are a formal designation, a term of art, referring to a specific type of state institution, first created in the late twentieth century and defined by a particular set of international standards. Yet it is also true that institutions always carry some baggage of the past with them, so understanding this contemporary phenomenon and its significance requires situating the institution in its broader historical context. Even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.5</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Tracking Global Diffusion</title>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>Sometimes ideas spread rapidly, as if out of nowhere, adopted by very diverse groups and across disparate contexts. Diffusion occurs when these ideas are institutionalized across a wide range of countries, despite obvious national differences and local resistance.¹ In this manner, democracy, liberalism, markets, and human rights became common parlance in the late twentieth century, embraced by a broad segment of political elites and social groups. The same is true of NHRIs, which in a relatively short span of time have proliferated around the world and evolved significantly. This chapter zeroes in on the global rise of NHRIs, offering evidence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.6</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Logic of Strategic Emulation</title>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>When institutions diffuse widely, resistance can be difficult to decipher. Like Havel’s green grocer, states can face system-wide pressures to embrace popular international symbols and practices. Adopting popular policies does not, of course, mean that state commitments are superficial or that the policies are inconsequential. Still, the rapid diffusion of an institution across highly diverse contexts invites caution—to look carefully for the subtle (and not-so-subtle) ways in which local actors may resist and transform international templates.² Doing so detracts from easy assumptions that global diffusion is largely good or mostly bad. The reality is that local actors appropriate international</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.7</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Trendsetters and Early Adopters, pre-1990</title>
                  </title-group>
                  <fpage>74</fpage>
                  <abstract>
                     <p>The story of how NHRIs have diffused around the world is one of multiple contingencies, of diverse but partially converging pathways. It is a story about the power of institutional models to spread, especially when they are promoted actively and adopted by others similarly situated. In this sense, no single NHRI can be understood in isolation or separated from its broader context. NHRIs created during similar periods are partially connected to one another, even if they are found halfway around the world.</p>
                     <p>The global rise of NHRIs has occurred in waves, corresponding roughly to three periods: pre-1990s, when just over</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.8</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Democratization Scripts and Bandwagoning in Africa</title>
                  </title-group>
                  <fpage>106</fpage>
                  <abstract>
                     <p>Dozens of countries joined the NHRI bandwagon after 1990, showcasing the role of international diffusion. Especially at the regional level, neighboring countries emulated each other, while international organizations and regional networks actively fostered the creation of these institutions. If earlier decades had set the normative foundations for the concept of an NHRI, the 1990s saw rising institutionalization. Beginning with the drafting of the seminal Paris Principles in 1991 and the promotion of NHRIs at the Vienna World Conference two years later, the notion of an NHRI was championed and popularized. Even before Vienna, however, resolutions within the United Nations focusing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.9</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Transitional Myths and Everyday Politics in the Americas</title>
                  </title-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>If Africa has more NHRIs than any region, Latin America and the Caribbean is the region of the world with the highest relative concentration of NHRIs. It is also the place with the greatest number of fully accredited NHRIs. These trends partly reflected a democratization wave that swept the region in the decade after the Cold War ended, combined with long-standing tendencies toward legalism and institutionalization in many of the region’s countries. Internationally, the 1990s marked a watershed period for NHRI growth, and numerous international actors (including foreign governments, the EU, and UNDP) provided Latin American governments creating human rights</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.10</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Appeasement via Localization in the Asia Pacific</title>
                  </title-group>
                  <fpage>192</fpage>
                  <abstract>
                     <p>Of all the regions in the world, the Asia Pacific has had the lowest concentration of NHRIs since the 1990s. Following the lead of New Zealand, Australia, and the Philippines in the 1970s and 1980s, nine countries in the Asia Pacific created an NHRI in the 1990s: India, Indonesia, and Palestine in 1993; Iran in 1995; Sri Lanka in 1996; Fiji, Nepal, and Thailand in 1997; and Malaysia in 1999. The fact that, unlike other regions, the vast area of the Asia Pacific (defined here by UN categories) did not have a region-wide system of human rights protection or undergo</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.11</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Membership Rites and Statehood in the New Europe</title>
                  </title-group>
                  <fpage>256</fpage>
                  <abstract>
                     <p>If political transitions and new constitutions serve as key regulatory moments, pushing states to lock in commitments, then no wonder Europe joined the global bandwagon of NHRI diffusion after 1990. With the collapse of the Soviet Union, newly independent states in Eastern Europe moved to create NHRIs in the 1990s. Strikingly, country after country across Eastern Europe established an NHRI as part of either postindependence democratic institution building or constitutional reform; membership in the Council of Europe also proved crucial. While most Eastern European countries failed to meet international standards, a few significant excetions existed: Russia, Bosnia, Georgia, Ukraine, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.12</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>How Accountability Institutions Matter</title>
                  </title-group>
                  <fpage>310</fpage>
                  <abstract>
                     <p>Institutional assessment is always rife with dilemmas. How does one capture the full array of what an institution does? How does one accommodate relative successes and failures alongside a coherent narrative of the institution’s overall effects? That institutional outcomes can be conceptualized (and measured) in vastly different and contested ways, only adds to the complexity of the task. In the anecdote above, was the human rights commission successful in equipping one person with the means to resist police violence? Was the incident isolated, or did it indicate some broader pattern? Should we evaluate the institution’s work in terms of how</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.13</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Adaptive States:</title>
                     <subtitle>Making and Breaking International Law</subtitle>
                  </title-group>
                  <fpage>350</fpage>
                  <abstract>
                     <p>Karl Polanyi asserted in<italic>The Great Transformation</italic>that “[n]o mere declaration of rights can suffice: institutions are required to make the rights effective.”¹ Polanyi was highlighting the necessity of public institutions, their role in translating rights into practice. Yet Polanyi also saw institutions as social products, “embodiments of human meaning and purpose” in a given historical moment.² In this view, institutions are the engines of change, just as they are shaped and constrained by their particular context. While Polanyi was commenting most directly on the nature of institutions, he was also implying that the origin of any right lies in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.14</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>361</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>443</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt5vkdcv.16</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>479</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
