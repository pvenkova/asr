<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">janimalecology</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100032</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Animal Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00218790</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">13652656</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40605437</article-id>
         <article-categories>
            <subj-group>
               <subject>Population ecology</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Density as an explanatory variable of movements and calf survival in savanna elephants across southern Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>K. D.</given-names>
                  <surname>Young</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>R. J.</given-names>
                  <surname>Van Aarde</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">79</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40026534</issue-id>
         <fpage>662</fpage>
         <lpage>673</lpage>
         <permissions>
            <copyright-statement>© 2010 British Ecological Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40605437"/>
         <abstract>
            <p>1. Southern Africa's 'elephant problem' is often attributed to an overabundance of elephants (Loxodonta africana) in conservation areas. Paradoxically, the African elephant is listed as 'vulnerable' (IUCN Redlist) despite occupying a large geographical range and numbering about 600 000. How densities influence elephant populations is therefore important for conservation management decisions, particularly because a move towards non-equilibrium management of savannas implies a need for elephant populations to fluctuate in response to variation in intrinsic (demographic) and extrinsic (resource) factors. 2. A study on one of the world's largest elephant populations demonstrated that population regulation is driven by a spatial response to water availability, environmental stochasticity and density.The challenge remains to identify the demographic and behavioural variables that drive density dependence. 3. We evaluated whether the movements of elephant family groups from 13 populations across a wide resource gradient were explained by variability in primary productivity, rainfall and population density. We then assessed whether density-related movements explained variability in juvenile survival, hence inferring a spatially driven behavioural mechanism that may explain densitydependent population growth. We also analysed whether management actions modified this mechanism. 4. In the dry season, daily-displacement distances (DDDs) increased non-linearly with density, and declined with increased vegetation productivity and previous wet season rainfall. In the wet season, DDDs were primarily explained by vegetation productivity. 5. The survival of weaned calves (4-7 years) decreased with increasing dry season DDDs, but this did not hold for suckling calves (1-3 years) or sub-adults (8-11 years). 6. Fences and supplementary water modified the shape and strength of relationships between DDDs and densities, vegetation productivity and rainfall and negated the relationships between DDDs and weaned calf survival. 7. We suggest that density dependence in weaned calf survival is driven by the response of dry season roaming activities of family groups to variations in density, rainfall and the distribution of food. Fences and supplementary water that alter this mechanism may contribute to the relatively high population growth rates of some populations.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>&lt;bold&gt;References&lt;/bold&gt;</title>
         <ref id="d37e213a1310">
            <mixed-citation id="d37e217" publication-type="other">
van Aarde, R.J. &amp; Jackson, T.P. (2007) Megaparks for metapopulations:
addressing the causes of locally high elephant numbers in southern Africa.
Biological Conservation, 134, 289-297.</mixed-citation>
         </ref>
         <ref id="d37e230a1310">
            <mixed-citation id="d37e234" publication-type="other">
van Aarde, R., Whyte, I. &amp; Pimm, S. (1999) Culling and the dynamics of the
Kruger National Park African elephant population. Animal Conservation, 2,
287-294.</mixed-citation>
         </ref>
         <ref id="d37e247a1310">
            <mixed-citation id="d37e251" publication-type="other">
van Aarde, R.J., Ferreira, S.M., Jackson, T.P., Page, B., de Beer, Y., Junker, J.,
Gough, K., Guldemond, R., Olivier, P.I., Ott, T. &amp; Trimble, M.J. (2008)
Population biology and ecology. Elephant Management: A Scientific Assess-
ment for South Africa (eds R.J. Scholes &amp; K.G. Mennell), pp. 84-145. Wits
University Press, Johannesburg.</mixed-citation>
         </ref>
         <ref id="d37e270a1310">
            <mixed-citation id="d37e274" publication-type="other">
de Beer, Y. &amp; van Aarde, R.J. (2008) Do landscape heterogeneity and water dis-
tribution explain aspects of elephant home range in southern Africa's arid
savannas? Journal of Arid Environments, 72, 2017-2025.</mixed-citation>
         </ref>
         <ref id="d37e288a1310">
            <mixed-citation id="d37e292" publication-type="other">
de Beer, Y., Kilian, W., Versfeld, W. &amp; van Aarde, R.J. (2006) Elephants and
low rainfall alter woody vegetation in Etosha National Park, Namibia. Jour-
nal of Arid Environments, 64, 412-421.</mixed-citation>
         </ref>
         <ref id="d37e305a1310">
            <mixed-citation id="d37e309" publication-type="other">
Blanc, J.J., Barnes, R.F.W., Craig, G.C., Dublin, H.T., Thouless, C.R., Doug-
las-Hamilton, I. &amp; Hart, J.A. (2007) African Elephant Status Report 2007:
An Update from the African Elephant Database. Occasional Paper Series of
the IUCN Species Survival Commission, No. 33. IUCN/SSC African Ele-
phant Specialist Group, IUCN, Gland, Switzerland.</mixed-citation>
         </ref>
         <ref id="d37e328a1310">
            <mixed-citation id="d37e332" publication-type="other">
Bonenfant, C, Gaillard, J.M., Coulson, T., Festa-Bianchet, M., Loison, A.,
Garel, M., Loe, L.E., Blanchard, P., Pettorelli, N., Owen-Smith, N., Du Toit,
J. &amp; Duncan, P. (2009) Empirical evidence of density-dependence in popula-
tions of large herbivores. Advances in Ecological Research, 41, 313-357.</mixed-citation>
         </ref>
         <ref id="d37e348a1310">
            <mixed-citation id="d37e352" publication-type="other">
Bradshaw, C.J.A. (2008) Having your water and drinking it too: resource
limitation modifies density regulation. Journal of Animal Ecology, 77,
1-4.</mixed-citation>
         </ref>
         <ref id="d37e365a1310">
            <mixed-citation id="d37e369" publication-type="other">
Burnham, K.P. &amp; Anderson, D.R. (2002) Model Selection and Multimodel
Inference: A Practical Information-Theoretic Approach, 2nd edn. Springer-
Verlag, New York.</mixed-citation>
         </ref>
         <ref id="d37e382a1310">
            <mixed-citation id="d37e386" publication-type="other">
Caughley, G. (1977) Analysis of Vertebrate Populations. John Wiley &amp; Sons,
Chichester.</mixed-citation>
         </ref>
         <ref id="d37e397a1310">
            <mixed-citation id="d37e401" publication-type="other">
Chamaillé-Jammes, S., Valeix, M. &amp; Fritz, H. (2007a) Elephant management:
why can't we throw out the babies with the artificial bathwater? Diversity
and Distributions, 13, 663-665.</mixed-citation>
         </ref>
         <ref id="d37e414a1310">
            <mixed-citation id="d37e418" publication-type="other">
Chamaillé-Jammes, S., Valeix, M. &amp; Fritz, H. (2007b) Managing heteroge-
neity in elephant distribution: interactions between elephant population
density and surface-water availability. Journal of Applied Ecology, 44,
625-633.</mixed-citation>
         </ref>
         <ref id="d37e434a1310">
            <mixed-citation id="d37e438" publication-type="other">
Chamaillé-Jammes, S., Fritz, H., Valeix, M., Murindagomo, F. &amp; Colbert, J.
(2008) Resource variability, aggregation and direct density dependence in an
open context. The local regulation of an elephant population. Journal of Ani-
mal Ecology, 77,135-144.</mixed-citation>
         </ref>
         <ref id="d37e454a1310">
            <mixed-citation id="d37e458" publication-type="other">
Clutton-Brock, T.H., Coulson, T.N., Milner-Gulland, E.J., Thomson, D. &amp;
Armstrong, H.M. (2002) Sex differences in emigration and mortality affect
optimal management of deer populations. Nature, 415, 633-637.</mixed-citation>
         </ref>
         <ref id="d37e471a1310">
            <mixed-citation id="d37e475" publication-type="other">
Conybeare, A. &amp; Haynes, G. (1984) Observations on elephant mortality and
bones in water holes. Quaternary Research, 22, 189-200.</mixed-citation>
         </ref>
         <ref id="d37e485a1310">
            <mixed-citation id="d37e489" publication-type="other">
Coughenour, M.B. &amp; Singer, F.J. (1996) Elk population processes in Yellow-
stone National Park under the policy of natural regulation. Ecological Appli-
cations, 6,513-593.</mixed-citation>
         </ref>
         <ref id="d37e503a1310">
            <mixed-citation id="d37e507" publication-type="other">
Coulson, T., Catchpole, E.A., Albon, S.D., Morgan, B.J.T., Pemberton, J.M.,
Clutton-Brock, T.H., Crawley, M.J. &amp; Grenfell, B.T. (2001) Age, sex, den-
sity, winter weather, and population crashes in Soay sheep. Science, 292,
1528-1531.</mixed-citation>
         </ref>
         <ref id="d37e523a1310">
            <mixed-citation id="d37e527" publication-type="other">
Coulson, T.N., Guinness, F.F., Pemberton, J.M. &amp; Clutton-Brock, T.H.
(2004) The demographic consequences of releasing a population of red deer
from culling. Ecology, 85, 411-422.</mixed-citation>
         </ref>
         <ref id="d37e540a1310">
            <mixed-citation id="d37e544" publication-type="other">
Dobson, A.P. (1993) Effect of fertility control on elephant population dynam-
ics. Journal of Reproduction and Fertility Supplement, 90, 293-298.</mixed-citation>
         </ref>
         <ref id="d37e554a1310">
            <mixed-citation id="d37e558" publication-type="other">
Dudley, J.P., Craig, G.C., Gibson, D.St.C, Haynes, G. &amp; Klimowicz, J. (2001)
Drought mortality of bush elephants in Hwange National Park, Zimbabwe.
African Journal of Ecology, 39, 187-194.</mixed-citation>
         </ref>
         <ref id="d37e571a1310">
            <mixed-citation id="d37e575" publication-type="other">
Eberhardt, L.L. (2002) A paradigm for population analysis of long-lived verte-
brates. Ecology, 83, 2841-2854.</mixed-citation>
         </ref>
         <ref id="d37e585a1310">
            <mixed-citation id="d37e589" publication-type="other">
Ferreira, S.M. &amp; van Aarde, R.J. (2008) A rapid method to estimate some of
the population variables for African elephants. Journal of Wildlife Manage-
ment, 72, 822-829.</mixed-citation>
         </ref>
         <ref id="d37e603a1310">
            <mixed-citation id="d37e607" publication-type="other">
Foley, C, Pettorelli, N. &amp; Foley, L. (2008) Severe drought and calf survival in
elephants. Biology Letters, 4, 541-544.</mixed-citation>
         </ref>
         <ref id="d37e617a1310">
            <mixed-citation id="d37e621" publication-type="other">
Fowler, C.W. &amp; Smith, T. (1973) Characterizing stable populations: an applica-
tion to the African elephant population. Journal of Wildlife Management, 37,
513-523.</mixed-citation>
         </ref>
         <ref id="d37e634a1310">
            <mixed-citation id="d37e638" publication-type="other">
Gaillard, J.-M., Festa-Bianchet, M. &amp; Yoccoz, N.G. (1998) Population dynam-
ics of large herbivores: variable recruitment with constant adult survival.
Trends in Ecology and Evolution, 13, 58-63.</mixed-citation>
         </ref>
         <ref id="d37e651a1310">
            <mixed-citation id="d37e655" publication-type="other">
Gaillard, J.-M., Festa-Bianchet, M., Yoccoz, N.G., Loison, A. &amp; Toigo, C.
(2000) Temporal variation in fitness components and population dynam-
ics of large herbivores. Annual Review of Ecology and Systematics, 31,
367-393.</mixed-citation>
         </ref>
         <ref id="d37e671a1310">
            <mixed-citation id="d37e675" publication-type="other">
Getz, W.M. (1996) A hypothesis regarding the abruptness of density depen-
dence and the growth rate of populations. Ecology, 11, 2014-2026.</mixed-citation>
         </ref>
         <ref id="d37e685a1310">
            <mixed-citation id="d37e689" publication-type="other">
Gillson, L. &amp; Lindsay, K. (2003) Ivory and ecology -changing perspectives on
elephant management and the international trade in ivory. Environmental
Science and Policy, 6, 411-419.</mixed-citation>
         </ref>
         <ref id="d37e703a1310">
            <mixed-citation id="d37e707" publication-type="other">
Gough, K.F. &amp; Kerley, G.I.H. (2006) Demography and population dynamics
in the elephants Loxodonta africana of Addo Elephant National Park, South
Africa: is there evidence of density dependent regulation? Oryx, 40, 434-441.</mixed-citation>
         </ref>
         <ref id="d37e720a1310">
            <mixed-citation id="d37e724" publication-type="other">
Harris, G.M., Russell, G.J., van Aarde, R.J. &amp; Pimm, S.L. (2008) Habitat use
of savanna elephants in southern Africa. Oryx, 42, 66-75.</mixed-citation>
         </ref>
         <ref id="d37e734a1310">
            <mixed-citation id="d37e738" publication-type="other">
Jackson, T.P., Mosojane, S., Ferreira, S.M. &amp; van Aarde, R.J. (2008) Solutions
for elephant Loxodonta africana crop raiding in northern Botswana: moving
away from symptomatic approaches. Oryx, 42, 1-9.</mixed-citation>
         </ref>
         <ref id="d37e751a1310">
            <mixed-citation id="d37e755" publication-type="other">
Junker, J. (2008) An analysis of numerical trends in African elephant populations.
MSc thesis, University of Pretoria, Pretoria South-Africa, pp. 179.</mixed-citation>
         </ref>
         <ref id="d37e765a1310">
            <mixed-citation id="d37e769" publication-type="other">
Krebs, C.J. (2003) Two complimentary paradigms for analysing popula-
tion dynamics. Wildlife Population Growth Rates (eds R.M. Silby, J.
Hone &amp; T.H. Clutton-Brock), pp. 110-126. Cambridge University Press,
Cambridge.</mixed-citation>
         </ref>
         <ref id="d37e785a1310">
            <mixed-citation id="d37e789" publication-type="other">
Lee, P.C. &amp; Moss, C.J. (1986) Early maternal investment in male and female
African elephant calves. Behavioural ecology and Sociobiology, 18, 353-361.</mixed-citation>
         </ref>
         <ref id="d37e800a1310">
            <mixed-citation id="d37e804" publication-type="other">
Leggett, K. (2003) Effect of artificial water points on the movement and behav-
ior of desert-dwelling elephants of north-western Namibia. Pachyderm, 40,
40-51.</mixed-citation>
         </ref>
         <ref id="d37e817a1310">
            <mixed-citation id="d37e821" publication-type="other">
Lehmann, A., Overton, J.Mc.C. &amp; Leathwick, J.R. (2002) GRASP: General-
ized Regression Analysis and Spatial Prediction. Ecological Modeling, 157,
189-207.</mixed-citation>
         </ref>
         <ref id="d37e834a1310">
            <mixed-citation id="d37e838" publication-type="other">
Loarie, S.R., van Aarde, R.J. &amp; Pimm, S.L. (2009) Fences and artificial water
distort elephant movements across wet and dry savannahs. Biological Con-
servation, 142, 3086-3098.</mixed-citation>
         </ref>
         <ref id="d37e851a1310">
            <mixed-citation id="d37e855" publication-type="other">
Loveridge, A.J., Hunt, J.E., Murindagomo, F. &amp; Macdonald, D.W. (2006)
Influence of drought on prédation of elephant (Loxodonta africana) calves
by lions (Panthera leo) in an African wooded savannah. Journal of Zoology,
270, 523-530.</mixed-citation>
         </ref>
         <ref id="d37e871a1310">
            <mixed-citation id="d37e875" publication-type="other">
Moss, C.J. (2001) The demography of an African elephant (Loxodonta afri-
cana) population in Amboseli, Kenya. Journal of Zoology, 255, 145-156.</mixed-citation>
         </ref>
         <ref id="d37e885a1310">
            <mixed-citation id="d37e889" publication-type="other">
Owen-Smith, R.N. (1988) Megaherbivores: The Influence of Very Large Body
Size on Ecology. Cambridge University Press, Cambridge.</mixed-citation>
         </ref>
         <ref id="d37e900a1310">
            <mixed-citation id="d37e904" publication-type="other">
Owen-Smith, N. (1996) Ecological guidelines for waterpoints in extensive pro-
tected areas. South African Journal of Wildlife Research, 26, 107-112.</mixed-citation>
         </ref>
         <ref id="d37e914a1310">
            <mixed-citation id="d37e918" publication-type="other">
Owen-Smith, N., Mason, D.R. &amp; Ogutu, J.O. (2005) Correlates of survival
rates for 10 African ungulate populations: density, rainfall and prédation.
Journal of Animal Ecology, 74, 774-788.</mixed-citation>
         </ref>
         <ref id="d37e931a1310">
            <mixed-citation id="d37e935" publication-type="other">
Owen-Smith, N., Kerley, G., Page, B., Slotow, R. &amp; van Aarde, R. (2006) A sci-
entific perspective on the management of elephants in the Kruger National
Park and elsewhere. South African Journal of Science, 102, 389-394.</mixed-citation>
         </ref>
         <ref id="d37e948a1310">
            <mixed-citation id="d37e952" publication-type="other">
Redfern, J.V., Grant, C.C., Gaylard, A. &amp; Getz, W.M. (2005) Surface water
availability and the management of herbivore distributions in an African
savanna ecosystem. Journal of Arid Environments, 63, 406-424.</mixed-citation>
         </ref>
         <ref id="d37e965a1310">
            <mixed-citation id="d37e969" publication-type="other">
Saether, B.E. (1997) Environmental stochasticity and population dynamics of
large herbivores: a search for mechanisms. Trends in Ecology and Evolution,
12, 143-149.</mixed-citation>
         </ref>
         <ref id="d37e982a1310">
            <mixed-citation id="d37e986" publication-type="other">
Scholes, R.J., Bond, W.J. &amp; Eckhardt, H.C. (2003) Vegetation dynamics in the
Kruger ecosystem. The Kruger Experience. Ecology and Management of
Savanna Heterogeneity (eds J.T. du Toit, K.H. Rogers &amp; H.C. Biggs), pp.
242-262. Island Press, Washington, DC, USA.</mixed-citation>
         </ref>
         <ref id="d37e1003a1310">
            <mixed-citation id="d37e1007" publication-type="other">
Sibly, R.M., Hone, J. &amp; Clutton-Brock, T.H. (2003) Wildlife Population Growth
Rates. Cambridge University Press, Cambridge.</mixed-citation>
         </ref>
         <ref id="d37e1017a1310">
            <mixed-citation id="d37e1021" publication-type="other">
Sinclair, A.R.E. (2003) Mammal population regulation, keystone processes
and ecosystem dynamics. Philosophical Transactions of the Royal Society of
London B, 358, 1729-1740.</mixed-citation>
         </ref>
         <ref id="d37e1034a1310">
            <mixed-citation id="d37e1038" publication-type="other">
Smit, I.P.J., Grant, C.C. &amp; Whyte, I.J. (2007) Landscape-scale segregation
in the dry season distribution and resource utilization of elephants in
Kruger National Park, South Africa. Diversity and Distributions, 13,
225-236.</mixed-citation>
         </ref>
         <ref id="d37e1054a1310">
            <mixed-citation id="d37e1058" publication-type="other">
Stokke, S. (1999) Sex differences in feeding-patch choice in megaherbivore: ele-
phants in Chobe National Park, Botswana. Canadian Journal of Zoology,
11, 1723-1732.</mixed-citation>
         </ref>
         <ref id="d37e1071a1310">
            <mixed-citation id="d37e1075" publication-type="other">
Stokke, S. &amp; du Toit, J.T. (2002) Sexual segregation in habitat use by elephants
in Chobe National Park, Botswana. African Journal of Ecology , 40, 360-371.</mixed-citation>
         </ref>
         <ref id="d37e1085a1310">
            <mixed-citation id="d37e1089" publication-type="other">
Trimble, M.J., Ferreira, S.M. &amp; van Aarde, R.J. (2009) Drivers of megaherbi-
vore demographic fluctuations: inference from elephants. Journal of Zool-
ogy, 279, 18-26.</mixed-citation>
         </ref>
         <ref id="d37e1103a1310">
            <mixed-citation id="d37e1107" publication-type="other">
Wang, G., Hobbs, N.T., Boone, R.B., Illius, A.W., Gordon, I.J., Gross, J.E. &amp;
Hamlin, K.L. (2006) Spatial and temporal variability modify density depen-
dence in populations of large herbivores. Ecology, 87, 95-102.</mixed-citation>
         </ref>
         <ref id="d37e1120a1310">
            <mixed-citation id="d37e1124" publication-type="other">
Wang, G., Hobbs, T., Twombly, S, Boone, R.B., Illius, A.W., Gordon, I.J. &amp;
Gross, J.E. (2009) Density dependence in northern ungulates: interactions
with prédation and resources. Population Ecology, 51, 123-132.</mixed-citation>
         </ref>
         <ref id="d37e1137a1310">
            <mixed-citation id="d37e1141" publication-type="other">
White, T.C.R. (2008) The role of food, weather and climate in limiting the
abundance of animals. Biological Review, 83, 227-248.</mixed-citation>
         </ref>
         <ref id="d37e1151a1310">
            <mixed-citation id="d37e1155" publication-type="other">
Whyte, I.J., van Aarde, R.J. &amp; Pimm, S.L. (2003) Kruger's elephant popula-
tion: its size and consequences for ecosystem heterogeneity. The Kruger
Experience: Ecology and Management of Savanna Heterogeneity (eds J.T. du
Toit, K.H. Rogers &amp; H.C. Biggs), pp. 332-348. Island Press, Washington,
DC.</mixed-citation>
         </ref>
         <ref id="d37e1174a1310">
            <mixed-citation id="d37e1178" publication-type="other">
Wittemyer, G. &amp; Getz, W.M. (2007) Testing socio-ecological predictions of
dominance relationships in a scramble competitor. Animal Behaviour, 73,
671-681.</mixed-citation>
         </ref>
         <ref id="d37e1191a1310">
            <mixed-citation id="d37e1195" publication-type="other">
Wittemyer, G., Rasmussen, H.B. &amp; Douglas-Hamilton, I. (2007a) Breeding
phenology in relation to NDVI variability in free-ranging African elephant.
Ecography, 30, 42-50.</mixed-citation>
         </ref>
         <ref id="d37e1209a1310">
            <mixed-citation id="d37e1213" publication-type="other">
Wittemyer, G., Getz, W., Volrath, F. &amp; Douglas-Hamilton, I. (2007b) Social
dominance, seasonal movements, and spatial segregation in African ele-
phants: a contribution to conservation behaviour. Behavioural Ecology and
Sociobiology, 61, 1919-1931.</mixed-citation>
         </ref>
         <ref id="d37e1229a1310">
            <mixed-citation id="d37e1233" publication-type="other">
Woolley, L., Mackey, R.L., Page, B.R. &amp; Slotow, R. (2008) Modelling the
effect of age-specific mortality on elephant Loxodonta africana populations:
can natural mortality provide regulation? Oryx, 42, 49-51.</mixed-citation>
         </ref>
         <ref id="d37e1246a1310">
            <mixed-citation id="d37e1250" publication-type="other">
Young, K.D., Ferreira, S.M. &amp; van Aarde, R.J. (2009a) Elephant spatial use in
wet and dry savannas of southern Africa. Journal of Zoology, 278, 189-205.</mixed-citation>
         </ref>
         <ref id="d37e1260a1310">
            <mixed-citation id="d37e1264" publication-type="other">
Young, K.D., Ferreira, S.M. &amp; van Aarde, R.J. (2009b) The influence of
increasing population size and vegetation productivity on elephant distribu-
tion in the Kruger National Park. Austral Ecology, 34, 329-342.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
