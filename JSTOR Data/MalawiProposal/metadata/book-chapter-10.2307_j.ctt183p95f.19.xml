<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt183p95f</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Fair Trade Revolution</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>Bowes</surname>
               <given-names>John</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745330785</isbn>
      <isbn content-type="epub">9781849645713</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>John Bowes</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt183p95f"/>
      <abstract abstract-type="short">
         <p>Fair Trade has come a long way in the last 20 years. The Fair Trade Revolution celebrates the movement's achievement and takes up the challenge of improving more lives through fair dealing with producers. Fair Trade is now mainstream, with large companies like Cadbury's and supermarkets such as Sainsbury's producing and stocking many Fair Trade products. The authors of this collection, many of whom were responsible for the initial success of Fair Trade, emphasise the importance of ensuring that farmers and other producers remain the main beneficiaries. Punchy chapters, illustrated with many real-world examples, cover all the important issues including the tensions between large and small operators, the impact of recession, environmental policy and the danger of large operators embracing Fair Trade more in word than in practice. Written by the leading lights of the Fair Trade movement, including Harriet Lamb (Executive Director of the Fairtrade Foundation) and Bruce Crowther (Establisher of the world's first Fair Trade Town) this book will inspire activists and consumers to keep making the right choices.</p>
      </abstract>
      <counts>
         <page-count count="288"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Robinson</surname>
                           <given-names>Mary</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>A few decades ago some pioneering individuals and organisations decided to put their values into practice with the development of something called ‘fair trade’. This book provides an insightful and important analysis of the development of the fair trade movement worldwide, built around the experiences and perspectives of several of the key individuals who led the way. As we reflect on the most important trends in business of the twentieth century, we certainly must include the expansion of fair trade products from a tiny niche market to a staple in many stores – and even more households – around the world. At</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bowes</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.5</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>A Brilliant Idea</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bowes</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>It is perhaps a little predictable to start a book about fair trade by referring to its dramatic growth over recent years. But the results have been truly impressive. The FAIRTRADE Mark is now almost ubiquitous and recognised by more than 70 per cent of adults in the United Kingdom.¹ And, in a decade, sales of Fairtrade products have increased more than forty-fold to a staggering £800 million in 2009.²</p>
                     <p>This is a spectacular achievement. It is a level of success which few can have anticipated.</p>
                     <p>Fair trade’s remarkable progress reflects the innate decency and instinctive humanitarianism of the UK</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.7</book-part-id>
                  <title-group>
                     <title>Colour Plates</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Impact of Fairtrade</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lamb</surname>
                           <given-names>Harriet</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>Rwanda’s red rolling hills are covered with millions of subdivided plots sprouting bright green crops. At the airport, a fellow passenger scrabbles around to hide a plastic bag – they are banned here. As he chats to his colleagues, I discover they are arriving for the celebrated Cup of Excellence Awards for top quality coffee. Ten years ago, as Rwanda emerged blinking from its vicious civil war, its coffee was off the map. Today, it’s scooping up awards. And coffee matters – to people and the whole country – making up 30 per cent of export earnings.</p>
                     <p>Before going, I</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Rachel’s Blog</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Archer</surname>
                           <given-names>Rachel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>40</fpage>
                  <abstract>
                     <p>I am going to introduce you to some of the inspiring people I have had the pleasure to meet over the last six years visiting farmers, workers and cooperative organisers in fair trade. They are just a few of a large and growing movement in different corners of the world, all linked by a desire to farm well, sell products for a fair and consistent price and improve conditions for their families and communities.</p>
                     <p>María Soveida de Mogollon is a tiny, delightful woman. A miniature tower of strength with fascinating hands – strong and twisted by years of work yet determined</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Nicaragua:</title>
                     <subtitle>The Road to Freedom</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Haslam</surname>
                           <given-names>Pedro</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hoskyns</surname>
                           <given-names>Nicholas</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>Nicaragua’s revolution triumphed on 19 July 1979. The Sandinistas came to power having overthrown the Somoza dictatorship. The revolution won the hearts of the vast majority of Nicaraguans and caught the imagination of the world. In the initial years there were huge and rapid advances in education, health and importantly for this chapter an extensive and successful land reform from 1980 to 1988 where valuable lands were given to the<italic>campesinos</italic>organised in cooperatives.</p>
                     <p>In response to this exciting revolutionary model in Central America, the newly elected US government presided over by Ronald Reagan financed the Contra war and imposed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Heroes and Demons</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kroezen</surname>
                           <given-names>Jeroen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>I first met Jorge Ramirez in January 1998. I was living in Quito at the time and flew down to Guayaquil; the big, chaotic, hot and dusty harbour city on Ecuador’s Pacific coast. From there I took a collective taxi, a yellow Chevrolet from the eighties, and embarked on a six hour journey along a pothole stricken road to Machala, the ‘banana capital of the world’.</p>
                     <p>I checked into the hotel Rizzo, where the corporate rate of $15 a night was reflected in the flaky paint work and worn out sheets. The hotel was close to the office of ExpoEcoAgro,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Campaigning for Justice</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Human</surname>
                           <given-names>Joe</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Crowther</surname>
                           <given-names>Bruce</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>Fairtrade has been a grassroots revolution. Just a few thousand committed, dedicated and like-minded individuals changed the attitudes of a nation and inspired a worldwide movement. By the time you read this there will be over 1,000 Fairtrade Towns¹ worldwide, including the cities of London, Paris, Rome, Brussels, Copenhagen, Wellington and San Francisco. The Fairtrade Town movement has the potential to create the largest single global campaign network ever seen and it all started in the very ordinary, traditional Lancashire market town of Garstang, with a population of just over 5,000. Fairtrade Towns are not about special people in special</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>A Glass and a Half Full – How Cadbury Embraced Fairtrade</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Croft</surname>
                           <given-names>David</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Cole</surname>
                           <given-names>Alex</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>107</fpage>
                  <abstract>
                     <p>When Cadbury announced that Britain’s most popular chocolate bar, Dairy Milk, was going Fairtrade in March 2009, many consumers wondered why it had taken so long. Yet those in the know wondered how it could be possible at all, given the move would more than double the amount of cocoa bought under the sustainable farming scheme. Could Fairtrade farmers supply such a massive increase in such a short time? And what impact would this have on the rest of the mainstream chocolate market?</p>
                     <p>The detail behind the headlines reveals a complex recipe of business realities, corporate values and heritage and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Honesty, Openness and Social Responsibility</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bowes</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>The Co-operative Group effectively pioneered the development of fair trade in the UK. Whilst the early marketing of fair trade products was essentially focused on alternative trading organisations, such as Oxfam, it was the Co-op who recognised the potential in the mainstream market. They seized the initiative and, between 1996 and 2003, they stole a march on all of their competitors by investing in an ambitious programme which, subsequently, became a catalyst for the growth of the whole sector.</p>
                     <p>The Co-op supported the concept of fair trade from the very start of this period. It began stocking Cafédirect, one of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Banana Breakthrough</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>North</surname>
                           <given-names>Matt</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>140</fpage>
                  <abstract>
                     <p>My first involvement and real understanding of the FAIRTRADE Mark began in 2005 when I became the banana and citrus fruit buyer at Sainsbury’s, 17 years after originally joining the supermarket chain as a trainee manager. During this time I had worked in the Retail, Property, Marketing and subsequently in its Trading divisions, meeting and working with customers and suppliers alike. This was all pretty straightforward commercial stuff until I was fortunate enough to meet banana growers in the developing world.</p>
                     <p>By 2006, one in five Sainsbury’s customers was already purchasing Fairtrade bananas, choosing to purchase the Fairtrade pack which</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.16</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Greatest Challenge</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rosenthal</surname>
                           <given-names>Jonathan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>In the United States, a common misconception prevails that fair trade was invented at the turn of the millennium with the introduction of fair trade certification. Surprisingly, US social justice and trade activists have been looking at changing the rules of trade and debating different theories of how to do that for several centuries. The strategic positioning of abolitionist groups in the 1800s parallels the strategy debates among sectors of the fair trade movement today.</p>
                     <p>To better understand where the US fair trade movement is, it is useful to look back at where we have been. The debate in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.17</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Tricky Waters</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mathew</surname>
                           <given-names>Tomy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>173</fpage>
                  <abstract>
                     <p>I’m writing from Kerala where it is an unusually hot summer. Heat stroke, unheard of here until this year, has been reported from several places in the state. Fair Trade Alliance Kerala (FTAK), a farming organisation with a membership expected to produce a cashew yield of more than 2,000 metric tonnes, will struggle to meet its contractual obligations. For the second year running, the size of the crop will be half the expected harvest.</p>
                     <p>The reservoirs of our dams are turning into parched dry beds and the Kerala State Electricity Board, dependent as it is on hydroelectric power, is talking</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.18</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Scale without Compromise</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lamb</surname>
                           <given-names>Harriet</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>The old former worker from the Chamraj tea estate in southern India was delighted. ‘My sons used to argue about who had to look after me now I am old,’ she said. ‘Now, thanks to Fairtrade, I have a pension and they are fighting to have me stay with them!’</p>
                     <p>Mr Henriksen, the ebullient Fairtrade Coordinator on the estate, chuckles as he tells the story, but pauses to reflect on his sadness about the terrible position of retired people when money is desperately tight and they are treated as a burden. That’s why he is so very proud of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.19</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Raising the Bar or Directing the Flood</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Murray</surname>
                           <given-names>Robin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>205</fpage>
                  <abstract>
                     <p>There is scarcely a corner of Britain which does not have an active presence in fair trade. In Keswick there are regular morning breakfasts of fair traders. On the borders you find fair trade goods in outby farmhouses. Co-op delivery wagons travel the motorways with ‘Fairtrade’ emblazoned in great letters on their sides. Schools, universities, and local government offices have become promoters of fair trade. In Lewisham borough council, the staff canteen has become a fair trade canteen, with pictures of farmers and their statements covering the walls. In less than a decade fair trade has changed from a practical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.20</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>When the Rain Stops</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bowes</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>227</fpage>
                  <abstract>
                     <p>In his classic novel,<italic>One Hundred Years of Solitude</italic>, Gabriel Garcia Marquez refers to an historical event which took place in December 1928. ‘Gabo’ was just 20 months old when 30,000 Colombian banana workers went on strike for better pay and working conditions. The Colombian army, after its officers had allegedly been lavishly entertained by the United Fruit Company, opened fire on more than 3,000 workers occupying the main square in Cienaga. Nobody knows for sure how many died; perhaps as many as 1,000 povertystricken banana workers breathed their last on the single most controversial day in Colombian history.¹ This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p95f.21</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
