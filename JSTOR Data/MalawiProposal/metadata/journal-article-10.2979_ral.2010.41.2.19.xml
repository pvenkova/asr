<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">reseafrilite</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Research in African Literatures</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Indiana University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00345210</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15272044</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">RAL.41.2.19</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2979/RAL.2010.41.2.19</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>ARTICLES</subject>
            </subj-group>
            <subj-group subj-group-type="subject">
               <subject>African studies, literary studies</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Cannibalization as Popular Tradition in Igbo Masquerade Performance</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Okoye</surname>
                  <given-names>Chukwuma</given-names>
               </string-name>
               <aff>University of Ibadan, Nigeria. chukwumaok@yahoo.com</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <month>June</month>
            <year>2010</year>
            <string-date>SUMMER 2010</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">41</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">ral.2010.41.issue-2</issue-id>
         <fpage>19</fpage>
         <lpage>31</lpage>
         <permissions>
            <copyright-statement>Copyright ©2010 Indiana University Press</copyright-statement>
            <copyright-year>2010</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/10.2979/ral.2010.41.2.19"/>
         <abstract>
            <title>Abstract</title>
            <p>Most of the critical strategies deployed in the investigation of the postcolonial condition of African cultures, such as hybridity and creolization, celebrate the emergence of a somewhat syncretist new culture. Notably, these strategies fail to sufficiently typify situations where one culture violently extracts cultural “pieces” from another for its own nourishment rather than the production of a new ethos. Describing this encounter as “cannibalization,” this paper argues that African communities have always scrutinized their contact with foreign cultures, accepting, rejecting, and appropriating practices and artifacts according to their needs, while still maintaining an underlying loyalty to their indigenous processes. This “tradition” is exemplified in Igbo masquerade performance, which adopts an expropriatory strategy by which it subjects Euro-American cultural forms to a process of indigenization, stripping them of their original symbolic equipment and immersing them in entirely local ones. Thus, rather than producing a qualitatively new performance form, these pieces sustain the Igbo masquerade tradition.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>NOTES</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="other">Reed notes “how interesting it was that ‘the tradition’ of Gedro—this performance of Dan religious identity—had become a kind of regional pop star and was for many in Man just another form of popular entertainment” (<fpage>62</fpage>). And Birch de Aguilar notes, “The oldest forms of masking, the mythical animal constructions have also undergone historic change. The large animal constructions now include similar constructions of cars, Galimoto, and helicopters, and a bus whirling round the night-time<italic>bwalo</italic>full of passengers” (<fpage>78</fpage>).</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="other">In this case I ignore its anthropologic sense and the tropology of “the cannibal” discourse as a paranoiac construction of the colonized by the colonizer (see<person-group person-group-type="author">
                  <string-name>Slemon</string-name>
               </person-group>).</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="other">The<italic>Atumma</italic>face is the typical Igbo maiden mask characterized by finely defined features with a small straight nose and thin lips. However, instead of the predominantly white color, the face was painted brilliant yellow with an elaborate headdress of cascading light-brown raffia.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list content-type="parsed-citations">
         <title>WORKS CITED</title>
         <ref id="ref4">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Achebe, Chinua</string-name>
               </person-group>.<article-title>“The Igbo World and Its Art.”</article-title>
               <source>
                  <italic>Hopes and Impediments</italic>
               </source>.:<publisher-name>Heinemann</publisher-name>,<year>1988</year>.<fpage>42</fpage>–<lpage>5</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="journal">
               <source>
                  <italic>African Arts</italic>
               </source>
               <volume>35</volume>.<issue>2</issue>(<year>2002</year>):<fpage>5</fpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Arnoldi, Mary Jo</string-name>
               </person-group>.<source>
                  <italic>Playing with Time: Art and Performance in Central Mali</italic>
               </source>.:<publisher-name>Indiana UP</publisher-name>,<year>1995</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Barber, Karin</string-name>
               </person-group>.<article-title>“Popular Arts in Africa.”</article-title>
               <source>
                  <italic>African Studies Review</italic>
               </source>
               <volume>30</volume>.<issue>3</issue>(<year>1987</year>):<fpage>1</fpage>–<lpage>78</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Bascom, William R.</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Melville H. Herskovits</string-name>
               </person-group>.<year>1959</year>.<source>
                  <italic>Continuity and Change in African Cultures</italic>
               </source>.:<publisher-name>U of Chicago P</publisher-name>,<year>1975</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Bhabha, Homi</string-name>
               </person-group>.<article-title>“The Commitment to Theory.”</article-title>
               <source>
                  <italic>The Location of Culture</italic>
               </source>.:<publisher-name>Routledge Classics</publisher-name>,<year>2004</year>.<fpage>28</fpage>–<lpage>56</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Birch de Aguilar, Laurel</string-name>
               </person-group>.<source>
                  <italic>Inscribing the Mask: Interpretation of Nyau Masks and Ritual Performance among the Chewa of Central Malawi</italic>
               </source>.<series>Studia Instituti Anthropos 47</series>.:<publisher-name>Fribourg UP</publisher-name>,<year>1996</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Childs, Peter</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Patrick Williams</string-name>
               </person-group>.<source>
                  <italic>An Introduction to Post-Colonial Theory</italic>
               </source>.:<publisher-name>Longman</publisher-name>,<year>1997</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Cole, Herbert. M.</string-name>
               </person-group>
               <article-title>“Introduction: The Mask, Masking and Masquerade</article-title>.<source>
                  <italic>“I am not myself”: The Art of African Masquerade</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Herbert M. Cole</string-name>
               </person-group>.:<publisher-name>Museum of Cultural History, U of California</publisher-name>,<year>1985</year>.<fpage>15</fpage>–<lpage>27</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Coplan, David B.</string-name>
               </person-group>
               <article-title>“Sounds of the ‘Third Way’–Zulu Maskanda, South African Popular Traditional Music.”</article-title>
               <source>
                  <italic>Playing with Identities in Contemporary Music in Africa</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Mai Palmberg</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>Annemette Kirkegaard</string-name>
               </person-group>.:<publisher-name>Nordiska Afrikainstitutet</publisher-name>,<year>2000</year>.<fpage>104</fpage>–<lpage>16</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Duerden, Dennis</string-name>
               </person-group>.<article-title>“The ‘Discovery’ of the African Mask.”</article-title>
               <source>
                  <italic>Research in African Literatures</italic>
               </source>
               <volume>31</volume>.<issue>4</issue>(<year>2000</year>):<fpage>29</fpage>–<lpage>47</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Gyekye, Kwame</string-name>
               </person-group>.<source>
                  <italic>Tradition and Modernity: Philosophical Reflections on the African Experience</italic>
               </source>.:<publisher-name>Oxford UP</publisher-name>,<year>1997</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Hufbauer, Benjamin</string-name>
               </person-group>, and<person-group person-group-type="author">
                  <string-name>Bess Reed</string-name>
               </person-group>.<article-title>“Adamma: A Contemporary Igbo Maiden Spirit.”</article-title>
               <source>
                  <italic>African Arts</italic>
               </source>
               <volume>36</volume>.<issue>3</issue>(<year>2003</year>):<fpage>56</fpage>–<lpage>65</lpage>,<fpage>94</fpage>–<lpage>95</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Jewsiewicki, Bogumil</string-name>
               </person-group>.<article-title>“Painting in Zaire: From the Invention of the West to the Representation of Social Self.”</article-title>
               <source>
                  <italic>Readings in African Popular Culture</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Karin Barber</string-name>
               </person-group>.:<publisher-name>Indiana U P</publisher-name>;:<publisher-name>James Currey</publisher-name>,<year>1997</year>.<fpage>99</fpage>–<lpage>110</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Kasfir, Sidney Littlefield</string-name>
               </person-group>.<article-title>“Museums and Contemporary African Art: Some Questions for Curators.”</article-title>
               <source>
                  <italic>African Arts</italic>
               </source>
               <volume>35</volume>.<issue>2</issue>(<year>2002</year>):<fpage>9</fpage>,<fpage>87</fpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Lamp, Frederick. J.</string-name>
               </person-group>
               <article-title>“Overture: a Consideration of the Fragment.”</article-title>
               <source>
                  <italic>See the Music and Hear the Dance: Rethinking African Art at the Baltimore Museum of Art</italic>
               </source>.:<publisher-name>Prestel</publisher-name>,<year>2004</year>.<fpage>19</fpage>–<lpage>27</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Lindfors, Bernth</string-name>
               </person-group>.<article-title>“Heroes and Hero-Worship in Nigerian Chapbooks.”</article-title>
               <source>
                  <italic>Journal of Popular Culture</italic>
               </source>
               <volume>1</volume>.<issue>1</issue>(<year>1967</year>):<fpage>1</fpage>–<lpage>22</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Makang, Jean-Marie</string-name>
               </person-group>.<article-title>“Of the Good Use of Tradition: Keeping the Critical Perspective in African Philosophy</article-title>.<source>
                  <italic>Postcolonial African Philosophy: A Critical Reader</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Emmanuel Chukwudi Eze</string-name>
               </person-group>.:<publisher-name>Blackwell</publisher-name>,<year>1997</year>.<fpage>324</fpage>–<lpage>38</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ottenberg, Simon</string-name>
               </person-group>.<article-title>“Ibo Receptivity to Change.”</article-title>
               <year>1959</year>.<source>
                  <italic>Continuity and Change in African Culture</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>W. R. Bascom</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>M. J. Herskovits</string-name>
               </person-group>.:<publisher-name>U of Chicago P</publisher-name>,<year>1975</year>.<fpage>130</fpage>–<lpage>43</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ranger, Terence</string-name>
               </person-group>.<article-title>“The Invention of Tradition in Colonial Africa.”</article-title>
               <source>
                  <italic>The Invention of Tradition</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Eric Hobsbawn</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>Terence Ranger</string-name>
               </person-group>.:<publisher-name>Cambridge UP</publisher-name>,<year>1992</year>.<fpage>211</fpage>–<lpage>62</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Reed, Daniel B.</string-name>
               </person-group>
               <source>
                  <italic>Dan Ge Performance: Masks and Music in Contemporary Cote d'Ivoire</italic>
               </source>.:<publisher-name>Indiana UP</publisher-name>,<year>2003</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Slemon, Stephen</string-name>
               </person-group>.<article-title>“Bones of Contention: Post-colonial Writing and the ‘Cannibal’ Question.”</article-title>
               <source>
                  <italic>Literature and the Body</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Anthony Purdy</string-name>
               </person-group>.:<publisher-name>Rodopi</publisher-name>,<year>1992</year>.<fpage>163</fpage>–<lpage>67</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Waterman, Christopher A.</string-name>
               </person-group>
               <article-title>“ ‘Our tradition is a very modern tradition’: Popular Music and the Construction of Pan-Yoruba Identity.”</article-title>
               <source>
                  <italic>Readings in African Popular Culture</italic>
               </source>. Ed.<person-group person-group-type="editor">
                  <string-name>Karen Barber</string-name>
               </person-group>.:<publisher-name>Indiana UP</publisher-name>;:<publisher-name>James Currey</publisher-name>,<year>1997</year>.<fpage>48</fpage>–<lpage>53</lpage>.<comment>Print</comment>.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Young, Robert J. C.</string-name>
               </person-group>
               <source>
                  <italic>Postcolonialism: A Very Short Introduction</italic>
               </source>.:<publisher-name>Oxford UP</publisher-name>,<year>2003</year>.<comment>Print</comment>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
