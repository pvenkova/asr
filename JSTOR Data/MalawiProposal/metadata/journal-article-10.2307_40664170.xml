<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">intestudquar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100193</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>International Studies Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Periodicals, Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00208833</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14682478</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40664170</article-id>
         <title-group>
            <article-title>Dealing with Tyranny: International Sanctions and the Survival of Authoritarian Rulers</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Abel</given-names>
                  <surname>Escribà-Folch</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Joseph</given-names>
                  <surname>Wright</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">54</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40028894</issue-id>
         <fpage>335</fpage>
         <lpage>359</lpage>
         <permissions>
            <copyright-statement>© 2010 International Studies Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1111/j.1468-2478.2010.00590.x"
                   xlink:title="an external site"/>
         <abstract>
            <p>This paper examines whether economic sanctions destabilize authoritarian rulers. We argue that the effect of sanctions is mediated by the type of authoritarian regime against which sanctions are imposed. Because personalist regimes and monarchies are more sensitive to the loss of external sources of revenue (such as foreign aid and taxes on trade) to fund patronage, rulers in these regimes are more likely to be destabilized by sanctions than leaders in other types of regimes. In contrast, when dominant single-party and military regimes are subject to sanctions, they increase their tax revenues and reallocate their expenditures to increase their levels of cooptation and repression. Using data on sanction episodes and authoritarian regimes from 1960 to 1997 and selection-corrected survival models, we test whether sanctions destabilize authoritarian rulers in different types of regimes. We find that personalist dictators are more vulnerable to foreign pressure than other types of dictators. We also analyze the modes of authoritarian leader exit and find that sanctions increase the likelihood of a regular and an irregular change of ruler, such as a coup, in personalist regimes. In single-party and military regimes, however, sanctions have little effect on leadership stability.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d301e219a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d301e226" publication-type="other">
http://www.whitehouse.gov/inaugural/(accessed June 2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e233a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d301e240" publication-type="other">
Funded by the Central Intelligence Agency, British intelligence, and the Saudis, the INA staged a failed coup
attempt in 1996.</mixed-citation>
            </p>
         </fn>
         <fn id="d301e250a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d301e257" publication-type="other">
Nurnberger (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e264a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d301e271" publication-type="other">
Kennedy administration reduced the Dominican Republic's sugar quota to pre-1960 levels
(Schreiber 1973).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e282a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d301e289" publication-type="other">
(Haggard and
Kaufman 1995: chapter 7;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d301e298" publication-type="other">
Geddes 1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d301e304" publication-type="other">
(Greene 2010).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e311a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d301e318" publication-type="other">
Magaloni (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e325a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d301e332" publication-type="other">
Gandhi and Przeworski (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e339a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d301e346" publication-type="other">
(Robinson and Torvik 2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e353a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d301e360" publication-type="other">
Marinov (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e367a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d301e374" publication-type="other">
Goemans, Gleditsch, and Chiozza (2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e382a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d301e389" publication-type="other">
Davenport's (2007</mixed-citation>
            </p>
         </fn>
         <fn id="d301e396a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d301e403" publication-type="other">
("Threat and Impo-
sition of Sanctions" Cliff Morgan, Valentin Krustev, Navin Bapat: http://www.unc.edu/bapat/TIES.htm).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e413a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d301e420" publication-type="other">
Geddes (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e427a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d301e434" publication-type="other">
Gasiorowski (1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e441a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d301e448" publication-type="other">
Przeworski, Alvarez, Antonio Cheibub, and
Limongi's (2000) ACLP.</mixed-citation>
            </p>
         </fn>
         <fn id="d301e458a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d301e465" publication-type="other">
(GAO 2002).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d301e471" publication-type="other">
(The Economist, July 7, 2001, 45-46).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e479a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d301e486" publication-type="other">
(Ross 2008),</mixed-citation>
            </p>
         </fn>
         <fn id="d301e493a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d301e500" publication-type="other">
Carter and Signorino (2007).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d301e506" publication-type="other">
Beck, Ratz, and Tucker (1998)</mixed-citation>
            </p>
         </fn>
         <fn id="d301e513a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d301e520" publication-type="other">
(Ross 2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d301e527a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d301e534" publication-type="other">
(Rowe 2001;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d301e540" publication-type="other">
Hufbauer, Schott, Elliott, and Oegg 2007).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d301e546" publication-type="other">
(Hufbauer et al. 2007).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d301e562a1310">
            <mixed-citation id="d301e566" publication-type="other">
Arendt, Hannah. (1962) The Origins of Totalitarianism. New York: Meridian.</mixed-citation>
         </ref>
         <ref id="d301e573a1310">
            <mixed-citation id="d301e577" publication-type="other">
Askari, Hossen G., John Forrer, Hildy Teegen, and Jiawen Yang. (2003) Economic Sanctions:
Explaining their Philosophy and Efficacy. Westport, CT: Praeger.</mixed-citation>
         </ref>
         <ref id="d301e587a1310">
            <mixed-citation id="d301e591" publication-type="other">
Beck, Nathaniel, Jonathan Katz, and Richard Tucker. (1998) Taking Time Seriously: Time-
Series-Cross-Section Analysis with a Binary Dependent Variable. American fournal of Political
Science 42 (4): 1260-1288.</mixed-citation>
         </ref>
         <ref id="d301e604a1310">
            <mixed-citation id="d301e608" publication-type="other">
Bertocchi, Graziella, and Michael Spagat. (2001) The Politics of Co-optation, fournal of Compara-
tive Economics 29 (4): 591-607.</mixed-citation>
         </ref>
         <ref id="d301e619a1310">
            <mixed-citation id="d301e623" publication-type="other">
Blaydes, Lisa. (2008) Electoral Budget Cycles under Authoritarianism: Economic Opportunism in
Mubarak's Egypt. Unpublished manuscript, Palo Alto, CA, Stanford University.</mixed-citation>
         </ref>
         <ref id="d301e633a1310">
            <mixed-citation id="d301e637" publication-type="other">
Box-Steffensmeier, Janet M., Dan Reiter, and Christopher Zorn. (2003) Nonproportional
Hazards and Event History Analysis in International Relations, fournal of Conflict Resolution 47
(1): 33-53.</mixed-citation>
         </ref>
         <ref id="d301e650a1310">
            <mixed-citation id="d301e654" publication-type="other">
Bratton, Michael, and Nicolas van de Walle. (1994) Patrimonial Regimes and Political Transi-
tions in Africa. World Politics 46 (4): 453-489.</mixed-citation>
         </ref>
         <ref id="d301e664a1310">
            <mixed-citation id="d301e668" publication-type="other">
Bratton, Michael, and Nicolas van de Walle. (1997) Democratic Experiments in Africa. Regime Tran-
sitions in Comparative Perspective. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d301e678a1310">
            <mixed-citation id="d301e682" publication-type="other">
Bräutigam, Deborah. (2000) Aid, Dependence, and Governance. Stockholm: Almqvist and Wiksell.</mixed-citation>
         </ref>
         <ref id="d301e689a1310">
            <mixed-citation id="d301e693" publication-type="other">
Brough, Wayne T., and Mwangi S. Kimenyi. (1986) On the Inefficient Extraction of Rents by
Dictators. Public Choice 48 (1): 37-48.</mixed-citation>
         </ref>
         <ref id="d301e704a1310">
            <mixed-citation id="d301e708" publication-type="other">
Brownlee, Jason. (2004) Ruling Parties and Durable Authoritarianism. Paper presented at the
American Political Science Association's annual meeting, Chicago, IL, September 2-5.</mixed-citation>
         </ref>
         <ref id="d301e718a1310">
            <mixed-citation id="d301e722" publication-type="other">
Bueno de Mequita, Bruce, Alastair Smith, Randolph Siverson, and James Morrow. (2003)
Logic of Political Survival Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d301e732a1310">
            <mixed-citation id="d301e736" publication-type="other">
Carter, David, and Curt Signorino. (2007) Back to the Future: Modeling Time Dependence in
Binary Data. Paper presented at the American Political Science Association's annual meeting,
Chicago, IL, August 30 -September 2.</mixed-citation>
         </ref>
         <ref id="d301e749a1310">
            <mixed-citation id="d301e753" publication-type="other">
Dashti-Gibson, Jaleh, Patricia Davis, and Benjamin Radcliff. (1997) On the Determinants of
the Success of Economic Sanctions: An Empirical Analysis. American Journal of Political Science 41
(2): 608-618.</mixed-citation>
         </ref>
         <ref id="d301e766a1310">
            <mixed-citation id="d301e770" publication-type="other">
Davenport, Christian. (2007) State Repression and the Tyrannical Peace. Journal of Peace Research
44 (4): 485-504.</mixed-citation>
         </ref>
         <ref id="d301e780a1310">
            <mixed-citation id="d301e784" publication-type="other">
Escribà-Folch, Abel. (2007) La Economía Política de la Supervivencia de los Dictadores. Revista
Española de Ciencia Política 16: 109-132.</mixed-citation>
         </ref>
         <ref id="d301e795a1310">
            <mixed-citation id="d301e799" publication-type="other">
Falk, Richard A. (1992) Theoretical Foundations of Human Rights. In Human Rights in the World
Community: Issues and Action, edited by Richard Pierre Claude and Burns H. Weston. Philadel-
phia, PA: University of Pennsylvania Press.</mixed-citation>
         </ref>
         <ref id="d301e812a1310">
            <mixed-citation id="d301e816" publication-type="other">
Feierabend, Rosalind, Ivo Feierabend, and Ted R. Gurr. (1972) Anger, Violence, and Politics.
Englewood Cliffs, NJ: Prentice-Hall.</mixed-citation>
         </ref>
         <ref id="d301e826a1310">
            <mixed-citation id="d301e830" publication-type="other">
Francisco, Ronald A. (1995) The Relationship between Coercion and Protest: An Empirical
Evaluation in Three Coercive States. Journal of Conflict Resolution 39 (2): 263-282.</mixed-citation>
         </ref>
         <ref id="d301e840a1310">
            <mixed-citation id="d301e844" publication-type="other">
Friedrich, Carl J., and Zbigniew K. Brzezinski. (1961) Totalitarian Dictatorship and Autocracy. New
York: Praeger.</mixed-citation>
         </ref>
         <ref id="d301e854a1310">
            <mixed-citation id="d301e858" publication-type="other">
Galetovic, Alexander, and Ricardo Sanhueza. (2000) Citizens, Autocrats, and Plotters: A Model
and New Evidence on Coups d'État. Economics and Politics 12 (2): 183-204.</mixed-citation>
         </ref>
         <ref id="d301e868a1310">
            <mixed-citation id="d301e872" publication-type="other">
Gandhi, Jennifer, and Adam Przeworski. (2006) Cooperation, Cooptation, and Rebellion under
Dictatorships. Economics and Politics 18 (1): 1-26.</mixed-citation>
         </ref>
         <ref id="d301e883a1310">
            <mixed-citation id="d301e887" publication-type="other">
Gandhi, Jennifer, and Adam Przeworski. (2007) Authoritarian Institutions and the Survival of
Autocrats. Comparative Political Studies 40 (11): 1279-1301.</mixed-citation>
         </ref>
         <ref id="d301e897a1310">
            <mixed-citation id="d301e901" publication-type="other">
GAO. (2002) U.N. Confronts Significant Challenges in Implementing Sanctions against Iraq. Washington,
DC: GAO-02-625.</mixed-citation>
         </ref>
         <ref id="d301e911a1310">
            <mixed-citation id="d301e915" publication-type="other">
Gasiorowski, Mark. (1995) Economic Crisis and Political Regime Change: An Event History Analy-
sis. American Political Science Review 89 (4): 882-887.</mixed-citation>
         </ref>
         <ref id="d301e925a1310">
            <mixed-citation id="d301e929" publication-type="other">
Geddes, Barbara. (1999) Authoritarian Breakdown: Empirical Test of a Game Theoretic Argu-
ment. Paper presented at the American Political Science Association's annual meeting,
Atlanta, GA, September 2-5.</mixed-citation>
         </ref>
         <ref id="d301e942a1310">
            <mixed-citation id="d301e946" publication-type="other">
Geddes, Barbara. (2003) Paradigms and Sand Castles. Ann Arbor: University of Michigan Press.</mixed-citation>
         </ref>
         <ref id="d301e953a1310">
            <mixed-citation id="d301e957" publication-type="other">
Geddes, Barbara. (2004) Auuioritarian Breakdown. Unpublished manuscript, Department of
Political Science, UCLA.</mixed-citation>
         </ref>
         <ref id="d301e968a1310">
            <mixed-citation id="d301e972" publication-type="other">
Geddes, Barbara. (2008) Party Creation as an Autocratic Survival Strategy. Paper presented at
"Dictatorships: Their Governance and Social Consequences" Conference at Princeton University,
April 25-26.</mixed-citation>
         </ref>
         <ref id="d301e985a1310">
            <mixed-citation id="d301e989" publication-type="other">
Gershenson, Dmitriy, and Herschel I. Grossman. (2001) Cooption and Repression in the Soviet
Union. Economics and Politics 13 (1): 31-47.</mixed-citation>
         </ref>
         <ref id="d301e999a1310">
            <mixed-citation id="d301e1003" publication-type="other">
Gleditsch, Kristian S., and Michael D. Ward. (2006) Diffusion and the International Context of
Democratization. International Organization 60 (4): 911-933.</mixed-citation>
         </ref>
         <ref id="d301e1013a1310">
            <mixed-citation id="d301e1017" publication-type="other">
Goemans, Hein, Kristian S. Gleditsch, and Giacomo Chiozza. (2009) Introducing Archigpsr.
A Data Set of Political Leaders, 1875-2003. Journal of Peace Research 46 (2): 269-283.</mixed-citation>
         </ref>
         <ref id="d301e1027a1310">
            <mixed-citation id="d301e1031" publication-type="other">
Greene, Kenneth F. (2010) The Political Economy of Authoritarian Single-Party Dominance.
Comparative Political Studies 43 (9): 1-27.</mixed-citation>
         </ref>
         <ref id="d301e1041a1310">
            <mixed-citation id="d301e1045" publication-type="other">
Haas, Richard N. (1997) Sanctioning Madness. Foreign Affairs 76 (6): 74-85.</mixed-citation>
         </ref>
         <ref id="d301e1053a1310">
            <mixed-citation id="d301e1057" publication-type="other">
Hafner-Burton, Emilie M., and Kiyoteru Tsutsui. (2007) Justice Lost! The Failure of International
Human Rights Law to Matter Where Needed Most. Journal of Peace Research 44 (4): 407-425.</mixed-citation>
         </ref>
         <ref id="d301e1067a1310">
            <mixed-citation id="d301e1071" publication-type="other">
Haggard, Stephen, and Robert R. Kaufman. (1995) The Political Economy of Democratic Transitions.
Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d301e1081a1310">
            <mixed-citation id="d301e1085" publication-type="other">
Hufbauer, Gary C, Jeffrey J. Schott, and Kimberley A. Elliott. (1990) Economic Sanctions Recon-
sidered: History and Current Policy. Washington, DC: Peterson Institute for International Econom-
ics.</mixed-citation>
         </ref>
         <ref id="d301e1098a1310">
            <mixed-citation id="d301e1102" publication-type="other">
Hufbauer, Gary C, Jeffrey J. Schott, Kimberley A. Elliott, and Barbara Oegg. (2007)
Economic Sanctions Reconsidered, 3rd edition. Washington, DC: Peterson Institute for International
Economics.</mixed-citation>
         </ref>
         <ref id="d301e1115a1310">
            <mixed-citation id="d301e1119" publication-type="other">
Humphreys, Macartan. (2005) Natural Resources, Conflict and Conflict Resolution. Journal of
Conflict Resolution 49 (4): 508-537.</mixed-citation>
         </ref>
         <ref id="d301e1129a1310">
            <mixed-citation id="d301e1133" publication-type="other">
Hunter, Wendy. (1997) Eroding Military Influence in Brazil: Politicians against Soldiers. Chapel Hill:
University of North Carolina Press.</mixed-citation>
         </ref>
         <ref id="d301e1144a1310">
            <mixed-citation id="d301e1148" publication-type="other">
Jackson, Robert H., and Carl G. Rosberg. (1984) Personal Rule: Theory and Practice in Africa.
Comparative Politics 16 (4): 421-442.</mixed-citation>
         </ref>
         <ref id="d301e1158a1310">
            <mixed-citation id="d301e1162" publication-type="other">
Johnson, Thomas H., Robert O. Slater, and Pat McGowan. (1984) Explaining African Military
Coups d'État, 1960-1982. American Political Science Review 78 (3): 622-640.</mixed-citation>
         </ref>
         <ref id="d301e1172a1310">
            <mixed-citation id="d301e1176" publication-type="other">
Kaempfer, William H., and Anton D. Lowenberg. (1988) The Theory of International Economic
Sanctions: A Public Choice Approach. American Economic Review 78 (4): 786-793.</mixed-citation>
         </ref>
         <ref id="d301e1186a1310">
            <mixed-citation id="d301e1190" publication-type="other">
Kaempfer, William H., Anton D. Lowenberg, and William Mertens. (2004) International
Economic Sanctions against a Dictator. Economics and Politics 16 (1): 29-51.</mixed-citation>
         </ref>
         <ref id="d301e1200a1310">
            <mixed-citation id="d301e1204" publication-type="other">
Kasza, Gregory J. (1995a) The Conscription Society: Administered Mass Organizations. New Haven, CT:
Yale University Press.</mixed-citation>
         </ref>
         <ref id="d301e1214a1310">
            <mixed-citation id="d301e1218" publication-type="other">
Kasza, Gregory J. (1995b) Weapons of the Strong: Organization and Terror. In Politics, Society, and
Democracy. Comparative Studies, edited by H. E. Chehabi and Alfred Stepan. Boulder, CO:
Westview.</mixed-citation>
         </ref>
         <ref id="d301e1232a1310">
            <mixed-citation id="d301e1236" publication-type="other">
Kirshner, Jonathan. (1997) The Microfoundations of Economic Sanctions. Security Studies 6 (3):
32-64.</mixed-citation>
         </ref>
         <ref id="d301e1246a1310">
            <mixed-citation id="d301e1250" publication-type="other">
Lai, Brian, and Dan Slater. (2006) Institutions of the Offensive: Domestic Sources of Dispute Initi-
ation in Authoritarian Regimes, 1950-1992. American Journal of Political Science 50 (1): 113-126.</mixed-citation>
         </ref>
         <ref id="d301e1260a1310">
            <mixed-citation id="d301e1264" publication-type="other">
Lektzian, David, and Mark Souva. (2007) An Institutional Theory of Sanctions Onset and Success.
Journal of Conflict Resolution 51 (6): 848-871.</mixed-citation>
         </ref>
         <ref id="d301e1274a1310">
            <mixed-citation id="d301e1278" publication-type="other">
Lieberman, Evan b. (2002) Taxation Data as Indicators or atate-bociety Relations: Possibilities and
Pitfalls in Cross-National Research. Studies in Comparative International Development 36 (4): 89-115.</mixed-citation>
         </ref>
         <ref id="d301e1288a1310">
            <mixed-citation id="d301e1292" publication-type="other">
Londregan, John B., and Keith T. Poole. (1990) Poverty, the Coup Trap, and the Seizure or Exec-
utive Power. Wtrrld Politics 42 (2): 151-183.</mixed-citation>
         </ref>
         <ref id="d301e1302a1310">
            <mixed-citation id="d301e1306" publication-type="other">
Maddison, Angus. (2006) World Population, GDP and Per Capita GDP, 1-2003 AD. Available at
http://www.ggdc.net/maddison/. (Accessed April 30, 2010.)</mixed-citation>
         </ref>
         <ref id="d301e1317a1310">
            <mixed-citation id="d301e1321" publication-type="other">
Magaloni, Beatriz. (2006) Voting for Autocracy. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d301e1328a1310">
            <mixed-citation id="d301e1332" publication-type="other">
Marino v, Nikolay. (2005) Do Sanctions Destabilize Country Leaders? American Journal of Political
Science 49 (3): 564-576.</mixed-citation>
         </ref>
         <ref id="d301e1342a1310">
            <mixed-citation id="d301e1346" publication-type="other">
Milner, Helen, and Keiko Kubota. (2005) Why the Move to Free Trade? Democracy and Trade
Policy in the Developing Countries. International Organization 59 (1): 107-143.</mixed-citation>
         </ref>
         <ref id="d301e1356a1310">
            <mixed-citation id="d301e1360" publication-type="other">
Morrison, Kevin M. (2009) Oil, Nontax Revenue, and the Redistributional Foundations of Regime
Stability. International Organization 63 (1): 107-138.</mixed-citation>
         </ref>
         <ref id="d301e1370a1310">
            <mixed-citation id="d301e1374" publication-type="other">
Mueller, John, and Karl Mueller. (1999) Sanctions of Mass Destruction. Foreign Affairs 78 (3):
43-53.</mixed-citation>
         </ref>
         <ref id="d301e1384a1310">
            <mixed-citation id="d301e1388" publication-type="other">
Nooruddin, Irfan. (2002) Modeling Selection Bias in Studies of Sanctions Efficacy. International
Interactions 28 (1): 59-75.</mixed-citation>
         </ref>
         <ref id="d301e1399a1310">
            <mixed-citation id="d301e1403" publication-type="other">
Nürnberger, Ralph. (1982) The United States and Idi Amin: Congress to the Rescue. African Studies
Review 25 (1): 49-65.</mixed-citation>
         </ref>
         <ref id="d301e1413a1310">
            <mixed-citation id="d301e1417" publication-type="other">
Nürnberger, Ralph. (2003) Why Sanctions (Almost) Never Work. The International Economy 17 (4):
71-72.</mixed-citation>
         </ref>
         <ref id="d301e1427a1310">
            <mixed-citation id="d301e1431" publication-type="other">
O'Kane, Rosemary H. T. (1981) A Probabilistic Approach to the Causes of Coups d'État. British
Journal of Political Science 11 (3): 287-308.</mixed-citation>
         </ref>
         <ref id="d301e1441a1310">
            <mixed-citation id="d301e1445" publication-type="other">
O'Kane, Rosemary H. T. (1993) Coups d'Etat in Africa: A Political Economy Approach. Journal of
Peace Research 30 (3): 251-270.</mixed-citation>
         </ref>
         <ref id="d301e1455a1310">
            <mixed-citation id="d301e1459" publication-type="other">
Olson, Robert S. (1979) Economic Coercion in World Politics: With a Focus on North-South
Relations. World Politics 31 (4): 471-494.</mixed-citation>
         </ref>
         <ref id="d301e1469a1310">
            <mixed-citation id="d301e1473" publication-type="other">
Pape, Robert A. (1997) Why Economic Sanctions Do Not Work. International Security 22 (2): 90-136.</mixed-citation>
         </ref>
         <ref id="d301e1481a1310">
            <mixed-citation id="d301e1485" publication-type="other">
Pepinsky, Thomas B. (2007) Autocracy, Elections, and Fiscal Policy in Malaysia. Studies in Comparative
International Development 42 (1-2): 136-163.</mixed-citation>
         </ref>
         <ref id="d301e1495a1310">
            <mixed-citation id="d301e1499" publication-type="other">
PintoDuschinsky, Michael. (1991) Foreign Political Aid: The German Political Foundations and
Their US Counterparts. International Affairs 67 (1): 33-63.</mixed-citation>
         </ref>
         <ref id="d301e1509a1310">
            <mixed-citation id="d301e1513" publication-type="other">
Przeworski, Adam, Michael Alvarez, Jose Antonio Cheibub, and Fernando Limongi. (2000)
Democracy and Development. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d301e1523a1310">
            <mixed-citation id="d301e1527" publication-type="other">
Rasler, Karen. (1996) Concessions, Repression, and Political Protest in the Iranian Revolution.
American Sociological Review 61 (1): 132-152.</mixed-citation>
         </ref>
         <ref id="d301e1537a1310">
            <mixed-citation id="d301e1541" publication-type="other">
Robinson, James, and Ragnar Torvik. (2005) White Elephants. Journal of Public Economics 89 (2-3):
197-210.</mixed-citation>
         </ref>
         <ref id="d301e1551a1310">
            <mixed-citation id="d301e1555" publication-type="other">
Ross, Michael. (2001) Does Oil Hinder Democracy? World Politics 53 (3): 325-361.</mixed-citation>
         </ref>
         <ref id="d301e1563a1310">
            <mixed-citation id="d301e1567" publication-type="other">
Ross, Michael. (2008) Oil, Islam, and Women. American Political Science Review 102 (1): 107-123.</mixed-citation>
         </ref>
         <ref id="d301e1574a1310">
            <mixed-citation id="d301e1578" publication-type="other">
Rowe, David M. (2001) Manipulating the Market: Understanding Economic Sanctions, Institutional Change,
and the Political Unity of White Rhodesia. Ann Arbor: The University of Michigan Press.</mixed-citation>
         </ref>
         <ref id="d301e1588a1310">
            <mixed-citation id="d301e1592" publication-type="other">
Schreiber, Anna P. (1973) Economic Coercion as an Instrument of Foreign Policy: US Economic
Measures against Cuba and the Dominican Republic. World Politics 25 (3): 387-413.</mixed-citation>
         </ref>
         <ref id="d301e1602a1310">
            <mixed-citation id="d301e1606" publication-type="other">
Smith, Benjamin. (2004) Oil Wealth and Regime Survival in the Developing World: 1960-1999.
American Journal of Political Science 48 (2): 232-246.</mixed-citation>
         </ref>
         <ref id="d301e1616a1310">
            <mixed-citation id="d301e1620" publication-type="other">
Smith, Benjamin. (2005) Life of the Party: The Origins of Regime Breakdown and Persistence under
Single-Party Rule. World Politics 57 (3): 421-451.</mixed-citation>
         </ref>
         <ref id="d301e1630a1310">
            <mixed-citation id="d301e1634" publication-type="other">
The Economist. (2001) Smart exit; Sanctions on Iraq. July 7, 45-46.</mixed-citation>
         </ref>
         <ref id="d301e1642a1310">
            <mixed-citation id="d301e1646" publication-type="other">
Tullock, Gordon. (1987) Autocracy. Boston, MA: Kluwer Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d301e1653a1310">
            <mixed-citation id="d301e1657" publication-type="other">
Ulfelder, Jay. (2005) Contentious Collective Action and the Breakdown of Authoritarian Regimes.
International Political Science Review 26 (3): 311-334.</mixed-citation>
         </ref>
         <ref id="d301e1667a1310">
            <mixed-citation id="d301e1671" publication-type="other">
Ulfelder, Jay. (2007) Natural-Resource Wealth and the Survival of Autocracy. Comparative Political
Studies 40 (8): 995-1018.</mixed-citation>
         </ref>
         <ref id="d301e1681a1310">
            <mixed-citation id="d301e1685" publication-type="other">
Ullman, Richard H. (1978) Human Rights and Economic Power: The United States versus Idi
Amin. Foreign Affairs 50 (3): 529-543.</mixed-citation>
         </ref>
         <ref id="d301e1695a1310">
            <mixed-citation id="d301e1699" publication-type="other">
Van Bergeijk, Peter A. G. (1989) Success and Failure in Economic Sanctions. Kyklos 42 (3): 385-
404.</mixed-citation>
         </ref>
         <ref id="d301e1709a1310">
            <mixed-citation id="d301e1713" publication-type="other">
Van de Walle, Nicolas. (2001) African Economies and the Politics of Permanent Crisis, 1979-1999.
Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d301e1724a1310">
            <mixed-citation id="d301e1728" publication-type="other">
Weeks, Jessica L. (2008) Autocratic Audience Costs: Regime Type and Signaling Resolve. International
Organization 62 (1): 35-64.</mixed-citation>
         </ref>
         <ref id="d301e1738a1310">
            <mixed-citation id="d301e1742" publication-type="other">
Wintrobe, Ronald. (1990) The Tinpot and the Totalitarian: An Economic Theory of Dictatorship.
American Political Science Review 83 (4): 849-872.</mixed-citation>
         </ref>
         <ref id="d301e1752a1310">
            <mixed-citation id="d301e1756" publication-type="other">
Wintrobe, Ronald. (1998) The Political Economy of Dictatorship. Cambridge: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d301e1766a1310">
            <mixed-citation id="d301e1770" publication-type="other">
Wood, Reed M. (2008) "A Hand upon the Throat of the Nation": Economic Sanctions and State
Repression, 1976-2001. International Studies Quarterly 52 (3): 489-513.</mixed-citation>
         </ref>
         <ref id="d301e1780a1310">
            <mixed-citation id="d301e1784" publication-type="other">
Wright, Joseph. (2008) Do Authoritarian Institutions Constrain? How Legislatures Affect Economic
Growth and Investment. American Journal of Political Science 52 (2): 322-343.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
