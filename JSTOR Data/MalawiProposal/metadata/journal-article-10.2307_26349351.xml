<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26349351</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Likoma Network Study</article-title>
            <subtitle>Context, data collection, and initial results</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Helleringer</surname>
                  <given-names>Stéphane</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Kohler</surname>
                  <given-names>Hans-Peter</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Chimbiri</surname>
                  <given-names>Agnes</given-names>
               </string-name>
               <xref ref-type="aff" rid="af3">³</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Chatonda</surname>
                  <given-names>Praise</given-names>
               </string-name>
               <xref ref-type="aff" rid="af4">⁴</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Mkandawire</surname>
                  <given-names>James</given-names>
               </string-name>
               <xref ref-type="aff" rid="af5">⁵</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Assistant Professor at the Mailman School of Public Health, Columbia University, 60 Haven Ave, New York, NY 10032, USA;</aff>
            <aff id="af2">
               <label>²</label>Professor of Sociology at the University of Pennsylvania, Population Studies Center, 3718 Locust Walk, Philadelphia, PA 19104-6299, USA;</aff>
            <aff id="af3">
               <label>³</label>Assistant Resident Representative at UNDP Malawi, P.O Box 30135, Lilongwe 3, Malawi;</aff>
            <aff id="af4">
               <label>⁴</label>Market Research Center, Celtel Lilongwe (Malawi).</aff>
            <aff id="af5">
               <label>⁵</label>MPH student, University of Malawi College of Medicine.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>7</month>
            <year>2009</year>
            <string-date>JULY - DECEMBER 2009</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>12</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">21</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26349336</issue-id>
         <fpage>427</fpage>
         <lpage>468</lpage>
         <permissions>
            <copyright-statement>© 2009 Stéphane Helleringer et al.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26349351"/>
         <abstract xml:lang="eng">
            <p>The extent and structure of sexual networks have important consequences for the spread of sexually transmitted diseases such as HIV. However, very few datasets currently exist that allow a detailed investigation of sexual networks in sub-Saharan African settings where HIV epidemics have become generalized. In this paper, we describe the context and methods of the<italic>Likoma Network Study</italic>(LNS), one of the few studies that have collected extensive information on sexual networks in sub-Saharan Africa. We start by reviewing theoretical arguments and empirical studies emphasizing the importance of network structures in the epidemiology of HIV and other sexually transmitted infections (STI). The island setting of this study is described, and we argue that the choice of an island as a research site limited potential biases that may make the collection of sexual network data difficult. We then document our empirical strategy for the collection of sexual network data and the subsequent identification of sexual network partners. A description of the protocol for the collection of biomarker data (HIV infection) is provided. Finally, we present initial results relating to the socioeconomic context of the island, the size and composition of sexual networks, the quality of the sexual network data, the determinants of successful contact tracing during the LNS, and the prevalence of HIV in the study population.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Adams, J. and Moody, J. (2007). To tell the truth: Measuring concordance in multiply reported network data. Social Networks 29(1): 44–58. doi:10.1016/j.socnet.2005.11.009.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Anderson, R.M. and May, R.M. (1991). Infectious diseases of humans: Dynamics and control. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Bailey, N.T.J. (1975). The mathematical theory of infectious disease and its applications. New York: Hafner Press.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Bearman, P.S., Moody, J., and Stovel, K. (2004). Chains of affection: The structure of adolescent romantic and sexual networks. American Journal of Sociology 110(1): 44–91. doi:10.1086/386272.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Bell, D., Montoya, I., and Atkinson, J. (2000). Partner concordance in reports of joint risk behaviors. Journal of the Acquired Immune Deficiency Syndromes 25(2): 173–181.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Blasnick, M. (2001). Gsoundex: Stata module to implement soundex algorithm.[unpublished manuscript]. Boston College Department of Economics. http://ideas.repec.org/c/boc/bocode/s420901.html.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Brewer, D.D. and Webster, C.M. (1999). Forgetting of friends and its effects on measuring friendship networks. Social Networks 21(4): 361–373. doi:10.1016/S0378- 8733(99)00018-0.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Burt, R.S. (1987). Social contagion and innovation: Cohesion versus structural equivalence. American Journal of Sociology 92(6): 1287–1335. doi:10.1086/228667.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Caldwell, J.C., Caldwell, P., and Quiggin, P. (1989). The social context of AIDS in sub-Saharan Africa. Population and Development Review 15(2): 185–234. doi:10.2307/1973703.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Cleland, J., Boerma, J., Carael, M., and Weir, S. (2004). Monitoring sexual behaviour in general populations: A synthesis of lessons of the past decade. Sexually Transmitted Infections 80(Supplement 2): 1–7. doi:10.1136/sti.2004.013151.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Cliff, A., Haggett, P., and Smallman-Raynor, M. (2000). Island Epidemics. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Coffee, M., Lurie, M.N., and Garnett, G.P. (2007). Modelling the impact of migration on the HIV epidemic in South Africa. AIDS 21(3): 343–350. doi:10.1097/QAD.0b013e328011dac9.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Coffee, M., Garnett, G., Mlilo, M., Voeten, H., Chandiwana, S., and Gregson, S. (2005). Patterns of movement and risk of HIV infection in rural Zimbabwe. Journal of Infectious Diseases 191(Supplement 1): 159–167. doi:10.1086/425270.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Coleman, J.D., Katz, E., and Menzel, H. (1966). Medical Innovation: A Diffusion Story. New York: Bobbs-Merrill.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Costenbader, E. and Valente, T.W. (2003). The stability of centrality measures when networks are sampled. Social Networks 25(4): 283–307. doi:10.1016/S0378- 8733(03)00012-1.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Doherty, I.A., Padian, N.S., Marlow, C., and Aral, S.O. (2005). Determinants and consequences of sexual networks as they affect the spread of sexually transmitted infections. Journal of Infectious Diseases 191(S1): 42–54. doi:10.1086/425277.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Ghani, A.C. and Garnett, G.P. (2000). Risks of acquiring and transmitting sexually transmitted diseases in sexual partner networks. Sexually Transmitted Diseases 27(10): 579-587. doi:10.1097/00007435-200011000-00006.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Ghani, A., Ison, C., Ward, H., Garnett, G., Bell, G., Kinghorn, G., Weber, J., and Day, S. (1996). Sexual partner networks in the transmission of sexually transmitted diseases. An analysis of gonorrhea cases in Sheffield, UK. Sexually Transmitted Disease 23(6): 498–503.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Glynn, J.R., Caraël, M., Auvert, B., Kahindo, M., Chege, J., Musonda, R., Kaona, F., Buvé, A., and the Study Group on the Heterogeneity of HIV Epidemics in African Cities (2001a). Why do young women have a much higher prevalence of HIV than young men? A study in Kisumu, Kenya and Ndola, Zambia. AIDS 15(Supplement 4): 51–60.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Glynn, J.R., Pönnighaus, J., Crampin, A.C., Sibande, F., Sichali, L., Nkhosa, P., Broadbent, P., and Fine, P.E.M. (2001b). The development of the HIV epidemic in Karonga District, Malawi. AIDS 15(15): 2025–2029. doi:10.1097/00002030-200110190-00016.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Handcock, M.S. and Jones, J.H. (2004). Likelihood-based inference for stochastic models of sexual network formation. Theoretical Population Biology 65(4): 413–422. doi:10.1016/j.tpb.2003.09.006.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Heesterbeek, J.A.P. (2002). A brief history of R0 and a recipe for its calculation. Acta Biotheoretica 50(3): 189–204. doi:10.1023/A:1016599411804.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Helleringer, S. and Kohler, H.P. (2007). Sexual network structure and the spread of HIV in Africa: Evidence from Likoma Island, Malawi. AIDS 21(17): 2323–2332. doi:10.1097/QAD.0b013e328285df98.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Helleringer, S. and Kohler, H.P. (2008). Cross-sectional research design and relatively low HIV incidence, rather than blood exposures, explain the peripheral location ofHIV cases within the sexual networks observed on Likoma. AIDS 22(11): 1378–1379. doi:10.1097/QAD.0b013e3282fc733b.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Helleringer, S., Kohler, H.P., and Chimbiri, A. (2007). Characteristics of external/bridge relationships by partner type and location where sexual relationship took place. AIDS 21(18): 2560–2561. doi:10.1097/QAD.0b013e3282f112bd.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Helleringer, S., Kohler, H.P., and Kalilani-Phiri, L. (2009a). The association of HIV serodiscordance and partnership concurrency in Likoma Island (Malawi). AIDS 22(11): 1378–1379. doi:10.1097/QAD.0b013e3282fc733b.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Helleringer, S., Kohler, H.P., and Mkandawire, J. (2009b). Increasing uptake of HIV testing and counseling among the poorest in Sub-Saharan countries through home-based service provision. Journal of Acquired Immune Deficiency Syndromes 51(2): 185–193. doi:10.1097/QAI.0b013e31819c1726.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Heuveline, P. (2003). HIV and population dynamics: A general model and maximum-likelihood standards for East Africa. Demography 40(2): 217–245. doi:10.1353/dem.2003.0013.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Hudson, C.P. (1996). AIDS in rural Africa: A paradigm for HIV-1 prevention. International Journal of STD &amp; AIDS 7(4): 236–243. doi:10.1258/0956462961917906.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Jones, J.H. and Handcock, M.S. (2003a). An assessment of preferential attachment as a mechanism for human sexual network formation. Proceedings of the Royal Society of London, Series B: Biological Sciences 270(1520): 1123–1128. doi:10.1098/rspb.2003.2369.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Jones, J.H. and Handcock, M.S. (2003b). Sexual contacts and epidemic thresholds. Nature 423(6940): 605–606. doi:10.1038/423605a.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Jones, J.H., Helleringer, S., and Kohler, H.P. (2007). Statistical Models for Sexual Networks on Likoma Island, Malawi: Implications for Sexual Behavior and HIV Control. Paper presented at the annual meeting of the Population Association of America, New York, NY, March 29–31, 2007. http://paa2007.princeton.edu.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Klovdahl, A.S. (1989). Urban social network: Some methodological problems and possibilities. In: M. Kochen (ed.) The small world. Norwood: Ablex Publishing: 176–210.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Klovdahl, A., Potterat, J., Woodhouse, D., Muth, J., Muth, S., and Darrow, W. (1994).Social networks and infectious disease: The Colorado Springs Study. Social Science and Medicine 38(1): 79–88. doi:10.1016/0277-9536(94)90302-6.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Koehly, L., Goodreau, S., and Morris, M. (2004). Exponential family models for census and sampled network data. Sociological Methodology 34(1): 241–270.doi:10.1111/j.0081-1750.2004.00153.x.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Kossinets, G. (2006). Effects of missing data in social networks. Social Networks 28(3): 247–268. doi:10.1016/j.socnet.2005.07.002.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Koumans, E., Farley, T., Gibson, J., Langley, C., Ross, M., McFarlane, M., Braxton, J., and St Louis, M. (2001). Characteristics of persons with syphilis in areas of persisting syphilis in the United States: Sustained transmission associated with concurrent partnerships. Sexually Transmitted Diseases 28(9): 497–503. doi:10.1097/00007435- 200109000-00004.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Krackhardt, D. (1987). Cognitive social structures. Social Networks 9(2): 109–134. doi:10.1016/0378-8733(87)90009-8.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Kretzschmar, M. and Morris, M. (1996). Measures of concurrency in networks and the spread of infectious disease. Mathematical Biosciences 133(2): 165–195. doi:10.1016/0025-5564(95)00093-3.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Lagarde, E., Schim van der Loeff, M., Enel, C., Holmgren, B., Dray-Spira, R., Pison, G., Piau, J., Delaunay, V., M’Boup, S., Ndoye, I., Coeuret-Pellicer, M., Whittle, H., and Aaby, P. (2003). Mobility and the spread of human immunodeficiency virus into rural areas of West Africa. International Journal of Epidemiology 32(5): 744–752. doi:10.1093/ije/dyg111.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Laumann, E., Marsden, P., and Prensky, D. (1983). The boundary specification problem in network analysis. In: R.S. Burt and M.J. Minor (eds.) Applied Network Analysis: A Methodological Introduction. London: Sage Publications: 18–34.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Liljeros, F., Edling, C.R., Amaral, L.A.N., Stanley, H.E., and Aberg, Y. (2001). The web of human sexual contacts. Nature 411(6840): 907–908. doi:10.1038/35082140.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Lurie, M., Williams, B., Zuma, K., Mkaya-Mwamburi, D., Garnett, G., Sweat, M., Gittelsohn, J., and Karim, S. (2003). Who infects whom? HIV-1 concordance and discordance among migrant and non-migrant couples in South Africa. AIDS 17(15): 2245-2252. doi:10.1097/00002030-200310170-00013.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Magruder, J. (2008). Marital shopping and epidemic AIDS. [unpublished manuscript]. Department of Agricultural and Resource Economics, University of California at Berkeley. http://are.berkeley.edu/%7Ejmagruder/research.html.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Moody, J., Morris, M., Adams, J., and Handcock, M. (2003). Epidemic potential in human sexual networks: Connectivity and the development of std cores. [unpublished working paper]. Department of Sociology, Duke University.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Morris, M. (1997). Sexual networks and HIV. AIDS 11(Supplement A): 209–216.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Morris, M. (2004). Overview of network survey designs. In: M. Morris (ed.) Network Epidemiology. Oxford: Oxford University Press. doi:10.1093/0199269017.003.0002.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Morris, M. and Kretzschmar, M. (1997). Concurrent partnerships and the spread of HIV. AIDS 11(5): 641–648. doi:10.1097/00002030-199705000-00012.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Newman, M.E.J. (2002). Spread of epidemic disease on networks. Physical Review E 66(1): 016128–016139. doi:10.1103/PhysRevE.66.016128.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Nnko, S., Boerma, J.T., Urassa, M., Mwaluko, G., and Zaba, B. (2004). Secretive females or swaggering males? An assessment of the quality of sexual partnership reporting in rural Tanzania. Social Science and Medicine 59(2): 299–310. doi:10.1016/j.socscimed.2003.10.031.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Padgett, J.F. and Ansell, C.K. (1993). Robust action and the rise of the Medici, 1400-1434. American Journal of Sociology 98(6): 1259–1319. doi:10.1086/230190.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Potterat, J.J., Phillips-Plummer, L., Muth, S.Q., Rothenberg, R.B., Woodhouse, D.E., Maldonado-Long, T.S., Zimmerman, H.P., and Muth, J.B. (2002). Risk network structure in the early epidemic phase of HIV transmission in Colorado Springs. Sexually Transmitted Infections 78(Supplement 1): 159–163. doi:10.1136/sti.78.suppl_1.i159.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Potterat, J.J., Zimmerman-Rogers, H., Muth, S.Q., Rothenberg, R.B., Green, D.L., Taylor, J.E., Bonney, M.S., and White, H.A. (1999). Chlamydia transmission: Concurrency, reproduction number, and the epidemic trajectory. American Journal of Epidemiology 150(12): 1331–1339.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Potterat, J.J., Woodhouse, D.E., Muth, S.Q., Rothenberg, R., Darrow, W.W., Klovdahl, A.S., and Muth, J.B. (2004). Network dynamism: History and lessons of the Colorado Springs study. In: M. Morris (ed.) Network Epidemiology. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Rothenberg, R.B., Sterk, C., Toomey, K.E., Potterat, J.J., Johnson, D., Schrader, M., and Hatch, S. (1998). Using social network and ethnographic tools to evaluate syphilis transmission. Sexually Transmitted Diseases 25(3): 154–160. doi:10.1097/00007435- 199803000-00009.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Salganik, M. and Heckathorn, D. (2004). Sampling and estimation in hidden populations using respondent-driven sampling. Sociological Methodology 34(1): 193–240. doi:10.1111/j.0081-1750.2004.00152.x.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Sampson, S.F. (1969). A novitiate during a period of change: An experimental and case study of relationships. [Unpublished Ph.D. dissertation]. Cornell University.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Van Den Bulte, C. and Lilien, G. (2001). Medical innovation revisited: Social contagion versus marketing effort. American Journal of Sociology 106(5): 1409–1435. doi:10.1086/320819.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Wasserman, S. and Faust, K. (1994). Social Network Analysis: Methods and Applications. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Watkins, S.C. (2004). Navigating the AIDS epidemic in rural Malawi. Population and Development Review 30(4): 673–705. doi:10.1111/j.1728-4457.2004.00037.x.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Watts, D. and Strogatz, S. (1998). Collective dynamics of ‘small-world’ networks. Nature 393: 440–443. doi:10.1038/30918.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Watts, D.J. (1999). Small Worlds: The Dynamics of Networks Between Order and Randomness. Princeton Studies in Complexity.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Wawer, M., Gray, R., Sewankambo, N., Serwadda, D., Li, X., Laeyendecker, O., Kiwanuka, N., Kigozi, G., Kiddugavu, M., Lutalo, T., Nalugoda, F., Wabwire-Mangen, F., Meehan, M., and Quinn, T. (2005). Rates of HIV-1 transmission per coital act, by stage of HIV-1 infection, in Rakai, Uganda. Journal of Infectious Diseases 191(9): 1403–1409. doi:10.1086/429411.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">White, K. and Watkins, S.C. (2000). Accuracy, stability and reciprocity in informal conversational networks in rural Kenya. Social Networks 22(4): 337–355. doi:10.1016/S0378-8733(00)00030-7.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Whittaker, R. (1999). Island Biogeography. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">World Health Organization (2002). HIV Assays: Operational Characteristics. Report 12: Simple/Rapid Tests, Whole Blood Specimens. Geneva, Switzerland: World Health Organization. Http://www.who.int/diagnostics_laboratory/publications/hiv_assays_rep_12.pdf.</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Wylie, J., Cabral, T., and Jolly, A. (2005). Identification of networks of sexually transmitted infection: A molecular, geographic, and social network analysis. Journal of Infectious Diseases 191(6): 899–906. doi:10.1086/427661.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
