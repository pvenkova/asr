<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amerpoliscierevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100077</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The American Political Science Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00030554</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15375943</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23275362</article-id>
         <title-group>
            <article-title>When Natural Experiments Are Neither Natural nor Experiments</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>JASJEET S.</given-names>
                  <surname>SEKHON</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>ROCÍO</given-names>
                  <surname>TITIUNIK</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">106</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23275357</issue-id>
         <fpage>35</fpage>
         <lpage>57</lpage>
         <permissions>
            <copyright-statement>© American Political Science Association 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23275362"/>
         <abstract>
            <p>Natural experiments help to overcome some of the obstacles researchers face when making causal inferences in the social sciences. However, even when natural interventions are randomly assigned, some of the treatment—control comparisons made available by natural experiments may not be valid. We offer a framework for clarifying the issues involved, which are subtle and often overlooked. We illustrate our framework by examining four different natural experiments used in the literature. In each case, random assignment of the intervention is not sufficient to provide an unbiased estimate of the causal effect. Additional assumptions are required that are problematic. For some examples, we propose alternative research designs that avoid these conceptual difficulties.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d277e210a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d277e217" publication-type="other">
Ansolabehere, Snyder, and Stewart
(2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e227a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d277e234" publication-type="other">
Bhavnani (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e241a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d277e248" publication-type="other">
Singh, Lele, Sathe, Sonalkar, and
Maydeo (1992)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e258a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d277e265" publication-type="other">
Bhavnani (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e273a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d277e280" publication-type="other">
Hahn, Todd, and
van der Klaauw (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e290a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d277e297" publication-type="other">
Sekhon (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e304a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d277e311" publication-type="other">
Heckman, Ichimura, and Todd (1998)</mixed-citation>
            </p>
         </fn>
         <fn id="d277e318a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d277e325" publication-type="other">
Heckman, Ichimura, Smith, and Todd (1998)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d277e341a1310">
            <mixed-citation id="d277e345" publication-type="other">
Abrajano, Marisa A., Jonathan Nagler, and R. Michael Alvarez.
2005. "A Natural Experiment of Race-Based and Issue Voting: The
2001 City of Los Angeles Elections." Political Research Quarterly
58 (2): 203-18.</mixed-citation>
         </ref>
         <ref id="d277e361a1310">
            <mixed-citation id="d277e365" publication-type="other">
Ansolabehere, Stephen, James M. Snyder, and Charles Stewart. 2000.
"Old Voters, New Voters, and the Personal Vote: Using Redisrict-
ing to Measure the Incumbency Advantage." American Journal of
Political Science 44 (1): 17-34.</mixed-citation>
         </ref>
         <ref id="d277e381a1310">
            <mixed-citation id="d277e385" publication-type="other">
Bhavnani, Rikhil R. 2009. "Do Electoral Quotas Work after They
Are Withdrawn? Evidence from a Natural Experiment in India."
American Political Science Review 103 (1): 23-35.</mixed-citation>
         </ref>
         <ref id="d277e398a1310">
            <mixed-citation id="d277e402" publication-type="other">
Butler, Daniel Mark. 2009. "A Regression Discontinuity Design
Analysis of the Incumbency Advantage and Tenure in the U.S.
House." Electoral Studies 28 (2): 123-28.</mixed-citation>
         </ref>
         <ref id="d277e416a1310">
            <mixed-citation id="d277e420" publication-type="other">
Carman, Christopher, James Mitchell, and Robert Johns. 2008. "The
Unfortunate Natural Experiment in Ballot Design: The Scottish
Parliamentary Elections of 2007." Electoral Studies 27 (3): 442-59.</mixed-citation>
         </ref>
         <ref id="d277e433a1310">
            <mixed-citation id="d277e437" publication-type="other">
Carson, Jamie L., Erik J. Engstrom, and Jason M. Roberts. 2007.
"Candidate Quality, the Personal Vote, and the Incumbency Ad-
vantage in Congress." American Political Science Review 101 (2):
289-301.</mixed-citation>
         </ref>
         <ref id="d277e453a1310">
            <mixed-citation id="d277e457" publication-type="other">
Caughey, Devin, and Jasjeet S. Sekhon. 2011. "Elections and the
Regression-Discontinuity Design: Lessons from Close U.S. House
Races, 1942-2008." Political Analysis 19 (4): 385^108.</mixed-citation>
         </ref>
         <ref id="d277e470a1310">
            <mixed-citation id="d277e474" publication-type="other">
Cox, Gary W., and Jonathan N. Katz. 2002. Elbridge Gerry's Sala-
mander: The Electoral Consequences of the Reapportionment Rev-
olution. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d277e487a1310">
            <mixed-citation id="d277e491" publication-type="other">
Desposato, Scott W., and John R. Petrocik. 2003. "The Variable In-
cumbency Advantage: New Voters, Redistricting, and the Personal
Vote." American Journal of Political Science 47 (1): 18-32.</mixed-citation>
         </ref>
         <ref id="d277e504a1310">
            <mixed-citation id="d277e508" publication-type="other">
Diamond, Alexis, and Jasjeet S. Sekhon. 2005. "Genetic Matching
for Estimating Causal Effects: A General Multivariate Matching
Method for Achieving Balance in Observational Studies." Work-
ing Paper. http://sekhon.berkeley.edu/papers/GenMatch.pdf (ac-
cessed November 16,2011).</mixed-citation>
         </ref>
         <ref id="d277e528a1310">
            <mixed-citation id="d277e532" publication-type="other">
Diamond, Jared, and James A. Robinson, eds. 2010. Natural Experi-
ments of History. Cambridge, MA: Belknap Press.</mixed-citation>
         </ref>
         <ref id="d277e542a1310">
            <mixed-citation id="d277e546" publication-type="other">
Dunning, Thad. 2008. "Improving Causal Inference: Strengths and
Limitations of Natural Experiments." Political Science Quarterly
61 (2): 282-93.</mixed-citation>
         </ref>
         <ref id="d277e559a1310">
            <mixed-citation id="d277e563" publication-type="other">
Erikson, Robert S. 1971. "The Advantage of Incumbency in Con-
gressional Elections." Polity 3 (3): 395^05.</mixed-citation>
         </ref>
         <ref id="d277e573a1310">
            <mixed-citation id="d277e577" publication-type="other">
Gelman, Andrew, and Gary King. 1990. "Estimating Incumbency
Advantage without Bias." American Journal of Political Science
34 (4): 1142-64.</mixed-citation>
         </ref>
         <ref id="d277e590a1310">
            <mixed-citation id="d277e594" publication-type="other">
Gordon, Sandy, and Greg Huber. 2007. "The Effect of Electoral
Competitiveness on Incumbent Behavior." Quarterly Journal of
Political Science 2 (2): 107-38.</mixed-citation>
         </ref>
         <ref id="d277e607a1310">
            <mixed-citation id="d277e611" publication-type="other">
Grofman, Bernard, Robert Griffin, and Gregory Berry. 1995. "House
Members Who Become Senators: Learning from a 'Natural Ex-
periment' in Representation." Legislative Studies Quarterly 20 (4):
513-29.</mixed-citation>
         </ref>
         <ref id="d277e628a1310">
            <mixed-citation id="d277e632" publication-type="other">
Hahn, Jinyong, Petra Todd, and Wilbert van der Klaauw. 2001. "Iden-
tification and Estimation of Treatment Effects with a Regression-
Discontinuity Design." Econometrica 69: 201-09.</mixed-citation>
         </ref>
         <ref id="d277e645a1310">
            <mixed-citation id="d277e649" publication-type="other">
Heckman, James J., Hidehiko Ichimura, Jeffrey Smith, and Petra
Todd. 1998. "Characterizing Selection Bias Using Experimental
Data." Econometrica 66 (5): 1017-98.</mixed-citation>
         </ref>
         <ref id="d277e662a1310">
            <mixed-citation id="d277e666" publication-type="other">
Heckman, James J., Hidehiko Ichimura, and Petra Todd. 1998.
"Matching as an Econometric Evaluation Estimator." Review of
Economic Studies 65 (2): 261-94.</mixed-citation>
         </ref>
         <ref id="d277e679a1310">
            <mixed-citation id="d277e683" publication-type="other">
Holland, Paul W. 1986. "Statistics and Causal Inference." Journal of
the American Statistical Association 81 (396): 945-60.</mixed-citation>
         </ref>
         <ref id="d277e693a1310">
            <mixed-citation id="d277e697" publication-type="other">
Kaushik, Susheela. 1992. "Women and Political Participation." In
Women in Politics: Forms and Processes, ed. Kamala Sankaran.
New Delhi: Friedrich Ebert Stiftung.</mixed-citation>
         </ref>
         <ref id="d277e710a1310">
            <mixed-citation id="d277e714" publication-type="other">
Kishwar, Madhu. 1996. "Women and Politics: Beyond Quotas." Eco-
nomic and Political Weekly 31 (43): 2867-74.</mixed-citation>
         </ref>
         <ref id="d277e725a1310">
            <mixed-citation id="d277e729" publication-type="other">
Krasno, Jonathan S., and Donald P. Green. 2008. "Do Tele-
vised Presidential Ads Increase Voter Turnout? Evidence
from a Natural Experiment." Journal of Politics 70 (1): 245-
61.</mixed-citation>
         </ref>
         <ref id="d277e745a1310">
            <mixed-citation id="d277e749" publication-type="other">
Lassen, David D. 2005. "The Effect of Information on Voter Turnout:
Evidence from a Natural Experiment." American Journal of Po-
litical Science 49 (1): 103-18.</mixed-citation>
         </ref>
         <ref id="d277e762a1310">
            <mixed-citation id="d277e766" publication-type="other">
Lee, David S. 2008. "Randomized Experiments from Non-random
Selection in U.S. House Elections." Journal of Econometrics 142
(2): 675-97.</mixed-citation>
         </ref>
         <ref id="d277e779a1310">
            <mixed-citation id="d277e783" publication-type="other">
Neyman, Jerzy. [1923] 1990. "On the Application of Probability The-
ory to Agricultural Experiments. Essay on Principles. Section 9."
Statistical Science 5 (4): 465-72. Trans. Dorota M. Dabrowska and
Terence P. Speed.</mixed-citation>
         </ref>
         <ref id="d277e799a1310">
            <mixed-citation id="d277e803" publication-type="other">
Posner, Daniel N. 2004. "The Political Salience of Cultural Dif-
ference: Why Chewas and Tumbukas Are Allies in Zambia and
Adversaries in Malawi." American Political Science Review 98 (4):
529-45.</mixed-citation>
         </ref>
         <ref id="d277e819a1310">
            <mixed-citation id="d277e823" publication-type="other">
Rosenbaum, Paul R. 2004. "Design Sensitivity in Observational
Studies." Biometrika 91 (1): 153-64.</mixed-citation>
         </ref>
         <ref id="d277e834a1310">
            <mixed-citation id="d277e838" publication-type="other">
Rosenbaum, Paul R. 2010. Design of Observational Studies. New
York: Springer-Verlag.</mixed-citation>
         </ref>
         <ref id="d277e848a1310">
            <mixed-citation id="d277e852" publication-type="other">
Rubin, Donald B. 1974. "Estimating Causal Effects of Treatments
in Randomized and Nonrandomized Studies." Journal of Educa-
tional Psychology 66 (6): 688-701.</mixed-citation>
         </ref>
         <ref id="d277e865a1310">
            <mixed-citation id="d277e869" publication-type="other">
Sekhon, Jasjeet S. 2008. "The Neyman-Rubin Model of Causal Infer-
ence and Estimation via Matching Methods." In The Oxford Hand-
book of Political Methodology, eds. Janet M. Box-Steffensmeier,
Henry E. Brady, and David Collier. New York: Oxford University
Press, 271-99.</mixed-citation>
         </ref>
         <ref id="d277e888a1310">
            <mixed-citation id="d277e892" publication-type="other">
Sekhon, Jasjeet S. 2010. "Opiates for the Matches: Matching Methods
for Causal Inference." Annual Review of Political Science 12:487-
508.</mixed-citation>
         </ref>
         <ref id="d277e905a1310">
            <mixed-citation id="d277e909" publication-type="other">
Sekhon, Jasjeet S. 2011. "Matching: Multivariate and Propensity
Score Matching with Automated Balance Search." Journal of Sta-
tistical Software 42 (7): 1-52. http://sekhon.berkeley.edu/matching/
(accessed November 16,2011).</mixed-citation>
         </ref>
         <ref id="d277e925a1310">
            <mixed-citation id="d277e929" publication-type="other">
Sekhon, Jasjeet S., and Richard Grieve. N.d. "A Non-parametric
Matching Method for Bias Adjustment with Applications to Eco-
nomic Evaluations." Health Economics. Forthcoming.</mixed-citation>
         </ref>
         <ref id="d277e943a1310">
            <mixed-citation id="d277e947" publication-type="other">
Singh, Gopal Simrita, Medha Kotwal Lele, Nirmala Sathe, Wandana
Sonalkar, and Anjali Maydeo. 1992. "Participation of Women in
Electoral Politics in Maharashtra." In Women in Politics: Forms
and Processes, ed. Kamala Sankaran. New Delhi: Friedrich Ebert
Stiftung, 63-108.</mixed-citation>
         </ref>
         <ref id="d277e966a1310">
            <mixed-citation id="d277e970" publication-type="other">
van der Brug, Wouter. 2001. "Perceptions, Opinions and Party Pref-
erences in the Face of a Real World Event: Chernobyl as a Natural
Experiment in Political Psychology." Journal of Theoretical Politics
13 (1): 53-80.</mixed-citation>
         </ref>
         <ref id="d277e986a1310">
            <mixed-citation id="d277e990" publication-type="other">
Whitford, Andrew B. 2002. "Decentralization and Political Control
of the Bureaucracy." Journal of Theoretical Politics 14 (2): 167-93.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
