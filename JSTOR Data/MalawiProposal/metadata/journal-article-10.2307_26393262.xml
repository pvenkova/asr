<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">conssoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019729</journal-id>
         <journal-title-group>
            <journal-title>Conservation and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>WOLTERS KLUWER INDIA PRIVATE LIMITED</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09724923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">09753133</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26393262</article-id>
         <article-categories>
            <subj-group>
               <subject>Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Animals’ Role in Proper Behaviour</article-title>
            <subtitle>Cheŵa Women’s Instructions in South-Central Africa</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Zubieta</surname>
                  <given-names>Leslie F.</given-names>
               </string-name>
               <aff>Current affiliation: Centre for Rock Art Research + Management, The University of Western Australia, Perth, Western Australia, Australia</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2016</year>
            <string-date>2016</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">14</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26393251</issue-id>
         <fpage>406</fpage>
         <lpage>415</lpage>
         <permissions>
            <copyright-statement>Copyright: © Zubieta 2016</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26393262"/>
         <abstract xml:lang="eng">
            <p>The most common role of animals in the Cheŵa culture of south-central Africa is twofold: they are regarded as an important source of food, and they also provide raw materials for the creation of traditional medicines. Animals, however, also have a nuanced symbolic role that impacts the way people behave with each other by embodying cultural protocols of proper — and not so proper — behaviour. They appear repeatedly in storytelling and proverbs to reference qualities that people need to avoid or pursue and learn from the moral of the story in which animals interplay with each other, just as humans do. For example, someone who wants to prevent the consequences of greed is often advised to heed hyena stories and proverbs. My contribution elaborates on Brian Morris’s instrumental work in south-central Africa, which has permitted us to elucidate the symbolism of certain animals and the perception of landscape for Indigenous populations in this region. I discuss some of the ways in which animals have been employed to teach and learn proper behaviour in a particular sacred ceremony of the Cheŵa people which takes place in celebration of womanhood:<italic>Chinamwali</italic>.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>Animal symbolism</kwd>
            <kwd>initiation</kwd>
            <kwd>Cheŵa</kwd>
            <kwd>rock art</kwd>
            <kwd>behaviour</kwd>
            <kwd>Indigenous women</kwd>
            <kwd>south-central Africa</kwd>
            <kwd>Indigenous knowledge</kwd>
            <kwd>Chinamwali</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Abbot, J.I.O. and R. Mace.1999. Managing protected woodlands: fuelwood collection and law enforcement in Lake Malawi National Park. Conservation Biology 13(2): 418-421.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Angus, H.C. 1898. A year in Azimba and Chipitaland: the customs and superstitions of the people. The Journal of the Royal Anthropological Institute of Great Britain and Ireland 27: 316-325.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Apthorpe, R. 1962. A note on Nsenga girl’s puberty designs. South African Archaeological Bulletin 17(65): 12-13.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Birch de Aguilar, L. 1996. Inscribing the mask: interpretation of Nyau masks and ritual performing among the Chewa of Central Malawi. Freiburg: University Press.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Boucher, C. Ch. Fr. 2002a. Digging our roots. Malawi: MAfr Publications.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Boucher, C. Ch. Fr. 2002b. The gospel seed: culture and faith in Malawi as expressed in the Missio Banner. Malawi: MAfr Publications.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Boucher, C. Ch. Fr. 2012. When animal sing and spirit dance. Gule Wamkulu: the Great Dance of the Chewa people of Malawi. Malawi: Kungoni Centre of Culture and Art.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Brown, P. and A. Young. 1965. The physical environment of central Malawi with special reference to soils and agriculture. Zomba: Malawi Government Press.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Bruwer, J. 1949. Die gesin onder die moederregtelike Acewa. M.A. thesis. University of Pretoria, Pretoria, South Africa.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Clark, J.D. 1973. Archaeological investigation of a painted rock shelter at Mwana Wa Chencherere, north of Dedza, Central Malawi. The Society of Malawi Journal 26(1): 28-46.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Chidumayo, E.N. 2002. Changes in miombio woodland structure under different land tenure and use systems in central Zambia. Journal of Biogeography 29: 1619-1626.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Chimombo, S. 1988. Malawian oral literature: the aesthetics of Indigenous arts. Zomba: Centre for Social Research &amp; University of Malawi.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Gamitto, A.C.P 1960. King Kazembe and the Marave, Cheva, Bisa, Bemba, Lunda and other peoples of Southern Africa. (Cunnison, I. trans.). Estudos de Ciencias Politicas e Sociais 42 (1). Lisbon: Junta de Investigates do Ultramar, Centro de Estudios Politicos e Sociais.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Giddens, A. 1984. The constitution of society: outline of the theory of structuration. Cambridge: Polity Press.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Hubbard, W. D. 1928. Observations on the elephants of Northern Rhodesia and Portuguese East Africa. Journal of Mammalogy 9(1): 39-43.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Kashoki, M.E. 1978. The language situation in Zambia. In: Language in Zambia (eds. Ohannessian, S. and M.E. Kashoki). Pp. 9-46. London: International African Institute.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Kirksey, S.E. and S. Helmreich. 2010. The emergence of multispecies ethnography. Cultural Anthropology 25(4): 545-576.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Lancaster, D.G. 1934. Africa: piles of stones. Man 34: 198-199.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Langworthy III, H. 1969. A history of the Undi’s Kingdom to 1890: aspects of Chewa history in east central Africa. Ph.D. thesis. Boston University, Boston, USA.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Lindgren, N.E. and J.M. Schoffeleers. 1978. Rock art and Nyau symbolism in Malawi. Department of Antiquities Publication 18. Zomba, Malawi: Government Press.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Morris, B. 1991. Changing conceptions of nature. The Society of Malawi Journal 44(2): 9-26.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Morris, B. 1994. Animals as meat and meat as food: reflections on meat eating in Southern Malawi. Food and Foodways: explorations in the History and Culture of Human Nourishment 6(1): 19—41. DOI: 10.1080/07409710.1994.9962023.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Morris, B. 1995a. Woodland and village: reflections on the ‘animal estate in rural Malawi. The Journal of the Royal Anthropological Institute 1(2): 301-315.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Morris, B. 1995b. Hunting and the gnostic vision. The Society of Malawi Journal 48(2): 26-46.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Morris, B. 1998. The power of nature. Anthropology &amp; Nature 5(1): 81-101.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Morris, B. 2000a. The power of animals: an ethnography. Oxford: Berg.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Morris, B. 2000b. Animals and ancestors and ethnography. Oxford: Berg.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Morris, B. 2001. Wildlife conservation in Malawi. Environment and History 7(3): 357-372.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Mtuta, A. 2001. Tradition and customs of Dedza and Lilongwe districts. Unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Phiri, K.M. 1975. Chewa history in central Malawi and the use of oral tradition, 1600—1920. Ph.D thesis. University of Wisconsin, Madison, USA.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Phiri, K.M. 1983. Some changes in the matrilineal family system among the Chewa of Malawi since the nineteenth century. Journal of African History 24(2): 257-274.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Preucel, R.W. and I. Hodder. 1996. Contemporary archaeology in theory: a reader. Oxford: Blackwell Publishing.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Rangeley, W.H.J. 1949. ‘Nyau’ in Kotakota District. The Nyasaland Journal 2(2): 45-49.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Rangeley, W.H.J. 1950. ‘Nyau’ in Kotakota District. Part II. The Nyasaland Journal 3(2): 19-33.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Robinson K.R. 1975. Iron Age sites in the Dedza district of Malawi. Department of Antiquities Publication 16. Zomba: Government Press.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Schoffeleers, J.M. 1971. The religious significance of bush fires in Malawi. Cahiers des Religions Africaines 5(10): 271-282.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Schoffeleers, J.M. 1976. Nyau societies: our present understanding. The Society of Malawi Journal 29(1): 59-68.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Schoffeleers, J.M. and A.A. Roscoe. 1985. Land of fire: oral literature from Malawi. Limbe: Popular Publications.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Shorter, C. 1989. An introduction to the common trees of Malawi. Malawi: Wildlife Society of Malawi.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Smith, B.W. 1995. Rock art in South-Central Africa. a study based on the pictographs of Dedza District, Malawi and Kasama District Zambia. Ph.D. thesis. University of Cambridge, Cambridge, United Kingdom.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Smith, B.W. 1997. Zambia's ancient rock art: the paintings of Kasama. Livingstone: The National Heritage Conservation Commission of Zambia.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Smith, B.W. 2014. Chongoni rock art area. In: Encyclopedia of global archaeology (ed. Smith, C). Pp. 1448-1452. New York: Springer.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Synnott, A. 1993. The body social: symbolism, self and society. London: Routledge.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Topham, P. 1952. Nyasaland trees and shrubs. The Nyasaland Journal 5(2): 11-17.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Turner, B.S. 1996. The body and society: explorations in social theory. Second edition. London: Sage.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">van Breugel, J.W.M. 2001. Chewa traditional religion. Blantyre: Kachere.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Wason, P. 1994. The archaeology of rank. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Werner, A. 1906. The natives of British Central Africa. London: Archibald Constable and Company.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Walker, P.A. and P.E. Peters. 2007. Making sense in time: remote sensing and the challenges of temporal heterogeneity in social analysis of environmental change: cases from Malawi. Human Ecology 35(1): 69-80.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Yoshida, K. 1992. Masks and transformation among the Chewa of Eastern Zambia. Senri Ethnological Studies 31: 203-273. Japan: National Museum of Ethnology.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Zubieta, L.F. 2006. The rock art of Mwana wa Chentcherere II rock shelter, Malawi: a site-specific study of girls ’ initiation rock art. Leiden: African Studies Centre.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Zubieta, L.F. 2009. The rock art of Chinamwali: material culture and girls' initiation in south-central Africa. Ph.D. thesis. Johannesburg: University of the Witwatersrand, Johannesburg, South Africa.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Zubieta, L.F. 2012. Animals and humans: metaphors of representation in south-central African rock art. In: Working with rock art: recording, presenting and understanding rock Art using indigenous knowledge (eds. Smith, B.W., D. Morris, and K. Helskog). Pp. 169 - 177. Johannesburg: Wits University Press.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Zubieta, L. F. 2014. The rock art of Chinamwali and its sacred landscape. In: Rock art and sacred landscapes (eds. Gillette, D., B. Murray, M. Greer, and M. Hayward). One World Archaeology Series. Pp. 49 - 66. New Mexico: Springer.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Zubieta, L. F. 2016. Learning through practise: Chewa women’s roles and the use of rock art in passing on cultural knowledge. Journal of Anthropological Archaeology 43: 13-28.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
