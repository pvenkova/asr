<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amerjpoliscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100487</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Journal of Political Science</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Subscription Services</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00925853</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15405907</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23025048</article-id>
         <title-group>
            <article-title>Foreign Aid, Democratization, and Civil Conflict: How Does Democracy Aid Affect Civil Conflict?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Burcu</given-names>
                  <surname>Savun</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel C.</given-names>
                  <surname>Tirone</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">55</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23024871</issue-id>
         <fpage>233</fpage>
         <lpage>246</lpage>
         <permissions>
            <copyright-statement>© 2011 Midwest Political Science Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23025048"/>
         <abstract>
            <p>It has been suggested that democratizing states are prone to civil wars. However, not all democratizing states experience domestic political violence. We argue that one of the key factors that "shelters" some democratizing states from domestic political violence is the receipt of democracy aid. Democratizing states that receive high levels of democracy assistance are less likely to experience civil conflict than countries that receive little or no external democracy assistance. During democratic transitions, the central authority weakens and uncertainty about future political commitments and promises among domestic groups increases. Democracy aid decreases the risk of conflict by reducing commitment problems and uncertainty. Using an instrumental variables approach that accounts for potential endogeneity problems in aid allocation, we find empirical support for our argument. We conclude that there is a potential path to democracy that ameliorates the perils of democratization, and democracy assistance programs can play a significant positive role in this process.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1121e153a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1121e160" publication-type="other">
Gleditsch et al. 2002</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1121e166" publication-type="other">
Marshall and Jaggers 2002</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e173a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1121e180" publication-type="other">
Kauffmann 1996</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e187a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1121e194" publication-type="other">
http:
//www.usaid.gov/our_work/cross-cutting_programs/transition_
initiatives/country/indones/progdesc.html. Accessed on January
6, 2010.</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e210a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1121e217" publication-type="other">
Kelley 2009</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e225a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1121e232" publication-type="other">
DAC Statistical Direc-
tives (2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e242a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1121e249" publication-type="other">
Balla and Reinhardt (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e256a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1121e263" publication-type="other">
Miguel, Satyanath, and Sergenti (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1121e269" publication-type="other">
de Ree and Nillesen (2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e276a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1121e283" publication-type="other">
Miguel,
Satyanath, and Sergenti (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1121e292" publication-type="other">
Angrist and Krueger (2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e299a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1121e306" publication-type="other">
Alesina and Dollar (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e313a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1121e320" publication-type="other">
Bearce and Bondanella (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e328a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1121e335" publication-type="other">
Baum, Schaffer, and Stillman (2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e342a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1121e349" publication-type="other">
Bearce, Flanagan, and
Floros (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1121e358" publication-type="other">
Wright (2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d1121e365a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1121e372" publication-type="other">
Treier and Jackman (2008)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1121e388a1310">
            <mixed-citation id="d1121e392" publication-type="other">
Alesina, Alberto, and David Dollar. 2000. "Who Gives Foreign
Aid to Whom and Why?" Journal of Economic Growth 5(1):
33-63.</mixed-citation>
         </ref>
         <ref id="d1121e405a1310">
            <mixed-citation id="d1121e409" publication-type="other">
Angrist, Joshua D., and Alan B. Krueger. 2001. "Instrumental
Variables and the Search for Identification: From Supply
and Demand to Natural Experiments." Journal of Economic
Perspectives 15(4): 69-85.</mixed-citation>
         </ref>
         <ref id="d1121e425a1310">
            <mixed-citation id="d1121e429" publication-type="other">
Angrist, Joshua D., and Jorn-Steffen Pischke. 2009. Mostly
Harmless Econometrics: An Empiricist's Companion. Prince-
ton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1121e442a1310">
            <mixed-citation id="d1121e446" publication-type="other">
Balla, Eliana, and Gina Yannitell Reinhardt. 2008. "Giving and
Receiving Foreign Aid: Does Conflict Count? World Devel-
opment 36(12): 2566-85.</mixed-citation>
         </ref>
         <ref id="d1121e460a1310">
            <mixed-citation id="d1121e464" publication-type="other">
Baum, Christopher F., Mark E. Schaffer, and Steven Stillman.
2007. ivreg2: Stata module for extended instrumental vari-
ables/2SLS, GMM and AC/HAC, LIML, and k-class regres-
sion. Boston College Department of Economics, Statistical
Software Components S425401.</mixed-citation>
         </ref>
         <ref id="d1121e483a1310">
            <mixed-citation id="d1121e487" publication-type="other">
Bearce, David, and Stacy Bondanella. 2007. "Intergovernmen-
tal Organizations, Socialization, and Member-State Interest
Convergence." International Organization 61(4): 703-33.</mixed-citation>
         </ref>
         <ref id="d1121e500a1310">
            <mixed-citation id="d1121e504" publication-type="other">
Bearce, David, Kristen M. Flanagan, and Katharine M. Floros.
2006. "Alliances, Internal Information, and Military Conflict
among Member-States." International Organization 60(3):
595-625.</mixed-citation>
         </ref>
         <ref id="d1121e520a1310">
            <mixed-citation id="d1121e524" publication-type="other">
Beck, Nathaniel, Jonathan N. Katz, and Richard Tucker. 1998.
"Taking Time Seriously: Time-Series-Cross-Section Analy-
sis with a Binary Dependent Variable." American Journal of
Political Science 42(4): 1260-88.</mixed-citation>
         </ref>
         <ref id="d1121e540a1310">
            <mixed-citation id="d1121e546" publication-type="other">
Burton, Michael, Richard Gunther, and John Higley. 1992.
"Introduction; Elite Transformations and Democratic
Regimes." In Elites and Democratic Consolidation in Latin
America and Southern Europe, ed. John Higley and Richard
Gunther. New York: Cambridge University Press, 1-37.</mixed-citation>
         </ref>
         <ref id="d1121e565a1310">
            <mixed-citation id="d1121e569" publication-type="other">
Carothers, Thomas. 1996. Assessing Democracy Assistance: The
Case of Romania. Washington, DC: Carnegie Endowment
for International Peace.</mixed-citation>
         </ref>
         <ref id="d1121e583a1310">
            <mixed-citation id="d1121e587" publication-type="other">
Carothers, Thomas. 1999. Aiding Democracy Abroad: The Learn-
ing Curve. Washington, DC: Carnegie Endowment for In-
ternational Peace.</mixed-citation>
         </ref>
         <ref id="d1121e600a1310">
            <mixed-citation id="d1121e604" publication-type="other">
Collier, Paul, and Anke Hoeffler. 2004. "Greed and Grievance
in Civil Wars." Oxford Economic Papers 56(4): 563-95.</mixed-citation>
         </ref>
         <ref id="d1121e614a1310">
            <mixed-citation id="d1121e618" publication-type="other">
Crawford, Gordon. 2001. Foreign Aid and Political Reform: A
Comparative Analysis of Democracy Assistance and Political
Conditionality. New York: Palgrave.</mixed-citation>
         </ref>
         <ref id="d1121e631a1310">
            <mixed-citation id="d1121e635" publication-type="other">
de Ree, loppe, and Eleonora Nillesen. 2009. "Aiding Violence or
Peace? The Impact of Foreign Aid on the Risk of Civil Con-
flict in Sub-Saharan Africa." Journal of Development Eco-
nomics 88(2): 301-13.</mixed-citation>
         </ref>
         <ref id="d1121e651a1310">
            <mixed-citation id="d1121e655" publication-type="other">
Devarajan, Shantayanan, David Dollar, and Torgny Holmgren,
eds. 2001. Aid and Reform in Africa. Washington, DC: World
Bank.</mixed-citation>
         </ref>
         <ref id="d1121e668a1310">
            <mixed-citation id="d1121e672" publication-type="other">
Diamond, Larry. 1995. Promoting Democracy in the 1990s: Actors
and Instruments, Issues and Imperatives. New York: Carnegie
Corporation of New York.</mixed-citation>
         </ref>
         <ref id="d1121e686a1310">
            <mixed-citation id="d1121e690" publication-type="other">
Djankov, Simeon, Jose G. Montalvo, and Marta Reynal-Querol.
2008. "The Curse of Aid." Journal of Economic Growth 13(3):
169-94.</mixed-citation>
         </ref>
         <ref id="d1121e703a1310">
            <mixed-citation id="d1121e707" publication-type="other">
Enterline, Andrew. 1996. "Driving While Democratizing." In-
ternational Security 20(4): 183-96.</mixed-citation>
         </ref>
         <ref id="d1121e717a1310">
            <mixed-citation id="d1121e721" publication-type="other">
Fearon, James D. 1998. "Commitment Problems and the Spread
of Ethnic Conflict." In The International Spread of Eth-
nic Conflict: Fear, Diffusion, and Escalation, ed. David Lake
and Donald Rothchild. Princeton, NJ: Princeton University
Press, 107-26.</mixed-citation>
         </ref>
         <ref id="d1121e740a1310">
            <mixed-citation id="d1121e744" publication-type="other">
Fearon, James D., and David D. Laitin. 2003. "Ethnicity, In-
surgency, and Civil War." American Political Science Review
97(1): 1-17.</mixed-citation>
         </ref>
         <ref id="d1121e757a1310">
            <mixed-citation id="d1121e761" publication-type="other">
Finkel, Steven, Anibal Perez-Linan, and Mitchell Seligson. 2007.
"The Effects of U.S. Foreign Assistance on Democracy Build-
ing, 1990-2003." World Politics 59(3): 404-39.</mixed-citation>
         </ref>
         <ref id="d1121e774a1310">
            <mixed-citation id="d1121e778" publication-type="other">
Gartzke, Erik, and Dong-Joon Jo. 2006. United Nations General
Assembly Voting, 1946-1996. Version 4.0.</mixed-citation>
         </ref>
         <ref id="d1121e789a1310">
            <mixed-citation id="d1121e793" publication-type="other">
Gleditsch, Kristian Skrede. 2002. All International Politics Is
Local: The Diffusion of Conflict, Integration, and Democrati-
zation. Ann Arbor: University of Michigan Press.</mixed-citation>
         </ref>
         <ref id="d1121e806a1310">
            <mixed-citation id="d1121e810" publication-type="other">
Gleditsch, Kristian Skrede, and Michael D. Ward. 2000. "Peace
and War in Time and Space: The Role of Democratization."
International Studies Quarterly 44(1): 1-29.</mixed-citation>
         </ref>
         <ref id="d1121e823a1310">
            <mixed-citation id="d1121e827" publication-type="other">
Gleditsch, Nils Petter, Peter Wallensteen, Mikael Eriksson, Mar-
gareta Sollenberg, and Havard Strand. 2002. "Armed Con-
flict 1946-2001: A New Dataset." Journal of Peace Research
39(5): 615-37.</mixed-citation>
         </ref>
         <ref id="d1121e843a1310">
            <mixed-citation id="d1121e847" publication-type="other">
Goldsmith, Arthur. 2010. "Mixed Regimes and Political Vi-
olence in Africa." Journal of Modern African Studies
48(3): 383-411.</mixed-citation>
         </ref>
         <ref id="d1121e860a1310">
            <mixed-citation id="d1121e864" publication-type="other">
Gyimah-Boadi, E. 1999. "Institutionalizing Credible Elections
in Ghana." In The Self-Restraining State: Power and Account-
ability in New Democracies, ed. Andreas Schedler, Larry Jay
Diamond, and Marc F. Plattner. Boulder: Lynne Reinner,
105-21.</mixed-citation>
         </ref>
         <ref id="d1121e883a1310">
            <mixed-citation id="d1121e887" publication-type="other">
Hansen, Gary. 1996. "Constituencies for Reform: Strategic Ap-
proaches for Donor-Supported Civic Advocacy Groups."
USAID Program and Operations Assessment Report No.
12. (PN-ABS-534).</mixed-citation>
         </ref>
         <ref id="d1121e904a1310">
            <mixed-citation id="d1121e908" publication-type="other">
Hawkins, Darren. 2008. "Protecting Democracy in Europe and
the Americas." International Organization 62(3): 373-403.</mixed-citation>
         </ref>
         <ref id="d1121e918a1310">
            <mixed-citation id="d1121e922" publication-type="other">
Hegre, Havard, Tanja Ellingsen, Scott Gates, and Nils Petter
Gleditsch. 2001. "Toward a Democratic Civil Peace? Democ-
racy, Political Change, and Civil War, 1816-1992." American
Political Science Review 95(1): 33-48.</mixed-citation>
         </ref>
         <ref id="d1121e938a1310">
            <mixed-citation id="d1121e942" publication-type="other">
Heston, Alan, Robert Summers, and Bettina Aten. 2006. "Penn
World Table Version 6.2." University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="d1121e952a1310">
            <mixed-citation id="d1121e956" publication-type="other">
Jeffries, Richard. 1998. "The Ghanaian Elections of 1996: To-
wards the Consolidation of Democracy?" African Affairs
97(387): 189-208.</mixed-citation>
         </ref>
         <ref id="d1121e969a1310">
            <mixed-citation id="d1121e973" publication-type="other">
Kalyvitis, Sarantis C., and Irene Vlachaki. 2010. "Democracy
Aid and the Democratization of Recipients." Contemporary
Economic Policy 28(2): 188-218.</mixed-citation>
         </ref>
         <ref id="d1121e986a1310">
            <mixed-citation id="d1121e990" publication-type="other">
Karl, Terry. 1990. "Dilemmas of Democratization in Latin
America." Comparative Politics 23(1): 1-21.</mixed-citation>
         </ref>
         <ref id="d1121e1001a1310">
            <mixed-citation id="d1121e1005" publication-type="other">
Kauffmann, Chaim. 1996. "Possible and Impossible Solutions
to Ethnic Civil Wars." International Security 20(4): 136-75.</mixed-citation>
         </ref>
         <ref id="d1121e1015a1310">
            <mixed-citation id="d1121e1019" publication-type="other">
Kelley, Judith. 2009. "D-Minus Elections: The Politics and
Norms of International Election Observation." International
Organization 63(4): 765-87.</mixed-citation>
         </ref>
         <ref id="d1121e1032a1310">
            <mixed-citation id="d1121e1036" publication-type="other">
Knack, Stephen. 2001. "Aid Dependence and the Quality of
Governance: Cross-Country Empirical Tests." Southern Eco-
nomic Journal 68(2): 310-29.</mixed-citation>
         </ref>
         <ref id="d1121e1049a1310">
            <mixed-citation id="d1121e1053" publication-type="other">
Knack, Stephen. 2004. "Does Foreign Aid Promote Democ-
racy?" International Studies Quarterly 48(1): 251-66.</mixed-citation>
         </ref>
         <ref id="d1121e1063a1310">
            <mixed-citation id="d1121e1067" publication-type="other">
Lake, David, and Donald Rothchild. 1996. "Containing Fear:
The Origins and Management of Ethnic Conflict." Interna-
tional Security 21(2): 41-75.</mixed-citation>
         </ref>
         <ref id="d1121e1080a1310">
            <mixed-citation id="d1121e1084" publication-type="other">
Mann, Michael. 1999. "The Dark Side of Democracy: The Mod-
ern Tradition of Ethnic and Political Economy." New Left
Review 1/235: 18-45.</mixed-citation>
         </ref>
         <ref id="d1121e1098a1310">
            <mixed-citation id="d1121e1102" publication-type="other">
Mann, Michael. 2005. The Dark Side of Democracy Explaining
Ethnic Cleansing. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1121e1112a1310">
            <mixed-citation id="d1121e1116" publication-type="other">
Mansfield, Edward D., and Jon C. Pevehouse. 2006. "Democ-
ratization and International Organizations." International
Organization 60(1): 137-67.</mixed-citation>
         </ref>
         <ref id="d1121e1129a1310">
            <mixed-citation id="d1121e1133" publication-type="other">
Mansfield, Edward D., and Jack Snyder. 1995. "Democratization
and the Danger of War." International Security 20(1): 5-38.</mixed-citation>
         </ref>
         <ref id="d1121e1143a1310">
            <mixed-citation id="d1121e1147" publication-type="other">
Mansfield, Edward D., and Jack Snyder. 1997. "A Reply to
Thompson and Tucker." Journal of Conflict Resolution 41(3):
457-61.</mixed-citation>
         </ref>
         <ref id="d1121e1160a1310">
            <mixed-citation id="d1121e1164" publication-type="other">
Mansfield, Edward D., and Jack Snyder. 2002. "Democratic
Transitions, Institutional Capacity, and the Onset of War."
International Organization 56(2): 297-337.</mixed-citation>
         </ref>
         <ref id="d1121e1177a1310">
            <mixed-citation id="d1121e1181" publication-type="other">
Mansfield, Edward D., and Jack Snyder. 2005. Electing to Fight.
Why Emerging Democracies Go to War. Cambridge, MA: MIT
Press.</mixed-citation>
         </ref>
         <ref id="d1121e1195a1310">
            <mixed-citation id="d1121e1199" publication-type="other">
Mansfield, Edward D., and Jack Snyder. 2007. "Turbulent Tran-
sitions: Why Emerging Democracies Go to War." In Leashing
the Dogs of War: Conflict Management in a Divided World,
ed. Chester A. Crocker, Fen Osier Hampson, and Pamela
Aall. Washington, DC: United States Institute of Peace Press,
161-76.</mixed-citation>
         </ref>
         <ref id="d1121e1222a1310">
            <mixed-citation id="d1121e1226" publication-type="other">
Mansfield, Edward D., and Jack Snyder. 2009. "Pathways to
War in Democratic Transitions." International Organization
63(2): 381-90.</mixed-citation>
         </ref>
         <ref id="d1121e1239a1310">
            <mixed-citation id="d1121e1243" publication-type="other">
Maoz, Zeev, and Nasrin Abdoladi. 1989. "Regime Types and
International Conflict, 1816-1976." Journal of Conflict Res-
olution 33(1): 3-35.</mixed-citation>
         </ref>
         <ref id="d1121e1256a1310">
            <mixed-citation id="d1121e1260" publication-type="other">
Maoz, Zeev, and Bruce Russett. 1993. "Normative and Struc-
tural Causes of Democratic Peace, 1946-1986." American
Political Science Review 87(3): 624-38.</mixed-citation>
         </ref>
         <ref id="d1121e1273a1310">
            <mixed-citation id="d1121e1277" publication-type="other">
Marshall, Monty G., and Keith Jaggers. 2002. Polity IV Dataset.
College Park, MD: Center for International Development
and Conflict Management, University of Maryland.</mixed-citation>
         </ref>
         <ref id="d1121e1290a1310">
            <mixed-citation id="d1121e1294" publication-type="other">
Miguel, Edward, Shanker Satyanath, and Ernest Sergenti. 2004.
"Economic Shocks and Civil Conflict: An Instrumental
Variables Approach." Journal of Political Economy 112(4):
725-53.</mixed-citation>
         </ref>
         <ref id="d1121e1311a1310">
            <mixed-citation id="d1121e1315" publication-type="other">
Morrison, Kevin M. 2009. "Oil, Non-Tax Revenue, and the
Redistributional Foundations of Regime Stability." Interna-
tional Organization 63(1): 107-38.</mixed-citation>
         </ref>
         <ref id="d1121e1328a1310">
            <mixed-citation id="d1121e1332" publication-type="other">
Narang, Vipin, and Rebecca Nelson. 2009. "Who Are These
Belligerent Democratizers? Reassessing the Impact of De-
mocratization on War." International Organization 63(2):
357-79.</mixed-citation>
         </ref>
         <ref id="d1121e1348a1310">
            <mixed-citation id="d1121e1352" publication-type="other">
Norris, Pippa. 2008. Driving Democracy: Do Power-Sharing In-
stitutions Work? New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1121e1362a1310">
            <mixed-citation id="d1121e1366" publication-type="other">
O'Donnell, Guillermo, Philippe C. Schmitter, and Laurence
Whitehead, eds. 1986. Transitions from Authoritarian Rule:
Southern Europe. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d1121e1379a1310">
            <mixed-citation id="d1121e1383" publication-type="other">
OECD. 2000. DAC Statistical Reporting Directives. Paris: Orga-
nization for Economic Cooperation and Development.</mixed-citation>
         </ref>
         <ref id="d1121e1393a1310">
            <mixed-citation id="d1121e1397" publication-type="other">
OECD 2007. Reporting Directives for the Creditor Reporting Sys-
tem. Paris: Organization for Economic Cooperation and De-
velopment.</mixed-citation>
         </ref>
         <ref id="d1121e1411a1310">
            <mixed-citation id="d1121e1415" publication-type="other">
Oneal, John, and Bruce Russett. 1997. "The Classical Liber-
als Were Right: Democracy, Interdependence, and Conflict,
1950-85." International Studies Quarterly 41(2): 267-93.</mixed-citation>
         </ref>
         <ref id="d1121e1428a1310">
            <mixed-citation id="d1121e1432" publication-type="other">
Pevehouse, Jon. 2005. Democracy from Above: Regional Organi-
zations and Democratization. New York: Cambridge Univer-
sity Press.</mixed-citation>
         </ref>
         <ref id="d1121e1445a1310">
            <mixed-citation id="d1121e1449" publication-type="other">
Pridham, Geoffrey. 2000. The Dynamics of Democratization: A
Comparative Approach. New York: Continuum.</mixed-citation>
         </ref>
         <ref id="d1121e1459a1310">
            <mixed-citation id="d1121e1463" publication-type="other">
Ray, James Lee. 1998. "Does Democracy Cause Peace?" Annual
Review of Political Science 1: 27-46.</mixed-citation>
         </ref>
         <ref id="d1121e1473a1310">
            <mixed-citation id="d1121e1477" publication-type="other">
Russett, Bruce. 1993. Grasping the Democratic Peace: Principles
for a Post-Cold War World. Princeton, NJ: Princeton Univer-
sity Press.</mixed-citation>
         </ref>
         <ref id="d1121e1490a1310">
            <mixed-citation id="d1121e1494" publication-type="other">
Russett, Bruce, and John Oneal. 2001. "Clear and Clean: The
Fixed Effects of the Liberal Peace." International Organiza-
tion 55(2): 469-85.</mixed-citation>
         </ref>
         <ref id="d1121e1508a1310">
            <mixed-citation id="d1121e1512" publication-type="other">
Scott, James M., and Carie A. Steele. 2005. "Assisting Democrats
or Resisting Dictators: The Nature and Impact of Democ-
racy Support by the United States National Endowment
for Democracy, 1990-1999." Democratization 12(4): 439-
60.</mixed-citation>
         </ref>
         <ref id="d1121e1531a1310">
            <mixed-citation id="d1121e1535" publication-type="other">
Scott, James M., and Carie A. Steele. 2011. "Sponsoring Democ-
racy: The United States and Democracy Aid to the Devel-
oping World, 1988-2001." International Studies Quarterly.
Forthcoming.</mixed-citation>
         </ref>
         <ref id="d1121e1551a1310">
            <mixed-citation id="d1121e1555" publication-type="other">
Shea, John. 1997. "Instrument Relevance in Multivariate Lin-
ear Models: A Simple Measure." Review of Economics and
Statistics 79(2): 348-52.</mixed-citation>
         </ref>
         <ref id="d1121e1568a1310">
            <mixed-citation id="d1121e1572" publication-type="other">
Smith, Benjamin. 2004. "Oil, Wealth, and Regime Survival in the
Developing World: 1960-1999." American Journal of Political
Science 48(2): 232—46.</mixed-citation>
         </ref>
         <ref id="d1121e1585a1310">
            <mixed-citation id="d1121e1589" publication-type="other">
Snyder, Jack. 2000. From Voting to Violence: Democratization
and Nationalist Conflict. New York: W. W. Norton.</mixed-citation>
         </ref>
         <ref id="d1121e1599a1310">
            <mixed-citation id="d1121e1603" publication-type="other">
Staiger, Douglas, and James H. Stock. 1997. "Instrumental
Variables Regression with Weak Instruments." Econometrica
65(3): 557-86.</mixed-citation>
         </ref>
         <ref id="d1121e1617a1310">
            <mixed-citation id="d1121e1621" publication-type="other">
Stock, James H., and Motohiro Yogo. 2002. "Testing for Weak
Instruments in Linear IV Regression." Technical Working
Paper 284. Cambridge, MA: NBER.</mixed-citation>
         </ref>
         <ref id="d1121e1634a1310">
            <mixed-citation id="d1121e1638" publication-type="other">
Tavares, Jose. 2003. "Does Foreign Aid Corrupt?" Economic
Letters 79(1): 99-106.</mixed-citation>
         </ref>
         <ref id="d1121e1648a1310">
            <mixed-citation id="d1121e1652" publication-type="other">
Treier, Shawn, and Simon Jackman. 2008. "Democracy as a
Latent Variable." American Journal of Political Science 52(1):
201-17.</mixed-citation>
         </ref>
         <ref id="d1121e1665a1310">
            <mixed-citation id="d1121e1669" publication-type="other">
Vreeland, James Raymond. 2008. "The Effect of Political Regime
on Civil War: Unpacking Anocracy." Journal of Conflict Res-
olution 52(3): 401-25.</mixed-citation>
         </ref>
         <ref id="d1121e1682a1310">
            <mixed-citation id="d1121e1686" publication-type="other">
Ward, Michael, and Kristian Skrede Gleditsch. 1998. "Democ-
ratizing for Peace." American Political Science Review 92(1):
51-62.</mixed-citation>
         </ref>
         <ref id="d1121e1699a1310">
            <mixed-citation id="d1121e1703" publication-type="other">
Weingast, Barry R. 1998. "Constructing Trust: The Politics of
Ethnic and Regional Conflict." In Where Is the New Insti-
tutionalism Now? ed. Virginia Haufler, Karol Soltan, and
Eric Uslaner. Ann Arbor: University of Michigan Press, 163-
200.</mixed-citation>
         </ref>
         <ref id="d1121e1723a1310">
            <mixed-citation id="d1121e1727" publication-type="other">
Wright, Joseph. 2009. "How Foreign Aid Can Foster Democ-
ratization in Authoritarian Regimes." American Journal of
Political Science 53(3): 552-71.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
