<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">climateresearch</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50018851</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Climate Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Inter-Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0936577X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16161572</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24865077</article-id>
         <article-categories>
            <subj-group>
               <subject>HUMAN HEALTH</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Methods for assessing public health vulnerability to global climate change</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jonathan A.</given-names>
                  <surname>Patz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John M.</given-names>
                  <surname>Balbus</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>19</day>
            <month>2</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24864525</issue-id>
         <fpage>113</fpage>
         <lpage>125</lpage>
         <permissions>
            <copyright-statement>Copyright © Inter-Research 1996</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24865077"/>
         <abstract>
            <p>Assessment of the human health risk posed by global climate change presents a new challenge to public health professionals. In contrast to conventional toxicological risk assessment, the health risk assessment related to global climate change must analyze stressors that consist of complex interrelated climate factors and risks that are mediated through intermediate species in varying ecosystems. A framework for ecologically based human health risk assessment helps distinguish the concepts of global climate change risk assessment from conventional risk assessment. Specific methods for linking climate variables with human disease include historical analysis of climate and disease data and the development of integrated mathematical models. Two historical climate-disease studies of malaria in Africa provide a starting point for further analysis. Early approaches to evaluating the human health risks from global climate change will include simple mapping of disease boundaries and climate factors. Computer-based geographical information system (GIS) technology will assist in the organization and analysis of climate, environment and disease data. Ultimately, complex integrated mathematical models may provide quantitative estimates of risk, but these models have not yet been validated. The collection of geographically organized relevant data through either field work or remote sensing technology will both help validate comprehensive integrated models and enhance our understanding of the associations between climate change and human health.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d449e217a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d449e224" publication-type="other">
Stone 1995</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d449e240a1310">
            <mixed-citation id="d449e244" publication-type="other">
Bouma MJ, Sondorp HE, van der Kaay HJ (1994) Climate
change and periodic epidemic malaria. Lancet 343:1440</mixed-citation>
         </ref>
         <ref id="d449e254a1310">
            <mixed-citation id="d449e258" publication-type="other">
Bureau of Meteorology Research Center (1993) Climate
change and the El Nino-Southern Oscillation: report of the
workshop on climate change and the El Nino-Southern
Oscillation. Bureau of Meteorology Research Center,
Report 36</mixed-citation>
         </ref>
         <ref id="d449e277a1310">
            <mixed-citation id="d449e281" publication-type="other">
Centers for Disease Control and Prevention (1994) Address-
ing emerging infectious disease threats: a prevention
strategy for the United States. U.S. Dept. of Health and
Human Services, Public Health Service, Atlanta</mixed-citation>
         </ref>
         <ref id="d449e297a1310">
            <mixed-citation id="d449e301" publication-type="other">
Dobson A, Carper R (1993) Biodiversity. Lancet 342 (October):
1096-1099</mixed-citation>
         </ref>
         <ref id="d449e312a1310">
            <mixed-citation id="d449e316" publication-type="other">
Epstein PR, Rogers DJ, Slooff R (1993) Satellite imaging and
vector-borne disease. Lancet 341:1404-1406</mixed-citation>
         </ref>
         <ref id="d449e326a1310">
            <mixed-citation id="d449e330" publication-type="other">
Focks DA, Haile DG, Daniels E, Mount GA (1993) Dynamic
life table model for Aedes aegypti (L.) (Diptera: Culicidae).
J Med Entomol 30:1003-1017</mixed-citation>
         </ref>
         <ref id="d449e343a1310">
            <mixed-citation id="d449e347" publication-type="other">
Gilles HM (1993) Epidemiology of malaria. In: Gilles HM,
Warrell DA (eds) Bruce-Chwatt's essential malariology.
Edward Arnold Div. of Hodder &amp; Stoughton, London</mixed-citation>
         </ref>
         <ref id="d449e360a1310">
            <mixed-citation id="d449e364" publication-type="other">
Glantz MH, Katz RW, Nicholls N (1991) Teleconnections link-
ing worldwide climate anomalies. Cambridge University
Press, Cambridge</mixed-citation>
         </ref>
         <ref id="d449e377a1310">
            <mixed-citation id="d449e381" publication-type="other">
Glass GE, Aron JL, Ellis JH, Yoon SS (1993) Applications of
GIS technology to disease control. The Johns Hopkins
University, Baltimore</mixed-citation>
         </ref>
         <ref id="d449e394a1310">
            <mixed-citation id="d449e398" publication-type="other">
Grant LD (1990) Respiratory effects associated with global
climate change. In: White JC (ed) Global atmospheric
change and public health. Elsevier, New York</mixed-citation>
         </ref>
         <ref id="d449e412a1310">
            <mixed-citation id="d449e416" publication-type="other">
Haines A, Fuchs C (1991) Potential impacts on health of
atmospheric change. J Pub Health Med 13(2):69-80</mixed-citation>
         </ref>
         <ref id="d449e426a1310">
            <mixed-citation id="d449e430" publication-type="other">
Houghton JT, Callander BA, Varney SK (eds) (1992) Climate
change 1992: the supplementary report to the IPCC scien-
tific assessment. Cambridge University Press, Cambridge</mixed-citation>
         </ref>
         <ref id="d449e443a1310">
            <mixed-citation id="d449e447" publication-type="other">
IPCC (1994) Radiative forcing of climate change, the 1994
report of the scientific assessment working group of IPCC:
summary for policy makers. Oxford University Press,
Oxford</mixed-citation>
         </ref>
         <ref id="d449e463a1310">
            <mixed-citation id="d449e467" publication-type="other">
Jetten TH, Takken W (1994) Impact of climate change on
malaria vectors. Clim Change 18:10-12</mixed-citation>
         </ref>
         <ref id="d449e477a1310">
            <mixed-citation id="d449e481" publication-type="other">
Kalkstein LS (1991) A new approach to evaluate the impact of
climate upon human mortality. Environ Health Perspect
96:145-150</mixed-citation>
         </ref>
         <ref id="d449e494a1310">
            <mixed-citation id="d449e498" publication-type="other">
Kalkstein LS, Smoyer KE (1993) The impact of climate change
on human health: some international implications. Experi-
entia 49:469-479</mixed-citation>
         </ref>
         <ref id="d449e512a1310">
            <mixed-citation id="d449e516" publication-type="other">
Karl TR, Knight RW, Easterling DR, Quayle RG (1995) Trends
in the U.S. climate during the twentieth century. Conse-
quences 1:3-12</mixed-citation>
         </ref>
         <ref id="d449e529a1310">
            <mixed-citation id="d449e533" publication-type="other">
Koopman JS, Prevots DR, Marin MAV, Dantes HG, Aqino
MLZ, Longini IMJ, Amor JS (1991) Determinants and pre-
dictors of dengue infection in Mexico. Am J Epidemiol
133:1168-1178</mixed-citation>
         </ref>
         <ref id="d449e549a1310">
            <mixed-citation id="d449e553" publication-type="other">
Leaf A (1989) Potential health effects of global climatic and
environmental changes. N Engl J Med 321:1577-1583</mixed-citation>
         </ref>
         <ref id="d449e563a1310">
            <mixed-citation id="d449e567" publication-type="other">
Linthincum KJ, Bailey CL, Davies FG, Tucker CJ (1987)
Detection of Rift Valley Fever viral activity in Kenya by
satellite remote sensing imagery. Science 235:1656-1659</mixed-citation>
         </ref>
         <ref id="d449e580a1310">
            <mixed-citation id="d449e584" publication-type="other">
Loevinsohn M (1994) Climatic warming and increased
malaria incidence in Rwanda. Lancet 343:714-718</mixed-citation>
         </ref>
         <ref id="d449e594a1310">
            <mixed-citation id="d449e598" publication-type="other">
Martens WJM (1996) Global atmospheric change and human
health: an integrated modelling approach. Clim Res
6:107-112</mixed-citation>
         </ref>
         <ref id="d449e612a1310">
            <mixed-citation id="d449e616" publication-type="other">
Martens WJM, Rotmans J, Niessen LW (1994) Climate change
and malaria risk: an integrated modeling approach. RIVM,
GLOBO Report Series 3, Report 461-502-003, Bilthoven</mixed-citation>
         </ref>
         <ref id="d449e629a1310">
            <mixed-citation id="d449e633" publication-type="other">
Mather JR (1993) Workbook in applied climatology. Publ Cli-
matol 46:2-109</mixed-citation>
         </ref>
         <ref id="d449e643a1310">
            <mixed-citation id="d449e647" publication-type="other">
Matsuoka Y, Kai K (1995) An estimation of climatic change
effects on malaria. J Global Environ Engineering 1:43-57</mixed-citation>
         </ref>
         <ref id="d449e657a1310">
            <mixed-citation id="d449e661" publication-type="other">
McMichael AJ, Martens WJM (1995) The health impacts of
global climate change: grappling with scenarios, predic-
tive models, and multiple uncertainties. Ecosystem Health
1(1) :23—33</mixed-citation>
         </ref>
         <ref id="d449e677a1310">
            <mixed-citation id="d449e681" publication-type="other">
Mills DM (1995) A climatic water budget approach to black
fly population dynamics. Publ Climatol 95</mixed-citation>
         </ref>
         <ref id="d449e691a1310">
            <mixed-citation id="d449e695" publication-type="other">
Nicholls N (1993) El Nino-southern oscillation and vector-
borne disease. Lancet 342:1284-1285</mixed-citation>
         </ref>
         <ref id="d449e706a1310">
            <mixed-citation id="d449e710" publication-type="other">
Parry ML, Rosenzweig C (1993) Food supply and the risk of
hunger. Lancet 342:1345-1347</mixed-citation>
         </ref>
         <ref id="d449e720a1310">
            <mixed-citation id="d449e724" publication-type="other">
Patz JA (in press) Global climate change and public health.
In: Shahi G, Levy B, Kjellstrom T, Lawrence R, Binger A
(eds) International perspectives in environment, develop-
ment and health: toward a sustainable world. Springer
Publications, New York</mixed-citation>
         </ref>
         <ref id="d449e743a1310">
            <mixed-citation id="d449e747" publication-type="other">
Reiter P (1988) Weather, vector biology, and arboviral recru-
descence. In: Monath TP (ed) The arboviruses: epidemiol-
ogy and ecology. CRC Press, Boca Raton, p 245-255</mixed-citation>
         </ref>
         <ref id="d449e760a1310">
            <mixed-citation id="d449e764" publication-type="other">
Risk Assessment Forum (1992) Framework for ecological risk
assessment. U.S. Environmental Protection Agency,
EPA/630/R-92-001, Washington, DC</mixed-citation>
         </ref>
         <ref id="d449e777a1310">
            <mixed-citation id="d449e781" publication-type="other">
Rogers DJ, Randolph SE (1991) Mortality rates and population
density of tsetse flies correlated with satellite imagery.
Nature 351:739-741</mixed-citation>
         </ref>
         <ref id="d449e794a1310">
            <mixed-citation id="d449e798" publication-type="other">
Rogers DL, Packer MJ (1993) Vector-borne diseases, models,
and global change. Lancet 342:1282-1284</mixed-citation>
         </ref>
         <ref id="d449e809a1310">
            <mixed-citation id="d449e813" publication-type="other">
Rotmans J et al. (1994) Global change and sustainable
development: the TARGETS approach. RIVM, Report
461502004, Bilthoven</mixed-citation>
         </ref>
         <ref id="d449e826a1310">
            <mixed-citation id="d449e830" publication-type="other">
Scripps Institution of Oceanography (1992) Simulated global
warming and model ENSO cycles. Scripps Institution of
Oceanography, Ref 9404, San Diego</mixed-citation>
         </ref>
         <ref id="d449e843a1310">
            <mixed-citation id="d449e847" publication-type="other">
Shope RE (1991) Global climate change and infectious dis-
eases. Environ Health Perspectives 96:171-174</mixed-citation>
         </ref>
         <ref id="d449e857a1310">
            <mixed-citation id="d449e861" publication-type="other">
Stone R (1995) A molecular approach to cancer risk (news &amp;
comment). Science 268:356-357</mixed-citation>
         </ref>
         <ref id="d449e871a1310">
            <mixed-citation id="d449e875" publication-type="other">
Taylor P, Mutambu SL (1986) A review of the malaria situa-
tion in Zimbabwe with special reference to the period
1972-1981. Trans Roy Soc Trop Med Hyg 80:12-19</mixed-citation>
         </ref>
         <ref id="d449e888a1310">
            <mixed-citation id="d449e892" publication-type="other">
Tegart WJM, Sheldon GW (eds) (1992) Climate change 1992:
the supplementary report to the IPCC impacts assessment.
Australian Government Publishing Service, Canberra</mixed-citation>
         </ref>
         <ref id="d449e906a1310">
            <mixed-citation id="d449e912" publication-type="other">
United Nations Environment Program (UNEP) and the
National Center for Atmospheric Research (1991) ENSO
and climate change. Bangkok</mixed-citation>
         </ref>
         <ref id="d449e925a1310">
            <mixed-citation id="d449e929" publication-type="other">
United Nations Institute for Training and Research (1994)
Change and time series analysis. UNITAR, Geneva</mixed-citation>
         </ref>
         <ref id="d449e939a1310">
            <mixed-citation id="d449e943" publication-type="other">
Washino RK, Wood BL (1994) Application of remote sensing to
arthropod vector surveillance and control. Am J Trop Med
Hyg 50(Suppl 6):134-144</mixed-citation>
         </ref>
         <ref id="d449e956a1310">
            <mixed-citation id="d449e960" publication-type="other">
Watts DM, Burke DS, Harrison BA (1987) Effect of tempera-
ture on the vector efficiency of Aedes aegypti for dengue
2 virus. Am J Trop Med Hyg 36:143-152</mixed-citation>
         </ref>
         <ref id="d449e973a1310">
            <mixed-citation id="d449e977" publication-type="other">
WHO (1990) Potential health effects of climatic change. World
Health Organization, Geneva</mixed-citation>
         </ref>
         <ref id="d449e987a1310">
            <mixed-citation id="d449e991" publication-type="other">
WHO (1995) Climate and health in a changing world. World
Health Organization, Geneva</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
