<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinteaffa</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50011763</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of International Affairs</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>School of International and Public Affairs at Columbia University</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0022197X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24388178</article-id>
         <article-categories>
            <subj-group>
               <subject>Inside the Authoritarian State</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>STATE INSTITUTIONS AND THE SURVIVAL OF DICTATORSHIPS</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Natasha M.</given-names>
                  <surname>Ezrow</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Erica</given-names>
                  <surname>Frantz</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2011</year>
      
            <day>1</day>
            <month>12</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">65</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24388174</issue-id>
         <fpage>1</fpage>
         <lpage>13</lpage>
         <permissions>
            <copyright-statement>© The Trustees of Columbia University in the City of New York</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24388178"/>
         <abstract>
            <p>Nominally democratic institutions such as political parties and legislatures are common in dictatorships, which rely on them to maintain control of the state. Parties and legislatures provide a means through which dictatorships co-opt potential opponents, distribute rents to supporters and mitigate elite conflicts. Indeed, regimes with these institutions have longer tenures than those without them. Using evidence from postwar dictatorships, this study demonstrates that parties and legislatures also enhance the ability of authoritarian regimes to withstand leadership transitions. Transfers of power are inherently destabilizing. Yet we find that dictatorships with parties and legislatures are far less likely to be associated with instability because these institutions insulate regimes from the disruptive effects of unconstitutional leadership transfers.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>NOTES</title>
         <ref id="d1380e165a1310">
            <label>1</label>
            <mixed-citation id="d1380e172" publication-type="other">
In this study we use the terms dictatorship and authoritarian regime interchangeably. We define
dictatorships as regimes where no turnover of executive power has occurred. See Barbara Geddes,
"What Do We Know about Democratization after Twenty Years?" Annual Review of Political Science 2
(1999): 115-44.</mixed-citation>
         </ref>
         <ref id="d1380e188a1310">
            <label>2</label>
            <mixed-citation id="d1380e195" publication-type="other">
"Political Party Organizations," Administration and Cost of Elections Project, Electoral Knowledge
Network, http://aceproject.org/main/english/ei/eid04.htm.</mixed-citation>
         </ref>
         <ref id="d1380e205a1310">
            <label>3</label>
            <mixed-citation id="d1380e212" publication-type="other">
Legislatures are distinguished from consultative councils in that they issue laws.</mixed-citation>
         </ref>
         <ref id="d1380e219a1310">
            <label>4</label>
            <mixed-citation id="d1380e226" publication-type="other">
For an extensive discussion of how parties and legislatures function under dictatorships, see
Jennifer Gandhi, Political Institutions under Dictatorship (Cambridge, UK: Cambridge University Press,
2008).</mixed-citation>
         </ref>
         <ref id="d1380e240a1310">
            <label>5</label>
            <mixed-citation id="d1380e247" publication-type="other">
Lisa Blaydes, "Authoritarian Elections and Elite Management: Theory and Evidence from Egypt"
(paper prepared for delivery at the Princeton University Conference on Dictatorships, Princeton,
NJ, April 2008), 2-4; Beatriz Magaloni, Voting for Autocracy: Hegemonic Party Survival and its Demise in
Mexico (Cambridge, UK: Cambridge University Press, 2006), 20; Barbara Geddes, "Why Parties and
Elections in Authoritarian Regimes?" (paper, American Political Science Association, Washington,
DC, September 2005).</mixed-citation>
         </ref>
         <ref id="d1380e270a1310">
            <label>6</label>
            <mixed-citation id="d1380e277" publication-type="other">
Geddes, "Parties and Elections," 8; Blaydes, "Authoritarian Elections and Elite Management";
Magaloni, Voting for Autocracy: Jennifer Gandhi and Adam Przeworski, "Cooperation, Cooptation and
Rebellion under Dictatorships," Ecmiomics and Politics 18, no. 1 (March 2006): 1-26.</mixed-citation>
         </ref>
         <ref id="d1380e290a1310">
            <label>7</label>
            <mixed-citation id="d1380e297" publication-type="other">
We measure dictatorships using the Geddes, Wright and Frantz update of an earlier classification
system for authoritarian regimes proposed by Geddes. See Barbara Geddes, Paradigms and Sand Castles:
Theory Building and Research Design in Comparative Politics (Ann Arbor: University of Michigan Press,
2003); Barbara Geddes, Joseph Wright and Erica Frantz, "Authoritarian Regimes: A New Data Set"
(unpublished manuscript, October 2011). We measure parties and legislatures using Cheibub, Gandhi
and Vreeland's classification system for authoritarian institutions. See Jose Antonio Cheibub, Jennifer
Gandhi and James Raymond Vreeland, "Democracy and Dictatorship Revisited," Public Choice 143,
no. 1-2 (2010): 67-101.</mixed-citation>
         </ref>
         <ref id="d1380e326a1310">
            <label>8</label>
            <mixed-citation id="d1380e333" publication-type="other">
Of the regime years in the sample, 71 percent are characterized by at least one party and a legisla-
ture, 14 percent by at least one party but no legislature, 10 percent by neither a party nor a legislature
and only 5 percent by a legislature but no party. Because it is very uncommon for dictatorships to
operate with a legislature but no party, these cases are not included in the discussion here.</mixed-citation>
         </ref>
         <ref id="d1380e349a1310">
            <label>9</label>
            <mixed-citation id="d1380e356" publication-type="other">
These regimes are Oman from 1970 to 2008, Qatar from 1971 to 2008, the United Arab Emirates
from 1972 to 2008 and Saudi Arabia from 1946 to 2008. All four countries are monarchies. It should
be noted that in monarchies, the ruling family functions very similarly to a political party. The power
of the monarch is checked by the ruling family, the family engages in discussion and consultation
before making decisions and the ruling family distributes spoils among members of the royal family
to keep everyone satisfied.</mixed-citation>
         </ref>
         <ref id="d1380e379a1310">
            <label>10</label>
            <mixed-citation id="d1380e386" publication-type="other">
By regime, we are referring to the group of individuals that holds power—either a single party, a
military junta, a single person, a hybrid of these or a ruling family. We use the term "last year of the
dictatorship" to describe the year preceding the transition of power away from the hands of power-
holders (but not necessarily a transition from a dictatorship to another form of government).</mixed-citation>
         </ref>
         <ref id="d1380e403a1310">
            <label>11</label>
            <mixed-citation id="d1380e410" publication-type="other">
Authoritarian leaders are distinct from authoritarian regimes. While some regimes collapse with
the ouster of the leader, in many dictatorships intra-regime leadership turnover is quite common. Erica
Frantz and Natasha Ezrow, The Politics of Dictatorships: Institutions and Outcomes in Authoritarian Regimes
(Boulder: Lynne Rienner, 2011).</mixed-citation>
         </ref>
         <ref id="d1380e426a1310">
            <label>12</label>
            <mixed-citation id="d1380e433" publication-type="other">
This contrasts with unconstitutional transfers of power, which typically occur via coups or revolts.
Hein E. Goemans, Kristian Skrede Gleditsch and Giacomo Chiozza, "Archigos: A Data Set on Leaders
1875-2004," Codebook Version 2.9, 13 August 2009, http://www.rochester.edu/college/faculty/hgoe-
mans/Archigos.2.9-August.pdf.</mixed-citation>
         </ref>
         <ref id="d1380e449a1310">
            <label>13</label>
            <mixed-citation id="d1380e456" publication-type="other">
Some dictatorships allow multiple political parties to operate. At least one of these parties,
however, is usually affiliated with the regime leadership.</mixed-citation>
         </ref>
         <ref id="d1380e466a1310">
            <label>14</label>
            <mixed-citation id="d1380e473" publication-type="other">
Beatriz Magaloni, "Credible Power-Sharing and the Longevity of Authoritarian Rule," Comparative
Political Studies 41, no. 4/5 (April/May 2008): 715-41.</mixed-citation>
         </ref>
         <ref id="d1380e483a1310">
            <label>15</label>
            <mixed-citation id="d1380e490" publication-type="other">
Barbara Geddes, "Minimum-Winning Coalitions and Personalization of Rule in Authoritarian
Regimes" (paper presented at the American Political Science Association, Chicago, IL, August
2004).</mixed-citation>
         </ref>
         <ref id="d1380e503a1310">
            <label>16</label>
            <mixed-citation id="d1380e510" publication-type="other">
For a discussion of political parties as forums for power distribution, see Magaloni, Voting for
Autocracy, Magaloni, "Credible Power-Sharing." For a discussion of parties as vehicles for alliance
forging, see Barbara Geddes, "Why Parties are Created after Authoritarian Seizures of Power" (paper,
American Political Science Association, Chicago, IL, August 2007).</mixed-citation>
         </ref>
         <ref id="d1380e527a1310">
            <label>17</label>
            <mixed-citation id="d1380e534" publication-type="other">
Blaydes, "Authoritarian Elections"; Geddes, "Parties and Elections"; Magaloni, Voting for
Autocracy.</mixed-citation>
         </ref>
         <ref id="d1380e544a1310">
            <label>18</label>
            <mixed-citation id="d1380e551" publication-type="other">
Blaydes, "Authoritarian Elections," 3.</mixed-citation>
         </ref>
         <ref id="d1380e558a1310">
            <label>19</label>
            <mixed-citation id="d1380e565" publication-type="other">
Magaloni, Voting for Autocracy; Magaloni, "Credible Power-Sharing."</mixed-citation>
         </ref>
         <ref id="d1380e572a1310">
            <label>20</label>
            <mixed-citation id="d1380e579" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d1380e586a1310">
            <label>21</label>
            <mixed-citation id="d1380e593" publication-type="other">
Jason Brownlee, Authoritarianism in an Age of Democratization (Cambridge, UK: Cambridge
University Press, 2007), 39.</mixed-citation>
         </ref>
         <ref id="d1380e603a1310">
            <label>22</label>
            <mixed-citation id="d1380e610" publication-type="other">
Geddes, "Parties and Elections"; Samer Shehata, "Inside an Egyptian Parliamentary Campaign,"
in Political Participation in the Middle East, ed. Ellen Lust-Okar and Saloua Zerhouni (Boulder: Lynne
Rienner, 2008), 95-120.</mixed-citation>
         </ref>
         <ref id="d1380e624a1310">
            <label>23</label>
            <mixed-citation id="d1380e631" publication-type="other">
Brownlee, Authoritarianism in an Age of Democratization, 60.</mixed-citation>
         </ref>
         <ref id="d1380e638a1310">
            <label>24</label>
            <mixed-citation id="d1380e645" publication-type="other">
Jennifer Gandhi and Ellen Lust-Okar, "Elections under Authoritarianism," Annual Review of
Political Science 12 (June 2009): 410.</mixed-citation>
         </ref>
         <ref id="d1380e655a1310">
            <label>25</label>
            <mixed-citation id="d1380e662" publication-type="other">
Beatriz Magaloni, "The Comparative Logic of Autocratic Survival" (paper, Hoover Institution,
Stanford, CA: 2007), 8.</mixed-citation>
         </ref>
         <ref id="d1380e672a1310">
            <label>26</label>
            <mixed-citation id="d1380e679" publication-type="other">
Magaloni, "Credible Power-Sharing."</mixed-citation>
         </ref>
         <ref id="d1380e686a1310">
            <label>27</label>
            <mixed-citation id="d1380e693" publication-type="other">
Geddes, Paradigms and Sand Castles.</mixed-citation>
         </ref>
         <ref id="d1380e700a1310">
            <label>28</label>
            <mixed-citation id="d1380e707" publication-type="other">
Dennis Charles Galvan, "Political Turnover and Social Change in Senegal," Journal of Democracy
12, no. 3 (July 2001): 54.</mixed-citation>
         </ref>
         <ref id="d1380e718a1310">
            <label>29</label>
            <mixed-citation id="d1380e725" publication-type="other">
Geddes, "Parties and Elections," 4.</mixed-citation>
         </ref>
         <ref id="d1380e732a1310">
            <label>30</label>
            <mixed-citation id="d1380e739" publication-type="other">
Ibid., 12.</mixed-citation>
         </ref>
         <ref id="d1380e746a1310">
            <label>31</label>
            <mixed-citation id="d1380e753" publication-type="other">
Ibid., 4.</mixed-citation>
         </ref>
         <ref id="d1380e760a1310">
            <label>32</label>
            <mixed-citation id="d1380e767" publication-type="other">
Jennifer Gandhi and Adam Przeworski, "Authoritarian Institutions and the Survival of Autocrats,"
Comparative Political Studies 40, no. 11 (November 2007): 1292.</mixed-citation>
         </ref>
         <ref id="d1380e777a1310">
            <label>33</label>
            <mixed-citation id="d1380e784" publication-type="other">
Geddes, "Parties and Elections," 4.</mixed-citation>
         </ref>
         <ref id="d1380e791a1310">
            <label>34</label>
            <mixed-citation id="d1380e798" publication-type="other">
Gandhi and Przeworski, "Cooperation, Cooptation and Rebellion."</mixed-citation>
         </ref>
         <ref id="d1380e806a1310">
            <label>35</label>
            <mixed-citation id="d1380e813" publication-type="other">
Gandhi and Przeworski, "Authoritarian Institutions," 1280.</mixed-citation>
         </ref>
         <ref id="d1380e820a1310">
            <label>36</label>
            <mixed-citation id="d1380e827" publication-type="other">
Ibid., 1282.</mixed-citation>
         </ref>
         <ref id="d1380e834a1310">
            <label>37</label>
            <mixed-citation id="d1380e841" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d1380e848a1310">
            <label>38</label>
            <mixed-citation id="d1380e855" publication-type="other">
Michele Penner Angrist, "The Expression of Political Dissent in the Middle East," Comparative
Studies in Society and History 41 (1999): 744-53.</mixed-citation>
         </ref>
         <ref id="d1380e865a1310">
            <label>39</label>
            <mixed-citation id="d1380e872" publication-type="other">
Andrew Nathan, "Authoritarian Resilience," Journal of Democracy 14, no. 1 (January 2003): 7.</mixed-citation>
         </ref>
         <ref id="d1380e879a1310">
            <label>40</label>
            <mixed-citation id="d1380e886" publication-type="other">
We measure leadership tenures using the Archigos data set. Hein E. Goemans, ICristian Skrede
Gleditsch and Giacomo Chiozza, "Introducing Archigos: A Data Set of Political Leaders," Journal of
Peace Research 46, no. 2 (2009): 269-83.</mixed-citation>
         </ref>
         <ref id="d1380e900a1310">
            <label>41</label>
            <mixed-citation id="d1380e907" publication-type="other">
Even unconstitutional transfers of power are typically driven by elites within the regime. Ezrow
and Frantz, The Politics of Dictatorships; Milan Svolik, "Power-sharing and Leadership Dynamics in
Authoritarian Regimes," American Journal of Political Science 53, no. 2 (2009): 477-94.</mixed-citation>
         </ref>
         <ref id="d1380e920a1310">
            <label>42</label>
            <mixed-citation id="d1380e927" publication-type="other">
The Archigos data set refers to constitutional transfers of power as leadership failures via "regular
means." It refers to unconstitutional transfers of power as leadership failures via "irregular means."</mixed-citation>
         </ref>
         <ref id="d1380e937a1310">
            <label>43</label>
            <mixed-citation id="d1380e944" publication-type="other">
Magaloni, "Credible Power-Sharing," 726.</mixed-citation>
         </ref>
         <ref id="d1380e951a1310">
            <label>44</label>
            <mixed-citation id="d1380e958" publication-type="other">
Geddes, "Democratization after Twenty Years," 139; Benjamin Smith, "Life of the Party: The
Origins of Regime Breakdown and Persistence under Single-Party Rule," World Politics 57, no. 3 (April
2005): 428; Magaloni, "The Comparative Logic of Autocratic Survival," 19.</mixed-citation>
         </ref>
         <ref id="d1380e971a1310">
            <label>45</label>
            <mixed-citation id="d1380e978" publication-type="other">
Smith, "Origins of Regime Breakdown," 428.</mixed-citation>
         </ref>
         <ref id="d1380e985a1310">
            <label>46</label>
            <mixed-citation id="d1380e992" publication-type="other">
For a discussion of the ways in which institutionalized rules for succession make it easier for
regimes to handle leadership transitions, see Samuel Huntington, Political Order in Changing Societies
(New Haven: Yale University Press, 1968).</mixed-citation>
         </ref>
         <ref id="d1380e1006a1310">
            <label>47</label>
            <mixed-citation id="d1380e1013" publication-type="other">
Nathan, "Authoritarian Resilience," 8-9.</mixed-citation>
         </ref>
         <ref id="d1380e1020a1310">
            <label>48</label>
            <mixed-citation id="d1380e1027" publication-type="other">
Ibid., 7.</mixed-citation>
         </ref>
         <ref id="d1380e1034a1310">
            <label>49</label>
            <mixed-citation id="d1380e1041" publication-type="other">
Lowell Dittmer, "Leadership Change and Chinese Political Development," China Quarterly 176
(December 2003): 913.</mixed-citation>
         </ref>
         <ref id="d1380e1051a1310">
            <label>50</label>
            <mixed-citation id="d1380e1058" publication-type="other">
Smith, "Origins of Regime Breakdown," 448.</mixed-citation>
         </ref>
         <ref id="d1380e1065a1310">
            <label>51</label>
            <mixed-citation id="d1380e1072" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d1380e1079a1310">
            <label>52</label>
            <mixed-citation id="d1380e1086" publication-type="other">
Hussin Mutalib, "Illiberal Democracy and the Future of Opposition in Singapore," Third World
Quality 21, no. 2 (2000): 313-42.</mixed-citation>
         </ref>
         <ref id="d1380e1097a1310">
            <label>53</label>
            <mixed-citation id="d1380e1104" publication-type="other">
Patrick McGowan, "African Military Coups d'État, 1956-2001: Frequency, Trends and
Distribution," Journal of Modern African Studies 41, no. 3 (2003): 339-70.</mixed-citation>
         </ref>
         <ref id="d1380e1114a1310">
            <label>54</label>
            <mixed-citation id="d1380e1121" publication-type="other">
Dieter Nohlen, Bernhard Thibault and Michael Krennerich, eds., Elections in Africa: A Data
Handbook (Oxford: Oxford University Press, 1999), 79-102.</mixed-citation>
         </ref>
         <ref id="d1380e1131a1310">
            <label>55</label>
            <mixed-citation id="d1380e1138" publication-type="other">
Parties were briefly allowed to exist in Ghana from 1981 to 1982. Nohlen et al., Elections in Africa,
423-46.</mixed-citation>
         </ref>
         <ref id="d1380e1148a1310">
            <label>56</label>
            <mixed-citation id="d1380e1155" publication-type="other">
McGowan, "African Military Coups d'État."</mixed-citation>
         </ref>
         <ref id="d1380e1162a1310">
            <label>57</label>
            <mixed-citation id="d1380e1169" publication-type="other">
Maxwell Owusu, "Rebellion, Revolution, and Tradition: Reinterpreting Coups in Ghana,"
Comparative Studies in Society and History 31, no. 2 (1989): 393.</mixed-citation>
         </ref>
         <ref id="d1380e1179a1310">
            <label>58</label>
            <mixed-citation id="d1380e1186" publication-type="other">
Nohlen et al., Elections in Africa, 79-102.</mixed-citation>
         </ref>
         <ref id="d1380e1194a1310">
            <label>59</label>
            <mixed-citation id="d1380e1201" publication-type="other">
Ibid., 423-44.</mixed-citation>
         </ref>
         <ref id="d1380e1208a1310">
            <label>60</label>
            <mixed-citation id="d1380e1215" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d1380e1222a1310">
            <label>61</label>
            <mixed-citation id="d1380e1229" publication-type="other">
McGowan, "African Military Coups d'État."</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
