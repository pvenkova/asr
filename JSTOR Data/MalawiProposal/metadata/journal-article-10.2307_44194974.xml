<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amercathstud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015939</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Catholic Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Catholic Historical Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">21618542</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">21618534</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44194974</article-id>
         <title-group>
            <article-title>Two Sides of the American Catholic Mission Coin: Mission Funding and Credit Unions</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Angelyn</given-names>
                  <surname>Dries</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">117</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40175297</issue-id>
         <fpage>23</fpage>
         <lpage>42</lpage>
         <permissions>
            <copyright-statement>© 2006 American Catholic Historical Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44194974"/>
         <abstract>
            <p>Jonathan Bonk has suggested that the wealth imbalance between missionaries and those to whom they are sent distorts the reception of the gospel message. This article examines that premise among twentieth-century American Catholic missionaries by identifying two sides of the mission coin: people representing three types of mission funding and missionary encouragement of credit unions. Underlying the examples is the thesis that they are a system of interconnecting relationships of gift exchanges expressed in and impacting social, cultural, and religious, as well as economic realms. The monetary is closely associated with the economy of salvation. Indigenous people critiqued not so much the personal affluence of missionaries as the composite cultural and economic world from which missionaries came. As missionaries critiqued the American global empire and chose alternative economic lifestyles, they lost some American Catholic financial support but affected the theological direction of the "economy of salvation" paradigm. Prior to 1970, giving to the missions was viewed as charity, and therefore, supererogatory. After 1970, as a result of addressing economic issues of injustice, mission included the social/structural dimensions at the heart of Catholic spirituality and life, and was, therefore, essential.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1590e150a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1590e157" publication-type="other">
Jonathan Bonk, Missions and Money: Affluence as a Western Missionary Problem
(Maryknoll, NY: Orbis Books, 1991), quotation from ix.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e166" publication-type="other">
Jan A. B. Jongeneel,

              Philosophy, Science, and Theology of Mission in the 19
              
              and 20
              
              Centuries. A
            
Missiological Encyclopedia. Part I (New York: Peter Lang, 1995), 320-333.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e185a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1590e192" publication-type="other">
David Raterman, The Latin American Apoštoláte (Unpublished Report, St. Louis
Archdiocesan Archives, 1956), 41.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e201" publication-type="other">
Stuart C. Bate, O.M.I., "Foreign Funding of Catholic Missions
in South Africa: A Case Study," Mission Studies XVIII-2 (2001): 50-86.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e211a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1590e218" publication-type="other">
Mary J. Oates,
The Catholic Philanthropic Tradition in America (Bloomington, IN: Indiana
University Press, 1995),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e230" publication-type="other">
Edward Hickey, The Society
for the Propagation of the Faith: Its Foundation, Organization, and Success (1822-
1922) (Washington, DC: The Catholic University of America Press, 1942; New York:
AMS, 1974);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e245" publication-type="other">
Francis Clement Kelley, The Story of Extension (Chicago: Extension
Press, 1922).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e255" publication-type="other">
Source: http://www.worldmissions-catholicchurch.org/spof/DesktopDefaul.aspx
Retrieved May 21, 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e265a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1590e272" publication-type="other">
James Gaffey, Francis Clement Kelley and the American Catholic Dream
(Bensenville, IL: The Heritage Foundation, 1980).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e283a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1590e290" publication-type="other">
The Mission Movement in America, Being the Mind of the Missionaries Assembled
in the Third Washington Conference at the Apostolic Mission House (Washington, DC:
Apostolic Mission House, 1906), 58.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e303a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1590e310" publication-type="other">
Kelley, The Story of Extension, 78.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e317a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1590e324" publication-type="other">
Fulton J. Sheen, Missions and the
World Crisis (Milwaukee, WI: Bruce Publishing Company, 1964).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e333" publication-type="other">
Kathleen L. Riley, Fulton J.
Sheen: An American Catholic Response to the Twentieth Century (Staten Island, NY:
Alba House, 2004), 233-268.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e346a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1590e353" publication-type="other">
M.C. Devine, The
World's Cardinal (Boston: St. Paul Editions, 1964), 36-37,</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e363a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1590e370" publication-type="other">
San Ricardo Social Service Center, Lima, Peru.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e377a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1590e386" publication-type="other">
Anne Butler, "Mother Katharine
Drexel: Spiritual Visionary for the West," in Glenda Riles and Richard Etulain, eds.,
By Grit and Grace. Eleven Women Who Shaped the American West (Golden, CO:
Fulcrum Press, 1997), 198-220.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e403a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1590e410" publication-type="other">
Francis Clement Kelley, ed., The Second American Catholic Missionary
Congress (Chicago: J.S. Hyland, 1913), 79.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e420a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1590e427" publication-type="other">
Oates, 28-29.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e434a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1590e441" publication-type="other">
Richard F. Flynn, "The Parish and the Missions," in Francis C. Kelley, ed., The
First American Catholic Missionary Congress, 203-209, here, 209.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e451a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1590e458" publication-type="other">
Anna Dengel, July 30, 1954.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e465a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1590e472" publication-type="other">
Oates, 28-
29.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e482a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1590e489" publication-type="other">
Script for lantern slide 64, China files, 1937.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e497a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1590e504" publication-type="other">
Raymond Corrigan, "Mission Aid Societies," Thought 10 (1935): 286-297, here,
293.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e514a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1590e521" publication-type="other">
Charles Decker, Saving the Solomons. From the Diary
Accounts of Rev. Mother Mary Rose, S.M.S.M. (Boston, MA: Society for the
Propagation of the Faith, 1942).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e534a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1590e541" publication-type="other">
Corrigan, "Mission Aid Societies," 292.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e548a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1590e555" publication-type="other">
Ibid., 297.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e562a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1590e569" publication-type="other">
Henri
Hubert and Marcel Mauss, Sacrifice: Its Nature and Function. Translated by W.D.
Halls (Chicago: University of Chicago Press, 1964)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e581" publication-type="other">
Marcel Mauss, The Gift. Forms
and Functions of Exchange in Archaic Societies (New York: W.W. Norton Company,
Inc., 1967).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e594a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1590e601" publication-type="other">
William A. Ryan, "Report on Belize,"
Jesuit Bulletin 27 (February 1948): 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e612a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1590e619" publication-type="other">
Malawi, in Visage Nouveau de la femme
Missionnaire (Louvain: Desclee de Brouwer, 1973), 91.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e628" publication-type="other">
Niall O'Brien, Revolution from the
Heart (New York: Oxford University Press, 1987).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e638a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1590e645" publication-type="other">
George Pearce, SM to Provincial Fisher, January 20, 1958.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e652a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1590e659" publication-type="other">
Mulherin served in Korea from 1926-1942 and from 1951-1967.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e665" publication-type="other">
Penny Lernoux, Hearts on Fire: The Story of the Maryknoll Sisters (Maryknoll,
NY: Orbis Books, 1993), 195, 197-201, 290.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e675a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1590e682" publication-type="other">
Ibid., 201.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e689a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1590e696" publication-type="other">
Marion Ganey to Very Reverend Father Fisher, S.J., "What Has Been Done in
Four Years – Nov 1953 to December 1957."</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e706a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1590e713" publication-type="other">
M[oses] M. Coady, Masters of Their Own Destiny: The Story of the Antigonish
Movement of Adult Education Through Economic Cooperation (New York: Harper,
1939)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e725" publication-type="other">
Moses M. Coady, The Social Significance of the Cooperative Movement
(Antigonish, Nova Scotia: St. Francis Xavier University, 1958).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e736a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1590e743" publication-type="other">
Marion
Ganey to "Dear Hugh," March 27, 1963.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e753a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1590e760" publication-type="other">
John Considine, M.M., ed., The Missionary's Role in Socio-Economic Betterment
(New York: Newman Press, 1960).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e770a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1590e777" publication-type="other">
Topshee quoted in Considine, ed., 203.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e784a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1590e791" publication-type="other">
Joseph P. Gremillion, "World Misery and Justice: A Missionary Concern," in
Considine, ed., 1-17, here 16.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e800" publication-type="other">
Shreveport, Louisiana (1949-1958)</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e807a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1590e814" publication-type="other">
Ganey, quoted in Gerald Arbuckle, SM, "Self-Development Through Credit
Unions," paper delivered at an unnamed conference, 1964.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e824a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1590e831" publication-type="other">
Edward L. Murphy, Teach Ye All Nations (Chicago, IL: Benziger Brothers, Inc.,
1957), 222-223.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e842a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1590e849" publication-type="other">
Quoted in Arbuckle, S.M., "Self-Development Through Credit Unions," 8-9.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e856a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1590e863" publication-type="other">
Mary Douglas and
Baron Isherwood, The World of Goods (New York, NY: Basic Books, Inc., 1979),
especially 25-43.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e876a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1590e883" publication-type="other">
Frederick William Danker, A Greek-English Lexicon of the New

              Testament and other Early Christian Literature, 3
              
              ed. (Chicago, IL: University of
            
Chicago Press, 2000), 697-698.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e899a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1590e906" publication-type="other">
J. Patout Burns, S.J., "The Economy of Salvation: Two Patristic Traditions,"
Theological Studies 37 (1976): 598-619.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e916a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1590e923" publication-type="other">
Reverend John Walsh, "Roman Catholics and Civilization," The
American Catholic Quarterly Review 10 (1885): 193-217;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e932" publication-type="other">
James J. Walsh, "Missionary
Work and Civilization," in Francis Clement Kelley, Second American Catholic
Missionary Congress, 265-270;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e944" publication-type="other">
Henry Woods, S.J., "What is Civilization?" The
Catholic Mind 13 (1915): 101-105;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e954" publication-type="other">
John E. Wickham, "Catholicism and Culture," The
Catholic Mind 20 (1922): 294-300;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1590e963" publication-type="other">
Robert Streit, "Missions and Civilization," The
Mission Apostolate (New York: National Office of the Society for the Propagation of
the Faith, 1942), 25-30.</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e976a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1590e985" publication-type="other">
William J. Richardson, M.M., ed., The Poor Church (Maryknoll, NY: Maryknoll
Publications, 1967).</mixed-citation>
            </p>
         </fn>
         <fn id="d1590e996a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1590e1003" publication-type="other">
Sister Mary Gabriella Mulherin, Director-Cooperative Education Institute, "A
Missioner Looks at Korean Culture." (November 2, 1965. typescript) H3.4 Box 3/11.
Maryknoll Mission Archives, Maryknoll, NY.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
