<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">souteconj</journal-id>
         <journal-id journal-id-type="jstor">j100373</journal-id>
         <journal-title-group>
            <journal-title>Southern Economic Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Southern Economic Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">00384038</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/1060835</article-id>
         <title-group>
            <article-title>International Trade and the Accumulation of Human Capital</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Ann L.</given-names>
                  <surname>Owen</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">66</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i243615</issue-id>
         <fpage>61</fpage>
         <lpage>81</lpage>
         <page-range>61-81</page-range>
         <permissions>
            <copyright-statement>Copyright 1999 Southern Economic Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1060835"/>
         <abstract>
            <p>Changes in the terms of trade affect both the incentives and the ability of individuals to purchase education in a credit-constrained economy. A model is developed that shows how individual decision-making is affected in a small economy when it opens up to trade. Empirical results indicate that credit constraints are an important factor influencing school enrollment rates, particularly in low income countries. As a result, countries with low human capital stocks tend to increase their accumulation of human capital with increased trade. The response in high income countries is more muted.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1191e120a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1191e127" publication-type="journal">
Barro and Lee 1993  </mixed-citation>
            </p>
         </fn>
         <fn id="d1191e136a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1191e143" publication-type="journal">
Galor and Zeira (1993)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1191e151" publication-type="journal">
Perotti (1994)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1191e160a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1191e167" publication-type="journal">
Burtless (1995)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1191e176a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1191e183" publication-type="journal">
Galor and Zeira (1993)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1191e191" publication-type="journal">
Fernandez and Rogerson (1995) and lyigun (in press)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1191e201a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1191e208" publication-type="journal">
Owen and Weil (1998)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1191e217a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1191e224" publication-type="journal">
Kiviet (1995)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1191e232" publication-type="book">
Judson and Owen (1997)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1191e241a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1191e248" publication-type="other">
Deininger and Squire (1996)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1191e264a1310">
            <mixed-citation id="d1191e268" publication-type="journal">
Anderson, T. W., and Cheng Hsiao. 1982. Formulation and estimation of dynamic models using panel data. Journal of
Econometrics 18:47-82.<person-group>
                  <string-name>
                     <surname>Anderson</surname>
                  </string-name>
               </person-group>
               <fpage>47</fpage>
               <volume>18</volume>
               <source>Journal of Econometrics</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e300a1310">
            <mixed-citation id="d1191e304" publication-type="journal">
Barro, Robert J., and Jong-Wha Lee. 1993. International comparisons of educational attainment. Journal of Monetary
Economics 32:363-94.<person-group>
                  <string-name>
                     <surname>Barro</surname>
                  </string-name>
               </person-group>
               <fpage>363</fpage>
               <volume>32</volume>
               <source>Journal of Monetary Economics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e336a1310">
            <mixed-citation id="d1191e340" publication-type="book">
Borjas, George J., and Valerie A. Ramey. 1994. The relationship between wage inequality and international trade. In The
changing distribution of income in an open U.S. economy, edited by Jeffrey H. Bergstrand and Thomas E
Cosimano. Amsterdam: North Holland, pp. 217-42.<person-group>
                  <string-name>
                     <surname>Borjas</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The relationship between wage inequality and international trade</comment>
               <fpage>217</fpage>
               <source>The changing distribution of income in an open U.S. economy</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e375a1310">
            <mixed-citation id="d1191e379" publication-type="journal">
Burtless, Gary. 1995. International trade and the rise in earnings inequality. Journal of Economic Literature 33:800-16.<object-id pub-id-type="jstor">10.2307/2729029</object-id>
               <fpage>800</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e393a1310">
            <mixed-citation id="d1191e397" publication-type="book">
Davis, Donald R., and Trevor Reeve. 1997. Human capital, unemployment, and relative wages in a global economy.
NBER Working Paper No. 6133.<person-group>
                  <string-name>
                     <surname>Davis</surname>
                  </string-name>
               </person-group>
               <source>Human capital, unemployment, and relative wages in a global economy</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e422a1310">
            <mixed-citation id="d1191e426" publication-type="other">
Deininger, Klaus, and Lyn Squire. 1996. Measuring income inequality: A new data base. Unpublished paper, World Bank.</mixed-citation>
         </ref>
         <ref id="d1191e433a1310">
            <mixed-citation id="d1191e437" publication-type="book">
Feenstra, Robert C., and Gordon H. Hanson. 1995. Foreign investment, outsourcing, and relative wages. NBER Working
Paper No. 5121.<person-group>
                  <string-name>
                     <surname>Feenstra</surname>
                  </string-name>
               </person-group>
               <source>Foreign investment, outsourcing, and relative wages</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e462a1310">
            <mixed-citation id="d1191e466" publication-type="journal">
Fernandez, Raquel, and Richard Rogerson. 1995. On the political economy of education subsidies. Review of Economic
Studies 62:249-62.<object-id pub-id-type="doi">10.2307/2297804</object-id>
               <fpage>249</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e482a1310">
            <mixed-citation id="d1191e486" publication-type="journal">
Findlay, Ronald, and Henryk Kierzkowski. 1983. International trade and human capital: A simple general equilibrium
model. Journal of Political Economy 91:957-78.<object-id pub-id-type="jstor">10.2307/1831199</object-id>
               <fpage>957</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e502a1310">
            <mixed-citation id="d1191e506" publication-type="journal">
Galor, Oded. 1992. A two sector overlapping generations model: A global characterization of the dynamical system.
Econometrica 60:1351-86.<person-group>
                  <string-name>
                     <surname>Galor</surname>
                  </string-name>
               </person-group>
               <fpage>1351</fpage>
               <volume>60</volume>
               <source>Econometrica</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e539a1310">
            <mixed-citation id="d1191e543" publication-type="journal">
Galor, Oded, and Joseph Zeira. 1993. Income distribution and macroeconomics. Review of Economic Studies 60:35-52.<object-id pub-id-type="doi">10.2307/2297811</object-id>
               <fpage>35</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e556a1310">
            <mixed-citation id="d1191e560" publication-type="journal">
Grossman, Gene M., and Elhanan Helpman. 1990. Trade, innovation, and growth. American Economic Review 80:86-91.<object-id pub-id-type="jstor">10.2307/2006548</object-id>
               <fpage>86</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e573a1310">
            <mixed-citation id="d1191e577" publication-type="journal">
Grossman, Gene M., and Elhanan Helpman. 1991. Endogenous product cycles. Economic Journal 101:1214-29.<object-id pub-id-type="doi">10.2307/2234437</object-id>
               <fpage>1214</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e590a1310">
            <mixed-citation id="d1191e594" publication-type="journal">
Lyigun, Murat E 1999. Public education and intergenerational economic mobility. International Economic Review. In
press.<person-group>
                  <string-name>
                     <surname>Lyigun</surname>
                  </string-name>
               </person-group>
               <source>International Economic Review</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e619a1310">
            <mixed-citation id="d1191e623" publication-type="book">
Judson, Ruth A., and Ann L. Owen. 1997. Estimating dynamic panel data models: A practical guide for macroeconomists.
Washington, DC: Federal Reserve Board. Finance and Economics Discussions Series Working Paper 1997-3.<person-group>
                  <string-name>
                     <surname>Judson</surname>
                  </string-name>
               </person-group>
               <source>Estimating dynamic panel data models: A practical guide for macroeconomists</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e648a1310">
            <mixed-citation id="d1191e652" publication-type="journal">
Kiviet, Jan E 1995. On bias, inconsistency, and efficiency of various estimators in dynamic panel data models. Journal
of Econometrics 68:53-78.<person-group>
                  <string-name>
                     <surname>Kiviet</surname>
                  </string-name>
               </person-group>
               <fpage>53</fpage>
               <volume>68</volume>
               <source>Journal of Econometrics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e685a1310">
            <mixed-citation id="d1191e689" publication-type="book">
Leamer, Edward E. 1996. In search of Stolper-Samuelson effects on U.S. wages. NBER Working Paper No. 5427.<person-group>
                  <string-name>
                     <surname>Leamer</surname>
                  </string-name>
               </person-group>
               <source>search of Stolper-Samuelson effects on U.S. wages</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e711a1310">
            <mixed-citation id="d1191e715" publication-type="journal">
Leontief, Wassily. 1956. Factor proportions and the structure of American trade: Further theoretical and empirical analysis.
Review of Economics and Statistics 38:386-407.<object-id pub-id-type="doi">10.2307/1926500</object-id>
               <fpage>386</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e731a1310">
            <mixed-citation id="d1191e735" publication-type="journal">
Levine, Ross, and David Renelt. 1992. A sensitivity analysis of cross-country growth regressions. American Economic
Review 82:942-63.<object-id pub-id-type="jstor">10.2307/2117352</object-id>
               <fpage>942</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1191e751a1310">
            <mixed-citation id="d1191e755" publication-type="book">
Murphy, Kevin M., and Finis Welch. 1991. The role of international trade in wage differentials. In Workers and their
wages: Changing patterns in the United States, edited by Marvin H. Kosters. Washington, DC: AEI Press, pp.
39-69.<person-group>
                  <string-name>
                     <surname>Murphy</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The role of international trade in wage differentials</comment>
               <fpage>39</fpage>
               <source>Workers and their wages: Changing patterns in the United States</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e790a1310">
            <mixed-citation id="d1191e794" publication-type="journal">
Nickell, S. 1981. Biases in dynamic models with fixed effects. Econometrica 49:1417-26.<person-group>
                  <string-name>
                     <surname>Nickell</surname>
                  </string-name>
               </person-group>
               <fpage>1417</fpage>
               <volume>49</volume>
               <source>Econometrica</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e823a1310">
            <mixed-citation id="d1191e827" publication-type="journal">
Owen, Ann L., and David N. Weil. 1998. Intergenerational earnings mobility, inequality, and growth. Journal of Monetary
Economics 41:71-104.<person-group>
                  <string-name>
                     <surname>Owen</surname>
                  </string-name>
               </person-group>
               <fpage>71</fpage>
               <volume>41</volume>
               <source>Journal of Monetary Economics</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e860a1310">
            <mixed-citation id="d1191e864" publication-type="journal">
Perotti, Roberto. 1994. Income distribution and investment. European Economic Review 38:827-35.<person-group>
                  <string-name>
                     <surname>Perotti</surname>
                  </string-name>
               </person-group>
               <fpage>827</fpage>
               <volume>38</volume>
               <source>European Economic Review</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e893a1310">
            <mixed-citation id="d1191e897" publication-type="journal">
Stokey, Nancy L. 1996. Free trade, factor returns, and factor accumulation. Journal of Economic Growth 1:421-47.<person-group>
                  <string-name>
                     <surname>Stokey</surname>
                  </string-name>
               </person-group>
               <fpage>421</fpage>
               <volume>1</volume>
               <source>Journal of Economic Growth</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e926a1310">
            <mixed-citation id="d1191e930" publication-type="book">
UNESCO. 1989. Statistical yearbook, 1960-1989.<person-group>
                  <string-name>
                     <surname>UNESCO</surname>
                  </string-name>
               </person-group>
               <source>Statistical yearbook</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1191e952a1310">
            <mixed-citation id="d1191e956" publication-type="book">
World Bank. 1993. World Development Report 1993, New York: Oxford University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report 1993</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
