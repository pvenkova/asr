<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">bmjbritmedj</journal-id>
         <journal-id journal-id-type="jstor">j50000213</journal-id>
         <journal-title-group>
            <journal-title>BMJ: British Medical Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>British Medical Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">09598138</issn>
         <issn pub-type="epub">17561833</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25469479</article-id>
         <article-id pub-id-type="pub-doi">10.1136/Bmj.38251.658229.55</article-id>
         <title-group>
            <article-title>Overdiagnosis Of Malaria In Patients With Severe Febrile Illness In Tanzania: A Prospective Study</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Hugh</given-names>
                  <surname>Reyburn</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Redempta</given-names>
                  <surname>Mbatia</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Chris</given-names>
                  <surname>Drakeley</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Ilona</given-names>
                  <surname>Carneiro</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Emmanuel</given-names>
                  <surname>Mwakasungula</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Ombeni</given-names>
                  <surname>Mwerinde</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Kapalala</given-names>
                  <surname>Saganda</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>John</given-names>
                  <surname>Shao</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Andrew</given-names>
                  <surname>Kitua</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Raimos</given-names>
                  <surname>Olomi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Brian M.</given-names>
                  <surname>Greenwood</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Christopher J. M.</given-names>
                  <surname>Whitty</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>20</day>
            <month>11</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">329</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7476</issue>
         <issue-id>i25469455</issue-id>
         <fpage>1212</fpage>
         <lpage>1215</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 BMJ Publishing Group Ltd</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25469479"/>
         <abstract>
            <p>Objective To study the diagnosis and outcomes in people admitted to hospital with a diagnosis of severe malaria in areas with differing intensities of malaria transmission. Design Prospective observational study of children and adults over the course a year. Setting 10 hospitals in north east Tanzania. Participants 17 313 patients were admitted to hospital; of these 4474 (2851 children aged under 5 years) fulfilled criteria for severe disease. Main outcome measure Details of the treatment given and outcome. Altitudes of residence (a proxy for transmission intensity) measured with a global positioning system. Results Blood film microscopy showed that 2062 (46.1%) of people treated for malaria had Plasmodium falciparum (slide positive). The proportion of slide positive cases fell with increasing age and increasing altitude of residence. Among 1086 patients aged ≥5 years who lived above 600 metres only 338 (31.1%) were slide positive, while in children &lt;5 years living in areas of intense transmission (&lt;600 metres) most (958/1392, 68.8%) were slide positive. Among 2375 people who were slide negative, 1571 (66.1%) were not treated with antibiotics and, of those, 120 (7.6%) died. The case fatality in slide negative patients was higher (292/2412, 12.1%) than for slide positive patients (142/2062, 6.9%) (P&lt;0.001). Respiratory distress and altered consciousness were the strongest predictors of mortality in slide positive and slide negative patients and in adults as well as children. Conclusions In Tanzania, malaria is commonly overdiagnosed in people presenting with severe febrile illness, especially in those living in areas with low to moderate transmission and in adults. This is associated with a failure to treat alternative causes of severe infection. Diagnosis needs to be improved and syndromic treatment considered. Routine hospital data may overestimate mortality from malaria by over twofold.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Endnotes]</title>
         <ref id="d1506e262a1310">
            <label>1</label>
            <mixed-citation id="d1506e269" publication-type="other">
Ministry of Health. Health statistics abstract 2002. Burden of disease arid
health utilization statistics. Vol 1. Dar Es Salaam, Tanzania: Ministry of
Health, 2002.</mixed-citation>
         </ref>
         <ref id="d1506e282a1310">
            <label>2</label>
            <mixed-citation id="d1506e289" publication-type="other">
World Health Organization. The Africa malaria report. Geneva: WHO,
2003:1093. (WHO/CDS/MAL/2003.)</mixed-citation>
         </ref>
         <ref id="d1506e299a1310">
            <label>3</label>
            <mixed-citation id="d1506e306" publication-type="other">
Makani J, Matuja W, Liyombo E, Snow RW, Marsh K, Warrell DA. Admis-
sion diagnosis of cerebral malaria in adults in an endemic area of Tanza-
nia: implications and clinical description. QJM 2003;96:355-62.</mixed-citation>
         </ref>
         <ref id="d1506e319a1310">
            <label>4</label>
            <mixed-citation id="d1506e326" publication-type="other">
English M, Esamai E, Wasunna A, Were F, Ogutu B, Wamae A, et al.
Assessment of inpatient paediatric care in first referral level hospitals in
13 districts in Kenya. Uncet 2004;363:1948-53.</mixed-citation>
         </ref>
         <ref id="d1506e340a1310">
            <label>5</label>
            <mixed-citation id="d1506e347" publication-type="other">
Nolan T, Angos P, Cunha AJ, Muhe L, Qazi S, Simoes EA, et al. Quality of
hospital care for seriously ill children in less-developed countries. Lancet
2001;357:106-10.</mixed-citation>
         </ref>
         <ref id="d1506e360a1310">
            <label>6</label>
            <mixed-citation id="d1506e367" publication-type="other">
Jonkman A, Chibwe RA, Khoromana CO, Liabunya UL, Chapanda ME,
Kandiero GE, et al. Cost-saving through microscopy-based versus
presumptive diagnosis of malaria in adult outpatients in Malawi. Bull
Wrrld Health Organ 1995;73:223-7.</mixed-citation>
         </ref>
         <ref id="d1506e383a1310">
            <label>7</label>
            <mixed-citation id="d1506e390" publication-type="other">
Snow RW, Craig M, Deichmann U, Marsh K. Estimating mortality,
morbidity and disability due to malaria among Africa's non-pregnant
population. Bull Wrrld Health Organ 1999;77:624-40.</mixed-citation>
         </ref>
         <ref id="d1506e403a1310">
            <label>8</label>
            <mixed-citation id="d1506e410" publication-type="other">
Bodker R, Akida J, Shayo D, Kisinza W, Msangeni HA, Pedersen EM, et al.
Relationship between altitude and intensity of malaria transmission in
the Usambara Mountains, Tanzania./MedKntomol 2003;40:706-17.</mixed-citation>
         </ref>
         <ref id="d1506e423a1310">
            <label>9</label>
            <mixed-citation id="d1506e430" publication-type="other">
WHO. Severe falciparum malaria. Trans R Soc Trop Med Hyg 2000;94:1 -2.</mixed-citation>
         </ref>
         <ref id="d1506e437a1310">
            <label>10</label>
            <mixed-citation id="d1506e444" publication-type="other">
Molyneux ME, Taylor TE, Wirima JJ, Borgstein A. Clinical features and
prognostic indicators in paediatric cerebral malaria: a study of 131 coma-
tose Malawian children. QJ Med 1989;71:441-59.</mixed-citation>
         </ref>
         <ref id="d1506e458a1310">
            <label>11</label>
            <mixed-citation id="d1506e465" publication-type="other">
WHO. Inegrated management of childhood illness, www.who.int/child-
adolescent-health/publications/IMCI/WHO_FCH_CAH_00.40.htm
(accessed 7 Oct 2004).</mixed-citation>
         </ref>
         <ref id="d1506e478a1310">
            <label>12</label>
            <mixed-citation id="d1506e485" publication-type="other">
BryceJ, el Arifeen S, Pariyo G, Lanata C, Gwatkin D, HabichtJP. Reduc-
ing child mortality: can public health deliver? Lancet 2003;362:159-64.</mixed-citation>
         </ref>
         <ref id="d1506e495a1310">
            <label>13</label>
            <mixed-citation id="d1506e502" publication-type="other">
World Health Organization. Africa malaria report 2003. Geneva: WHO,
2003. (WHO/CDS/MAL/2003.1093 ed.)</mixed-citation>
         </ref>
         <ref id="d1506e512a1310">
            <label>14</label>
            <mixed-citation id="d1506e519" publication-type="other">
Bodker R. Variations in malaria risk in the Usambara Mountains, Tanzania.
Charlottenlund, Denmark: Danish Bilharzia Laboratory, 2000:56-84.</mixed-citation>
         </ref>
         <ref id="d1506e529a1310">
            <label>15</label>
            <mixed-citation id="d1506e536" publication-type="other">
Planche T, Agbenyega T, Bedu-Addo G, Ansong D, Owusu-Ofori A,
Micah F, et al. A prospective comparison of malaria with other severe dis-
eases in African children: prognosis and optimization of management.
Clin Infect Dis 2003;37:890-7.</mixed-citation>
         </ref>
         <ref id="d1506e552a1310">
            <label>16</label>
            <mixed-citation id="d1506e559" publication-type="other">
Marsh K, Forster D, Waruiru C, Mwangi I, Winstanley M, Marsh V, et al.
Indicators of life-threatening malaria in African children. N Engl J Med
1995;332:1399-404.</mixed-citation>
         </ref>
         <ref id="d1506e573a1310">
            <label>17</label>
            <mixed-citation id="d1506e580" publication-type="other">
Schellenberg D, Menendez C, Kahigwa E, Font F, Galindo C, Acosta C, et
al. African children with malaria in an area of intense Plasmodium falci-
parum transmission: features on admission to the hospital and risk
factors for death. Am J Trop Med Hyg 1999;61:431 -8.</mixed-citation>
         </ref>
         <ref id="d1506e596a1310">
            <label>18</label>
            <mixed-citation id="d1506e603" publication-type="other">
Smith T, Schellenberg JA, Hayes R. Attributable fraction estimates and
case definitions for malaria in endemic areas. Stat Med 1994;13:2345-58.</mixed-citation>
         </ref>
         <ref id="d1506e613a1310">
            <label>19</label>
            <mixed-citation id="d1506e620" publication-type="other">
Mundy C, Ngwira M, Kadewele G, Bates I, Squire SB, Gilks CF. Evaluation
of microscope condition in Malawi. Trans R Soc Trop Med Hyg
2000;94:583-4.</mixed-citation>
         </ref>
         <ref id="d1506e633a1310">
            <label>20</label>
            <mixed-citation id="d1506e640" publication-type="other">
WHO. Management of the child with severe infection or severe malnutrition.
Geneva: WHO, 2000</mixed-citation>
         </ref>
         <ref id="d1506e650a1310">
            <label>21</label>
            <mixed-citation id="d1506e657" publication-type="other">
English M, Berkley J, Mwangi I, Mohammed S, Ahmed M, Osire F, et al.
Hypothetical performance of syndrome-based management of acute
paediatric admissions of children aged more than 60 days in a Kenyan
district hospital. Bull Wrrld Health Organ 2003;81:166-73.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
