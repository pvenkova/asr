<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41352700</article-id>
         <title-group>
            <article-title>Marriage exchanges, seed exchanges, and the dynamics of manioc diversity</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Marc</given-names>
                  <surname>Delêtre</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Doyle B.</given-names>
                  <surname>McKey</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Trevor R.</given-names>
                  <surname>Hodkinson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>8</day>
            <month>11</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">108</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">45</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40064397</issue-id>
         <fpage>18249</fpage>
         <lpage>18254</lpage>
         <permissions>
            <copyright-statement>copyright © 1993-2008 National Academy of Sciences of the United States of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41352700"/>
         <abstract>
            <p>The conservation of crop genetic resources requires understanding the different variables—cultural, social, and economic—that impinge on crop diversity. In small-scale farming systems, seed exchanges represent a key mechanism in the dynamics of crop genetic diversity, and analyzing the rules that structure social networks of seed exchange between farmer communities can help decipher patterns of crop genetic diversity. Using a combination of ethnobotanical and molecular genetic approaches, we investigated the relationships between regional patterns of manioc genetic diversity in Gabon and local networks of seed exchange. Spatially explicit Bayesian clustering methods showed that geographical discontinuities of manioc genetic diversity mirror major ethnolinguistic boundaries, with a southern matrilineal domain characterized by high levels of varietal diversity and a northern patrilineal domain characterized by low varietal diversity. Borrowing concepts from anthropology—kinship, bridewealth, and filiation—we analyzed the relationships between marriage exchanges and seed exchange networks in patrilineal and matrilineal societies. We demonstrate that, by defining marriage prohibitions, kinship systems structure social networks of exchange between farmer communities and influence the movement of seeds in metapopulations, shaping crop diversity at local and regional levels.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d879e186a1310">
            <label>1</label>
            <mixed-citation id="d879e193" publication-type="other">
Almekinders GM, Louwaars NP, de Bruijn GH (1994) Local seed systems and their im-
portance for an improved seed supply in developing countries. Euphytica 78:207-216.</mixed-citation>
         </ref>
         <ref id="d879e203a1310">
            <label>2</label>
            <mixed-citation id="d879e210" publication-type="other">
Berkes F, Folke С (1998) Linking Ecological and Social Systems: Management Practices
and Social Mechanisms for Building Resilience, eds Folke C, Berkes F (Cambridge
University Press, Cambridge, UK), pp 1-25.</mixed-citation>
         </ref>
         <ref id="d879e223a1310">
            <label>3</label>
            <mixed-citation id="d879e230" publication-type="other">
Brush SB, Carney HJ, Huamán Z (1981) Dynamics of Andean potato agriculture. Econ
Bot 35:70-88.</mixed-citation>
         </ref>
         <ref id="d879e240a1310">
            <label>4</label>
            <mixed-citation id="d879e247" publication-type="other">
Louette D, Charrier A, Berthaud J (1997) In situ conservation of maize in Mexico: Genetic
diversity and maize seed management in a traditional community. Econ Bot 51:20-38.</mixed-citation>
         </ref>
         <ref id="d879e258a1310">
            <label>5</label>
            <mixed-citation id="d879e265" publication-type="other">
Bellon MR, Hodson D, Hellin J (2011) Assessing the vulnerability of traditional maize
seed systems in Mexico to climate change. Proc Natl Acad Sci USA 108:13432-13437.</mixed-citation>
         </ref>
         <ref id="d879e275a1310">
            <label>6</label>
            <mixed-citation id="d879e282" publication-type="other">
Boster JS (1986) Exchange of varieties and information between Aguaruna manioc
cultivators. Am Anthropol 88:428-436.</mixed-citation>
         </ref>
         <ref id="d879e292a1310">
            <label>7</label>
            <mixed-citation id="d879e299" publication-type="other">
Chernela JM (1986) SUMA: Etnológica Brasilieira, ed Ribeiro В (FINEP, Petrópolis,
Brazil), pp 151-158.</mixed-citation>
         </ref>
         <ref id="d879e309a1310">
            <label>8</label>
            <mixed-citation id="d879e316" publication-type="other">
Emperaire L, Peroni N (2007) Traditional management of agrobiodiversity in Brazil: A
case studv of manioc. Hum Ecol 35:761-768.</mixed-citation>
         </ref>
         <ref id="d879e326a1310">
            <label>9</label>
            <mixed-citation id="d879e333" publication-type="other">
Badstue LB, et al. (2007) The dynamics of farmers' maize seed supply practices in the
central valleys of Oaxaca, Mexico. World Dev 35:1579-1593.</mixed-citation>
         </ref>
         <ref id="d879e343a1310">
            <label>10</label>
            <mixed-citation id="d879e350" publication-type="other">
Coomes ОТ (2010) Of stakes, stems and cuttings: The importance of local seed systems
in traditional Amazonian societies. Prof Geogr 62:323-334.</mixed-citation>
         </ref>
         <ref id="d879e361a1310">
            <label>11</label>
            <mixed-citation id="d879e368" publication-type="other">
Stromberg PM, Pascual U, Bellon MR (2010) Seed systems and farmers' seed choices:
The case of maize in the Peruvian Amazon. Hum Ecol 38:539-553.</mixed-citation>
         </ref>
         <ref id="d879e378a1310">
            <label>12</label>
            <mixed-citation id="d879e385" publication-type="other">
Eyzaguirre P (2006) Researching the Culture in Agriculture, eds Cernea M, Kassam A
(CABI, Wallingford, UK), pp 264-284.</mixed-citation>
         </ref>
         <ref id="d879e395a1310">
            <label>13</label>
            <mixed-citation id="d879e402" publication-type="other">
Emperaire L, Pintón F, Second G (1998) Gestion dynamique de la diversité variétale du
manioc en Amazonie du Nord-Ouest. Nat Sci Soc 6:27-42.</mixed-citation>
         </ref>
         <ref id="d879e412a1310">
            <label>14</label>
            <mixed-citation id="d879e419" publication-type="other">
Sirbanchongkran A, et al. (2004) Varietal turnover and seed exchange: Implications
for conservation of rice genetic diversity on farm. Int Rice Res Notes 29:12-14.</mixed-citation>
         </ref>
         <ref id="d879e429a1310">
            <label>15</label>
            <mixed-citation id="d879e436" publication-type="other">
Perales HR, Benz BF, Brush SB (2005) Maize diversity and ethnolinguistic diversity in
Chiapas, Mexico. Proc Natl Acad Sci USA 102:949-954.</mixed-citation>
         </ref>
         <ref id="d879e446a1310">
            <label>16</label>
            <mixed-citation id="d879e453" publication-type="other">
Berthouly C, et al. (2009) How does farmer connectivity influence livestock genetic
structure? A case-study in a Vietnamese goat population. Mol Ecol 18:3980-3991.</mixed-citation>
         </ref>
         <ref id="d879e464a1310">
            <label>17</label>
            <mixed-citation id="d879e471" publication-type="other">
Murdock GP (1967) Ethnographic Atlas (University of Pittsburgh Press, Pittsburgh).</mixed-citation>
         </ref>
         <ref id="d879e478a1310">
            <label>18</label>
            <mixed-citation id="d879e485" publication-type="other">
Aberle DF (1961) Matrilineal Kinship, eds Schneider DM, Gough К (University of
California Press, Berkeley, CA), pp 655-727.</mixed-citation>
         </ref>
         <ref id="d879e495a1310">
            <label>19</label>
            <mixed-citation id="d879e502" publication-type="other">
Guthrie M (1948) The Classification of the Bantu Languages (Oxford University Press
for the International African Institute, London).</mixed-citation>
         </ref>
         <ref id="d879e512a1310">
            <label>20</label>
            <mixed-citation id="d879e519" publication-type="other">
Jones WO (1959) Manioc in Africa (Stanford University Press, Palo Alto, CA).</mixed-citation>
         </ref>
         <ref id="d879e526a1310">
            <label>21</label>
            <mixed-citation id="d879e533" publication-type="other">
Howard PL (2003) Women and Plants: Gender Relations in Biodiversity Management
and Conservation (Zed Press, London).</mixed-citation>
         </ref>
         <ref id="d879e543a1310">
            <label>22</label>
            <mixed-citation id="d879e550" publication-type="other">
Boster JS (1985) Directions in Cognitive Anthropology, ed Dougherty J (University of
Illinois Press, Urbana, IL), pp 177-197.</mixed-citation>
         </ref>
         <ref id="d879e561a1310">
            <label>23</label>
            <mixed-citation id="d879e568" publication-type="other">
Ferguson AE (1994) Gendered science: A critique of agricultural development. Am
Anthropol 96:540-552.</mixed-citation>
         </ref>
         <ref id="d879e578a1310">
            <label>24</label>
            <mixed-citation id="d879e585" publication-type="other">
Chambers KJ, Momsen JH (2007) From the kitchen and the field: Gender and maize
diversity in the Bajo region of Mexico. Singap J Trop Geogr 28:39-56.</mixed-citation>
         </ref>
         <ref id="d879e595a1310">
            <label>25</label>
            <mixed-citation id="d879e602" publication-type="other">
Tapia ME, De la Torre A (1998) Women Farmers and Andean Seeds (FAO/IPGRI. Rome).</mixed-citation>
         </ref>
         <ref id="d879e609a1310">
            <label>26</label>
            <mixed-citation id="d879e616" publication-type="other">
Rossel G (1987) Gewas innovaties in Gabon van prehistorie tot koloniale tijd. MSc
dissertation (Wageningen Agricultural University, Wageningen, The Netherlands).</mixed-citation>
         </ref>
         <ref id="d879e626a1310">
            <label>27</label>
            <mixed-citation id="d879e633" publication-type="other">
Alexandre P (1965) Proto-histoire du groupe Beti-Bulu-Fang: Essai de synthèse pro-
visoire. Cah Etud Afr 4:503-560.</mixed-citation>
         </ref>
         <ref id="d879e643a1310">
            <label>28</label>
            <mixed-citation id="d879e650" publication-type="other">
Joiris DV, Bahuchet S (1994) Situation des Populations Indigènes des Forêts Denses
Humides, eds Bahuchet S, de Maret P (Office des Publications Officielles des Com-
munautés Européennes, Luxembourg), pp 387-441.</mixed-citation>
         </ref>
         <ref id="d879e664a1310">
            <label>29</label>
            <mixed-citation id="d879e671" publication-type="other">
Delêtre M (2010) The ins and outs of manioc diversity in Gabon, Central Africa: A plu-
ridisciplinary approach to the dynamics of genetic diversity of Manihot esculenta Crantz
(Euphorbiaceae). PhD dissertation (Trinity College, University of Dublin, Dublin, Ireland).</mixed-citation>
         </ref>
         <ref id="d879e684a1310">
            <label>30</label>
            <mixed-citation id="d879e691" publication-type="other">
McKey D, Elias M, Pujol В, Duputié A (2010) The evolutionary ecology of donally
propagated domesticated plants. New Phytol 186:318-332.</mixed-citation>
         </ref>
         <ref id="d879e701a1310">
            <label>31</label>
            <mixed-citation id="d879e708" publication-type="other">
Peroni N, Hanazaki N (2002) Current and lost diversity of cultivated varieties, espe-
cially cassava, under swidden cultivation systems in the Brazilian Atlantic Forest. Agrie
Ecosyst Environ 92:171-183.</mixed-citation>
         </ref>
         <ref id="d879e721a1310">
            <label>32</label>
            <mixed-citation id="d879e728" publication-type="other">
Cogéis S (2002) Les Ntumu du sud-Cameroun forestier: Une société de non-spécial-
istes. Système de production, stratégies d'usage des ressources et enjeux du
changement. PhD dissertation (Université Libre de Bruxelles, Brussels, Belgium).</mixed-citation>
         </ref>
         <ref id="d879e741a1310">
            <label>33</label>
            <mixed-citation id="d879e748" publication-type="other">
Lévi-Strauss С (1966) The Savage Mind (Weidenfeld &amp; Nicolson, London).</mixed-citation>
         </ref>
         <ref id="d879e755a1310">
            <label>34</label>
            <mixed-citation id="d879e762" publication-type="other">
Karp I (1988) Laughter at marriage: Subversion in performance. J Folklore Res 25:35-52.</mixed-citation>
         </ref>
         <ref id="d879e770a1310">
            <label>35</label>
            <mixed-citation id="d879e777" publication-type="other">
Meillassoux С (1960) Essai d'interprétation du phénomène économique dans les so-
ciétés traditionnelles d'autosubsistance. Cah Etud Afr 1 :38-67.</mixed-citation>
         </ref>
         <ref id="d879e787a1310">
            <label>36</label>
            <mixed-citation id="d879e794" publication-type="other">
Herbich I, Dietler M (2008) Breaking Down Boundaries: Anthropological Approaches
to Cultural Transmission and Material Culture, eds Stark MT, Bowser BJ, Hörne L
(University of Arizona Press, Tucson, AZ), pp 223-244.</mixed-citation>
         </ref>
         <ref id="d879e807a1310">
            <label>37</label>
            <mixed-citation id="d879e814" publication-type="other">
Goheen M (1996) Men Own the Fields, Women Own the Crops: Gender and Power in
the Cameroon Grassfields (University of Wisconsin Press. Madison. Wl).</mixed-citation>
         </ref>
         <ref id="d879e824a1310">
            <label>38</label>
            <mixed-citation id="d879e831" publication-type="other">
Goody J (1969) Comparative Studies in Kinship (Routledge and Kegan Paul, London),
pp 120-146.</mixed-citation>
         </ref>
         <ref id="d879e841a1310">
            <label>39</label>
            <mixed-citation id="d879e848" publication-type="other">
Perrois L (1970) Chroniques du pays Kota (Gabon). Cah Orstom (Sci Hum) 7:15-119.</mixed-citation>
         </ref>
         <ref id="d879e855a1310">
            <label>40</label>
            <mixed-citation id="d879e862" publication-type="other">
Bûcher HH (1975) Mpongwe origins: Historiographical perspectives. Hist Afr 2:59-89.</mixed-citation>
         </ref>
         <ref id="d879e870a1310">
            <label>41</label>
            <mixed-citation id="d879e877" publication-type="other">
Bohannan L (1949) Dahomean marriage: A revaluation. Africa 19:273-287.</mixed-citation>
         </ref>
         <ref id="d879e884a1310">
            <label>42</label>
            <mixed-citation id="d879e891" publication-type="other">
Goody J, Buckley J (1973) Inheritance and women's labour in Africa. Africa 43:108-121.</mixed-citation>
         </ref>
         <ref id="d879e898a1310">
            <label>43</label>
            <mixed-citation id="d879e905" publication-type="other">
Rival L (2001) Gender in Amazonia and Melanesia: An Exploration of the Comparative
Method, eds Gregor T, Tuzin D (University of California Press, Berkeley, CA), pp 57-79.</mixed-citation>
         </ref>
         <ref id="d879e915a1310">
            <label>44</label>
            <mixed-citation id="d879e922" publication-type="other">
Weiner AB (1979) Trobriand kinship from another view: The reproductive power of
women and men. Man (Lond) 14:328-348.</mixed-citation>
         </ref>
         <ref id="d879e932a1310">
            <label>45</label>
            <mixed-citation id="d879e939" publication-type="other">
Pintón F, Emperaire L (2001) Le manioc en Amazonie brésilienne: Diversité variétale
et marché. Genet Sel Evol 33:S491-S512.</mixed-citation>
         </ref>
         <ref id="d879e949a1310">
            <label>46</label>
            <mixed-citation id="d879e956" publication-type="other">
Binot A (1998) Particularités de l'agriculture et approche de la dynamique post-
culturale en périphérie de la réserve de la Lopé, Gabon. MSc dissertation (Université
Libre de Bruxelles, Brussels, Belgium).</mixed-citation>
         </ref>
         <ref id="d879e970a1310">
            <label>47</label>
            <mixed-citation id="d879e977" publication-type="other">
Descola P (2001) Gender in Amazonia and Melanesia: An Exploration of the Compara-
tive Method, eds Gregor T, Tuzin D (University of California Press, Berkeley, CA), pp 91-1 14.</mixed-citation>
         </ref>
         <ref id="d879e987a1310">
            <label>48</label>
            <mixed-citation id="d879e994" publication-type="other">
Hugh-Jones S (2001) Gender in Amazonia and Melanesia: An Exploration of the
Comparative Method, eds Gregor T, Tuzin D (University of California Press, Berkeley,
CA), pp 245-278.</mixed-citation>
         </ref>
         <ref id="d879e1007a1310">
            <label>49</label>
            <mixed-citation id="d879e1014" publication-type="other">
Biersack A (2001) Gender in Amazonia and Melanesia: An Exploration of the Comparative
Method, eds Gregor T, Tuzin D (University of California Press, Berkeley, CA), pp 69-90.</mixed-citation>
         </ref>
         <ref id="d879e1024a1310">
            <label>50</label>
            <mixed-citation id="d879e1031" publication-type="other">
Barth F (1969) Ethnic Groups and Boundaries: The Social Organization of Culture
Difference (Little Brown &amp; Co, Boston).</mixed-citation>
         </ref>
         <ref id="d879e1041a1310">
            <label>51</label>
            <mixed-citation id="d879e1048" publication-type="other">
Hamilton G, Stoneking M, Excoffier L (2005) Molecular analysis reveals tighter social
regulation of immigration in patrilocal populations than in matrilocal populations.
Proc Natl Acad Sci USA 102:7476-7480.</mixed-citation>
         </ref>
         <ref id="d879e1061a1310">
            <label>52</label>
            <mixed-citation id="d879e1068" publication-type="other">
Chaix R, et al. (2007) From social to genetic structures in central Asia. Curr Biol 1 7:43-48.</mixed-citation>
         </ref>
         <ref id="d879e1076a1310">
            <label>53</label>
            <mixed-citation id="d879e1083" publication-type="other">
Heyer E, et al. (2009) Genetic diversity and the emergence of ethnic groups in Central
Asia. BMC Genet 10:49.</mixed-citation>
         </ref>
         <ref id="d879e1093a1310">
            <label>54</label>
            <mixed-citation id="d879e1100" publication-type="other">
Haudricourt AG (1964) Nature et culture dans la civilisation de l'igname: L'origine des
clones et des clans. Homme 4:93-104.</mixed-citation>
         </ref>
         <ref id="d879e1110a1310">
            <label>55</label>
            <mixed-citation id="d879e1117" publication-type="other">
Guillot G, Mortier F, Estoup A (2005) Geneland: A computer package for landscape
genetics. Mol Ecol Notes 5:712-715.</mixed-citation>
         </ref>
         <ref id="d879e1127a1310">
            <label>56</label>
            <mixed-citation id="d879e1134" publication-type="other">
Goudet J (1995) FSTAT (version 1.2): A computer program to calculate F-statistics.
J Hered 86:485-486.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
