<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">revinterstud</journal-id>
         <journal-id journal-id-type="jstor">j50000149</journal-id>
         <journal-title-group>
            <journal-title>Review of International Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">02602105</issn>
         <issn pub-type="epub">14699044</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20097515</article-id>
         <title-group>
            <article-title>Degrees of Statehood</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Christopher</given-names>
                  <surname>Clapham</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">24</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i20097513</issue-id>
         <fpage>143</fpage>
         <lpage>157</lpage>
         <permissions>
            <copyright-statement>Copyright 1998 British International Studies Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20097515"/>
         <abstract>
            <p>This paper explores the relationship between statehood and the international system, with particular reference to the states of sub-Saharan Africa. It suggests, as the title implies, that statehood should be regarded as a relative concept; and that rather than distinguish sharply between entities that are, and are not, states, we should regard different entities as meeting the criteria for international statehood to a greater or lesser degree. Entities which we have been accustomed to regard as states, at least for the purposes of studying them in international relations, sometimes fail to exercise even the minimal responsibilities associated with state power, while those who control them do not behave in the way that is normally ascribed to the 'rulers' of states. Entities that are not accorded the status of states, such as guerrilla insurgencies or even voluntary organizations, may take on attributes that have customarily been associated with sovereign statehood. This conclusion carries at least a salutary warning against too readily ascribing the supposedly universal characteristics of states to peripheral areas of the modern global system, in which the categories in which we are accustomed to regard international politics have become blurred. More broadly, given the peculiar and privileged position of states in the conventional analysis of international relations, it may carry significant implications for the idea of international relations itself.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1177e114a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d1177e121" publication-type="other">
Clapham, Africa and the International System: The Politics of State Survival
(Cambridge, 1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e131a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1177e138" publication-type="other">
Mohammed Ayoob, The Third World Security Predicament: State Making, Regional Conflict, and
the International System (Boulder, CO, 1995), esp. ch. 2</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e148a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1177e155" publication-type="other">
Robert H. Jackson, Quasi-states: Sovereignty, International Relations and the Third World
(Cambridge, 1990).</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e165a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1177e172" publication-type="other">
Ibid., p. 22.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e180a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1177e187" publication-type="other">
Fred Halliday, Rethinking International Relations (London, 1994), pp. 3-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e194a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1177e201" publication-type="other">
G. J. Naldi, Documents of the Organisation of African Unity (London, 1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e207" publication-type="other">
Caroline Thomas, In Search of Security: The Third World in International
Relations (Brighton, 1987), pp. 64-70.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e217a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1177e224" publication-type="other">
Ayoob, Third World Security Predicament, p. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e231a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1177e238" publication-type="other">
Steven R. David, Changing Sides: Alignment and Realignment in the Third World (Baltimore, MD,
1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e248a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1177e255" publication-type="other">
Bade Onimode, The IMF, the World Bank and the African Debt (London, 1989)</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e262a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1177e269" publication-type="other">
Third World Quarterly, 17:4 (1996), special issue on
'The Developmental State?'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e280a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1177e287" publication-type="other">
John Toye, 'Structural
Adjustment: Context, Assumptions, Origins and Diversity', in R. van der Hoeven and F. van der
Kraaij (eds.), Structural Adjustment and Beyond in Sub-Saharan Africa (London, 1994), ch. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e300a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1177e307" publication-type="other">
Clapham, Africa and the International System, ch. 8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e314a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1177e321" publication-type="other">
Jackson, Quasi-states, pp. 154-9</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e327" publication-type="other">
Claude E. Welch,
'The Organisation of African Unity and the Promotion of Human Rights', Journal of Modern
African Studies, 29 (1991), pp. 535-55.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e340a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1177e347" publication-type="other">
Thomas M. Callaghy, 'Africa and the World Political Economy: Still
Caught Between a Rock and a Hard Place', in John W. Harbeson and Donald Rothchild (eds.), Africa
in World Politics: Post-Cold War Challenges (Boulder, CO, 1995), ch. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e360a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1177e367" publication-type="other">
Stephen Ellis and Tsepo Sechaba, Comrades against Apartheid: The ANC and the
South African Communist Party in Exile (London, 1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e376" publication-type="other">
Colin Leys and John Saul (eds.), Namibia's
Liberation Struggle: The Two-edged Sword (London, 1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e385" publication-type="other">
W. Cyrus Reed, 'International Politics
and National Liberation: ZANU and the Politics of Contested Sovereignty in Zimbabwe', African
Studies Review, 36 (1993), pp. 31-59</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e398" publication-type="other">
Clapham, Africa and the International System, ch. 9</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e404" publication-type="other">
Christopher
Clapham, 'The International Relations of Africa's Guerrilla Movements', South African Journal of
International Affairs, 3 (1995), pp. 81-91.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e417a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1177e424" publication-type="other">
Paul Richards, 'Rebellion in Liberia and Sierra Leone: A Crisis of Youth?', in Oliver Furley (ed.),
Conflict in Africa (London, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e435a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1177e442" publication-type="other">
William Reno, 'Foreign Firms and the Financing of Charles Taylor's NPFL', Liberian Studies
Journal, 18 (1993), pp. 175-88.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e452a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1177e459" publication-type="other">
William DeMars, 'Tactics of Protection: International Human Rights Organizations in the
Ethiopian Conflict, 1980-1986', in Eileen McCarthy-Arnolds et al. (eds.), Africa, Human Rights and
the International System (Westport, CT, 1994), ch.5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e472a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1177e479" publication-type="other">
Scott Thomas, The Diplomacy of Liberation: The International Relations of the African National
Congress of South Africa, 1960-1985 (London, 1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e489a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1177e496" publication-type="other">
William Reno, Corruption and State Politics in Sierra Leone (Cambridge, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e503a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1177e510" publication-type="other">
Crawford Young and Thomas Turner, The Rise and Decline of the Zairean State (Madison, WI,
1985), pp. 387-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e520a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1177e527" publication-type="other">
Reno, Corruption and State Politics, pp. 137-8</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e533" publication-type="other">
Samuel O. Atteh, 'Political Economy of
Environmental Degradation: The Dumping of Toxic Waste in Africa', International Studies (New
Delhi), 30 (1993), pp. 277-98</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1177e545" publication-type="other">
Phil O'Keefe, 'Toxic Terrorism', Review of African Political Economy,
42 (1988), pp. 84-90.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e556a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1177e563" publication-type="other">
Reno, Corruption and State Politics.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e570a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1177e577" publication-type="other">
F. De Boeck, 'Postcolonialism, Power and Identity: Local and Global Perspectives from Zaire', in
Richard Werbner and Terence Ranger (eds.), Postcolonial Identities in Africa (London, 1996), ch. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e587a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1177e594" publication-type="other">
I. William Zartman (ed.), Collapsed States: The Disintegration and Restoration of Legitimate
Authority (Boulder, CO, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e604a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1177e611" publication-type="other">
Gerald B. Helman and Steven R. Ratner, 'Saving Failed States', Foreign Policy, 89 (1992/3), pp. 3-20.</mixed-citation>
            </p>
         </fn>
         <fn id="d1177e618a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1177e625" publication-type="other">
Walter Clarke and Jeffrey Herbst (eds.), Learning from Somalia: The Lessons of Armed
Humanitarian Intervention (Boulder, CO, 1997).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
