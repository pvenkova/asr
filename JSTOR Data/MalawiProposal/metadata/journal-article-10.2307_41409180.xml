<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">reprhealmatt</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100842</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Reproductive Health Matters</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Elsevier Ltd.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09688080</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41409180</article-id>
         <article-categories>
            <subj-group>
               <subject>Features</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Sub-Saharan Africa and the health MDGs: the need to move beyond the "quick impact" model</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Fabienne</given-names>
                  <surname>Richard</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David</given-names>
                  <surname>Hercot</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Charlemagne</given-names>
                  <surname>Ouédraogo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thérèse</given-names>
                  <surname>Delvaux</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Salif</given-names>
                  <surname>Samaké</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Josefien</given-names>
                  <surname>van Olmen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ghislaine</given-names>
                  <surname>Conombo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rachel</given-names>
                  <surname>Hammonds</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jan</given-names>
                  <surname>Vandemoortele</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">19</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">38</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40068584</issue-id>
         <fpage>42</fpage>
         <lpage>55</lpage>
         <permissions>
            <copyright-statement>© Reproductive Health Matters 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41409180"/>
         <abstract>
            <p>The Millennium Development Goals (MDGs) were defined in 2001, making poverty the central focus of the global political agenda. In response to MDG targets for health, new funding instruments called Global Health Initiatives were set up to target specific diseases, with an emphasis on "quick win" interventions, in order to show improvements by 2015. In 2005 the UN Millennium Project defined quick wins as simple, proven interventions with "very high potential short-term impact that can be immediately implemented", in contrast to "other interventions which are more complicated and will take a decade of effort or have delayed benefits". Although the terminology has evolved from "quick wins" to "quick impact initiatives" and then to "high impact interventions", the short-termism of the approach remains. This paper examines the merits and limitations of MDG indicators for assessing progress and their relationship to quick impact interventions. It then assesses specific health interventions through both the lens of time and their integration into health care services, and examines the role of health systems strengthening in support of the MDGs. We argue that fast-track interventions promoted by donors and Global Health Initiatives need to be complemented by mid-and long-term strategies, cutting across specific health problems. Implementing the MDGs is more than a process of "money changing hands". Combating poverty needs a radical overhaul of the partnership between rich and poor countries and between rich and poor people within countries. Les objectifs du Millénaire pour le développement (OMD), définis en 2001, ont placé la pauvreté au centre des préoccupations politiques mondiales.En réponse aux OMD de la santé, de nouveaux instruments de financement, appelés initiatives de santé mondiale, ont été créés pour cibler des maladies spécifiques avec des interventions à « gains rapides » , afin d'obtenir des améliorations d'ici 2015. En 2005, le Projet Objectifs du Millénaire des Nations Unies a décrit les gains rapides comme des interventions simples et éprouvées avec « un impact potentiel très élevé à court terme et pouvant être appliquées immédiatement » , contrairement à « d'autres interventions plus compliquées, qui demanderont dix ans d'efforts ou auront des bienfaits différés » .La terminologie a évolué de « gains rapides » à « initiatives à impact rapide » , puis « interventions à fort impact » , mais la nature à court terme de l'approche demeure. Cet article examine les mérites et les limites des indicateurs des 0MD pour évaluer les progrès et leur lien avec les interventions à impact rapide. Il évalue ensuite des interventions de santé dans la perspective des délais et de leur intégration dans les services de santé, et analyse le rôle du renforcement des systèmes de santé à l'appui des 0MD. Nous estimons qu'il faut compléter les interventions à court terme prônées par les donateurs et les initiatives de santé mondiale avec des stratégies à moyen et long terme qui dépasseront les problèmes spécifiques de santé. La réalisation des OMD est davantage qu'un « transfert de fonds » . La lutte contre la pauvreté requiert une refonte radicale du partenariat entre pays riches et pauvres, et entre riches et pauvres à l'intérieur des pays. Los Objetivos de Desarrollo del Milenio (ODM), definidos en 2001, centraron la agenda política mundial en la pobreza. En respuesta a los 0DM en salud, se establecieron nuevos mecanismos de financiamiento, llamados Iniciativas Sanitarias Mundiales dirigidas a enfermedades específicas, con énfasis en intervenciones que produzcan resultados positivos rápidos, a fin de mostrar mejoras para el 2015. En 2005 el Proyecto del Milenio de las Naciones Unidas definió resultados positivos rápidos como intervenciones sencillas comprobadas con "un potencial muy alto de impacto a corto plazo que se puede implementar inmediatamente", a diferencia de "otras intervenciones que son más complicadas y requerirán una década de esfuerzos o tendrán beneficios atrasados". Aunque la terminología ha evolucionado de "resultados positivos rápidos" a "iniciativas de rápido impacto" y luego a "intervenciones de gran impacto", aún continúa siendo un enfoque de corto plazo. En este artículo se examinan los méritos y las limitaciones de los indicadores de los ODM para evaluar los avances y su relación con las intervenciones de rápido impacto. Se evalúan determinadas intervenciones en salud a lo largo del tiempo y su integración en los servicios de salud; además, se examina el rol de fortalecer los sistemas de salud para apoyar los ODM. Se arguye que las intervenciones que están en la "vía rápida", promovidas por donantes, y las Iniciativas Sanitarias Mundiales deben ser complementadas por estrategias de medio y largo plazo, que abarquen diversos problemas de salud.Realizar los ODM es más que una "transferencia de fondos". La lucha contra la pobreza requiere una revisión radical de las alianzas entre los países ricos y los pobres y entre ricos y pobres en esos países.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d214e261a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d214e268" publication-type="other">
&lt;www.abdn.ac.uk/femhealth/&gt;. August 2011.</mixed-citation>
            </p>
         </fn>
         <fn id="d214e275a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d214e282" publication-type="other">
&lt;www.thenigerianvoice.com/nvnews/3003 2/1 /mdgs-
quick-win-as-campaign-tools-for-2011-election.html&gt;.
June 2010.</mixed-citation>
            </p>
         </fn>
         <fn id="d214e295a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d214e302" publication-type="other">
&lt;www.kff.org/globalhealth/upload/8160.pdf&gt;.
March 2011.</mixed-citation>
            </p>
         </fn>
         <fn id="d214e312a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d214e319" publication-type="other">
&lt;www.
who.int/dg/reform/en/index.html&gt;.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d214e338a1310">
            <label>1</label>
            <mixed-citation id="d214e345" publication-type="other">
United Nations. United Nations
Millennium Declaration.
Resolution adopted by the UN
General Assembly. A/RES/55/2.
New York: UN, 2000.</mixed-citation>
         </ref>
         <ref id="d214e364a1310">
            <label>2</label>
            <mixed-citation id="d214e373" publication-type="other">
United Nations. Road map
towards the implementation of
the United Nations Millenium
Declaration. UN General Assembly.
A/56/326. New York: UN, 2001.</mixed-citation>
         </ref>
         <ref id="d214e392a1310">
            <label>3</label>
            <mixed-citation id="d214e401" publication-type="other">
Waage J, Baneiji R, Campbell 0,
et al. The Millennium
Development Goals: a cross-
sectoral analysis and principles
for goal setting after 2015. Lancet
2010;376(9745):99 1-1023.</mixed-citation>
         </ref>
         <ref id="d214e424a1310">
            <label>4</label>
            <mixed-citation id="d214e433" publication-type="other">
UN Development Group.
Making the MDGs matter: the
country response. New York:
UN Development Group, 2005.</mixed-citation>
         </ref>
         <ref id="d214e450a1310">
            <label>5</label>
            <mixed-citation id="d214e457" publication-type="other">
UN Millennium Project. Public
Choices, Private Decisions:
Sexual and Reproductive
Health and the Millennium
Development Goals. London:
Earthscan, 2006. At: &lt;http://
www.unmillenniumproject.org/
documents/MP_Sexual_Health_
screen-final.pdf&gt;. Accessed
30 July 2011.</mixed-citation>
         </ref>
         <ref id="d214e492a1310">
            <label>6</label>
            <mixed-citation id="d214e499" publication-type="other">
United Nations. The Millennium
Developement Goal Report
2005. New York: UN, 2005.
At: &lt;http://www.un.org/
summit2005/MDGBook.pdf&gt;.
Accessed 30 July 2011.</mixed-citation>
         </ref>
         <ref id="d214e522a1310">
            <label>7</label>
            <mixed-citation id="d214e529" publication-type="other">
Sundewall J, Swanson RC,
Betigeri A, et al. Health-systems
strengthening: current and
future activities. Lancet 2011;
377(9773): 1222-23.</mixed-citation>
         </ref>
         <ref id="d214e548a1310">
            <label>8</label>
            <mixed-citation id="d214e555" publication-type="other">
United Nations. The Millenium
Developement Goal Report
2010. New York: UN, 2010.
At: &lt;http://unstats.un.org/unsd/
mdg/Resources/Static/Products/
Progress20 1 0/MDG_Report_
20 1 0_En_low°/o20res.pdf&gt;.
Accessed 26 April 2011.</mixed-citation>
         </ref>
         <ref id="d214e584a1310">
            <label>9</label>
            <mixed-citation id="d214e591" publication-type="other">
Commission on Macroeconomics
and Health. Macroeconomics and
Health I Investing in Health for
Economic Development. Geneva:
WHO, 2001.</mixed-citation>
         </ref>
         <ref id="d214e610a1310">
            <label>10</label>
            <mixed-citation id="d214e617" publication-type="other">
Brugha R, Walt G. A global
health fund: a leap of faith?
BMJ 2001 ;323(7305): 152-54.</mixed-citation>
         </ref>
         <ref id="d214e631a1310">
            <label>11</label>
            <mixed-citation id="d214e638" publication-type="other">
Biesma RG, Brugha R, Harmer
A, et al. The effects of global
health initiatives on country
health systems: a review of the
evidence from HIV/AIDS
control. Health Policy and
Planning 2009;24(4):239-52.</mixed-citation>
         </ref>
         <ref id="d214e664a1310">
            <label>12</label>
            <mixed-citation id="d214e671" publication-type="other">
Travis P, Bennett S, Haines A,
et al. Overcoming health systems
constraints to achieve the
Millennium Development Goals.
Lancet 2004 ;3 64(9437) :900-06.</mixed-citation>
         </ref>
         <ref id="d214e690a1310">
            <label>13</label>
            <mixed-citation id="d214e699" publication-type="other">
World Health Organization
Maximizing Positive Synergies
Collaborative Group. An
assessment of interactions
between global health
initiatives and country
health systems. Lancet 2009;
373(968 1):2 137-69.</mixed-citation>
         </ref>
         <ref id="d214e728a1310">
            <label>14</label>
            <mixed-citation id="d214e735" publication-type="other">
UN Millenium Project. Investing
in development. A practical plan
to achieve the Millenium
Development Goals. Overview.
Washington DC: Communications
Developement Ine, 2005. At:
&lt; www.unmillenniumproj ect.
org/documents/
MainReportComplete-lowres.
pdf&gt;. Accessed 26 April 201 1.</mixed-citation>
         </ref>
         <ref id="d214e770a1310">
            <label>15</label>
            <mixed-citation id="d214e777" publication-type="other">
Shiffman J. A social
explanation for the rise and fall
of global health issues. Bulletin
of World Health Organization
2009;87(8):608-13.</mixed-citation>
         </ref>
         <ref id="d214e796a1310">
            <label>16</label>
            <mixed-citation id="d214e803" publication-type="other">
Ooms G, Hammonds R, Decoster
C, et al. Global health: what it has
been so far, what it should be, and
what it could become. Decoster C,
Marchai В, Soors W, et al, editors.
Working Paper Series of the
Studies in Health Services
Organisation and Policy.
Antwerp: ГГС Press, 2011.</mixed-citation>
         </ref>
         <ref id="d214e836a1310">
            <label>17</label>
            <mixed-citation id="d214e843" publication-type="other">
Yamey G. Scaling up global
health interventions:
a proposed framework for
success. PLoS Medicine 2011;
8(6):el001049.</mixed-citation>
         </ref>
         <ref id="d214e862a1310">
            <label>18</label>
            <mixed-citation id="d214e869" publication-type="other">
Marchai В, Cavalli A, Kegels G.
Global health actors claim
to support health system
strengthening: is this reality
or rhetoric? PLoS Medicine
2009 ;6(4) :e 1 000059.</mixed-citation>
         </ref>
         <ref id="d214e892a1310">
            <label>19</label>
            <mixed-citation id="d214e899" publication-type="other">
Bousseiy G, Campos da Silvera V,
Criei В. Community of practice
on the service delivery study
high impact interventions.
Harmonization for Health in
Africa. Antwerp: Institute of
Tropical Medicine, 201 1.</mixed-citation>
         </ref>
         <ref id="d214e925a1310">
            <label>20</label>
            <mixed-citation id="d214e932" publication-type="other">
Coulibaly Y, Cavalli A, Van DM,
et al. Programme activities: a
major burden for district health
systems? Tropical Medicine and
International Health 2008;
13(12): 1430-32.</mixed-citation>
         </ref>
         <ref id="d214e955a1310">
            <label>21</label>
            <mixed-citation id="d214e962" publication-type="other">
Campbell OM, Graham WJ.
Strategies for reducing maternal
mortality: getting on with what
works. Lancet 2006;368(9543):
1284-99.</mixed-citation>
         </ref>
         <ref id="d214e981a1310">
            <label>22</label>
            <mixed-citation id="d214e990" publication-type="other">
Jones G, Steketee RW, Black
RE, et al. How many child
deaths can we prevent this
year? Lancet 2003;362(9377):
65-71.</mixed-citation>
         </ref>
         <ref id="d214e1010a1310">
            <label>23</label>
            <mixed-citation id="d214e1017" publication-type="other">
Chopra M, Daviaud E, Pattinson
R, et al. Saving the lives of
South Africa's mothers, babies,
and children: can the health
system deliver? Lancet 2009;
374(9692):835-46.</mixed-citation>
         </ref>
         <ref id="d214e1040a1310">
            <label>24</label>
            <mixed-citation id="d214e1047" publication-type="other">
UNICEF, World Bank, UNFPA,
PNMCH, UNAIDS. A Strategic
Framework and Investment
Case for Reaching the
Health Related Millennium
Development Goals in Africa by
Strenghtening Primary Health
Care Systems for Outcomes.
New York: UNICEF, 2009.</mixed-citation>
         </ref>
         <ref id="d214e1079a1310">
            <label>25</label>
            <mixed-citation id="d214e1086" publication-type="other">
United Nations. Keeping the
promise: united to achieve the
Millenium Development Goals.
UN General Assembly. A/65/L.1.
New York: UN, 2010.</mixed-citation>
         </ref>
         <ref id="d214e1105a1310">
            <label>26</label>
            <mixed-citation id="d214e1112" publication-type="other">
Murray С J, Frenk J, Evans T. The
Global Campaign for the Health
MDGs: challenges, opportunities,
and the imperative of shared
learning. Lancet 2007;370(9592):
1018-20.</mixed-citation>
         </ref>
         <ref id="d214e1135a1310">
            <label>27</label>
            <mixed-citation id="d214e1142" publication-type="other">
United Nations. The Millenium
Developement Goal Report
2011. New York: UN, 2011. At:
&lt;www.un.org/millenniumgoals/
1 l_MDG°/o20Report_EN.pdf&gt;.
Accessed 30 July 2011.</mixed-citation>
         </ref>
         <ref id="d214e1165a1310">
            <label>28</label>
            <mixed-citation id="d214e1172" publication-type="other">
Vandemoortele J. Is Africa
missing the target or are you
missing the point? Presentation
at IOB, University of Antwerpen,
Debating development series,
10 December 2008. At: &lt;www.ua.
ac.be/main.aspx?c=.IOB&amp;n=
70225&gt;. Accessed 26 April 201 1.</mixed-citation>
         </ref>
         <ref id="d214e1202a1310">
            <label>29</label>
            <mixed-citation id="d214e1209" publication-type="other">
Vandemoortele J. Making sense
of the MDGs. Development
2008;(51):220-27.</mixed-citation>
         </ref>
         <ref id="d214e1222a1310">
            <label>30</label>
            <mixed-citation id="d214e1229" publication-type="other">
UNICEF. Trends in under-fìve
mortality rates 1960-2009. At:
&lt;www.childinfo.org/mortality_
ufmrcountrydata.php&gt;.
Accessed 30 August 2011.</mixed-citation>
         </ref>
         <ref id="d214e1248a1310">
            <label>31</label>
            <mixed-citation id="d214e1255" publication-type="other">
Bhutta ZA, Chopra M, Axelson
H, et al. Countdown to 2015
decade report (2000-10): taking
stock of maternal, newborn, and
child survival. Lancet 2010;
375(9730):2032-44.</mixed-citation>
         </ref>
         <ref id="d214e1278a1310">
            <label>32</label>
            <mixed-citation id="d214e1285" publication-type="other">
Fukuda-Parr S, Greenstein J.
How should MDG
implementation be measured:
faster progress or meeting
targets? Brasilia: International
Policy Centre for Inclusive
Growth, 2010. p.1-26.</mixed-citation>
         </ref>
         <ref id="d214e1311a1310">
            <label>33</label>
            <mixed-citation id="d214e1318" publication-type="other">
Zere E, Tumusiime P, Walker 0,
et al. Inequities in utilization of
maternal health interventions in
Namibia: implications for
progress towards MDG 5 targets.
International Journal Equity
in Health 2010;9:16.</mixed-citation>
         </ref>
         <ref id="d214e1344a1310">
            <label>34</label>
            <mixed-citation id="d214e1351" publication-type="other">
Gwatkin DR, Bhuiya A, Victora
CG. Making health systems
more equitable. Lancet 2004;
364(9441): 1273-80.</mixed-citation>
         </ref>
         <ref id="d214e1368a1310">
            <label>35</label>
            <mixed-citation id="d214e1375" publication-type="other">
Overseas Development Institute.
MDG Report Card: Measuring
Progress Across Countries.
London: ODI Publications, 2011.</mixed-citation>
         </ref>
         <ref id="d214e1391a1310">
            <label>36</label>
            <mixed-citation id="d214e1398" publication-type="other">
Vandemoortele J, Delamonica E.
Taking the MDGs beyond 2015.
IDS Bulletin 2010;41(1).</mixed-citation>
         </ref>
         <ref id="d214e1411a1310">
            <label>37</label>
            <mixed-citation id="d214e1418" publication-type="other">
Kenya National Bureau of
Statistics, ICF Macro. Kenya
Demographic and Health Survey
2008-09. Calverton, MD: KNBS
and ICF Macro, 2010.</mixed-citation>
         </ref>
         <ref id="d214e1437a1310">
            <label>38</label>
            <mixed-citation id="d214e1444" publication-type="other">
National Council for Population
and Development. Kenya
Demographic and Health Survey
1993. Calverton, MD: NCPD,
CBS, and MI, 1994.</mixed-citation>
         </ref>
         <ref id="d214e1463a1310">
            <label>39</label>
            <mixed-citation id="d214e1470" publication-type="other">
Meessen B, Hercot D,
Noirhomme M, et al. Removing
user fees in the health sector
in low-income countries: a
multi-country review.
New York: UNICEF, 2009.</mixed-citation>
         </ref>
         <ref id="d214e1493a1310">
            <label>40</label>
            <mixed-citation id="d214e1500" publication-type="other">
El-Khouiy M, Gandaho T, Arur A,
et al. Improving Access to
Life-saving Maternal Health
Services: The Effects of
Removing User Fees for
Caesareans in Mali. Health
Systems 20/20, editors. Bethesda,
MD: Abt Associates Ine, 201 1.</mixed-citation>
         </ref>
         <ref id="d214e1530a1310">
            <label>41</label>
            <mixed-citation id="d214e1537" publication-type="other">
Amnesty International. Giving
Birth, Risking Death:
Maternal Mortality in Burkina
Faso. London: Amnesty
International, 2009.</mixed-citation>
         </ref>
         <ref id="d214e1556a1310">
            <label>42</label>
            <mixed-citation id="d214e1565" publication-type="other">
Ministère de la Santé. Tableau
de bord santé 2009. Direction
générale de l'information
et des statistiques sanitaires,
editor. Ouagadougou:
Ministère de la Santé du
Burkina Faso, 2010.</mixed-citation>
         </ref>
         <ref id="d214e1591a1310">
            <label>43</label>
            <mixed-citation id="d214e1598" publication-type="other">
Ridde V, Richard F, Bicaba A, et al.
The national subsidy for deliveries
and emergency obstetric care in
Burkina Faso. Health Policy and
Planning 2011. (In press)</mixed-citation>
         </ref>
         <ref id="d214e1617a1310">
            <label>44</label>
            <mixed-citation id="d214e1624" publication-type="other">
Guinée Direct. Santé: Gratuité
de la césarienne en Guinée,
entre mythes et réalités.
27 Mai 2011. At: &lt;www.
guineedirect.info/index.php?
option=com_content&amp;view=
article&amp;id=2135%3Asante-
gratuite-de-la-cesarienne-en-
guinee-entre-mythes-et-
realites&amp;catid=293%
3Apolitique&amp;Itemid=400&gt;.
Accessed 28 July 2011.</mixed-citation>
         </ref>
         <ref id="d214e1666a1310">
            <label>45</label>
            <mixed-citation id="d214e1673" publication-type="other">
Gerein N, Green A, Pearson S.
The implications of shortages
of health professionals for
maternal health in sub-saharan
Africa. Reproductive Health
Matters 2006 ; 14(27) :40-50.</mixed-citation>
         </ref>
         <ref id="d214e1696a1310">
            <label>46</label>
            <mixed-citation id="d214e1703" publication-type="other">
Thomas LS, Jina R, Tint KS, et al.
Making systems work: the hard
part of improving maternal
health services in South Africa.
Reproductive Health Matters
2007 ; 15(30) :38-49.</mixed-citation>
         </ref>
         <ref id="d214e1727a1310">
            <label>47</label>
            <mixed-citation id="d214e1734" publication-type="other">
Richard F, Witter S, De
Brouwere V. Innovative
approaches to reducing
financial barriers to obstetric
care in low-income countries.
American Journal of Public
Health 2010; 100(10): 1845-52.</mixed-citation>
         </ref>
         <ref id="d214e1760a1310">
            <label>48</label>
            <mixed-citation id="d214e1767" publication-type="other">
Commission on the Social
Determinants of Health. Closing
the gap in a generation: health
equity through action on the
social determinants of health.
Geneva: WHO, 2008.</mixed-citation>
         </ref>
         <ref id="d214e1790a1310">
            <label>49</label>
            <mixed-citation id="d214e1797" publication-type="other">
Ministère de la Santé. Guide
national de mise en oeuvre
intégrée des interventions à
gain rapide. Ouagadougou:
Ministère de la Santé du
Burkina Faso, 2009.</mixed-citation>
         </ref>
         <ref id="d214e1820a1310">
            <label>50</label>
            <mixed-citation id="d214e1827" publication-type="other">
UNAIDS. AIDS at 30, Nations
at the crossroads. Geneva:
UNAIDS, 2011.</mixed-citation>
         </ref>
         <ref id="d214e1840a1310">
            <label>51</label>
            <mixed-citation id="d214e1849" publication-type="other">
Sontag D.. Early test for US on
its gobal fight on AIDS. New
York Times. 14 July 2004. At:
&lt;www.nytimes.com/2004/07 /
14/world/early-tests-for-us-in-
its-global-fight-on-aids.html&gt;.
Accessed 26 April 2011.</mixed-citation>
         </ref>
         <ref id="d214e1875a1310">
            <label>52</label>
            <mixed-citation id="d214e1882" publication-type="other">
Van DW, Pirard M, Assefa Y,
et al. How can disease control
programs contribute to health
systems strengthening in
sub-Saharan Africa? Decoster C,
Marchai В, Soors W, et al,
editors. Working Paper Series of
the Studies in Health Services
Organisation and Policy.
Antwerp: ITG Press, 2011.</mixed-citation>
         </ref>
         <ref id="d214e1918a1310">
            <label>53</label>
            <mixed-citation id="d214e1925" publication-type="other">
Rohde J, Cousens S, Chopra M,
et al. 30 years after Alma-Ata:
has primary health care worked
in countries? Lancet 2008;
372(9642) :950-61 .</mixed-citation>
         </ref>
         <ref id="d214e1944a1310">
            <label>54</label>
            <mixed-citation id="d214e1951" publication-type="other">
World Health Organization,
UNICEF. Countdown to 2015
Decade Report (2000-2010):
taking stock of maternal,
newborn and child survival.
Washington, DC: WHO,
UNICEF, 2010.</mixed-citation>
         </ref>
         <ref id="d214e1977a1310">
            <label>55</label>
            <mixed-citation id="d214e1984" publication-type="other">
World Health Organization.
Research and development
coordination and financing:
report of the expert working
group. Geneva: WHO, 2010. At:
&lt;www.who.int/phi/documents/
RDFinancingwithISBN.pdf&gt;.
Accessed 30 July 2011.</mixed-citation>
         </ref>
         <ref id="d214e2013a1310">
            <label>56</label>
            <mixed-citation id="d214e2020" publication-type="other">
Penn-Kekana L, McPake B, et al.
Improving maternal health:
getting what works to happen.
Reproductive Health Matters
2007 ; 15(30) :28-37.</mixed-citation>
         </ref>
         <ref id="d214e2039a1310">
            <label>57</label>
            <mixed-citation id="d214e2046" publication-type="other">
Aidspan. Global Fund board
approves two health systems
funding platform projects.
4-5-2010. At: &lt;www.aidspan.org/
index.php?issue= 122&amp;article=
6&gt;. Accessed 26 April 2011.</mixed-citation>
         </ref>
         <ref id="d214e2069a1310">
            <label>58</label>
            <mixed-citation id="d214e2076" publication-type="other">
Hill PS, Vermeiren P, Miti k,
et al. The Health Systems
Funding Platform: is this where
we thought we were going?
Global Health 201 1;7:16.</mixed-citation>
         </ref>
         <ref id="d214e2096a1310">
            <label>59</label>
            <mixed-citation id="d214e2103" publication-type="other">
Gender equity is the key to
maternal and child health
[Editorial]. Lancet 2010;375:1939.</mixed-citation>
         </ref>
         <ref id="d214e2116a1310">
            <label>60</label>
            <mixed-citation id="d214e2123" publication-type="other">
National Health Insurance
Authority. The road to Ghana's
health care financing. 2011.</mixed-citation>
         </ref>
         <ref id="d214e2136a1310">
            <label>61</label>
            <mixed-citation id="d214e2143" publication-type="other">
Ooms G. The new dichotomy in
health systems strengthening
and the role of global health
initiatives: what can we learn
from Ethiopia? Journal of
Public Health Policy 2010;31:
100-09.</mixed-citation>
         </ref>
         <ref id="d214e2169a1310">
            <label>62</label>
            <mixed-citation id="d214e2178" publication-type="other">
Hayman R. Milking the Cow:
Negotiating Ownership of
Aid and Policy in Rwanda.
Global Economic Governance
Working Paper 2007/26.
Oxford: University College,
2007. At: &lt;www.
globaleconomicgovernance.org/
wp-content/uploads/hayman_
rwanda_2007-26 1 ,pdf&gt;.
Accessed 30 July 2011.</mixed-citation>
         </ref>
         <ref id="d214e2216a1310">
            <label>63</label>
            <mixed-citation id="d214e2223" publication-type="other">
Donnelly J. Ethiopia gears up
for more major health
reforms. Lancet 2011;
377(9781): 1907-08.</mixed-citation>
         </ref>
         <ref id="d214e2239a1310">
            <label>64</label>
            <mixed-citation id="d214e2246" publication-type="other">
Wisman R, Heller J, Clark P.
A blueprint for countiy-driven
development. Lancet 2011;
377(9781): 1902-03.</mixed-citation>
         </ref>
         <ref id="d214e2263a1310">
            <label>65</label>
            <mixed-citation id="d214e2270" publication-type="other">
Bhutta ZA, Ali S, Cousens S,
et al. Alma-Ata: rebirth and
revision. 6 interventions to
address maternal, newborn,
and child survival: what
difference can integrated
primary health care strategies
make? Lancet 2008;72(9642):
972-89.</mixed-citation>
         </ref>
         <ref id="d214e2302a1310">
            <label>66</label>
            <mixed-citation id="d214e2309" publication-type="other">
Riley J. Low income, social
growth, and good health.
Berkeley: University of
California Press, 2008.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
