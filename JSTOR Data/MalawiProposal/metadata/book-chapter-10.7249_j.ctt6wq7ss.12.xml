<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wq7ss</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Chinese Engagement in Africa</book-title>
         <subtitle>Drivers, Reactions, and Implications for U.S. Policy</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Hanauer</surname>
               <given-names>Larry</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Morris</surname>
               <given-names>Lyle J.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>12</day>
         <month>03</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780833084118</isbn>
      <isbn content-type="epub">9780833084149</isbn>
      <publisher>
         <publisher-name>RAND Corporation</publisher-name>
         <publisher-loc>SANTA MONICA, CA; WASHINGTON, DC; PITTSBURGH, PA; NEW ORLEANS, LA; JACKSON, MS; BOSTON, MA; DOHA, QA; CAMBRIDGE, UK; BRUSSELS, BE</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>RAND Corporation</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7249/j.ctt6wq7ss"/>
      <abstract abstract-type="short">
         <p>Examines Chinese engagement with African nations, focusing on (1) Chinese and African objectives in the political and economic spheres and how they work to achieve them, (2) African perceptions of Chinese engagement, (3) how China has adjusted its policies to accommodate African views, and (4) whether the United States and China are competing for influence, access, and resources in Africa and how they might cooperate in the region.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.2</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.3</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.4</book-part-id>
                  <title-group>
                     <title>Figures and Table</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.5</book-part-id>
                  <title-group>
                     <title>Summary</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.6</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.7</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.8</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Most analyses of Chinese engagement with African nations focus on what China gets out of these partnerships—primarily natural resources and export markets to fuel its burgeoning economy, and agricultural products to feed its increasingly urbanized population. Some studies have described the impacts—both positive and negative—that China’s aid and investment policies have had on African countries. However, few analyses have approached Sino-African relations as a vibrant, two-way dynamic in which both sides adjust to policy initiatives and popular perceptions emanating from the other.</p>
                     <p>Moreover, China’s global intentions have increasingly become the focus of debate among scholars and practitioners.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.9</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>Intertwined Interests of China and Africa</title>
                  </title-group>
                  <fpage>5</fpage>
                  <abstract>
                     <p>China and its African partners interact in a wide range of political, economic, and military-related arenas in ways that advance mutual interests, but economic pursuits lie at the heart of Sino-African relations. China seeks to acquire oil, gas, minerals, and other natural resources to fuel its economic growth, and many African countries have plentiful resource reserves whose extraction represents their primary economic output. Beijing also seeks markets for Chinese companies to sell their goods and services; consumers in many less-developed countries on the continent welcome inexpensive Chinese manufactured goods, while African governments purchase inexpensive Chinese military materiel and hire Chinese</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.10</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>How China-Africa Relations Have Developed</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>Starting in the late 1950s, Africa became central to China’s ideologically driven campaign promoting revolution, anti-colonialism, and Third World solidarity.¹ This included moral and material support for liberation movements. Relations were further consolidated when Premier Zhou Enlai visited ten African countries in 1963–1964 and articulated the “Five Principles Governing the Development of Relations with Arab and African Countries” and “Eight Principles for Economic Aid and Technical Assistance to Other Countries” that would underpin China-Africa relations going forward.² These two documents laid out the principle that relations would be governed by “equality, mutual interest and non-interference,” based on China’s “Five</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.11</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>The Impact of Chinese Engagement on African Countries</title>
                  </title-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>China’s impact on Africa has been mixed. Its investments have created jobs, developed critically needed infrastructure, and contributed to economic growth, particularly in sectors or geographic areas in which international financial institutions and Western governments and companies have been unwilling to engage. Its expenditures on education, training, and economic development have contributed positively to Africans’ standard of living and economic opportunities. Beijing’s participation in UN peacekeeping missions in Africa has contributed in concrete ways to peace and security on the continent.</p>
                     <p>That said, China’s engagement has had deleterious effects as well. Its investments and political and military support have helped</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.12</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>African Reactions to Chinese Engagement</title>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>Africans themselves have had a range of reactions to Chinese engagement in the region. African leaders and governments generally portray Chinese engagement as positive. Many have praised Chinese contributions to their nations’ infrastructure, pointing out very visible improvements that contribute to expanded economic activity, create jobs for local workers, and, in many cases, result in tangible improvements to roads, rails, bridges, and other transportation networks that benefit ordinary citizens engaged in their daily activities. Others draw attention to Chinese aid projects to highlight the ways in which their governments, thanks to Chinese cooperation, have enhanced basic services and quality of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.13</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>China Modifies Its Approach</title>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Recent actions and policy proposals issued by senior Chinese officials suggest a subtle but important shift occurring in China’s approach to the continent. The shift highlights a reality that economics and trade, long the bedrock of Sino-Africa relations, are insufficient in promoting the kind of long-term, sustainable relationship that both Africa and China desire. It also reflects an acknowledgement by Chinese elites that certain Chinese policies are producing negative perceptions among Africans and require attention from policymakers in Beijing. Going forward, issues such as soft power (media, culture, and people-to-people exchanges), health aid, sustainability, and security will occupy larger components</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.14</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>Implications for U.S. Interests</title>
                  </title-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>The United States and China actually have similar strategic interests in Africa. China, as discussed in the Chapter Two, has four overarching strategic interests in Africa: access to natural resources, particularly oil and gas; export markets; political support, particularly in international fora and adherence to its “One China” policy; and sufficient security and stability to ensure the safety of its investments and the continuation of its commercial activities.</p>
                     <p>With the exception of the “One China” policy, the United States shares these interests; it seeks access to natural resources, export markets for U.S. products, and support for policy initiatives at the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq7ss.15</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>121</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
