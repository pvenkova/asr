<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jprodanalysis</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50005590</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Productivity Analysis</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Spring Science+Business Media</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0895562X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15730441</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43549939</article-id>
         <title-group>
            <article-title>Polarization of the worldwide distribution of productivity</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Oleg</given-names>
                  <surname>Badunenko</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel J.</given-names>
                  <surname>Henderson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>R. Robert</given-names>
                  <surname>Russell</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40141044</issue-id>
         <fpage>153</fpage>
         <lpage>171</lpage>
         <permissions>
            <copyright-statement>© 2013 Springer Science+Business Media</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43549939"/>
         <abstract>
            <p>We employ data envelopment analysis (DEA) methods to construct the world production frontier, which is in turn used to decompose (labor) productivity growth into components attributable to technological change (shift of the production frontier), efficiency change (movements toward or away from the frontier), physical capital deepening, and human capital accumulation over the 1965-2007 period. Using this decomposition, we provide new findings on the causes of polarization (the emergence of bimodality) and divergence (increased variance) of the world productivity distribution. First, unlike earlier studies, we find that efficiency change is the unique driver of the emergence of a second (higher) mode. Second, while earlier studies attributed the overall change in the distribution exclusively to physical capital accumulation, we find that technological change and human capital accumulation are also significant factors explaining this change in the distribution (most notably the emergence of a long right-hand tail). Robustness exercises indicate that these revisions of earlier findings are attributable to the addition of (more recent) years and a much greater number of countries included in our sample. We also check to see whether our results are changed by a correction for the downward bias in the DEA construction of the frontier, concluding that these corrections affect none of our major findings (essentially because the level correction roughly washes out in changes.)</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1065e293a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1065e300" publication-type="other">
Duelos et al. 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e306" publication-type="other">
Quah 1997</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e313a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1065e320" publication-type="other">
Pittau et al. (2010),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e326" publication-type="other">
Bianchi (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e332" publication-type="other">
Henderson et al. (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e339a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1065e346" publication-type="other">
Kumar and Russell (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e353a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1065e360" publication-type="other">
Gong and Sickles
(1992);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e369" publication-type="other">
Bojanic et al. (1998);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e375" publication-type="other">
Cubbin and Tzanidakis (1998);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e382" publication-type="other">
Badunenko et al. (2012),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e388" publication-type="other">
Park and Lesourd (2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e396a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1065e403" publication-type="other">
Caselli (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e410a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1065e417" publication-type="other">
Basu and Weil (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e423" publication-type="other">
Los and
Timmer (2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e433a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1065e440" publication-type="other">
Sheather and Jones (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e447a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1065e454" publication-type="other">
Hall and York (2001),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e460" publication-type="other">
Henderson et al.
(2008),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e469" publication-type="other">
Silverman (1981).</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e476a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1065e483" publication-type="other">
Fan and Ullah (1999),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e489" publication-type="other">
Li (1996),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e495" publication-type="other">
Pagan
and Ullah (1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d1065e505a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1065e512" publication-type="other">
Färe et al. (1985, 1994),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e518" publication-type="other">
Charnes
et al. (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1065e527" publication-type="other">
Farrell (1957).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1065e543a1310">
            <mixed-citation id="d1065e547" publication-type="other">
Afriat SN (1972) Efficiency estimation of production functions. Int
Econ Rev 13:568-598</mixed-citation>
         </ref>
         <ref id="d1065e557a1310">
            <mixed-citation id="d1065e561" publication-type="other">
Allen RC (2012) Technology and the great divergence. Explor Econ
Hist 49(1):1-16</mixed-citation>
         </ref>
         <ref id="d1065e571a1310">
            <mixed-citation id="d1065e575" publication-type="other">
Badunenko O, Henderson DJ, Kumbhakar SC (2012) When, where
and how to perform efficiency estimation. J R Stat Soc Ser A
175(4):863-892</mixed-citation>
         </ref>
         <ref id="d1065e588a1310">
            <mixed-citation id="d1065e592" publication-type="other">
Badunenko O, Henderson DJ, Zelenyuk V (2008) Technological
change and transition: relative contributions to worldwide
growth during the 1990s. Oxf Bull Econ Stat 70(4):461-492</mixed-citation>
         </ref>
         <ref id="d1065e606a1310">
            <mixed-citation id="d1065e610" publication-type="other">
Barro RJ, Lee J-W (2010) A new data set of educational attainment in
the world, 1950-2010, NBER working papers 15902. National
Bureau of Economic Research, Inc. http://ideasrepecorg/p/nbr/
nberwo/15902html</mixed-citation>
         </ref>
         <ref id="d1065e626a1310">
            <mixed-citation id="d1065e630" publication-type="other">
Basu S, Weil DN (1998) Appropriate technology and growth. Q J
Econ 113(4):1025-1054</mixed-citation>
         </ref>
         <ref id="d1065e640a1310">
            <mixed-citation id="d1065e644" publication-type="other">
Battisti M, Parmeter C (forthcoming) Income polarization, convergence
tools and mixture analysis. J Macroecon. http://works.bepress.
com/parms/31</mixed-citation>
         </ref>
         <ref id="d1065e657a1310">
            <mixed-citation id="d1065e661" publication-type="other">
Bianchi M (1997) Testing for convergence: evidence from non-
parametric multimodality tests. J Appl Econ 12:393-409</mixed-citation>
         </ref>
         <ref id="d1065e671a1310">
            <mixed-citation id="d1065e675" publication-type="other">
Bojanic AN, Caudill SB, Ford JM (1998) Small-sample properties of
ML, COLS, and DEA estimators of frontier models in the
presence of heteroscedasticity. Eur J Oper Res 180(1):140-148</mixed-citation>
         </ref>
         <ref id="d1065e688a1310">
            <mixed-citation id="d1065e692" publication-type="other">
Brynjolfsson E, Hitt LM (2000) Beyond computation: information
technology, organizational transformation and business perfor-
mance. J Econ Perspect 14:23-48</mixed-citation>
         </ref>
         <ref id="d1065e706a1310">
            <mixed-citation id="d1065e710" publication-type="other">
Caselli F (2005) Accounting for cross-country income differences. In:
Aghion P, Durlauf S (eds) Handbook of economic growth. Vol
1A, chapter 9. Elsevier, Amsterdam, pp 679-741</mixed-citation>
         </ref>
         <ref id="d1065e723a1310">
            <mixed-citation id="d1065e727" publication-type="other">
Caselli F, Feyrer J (2007) The marginal product of capital. Q J Econ
122(2):535-568</mixed-citation>
         </ref>
         <ref id="d1065e737a1310">
            <mixed-citation id="d1065e741" publication-type="other">
Chames A, Cooper WW, Lewin AY, Seiford LM (1994) Data
envelopment analysis: theory, methodology, and application.
Kluwer Academic, Boston</mixed-citation>
         </ref>
         <ref id="d1065e754a1310">
            <mixed-citation id="d1065e758" publication-type="other">
Cubbin J, Tzanidakis G (1998) Regression versus data envelopment
analysis for efficiency measurement: an application to the England
and Wales regulated water industry. Util Policy 7(2):63-119</mixed-citation>
         </ref>
         <ref id="d1065e771a1310">
            <mixed-citation id="d1065e775" publication-type="other">
Diewert WE (1980) Capital and the theory of productivity measure-
ment. Am Econ Rev 70(2):260-267</mixed-citation>
         </ref>
         <ref id="d1065e785a1310">
            <mixed-citation id="d1065e789" publication-type="other">
Duelos J-Y, Esteban J, Ray D (2004) Polarization: concepts,
measurement, estimation. Econometrica 72:1737-1772</mixed-citation>
         </ref>
         <ref id="d1065e800a1310">
            <mixed-citation id="d1065e804" publication-type="other">
Enfio K, Hjertstrand P (2008) Relative sources of european regional
productivity convergence: a bootstrap frontier approach. Reg
Stud 42(10):1-17</mixed-citation>
         </ref>
         <ref id="d1065e817a1310">
            <mixed-citation id="d1065e821" publication-type="other">
Fan Y, Ullah A (1999) On goodness-of-fit tests for weakly dependent
processes using kernel method. J Nonparametr Stat 11(1-3):337-60</mixed-citation>
         </ref>
         <ref id="d1065e831a1310">
            <mixed-citation id="d1065e835" publication-type="other">
Färe R, Grosskopf S, Lovell CAK (1985) Production frontiers.
Kluwer-Nijhoff, Boston</mixed-citation>
         </ref>
         <ref id="d1065e845a1310">
            <mixed-citation id="d1065e849" publication-type="other">
Färe R, Grosskopf S, Lovell CAK (1994) The measurement of
efficiency of production. Cambridge University Press, New York</mixed-citation>
         </ref>
         <ref id="d1065e859a1310">
            <mixed-citation id="d1065e863" publication-type="other">
Farrell MJ (1957) The measurement of productive efficiency. J R Stat
Soc Ser A 120(3):253-290</mixed-citation>
         </ref>
         <ref id="d1065e873a1310">
            <mixed-citation id="d1065e877" publication-type="other">
Galor O (1996) Convergence? inferences from theoretical models.
Econ J 106:1056-1096</mixed-citation>
         </ref>
         <ref id="d1065e888a1310">
            <mixed-citation id="d1065e892" publication-type="other">
Gong B-H, Sickles R (1992) Finite sample evidence on the performance
of stochastic frontiers and data envelopment analysis using panel
data. J Econom 51(1-2):259-284</mixed-citation>
         </ref>
         <ref id="d1065e905a1310">
            <mixed-citation id="d1065e909" publication-type="other">
Hall P, York M (2001) On the calibration of Silverman s test for
multimodality. Stat Sin 11:515-536</mixed-citation>
         </ref>
         <ref id="d1065e919a1310">
            <mixed-citation id="d1065e923" publication-type="other">
Hall RE, Jones CI (1999) Why do some countries produce so much
more output per worker than others. Q J Econ 114(1):83-116</mixed-citation>
         </ref>
         <ref id="d1065e933a1310">
            <mixed-citation id="d1065e937" publication-type="other">
Henderson DJ, Parmeter CF, Russell RR (2008) Modes, weighted
modes and calibrated modes: evidence of clustering using
modality tests. J Appi Econ 23:607-638</mixed-citation>
         </ref>
         <ref id="d1065e950a1310">
            <mixed-citation id="d1065e954" publication-type="other">
Henderson DJ, Russell RR (2005) Human capital and convergence: a
production-frontier approach. Int Econ Rev 46(4):1167-1205</mixed-citation>
         </ref>
         <ref id="d1065e964a1310">
            <mixed-citation id="d1065e968" publication-type="other">
Henderson DJ, Zelenyuk V (2007) Testing for (efficiency) catching-
up. South Econ J 73(4):1003-1019</mixed-citation>
         </ref>
         <ref id="d1065e979a1310">
            <mixed-citation id="d1065e983" publication-type="other">
Heston A, Summers R, Aten B (2006) Penn world table version 6.2,
Center for International Comparisons of Production, Income and
Prices at the University of Pennsylvania, Philadelphia</mixed-citation>
         </ref>
         <ref id="d1065e996a1310">
            <mixed-citation id="d1065e1000" publication-type="other">
Hulten C, Wykoff FC (1996) Issues in the measurement of economic
depreciation. Econ Inq XXXIV(1):337-360</mixed-citation>
         </ref>
         <ref id="d1065e1010a1310">
            <mixed-citation id="d1065e1014" publication-type="other">
Kim J-I, Lau LJ (1994) The sources of economic growth of the East
Asian newly industrialized countries. J Jpn Int Econ 8:235-271</mixed-citation>
         </ref>
         <ref id="d1065e1024a1310">
            <mixed-citation id="d1065e1028" publication-type="other">
Kneip A, Simar L, Wilson PW (2008) Asymptotics for DEA
estimators in non-parametric frontier models. Econ Theory
24:1663-1697</mixed-citation>
         </ref>
         <ref id="d1065e1041a1310">
            <mixed-citation id="d1065e1045" publication-type="other">
Koopmans TC (1951) Activity analysis of production and allocation.
Physica-Verlag, Heidelberg</mixed-citation>
         </ref>
         <ref id="d1065e1055a1310">
            <mixed-citation id="d1065e1059" publication-type="other">
Kumar S, Russell RR (2002) Technological change, technological
catch-up, and capital deepening: relative contributions to growth
and convergence. Am Econ Rev 92(3):527-548</mixed-citation>
         </ref>
         <ref id="d1065e1073a1310">
            <mixed-citation id="d1065e1077" publication-type="other">
Li Q (1996) Nonparametric testing of closeness between two
unknown distribution functions. Econ Rev 15:261-274</mixed-citation>
         </ref>
         <ref id="d1065e1087a1310">
            <mixed-citation id="d1065e1091" publication-type="other">
Los B, Timmer MP (2005) The appropriate technology explanation of
productivity growth differentials: an empirical approach. J Dev
Econ 77(2):517-531</mixed-citation>
         </ref>
         <ref id="d1065e1104a1310">
            <mixed-citation id="d1065e1108" publication-type="other">
Pagan A, Ullah A (1999) Nonparametric econometics. Cambridge
University Press, Cambridge</mixed-citation>
         </ref>
         <ref id="d1065e1118a1310">
            <mixed-citation id="d1065e1122" publication-type="other">
Park S-U, Lesourd J-B (2000) The efficiency of conventional fuel
power plants in South Korea: a comparison of parametric and
non-parametric approaches. Int J Prod Econ 63(1):59-67</mixed-citation>
         </ref>
         <ref id="d1065e1135a1310">
            <mixed-citation id="d1065e1139" publication-type="other">
Pittau MG, Zelli R, Johnson PA (2010) Mixture models, convergence
clubs, and polarization. Rev Income Wealth 56(1):102-122</mixed-citation>
         </ref>
         <ref id="d1065e1149a1310">
            <mixed-citation id="d1065e1153" publication-type="other">
Psacharopoulos G (1994) Returns to investment in education: a global
update. World Dev 22:1325-1343</mixed-citation>
         </ref>
         <ref id="d1065e1164a1310">
            <mixed-citation id="d1065e1168" publication-type="other">
Quah D (1993) Galton' s fallacy and tests of the convergence
hypothesis. Scand J Econ 95(4):427-443</mixed-citation>
         </ref>
         <ref id="d1065e1178a1310">
            <mixed-citation id="d1065e1182" publication-type="other">
Quah D (1996) Twin peaks: growth and convergence in models of
distribution dynamics. Econ J 106(437):1045-1055</mixed-citation>
         </ref>
         <ref id="d1065e1192a1310">
            <mixed-citation id="d1065e1196" publication-type="other">
Quah D (1997) Empirics for growth and distribution: stratification,
polarization, and convergence clubs. J Econ Growth 2(1):27-59</mixed-citation>
         </ref>
         <ref id="d1065e1206a1310">
            <mixed-citation id="d1065e1210" publication-type="other">
Sheather SJ, Jones MC (1991) A reliable data based bandwidth
selection method for kernel density estimation. J R Stat Soc Ser
B 53:683-990</mixed-citation>
         </ref>
         <ref id="d1065e1223a1310">
            <mixed-citation id="d1065e1227" publication-type="other">
Silverman BW (1981) Using kernel density estimates to investigate
multimodality. J R Stat Soc Ser B 43:97-99</mixed-citation>
         </ref>
         <ref id="d1065e1237a1310">
            <mixed-citation id="d1065e1241" publication-type="other">
Simar L, Wilson PW (1998) Sensitivity analysis of efficiency scores:
how to bootstrap in nonparametric frontier models. Manag Sci
44:49-61</mixed-citation>
         </ref>
         <ref id="d1065e1255a1310">
            <mixed-citation id="d1065e1259" publication-type="other">
Simar L, Wilson PW (2000) A general methodology for bootstrapping
in non-parametric frontier models. J Appl Stat 27(6):779-802</mixed-citation>
         </ref>
         <ref id="d1065e1269a1310">
            <mixed-citation id="d1065e1273" publication-type="other">
Young A (1995) The tyranny of numbers: confronting the statistical
realities of the East Asian growth experience. Q J Econ 110(3):
641-680</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
