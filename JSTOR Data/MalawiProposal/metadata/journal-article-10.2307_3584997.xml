<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">nbermacrannu</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100793</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>NBER Macroeconomics Annual</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>MIT Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08893365</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15372642</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2307/3584997</article-id>
         <title-group>
            <article-title>The Debt Crisis: A Postmortem</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel</given-names>
                  <surname>Cohen</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i284970</issue-id>
         <fpage>65</fpage>
         <lpage>105</lpage>
         <page-range>65-105</page-range>
         <permissions>
            <copyright-statement>Copyright 1992 The National Bureau of Economic Research and The Massachusetts Institute of Technology</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3584997"/>
         <abstract>
            <p>In the first part of the paper, I calculate the returns on the developing countries debt obtained by their (private and public) creditors when taking account of the transfers already generated, and of the liquidative value of the debt. I show that they are good. I then evaluate the conflict of interest between private and public creditors and assess the role of the Brady deal as a vehicle for bringing about a "grand settlement" of the debt crisis. I argue that they are not as good. In the second part of the paper, I show that the group of reschedulers did experience lower growth in the 1980s, but I also show that their rate of capital accumulation was not lifted up in the years before the debt crisis. I evaluate the extent to which sovereign risk, rather than low returns, explains the failure of foreign finance to speed up capital accumulation in the large debtor countries.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d183e120a1310">
            <mixed-citation id="d183e124" publication-type="journal">
Barro, R., and X. Sali-i-Martin. (1991). Convergence across states and regions.
Brookings Papers on Economic Activity No. 1.<object-id pub-id-type="doi">10.2307/2534639</object-id>
            </mixed-citation>
         </ref>
         <ref id="d183e137a1310">
            <mixed-citation id="d183e141" publication-type="book">
---, and ---. (1990). Economic growth and convergence across the United
States. NBER Working Paper No. 3419.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Barro</surname>
                  </string-name>
               </person-group>
               <source>Economic growth and convergence across the United States</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d183e166a1310">
            <mixed-citation id="d183e170" publication-type="other">
Bartollini, L., and A. Dixit. (forthcoming). Market valuation of illiquid debt and
implications for conflict among creditors. IMF Staff Papers.</mixed-citation>
         </ref>
         <ref id="d183e180a1310">
            <mixed-citation id="d183e184" publication-type="journal">
Bulow, J., and K. Rogoff. (1991). Debt buy-back: No cure for debt overhang.
Quarterly Journal of Economics.<object-id pub-id-type="doi">10.2307/2937962</object-id>
            </mixed-citation>
         </ref>
         <ref id="d183e198a1310">
            <mixed-citation id="d183e202" publication-type="journal">
---, and ---. (1989). LDC Debt: Is to forgive to forget? American Economic
Review March.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bulow</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <source>American Economic Review</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e231a1310">
            <mixed-citation id="d183e235" publication-type="book">
Cardoso, E., and R. Dornbusch. (1989). Brazilian debt crises: Past and present.
In International Debt Crisis in Historical Perspectives. B. Eichengreen and P. Lin-
dert (eds.). Cambridge, MA: MIT Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cardoso</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Brazilian debt crises: Past and present</comment>
               <source>International Debt Crisis in Historical Perspectives</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e267a1310">
            <mixed-citation id="d183e271" publication-type="book">
Claessens, S., and S. Van Wijnbergen. (1990). Secondary market prices under
alternative debt reduction strategies: An option pricing approach with an
application to Mexico. Working paper, World Bank.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Claessens</surname>
                  </string-name>
               </person-group>
               <source>Secondary market prices under alternative debt reduction strategies: An option pricing approach with an application to Mexico</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d183e300a1310">
            <mixed-citation id="d183e304" publication-type="other">
Cohen, D. (forthcoming). A valuation formula for LDC debt. Journal of Interna-
tional Economics.</mixed-citation>
         </ref>
         <ref id="d183e314a1310">
            <mixed-citation id="d183e318" publication-type="other">
---. (forthcoming). Low investment and large LDC debt in the eighties.
American Economic Review.</mixed-citation>
         </ref>
         <ref id="d183e328a1310">
            <mixed-citation id="d183e332" publication-type="book">
---. (1991a). Private Lending to Sovereign States: A Theoretical Autopsy. Cam-
bridge, MA: MIT Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cohen</surname>
                  </string-name>
               </person-group>
               <source>Private Lending to Sovereign States: A Theoretical Autopsy</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d183e358a1310">
            <mixed-citation id="d183e362" publication-type="other">
---. (1991b). Tests of the convergence hypothesis: A critical note.
CEPREMAP, mimeo.</mixed-citation>
         </ref>
         <ref id="d183e372a1310">
            <mixed-citation id="d183e376" publication-type="book">
---, and R. Portes. (1990). The price of LDC debt. CEPR Discussion Paper
459.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cohen</surname>
                  </string-name>
               </person-group>
               <source>The price of LDC debt</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d183e401a1310">
            <mixed-citation id="d183e405" publication-type="book">
---, and T. Verdier. (1990). "Secret" buy-backs of LDC debt. CEPR WP no.
462.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cohen</surname>
                  </string-name>
               </person-group>
               <source>"Secret" buy-backs of LDC debt</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d183e430a1310">
            <mixed-citation id="d183e434" publication-type="book">
Diwan, I., and K. Kletzer. (1990). Voluntary choices in concerted deal. World
Bank PPR, Working Paper No. 527.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Diwan</surname>
                  </string-name>
               </person-group>
               <source>Voluntary choices in concerted deal</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d183e459a1310">
            <mixed-citation id="d183e463" publication-type="journal">
Dooley, M. (1988). Buy-backs and the market valuation of external debt. IMF
Staff Papers 35:215-229.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Dooley</surname>
                  </string-name>
               </person-group>
               <fpage>215</fpage>
               <volume>35</volume>
               <source>IMF Staff Papers</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d183e495a1310">
            <mixed-citation id="d183e499" publication-type="book">
Eaton, J. (1991). Sovereign debt: A primer. Mimeo. The World Bank.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eaton</surname>
                  </string-name>
               </person-group>
               <source>Sovereign debt: A primer</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d183e522a1310">
            <mixed-citation id="d183e526" publication-type="journal">
---, M. Gersovitz, and J. Stiglitz. (1986). The pure theory of country risk.
European Economic Review 30:481-513.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eaton</surname>
                  </string-name>
               </person-group>
               <fpage>481</fpage>
               <volume>30</volume>
               <source>European Economic Review</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d183e558a1310">
            <mixed-citation id="d183e562" publication-type="book">
Eichengreen, B., and P. Lindert (eds.). (1989). The International Debt Crisis in
Historical Perspective. Cambridge, MA: MIT Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eichengreen</surname>
                  </string-name>
               </person-group>
               <source>The International Debt Crisis in Historical Perspective</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e587a1310">
            <mixed-citation id="d183e591" publication-type="book">
---, and R. Portes. (1989). After the deluge: Default, negotiation, and read-
justment during the interwar years. In The International Debt Crisis in Historical
Perspectives. B. Eichengreen and P. Lindert (eds.).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eichengreen</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">After the deluge: Default, negotiation, and readjustment during the interwar years</comment>
               <source>The International Debt Crisis in Historical Perspectives</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e623a1310">
            <mixed-citation id="d183e627" publication-type="journal">
---, and ---. (1986). Debt and default in the 1930s: Causes and conse-
quences. European Economic Review 30:599-640.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eichengreen</surname>
                  </string-name>
               </person-group>
               <fpage>599</fpage>
               <volume>30</volume>
               <source>European Economic Review</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d183e659a1310">
            <mixed-citation id="d183e663" publication-type="book">
---, and ---. (1989). Debt and default in the 1930s and the 1980s.; In
Dealing with the Debt Crisis. M. Husain and I. Diwan (eds.). Washington, DC:
The World Bank.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eichengreen</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Debt and default in the 1930s and the 1980s</comment>
               <source>Dealing with the Debt Crisis</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e695a1310">
            <mixed-citation id="d183e699" publication-type="book">
Fernandez, R., and S. Ozler. (1991). Debt concentration and secondary market
prices: a theoretical and empirical analysis. NBER WP No. 3654.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Fernandez</surname>
                  </string-name>
               </person-group>
               <source>Debt concentration and secondary market prices: a theoretical and empirical analysis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d183e725a1310">
            <mixed-citation id="d183e729" publication-type="journal">
Financial Flows to Developing Countries (Dec. 1991). Debt and International Fi-
nance Division, Washington, DC: The World Bank.<issue>Dec.</issue>
               <source>Financial Flows to Developing Countries</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d183e748a1310">
            <mixed-citation id="d183e752" publication-type="book">
Husain, M., and I. Diwan. (1989). Dealing with the Debt Crisis. Washington, DC:
The World Bank.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Husain</surname>
                  </string-name>
               </person-group>
               <source>Dealing with the Debt Crisis</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e777a1310">
            <mixed-citation id="d183e781" publication-type="book">
Jorgensen, E., and J. Sachs. (1989). Default and renegotiation of Latin American
foreign bonds in the interwar period. In International Debt Crisis in Historical
Perspectives. B. Eichengreen and P. Lindert (eds.). Cambridge, MA: MIT Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Jorgensen</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Default and renegotiation of Latin American foreign bonds in the interwar period</comment>
               <source>International Debt Crisis in Historical Perspectives</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e813a1310">
            <mixed-citation id="d183e817" publication-type="journal">
Krugman, P. (1988). Financing vs forgiving a debt overhang: Some analytical
notes. Journal of Development Economics 29:253-268.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Krugman</surname>
                  </string-name>
               </person-group>
               <fpage>253</fpage>
               <volume>29</volume>
               <source>Journal of Development Economics</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d183e849a1310">
            <mixed-citation id="d183e853" publication-type="other">
. (1991). International financial integration and economic development.
Mimeo.</mixed-citation>
         </ref>
         <ref id="d183e863a1310">
            <mixed-citation id="d183e867" publication-type="book">
Lindert, P. (1989). Response to the debt crisis: What is different about the 1980s?
In International Debt Crisis in Historical Perspectives. B. Eichengreen and P. Lind-
ert (eds.). Cambridge, MA: MIT Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lindert</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Response to the debt crisis: What is different about the 1980s</comment>
               <source>International Debt Crisis in Historical Perspectives</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e900a1310">
            <mixed-citation id="d183e904" publication-type="book">
---, and P. Morton. (1989). How Sovereign Debt Has Worked. In Sachs, J.
(ed.): Developing Country Debt and Macroeconomic Performance. Chicago: Univer-
sity of Chicago Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lindert</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">How Sovereign Debt Has Worked</comment>
               <source>Developing Country Debt and Macroeconomic Performance</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e936a1310">
            <mixed-citation id="d183e940" publication-type="journal">
Lucas, R. (1990). Why can't capital flow from rich to poor countries? American
Economic Review, papers and proceedings.<object-id pub-id-type="jstor">10.2307/2006549</object-id>
            </mixed-citation>
         </ref>
         <ref id="d183e953a1310">
            <mixed-citation id="d183e957" publication-type="journal">
Mankiw, G., D. Romer, and D. Weil. (1992). A contribution to the empirics of
economic growth. Quarterly Journal of Economics, May, vol. 57, no. 2, pp.
407-43.<object-id pub-id-type="doi">10.2307/2118477</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d183e976a1310">
            <mixed-citation id="d183e980" publication-type="book">
Marcet, A., and R. Marimon. (1991). Communication, commmitment and
growth. Universitat Pompeu Fabra.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Marcet</surname>
                  </string-name>
               </person-group>
               <source>Communication, commmitment and growth</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d183e1005a1310">
            <mixed-citation id="d183e1009" publication-type="journal">
Ozler, S. (1989). On the relation between reschedulings and bank value. Ameri-
can Economic Review, vol. 79, no. 5, pp. 1117-1135.<object-id pub-id-type="jstor">10.2307/1831440</object-id>
               <fpage>1117</fpage>
            </mixed-citation>
         </ref>
         <ref id="d183e1025a1310">
            <mixed-citation id="d183e1029" publication-type="other">
---. (forthcoming). Have commercial banks ignored history? American Eco-
nomic Review.</mixed-citation>
         </ref>
         <ref id="d183e1040a1310">
            <mixed-citation id="d183e1044" publication-type="book">
---, and H. Huizinga. (1992). Bank exposure, capital and secondary market
discounts on developing countries debt. NBER WP 3961.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ozler</surname>
                  </string-name>
               </person-group>
               <source>Bank exposure, capital and secondary market discounts on developing countries debt</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d183e1069a1310">
            <mixed-citation id="d183e1073" publication-type="book">
Sachs, J. (1988). The debt overhang of developing countries. In Debt, Growth and
Stabilization: Essays in Memory of Carlos Dias Alejandro. J. de Macedo and R.
Findlay (eds.). Oxford: Blackwell.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Sachs</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The debt overhang of developing countries</comment>
               <source>Debt, Growth and Stabilization: Essays in Memory of Carlos Dias Alejandro</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d183e1105a1310">
            <mixed-citation id="d183e1109" publication-type="book">
---. (ed.). (1989). Developing Country Debt and Macroeconomic Performance. Chi-
cago: University of Chicago Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Sachs</surname>
                  </string-name>
               </person-group>
               <source>Developing Country Debt and Macroeconomic Performance</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d183e1134a1310">
            <mixed-citation id="d183e1138" publication-type="journal">
---,, and H. Huizinga (1987). "U.S. Commercial Banks and the Developing
Country Debt Crisis."Brookings Papers on Economic Activity, 2.<object-id pub-id-type="doi">10.2307/2534490</object-id>
               <fpage>2</fpage>
            </mixed-citation>
         </ref>
         <ref id="d183e1154a1310">
            <mixed-citation id="d183e1158" publication-type="journal">
Summers, R., and A. Heston. (1988). A new set of international comparisons
of real product and price levels. The Review of Income and Wealth March:1-25.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Summers</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>1</fpage>
               <source>The Review of Income and Wealth</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d183e1190a1310">
            <mixed-citation id="d183e1194" publication-type="book">
Van Wijnbergen, S. (1990). Mexico's external debt restructuring in 1989/90: An
economic analysis. Economic Policy.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Van Wijnbergen</surname>
                  </string-name>
               </person-group>
               <source>Mexico's external debt restructuring in 1989/90: An economic analysis</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d183e1220a1310">
            <mixed-citation id="d183e1224" publication-type="journal">
Warner, A. (1991). Did the debt crisis cause the investment crisis? Quarterly
Journal of Economics.<object-id pub-id-type="doi">10.2307/2118384</object-id>
            </mixed-citation>
         </ref>
         <ref id="d183e1237a1310">
            <mixed-citation id="d183e1241" publication-type="other">
World Debt Tables. (1990). Washington, DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d183e1248a1310">
            <mixed-citation id="d183e1252" publication-type="other">
World Debt Tables (1989) and (1991). External debt of developing countries. Wash-
ington, DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d183e1262a1310">
            <mixed-citation id="d183e1266" publication-type="book">
Young, A. (1992). A tale of two cities: Factor accumulation and technical change
in Hong Kong and Singapore. In Macroeconomics Annual 1992. Cambridge,
MA: MIT Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A tale of two cities: Factor accumulation and technical change in Hong Kong and Singapore</comment>
               <source>Macroeconomics Annual 1992</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
