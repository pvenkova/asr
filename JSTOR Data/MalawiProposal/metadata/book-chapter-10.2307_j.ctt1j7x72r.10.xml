<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1j7x72r</book-id>
      <subj-group>
         <subject content-type="call-number">PR9340.5.C36 2011</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African literature (English)</subject>
         <subj-group>
            <subject content-type="lcsh">History and criticism</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ecology in literature</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">In literature</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ecology</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ecocriticism</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Language &amp; Literature</subject>
         <subject>Political Science</subject>
         <subject>Biological Sciences</subject>
         <subject>Sociology</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Environment at the Margins</book-title>
         <subtitle>Literary and Environmental Studies in Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Caminero-Santangelo</surname>
               <given-names>Byron</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Myers</surname>
               <given-names>Garth</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>08</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="epub">9780821444245</isbn>
      <isbn content-type="epub">0821444247</isbn>
      <publisher>
         <publisher-name>Ohio University Press</publisher-name>
         <publisher-loc>ATHENS</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>Ohio University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1j7x72r"/>
      <abstract abstract-type="short">
         <p>
            <italic>Environment at the Margins</italic>brings literary and environmental studies into a robust interdisciplinary dialogue, challenging dominant ideas about nature, conservation, and development in Africa and exploring alternative narratives offered by writers and environmental thinkers. The essays bring together scholarship in geography, anthropology, and environmental history with the study of African and colonial literatures and with literary modes of analysis. Contributors analyze writings by colonial administrators and literary authors, as well as by such prominent African activists and writers as Ngugi wa Thiong'o, Mia Couto, Nadine Gordimer, Wangari Maathai, J. M. Coetzee, Zakes Mda, and Ben Okri. These postcolonial ecocritical readings focus on dialogue not only among disciplines but also among different visions of African environments. In the process,<italic>Environment at the Margins</italic>posits the possibility of an ecocriticism that will challenge and move beyond marginalizing, limiting visions of an imaginary Africa. Contributors:Jane CarruthersMara GoldmanAmanda HammarJonathan HighfieldDavid McDermott HughesRoderick P. NeumannRob NixonAnthony VitalLaura Wright</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Caminero-Santangelo</surname>
                           <given-names>Byron</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Myers</surname>
                           <given-names>Garth A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>IN EARLY June 2009, Shell Oil Corporation agreed to pay more than fifteen million U.S. dollars to a group of ten Nigerian plaintiffs, most prominently the son of writer Ken Saro-Wiwa. The plaintiffs had accused Shell Oil of collaborating with the Nigerian military in the 1995 execution of Saro-Wiwa and eight other leaders of the Movement for the Survival of the Ogoni Peoples (MOSOP). MOSOP and Saro-Wiwa had tied their nonviolent advocacy for human rights in Ogoniland to highlighting the oil industry’s devastating impacts on the ecosystem of their Niger Delta homeland. Although Shell Oil still dismissed the charges, its</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.5</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>“A Beautiful Country Badly Disfigured”:</title>
                     <subtitle>Enframing and Reframing Eric Dutton’s The Basuto of Basutoland</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Myers</surname>
                           <given-names>Garth A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>22</fpage>
                  <abstract>
                     <p>THIS CHAPTER is an analysis of Eric Dutton’s 1925 book<italic>The Basuto of Basutoland</italic>.¹ I use Timothy Mitchell’s concept in<italic>Colonising Egypt</italic>of an enframing colonial discourse, in combination with other theoretical insights, to analyze the book.² Dutton, who later worked as an administrator in four British African colonies, orchestrated the construction of Lusaka as the capital of the future Zambia, and authored four other books, began his colonial career in today’s Lesotho in 1918–19. He used the experience as the basis for writing this text, mostly while recovering from multiple surgeries that followed his severe injury during World</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.6</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>“Through the Pleistocene”:</title>
                     <subtitle>Nature and Race in Theodore Roosevelt’s African Game Trails</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Neumann</surname>
                           <given-names>Roderick P.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>ON A summery March morning in 1909, former president Theodore Roosevelt stood on the deck of the German ocean liner<italic>Hamburg</italic>as preparations for its departure from the Hoboken, New Jersey, pier were completed. In the crush of thousands assembled to see him off, the gilt buttons had been cut from his Rough Rider overcoat, and his hat had been knocked from his head. He waved to the raucous crowd of well-wishers one last time before ducking into his specially fitted “imperial suite.”¹ His son Kermit, three Smithsonian scientists, a double-barreled Holland &amp; Holland rifle (dubbed the “Big Stick”), and two</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.7</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>“Hunter of Elephants, Take Your Bow!”</title>
                     <subtitle>A Historical Analysis of Nonfiction Writing about Elephant Hunting in Southern Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Carruthers</surname>
                           <given-names>Jane</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>THE PURPOSE of environmental history is to probe the nexus between humans and nature (the environment). Like all historical studies, environmental history relies on the critical evaluation of sources, usually but not exclusively the written word. Moran asserts that literary studies and history have had a close but problematic relationship, and this may well be evident in what follows.¹ This chapter is based on a selection of nonfiction relating to elephants in southern Africa produced over the course of more than a century.² Considering how hunting, killing, and managing elephants has been expressed in nonfiction elucidates changing attitudes toward this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.8</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Keeping the Rhythm, Encouraging Dialogue, and Renegotiating Environmental Truths:</title>
                     <subtitle>Writing in the Oral Tradition of a Maasai Enkiguena</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Goldman</surname>
                           <given-names>Mara</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>UNDERSTANDING CHANGE and continuity in African environments has always involved storytelling. As scholars of African history, anthropology, and geography have well illustrated, history has shown a disproportionate privileging of a particular kind of story: the heroic tales of Europeans and Americans exploring, studying, managing, and otherwise “knowing” Africa. The narratives inspired by such adventure-cum-science kept African people and African voices out of the story despite the reliance of many of the authors on African informants, African laborers, and sometimes African writers, intellectuals, and politicians for the stories they were transcribing for a global audience.¹ As African historians have argued, the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.9</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>Sleepwalking Lands:</title>
                     <subtitle>Literature and Landscapes of Transformation in Encounters with Mia Couto</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hammar</surname>
                           <given-names>Amanda</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>At the heart of this chapter is an exploration of the ways in which words—mostly written words but also language more broadly and fiction and poetry more specifically—engage with the world and how the world (human and nonhuman) in turn “speaks” and thus asserts and (re)creates itself. Related to this, the chapter reflects on the kinds of naturalized boundaries inherent in traditional scientific modes of knowing and representation that more intimate subjective modes can productively challenge. Such naturalized boundaries have produced<italic>un</italic>natural dichotomies between, for example, object and subject, nature and culture, science and art, outer and inner</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.10</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>No Longer Praying on Borrowed Wine:</title>
                     <subtitle>Agroforestry and Food Sovereignty in Ben Okri’s Famished Road Trilogy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Highfield</surname>
                           <given-names>Jonathan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>141</fpage>
                  <abstract>
                     <p>IN<italic>THE Wretched of the Earth</italic>, Frantz Fanon writes that “The relations of man with matter, with the world outside, and with history are in the colonial period simply relations with food.”¹ By this Fanon means that existence itself is so threatened that every bit of food that a person can gain access to is “a victory felt as a triumph for life” (308). He insists that “Independence is not a word which can be used as an exorcism, but an indispensable condition for the existence of men and women who are truly liberated, in other words who are truly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.11</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Whites Lost and Found:</title>
                     <subtitle>Immigration and Imagination in Savanna Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hughes</surname>
                           <given-names>David McDermott</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>Sympathetic authors frequently describe African whites as a “lost tribe.”¹ The phrase suggests a population marooned, wandering, or scattered (from Israel) or otherwise out of step with its surroundings. Indeed, in this metaphorical sense, Europeans partly failed as settlers and immigrants to Africa in the twentieth century. Of course, at various times and places they nearly monopolized power, wealth, and/or land. But in the realm of ideas, few could convince themselves and others that they belonged. Barronness Blixen, under the pen name Isak Dinesen, wrote with unmatched certainty when she declared, “Here I am, where I ought to be.” (If</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.12</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Waste and Postcolonial History:</title>
                     <subtitle>An Ecocritical Reading of J. M. Coetzee’s Age of Iron</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Vital</surname>
                           <given-names>Anthony</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>185</fpage>
                  <abstract>
                     <p>IF ECOCRITICISM in the global North began with focus on the North’s tradition of “Nature writing” and its associated environmental concerns, it has since reached into the North-South divide to articulate a more historically informed sense of nature and human need, with Rob Nixon’s “Environmentalism and Postcolonialism” (2005) cited frequently as a marker of this shift.¹ Such ecocriticism, as it reaches into a global world, has to take into account this world’s roots in European colonialism, with the accompanying social displacements, expropriations, and enslavements as well as the many forms of resistance, accommodation, and transformation that colonial activities have generated.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.13</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>Never a Final Solution:</title>
                     <subtitle>Nadine Gordimer and the Environmental Unconscious</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Caminero-Santangelo</surname>
                           <given-names>Byron</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>213</fpage>
                  <abstract>
                     <p>IN HIS seminal work<italic>Justice, Nature &amp; the Geography of Difference</italic>, the geographer David Harvey draws on the gap between Raymond Williams’s cultural theory and the treatment of the concepts of “place, space, and environment” in his novels in order to emphasize “the difficulty in getting this tripartite conceptual apparatus into the heart of cultural theory.”¹ The challenge in working with such an “apparatus” is to negotiate among different “kinds and levels of abstraction” (42): place and space, the local and the global, social processes and environment. Harvey claims that Williams chooses the novel “as a vehicle to explore these themes”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.14</book-part-id>
                  <title-group>
                     <label>Chapter 10</label>
                     <title>Inventing Tradition and Colonizing the Plants:</title>
                     <subtitle>Ngugi wa Thiong’o’s Petals of Blood and Zakes Mda’s The Heart of Redness</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wright</surname>
                           <given-names>Laura</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>235</fpage>
                  <abstract>
                     <p>IN HIS famous coauthored study<italic>The Invention of Tradition</italic>, historian Eric Hobsbawm argues that “‘traditions’ which appear or claim to be old are often quite recent in origin and sometimes invented.” He goes on to state that “‘Invented tradition’ is taken to mean a set of practices, normally governed by overtly or tacitly accepted rules and of a ritual or symbolic nature, which seek to inculcate certain values and norms of behavior by repetition, which automatically implies continuity with the past.”¹</p>
                     <p>Through repetition and lack of variation, Hobsbawm asserts, ritual becomes codified as tradition in that it ultimately becomes linked</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.15</book-part-id>
                  <title-group>
                     <label>Chapter 11</label>
                     <title>Slow Violence, Gender, and the Environmentalism of the Poor</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nixon</surname>
                           <given-names>Rob</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>257</fpage>
                  <abstract>
                     <p>WE ARE accustomed to conceiving of violence in terms that are immediate, explosive, and spectacular, as erupting into instant, concentrated visibility. But as environmentalists, we need to engage the representational and strategic challenges posed by the relative invisibility of what I call slow violence, a violence that is neither spectacular nor instantaneous but instead is incremental, as its calamitous repercussions are postponed across a range of temporal scales.¹ In so doing, we can complicate conventional assumptions about violence as a highly visible act that is newsworthy because it is event focused, time bound, and targeted at a specific body or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.16</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>287</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x72r.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>291</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
