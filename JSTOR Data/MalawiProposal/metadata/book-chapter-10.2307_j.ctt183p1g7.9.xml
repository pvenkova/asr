<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt183p1g7</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Elite Transition - Revised and Expanded Edition</book-title>
         <subtitle>From Apartheid to Neoliberalism in South Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Bond</surname>
               <given-names>Patrick</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745334776</isbn>
      <isbn content-type="epub">9781783711444</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Patrick Bond</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt183p1g7"/>
      <abstract abstract-type="short">
         <p>Released to coincide with the 20th anniversary of the end of apartheid, this is an updated edition of a best-selling work of political analysis. Patrick Bond, a former adviser to the ANC, investigates how groups such as the ANC went from being a force of liberation to a vehicle now perceived as serving the economic interests of an elite few. This edition includes new analysis looking at the 2008 internal coup against Thabo Mbeki, the subsequent economic crisis and the massacre of miners at Marikana in 2012. Bond also assesses the historiography of the transition written since 2000 from nationalist, liberal and radical perspectives, and replies to critics of his work, both from liberal and nationalist perspectives. This is an essential text on post-Apartheid South Africa, which will be vital reading for all who study or have an interest in this part of the continent, and in social change and neoliberal public policy more generally.</p>
      </abstract>
      <counts>
         <page-count count="352"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.3</book-part-id>
                  <title-group>
                     <title>List of Acronyms and Abbreviations</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.4</book-part-id>
                  <title-group>
                     <title>INTRODUCTION:</title>
                     <subtitle>Dissecting South Africa’s Transition</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This book aims to fill some gaps in the literature about South Africa’s late twentieth-century democratisation. There is already an abundance of commentary on the years of liberation struggle and particularly on the period 1990–94 – empiricist accounts, academic tomes, self-serving biographies – and many more narratives have been and are being drafted about the power-sharing arrangements that followed the April 1994 election, as well as the record of the ANC in its first term.</p>
                     <p>Some of these have been penned by progressives and are generally critical of the course the transition has taken thus far. In the development</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Neoliberal Economic Constraints on Liberation</title>
                  </title-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>The unbanning of South Africa’s liberation organisations and release of Nelson Mandela in February 1990 provided a moment of uncertainty – perhaps five or six years’ duration – when, it seemed to most observers, nearly any kind of political-economic future was possible. The existence of fluidity within and around the ANC heightened the country’s already intense ideological and factional struggles. There was little doubt that an overhaul of the country’s notoriously inefficient, skewed and stagnant economy was in store, but what forces would set the parameters during the crucial first half of the decade were by no means evident.</p>
                     <p>It</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Social Contract Scenarios</title>
                  </title-group>
                  <fpage>42</fpage>
                  <abstract>
                     <p>With neoliberalism failing to deliver the goods throughout the early 1990s, it should have been easy to marshall a broad alliance within the ANC against the inherited macroeconomic policy. That the core ANC leaders held firm in favour of more neoliberalism during the mid-1990s suggests an extraordinary disjuncture with the past. Understanding this disjuncture requires delving beyond issues of ‘structure’ (the balance of forces in the economy and society) and into the particular way in which ‘agency’ (ANC leadership) was shaped.</p>
                     <p>This chapter examines one of the most important<italic>modus operandi</italic>– certainly the best documented and most telling of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Rumours, Dreams and Promises</title>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>The<italic>RDP</italic>became official ANC policy in January 1994 due in large part to the initiative of Cosatu (led by Jay Naidoo), supported by key figures of the SACP, the broad ANC Left and ANC-oriented social movements and NGOs – the Mass Democratic Movement (MDM). To illustrate: in a deal brokered between Nelson Mandela and Moses Mayekiso in November 1993, Sanco endorsed the ANC for the 1994 election in exchange for the integration of the Sanco housing and economic development policy into the<italic>RDP</italic>.¹ The final draft of the<italic>RDP</italic>booklet appeared in early March, and as a result of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Housing Question</title>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>Whether looking at housing from bottom up or top down, this is perhaps the most important component of social policy to get right.¹ The employment potential, substantial macroeconomic multipliers and relatively low import cost together mean that housing is well suited to play a central role in any progressive economic development strategy. From the standpoint of the household – particularly women caregivers – decent housing improves family health and hygiene, provides privacy and a chance to raise children, and ensures the psycho-logical security that comes from ‘tenure’ (the ability to stay in a house without fear of being displaced). Finally,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The World Bank as ‘Knowledge Bank’ (sic)</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>The contradictions between expectations and reality, in relation to democratic South Africa’s relations with the World Bank, were unveiled in this 1992<italic>Business Day</italic>report:</p>
                     <p>A questioner from the floor of the World Bank’s final function at the annual meetings asked Bank president Lewis Preston whether this year’s meetings had been any different because of the absence of socialists following the collapse of the command economies. ‘There are still some socialists here,’ Preston replied. ‘There are still even some communists around. But they are talking in very low voices, and they are mostly South Africans.’¹</p>
                     <p>Offhand wit aside, the reason</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Beyond Neoliberalism?</title>
                     <subtitle>South Africa and Global Economic Crisis</subtitle>
                  </title-group>
                  <fpage>150</fpage>
                  <abstract>
                     <p>There are many reasons why, in the wake of the 1999 election, South Africa was ripe for dramatic turns of phrase, if not policy, on the political and economic fronts. With respect to its political traditions, the African National Congress locates itself within a community of Third World nationalist governments that resolutely guard their pride of independence. Global processes that once appeared full of promise for post-apartheid South Africa did not deliver the goods. Many observers asked, has globalisation gone too far? If, as even the International Monetary Fund reported in the closing days of 1998, Malaysia’s reaction to financial</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.11</book-part-id>
                  <title-group>
                     <title>AFTERWORD:</title>
                     <subtitle>From Racial to Class Apartheid</subtitle>
                  </title-group>
                  <fpage>198</fpage>
                  <abstract>
                     <p>Five years after the publication of<italic>Elite Transition</italic>, this Afterword draws a brief balance sheet of South Africa’s post-apartheid era, from a critical perspective. Against the grain of so many uncritical and semi-critical accounts that have recently surfaced, this book maintains a simple thesis. Because of a broader structural crisis in South African capitalism dating back to the 1970s, white elites finally agreed to share power, so as to facilitate a new round of capital accumulation and to dampen the class and community struggles that were making life unprofitable and uncomfortable.</p>
                     <p>Fortunately for them, during the early 1990s, a small</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.12</book-part-id>
                  <title-group>
                     <title>AFTERWORD TO THE NEW EDITION:</title>
                     <subtitle>South Africa Faces its ‘Faustian Pact’: Neoliberalism, Financialisation and Proto-Fascism</subtitle>
                  </title-group>
                  <fpage>240</fpage>
                  <abstract>
                     <p>Around 500 years before Nelson Mandela was released from his long imprisonment, a simple bargain was offered to Johann Georg Faust, a legendary figure in what is now southwestern Germany. Faust, who dabbled in theology, medicine and astrology, did a deal with the devil’s assistant, Mephistopheles, whom he met one day roaming a nearby forest. Faust would enjoy 24 more years of a charmed life, but that meant selling the devil his soul. His death was reportedly the most ghastly imaginable, in the tragic culmination to a parable retold countless times by some of Europe’s greatest bards.</p>
                     <p>Mandela met several</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.13</book-part-id>
                  <title-group>
                     <title>Notes and References</title>
                  </title-group>
                  <fpage>275</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt183p1g7.14</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="editor" id="contrib1" xlink:type="simple">
                        <role>Compiled by</role>
                        <name name-style="western">
                           <surname>Carlton</surname>
                           <given-names>Sue</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>327</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
