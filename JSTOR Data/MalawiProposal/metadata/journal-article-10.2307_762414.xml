<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">humarighquar</journal-id>
         <journal-id journal-id-type="jstor">j100618</journal-id>
         <journal-title-group>
            <journal-title>Human Rights Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Johns Hopkins University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">02750392</issn>
         <issn pub-type="epub">1085794X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/762414</article-id>
         <title-group>
            <article-title>Creating a Composite Index for Assessing Country Performance in the Field of Human Rights: Proposal for a New Methodology</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Dipak K.</given-names>
                  <surname>Gupta</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Albert J.</given-names>
                  <surname>Jongman</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Alex P.</given-names>
                  <surname>Schmid</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">16</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i230894</issue-id>
         <fpage>131</fpage>
         <lpage>162</lpage>
         <page-range>131-162</page-range>
         <permissions>
            <copyright-statement>Copyright 1994 The Johns Hopkins University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/762414"/>
         <abstract>
            <p>This article sets out to provide a new methodology for attributing weights to the various indicators of human rights abuse. A number of studies have already collected data on various indicators of human rights abuse. These studies fall short, however, because they do not attribute weight to these indicators, and thus produce neither a composite indicator nor a group classification of countries according to their overall levels of performance. Relative weights can be attributed to these indicators in one of two ways. First, indicators can be weighted to reflect the values of the one constructing the index--an arbitrary scale. Alternatively, our methodology defines the extreme ends of the spectrum of human rights records by some widely acceptable standard, and then assesses the weights for the whole spectrum through Discriminant Analysis. Although this methodology will not end all the controversies on giving each human right indicator its relative place, nor rank individual countries on a world scale, this method is a step closer to a more objective measurement of human rights performance.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1212e144a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1212e151" publication-type="book">
Louis A. Wiesner and Steve Edminster, "Asylum Seekers, Other Foreigners, and Neo-
Nazi Violence in Germany," in 1993 World Refugee Survey (Washington, D.C.: U.S.
Committee for Refugees, 1993), 121.<person-group>
                     <string-name>
                        <surname>Wiesner</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Asylum Seekers, Other Foreigners, and NeoNazi Violence in Germany</comment>
                  <fpage>121</fpage>
                  <source>1993 World Refugee Survey</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e186a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1212e197" publication-type="book">
World Bank, World Development Report (New York: Oxford University Press, 1991),
25<person-group>
                     <string-name>
                        <surname>World Bank</surname>
                     </string-name>
                  </person-group>
                  <fpage>25</fpage>
                  <source>World Development Report</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e226" publication-type="book">
Dipak K. Gupta, The Economics of Political Vio-
lence (New York: Praeger, 1990)<person-group>
                     <string-name>
                        <surname>Gupta</surname>
                     </string-name>
                  </person-group>
                  <source>The Economics of Political Violence</source>
                  <year>1990</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e250" publication-type="journal">
Yiannis P. Venieris and Dipak K. Gupta, "Income
Distribution and Sociopolitical Instability as Determinants of Savings: A Cross-Sec-
tional Model," Journal of Political Economy 94 (1986): 873<object-id pub-id-type="jstor">10.2307/1833207</object-id>
                  <fpage>873</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e268" publication-type="journal">
Venieris and Gupta,
"Macro Interactions in a Social System: A Case Study of Great Britain," Southern Eco-
nomic journal 51 (1985): 681<object-id pub-id-type="doi">10.2307/1057872</object-id>
                  <fpage>681</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e286" publication-type="journal">
Venieris and Gupta, "Sociopolitical and Economic
Dimensions of Development: A Cross-Sectional Model," Economic Development and
Cultural Change 31 (1983): 727<object-id pub-id-type="jstor">10.2307/1153405</object-id>
                  <fpage>727</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e305a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1212e312" publication-type="journal">
Russel L. Barsh, "Measuring Human Rights: Problems of Methodology and Purpose,"
Human Rights Quarterly 15 (February 1993): 89<object-id pub-id-type="doi">10.2307/762653</object-id>
                  <fpage>89</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e327" publication-type="book">
Katerina Tomasevski, "A Critique of the UNDP Political Freedom Index
1991," in Human Rights in Developing Countries: Yearbook 1991, eds. B. Andreassen
and T. Swinehart (Oslo: Scandinavian University Press, 1992), 12-16<person-group>
                     <string-name>
                        <surname>Tomasevski</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">A Critique of the UNDP Political Freedom Index 1991</comment>
                  <fpage>12</fpage>
                  <source>Human Rights in Developing Countries: Yearbook 1991</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e362a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1212e369" publication-type="book">
Marlies Galenkamp, Individualism versus Collectivism: The Concepts of Collective Rights
(Rotterdam: Erasmus University, 1993), 249<person-group>
                     <string-name>
                        <surname>Galenkamp</surname>
                     </string-name>
                  </person-group>
                  <fpage>249</fpage>
                  <source>Individualism versus Collectivism: The Concepts of Collective Rights</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e397" publication-type="journal">
R.G. Ramcharan, "Individual, Collective and Group Rights: History, Theory, Practice, and
Contemporary Evolution," International Journal on Group Rights 1 (1993): 27-43<person-group>
                     <string-name>
                        <surname>Ramcharan</surname>
                     </string-name>
                  </person-group>
                  <fpage>27</fpage>
                  <volume>1</volume>
                  <source>International Journal on Group Rights</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e430a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1212e437" publication-type="other">
Thomas B. Jabine and Richard P. Claude, Human Rights and Statistics: Getting the Record
Straight (Philadelphia: Pennsylvania Press, 1992)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e446" publication-type="journal">
Barsh, note 3  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e454" publication-type="book">
Andreassen and
Swinehart (eds.), Human Rights in Developing Countries: Yearbook 1991 (Oslo: Scandi-
navian University Press, 1992)<person-group>
                     <string-name>
                        <surname>Andreassen</surname>
                     </string-name>
                  </person-group>
                  <source>Human Rights in Developing Countries: Yearbook 1991</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e483a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1212e490" publication-type="other">
Robert J. Goldstein, "The Limitations of Using Quantitative Data in Studying Human
Rights Abuses," in Thomas B. Jabine and Richard P. Claude, Human Rights and Statis-
tics: Getting the Record Straight (Philadelphia: Pennsylvania Press, 1992), 35-61.</mixed-citation>
            </p>
         </fn>
         <fn id="d1212e503a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1212e510" publication-type="book">
Jack
Donnelly, "Conceptual Issues in Monitoring Human Rights Violations," in Monitoring
Human Rights Violations, eds. Alex P. Schmid and Albert J. Jongman (Leiden: COMT,
1992), 85<person-group>
                     <string-name>
                        <surname>Donnelly</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Conceptual Issues in Monitoring Human Rights Violations</comment>
                  <fpage>85</fpage>
                  <source>Monitoring Human Rights Violations</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e548a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1212e555" publication-type="book">
Michael Cain, Richard P. Claude, and Thomas B. Jabine, "Guide to Human Rights Data
Sources" in Thomas B. Jabine and Richard P. Claude, Human Rights and Statistics: Getting
the Record Straight (Philadelphia: Pennsylvania Press, 1992), 397-442<person-group>
                     <string-name>
                        <surname>Mitchell</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Guide to Human Rights Data Sources</comment>
                  <fpage>397</fpage>
                  <source>Human Rights and Statistics: Getting the Record Straight</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e590a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1212e597" publication-type="book">
Christopher Mitchell, Michael Stohl, David Carleton and George A. Lopez, "State Ter-
rorism: Issues of Concept and Measurement," in Government Violence and Repression:
An Agenda for Research, eds. Michael Stohl &amp; George A. Lopez (Westport, Conn.:
Greenwood, 1986), 1.<person-group>
                     <string-name>
                        <surname>Mitchell</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">State Terrorism: Issues of Concept and Measurement</comment>
                  <fpage>1</fpage>
                  <source>Government Violence and Repression: An Agenda for Research</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e635a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1212e642" publication-type="journal">
Joseph E. Ryan, "Survey Methodology," Freedom Review 23 (1992): 13.<person-group>
                     <string-name>
                        <surname>Ryan</surname>
                     </string-name>
                  </person-group>
                  <fpage>13</fpage>
                  <volume>23</volume>
                  <source>Freedom Review</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e672a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1212e679" publication-type="book">
Charles Humana
(Comp.), World Human Rights Guide (London: Pan Books, 1987), vii-x<person-group>
                     <string-name>
                        <surname>Humana</surname>
                     </string-name>
                  </person-group>
                  <fpage>vii</fpage>
                  <source>World Human Rights Guide</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e707" publication-type="book">
Charles Humana
(Comp.), World Human Rights Guide (London: Oxford University Press, 3rd ed., 1992)<person-group>
                     <string-name>
                        <surname>Humana</surname>
                     </string-name>
                  </person-group>
                  <edition>3</edition>
                  <source>World Human Rights Guide</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e735" publication-type="book">
Zehra F. Arat, Democracy and Human Rights in Developing Countries (London: Lynne
Rienner, 1991)<person-group>
                     <string-name>
                        <surname>Arat</surname>
                     </string-name>
                  </person-group>
                  <source>Democracy and Human Rights in Developing Countries</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e760a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1212e767" publication-type="book">
Mitchell, note 10 above, 21-22  <fpage>21</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e779a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1212e788" publication-type="journal">
Ryan, note 11 above, 14  <fpage>14</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e800a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1212e807" publication-type="book">
Humana (1992), note 12  </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e816a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1212e823" publication-type="book">
Ibid.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e832a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1212e841" publication-type="book">
Humana (1987), note 12 above, 5  <fpage>5</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e854a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1212e861" publication-type="book">
Humana (1987), note 12 above, 6.  <fpage>6</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e873a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1212e880" publication-type="book">
Humana, note 12 above, 11  <fpage>11</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e892a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1212e899" publication-type="other">
Jacobellis v. Ohio, 378 U.S. 184, 197 (1964).</mixed-citation>
            </p>
         </fn>
         <fn id="d1212e906a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1212e913" publication-type="journal">
Irma Adelman and Cynthia T. Morris, "Per-
formance Criteria for Evaluating Economic Development Potential: An Operational
Approach," The QuarterlyJournal of Economics 82 (1968): 260<object-id pub-id-type="doi">10.2307/1885897</object-id>
                  <fpage>260</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e931" publication-type="journal">
Dipak K. Gupta, The Economics of Political Violence, note 2  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1212e939" publication-type="book">
Marija J. Norusis, SPSS Advanced Statistics
User's Guide (Chicago: McGraw Hill, 1990)<person-group>
                     <string-name>
                        <surname>Norusis</surname>
                     </string-name>
                  </person-group>
                  <source>SPSS Advanced Statistics User's Guide</source>
                  <year>1990</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e964a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1212e971" publication-type="journal">
D. Campbell and D. Fisk, "Convergent and Discriminant Validation by the Multitrait-
Multimethod Matrix," Psychological Bulletin 56 (1992): 81-105.<person-group>
                     <string-name>
                        <surname>Campbell</surname>
                     </string-name>
                  </person-group>
                  <fpage>81</fpage>
                  <volume>56</volume>
                  <source>Psychological Bulletin</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e1003a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1212e1010" publication-type="book">
Humana (1992), note 12 above, xi.  <fpage>xi</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e1023a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1212e1030" publication-type="book">
Ibid.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e1039a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1212e1046" publication-type="book">
PIOOM, Monitoring Human Rights: Manual forAssessing Country Performance (Leiden:
PIOOM, 1993), 292.<person-group>
                     <string-name>
                        <surname>Pioom</surname>
                     </string-name>
                  </person-group>
                  <fpage>292</fpage>
                  <source>Monitoring Human Rights: Manual forAssessing Country Performance</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1212e1075a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1212e1082" publication-type="book">
Gregg A. Beyer, "Matrix of Fundamental Human Rights and Indicative Levels of Viola-
tion/Abuse," in Monitoring Human Rights Violations, eds. Alex P. Schmid and Albert
J. Jongman (Leiden: COMT, 1992), 171.<person-group>
                     <string-name>
                        <surname>Beyer</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Matrix of Fundamental Human Rights and Indicative Levels of Violation/Abuse</comment>
                  <fpage>171</fpage>
                  <source>Monitoring Human Rights Violations</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
