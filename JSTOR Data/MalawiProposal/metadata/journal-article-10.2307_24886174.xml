<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">comppoli</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100131</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Comparative Politics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The City University of New York</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00104159</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24886174</article-id>
         <title-group>
            <article-title>The Historical Roots of Corruption: State Building, Economic Inequality, and Mass Education</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eric M.</given-names>
                  <surname>Uslaner</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bo</given-names>
                  <surname>Rothstein</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2016</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">48</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24885867</issue-id>
         <fpage>227</fpage>
         <lpage>248</lpage>
         <permissions>
            <copyright-statement>Copyright © 2016 City University of New York</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24886174"/>
         <abstract>
            <p>We show a link between levels of mass education in 1870 and corruption levels in 2010 for seventy-eight countries that remains strong when controlling for change in the level of education, GDP/ capita, and democracy. A model for the causal mechanism between universal education and control of corruption is presented. Early introduction of universal education is linked to levels of economic equality and to efforts to increase state capacity. First, societies with more equal education gave citizens more opportunities and power for opposing corruption. Secondly, the need for increased state capacity was a strong motivation for the introduction of universal education in many countries. Strong states provided more education to their publics and such states were more common where economic disparities were initially smaller.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>NOTES</title>
         <ref id="d204e181a1310">
            <label>1</label>
            <mixed-citation id="d204e188" publication-type="other">
John Joseph Wallis, "The Concept of Systemic Corruption in American History," in Edward L. Glaeser
and Claudia Goldin, eds., Corruption and Reform: Lessons from America's Economic History (Chicago:
University of Chicago Press, 2006), 7.</mixed-citation>
         </ref>
         <ref id="d204e201a1310">
            <label>2</label>
            <mixed-citation id="d204e208" publication-type="other">
Alina Mungiu-Pippidi, "Corruption: Diagnosis and Treatment," Journal of Democracy, 17 (July 2006),
86-99.</mixed-citation>
         </ref>
         <ref id="d204e218a1310">
            <label>3</label>
            <mixed-citation id="d204e225" publication-type="other">
Bo Rothstein, The Quality of Government: Corruption, Social Trust and Inequality in a Comparative
Perspective (Chicago: The University of Chicago Press, 2011).</mixed-citation>
         </ref>
         <ref id="d204e235a1310">
            <label>4</label>
            <mixed-citation id="d204e242" publication-type="other">
Bo Rothstein and Jan Teorell, "Defining and Measuring Quality of Government," in Sören Holmberg
and Bo Rothstein, eds., Good Government: The Relevance of Political Science (Cheltenham: Edward Elgar,
2012), 6-26.</mixed-citation>
         </ref>
         <ref id="d204e256a1310">
            <label>5</label>
            <mixed-citation id="d204e263" publication-type="other">
Daron Acemoglu and James A. Robinson, Why Nations Fail: The Origins of Power; Prosperity and
Poverty (London: Profile, 2012).</mixed-citation>
            <mixed-citation id="d204e272" publication-type="other">
Diego Comin, William Easterly, and Erick Gong. "Was the Wealth of
Nations Determined in 1000 BC?" American Economic Journal-Macroeconomics, 2 (July 2010), 65-97.</mixed-citation>
            <mixed-citation id="d204e281" publication-type="other">
Stanley L. Engerman and Kenneth L. Sokoloff, "Factor Endowments, Inequality, and Paths of
Development Among New World Economies," NBER Working Paper 9259 (2002), available at http://www.
nber.org/papers/w9259.</mixed-citation>
            <mixed-citation id="d204e294" publication-type="other">
Paola Guiliano and Nathan Nunn, "The Transmission of Democracy: From the Village
to the Nation-State," American Economic Review, 103 (May 2013), 86-92.</mixed-citation>
            <mixed-citation id="d204e303" publication-type="other">
Christian Welzel, Freedom Rising:
Human Empowerment and the Quest for Emancipation (Cambridge: Cambridge University Press, 2013).</mixed-citation>
         </ref>
         <ref id="d204e313a1310">
            <label>6</label>
            <mixed-citation id="d204e320" publication-type="other">
Edward Glaeser, Rafael La Porta, Florencio Lopez-De-Silanes, and Andrei Shleifer, "Do Institutions
Cause Growth?" Journal of Economic Growth, 9 (September 2004), 271-303.</mixed-citation>
         </ref>
         <ref id="d204e330a1310">
            <label>7</label>
            <mixed-citation id="d204e337" publication-type="other">
Claudia Goldin and Lawrence F. Katz, "Human Capital and Social Capital: The Rise of Secondary
Schooling in America, 1910-1940," Journal of Interdisciplinary History, 29 (Spring, 1999), 683-723.</mixed-citation>
            <mixed-citation id="d204e346" publication-type="other">
Rothstein, 2011, 111-15.</mixed-citation>
            <mixed-citation id="d204e352" publication-type="other">
Eric M. Uslaner, Corruption, Inequality, and the Rule of Law (New York:
Cambridge University Press, 2008), 236-41.</mixed-citation>
         </ref>
         <ref id="d204e362a1310">
            <label>8</label>
            <mixed-citation id="d204e369" publication-type="other">
Keith Darden, Resisting Occupation: Mass Literacy and the Creation of Durable National Loyalties
(New York: Cambridge University Press, 2013).</mixed-citation>
            <mixed-citation id="d204e378" publication-type="other">
Eric M. Uslaner, The Moral Foundations of Trust (New York:
Cambridge University Press, 2002), 208.</mixed-citation>
         </ref>
         <ref id="d204e388a1310">
            <label>9</label>
            <mixed-citation id="d204e395" publication-type="other">
Uslaner, 2008. Edward L. Glaeser, Jose Scheinkman, and Andrei Shleifer, "The Injustice of Inequality,"
Journal of Monetary Economics, 50 (January, 2003) 199-222.</mixed-citation>
         </ref>
         <ref id="d204e405a1310">
            <label>11</label>
            <mixed-citation id="d204e412" publication-type="other">
William Easterly and Ross Levine, "The European Origins of Economic Development," 2012,
available at http://www.econ.brown.edu/fac/Ross_Levine/other%20files/European_Origins.pdf.</mixed-citation>
         </ref>
         <ref id="d204e423a1310">
            <label>12</label>
            <mixed-citation id="d204e430" publication-type="other">
Bo Rothstein and Eric M. Uslaner, "All for All: Equality, Corruption, and Social Trust," World Politics,
58 (October 2005), 41-72.</mixed-citation>
         </ref>
         <ref id="d204e440a1310">
            <label>13</label>
            <mixed-citation id="d204e447" publication-type="other">
Carl Dahlström, Johannes Lindvall, and Bo Rothstein, "Corruption, Bureaucratic Failure and Social
Policy Priorities," Political Studies, 61 (October 2013), 523-42.</mixed-citation>
            <mixed-citation id="d204e456" publication-type="other">
Staffan Kumlin and Bo Rothstein, "Ques-
tioning the New Liberal Dilemma: Immigrants, Social Networks, and Institutional Fairness," Comparative
Politics, 41 (October 2010), 63-87.</mixed-citation>
            <mixed-citation id="d204e468" publication-type="other">
Rothstein and Uslaner, 2005, 41-72.</mixed-citation>
         </ref>
         <ref id="d204e475a1310">
            <label>14</label>
            <mixed-citation id="d204e482" publication-type="other">
Glaeser et al., 2004, 271-303.</mixed-citation>
         </ref>
         <ref id="d204e489a1310">
            <label>15</label>
            <mixed-citation id="d204e496" publication-type="other">
Darden, 2013</mixed-citation>
            <mixed-citation id="d204e502" publication-type="other">
Uslaner, 2002.</mixed-citation>
         </ref>
         <ref id="d204e509a1310">
            <label>16</label>
            <mixed-citation id="d204e516" publication-type="other">
Eugen Weber, Peasants into Frenchmen: The Modernization of Rural France 1870-1914 (Stanford:
Stanford University Press, 1976).</mixed-citation>
            <mixed-citation id="d204e525" publication-type="other">
John Bob, New Citizens for a New Society: The Institutional Origins of
Mass Schooling in Sweden (Oxford: Pergamon, 1989).</mixed-citation>
            <mixed-citation id="d204e534" publication-type="other">
Andy Green, Education and State Formation: The
Rise of Education Systems in England, France and the USA (New York: St. Martin's Press, 1990).</mixed-citation>
         </ref>
         <ref id="d204e544a1310">
            <label>17</label>
            <mixed-citation id="d204e551" publication-type="other">
Claudia Goldin and Lawrence F. Katz, The Race between Education and Technology (Cambridge:
Belknap Press, 2008), 135-36.</mixed-citation>
         </ref>
         <ref id="d204e562a1310">
            <label>18</label>
            <mixed-citation id="d204e569" publication-type="other">
Juan Botero, Alejandro Ponce, and Andrei Shleifer, "Education and the Quality of Government," NBER
Working Paper (2012), available at www/nber.org/papers/wl8119.</mixed-citation>
         </ref>
         <ref id="d204e579a1310">
            <label>19</label>
            <mixed-citation id="d204e586" publication-type="other">
Indranil Dutta and Ajit Mishra, "Does Inequality Foster Corruption?" Journal of Public Economic
Theory, 15 (August 2013), 602-19.</mixed-citation>
            <mixed-citation id="d204e595" publication-type="other">
Glaeser et al., 2004. Jong-sung You, "Inequality and Corruption: The
Role of Land Reform in Korea, Taiwan, and the Philippines," Paper presented at the Annual Conference of
the Association for Asian Studies, Atlanta, April, 2008, available at http://irps.ucsd.edu/assets/001/503066.pdf.</mixed-citation>
         </ref>
         <ref id="d204e608a1310">
            <label>20</label>
            <mixed-citation id="d204e615" publication-type="other">
Goldin and Katz, 2008, 29, 133.</mixed-citation>
         </ref>
         <ref id="d204e622a1310">
            <label>21</label>
            <mixed-citation id="d204e629" publication-type="other">
Christian Morrison and Fabrice Murtin, ".Journal of Human Capital (June, 2009), 1-42, available at
www. fabricemurtin.com.</mixed-citation>
         </ref>
         <ref id="d204e639a1310">
            <label>22</label>
            <mixed-citation id="d204e646" publication-type="other">
Goldin and Katz, 2008, 21, 133.</mixed-citation>
            <mixed-citation id="d204e652" publication-type="other">
Aaron Benavot and Phyllis Riddle, "The Expansion of Primary
Education, 1870-1940: Trends and Issues," Sociology of Education, 61 (July 1988), 191-210.</mixed-citation>
         </ref>
         <ref id="d204e662a1310">
            <label>23</label>
            <mixed-citation id="d204e669" publication-type="other">
Lena Wängnerud, "Why Women Are Less Corrupt than Men," in Sctren Holmberg and Bo Rothstein,
eds., Good Government: The Relevance of Political Science (Cheltenham: Edward Elgar, 2012), 212-32.</mixed-citation>
            <mixed-citation id="d204e678" publication-type="other">
Marcia Grimes and Lena Wängnerud, "Curbing Corruption Through Social Welfare Reform? The Effects
of Mexico's Conditional Cash Transfer Program on Good Government," American Review of Public Admin-
istration, 40 (November 2010), 671-90.</mixed-citation>
         </ref>
         <ref id="d204e692a1310">
            <label>24</label>
            <mixed-citation id="d204e699" publication-type="other">
Alicia Adserà, Carles Boix, and Mark Payne, "Are You Being Served? Political Accountability and
Quality of Government," The Journal of Law, Economics, Organization, 19 (October 2003), 445-90.</mixed-citation>
            <mixed-citation id="d204e708" publication-type="other">
Aymo Brunetti and Beatrice Weder, "A Free Press is Bad for Corruption," Journal of Public Economics, 87
(August 2003), 1801-29.</mixed-citation>
         </ref>
         <ref id="d204e718a1310">
            <label>25</label>
            <mixed-citation id="d204e725" publication-type="other">
Mungiu-Pippidi, 2006</mixed-citation>
            <mixed-citation id="d204e731" publication-type="other">
Douglass North, John Joseph Wallis, and Barry R. Weingast, eds., Violence and
Social Orders: A Conceptual Framework for Interpreting Recorded History (New York: Cambridge University
Press, 2009).</mixed-citation>
         </ref>
         <ref id="d204e744a1310">
            <label>26</label>
            <mixed-citation id="d204e751" publication-type="other">
Robert D. Woodberry, "The Missionary Roots of Liberal Democracy," American Political Science
Review, 106 (May 2012), 244-74.</mixed-citation>
         </ref>
         <ref id="d204e761a1310">
            <label>27</label>
            <mixed-citation id="d204e768" publication-type="other">
Ewout Frankema, "The Colonial Roots of Land Inequality: Geography, Factor Endowments, or
Institutions?" Economic History Review, 63 (May 2010), 418-351.</mixed-citation>
         </ref>
         <ref id="d204e778a1310">
            <label>28</label>
            <mixed-citation id="d204e785" publication-type="other">
Acemoglu and Robinson, 2012.</mixed-citation>
         </ref>
         <ref id="d204e792a1310">
            <label>29</label>
            <mixed-citation id="d204e801" publication-type="other">
Sören Holmberg, Bo Rothstein, and Naghmeh Nasiritousi, "Quality of
Government: What You Get," Annual Review of Political Science, 13 (June 2009), 135-62</mixed-citation>
            <mixed-citation id="d204e810" publication-type="other">
Insa
Bechert and Markus Quandt, "ISSP Data Report: Attitudes towards the Role of Government," Working
Paper 2009:2 (Bonn: GESIS. Liebniz-Institute für Sozialwissenschaften, 2009).</mixed-citation>
            <mixed-citation id="d204e823" publication-type="other">
Stefan Svallfors, "Gov-
ernment Quality, Egalitarianism, and Attitudes to Taxes and Social Spending: a European Comparison,"
European Political Science Review (online preview), 5 (November 2013), 363-80.</mixed-citation>
         </ref>
         <ref id="d204e837a1310">
            <label>30</label>
            <mixed-citation id="d204e844" publication-type="other">
The Morrison-Murtin data set is available at http://www.fabricemurtin.com/</mixed-citation>
            <mixed-citation id="d204e850" publication-type="other">
Bourginon-
Morrison economic data are available at http://www.delta.ens.fr/XIX/#1870.</mixed-citation>
            <mixed-citation id="d204e859" publication-type="other">
Glaeser et al., 2004,
use Lindert's measure of education for 1900</mixed-citation>
            <mixed-citation id="d204e869" publication-type="other">
Vanhanen, 1997, for percent family farms and democratization (available at http://www.fsd.uta.fi/english/
data/catalogue/FSD1216/)</mixed-citation>
            <mixed-citation id="d204e878" publication-type="other">
You and Khagram, 2005</mixed-citation>
            <mixed-citation id="d204e884" publication-type="other">
Marshall and Jaggers, 2010, available at http://www.systemicpeace.org/
polity/polity4.htm</mixed-citation>
            <mixed-citation id="d204e893" publication-type="other">
Tatu \hnhanen, Prospects of
Democracy: A Study of 172 Countries (London: Routledge, 1997).</mixed-citation>
            <mixed-citation id="d204e902" publication-type="other">
Jong-Sung You and S. Khagram, "A
Comparative Study of Inequality and Corruption," American Sociological Review, 70 (February 2005),
136-57.</mixed-citation>
            <mixed-citation id="d204e914" publication-type="other">
Monty G. Marshall, Polity IV Project: Political Regime Characteristics and Transitions, 1800-2013
(Vienna VA: Center for Systemic Peace, 2010).</mixed-citation>
         </ref>
         <ref id="d204e924a1310">
            <label>31</label>
            <mixed-citation id="d204e931" publication-type="other">
Uslaner, 2008, 74-79.</mixed-citation>
         </ref>
         <ref id="d204e938a1310">
            <label>32</label>
            <mixed-citation id="d204e945" publication-type="other">
Frankema, 2010, 418-351.</mixed-citation>
            <mixed-citation id="d204e951" publication-type="other">
Kenneth L. Sokoloff and Stanley L. Engerman, "Institutions, Factor
Endowments, and Paths of Development in the New World," Journal of Economic Perspectives, 14 (Summer
2000), 217-32.</mixed-citation>
         </ref>
         <ref id="d204e964a1310">
            <label>33</label>
            <mixed-citation id="d204e971" publication-type="other">
Comin et al., 2010.</mixed-citation>
         </ref>
         <ref id="d204e978a1310">
            <label>35</label>
            <mixed-citation id="d204e985" publication-type="other">
Green, 3If.</mixed-citation>
         </ref>
         <ref id="d204e992a1310">
            <label>36</label>
            <mixed-citation id="d204e999" publication-type="other">
Ibid, 1990.</mixed-citation>
         </ref>
         <ref id="d204e1007a1310">
            <label>37</label>
            <mixed-citation id="d204e1014" publication-type="other">
Ibid, 1990</mixed-citation>
            <mixed-citation id="d204e1020" publication-type="other">
Boli, 1989</mixed-citation>
            <mixed-citation id="d204e1026" publication-type="other">
Weber, 1976.</mixed-citation>
         </ref>
         <ref id="d204e1033a1310">
            <label>38</label>
            <mixed-citation id="d204e1040" publication-type="other">
Green, 79.</mixed-citation>
         </ref>
         <ref id="d204e1047a1310">
            <label>39</label>
            <mixed-citation id="d204e1054" publication-type="other">
Green, 79.</mixed-citation>
         </ref>
         <ref id="d204e1061a1310">
            <label>40</label>
            <mixed-citation id="d204e1068" publication-type="other">
Boli, 34, 232.</mixed-citation>
         </ref>
         <ref id="d204e1075a1310">
            <label>4L</label>
            <mixed-citation id="d204e1082" publication-type="other">
Francisco O. Ramirez and John Boli, "The Political Construction of Mass Schooling: European Origins
and Worldwide Institutionalization," Sociology of Education, 60 (January 1987), 2-17.</mixed-citation>
         </ref>
         <ref id="d204e1092a1310">
            <label>42</label>
            <mixed-citation id="d204e1099" publication-type="other">
Ramirez and Boli, 4.</mixed-citation>
         </ref>
         <ref id="d204e1107a1310">
            <label>43</label>
            <mixed-citation id="d204e1114" publication-type="other">
Ramirez and Boli, 4.</mixed-citation>
         </ref>
         <ref id="d204e1121a1310">
            <label>44</label>
            <mixed-citation id="d204e1128" publication-type="other">
Ramirez and Boli. 5.</mixed-citation>
         </ref>
         <ref id="d204e1135a1310">
            <label>45</label>
            <mixed-citation id="d204e1142" publication-type="other">
ibid</mixed-citation>
            <mixed-citation id="d204e1148" publication-type="other">
Green 1990.</mixed-citation>
         </ref>
         <ref id="d204e1155a1310">
            <label>46</label>
            <mixed-citation id="d204e1162" publication-type="other">
Boli, 218</mixed-citation>
            <mixed-citation id="d204e1168" publication-type="other">
Weber, 1976.</mixed-citation>
         </ref>
         <ref id="d204e1175a1310">
            <label>47</label>
            <mixed-citation id="d204e1182" publication-type="other">
Denis Mack Smith, Modern Italy: A Political History (New Haven: Yale University Press, 1997).</mixed-citation>
         </ref>
         <ref id="d204e1189a1310">
            <label>48</label>
            <mixed-citation id="d204e1196" publication-type="other">
Robert Putnam, Making Democracy Work: Civic Traditions in Modern Italy (Princeton: Princeton
University Press, 1993).</mixed-citation>
         </ref>
         <ref id="d204e1207a1310">
            <label>49</label>
            <mixed-citation id="d204e1214" publication-type="other">
Nicholas Charron, Victor Lapuente, and Bo Rothstein, Quality of Government and Corruption from
a European Perspective (Cheltenham: Edward Elgar, 2013).</mixed-citation>
         </ref>
         <ref id="d204e1224a1310">
            <label>50</label>
            <mixed-citation id="d204e1231" publication-type="other">
Smith, 1997.</mixed-citation>
         </ref>
         <ref id="d204e1238a1310">
            <label>51</label>
            <mixed-citation id="d204e1245" publication-type="other">
Woodberry, 2011.</mixed-citation>
         </ref>
         <ref id="d204e1252a1310">
            <label>52</label>
            <mixed-citation id="d204e1259" publication-type="other">
Gill, 1998.</mixed-citation>
         </ref>
         <ref id="d204e1266a1310">
            <label>53</label>
            <mixed-citation id="d204e1273" publication-type="other">
Weber, 1976, ch. 18.</mixed-citation>
            <mixed-citation id="d204e1279" publication-type="other">
Herbert Tingsten, Gud och fosterlandet: studier i hundra ârs skolpropaganda
(Stockholm: Norstedt, 1969).</mixed-citation>
         </ref>
         <ref id="d204e1289a1310">
            <label>54</label>
            <mixed-citation id="d204e1296" publication-type="other">
Boli, 209-12</mixed-citation>
            <mixed-citation id="d204e1302" publication-type="other">
Weber, 362-64</mixed-citation>
            <mixed-citation id="d204e1308" publication-type="other">
Green, 1990.</mixed-citation>
         </ref>
         <ref id="d204e1316a1310">
            <label>55</label>
            <mixed-citation id="d204e1323" publication-type="other">
Green, 29.</mixed-citation>
         </ref>
         <ref id="d204e1330a1310">
            <label>56</label>
            <mixed-citation id="d204e1337" publication-type="other">
Sascha O. Becker and Ludger Woessmann, "Whs Weber Wrong? A Human Capital Theory of Protestant
Economic History," Quarterly Journal of Economics, 124 (May 2009), 531-96.</mixed-citation>
         </ref>
         <ref id="d204e1347a1310">
            <label>58</label>
            <mixed-citation id="d204e1354" publication-type="other">
Caroline Bledsoe, "The Cultural Transformation of Western Education in Sierra Leone," Africa: Journal
of the International African Institute, 62 (April 1992), 182-202.</mixed-citation>
            <mixed-citation id="d204e1363" publication-type="other">
Alf Andrew Heggoy, "Education in French
Algeria: An Essay on Cultural Conflict," Comparative Education Review., 17 (June 1973), 180-97.</mixed-citation>
            <mixed-citation id="d204e1372" publication-type="other">
Bronislaw
Malinowski, "The Pan-African Problem of Culture Contact," American Journal of Sociology, 48 (May 1943),
649-65.</mixed-citation>
            <mixed-citation id="d204e1385" publication-type="other">
M. A. Mpka, "Overview of Educational Development: Pre-colonial to Present Day," n.d., available
at http://onlinenigeria.com/education/7blurb=534.</mixed-citation>
         </ref>
         <ref id="d204e1395a1310">
            <label>59</label>
            <mixed-citation id="d204e1402" publication-type="other">
Angus Maddison, "The Economic and Social Impact of Colonial Rule in India," in Angus Maddison,
eds., Class Structure and Economic Growth: India Pakistan since the Moghuls, 1971, available at http://www.
ggdc.net/maddison/articles/moghul_3.pdf.</mixed-citation>
            <mixed-citation id="d204e1414" publication-type="other">
Mpka, n.d.</mixed-citation>
         </ref>
         <ref id="d204e1421a1310">
            <label>60</label>
            <mixed-citation id="d204e1428" publication-type="other">
Rama Sundari Mantena, "Imperial Ideology and the Uses of Rome in Discourses on Britain's Indian
Empire," in Mark Bradley, ed., Classics and Imperialism in the British Empire (Oxford: Oxford University
Press, 2010), available at http://ramamantena.files.wordpress.com/2011/03/rama-mantena-contribution-to-
classics-and-imperialism-volumel.pdf.</mixed-citation>
            <mixed-citation id="d204e1443" publication-type="other">
Maddison, 1971.</mixed-citation>
         </ref>
         <ref id="d204e1450a1310">
            <label>61</label>
            <mixed-citation id="d204e1457" publication-type="other">
Thomas Willing Balch, "French Colonization in North Africa," American Political Science Review,
3 (November, 1909), 539-5511.</mixed-citation>
            <mixed-citation id="d204e1466" publication-type="other">
Heggoy, 1973.</mixed-citation>
         </ref>
         <ref id="d204e1474a1310">
            <label>62</label>
            <mixed-citation id="d204e1481" publication-type="other">
Bianca Premo, Children of the Father King: Youth, Authority, and Legal Minority in Colonial Lima
(Chapel Hill: University of North Carolina Press, 2005).</mixed-citation>
         </ref>
         <ref id="d204e1491a1310">
            <label>63</label>
            <mixed-citation id="d204e1498" publication-type="other">
Edward D. Fitchen, "Primary Education in Colonial Cuba: Spanish Tool for Retaining 'La Isla Siempre'
Leal?" Caribbean Studies, 14 (April 1974), 105-20.</mixed-citation>
         </ref>
         <ref id="d204e1508a1310">
            <label>64</label>
            <mixed-citation id="d204e1515" publication-type="other">
Rodgrigo Arocena and Judith Sutz, "Uruguay: Higher Education, National System of Innovation and
Economic Development in a Small Peripheral Country," Lund University Research Policy Institute, 2008,
available at www.fpi.lu.se/_media/en/research/UniDev_DP_Uruguay.pdf.</mixed-citation>
         </ref>
         <ref id="d204e1528a1310">
            <label>65</label>
            <mixed-citation id="d204e1535" publication-type="other">
Don Adams, "Problems of Reconstruction in Korean Education," Comparative Education Review,
3 (February 1960), 27-32.</mixed-citation>
            <mixed-citation id="d204e1544" publication-type="other">
R. R Dor, "Education: Japan," in Robert E. Ward and Dankwart A. Rustow,
eds., Political Modernization in Japan and Turkey (Princeton: Princeton University Press, 1964).</mixed-citation>
            <mixed-citation id="d204e1553" publication-type="other">
Frederick
W. Frey, "Education: Turkey," in Ward and Rustow, eds. Sevinc Sevda Kilicap, Exploring Politics of Philan-
thropy, unpublished thesis, Master of International Studies in Philanthropy (University of Bologna, 2009).</mixed-citation>
         </ref>
         <ref id="d204e1566a1310">
            <label>66</label>
            <mixed-citation id="d204e1573" publication-type="other">
Ministry of Education and Culture (Hungary), Education in Hungary: Past, Present, and Future, An
Overview, 2008, available at http://www.nefmi.gov.hu/letolt/english/education_in_hungary_080805.pdf.</mixed-citation>
            <mixed-citation id="d204e1582" publication-type="other">
Bulgarian Properties, "History of Bulgarian Education," 2008, available at http://bulgarianproperties.info/
history-of-bulgarian-education/.</mixed-citation>
         </ref>
         <ref id="d204e1592a1310">
            <label>67</label>
            <mixed-citation id="d204e1599" publication-type="other">
Stanley L. Engerman and Kenneth L. Sokoloff, "Factor Endowments, Inequality, and Paths of
Development Among New World Economies," NBER Working Paper 9259 (2002), available at http://www.
nber. org/papers/w9259.</mixed-citation>
         </ref>
         <ref id="d204e1613a1310">
            <label>68</label>
            <mixed-citation id="d204e1620" publication-type="other">
Easterly and Levine, 2012.</mixed-citation>
         </ref>
         <ref id="d204e1627a1310">
            <label>69</label>
            <mixed-citation id="d204e1634" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d204e1641a1310">
            <label>70</label>
            <mixed-citation id="d204e1648" publication-type="other">
\hnhanen's, 1997, 48.</mixed-citation>
         </ref>
         <ref id="d204e1655a1310">
            <label>71</label>
            <mixed-citation id="d204e1662" publication-type="other">
Carles Boix, "Civil Wars and Guerrilla Warfare in the Contemporary World: Toward a Joint Theory of
Motivations and Opportunities," in Stathis Kalyvas, Ian Shapiro, and Tarek Masoud, eds, Order Conflict and
Violence (New York: Cambridge University Press, 2008).</mixed-citation>
         </ref>
         <ref id="d204e1675a1310">
            <label>72</label>
            <mixed-citation id="d204e1682" publication-type="other">
William Easterly, "Inequality Does Cause Underdevelopment: Insights from a New Instrument," unpub-
lished paper (New York University, 2006), available at www.intemational.ucla.edu/cms/files/PERG.easterly.
pdf.</mixed-citation>
            <mixed-citation id="d204e1694" publication-type="other">
Dietrich Rueschemeyer, Evelyne Huber Stephens, and John D. Stephens, Capitalist Development
and Democracy (Chicago: University of Chicago Press, 1992)</mixed-citation>
            <mixed-citation id="d204e1703" publication-type="other">
Oded Galor, Omer Moav, and Dietrich
\bllrath, "Inequality in Landownership, the Emergence of Human-Capital Promoting Institutions, and the
Great Divergence," Review of Economic Studies, 76 (June 2006), 143-79.</mixed-citation>
         </ref>
         <ref id="d204e1716a1310">
            <label>74</label>
            <mixed-citation id="d204e1723" publication-type="other">
http://www.sscnet.ucla.edu/polisci/faculty/treisman/Pages/unpublishedpapers.
html.</mixed-citation>
            <mixed-citation id="d204e1732" publication-type="other">
http://www.freedomhouse.org/report-
types/freedom-press#.U81AbvldXh4</mixed-citation>
         </ref>
         <ref id="d204e1743a1310">
            <label>75</label>
            <mixed-citation id="d204e1750" publication-type="other">
Mathias A. Färdigh, What's the use of a free media? The role of media in curbing corruption and
promoting quality of government. (Gothenburg: Department of Journalism Media and Communication Uni-
versity of Gothenburg, 2013), 19.</mixed-citation>
         </ref>
         <ref id="d204e1763a1310">
            <label>76</label>
            <mixed-citation id="d204e1770" publication-type="other">
Glaeser et al., 2004.</mixed-citation>
         </ref>
         <ref id="d204e1777a1310">
            <label>77</label>
            <mixed-citation id="d204e1784" publication-type="other">
Acemoglu and Robinson, 2012, 18-19, 27.</mixed-citation>
         </ref>
         <ref id="d204e1791a1310">
            <label>78</label>
            <mixed-citation id="d204e1798" publication-type="other">
Acemoglu and Robinson, 2012, 19.</mixed-citation>
         </ref>
         <ref id="d204e1805a1310">
            <label>79</label>
            <mixed-citation id="d204e1812" publication-type="other">
Chon Sum Ihm, "South Korea," in Paul Morris and Anthony Sweeting, eds.. Education and
Development in East Asia (New York: Garland, 1995).</mixed-citation>
            <mixed-citation id="d204e1821" publication-type="other">
Gwang-Jo Kim, "Education Policies and Reform in
South Korea," in Africa Region, The World Bank, Secondary Education in Africa: Strategies for Renewal,
2002, available at http://siteresources.worldbank.org/INTAFRREGTOPEDUCATION/Resources/444659-
1220976732806/Secondary_Education_Strategies_renewal.pdf.</mixed-citation>
            <mixed-citation id="d204e1836" publication-type="other">
Sunwoong Kim and Ju-Ho Lee, "The
Secondary School Equalization Policy in South Korea," unpublished paper (University of Wisconsin-
Milwaukee, 2003).</mixed-citation>
         </ref>
         <ref id="d204e1849a1310">
            <label>80</label>
            <mixed-citation id="d204e1856" publication-type="other">
Jong-sung You, "Inequality and Corruption: The Role of Land Reform in Korea, Taiwan, and the
Philippines," unpublished paper, University of California-San Diego, n.d, available at http://irps.ucsd.edu/
assets/001/503066.pdf</mixed-citation>
            <mixed-citation id="d204e1868" publication-type="other">
Jong-sung You, A Comparative Study of Income Inequality, Corruption, and Social
Trust: How Inequality and Corruption Reinforce Each Other and Erode Social Trust, Ph.D. Dissertation
(draft) (Department of Government, Harvard University, 2005).</mixed-citation>
         </ref>
         <ref id="d204e1882a1310">
            <label>81</label>
            <mixed-citation id="d204e1889" publication-type="other">
William K. Cummings, Education and Equality in Japan (Princeton: Princeton University Press, 1980).</mixed-citation>
         </ref>
         <ref id="d204e1896a1310">
            <label>82</label>
            <mixed-citation id="d204e1903" publication-type="other">
Kaori Okano and Motonori Tsuchiya, Education in Contemporary Japan: Inequality and Diversity
(Cambridge: Cambridge University Press, 1999).</mixed-citation>
         </ref>
         <ref id="d204e1913a1310">
            <label>83</label>
            <mixed-citation id="d204e1920" publication-type="other">
David G. Kirby, A Concise History of Finland (New York: Cambridge University Press, 2006).</mixed-citation>
            <mixed-citation id="d204e1926" publication-type="other">
Henrik
Meinander and Tom Geddes, A History of Finland (London: Hurst, 2011). Ministry of Education and Culture
(Hungary), Education in Hungary: Past, Present, and Future. An Overview, 2008, available at http://www.
nefmi.gov.hu/letolt/english/education_inJiungary_080805.pdf.</mixed-citation>
         </ref>
         <ref id="d204e1942a1310">
            <label>84</label>
            <mixed-citation id="d204e1949" publication-type="other">
Ibid, 89.</mixed-citation>
         </ref>
         <ref id="d204e1956a1310">
            <label>85</label>
            <mixed-citation id="d204e1963" publication-type="other">
Heikki Ylikangas, Vagen till Tammerfors. striden mellan röda och vita i finska inbördeskriget 1918
(Stockholm: Atlantis, 1995). Meinander, 2011.</mixed-citation>
         </ref>
         <ref id="d204e1973a1310">
            <label>86</label>
            <mixed-citation id="d204e1980" publication-type="other">
Phillipe Aghion, Torsten Persson, and Dorothee Rouzet, "Education and Military Rivalry,"
unpublished paper (Harvard University, 2012), available at: http://scholar.harvard.edu/files/aghion/files/
education_and_military_rivalry.pdf.</mixed-citation>
         </ref>
         <ref id="d204e1994a1310">
            <label>87</label>
            <mixed-citation id="d204e2001" publication-type="other">
Uslaner, 2008, ch. 7.</mixed-citation>
         </ref>
         <ref id="d204e2008a1310">
            <label>88</label>
            <mixed-citation id="d204e2015" publication-type="other">
Ben Ansell and Johannes Lindvall, "The Political Origins of Primary Education Systems," American
Political Science Review, 107 (August 2013), 505-22.</mixed-citation>
         </ref>
         <ref id="d204e2025a1310">
            <label>89</label>
            <mixed-citation id="d204e2032" publication-type="other">
Oliver Grant, Migration and Inequality in Germany 1870-1913 (Oxford: Clarendon Press, 2005).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
