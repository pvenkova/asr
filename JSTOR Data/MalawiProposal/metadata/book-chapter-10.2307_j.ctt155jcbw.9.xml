<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt1551n80</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt155jcbw</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Scots in South Africa</book-title>
         <subtitle>Ethnicity, identity, gender and race, 1772-1914</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>MacKenzie</surname>
               <given-names>John M.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <role>with</role>
            <name name-style="western">
               <surname>Dalziel</surname>
               <given-names>Nigel R.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>19</day>
         <month>07</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9780719087837</isbn>
      <isbn content-type="epub">9781847794468</isbn>
      <publisher>
         <publisher-name>Manchester University Press</publisher-name>
         <publisher-loc>Manchester; New York</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2007</copyright-year>
         <copyright-holder>John M. MacKenzie</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt155jcbw"/>
      <abstract abstract-type="short">
         <p>The description of South Africa as a 'rainbow nation' has always been taken to embrace the black, brown and white peoples who constitute its population. But each of these groups can be sub-divided and in the white case, the Scots have made one of the most distinctive contributions to the country's history. The Scots, as in North America and Australasia, constituted an important element in the patterns of White settlement. They were already present in the area of Dutch East India Company rule and, after the first British occupation of the Cape in 1795, their numbers rose dramatically. They were exceptionally active in such areas as exploration, botanical and scientific endeavour, military campaigns, the emergence of Christian missions, Western education, intellectual institutions, the professions as well as enterprise and technical developments, business, commerce and journalism. This book is the first full-length study of their role from the eighteenth to twentieth centuries. It highlights the interaction of Scots with African peoples, the manner in which missions and schools were credited with producing 'Black Scotsmen' and the ways in which they pursued many distinctive policies. It also deals with the inter-weaving of issues of gender, class and race as well as with the means by which Scots clung to their ethnicity through founding various social and cultural societies. This book offers a major contribution to both Scottish and South African history and in the process illuminates a significant field of the Scottish Diaspora that has so far received little attention.</p>
      </abstract>
      <counts>
         <page-count count="304"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.3</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Devine</surname>
                           <given-names>T. M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>This important book is one product of an innovative academic development in diaspora studies sponsored by the UK Arts and Humanities Research Council (AHRC) from 2001. In that year the AHRC provided substantial funding for a Research Centre in Irish and Scottish Studies hosted by the University of Aberdeen but also consisting of scholars drawn from its partner institutions of Queen’s University, Belfast, and Trinity College, Dublin. Both the Irish and Scots contributed disproportionately to emigration from the British Isles and so intrinsic to the venture, in addition to research programmes in languages and literatures, were several projects on the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGEMENTS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>J. M. M.</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.5</book-part-id>
                  <title-group>
                     <title>LIST OF ABBREVIATIONS</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.6</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Introduction:</title>
                     <subtitle>imperialism and identities</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>When Archbishop Desmond Tutu coined the phrase ‘the rainbow nation’ for post-apartheid South Africa he was no doubt thinking of varying shades of black, brown and white. But within each of those categories there are of course further ethnic subdivisions, identifiable through language, history, cultural traditions and religious forms. Africans are mainly divided into different branches of the Nguni and the Sotho–Tswana peoples, as well as migrants (and sometimes refugees) from adjacent states. People of Asian descent have migrated, or been brought as slaves and indentured labourers, from parts of the Indian subcontinent and South East Asia. The Cape</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.7</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>The Scots presence at the Cape</title>
                  </title-group>
                  <fpage>29</fpage>
                  <abstract>
                     <p>The modern visitor to Cape Town may notice on the map an area known as Schotsche Kloof, a key route between Table Mountain and the Lion’s Head connecting the city with the southern part of the Cape peninsula. No one seems quite clear how it acquired its name, or when. It may have run over land owned by a Scot or it may commemorate the presence of the Scots Brigade as the garrison at the Cape for a substantial period in the eighteenth century. At any rate, it symbolises a Scots presence which is of long standing. Indeed, Scots did</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.8</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>Radicals, evangelicals, the Scottish Enlightenment and Cape Colonial autocracy</title>
                  </title-group>
                  <fpage>64</fpage>
                  <abstract>
                     <p>Pringle was a figure of both the frontier and the Cape capital. He moved between the two, viewing himself as a patriarch and observant protagonist of settlers in the one and as a liberal writer and evangelical controversialist in the other. He travelled overland between the frontier and Cape Town in 1822, a journey which inspired some of his poems. In these he applied a romantic sensibility, heavily derived from Scott, to the African landscape, incorporating it into European aesthetic and literary norms.¹ Once in the capital, he joined a group of Scots who were busily creating the intellectual and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.9</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>Scots missions and the frontier</title>
                  </title-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>The histories of the Cape frontier, of Scots military figures and of missionaries are inseparably intertwined. Yet, in a significant corpus of historical writing on the frontier, they have rarely been satisfactorily combined. Moreover, until recent times the Scots missionaries have seldom been examined as a separate ethnic group with different objectives and methods, even although their activities upon the frontier were important in both white and, more particularly, black history. The missionaries constituted a separate pressure group with connections with the imperial metropole and to Scottish society and its various Churches. They were frontier ‘pioneers’ who arrived when that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.10</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>Continuing migration to Natal, the Cape and the Transvaal</title>
                  </title-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>From the 1850s there were fresh attempts to encourage migrants to move to South Africa, particularly to Natal. The colony of Natal had been annexed by the British in 1843 in order to frustrate the Boer Voortrekkers in their desire to establish a republic there, a settlement which had led to conflict with the Zulu which would become a major element of the Afrikaners’ historical mythology. English-speakers had been operating in Natal, mainly as hunters and traders, from the 1820s, but once the protection of the imperial power appeared to have been offered them, their numbers began to grow rapidly.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.11</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>Professionals:</title>
                     <subtitle>the Church and education</subtitle>
                  </title-group>
                  <fpage>169</fpage>
                  <abstract>
                     <p>Scots went further than simply exporting aspects of their civil society and cultural forms to South Africa. As Jonathan Hyslop has convincingly argued, they also despatched their social tensions and divisions, as well as their propensity for radical politics and trade union activity.¹ On the one hand, the most visible and prominent Scots were unquestionably either bourgeois or of rising social aspirations: educated professionals and business people, usually Presbyterian and eager for respectability, embracing a mix of ambition and continued adherence to Scottish cultural norms. But, at the lower end of the social order, Scotland contributed people who had often</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.12</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>The professionals:</title>
                     <subtitle>the environment, medicine, business and radicals</subtitle>
                  </title-group>
                  <fpage>204</fpage>
                  <abstract>
                     <p>The relationship between Scots, their vision of Scotland and the landscape of the Cape has already been noted, particularly in respect of Thomas Pringle, in Chapter 2. But these environmental connections between Scotland and the Cape go much deeper. They can be identified as central to the development of the intertwining of environmentalism and evangelicalism in nineteenth-century southern Africa. Scots had a striking influence upon environmental ideas and the emergence of the sciences relating to forestry, agriculture, climate, hydrology and animal husbandry. Debates connected with drought, aridity, deforestation and dramatic climate swings had wider global significance, and these were in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.13</book-part-id>
                  <title-group>
                     <label>CHAPTER EIGHT</label>
                     <title>Maintaining Scots identity</title>
                  </title-group>
                  <fpage>240</fpage>
                  <abstract>
                     <p>In his most celebrated poem, ‘Afar in the desert’, Thomas Pringle recounted a ride across the Karoo, evoking the landscape and the fauna while pessimistically ruminating on ‘My high aims abandoned — my good acts undone’. But his bitterness is assuaged by memories of:</p>
                     <p>My Native Land — whose magical name</p>
                     <p>Thrills to the heart like electric flame;</p>
                     <p>The home of my childhood; the haunts of my prime;</p>
                     <p>All the passions and scenes of that rapturous time</p>
                     <p>When the feelings were young and the world was new,</p>
                     <p>Like the fresh bowers of Eden unfolding to view;</p>
                     <p>All — all now forsaken — forgotten — forgone!</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.14</book-part-id>
                  <title-group>
                     <label>CHAPTER NINE</label>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>267</fpage>
                  <abstract>
                     <p>One of the enduring themes of South African history in the British imperial period is Anglicisation. Lord Charles Somerset set out to Anglicise the administrative, legal and educational systems. The British sought to Anglicise the frontier, with military force and by settlement. They took over Natal from the Boer Trekkers. They made repeated and unsuccessful attempts to Anglicise the interior between the 1840s and 1870s, always foundering on the resistance of Boers and the environment, as well as upon their own hesitation or incompetence. Helped by diamonds, they extended and partially Anglicised the north-western Cape frontier, though gold had the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt155jcbw.15</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>276</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
