<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/tava17298</book-id>
      <subj-group>
         <subject content-type="call-number">HG4521 .R477 2016</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Investments</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Investments, Foreign</subject>
         <subj-group>
            <subject content-type="lcsh">Government policy</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Investments, Foreign</subject>
         <subj-group>
            <subject content-type="lcsh">Taxation</subject>
            <subj-group>
               <subject content-type="lcsh">Law and legislation</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">International business enterprises</subject>
         <subj-group>
            <subject content-type="lcsh">Government policy</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Economics</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Rethinking Investment Incentives</book-title>
         <subtitle>Trends and Policy Options</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Tavares-Lehmann</surname>
               <given-names>Ana Teresa</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Toledano</surname>
               <given-names>Perrine</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Johnson</surname>
               <given-names>Lise</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib4">
            <name name-style="western">
               <surname>Sachs</surname>
               <given-names>Lisa</given-names>
            </name>
         </contrib>
         <role content-type="editor">Editors</role>
      </contrib-group>
      <pub-date>
         <day>12</day>
         <month>07</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="epub">9780231541640</isbn>
      <isbn content-type="epub">0231541643</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/tava17298"/>
      <abstract abstract-type="short">
         <p>Governments often use direct subsidies or tax credits to encourage investment and promote economic growth and other development objectives. Properly designed and implemented, these incentives can advance a wide range of policy objectives (increasing employment, promoting sustainability, and reducing inequality). Yet since design and implementation are complicated, incentives have been associated with rent-seeking and wasteful public spending.</p>
         <p>This collection illustrates the different types and uses of these initiatives worldwide and examines the institutional steps that extend their value. By combining economic analysis with development impacts, regulatory issues, and policy options, these essays show not only how to increase the mobility of capital so that cities, states, nations, and regions can better attract, direct, and retain investments but also how to craft policy and compromise to ensure incentives endure.</p>
      </abstract>
      <counts>
         <page-count count="368"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>I</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>V</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.3</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Moran</surname>
                           <given-names>Theodore H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>VII</fpage>
                  <abstract>
                     <p>Policy makers and theorists have had more than two centuries—since the times of Adam Smith and David Ricardo—to investigate the dynamics of trade flows, and to explore the intricacies of trade policy. It should not be surprising, therefore, that we are still in the formative stage of coming to grips with analytical challenges and policy quandaries associated with today’s much more complicated realm of trade-and-investment.</p>
                     <p>Over the past three and a half decades, foreign direct investment has become the principal vehicle to deliver goods and services across borders. World nominal GDP has increased four times, and world bilateral</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.4</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tavares-Lehmann</surname>
                           <given-names>Ana Teresa</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sachs</surname>
                           <given-names>Lisa</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Johnson</surname>
                           <given-names>Lise</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Toledano</surname>
                           <given-names>Perrine</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In July 2015, the government of Ethiopia hosted the Third International Financing for Development Conference, bringing together world leaders from governments, businesses, and international organizations to chart a course for financing the post-2015 development agenda. That agenda includes the most critical challenges facing society—ending extreme poverty, eradicating preventable diseases, and halting global warming, among others. Achieving the resulting sustainable development goals (SDGs) by 2030 will require mobilizing and harnessing substantial resources from both the public and the private sectors. At the conference, the global leaders recognized that “private business activity, investment and innovation are major drivers of productivity, inclusive</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Types of Investment Incentives</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tavares-Lehmann</surname>
                           <given-names>Ana Teresa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>This chapter clarifies the types of incentives that territories can use to attract or influence the behavior of investors, drawing on the definitions of incentives and on the considerations found in chapter 1. It does not purport to evaluate the different types of incentives in terms of their efficiency (Are their costs compensated for by appropriate benefits?), effectiveness¹ (Are they successful?), and other criteria; several chapters in this volume address these questions (see, for instance, chapter 9, which suggests the importance of aligning investment incentives with sustainable development goals). Nor does it purport to explain the economic rationale for the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Definitions, Motivations, and Locational Determinants of Foreign Direct Investment</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lundan</surname>
                           <given-names>Sarianna M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>This chapter examines the different motivations for foreign direct investment (FDI) as well as the economic and institutional determinants of location choice for foreign investment. It concentrates on what determines the extent of cross-border activity by multinational enterprises (MNEs) based on the fundamentals of the interplay between the location and firm-specific determinants of investment. Because investment incentives seek to influence the decision to invest and location choice for MNEs, an understanding of the fundamental determinants is essential to evaluating which incentives are likely to be effective in influencing investment decisions.</p>
                     <p>The chapter also discusses some of the policy challenges related</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>The Use of Investment Incentives:</title>
                     <subtitle>THE CASES OF R&amp;D-RELATED INCENTIVES AND INTERNATIONAL INVESTMENT AGREEMENTS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bellak</surname>
                           <given-names>Christian</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Leibrecht</surname>
                           <given-names>Markus</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>63</fpage>
                  <abstract>
                     <p>Investment incentives, broadly defined as governmental provisions that lead to money-valued advantages for investors, have increased in importance over the last three decades (e. g., CCSI 2015). They are granted to both domestic and foreign firms and have taken on an increasing variety of forms (see chapter 2 in this volume). Some investment incentives are general in nature. Others are very specific and are granted only in case of a particular type of investment. Investment incentives frequently take the form of higher public expenditures or lower tax revenues. They also can be public guarantees (e. g., loan guarantees) and are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Incentives in the European Union</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gugler</surname>
                           <given-names>Philippe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>Both the European Union (EU) and its individual Member States deploy many strategies to strengthen the attractiveness of their business environment. Fostering investment and innovation is seen as a necessary step to increase competitiveness. Competition for investment occurs at the global level (OECD 2013; Guimón 2007) and between Member States within the EU (Oxelheim and Ghauri 2004). State incentives are among the numerous measures adopted by a country (or jurisdiction within it) to increase its attractiveness to investors. State aid is governed by a strong regulatory framework within the EU that is aimed at limiting destructive competition for investment. The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Incentives in the United States</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Krakoff</surname>
                           <given-names>Charles</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Steele</surname>
                           <given-names>Chris</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>122</fpage>
                  <abstract>
                     <p>This chapter explores federal, state, and county/municipal investment incentives in the United States. It examines data on specific investments and incentives awards as well as macro-level data on investments by state and metropolitan area, with a view to gauging the correlations, if any, between the incentives granted and the investment outcomes achieved, as measured in total investment, employment creation, higher wages, and/or sustainability of investment, among other factors. In addition to looking at the value of incentives, we seek to evaluate the relative effects of different kinds of incentives, such as exemptions from property taxes or income taxes, accelerated depreciation</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Tax Incentives Around the World</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>James</surname>
                           <given-names>Sebastian</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>In 2013, Jamaica, under severe fiscal pressure, agreed to undertake comprehensive tax reform and eliminated many of its generous and discretionary tax incentives for investment (Collister 2013). On the passing of the legislation, the minister of justice, Senator Mark Golding, said, “The Jamaican economy has not been well served by the existing regime of sector-based incentives. The consensus is that such incentives may have been partly responsible for Jamaica’s lackluster record of growth by encouraging the misallocation of limited economic resources in our country.” He also explained that the reform would ensure that an equitable rules-based system is created for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>A Holistic Approach to Investment Incentives</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Brennan</surname>
                           <given-names>Louis</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ruane</surname>
                           <given-names>Frances</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>179</fpage>
                  <abstract>
                     <p>As the process of globalization took off in the 1990s, countries on every continent began to look to foreign direct investment (FDI) as a means to propel growth. For some countries, such as Singapore, the Netherlands, and Ireland, this belief in the growth-enhancing potential of FDI represented a further intensification of existing strategies; for others, such as Poland, Hungary, and Vietnam, it marked a completely new approach to development. Many countries have begun to view inward investment positively as a source of new capital, ideas, and access to networks and markets—reflecting a change from an earlier stance that was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Investment Incentives for Sustainable Development</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zhan</surname>
                           <given-names>James</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Karl</surname>
                           <given-names>Joachim</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>204</fpage>
                  <abstract>
                     <p>Governments widely use investment incentives, including fiscal, financial, and regulatory incentives, to further certain policy objectives—above all, job creation, technology and skills transfer, and research and development (R&amp; D). According to UNCTAD’s Investment Policy Monitor Database, between 2009 and 2013 investment incentives measures comprised 39 percent of all investment promotion and facilitation measures adopted during this period.¹ A recent UNCTAD survey of investment promotion agencies (IPAs) found that fiscal incentives are the most frequently used type of incentives for attracting and benefiting from foreign investment, whereas financial and regulatory incentives are less commonly used for these purposes.²</p>
                     <p>Despite their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>Cost-Benefit Analysis of Investment Incentives</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Harpel</surname>
                           <given-names>Ellen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>228</fpage>
                  <abstract>
                     <p>Global competition for investment and the desire to increase the quantity and quality of jobs within their jurisdictions have led governments to expand their use of economic development incentives. These incentives are intended to influence where private companies invest and set up operations in order to generate net benefits for the jurisdiction hosting those operations and financing the incentives. This chapter addresses steps governments can take to assess both costs and benefits in order to make sound decisions when providing incentives to investors.</p>
                     <p>Governments providing incentives are increasingly concerned that the benefits they expect to receive after offering incentives packages</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>Regulation of Investment Incentives:</title>
                     <subtitle>NATIONAL AND SUBNATIONAL EFFORTS TO REGULATE COMPETITION FOR INVESTMENT THROUGH THE USE OF INCENTIVES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Thomas</surname>
                           <given-names>Kenneth P.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>251</fpage>
                  <abstract>
                     <p>Investment incentives have substantial potential drawbacks, particularly in terms of reduced economic efficiency; increased post-tax, post-transfer inequality; and sometimes even contributions to environmentally harmful projects (Thomas 2011a). Yet, as we know, government use of incentives is pervasive and increasing, as documented in chapter 7 in this volume. Interestingly, this is recognized by the governments themselves, a good example being that of the National Governors Association (NGA; Kayne and Shonka 1994) in the United States. In a report on state development policies and programs, the NGA argued that state governments should reduce their use of such incentives because it was bad</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 12</label>
                     <title>Regulation of Investment Incentives:</title>
                     <subtitle>INSTRUMENTS AT AN INTERNATIONAL/SUPRANATIONAL LEVEL</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Johnson</surname>
                           <given-names>Lise</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>264</fpage>
                  <abstract>
                     <p>Over the past several decades, as countries have increasingly opened their markets, cross-border flows of foreign direct investment (FDI) have grown rapidly, albeit sometimes erratically. With that overall rise in FDI flows, we have also seen a growth in competition for capital. Nations and subnational jurisdictions are adopting a range of strategies to court new investors and encourage existing ones to stay and even expand their operations. As detailed elsewhere in this volume, those strategies often entail offering investors a range of potentially costly incentives.</p>
                     <p>To date, as described in chapter II in this volume, national regulatory regimes seeking to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.16</book-part-id>
                  <title-group>
                     <label>CHAPTER 13</label>
                     <title>Conclusions:</title>
                     <subtitle>OUTSTANDING ISSUES ON THE DESIGN AND IMPLEMENTATION OF INCENTIVES POLICIES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Johnson</surname>
                           <given-names>Lise</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Toledano</surname>
                           <given-names>Perrine</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sachs</surname>
                           <given-names>Lisa</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tavares-Lehmann</surname>
                           <given-names>Ana Teresa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>323</fpage>
                  <abstract>
                     <p>As we saw in the introductory chapter, in 2015 governments set landmark priorities and commitments to ensure long-term sustainable development. The United Nations adopted a set of sustainable development goals (SDGs) to set the post-2015 development agenda, including ending poverty and hunger; ensuring access to affordable, reliable, and modern energy; and promoting inclusive and sustainable economic growth. The Conference of the Parties of the UN Framework Convention similarly established a new binding agreement aimed at tackling the challenges of climate change. Implementing these global agreements will require the strategic and coordinated use of public and private resources. Public capital will</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.17</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>329</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.18</book-part-id>
                  <title-group>
                     <title>CONTRIBUTORS</title>
                  </title-group>
                  <fpage>331</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/tava17298.19</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>335</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
