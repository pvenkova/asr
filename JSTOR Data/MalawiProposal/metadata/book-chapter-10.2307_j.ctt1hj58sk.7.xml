<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1hj58sk</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Gender and the Millennium Development Goals</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>Sweetman</surname>
               <given-names>Caroline</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>12</month>
         <year>2005</year>
      </pub-date>
      <isbn content-type="ppub">9780855985509</isbn>
      <isbn content-type="epub">9780855987329</isbn>
      <publisher>
         <publisher-name>Oxfam</publisher-name>
         <publisher-loc>Oxford</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2005</copyright-year>
         <copyright-holder>Oxfam GB</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1hj58sk"/>
      <abstract abstract-type="short">
         <p>This collection focuses on the Millennium Development Goals from a gender perspective. It examines the strengths and weaknesses of this way of understanding and addressing poverty, and suggests ways of strengthening the approach by using key insights and approaches associated with the struggle to establish and uphold the rights of women.</p>
      </abstract>
      <counts>
         <page-count count="112"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>[i]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>1</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.3</book-part-id>
                  <title-group>
                     <title>Editorial</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sweetman</surname>
                           <given-names>Caroline</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>2</fpage>
                  <abstract>
                     <p>In 2000, the leaders and heads of state of 189 countries signed the Millennium Declaration, which set a series of targets for global action against poverty by 2015. The Millennium Development Goals (MDGs) are the result of this process. Meeting the MDGs would not end economic poverty; but meeting them could make a positive difference to millions of women, men, and children. In the past decade, 59 countries - predominantly in sub-Saharan Africa and the former Soviet Union -have slid further down the poverty ladder, as they contend with HIV/AIDS, conflict, and enormous foreign debts (UNDP 2004).</p>
                     <p>In 2005, existing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.4</book-part-id>
                  <title-group>
                     <title>Making the links:</title>
                     <subtitle>women’s rights and empowerment are key to achieving the Millennium Development Goals</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Heyzer</surname>
                           <given-names>Noeleen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>The Millennium Declaration, adopted by all UN Member States in 2000, outlines a vision of freedom from want and freedom from fear. Together with the eight Millennium Development Goals (MDGs), which make that vision concrete, the Millennium Declaration commits states to ‘promote gender equality and the empowerment of women as effective ways to combat poverty, hunger, disease and to stimulate development that is truly sustainable’ (UN 2000a, 5).</p>
                     <p>The recognition that women’s equality and rights are central to achieving economic and social priorities is important. But it is not by chance that this has come about. It is the result</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.5</book-part-id>
                  <title-group>
                     <title>Gender equality and women’s empowerment:</title>
                     <subtitle>a critical analysis of the third Millennium Development Goal</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kabeer</surname>
                           <given-names>Naila</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>Gender equality and women’s empowerment is the third of eight MDGs. It is an intrinsic rather than an instrumental goal, explicitly valued as an end in itself rather than as an instrument for achieving other goals. Important as education is, the translation of this goal into the target of eliminating gender disparities at all levels of education within a given time period is disappointingly narrow. However, the indicators to monitor progress in achieving the goal are somewhat more wideranging:</p>
                     <p>closing the gender gap in education at all levels;</p>
                     <p>increasing women’s share of wage employment in the non-agricultural sector;</p>
                     <p>and increasing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.6</book-part-id>
                  <title-group>
                     <title>Where to for women’s movements and the MDGs?</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Barton</surname>
                           <given-names>Carol</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>Women’s organisations¹ have had an ambivalent relationship with the MDGs since their inception. When leaders gathered at the UN Millennium Summit in 2000 and agreed on the Millennium Declaration, many women’s organisations were engaged in a pitched battle to defend the Cairo Programme of Action (the output of the UN International Conference on Population and Development) and the Beijing Platform for Action (the output of the UN Fourth World Conference on Women) from right-wing assaults, which took place at the five-year reviews of the accords in the same year. Few women’s rights activists were focused on the Millennium Summit.</p>
                     <p>When</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.7</book-part-id>
                  <title-group>
                     <title>Approaches to reducing maternal mortality:</title>
                     <subtitle>Oxfam and the MDGs</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fraser</surname>
                           <given-names>Arabella</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>36</fpage>
                  <abstract>
                     <p>Oxfam has adopted the MDGs as a framework for action in 2005 and beyond, in the belief that they represent an unprecedented opportunity to combat global poverty and suffering. The commitments made by developed and developingcountry governments in 2000, the constellation of events in 2005 (in particular the UN Special Assembly on MDG progress), and the political momentum that this is now generating provide civil society organisations with a critical opportunity to effect change. Undoubtedly, the aims of the MDGs fall far short of the eradication of global poverty and suffering: it is estimated that reaching the Goal 1 target</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.8</book-part-id>
                  <title-group>
                     <title>The education MDGs:</title>
                     <subtitle>achieving gender equality through curriculum and pedagogy change</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Aikman</surname>
                           <given-names>Sheila</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Unterhalter</surname>
                           <given-names>Elaine</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Challender</surname>
                           <given-names>Chloe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>44</fpage>
                  <abstract>
                     <p>Today, millions of girls who attend school are the first in their families ever to do so. Yet for many of them, gender inequality is not only a feature of the political, economic, and social conditions in which they live, but often pervades their educational experience.</p>
                     <p>At the Millennium Summit of the UN, Millennium Development Goal (MDG) 3 was broadly framed to ‘promote gender equality and empower women’. Within the Goal, the target relating to education was set in terms of eliminating gender disparity in primary and secondary education preferably by 2005 and in all levels by 2015 (www.un.org /millenniumgoals).</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.9</book-part-id>
                  <title-group>
                     <title>Not a sufficient condition:</title>
                     <subtitle>the limited relevance of the gender MDG to women’s progress</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Johnson</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>56</fpage>
                  <abstract>
                     <p>So begins the UNDP<italic>Human Development Report</italic>(HDR) for 2003, subtitled<italic>Millennium Development Goals: a compact among nations to end human poverty.</italic>It is not clear what is ‘unprecedented’ about such a declaration, in the wake of failed commitments to 0.7 percent GDP official development assistance by developed states, the poverty-reduction undertakings of the 1995 World Summit on Social Development, the actual effects on poor people of structural adjustment programmes, the International Monetary Fund’s (IMF’s) Poverty Reduction and Growth Facility, the World Bank’s Poverty Reduction Strategy Plan, and other global declarations and initiatives concerning poverty. Perhaps it is that this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.10</book-part-id>
                  <title-group>
                     <title>Out of the margins:</title>
                     <subtitle>the MDGS through a CEDAW lens</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hayes</surname>
                           <given-names>Ceri</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>The inclusion of the goal to ‘promote gender equality and empower women’ (Goal 3) in the MDGs demonstrates the impact of many years of lobbying by the women’s movement to promote gender equality and women’s human rights in development. Nevertheless, many gender activists have expressed concern that the MDGs fail to represent the vision and commitment to gender equality and women’s empowerment that are set out in key human rights instruments, such as CEDAW, and outcome documents of intergovernment conferences of the 1990s. The most notable of these outcome documents is the Beijing Declaration and Platform for Action (1995).</p>
                     <p>Given</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.11</book-part-id>
                  <title-group>
                     <title>Linking women’s human rights and the MDGs:</title>
                     <subtitle>an agenda for 2005 from the UK Gender and Development Network</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Painter</surname>
                           <given-names>Genevieve Renard</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>The year 2005 is a key moment for women’s human rights advocacy, because the Beijing Platform for Action (BPFA), the Millennium Declaration, and the MDGs will be reviewed.</p>
                     <p>To prepare for the 2005 reviews, the UK’s Gender and Development Network (GADN) commissioned research on the conceptual and practical links between the review processes.² The GADN believes that the 2005 reviews are an arena for emphasising the centrality of a women’s human rights approach to development. They are an opportunity to reclaim gender mainstreaming as a strategy to achieving women’s human rights, grounded in treaty obligations, not a technical process for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.12</book-part-id>
                  <title-group>
                     <title>Critiquing the MDGs from a Caribbean perspective</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Antrobus</surname>
                           <given-names>Peggy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>Overall, as a feminist I think of the MDGs as a Major Distraction Gimmick - a distraction from the much more important Platforms for Action from the UN conferences of the 1990s, in Rio 1992 (Environment), Vienna 1993 (Human Rights), Cairo 1994 (Population), Copenhagen (Social Development) and Beijing 1995 (Women), Istanbul 1996 (Habitats), and Rome 1997 (Food), on which the MDGs are based. But despite believing this, I think it worthwhile to join other activists within women’s movements who are currently developing strategies to try to ensure that the MDGs can be made to work to promote women’s equality and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj58sk.13</book-part-id>
                  <title-group>
                     <title>Resources</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="editor" id="contrib1" xlink:type="simple">
                        <role>Compiled by</role>
                        <name name-style="western">
                           <surname>Lang</surname>
                           <given-names>Kanika</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>105</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
