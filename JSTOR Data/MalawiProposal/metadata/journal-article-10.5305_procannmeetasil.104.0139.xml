<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">procannmeetasil</journal-id>
         <journal-title-group>
            <journal-title content-type="full" xml:lang="en">Proceedings<italic>of the</italic>104th Annual Meeting</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Society for International Law</publisher-name>
         </publisher>
         <issn pub-type="ppub">02725037</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">procannmeetasil.104.0139</article-id>
         <article-id pub-id-type="doi">10.5305/procannmeetasil.104.0139</article-id>
         <article-categories>
            <subj-group>
               <subject>Hot Topics in GATS and Human Rights</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>“Predatory Globalization”: The WTO Agreement on Trade in Services, Migration, and Public Health in Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>Obijiofor Aginam</string-name>
            </contrib>
            <bio xlink:type="simple">
               <p>
                  <sup>*</sup>Director of Studies, Policy &amp; Institutional Frameworks, United Nations University headquarters, Tokyo; Adjunct Research Professor of Law, Carleton University, Ottawa, Canada.</p>
            </bio>
         </contrib-group>
         <pub-date pub-type="ppub">
                <day>24</day>
                <month>3</month>
                <year>2010</year>
                <string-date>2010</string-date>
            </pub-date>
         <pub-date pub-type="ppub">
                <day>27</day>
                <month>3</month>
                <year>2010</year>
            </pub-date>
         <volume>104</volume>
         <issue-id>procannmeetasil.104</issue-id>
         <fpage>139</fpage>
         <lpage>146</lpage>
         <permissions>
            <copyright-statement>Copyright 2011 The American Society of International Law</copyright-statement>
            <copyright-year>2011</copyright-year>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.5305/procannmeetasil.104.0139"/>
         <abstract>
            <p>Several characteristics of the global political economy shape the tactics and aspirations of progressive social forces: extreme hierarchy and unevenness of circumstances, acute deprivation and mass misery among the poor; erosion of autonomy at the level of the state as a consequence of the play of non-territorial capital forces.</p>
            <p>—Richard Falk<sup>1</sup>
            </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>Footnotes</title>
         <ref id="fn1">
            <label>1</label>
            <mixed-citation publication-type="other">
               <source>Predatory Globalization: A Critique</source>(<year>1999</year>).</mixed-citation>
         </ref>
         <ref id="fn2">
            <label>2</label>
            <mixed-citation publication-type="other">
               <italic>Id.</italic>
            </mixed-citation>
         </ref>
         <ref id="fn3">
            <label>3</label>
            <mixed-citation publication-type="other">
               <source>Comm'n on Macroeconomics &amp; Health, Macroeconomics &amp; Health: Investing in Health for Economic Development</source>
               <fpage>75</fpage>(<year>2001</year>) [hereinafter Macroeconomics &amp; Health].</mixed-citation>
         </ref>
         <ref id="fn4">
            <label>4</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Alfonson Mejia</string-name>
               </person-group>et al.,<source>Physician &amp; Nurse Migration: Analysis &amp; Policy Implications</source>(<year>1979</year>).</mixed-citation>
         </ref>
         <ref id="fn5">
            <label>5</label>
            <mixed-citation publication-type="other">
               <italic>See, e.g</italic>.,<person-group person-group-type="author">
                  <string-name>John Hilary</string-name>
               </person-group>,<source>The Wrong Model: GATS, Trade Liberalisation &amp; Children's Right to Health</source>(<year>2001</year>);<person-group person-group-type="author">
                  <string-name>Scott Sinclair</string-name>
               </person-group>,<source>GATS: How the World Trade Organization's “New Services” Negotiations Threaten Democracy</source>(<year>2000</year>),<italic>available at</italic>
               <uri xlink:href="http://www.ratical.com/co-globalize/GATSsummary.pdf">http://www.ratical.com/co-globalize/GATSsummary.pdf</uri>;<collab>Scott Sinclair &amp; Jim Grieshaber-Otto</collab>,<source>Facing the Facts: A Guide to the GATS Debate</source>(<year>2002</year>).</mixed-citation>
         </ref>
         <ref id="fn6">
            <label>6</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Omar B. Ahmad</string-name>
               </person-group>,<article-title>
                  <italic>Brain Drain: The Flight of Human Capital</italic>
               </article-title>,<volume>82</volume>
               <source>Bull. World Health Org</source>.<fpage>797</fpage>–<lpage>98</lpage>(<year>2004</year>).</mixed-citation>
         </ref>
         <ref id="fn7">
            <label>7</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Falk</string-name>
               </person-group>,<italic>supra</italic>note 1, at<fpage>13</fpage>,<fpage>17</fpage>.</mixed-citation>
         </ref>
         <ref id="fn8">
            <label>8</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Fatoumata Jawara</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Aileen Kwa</string-name>
               </person-group>,<source>Behind the Scenes at the WTO: The Real World of International Trade Negotiations</source>(<year>2003</year>).</mixed-citation>
         </ref>
         <ref id="fn9">
            <label>9</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>David Woodward</string-name>
               </person-group>,<article-title>
                  <italic>The GATS and Trade in Health Services: Implications for Health in Developing Countries</italic>
               </article-title>,<volume>12</volume>
               <source>Rev. Int'l Pol. Econ.</source>
               <fpage>511</fpage>,<fpage>511</fpage>–<lpage>34</lpage>(<year>2005</year>);<conf-name>Conference on Trade &amp; Development &amp; the World Health Organization</conf-name>,<month>June</month>
               <year>1997</year>;<source>International Trade in Health Services: A Development Perspective</source>(<person-group person-group-type="editor">
                  <string-name>Simonetta Zarrilli</string-name>
               </person-group>&amp;<person-group person-group-type="editor">
                  <string-name>Colette Kinnon</string-name>
               </person-group>eds.,<year>1998</year>), UNCTAD/ITCD/TSB/5 WHO/TFHE/98.1 [hereinafter International Trade];<source>Trade in Health Services: Global, Regional, and Country Perspectives</source>(<person-group person-group-type="editor">
                  <string-name>Nick Drager</string-name>
               </person-group>&amp;<person-group person-group-type="editor">
                  <string-name>Cesar Vieira</string-name>
               </person-group>eds.,<year>2002</year>),<italic>available at</italic>
               <uri xlink:href="http://www.who.int/trade/resource/THS/en/index.html">http://www.who.int/trade/resource/THS/en/index.html</uri>[hereinafter Trade in Health Services];<person-group person-group-type="author">
                  <string-name>Rupa Chanda</string-name>
               </person-group>,<article-title>
                  <italic>Trade in Health Services</italic>
               </article-title>,<italic>in</italic>
               <source>Trade in Health Services</source>,<italic>Id.</italic>, at<fpage>35</fpage>–<lpage>44</lpage>.</mixed-citation>
         </ref>
         <ref id="fn10">
            <label>10</label>
            <mixed-citation publication-type="other">On the evolution of the international trading system, see<person-group person-group-type="author">
                  <string-name>John Jackson</string-name>
               </person-group>,<source>The World Trading System: Law &amp; Policy of International Economic Relations</source>(2d ed.<year>1997</year>);<person-group person-group-type="author">
                  <string-name>Michael J. Trebilcock</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Robert Howse</string-name>
               </person-group>,<source>The Regulation of International Trade</source>(3d ed.<year>2005</year>).</mixed-citation>
         </ref>
         <ref id="fn11">
            <label>11</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Kamal Malhotra</string-name>
               </person-group>et al.,<source>Making Global Trade Work for People</source>
               <fpage>3</fpage>(<year>2003</year>).</mixed-citation>
         </ref>
         <ref id="fn12">
            <label>12</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Naomi Hamada</string-name>
               </person-group>et al.,<article-title>
                  <italic>International Flow of Zambian Nurses</italic>
               </article-title>,<volume>7</volume>
               <source>Hum. Resources Health</source>
               <fpage>1</fpage>,<fpage>1</fpage>–<lpage>5</lpage>(<year>2009</year>) (discussing the changing patterns of outward migration of Zambian nurses and various visa schemes in their destination countries).</mixed-citation>
         </ref>
         <ref id="fn13">
            <label>13</label>
            <mixed-citation publication-type="other">
               <source>Macroeconomics &amp; Health</source>,<italic>supra</italic>note 3, at<fpage>76</fpage>.</mixed-citation>
         </ref>
         <ref id="fn14">
            <label>14</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Stephen Lewis</string-name>
               </person-group>,<source>Race Against Time: Searching for Hope in AIDS-Ravaged Africa</source>
               <volume>48</volume>(<year>2005</year>).</mixed-citation>
         </ref>
         <ref id="fn15">
            <label>15</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Chanda</string-name>
               </person-group>,<italic>supra</italic>note 9, at<fpage>64</fpage>.</mixed-citation>
         </ref>
         <ref id="fn16">
            <label>16</label>
            <mixed-citation publication-type="other">
               <collab>World Bank</collab>,<source>World Development Report 1993: Investing in Health</source>(<year>1993</year>).</mixed-citation>
         </ref>
         <ref id="fn17">
            <label>17</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Orvill Adams</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Colette Kinnon</string-name>
               </person-group>,<article-title>
                  <italic>A Public Health Perspective</italic>
               </article-title>,<italic>in</italic>
               <source>International Trade</source>,<italic>supra</italic>note 9, at<fpage>35</fpage>–<lpage>54</lpage>.</mixed-citation>
         </ref>
         <ref id="fn18">
            <label>18</label>
            <mixed-citation publication-type="other">
               <italic>Id.</italic>at<fpage>37</fpage>(citing<person-group person-group-type="author">
                  <string-name>S. Ababulgu</string-name>
               </person-group>,<source>Problem of Physician Migration in Ethiopia</source>(<year>1997</year>) (unpublished document)).</mixed-citation>
         </ref>
         <ref id="fn19">
            <label>19</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Adams</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Kinnon</string-name>
               </person-group>,<italic>supra</italic>note 17, at<fpage>37</fpage>(citing<collab>Volta Regional Research Team</collab>,<source>The Doctors Are Out—Where Are They?</source>(<year>1997</year>) (unpublished document)).</mixed-citation>
         </ref>
         <ref id="fn20">
            <label>20</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Chanda</string-name>
               </person-group>,<italic>supra</italic>note 9, at<fpage>22</fpage>(citing<person-group person-group-type="author">
                  <string-name>D. Kaplan</string-name>
               </person-group>et al.,<article-title>
                  <italic>Brain Drain: New Data, New Options</italic>
               </article-title>,<volume>11</volume>
               <source>Trade &amp; Indus. Monitor</source>(<year>1999</year>)).</mixed-citation>
         </ref>
         <ref id="fn21">
            <label>21</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Chanda</string-name>
               </person-group>,<italic>supra</italic>note 9, at<fpage>22</fpage>(citing<person-group person-group-type="author">
                  <string-name>Ashraf Khalil</string-name>
               </person-group>,<article-title>
                  <italic>Unchecked Exodus</italic>
               </article-title>,<source>Cairo Times</source>,<month>April</month>
               <fpage>1</fpage>–<lpage>14</lpage>,<year>1999</year>, at<fpage>3</fpage>:<fpage>3</fpage>).</mixed-citation>
         </ref>
         <ref id="fn22">
            <label>22</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Chanda</string-name>
               </person-group>,<italic>supra</italic>note 9, at<fpage>22</fpage>(citing<person-group person-group-type="author">
                  <string-name>Augustine Oyowe</string-name>
               </person-group>,<article-title>
                  <italic>Brain Drain: Colossal Loss of Investment for Developing Countries</italic>
               </article-title>,<volume>159</volume>
               <source>Courier ACP-EU</source>
               <fpage>59</fpage>–<lpage>60</lpage>(<year>1996</year>)).</mixed-citation>
         </ref>
         <ref id="fn23">
            <label>23</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Ike Anya</string-name>
               </person-group>et al.,<article-title>
                  <italic>Searching the World: Following Three Graduating Classes of a Nigerian Medical School</italic>
               </article-title>,<italic>in</italic>
               <person-group person-group-type="author">
                  <string-name>Toyin Falola</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Niyi Afolabi</string-name>
               </person-group>,<source>The Human Cost of African Migrations</source>
               <fpage>79</fpage>–<lpage>88</lpage>(<year>2007</year>).</mixed-citation>
         </ref>
         <ref id="fn24">
            <label>24</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>David Woodward</string-name>
               </person-group>,<article-title>
                  <italic>The GATS and Trade in Health Services: Implications for Health Care in Developing Countries</italic>
               </article-title>,<volume>12</volume>
               <source>Rev. Int'l Pol. Econ.</source>
               <fpage>511</fpage>,<fpage>518</fpage>(<year>2005</year>).</mixed-citation>
         </ref>
         <ref id="fn25">
            <label>25</label>
            <mixed-citation publication-type="other">I use the term “sub-Saharan Africa” only in direct reference to a seminal work where the term is used. The term, now firmly embedded in most global development discourses, balkanizes the African continent by cutting off its northern part and projects most of Black Africa as poor, undeveloped, and a reservoir of diseases, wars, and conflicts. According to Asante: there is neither an Africa north of the Sahara nor an Africa south of the Sahara … the Sahara is in Africa and human populations have inhabited the Sahara for thousands of years. It is useless to speak of Africa separated by deserts, as it is to speak of separations by rain forests.<italic>See</italic>
               <person-group person-group-type="author">
                  <string-name>Molefi K. Asante</string-name>
               </person-group>,<source>Kemet, Afrocentricity, and Knowledge</source>
               <fpage>33</fpage>(<year>1990</year>).</mixed-citation>
         </ref>
         <ref id="fn26">
            <label>26</label>
            <mixed-citation publication-type="other">
               <collab>World Health Organization</collab>,<source>WHO Estimates of Health Personnel: Physicians, Nurses, Mid-wives, Dentists and Pharmacists (Around 1998)</source>(<year>2002</year>).</mixed-citation>
         </ref>
         <ref id="fn27">
            <label>27</label>
            <mixed-citation publication-type="other">
               <collab>WTO Secretariat</collab>,<source>Trade in Services Division, An Introduction to the GATS</source>(<year>1999</year>).</mixed-citation>
         </ref>
         <ref id="fn28">
            <label>28</label>
            <mixed-citation publication-type="other">
               <source>General Agreement on Tariffs and Trade</source>,<month>Oct.</month>
               <day>30</day>,<year>1947</year>,<fpage>61</fpage>Stat. A-11, 55 UNTS 194, art. XXI.</mixed-citation>
         </ref>
         <ref id="fn29">
            <label>29</label>
            <mixed-citation publication-type="other">
               <collab>WTO Secretariat</collab>,<source>GATS—Facts and Fiction</source>(<year>2001</year>),<italic>available at</italic>
               <uri xlink:href="http://www.wto.org/english/tratop_e/serv_e/gatsfacts1004_e.pdf">http://www.wto.org/english/tratop_e/serv_e/gatsfacts1004_e.pdf</uri>.</mixed-citation>
         </ref>
         <ref id="fn30">
            <label>30</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>David P. Fidler</string-name>
               </person-group>et al.,<article-title>
                  <italic>Making Commitments in Health Services Under the GATS: Legal Dimensions</italic>
               </article-title>,<italic>in</italic>
               <source>International Trade in Health Services and the GATS: Current Issues and Debates</source>
               <fpage>143</fpage>(<person-group person-group-type="editor">
                  <string-name>Chantal Blouin</string-name>
               </person-group>et al. eds.,<year>2006</year>).</mixed-citation>
         </ref>
         <ref id="fn31">
            <label>31</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Chanda</string-name>
               </person-group>,<italic>supra</italic>note 9, at<fpage>20</fpage>.<italic>See also Id.</italic>at<fpage>35</fpage>–<lpage>44</lpage>.</mixed-citation>
         </ref>
         <ref id="fn32">
            <label>32</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Dilip Ratha</string-name>
               </person-group>,<article-title>
                  <italic>Workers' Remittances: An Important and External Source of External Development Finance</italic>
               </article-title>,<italic>in</italic>
               <source>Global Development Finance 2003: Striving for Stability in Development Finance</source>
               <fpage>157</fpage>–<lpage>75</lpage>(<year>2003</year>).</mixed-citation>
         </ref>
         <ref id="fn33">
            <label>33</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Rudolf Adlung</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Antonia Carzaniga</string-name>
               </person-group>,<article-title>
                  <italic>Health Services Under the General Agreement on Trade in Services</italic>
               </article-title>,<italic>in</italic>
               <source>Trade in Health Services</source>,<italic>supra</italic>note 9, at<fpage>13</fpage>–<lpage>34</lpage>.</mixed-citation>
         </ref>
         <ref id="fn34">
            <label>34</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Ted Schrecker</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Ronald Labonte</string-name>
               </person-group>,<article-title>
                  <italic>Taming the Brain Drain: A Challenge for Public Health Systems in Southern Africa</italic>
               </article-title>,<volume>10</volume>
               <source>Int'l J. Occupational &amp; Envtl. Health</source>
               <fpage>410</fpage>(<year>2002</year>).</mixed-citation>
         </ref>
         <ref id="fn35">
            <label>35</label>
            <mixed-citation publication-type="journal">
               <italic>Id.</italic>;<person-group person-group-type="author">
                  <string-name>Stephen Bach</string-name>
               </person-group>,<article-title>
                  <italic>International Migration of Health Workers: Labor and Social Issues</italic>
               </article-title>
               <volume>13</volume>(<source>Int'l Labour Office</source>,<comment>Working Paper No. 209</comment>,<year>2003</year>),<italic>at</italic>
               <uri xlink:href="http://www.citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.90.4214&amp;#x00026;rep=rep1&amp;#x00026;type=pdf">http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.90.4214&amp;rep=rep1&amp;type=pdf</uri>.</mixed-citation>
         </ref>
         <ref id="fn36">
            <label>36</label>
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Schrecker</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Labonte</string-name>
               </person-group>,<italic>supra</italic>note 34, at<fpage>412</fpage>.</mixed-citation>
         </ref>
         <ref id="fn37">
            <label>37</label>
            <mixed-citation publication-type="other">
               <italic>See, e.g.</italic>,<collab>Commonwealth Secretariat</collab>,<source>Companion Document to the Commonwealth Code of Practice for International Recruitment of Health Workers</source>(<year>2003</year>).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
