<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jbusiethi</journal-id>
         <journal-id journal-id-type="jstor">j50000129</journal-id>
         <journal-title-group>
            <journal-title>Journal of Business Ethics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">01674544</issn>
         <issn pub-type="epub">15730697</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25074668</article-id>
         <title-group>
            <article-title>International Validation of the Corruption Perceptions Index: Implications for Business Ethics and Entrepreneurship Education</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Paul G.</given-names>
                  <surname>Wilhelm</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i25074664</issue-id>
         <fpage>177</fpage>
         <lpage>189</lpage>
         <permissions>
            <copyright-statement>Copyright 2002 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25074668"/>
         <abstract>
            <p>International government and corporate corruption is increasingly under siege. Although various groups of researchers have quantified and documented world-wide corruption, apparently no one has validated the measures. This study finds a very strong significant correlation of three measures of corruption with each other, thereby indicating validity. One measure was of Black Market activity, another was of overabundance of regulation or unnecessary restriction of business activity. The third measure was an index based on interview perceptions, of corruption (Corruption Perceptions Index or CPI) in that nation. Validity of the three measures was further established by finding a highly significant correlation with real gross domestic product per capita (RGDP/Cap). The CPI had by far the strongest correlation with RGDP/Cap, explaining over three fourths of the variance. Corruption is increasingly argued to be a barrier to development and economic growth. Business students often do not see ethics courses as being as relevant as other value-free disciplines or core courses. The data in this study suggests otherwise. Sustainable economic development appears very dependent on a constant, virtuous cycle that includes corruption fighting, and the maintenance of trust and innovation, all reinforcing each other.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1024e114a1310">
            <mixed-citation id="d1024e118" publication-type="other">
Acemoglu, D. and T. Verdier: 2000, 'The Choice
between Market Failures and Corruption', The
American Economic Review 90(1), 194-211.</mixed-citation>
         </ref>
         <ref id="d1024e131a1310">
            <mixed-citation id="d1024e135" publication-type="other">
Aggarwal, R.: 1997, 'Ethical Issues in Finance', in
P. H. Werhane and E. R. Freeman (eds.), The
Blackwell Encyclopedic Dictionary of Business Ethics
(Blackwell, Maiden, MA).</mixed-citation>
         </ref>
         <ref id="d1024e151a1310">
            <mixed-citation id="d1024e155" publication-type="other">
Amba-Rao: 1993, 'Multinational Corporate Social
Responsibility, Ethics, Interactions and Third
World Governments: An Agenda for the 1990s',
Journal of Business Ethics 12, 75-94.</mixed-citation>
         </ref>
         <ref id="d1024e171a1310">
            <mixed-citation id="d1024e175" publication-type="other">
Amba-Rao: 1997, 'Business Ethics in Developing
Countries', in P. H. Werhane and E. R. Freeman
(eds.), The Blackwell Encyclopedic Dictionary of
Business Ethics (Blackwell, Maiden, MA).</mixed-citation>
         </ref>
         <ref id="d1024e192a1310">
            <mixed-citation id="d1024e196" publication-type="other">
Ball, D. A. and W. H. McCulloch, Jr.: 1996,
International Business: The Challenge of Global
Competition (Irwin, Chicago, IL).</mixed-citation>
         </ref>
         <ref id="d1024e209a1310">
            <mixed-citation id="d1024e213" publication-type="other">
Barney, J. B. and Hansen, M. H.: 1994,
'Trustworthiness as a Source of Competitive
Advantage', Strategic Management Journal 15,
175-190.</mixed-citation>
         </ref>
         <ref id="d1024e229a1310">
            <mixed-citation id="d1024e233" publication-type="other">
Barrier, M.: 1999, 'Doing the Right Thing', in E H.
Maidment (ed.), Annual Editions: Management,
Pushkin/McGraw-Hill, Guilford, CT).</mixed-citation>
         </ref>
         <ref id="d1024e246a1310">
            <mixed-citation id="d1024e250" publication-type="other">
Barton, L.: 1995, Ethics: The Enemy in the Workplace
(Southwestern College Publishing, Cincinnati,
OH).</mixed-citation>
         </ref>
         <ref id="d1024e263a1310">
            <mixed-citation id="d1024e267" publication-type="other">
Boulding, K. F.: 1969, 'Economics as a Moral
Science', American Economic Review 59(4), 1-23.</mixed-citation>
         </ref>
         <ref id="d1024e277a1310">
            <mixed-citation id="d1024e281" publication-type="other">
Burton, S., Johnston, M. W. and E. J. Wilson: 1991,
'An Experimental Assessment of Alternative
Teaching Approaches for Introducing Business
Ethics to Undergraduate Business Students', Journal
of Business Ethics 10(7), 507-517.</mixed-citation>
         </ref>
         <ref id="d1024e301a1310">
            <mixed-citation id="d1024e307" publication-type="other">
Coicaud, J-M. and D. Warner: 2001, 'Reflection on
the Extent and Limits of Contemporary
International Ethics', in J-M. Coicaud and D.
Warner (eds.), Ethics and International Affairs: Extents
and Limits (United Nations University Press,
Tokyo, Japan).</mixed-citation>
         </ref>
         <ref id="d1024e330a1310">
            <mixed-citation id="d1024e334" publication-type="other">
Coutinho de Arruda, M. C: 1997, 'Business Ethics
in South America', in P. H. Werhane and R. E.
Freeman (eds.), The Blackwell Encyclopedic Dictionary
of Business Ethics (Blackwell, Maiden, MA).</mixed-citation>
         </ref>
         <ref id="d1024e350a1310">
            <mixed-citation id="d1024e354" publication-type="other">
Crespo, R. R: 1998, 'Controversy: Is Economics a
Moral Science?', Journal of Markets and Morality
1(2), 201-211.</mixed-citation>
         </ref>
         <ref id="d1024e367a1310">
            <mixed-citation id="d1024e371" publication-type="other">
DeBow, M.: 1999, 'The End of the World as We
Know It?: Some Thoughts on the Economics of
Moral Decline', The Journal of Private Enterprise
14(2), 41-56.</mixed-citation>
         </ref>
         <ref id="d1024e387a1310">
            <mixed-citation id="d1024e391" publication-type="other">
DeGeorge, R.: 1993, Competing with Integrity in
International Business (Oxford University Press,
New York).</mixed-citation>
         </ref>
         <ref id="d1024e404a1310">
            <mixed-citation id="d1024e408" publication-type="other">
DeGeorge, R. T.: 1999, Business Ethics (Prentice Hall,
Upper Saddle River, NJ).</mixed-citation>
         </ref>
         <ref id="d1024e419a1310">
            <mixed-citation id="d1024e423" publication-type="other">
Dienhart, J. W: 2000, Business, Institutions, and Ethics:
A Text with Cases and Readings (Oxford University
Press, New York).</mixed-citation>
         </ref>
         <ref id="d1024e436a1310">
            <mixed-citation id="d1024e440" publication-type="other">
Etzioni, A.: 1993, The Spirit of Community: Rights,
Responsibilities, and the Communitarian Agenda
(Crown, New York).</mixed-citation>
         </ref>
         <ref id="d1024e453a1310">
            <mixed-citation id="d1024e457" publication-type="other">
Friedman, M.: 1966, Essays in Positive Economics
(Chicago Press).</mixed-citation>
         </ref>
         <ref id="d1024e467a1310">
            <mixed-citation id="d1024e471" publication-type="other">
Garten, J. E.: 1999, 'Troubles Ahead in Emerging
Markets', in F. Maidment (ed.), Annual Editions:
International Business, 1999/2000 (Dushkin/
McGraw-Hill, Guilford, CT).</mixed-citation>
         </ref>
         <ref id="d1024e487a1310">
            <mixed-citation id="d1024e491" publication-type="other">
Gatewood, R. D. and H. S. Field: 1998, Human
Resource Selection (Dryden Press, Fort Worth, TX).</mixed-citation>
         </ref>
         <ref id="d1024e501a1310">
            <mixed-citation id="d1024e505" publication-type="other">
Goldsmith, A. A.: 1995, 'Democracy, Property Rights
and Economic Growth', Journal of Development
Studies 32(2), 157-175.</mixed-citation>
         </ref>
         <ref id="d1024e519a1310">
            <mixed-citation id="d1024e523" publication-type="other">
Griffin, R. W. and M. W. Pustay: 1999, International
Business: A Managerial Perspective (Addison Wesley,
Reading, MA).</mixed-citation>
         </ref>
         <ref id="d1024e536a1310">
            <mixed-citation id="d1024e540" publication-type="other">
Harper, M.: 1991, 'Enterprise Development in Poorer
Nations', Entrepreneurship Theory and Practice 15,
7-11.</mixed-citation>
         </ref>
         <ref id="d1024e553a1310">
            <mixed-citation id="d1024e557" publication-type="other">
Jain, A. K.: 1998, 'Corruption: An Introduction', in
A. K. Jain (ed.), Economics of Corruption (Kluwer
Academic Publishers, Boston, MA).</mixed-citation>
         </ref>
         <ref id="d1024e570a1310">
            <mixed-citation id="d1024e574" publication-type="other">
Johnson, B. T., K. R. Holmes and M. Kirkpatrick:
1998, Index of Economic Freedom (Heritage
Foundation, Washington, DC).</mixed-citation>
         </ref>
         <ref id="d1024e587a1310">
            <mixed-citation id="d1024e591" publication-type="other">
Johnston, J. P.: 1998, 'Value Added: A
Communications Assignment that Develops Ethical
Perception', Business Communications Quarterly
61(4), 121-123.</mixed-citation>
         </ref>
         <ref id="d1024e607a1310">
            <mixed-citation id="d1024e613" publication-type="other">
Jones, T. M. (1995). Instrumental stakeholder theory:
A Synthesis of Ethics and Economies', Academy of
Management Review 20, 404-437.</mixed-citation>
         </ref>
         <ref id="d1024e627a1310">
            <mixed-citation id="d1024e631" publication-type="other">
Kehoe, W. J.: 1999, 'GATT and WTO Facilitating
Global Trade', in F. Maidment (ed.), Annual
Edition: Management, 1999/2000 (Dushkin/
McGraw-Hill, Guilford, CT).</mixed-citation>
         </ref>
         <ref id="d1024e647a1310">
            <mixed-citation id="d1024e651" publication-type="other">
Korsgaard, M., D. Schweiger and H. Sapienza: 1995,
'Building Commitment, Attachment, and Trust in
Strategic Decision-making Teams: The Role of
Procedural Justice', Academy of Management Journal
38, 60-84.</mixed-citation>
         </ref>
         <ref id="d1024e670a1310">
            <mixed-citation id="d1024e674" publication-type="other">
Kurtenbach, E.: 1999, 'Rampant Corruption
Wrecking Construction of Modern China',
Associated Press. Anniston Star. January 4. 2D.</mixed-citation>
         </ref>
         <ref id="d1024e687a1310">
            <mixed-citation id="d1024e691" publication-type="other">
Lane, H. W, J. J. DiStefano and Maznevski: 1997,
International Management Behavior (Blackwell
Publishers, Cambridge, MA).</mixed-citation>
         </ref>
         <ref id="d1024e704a1310">
            <mixed-citation id="d1024e708" publication-type="other">
Lee, K.: 1998, 'Business Ethics in China:
Confucianism, Socialist Market Economy and the
Multinational Enterprises', in B. J. Kumar and H.
Steinman (eds.), Ethics in International Management
(de Gruyter, New York).</mixed-citation>
         </ref>
         <ref id="d1024e727a1310">
            <mixed-citation id="d1024e731" publication-type="other">
Martin, D. W: 1997, 'Business Ethics in Russia', in
P. H. Werhane and R. E. Freeman (eds.), The
Blackwell Encyclopedic Dictionary of Business Ethics
(Blackwell, Maiden, MA).</mixed-citation>
         </ref>
         <ref id="d1024e748a1310">
            <mixed-citation id="d1024e752" publication-type="other">
Mauro, P.: 1995, 'Corruption and Growth', The
Quarterly Journal of Economics 110(3), 681-712.</mixed-citation>
         </ref>
         <ref id="d1024e762a1310">
            <mixed-citation id="d1024e766" publication-type="other">
Mayer. R. C, J. H. Davis and F. D. Schoorman: 1995,
'An Integrative Model of Organizational Trust',
Academy of Management Review 20, 709-734.</mixed-citation>
         </ref>
         <ref id="d1024e779a1310">
            <mixed-citation id="d1024e783" publication-type="other">
McAllister, D. J.: 1995, 'Affect- and Cognition-based
Trust as Foundations for Interpersonal Cooperation
in Organizations', Academy of Management Journal
38, 24-59.</mixed-citation>
         </ref>
         <ref id="d1024e799a1310">
            <mixed-citation id="d1024e803" publication-type="other">
McCarthy, M. J.: 1999, 'An Ex-divinity Student
Works on Searching the Corporate Soul', Wall
Street Journal (June 18), Bl.</mixed-citation>
         </ref>
         <ref id="d1024e816a1310">
            <mixed-citation id="d1024e820" publication-type="other">
McDonald, G.: 2000, 'Cross Cultural Methodological
Issues in Ethical Research', Journal of Business Ethics
27(1/2), 89-104.</mixed-citation>
         </ref>
         <ref id="d1024e833a1310">
            <mixed-citation id="d1024e837" publication-type="other">
Melloan, G.: 1999, 'Examining the Engine of
Economic Growth', The Wall Street Journal (January
12), A23.</mixed-citation>
         </ref>
         <ref id="d1024e851a1310">
            <mixed-citation id="d1024e855" publication-type="other">
Nair, S. R.: 1998, 'Doing Business in China: It's Far
from Easy', in F. Maidment (ed.), Annual Editions:
International Business (Dushkin/McGraw-Hill,
Guilford, CT).</mixed-citation>
         </ref>
         <ref id="d1024e871a1310">
            <mixed-citation id="d1024e875" publication-type="other">
Oddo, A. R.: 1997, 'A Framework for Teaching
Business Ethics', Journal of Business Ethics 16(3),
293-297.</mixed-citation>
         </ref>
         <ref id="d1024e888a1310">
            <mixed-citation id="d1024e892" publication-type="other">
Parfit, M.: 1996, 'Mexico City: Pushing the Limits',
National Geographic 190(2), 24-43.</mixed-citation>
         </ref>
         <ref id="d1024e902a1310">
            <mixed-citation id="d1024e906" publication-type="other">
Phillips, M. M.: 1999, 'World Bank Says it Failed to
Confront Indonesian Corruption, Financial Ills',
Wall Street Journal (February 12), A15.</mixed-citation>
         </ref>
         <ref id="d1024e919a1310">
            <mixed-citation id="d1024e923" publication-type="other">
Roepke, W.: 1996, The Moral Foundations of Civil
Society, translated by C. S. Fox (Transaction
Publishers, New Brunswick, NJ).</mixed-citation>
         </ref>
         <ref id="d1024e936a1310">
            <mixed-citation id="d1024e940" publication-type="other">
Rousseau, D., S. Sitkin, R. Burt and C. Camerer:
1998, 'Not so Different after All: A Cross-disci-
pline View of Trust', Academy of Management
Review 23, 393-404.</mixed-citation>
         </ref>
         <ref id="d1024e957a1310">
            <mixed-citation id="d1024e961" publication-type="other">
Shellenbarger, S.: 1999, 'Companies Declare Lofty
Employee Values, Then Forget to Act', Wall Street
Journal (June 16), Bl.</mixed-citation>
         </ref>
         <ref id="d1024e974a1310">
            <mixed-citation id="d1024e978" publication-type="other">
Society for Human Resource Management: 1998,
'International Attention Turns to Corruption',
Workplace Visions (July/August), 4-5.</mixed-citation>
         </ref>
         <ref id="d1024e991a1310">
            <mixed-citation id="d1024e995" publication-type="other">
Solberg, J., K. C. Strong and C. McGuirre, Jr.: 1995,
'Living (not Learning) Ethics', Journal of Business
Ethics 14(1), 71-81.</mixed-citation>
         </ref>
         <ref id="d1024e1008a1310">
            <mixed-citation id="d1024e1012" publication-type="other">
Solomon, C. M.: 1998, 'Put Your Ethics to a Global
Test', in F. Maidment (ed.), Annual Editions:
International Business, 1998/99 (Dushkin/McGraw-
Hill, Guilford, CT).</mixed-citation>
         </ref>
         <ref id="d1024e1028a1310">
            <mixed-citation id="d1024e1032" publication-type="other">
Solomon, R. C: 1998, 'The Moral Psychology
of Business: Care and Compassion in the
Corporation', Business Ethics Quarterly 8(3),
515-533.</mixed-citation>
         </ref>
         <ref id="d1024e1048a1310">
            <mixed-citation id="d1024e1052" publication-type="other">
Thomas, C. W: 1998, 'What's so Special about
International Fraud?', Today's CPA 25(3), 10.</mixed-citation>
         </ref>
         <ref id="d1024e1063a1310">
            <mixed-citation id="d1024e1067" publication-type="other">
Torstensson, J.: 1994, 'Property Rights and Economic
Growth: An Empirical Study', Kyklos 47, 231-247.</mixed-citation>
         </ref>
         <ref id="d1024e1077a1310">
            <mixed-citation id="d1024e1081" publication-type="other">
Transparency International: 1998, TI Press Release:
1998 Corruption Perceptions Index, www.
transparency.de/documents/press releases/1998/
1998.09.22.cpi.html.</mixed-citation>
         </ref>
         <ref id="d1024e1097a1310">
            <mixed-citation id="d1024e1101" publication-type="other">
Transparency International: 1997a, TI Press Release:
1997 Corruption Perceptions Index, www. trans-
parency, de/documents/press releases/1997/1997.
31.7.cpi.html.</mixed-citation>
         </ref>
         <ref id="d1024e1117a1310">
            <mixed-citation id="d1024e1121" publication-type="other">
Transparency International: 1997b, Corruption
Perceptions Index Sources, www.transparency.de/
documents/cpi/cpi-sources.html.</mixed-citation>
         </ref>
         <ref id="d1024e1134a1310">
            <mixed-citation id="d1024e1138" publication-type="other">
Tsui, J. and C. Windsor: 2001, 'Some Cross-cultural
Evidence on Ethical Reasoning', Jo urnal of Business
Ethics 31(2), 143-150.</mixed-citation>
         </ref>
         <ref id="d1024e1151a1310">
            <mixed-citation id="d1024e1155" publication-type="other">
United Nations Development Programme: 1996,
Human Development Report (Oxford University
Press, New York, NY).</mixed-citation>
         </ref>
         <ref id="d1024e1169a1310">
            <mixed-citation id="d1024e1173" publication-type="other">
Wartick, S. L. and D. J. Wood: 1998, International
Business and Society (Blackwell, Maiden).</mixed-citation>
         </ref>
         <ref id="d1024e1183a1310">
            <mixed-citation id="d1024e1187" publication-type="other">
Werhane, P. H. and R. E. Freeman: 1999, 'Business
Ethics: The State of the Art', International Journal
of Management Reviews 1(1), 1-16.</mixed-citation>
         </ref>
         <ref id="d1024e1200a1310">
            <mixed-citation id="d1024e1204" publication-type="other">
Werhane, P. H. and R. E. Freeman: 1998, 'Moral
Imagination and the Search for Ethical Decision-
making in Management', Business Ethics Quarterly
8 (Special Issue No. 1), 75-98.</mixed-citation>
         </ref>
         <ref id="d1024e1220a1310">
            <mixed-citation id="d1024e1224" publication-type="other">
Wicks, A. C, S. L. Berman and T. M. Jones: 1999,
'The Structure of Optimal Trust: Moral and
Strategic Implications', Academy of Management
Review 24(1), 99-116.</mixed-citation>
         </ref>
         <ref id="d1024e1240a1310">
            <mixed-citation id="d1024e1244" publication-type="other">
Wiehen, M. H.: 1998, 'Corruption in International
Business Relations', in B.N. Kumar and H.
Steinman (eds.), Ethics in International Management.
(de Gruyter, New York).</mixed-citation>
         </ref>
         <ref id="d1024e1260a1310">
            <mixed-citation id="d1024e1264" publication-type="other">
World Bank: 1999, World Development Report (Oxford,
New York, NY).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
