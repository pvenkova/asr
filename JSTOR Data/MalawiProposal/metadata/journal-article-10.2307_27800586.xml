<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">globgove</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000814</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Global Governance</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Lynn Rienner Publishers</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10752846</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19426720</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27800586</article-id>
         <title-group>
            <article-title>The Political Economy of Food Aid in an Era of Agricultural Biotechnology</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jennifer</given-names>
                  <surname>Clapp</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2005</year>
      
            <day>1</day>
            <month>12</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27800580</issue-id>
         <fpage>467</fpage>
         <lpage>485</lpage>
         <permissions>
            <copyright-statement>© 2005 Lynne Rienner Publishers</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27800586"/>
         <abstract>
            <p>Recent years have seen numerous rejections of food aid containing genetically modified organisms (GMOs). The United States, as the principal donor of this aid, went on the defensive and blamed the European Union for hunger in developing countries. Rarely is food aid rejected. And rarely do food aid donors act so strongly to blame other donors. The reaction of both donors and recipients is also puzzling because it contradicts much of the literature from the 1990s that argued that the international food aid regime had become largely "depoliticized" following reforms to food aid policies in the 1980s. The current literature on food aid has not adequately addressed the ways in which the advent of GMOs has affected the food aid regime. I argue that scientific debates over the safety of GMOs, and economic factors tied to the market for genetically modified crops—both highly political issues—are extremely relevant to current debates on food aid.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d911e126a1310">
            <label>1</label>
            <mixed-citation id="d911e133" publication-type="other">
Robert Falkner, "Regulating Biotech Trade: The
Cartagena Protocol on Biosafety," International Affairs 76, no. 2 (2000)</mixed-citation>
            <mixed-citation id="d911e142" publication-type="other">
Peter
Newell and Ruth Mackenzie, "The 2000 Cartagena Protocol on Biosafety:
Legal and Political Dimensions," Global Environmental Change 10 (2000)</mixed-citation>
            <mixed-citation id="d911e154" publication-type="other">
Cristoph Bail, Robert Falkner, and Helen Marquard, eds., The Cartegena Proto-
col on Biosafety: Reconciling Trade in Biotechnology with Environment and
Development? (London: Earthscan, 2002).</mixed-citation>
         </ref>
         <ref id="d911e167a1310">
            <label>2</label>
            <mixed-citation id="d911e174" publication-type="other">
Peter Uvin, "Regime, Surplus and Self-interest: The International Poli-
tics of Food Aid," International Studies Quarterly 36, no. 3 (1992): 293–312</mixed-citation>
            <mixed-citation id="d911e183" publication-type="other">
Raymond Hopkins, "Reform in the International Food Aid Regime: The Role of
Consensual Knowledge," International Organization 46, no. 1 (1992): 225–
264</mixed-citation>
            <mixed-citation id="d911e195" publication-type="other">
Raymond Hopkins, "The Evolution of Food Aid: Towards a Development-
First Regime," in Vernan Ruttan, ed., Why Food Aid? (Baltimore: Johns Hop-
kins University Press, 1993)</mixed-citation>
            <mixed-citation id="d911e208" publication-type="other">
Edward Clay and Olav Stokke, eds., Food Aid
and Human Security (London: Frank Cass, 2000).</mixed-citation>
         </ref>
         <ref id="d911e218a1310">
            <label>3</label>
            <mixed-citation id="d911e225" publication-type="other">
Cheryl Christiansen, "The New Policy Environment for Food Aid: The
Challenge of Sub-Saharan Africa," Food Policy 25, no. 3 (2000): 256.</mixed-citation>
         </ref>
         <ref id="d911e235a1310">
            <label>4</label>
            <mixed-citation id="d911e242" publication-type="other">
US AID, Food Aid and Food Security Policy Paper (PN-ABU-219)
(Washington, D.C.: USAID, 1995).</mixed-citation>
         </ref>
         <ref id="d911e253a1310">
            <label>5</label>
            <mixed-citation id="d911e260" publication-type="other">
Harriet Friedmann, "The Political Economy of Food: The Rise and Fall
of the Postwar International Food Order," in M. Burawoy and T. Skocpol, eds.,
Marxist Inquiries: Studies of Labour, Class and States, supplement to American
Journal of Sociology 88 (1982): S248–286</mixed-citation>
            <mixed-citation id="d911e275" publication-type="other">
Vernan Ruttan, "The Politics of
U.S. Food Aid Policy: A Historical Review," in Ruttan, Why Food Aid?</mixed-citation>
         </ref>
         <ref id="d911e285a1310">
            <label>6</label>
            <mixed-citation id="d911e292" publication-type="other">
Mitchel Wallerstein, Food for War—Food for Peace (Cambridge: MIT
Press, 1980); Shlomo Reutlinger, "From 'Food Aid' to 'Aid for Food': Into the
21st Century," Food Policy 24, no. 1 (1999): 9.</mixed-citation>
         </ref>
         <ref id="d911e305a1310">
            <label>7</label>
            <mixed-citation id="d911e312" publication-type="other">
John Cathie, European Food Aid Policy (Aldershot,
England: Ashgate, 1997)</mixed-citation>
            <mixed-citation id="d911e321" publication-type="other">
Mark Charlton, The Making of
Canadian Food Aid Policy (Montreal: McGill-Queens University Press, 1992).</mixed-citation>
         </ref>
         <ref id="d911e331a1310">
            <label>8</label>
            <mixed-citation id="d911e338" publication-type="other">
International Grains Council (IGC), IGC Annual Report (London: IGC,
2004).</mixed-citation>
         </ref>
         <ref id="d911e348a1310">
            <label>9</label>
            <mixed-citation id="d911e355" publication-type="other">
International Wheat Council, The Food Aid Convention of the Inter-
national Wheat Agreement (London: International Wheat Council, 1991).</mixed-citation>
         </ref>
         <ref id="d911e365a1310">
            <label>10</label>
            <mixed-citation id="d911e372" publication-type="other">
Food Aid Convention, Food Aid Convention (London: International
Grains Council, 1999).</mixed-citation>
         </ref>
         <ref id="d911e383a1310">
            <label>11</label>
            <mixed-citation id="d911e390" publication-type="other">
Christiansen, "The New Policy Environment for Food Aid.</mixed-citation>
         </ref>
         <ref id="d911e397a1310">
            <label>12</label>
            <mixed-citation id="d911e404" publication-type="other">
Christopher Barrett and D. Maxwell, Food Aid After Fifty Years:
Recasting Its Role (London: Routledge, 2005)</mixed-citation>
            <mixed-citation id="d911e413" publication-type="other">
EU Council Regulation (ED)
No. 1292/96.</mixed-citation>
         </ref>
         <ref id="d911e423a1310">
            <label>13</label>
            <mixed-citation id="d911e430" publication-type="other">
Edward Clay and Olav Stokke, eds., Food Aid Re-
considered: Assessing the Impact on Third World Countries (London: Frank
Cass, 1991).</mixed-citation>
         </ref>
         <ref id="d911e443a1310">
            <label>14</label>
            <mixed-citation id="d911e450" publication-type="other">
Uvin, "Regime, Surplus and Self-Interest"</mixed-citation>
            <mixed-citation id="d911e456" publication-type="other">
Hopkins, "Reform in the
International Food Aid Regime," and "The Evolution of Food Aid."</mixed-citation>
         </ref>
         <ref id="d911e466a1310">
            <label>15</label>
            <mixed-citation id="d911e473" publication-type="other">
Uvin, "Regime, Surplus and Self-interest," pp. 307–308.</mixed-citation>
         </ref>
         <ref id="d911e480a1310">
            <label>16</label>
            <mixed-citation id="d911e487" publication-type="other">
Eric Neumayer, "Is the Allocation of Food Aid Free from Donor Inter-
est Bias?" Journal of Development Studies 41, no. 3 (2005).</mixed-citation>
         </ref>
         <ref id="d911e498a1310">
            <label>17</label>
            <mixed-citation id="d911e505" publication-type="other">
International Service for the Acquisition of Agri-Biotech Applications,
Preview: Global Status of Commercialized Transgenic Crops: 2003, Executive
Summary No. 30, pp. 3–4, available online at www.isaaa.org.</mixed-citation>
         </ref>
         <ref id="d911e518a1310">
            <label>18</label>
            <mixed-citation id="d911e525" publication-type="other">
USAID, "United States and Food Assistance," Africa Humanitarian
Crisis (3 July 2003), available online at www.usaid.gov/about/africafoodcrisis/
bio_answers.html#8.</mixed-citation>
         </ref>
         <ref id="d911e538a1310">
            <label>19</label>
            <mixed-citation id="d911e545" publication-type="other">
Geoffrey Lean, "Rejected GM Food Dumped on the Poor," The Inde-
pendent (London), 18 June 2000; Fred Pearce, "UN Is Slipping Modified Food
into Aid," New Scientist 175, no. 2361 (2003): 5.</mixed-citation>
         </ref>
         <ref id="d911e558a1310">
            <label>20</label>
            <mixed-citation id="d911e565" publication-type="other">
Friends of the Earth International (FOEI), Playing with Hunger: The
Reality Behind the Shipment of GMOs as Food Aid (Amsterdam: FOEI, April
2003), p. 5.</mixed-citation>
         </ref>
         <ref id="d911e578a1310">
            <label>21</label>
            <mixed-citation id="d911e585" publication-type="other">
Ibid., pp. 6–7</mixed-citation>
            <mixed-citation id="d911e591" publication-type="other">
ACDI/VOCA, Genetically Modified Food: Implications
for US Food Aid Programs, 2d ed. (April 2003), pp. 6–10, available online at
www.acdivoca.org/acdivoca/acdiweb2.nsf/news/gmfoodsarticle.</mixed-citation>
         </ref>
         <ref id="d911e604a1310">
            <label>22</label>
            <mixed-citation id="d911e611" publication-type="other">
Genetic Resources Action International (GRAIN) "Better Dead than
GM Fed?" Seedling (October 2002).</mixed-citation>
         </ref>
         <ref id="d911e622a1310">
            <label>23</label>
            <mixed-citation id="d911e629" publication-type="other">
Oxfam
International, "Crisis in Southern Africa," Oxfam Briefing Paper 23 (2002), p. 6.</mixed-citation>
         </ref>
         <ref id="d911e639a1310">
            <label>24</label>
            <mixed-citation id="d911e646" publication-type="other">
WFP, Policy on Donations of Foods Derived from Biotechnology (GM/
Biotech Foods), WFP/EB, 3/2002/4-C (14 October 2002), pp. 4–5.</mixed-citation>
         </ref>
         <ref id="d911e656a1310">
            <label>25</label>
            <mixed-citation id="d911e663" publication-type="other">
Institute for the Study of International Migration, Genetically Modified
Food in the Southern Africa Food Crisis of 2002–2003 (Washington, D.C.:
Georgetown University School of Foreign Service, 2004), pp. 16–17.</mixed-citation>
         </ref>
         <ref id="d911e676a1310">
            <label>26</label>
            <mixed-citation id="d911e683" publication-type="other">
Jon Bennett, "Food Aid Logistics and the Southern Africa Emergency,"
Forced Migration Review 18, no. 5 (2003): 29.</mixed-citation>
         </ref>
         <ref id="d911e693a1310">
            <label>27</label>
            <mixed-citation id="d911e700" publication-type="other">
FAO, FAO Statistical Database, available online at
http://faostat.fao.org (accessed October 2004).</mixed-citation>
         </ref>
         <ref id="d911e710a1310">
            <label>28</label>
            <mixed-citation id="d911e717" publication-type="other">
Rob Crilly, "Children Go Hungry as GM Food Rejected," The Herald
(Glasgow), 30 October 2002, p. 12.</mixed-citation>
         </ref>
         <ref id="d911e728a1310">
            <label>29</label>
            <mixed-citation id="d911e735" publication-type="other">
Bennett, "Food Aid Logistics and the Southern Africa Emergency," p. 29.</mixed-citation>
         </ref>
         <ref id="d911e742a1310">
            <label>30</label>
            <mixed-citation id="d911e749" publication-type="other">
ACDI/VOCA, Genetically Modified Food, p. 9; USAID "United States
and Food Assistance."</mixed-citation>
         </ref>
         <ref id="d911e759a1310">
            <label>31</label>
            <mixed-citation id="d911e766" publication-type="other">
Matt Mellen, "Who Is Getting Fed?" Seedling (April 2003), available
online at www.grain.org/seedline/seed-03-04-3-en.cfm.</mixed-citation>
         </ref>
         <ref id="d911e776a1310">
            <label>32</label>
            <mixed-citation id="d911e783" publication-type="other">
Chuck Grassley, "Salvation or Starvation? GMO Food Aid to Africa,"
remarks of Senator Chuck Grassley to the Congressional Economic Leadership
Institute, 5 March 2003.</mixed-citation>
         </ref>
         <ref id="d911e796a1310">
            <label>33</label>
            <mixed-citation id="d911e803" publication-type="other">
Norman Borlaug, "Science vs. Hysteria," Wall Street Journal, 22 Jan-
uary 2003.</mixed-citation>
         </ref>
         <ref id="d911e813a1310">
            <label>34</label>
            <mixed-citation id="d911e820" publication-type="other">
Duncan Brack, Robert Falkner, and Judith Göll, "The Next Trade War? GM
Products, the Cartagena Protocol and the WTO," Royal Institute of Inter-
national Affairs Briefing Paper No.8, September 2003.</mixed-citation>
         </ref>
         <ref id="d911e834a1310">
            <label>35</label>
            <mixed-citation id="d911e841" publication-type="other">
Charlotte Denny and Larry Elliott, "French Plan to Aid
Africa Could Be Sunk by Bush," The Guardian, 23 May 2003.</mixed-citation>
         </ref>
         <ref id="d911e851a1310">
            <label>36</label>
            <mixed-citation id="d911e858" publication-type="other">
Edward Alden, "US Beats Egypt with Trade Stick," Financial Times,
29 June 2002.</mixed-citation>
         </ref>
         <ref id="d911e868a1310">
            <label>37</label>
            <mixed-citation id="d911e875" publication-type="other">
European Union, press release IP/03/681, 13 May 2003.</mixed-citation>
         </ref>
         <ref id="d911e882a1310">
            <label>38</label>
            <mixed-citation id="d911e889" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d911e896a1310">
            <label>39</label>
            <mixed-citation id="d911e903" publication-type="other">
Africa Center for Biosafety, Earthlife Africa, Environmental Rights
Action-Friends of the Earth Nigeria, Grain and SafeAge, GE Food Aid: Africa
Denied Choice Once Again? 4 May 2004.</mixed-citation>
         </ref>
         <ref id="d911e916a1310">
            <label>40</label>
            <mixed-citation id="d911e923" publication-type="other">
Heike Baumüller, "Domestic Import Regulations for Genetically Modi-
fied Organisms and Their Compatibility with WTO Rules," Trade Knowledge
Network (Winnepeg: International Institute for Sustainable Development</mixed-citation>
            <mixed-citation id="d911e935" publication-type="other">
Geneva: International Centre for Trade and Sustainable Development, 2003), pp.
13–15, available online at www.tradeknowledgenetwork.org/pdf/tkn_domestic_
regs.pdf.</mixed-citation>
         </ref>
         <ref id="d911e949a1310">
            <label>41</label>
            <mixed-citation id="d911e956" publication-type="other">
FOEI, Playing with Hunger, p. 9.</mixed-citation>
         </ref>
         <ref id="d911e963a1310">
            <label>42</label>
            <mixed-citation id="d911e970" publication-type="other">
Baumüller, "Domestic Import Regulations for Genetically Modified
Organisms," p. 14.</mixed-citation>
         </ref>
         <ref id="d911e980a1310">
            <label>43</label>
            <mixed-citation id="d911e987" publication-type="other">
Bail, Falkner, and
Marquard, The Cartegena Protocol on Biosafety; Falkner, "Regulating Biotech
Trade"; Newell and MacKenzie, "The 2000 Cartagena Protocol on Biosafety."</mixed-citation>
         </ref>
         <ref id="d911e1000a1310">
            <label>45</label>
            <mixed-citation id="d911e1007" publication-type="other">
Codex Alimentarius Commission, Report of the Fourth Session of the
Ad Hoc Intergovernmental Task Force of Foods Derived from Biotechnology,
ALINORM 02/34A (Rome: FAO and WHO, 2003).</mixed-citation>
         </ref>
         <ref id="d911e1020a1310">
            <label>46</label>
            <mixed-citation id="d911e1027" publication-type="other">
WFP, Policy on Donations of Foods Derived from Biotechnology (GM/
Biotech Foods), p. 5.</mixed-citation>
         </ref>
         <ref id="d911e1037a1310">
            <label>47</label>
            <mixed-citation id="d911e1044" publication-type="other">
WFP, Policy on Donations of Foods Derived from Biotechnology,
WFP/EB.A/2003/5-0B/Rev.l (2003).</mixed-citation>
         </ref>
         <ref id="d911e1055a1310">
            <label>48</label>
            <mixed-citation id="d911e1062" publication-type="other">
Aseem Prakash and Kelly Kollman. "Biopolitics in the EU and the
U.S.: A Race to the Bottom or Convergence to the Top?" International Studies
Quarterly 47, no. 4 (2003): 625.</mixed-citation>
         </ref>
         <ref id="d911e1075a1310">
            <label>49</label>
            <mixed-citation id="d911e1082" publication-type="other">
Robert Paarlberg, "The Global Food Fight," Foreign Affairs 79, no. 3
(2000): 24–38.</mixed-citation>
         </ref>
         <ref id="d911e1092a1310">
            <label>50</label>
            <mixed-citation id="d911e1099" publication-type="other">
Grant Isaac and William Kerr, "Genetically Modified Organisms at the
World Trade Organization: A Harvest of Trouble," Journal of World Trade 37,
no. 6: 1086–1090</mixed-citation>
            <mixed-citation id="d911e1111" publication-type="other">
Prakash and Kollman, "Biopolitics in the EU and the U.S.,"
p. 626.</mixed-citation>
         </ref>
         <ref id="d911e1121a1310">
            <label>51</label>
            <mixed-citation id="d911e1128" publication-type="other">
Grassley, "Salvation or Starvation?"</mixed-citation>
         </ref>
         <ref id="d911e1135a1310">
            <label>52</label>
            <mixed-citation id="d911e1142" publication-type="other">
Michael Dynes, "Africa Torn Between GM Aid and Starvation," The
Times (London), 6 August 2002, p. 12.</mixed-citation>
         </ref>
         <ref id="d911e1152a1310">
            <label>53</label>
            <mixed-citation id="d911e1159" publication-type="other">
Rory Carroll, "Zambia Slams Door Shut on GM Food Relief," The
Guardian (London), 30 October 2002</mixed-citation>
            <mixed-citation id="d911e1168" publication-type="other">
Institute for the Study of International
Migration, Genetically Modified Food in the Southern Africa Food Crisis of
2002–2003, p. 20.</mixed-citation>
         </ref>
         <ref id="d911e1182a1310">
            <label>54</label>
            <mixed-citation id="d911e1189" publication-type="other">
Polly Diven, "The Domestic Determinants of US Food Aid Policy,"
Food Policy 26, no. 5 (2001): 471.</mixed-citation>
         </ref>
         <ref id="d911e1199a1310">
            <label>55</label>
            <mixed-citation id="d911e1206" publication-type="other">
Christiansen, "The New Policy Environment for Food Aid," p. 257.</mixed-citation>
         </ref>
         <ref id="d911e1213a1310">
            <label>56</label>
            <mixed-citation id="d911e1220" publication-type="other">
Brack, Faulkner, and Göll, "The Next Trade War?" p. 3.</mixed-citation>
         </ref>
         <ref id="d911e1227a1310">
            <label>57</label>
            <mixed-citation id="d911e1234" publication-type="other">
Katrin Dauenhauer, "Health: Africans Challenge Bush Claim That GM
Food Is Good for Them," SUNS: South-North Development Monitor, No. 5368
(23 June 2003).</mixed-citation>
         </ref>
         <ref id="d911e1247a1310">
            <label>58</label>
            <mixed-citation id="d911e1254" publication-type="other">
WFP official Richard Lee, quoted in "Greenpeace Statement on the
Southern African Food Crisis," Greenpeace UK, November 2002.</mixed-citation>
         </ref>
         <ref id="d911e1264a1310">
            <label>59</label>
            <mixed-citation id="d911e1271" publication-type="other">
National Association of Wheat Growers (NAWG), "Letter to Robert
Zoellick," portions reprinted in the NAWG Weekly Newsletter, 13 February
2004, available online at www.wheatworld.org.</mixed-citation>
         </ref>
         <ref id="d911e1285a1310">
            <label>60</label>
            <mixed-citation id="d911e1292" publication-type="other">
Oxfam America, US Export Credits: Denials and Double Standards
(Washington, D.C.: Oxfam, 2003), p. 8.</mixed-citation>
         </ref>
         <ref id="d911e1302a1310">
            <label>61</label>
            <mixed-citation id="d911e1309" publication-type="other">
Declan Walsh, "America Finds Ready Market for GM Food—The
Hungry," The Independent (London), 30 March, 2000, p. 18.</mixed-citation>
         </ref>
         <ref id="d911e1319a1310">
            <label>62</label>
            <mixed-citation id="d911e1326" publication-type="other">
Katherine Stapp, "Biotech Boom Linked to Development Dollars, Say
Critics," SUNS (3 December 2003).</mixed-citation>
         </ref>
         <ref id="d911e1336a1310">
            <label>63</label>
            <mixed-citation id="d911e1343" publication-type="other">
Section 1543A of the Farm Bill, cited in ACDI/VOCA, Genetically
Modified Food: Implications for US Food Aid Programs, p. 5.</mixed-citation>
         </ref>
         <ref id="d911e1353a1310">
            <label>64</label>
            <mixed-citation id="d911e1360" publication-type="other">
"GM Food Aid," The Ecologist 33, no. 2 (March 2003), p. 46.</mixed-citation>
         </ref>
         <ref id="d911e1367a1310">
            <label>65</label>
            <mixed-citation id="d911e1374" publication-type="other">
African Agricultural Technology Foundation (AATF), "Rationale and
Design of the AATF" (June 2002), available online at www.aftechfound.org/
rationale.php.</mixed-citation>
         </ref>
         <ref id="d911e1388a1310">
            <label>66</label>
            <mixed-citation id="d911e1395" publication-type="other">
Agricultural Biotechnology Support Project II (ABSPII), "Scope and
Activities" (2004), available online at www.absp2.cornell.edu/whatisabsp2.</mixed-citation>
         </ref>
         <ref id="d911e1405a1310">
            <label>67</label>
            <mixed-citation id="d911e1412" publication-type="other">
Mellen, "Who Is Getting Fed?"</mixed-citation>
         </ref>
         <ref id="d911e1419a1310">
            <label>68</label>
            <mixed-citation id="d911e1426" publication-type="other">
Devlin Kuyek, "Past Predicts the Future—GM Crops and African
Farmers," Seedling (October 2002), available online at www.grain.org/seedling/
seed-02-10-3-en.cfm; Mellen, "Who Is Getting Fed?"</mixed-citation>
         </ref>
         <ref id="d911e1439a1310">
            <label>69</label>
            <mixed-citation id="d911e1446" publication-type="other">
Dominic Glover, "GMOs and the Politics of International Trade,"
Democratising Biotechnology: Genetically Modified Crops in Developing Coun-
tries Briefing Series, Briefing 5 (Brighton, England: Institute of Development
Studies, 2003)</mixed-citation>
            <mixed-citation id="d911e1461" publication-type="other">
Brewster Kneen, Farmageddon (Gabriola, B.C.: New Society,
1999)</mixed-citation>
            <mixed-citation id="d911e1470" publication-type="other">
Kuyek, "Past Predicts the Future;" Mellen, "Who Is Getting Fed?"</mixed-citation>
         </ref>
         <ref id="d911e1477a1310">
            <label>70</label>
            <mixed-citation id="d911e1484" publication-type="other">
Jennifer Clapp, "WTO Agricultural Trade Battles and Food Aid," Third
World Quarterly 25, no. 8 (2004).</mixed-citation>
         </ref>
         <ref id="d911e1494a1310">
            <label>71</label>
            <mixed-citation id="d911e1501" publication-type="other">
Institute for the Study of International Migration, Genetically Modified
Food in the Southern Africa Food Crisis of 2002-–003', pp. 18–19.</mixed-citation>
         </ref>
         <ref id="d911e1512a1310">
            <label>72</label>
            <mixed-citation id="d911e1519" publication-type="other">
An exception to this is Barrett and Maxwell, Food Aid After Fifty
Years, who incorporate some discussion of GMOs.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
