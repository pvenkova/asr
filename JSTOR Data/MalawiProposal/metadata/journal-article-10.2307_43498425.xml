<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plansystevol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009192</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant Systematics and Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer-Verlag Gmbh</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03782697</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16156110</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43498425</article-id>
         <title-group>
            <article-title>Does the Platanthera dilatata (Orchidaceae) complex contain cryptic species or continuously variable populations?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Binaya</given-names>
                  <surname>Adhikari</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lisa E.</given-names>
                  <surname>Wallace</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">300</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40138844</issue-id>
         <fpage>1465</fpage>
         <lpage>1476</lpage>
         <permissions>
            <copyright-statement>© Springer-Verlag 2014</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43498425"/>
         <abstract>
            <p>Floral phenotypic traits are expected to reflect evolutionary changes and are used as a reliable basis for species delimitation. However, when traits overlap among populations of newly emerging species, this confounds identification of evolutionarily distinct lineages and reduces taxonomic stability. In this study, we quantified variation in ten floral traits and plastid DNA sequences across 26 populations of Platanthera dilatata (Orchidaceae) in North America to determine geographic structure among populations and to evaluate support for three varieties recognized in the current taxonomy, k-means clustering analysis, in the absence of a priori designation of groups, indicated two morphologically distinct groups. Spur length was the most distinctive character between groups. The group containing larger flowers with longer spurs corresponds to the var. leucostachys and most samples in this group are from western North America. The vars. albiflora and dilatata could not be distinguished within the second group, which exhibited flowers with short to intermediate spurs and include samples from eastern and western North America. Morphological variation in P. dilatata may reflect pollinator-mediated selection, particularly in spur length, which is known to vary in association with pollinators across Platanthera. Significant genetic divergence was observed between the two groups (FST = 0.15; P ≤ 0.001), but we did not find corresponding phylogenetic structure, which may reflect recent divergence and retention of ancestral polymorphisms. Based on these results, we suggest preserving the current intraspecific taxonomy until further studies determine the origin of floral variation and the extent of gene flow between morphologically divergent populations.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d155e199a1310">
            <mixed-citation id="d155e203" publication-type="other">
Ames O (1910) Orchidaceae: illustrations and studies of the family
Orchidaceae. The Genus Habenaria in North America, vol 4.
Merrymount Press, Boston</mixed-citation>
         </ref>
         <ref id="d155e216a1310">
            <mixed-citation id="d155e220" publication-type="other">
Armbruster WS (1985) Patterns of character divergence and the
evolution of reproductive ecotypes of Dalechampia scandens
(Euphorbiaceae). Evolution 39:733-752</mixed-citation>
         </ref>
         <ref id="d155e233a1310">
            <mixed-citation id="d155e237" publication-type="other">
Barrett CF, Freudenstein JV (2011) An integrative approach to
delimiting species in a rare but widespread mycoheterotrophic
orchid. Mol Ecol 20:2771-2786</mixed-citation>
         </ref>
         <ref id="d155e250a1310">
            <mixed-citation id="d155e254" publication-type="other">
Boberg E, Ágren J (2009) Despite their apparent integration, spur
length but not perianth size affects reproductive success in the
moth-pollinated orchid Platanthera bifolia. Funct Ecol
23:1022-1028</mixed-citation>
         </ref>
         <ref id="d155e271a1310">
            <mixed-citation id="d155e275" publication-type="other">
Boland JT (1993) The floral biology of Platanthera dilatata (Pursh.)
Lindl. (Orchidaceae). M. S. Thesis, Memorial University of
Newfoundland, St. Johns, Newfoundland</mixed-citation>
         </ref>
         <ref id="d155e288a1310">
            <mixed-citation id="d155e292" publication-type="other">
Brunsfeld S J, Sullivan J, Soltis DE, Soltis PS (2001) Comparative
phylogeography of Northwestern North America: a synthesis. In:
Silvertown J, Antonovics J (eds) Integrating ecological and
evolutionary processes in a spatial context. Blackwell Science,
Oxford, pp 319-339</mixed-citation>
         </ref>
         <ref id="d155e311a1310">
            <mixed-citation id="d155e315" publication-type="other">
Bulgin NL, Gibbs HL, Vickery P, Baker AJ (2013) Ancestral
polymorphism in genetic markers obscure detection of evolu-
tionarily distinct populations in the endangered Florida grass-
hopper sparrow (Ammodramus savannarum floridanus). Mol
Ecol 12:831-844</mixed-citation>
         </ref>
         <ref id="d155e334a1310">
            <mixed-citation id="d155e338" publication-type="other">
Calinski RB, Harabasz J (1974) A dendrite method for cluster
analysis. Commun Stat 3:1-27</mixed-citation>
         </ref>
         <ref id="d155e348a1310">
            <mixed-citation id="d155e352" publication-type="other">
Catling PM, Catling VR (1991) A synopsis of breeding systems and
pollination in North American orchids. Lindleyana 6:187-210</mixed-citation>
         </ref>
         <ref id="d155e362a1310">
            <mixed-citation id="d155e366" publication-type="other">
Cooper EA, Whittall JB, Hodges SA, Nordborg M (2010) Genetic
variation at nuclear loci fails to distinguish two morphologically
distinct species of Aquilegia. PLoS ONE 5:e8655. doi: 10. 1371/
journal.pone.0008655</mixed-citation>
         </ref>
         <ref id="d155e383a1310">
            <mixed-citation id="d155e387" publication-type="other">
Coyne JA, Orr HA (2004) Speciation. Sinauer Associates Ine,
Sunderland</mixed-citation>
         </ref>
         <ref id="d155e397a1310">
            <mixed-citation id="d155e401" publication-type="other">
Cronquist A (1978) Once again, what is a species? In: Kuntson L (ed)
Biosystematics in agriculture. Alleheld Osmun, Montclair,
pp 3-20</mixed-citation>
         </ref>
         <ref id="d155e414a1310">
            <mixed-citation id="d155e418" publication-type="other">
Darwin CR (1862) On the various contrivances by which British and
foreign orchids are fertilised by insects. John Murray, London</mixed-citation>
         </ref>
         <ref id="d155e428a1310">
            <mixed-citation id="d155e432" publication-type="other">
Darwin CR (1877) The different forms of flowers on plants of the
same species. John Murray, London</mixed-citation>
         </ref>
         <ref id="d155e442a1310">
            <mixed-citation id="d155e446" publication-type="other">
Dayrat B (2005) Towards integrative taxonomy. Biol J Linn Soc
85:407-415</mixed-citation>
         </ref>
         <ref id="d155e456a1310">
            <mixed-citation id="d155e460" publication-type="other">
de Queiroz K (1998) The general lineage concept of species, species
criteria, and the process of speciation: a conceptual unification
and terminological recommendations. In: Howard DJ, Berlocher
SH (eds) Endless forms: species and speciation. Oxford Univer-
sity Press, Oxford, pp 57-75</mixed-citation>
         </ref>
         <ref id="d155e480a1310">
            <mixed-citation id="d155e484" publication-type="other">
de Queiroz K (2007) Species concepts and species delimitation. Syst
Biol 56:879-886</mixed-citation>
         </ref>
         <ref id="d155e494a1310">
            <mixed-citation id="d155e498" publication-type="other">
Dodd M, Silvertown EJ, Chase MW (1999) Phylogenetic analysis of
trait evolution and species diversity variation among angiosperm
families. Evolution 53:732-744</mixed-citation>
         </ref>
         <ref id="d155e511a1310">
            <mixed-citation id="d155e515" publication-type="other">
Doyle JJ, Doyle JL (1987) A rapid DNA isolation procedure for small
amounts of fresh leaf tissue. Phytochem Bull 19:11-15</mixed-citation>
         </ref>
         <ref id="d155e525a1310">
            <mixed-citation id="d155e529" publication-type="other">
Duda RO, Hart PE (1973) Pattern classification and scene analysis.
Wiley, New York</mixed-citation>
         </ref>
         <ref id="d155e539a1310">
            <mixed-citation id="d155e543" publication-type="other">
Ersts PJ (2012) Geographic distance matrix generator (version 1.2.3).
American Museum of Natural History, Center for Biodiversity
and Conservation, http://biodiversityinformatics.amnh.org/open_
source/gdmg. Accessed 20 May 2012</mixed-citation>
         </ref>
         <ref id="d155e559a1310">
            <mixed-citation id="d155e563" publication-type="other">
Excoffier L, Lischer HEL (2010) Arlequin suite ver 3.5: a new series
of programs to perform population genetics analyses under
Linux and Windows. Mol Ecol Resour 10:564-567</mixed-citation>
         </ref>
         <ref id="d155e577a1310">
            <mixed-citation id="d155e581" publication-type="other">
Excoffier L, Smouse PE, Quattro JM (1992) Analysis of molecular
variance inferred from metric distances among DNA haplotype:
applications to human mitochondrial DNA restriction data.
Genetics 131:479-491</mixed-citation>
         </ref>
         <ref id="d155e597a1310">
            <mixed-citation id="d155e601" publication-type="other">
Fenster CB, Armbruster WS, Wilson P, Dudash MR, Thomson JD
(2004) Pollination syndromes and floral specialization. Annu
Rev Ecol Evol S 35:375-403</mixed-citation>
         </ref>
         <ref id="d155e614a1310">
            <mixed-citation id="d155e618" publication-type="other">
Haig SM, Beever EA, Chambers SM, Draheim HM, Dugger BD et al
(2006) Taxonomie considerations in listing subspecies under the
U. S. Endangered Species Act. Conserv Biol 20:1584-1594</mixed-citation>
         </ref>
         <ref id="d155e631a1310">
            <mixed-citation id="d155e635" publication-type="other">
Hapeman JR (1997) Pollination and floral biology of Platanthera
peramoena. Lindleyana 12:19-25</mixed-citation>
         </ref>
         <ref id="d155e645a1310">
            <mixed-citation id="d155e649" publication-type="other">
Hapeman JR, Inoue K (1997) Plant-pollinator interactions and floral
radiation in Platanthera (Orchidaceae). In: Givnish TJ, Sytsma
KJ (eds) Molecular evolution and adaptive radiation. Cambridge
University Press, Cambridge, pp 433-454</mixed-citation>
         </ref>
         <ref id="d155e665a1310">
            <mixed-citation id="d155e669" publication-type="other">
Hennig C (2013) Fpc: flexible procedures for clustering. R package
version 2.1.5. http://cran.r-project.org/web/packages/fpc/index.
html. Accessed 21 May 2013</mixed-citation>
         </ref>
         <ref id="d155e683a1310">
            <mixed-citation id="d155e687" publication-type="other">
Hodges SA, Arnold ML (1995) Spurring plant diversification: are
floral nectar spurs a key innovation? P Roy Soc Lond B Bio
262:343-348</mixed-citation>
         </ref>
         <ref id="d155e700a1310">
            <mixed-citation id="d155e704" publication-type="other">
Huelsenbeck JP, Ronquist F (2001) MRBAYES: Bayesian inference
of phylogeny. Bioinformatics 17:754-755</mixed-citation>
         </ref>
         <ref id="d155e714a1310">
            <mixed-citation id="d155e718" publication-type="other">
Inoue K (1983) Systematics of the genus Platanthera (Orchidaceae)
in Japan and adjacent regions with special reference to pollina-
tion. J Fac Sci U Tokyo 3(13):285-374</mixed-citation>
         </ref>
         <ref id="d155e731a1310">
            <mixed-citation id="d155e735" publication-type="other">
Inoue K (1986) Experimental studies on male and female reproduc-
tive success: effects of variation in spur length and pollinator
activity on Platanthera mandarionorum spp. Hachijoensis
(Orchidaceae). Plant Spec Biol 1:207-215</mixed-citation>
         </ref>
         <ref id="d155e751a1310">
            <mixed-citation id="d155e755" publication-type="other">
Isaac NJB, Mallet J, Mace GM (2004) Taxonomie inflation: its
influence on macroecology and conservation. Trends Ecol Evol
19:464-469</mixed-citation>
         </ref>
         <ref id="d155e768a1310">
            <mixed-citation id="d155e772" publication-type="other">
Kipping JL (1971) Pollination studies of native orchids. M. S. Thesis,
San Francisco State College, San Francisco</mixed-citation>
         </ref>
         <ref id="d155e783a1310">
            <mixed-citation id="d155e787" publication-type="other">
Kölreuter JG (1761) Vorläufige Nachrichten von einigen das Gesch-
lect der Pflanzen betreffenden Versuchen und Beobachtungen.
Gleditschischen Handlung, Leipzig</mixed-citation>
         </ref>
         <ref id="d155e800a1310">
            <mixed-citation id="d155e804" publication-type="other">
Librado P, Rozas J (2009) DnaSP v5: a software for comprehensive
analysis of DNA polymorphism data. Bioinformatics 25: 1451-1452</mixed-citation>
         </ref>
         <ref id="d155e814a1310">
            <mixed-citation id="d155e818" publication-type="other">
Luer CA (1975) The native orchids of the United States and Canada,
excluding Florida. New York Botanical Garden, New York</mixed-citation>
         </ref>
         <ref id="d155e828a1310">
            <mixed-citation id="d155e832" publication-type="other">
Maad J (2000) Phenotypic selection in hawkmoth-pollinated Platan-
thera bifolia : targets and fitness surfaces. Evolution 54:112-123</mixed-citation>
         </ref>
         <ref id="d155e842a1310">
            <mixed-citation id="d155e846" publication-type="other">
Maad J, Alexandersson R (2004) Variable selection in Platanthera
bifolia (Orchidaceae): phenotypic selection differed between sex
functions in a drought year. J Evolution Biol 17:642-650</mixed-citation>
         </ref>
         <ref id="d155e859a1310">
            <mixed-citation id="d155e863" publication-type="other">
Maad J, Nilsson LA (2004) On the mechanism of floral shifts in
speciation: gained pollination efficiency from tongue-to eye-
attachment of pollinia in Platanthera (Orchidaceae). Biol J Linn
Soc 83:481-495</mixed-citation>
         </ref>
         <ref id="d155e880a1310">
            <mixed-citation id="d155e884" publication-type="other">
Mantel N (1967) The detection of disease clustering and a generalized
regression approach. Cancer Res 27:209-220</mixed-citation>
         </ref>
         <ref id="d155e894a1310">
            <mixed-citation id="d155e898" publication-type="other">
Mayden RL (1997) A hierarchy of species concepts: the document of
the species problem. In: Claridge MF, Dawah HA, Wilson MR
(eds) The units of biodiversity -species in practice, vol 54.
Systematics Association, Chapman and Hall Ltd, London,
p 381-424</mixed-citation>
         </ref>
         <ref id="d155e917a1310">
            <mixed-citation id="d155e921" publication-type="other">
Mayfield JA, Reiber GE, Maynard C, Czerniecki JM, Caps MT,
Sangeorzan BJ (2001) Survival following lower-limb amputation
in a veteran population. J Rehabil Res Dev 38:341-345</mixed-citation>
         </ref>
         <ref id="d155e934a1310">
            <mixed-citation id="d155e938" publication-type="other">
McDade LA (1995) Species concepts and problems in practice:
insights from botanical monographs. Syst Bot 20:606-622</mixed-citation>
         </ref>
         <ref id="d155e948a1310">
            <mixed-citation id="d155e952" publication-type="other">
Medina R, Lara F, Goffinet B, Garilleti R, Mazimpaka V (2012)
Integrative taxonomy of the disjunct epiphytic moss Orthotri-
chum consimile s.l. (Orthotrichaceae). Taxon 61:1180-1198</mixed-citation>
         </ref>
         <ref id="d155e965a1310">
            <mixed-citation id="d155e969" publication-type="other">
Mims MC, Hulsey CD, Fitzpatrick BM, Streelman JT (2010)
Geography disentangles introgression from ancestral polymor-
phism in Lake Malawi cichlids. Mol Ecol 19:940-951</mixed-citation>
         </ref>
         <ref id="d155e983a1310">
            <mixed-citation id="d155e987" publication-type="other">
Moritz C (1994) Applications of mitochondrial DNA analysis in
conservation: a critical review. Mol Ecol 3:401-411</mixed-citation>
         </ref>
         <ref id="d155e997a1310">
            <mixed-citation id="d155e1001" publication-type="other">
Muller K (2005) SeqState -primer design and sequence statistics for
phylogenetic DNA data sets. Appi Bioinformatics 4:65-69</mixed-citation>
         </ref>
         <ref id="d155e1011a1310">
            <mixed-citation id="d155e1015" publication-type="other">
Naomi SI (2011) On the integrated frameworks of species concepts:
Mayden's hierarchy of species concepts and de Queiroz' s unified
concept of species. J Zool Syst Evol Res 49:177-184</mixed-citation>
         </ref>
         <ref id="d155e1028a1310">
            <mixed-citation id="d155e1032" publication-type="other">
Nei M (1987) Molecular evolutionary genetics. Columbia University
Press, New York</mixed-citation>
         </ref>
         <ref id="d155e1042a1310">
            <mixed-citation id="d155e1046" publication-type="other">
Nei M, Tajima F (1987) Problems arising in phylogenetic inference
from restriction-site data. Mol Biol Evol 4:320-323</mixed-citation>
         </ref>
         <ref id="d155e1056a1310">
            <mixed-citation id="d155e1060" publication-type="other">
Nilsson LA (1978) Pollination ecology and adaptation in Platanthera
chlorantha (Orchidaceae). Bot Notiser 131:35-51</mixed-citation>
         </ref>
         <ref id="d155e1071a1310">
            <mixed-citation id="d155e1075" publication-type="other">
Nilsson LA (1983) Processes of isolation and introgressive interplay
between Platanthera bifolia (L.) Rich, and P. chlorantha
(Custer) Reichb. (Orchidaceae). Bot J Linn Soc 87:325-350</mixed-citation>
         </ref>
         <ref id="d155e1088a1310">
            <mixed-citation id="d155e1092" publication-type="other">
Nilsson LA (1988) The evolution of flowers with deep corolla tubes.
Nature 334:147-149</mixed-citation>
         </ref>
         <ref id="d155e1102a1310">
            <mixed-citation id="d155e1106" publication-type="other">
Olmstead RG, Palmer JD (1994) Chloroplast DNA systematics: a
review of methods and data analysis. Am J Bot 81:1205-1224</mixed-citation>
         </ref>
         <ref id="d155e1116a1310">
            <mixed-citation id="d155e1120" publication-type="other">
Olsen KM (1997) Pollination effectiveness and pollinator importance
in a population of Heterotheca subaxillaris (Asteraceae). Oec-
ologia 109:114-121</mixed-citation>
         </ref>
         <ref id="d155e1133a1310">
            <mixed-citation id="d155e1137" publication-type="other">
Patt JM, Merchant MW, Williams DRE, Meeuse BJD (1989)
Pollination biology of Platanthera stricta (Orchidaceae) in
Olympic National Park, Washington. Am J Bot 76:1097-1106</mixed-citation>
         </ref>
         <ref id="d155e1150a1310">
            <mixed-citation id="d155e1154" publication-type="other">
R Development Core Team (2012) R: A language and environment
for statistical computing. R foundation for statistical computing,
Vienna, Austria. http://R-proiect.org. Accessed 21 May 2013</mixed-citation>
         </ref>
         <ref id="d155e1168a1310">
            <mixed-citation id="d155e1172" publication-type="other">
Rambaut A (2010) Se-AL: sequence alignment editor, http://tree.bio.
ed.ac.uk/software/seal/. Accessed 6 June 2013</mixed-citation>
         </ref>
         <ref id="d155e1182a1310">
            <mixed-citation id="d155e1186" publication-type="other">
Rieseberg LH, Church SA, Moijan CL (2003) Integration of
populations and differentiation of species. New Phytol
161:59-69</mixed-citation>
         </ref>
         <ref id="d155e1199a1310">
            <mixed-citation id="d155e1203" publication-type="other">
Robertson JL, Wyatt R (1990) Evidence for pollination ecotypes in
the yellow fringed orchid, Platanthera ciliaris. Evolution
44:121-133</mixed-citation>
         </ref>
         <ref id="d155e1216a1310">
            <mixed-citation id="d155e1220" publication-type="other">
Ronquist F, Huelsenbeck JP (2003) MrBayes 3: Bayesian phyloge-
netic inference under mixed models. Bioinformatics
19:1572-1574</mixed-citation>
         </ref>
         <ref id="d155e1233a1310">
            <mixed-citation id="d155e1237" publication-type="other">
Rosenberg MS, Anderson CD (2011) PASSaGE: pattern analysis,
spatial statistics, and geographic exegesis. Version 2. Method
Ecol Evol 2:229-232</mixed-citation>
         </ref>
         <ref id="d155e1250a1310">
            <mixed-citation id="d155e1254" publication-type="other">
Rydberg PA (1901) The American species of Limnorchis and Pipería ,
North of Mexico. B Torrey Bot Club 28:605-643</mixed-citation>
         </ref>
         <ref id="d155e1265a1310">
            <mixed-citation id="d155e1269" publication-type="other">
Ryder OA (1986) Conservation and systematics: the delimitation of
sub-species. Trends Ecol Evol 1:9-10</mixed-citation>
         </ref>
         <ref id="d155e1279a1310">
            <mixed-citation id="d155e1283" publication-type="other">
Schemske DW, Horvitz CC (1984) Variation among floral visitors in
pollination ability a precondition for mutualism specialization.
Science 225:519-521</mixed-citation>
         </ref>
         <ref id="d155e1296a1310">
            <mixed-citation id="d155e1300" publication-type="other">
Schiestl FP, Schlüter PM (2009) Floral isolation, specialized polli-
nation, and pollinator behavior in orchids. Annu Rev Entomol
54:425-446</mixed-citation>
         </ref>
         <ref id="d155e1313a1310">
            <mixed-citation id="d155e1317" publication-type="other">
Schrenk WJ (1978) North American Platanthera' s : evolution in the
making. Am Orchid Soc Bull 47:429-437</mixed-citation>
         </ref>
         <ref id="d155e1327a1310">
            <mixed-citation id="d155e1331" publication-type="other">
Sheviak CJ, Bracht M (1998) New chromosome number determina-
tions in Platanthera. N Am Native Orchid J 4:168-172</mixed-citation>
         </ref>
         <ref id="d155e1341a1310">
            <mixed-citation id="d155e1345" publication-type="other">
Sheviak CJ (2002) Platanthera. In: Flora of North America Editorial
Committee (ed) Flora of North America North of Mexico, vol
26. Oxford University Press, Oxford, p 551-571</mixed-citation>
         </ref>
         <ref id="d155e1359a1310">
            <mixed-citation id="d155e1363" publication-type="other">
Simmons MP, Ochoterena H (2000) Gaps as characters in sequence-
based phylogenetic analyses. Syst Biol 49:369-381</mixed-citation>
         </ref>
         <ref id="d155e1373a1310">
            <mixed-citation id="d155e1377" publication-type="other">
Stebbins GL Jr (1970) Adaptive radiation of reproductive character-
istics in angiosperms, I: pollination mechanisms. Annu Rev Ecol
Syst 1:307-326</mixed-citation>
         </ref>
         <ref id="d155e1390a1310">
            <mixed-citation id="d155e1394" publication-type="other">
Tamura K, Nei M (1993) Estimation of the number of nucleotide
substitutions in the control region of mitochondrial DNA in
humans and chimpanzees. Mol Biol Evol 10:512-526</mixed-citation>
         </ref>
         <ref id="d155e1407a1310">
            <mixed-citation id="d155e1411" publication-type="other">
Thiers B (2013) Index herbariorum: a global directory of public
herbaria and associated staff. New York Botanical Garden's
Virtual Herbarium, http://sweetgum.nybg.org/ih/Accessed 25
Sept 2013</mixed-citation>
         </ref>
         <ref id="d155e1427a1310">
            <mixed-citation id="d155e1431" publication-type="other">
Wallace LE (2003) An evaluation of taxonomie boundaries in
Platanthera dilatata (Orchidaceae) based on morphological and
molecular variation. Rhodora 105:322-336</mixed-citation>
         </ref>
         <ref id="d155e1444a1310">
            <mixed-citation id="d155e1448" publication-type="other">
Waples RS (1991) Pacific salmon, Oncorynchus spp., and the
definition of a "species" under the Endangered Species Act.
Mar Fish Rev 53:11-22</mixed-citation>
         </ref>
         <ref id="d155e1462a1310">
            <mixed-citation id="d155e1466" publication-type="other">
Wiens JJ (2004) What is speciation and how should we study it? Am
Nat 163:914-923</mixed-citation>
         </ref>
         <ref id="d155e1476a1310">
            <mixed-citation id="d155e1480" publication-type="other">
Xu S, Schlüter PM, Scopece G, Breitkopf H, Gross K, Cozzolino S,
Schiestl FP (2011) Floral isolation is the main reproductive
barrier among closely related sexually deceptive orchids.
Evolution 65:2606-2620</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
