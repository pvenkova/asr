<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt18fs91v</book-id>
      <subj-group>
         <subject content-type="call-number">DT30.2.M47 2000</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
            <subj-group>
               <subject content-type="lcsh">1960–</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Mercenary troops</subject>
         <subj-group>
            <subject content-type="lcsh">African</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Mercenaries</book-title>
         <subtitle>An African Security Dilemma</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Musah</surname>
               <given-names>Abdel-Fatau</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Fayemi</surname>
               <given-names>J. ‘Kayode</given-names>
            </name>
         </contrib>
      </contrib-group>
      <contrib-group>
         <contrib contrib-type="foreword-author" id="contrib3">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Avebury</surname>
               <given-names>Lord</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745314716</isbn>
      <isbn content-type="epub">9781849640398</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>LONDON; STERLING, VIRGINIA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2000</copyright-year>
         <copyright-holder>Abdel-Fatau Musah</copyright-holder>
         <copyright-holder>J. ‘Kayode Fayemi</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt18fs91v"/>
      <abstract abstract-type="short">
         <p>This powerful book critiques mercenary involvement in post-Cold War African conflicts. The contributors investigate the links between the rise in internal conflicts and the proliferation of mercenary activities in the 1990s; the distinction in the methods adopted by Cold War mercenaries and their contemporary counterparts; the convoluted network between private armies; business interests and sustained poverty in Africa’s poorest countries; and the connection between mercenary activities and arms proliferation. Countries discussed include Sierra Leone, Zaire, Angola, Uganda and Congo.</p>
      </abstract>
      <counts>
         <page-count count="320"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>iii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.3</book-part-id>
                  <title-group>
                     <title>About the Centre for Democracy and Development</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.4</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Avebury</surname>
                           <given-names>Lord Eric</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>viii</fpage>
                  <abstract>
                     <p>There has been a pressing need for this study, which examines the connections between African conflicts, the extraction of minerals, and the use of private military companies (PMCs). Armed opposition groups exist in Angola, Algeria, Burundi, Central African Republic, Chad, Democratic Republic of Congo, Republic of Congo, Egypt, Ethiopia, Guinea Bissau, Liberia, Rwanda, Senegal, Sierra Leone, Somalia, Sudan, Uganda and Western Sahara. In some cases, the fighting arises from unresolved aspirations towards self-determination, while in others, religious and ethnic differences are at issue. In a significant proportion of these conflicts, however, the ownership and control of valuable resources is a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.5</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <collab>Editors</collab>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.6</book-part-id>
                  <title-group>
                     <title>Abbreviations and Acronyms</title>
                  </title-group>
                  <fpage>xii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.7</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The widespread deployment of mercenary forces in Africa’s internal conflicts in the 1990s underlines an increasing acceptance by the international community of a profession until now considered pariah. This growing influence of corporate armies is occurring at a particular point in history worth explaining. Mercenary forces were used widely by the colonising powers in the 1870s and subsequent years when they first invaded the continent. They are being used again now, as the continent’s main resources are being exploited on the cheap by transnational extracting companies. Are mercenaries becoming the shock forces of corporate recolonisation, or, as some contend, a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.8</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Africa in Search of Security:</title>
                     <subtitle>Mercenaries and Conflicts – An Overview</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Musah</surname>
                           <given-names>Abdel-Fatau</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fayemi</surname>
                           <given-names>J. ‘Kayode</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>With the seemingly endless nature of internal conflicts in parts of Africa, a growing trend has emerged among scholars and policy-makers to establish a causal link between the cessation of conflicts, failure (or absence) of international action and the rise of the ‘good mercenaries’ or ‘private security forces’. The involvement of Executive Outcomes in Angola first drew attention to this phenomenon in post-Cold War Africa, even though it has existed in various forms throughout the 1970s and 1980s. A recent investigation into the activities of Sandline International in Sierra Leone by the UK Parliament has brought into sharper focus the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.9</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Private Military Companies and African Security 1990–98</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>O’Brien</surname>
                           <given-names>Kevin A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>Since the Pretoria-based private military company (PMC) Executive Outcomes Pty Ltd (EO) first emerged publicly in Angola in late 1992, international attention has focused on the role and influence of these companies in stabilising – or, alternately, destabilising – national and regional security throughout Africa. While the focus of attention on PMCs grew initially, and then continually, from an interest in tracking the involvement of EO in African countries, it is unlikely that if EO had not come from the background it did (its personnel were almost exclusively former military and police special forces who served in the South African</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.10</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>A Country Under Siege:</title>
                     <subtitle>State Decay and Corporate Military Intervention in Sierra Leone</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Musah</surname>
                           <given-names>Abdel-Fatau</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>76</fpage>
                  <abstract>
                     <p>The civil war in Sierra Leone has already claimed close to 20,000 lives and displaced over 1.5 million people – mainly innocent civilians. Sparked by the complete breakdown in the internal negotiation process, the war nonetheless traces its roots to the state’s peripheral status in the global economy, a status that has been exacerbated by illegitimate methods of resource appropriation. The fundamental link between imperial order at the centre and perpetual disorder at the periphery has thus become central to our discussion of the war, not least because of the role that resource appropriation played in igniting the war and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.11</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Hand of War:</title>
                     <subtitle>Mercenaries in the Former Zaire 1996–97</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pech</surname>
                           <given-names>Khareen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>The origins of modern mercenarism in Africa lie in the heart of the continent, in the vast, mineral-rich wilderness of what was formerly the Belgian Congo. This giant territory – discovered by European explorers only at the end of the nineteenth century – has always played a crucial role in determining new epochs in African history. In the early 1880s, a race between two explorers to discover the interior and navigate the Congo river, and thereby secure foreign control of the region, began a frenzied race among European powers for the whole continent.¹</p>
                     <p>At the end of this great scramble,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.12</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Mining for Serious Trouble:</title>
                     <subtitle>Jean-Raymond Boulle and his Corporate Empire Project</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Peleman</surname>
                           <given-names>Johan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>Anews release by the America Mineral Fields corporation, dated 20 May, 1997 read: ‘America Mineral Fields Inc. wishes to recognise the triumphant liberation of the people of Zaire by President Laurent Kabila and the Democratic Liberation Forces of the Congo.’¹ That very evening rebel leader Laurent Kabila was to enter Kinshasa as the new President of Zaire and instantly rename the vast central African state the Democratic Republic of Congo. The mining corporation must have been the first entity to ‘recognise’ Kabila as the new leader of the Democratic Republic of Congo. One week before the US government officially recognised</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.13</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Mercenaries, Human Rights and Legality</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Vines</surname>
                           <given-names>Alex</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>169</fpage>
                  <abstract>
                     <p>Africa in the 1990s has seen a significant growth in the private security sector. This has been driven by increased perceptions of insecurity caused by terrorism, kidnapping, random acts of violence, urban unrest, increasing general crime, corporate crime and weakened and poorly resourced and trained state law enforcement agencies. The rapid expansion of this industry has given rise to many thousands of providers of security-related services and products worldwide. In South Africa alone there are 5,939 companies regulated by the Security Officers Board.¹ Many of these companies remain relatively small in global terms (average size is under US$5 million in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The OAU Convention for the Elimination of Mercenarism and Civil Conflicts</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kufuor</surname>
                           <given-names>Kofi Oteng</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>198</fpage>
                  <abstract>
                     <p>There is well-documented evidence of mercenary activity in a number of African conflicts since the collapse of the colonial system. In these instances, mercenaries have been recruited either by established governments trying to hold on to their authority, or by rebel movements committed either to overthrowing the government of the day, or radically altering the power configuration within a given state. For example, during the 1960s in Kinshasa, in the Congo, Moise Tshombe, the prime minister at the time, relied on the services of mercenaries for military support.¹ Mercenaries played a role in the abortive invasions of Guinea in 1970²</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.15</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Understanding the African Security Crisis</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hutchful</surname>
                           <given-names>Eboe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>210</fpage>
                  <abstract>
                     <p>African politics since 1990 has undergone a wave of formal democratisation, coinciding with and often spurred on by the end of the Cold War. This dual process has facilitated the signing of peace accords which have brought an end to some of the continent’s most durable armed conflicts and to the dumping of weapons on the continent by great powers. It also encouraged fleeting thought about a paradigm shift in the security discourse away from the military-based concept. However, the winding down of the Cold War has also empowered internal constituencies within individual African states to challenge former client regimes</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.16</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Arresting the Tide of Mercenaries:</title>
                     <subtitle>Prospects for Regional Control</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Olonisakin</surname>
                           <given-names>’Funmi</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>Private armies¹ exist because there is a demand for them. Their supporters argue that they are effective and that their decisive use of force can gain victory for their clients over their adversaries, thereby serving to end armed conflict and the accompanying human suffering.² This has been the case in some conflict areas like Sierra Leone (albeit temporarily). If mercenaries are so effective, why should they not continue to operate? What makes them attractive in some conflict areas and not in others? The issues at the root of the resurgence of these private armies in Africa have not been subjected</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.17</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>257</fpage>
                  <abstract>
                     <p>On the threshold of the twenty-first century, Africa continues to intrigue and shock the world with its ability to engineer ever-new security dilemmas even as it recycles old ones. With the formal end of the Cold War, Africa is experiencing progressive value depreciation in relation to the rest of the world. Economic liberalisation, touted as the only viable option to regenerate the weak political economies of the continent, has set forth conflict-laden methods of resource appropriation. Shorn of big power protection and too weak to exercise their coercive prerogatives, African states are increasingly relying on private military companies to guarantee</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.18</book-part-id>
                  <title-group>
                     <title>Appendix I</title>
                     <subtitle>Mercenaries: Africa’s Experience 1950s–1990</subtitle>
                  </title-group>
                  <fpage>265</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.19</book-part-id>
                  <title-group>
                     <title>Appendix II</title>
                     <subtitle>Convention for the Elimination of Mercenarism in Africa</subtitle>
                  </title-group>
                  <fpage>275</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.20</book-part-id>
                  <title-group>
                     <title>Appendix III</title>
                     <subtitle>OAU Resolution on the Activities of Mercenaries (1967)</subtitle>
                  </title-group>
                  <fpage>281</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.21</book-part-id>
                  <title-group>
                     <title>Appendix IV</title>
                     <subtitle>OAU Declaration on the Activities of Mercenaries in Africa (1971)</subtitle>
                  </title-group>
                  <fpage>283</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.22</book-part-id>
                  <title-group>
                     <title>Appendix V</title>
                     <subtitle>OAU Convention for the Elimination of Mercenaries in Africa OAU Doc. CM/433/Rev.L., Annex 1 (1972)</subtitle>
                  </title-group>
                  <fpage>286</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.23</book-part-id>
                  <title-group>
                     <title>Appendix VI</title>
                     <subtitle>The Report by the UN Special Rapporteur on the Use of Mercenaries, 1998</subtitle>
                  </title-group>
                  <fpage>289</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.24</book-part-id>
                  <title-group>
                     <title>Notes on Contributors</title>
                  </title-group>
                  <fpage>321</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18fs91v.25</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="editor" id="contrib1" xlink:type="simple">
                        <role>Compiled by</role>
                        <name name-style="western">
                           <surname>Griffith-Jones</surname>
                           <given-names>Auriol</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>323</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
