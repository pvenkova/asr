<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jecongrowth</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000470</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Economic Growth</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">13814338</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737020</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42635317</article-id>
         <title-group>
            <article-title>Measuring human development: a stochastic dominance approach</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mehmet</given-names>
                  <surname>Pinar</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thanasis</given-names>
                  <surname>Stengos</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nikolas</given-names>
                  <surname>Topaloglou</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">18</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40098193</issue-id>
         <fpage>69</fpage>
         <lpage>108</lpage>
         <permissions>
            <copyright-statement>© 2013 Springer Science+Business Media</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s10887-012-9083-8"
                   xlink:title="an external site"/>
         <abstract>
            <p>We consider a weighting scheme that yields a best-case scenario for measured human development such as the official equally-weighted Human Development Index (HDI) using an approach that relies on consistent tests for stochastic dominance efficiency. We compare the official equally-weighted HDI to all possible indices constructed from a set of individual components to obtain the most optimistic scenario for development. In the bestcase scenario index education is weighted considerably more than the other two components, per capita income and life expectancy, relative to the weight that it gets in the official equallyweighted index. It also turns out that the improvement in the official HDI is mainly driven by improvements over time in the education index, the component moving fastest relative to its targets, when compared with per capita income and life expectancy. We find that the best-case scenario hybrid index leads to a marked improvement of measured development over time when compared with the official equally-weighted HDI.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d505e259a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d505e266" publication-type="other">
Shorrocks (1983)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e272" publication-type="other">
Kakwani (1984).</mixed-citation>
            </p>
         </fn>
         <fn id="d505e279a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d505e286" publication-type="other">
Becker et al. (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e292" publication-type="other">
Easterlin 1995</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e298" publication-type="other">
Ponthière 2004</mixed-citation>
            </p>
         </fn>
         <fn id="d505e305a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d505e312" publication-type="other">
Ram (1982),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e318" publication-type="other">
Lai
(2000),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e327" publication-type="other">
McGillivray (2005),</mixed-citation>
            </p>
         </fn>
         <fn id="d505e334a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d505e341" publication-type="other">
Anderson (1996),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e347" publication-type="other">
Beach and Davidson (1983)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e353" publication-type="other">
Davidson and Duelos
(2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e363" publication-type="other">
Davidson and Duelos (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d505e371a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d505e378" publication-type="other">
McGillivray (1991),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e384" publication-type="other">
McGillivray and White (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d505e390" publication-type="other">
Cahill
(2002)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d505e409a1310">
            <mixed-citation id="d505e413" publication-type="other">
Abadie, A. (2002). Bootstrap tests for distributional treatment effects in instrumental variable models. Journal
of the American Statistical Association, 97(457), 284-292.</mixed-citation>
         </ref>
         <ref id="d505e423a1310">
            <mixed-citation id="d505e427" publication-type="other">
Anderson, G. (1996). Nonparametric tests of stochastic dominance in income distributions. Econometrica,
64(5), 1183-1193.</mixed-citation>
         </ref>
         <ref id="d505e437a1310">
            <mixed-citation id="d505e441" publication-type="other">
Barrett, G. F., &amp; Donald, S. G. (2003). Consistent tests for stochastic dominance. Econometrica, 71(1), 71-
104.</mixed-citation>
         </ref>
         <ref id="d505e451a1310">
            <mixed-citation id="d505e455" publication-type="other">
Beach, C, &amp; Davidson, R. (1983). Distribution-free statistical inference with Lorenz curves and income
shares. Review of Economic Studies, 50(4), 723-735.</mixed-citation>
         </ref>
         <ref id="d505e466a1310">
            <mixed-citation id="d505e470" publication-type="other">
Becker, G. S., Philipson, T. J., &amp; Soares, R. R. (2005). The quantity and quality of life and the evolution
of world inequality. American Economic Review, 95(1), 277-291.</mixed-citation>
         </ref>
         <ref id="d505e480a1310">
            <mixed-citation id="d505e484" publication-type="other">
Biswas, B., &amp; Caliendo, F. (2002). A multivariate analysis of the human development index. Indian
Economic Journal, 49(4), 96-100.</mixed-citation>
         </ref>
         <ref id="d505e494a1310">
            <mixed-citation id="d505e498" publication-type="other">
Cahill, M. B. (2002). Diminishing returns to GDP and the human development index. Applied Economics
Letters, 9(13), 885-887.</mixed-citation>
         </ref>
         <ref id="d505e508a1310">
            <mixed-citation id="d505e512" publication-type="other">
Cahill, M. B. (2005). Is the human development index redundant. Eastern Economic Journal, 31(1), 1-5.</mixed-citation>
         </ref>
         <ref id="d505e519a1310">
            <mixed-citation id="d505e523" publication-type="other">
Carlstein, E. (1986). The use of subseries methods for estimating the variance of a general statistic from
a stationary time series. Annals of Statistics, 14(3), 1171-1179.</mixed-citation>
         </ref>
         <ref id="d505e533a1310">
            <mixed-citation id="d505e537" publication-type="other">
Chamie, J. (1994). Population databases in development analysis. Journal of Development Economics,
44(1), 131-146.</mixed-citation>
         </ref>
         <ref id="d505e548a1310">
            <mixed-citation id="d505e552" publication-type="other">
Davidson, R., &amp; Duelos, J.-Y. (2000). Statistical inference for stochastic dominance and for the measurement
of poverty and inequality. Econometrica, 68(6), 1435-1464.</mixed-citation>
         </ref>
         <ref id="d505e562a1310">
            <mixed-citation id="d505e566" publication-type="other">
Duelos, J.-Y., Sahn, D. E., &amp; Younger, S. D. (2006). Robust multidimensional poverty comparisons. Economic
Journal, 116(514), 943-968.</mixed-citation>
         </ref>
         <ref id="d505e576a1310">
            <mixed-citation id="d505e580" publication-type="other">
Easterlin, R. A. (1995). Will raising the incomes of all increase the happiness of all?. Journal of Economic
Behavior and Organization, 27(1), 35-47.</mixed-citation>
         </ref>
         <ref id="d505e590a1310">
            <mixed-citation id="d505e594" publication-type="other">
Engineer, M., &amp; King, I. (2010). Maximizing human development. Research Paper No. 1111, Department
of Economics, University of Melbourne.</mixed-citation>
         </ref>
         <ref id="d505e604a1310">
            <mixed-citation id="d505e608" publication-type="other">
Fleurbaey, M. (2009). Beyond the GDP: The quest for a measure of social welfare. Journal of Economic
Literature, 47(4), 1029-1075.</mixed-citation>
         </ref>
         <ref id="d505e618a1310">
            <mixed-citation id="d505e622" publication-type="other">
Hopkins, M. (1991). Human development revisited: A new UNDP report. World Development, 19(10),
1469-1473.</mixed-citation>
         </ref>
         <ref id="d505e633a1310">
            <mixed-citation id="d505e637" publication-type="other">
Kakwani, N. (1984). Welfare ranking of income distributions. Advances in Econometrics, 3, 191-213.</mixed-citation>
         </ref>
         <ref id="d505e644a1310">
            <mixed-citation id="d505e648" publication-type="other">
Kelley, A. C. (1991). The human development index: "Handle with care". Population and Development
Review, 17(2), 315-324.</mixed-citation>
         </ref>
         <ref id="d505e658a1310">
            <mixed-citation id="d505e662" publication-type="other">
Lai, D. (2000). Temporal analysis of human development indicators: Principal component approach. Social
Indicators Research, 51(3), 331-366.</mixed-citation>
         </ref>
         <ref id="d505e672a1310">
            <mixed-citation id="d505e676" publication-type="other">
Lehmann, E. L. (1986). Testing statistical hypotheses (2nd ed.). New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d505e683a1310">
            <mixed-citation id="d505e687" publication-type="other">
Linton, O., Maasoumi, E., &amp; Whang, Y.-J. (2005). Consistent testing for stochastic dominance under
general sampling schemes. Review of Economic Studies, 72(3), 735-765.</mixed-citation>
         </ref>
         <ref id="d505e697a1310">
            <mixed-citation id="d505e701" publication-type="other">
Maasoumi, E. (1999). Multidimensional approaches to welfare analysis. In J. Silber (Ed.), Handbook of
income inequality measurement (pp. 437-484). Dordrecht: Kluwer.</mixed-citation>
         </ref>
         <ref id="d505e712a1310">
            <mixed-citation id="d505e716" publication-type="other">
McGillivray, M. (1991). The human development index: Yet another redundant composite development
indicator? World Development, 79(10), 1461-1468.</mixed-citation>
         </ref>
         <ref id="d505e726a1310">
            <mixed-citation id="d505e730" publication-type="other">
McGillivray, M. (2005). Measuring non-economic well-being achievement. Review of Income and
Wealth, 51(2), 337-364.</mixed-citation>
         </ref>
         <ref id="d505e740a1310">
            <mixed-citation id="d505e744" publication-type="other">
McGillivray, M., &amp; White, H. (1993). Measuring development? The UNDP's human development
index. Journal of International Development, 5(2), 183-192.</mixed-citation>
         </ref>
         <ref id="d505e754a1310">
            <mixed-citation id="d505e758" publication-type="other">
Noorbakhsh, F. (1998). A modified human development index. World Development, 26(3), 517-528.</mixed-citation>
         </ref>
         <ref id="d505e765a1310">
            <mixed-citation id="d505e769" publication-type="other">
Ogwang, T. (1994). The choice of principal variables for computing the human development index. World
Development, 22(12), 2011-2014.</mixed-citation>
         </ref>
         <ref id="d505e779a1310">
            <mixed-citation id="d505e783" publication-type="other">
Ogwang, T., &amp; Abdou, A. (2003). The choice of principal variables for computing some measures of
human well-being. Social Indicators Research, 64(1), 139-152.</mixed-citation>
         </ref>
         <ref id="d505e794a1310">
            <mixed-citation id="d505e798" publication-type="other">
Ponthière, G. (2004). Incorporating changes in life expectancy into economic growth rates: An application
to Belgium, 1867-1997. CREPP Working Papers 2004/03.</mixed-citation>
         </ref>
         <ref id="d505e808a1310">
            <mixed-citation id="d505e812" publication-type="other">
Pressman, S., &amp; Summerfield, G. (2000). The economic contributions of Amartya Sen. Review of Political
Economy, 12(1), 89-113.</mixed-citation>
         </ref>
         <ref id="d505e822a1310">
            <mixed-citation id="d505e826" publication-type="other">
Ram, R. (1982). Composite indices of physical quality of life, basic needs fulfilment, and income: A
principal component' representation. Journal of Development Economics, 11(2), 227-247.</mixed-citation>
         </ref>
         <ref id="d505e836a1310">
            <mixed-citation id="d505e840" publication-type="other">
Ravallion, M. (1997). Good and bad growth: The human development reports. World Development, 25(5), 631-
638.</mixed-citation>
         </ref>
         <ref id="d505e850a1310">
            <mixed-citation id="d505e854" publication-type="other">
Rio, E. (2000). Théorie asymptotique des processus aléatoires faiblement dépendants. Mathématiques et
Applications, 31. Berlin: Springer.</mixed-citation>
         </ref>
         <ref id="d505e864a1310">
            <mixed-citation id="d505e868" publication-type="other">
Saito, M. (2003). Amartya Sen's capability approach to education: A critical exploration. Journal of
Philosophy of Education, 37(1), 17-33.</mixed-citation>
         </ref>
         <ref id="d505e879a1310">
            <mixed-citation id="d505e883" publication-type="other">
Scaillet, O., &amp; Topaloglou, N. (2010). Testing for stochastic dominance efficiency. Journal of Business
and Economic Statistics, 28(1), 169-180.</mixed-citation>
         </ref>
         <ref id="d505e893a1310">
            <mixed-citation id="d505e897" publication-type="other">
Sen, A. (1985). Commodities and capabilities. Amsterdam: North-Holland.</mixed-citation>
         </ref>
         <ref id="d505e904a1310">
            <mixed-citation id="d505e908" publication-type="other">
Sen, A. (1987). The standard of living: The Tanner lectures. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d505e915a1310">
            <mixed-citation id="d505e919" publication-type="other">
Shorrocks, A. F. (1983). Ranking income distributions. Economica, 50(197), 3-17.</mixed-citation>
         </ref>
         <ref id="d505e926a1310">
            <mixed-citation id="d505e930" publication-type="other">
Srinivasan, T. N. (1994). Human development: A new paradigm or reinvention of the wheel? American
Economic Review, Papers and Proceedings, 84(2), 238-243.</mixed-citation>
         </ref>
         <ref id="d505e940a1310">
            <mixed-citation id="d505e944" publication-type="other">
Streeten, P. (1994). Human development: Means and ends. American Economic Review, Papers and
Proceedings, 84(2), 232-237.</mixed-citation>
         </ref>
         <ref id="d505e955a1310">
            <mixed-citation id="d505e959" publication-type="other">
United Nations Development Program. (1993). Human Development Report. New York: Oxford University
Press.</mixed-citation>
         </ref>
         <ref id="d505e969a1310">
            <mixed-citation id="d505e973" publication-type="other">
Van Der Vaart, A. W., &amp; Wellner, J. A. (1996). Weak convergence and empirical processes: With applications
to statistics. New York: Springer.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
