<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">afrdevafrdev</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Africa Development / Afrique et Développement</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CODESRIA</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08503907</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>jstor_product</meta-name>
               <meta-value>archive_collection_journals</meta-value>
            </custom-meta>
         </custom-meta-group>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">afrdevafrdev.35.4.141</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">afrdevafrdev.35.4.141</article-id>
         <title-group>
            <article-title>Behind the Curtains of State Power: Religious Groups and the Struggle for Ascendancy in Nigerian Public Institutions - A Critical Appraisal</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Obianyo</surname>
                  <given-names>Nkolika E.</given-names>
               </string-name>
               <aff>Department of Political Science, Nnamdi Azikiwe University, Awka, Anambra State, Nigeria. Email:<email xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="nkolikae@yahoo.com">nkolikae@yahoo.com</email>
               </aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <year>2010</year>
            <string-date>2010</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">afrdevafrdev.35.issue-4</issue-id>
         <fpage>141</fpage>
         <lpage>164</lpage>
         <permissions>
            <copyright-statement>© 2010 Council for the Development of Social Science Research in Africa</copyright-statement>
            <copyright-year>2010</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/afrdevafrdev.35.4.141"/>
         <abstract>
            <label>
               <bold>Abstract</bold>
            </label>
            <p>In secular states, religion is expected to occupy a backseat in public life; but in states like Nigeria, this is not so as religion, after ethnicity, has turned out to become a major determinant of political behaviour. It serves as both a unifying and a divisive factor. It unifies Nigerians who share the same belief across ethnic lines and, at the same time, puts them in contest with others who hold different beliefs. Hence, there are several contestations along religious lines. The proliferation of these divides has also given rise to the proliferation of these contestations. Traditionally, this contest has been between the Moslem and Christian groups; but contrary to popular opinion, within the two major religious groups there are several other contestations. Today, there is the contest between Pentecostal and non-Pentecostal Christians, Roman Catholics and non-Roman Catholic Christians (especially the Anglican sect). These contests focus on the relative strength of each group or sect in the corridors of state power. Against this backdrop, this article examines the origin and dynamics of this trend with respect to the Christian religious group in Nigeria. It argues that religion, like ethnicity, serves as instrument for the acquisition of state power, public positions and resources, and consequently as an instrument of exclusion and inclusion in the quest for ascendancy among contestants for state power and public positions. The article argues that this trend cannot be divorced from the ‘rentier’ and ‘allocative’ character of the post-colonial state of Nigeria in which occupants of state power not only use the state to improve their material well-being but also that of their primordial groups from which they draw support when the competition gets stiff. The ar ticle examines the implications of this trend for development and efficiency of public institutions in Nigeria.</p>
         </abstract>
         <trans-abstract xml:lang="fr">
            <label>
               <bold>Résumé</bold>
            </label>
            <p>Dans les états laïques, la religion est censée être reléguée au second plan de la vie publique ; mais dans des Etats comme le Nigeria, il n'en n'est pas ainsi, puisque la religion, après l'ethnicité, est devenue un déterminant majeur du comportement politique. Elle sert à la fois de facteur d'unification et de division. Elle unifie les Nigérians qui partagent les mêmes convictions religieuses, quelle que soit leur appartenance ethnique et, en même temps, les oppose à d'autres qui ont des convictions différentes. Ainsi, il y a plusieurs contestations fondées sur la religion. La prolifération de ces dissensions a également entraîné la prolifération de ces contestations. Traditionnellement, cette contestation opposait les groupes musulmans et chrétiens ; mais contrairement à l'opinion populaire, au sein de ces deux grands groupes religieux, il y a plusieurs autres contestations. Aujourd'hui, il y a la contestation entre chrétiens pentecôtistes et non-pentecôtistes, catholiques romains et non-romains (en particulier la secte anglicane). Ces contestations sont axées sur la force relative de chaque groupe ou secte dans les coulisses du pouvoir étatique. Sur cette toile de fond, le présent article examine l'origine et la dynamique de cette tendance, en ce qui concerne le groupe religieux chrétien au Nigeria. L'article soutient que la religion, tout comme l'ethnicité, sert d'instrument pour l'acquisition de pouvoir étatique, de postes publics et de ressources, et par conséquent, d'instrument d'exclusion et d'inclusion dans la quête d'ascendance entre ceux qui se disputent le pouvoir étatique et les postes publics. L'article soutient que cette tendance ne saurait être dissociée du caractère « rentier » et « allocatif » de l'État postcolonial du Nigeria dans lequel les détenteurs du pouvoir étatique non seulement se servent de l'État pour améliorer leur bien-être matériel, mais aussi celui de leurs groupes primordiaux auprès desquels ils obtiennent du soutien lorsque la compétition devient rude. L'article examine les implications de cette tendance pour le développement et l'efficacité des institutions publiques au Nigeria.</p>
         </trans-abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ake, C.</string-name>
               </person-group>,<year>1981</year>,<source>
                  <italic>Political Economy of Africa</italic>
               </source>,:<publisher-name>Longman</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ake, C.</string-name>
               </person-group>,<year>1996</year>,<source>
                  <italic>Is Africa Democratizing</italic>
               </source>? CASS Monograph No.5,,<publisher-name>Centre for Advanced Social Science</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ake, C.</string-name>
               </person-group>,<year>2001</year>,<source>
                  <italic>Democracy and Development in Africa</italic>
               </source>,:<publisher-name>Spectrum Books</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Amucheazi, E.</string-name>
               </person-group>,<year>1986</year>,<source>
                  <italic>Church and Politics in Eastern Nigeria: 1945-1966</italic>
               </source>,:<publisher-name>Macmillan</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Anders, G.</string-name>
               </person-group>,<year>2002</year>,<article-title>‘Like Chameleons - Civil Servants and Corruption in Malawi’</article-title>,<conf-name>Discussion Paper, prepared for the Conference on ‘The Governance of Daily Life in Africa: Public and Collective Services and their Users’</conf-name>,<conf-name>University of Leiden</conf-name>,<conf-date>22-25. May</conf-date>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Chabal, P. and Daloz, J.</string-name>
               </person-group>,<year>1999</year>,<source>
                  <italic>Africa Works: Disorder as Political Instrument</italic>
               </source>,;<publisher-name>The International African Institute &amp; James Currey</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Coleman J.S.</string-name>
               </person-group>,<year>1986</year>,<source>
                  <italic>Nigeria, Background to Nationalism</italic>
               </source>,:<publisher-name>Brouburg &amp; Wistrom</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Dawodu, S.T.</string-name>
               </person-group>,<year>2002</year>,<article-title>‘Enugu Debacle: Politics in the Name of God - Catholicism and Theocracy’</article-title>(<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.nigerianworld.com/letetters/2002/mar/192.html">http://www.nigerianworld.com/letetters/2002/mar/192.html</uri>),<day>15</day>September<year>2002</year>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ekeh, P.</string-name>
               </person-group>,<year>1978</year>,<article-title>‘Colonialism and the development of Citizenship in Africa: A Study in Ideologies of Legitimation’</article-title>in<person-group person-group-type="editor">
                  <string-name>O, Otite</string-name>
               </person-group>, ed.<source>
                  <italic>Themes in African Social andPolitical Thought</italic>
               </source>,:<publisher-name>Fourth Dimensions</publisher-name>
            </mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Ibeanu, O.</string-name>
               </person-group>,<year>2006</year>,<article-title>‘The Church and Democratic Governance in Nigeria: An Explanatory Analysis’</article-title>,<conf-name>Paper presented at the Valedictory Conference in honour of Professor Elo Amucheazi at the University of Nigeria</conf-name>,<conf-loc>Enugu</conf-loc>,<conf-date>7 September 2008</conf-date>.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Ibrahim, J.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘The Transformation of Ethno-Regional Identities in Nigeria’</article-title>, in<person-group person-group-type="editor">
                  <string-name>Jega, A.</string-name>
               </person-group>ed.,<source>
                  <italic>Identity Formation and Identity Politics Under StructuralAdjustment in Nigeria</italic>
               </source>,:<publisher-name>The Centre for Research and Documentation and Nordiska Africainstitutet</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Jega, A.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘General Introduction. Identity Transformation and the Politics of Identity Transformation under Crisis and Adjustment’</article-title>, in<person-group person-group-type="editor">
                  <string-name>A. Jega</string-name>
               </person-group>, ed.,<source>
                  <italic>Identity Formation and Identity Politics Under Structural Adjustment In Nigeria</italic>
               </source>,,<publisher-name>Centre for Research and Documentation and Nordiska Africainstitutet</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Jega, A.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘The State and Identity Transformation under Structural Adjustment in Nigeria’</article-title>, in<person-group person-group-type="editor">
                  <string-name>A. Jega</string-name>
               </person-group>, ed.,<source>
                  <italic>Identity Formation and Identity Politics UnderStructural Adjustment in Nigeria</italic>
               </source>,:<publisher-name>Centre for Research and Documentation. and Nordiska Africainstitutet</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Joseph, R.</string-name>
               </person-group>,<year>1991</year>,<source>
                  <italic>Democracy and Prebendal Politics in Nigeria: The Rise andFall of the Second Republic</italic>
               </source>,:<publisher-name>Spectrum Books</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Korieh, J.</string-name>
               </person-group>,<year>2005</year>,<article-title>‘Islam and Politics in Nigeria: Historical Perspectives’</article-title>, in<person-group person-group-type="editor">
                  <string-name>J. Korieh and G. Nwokeji</string-name>
               </person-group>, ed.,<source>
                  <italic>Religion, History and Politics in Nigeria: Essays in Honour of Ogbuu Kalu</italic>
               </source>,<publisher-name>University Press of America</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Kukah, M.H.</string-name>
               </person-group>,<year>2003</year>,<source>
                  <italic>Religion, Politics and Power in Northern Nigeri</italic>
               </source>,:<publisher-name>Spectrum Books</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="web">
               <collab>List of Vice Presidents of Nigeria</collab>(<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://en.wikipedia.org/wikivice_president_of_Nigeria">http://en.wikipedia.org/wikivice_president_of_Nigeria</uri>)<day>15</day>
               <month>September</month>
               <year>2008</year>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Mu'azzam, I.</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>Ibrahim, J.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘Religious Identity in the Context of Structural Adjustment in Nigeria’</article-title>, in<person-group person-group-type="editor">
                  <string-name>A. Jega</string-name>
               </person-group>, ed.,<source>
                  <italic>Identity Formation and IdentityPolitics Under Structural Adjustment in Nigeria</italic>
               </source>.:<publisher-name>Centre for Research and Documentation and Nordiska Africainstitutet</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Nabudere, D.W.</string-name>
               </person-group>,<year>2000</year>,<article-title>‘Globalization, the African Post-Colonial State, Post Traditionalism, and the New World Order’</article-title>, in<person-group person-group-type="editor">
                  <string-name>D. W. Nabudere</string-name>
               </person-group>, ed.,<source>
                  <italic>Globalization and The Post-Colonial African State</italic>
               </source>,:<publisher-name>Zimbabwe AAPS Books</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Nnoli, O.</string-name>
               </person-group>,<year>1981</year>,<article-title>‘A Short History of Nigerian Development’</article-title>, in<person-group person-group-type="editor">
                  <string-name>O. Nnoli</string-name>
               </person-group>ed.,<source>
                  <italic>Pathto Nigerian Development</italic>
               </source>,:.<publisher-name>CODESRIA</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Nnoli, O.</string-name>
               </person-group>,<year>1989</year>,<source>
                  <italic>Ethnic Politics in Africa</italic>
               </source>,,<publisher-name>African Association of Political Science</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Obianyo, N.E.</string-name>
               </person-group>,<year>2001</year>,<article-title>‘The Political Economy of Sectionalismè in Anambra State’</article-title>, in<source>
                  <italic>Nnamdi Azikiwe Journal of Political Science</italic>
               </source>, Vol.<volume>2</volume>, No.<issue>1</issue>.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Obianyo, N.E.</string-name>
               </person-group>,<year>2008</year>,<article-title>‘Citizenship and Ethnic Militia Politics in Nigeria: Marginalization or Identity Question? –The Case of MASSOB’</article-title>,<conf-name>Paper presented at the 4<sup>th</sup>Global Conference on Pluralism Inclusion and Citizenship</conf-name>,<conf-loc>Salzburg</conf-loc>,<conf-date>31 October,- 4 November 2008</conf-date>.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Offiong, D.A.</string-name>
               </person-group>,<year>2001</year>,<source>
                  <italic>Globalization and Post Neo Dependency in Africa</italic>
               </source>,;<publisher-name>Fourth Dimensions</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Osaghae, E.</string-name>
               </person-group>,<year>2003</year>,<article-title>‘Colonialism and Civil Society in Africa: The perspective of Eke's Two Publics’</article-title>,<conf-name>Paper presented at the symposium on Canonical Works and Continuing Innovation in African Arts and Humanities</conf-name>,<conf-loc>Accra, Ghana</conf-loc>.<conf-date>17-19 September</conf-date>Available Online (<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http:/www.codesria.org/links/conferences/accra/osaghae.pdf">http:/www.codesria.org/links/conferences/accra/osaghae.pdf</uri>).<day>22</day>
               <month>September</month>
               <year>2008</year>.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="book">
               <collab>Unizik Comet</collab>,<year>2003</year>,<article-title>‘Current Appointment in Unizik’</article-title>,<publisher-name>Mass Communication Department</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Zeleza, P.T.</string-name>
               </person-group>(n.d),<article-title>‘Imaging and Inventing the Post Colonial State in Africa’</article-title>, in<source>
                  <italic>Contours</italic>
               </source>(<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.press.uillinois.edu/journals/contours/1.1/zeleza.html">http://www.press.uillinois.edu/journals/contours/1.1/zeleza.html</uri>).<day>11</day>
               <month>November</month>
               <year>2006</year>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
