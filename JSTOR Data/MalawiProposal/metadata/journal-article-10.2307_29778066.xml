<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100641</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03057070</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14653893</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">29778066</article-id>
         <article-categories>
            <subj-group>
               <subject>Perspectives on the 1959 Nyasaland Emergency</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>A Police State? The Nyasaland Emergency and Colonial Intelligence</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Philip</given-names>
                  <surname>Murphy</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i29778062</issue-id>
         <fpage>765</fpage>
         <lpage>780</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 The Editorial Board of the Journal of Southern African Studies</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/03057070.2010.527634"
                   xlink:title="an external site"/>
         <abstract>
            <p>The Nyasaland Emergency in 1959 proved a decisive turning point in the history of the Federation of Rhodesia and Nyasaland, which from 1953 to 1963 brought together the territories of Northern Rhodesia (Zambia), Southern Rhodesia (Zambia) and Nyasaland (Malawi) under a settler-dominated federal government. The British and Nyasaland governments defended the emergency by claiming to have gathered intelligence which showed that the Nyasaland African Congress was preparing a campaign of sabotage and murder. The Devlin Commission, appointed to investigate the emergency, dismissed the evidence of a 'murder plot', criticised the Nyasaland government's handling of the Emergency and, notoriously, described Nyasaland as a 'police state'. This article has two principal aims. First, using the recently declassified papers of the Intelligence and Security Department (ISD) of the Colonial Office, it seeks to provide the first detailed account of what the British government knew of the intelligence relating to the 'murder plot' and how they assessed it, prior to the outbreak of the emergency. It demonstrates that officials in the ISD and members of the Security Service adopted a far more cautious attitude towards the intelligence than did Conservative ministers, and had greater qualms about allowing it into the public domain to justify government policy. Second, the article examines the implications of Devlin's use of the phrase 'police state' for Nyasaland and for the late colonial state in general. It contrasts Devlin's use of the term with that of security experts in the ISD, who routinely applied it to policing systems that diverged from their own preferred model. Hence, whereas Devlin compared policing in Nyasaland unfavourably with that in Southern Rhodesia, implying, ironically, that Nyasaland was 'under-policed' (because there were fewer police per head of population in Nyasaland than in Southern Rhodesia), the ISD regarded the intensive system of policing operated by the British South Africa Police in Southern Rhodesia as characteristic of a 'police state'. The article suggests that the frequent use of the term 'police state' was indicative of broader anxieties about what Britain's legacy would be for the post-independence African state.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1440e125a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1440e132" publication-type="other">
C. Baker, State of Emergency: Crisis in Central Africa, Nyasaland 1959-1960 (London, LB, Tauris, 1997), p. 55.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e139a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1440e146" publication-type="other">
Parliamentary Debates (Commons), 601, 3 March 1959, cols. 290 and 337.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e153a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1440e160" publication-type="other">
'Nyasaland: State of Emergency', Cmnd 707 (1959), para. 24.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e167a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1440e174" publication-type="other">
'Report of the Nyasaland Commission of Inquiry' (hereafter 'Devlin'), Cmnd 814 (1959), para. 149.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e182a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1440e189" publication-type="other">
P. Murphy, Alan Lennox-Boyd: A Biography (London, LB. Tauris, 1999), p. 217.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e196a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1440e203" publication-type="other">
The National Archives, Kew (hereafter 'TNA') CO 1027/140, Minute by Morris, 17 July 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e210a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1440e217" publication-type="other">
J. Darwin, 'The Central African Emergency, 1959', Journal of Imperial and Commonwealth History, 21, 3
(1993), pp. 217-34.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e227a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1440e234" publication-type="other">
P. Murphy (ed.), British Documents on the End of Empire: Central Africa (London, Stationery Office, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e241a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1440e248" publication-type="other">
C. Baker, State of Emergency: Crisis in Central Africa, Nyasaland 1959-1960 (London, LB. Tauris, 1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e255a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1440e262" publication-type="other">
B. Simpson, 'The Devlin Commission (1959): Colonialism, Emergencies, and the Rule of Law', Oxford Journal
of Legal Studies, 22 (2002), pp. 17-52.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1440e271" publication-type="other">
A.W. Brian Simpson,
Human Rights and the End of Empire: Britain and the Genesis of the European Convention (Oxford, Oxford
University Press, 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e285a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1440e292" publication-type="other">
P. Murphy, 'Creating a Commonwealth Intelligence Culture: The View from Central Africa 1945-1965',
Intelligence and National Security, 17, 3 (2002), p. 140.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e302a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1440e309" publication-type="other">
Baker, State of Emergency, for example (p. 20)</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e316a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1440e323" publication-type="other">
Devlin Commission Papers (hereafter 'DC'), Rhodes House Library, Oxford, Box 6. 'The Congress Plan of
Violence: Synopsis of Information Obtained before 3rd March on the Secret Meeting of 25th January', Special
Branch Police Headquarters, Zomba, April 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e336a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1440e343" publication-type="other">
Baker, State of Emergency, p. 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e350a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1440e357" publication-type="other">
DC, Box 6, 'The Congress Plan of Violence: Synopsis of Information'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e364a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1440e371" publication-type="other">
Baker, State of Emergency, p. 19.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e379a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1440e386" publication-type="other">
R. Welensky, Welensky 's 4000 Days. The Life and Death of the Federation of. Rhodesia and Nyasaland (London,
Collins, 1964), pp. 117-18.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e396a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1440e403" publication-type="other">
DC, Box 6, 'The Congress Plan of Violence: Synopsis of Information'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e410a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1440e417" publication-type="other">
DC, Box 9, Mullin to Footman, 18 Feb 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e424a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1440e431" publication-type="other">
Murphy, Central Africa, Part 2, pp. 15-16.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e438a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1440e445" publication-type="other">
Commissioner of Police to Chief Secretary, 18 Feb 1959, DC, Box 9.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e452a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1440e459" publication-type="other">
Baker, State of Emergency, p. 17.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e467a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1440e474" publication-type="other">
TNA, CO 1035/143, Footman to Morgan, 19 Feb 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e481a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1440e488" publication-type="other">
Murphy, Central Africa, Part Two, p. 17.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e495a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1440e502" publication-type="other">
TNA, CO 1015/1977, Armitage to Perth, 25 Feb 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e509a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1440e516" publication-type="other">
TNA, CO 1035/143, Minute by Watson, 5 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e523a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1440e530" publication-type="other">
TNA, CO 1035/143, Minute by Carstairs, 5 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e537a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1440e544" publication-type="other">
TNA, CO 1035/143, Minute by Watson, 5 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e552a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1440e559" publication-type="other">
TNA, CO 1035/143, Minute by Fairclough, 29 June 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e566a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1440e573" publication-type="other">
TNA, CO 1035/143, Minute by Watson, 2 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e580a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1440e587" publication-type="other">
TNA, CO 1053/143, 'The Situation in Nyasaland and its Possible Repercussions Elsewhere in the Federation', 25
Feb 1959, forwarded Day to Watson, 3 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e597a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1440e604" publication-type="other">
TNA, CO 1053/145, Minute by Posnett, 31 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e611a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1440e618" publication-type="other">
TNA, CO 1035/143, Minute by Watson, 9 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e625a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1440e632" publication-type="other">
Baker, State of Emergency, p. 67.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e640a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1440e647" publication-type="other">
S. Onslow, 'The Ultimate Imperial Adventurer: Julian Amery', in Wm R. Louis (ed.), Ultimate Adventures with
Britannia (London, LB. Tauris, 2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e657a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1440e664" publication-type="other">
Churchill College Archives, Cambridge, AMEJ 2/2 Box 556, Papers of Julian Amery, Amery to Tapp, 10 March
1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e674a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1440e681" publication-type="other">
Cabinet minutes, CC (59) 13, 27 Feb. 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e688a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1440e695" publication-type="other">
Murphy, Lennox-Boyd, p. 206.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e702a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1440e709" publication-type="other">
TNA, CO 1035/119, Minute by Morgan, 6 July 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e716a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1440e723" publication-type="other">
Murphy, Central Africa, Part Two, p. 9.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e731a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1440e738" publication-type="other">
Ibid., Part Two, p. 29.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e745a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1440e752" publication-type="other">
Baker, State of Emergency, p. 36.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e759a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1440e766" publication-type="other">
Darwin, p. 220.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e773a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1440e780" publication-type="other">
Baker, State of Emergency, p. 160.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e787a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1440e794" publication-type="other">
Baker, State of Emergency, p. 130.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e801a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1440e808" publication-type="other">
TNA, CO 1035/142, Minute by Watson, 4 March 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e816a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1440e823" publication-type="other">
TNA, CO
1035/142, Minute by Amery, 23 March 1959</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e833a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1440e840" publication-type="other">
P. Murphy and J. Lewis,
'"The Old Pals' Protection Society?" The Colonial Office and the British Press on the Eve of Decolonisation', in
C. Kaul (ed.), Media and the British Empire (London, Routledge, 2006), pp. 55-69.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e853a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1440e860" publication-type="other">
The Times, 16 July 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e867a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1440e874" publication-type="other">
TNA, DO 35/7627, Morgan to Kirkness, 27 April 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e881a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d1440e888" publication-type="other">
TNA, DO 35/7627, Minute by Shannon, 11 May 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e895a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d1440e902" publication-type="other">
C. Andrew, The Defence of the Realm: the Authorized History of MI5 (London, Allen Lane, 2009), p. 322.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e910a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d1440e917" publication-type="other">
Murphy, 'Creating a Commonwealth Intelligence Culture', p. 139.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e924a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d1440e931" publication-type="other">
TNA, CO 1035/28, Benson to Lennox-Boyd, 4 June 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e938a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d1440e945" publication-type="other">
TNA, CO 1035/28, Minute by Barton, 11 June 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e952a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d1440e959" publication-type="other">
TNA, CO 1035/87, Minute by Watson, 10 Aug 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e966a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d1440e973" publication-type="other">
TNA, CO 1035/119, 'The Federation of Rhodesia and Nyasaland: Memorandum by the Commonwealth
Relations Office', Dec 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e983a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d1440e990" publication-type="other">
TNA, CO 1035/119, 'Report on the Intelligence Organisation in Nyasaland', CA. Herbert, 22 Feb 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e998a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d1440e1005" publication-type="other">
TNA, CO 1035/88, Lennox-Boyd to Armitage, 10 May 1956.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1012a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d1440e1019" publication-type="other">
Simpson, 'The Devlin Commission', p. 45.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1026a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d1440e1033" publication-type="other">
Ibid., p. 31.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1040a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d1440e1047" publication-type="other">
TNA, CO 1015/1519, Minute by Watson, 5 March 1959, cited in Murphy, Central Africa, Part Two, p. 25.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1054a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d1440e1061" publication-type="other">
Simpson, Human Rights, p. 1,068.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1068a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d1440e1075" publication-type="other">
Ibid., p. 1,070.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1083a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d1440e1090" publication-type="other">
DC, Box 1, Devlin to Colin Legum, 9 Oct 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1097a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d1440e1104" publication-type="other">
DC, Box 26, Devlin to Beadle, 17 Sept 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1111a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d1440e1118" publication-type="other">
DC, Box 1, Devlin to Colin Legum, 19 Oet 1959.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1125a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d1440e1132" publication-type="other">
J. McCracken, 'Coercion and Control in Nyasaland: Aspects of the History of a Colonial Police Force', Journal
of African History, 27, 1 (1986), pp. 127-47.</mixed-citation>
            </p>
         </fn>
         <fn id="d1440e1142a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d1440e1149" publication-type="other">
McCracken, 'Coercion and Control in Nyasaland', p. 140.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
