<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41727377</article-id>
         <title-group>
            <article-title>Superinfection and the evolution of resistance to antimalarial drugs</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eili Y.</given-names>
                  <surname>Klein</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David L.</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ramanan</given-names>
                  <surname>Laxminarayan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Simon</given-names>
                  <surname>Levin</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>22</day>
            <month>9</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">279</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1743</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40081204</issue-id>
         <fpage>3834</fpage>
         <lpage>3842</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41727377"/>
         <abstract>
            <p>A major issue in the control of malaria is the evolution of drug resistance. Ecological theory has demonstrated that pathogen superinfection and the resulting within-host competition influences the evolution of specific traits. Individuals infected with Plasmodium falciparum are consistently infected by multiple parasites; however, while this probably alters the dynamics of resistance evolution, there are few robust mathematical models examining this issue. We developed a general theory for modelling the evolution of resistance with host superinfection and examine: (i) the effect of transmission intensity on the rate of resistance evolution; (ii) the importance of different biological costs of resistance; and (iii) the best measure of the frequency of resistance. We find that within-host competition retards the ability and slows the rate at which drug-resistant parasites invade, particularly as the transmission rate increases. We also find that biological costs of resistance that reduce transmission are less important than reductions in the duration of drugresistant infections. Lastly, we find that random sampling of the population for resistant parasites is likely to significantly underestimate the frequency of resistance. Considering superinfection in mathematical models of antimalarial drug resistance may thus be important for generating accurate predictions of interventions to contain resistance.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d845e184a1310">
            <label>1</label>
            <mixed-citation id="d845e193" publication-type="other">
Ehrlich, P. 1913 Address in pathology, ON CHE-
MIOTHERAPY. Delivered before the Seventeenth
International Congress of Medicine. Br. Med. J. 2,
353-359. (doi:10.1136/bmj.2.2746.353)</mixed-citation>
         </ref>
         <ref id="d845e209a1310">
            <label>2</label>
            <mixed-citation id="d845e216" publication-type="other">
Levin, S. &amp; Pimentel, D. 1981 Selection of intermediate
rates of increase in parasite-host systems. Am. Nat. 117,
308-315. (doi:10.1086/283708)</mixed-citation>
         </ref>
         <ref id="d845e229a1310">
            <label>3</label>
            <mixed-citation id="d845e236" publication-type="other">
Nowak, M. A. &amp; May, R. M. 1994 Superinfection and
the evolution of parasite virulence. Proc. R. Soc. Lond.
B 255, 81-89. (doi:10.1098/rspb.1994.0012)</mixed-citation>
         </ref>
         <ref id="d845e249a1310">
            <label>4</label>
            <mixed-citation id="d845e256" publication-type="other">
van Baalen, M. &amp; Sabelis, M. W. 1995 The dynamics of
multiple infection and the evolution of virulence. Am.
Nat. 146, 881. (doi:10.1086/285830)</mixed-citation>
         </ref>
         <ref id="d845e270a1310">
            <label>5</label>
            <mixed-citation id="d845e277" publication-type="other">
Macdonald, G. 1950 The analysis of infection rates in
diseases in which superinfection occurs. Trop. Dis. Bull.
47, 907-915.</mixed-citation>
         </ref>
         <ref id="d845e290a1310">
            <label>6</label>
            <mixed-citation id="d845e297" publication-type="other">
McKenzie, F. E., Smith, D. L., O'Meara, W. P., Riley,
E. M., Rollinson, D. &amp; Hay, S. I. 2008 Strain theory of
malaria: the first 50 years. Adv. Parasitol. 66, 1-46.
(doi:10.1016/S0065-308X(08)00201-7)</mixed-citation>
         </ref>
         <ref id="d845e313a1310">
            <label>7</label>
            <mixed-citation id="d845e320" publication-type="other">
Paul, R., Hackford, I., Brockman, A., Muller-Graf, C.,
Price, R., Luxemburger, C., White, N., Nosten, F. &amp;
Day, K. 1998 Transmission intensity and Plasmodium
falciparum diversity on the northwestern border of
Thailand. Am. J. Trop. Med. Hyg. 58, 195-203.</mixed-citation>
         </ref>
         <ref id="d845e339a1310">
            <label>8</label>
            <mixed-citation id="d845e346" publication-type="other">
White, N. J. 2004 Antimalarial drug resistance. J. Clin.
Invest. 113, 1084-1092.</mixed-citation>
         </ref>
         <ref id="d845e356a1310">
            <label>9</label>
            <mixed-citation id="d845e363" publication-type="other">
Felger, I. &amp; Beck, H.-P. 2008 Fitness costs of resistance
to antimalarial drugs. Trends Parasitol. 24, 331-333.
(doi:10.1016/j.pt.2008.05.004)</mixed-citation>
         </ref>
         <ref id="d845e376a1310">
            <label>10</label>
            <mixed-citation id="d845e383" publication-type="other">
Babiker, H. A., Hastings, I. M. &amp; Swedberg, G. 2009
Impaired fitness of drug-resistant malaria parasites: evi-
dence and implication on drug-deployment policies.
Expert Rev. Anti-Infect. Ther. 7, 581-593. (doi:10.1586/
eri.09.29)</mixed-citation>
         </ref>
         <ref id="d845e403a1310">
            <label>11</label>
            <mixed-citation id="d845e410" publication-type="other">
Laufer, M. K., Thesing, P. C., Eddington, N. D.,
Masonga, R., Dzinjalamala, F. K., Takala, S. L.,
Taylor, T. E. &amp; Plowe, C. V. 2006 Return of chloroquine
antimalarial efficacy in Malawi. N Engl. J. Med. 355,
1959-1966. (doi:10.1056/NEJMoa062032)</mixed-citation>
         </ref>
         <ref id="d845e429a1310">
            <label>12</label>
            <mixed-citation id="d845e436" publication-type="other">
De-quan, L. et al. 1995 Changes in the resistance of Plas-
modium falciparum to chloroquine in Hainan, China. Bull.
World Health Organ. 73, 483-486.</mixed-citation>
         </ref>
         <ref id="d845e449a1310">
            <label>13</label>
            <mixed-citation id="d845e456" publication-type="other">
Levin, S. A. 1983 Some approaches to the modelling of co-
evolutionary interactions. In Coevolution (ed. M. Ntecki),
pp. 21-65. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d845e469a1310">
            <label>14</label>
            <mixed-citation id="d845e476" publication-type="other">
Levin, S. A. 1983 Coevolution. Lect. Notes Biomath. 52,
328-334. (doi:10.1007/978-3-642-87893-0_41)</mixed-citation>
         </ref>
         <ref id="d845e486a1310">
            <label>15</label>
            <mixed-citation id="d845e493" publication-type="other">
Daubersies, P., Sallenave-Sales, S., Magne, S., Trape,
J. F., Contamin, H., Fandeur, T., Rogier, C.,
Mercereau-Puijalon, O. &amp; Druilhe, P. 1996 Rapid turn-
over of Plasmodium falciparum populations in
asymptomatic individuals living in a high transmission
area. Am. J. Trop. Med. Hyg. 54, 18-26.</mixed-citation>
         </ref>
         <ref id="d845e516a1310">
            <label>16</label>
            <mixed-citation id="d845e523" publication-type="other">
Arnot, D. 1998 Clone multiplicity of Plasmodium falci-
parum infections in individuals exposed to variable
levels of disease transmission. Trans. R. Soc. Trop. Med.
Hyg. 92, 580-585. (doi:10.1016/S0035-9203(98)
90773-8)</mixed-citation>
         </ref>
         <ref id="d845e543a1310">
            <label>17</label>
            <mixed-citation id="d845e550" publication-type="other">
Taylor, L. H., Walliker, D. &amp; Read, A. F. 1997 Mixed-
genotype infections of malaria parasites: within-host
dynamics and transmission success of competing
clones. Proc. R. Soc. Lond. B 264, 927-935. (doi:10.
1098/rspb.1997.0128)</mixed-citation>
         </ref>
         <ref id="d845e569a1310">
            <label>18</label>
            <mixed-citation id="d845e576" publication-type="other">
Snounou, G., Jarra, W., Viriyakosol, S., Wood, J. C. &amp;
Brown, K. N. 1989 Use of a DNA probe to analyse the
dynamics of infection with rodent malaria parasites con-
firms that parasite clearance during crisis is predominantly
strain-and species-specific. Mol. Biochem. Parasitol. 37,
37-46. (doi:10.1016/0166-6851(89)90100-x)</mixed-citation>
         </ref>
         <ref id="d845e599a1310">
            <label>19</label>
            <mixed-citation id="d845e608" publication-type="other">
De Roode, J., Read, A., Chan, B. &amp; Mackinnon, M. 2003
Rodent malaria parasites suffer from the presence of
conspecific clones in three-clone Plasmodium chabaudi
infections. Parasitology 127, 411-418. (doi:10.1017/
S0031182003004001)</mixed-citation>
         </ref>
         <ref id="d845e627a1310">
            <label>20</label>
            <mixed-citation id="d845e634" publication-type="other">
Dietz, K. 1988 Mathematical models for transmission
and control of malaria. In Malaria: principles and practice
of malariology (eds W. H. Wernsdorfer &amp; I. A. McGregor),
pp. 1091-1134. Edinburgh, UK: Churchill Livingstone.</mixed-citation>
         </ref>
         <ref id="d845e650a1310">
            <label>21</label>
            <mixed-citation id="d845e657" publication-type="other">
Fine, P. 1975 Superinfection: a problem in formulating a
problem. Bur. Hyg. Trop. Dis. 72, 475-488.</mixed-citation>
         </ref>
         <ref id="d845e667a1310">
            <label>22</label>
            <mixed-citation id="d845e674" publication-type="other">
Bailey, N. T. J. 1982 The biomathematics of malaria.
London, UK: C. Griffin &amp; Co.</mixed-citation>
         </ref>
         <ref id="d845e685a1310">
            <label>23</label>
            <mixed-citation id="d845e692" publication-type="other">
Dietz, K., Molineaux, L. &amp; Thomas, A. 1974 A malaria
model tested in the African savannah. Bull. World Health
Organ. 50, 347-357.</mixed-citation>
         </ref>
         <ref id="d845e705a1310">
            <label>24</label>
            <mixed-citation id="d845e712" publication-type="other">
Walton, G. 1947 On the control of malaria in Freetown,
Sierra Leone: 1. Plasmodium falciparum and Anopheles
gambiae in relation to malaria occurring in infants. Ann.
Trop. Med. Parasitol. 41, 380-407.</mixed-citation>
         </ref>
         <ref id="d845e728a1310">
            <label>25</label>
            <mixed-citation id="d845e735" publication-type="other">
Klein, E. Y., Smith, D. L., Boni, M. F. &amp; Laxminarayan,
R. 2008 Clinically immune hosts as a refuge for drug-
sensitive malaria parasites. Malar. J. 7, 67. (doi:10.
1186/1475-2875-7-67)</mixed-citation>
         </ref>
         <ref id="d845e751a1310">
            <label>26</label>
            <mixed-citation id="d845e758" publication-type="other">
Aneke, S. J. 2002 Mathematical modelling of drug resist-
ant malaria parasites and vector populations. Math.
Methods Appl. Sci. 25, 335-346. (doi:10.1002/mma.291)</mixed-citation>
         </ref>
         <ref id="d845e771a1310">
            <label>27</label>
            <mixed-citation id="d845e778" publication-type="other">
Esteva, L., Gumel, A. B. &amp; de León, C. V. 2009 Qualitat-
ive study of transmission dynamics of drug-resistant
malaria. Math. Comput. Model 50, 611-630. (doi:10.
1016/j.mcm.2009.02.012)</mixed-citation>
         </ref>
         <ref id="d845e794a1310">
            <label>28</label>
            <mixed-citation id="d845e801" publication-type="other">
Koella, J. &amp; Antia, R. 2003 Epidemiological models for
the spread of anti-malarial resistance. Malar. J. 2, 3.
(doi:10.1186/1475-2875-2-3)</mixed-citation>
         </ref>
         <ref id="d845e815a1310">
            <label>29</label>
            <mixed-citation id="d845e822" publication-type="other">
Slatkin, M. 1974 Competition and regional coexistence.
Ecology SS, 128-134. (doi:10.2307/1934625)</mixed-citation>
         </ref>
         <ref id="d845e832a1310">
            <label>30</label>
            <mixed-citation id="d845e839" publication-type="other">
Cohen, J. E. 1970 A Markov contingency-table model for
replicated Lo tka-Volterra systems near equilibrium. Am.
Nat. 104, 547-560. (doi:10.1086/282689)</mixed-citation>
         </ref>
         <ref id="d845e852a1310">
            <label>31</label>
            <mixed-citation id="d845e859" publication-type="other">
Wargo, A. R., Huijben, S., de Roode, J. C., Shepherd, J. &amp;
Read, A. F. 2007 Competitive release and facilitation of
drug-resistant parasites after therapeutic chemotherapy
in a rodent malaria model. Proc. Natl Acad. Sci. USA
104, 19 914-19 919. (doi:10.1073/pnas.0707766104)</mixed-citation>
         </ref>
         <ref id="d845e878a1310">
            <label>32</label>
            <mixed-citation id="d845e885" publication-type="other">
O'Meara, W. R, Smith, D. L. &amp; McKenzie, F. E. 2006
Potential impact of intermittent preventive treatment
(IPT) on spread of drug-resistant malaria. PLoS Med.
3, e141. (doi:10.1371/journal.pmed.0030141)</mixed-citation>
         </ref>
         <ref id="d845e901a1310">
            <label>33</label>
            <mixed-citation id="d845e908" publication-type="other">
Hastings, I. M. 1997 A model for the origins and spread
of drug-resistant malaria. Parasitology 115, 133-141.
(doi:10.1017/S0031182097001261)</mixed-citation>
         </ref>
         <ref id="d845e921a1310">
            <label>34</label>
            <mixed-citation id="d845e928" publication-type="other">
Hastings, I. M. 2006 Complex dynamics and stability
of resistance to antimalarial drugs. Parasitology 132,
615-624. (doi:10.1017/S0031182005009790)</mixed-citation>
         </ref>
         <ref id="d845e942a1310">
            <label>35</label>
            <mixed-citation id="d845e949" publication-type="other">
Smith, D. L., Battle, K. E., Hay, S. I., Barker, C. M.,
Scott, T. W. &amp; McKenzie, F. E. 2012 Ross, Macdonald,
and a theory for the dynamics and control of mosquito-
transmitted pathogens. PLoS Pathog. 8, e1002588.
(doi:10.1371/journal.ppat.1002588)</mixed-citation>
         </ref>
         <ref id="d845e968a1310">
            <label>36</label>
            <mixed-citation id="d845e975" publication-type="other">
Macdonald, G. 1957 The epidemiology and control of
malaria. London, UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d845e985a1310">
            <label>37</label>
            <mixed-citation id="d845e992" publication-type="other">
Smith, D. L. &amp; McKenzie, F. E. 2004 Statics and
dynamics of malaria infection in Anopheles mosquitoes.
Malar. J. 3, 13. (doi:10.1186/1475-2875-3-13)</mixed-citation>
         </ref>
         <ref id="d845e1005a1310">
            <label>38</label>
            <mixed-citation id="d845e1012" publication-type="other">
Medica, D. L. &amp; Sinnis, P. 2005 Quantitative dynamics of
Plasmodium yoelii sporozoite transmission by infected
Anopheline mosquitoes. Infect. Immun. 73, 4363-4369.
(doi:10.1128/IAI.73.7.4363-4369.2005)</mixed-citation>
         </ref>
         <ref id="d845e1028a1310">
            <label>39</label>
            <mixed-citation id="d845e1035" publication-type="other">
Shute, P. G. 1945 An investigation into the number of
sporozoites found in the salivary glands of Anopheles mos-
quitoes. Trans. R. Soc. Trop. Med. Hyg. 38, 493-498.
(doi:10.1016/0035-9203(45)90058-7)</mixed-citation>
         </ref>
         <ref id="d845e1051a1310">
            <label>40</label>
            <mixed-citation id="d845e1058" publication-type="other">
Rosenberg, R., Wirtz, R. A., Schneider, I. &amp; Bürge, R.
1990 An estimation of the number of malaria sporo-
zoites ejected by a feeding mosquito. Trans. R. Soc.
Trop. Med. Hyg. 84, 209-212. (doi:10.1016/0035-
9203(90)90258-G)</mixed-citation>
         </ref>
         <ref id="d845e1078a1310">
            <label>41</label>
            <mixed-citation id="d845e1085" publication-type="other">
Druilhe, P., Daubersies, P., Patarapotikul, J., Gentil, C.,
Chene, L., Chongsuphajaisiddhi, T., Mellouk, S. &amp;
Langsley, G. 1998 A primary malarial infection is com-
posed of a very wide range of genetically diverse but
related parasites. J. Clin. Invest. 101, 2008-2016.
(doi:10.1172/JCI19890)</mixed-citation>
         </ref>
         <ref id="d845e1108a1310">
            <label>42</label>
            <mixed-citation id="d845e1115" publication-type="other">
Sinden, R. E. &amp; Billingsley, P. F. 2001 Plasmodium inva-
sion of mosquito cells: hawk or dove? Trends Parasitol.
17, 209-211. (doi:10.1016/S1471-4922(01)01928-6)</mixed-citation>
         </ref>
         <ref id="d845e1128a1310">
            <label>43</label>
            <mixed-citation id="d845e1135" publication-type="other">
Lipsitch, M., Colijn, C., Cohen, T., Hanage, W. P. &amp;
Fraser, C. 2009 No coexistence for free: neutral null
models for multistrain pathogens. Epidemics 1, 2.
(doi:10.1016/j.epidem.2008.07.001)</mixed-citation>
         </ref>
         <ref id="d845e1151a1310">
            <label>44</label>
            <mixed-citation id="d845e1158" publication-type="other">
Garrett-Jones, C. 1964 The human blood index of
malaria vectors in relation to epidemiological assessment.
Bull. World Health Orean. 30, 241-261.</mixed-citation>
         </ref>
         <ref id="d845e1171a1310">
            <label>45</label>
            <mixed-citation id="d845e1178" publication-type="other">
Hastings, I. M., Nsanzabana, C. &amp; Smith, T. A. 2010
A comparison of methods to detect and quantify
the markers of antimalarial drug resistance. Am. J.
Trop. Med. Hyg. 83, 489-495. (doi:10.4269/ajtmh.
2010.10-0072)</mixed-citation>
         </ref>
         <ref id="d845e1197a1310">
            <label>46</label>
            <mixed-citation id="d845e1204" publication-type="other">
Bioland, P. B., Kazembe, P. N., Oloo, A. J., Himonga, B.,
Barat, L. M. &amp; Ruebush, T. K. 1998 Chloroquine in
Africa: critical assessment and recommendations for
monitoring and evaluating chloroquine therapy efficacy
in sub-Saharan Africa. Trop. Med. Int. Health 3,
543-552. (doi:10.1046/J.1365-3156.1998.00270.x)</mixed-citation>
         </ref>
         <ref id="d845e1228a1310">
            <label>47</label>
            <mixed-citation id="d845e1235" publication-type="other">
White, N. J. 1998 Preventing antimalarial drug resistance
through combinations. Drug Resist. Updates 1, 3-9.
(doi:10.1016/S1368-7646(98)80208-2)</mixed-citation>
         </ref>
         <ref id="d845e1248a1310">
            <label>48</label>
            <mixed-citation id="d845e1255" publication-type="other">
White, N. J. &amp; Pongtavornpinyo, W. 2003 The de novo
selection of drug-resistant malaria parasites.
Proc. R. Soc. Lond. B 270, 545-554. (doi:10.1098/rspb.
2002.2241)</mixed-citation>
         </ref>
         <ref id="d845e1271a1310">
            <label>49</label>
            <mixed-citation id="d845e1278" publication-type="other">
Smith, D. L., Dushoff, J., Snow, R. W. &amp; Hay, S. I. 2005
The entomological inoculation rate and Plasmodium
falciparum infection in African children. Nature 438,
492-495. (doi:10.1038/nature04024)</mixed-citation>
         </ref>
         <ref id="d845e1294a1310">
            <label>50</label>
            <mixed-citation id="d845e1301" publication-type="other">
Smith, D. &amp; Hay, S. 2009 Endemicity response timelines
for Plasmodium falciparum elimination. Malar J 8, 87.
(doi:10.1186/1475-2875-8-87)</mixed-citation>
         </ref>
         <ref id="d845e1314a1310">
            <label>51</label>
            <mixed-citation id="d845e1321" publication-type="other">
Sama, W., Owusu-Agyei, S., Felger, I., Dietz, K. &amp;
Smith, T. 2006 Age and seasonal variation in the tran-
sition rates and detectability of Plasmodium falciparum
malaria. Parasitology 132, 13-21. (doi:10.1017/
S0001182005008607)</mixed-citation>
         </ref>
         <ref id="d845e1340a1310">
            <label>52</label>
            <mixed-citation id="d845e1347" publication-type="other">
Hayward, R., Saliba, K. J. &amp; Kirk, K. 2005 pfmdr1
mutations associated with chloroquine resistance incur a
fitness cost in Plasmodium falciparum. Mol. Microbiol. 55,
1285-1295. (doi:10.1111/j.1365-2958.2004.04470.x)</mixed-citation>
         </ref>
         <ref id="d845e1364a1310">
            <label>53</label>
            <mixed-citation id="d845e1371" publication-type="other">
Peters, J. M., Chen, N., Gatton, M., Korsinczky, M.,
Fowler, E. V., Manzetti, S., Saul, A. &amp; Cheng, Q. 2002
Mutations in cytochrome b resulting in atovaquone
resistance are associated with loss of fitness in Plasmodium
falciparum. Antimicrob. Agents Chemother. 46, 2435-2441.
(doi:10.1128/aac.46.8.2435-2441.2002)</mixed-citation>
         </ref>
         <ref id="d845e1394a1310">
            <label>54</label>
            <mixed-citation id="d845e1401" publication-type="other">
Chawira, A. N., Warhurst, D. C. &amp; Peters, W. 1986 Qin-
ghaosu resistance in rodent malaria. Trans. R. Soc. Trop.
Med. Hyg. 80, 477-480. (doi:10.1016/0035-9203(86)
90351-2)</mixed-citation>
         </ref>
         <ref id="d845e1417a1310">
            <label>55</label>
            <mixed-citation id="d845e1424" publication-type="other">
Walliker, D., Hunt, P. &amp; Babiker, H. 2005 Fitness
of drug-resistant malaria parasites. Acta Trop. 94,
251-259. (doi:10.1016/j.actatropica.2005.04.005)</mixed-citation>
         </ref>
         <ref id="d845e1437a1310">
            <label>56</label>
            <mixed-citation id="d845e1446" publication-type="other">
Barnes, K. I. &amp; White, N. J. 2005 Population biology and
antimalarial resistance: the transmission of antimalarial
drug resistance in Plasmodium falciparum. Acta Trop. 94,
230-240. (doi:10.1016/j.actatropica.2005.04.014)</mixed-citation>
         </ref>
         <ref id="d845e1462a1310">
            <label>57</label>
            <mixed-citation id="d845e1469" publication-type="other">
Dondorp, A. M., Yeung, S., White, L., Nguon, C.,
Day, N. P. J., Socheat, D. &amp; von Seidlein, L. 2010 Arte-
misinin resistance: current status and scenarios for
containment. Nat. Rev. Microbiol. 8, 272-280. (doi:10.
1038/nrmicro2331)</mixed-citation>
         </ref>
         <ref id="d845e1488a1310">
            <label>58</label>
            <mixed-citation id="d845e1495" publication-type="other">
Pongtavornpinyo, W., Yeung, S., Hastings, I., Dondorp,
A., Day, N. &amp; White, N. 2008 Spread of anti-malarial
drug resistance: mathematical model with implications
for ACT drug policies. Malar. J. 7, 229. (doi:10.1186/
1475-2875-7-229)</mixed-citation>
         </ref>
         <ref id="d845e1515a1310">
            <label>59</label>
            <mixed-citation id="d845e1522" publication-type="other">
Picot, S., Olliaro, P., de Monbrison, F., Bienvenu, A. L.,
Price, R. N. &amp; Ringwald, P. 2009 A systematic review and
meta-analysis of evidence for correlation between mol-
ecular markers of parasite resistance and treatment
outcome in falciparum malaria. Malar. J. 8, 89. (doi:10.
1186/1475-2875-8-89)</mixed-citation>
         </ref>
         <ref id="d845e1545a1310">
            <label>60</label>
            <mixed-citation id="d845e1552" publication-type="other">
Laufer, M. K., Takala-Harrison, S., Dzinjalamala,
F. K., Stine, O. C., Taylor, T. E. &amp; Plowe, C. V.
2010 Return of chloroquine-susceptible falciparum
malaria in Malawi was a reexpansion of diverse sus-
ceptible parasites. J. Infect. Dis. 202, 801-808. (doi:10.
1086/655659)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
