<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">consbiol</journal-id>
         <journal-id journal-id-type="jstor">j100791</journal-id>
         <journal-title-group>
            <journal-title>Conservation Biology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Science</publisher-name>
         </publisher>
         <issn pub-type="ppub">08888892</issn>
         <issn pub-type="epub">15231739</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3588955</article-id>
         <title-group>
            <article-title>Weitzman's Approach and Conservation of Breed Diversity: An Application to African Cattle Breeds</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>S. B.</given-names>
                  <surname>Reist-Marti</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>H.</given-names>
                  <surname>Simianer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>J.</given-names>
                  <surname>Gibson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>O.</given-names>
                  <surname>Hanotte</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>J. E. O.</given-names>
                  <surname>Rege</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">17</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id>i369432</issue-id>
         <fpage>1299</fpage>
         <lpage>1311</lpage>
         <page-range>1299-1311</page-range>
         <permissions>
            <copyright-statement>Copyright 2003 Society for Conservation Biology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3588955"/>
         <abstract>
            <p>To evaluate the Weitzman approach for assessing alternative conservation strategies for breed (population) diversity, we used genetic distance measures between 49 African cattle breeds divided into two groups of 26 taurine Bos taurus) and sanga (Bos taurus x Bos indicus) breeds and 23 zebu (Bos indicus) and zenga (sanga x Bos indicus) breeds. The derived maximum-likelihood trees clearly displayed the geographic distribution and the zebu-taurine admixture of the breeds. We developed a novel scheme to estimate the extinction probability for each breed which considered total population size, change of population size over the last 10 years, distribution of the breed, risk of indiscriminate crossing, organization among farmers, establishment of conservation schemes, political situation of the countries, special traits, sociocultural importance, and reliability of information. This scheme yielded reasonable estimates of extinction probabilities for the analyzed breeds, which were significantly influenced by the population size and its recent change, distribution of the breed, organization among farmers, establishment of conservation schemes, and reliability of information. The average extinction probability over all breeds and for each breed group was around 48%. Diversity in the zebu-zenga group was only half the diversity in the taurine-sanga group, mainly because of the lower number of breeds and their genetic origin. For both groups, the expected diversity after 20-50 years was about half the current diversity, and the coefficient of variation was about 20%. Our results suggest that the optimum conservation strategy is to give priority to those breeds with the highest marginal diversity, rather than to the most endangered breeds; thus, Madagascar Zebu and Muturu should be given conservation priority in their respective groups. Our study demonstrates that efficient conservation of genetic diversity with limited funds has to take genetic and nongenetic factors into account. Nongenetic factors are accounted for within our scheme to derive extinction probabilities. Within-breed and within-population diversity are not accounted for. Extending Weitzman's basic approach accordingly could yield an effective methodology for determining conservation strategies under highly varying circumstances and for many species, including wild organisms.</p>
         </abstract>
         <kwd-group>
            <kwd>Africa</kwd>
            <kwd>Cattle Breeds</kwd>
            <kwd>Diversity</kwd>
            <kwd>Extinction Probability</kwd>
            <kwd>Weitzman Approach</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d825e255a1310">
            <mixed-citation id="d825e259" publication-type="book">
Barker, J. S. F. 1994. Animal breeding and conservation genetics. Pages
381-395 in V. Loeschcke, J. Tomiuk, and S. K. Jain, editors. Conser-
vation genetics. Birkhauser Verlag, Basel.<person-group>
                  <string-name>
                     <surname>Barker</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Animal breeding and conservation genetics</comment>
               <fpage>381</fpage>
               <source>Conservation genetics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d825e294a1310">
            <mixed-citation id="d825e298" publication-type="journal">
Barker, J. S. F. 1999. Conservation of livestock breed diversity. Animal
Genetic Resources Information 25:33-43.<person-group>
                  <string-name>
                     <surname>Barker</surname>
                  </string-name>
               </person-group>
               <fpage>33</fpage>
               <volume>25</volume>
               <source>Animal Genetic Resources Information</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d825e330a1310">
            <mixed-citation id="d825e334" publication-type="book">
Bremond, J., editor. 2001. Molekulargenetische Differenzierung ver-
schiedener Rotviehpopulationen. Bundesministerium für Verbrauch-
erschutz, Ernihrung und Landwirtschaft, Miinster-Hiltrup, Germany.<person-group>
                  <string-name>
                     <surname>Bremond</surname>
                  </string-name>
               </person-group>
               <source>Molekulargenetische Differenzierung verschiedener Rotviehpopulationen</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d825e363a1310">
            <mixed-citation id="d825e367" publication-type="journal">
Caballero, A., and M. A. Toro. 2002. Analysis of genetic diversity for
the management of conserved subdivided populations. Conserva-
tion Genetics 3:289-299.<person-group>
                  <string-name>
                     <surname>Caballero</surname>
                  </string-name>
               </person-group>
               <fpage>289</fpage>
               <volume>3</volume>
               <source>Conservation Genetics</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d825e403a1310">
            <mixed-citation id="d825e407" publication-type="journal">
Cation, J., P. Alexandrino, I. Bessa, C. Carleos, Y. Carretero, S. Dunner,
N. Ferran, D. Garcia, J. Jordana, D. Laloe, A. Pereira, A. Sanchez,
and K. Moazami-Goudarzi. 2001. Genetic diversity measures of lo-
cal European beef cattle breeds for conservation purposes. Genet-
ics Selection Evolution 33:311-332.<person-group>
                  <string-name>
                     <surname>Cation</surname>
                  </string-name>
               </person-group>
               <fpage>311</fpage>
               <volume>33</volume>
               <source>Genetics Selection Evolution</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d825e448a1310">
            <mixed-citation id="d825e452" publication-type="journal">
Crozier, R. H. 1992. Genetic diversity and the agony of choice. Biologi-
cal Conservation 61:11-15.<person-group>
                  <string-name>
                     <surname>Crozier</surname>
                  </string-name>
               </person-group>
               <fpage>11</fpage>
               <volume>61</volume>
               <source>Biological Conservation</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d825e484a1310">
            <mixed-citation id="d825e488" publication-type="book">
Cunningham, P. 1996. Genetic diversity in domestic animals: strategies
for conservation and development. Pages 13-23 in R. H. Miller, V.
G. Pursel, and H. D. Norman, editors. Biotechnology's role in the
genetic improvement of farm animals. American Society of Animal
Science, Savoy, Illinois.<person-group>
                  <string-name>
                     <surname>Cunningham</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Genetic diversity in domestic animals: strategies for conservation and development</comment>
               <fpage>13</fpage>
               <source>Biotechnology's role in the genetic improvement of farm animals</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d825e529a1310">
            <mixed-citation id="d825e533" publication-type="journal">
d'Ieteren, G., and K. Kimani. 2001. Indigenous genetic resources: a
sustainable and environmentally friendly option for livestock pro-
duction in areas at risk from trypanosomes. Science in Africa (1):
http://www.scienceinafrica.co.za/ndamafull.htm.<person-group>
                  <string-name>
                     <surname>d'Ieteren</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <source>Science in Africa</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d825e568a1310">
            <mixed-citation id="d825e572" publication-type="other">
Eidgenéssisches Departement für auswartige Angelegenheiten (EDA).
2001. Reisehinweise. EDA Sektion für konsularischen Schutz, Bern,
Switzerland. Available from http://www.eda.admin.ch/eda/g/home/
traliv/travel/travelad.html#0008 (accessed January 2001 ).</mixed-citation>
         </ref>
         <ref id="d825e588a1310">
            <mixed-citation id="d825e592" publication-type="other">
Eding, H. 2002. Conservation of genetic diversity: assessing genetic
variation using marker estimated kinships. Ph.D. thesis. Wagenin-
gen University, Wageningen, The Netherlands.</mixed-citation>
         </ref>
         <ref id="d825e606a1310">
            <mixed-citation id="d825e610" publication-type="book">
Eding, J. H., and G. Laval. 1999. Measuring the genetic uniqueness in
livestock. Pages 33-58 in J. K. Oldenbroek, editor. Genebanks and
the conservation of farm animal genetic resources. Institute for An-
imal Science and Health, Lelystad, The Netherlands.<person-group>
                  <string-name>
                     <surname>Eding</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Measuring the genetic uniqueness in livestock</comment>
               <fpage>33</fpage>
               <source>Genebanks and the conservation of farm animal genetic resources</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d825e648a1310">
            <mixed-citation id="d825e652" publication-type="journal">
Eding, H., and T. H. E. Meuwissen. 2001. Marker-based estimates of
between and within population kinships for the conservation of
genetic diversity. Journal of Animal Breeding and Genetics 118:
141-159.<person-group>
                  <string-name>
                     <surname>Eding</surname>
                  </string-name>
               </person-group>
               <fpage>141</fpage>
               <volume>118</volume>
               <source>Journal of Animal Breeding and Genetics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d825e690a1310">
            <mixed-citation id="d825e694" publication-type="book">
Epstein, H., and I. L. Mason. 1984. Cattle. Pages 6-27 in I. L. Mason, ed-
itor. Evolution of domesticated animals. Longman, London.<person-group>
                  <string-name>
                     <surname>Epstein</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cattle</comment>
               <fpage>6</fpage>
               <source>Evolution of domesticated animals</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d825e726a1310">
            <mixed-citation id="d825e730" publication-type="journal">
Faith, D. P. 1992. Conservation evaluation and phylogenetic diversity.
Biological Conservation 61:1-10.<person-group>
                  <string-name>
                     <surname>Faith</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>61</volume>
               <source>Biological Conservation</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d825e762a1310">
            <mixed-citation id="d825e766" publication-type="book">
Felius, M. 1995. Cattle breeds: an encyclopedia. Misset uitgeverji by,
Doetinchem, The Netherlands.<person-group>
                  <string-name>
                     <surname>Felius</surname>
                  </string-name>
               </person-group>
               <source>Cattle breeds: an encyclopedia</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d825e791a1310">
            <mixed-citation id="d825e795" publication-type="other">
Felsenstein, J. 1993. PHYLIP (phylogeny inference package). Version
3.57c. Department of Genetics, University of Washington, Seattle.
Available from http://evolution.genetics.washington.edu/phylip.html
(accessed November 2001).</mixed-citation>
         </ref>
         <ref id="d825e812a1310">
            <mixed-citation id="d825e816" publication-type="book">
Fitzhugh, H. A. 1992. Appendix II: what is a breed? Page 123 in J. E. O.
Rege and M. E. Lipner, editors. African animal genetic resources:
their characterisation, conservation and utilisation. Proceedings of
a research planning workshop. International Livestock Center for
Africa, Addis Ababa, Ethiopia.<person-group>
                  <string-name>
                     <surname>Fitzhugh</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Appendix II: what is a breed?</comment>
               <fpage>123</fpage>
               <source>African animal genetic resources: their characterisation, conservation and utilisation</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d825e857a1310">
            <mixed-citation id="d825e861" publication-type="other">
Food and Agriculture Organization of the United Nations (FAO). 2000.
DAD-IS (global system for domestic animal diversity) Version 2.0.
FAO, Rome. Available from http://www.fao.org/dad-is (accessed
January 2001).</mixed-citation>
         </ref>
         <ref id="d825e877a1310">
            <mixed-citation id="d825e881" publication-type="book">
Hammond, K., and H. W. Leitch. 1996. The FAO Global Program for
the Management of Farm Animal Genetic Resources. Pages 24-42
in R. H. Miller, V. G. Pursel, and H. D. Norman, editors. Biotechnol-
ogy's role in the genetic improvement of farm animals. American
Society of Animal Science, Savoy, Illinois.<person-group>
                  <string-name>
                     <surname>Hammond</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The FAO Global Program for the Management of Farm Animal Genetic Resources</comment>
               <fpage>24</fpage>
               <source>Biotechnology's role in the genetic improvement of farm animals</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d825e922a1310">
            <mixed-citation id="d825e926" publication-type="journal">
Hanotte, O., C. L. Tawah, D. G. Bradley, M. Okomo, Y. Verjee, J.
Ochieng, and J. E. O. Rege. 2000. Geographic distribution and fre-
quency of a taurine Bos taurus and an indicine Bos indicus Y spe-
cific allele amongst sub-Saharan African cattle breeds. Molecular
Ecology 9:387-396.<person-group>
                  <string-name>
                     <surname>Hanotte</surname>
                  </string-name>
               </person-group>
               <fpage>387</fpage>
               <volume>9</volume>
               <source>Molecular Ecology</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d825e967a1310">
            <mixed-citation id="d825e971" publication-type="journal">
Hanotte, O., D. G. Bradley, J. W. Ochieng, Y. Verjee, E. W. Hill, and
J. E. O. Rege. 2002. African pastoralism: genetic imprints of origins
and migrations. Science 296:336-339.<object-id pub-id-type="jstor">10.2307/3076523</object-id>
               <fpage>336</fpage>
            </mixed-citation>
         </ref>
         <ref id="d825e990a1310">
            <mixed-citation id="d825e994" publication-type="book">
International Livestock Research Institute (ILRI). 2001. DAGRIS (do-
mestic animal genetic resources information system). Animal Ge-
netic Resources Programme, ILRI, Nairobi, Kenya.<person-group>
                  <string-name>
                     <surname>International Livestock Research Institute (ILRI)</surname>
                  </string-name>
               </person-group>
               <source>DAGRIS (domestic animal genetic resources information system)</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1024a1310">
            <mixed-citation id="d825e1028" publication-type="journal">
Laval, G., N. Iannuccelli, C. Legault, D. Milan, M. A. M. Groenen, E.
Giuffra, L. Andersson, P. H. Nissen, C. B. Jorgensen, P. Beeckmann,
H. Geldermann, J. L. Foulley, C. Chevalet, and L. Ollivier. 2000. Ge-
netic diversity of eleven European pig breeds. Genetics Selection
Evolution 32:187-203.<person-group>
                  <string-name>
                     <surname>Laval</surname>
                  </string-name>
               </person-group>
               <fpage>187</fpage>
               <volume>32</volume>
               <source>Genetics Selection Evolution</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1069a1310">
            <mixed-citation id="d825e1073" publication-type="book">
Mason, I. L. 1988. World dictionary of livestock breeds. 3rd edition.
CAB International, Wallingford, United Kingdom.<person-group>
                  <string-name>
                     <surname>Mason</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>World dictionary of livestock breeds</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1102a1310">
            <mixed-citation id="d825e1106" publication-type="book">
Oldenbroek, J. K. 1999. Introduction. Pages 1-9 in J. K. Oldenbroek,
editor. Genebanks and the conservation of farm animal genetic re-
sources. Institute for Animal Science and Health, Lelystad, The
Netherlands.<person-group>
                  <string-name>
                     <surname>Oldenbroek</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Introduction</comment>
               <fpage>1</fpage>
               <source>Genebanks and the conservation of farm animal genetic resources</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1144a1310">
            <mixed-citation id="d825e1150" publication-type="other">
Page, R. D. M. 1996. Tree View: an application to display phylogenetic
trees on personal computers. Computer Applications in the Bio-
sciences 12:357-358. Available from http://taxonomy.zoology.
gla.ac.uk/rod/treeview.html (accessed December 2001).</mixed-citation>
         </ref>
         <ref id="d825e1166a1310">
            <mixed-citation id="d825e1170" publication-type="book">
Reed, C. A. 1984. The beginnings of animal domestication. Pages 1-6
in I. L. Mason, editor. Evolution of domesticated animals. Longman,
London.<person-group>
                  <string-name>
                     <surname>Reed</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The beginnings of animal domestication</comment>
               <fpage>1</fpage>
               <source>Evolution of domesticated animals</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1205a1310">
            <mixed-citation id="d825e1209" publication-type="journal">
Rege, J. E. O. 1999. The state of African cattle genetic resources. I.
Classification frame work and identification of threatened and ex-
tinct breeds. Animal Genetic Resources Information 25:1-25.<person-group>
                  <string-name>
                     <surname>Rege</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>25</volume>
               <source>Animal Genetic Resources Information</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1245a1310">
            <mixed-citation id="d825e1249" publication-type="journal">
Reynolds, J., B. S. Weir, and C. C. Cockerham. 1983. Estimation of the
coancestry coefficient basis for a short-term genetic distance. Ge-
netics 105:767-779.<person-group>
                  <string-name>
                     <surname>Reynolds</surname>
                  </string-name>
               </person-group>
               <fpage>767</fpage>
               <volume>105</volume>
               <source>Genetics</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1284a1310">
            <mixed-citation id="d825e1288" publication-type="book">
Ruane, J. 1999. Selecting breeds for conservation. Pages 59-73 in J. K.
Oldenbroek, editor. Genebanks and the conservation of farm ani-
mal genetic resources. Institute for Animal Science and Health,
Lelystad, The Netherlands.<person-group>
                  <string-name>
                     <surname>Ruane</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Selecting breeds for conservation</comment>
               <fpage>59</fpage>
               <source>Genebanks and the conservation of farm animal genetic resources</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1326a1310">
            <mixed-citation id="d825e1330" publication-type="journal">
Ruane, J. 2000. A framework for prioritizing domestic animal breeds
for conservation purposes at the national level: a Norwegian case
study. Conservation Biology 14:1385-1393.<object-id pub-id-type="jstor">10.2307/2641791</object-id>
               <fpage>1385</fpage>
            </mixed-citation>
         </ref>
         <ref id="d825e1349a1310">
            <mixed-citation id="d825e1353" publication-type="book">
Scherf, B. D. editor. 2000. World watch list for domestic animal diver-
sity. 3rd edition. Food and Agriculture Organization of the United
Nations, Rome.<person-group>
                  <string-name>
                     <surname>Scherf</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>World watch list for domestic animal diversity</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1385a1310">
            <mixed-citation id="d825e1389" publication-type="journal">
Simianer, H., S. B. Marti, J. Gibson, O. Hanotte, and J. E. O. Rege. 2003.
An approach to the optimal allocation of conservation funds to
minimise loss of genetic diversity between livestock breeds. Eco-
logical Economics 45:377-392.<person-group>
                  <string-name>
                     <surname>Simianer</surname>
                  </string-name>
               </person-group>
               <fpage>377</fpage>
               <volume>45</volume>
               <source>Ecological Economics</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1427a1310">
            <mixed-citation id="d825e1431" publication-type="journal">
Takezaki, N., and M. Nei. 1996. Genetic distances and reconstruction
of phylogenetic trees from microsatellite DNA. Genetics 144:
389-399.<person-group>
                  <string-name>
                     <surname>Takezaki</surname>
                  </string-name>
               </person-group>
               <fpage>389</fpage>
               <volume>144</volume>
               <source>Genetics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1467a1310">
            <mixed-citation id="d825e1471" publication-type="journal">
Tawah, C. L., J. E. O. Rege, and G. S. Aboagye. 1997. A close look at a
rare African breed-the Kuri cattle of Lake Chad Basin: origin, dis-
tribution, production and adaptive characteristics. South African
Journal of Animal Science 27:31-40.<person-group>
                  <string-name>
                     <surname>Tawah</surname>
                  </string-name>
               </person-group>
               <fpage>31</fpage>
               <volume>27</volume>
               <source>South African Journal of Animal Science</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1509a1310">
            <mixed-citation id="d825e1513" publication-type="journal">
Thaon d'Arnoldi, C., J. L. Foulley, and L. Ollivier. 1998. An overview of
the Weitzman approach to diversity. Genetics Selection Evolution
30:149-161.<person-group>
                  <string-name>
                     <surname>Thaon d'Arnoldi</surname>
                  </string-name>
               </person-group>
               <fpage>149</fpage>
               <volume>30</volume>
               <source>Genetics Selection Evolution</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d825e1548a1310">
            <mixed-citation id="d825e1552" publication-type="journal">
Weitzman, M. L. 1992. On diversity. Quarterly Journal of Economics
CVII:363-405.<object-id pub-id-type="doi">10.2307/2118476</object-id>
               <fpage>363</fpage>
            </mixed-citation>
         </ref>
         <ref id="d825e1568a1310">
            <mixed-citation id="d825e1572" publication-type="journal">
Weitzman, M. L. 1993. What to preserve? An application of diversity
theory to crane conservation. Quarterly Journal of Economics
CVIII:157-183.<object-id pub-id-type="doi">10.2307/2118499</object-id>
               <fpage>157</fpage>
            </mixed-citation>
         </ref>
         <ref id="d825e1591a1310">
            <mixed-citation id="d825e1595" publication-type="journal">
Weitzman, M. L. 1998. The Noah's Ark problem. Econometrica 66:
1279-1298.<object-id pub-id-type="doi">10.2307/2999617</object-id>
               <fpage>1279</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
