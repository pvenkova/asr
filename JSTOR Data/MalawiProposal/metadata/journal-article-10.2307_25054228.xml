<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">worldpolitics</journal-id>
         <journal-id journal-id-type="jstor">j100410</journal-id>
         <journal-title-group>
            <journal-title>World Politics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Johns Hopkins University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00438871</issn>
         <issn pub-type="epub">10863338</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25054228</article-id>
         <title-group>
            <article-title>Clientelism and Voting Behavior: Evidence from a Field Experiment in Benin</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Leonard</given-names>
                  <surname>Wantchekon</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">55</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i25054223</issue-id>
         <fpage>399</fpage>
         <lpage>422</lpage>
         <permissions>
            <copyright-statement>Copyright 2003 The Johns Hopkins University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25054228"/>
         <abstract>
            <p>The author conducted a field experiment in Benin to investigate the impact of clientelism on voting behavior. In collaboration with four political parties involved in the 2001 presidential elections, clientelist and broad public policy platforms were designed and run in twenty randomly selected villages of an average of 756 registered voters. Using the survey data collected after the elections, the author estimated the effect of each type of message by comparing voting behavior in the villages exposed to clientelism or public policy messages (treatment groups) with voting behavior in the other villages (control groups). The author found that clientelist messages have positive and significant effect in all regions and for all types of candidates. The author also found that public policy messages have a positive and significant effect in the South but a negative and significant effect in the North. In addition, public policy messages seem to hurt incumbents as well as regional candidates. Finally, the evidence indicates that female voters tend to have stronger preference for public policy platforms than male voters.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d624e114a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d624e121" publication-type="other">
Robert Bates, Markets and States in Tropical Africa (Berkeley: University of Cal-
ifornia Press, 1982)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e130" publication-type="other">
Jean-François Bayart, LEtat en Afrique: la politique du ventre (Paris: Fayard, 1989)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e136" publication-type="other">
C.James Scott, "Patron-Client Politics and Political Change in Southeast Asia," American Political Sci-
ence Review 66 (March 1972)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e146" publication-type="other">
Michael Bratton and Nicolas van de Walle, "Neopatrimonial
Regimes and Political Transitions in Africa," World Politics 46 (July 1994).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e156a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d624e163" publication-type="other">
Bruce Cain, John Ferejohn, and Morris Fiorina, The Personal Vote: Constituency
Service and Electoral Independence (Cambridge: Harvard University Press, 1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e173a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d624e180" publication-type="other">
Maurice Kugler and Howard Rosenthal, "Checks and Balances: An Assessment of the Institu-
tional Separation of Powers in Colombia," Working Paper, no. 9 (Department of Economics and
Econometrics, University of Southampton, 2000).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e192" publication-type="other">
Brusco, Nazareno, and Stokes,
"Clientelism and Democracy: Evidence from Argentina" (Paper presented at the Yale Conference on
Political Parties and Legislative Organizations in Parliamentary Democracies, 2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d624e205a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d624e212" publication-type="other">
James Robinson and Thierry Verdier, "Political Economy of Clientelism," Working Paper (University
of California, Berkeley, 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e223a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d624e230" publication-type="other">
Herbert Kitschelt, "Linkages between Citizens and Politicians in Democratic Polities," Compara-
tive Political Studies 33 (August-September 2000), 869.</mixed-citation>
            </p>
         </fn>
         <fn id="d624e240a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d624e247" publication-type="other">
Kitschelt (fn. 7)</mixed-citation>
            </p>
         </fn>
         <fn id="d624e254a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d624e261" publication-type="other">
John R. Heilbrunn, "Social Origins of National Conferences in Benin and Togo," Journal of Mod-
ern African Studies 31 (June 1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e270" publication-type="other">
Kathryn Nwajiaku, "The National Conferences in Benin and Togo
Revisited," Journal of 'Modern African Studies 32 (September 1994).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e280a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d624e287" publication-type="other">
Eboussi Boulaga, Les Conferences na-
tionales en Afrique noire (Paris: Editions Karthala, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e297a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d624e304" publication-type="other">
Assar Lindbeck and Jörgen Weibull, "Balanced Budget Distribution as Outcome of Political
Competition," Public Choice 52 (June 1987).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e314a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d624e321" publication-type="other">
Avinash Dixit and John Londregan, "The Determinants of Success of Special Interest in Redis-
tributive Politics," Journal of Politics 58 (November 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e332a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d624e339" publication-type="other">
Alessandro Lizzeri and Nicola Pérsico, "The Problem of Public Goods under Alternative Elec-
toral Incentives," American Economic Review (March 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e349a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d624e356" publication-type="other">
Alan Gerber and Donald Green, "The Effects of Canvassing, Telephone Calls, and Direct Mail
on Voter Turnout: A Field Experiment," American Political Science Review 94 (September 2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e366a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d624e373" publication-type="other">
Clapham, Patronage and Political Power (New York: St. Martins Press, 1982).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e380a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d624e387" publication-type="other">
Institut National de la Statistique et de l'Analyse Economique, Tableau de Bord Social (Coto-
nou, Benin : Publication gouvernementale, 2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e397a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d624e404" publication-type="other">
Marcel Fafchamps and Elini Gabre-Madhin, "Agricultural Markets in Benin and Malawi," Work-
ing Paper 2734 (Washington, D.C.: World Bank, Development Research Group, 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e414a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d624e421" publication-type="other">
Banegas, La Démocratie à pas de caméléon (Paris: Editions
Karthala, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e432a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d624e439" publication-type="other">
Lindbeck and Weibull (fn. 13).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e446a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d624e453" publication-type="other">
Dixit and Londregan (fn. 14).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e460a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d624e467" publication-type="other">
Avinash Dixit and John Londregan, "Ideology, Tactics, and Efficiency in Redistributive Politics,"
Quarterly Journal of Economics 113 (May 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e477a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d624e484" publication-type="other">
Raghabendra Chattopadyay and Esther Duflo, "Women's Leadership and Policy Decisions: Evi-
dence from a National Randomized Experiment in India," Working Paper (Cambridge: MIT, 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e494a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d624e501" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d624e508a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d624e515" publication-type="other">
René Lemarchand, "Political Clientelism and Ethnicity in Tropical Africa: Competing Solidari-
ties in Nation-Building," American Political Science Review 66 (March 1972).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e526a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d624e533" publication-type="other">
Ronald Fisher, The Design of Experiments (London: Oliver and Boyd, 1935).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e540a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d624e547" publication-type="other">
Harold Grosnell, Getting Out the Vote: An Experiment in the Stimulation of Voting (Chicago: Uni-
versity of Chicago Press, 1927)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e556" publication-type="other">
Samuel J. Elderveld, "Experimental Propaganda Techniques and Vot-
ing Behavior," American Political Science Review 50 (March 1956)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e565" publication-type="other">
William C. Adams and Dennis
Smith, "Effects of Telephone Canvassing on Turnout and Preferences: A Field Experiment," Public
Opinion Quarterly AA (Autumn 1980)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e578" publication-type="other">
Roy E. Miller, David Bositis, and Denise Baer, "Stimulating
Voter Turnout in a Primary with a Precinct Committeeman," International Political Science Review 2,
no. 4 (1981)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d624e590" publication-type="other">
Gerber and Green (fn. 17).</mixed-citation>
            </p>
         </fn>
         <fn id="d624e597a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d624e604" publication-type="other">
Gerber and Green (fn. 17).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
