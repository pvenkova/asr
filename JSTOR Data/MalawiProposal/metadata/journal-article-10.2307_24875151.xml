<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">humaecolrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50018540</journal-id>
         <journal-title-group>
            <journal-title>Human Ecology Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>ANU Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10744827</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">22040919</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24875151</article-id>
         <article-categories>
            <subj-group>
               <subject>Research and Theory in Human Ecology</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Urbanization, Slums, and the Carbon Intensity of Well-being:</article-title>
            <subtitle>Implications for Sustainable Development</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Givens</surname>
                  <given-names>Jennifer E.</given-names>
               </string-name>
               <aff>Department of Sociology, Washington State University, Pullman, United States</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2015</year>
            <string-date>2015</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">22</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e24875143</issue-id>
         <fpage>107</fpage>
         <lpage>128</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24875151"/>
         <abstract>
            <label>Abstract</label>
            <p>Previous research in macro comparative environmental sociology analyzes both environmental and human well-being outcomes of urbanization. The carbon intensity of well-being (CIWB) concept simultaneously measures environmental and human well-being. Here I ask how various types of urbanization, an underexplored concept in the CIWB research, contributes differently to the CIWB of nations. Using longitudinal two-way fixed effects Prais-Winsten regression models for the years 1990–2011 for 78 countries, I find that level of development and urbanization are associated with higher CIWB, as are the percentage of urban populations with access to improved water and sanitation; conversely, urban slum prevalence is associated with lower CIWB. Comparing more versus less developed countries, I find the results are especially robust for lower-income countries. I also find that overall population access to water and sanitation is associated with lower CIWB. The findings suggest directions for sustainable development that take into account different forms of urbanization and both rural and urban population well-being.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>urbanization</kwd>
            <kwd>slum</kwd>
            <kwd>well-being</kwd>
            <kwd>carbon</kwd>
            <kwd>development</kwd>
            <kwd>environment</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="r1">
            <mixed-citation publication-type="other">Aßheuer, T., Thiele-Eich, I., &amp; Braun, B. (2013). Coping with the impacts of severe flood events in Dhaka's slums—the role of social capital. Erdkunde, 21–35.</mixed-citation>
         </ref>
         <ref id="r2">
            <mixed-citation publication-type="other">Amnesty International. (2012). Speaking up from the slums. www.amnesty.org/en/latest/news/2012/04/speaking-slums/, accessed June 21, 2015.</mixed-citation>
         </ref>
         <ref id="r3">
            <mixed-citation publication-type="other">Beck, N., &amp; Katz, J. N. (1995). What to do (and not to do) with time-series crosssection data. American Political Science Review, 89(3), 634–647.</mixed-citation>
         </ref>
         <ref id="r4">
            <mixed-citation publication-type="other">Burra, S., Patel, S., &amp; Kerr, T. (2003). Community-designed, built and managed toilet blocks in Indian cities. Environment and Urbanization, 15(2), 11–32.</mixed-citation>
         </ref>
         <ref id="r5">
            <mixed-citation publication-type="other">Chase-Dunn, C. (1975). The effects of international economic dependence on development and inequality: A cross-national study. American Sociological Review, 720–738.</mixed-citation>
         </ref>
         <ref id="r6">
            <mixed-citation publication-type="other">Companion, M. (2010). Commodities and competition: The economic marginalization of female food vendors in northern Mozambique. WSQ: Women's Studies Quarterly, 38(3), 163–181.</mixed-citation>
         </ref>
         <ref id="r7">
            <mixed-citation publication-type="other">Cramer, J. C. (1997). A demographic perspective on air quality: Conceptual issues surrounding environmental impacts of population growth. Human Ecology Review, 3, 191–196.</mixed-citation>
         </ref>
         <ref id="r8">
            <mixed-citation publication-type="other">Cramer, J. C. (1998). Population growth and air quality in California. Demography, 35, 45–56.</mixed-citation>
         </ref>
         <ref id="r9">
            <mixed-citation publication-type="other">Davis, M. (2007). Planet of Slums. New York: Verso.</mixed-citation>
         </ref>
         <ref id="r10">
            <mixed-citation publication-type="other">De Soto, H. (2000). The Mystery of Capital. New York: Basic Books.</mixed-citation>
         </ref>
         <ref id="r11">
            <mixed-citation publication-type="other">Dietz, T., Frey, R. S., &amp; Kalof, L. (1987). Estimation with cross-national data: Robust and nonparametric methods. American Sociological Review, 52, 380–390.</mixed-citation>
         </ref>
         <ref id="r12">
            <mixed-citation publication-type="other">Dietz, T., &amp; Jorgenson, A. (Eds.) (2013). Structural Human Ecology. Pullman, WA: Washington State University Press.</mixed-citation>
         </ref>
         <ref id="r13">
            <mixed-citation publication-type="other">Dietz, T., Kalof, L., &amp; Frey, R. S. (1991). On the utility of robust and resampling procedures. Rural Sociology, 56, 461–474.</mixed-citation>
         </ref>
         <ref id="r14">
            <mixed-citation publication-type="other">Dietz, T., Rosa, E. A., &amp; York, R. (2009). Environmentally efficient well-being: Rethinking sustainability as the relationship between human well-being and environmental impacts. Human Ecology Review, 16(1), 114–123.</mixed-citation>
         </ref>
         <ref id="r15">
            <mixed-citation publication-type="other">Dietz, T., Rosa, E. A., &amp; York, R. (2012). Environmentally efficient well-being: Is there a Kuznets curve? Applied Geography, 32(1), 21–28.</mixed-citation>
         </ref>
         <ref id="r16">
            <mixed-citation publication-type="other">Evans, P. B., &amp; Timberlake, M. (1980). Dependence, inequality, and the growth of the tertiary: A comparative analysis of less developed countries. American Sociological Review, 531–552.</mixed-citation>
         </ref>
         <ref id="r17">
            <mixed-citation publication-type="other">Fazal, S. (2000). Urban expansion and loss of agricultural land – a GIS based study of Saharanpur City, India. Environment and Urbanization, 12(2), 133–149.</mixed-citation>
         </ref>
         <ref id="r18">
            <mixed-citation publication-type="other">Folke, C. (2006). Resilience: The emergence of a perspective for social–ecological systems analyses. Global Environmental Change, 16(3), 253–267.</mixed-citation>
         </ref>
         <ref id="r19">
            <mixed-citation publication-type="other">Firebaugh, G. (1979). Structural determinants of urbanization in Asia and Latin America, 1950–1970. American Sociological Review, 199–215.</mixed-citation>
         </ref>
         <ref id="r20">
            <mixed-citation publication-type="other">Gallaher, C. M., Kerr, J. M., Njenga, M., Karanja, N. K., &amp; WinklerPrins, A. M. (2013). Urban agriculture, social capital, and food security in the Kibera slums of Nairobi, Kenya. Agriculture and Human Values, 30(3), 389–404.</mixed-citation>
         </ref>
         <ref id="r21">
            <mixed-citation publication-type="other">Goldemberg, J., Johansson, T. B., Reddy, A. K., &amp; Williams, R. H. (1985). Basic needs and much more with one kilowatt per capita. Ambio, 190–200.</mixed-citation>
         </ref>
         <ref id="r22">
            <mixed-citation publication-type="other">Jorgenson, A. K. (2011). Carbon dioxide emissions in Central and Eastern European Nations, 1992–2005: A test of ecologically unequal exchange theory. Human Ecology Review, 18(2), 105.</mixed-citation>
         </ref>
         <ref id="r23">
            <mixed-citation publication-type="other">Jorgenson, A. K. (2014). Economic development and the carbon intensity of human well-being. Nature Climate Change, 4(3), 186–189.</mixed-citation>
         </ref>
         <ref id="r24">
            <mixed-citation publication-type="other">Jorgenson, A. K. (2015). Inequality and the carbon intensity of human wellbeing. Journal of Environmental Studies and Sciences, 1–6.</mixed-citation>
         </ref>
         <ref id="r25">
            <mixed-citation publication-type="other">Jorgenson, A. K., Alekseyko, A., &amp; Giedraitis, V. (2014). Energy consumption, human well-being and economic development in central and eastern European nations: A cautionary tale of sustainability. Energy Policy, 66, 419–427.</mixed-citation>
         </ref>
         <ref id="r26">
            <mixed-citation publication-type="other">Jorgenson, A. K., Auerbach, D., &amp; Clark, B. (2014). The (De-) carbonization of urbanization, 1960–2010. Climatic Change, 127(3–4), 561–575.</mixed-citation>
         </ref>
         <ref id="r27">
            <mixed-citation publication-type="other">Jorgenson, A. K., &amp; Dietz, T. (2015). Economic growth does not reduce the ecological intensity of human well-being. Sustainability Science, 10(1), 149–156.</mixed-citation>
         </ref>
         <ref id="r28">
            <mixed-citation publication-type="other">Jorgenson, A. K., &amp; Givens, J. (2015). The changing effect of economic development on the consumption-based carbon intensity of well-being, 1990–2008, PLOSone, 1–14.</mixed-citation>
         </ref>
         <ref id="r29">
            <mixed-citation publication-type="other">Jorgenson, A. K., &amp; Rice, J. (2010). Urban slum growth and human health: A panel study of infant and child mortality in less-developed countries, 1990–2005. Journal of Poverty, 14(4), 382–402.</mixed-citation>
         </ref>
         <ref id="r30">
            <mixed-citation publication-type="other">Jorgenson, A. K., Rice, J., &amp; Clark, B. (2010). Cities, slums, and energy consumption in less developed countries, 1990 to 2005. Organization &amp; Environment, 23(2), 189–204.</mixed-citation>
         </ref>
         <ref id="r31">
            <mixed-citation publication-type="other">Jorgenson, A., Rice, J., &amp; Clark, B. (2012). Assessing the temporal and regional differences in the relationships between infant and child mortality and urban slum prevalence in less developed countries, 1990–2005. Urban Studies, 49(16), 3495–3512.</mixed-citation>
         </ref>
         <ref id="r32">
            <mixed-citation publication-type="other">Kasarda, J. D., &amp; Crenshaw, E. M. (1991). Third world urbanization: Dimensions, theories, and determinants. Annual Review of Sociology, 467–501.</mixed-citation>
         </ref>
         <ref id="r33">
            <mixed-citation publication-type="other">Kentor, J. (1981). Structural determinants of peripheral urbanization: The effects of international dependence. American Sociological Review, 201–211.</mixed-citation>
         </ref>
         <ref id="r34">
            <mixed-citation publication-type="other">Knight, K. W., &amp; Rosa, E. A. (2011). The environmental efficiency of well-being: A cross-national analysis. Social Science Research, 40(3), 931–949.</mixed-citation>
         </ref>
         <ref id="r35">
            <mixed-citation publication-type="other">Lamb, W. F., Steinberger, J. K., Bows-Larkin, A., Peters, G. P., Roberts, J. T., &amp; Wood, F. R. (2014). Transitions in pathways of human development and carbon emissions. Environmental Research Letters, 9(1), 014011–014020.</mixed-citation>
         </ref>
         <ref id="r36">
            <mixed-citation publication-type="other">Liu, J., Daily, G .C., Ehrlich, P. R., &amp; Luck, G. W. (2003). Effects of household dynamics on resource consumption and biodiversity. Nature, 421, 530–533.</mixed-citation>
         </ref>
         <ref id="r37">
            <mixed-citation publication-type="other">Liu, J., Dietz, T., Carpenter, S. R., Folke, C., Alberti, M., Redman, C. L., Schneider, S. H., Ostrom, E., Pell, A. N., Lubchenco, J., Taylor, W. W., Ouyang, Z.,</mixed-citation>
         </ref>
         <ref id="r38">
            <mixed-citation publication-type="other">Deadman, P., Kratz, T., &amp; Provencher, W. (2007). Coupled human and natural systems. AMBIO: A Journal of the Human Environment, 36(8), 639–649.</mixed-citation>
         </ref>
         <ref id="r39">
            <mixed-citation publication-type="other">Liu, Y. S., Wang, J. Y., &amp; Long, H. L. (2010). Analysis of arable land loss and its impact on rural sustainability in Southern Jiangsu Province of China. Journal of Environmental Management, 91(3), 646–653.</mixed-citation>
         </ref>
         <ref id="r40">
            <mixed-citation publication-type="other">London, B. (1987). Structural determinants of third world urban change: An ecological and political economic analysis. American Sociological Review, 28–43.</mixed-citation>
         </ref>
         <ref id="r41">
            <mixed-citation publication-type="other">Mazur, A. (2011). Does increasing energy or electricity consumption improve quality of life in industrial nations?. Energy Policy, 39(5), 2568–2572.</mixed-citation>
         </ref>
         <ref id="r42">
            <mixed-citation publication-type="other">Mazur, A., &amp; Rosa, E. (1974). Energy and life-style. Science, 186(4164), 607–610.</mixed-citation>
         </ref>
         <ref id="r43">
            <mixed-citation publication-type="other">Molotch, H. (1976). The city as a growth machine: Toward a political economy of place. American Journal of Sociology, 309–332.</mixed-citation>
         </ref>
         <ref id="r44">
            <mixed-citation publication-type="other">Morinière, L. (2012). Environmentally influenced urbanisation: Footprints bound for town? Urban Studies, 49(2), 435–450.</mixed-citation>
         </ref>
         <ref id="r45">
            <mixed-citation publication-type="other">O'Connor, J. (1988). Capitalism, nature, socialism: A theoretical introduction. Capitalism, Nature, Socialism, 1, 11–38.</mixed-citation>
         </ref>
         <ref id="r46">
            <mixed-citation publication-type="other">Patel, S., Burra, S., &amp; d'Cruz, C. (2001). Slum/shack dwellers international (SDI)-foundations to treetops. Environment and Urbanization, 13(2), 45–59.</mixed-citation>
         </ref>
         <ref id="r47">
            <mixed-citation publication-type="other">Portes, A. (1998). Social capital: Its origins and applications in modern sociology. Annual Review of Sociology, 24, 1–24.</mixed-citation>
         </ref>
         <ref id="r48">
            <mixed-citation publication-type="other">Preston, S. H. (1979). Urban growth in developing countries: A demographic reappraisal. Population and Development Review, 195–215.</mixed-citation>
         </ref>
         <ref id="r49">
            <mixed-citation publication-type="other">Rees, W. E. (1992). Ecological footprints and appropriated carrying capacity: What urban economics leaves out. Environment and Urbanization, 4(2), 121–130.</mixed-citation>
         </ref>
         <ref id="r50">
            <mixed-citation publication-type="other">Rice, J. (2008). Material consumption and social well-being within the periphery of the world economy: An ecological analysis of maternal mortality. Social Science Research, 37(4), 1292–1309.</mixed-citation>
         </ref>
         <ref id="r51">
            <mixed-citation publication-type="other">Rice, J., &amp; Rice, J. S. (2009). The concentration of disadvantage and the rise of an urban penalty: Urban slum prevalence and the social production of health inequalities in the developing countries. International Journal of Health Services, 39(4), 749–770.</mixed-citation>
         </ref>
         <ref id="r52">
            <mixed-citation publication-type="other">Rice, J., &amp; Rice, J. S. (2012). Debt and the built urban environment: Examining the growth of urban slums in the less developed countries, 1990–2010. Sociological Spectrum, 32(2), 114–137.</mixed-citation>
         </ref>
         <ref id="r53">
            <mixed-citation publication-type="other">Rosa, E. A., &amp; Dietz, T. (2012). Human drivers of national greenhouse-gas emissions. Nature Climate Change, 2(8), 581–586.</mixed-citation>
         </ref>
         <ref id="r54">
            <mixed-citation publication-type="other">Sachs, J. D. (2015). The Age of Sustainable Development. New York: Columbia University Press.</mixed-citation>
         </ref>
         <ref id="r55">
            <mixed-citation publication-type="other">Sanderson, M. R. (2009). Globalization and the environment: Implications for human migration. Human Ecology Review, 16(1), 93.</mixed-citation>
         </ref>
         <ref id="r56">
            <mixed-citation publication-type="other">Satterthwaite, D. (2001). Reducing urban poverty: Constraints on the effectiveness of aid agencies and development banks and some suggestions for change. Environment and Urbanization, 13(1), 137–157.</mixed-citation>
         </ref>
         <ref id="r57">
            <mixed-citation publication-type="other">Schnaiberg, A. (1980). The Environment: From Surplus to Scarcity. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="r58">
            <mixed-citation publication-type="other">Sen, A. (1999). Development as Freedom. New York: Anchor Books.</mixed-citation>
         </ref>
         <ref id="r59">
            <mixed-citation publication-type="other">Shandra, J. M., London, B., &amp; Williamson, J. B. (2003). Environmental degradation, environmental sustainability, and overurbanization in the developing world: A quantitative, cross-national analysis. Sociological Perspectives, 46(3), 309–329.</mixed-citation>
         </ref>
         <ref id="r60">
            <mixed-citation publication-type="other">Smith, D. A. (1996). Third World Cities in Global Perspective. Boulder: Westview Press.</mixed-citation>
         </ref>
         <ref id="r61">
            <mixed-citation publication-type="other">Smith, D. A., &amp; London, B. (1990). Convergence in world urbanization? A quantitative assessment. Urban Affairs Review, 25(4), 574–590.</mixed-citation>
         </ref>
         <ref id="r62">
            <mixed-citation publication-type="other">Stein, K. (2009). Understanding consumption and environmental change in China: A cross-national comparison of consumer patterns. Human Ecology Review, 16(1), 41.</mixed-citation>
         </ref>
         <ref id="r63">
            <mixed-citation publication-type="other">Steinberger, J. K., Krausmann, F., Getzner, M., Schandl, H., &amp; West, J. (2013). Development and dematerialization: An international study. PLOSone 8(10), 1–11.</mixed-citation>
         </ref>
         <ref id="r64">
            <mixed-citation publication-type="other">Steinberger, J. K., &amp; Roberts, J. T. (2010). From constraint to sufficiency: The decoupling of energy and carbon from human needs, 1975–2005. Ecological Economics, 70(2), 425–433.</mixed-citation>
         </ref>
         <ref id="r65">
            <mixed-citation publication-type="other">Steinberger, J. K., Roberts, J. T., Peters, G. P., &amp; Baiocchi, G. (2012). Pathways of human development and carbon emissions embodied in trade. Nature Climate Change, 2(2), 81–85.</mixed-citation>
         </ref>
         <ref id="r66">
            <mixed-citation publication-type="other">Tacoli, C. (2003). The links between urban and rural development. Environment and Urbanization, 15(1), 3–12.</mixed-citation>
         </ref>
         <ref id="r67">
            <mixed-citation publication-type="other">Timberlake, M., &amp; Kentor, J. (1983). Economic dependence, overurbanization, and economic growth: A study of less developed countries. The Sociological Quarterly, 24(4), 489–507.</mixed-citation>
         </ref>
         <ref id="r68">
            <mixed-citation publication-type="other">Villarreal, A., &amp; Silva, B. F. (2006). Social cohesion, criminal victimization and perceived risk of crime in Brazilian neighborhoods. Social Forces, 84(3), 1725–1753.</mixed-citation>
         </ref>
         <ref id="r69">
            <mixed-citation publication-type="other">Williamson, J. G. (1988). Migrant selectivity, urbanization, and industrial revolutions. Population and Development Review, 287–314.</mixed-citation>
         </ref>
         <ref id="r70">
            <mixed-citation publication-type="other">World Bank, World Development Indicators. databank.worldbank.org/data/home.aspx, accessed July 24 and September 4, 2014.</mixed-citation>
         </ref>
         <ref id="r71">
            <mixed-citation publication-type="other">World Resources Institute's CAIT 2.0 climate data explorer. www.cait2.wri.org, accessed August 2, 2014.</mixed-citation>
         </ref>
         <ref id="r72">
            <mixed-citation publication-type="other">United Nations Human Settlements Program (UN-HABITAT). (2003). Slums of the World: The Face of Urban Poverty in the New Millennium? London: Earthscan. www.unhabitat.org, accessed June 21, 2015.</mixed-citation>
         </ref>
         <ref id="r73">
            <mixed-citation publication-type="other">UN-HABITAT. (2006). State of the World's Cities Report 2006/2007. London: Earthscan. www.unhabitat.org and www.unhabitat.org/mediacentre/documents/sowcr2006/SOWCR%205.pdf, accessed June 21, 2015.</mixed-citation>
         </ref>
         <ref id="r74">
            <mixed-citation publication-type="other">UN-HABITAT. (2012). State of the World's Cities Report 2012/2013: Prosperity of Cities. World Urban Forum Edition. www.sustainabledevelopment.un.org/content/documents/745habitat.pdf, accessed June 21, 2015.</mixed-citation>
         </ref>
         <ref id="r75">
            <mixed-citation publication-type="other">UN-HABITAT. (2015a). Housing &amp; slum upgrading. unhabitat.org/urban-themes/housing-slum-upgrading/, accessed June 21, 2015.</mixed-citation>
         </ref>
         <ref id="r76">
            <mixed-citation publication-type="other">UN-HABITAT. (2015b). UN-Habitat at a glance. www.unhabitat.org/un-habitatat-a-glance/, accessed June 21, 2015.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
