<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">inteorga</journal-id>
         <journal-id journal-id-type="jstor">j100191</journal-id>
         <journal-title-group>
            <journal-title>International Organization</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00208183</issn>
         <issn pub-type="epub">15315088</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3877863</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Note</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Conditioning the Effects of Aid: Cold War Politics, Donor Credibility, and Democracy in Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Thad</given-names>
                  <surname>Dunning</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">58</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i371803</issue-id>
         <fpage>409</fpage>
         <lpage>423</lpage>
         <page-range>409-423</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The IO Foundation</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3877863"/>
         <abstract>
            <p>The effect of foreign aid on regime type in recipient countries remains widely debated. In this research note, I argue that a recent focus on "moral hazard" has distracted attention from another mechanism linking foreign aid to domestic political institutions. During the Cold War, donors' geopolitical objectives diminished the credibility of threats to condition aid on the adoption of democratic reforms. The demise of the Soviet Union and the end of the Cold War, on the other hand, enhanced the effectiveness of Western aid conditionality. I reanalyze an important recent study and demonstrate that the small positive effect of foreign aid on democracy in sub-Saharan African countries between 1975 and 1997 is limited to the post-Cold War period. This new empirical evidence underscores the importance of geopolitical context in conditioning the causal impact of development assistance, and the evidence confirms that the end of the Cold War marked a watershed in the politics of foreign aid in Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1214e159a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1214e166" publication-type="other">
Goldsmith 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e173a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1214e180" publication-type="other">
Bräutigam 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e186" publication-type="other">
Devarajan et al. 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e192" publication-type="other">
Killick 1998</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e199" publication-type="other">
Maren 1997</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e205" publication-type="other">
Moore 1998</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e212a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1214e219" publication-type="other">
Ake 1996</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e225" publication-type="other">
Hook 1998</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e232a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1214e239" publication-type="other">
Abrahamsen 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e245" publication-type="other">
Bräutigam 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e251" publication-type="other">
Devarajan et al. 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e258" publication-type="other">
Easterly
2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e267" publication-type="other">
Hook 1998</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e273" publication-type="other">
Lancaster 1999</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e279" publication-type="other">
Maren 1997</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e285" publication-type="other">
Moore 1998</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e293a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1214e300" publication-type="other">
Goldsmith 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e307a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1214e314" publication-type="other">
Goldsmith's 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e320" publication-type="other">
Diamond 1999, 12</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e327a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1214e334" publication-type="other">
Easterly 2002</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e340" publication-type="other">
note 3</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e347a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1214e354" publication-type="other">
Ake 1996.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e361a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1214e368" publication-type="other">
Ibid., 63-64.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e375a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1214e382" publication-type="other">
Ibid., 64.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e390a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1214e397" publication-type="other">
Goldsmith 2001, 124.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e404a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1214e411" publication-type="other">
World Bank 1999, 286</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e418a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1214e425" publication-type="other">
Goldsmith 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e431" publication-type="other">
&lt;http://www.stata.com/support/faqs/
stat/endogeneity.html&gt;. Accessed 29 December 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e441a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1214e448" publication-type="other">
Goldsmith 2001, 124.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e455a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1214e462" publication-type="other">
Breslauer 1992, 196.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e469a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1214e476" publication-type="other">
Herbst 1990.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e484a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1214e491" publication-type="other">
Ake 1996</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e497" publication-type="other">
note 11</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e504a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1214e511" publication-type="other">
Goldsmith 2001, 131.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e518a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1214e525" publication-type="other">
Ibid., 140.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e532a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1214e539" publication-type="other">
Ibid., 136.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e546a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1214e553" publication-type="other">
Albright 1991, 38-39</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e560a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1214e567" publication-type="other">
World Bank 1999</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e573" publication-type="other">
note 13</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e581a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1214e588" publication-type="other">
Beck and Katz 1995</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e594" publication-type="other">
Goldsmith's 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e601a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1214e608" publication-type="other">
Bratton and Van de Walle 1997, 241.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e615a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1214e622" publication-type="other">
Goldsmith 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e629a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1214e636" publication-type="other">
Ibid., 124.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e643a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1214e650" publication-type="other">
Bratton and Van de Walle 1997, 160</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e656" publication-type="other">
Herbst 1990</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e663a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1214e670" publication-type="other">
Allen 1992</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e676" publication-type="other">
Bratton and Van de Walle 1997</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e684a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1214e691" publication-type="other">
Nwajiaku 1994.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e698a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1214e705" publication-type="other">
Ibid., 434.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e712a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1214e721" publication-type="other">
Ibid., 438</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e727" publication-type="other">
Bratton and Van de Walle
1997, 241</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e737a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1214e744" publication-type="other">
Ross 2001</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e751a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1214e758" publication-type="other">
note 14</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1214e764" publication-type="other">
Bratton and Van de Walle 1997</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e771a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1214e778" publication-type="other">
Easterly 2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d1214e786a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1214e793" publication-type="journal">
James Dao, "In Quietly Courting Africa, U.S. Likes Dowry," New York Times,
19 September 2002, A1<person-group>
                     <string-name>
                        <surname>Dao</surname>
                     </string-name>
                  </person-group>
                  <issue>19 September</issue>
                  <fpage>A1</fpage>
                  <source>New York Times</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1214e834a1310">
            <mixed-citation id="d1214e838" publication-type="book">
Abrahamsen, Rita. 2000. Disciplining Democracy: Development Discourse and Good Governance in
Africa. London: Zed Books.<person-group>
                  <string-name>
                     <surname>Abrahamsen</surname>
                  </string-name>
               </person-group>
               <source>Disciplining Democracy: Development Discourse and Good Governance in Africa</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e863a1310">
            <mixed-citation id="d1214e867" publication-type="book">
Ake, Claude. 1996. Rethinking African Democracy. In The Global Resurgence of Democracy, 2d ed.,
edited by Larry Diamond and Marc F. Plattner, 63-75. Baltimore, Md.: Johns Hopkins University
Press.<person-group>
                  <string-name>
                     <surname>Ake</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Rethinking African Democracy</comment>
               <fpage>63</fpage>
               <source>The Global Resurgence of Democracy</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e902a1310">
            <mixed-citation id="d1214e906" publication-type="journal">
Albright, David E. 1991. Soviet Economic Development and the Third World. Soviet Studies 43
(1):27-59.<object-id pub-id-type="jstor">10.2307/152483</object-id>
               <fpage>27</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1214e922a1310">
            <mixed-citation id="d1214e926" publication-type="journal">
Allen, Chris. 1992. "Goodbye to All That": The Short and Sad Story of Socialism in Benin. Journal of
Communist Studies 8 (2):63-81.<person-group>
                  <string-name>
                     <surname>Allen</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>63</fpage>
               <volume>8</volume>
               <source>Journal of Communist Studies</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e962a1310">
            <mixed-citation id="d1214e966" publication-type="journal">
Beck, Nathaniel, and Jonathan N. Katz. 1995. "What to Do (and Not to Do) with Time-Series Cross-
Sectional Data." American Political Science Review 89 (3):634-47.<object-id pub-id-type="doi">10.2307/2082979</object-id>
               <fpage>634</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1214e982a1310">
            <mixed-citation id="d1214e986" publication-type="book">
Bratton, Michael, and Nicolas van de Walle. 1997. Democratic Experiments in Africa: Regime Tran-
sitions in Comparative Perspective. Cambridge: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Bratton</surname>
                  </string-name>
               </person-group>
               <source>Democratic Experiments in Africa: Regime Transitions in Comparative Perspective</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1011a1310">
            <mixed-citation id="d1214e1015" publication-type="book">
Bräutigam, Deborah. 2000. Aid Dependence and Governance. Stockholm: Almquist and Wiksell
International.<person-group>
                  <string-name>
                     <surname>Bräutigam</surname>
                  </string-name>
               </person-group>
               <source>Aid Dependence and Governance</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1040a1310">
            <mixed-citation id="d1214e1044" publication-type="book">
Breslauer, George W. 1992. Explaining Soviet Policy Changes: Politics, Ideology, and Learning. In
Soviet Policy in Africa: From the Old to the New Thinking, edited by George W. Breslauer, 196-
216. Berkeley: Center for Slavic and East European Studies, University of California, and the
Berkeley-Stanford Program in Soviet Studies.<person-group>
                  <string-name>
                     <surname>Breslauer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Explaining Soviet Policy Changes: Politics, Ideology, and Learning</comment>
               <fpage>196</fpage>
               <source>Soviet Policy in Africa: From the Old to the New Thinking</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1082a1310">
            <mixed-citation id="d1214e1086" publication-type="book">
Devarajan, Shantayanan, David R. Dollar, and Torgny Holmgren, eds. 2001. Aid and Reform in Africa:
Lessons from Ten Case Studies. Washington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Devarajan</surname>
                  </string-name>
               </person-group>
               <source>Aid and Reform in Africa: Lessons from Ten Case Studies</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1111a1310">
            <mixed-citation id="d1214e1115" publication-type="book">
Diamond, Larry. 1999. Developing Democracy: Toward Consolidation. Baltimore, Md.: Johns Hop-
kins University Press.<person-group>
                  <string-name>
                     <surname>Diamond</surname>
                  </string-name>
               </person-group>
               <source>Developing Democracy: Toward Consolidation</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1141a1310">
            <mixed-citation id="d1214e1145" publication-type="book">
Easterly, William. 2001. The Elusive Quest for Growth: Economists' Adventures and Misadventures in
the Tropics. Cambridge, Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Easterly</surname>
                  </string-name>
               </person-group>
               <source>The Elusive Quest for Growth: Economists' Adventures and Misadventures in the Tropics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1170a1310">
            <mixed-citation id="d1214e1174" publication-type="journal">
--- 2002. The Cartel of Good Intentions. Foreign Policy 131 (July/August):40-44.<object-id pub-id-type="doi">10.2307/3183416</object-id>
               <fpage>40</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1214e1187a1310">
            <mixed-citation id="d1214e1191" publication-type="journal">
Goldsmith, Arthur A. 2001. Foreign Aid and Statehood in Africa. International Organization 55
(1):123-48.<object-id pub-id-type="jstor">10.2307/3078599</object-id>
               <fpage>123</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1214e1207a1310">
            <mixed-citation id="d1214e1211" publication-type="journal">
Herbst, Jeffrey. 1990. Third World Communism in Crisis: The Fall of Afro-Marxism. Journal of De-
mocracy 1 (3):92-101.<person-group>
                  <string-name>
                     <surname>Herbst</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>92</fpage>
               <volume>1</volume>
               <source>Journal of Democracy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1246a1310">
            <mixed-citation id="d1214e1250" publication-type="journal">
Hook, Steven W. 1998. "Building Democracy" through Foreign Aid: The Limitations of United States
Political Conditionalities, 1992-1996. Democratization 5 (3): 156-80.<person-group>
                  <string-name>
                     <surname>Hook</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>156</fpage>
               <volume>5</volume>
               <source>Democratization</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1285a1310">
            <mixed-citation id="d1214e1289" publication-type="book">
Killick, Tony, Ramani Gunatilaka, and Ana Marr. 1998. Aid and the Political Economy of Policy Change.
London: Routledge.<person-group>
                  <string-name>
                     <surname>Killick</surname>
                  </string-name>
               </person-group>
               <source>Aid and the Political Economy of Policy Change</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1315a1310">
            <mixed-citation id="d1214e1319" publication-type="book">
Lancaster, Carol. 1999. Aid to Africa: So Much to Do, So Little Done. Chicago: University of Chicago
Press.<person-group>
                  <string-name>
                     <surname>Lancaster</surname>
                  </string-name>
               </person-group>
               <source>Aid to Africa: So Much to Do, So Little Done</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1344a1310">
            <mixed-citation id="d1214e1348" publication-type="book">
Maren, Michael. 1997. Road to Hell: The Ravaging Effects of Foreign Aid and International Charity.
New York: Free Press.<person-group>
                  <string-name>
                     <surname>Maren</surname>
                  </string-name>
               </person-group>
               <source>Road to Hell: The Ravaging Effects of Foreign Aid and International Charity</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1373a1310">
            <mixed-citation id="d1214e1377" publication-type="book">
Moore, Mick. 1998. Death Without Taxes: Democracy, State Capacity, and Aid Dependence in the
Fourth World. In The Democratic Developmental State: Politics and Institutional Design, edited by
Mark Robinson and Gordon White, 84-121. Oxford: Oxford University Press.<person-group>
                  <string-name>
                     <surname>Moore</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Death Without Taxes: Democracy, State Capacity, and Aid Dependence in the Fourth World</comment>
               <fpage>84</fpage>
               <source>The Democratic Developmental State: Politics and Institutional Design</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1412a1310">
            <mixed-citation id="d1214e1416" publication-type="journal">
Nwajiaku, Kathryn. 1994. The National Conferences in Benin and Togo Revisited. Journal of Modern
African Studies 32 (3):429-47.<object-id pub-id-type="jstor">10.2307/161983</object-id>
               <fpage>429</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1214e1432a1310">
            <mixed-citation id="d1214e1436" publication-type="journal">
Ross, Michael. 2001. Does Oil Hinder Democracy? World Politics 53 (3):325-61.<person-group>
                  <string-name>
                     <surname>Ross</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>325</fpage>
               <volume>53</volume>
               <source>World Politics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1214e1468a1310">
            <mixed-citation id="d1214e1472" publication-type="book">
World Bank. 1999. Entering the 21st Century: World Development Report 1999/2000. Washington,
D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Entering the 21st Century: World Development Report 1999/2000</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
