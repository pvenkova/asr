<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7p396</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1x736n</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>South Africa's Gold Mines and the Politics of Silicosis</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>McCULLOCH</surname>
               <given-names>JOCK</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>10</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9781847010599</isbn>
      <isbn content-type="epub">9781782040422</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge Suffolk (GB); Rochester, NY (US); South Africa; Namibia; Lesotho; Swaziland; Botswana</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>Jock McCulloch</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt1x736n"/>
      <abstract abstract-type="short">
         <p>South Africa's gold mines are the largest and historically among the most profitable in the world. Yet at what human cost? This book reveals how the mining industry, abetted by a minority state, hid a pandemic of silicosis for almost a century and allowed miners infected with tuberculosis to spread disease to rural communities in South Africa and to labour-sending states. In the twentieth century, South African mines twice faced a crisis over silicosis, which put its workers at risk of contracting pulmonary tuberculosis, often fatal. The first crisis, 1896-1912, saw the mining industry invest heavily in reducing dust and South Africa became renowned for its mine safety. The second began in 2000 with mounting scientific evidence that the disease rate among miners is more than a hundred times higher than officially acknowledged. The first crisis also focused upon disease among the minority white miners: the current crisis is about black migrant workers, and is subject to major class actions for compensation. Jock McCulloch was a Legislative Research Specialist for the Australian parliament and has taught at various universities. His books include 'Asbestos Blues'. Southern Africa (South Africa, Namibia, Lesotho, Swaziland &amp; Botswana) : Jacana.</p>
      </abstract>
      <counts>
         <page-count count="208"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.3</book-part-id>
                  <title-group>
                     <title>LIST OF PHOTOGRAPHS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.4</book-part-id>
                  <title-group>
                     <title>Preface:</title>
                     <subtitle>South Africa in the Twentieth Century</subtitle>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.5</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McCulloch</surname>
                           <given-names>Jock</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xviii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.6</book-part-id>
                  <title-group>
                     <title>LIST OF ABBREVIATIONS</title>
                  </title-group>
                  <fpage>xx</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.7</book-part-id>
                  <title-group>
                     <title>A BRIEF CHRONOLOGY</title>
                  </title-group>
                  <fpage>xxii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.8</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Gold Mining &amp; Life-Threatening Disease</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>In October 2006 the case of Thembekile Mankayi versus AngloGold Ashanti, a subsidiary of Anglo American, was filed in the Johannesburg High Court. Mr Mankayi had for sixteen years worked as a gold miner at the Vaal Reefs Mine and then been dismissed in 1995 because of ill health. Suffering from silicosis and tuberculosis, for which he was seeking compensation, Mankayi claimed that AngloGold knew or should have known that silica dust causes serious lung disease and that it failed to protect him from the risk of injury. It had long been assumed that the various Mines Acts stretching back</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.9</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Creating a Medical System</title>
                  </title-group>
                  <fpage>14</fpage>
                  <abstract abstract-type="extract">
                     <p>In May 1901 the gold mines, which had been closed during the South African War, reopened. Many of the Cornish miners who had left for Britain at the beginning of the war had never returned and, according to the government mining engineer for the Transvaal, in the interim more than two hundred former rock drillers had died of silicosis. The report created adverse publicity in the British press and questions were asked in the House of Commons. In February 1902 a British parliamentary delegation visited South Africa to investigate the incidence of silicosis and one MP, Gilbert Parker, wrote to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.10</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Compensation</title>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract abstract-type="extract">
                     <p>The provision of fair benefits for men and women injured in the workplace has everywhere depended upon the vigilance of trade unions, the presence of competent medical authorities and governments that maintain some critical distance from industry.¹ On the surface South Africa, which was the first state to compensate for silicosis and tuberculosis as occupational diseases, appears to have met those criteria. Stonemasons and foundry workers in the UK facing similar risks had to wait for the Silicosis Act of 1918, which was narrower in scope and provided awards that were never as generous as those available to white miners</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.11</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>A White Science</title>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract abstract-type="extract">
                     <p>In Southern Africa a range of physicians saw at first hand the impact of silica dust on gold miners. They included mine medical officers who conducted pre-employment, periodic and exit medicals at the Witwatersrand Native Labour Association (WNLA) compound or at individual mines, the interns at the Miners’ Phthisis Medical Bureau (MPMB) who adjudicated compensation claims, and government doctors in the laboursending States of Basutoland, Nyasaland and Mozambique who saw the effects of mining on migrant workers. In addition there were scientists at the South African Institute ofMedical Research (SAIMR) in Johannesburg who conducted research on silicosis. Finally, there were</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.12</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Myth Making &amp; the 1930 Silicosis Conference</title>
                  </title-group>
                  <fpage>74</fpage>
                  <abstract abstract-type="extract">
                     <p>The 1930 Silicosis Conference in Johannesburg was a pivotal moment in the global response to occupational disease. Sponsored by the ILO and the Transvaal Chamber of Mines, it brought together delegates from Canada, the UK, the USA, Australia, Italy, the Netherlands and Germany. Given the slowness of travel in 1930, it was probably the first time that many of those scientists had met. The siting of the Conference was recognition of the achievements of South Africa in terms of research, data collection and state regulation. The data included the world’s largest collection of X-rays of a workforce. As the British</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.13</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Tuberculosis &amp; Tropical Labour</title>
                  </title-group>
                  <fpage>85</fpage>
                  <abstract abstract-type="extract">
                     <p>The 1930 Silicosis Conference had publicised the safety of the mines, but it was insufficient to justify the return of Tropicals. To do so the industry needed proof that the mines would not incubate tuberculosis and spread infection to rural areas. While the Conference was in session, Dr Peter Allan was conducting fieldwork in the Ciskei and the Transkei for the South African Institute of Medical Research (SAIMR) inquiry. The report, published in 1932, was the first of its kind since Turner’s in 1906, and it was to be the last in the public domain until majority rule.</p>
                     <p>The project</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Conflict over the Compensation System</title>
                  </title-group>
                  <fpage>106</fpage>
                  <abstract abstract-type="extract">
                     <p>The opening of the new gold fields in the Free State was the largest single investment in the industry’s history. Between 1946 and 1950 Anglo American poured more than £65,000,000 into the development of thirteen new mines.¹ It was a massive undertaking designed to ensure the corporation’s future and it coincided both with the advent of apartheid, which extended and deepened existing inequalities, and with a period of intense conflict between the white trade unions and industry. That conflict began with the passage of the Silicosis Act No. 47 of 1946 and ended twelve years later with the Pneumoconiosis Act</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.15</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Healing Miners</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract abstract-type="extract">
                     <p>The period from 1954 to 1980 saw the industry faced with two major challenges: a change in the pattern of recruitment and the introduction of treatment for pulmonary tuberculosis. Migrant labour was the life blood of the mines, and the Chamber of Mines had at various points in its history responded successfully to labour shortages. A cure for tuberculosis was something the mines had never faced. Apartheid would eventually prove the perfect setting for resolving that challenge to the industry’s advantage.</p>
                     <p>After World War II the gold mines began to lose South African workers to better-paid jobs in other industries.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.16</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>The Sick Shall Work</title>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract abstract-type="extract">
                     <p>The development of the Free State mines shifted investment and labour away from the Rand. By the early 1980s the new fields were employing two hundred thousand men. Most of the mines were operated by Anglo American, which built the nine-hundred-bed Ernest Oppenheimer Hospital at Welkom as its central facility. The largest and best-equipped hospital of its kind in Southern Africa, Ernest Oppenheimer was to become the site of the most influential medical research on gold miners. A second change during that period was the formation of the National Union ofMineworkers (NUM). Founded in 1982, the NUM was successful in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.17</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Men Without Qualities</title>
                  </title-group>
                  <fpage>155</fpage>
                  <abstract abstract-type="extract">
                     <p>The history of silicosis in South Africa is filled with paradoxes. The Rand mines were the first in the world to invest heavily in dust extraction technologies and instruments, such as the konimeter, to reduce risk. South Africa was the first state to recognise silicosis and tuberculosis as occupational diseases, and the gold mines were the first to use radiography to screen workers. Yet South Africa was unsuccessful in making the mines safe or in providing adequate compensation. The major paradox is between the intensity of public debate about silicosis and the invisibility of the disease burden. In the period</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.18</book-part-id>
                  <title-group>
                     <title>SELECT BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>162</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.19</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>173</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x736n.20</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>180</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
