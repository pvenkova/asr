<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt17kk8q9</book-id>
      <subj-group>
         <subject content-type="call-number">HD82.H4796 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Forecasting</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Energy development</subject>
         <subj-group>
            <subject content-type="lcsh">Forecasting</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Geopolitics</subject>
         <subj-group>
            <subject content-type="lcsh">Forecasting</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Energy, Economic Growth, and Geopolitical Futures</book-title>
         <subtitle>Eight Long-Range Scenarios</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Hillebrand</surname>
               <given-names>Evan</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Closson</surname>
               <given-names>Stacy</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>17</day>
         <month>04</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780262028899</isbn>
      <isbn content-type="epub">9780262328722</isbn>
      <isbn content-type="epub">0262328720</isbn>
      <publisher>
         <publisher-name>The MIT Press</publisher-name>
         <publisher-loc>Cambridge, Massachusetts; London, England</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Massachusetts Institute of Technology</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt17kk8q9"/>
      <abstract abstract-type="short">
         <p>This book presents eight varied scenarios of possible global futures, emphasizing the interconnectedness of three drivers of change: energy prices, economic growth, and geopolitics. Other published global future scenarios focus on only one of these factors, viewing, for example, economic growth as unaffected by energy prices or energy prices in isolation from geopolitical conditions. In this book, Evan Hillebrand and Stacy Closson offer a new approach to scenario construction that acknowledges the codependence of these key drivers and integrates qualitative analysis with a quantitative model. The eight scenarios represent possible combinations of high or low energy prices, strong or weak economic growth, and global harmony or disharmony across three time periods: the 2010s, 2020 to 2040, and 2040 to 2050. The "Regional Mercantilism" scenario, for example, envisions high energy prices, weak economic growth, and global disharmony. To impose numerical consistency across scenarios, Hillebrand and Closson employ the International Futures (IFs) model developed by Barry Hughes. (Interested readers can download this interactive model to alter or build scenarios themselves.) Assessing the probability of each scenario, they conclude that increased U.S. energy supply and the sustainability of the Chinese growth miracle are the most significant drivers over the next forty years.</p>
      </abstract>
      <counts>
         <page-count count="248"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Modeling Future Scenarios</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>How can we think about the future? Will there be more wars or peace? What country or countries will dominate, and which ones will fade in geopolitical significance? Will the unprecedented 60-year surge in world economic growth continue or come to an end? Will shortages of energy or other resources constrain the global future?</p>
                     <p>There are many books and articles that describe global future scenarios for each of these questions. We offer a new approach that considers the interconnectedness of what we consider to be the key drivers of the outlook: energy prices, economic growth, and geopolitics. We set out</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Catching Up to America</title>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>The first scenario is marked by low energy prices, strong economic growth, and global harmony. The OECD countries suffer low growth in 2010–2019, while Asian growth continues to surge. Rapid expansion of oil and gas production, coupled with slow OECD-country growth, results in subdued energy prices. The OECD economies eventually right themselves with a burst of technological innovation, some of which significantly reduces energy consumption and thus allows energy prices to remain low. An entirely new energy market is created globally that is more competitive and includes a variety of new energy sources.</p>
                     <p>By 2025, China has become by</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Global Backtracking</title>
                  </title-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>The second scenario is marked by low energy prices, weak economic growth, and global disharmony. The United States and the European Union falter because their macroeconomic policies never come to grips with unsustainable budget deficits caused by rising transfer payments in the face of declining working-age populations. Recurrent financial crises afflict the OECD countries and wreak havoc on the developing world. China is never able to establish the conditions of secure property rights, impartial rule of law, and transparent governance for modern economic growth.</p>
                     <p>The result is high volatility and low-trend economic growth in the world’s biggest economies, which drives</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Peaceful Power Transition</title>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>The third scenario is marked by high energy prices, strong economic growth, and global harmony. In this scenario the world economy recovers from the stagnation in 2008–2012, and energy prices are pushed up sharply. Countries are assumed to follow policies that enhance property rights, improve the rule of law, and increase trade openness. All these assumptions generate higher GDP growth than would be the case otherwise. China continues to have very high growth but finds that accommodating the existing global regime makes more sense for it economically and politically than challenging the existing order. Since energy prices are higher,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Regional Mercantilism</title>
                  </title-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>The fourth scenario is marked by high energy prices, weak economic growth, and global disharmony. Economic weakness in the United States and the government’s failure to come to grips with its long-term problems results in a gradually diminishing respect for its model of democratic capitalism and a diminishing role for the dollar as a reserve currency. The leading alternative, Chinese-style state capitalism, has its own set of problems, but at least it offers populist leaders a politically attractive rallying point. The push for state capitalism engenders a move away from competition and globalization and weakens the prestige of the multilateral</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>A New Bipolarity</title>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract>
                     <p>In the fifth scenario, the world economy recovers from the quagmire of 2008, which has pushed up energy prices sharply. China continues to have very high growth, which tempts it to assert its newfound power against its neighbors and its great-power rival, the United States. However, the United States employs trade measures and diplomacy that contain China’s power. Thus, there is a new bipolarity in which there is no superpower, and both the United States and China struggle for influence over other nations. It is a tenuous bipolarity, since the emerging powers focus on economic benefits from both camps and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Eco World</title>
                  </title-group>
                  <fpage>115</fpage>
                  <abstract>
                     <p>In the sixth scenario, the world economy continues to struggle, but drastic changes are made in global environmental policies. The United States and the European Union have only weak recoveries from the travails of 2008–2013, and China is unable to transition to a sustainable high-growth path. Energy production of fossil fuels runs into geological and political barriers, and energy prices rise despite weak economic growth. There is a consensus, at least among the OECD countries, that the fossil fuel–driven global economy is environmentally unsustainable and that much higher fuel taxes are the correct way to proceed, even if</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Ambition Fuels Rivalry</title>
                  </title-group>
                  <fpage>133</fpage>
                  <abstract>
                     <p>In the seventh scenario, rapid technological change, particularly in energy and nanotechnology, is assumed to produce strong economic growth in the OECD countries, while improved economic governance propels growth in the non-OECD countries, particularly India. At first, this scenario has much in common with scenario 5, in which the world economy recovers from the derailment of 2008 and China asserts its newfound power against its neighbors and its great-power rival, the United States.</p>
                     <p>Different in this scenario, however, is growing energy abundance in the United States and increasing energy insecurity in China. Strong economic growth and cheap energy cause many</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Natural Disasters Promote Unity</title>
                  </title-group>
                  <fpage>151</fpage>
                  <abstract>
                     <p>In the eighth scenario, a series of catastrophic natural disasters bring disparate countries together in humanitarian relief efforts, rescuing victims of floods, droughts, earthquakes, tsunamis, and fires. These transnational calamities forge a strong international will to address human security above all else. The majority of countries, particularly in the developing world, are unwilling as well as incapable of dealing with the wave of climate catastrophes, and domestic unrest increases.</p>
                     <p>Some of the more able countries take on an adaptive management approach to deal with the crises. The flood of refugees seeking haven in OECD countries increases, placing a burden on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Summary and Conclusions</title>
                  </title-group>
                  <fpage>167</fpage>
                  <abstract>
                     <p>We have presented eight different scenarios covering a broad range of outcomes that we can represent in five graphs. Figure 10.1 shows the range of world GDP growth estimates presented in this book. Scenario 1, Catching Up to America, shows world economic output rising by more than 4 percent a year for 40 years. Scenario 2, Global Backtracking, shows world output rising by only 1.2 percent a year. All the other scenarios lie within these extremes, and the composition of growth—that is, which countries grow faster or slower—varies considerably.</p>
                     <p>World growth could, of course, be faster than shown</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.14</book-part-id>
                  <title-group>
                     <title>Appendix A:</title>
                     <subtitle>Conference and Workshop Participants</subtitle>
                  </title-group>
                  <fpage>193</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.15</book-part-id>
                  <title-group>
                     <title>Appendix B:</title>
                     <subtitle>The International Futures Model</subtitle>
                  </title-group>
                  <fpage>195</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.16</book-part-id>
                  <title-group>
                     <title>Appendix C:</title>
                     <subtitle>The Composition of Regional Aggregations</subtitle>
                  </title-group>
                  <fpage>201</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.17</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>205</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.18</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>207</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt17kk8q9.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>219</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
