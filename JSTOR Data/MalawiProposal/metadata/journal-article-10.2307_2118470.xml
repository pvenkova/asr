<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">quarjecon</journal-id>
         <journal-id journal-id-type="jstor">j100335</journal-id>
         <journal-title-group>
            <journal-title>The Quarterly Journal of Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>MIT Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00335533</issn>
         <issn pub-type="epub">15314650</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2118470</article-id>
         <title-group>
            <article-title>Distributive Politics and Economic Growth</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Alberto</given-names>
                  <surname>Alesina</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Dani</given-names>
                  <surname>Rodrik</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">109</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i337099</issue-id>
         <fpage>465</fpage>
         <lpage>490</lpage>
         <page-range>465-490</page-range>
         <permissions>
            <copyright-statement>Copyright 1994 President and Fellows of Harvard College and the Massachusetts Institute of Technology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2118470"/>
         <abstract>
            <p>We study the relationship between politics and economic growth in a simple model of endogenous growth with distributive conflict among agents endowed with varying capital/labor shares. We establish several results regarding the factor ownership of the median individual and the level of taxation, redistribution, and growth. Policies that maximize growth are optimal only for a government that cares solely about pure "capitalists." The greater the inequality of wealth and income, the higher the rate of taxation, and the lower growth. We present empirical results that show that inequality in land and income ownership is negatively correlated with subsequent economic growth.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d2150e132a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d2150e139" publication-type="journal">
Perotti [1992]  </mixed-citation>
            </p>
         </fn>
         <fn id="d2150e148a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d2150e155" publication-type="journal">
Fischer [1980]  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2150e163" publication-type="journal">
Rodrik [1993]  </mixed-citation>
            </p>
         </fn>
         <fn id="d2150e172a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d2150e179" publication-type="book">
Alesina and Rodrik 1991  </mixed-citation>
            </p>
         </fn>
         <fn id="d2150e188a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d2150e195" publication-type="journal">
Romer [1975]  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2150e203" publication-type="journal">
Roberts [1977]  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2150e211" publication-type="journal">
Meltzer
and Richards [1981]  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2150e223" publication-type="journal">
Mayer [1984]  </mixed-citation>
            </p>
         </fn>
         <fn id="d2150e233a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d2150e240" publication-type="book">
Alesina and Rodrik 1991  </mixed-citation>
            </p>
         </fn>
         <fn id="d2150e249a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d2150e256" publication-type="other">
Fields and Jakubson [1993, pp. 3-4]</mixed-citation>
            </p>
         </fn>
         <fn id="d2150e263a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d2150e270" publication-type="other">
Barro-Wolf [1989]</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2150e276" publication-type="other">
Jain
[1975]</mixed-citation>
            </p>
         </fn>
         <fn id="d2150e286a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d2150e293" publication-type="other">
Barro-Wolf [1989]</mixed-citation>
            </p>
         </fn>
         <fn id="d2150e300a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d2150e307" publication-type="other">
Persson and Tabellini [1991]</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2150e313" publication-type="other">
Fields [1993]</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d2150e329a1310">
            <mixed-citation id="d2150e333" publication-type="book">
Alesina, Alberto, and Dani Rodrik, "Distributive Politics and Economic Growth,"
NBER Working Paper No. 3668, 1991.<person-group>
                  <string-name>
                     <surname>Alesina</surname>
                  </string-name>
               </person-group>
               <source>Distributive Politics and Economic Growth</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e358a1310">
            <mixed-citation id="d2150e362" publication-type="journal">
Barro, Robert, "Government Spending in a Simple Model of Economic Growth,"
Journal of Political Economy, XCVIII (1990), 103-125.<object-id pub-id-type="jstor">10.2307/2937633</object-id>
               <fpage>103</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e378a1310">
            <mixed-citation id="d2150e382" publication-type="journal">
—, Economic Growth in a Cross Section of Countries," Quarterly Journal of
Economics, CVI (May 1991),407-44.<object-id pub-id-type="doi">10.2307/2937943</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e398a1310">
            <mixed-citation id="d2150e402" publication-type="other">
Barro, Robert, and Holger Wolf, "Data Appendix for Economic Growth in a
Cross-Section of Countries," unpublished manuscript, 1989.</mixed-citation>
         </ref>
         <ref id="d2150e413a1310">
            <mixed-citation id="d2150e417" publication-type="other">
Barro, Robert, and Xavier Sala-i-Martin, "Public Finance in the Theory of
Economic Growth," unpublished manuscript, 1990.</mixed-citation>
         </ref>
         <ref id="d2150e427a1310">
            <mixed-citation id="d2150e431" publication-type="other">
Benhabib, Jess, and Aldo Rustichini, "Social Conflict, Growth and Income
Distribution," unpublished manuscript, 1991.</mixed-citation>
         </ref>
         <ref id="d2150e441a1310">
            <mixed-citation id="d2150e445" publication-type="journal">
Bertola, Giuseppe, "Factor Shares, Saving Propensities, and Endogenous Growth,"
American Economic Review, LXXXIII (December 1993), 1184-98.<object-id pub-id-type="jstor">10.2307/2117555</object-id>
               <fpage>1184</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e461a1310">
            <mixed-citation id="d2150e465" publication-type="other">
Clarke, George R. G., "More Evidence on Income Distribution and Growth,"
University of Rochester, unpublished paper, June 1993.</mixed-citation>
         </ref>
         <ref id="d2150e475a1310">
            <mixed-citation id="d2150e479" publication-type="book">
Fernandez, Raquel, and Richard Rogerson, "Human Capital Accumulation and
Income Distribution," NBER Working Paper No. 3994, 1992.<person-group>
                  <string-name>
                     <surname>Fernandez</surname>
                  </string-name>
               </person-group>
               <source>Human Capital Accumulation and Income Distribution</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e504a1310">
            <mixed-citation id="d2150e508" publication-type="book">
Fields, Gary, Poverty, Inequality and Development (Cambridge, MA: Harvard
University Press, 1980).<person-group>
                  <string-name>
                     <surname>Fields</surname>
                  </string-name>
               </person-group>
               <source>Poverty, Inequality and Development</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e534a1310">
            <mixed-citation id="d2150e538" publication-type="other">
—, "A Compendium of Data on Inequality and Poverty for the Developing
World," Cornell University, unpublished manuscript, 1989.</mixed-citation>
         </ref>
         <ref id="d2150e548a1310">
            <mixed-citation id="d2150e552" publication-type="other">
Fields, Gary, and George Jakubson, "New Evidence on the Kuznets Curve," Cornell
University, unpublished manuscript, 1993.</mixed-citation>
         </ref>
         <ref id="d2150e562a1310">
            <mixed-citation id="d2150e566" publication-type="journal">
Fischer, Stanley, "Dynamic Inconsistency, Cooperation and the Benevolent Dissem-
bling Government," Journal of Economic Dynamics and Control, II (1980),
93-107.<person-group>
                  <string-name>
                     <surname>Fischer</surname>
                  </string-name>
               </person-group>
               <fpage>93</fpage>
               <volume>II</volume>
               <source>Journal of Economic Dynamics and Control</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e601a1310">
            <mixed-citation id="d2150e605" publication-type="journal">
Galor, Oded, and Joseph Zeira, "Income Distribution and Macroeconomics,"
Review of Economic Studies, LX (January 1993), 35-52.<object-id pub-id-type="doi">10.2307/2297811</object-id>
               <fpage>35</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e621a1310">
            <mixed-citation id="d2150e625" publication-type="journal">
Heston, Alan, and Robert Summers, "A New Set of International Comparisons of
Real Product and Price Levels: Estimates for 130 Countries," The Review of
Income and Wealth, XXXIV (1988), 1-25.<person-group>
                  <string-name>
                     <surname>Heston</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>XXXIV</volume>
               <source>The Review of Income and Wealth</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e660a1310">
            <mixed-citation id="d2150e664" publication-type="other">
Jain, S., "Size Distribution of Income: A Comparison of Data," The World Bank,
unpublished manuscript, 1975.</mixed-citation>
         </ref>
         <ref id="d2150e675a1310">
            <mixed-citation id="d2150e679" publication-type="book">
Lecallion, Jack, Felix Paukert, Christian Morrison, and Dimitri Gemiolis, "Income
Distribution and Economic Development: Analytical Survey" (Geneva: Interna-
tional Labor Office, 1984).<person-group>
                  <string-name>
                     <surname>Lecallion</surname>
                  </string-name>
               </person-group>
               <source>Income Distribution and Economic Development: Analytical Survey</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e708a1310">
            <mixed-citation id="d2150e712" publication-type="journal">
Kuznets, Simon, "Economic Growth and Income Inequality," American Economic
Review, XLV (1955), 1-28.<object-id pub-id-type="jstor">10.2307/1811581</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e728a1310">
            <mixed-citation id="d2150e732" publication-type="journal">
Lucas, Robert E., Jr., "On the Mechanics of Economic Development," Journal of
Monetary Economics, XXII (1988), 3-42.<person-group>
                  <string-name>
                     <surname>Lucas</surname>
                  </string-name>
               </person-group>
               <fpage>3</fpage>
               <volume>XXII</volume>
               <source>Journal of Monetary Economics</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e764a1310">
            <mixed-citation id="d2150e768" publication-type="journal">
Mayer, Wolfgang, "Endogenous Tariff Formation," American Economic Review,
LXXIV (1984),970-85.<object-id pub-id-type="jstor">10.2307/556</object-id>
               <fpage>970</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e784a1310">
            <mixed-citation id="d2150e788" publication-type="journal">
Meltzer, Allan H., and Scott F. Richard, "A Rational Theory of the Size of the
Government," Journal of Political Economy, LXXXIX (1981), 914-27.<object-id pub-id-type="jstor">10.2307/1830813</object-id>
               <fpage>914</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e804a1310">
            <mixed-citation id="d2150e808" publication-type="journal">
Murphy, Kevin M., Andrei Shleifer, and Robert Vishny, "Income Distribution,
Market Size, and Industrialization," Quarterly Journal of Economics, CIV
(August 1989), 537-64.<object-id pub-id-type="doi">10.2307/2937810</object-id>
               <fpage>537</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e828a1310">
            <mixed-citation id="d2150e832" publication-type="journal">
Perotti, Roberto, "Income Distribution, Politics, and Growth," American Economic
Review Papers and Proceedings, LXXXII (May 1992), 311-16.<object-id pub-id-type="jstor">10.2307/2117420</object-id>
               <fpage>311</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e848a1310">
            <mixed-citation id="d2150e852" publication-type="journal">
—, Political Equilibrium Income Distribution and Growth," Review of Eco-
nomic Studies, LX (October 1993), 755-76.<object-id pub-id-type="doi">10.2307/2298098</object-id>
               <fpage>755</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e868a1310">
            <mixed-citation id="d2150e872" publication-type="other">
Persson, Torsten, and Guido Tabellini, "Is Inequality Harmful for Growth? Theory
and Evidence," unpublished paper, May 1991.</mixed-citation>
         </ref>
         <ref id="d2150e882a1310">
            <mixed-citation id="d2150e886" publication-type="book">
Ranis, Gustav, "Contrasts in the Political Economy of Development Policy Change,"
in Gary Gereffi and Donald L. Wyman, eds., Manufacturing Miracles: Paths of
Industrialization in Latin America and East Asia (Princeton, NJ: Princeton
University Press, 1990).<person-group>
                  <string-name>
                     <surname>Ranis</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Contrasts in the Political Economy of Development Policy Change</comment>
               <source>Manufacturing Miracles: Paths of Industrialization in Latin America and East Asia</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e921a1310">
            <mixed-citation id="d2150e925" publication-type="journal">
Roberts, Kevin W. S., "Voting over Income Tax Schedules," Journal of Public
Economics, VIII (1977), 329-40.<person-group>
                  <string-name>
                     <surname>Roberts</surname>
                  </string-name>
               </person-group>
               <fpage>329</fpage>
               <volume>VIII</volume>
               <source>Journal of Public Economics</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e957a1310">
            <mixed-citation id="d2150e961" publication-type="journal">
Rodrik, Dani, "Redistributive Taxation without Excess Burden," Economics &amp;
Politics, V (March 1993), 53-60.<person-group>
                  <string-name>
                     <surname>Rodrik</surname>
                  </string-name>
               </person-group>
               <issue>March</issue>
               <fpage>53</fpage>
               <volume>V</volume>
               <source>Economics &amp; Politics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e997a1310">
            <mixed-citation id="d2150e1001" publication-type="journal">
Romer, Paul M., "Increasing Returns and Long-Run Growth," Journal of Political
Economy, XCIV (1986), 1002-37.<object-id pub-id-type="jstor">10.2307/1833190</object-id>
               <fpage>1002</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e1017a1310">
            <mixed-citation id="d2150e1021" publication-type="journal">
Romer, Thomas, "Individual Welfare, Majority Voting, and the Properties of a
Linear Income Tax," Journal of Public Economics, XIV (1975), 163-85.<person-group>
                  <string-name>
                     <surname>Romer</surname>
                  </string-name>
               </person-group>
               <fpage>163</fpage>
               <volume>XIV</volume>
               <source>Journal of Public Economics</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e1053a1310">
            <mixed-citation id="d2150e1057" publication-type="book">
Saint-Paul, Gilles, and Thierry Verdier, "Education, Democracy and Growth,"
CEPR Discussion Paper No. 613, February 1992.<person-group>
                  <string-name>
                     <surname>Saint-Paul</surname>
                  </string-name>
               </person-group>
               <source>Education, Democracy and Growth</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e1082a1310">
            <mixed-citation id="d2150e1086" publication-type="journal">
Stolper, Wolfgang, and Paul Samuelson, "Protection and Real Wages," Review of
Economic Studies, IX (1941), 58-73.<object-id pub-id-type="doi">10.2307/2967638</object-id>
               <fpage>58</fpage>
            </mixed-citation>
         </ref>
         <ref id="d2150e1102a1310">
            <mixed-citation id="d2150e1106" publication-type="book">
Taylor, C. L., and M. C. Hudson, World Handbook of Political and Social Indicators,
2nd. ed. (New Haven, CT: Yale University Press, 1972).<person-group>
                  <string-name>
                     <surname>Taylor</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>World Handbook of Political and Social Indicators</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e1135a1310">
            <mixed-citation id="d2150e1139" publication-type="book">
Wade, Robert, Governing the Market: Economic Theory and the Role of Government
in East Asian Industrialization (Princeton, NJ: Princeton University Press,
1990).<person-group>
                  <string-name>
                     <surname>Wade</surname>
                  </string-name>
               </person-group>
               <source>Governing the Market: Economic Theory and the Role of Government in East Asian Industrialization</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d2150e1169a1310">
            <mixed-citation id="d2150e1173" publication-type="book">
World Bank, World Development Report, 1990 (Washington, DC: The World Bank,
1990).<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report, 1990</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
