<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7p396</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt18gzfj6</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>Anthropology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Volunteer Economies</book-title>
         <subtitle>The Politics and Ethics of Voluntary Labour in Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Prince</surname>
               <given-names>Ruth</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Brown</surname>
               <given-names>Hannah</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>19</day>
         <month>05</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781847011404</isbn>
      <isbn content-type="epub">9781782046691</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk, GB; Rochester, NY, USA</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Contributors</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt18gzfj6"/>
      <abstract abstract-type="short">
         <p>Across Africa today, as development activities animate novel forms of governance, new social actors are emerging, among them the volunteer. Yet, where work and resources are limited, volunteer practices have repercussions that raise contentious ethical issues. What has been the real impact of volunteers economically, politically and in society? The interdisciplinary experts in this collection examine the practices of volunteers - both international and local - and ideologies of volunteerism. They show the significance of volunteerism to processes of social and economic transformation, and political projects of national development and citizenship, as well as to individual aspirations in African societies. These case studies - from South Africa, Lesotho, Kenya, Tanzania, Zambia, Sierra Leone and Malawi - examine everyday experiences of volunteerism and trajectories of voluntary work, trace its broader historical, political and economic implications, and situate African experiences of voluntary labour within global exchanges and networks of resources, ideas and political technologies. Offering insights into changing configurations of work, citizenship, development and social mobility, the authors offer new perspectives on the relations between labour, identity and social value in Africa. Ruth Prince is Associate Professor in Medical Anthropology at the University of Oslo; with her co-author Wenzel Geissler, she won the 2010 Amaury Talbot Prize for their book The Land is Dying: Contingency, Creativity and Conflict in Western Kenya. Hannah Brown is a lecturer in Anthropology at Durham University.</p>
      </abstract>
      <counts>
         <page-count count="254"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.3</book-part-id>
                  <title-group>
                     <title>NOTES ON CONTRIBUTORS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGEMENTS</title>
                  </title-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.5</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                     <subtitle>The Politics &amp; Ethics of Voluntary Labour in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PRINCE</surname>
                           <given-names>RUTH</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>BROWN</surname>
                           <given-names>HANNAH</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In recent years the concept of ‘volunteering’ and the figure of the volunteer have become increasingly prominent in political ideologies, social movements and individual experience across the globe (Allahyari 2000; Eliasoph 2011; Hilton &amp; McKay 2011; Milligan &amp; Conradson 2006). The ethic of volunteering drives the work of charities, philanthropic groups, religious organizations and non-government organizations (NGOs) concerned with global inequalities, poverty alleviation, development, disaster response and emergency relief. Not only does volunteerism underpin an organizational ethic, heard for example in the term ‘voluntary sector’, but many of these organizations themselves also rely upon voluntary labour. Volunteer medical professionals are central to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Many Uses of Moral Magnetism</title>
                     <subtitle>Volunteer Caregiving &amp; the HIV/AIDS Epidemic in South Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>COLVIN</surname>
                           <given-names>CHRISTOPHER J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>I first met Nolwazi Skweyiya several years ago during an evaluation of government efforts in South Africa’s Western Cape Province to support the community-based response to the HIV/AIDS epidemic. The provincial government had money from the Global Fund to Fight AIDS, Tuberculosis and Malaria to provide seed funding for projects run by small community-based organizations (CBOs) and larger NGOs. These projects were supposed to mobilize volunteer support within communities to aid those infected and affected by HIV.</p>
                     <p>When we first met, Nolwazi was managing a relatively large volunteer operation from her two-roomed shack in a township on the edge of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Civics of Urban Malaria Vector Control</title>
                     <subtitle>Grassroots &amp; Breeding Places in Dar es Salaam</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KELLY</surname>
                           <given-names>ANN</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>CHAKI</surname>
                           <given-names>PROSPER</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>Architect of post-war British social policy, Richard Titmuss understood state capacity as underwritten by volunteer action. Volunteers actualize imagined communities by nurturing the common good: social-welfare systems, Titmuss argued, should not be judged by what is given to citizens but rather by the opportunities presented for citizens to give to each other.¹ During times of crisis, volunteers act as stopgap and scaffold for society, linking dispersed populations to public goods and it is this prosthetic potential that animates the Health Service Report Titmuss prepared for a newly independent Tanganyika. Drawing from detailed surveys of staff numbers, hospital capacities, epidemiological trends</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Purchase of Volunteerism</title>
                     <subtitle>Uses &amp; Meanings of Money in Lesothoʹs Development Sector</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WIG</surname>
                           <given-names>STÅLE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>75</fpage>
                  <abstract>
                     <p>Can there be such a thing as a paid volunteer? The idea of exchanging money for work seems to be at odds with how many naturally think about volunteering. To volunteer is to offer something for free, isn’t it? Yet, the position from which a paid volunteer appears as a paradox is itself, I will argue, all but natural. In reality, the lines between voluntary and paid labour can be confusingly blurred and – for the purpose of this chapter – an interesting empirical point of focus. The question of whether one can purchase a volunteer goes to the contested</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Positions &amp; Possibilities in Volunteering for Transnational Medical Research in Lusaka</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BRUUN</surname>
                           <given-names>BIRGITTE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>Upon completion of a transnational medical research project in Lusaka in 2008, outreach workers received a certificate of participation that stated: ‘Your contributions help pave the way for women in your community and all over the world to one day have a safe and effective way of preventing HIV transmission.’²</p>
                     <p>The text invoked a link between a particular kind of contribution and notions of solidarity, if not altruism, that embraced women in one’s own community and also women far beyond it.³ Voluntaristic contributions to the progress of health based on solidarity and altruism are at the very core of medical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Doing Good While They Can</title>
                     <subtitle>International Volunteers, Development &amp; Politics in Early Independence Tanzania</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>JENNINGS</surname>
                           <given-names>MICHAEL</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>As the winds of change moved across Sub-Saharan Africa in the late 1950s and early 1960s, leading to the first wave of independence from former colonial masters, the breeze blew in a new wave of Europeans and (for the first time in substantial numbers) North Americans. These were not missionaries, nor were they administrative officials (although both of these did continue to follow the migratory patterns established from the mid-nineteenth century that increased engagement of Europe with Sub-Saharan Africa). They were a newly emerging set of actors, ones that would become an increasingly important part of the non-African presence in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Hosting Gazes</title>
                     <subtitle>Clinical Volunteer Tourism &amp; Hospital Hospitality in Tanzania</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SULLIVAN</surname>
                           <given-names>NOELLE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>140</fpage>
                  <abstract>
                     <p>On a Tuesday morning in July 2011, at Kiunga District Hospital (KDH)² in northern Tanzania, two doctors sat in the congested anteroom of the facility’s humble major operating theatre, discussing the file of a preoperative patient. Three Tanzanian nurses busied themselves between the anteroom and the operating theatre, cleaning surfaces or checking equipment in preparation for surgery. In one corner of the room, three<italic>wazungu</italic>³ volunteers in scrubs sat on a broken cot, sharing cell phone images with each other, lingering in hopes of observing a procedure. I was in another corner, compiling a list of medical equipment lacking in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Beneath the Spin</title>
                     <subtitle>Moral Complexity &amp; Rhetorical Simplicity in ʹGlobal Healthʹ Volunteering</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WENDLAND</surname>
                           <given-names>CLAIRE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>ERIKSON</surname>
                           <given-names>SUSAN L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>SULLIVAN</surname>
                           <given-names>NOELLE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>164</fpage>
                  <abstract>
                     <p>Two smiling young white men in sunglasses, proudly displaying tee-shirts with the logo of their medical school, beamed from the cover photo of the alumni magazine. Behind them a wooden sign beginning with an all-capitals ‘CONGRATULATIONS’ announced that at 5,895 metres this was the highest point in Tanzania, Kilimanjaro’s Uhuru Peak. The cover story’s title: ‘STUDENTS TAKE THE COLLEGE OF HUMAN MEDICINE TO NEW HEIGHTS’.</p>
                     <p>As the story made clear, those ‘new heights’ represented the many volunteer opportunities taken up by medical students. The two men featured on the cover were the central illustration. Having just finished their first year</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>A Third Mode of Engagement with the Excluded Other</title>
                     <subtitle>Student Volunteers from an Elite Boarding School in Kenya</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HOLTE</surname>
                           <given-names>BJØRN HALLSTEIN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>185</fpage>
                  <abstract>
                     <p>This chapter presents an ethnographic account² of students from an elite school in Kenya who volunteer at a Bible Club for street children and children from poor families. Volunteering is often cast as requiring the construction of a different other and for the students who feature in this chapter, volunteering at the Bible Club is precisely an encounter with the excluded other of their school, ‘the people outside the gates’. The ethnographic account and analysis in this chapter concerns how volunteering as a mode of engagement with the excluded other feeds into the formation of the students who volunteer as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Undoing Apartheid Legacies?</title>
                     <subtitle>Volunteering as Repentance &amp; Politics by Other Means</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KIRSCH</surname>
                           <given-names>THOMAS G.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>201</fpage>
                  <abstract>
                     <p>For political commentators, South Africa’s general elections in May 2014 were a watershed event in this country’s more recent past because, about twenty years after the introduction of a non-racial democracy, members of the first generation born after the end of apartheid were among those voting. Mass media speculated about how South African political culture might be transformed under the growing influence of the so-called ‘born-free generation’ which has no personal recollections of the apartheid era and which grew up at a time when neo-liberal policies were gaining momentum in South Africa. This generation can be said to be ‘born</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.15</book-part-id>
                  <title-group>
                     <title>Epilogue</title>
                     <subtitle>Ebola &amp; the Vulnerable Volunteer</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>REDFIELD</surname>
                           <given-names>PETER</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>222</fpage>
                  <abstract>
                     <p>The contemporary world order leans heavily on volunteers. If often overlooked in sweeping discussions of capital flows and geopolitics, actual relations between people and places involve both material and imagined forms of voluntary action. Indeed, upon inspection, the figure of the volunteer appears increasingly vital to global governance and exchange. As this volume demonstrates, voluntary labour and associated sentiment play a particularly prominent role in Sub-Saharan Africa. Both national and international efforts to foster public health and economic development have repeatedly sought to inspire altruism and a sense of social solidarity as a means to mobilization. In the aftermath of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.16</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>234</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>263</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18gzfj6.18</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>271</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
