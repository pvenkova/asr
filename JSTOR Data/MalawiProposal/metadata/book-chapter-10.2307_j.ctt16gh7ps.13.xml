<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt16gh7ps</book-id>
      <subj-group>
         <subject content-type="call-number">DT3.A23 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Anthropology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Africa, Fourth Edition</book-title>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">EDITED BY</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>GROSZ-NGATÉ</surname>
               <given-names>MARIA</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>HANSON</surname>
               <given-names>JOHN H.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>O’MEARA</surname>
               <given-names>PATRICK</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>04</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780253012920</isbn>
      <isbn content-type="epub">9780253013026</isbn>
      <isbn content-type="epub">025301302X</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington; Indianapolis</publisher-loc>
      </publisher>
      <edition>4</edition>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Indiana University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt16gh7ps"/>
      <abstract abstract-type="short">
         <p>Since the publication of the first edition in 1977, Africa has established itself as a leading resource for teaching, business, and scholarship. This fourth edition has been completely revised and focuses on the dynamism and diversity of contemporary Africa. The volume emphasizes contemporary culture-civil and social issues, art, religion, and the political scene-and provides an overview of significant themes that bear on Africa's place in the world. Historically grounded, Africa provides a comprehensive view of the ways that African women and men have constructed their lives and engaged in collective activities at the local, national, and global levels.</p>
      </abstract>
      <counts>
         <page-count count="376"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <collab>The Editors</collab>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Africa has moved dynamically into the twenty-first century. It has more mobile phone users than the United States, for example, and cables placed along its Atlantic and Indian Ocean coasts recently have expanded broadband internet access. Africa still has some of the poorest countries in the world, but it also has six of the world’s ten fastest-growing economies of the past decade. Africans increasingly are city dwellers: nearly 40 percent of Africans live in urban areas now, and projections suggest that figure will increase to 50 percent by 2030. Occasional famines still claim lives, but overall rates of African infant</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Africa:</title>
                     <subtitle>A Geographic Frame</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Delehanty</surname>
                           <given-names>James</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>Africa is a continent, the second-largest after Asia. It contains fifty-four countries, several of them vast. Each of Africa’s biggest countries—Algeria, Congo, and Sudan—is about three times the size of Texas, four times that of France. Africa could hold 14 Greenlands, 20 Alaskas, 71 Californias, or 125 Britains. Newcomers to the study of Africa often are surprised by the simple matter of the continent’s great size. No wonder so much else about Africa is vague to outsiders.</p>
                     <p>This chapter introduces Africa from the perspective of geography, an integrative discipline rooted in the ancient need to describe the qualities</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Legacies of the Past:</title>
                     <subtitle>Themes in African History</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Aden</surname>
                           <given-names>John Akare</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hanson</surname>
                           <given-names>John H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>32</fpage>
                  <abstract>
                     <p>Africa and its peoples have a long and distinguished history. The earliest evidence for humankind is found on the continent, and some of the first successful efforts to domesticate plants and produce metals involved African pioneers and innovators. Africans constructed complex societies, some with elaborate political hierarchies and others with dynamic governance systems without titular authorities such as kings and queens. Extensive commercial networks connected local producers in diverse environmental niches with regional markets, and these networks in turn were connected to transcontinental trade networks funneling goods to Asia, Europe, and the Americas. The trans-Atlantic slave trade did not bring</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Social Relations:</title>
                     <subtitle>Family, Kinship, and Community</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Grosz-Ngaté</surname>
                           <given-names>Maria</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>56</fpage>
                  <abstract>
                     <p>News accounts of violent conflict in Africa frequently make reference to “tribe” and “tribalism” as potent ingredients of discord. The use of “tribe” in the African context is a legacy of colonialism and the research of early anthropologists. Anthropologists wanted to know how African societies without centralized leadership maintained order and stability, while colonial officials demarcated African societies for the purpose of rule, ignoring complexities, interactions between groups, and the fluidity of boundaries. The persis tent characterization of African populations as “tribes” gives the appearance of timelessness and glosses over the different forms of political organization that existed in the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Making a Living:</title>
                     <subtitle>African Livelihoods</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Clark</surname>
                           <given-names>Gracia</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wiley</surname>
                           <given-names>Katherine</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>83</fpage>
                  <abstract>
                     <p>Impressive tenacity and ingenuity enable Africans to survive and even prosper under extremely challenging circumstances. The widespread stereo type of the passive victim crumbles away in the face of Africans’ incessant efforts to protect their families’ interests and ensure security and progress for the next generation. It is a struggle that some people shirk and that many do not win. Even so, people’s agency must be taken seriously. Continuous experimentation and innovation are among the legacies of African societies in every part of the continent, as people make their living often under severe resource constraints and despite external shocks such</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Religions in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hanson</surname>
                           <given-names>John H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>103</fpage>
                  <abstract>
                     <p>Spirit possessions, harvest festivals, and other activities associated with African traditional religions (or religions with African roots) remain vital, but attendance at Christian churches and Muslim mosques in Africa has increased significantly during the last century. From 1900 to 2010 the number of Christians in Africa grew from less than 10 million to 470 million, more than 20 percent of the world Christian community. The number of Muslims in Africa also grew to more than 450 million, over 25 percent of the global Muslim community. This chapter discusses the endurance of religions with African roots and how Africans have accepted,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Urban Africa:</title>
                     <subtitle>Lives and Projects</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hansen</surname>
                           <given-names>Karen Tranberg</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>123</fpage>
                  <abstract>
                     <p>In Africa and everywhere else, cities are where the action is. Cities are gateways to the global world, the prime sites for globalization’s translation into local understandings and experiences. This urban global exposure demands that scholars of urban life in Africa pay attention to people’s engagements with a diverse sweep of processes that range from the economic to the cultural. As they manifest them selves in distinct urban locations, such global exposures resonate in complicated ways with local cultural norms and practices. Focusing on a selection of themes that arise from these processes, this chapter is concerned with spatial transformations</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Health, Illness, and Healing in African Societies</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Luedke</surname>
                           <given-names>Tracy J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>140</fpage>
                  <abstract>
                     <p>In African societies, as elsewhere in the world, health and illness are experienced both at the level of the individual body and at the level of the social body. Individual suffering often reveals social structures and tensions, for example when a child’s illness strains family relationships or when a treatable disease proves fatal among the poorer members of a society; healing practices may also create new kinds of community, as when a doctor and patient form a lasting bond or when the pursuit of health care spawns a social movement. The experiences associated with health, illness, and healing always reflect</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Visual Arts in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McNaughton</surname>
                           <given-names>Patrick</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pelrine</surname>
                           <given-names>Diane</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>African art has been made for many thousands of years, undergoing numerous major and often dramatic changes through the centuries and right up until today. Its forms and materials, meanings and functions have always been tremendously varied, deeply imaginative, and dynamically part of people’s individual and social lives. Frequently stunning and formally sophisticated, it has been collected by Westerners for at least half a millennium and in fact profoundly influenced the modern history of European art.</p>
                     <p>The study of African art has changed drastically over time. For centuries Europeans viewed it as the exotic production of strange societies, which did</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>African Music Flows</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Reed</surname>
                           <given-names>Daniel B.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stone</surname>
                           <given-names>Ruth M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>A man walked down the street in the busy Adjame marketplace in Abidjan, Ivory Coast, in West Africa. Amid the sounds of the street—honking horns, ringing cell phones, goat cries, people’s’ voices—he heard the latest hit song by reggae singer Tiken Jah Fakoly drifting toward him from a CD seller’s stall in the market. The song began with a distinctive slide guitar line, which was a sample from a 1990 recording by Geoffrey Oryema of Uganda in eastern Africa. Anchored by the repeating guitar line, the song developed as a twenty-one-string harp lute, the kora, entered along with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Literature in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Julien</surname>
                           <given-names>Eileen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>209</fpage>
                  <abstract>
                     <p>When most Americans and Europeans use the expression “African literature,” they are referring to the poetry, plays, and novels written by Africans that reach Western and Northern shores. These have typically been written in English, French, and, increasingly, Portuguese. If one takes the long or broad view, however, these contemporary works of international standing are but one segment of a vast array of word arts in Africa, which have a long, complex, and varied history.</p>
                     <p>We have no record of the earliest oral traditions, but we know that verbal arts in Africa, oral and written, are ancient and long preceded</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>African Film</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Adesokan</surname>
                           <given-names>Akin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>It is a truism of African cinema that one cannot productively discuss the films that make up the field without keeping in mind the social and economic conditions under which they are made. Fifty years after the first feature film to be written, produced, and directed by an African, and with this cinematic tradition becoming as globally important an art form as African literature and the Afro pop component of world music, economic, political, and cultural factors continue to be central to its full understanding. It is therefore not surprising that, across three generations, issues of political and cultural identity</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>African Politics and the Future of Democracy</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sawyer</surname>
                           <given-names>Amos</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>MacLean</surname>
                           <given-names>Lauren M.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Holmes</surname>
                           <given-names>Carolyn E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>250</fpage>
                  <abstract>
                     <p>African political systems have a long history that substantially predates the arrival of Europeans in the 1400s or the political boundaries of nation-states found on any current map. The peoples of Africa have organized many different types of political systems and witnessed tremendous political changes over time. And yet one of the most enduring puzzles has been whether African political systems will grow into stable democracies. During the late 1950s and early 1960s, when the majority of African countries achieved in dependence from colonial rule, many analysts were hopeful about the propects for expanding citizenship in newly independent regimes. Debates</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Development in Africa:</title>
                     <subtitle>Tempered Hope</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Muhula</surname>
                           <given-names>Raymond</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ndegwa</surname>
                           <given-names>Stephen N.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>275</fpage>
                  <abstract>
                     <p>Sub-Saharan Africa, home to more than eight hundred million people in more than fifty countries, is the least-developed continent in the world. It continues to have relatively low levels of industrialization and urbanization, and instead subsists on narrow economic bases, overly dependent on primary commodities and foreign aid. Livelihoods and life chances on the continent are often among the most challenged in the world, with low life expectancy (especially with the impact of HIV/AIDS), literacy rates, and access to health care and education. Moreover, governance institutions are weak, as evidenced by the fragility of democracies emerging after three de cades</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Human Rights in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Manuh</surname>
                           <given-names>Takyiwaa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>293</fpage>
                  <abstract>
                     <p>Human rights norms are critical mea sures of human existence and development in the contemporary period. Within the community of nations, they have become the third institutional pillar of the United Nations since the setting up of the UN Human Rights Council in 2006. This is a fairly recent development in the long span of historical time, but there is little contention about the salience of human rights to the full enjoyment of life, dignity, and development. Although disagreements exist concerning different conceptions and expressions of human rights, no one seriously questions that they are necessary. It has been argued,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.19</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Print and Electronic Resources</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Frank-Wilson</surname>
                           <given-names>Marion</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>315</fpage>
                  <abstract>
                     <p>“Digital technologies, in reshaping the information landscape, also have altered the relationship between recorded knowledge and the activities of research and teaching.” This statement by Dan Hazen points to several developments that have shaped the way we conduct research and that are worth keeping in mind before embarking on research in African studies. Electronic information is widely available. Libraries subscribe to vast databases, which provide access to journal literature; Google continues to digitize books and to make many of them available on the web; initiatives such as Hathi Trust make the full text of out-of-copyright books available; libraries digitize many</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.20</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>349</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gh7ps.21</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>353</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
