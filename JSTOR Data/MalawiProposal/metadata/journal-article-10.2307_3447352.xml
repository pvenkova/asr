<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">highereducation</journal-id>
         <journal-id journal-id-type="jstor">j100171</journal-id>
         <journal-title-group>
            <journal-title>Higher Education</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00181560</issn>
         <issn pub-type="epub">1573174X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3447352</article-id>
         <title-group>
            <article-title>Student Loans and Their Alternatives: Improving the Performance of Deferred Payment Programs</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Douglas</given-names>
                  <surname>Albrecht</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Adrian</given-names>
                  <surname>Ziderman</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">23</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i368175</issue-id>
         <fpage>357</fpage>
         <lpage>374</lpage>
         <page-range>357-374</page-range>
         <permissions>
            <copyright-statement>Copyright 1992 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3447352"/>
         <abstract>
            <p>The opportunities for increasing student contributions to the costs of higher education are many. Student loans have received much attention both in literature and in practice. While they have not always worked well, we have argued that suitably reformed, they can constitute a productive, though limited, mechanism for cost recovery. In certain countries, however, other mechanisms may be more appropriate. Indeed, the policy maker is presented with a wide menu of policy choices, though some creativity may be required in their application to particular local settings. Currently, loan programs exist in over 50 developing and industrial countries, and have been introduced most commonly to assist students to pay their living expenses. In order to improve financial effectiveness, programs should be targeted toward the most needy and able students. Hidden subsidies should be limited by charging positive real interest rates, combined with repayment plans that take account of the likely pattern of graduate earnings. Default reductions require that loan programs be managed by institutions with the capacity and financial incentives to collect - namely banks, private collection agencies, or taxation departments. Such reforms offer great potential to transform small programs into relatively efficient forms of student support. Larger programs, however, may be more difficult to manage. Some countries have considered alternatives which preserve the basic concept of paying for education from future income. The most notable is a graduate tax in which a student pays a fixed percentage of income over the entire working life, regardless of how much is repaid. Another option is national service which require students to perform socially productive work in exchange for part or all of education costs. In the presence of an effective tax system, a graduate tax could bring in significantly more revenue than traditional loan programs. Besides improved financial efficiency, income contingent payments may be more equitable since they limit the risk to poorer students. In countries with weak taxation systems, this option may not be feasible.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d722e178a1310">
            <label>2</label>
            <mixed-citation id="d722e185" publication-type="book">
Friedman 1962  </mixed-citation>
            <mixed-citation id="d722e193" publication-type="book">
Blaug 1973  </mixed-citation>
            <mixed-citation id="d722e201" publication-type="book">
Barnes and Barr 1988  </mixed-citation>
         </ref>
         <ref id="d722e210a1310">
            <label>3</label>
            <mixed-citation id="d722e217" publication-type="book">
Brittain (1972)  </mixed-citation>
            <mixed-citation id="d722e225" publication-type="partial">
Whalley and Ziderman (1991)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d722e241a1310">
            <mixed-citation id="d722e245" publication-type="book">
Albrecht, D. and Ziderman, A. (1991). Deferred Cost Recovery for Higher Education: Student Loan
Programs in Developing Countries. World Bank Discussion Paper, No. 137. Washington, D.C.: The
World Bank<person-group>
                  <string-name>
                     <surname>Albrecht</surname>
                  </string-name>
               </person-group>
               <source>Deferred Cost Recovery for Higher Education: Student Loan Programs in Developing Countries</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d722e274a1310">
            <mixed-citation id="d722e278" publication-type="book">
Barnes, J. and Barr, N. (1988). Strategies for Higher Education: The Alternative White Paper.
Edinburgh: Aberdeen University Press<person-group>
                  <string-name>
                     <surname>Barnes</surname>
                  </string-name>
               </person-group>
               <source>Strategies for Higher Education: The Alternative White Paper</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d722e303a1310">
            <mixed-citation id="d722e307" publication-type="book">
Blaug, M. (1973). An Introduction to the Economics of Education. London: Penguin<person-group>
                  <string-name>
                     <surname>Blaug</surname>
                  </string-name>
               </person-group>
               <source>An Introduction to the Economics of Education</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d722e329a1310">
            <mixed-citation id="d722e333" publication-type="book">
Brittain, J.A. (1972). The Payroll Tax for Social Security. Washington, D.C.: Brookings Institution<person-group>
                  <string-name>
                     <surname>Brittain</surname>
                  </string-name>
               </person-group>
               <source>The Payroll Tax for Social Security</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d722e356a1310">
            <mixed-citation id="d722e360" publication-type="journal">
Colclough, C. (1990). Raising additional resources for education in developing countries: are graduate
payroll taxes preferable to student loans? International Journal of Educational Development10, 2/3,
169-80<person-group>
                  <string-name>
                     <surname>Colclough</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>169</fpage>
               <volume>10</volume>
               <source>International Journal of Educational Development</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d722e398a1310">
            <mixed-citation id="d722e402" publication-type="book">
Friedman, M. (1962). Capitalism and Freedom. Chicago University of Chicago Press<person-group>
                  <string-name>
                     <surname>Friedman</surname>
                  </string-name>
               </person-group>
               <source>Capitalism and Freedom</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d722e424a1310">
            <mixed-citation id="d722e428" publication-type="journal">
Hope, J. and Miller, P. (1988). Financing tertiary education: an examination of the issues. Australian
Economic Review4<person-group>
                  <string-name>
                     <surname>Hope</surname>
                  </string-name>
               </person-group>
               <volume>4</volume>
               <source>Australian Economic Review</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d722e457a1310">
            <mixed-citation id="d722e461" publication-type="book">
International Academy of Education. (1990). Rethinking the Finance of Post-Compulsory Education.
(Processed)<person-group>
                  <string-name>
                     <surname>International Academy of Education</surname>
                  </string-name>
               </person-group>
               <source>Rethinking the Finance of Post-Compulsory Education</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d722e486a1310">
            <mixed-citation id="d722e490" publication-type="book">
Jimenez, E. (1987). Pricing Policy in the Social Sectors: Cost Recovery for Education and Health in
Developing Countries. Baltimore: Johns Hopkins University Press<person-group>
                  <string-name>
                     <surname>Jimenez</surname>
                  </string-name>
               </person-group>
               <source>Pricing Policy in the Social Sectors: Cost Recovery for Education and Health in Developing Countries</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d722e515a1310">
            <mixed-citation id="d722e519" publication-type="journal">
McMahon, W. (1988). Potential resource recovery in higher education in the developing countries and
the parents' expected contribution. Economics of Education Review1, 1, 135-152<person-group>
                  <string-name>
                     <surname>McMahon</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>135</fpage>
               <volume>1</volume>
               <source>Economics of Education Review</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d722e555a1310">
            <mixed-citation id="d722e559" publication-type="book">
Middleton, J., Ziderman, A. and Adams, A.V. (Forthcoming). Skills for Productivity: Policies for
Vocational Education and Training in Developing Countries. New York: Oxford University Press<person-group>
                  <string-name>
                     <surname>Middleton</surname>
                  </string-name>
               </person-group>
               <source>Skills for Productivity: Policies for Vocational Education and Training in Developing Countries</source>
            </mixed-citation>
         </ref>
         <ref id="d722e581a1310">
            <mixed-citation id="d722e585" publication-type="book">
Psacharopoulos, G., Tan, J.-P. and Jimenez, E. (1986). Financing Education in Developing Countries:
An Exploration of Policy Options. Washington, D.C.: The World Bank<person-group>
                  <string-name>
                     <surname>Psacharopoulos</surname>
                  </string-name>
               </person-group>
               <source>Financing Education in Developing Countries: An Exploration of Policy Options</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d722e610a1310">
            <mixed-citation id="d722e614" publication-type="book">
Psacharopoulos, G. and Woodhall, M. (1985). Education for Development: An Analysis of Investment
Choices. New York: Oxford University Press<person-group>
                  <string-name>
                     <surname>Psacharopoulos</surname>
                  </string-name>
               </person-group>
               <source>Education for Development: An Analysis of Investment Choices</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d722e639a1310">
            <mixed-citation id="d722e643" publication-type="journal">
Reuterberg, S.E. and Svensson, A. (1990). Student financial aid in Sweden. Higher Education Policy3,
3<person-group>
                  <string-name>
                     <surname>Reuterberg</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <volume>3</volume>
               <source>Higher Education Policy</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d722e675a1310">
            <mixed-citation id="d722e679" publication-type="journal">
Tilak, J. and Varghese, N.V. (1991). Financing higher education in India. Higher Education21, 1, 83-
102<object-id pub-id-type="jstor">10.2307/3447129</object-id>
               <fpage>83</fpage>
            </mixed-citation>
         </ref>
         <ref id="d722e695a1310">
            <mixed-citation id="d722e699" publication-type="book">
U.S. Department of Education (1990). Reducing Student Loan Defaults: A Plan for Action. Washington,
D.C.<person-group>
                  <string-name>
                     <surname>U.S. Department of Education</surname>
                  </string-name>
               </person-group>
               <source>Reducing Student Loan Defaults: A Plan for Action</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d722e725a1310">
            <mixed-citation id="d722e729" publication-type="journal">
Whalley, J. and Ziderman, A. (1990). Financing training in developing countries: the role of payroll
taxes, Economics of Education Review9, 4, 377-387<person-group>
                  <string-name>
                     <surname>Whalley</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>377</fpage>
               <volume>9</volume>
               <source>Economics of Education Review</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d722e764a1310">
            <mixed-citation id="d722e768" publication-type="book">
Woodhall, M. (1983). Student Loans as a Means of Financing Higher Education: Lessons from
International Experience. World Bank Staff Working Paper No. 599. Washington, D.C.: The World
Bank<person-group>
                  <string-name>
                     <surname>Woodhall</surname>
                  </string-name>
               </person-group>
               <source>Student Loans as a Means of Financing Higher Education: Lessons from International Experience</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d722e797a1310">
            <mixed-citation id="d722e801" publication-type="book">
Woodhall, M. (1987). Lending for Learning: Designing a Student Loan Programme for Developing
Countries. London Commonwealth Secretariat<person-group>
                  <string-name>
                     <surname>Woodhall</surname>
                  </string-name>
               </person-group>
               <source>Lending for Learning: Designing a Student Loan Programme for Developing Countries</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d722e826a1310">
            <mixed-citation id="d722e830" publication-type="book">
Woodhall, M., ed. (1989). Financial Support for Students: Grants, Loans or Graduate Tax? London,
Kogan Page<person-group>
                  <string-name>
                     <surname>Woodhall</surname>
                  </string-name>
               </person-group>
               <source>Financial Support for Students: Grants, Loans or Graduate Tax?</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
