<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26347899</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Neonatal mortality in the developing world</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Hill</surname>
                  <given-names>Kenneth</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Choi</surname>
                  <given-names>Yoonjoung</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Department of Population and Family Health Sciences, Bloomberg School of Public Health, Johns Hopkins University</aff>
            <aff id="af2">
               <label>²</label>Department of International Health, Bloomberg School of Public Health, Johns Hopkins University Address for correspondence: Yoonjoung Choi, Department of International Health, Bloomberg School of Public Health, Johns Hopkins University, 615 North Wolfe Street, Baltimore, MD 21205, U.S.A.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2006</year>
            <string-date>JANUARY - JUNE 2006</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">14</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26347881</issue-id>
         <fpage>429</fpage>
         <lpage>452</lpage>
         <permissions>
            <copyright-statement>© 2006 Max-Planck-Gesellschaft</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26347899"/>
         <abstract xml:lang="eng">
            <p>This paper examines age patterns and trends of early and late neonatal mortality in developing countries, using birth history data from the Demographic and Health Surveys (DHS). Data quality was assessed both by examination of internal consistency and by comparison with historic age patterns of neonatal mortality from England and Wales. The median neonatal mortality rate (NMR) across 108 nationally-representative surveys was 33 per 1000 live births. NMR averaged an annual decline of 1.9 % in the 1980s and 1990s. Declines have been faster for late than for early neonatal mortality and slower in Sub-Saharan Africa than in other regions. Age patterns of neonatal mortality were comparable with those of historical data, indicating no significant underreporting of early neonatal deaths in DHS birth histories.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Artzrouni M, Zaba B. HIV-induced bias in the estimation of child mortality using birth history reports. Presented at the Technical Meeting on HIV and Child Mortality. London: March 2003.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Bang A, Reddy MH, Deshmukh MD. Child mortality in Maharashtra. Economic and Political Weekly 2002; 37(49):4947-4965.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Black RE, Morris SS, Bryce J. 2003. Where and why are 10 million children dying every year? The Lancet 361(9376):2226-2234.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Curtis S. Assessment of the quality of data used for direct estimation of infant and child mortality in DHS-II surveys. Occasional Papers 3. 1995. Calverton, MD, Macro International Inc. Arnold F, Blanc A. Fertility levels and trends. DHS Comparative Studies 2. 1990. Columbia, MD, Institute for Resource Development.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">El Arifeen S. 2003. Personal Communication</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">ICDDR B. Health and demographic surveillance system - Matlab: Registration of health and demographic events 2000. 2002. Dhaka, Bangladesh, ICDDR, B.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Lawn JE, Cousens S, Zupan J; Lancet Neonatal Survival Steering Team. 4 million neonatal deaths: when? Where? Why? Lancet. 2005 Mar 2;365(9462):891-900.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">United Nations. 2001. General Assembly, 56th session. Road map towards the implementation of the United Nations millennium declaration: report of the Secretary-General (UN document no. A/56/326). New York: United Nations.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">World Health Organization. 2005. World Health Report 2005- Make Every Mother and Child Count. Geneva: World Health Organization.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
