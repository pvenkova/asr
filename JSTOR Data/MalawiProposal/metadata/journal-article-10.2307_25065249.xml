<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100641</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03057070</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14653893</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25065249</article-id>
         <title-group>
            <article-title>The Modern Traditional Healer: Locating 'Hybridity' in Modern Traditional Medicine, Southern Tanzania</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rebecca</given-names>
                  <surname>Marsland</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">33</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25065244</issue-id>
         <fpage>751</fpage>
         <lpage>765</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 The Editorial Board of the Journal of Southern African Studies</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25065249"/>
         <abstract>
            <p>This article explores how 'modern traditional' healers, working in Kyela District in the south west of Tanzania, attempt to challenge and transcend a widely-recognised dualism that places forms of biomedicine as 'modern' and all varieties of indigenous healing as 'traditional'. Drawing on the notion of 'intentional hybridity', the article analyses conversations with healers, which reveal how their practices and aspirations operate to destabilise the boundaries that are so essential to the way that they and their medicines are imagined. Framing their practices in terms of competition for business between both mission medicine and government-employed biomedical practitioners, the healers worked to reposition themselves within this 'modern' and moral space by emphasising the similarities between their own medicines and biomedical pharmaceuticals, for instance, or claiming that biomedical personnel are jealous of their ability to attract patients (or customers). They were also prepared to support a local government initiative that, if effective, may reduce the incidence of witchcraft-related illness - a stance which in theory does not make good 'business sense', yet that places them firmly in opposition to some forms of 'tradition'. Finally, the article details the technical innovations which healers would like to appropriate from biomedicine. In conclusion, the material presented demonstrates the ability of the healers to transcend, contest and make use of the constructed categories through which they are imagined in Tanzania.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1247e114a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1247e121" publication-type="other">
B. Latour, We Have Never Been Modern (trans. C. Porter) (New York, London, Harvester Wheatsheaf, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e128a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1247e135" publication-type="other">
H. Englund and J. Leach, 'Ethnography and the Meta-Narratives of Modernity', Current Anthropology, 41, 2
(2000), pp. 225-39.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e144" publication-type="other">
J.-F. Lyotard, The Postmodern Condition: A Report on Knowledge (Minneapolis, University
of Minnesota Press, 1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e154a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1247e161" publication-type="other">
S. Beckerleg, 'Modernity Has Been Swahili-ised: The Case of Malindi', in P. Caplan and F. Topan (eds), Swahili
Modernities: Culture, Politics and Identity on the East Coast of Africa (Trenton, Eritrea, Africa World Press,
2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e174a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1247e181" publication-type="other">
R. Marsland, 'Ethnographic Malaria. The
Uses of Medical Knowledge in Tanzania' (PhD thesis, SOAS, University of London, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e192a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1247e199" publication-type="other">
S. Feierman, 'Struggles for Control: The Social Roots of Health and Healing in Modern Africa', African Studies
Review, 28, 2/3 (1985), pp. 110-12.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e208" publication-type="other">
M. Last, 'The Professionalisation of African Medicines: Ambiguities
and Definitions', in M. Last and G.L. Chavanduka (eds), The Professionalisation of African Medicines
(Manchester, Manchester University Press in association with the International African Institute, 1986), p. 5.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e220" publication-type="other">
H.G. West and TJ. Luedke, 'Introduction. Healing Divides: Therapeutic Border Work in Southeast Africa', in T.J.
Luedke and H.G. West (eds), Borders and Healers: Brokering Therapeutic Resources in Southeast Africa
(Bloomington and Indianapolis, Indiana University Press, 2006), pp. 4-5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e233a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1247e240" publication-type="other">
J. Nyerere, Freedom and Socialism: A Selection from Writings and Speeches (Dar es Salaam, Oxford University
Press, 1968).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e249" publication-type="other">
H.G. West, 'Working the Borders to Beneficial Effect:
The Not-So-Indigenous Knowledge of Not-So-Traditional Healers in Northern Mozambique', in Luedke and
West (eds), Borders and Healers, p. 21.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e262a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1247e269" publication-type="other">
P. Caplan, '"Struggling to be Modern": Recent Letters from Mafia Island', in P. Caplan and F. Topan (eds),
Swahili Modernities: Culture, Politics, and Identity on the East Coast of Africa (Trenton, Eritrea, Africa World
Press, 2004).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e281" publication-type="other">
M. Green, 'Participatory Development and the Appropriation of Agency in Southern Tanzania',
Critique of Anthropology, 20, 1 (2000), pp. 67-89.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e290" publication-type="other">
C. Mercer, 'The Discourse of Maendeleo', Development and
Change, 33 (2002), pp. 101-27.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e300a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1247e307" publication-type="other">
W.C. Bissell, 'Engaging Colonial Nostaliga', Cultural Anthropology, 20, 2 (2005), pp. 215-48.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e313" publication-type="other">
Caplan,
'Struggling to be Modern'.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e322" publication-type="other">
B. Weiss, 'Thug Realism: Inhabiting Fantasy in Urban Tanzania', Cultural
Anthropology, 17, 1 (2002), pp. 93-124.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e332a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1247e339" publication-type="other">
Marsland, 'Ethnographic Malaria'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e346a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1247e353" publication-type="other">
Latour, We Have Never Been Modern, p. 11</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e361a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1247e368" publication-type="other">
J. Iliffe, East African Doctors: A History of the Modern Profession (Cambridge, Cambridge University Press,
1998).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e377" publication-type="other">
Last, 'The Professionalisation of African Medicines', pp. 10-11.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e384a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1247e391" publication-type="other">
Last, 'The Professionalisation of African Medicines', p. 11</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e397" publication-type="other">
J. Pfeiffer,
'Money, Modernity and Morality: Traditional Healing and the Expansion of the Holy Spirit in Mozambique', in
Luedke and West, Borders and Healers, pp. 81-100</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e409" publication-type="other">
D. Simmons, 'Of Markets and Medicine: the Changing
Significance of Zimbabwean Muti in the Age of Intensified Globalization', in Luedke and West, Borders and
Healers, pp. 65-80.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e422" publication-type="other">
J. Murchison, 'From HIV/AIDS to Ukimwi: Narrating Local Accounts of a Cure', in Luedke and West, Borders
and Healers, pp. 125-42</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e431" publication-type="other">
P. Probst, 'Mchape '95, or, the Sudden Fame of Billy Goodson Chisupe: Healing,
Social Memory and the Enigma of the Public Sphere in Post-Banda Malawi', Africa, 69, 1 (1999), pp. 108-38</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e440" publication-type="other">
C.B. Yamba, 'Cosmologies in Turmoil: Witchfinding and AIDS in Chiawa, Zambia', Africa, 67, 2 (1997),
pp. 200-23.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e450a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1247e457" publication-type="other">
P. Werbner, 'The Limits of Cultural Hybridity: On Ritual Monsters, Poetic Licence and Contested Postcolonial
Purifications', Journal of the Royal Anthropological Institute, 7 (2001), p. 137.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e467a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1247e474" publication-type="other">
P.P. Mhame, 'The Role of Traditional Medicine (Medicinal Flora and Fauna) in the National Economy'
(unpublished paper, National Institute for Medical Research, Dar es Salaam, Tanzania, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e484a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1247e491" publication-type="other">
West and Luedke,'Introduction'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e498a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1247e505" publication-type="other">
Dar es Salaam. E. Hsu, 'Time Inscribed in Space, and the
Process of Diagnosis in African and Chinese Medical Practices', in W. James and D. Mills (eds), The Qualities of
Time: Anthropological Approaches (Oxford; New York, Berg, 2005), p. 165.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e519a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1247e526" publication-type="other">
M.C. Gessler, D. Msuya, M.H.H. Nkunya, A. Schar, M. Heinrich and M. Tanner, 'Traditional Healers in
Tanzania: Sociocultural Profile and Three Short Portraits', Journal ofEthnopharmacology, 48 (1995), pp. 153-4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e536a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1247e543" publication-type="other">
West, 'Working the Borders to Beneficial Effect', p. 23.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e550a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1247e557" publication-type="other">
Gessler et al., 'Traditional Healers', pp. 152-3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e564a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1247e571" publication-type="other">
T. Ranger, 'Godly Medicine: the Ambiguities of Medical Mission in Southeast Tanzania, 1900-1945', Social
Science and Medicine, 15B (1981), p. 262</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e580" publication-type="other">
S. Langwick, 'Geographies of Medicine: Interrogating the Boundary
Between "Traditional" and "Modern" Medicine in Colonial Tanganyika', in Luedke and West, Borders and
Healers, pp. 143-65.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e593a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1247e600" publication-type="other">
Ranger, 'Godly Medicine', pp. 261-77.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e607a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1247e614" publication-type="other">
S. Mesaki, 'Witchkilling in Sukumaland', in R. Abrahams (ed.), Witchcraft in Contemporary Tanzania
(Cambridge, Cambridge University Press, 1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e623" publication-type="other">
Yamba, 'Cosmologies in Turmoil', pp. 200-23</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e629" publication-type="other">
M. Green and
S. Mesaki, 'The Birth of the "Salon": Poverty, "Modernization" and Dealing with Witchcraft in Southern
Tanzania', American Ethnologist, 32, 3 (2005), pp. 371-88.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e643a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1247e650" publication-type="other">
S. Hausmann-Muela, R.J. Muela and M. Tanner, 'Fake Malaria and Hidden Parasites - the Ambiguity of
Malaria', Anthropology and Medicine, 5, 1 (1998), pp. 43-61.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e659" publication-type="other">
M. Wilson, Divine Kings and the Breath of Men
(Cambridge, Cambridge University Press, 1959), p. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e669a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1247e676" publication-type="other">
Marsland, 'Ethnographic Malaria'</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e683a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1247e690" publication-type="other">
R. Marsland, 'Intervening in Witchcraft: Public Health and the Nyakyusa in Tanzania',
submitted to Africa for consideration.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e700a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1247e707" publication-type="other">
S.J. Tambiah, 'The Magical Power of Words', Man N.S., 3, 2 (1968), pp. 175-208.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e714a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1247e721" publication-type="other">
M. Wilson, 'Witch Beliefs and Social Structure', American Journal of Sociology, 56 ( 1951 ) pp. 307 -13.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e728a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1247e735" publication-type="other">
M. Green, 'Discourses on Inequality: Poverty, Public Bads and Entrenching Witchcraft in Post-adjustment
Tanzania', Anthropological Theory, 5, 3 (2005), pp. 247-66.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e746a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1247e753" publication-type="other">
Green and Mesaki. 'The Birth of the Salon'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e760a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1247e767" publication-type="other">
C. Msoka, 'Re-Emergence of African Traditional Medicines in Cities: Weaknesses of the Western-Modern-
Global Medicines?' (unpublished paper, presented at 'From Western Medicine to Global Medicine: The Hospital
Beyond the West', Wellcome Institute for the History of Medicine, University of Oxford, 2004).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e779" publication-type="other">
Simmons, 'Of Markets and Medicine'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e786a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1247e793" publication-type="other">
O.B. Rekdal, 'Cross-cultural Healing in East African Ethnography', Medical Anthropology, 13, 4 (1999),
pp. 458-82.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e802" publication-type="other">
Iliffe, East African Doctors</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e808" publication-type="other">
S.R. Wfiyte, 'The Power of Medicines in East Africa', in S.
van der Geest and S.R. Whyte (eds), The Context of Medicines in Developing Countries (Dordrecht, Kluwer
Academic Publishers, 1998), p. 226</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e821" publication-type="other">
M. Vaughan, Curing Their Ills: Colonial Power and African Illness
Cambridge, Polity Press, 1991), p. 24.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e831a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1247e838" publication-type="other">
A. Appadurai, 'Introduction: Commodities and the Politics of Value', in A. Appadurai (ed.), The Social Life of
Things: Commodities in Cultural Perspective (Cambridge, Cambridge University Press, 1986), p. 34.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e848a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1247e855" publication-type="other">
Werbner, 'The Limits of Cultural Hybridity', p. 136.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e862a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1247e869" publication-type="other">
A. Appadurai, Modernity at Large: Cultural Dimensions of Globalization (Minneapolis, University of Minnesota
Press, 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e880a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1247e887" publication-type="other">
Gessler et al., 'Traditional Healers in Tanzania', p. 155</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e894a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1247e901" publication-type="other">
West, 'Working the Borders to Beneficial Effect', p. 31.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e908a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1247e915" publication-type="other">
L. Manderson and L. Whiteford, Global Health Policy, Local Realities: The Fallacy of a Level Playing Field
(Boulder, CO, Lynne Rienner, 2000), p. 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e925a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1247e932" publication-type="other">
J.N. Pieterse, 'Hybridity, So What? The Anti-Hybridity Backlash and the Riddles of Recognition', Theory,
Culture and Society, 18, 2-3 (2001), p. 220.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
