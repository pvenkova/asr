<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt4z718</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1803z89</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Zoology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Bovids of the World</book-title>
         <subtitle>Antelopes, Gazelles, Cattle, Goats, Sheep, and Relatives</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Castelló</surname>
               <given-names>José R.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <contrib-group>
         <role content-type="foreword-author">Foreword by</role>
         <contrib contrib-type="foreword-author" id="contrib2">
            <name name-style="western">
               <surname>Huffman</surname>
               <given-names>Brent</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib3">
            <name name-style="western">
               <surname>Groves</surname>
               <given-names>Colin</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>12</day>
         <month>04</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9780691167176</isbn>
      <isbn content-type="ppub">0691167176</isbn>
      <isbn content-type="epub">9781400880652</isbn>
      <isbn content-type="epub">1400880653</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>José R. Castelló</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1803z89"/>
      <abstract abstract-type="short">
         <p>Bovids are a diverse group of ruminant mammals that have hooves and unbranched hollow horns.<italic>Bovids of the World</italic>is the first comprehensive field guide to cover all 279 bovid species, including antelopes, gazelles, cattle, buffaloes, sheep, and goats. From the hartebeest of Africa and the takin of Asia to the muskox of North America, bovids are among the world's most spectacular animals and this stunningly illustrated and easy-to-use field guide is an ideal way to learn more about them.</p>
         <p>The guide covers all species and subspecies of bovids described to date. It features more than 300 superb full-color plates depicting every kind of bovid, as well as detailed facing-page species accounts that describe key identification features, horn morphology, distribution, subspeciation, habitat, and conservation status in the wild. This book also shows where to observe each species and includes helpful distribution maps.</p>
         <p>Suitable for anyone with an interest in natural history,<italic>Bovids of the World</italic>is a remarkable and attractive reference, showcasing the range and beauty of these important mammals.</p>
         <p>The first comprehensive field guide to all 279 bovid species337 full-color plates, with more than 1,500 photographsDetailed species accounts describe key identification features, distribution, subspeciation, habitat, behavior, reproduction, and conservation statusFully updated and revised taxonomy, with common and scientific namesEasy-to-read distribution maps</p>
      </abstract>
      <counts>
         <page-count count="664"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>1</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>3</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.3</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Huffman</surname>
                           <given-names>Brent</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Groves</surname>
                           <given-names>Colin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>5</fpage>
                  <abstract>
                     <p>I have been waiting for a book like<italic>Bovids of the World</italic>to be published since I was in seventh grade. In my science class that year, we were given the assignment of creating an identification key for 20 closely related animals. Having just completed a report on rumination, I naturally chose ruminants. (“Will you be able to find enough species for this assignment?” worried my teacher.) My childhood animal encyclopedia readily yielded 20 species, and although they were all interesting, the ones with horns immediately caught my eye. The horns, with their myriad ridges, curls, and twists, were like</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>7</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>8</fpage>
                  <abstract>
                     <p>The family Bovidae, which includes Antelopes, Cattle, Duikers, Gazelles, Goats, and Sheep, is the largest family within Artiodactyla and the most diverse family of ungulates, with more than 270 recent species. Their common characteristic is their unbranched, non-deciduous horns. Bovids are primarily Old World in their distribution, although a few species are found in North America. The name antelope is often used to describe many members of this family, but it is not a definable, taxonomically based term.</p>
                     <p>Shape, size, and color: Bovids encompass an extremely wide size range, from the minuscule Royal Antelope and the Dik-diks, weighing as little</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.6</book-part-id>
                  <title-group>
                     <title>TRIBE AEPYCEROTINI</title>
                  </title-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>BL: 125-160 cm (♂), 120-150 cm (♀). SH: 80-95 cm (♂), 75-90 cm (♀). TL: 30-45 cm. W: 45-80 kg (♂), 40-60 kg (♀). HL: 45-92 cm (♂). Short, glossy coat with a reddish saddle over a light tan torso. Undersides white, as are the rings around the eyes. Muzzle and chin lighter in color. Black markings on the ear tips and 3 black stripes on the rump: 2 on the flanks and 1 down the tail. Unique among the Bovidae, Impala possess metatarsal glands, which are strikingly marked with a tuft of black hair above the rear hooves. Lyre-shaped</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.7</book-part-id>
                  <title-group>
                     <title>TRIBE NEOTRAGINI</title>
                  </title-group>
                  <fpage>28</fpage>
                  <abstract>
                     <p>BL: 57-62 cm. SH: 33-35 cm. TL: 8-13 cm. W: 4.5 kg. HL: 6.5-13.3 cm (♂). A very small antelope, with long and slender legs, a typically compact stance, and a disproportionately broad head and short neck. Slightly smaller than the Livingstone’s Suni, with a lighter, duller coloration. General coat color is reddish brown, with the back darker than the flanks and legs. Underparts, including the chin, throat, and insides of the legs, are white. Lighter ring around the eye. Facial glands are enormous, especially in males. Pink-lined ears. The legs are ringed with a black band just above the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.8</book-part-id>
                  <title-group>
                     <title>TRIBE REDUNCINI</title>
                  </title-group>
                  <fpage>38</fpage>
                  <abstract>
                     <p>BL: 180-220 cm. SH: 100-136 cm. TL: 33-45 cm. W: 250-275 kg (♂), 160-180 kg (♀). HL: 79-92 cm (♂). Large and robust antelope. Shaggy, coarse coat is reddish brown to grizzled gray in color, darkening with age. Hair on the neck is especially long and forms a rough mane. Facial markings composed of a white muzzle, lighter eyebrows, and insides of the ears. Cream-colored bib on the throat. Large white halo surrounding the base of the tail on the rump (no other antelope has such a marking). Body is heavyset, strong legs are black in color, with a white</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.9</book-part-id>
                  <title-group>
                     <title>TRIBE ANTILOPINI</title>
                  </title-group>
                  <fpage>82</fpage>
                  <abstract>
                     <p>BL: 112-127 cm. SH: 68-82 cm. TL: 20-30 cm. W: 20-38 kg (♂), 20-28 kg (♀). HL: 28-44 cm (♂), 16 cm (♀). Medium-sized gazelle. The smallest Springbok. White and rich chestnut-brown color, with a deep brown stripe extending along each side from the shoulder to inside the thigh. Head white except for a very thin brown stripe from eye to muzzle. Skin fold normally closed, but when the animal is excited it opens to form a fan of stiff, white hairs. Brown or fawn on forehead not extending in front of the level of the eyes, and not sharply</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.10</book-part-id>
                  <title-group>
                     <title>TRIBE OREOTRAGINI</title>
                  </title-group>
                  <fpage>224</fpage>
                  <abstract>
                     <p>BL: 83 cm. SH: 42 cm. TL: 7-13 cm. W: 9-12 kg (♂), 11-18 kg (♀). HL: 7.5-9 cm (♂). Largest subspecies of Klipspringer, with particularly short horns. Short coat, speckled overall color. Middle of the underside is pale, and the chin and throat are pale yellowish. Dark brown patch above each hoof. Ears relatively short, whitish, with a thick black line along the rim. Forehead reddish brown. Very large, slit-like preorbital glands, especially in males, with bare dark skin surrounding them. Tail very short, reduced to a mere stump. Horns, found in males, are wide-set on the forehead and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.11</book-part-id>
                  <title-group>
                     <title>TRIBE CEPHALOPHINI</title>
                  </title-group>
                  <fpage>244</fpage>
                  <abstract>
                     <p>BL: 70-105 cm (♂), 90-115 cm (♀)). SH: 40-65 cm. TL: 10-20 cm. W: 10-22 kg (♂), 10-26 kg (♀). HL: 7-18 cm (♂). Largest subspecies. Coat color is a pale grayish brown, grizzled with black. Lower parts colored like the back. Chin, belly, and insides of the upper legs are whitish. Short tail is black on the top, contrasting sharply with the fluffy white underside. Forelegs are black. Face is reddish with a dark brown nose stripe, not reaching the top of the head. Ears are long, with narrow pointed tips, separated by a tuft of hair on the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.12</book-part-id>
                  <title-group>
                     <title>TRIBE CAPRINI</title>
                  </title-group>
                  <fpage>302</fpage>
                  <abstract>
                     <p>BL: 130-165 cm. SH: 75-110 cm. TL: 15-20 cm. W: 30-145 kg. HL: Up to 88 cm (♂). Medium-sized sheep. Short, bristly outer coat reddish to sandy brown in color. Underparts are moderately lighter. Both sexes have a heavy fringe of hair on the throat, although in males this extends down the neck to encompass the chest and front legs. Tail is also fringed. Body quite thick and sturdy. Thick, triangular-based horns, found in both sexes, although slightly larger in males. Horns have numerous fine rings, although in older individuals these may be worn down, causing the horn surface to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.13</book-part-id>
                  <title-group>
                     <title>TRIBE HIPPOTRAGINI</title>
                  </title-group>
                  <fpage>466</fpage>
                  <abstract>
                     <p>BL: 190-255 cm. SH: 117-143 cm. TL: 40-75 cm. W: 190-250 kg (♂), 160-180 kg (♀). HL: 80-165 cm (♂), 60-100 cm (♀). Large antelope, with shoulders higher than hindquarters. This is the only subspecies in which females become nearly black, though they tend to be lighter than males. Mature males are chestnut to jet black. The white belly contrasts greatly with the back and sides. Face is white with a black facial mask consisting of a wide black stripe on the bridge of the nose, and stripes running from the eyes to the nose. Thick neck enhanced by a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.14</book-part-id>
                  <title-group>
                     <title>TRIBE ALCELAPHINI</title>
                  </title-group>
                  <fpage>496</fpage>
                  <abstract>
                     <p>BL: 180-240 cm (♂), 170-230 cm (♀). SH: 141-156 cm (♂), 129-144 cm (♀). TL: 60-100 cm. W: 232-295 kg (♂), 165-216 kg (♀). HL: To 83 cm (♂), 30-40 cm (♀). Large antelope. Coloration dark blue gray with a tinge of brown and a silvery sheen. Lower segments of limbs tan colored. Underparts slightly darker. Series of short black stripes on the neck and shoulders. Slight hump above the shoulders, with a slight slope toward the rear. Long, horse-like tail is black. Convex face black from crown to nostrils, with a reddish tinge on the forehead; side of face</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.15</book-part-id>
                  <title-group>
                     <title>TRIBE BOSELAPHINI</title>
                  </title-group>
                  <fpage>542</fpage>
                  <abstract>
                     <p>BL: 180-210 cm (♂), 170 cm (♀). SH: 120-150 cm. TL: 40-45 cm. W: 200-290 kg (♂), 120-212 kg (♀). HL: 15-25 cm (♂). Large Bovid, equine in appearance. Short coat yellow brown in females, gradually turns blue gray in males as they mature. Erectile mane on the nape and back and a hair pennant in the middle of the underside of the neck. White markings in the form of cheek spots, edges of the lips, ears, fetlocks, and a throat bib. Preorbital glands are small. Slender legs supporting a stocky body, which slopes downward toward the rear. Head long</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.16</book-part-id>
                  <title-group>
                     <title>TRIBE TRAGELAPHINI</title>
                  </title-group>
                  <fpage>546</fpage>
                  <abstract>
                     <p>BL: 220-290 cm. SH: 150-175 cm. TL: 90 cm. W: 440-900 kg. HL: 120 cm. A very large antelope. Sandy-colored ground coat, with around 12 vertical white stripes on the torso. Short-haired black spinal crest down the neck to the middle of the back. Slender legs, with black and white markings just above the hooves and large black spots on the upper forelegs. Bridge of the nose charcoal black in color; thin, indistinct tan-colored chevron between the eyes. Lips white, along with several dots along the jawline. Pendulous dewlap, larger in males, from between the jowls hanging to the upper</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.17</book-part-id>
                  <title-group>
                     <title>TRIBE BOVINI</title>
                  </title-group>
                  <fpage>596</fpage>
                  <abstract>
                     <p>BL: 240-300 cm. SH: 150-190 cm. TL: 60-100 cm. W: 700-1,200 kg. A large, heavily built animal, without a pronounced hump. Skin color light gray to black, usually mud covered, with rather long hairs, but many parts are hairless, particularly with age. Neck large and thick, with a white V running across the front. Forehead narrow, ears large. No dewlap on the throat. Tail has a bushy end. The 4 legs have an off-white coloring that runs down from knees to hooves. Hooves large and splayed. Horns in both sexes, heavy at the base, triangular in cross section, and widely</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.18</book-part-id>
                  <title-group>
                     <title>SKULLS</title>
                     <subtitle>SKULLS OF BOVIDS</subtitle>
                  </title-group>
                  <fpage>650</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.19</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>659</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1803z89.20</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>660</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
