<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plantandsoil</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50010412</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant and Soil</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic Publishers</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0032079X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15735036</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42939775</article-id>
         <title-group>
            <article-title>Peanut growth as affected by liming, Ca-Mn interactions, and Cu plus Zn applications to oxidic samoan soils</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>A.W.</given-names>
                  <surname>Bekker</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>N.V.</given-names>
                  <surname>Hue</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>L.G.G.</given-names>
                  <surname>Yapa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>R.G.</given-names>
                  <surname>Chase</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">164</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40113179</issue-id>
         <fpage>203</fpage>
         <lpage>211</lpage>
         <permissions>
            <copyright-statement>© 1994 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42939775"/>
         <abstract>
            <p>Effects of coralline lime, in combination with 3 kg Cu ha⁻¹ plus 3 kg Zn ha⁻¹, on yield and nutrient uptake by peanut (Arachis hypoged) were studied at three locations in Western Samoa. Coarse (0-10 mm) coralline lime material containing 31.1% Ca and 1.7% Mg was used as lime at 0, 555, 2222 and 5000 kg ha⁻¹. In the Togitogiga soil, which had the lowest level of exchangeable Ca, peanut yield increased by 6 fold after liming with 555 kg ha⁻¹, relative to the unamended control. This yield increase was associated with reduced Mn toxicity as well as reduced Ca deficiency. The alleviation of Mn toxicity was not likely due to decreased Mn solubility because the lime application (555 kg ha⁻¹) increased soil pH by &lt; 0.1 unit. Rather it was the increased Ca availability which reduced the Mn toxicity through a Ca/Mn antagonism. The critical range of exchangeable Ca for peanut growth was found to be about 1.5-1.6 cmol ½Ca²+ kg⁻¹. A Ca/Mn-ratio &gt; 80 was required for a desirable ½Ca²⁺/Mn balance in peanut tissue. On the other two locations (with exchangeable Ca levels of 1.5-1.6 cmol ½Ca²⁺ kg⁻¹), liming increased peanut yields by 15-20%. Additions of Cu plus Zn also increased the yields, although the increases were small (7%) and not significant at the 95% probability level.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d714e214a1310">
            <mixed-citation id="d714e218" publication-type="other">
Asghar M 1988 The nature and properties of Western Samoan soils.
In Proceedings of the XVth International Forum on Soil Taxon-
omy and Agrotechnology Transfer. Eds. M Asghar, T J David-
son and R J Morrison, pp 366-375. IRETA/USP, Apia, Western
Samoa.</mixed-citation>
         </ref>
         <ref id="d714e237a1310">
            <mixed-citation id="d714e241" publication-type="other">
Adams F, Burmester C, Hue N V and Long FL 1980 A comparison of
column-displacement and centrifuge methods for obtaining soil
solutions. Soil Sei. Soc. Am. J. 44, 733-735.</mixed-citation>
         </ref>
         <ref id="d714e254a1310">
            <mixed-citation id="d714e258" publication-type="other">
Bekker A W and Chase R G 1994a Some aspects of the use of coarse
coralline lime in liming experiments. J. South Pacific Agrie. (In
press).</mixed-citation>
         </ref>
         <ref id="d714e271a1310">
            <mixed-citation id="d714e275" publication-type="other">
Bekker A W and Chase R G 1994b Response of cowpea and maize
to liming pots with coralline lime from Mulinuu, Western Samoa.
J. South Pacific Agrie. (In press).</mixed-citation>
         </ref>
         <ref id="d714e289a1310">
            <mixed-citation id="d714e293" publication-type="other">
Bekker A W, Chase R G and Hue N V 1993 Effects of coralline
lime on nutrient uptake and yield of sweet corn and peanuts field-
grown in Western Samoa. Fert. Res. 36, 211-219.</mixed-citation>
         </ref>
         <ref id="d714e306a1310">
            <mixed-citation id="d714e310" publication-type="other">
Bot J le, Goss M J, Carvalho M J G P R and Beusichem M L van
1990 The significance of the magnesium to manganese ratio in
plant tissue for growth and alleviation of Mn toxicity in tomato
and wheat. Plant and Soil 124, 205-210.</mixed-citation>
         </ref>
         <ref id="d714e326a1310">
            <mixed-citation id="d714e330" publication-type="other">
Cornforth I S 1982 Plant analysis. In Fertilizer and Lime Recom-
mendations for Pastures and Crops in New Zealand. Eds. I S
Cornforth and A G Sinclair, pp 34-36. Ministry of Agriculture
and Fisheries, Wellington, New Zealand.</mixed-citation>
         </ref>
         <ref id="d714e346a1310">
            <mixed-citation id="d714e352" publication-type="other">
Foy C D 1984 Physiological effects of hydrogen, aluminum and
manganese toxicities in acid soils. In Soil Acidity and Liming,
2nd ed., Agronomy series #12, Ed. F Adams, pp 57-98. American
Society of Agronomy, Madison, Wisconsin, USA.</mixed-citation>
         </ref>
         <ref id="d714e368a1310">
            <mixed-citation id="d714e372" publication-type="other">
Fox R L, Hue N V, Jones R C and Yost RS 1991 Plant-soil inter-
actions associated with acid weathered soils. Plant and Soil 134,
65-72.</mixed-citation>
         </ref>
         <ref id="d714e385a1310">
            <mixed-citation id="d714e389" publication-type="other">
Gomez K A and Gomez A A 1984 Statistical Procedures for Agri-
cultural Research. 2nd ed. Wiley, New York, USA.</mixed-citation>
         </ref>
         <ref id="d714e400a1310">
            <mixed-citation id="d714e404" publication-type="other">
Haby V A, Anderson W B and Welch C D 1979 Effect of limestone
variables on amendment of acid soils and production of corn and
coastal Bermudagrass. Soil Sei. Soc. Am. J. 43, 343-347.</mixed-citation>
         </ref>
         <ref id="d714e417a1310">
            <mixed-citation id="d714e421" publication-type="other">
Haynes R J and Ludecke TE 1981 Effect of lime and phosphorus
applications on concentrations of available nutrients and on P, Al
and Mn uptale by two pasture legumes in an acid soil. Plant and
Soil 62, 117-128.</mixed-citation>
         </ref>
         <ref id="d714e437a1310">
            <mixed-citation id="d714e441" publication-type="other">
Heenan D P and Carter O G 1975 Response of two soya bean
cultivars to manganese toxicity as affected by pH and calcium
levels. Aust. J. Aerie. Res. 26, 967-974.</mixed-citation>
         </ref>
         <ref id="d714e454a1310">
            <mixed-citation id="d714e458" publication-type="other">
Hue N V, Fox R L and McCall W W 1987a Influence of soil acid-
ity and liming on growth and leaf composition of macadamia
seedlings. In Proceedings of the 27th Annual Meeting of Hawaii
MacademiaNut Association, May 16, 1987, Hilo, HI. pp 53-67.</mixed-citation>
         </ref>
         <ref id="d714e474a1310">
            <mixed-citation id="d714e478" publication-type="other">
Hue N V, Fox R L and McCall W W 1987b Aluminum, Ca and Mn
concentrations in macadamia seedlings as affected by soil acidity
and liming. Commun. Soil Sci. Plant Anal. 20, 1499-1511.</mixed-citation>
         </ref>
         <ref id="d714e491a1310">
            <mixed-citation id="d714e495" publication-type="other">
Jones J B 1974 Plant analysis handbook for Georgia. Univ. Georgia
Coll. Agrie. Bull. 735.</mixed-citation>
         </ref>
         <ref id="d714e506a1310">
            <mixed-citation id="d714e510" publication-type="other">
Kelly G. 1985 A review of agronomic work in Zambia and prospects
for the future. In Proceedings of the Regional Groundnut Work-
shop Southern Africa, Malawi. Ed. D McDonald, pp 115-118.
ICRISAT, Andra Pradesh, India.</mixed-citation>
         </ref>
         <ref id="d714e526a1310">
            <mixed-citation id="d714e530" publication-type="other">
Löhnis M P 1960 Effect of magnesium and calcium supply on the
uptake of manganese by various crop plants. Plant and Soil 4,
339-376.</mixed-citation>
         </ref>
         <ref id="d714e543a1310">
            <mixed-citation id="d714e547" publication-type="other">
Marchai J and Fouré E 1983 Un cas de toxicité du manganese chez
des bananiers plantains au Gabon. Oleagineux 38, 153-160.</mixed-citation>
         </ref>
         <ref id="d714e557a1310">
            <mixed-citation id="d714e561" publication-type="other">
Morrison R J, Naidu R, Ganyaiya P and Sing Y W 1989 Amélioration
of soil acidity and the impact on the productivity of some highly
weathered Fijian soils. In Soil Management and small holder
Development in the Pacific Islands, IBSRAM proc #8. pp 255-
270. International Board for Soil Research and Management Ine,
Thailand.</mixed-citation>
         </ref>
         <ref id="d714e584a1310">
            <mixed-citation id="d714e588" publication-type="other">
Morrison R J 1991 Variability of the Fagaga series soils of Western
Samoa. Geoderma 49, 105-116.</mixed-citation>
         </ref>
         <ref id="d714e598a1310">
            <mixed-citation id="d714e602" publication-type="other">
Morrison R J and Asghar M 1992 Soils of the Laloanea Farm,
Northwestern Upolu, Western Samoa. Pacific Sci. 46, 35-45.</mixed-citation>
         </ref>
         <ref id="d714e613a1310">
            <mixed-citation id="d714e617" publication-type="other">
Motto H L and Melsted S W 1960 The efficiency of various particle
size fractions of limestone. Soil Sei. Soc. Am. Proc. 24, 488-490.</mixed-citation>
         </ref>
         <ref id="d714e627a1310">
            <mixed-citation id="d714e631" publication-type="other">
Murphey J and Riley J P 1962 A modified single solution method for
determination of phosphate in natural waters. Anal. Chim. Acta
27, 31-36.</mixed-citation>
         </ref>
         <ref id="d714e644a1310">
            <mixed-citation id="d714e648" publication-type="other">
Ngwira P 1985 Groundnut improvement in Malawi. In Proceedings
of the Regional Groundnut Workshop Southern Africa, Malawi.
Ed. D McDonald, pp 49-53. ICRISAT, Andra Pradesh, India.</mixed-citation>
         </ref>
         <ref id="d714e661a1310">
            <mixed-citation id="d714e665" publication-type="other">
Norvell W A 1984 Comparison of chelating agents as extradants for
metals in divers soil materials. Soil Sei. Soc. Am. J. 48, 1285-
1292.</mixed-citation>
         </ref>
         <ref id="d714e678a1310">
            <mixed-citation id="d714e682" publication-type="other">
Robson A D and Loneragan J F 1970 Sensitivity of annual Medic ago
species to manganese toxicity as affected by calcium and pH.
Aust. J. Agrie. Res. 21, 223-232.</mixed-citation>
         </ref>
         <ref id="d714e695a1310">
            <mixed-citation id="d714e699" publication-type="other">
Russell P (Ed.) 1990 Land recourse planning study, Western Samoa,
Asian Development Bank TA No. 1065-SAM, Final Report.
ANZDEC Limited Consultants in association with DSIR Divi-
sion of Land and Soil Sciences. Lower Hütt, New Zealand.</mixed-citation>
         </ref>
         <ref id="d714e716a1310">
            <mixed-citation id="d714e720" publication-type="other">
Small H G and Ohlrogge A J 1973 Plant analysis as an aid in fertil-
izing soya beans and peanuts. In Soil Testing and Plant Analysis
Eds. L M Walsh and J D Beaton, pp 315-327. Soil Science Society
of America Inc., Madison, Wisconsin, USA.</mixed-citation>
         </ref>
         <ref id="d714e736a1310">
            <mixed-citation id="d714e740" publication-type="other">
Vega S, Calisay M and Hue N V 1992 Manganese toxicity in cowpea
as affected by soil pH and sewage sludge amendments. J. Plant
Nutr. 15,219-231.</mixed-citation>
         </ref>
         <ref id="d714e753a1310">
            <mixed-citation id="d714e757" publication-type="other">
Wright ACS 1963 Soils andland use of Western Samoa. New
Zealand Soil Bureau Bulletin #22. Wellington, New Zealand.</mixed-citation>
         </ref>
         <ref id="d714e767a1310">
            <mixed-citation id="d714e771" publication-type="other">
Yost R 1988 Soil acidity and liming. In Proceedings of the XVth
International Forum on Soil Taxonomy and Agrotechnology
Transfer. Eds. M Asghar, T J Davidson and R J Morrison, pp
218-223. IRETA/USP, Apia, Western Samoa.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
