<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt1df4hzq</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Religion</subject>
         <subject>Sociology</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Gulf Charities and Islamic Philanthropy in the "Age of Terror" and Beyond</book-title>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Lacey</surname>
               <given-names>Robert</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Benthall</surname>
               <given-names>Jonathan</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>03</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9783940924322</isbn>
      <isbn content-type="epub">9783940924339</isbn>
      <publisher>
         <publisher-name>Gerlach Press</publisher-name>
         <publisher-loc>Berlin</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Gulf Research Center Cambridge</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1df4hzq"/>
      <abstract abstract-type="short">
         <p>Gulf Charities and Islamic Philanthropy in the "Age of Terror" and Beyond is the first book to be published on the charities of Saudi Arabia and the Arabian Gulf, covering their work both domestic and international. From a diversity of viewpoints, the book addresses: The historical roots of Islamic philanthropy in religious traditions and geopolitical movements, the interactions of the Gulf charities with "Western" relief and development institutions (now under pressure owing to budgetary constraints), numerous case studies from the Middle East, Africa, and South Asia, the impact of violent extremism on the sector, with the legal repercussions that have followed (especially in the USA), the recent history of attempts to alleviate the obstacles faced by bona fide Islamic charities, whose absence from major conflict zones now leaves a vacuum for extremist groups to penetrate, and the prospects for a less politicized Islamic charity sector when the so-called "war on terror" eventually loses its salience.</p>
      </abstract>
      <counts>
         <page-count count="411"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.3</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lacey</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Benthall</surname>
                           <given-names>Jonathan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In the last forty years the Gulf states of Kuwait, Oman, Qatar, Saudi Arabia, and the United Arab Emirates have dispensed charitable donations and aid flows that now earn them rankings, per capita, among the most generous nations on earth – a modern expansion of philanthropy made possible by the petrochemical price explosion of the late twentieth century.¹ But the generosity of Gulf charities also reflects an ancient tradition that goes back to the very origins of Islam, since the injunction to give generously is a cornerstone of the Muslim faith.² All religious traditions encourage charitable giving, but almsgiving (<italic>zakat</italic>) is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Sacralized or secularized aid?</title>
                     <subtitle>Positioning Gulf-based Muslim charities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Petersen</surname>
                           <given-names>Marie Juul</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>Muslim aid organizations such as the Kuwaiti NGO International Islamic Charitable Organization and the Saudi Arabian International Islamic Relief Organization provide meals, medicine, and mosques to poor people all over the globe. Despite this, not much research has been dedicated to the study of these – and other – international Muslim charities. Especially since 9/11, much of the existing literature, often stemming from political science and terrorism studies, casts international Muslim charities as primarily or even entirely political actors, whether analyzing them as front organizations for global militant networks such as Al-Qaeda or as supporters of national political parties and resistance groups</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Islamic charities on a fault line:</title>
                     <subtitle>The Jordanian case</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Challand</surname>
                           <given-names>Benoît</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>The wave of Arab revolts undoubtedly represents a watershed period for Arab politics. The Gulf countries, which have not been spared by the upheavals generated by the initial Tunisian revolt, have reacted in different ways. Significantly, the Gulf Cooperation Council (GCC) extended its hand for membership to Jordan and Morocco, a sign that monarchies were not willing to see their members breaking ranks over the means to organize political order, even if at the heavy cost of nipping dissidence in the bud. Though Jordan has generally not been studied or observed from a Gulf perspective, the developments of 2012 call</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Gulf Charities in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kaag</surname>
                           <given-names>Mayke</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>Gulf charities have expanded their reach to other countries and continents, including some of the poorest regions of the world. This chapter discusses the work of Gulf charities in Africa, where they are encountering working conditions that are quite different from those in their countries of origin. The differences concern among other things the levels of wealth, state policies, and social and cultural circumstances, including religion and forms of Islamic practice.</p>
                     <p>Gulf charities have come with their messages and objectives of assistance and changing social conditions, but they have also adapted to local environments and have tried to work with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Charities and Politics in Arabia during the First Half of the 20th Century:</title>
                     <subtitle>The Al-Kafs of Hadhramaut in Comparative Perspective</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lekon</surname>
                           <given-names>Christian</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>This article focuses upon the Al-Kafs, a prominent family in Hadhramaut, and the public goods provided by them between the 1910s and the 1940s. By way of comparison, mention will also be made of the schools supported by a member of the Zainal Alirezas, a famous Hijazi merchant family, during approximately the same years. We are thus dealing with a period much earlier than that covered by the other contributions to this volume. Nevertheless, both from a global and from a regional perspective, there are some striking similarities between the period discussed in this article and the contemporary period.</p>
                     <p>Before</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Gulf states as multilateral donors:</title>
                     <subtitle>The Special Case of the OPEC Fund for International Development (OFID)</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kropf</surname>
                           <given-names>Annika</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>Today, a world without development aid is unthinkable. The Western developed countries reiterate their commitment to help less and least developed countries and aim at giving 0.7 per cent of their GDP -a threshold they mostly fail to achieve. While the public has come to expect developed nations to give development assistance, this is not as clear for countries which are themselves less developed, but dispose of a certain wealth or “liquidity” due to natural resources. This is the case of the OPEC countries, among them the OPEC members in the Gulf region: Iraq, Iran, Qatar, Saudi Arabia, the United</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Domestic, Religious, Civic?</title>
                     <subtitle>Notes on the Institutionalized Charitable Field in Jeddah, Saudi Arabia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Derbal</surname>
                           <given-names>Nora</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>Saudi charity and benevolence are often the subject of debate – usually in the context of allegedly supporting terrorist activities or fundamentalist campaigning for innocent minds in Third World countries. Saudi benevolence is said to have given support to election campaigns by Islamist parties in Tunisia and Egypt (in the latter case, sharply reversed later). Saudi charities are reported to have supported mujahideen fighters against the Soviet invasion of Afghanistan in the 1980s, and later to have helped to finance the Taliban in Afghanistan in the 1990s; Saudi charitable money was found to be at the heart of the “Golden Chain”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Saudi Arabia as a Global Humanitarian Donor</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Al-Yahya</surname>
                           <given-names>Khalid</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Fustier</surname>
                           <given-names>Nathalie</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>169</fpage>
                  <abstract>
                     <p>Saudi Arabia has recently emerged as the world’s largest donor of humanitarian assistance outside the Western donors, traditionally the members of OECD Development Assistance Committee (DAC). In many recent natural disasters, its contributions have exceeded those of any Western donor. In 2007, in response to Cyclone Sidr in Bangladesh, which killed more than 3,000 people and left millions homeless, Saudi Arabia gave Bangladesh $158 million, compared to only $20 million that came from the United States and an even lower contribution from the United Kingdom (Smith–Global Humanitarian Assistance (GHA) 2010). Following the Haiti earthquake in 2010, the kingdom made</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>A Good Day to Bury a Bad Charity:</title>
                     <subtitle>The Rise and Fall of the Al-Haramain Islamic Foundation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bokhari</surname>
                           <given-names>Yusra</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chowdhury</surname>
                           <given-names>Nasim</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lacey</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>Through the 1990s and into the early 21st century, the Al-Haramain Islamic Foundation (<italic>Mu’assasat al-Haramain al-Khayriyya</italic>) was the most pro-active Saudi charity operating in and around the trouble spots of the Islamic world. Under the flamboyant leadership of its founder, “Sheikh” Aqil Abdul-Aziz Al-Aqil, Al-Haramain presented itself as the Oxfam or United Way of Saudi Arabia, with collecting baskets and boxes outside almost every major Saudi mosque, and a reputation for delivering aid directly to sufferers in the most dangerous circumstances.</p>
                     <p>But on June 2nd, 2004 a joint US-Saudi press conference in Washington announced the closure of Al-Haramain, and an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>The Impact of US Laws, Regulations, and Policies on Gulf Charities</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Belew</surname>
                           <given-names>Wendell</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>231</fpage>
                  <abstract>
                     <p>This chapter will examine the impact of US laws, policies, and regulations on Gulf charities. These laws and policies developed in reaction to historical events and were justified by reference to, and based upon, certain factual assumptions. The events, of course, were the attacks on September 11th, 2001 as well as precursor attacks on the USS<italic>Cole</italic>and American embassies in Kenya and Tanzania. The 9/11 attacks – unlike those in Yemen and East Africa – had a significant impact on American culture, institutions, and leadership in ways that had few historical precedents.</p>
                     <p>The 9/11 attacks differed from events such as the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Qatar Authority for Charitable Activities (QACA) from Commencement to Dissolution (2004–2009)</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mohamed</surname>
                           <given-names>Abdul Fatah S.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>259</fpage>
                  <abstract>
                     <p>Historically,<italic>hisba</italic>or accountability in a number of forms has been an established practice in the Muslim World. However, in recent times, the more conservative Muslim societies have tended to adopt informal structures with a lack of accountability. The regulation of the charitable sector in the Gulf States is no exception. This had left charities for many years to function in a relatively informal manner, as used to be the case in Europe. The worldwide trend towards transparency and formal governance is now, however, irresistible. The evolution of the regulation of the charitable sector in Qatar reflects the global call</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Islamic Charities Project (formerly Montreux Initiative)</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Benthall</surname>
                           <given-names>Jonathan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>285</fpage>
                  <abstract>
                     <p>The proposal arose in the framework of a decision by the Swiss Federal Department of Foreign Affairs (FDFA), Political Division IV (PD IV) in 2004 to “make ‘religiopolitical conflicts’ a new sector of activity of its peace promotion activities; in other words conflicts where the mix of religious and political factors is a determining factor. In this sphere of activity there is a special, albeit not exclusive, emphasis on Islam and the Middle East.” FDFA/PD IV’s overall mission is promoting peace and security. Already in 2003–2004 it was engaged in various confidence-building endeavors. An important aspect of this approach</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>The Madrasas of South Asia and their implications for Gulf charities</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mulack</surname>
                           <given-names>Gunter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>307</fpage>
                  <abstract>
                     <p>The system of the madrasa, an institution of learning that was originally attached to a mosque, was probably introduced into the Indian subcontinent as early as the 12th century. Having become the backbone of Muslim education under the Moghul rule, the madrasa lost its importance when the British colonial rulers of India introduced secular and Christian school systems in the second half of the 19th century. But Gulf charities have played a significant role in its revival in the late 20th and early 21st centuries – and have also changed its character in a controversial fashion.</p>
                     <p>Traditional Islamic learning was systematized</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Madrasas in South Asia (India, Pakistan and Afghanistan):</title>
                     <subtitle>The Strategic Geopolitical Concern about Gulf Charities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Siddiqui</surname>
                           <given-names>Rushda</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>313</fpage>
                  <abstract>
                     <p>Religious institutions impart secular education and secular institutions impart religious education. Following September 11th, 2001, one of the first objects of intense scrutiny, arising from the imperative to trace the root causes of the attacks, was the nature of the linkage between the militants and extremist madrasas located in the “medieval outposts” of Afghanistan, Pakistan, and other fundamentalist states (Thobani 2010: 2). The rise of the Taliban (or students from madrasas in the North-West Frontier Province of Pakistan and the borders of Afghanistan), and their ability to form a group capable of taking over power and forming a government, made</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Giving to give, and giving to receive:</title>
                     <subtitle>The construction of charity in Dubai</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Parkhurst</surname>
                           <given-names>Aaron</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>335</fpage>
                  <abstract>
                     <p>“Charity creates a multitude of sins,” wrote Oscar Wilde (1891). The very concept of charity deserves our attention. Within Gulf Islam, the practice of giving money to charity is in some sense dogmatic. However, as with many seemingly simple social institutions, the complexity of relations that pervade the act of giving seep into domains outside religious obligation, and into political, economic, and other cultural categories that shape the anthropology of giving. This chapter attempts to speak to this complexity through brief ethnographic accounts of giving charity, or<italic>sadaqa,</italic>in Dubai and Abu Dhabi. A strong sentiment and discourse of benevolence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.19</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Care, Redemption and the Afterlife:</title>
                     <subtitle>Spiritual Experiences of Bathing Volunteers in a Charity Care Center in Iran</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hosoya</surname>
                           <given-names>Sachiko</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>353</fpage>
                  <abstract>
                     <p>Since the Islamic Revolution of 1979, the guarantee of national welfare has been regarded as one of the important national policies of Iran. The policy has emphasized the development of nationwide networks of medical services and education, and the improvement of social security systems, such as medical insurance and pension schemes along with the construction of an infrastructure from the center to the periphery (Khajehpour 2000; Madani 2000 (1379)).</p>
                     <p>Islamic concepts, on which charitable activities are based, have been reflected on a national scale in welfare projects and policies after the Islamic revolution. Since the latter part of the 1980s,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.20</book-part-id>
                  <title-group>
                     <label>16</label>
                     <title>Afterword:</title>
                     <subtitle>Capital, Migration, Intervention: Rethinking Gulf Islamic Charities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Li</surname>
                           <given-names>Darryl</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>375</fpage>
                  <abstract>
                     <p>The essays in this collection provide a counterpoint to the overheated and often misdirected public and policy debates over Islamic charities and “terrorism” that have attracted much attention in recent years. In their topical and methodological variety, they cast light in the manner of a prism scattering discrete and refracted rays over a darkened surface, gradually revealing its contours and colors. For the past decade or more, audiences in the West have demanded to know how much they should fear Islamic charities, especially from the Gulf Cooperation Council (GCC) member states. This loaded question has thrived on an imagined elision</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.21</book-part-id>
                  <title-group>
                     <title>Envoi:</title>
                     <subtitle>“… and beyond”</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lacey</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Benthall</surname>
                           <given-names>Jonathan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>387</fpage>
                  <abstract>
                     <p>This book does not purport to explain the complex geopolitical relations in which the Gulf states are currently embroiled; but as we go to press, we have to note two contemporary crises where legal and political constraints on humanitarian aid – including constraints on aid from the Gulf – have aggravated the sufferings of innocent people.</p>
                     <p>The first is in Somalia, where it has been estimated that nearly 5 per cent of the region’s population, and 10 per cent of its children, died because of severe food shortages in the eighteen months between October 2010 and April 2012 – some 258,000 more people</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.22</book-part-id>
                  <title-group>
                     <title>About the Contributors</title>
                  </title-group>
                  <fpage>391</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1df4hzq.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>397</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
