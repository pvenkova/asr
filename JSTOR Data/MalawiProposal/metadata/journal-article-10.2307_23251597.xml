<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">inteeconrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100187</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>International Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Periodicals</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00206598</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14682354</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23251597</article-id>
         <title-group>
            <article-title>GLOBAL BUSINESS CYCLES: CONVERGENCE OR DECOUPLING?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>M. Ayhan</given-names>
                  <surname>Kose</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Christopher</given-names>
                  <surname>Otrok</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Eswar</given-names>
                  <surname>Prasad</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">53</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23251489</issue-id>
         <fpage>511</fpage>
         <lpage>538</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 Economics Department of the University of Pennsylvania and the Osaka University Institute of Social and Economic Research Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23251597"/>
         <abstract>
            <p>We analyze the evolution of the degree of global cyclical interdependence over the period 1960—2008. Using a dynamic factor model, we decompose macroeconomic fluctuations in output, consumption, and investment into a global factor, factors specific to country groups, and country-specific factors. We find that during 1985—2008, there is some convergence of business cycle fluctuations among industrial economies and among emerging market economies. Surprisingly, there is a concomitant decline in the relative importance of the global factor. We conclude that there is evidence of business cycle convergence within each of these two groups of countries but divergence (or decoupling) between them.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d562e231a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d562e238" publication-type="other">
Mumtaz et al., 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e244" publication-type="other">
Flood and Rose, 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e250" publication-type="other">
Walti, 2009</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e257" publication-type="other">
Kim et al.,
2009</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e266" publication-type="other">
Fidrmuc and Korhonen, 2010</mixed-citation>
            </p>
         </fn>
         <fn id="d562e273a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d562e280" publication-type="other">
Kose et al. (2008b)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e286" publication-type="other">
Kose et al. (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e293a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d562e300" publication-type="other">
Blanchard
and Quah (1989)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e309" publication-type="other">
King et al. (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e316a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d562e323" publication-type="other">
Boivin and Giannoni's (2010)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e329" publication-type="other">
Reichlin
(2010)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e340a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d562e347" publication-type="other">
Claessens et al. (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e354a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d562e361" publication-type="other">
Stock and Watson (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e368a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d562e375" publication-type="other">
Artis et al. (2004).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e381" publication-type="other">
Canova et al. (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e388a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d562e395" publication-type="other">
Stock and Watson (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e402a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d562e409" publication-type="other">
Kose et al. (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e415" publication-type="other">
Gregory
et al. (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e424" publication-type="other">
Kose et al. (2008b)</mixed-citation>
            </p>
         </fn>
         <fn id="d562e431a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d562e438" publication-type="other">
Head's
(2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d562e447" publication-type="other">
Heathcote and Perri (2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d562e455a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d562e462" publication-type="other">
http://people.virginia.edu/~cmo3h/research2.html.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d562e478a1310">
            <mixed-citation id="d562e482" publication-type="other">
Aizenman, J., B. Pinto, and A. Radziwill, "Sources for Financing Domestic Capital—Is Foreign Saving
a Viable Option for Developing Countries?" Journal of International Money and Finance 26 (2007),
682-702.</mixed-citation>
         </ref>
         <ref id="d562e495a1310">
            <mixed-citation id="d562e499" publication-type="other">
Akin, C., and M. A. Kose, "Changing Nature of North-South Linkages: Stylized Facts and Explanations,"
Journal of Asian Economics 19 (2008), 1-28.</mixed-citation>
         </ref>
         <ref id="d562e509a1310">
            <mixed-citation id="d562e513" publication-type="other">
Altug, S., "Time-to-Build and Aggregate Fluctuations: Some New Evidence," International Economic
Review 30 (1989), 889-920.</mixed-citation>
         </ref>
         <ref id="d562e523a1310">
            <mixed-citation id="d562e527" publication-type="other">
Andrews, D., "Tests for Parameter Instability and Structural Change with Unknown Change Point,"
Econometrica 61 (1993), 821-56.</mixed-citation>
         </ref>
         <ref id="d562e538a1310">
            <mixed-citation id="d562e542" publication-type="other">
Artis, M. J., H. M. Krolzig, and J. Toro, "The European Business Cycle," Oxford Economic Papers 56
(2004), 1-44.</mixed-citation>
         </ref>
         <ref id="d562e552a1310">
            <mixed-citation id="d562e556" publication-type="other">
Backus, D. K., and M. J. Crucini, "Oil Prices and the Terms of Trade," Journal of International Economics
50 (2000), 203-31.</mixed-citation>
         </ref>
         <ref id="d562e566a1310">
            <mixed-citation id="d562e570" publication-type="other">
_, P. J. Kehoe, and F. E. Kydland, "International Business Cycles: Theory and Evidence," in T. F.
Cooley, ed., Frontiers of Business Cycle Research (Princeton, NJ: Princeton University Press, 1995),
331-57.</mixed-citation>
         </ref>
         <ref id="d562e583a1310">
            <mixed-citation id="d562e587" publication-type="other">
Bai, J., and S. Ng, "Determining the Number of Factors in Approximate Factor Models," Econometrica
70 (2002), 191-221.</mixed-citation>
         </ref>
         <ref id="d562e597a1310">
            <mixed-citation id="d562e601" publication-type="other">
Baxter, M., and M. Kouparitsas, "Determinants of Business Cycle Comovement: A Robust Analysis,"
Journal of Monetary Economics 52 (2005), 113-57.</mixed-citation>
         </ref>
         <ref id="d562e611a1310">
            <mixed-citation id="d562e615" publication-type="other">
Blanchard, O., and D. Quah, "The Dynamic Effects of Aggregate Demand and Supply Disturbances,"
American Economic Review 79 (1989), 655-73.</mixed-citation>
         </ref>
         <ref id="d562e626a1310">
            <mixed-citation id="d562e630" publication-type="other">
Boivin, J., and M. Giannoni, "Global Forces and Monetary Policy Effectiveness," in J. Gali and M.
J. Gertler, eds., International Dimensions of Monetary Policy (Chicago: The University of Chicago
Press, 2010), 429-78.</mixed-citation>
         </ref>
         <ref id="d562e643a1310">
            <mixed-citation id="d562e647" publication-type="other">
Burns, A. F., and W. C. Mitchell, Measuring Business Cycles (New York: National Bureau of Economic
Research, 1946).</mixed-citation>
         </ref>
         <ref id="d562e657a1310">
            <mixed-citation id="d562e661" publication-type="other">
Canova, F., M. Ciccarelli, and E. Ortega, "Similarities and Convergence in G-7 Cycles," Journal of
Monetary Economics 54 (2007), 850-78.</mixed-citation>
         </ref>
         <ref id="d562e671a1310">
            <mixed-citation id="d562e675" publication-type="other">
Claessens, S., and K. Forbes, "International Financial Contagion: An Overview of the Issues and the
Book," in S. Claessens and K. Forbes, eds., International Financial Contagion (Boston: Kluwer Aca-
demic Press, 2001).</mixed-citation>
         </ref>
         <ref id="d562e688a1310">
            <mixed-citation id="d562e692" publication-type="other">
_, M. A. Kose, and M. E. Terrones, "What Happens during Recessions, Crunches and Busts?"
Economic Policy 24 (2009), 653-700.</mixed-citation>
         </ref>
         <ref id="d562e702a1310">
            <mixed-citation id="d562e706" publication-type="other">
Crucini, M. J., M. A. Kose, and C. Otrok, "What Are the Driving Forces of International Business
Cycles?" Review of Economic Dynamics (2010), forthcoming.</mixed-citation>
         </ref>
         <ref id="d562e717a1310">
            <mixed-citation id="d562e721" publication-type="other">
Doz, C., D. Giannone, and L. Reichlin, "A Quasi Maximum Likelihood Approach for Large Approximate
Dynamic Factor Models," ECARES Working Papers 2008_034, Universite Libre de Bruxelles, 2008.</mixed-citation>
         </ref>
         <ref id="d562e731a1310">
            <mixed-citation id="d562e735" publication-type="other">
Fidrmuc, J., and I. Korhonen, "The Impact of the Global Financial Crisis on Business Cycles in Asian
Emerging Economies," Journal of Asian Economics 21 (2010), 293-303.</mixed-citation>
         </ref>
         <ref id="d562e745a1310">
            <mixed-citation id="d562e749" publication-type="other">
Flood, R. P., and A. K. Rose, "Inflation Targeting and Business Cycle Synchronization," Journal of
International Money and Finance 29 (2010), 704-27.</mixed-citation>
         </ref>
         <ref id="d562e759a1310">
            <mixed-citation id="d562e763" publication-type="other">
Forni, M., and L. Reichlin, "Let's Get Real: A Factor Analytical Approach to Disaggregated Business
Cycle Dynamics," Review of Economic Studies 65 (1998), 453-73.</mixed-citation>
         </ref>
         <ref id="d562e773a1310">
            <mixed-citation id="d562e777" publication-type="other">
_, M. Hallin, M. Lippi, and L. Reichlin, "The Generalized Dynamic-Factor Model: Identification and
Estimation," Review of Economics and Statistics 82 (2000), 540-54.</mixed-citation>
         </ref>
         <ref id="d562e787a1310">
            <mixed-citation id="d562e791" publication-type="other">
Frankel, J., and A. Rose, "The Endogeneity of the Optimum Currency Area Criteria," Economic Journal
108 (1998), 1009-25.</mixed-citation>
         </ref>
         <ref id="d562e802a1310">
            <mixed-citation id="d562e806" publication-type="other">
Giannone, D., L. Reichlin, and L. Sala, "VARs, Common Factors and the Empirical Validation of
Equilibrium Business Cycle Models," Journal of Econometrics 132 (2006), 257-79.</mixed-citation>
         </ref>
         <ref id="d562e816a1310">
            <mixed-citation id="d562e820" publication-type="other">
Gourinchas, P. O., and O. Jeanne, "Capital Flows to Developing Countries: The Allocation Puzzle,"
Peterson Institute Working Paper Series WP09-12, Peterson Institute for International Economics,
2009.</mixed-citation>
         </ref>
         <ref id="d562e833a1310">
            <mixed-citation id="d562e837" publication-type="other">
Gregory, A. W., A. Head, and J. Raynauld, "Measuring World Business Cycles," International Economic
Review 38 (1997), 677-702.</mixed-citation>
         </ref>
         <ref id="d562e847a1310">
            <mixed-citation id="d562e851" publication-type="other">
Head, A., "Aggregate Fluctuations with National and International Returns to Scale," International
Economic Review 43 (2002), 1101-25.</mixed-citation>
         </ref>
         <ref id="d562e861a1310">
            <mixed-citation id="d562e865" publication-type="other">
Heathcote, J., and F. Perri, "Financial Autarky and International Business Cycles," Journal of Monetary
Economics 49 (2002), 601-28.</mixed-citation>
         </ref>
         <ref id="d562e875a1310">
            <mixed-citation id="d562e879" publication-type="other">
_, and ——, "Financial Globalization and Real Regionalization," Journal of Economic Theory 119
(2004), 207-43.</mixed-citation>
         </ref>
         <ref id="d562e890a1310">
            <mixed-citation id="d562e894" publication-type="other">
Helbling, T., P. Berezin, M. A. Kose, M. Kumhof, D. Laxton, and N. Spatafora, "Decoupling the
Train? Spillovers and Cycles in the Global Economy," in The World Economic Outlook (IMF, 2007),
Chapter 4.</mixed-citation>
         </ref>
         <ref id="d562e907a1310">
            <mixed-citation id="d562e911" publication-type="other">
Imbs, J., "The Real Effects of Financial Integration," Journal of International Economics 68 (2006), 296-
324.</mixed-citation>
         </ref>
         <ref id="d562e921a1310">
            <mixed-citation id="d562e925" publication-type="other">
_, and R. Wacziarg, "Stages of Diversification," American Economic Review 93 (2003), 63-86.</mixed-citation>
         </ref>
         <ref id="d562e932a1310">
            <mixed-citation id="d562e936" publication-type="other">
Kalemli-Ozcan, S., B. E. Sorensen, and O. Yosha, "Economic Integration, Industrial Specialization,
and the Asymmetry of Macroeconomic Fluctuations," Journal of International Economics 55 (2001),
107-37.</mixed-citation>
         </ref>
         <ref id="d562e949a1310">
            <mixed-citation id="d562e953" publication-type="other">
Kim, S., J. W. Lee, and C. Y. Park, "Emerging Asia: Decoupling or Recoupling," Working Papers on
Regional Economic Integration 31, Asian Development Bank, 2009.</mixed-citation>
         </ref>
         <ref id="d562e963a1310">
            <mixed-citation id="d562e967" publication-type="other">
King, R., C. Plosser, J. Stock, and M. Watson, "Stochastic Trends and Economic Fluctuations," American
Economic Review 81 (1991), 819—40.</mixed-citation>
         </ref>
         <ref id="d562e978a1310">
            <mixed-citation id="d562e982" publication-type="other">
Kose, M. A., and K.-M. Yi, "International Trade and Business Cycles: Is Vertical Specialization the
Missing Link?" American Economic Review 91 (2001), 371-75.</mixed-citation>
         </ref>
         <ref id="d562e992a1310">
            <mixed-citation id="d562e996" publication-type="other">
_, and, "Can the Standard International Business Cycle Model Explain the Relation Between
Trade and Comovement?" Journal of International Economics 68 (2006), 267-95.</mixed-citation>
         </ref>
         <ref id="d562e1006a1310">
            <mixed-citation id="d562e1010" publication-type="other">
_, C. Otrok, and E. Prasad, "Global Business Cycles: Convergence or Decoupling?" NBER Working
Paper 14292, 2008a.</mixed-citation>
         </ref>
         <ref id="d562e1020a1310">
            <mixed-citation id="d562e1024" publication-type="other">
_,, and C. Whiteman, "International Business Cycles: World, Region, and Country Specific
Factors," American Economic Review 93 (2003), 1216-39.</mixed-citation>
         </ref>
         <ref id="d562e1034a1310">
            <mixed-citation id="d562e1038" publication-type="other">
_,, and, "Understanding the Evolution of World Business Cycles," Journal of International
Economics 75 (2008b), 110-30.</mixed-citation>
         </ref>
         <ref id="d562e1048a1310">
            <mixed-citation id="d562e1052" publication-type="other">
_——•,, and, "Does Financial Globalization Promote Risk Sharing?" Journal of Development
Economics 89 (2009), 258-70.</mixed-citation>
         </ref>
         <ref id="d562e1063a1310">
            <mixed-citation id="d562e1067" publication-type="other">
Krugman, P., "Lessons from Massachusetts for EMU," in F. Torres and F. Giavazzi, eds., Adjustment and
Growth in the European Monetary Union London (Cambridge, UK: CEPR and Cambridge University
Press, 1993).</mixed-citation>
         </ref>
         <ref id="d562e1080a1310">
            <mixed-citation id="d562e1084" publication-type="other">
Lane, P., and G. M. Milesi-Ferretti, "The External Wealth of Nations Mark II: Revised and Extended
Estimates of Foreign Assets and Liabilities: 1970-2004, "Journal of International Economics 73 (2007),
223-50.</mixed-citation>
         </ref>
         <ref id="d562e1097a1310">
            <mixed-citation id="d562e1101" publication-type="other">
Mcconnell, M., and G. Perez-Quiros, "Output Fluctuations in the United States: What Has Changed
Since the Early 1980s?" American Economic Review 90 (2000), 1464-76.</mixed-citation>
         </ref>
         <ref id="d562e1111a1310">
            <mixed-citation id="d562e1115" publication-type="other">
Mumtaz, H., S. Simonelli, and P. Surico, "International Comovements, Business Cycle and Inflation: A
Historical Perspective," Review of Economic Dynamics (2010), forthcoming.</mixed-citation>
         </ref>
         <ref id="d562e1125a1310">
            <mixed-citation id="d562e1129" publication-type="other">
Otrok, C., and C. Whiteman, "Bayesian Leading Indicators: Measuring and Predicting Economic Con-
ditions in Iowa," International Economic Review 39 (1998), 997-1014.</mixed-citation>
         </ref>
         <ref id="d562e1139a1310">
            <mixed-citation id="d562e1143" publication-type="other">
Prasad, E., R. Rajan, and A. Subramanian, "Foreign Capital and Economic Growth," Brookings Papers
on Economic Activity 1 (2007), 153-230.</mixed-citation>
         </ref>
         <ref id="d562e1154a1310">
            <mixed-citation id="d562e1158" publication-type="other">
_, K. Rogoff, S. J. Wei, and M. A. Kose, "Effects of Financial Globalization on Developing Countries:
Some Empirical Evidence," IMF Occasional Paper 220, 2003.</mixed-citation>
         </ref>
         <ref id="d562e1168a1310">
            <mixed-citation id="d562e1172" publication-type="other">
Reichlin, L., "Comment on Global Forces and Monetary Policy Effectiveness." in J. Gali and M. J. Gertler,
eds., International Dimensions of Monetary Policy (Chicago: The University of Chicago Press, 2010),
478-88.</mixed-citation>
         </ref>
         <ref id="d562e1185a1310">
            <mixed-citation id="d562e1189" publication-type="other">
Sargent, T. J., "Two Models of Measurements and the Investment Accelerator," Journal of Political
Economy 97 (1989), 251-87.</mixed-citation>
         </ref>
         <ref id="d562e1199a1310">
            <mixed-citation id="d562e1203" publication-type="other">
_, and C. A. Sims, "Business Cycle Modeling without Pretending to Have Too Much a Priori Economic
Theory," in C. A. Sims, ed., New Methods in Business Cycle Research (Minneapolis: Federal Reserve
Bank of Minneapolis, 1977).</mixed-citation>
         </ref>
         <ref id="d562e1216a1310">
            <mixed-citation id="d562e1220" publication-type="other">
Sorensen, B. E., Y. T. Wu, and Y. Zhu, "Home Bias and International Risk Sharing: Twin Puzzles
Separated at Birth," Journal of International Money and Finance 26 (2007), 587-605.</mixed-citation>
         </ref>
         <ref id="d562e1230a1310">
            <mixed-citation id="d562e1234" publication-type="other">
Stock, J. H., and M. W. Watson, "New Indexes of Coincident and Leading Economic Indicators," in O.
J. Blanchard and S. Fischer, eds., NBER Macroeconomics Annual 1989 Volume 4 (Cambridge, MA:
The MIT Press, 1989), 351-409.</mixed-citation>
         </ref>
         <ref id="d562e1248a1310">
            <mixed-citation id="d562e1252" publication-type="other">
_, and, "Macroeconomic Forecasting Using Diffusion Indexes," Journal of Business and Economic
Statistics 20 (2002), 147-62.</mixed-citation>
         </ref>
         <ref id="d562e1262a1310">
            <mixed-citation id="d562e1266" publication-type="other">
_, and, "Understanding Changes in International Business Cycles," Journal of the European
Economic Association 3 (2005), 968-1006.</mixed-citation>
         </ref>
         <ref id="d562e1276a1310">
            <mixed-citation id="d562e1280" publication-type="other">
_, and, "Forecasting in Dynamic Factor Models Subject to Structural Instability," in J. Castle and
N. Shephard, eds., The Methodology and Practice of Econometrics (Oxford: Oxford University Press,
2009).</mixed-citation>
         </ref>
         <ref id="d562e1293a1310">
            <mixed-citation id="d562e1297" publication-type="other">
Walti, S., "The Myth of Decoupling," MPRA Paper 20870, University Library of Munich, 2009.</mixed-citation>
         </ref>
         <ref id="d562e1304a1310">
            <mixed-citation id="d562e1308" publication-type="other">
Zarnowitz, V., Business Cycles: Theory, History, Indicators, and Forecasting, NBER Studies in Business
Cycles, Volume 27 (Chicago: University of Chicago Press, 1992).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
