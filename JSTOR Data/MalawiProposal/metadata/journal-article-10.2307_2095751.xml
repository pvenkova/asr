<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">amersocirevi</journal-id>
         <journal-id journal-id-type="jstor">j100080</journal-id>
         <journal-title-group>
            <journal-title>American Sociological Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Sociological Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">00031224</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2095751</article-id>
         <title-group>
            <article-title> Explaining Military Coups D'État: Black Africa, 1957-1984 </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. Craig</given-names>
                  <surname>Jenkins</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Augustine J.</given-names>
                  <surname>Kposowa</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1990</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">55</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id>i336580</issue-id>
         <fpage>861</fpage>
         <lpage>875</lpage>
         <page-range>861-875</page-range>
         <permissions>
            <copyright-statement>Copyright 1990 American Sociological Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2095751"/>
         <abstract>
            <p>Military coups and related problems of political control in Third World countries present a major obstacle to economic and social development. We evaluate a synthetic theory of military coups derived from political development theory, military centrality arguments, several theories of ethnic antagonism, and economic dependency theory. Using data on military interventions in 33 Black African states between 1957 and 1984, we carry out a LISREL analysis of the structural propensity for military coups. We find strong support for modernization and competition theories of ethnic antagonisms, military centrality theory and aspects of dependency theory. Political development theory is not supported. Ethnic diversity and competition, military centrality, debt dependence, and political factionalism are major predictors of coup activity. Military centrality is, in turn, rooted in the same underlying structures. Ethnic dominance is a stabilizing force creating social integration and weakening opposition. Intractable conflicts rooted in ethnic competition and economic dependence appear to create a structural context for military coups and related instabilities.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1408e147a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1408e154" publication-type="book">
Taylor and Jodice (1983)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1408e162" publication-type="journal">
Johnson, Slater, and
McGowan (1984, p. 627)  <fpage>627</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1408e177a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1408e184" publication-type="book">
Zimmermann (1983, p. 283-86)  <fpage>283</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1408e196a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1408e203" publication-type="book">
Morrison et al. 1972, pp. 102-3  <fpage>102</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1408e215a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1408e222" publication-type="book">
Hoffman (1989)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1408e240a1310">
            <mixed-citation id="d1408e244" publication-type="book">
Andreski, Stanislav. 1968. Military Organization and
Society (2nd ed). Berkeley: University of California
Press.<person-group>
                  <string-name>
                     <surname>Andreski</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Military Organization and Society</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e276a1310">
            <mixed-citation id="d1408e280" publication-type="journal">
Barrows, W. L. 1976. "Ethnic Diversity and Political
Instability in Black Africa." Comparative Political
Studies9:139-70.<person-group>
                  <string-name>
                     <surname>Barrows</surname>
                  </string-name>
               </person-group>
               <fpage>139</fpage>
               <volume>9</volume>
               <source>Comparative Political Studies</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e315a1310">
            <mixed-citation id="d1408e319" publication-type="journal">
Bates, Robert H. 1974. "Ethnic Competition and
Modernization in Contemporary Africa." Compar-
ative Political Studies6:457-84.<person-group>
                  <string-name>
                     <surname>Bates</surname>
                  </string-name>
               </person-group>
               <fpage>457</fpage>
               <volume>6</volume>
               <source>Comparative Political Studies</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e354a1310">
            <mixed-citation id="d1408e358" publication-type="book">
—1983. "Modernization, Ethnic Competi-
tion and the Rationality of Politics in Contemporary
Africa." Pp. 152-71 in State Versus Ethnic Claims,
edited by D. Rothchild and V. A. Olorundsola.
Boulder: Westview.<person-group>
                  <string-name>
                     <surname>Bates</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Modernization, Ethnic Competition and the Rationality of Politics in Contemporary Africa</comment>
               <fpage>152</fpage>
               <source>State Versus Ethnic Claims</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e400a1310">
            <mixed-citation id="d1408e404" publication-type="book">
Bornschier, V. and C. Chase-Dunn. 1985. Transna-
tional Corporations and Underdevelopment. New
York: Praeger.<person-group>
                  <string-name>
                     <surname>Bornschier</surname>
                  </string-name>
               </person-group>
               <source>Transnational Corporations and Underdevelopment</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e433a1310">
            <mixed-citation id="d1408e437" publication-type="journal">
Bradshaw, York. 1985. "Dependent Development in
Black Africa." American Sociological Review
50:195?207.<object-id pub-id-type="doi">10.2307/2095409</object-id>
               <fpage>195</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e456a1310">
            <mixed-citation id="d1408e460" publication-type="journal">
Bradshaw, York and Z. Tshandu. 1990. "Foreign
Capital Penetration, State Intervention and Devel-
opment in Sub-Saharan Africa." International
Studies Quarterly34:229-5 1.<object-id pub-id-type="doi">10.2307/2600710</object-id>
               <fpage>229</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e483a1310">
            <mixed-citation id="d1408e487" publication-type="book">
Brass, Paul. 1985. "Ethnic Groups and the State." Pp.
1-57 in Ethnic Groups and the State, edited by P.
Brass. Totowa, NJ: Barnes &amp; Noble.<person-group>
                  <string-name>
                     <surname>Brass</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ethnic Groups and the State</comment>
               <fpage>1</fpage>
               <source>Ethnic Groups and the State</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e522a1310">
            <mixed-citation id="d1408e526" publication-type="book">
Chazan, Naomi, Robert Mortimer, John Ravenhill,
and Donald Rothchild. 1988. Politics and Society in
Contemporary Africa. Boulder: Lynn Rienner.<person-group>
                  <string-name>
                     <surname>Chazan</surname>
                  </string-name>
               </person-group>
               <source>Politics and Society in Contemporary Africa</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e555a1310">
            <mixed-citation id="d1408e559" publication-type="book">
Collier, Ruth. 1983. Regimes in Tropical Africa.
Berkeley: University of California Press.<person-group>
                  <string-name>
                     <surname>Collier</surname>
                  </string-name>
               </person-group>
               <source>Regimes in Tropical Africa</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e585a1310">
            <mixed-citation id="d1408e589" publication-type="journal">
Delacroix, Jacques and Charles Ragin. 1981. "Struc-
tural Blockage." American Journal of Sociology
86:1311-47.<object-id pub-id-type="jstor">10.2307/2778817</object-id>
               <fpage>1311</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e608a1310">
            <mixed-citation id="d1408e612" publication-type="book">
Deutsch, Karl. 1969. Nationalism and Its Alternatives.
New York: Knopf.<person-group>
                  <string-name>
                     <surname>Deutsch</surname>
                  </string-name>
               </person-group>
               <source>Nationalism and Its Alternatives</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e637a1310">
            <mixed-citation id="d1408e641" publication-type="book">
Enloe, Cynthia. 1980. Ethnic Soldiers. Athens, GA:
University of Georgia Press.<person-group>
                  <string-name>
                     <surname>Enloe</surname>
                  </string-name>
               </person-group>
               <source>Ethnic Soldiers</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e666a1310">
            <mixed-citation id="d1408e670" publication-type="book">
Finer, S. E. 1988. The Man on Horseback (2nd ed.).
Boulder: Westview.<person-group>
                  <string-name>
                     <surname>Finer</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>The Man on Horseback</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e699a1310">
            <mixed-citation id="d1408e703" publication-type="book">
Gurr, Ted R. 1989. "Protest and Rebellion in the
1960s." Pp. 101-30 in Violence in America, edited
by T. R. Gurr. Newbury Park: Sage.<person-group>
                  <string-name>
                     <surname>Gurr</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Protest and Rebellion in the 1960s</comment>
               <fpage>101</fpage>
               <source>Violence in America</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e738a1310">
            <mixed-citation id="d1408e742" publication-type="book">
Hoffman, Mark F. 1989. World Almanac and Book of
Facts. New York: Pharos Books.<person-group>
                  <string-name>
                     <surname>Hoffman</surname>
                  </string-name>
               </person-group>
               <source>World Almanac and Book of Facts</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e768a1310">
            <mixed-citation id="d1408e772" publication-type="book">
Horowitz, DonaldL. 1985. Ethnic Groups in Conflict.
Berkeley: University of California Press.<person-group>
                  <string-name>
                     <surname>Horowitz</surname>
                  </string-name>
               </person-group>
               <source>Ethnic Groups in Conflict</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e797a1310">
            <mixed-citation id="d1408e801" publication-type="book">
Huntington, Samuel P. 1968. Political Order in
Changing Societies. New Haven: Yale University
Press.<person-group>
                  <string-name>
                     <surname>Huntington</surname>
                  </string-name>
               </person-group>
               <source>Political Order in Changing Societies</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e830a1310">
            <mixed-citation id="d1408e834" publication-type="journal">
Jackman, Robert. 1978. "The Predictability of Coups
d'Etat."American Political Science Review72:1262-
75.<object-id pub-id-type="doi">10.2307/1954538</object-id>
               <fpage>1262</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e853a1310">
            <mixed-citation id="d1408e857" publication-type="other">
Jenkins, J. Craig and Augustine Kposowa. 1990. Elite
Instability and Military Coups in Black Africa: An
Assessment of Alternative Measures. Unpublished
paper, The Ohio State University, Dept. of Sociol-
ogy.</mixed-citation>
         </ref>
         <ref id="d1408e876a1310">
            <mixed-citation id="d1408e880" publication-type="journal">
Johnson, T. H., R. 0. Slater, and P. McGowan. 1984.
"Explaining African Military Coups d'Etat."
American Political Science Review78:622-40.<object-id pub-id-type="doi">10.2307/1961833</object-id>
               <fpage>622</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e899a1310">
            <mixed-citation id="d1408e903" publication-type="book">
Joreskog, Karl and Dag Sorbom. 1983. LISREL:
Analysis of Linear Structural Relations by the Meth-
od of Maximum Likelihood (Version VI). Chicago:
National Educational Services.<person-group>
                  <string-name>
                     <surname>Joreskog</surname>
                  </string-name>
               </person-group>
               <source>LISREL: Analysis of Linear Structural Relations by the Method of Maximum Likelihood (Version VI)</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e936a1310">
            <mixed-citation id="d1408e940" publication-type="book">
Kasfir, Nelson. 1976. The Shrinking Political Arena.
Berkeley: University of California Press.<person-group>
                  <string-name>
                     <surname>Kasfir</surname>
                  </string-name>
               </person-group>
               <source>The Shrinking Political Arena</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e965a1310">
            <mixed-citation id="d1408e969" publication-type="book">
Kennedy,Gavin. 1974. The Military in the Third World.
London: Duckworth.<person-group>
                  <string-name>
                     <surname>Kennedy</surname>
                  </string-name>
               </person-group>
               <source>The Military in the Third World</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e994a1310">
            <mixed-citation id="d1408e998" publication-type="journal">
Korpi, Walter. 1974. "Conflict, Power and Relative
Deprivation." American Sociological Review
68:1569-78.<person-group>
                  <string-name>
                     <surname>Korpi</surname>
                  </string-name>
               </person-group>
               <fpage>1569</fpage>
               <volume>68</volume>
               <source>American Sociological Review</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1033a1310">
            <mixed-citation id="d1408e1037" publication-type="journal">
Lipjhart, Arendt. 1975. "The Comparable Cases Strat-
egy in Comparative Research." Comparative Po-
litical Studies8:158-77.<person-group>
                  <string-name>
                     <surname>Lipjhart</surname>
                  </string-name>
               </person-group>
               <fpage>158</fpage>
               <volume>8</volume>
               <source>Comparative Political Studies</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1072a1310">
            <mixed-citation id="d1408e1076" publication-type="book">
Mazrui, Ali and Michael Tidy. 1984. Nationalism and
New States in Africa. London: Heinemann.<person-group>
                  <string-name>
                     <surname>Mazrui</surname>
                  </string-name>
               </person-group>
               <source>Nationalism and New States in Africa</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1101a1310">
            <mixed-citation id="d1408e1105" publication-type="book">
McGowan, Pat. 1975. "Predicting Instability in Trop-
ical Africa." Pp. 35-78 in Quantitative Analysis in
Foreign Policy and Forecasting, edited by M. K.
O'Leary and W. D. Coplin. New York: Praeger.<person-group>
                  <string-name>
                     <surname>McGowan</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Predicting Instability in Tropical Africa</comment>
               <fpage>35</fpage>
               <source>Quantitative Analysis in Foreign Policy and Forecasting</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1144a1310">
            <mixed-citation id="d1408e1148" publication-type="journal">
McGowan, Pat and Thomas H. Johnson. 1986. "Sixty
Coups in Thirty Years." Journal of Modern African
Studies24:539-46.<object-id pub-id-type="jstor">10.2307/160357</object-id>
               <fpage>539</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1167a1310">
            <mixed-citation id="d1408e1171" publication-type="journal">
McGowan, Pat and D. Smith. 1978. "Economic De-
pendency in Black Africa." International Organi-
zation32:179-235.<object-id pub-id-type="jstor">10.2307/2706199</object-id>
               <fpage>179</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1190a1310">
            <mixed-citation id="d1408e1194" publication-type="journal">
Melson, R. and H. Wolpe. 1970. "Modernization and
the Politics of Communalism." American Political
Science Review64:1112-130.<object-id pub-id-type="doi">10.2307/1958361</object-id>
               <fpage>1112</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1213a1310">
            <mixed-citation id="d1408e1217" publication-type="book">
Migdal, Joel. 1988. Strong Societies and Weak States.
Princeton: Princeton University Press.<person-group>
                  <string-name>
                     <surname>Migdal</surname>
                  </string-name>
               </person-group>
               <source>Strong Societies and Weak States</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1242a1310">
            <mixed-citation id="d1408e1246" publication-type="journal">
Morrison, Donald G. and H. M. Stevenson. 1972a.
"Integration and Instability." American Political
Science Review66:902-27.<object-id pub-id-type="doi">10.2307/1957486</object-id>
               <fpage>902</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1265a1310">
            <mixed-citation id="d1408e1269" publication-type="journal">
—1972b. "Cultural Pluralism, Moderniza-
tion and Conflict." Canadian Journal of Political
Science5:82-103.<person-group>
                  <string-name>
                     <surname>Morrison</surname>
                  </string-name>
               </person-group>
               <fpage>82</fpage>
               <volume>5</volume>
               <source>Canadian Journal of Political Science</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1305a1310">
            <mixed-citation id="d1408e1309" publication-type="book">
Morrison, Donald, R. C. Mitchell, J. N. Paden, and H.
M. Stevenson (Eds.). 1972. Black Africa: A Com-
parative Handbook. New York: Free Press.<person-group>
                  <string-name>
                     <surname>Morrison</surname>
                  </string-name>
               </person-group>
               <source>Black Africa: A Comparative Handbook</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1338a1310">
            <mixed-citation id="d1408e1342" publication-type="book">
—1990. Black Africa: A Comparative Hand-
book (2nd ed.). Irvington, NY: Irvington Press.<person-group>
                  <string-name>
                     <surname>Morrison</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Black Africa: A Comparative Handbook</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1371a1310">
            <mixed-citation id="d1408e1375" publication-type="book">
Muller, Edward N. and Ekkart Zimmermann. 1987.
"United States Military Aid and Coups d'État in the
Third World." Pp. 197-218 in America's Changing
Role in the World-System, edited by T. Boswell and
A. Bergesen. New York: Praeger.<person-group>
                  <string-name>
                     <surname>Muller</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">United States Military Aid and Coups d'État in the Third World</comment>
               <fpage>197</fpage>
               <source>America's Changing Role in the World-System</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1416a1310">
            <mixed-citation id="d1408e1420" publication-type="book">
Mullins, A. F. 1987. Born Arming. New York: Prae-
ger.<person-group>
                  <string-name>
                     <surname>Mullins</surname>
                  </string-name>
               </person-group>
               <source>Born Arming</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1445a1310">
            <mixed-citation id="d1408e1449" publication-type="book">
Myrdal, Gunnar. 1968. Asian Drama. New York:
Pantheon.<person-group>
                  <string-name>
                     <surname>Myrdal</surname>
                  </string-name>
               </person-group>
               <source>Asian Drama</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1474a1310">
            <mixed-citation id="d1408e1478" publication-type="book">
Nordlinger, Eric A. 1977. Soldiers in Politics. Engle-
wood Cliffs, NJ: Prentice-Hall.<person-group>
                  <string-name>
                     <surname>Nordlinger</surname>
                  </string-name>
               </person-group>
               <source>Soldiers in Politics</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1504a1310">
            <mixed-citation id="d1408e1508" publication-type="book">
O'Donnell, Guillermo. 1979. "Tensions in the Bu-
reaucratic-Authoritarian State and the Question of
Democracy." Pp. 291-323 in The New Authoritari-
anism in Latin America, edited by D. Collier. Prin-
ceton: Princeton University Press.<person-group>
                  <string-name>
                     <surname>O'Donnell</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Tensions in the Bureaucratic-Authoritarian State and the Question of Democracy</comment>
               <fpage>291</fpage>
               <source>The New Authoritarianism in Latin America</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1549a1310">
            <mixed-citation id="d1408e1553" publication-type="journal">
O'Kane, Rosemary H. L. 1981. "A Probabilistic Ap-
proach to the Causes of Coups d'Etat." British
Journal of Political Science11:287-308.<object-id pub-id-type="jstor">10.2307/193491</object-id>
               <fpage>287</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1572a1310">
            <mixed-citation id="d1408e1576" publication-type="journal">
Olzak, Susan. 1983. "Contemporary Ethnic Mobiliza-
tion." Annual Review of Sociology9:355-74.<object-id pub-id-type="jstor">10.2307/2946070</object-id>
               <fpage>355</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1592a1310">
            <mixed-citation id="d1408e1596" publication-type="book">
Olzak, Susan and Joane Nagel. 1986. "Competitive
Ethnic Relations." Pp. 1-14 in Competitive Ethnic
Relations, edited by S. Olzak and J. Nagel. New
York: Academic.<person-group>
                  <string-name>
                     <surname>Olzak</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Competitive Ethnic Relations</comment>
               <fpage>1</fpage>
               <source>Competitive Ethnic Relations</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1634a1310">
            <mixed-citation id="d1408e1638" publication-type="book">
Paige, Jeffrey. 1975. Agrarian Revolution. New York:
Free Press.<person-group>
                  <string-name>
                     <surname>Paige</surname>
                  </string-name>
               </person-group>
               <source>Agrarian Revolution</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1663a1310">
            <mixed-citation id="d1408e1667" publication-type="book">
See, Kathleen O'Sullivan and William J. Wilson.
1988. "Race and Ethnicity." Pp. 223-242 in Hand-
book of Sociology, edited by Neil Smelser. New-
bury Park: Sage.<person-group>
                  <string-name>
                     <surname>See</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Race and Ethnicity</comment>
               <fpage>223</fpage>
               <source>Handbook of Sociology</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1706a1310">
            <mixed-citation id="d1408e1710" publication-type="book">
Sivard, Ruth. 1983. World Military and Social Expen-
ditures, 1983. Washington, D.C.: World Priorities.<person-group>
                  <string-name>
                     <surname>Sivard</surname>
                  </string-name>
               </person-group>
               <source>World Military and Social Expenditures, 1983</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1735a1310">
            <mixed-citation id="d1408e1739" publication-type="book">
Smaldone, Joseph. 1974. "The Paradox of Military
Politics in Sub-Saharan Africa." Pp. 201-27 in Civil-
Military Relations, edited by Charles Cochran. New
York: Free Press.<person-group>
                  <string-name>
                     <surname>Smaldone</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Paradox of Military Politics in Sub-Saharan Africa</comment>
               <fpage>201</fpage>
               <source>Civil-Military Relations</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1777a1310">
            <mixed-citation id="d1408e1781" publication-type="book">
Smelser, Neil. 1968. Essays in Sociological Explana-
tion. Englewood Cliffs: Prentice-Hall.<person-group>
                  <string-name>
                     <surname>Smelser</surname>
                  </string-name>
               </person-group>
               <source>Essays in Sociological Explanation</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1806a1310">
            <mixed-citation id="d1408e1810" publication-type="book">
Taylor, Charles Lewis and David A. Jodice. 1983.
World Handbook of Political and Social Indicators.
New Haven: Yale University Press.<person-group>
                  <string-name>
                     <surname>Taylor</surname>
                  </string-name>
               </person-group>
               <source>World Handbook of Political and Social Indicators</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1839a1310">
            <mixed-citation id="d1408e1843" publication-type="book">
Thomas, Clive Y. 1984. The Rise of the Authoritarian
State in Peripheral Societies. New York: Monthly
Review.<person-group>
                  <string-name>
                     <surname>Thomas</surname>
                  </string-name>
               </person-group>
               <source>The Rise of the Authoritarian State in Peripheral Societies</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1872a1310">
            <mixed-citation id="d1408e1876" publication-type="book">
Thompson, William R. 1973. The Grievances of Mil-
itary Coup-Makers. Beverly Hills: Sage.<person-group>
                  <string-name>
                     <surname>Thompson</surname>
                  </string-name>
               </person-group>
               <source>The Grievances of Military Coup-Makers</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1902a1310">
            <mixed-citation id="d1408e1906" publication-type="journal">
—1980. "Corporate Coup-Maker Grievances
and Types of Regimes Targets." Comparative Po-
litical Studies12:485-96.<person-group>
                  <string-name>
                     <surname>Thompson</surname>
                  </string-name>
               </person-group>
               <fpage>485</fpage>
               <volume>12</volume>
               <source>Comparative Political Studies</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e1941a1310">
            <mixed-citation id="d1408e1945" publication-type="journal">
Timberlake, Michael and Kirk Williams. 1984. "Eco-
nomic Dependence, Political Exclusion and Gov-
ernment Repression." American Sociological Re-
view49:141-46.<object-id pub-id-type="doi">10.2307/2095563</object-id>
               <fpage>141</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e1968a1310">
            <mixed-citation id="d1408e1972" publication-type="book">
United Nations Commission on Trade and Develop-
ment. 1986. UNCTAD Commodity Yearbook. New
York: United Nations.<person-group>
                  <string-name>
                     <surname>United Nations Commission on Trade and Development</surname>
                  </string-name>
               </person-group>
               <source>UNCTAD Commodity Yearbook</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2001a1310">
            <mixed-citation id="d1408e2005" publication-type="book">
Welch, Claude E. and A. K. Smith. 1974. Military Role
and Rule. North Scituate, MA: Duxbury Press.<person-group>
                  <string-name>
                     <surname>Welch</surname>
                  </string-name>
               </person-group>
               <source>Military Role and Rule</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2030a1310">
            <mixed-citation id="d1408e2034" publication-type="journal">
Wells, David. 1974. "The Coup d'Etat in Theory and
Practice." American Journal of Sociology79:871-
87.<object-id pub-id-type="jstor">10.2307/2776347</object-id>
               <fpage>871</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1408e2053a1310">
            <mixed-citation id="d1408e2057" publication-type="journal">
Wells, David and Richard Pollnac. 1988. "The Coup
d'Etat in Sub-Saharan Africa."Journal of Political
and Military Sociology16:43-56.<person-group>
                  <string-name>
                     <surname>Wells</surname>
                  </string-name>
               </person-group>
               <fpage>43</fpage>
               <volume>16</volume>
               <source>Journal of Political and Military Sociology</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2093a1310">
            <mixed-citation id="d1408e2097" publication-type="book">
World Bank. 1985. World Tables. Baltimore, MD:
Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Tables</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2122a1310">
            <mixed-citation id="d1408e2126" publication-type="book">
Young, Crawford. 1976. The Politics of Cultural Plu-
ralism. Madison: University of Wisconsin Press.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <source>The Politics of Cultural Pluralism</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2151a1310">
            <mixed-citation id="d1408e2155" publication-type="book">
—1988. "The African Colonial State and its
Political Legacy." Pp. 25-66 in The Precarious
Balance, edited by D. Rothchild and N. Chazan.
Boulder, CO: Westview.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The African Colonial State and its Political Legacy</comment>
               <fpage>25</fpage>
               <source>The Precarious Balance</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2193a1310">
            <mixed-citation id="d1408e2197" publication-type="journal">
Zimmermann, Ekkart. 1979. "Toward a Causal Model
of Military Coups d'Etat." Armed Forces and Soci-
ety5:387-413.<person-group>
                  <string-name>
                     <surname>Zimmermann</surname>
                  </string-name>
               </person-group>
               <fpage>387</fpage>
               <volume>5</volume>
               <source>Armed Forces and Society</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1408e2232a1310">
            <mixed-citation id="d1408e2236" publication-type="book">
—1983. Political Violence, Crises and Revo-
lutions. Cambridge: Schenkman.<person-group>
                  <string-name>
                     <surname>Zimmermann</surname>
                  </string-name>
               </person-group>
               <source>Political Violence, Crises and Revolutions</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
