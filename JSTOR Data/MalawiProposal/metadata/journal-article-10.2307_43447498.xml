<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">englliterena</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50012122</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>English Literary Renaissance</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Department of English, University of Massachusetts</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00138312</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14756757</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43447498</article-id>
         <title-group>
            <article-title>Cyberspace Renaissance</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>LEAH S.</given-names>
                  <surname>MARCUS</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>1995</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">25</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40136282</issue-id>
         <fpage>388</fpage>
         <lpage>401</lpage>
         <permissions>
            <copyright-statement>copyright © 1995 English Literary Renaissance</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43447498"/>
         <abstract>
            <p>Those who process "the Renaissance" have a peculiarly strong investment in the changes wrought by computerization because they appear to be creating a new, neutral space within which we can reengage our fascination with ideas of intellectual rebirth and renewal without appearing to condone the originary violence that so often accompanied the new ideas in that historicalera. With the advent of computerization, the book and the handwritten manuscript are becoming endangered species as well as subjects for revitalized investigation. Recent and emerging scholarship has developed a new sensitivity to the book as physical object, to the impact of print upon predominantly oral elements of early modern culture, and to the continuing importance of manuscript circulation during the period despite the increasing dominance of print. The onset of computerization in the discipline has helped bring about a decline in recent agonistic interpretive models in favor of the model of the network; it has also, arguably, increased interest in playful, performative critical modes as opposed to an older hermeneutics of suspicion; it has and will continue to alter the discipline in ways that cannot presently be imagined.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d43e116a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d43e123" publication-type="other">
The Internet Navigator by Paul Gilster (New York, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d43e130a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d43e137" publication-type="other">
"Renaissance / Early Modern Studies," in Redrawing the Boundaries: The Transformation of
English and American Literary Studies, ed. Greenblatt and Gunn (New York, 1992), pp. 41-68.</mixed-citation>
            </p>
         </fn>
         <fn id="d43e147a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d43e154" publication-type="other">
Paul Henry Saenger, A Catalogue of the Pre-1500 Western Manuscript Books
at the Newberry Library (Chicago, 1989).</mixed-citation>
            </p>
         </fn>
         <fn id="d43e164a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d43e171" publication-type="other">
Anne Barton and John Kerrigan (Cambridge, Eng., 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d43e179a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d43e186" publication-type="other">
Evelyn B. Tribble, Margins and Marginality: The Printed Page in Early
Modem England (Charlottesville, 1993);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e195" publication-type="other">
Kevin Dunn, Pretexts of Authority: The Rhetoric of
Authorship in the Renaissance Preface (Stanford, 1994);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e204" publication-type="other">
Tessa Watt, Cheap Print and Popular Piety
1550-1640 (Cambridge, Eng., 1991);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e214" publication-type="other">
Roger Chartier, The Order of Books, trans. Lydia G.
Cochrane (1992; English edition, Stanford, 1994).</mixed-citation>
            </p>
         </fn>
         <fn id="d43e224a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d43e231" publication-type="other">
Margaret J. M. Ezell, The Patriarch's Wife: Literary Evidence and the History of the Family
(Chapel Hill, 1987)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e240" publication-type="other">
"'To Be Your Daughter in Your Pen': The Social Functions of
Literature in the Writings of Lady Elizabeth Brackley and Lady Jane Cavendish," Huntington
Library Quarterly 51 (1988), 281-96;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e252" publication-type="other">
Arthur Marotti, John Donne, Coterie Poet (Madison, 1986)
and Manuscript, Print, and the English Renaissance Lyric (Ithaca, 1994);</mixed-citation>
            </p>
         </fn>
         <fn id="d43e262a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d43e271" publication-type="other">
Chartier, The Order of Books, p. 9;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e277" publication-type="other">
D. F. McKenzie, "Speech-Manuscript-Print," in New
Directions in Textual Studies, ed. Dave Oliphant, Robin Bradford, and Larry Carver (Austin: The
Harry Ransom Center of the University of Texas 1991), pp. 87-109.</mixed-citation>
            </p>
         </fn>
         <fn id="d43e290a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d43e297" publication-type="other">
Richard C. Newton, Foundations of Ben Jonson's Poetic Style: "Epigrammes"
and "The Forrest" (New York, 1988)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e306" publication-type="other">
Martin Elsky, Authorizing Words: Speech, Writing, and
Print in the English Renaissance (Ithaca, 1989);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e315" publication-type="other">
Hamlet and Milton chapters in my
forthcoming Unediting the Renaissance: Shakespeare, Marlowe, Milton, with projected publication
for late 1995 or early 1996 from Routledge</mixed-citation>
            </p>
         </fn>
         <fn id="d43e328a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d43e335" publication-type="other">
Patricia Parker, Literary Fat Ladies: Rhetoric, Gender, Property (London, 1987);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d43e341" publication-type="other">
Wayne A. Rebhorn, The Emperor of Men's Minds: Literature and the Renaissance Discourse of
Rhetoric (Ithaca, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d43e351a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d43e358" publication-type="other">
The Eloquent "I": Style and Self in Seventeenth-Century Prose (Madison, 1968).</mixed-citation>
            </p>
         </fn>
         <fn id="d43e366a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d43e373" publication-type="other">
The Printing Press as an Agent of Change (Cambridge, Eng., 1979), 2 vols.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
