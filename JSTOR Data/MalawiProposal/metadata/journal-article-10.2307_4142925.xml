<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id journal-id-type="jstor">j100837</journal-id>
         <journal-title-group>
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">09628452</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4142925</article-id>
         <title-group>
            <article-title>Do Wildlife Laws Work? Species Protection and the Application of a Prey Choice Model to Poaching Decisions</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. Marcus</given-names>
                  <surname>Rowcliffe</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Emmanuel</given-names>
                  <surname>de Merode</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Guy</given-names>
                  <surname>Cowlishaw</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>22</day>
            <month>12</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">271</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1557</issue>
         <issue-id>i388394</issue-id>
         <fpage>2631</fpage>
         <lpage>2636</lpage>
         <page-range>2631-2636</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4142925"/>
         <abstract>
            <p>Legislation for the protection of species is a global conservation tool. However, in many developing countries lack of resources means that effectiveness relies on voluntary compliance., leading to contradictory assumptions. On one hand, laws introduced without effective enforcement mechanisms carry an implicit assumption that voluntary compliance will occur. On the other hand, it is often openly assumed that, without enforcement, there will in fact be no compliance. Which assumption holds has rarely been rigorously tested. Here we show that laws for the protection of some species of large mammal have no effect on the prey choice patterns of primarily commercial hunters in the Democratic Republic of Congo, confirming the second assumption. We established this result by using an optimal diet model to predict the pattern of prey choice in the absence of regulation. Prey choice patterns predicted by the model were accurate across a range of conditions defined by time, space and type of hunting weapon. Given that hunters will not comply voluntarily, the protection of vulnerable species can only take place through effective enforcement, for example by wildlife authorities restricting access to protected areas, or by traditional authorities restricting the sale of protected species in local markets.</p>
         </abstract>
         <kwd-group>
            <kwd>Bushmeat</kwd>
            <kwd>Wildlife Law Enforcement</kwd>
            <kwd>Human Hunters</kwd>
            <kwd>Optimum Diet Model</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d92e196a1310">
            <mixed-citation id="d92e200" publication-type="journal">
Abbot, J. I. O. &amp; Mace, R. 1999 Managing protected wood-
lands: fuelwood collection and law enforcement in Lake
Malawi National Park. Conserv. Biol. 13, 418-421.<object-id pub-id-type="jstor">10.2307/2641484</object-id>
               <fpage>418</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e219a1310">
            <mixed-citation id="d92e223" publication-type="journal">
Alvard, M. S. 1993 Testing the ecologically noble savage
hypothesis-interspecific prey choice by Piro hunters of
Amazonian Peru. Hum. Ecol. 21, 355-387.<person-group>
                  <string-name>
                     <surname>Alvard</surname>
                  </string-name>
               </person-group>
               <fpage>355</fpage>
               <volume>21</volume>
               <source>Hum. Ecol.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d92e258a1310">
            <mixed-citation id="d92e262" publication-type="journal">
Charnov, E. L. 1976 Optimal foraging: attack strategy of a
mantid. Am. Nat. 110, 141-151.<object-id pub-id-type="jstor">10.2307/2459883</object-id>
               <fpage>141</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e278a1310">
            <mixed-citation id="d92e282" publication-type="other">
de Merode, E. 1998 Protected areas and local livelihoods:
contrasting systems of wildlife management in the Demo-
cratic Republic of Congo. PhD thesis, University College
London.</mixed-citation>
         </ref>
         <ref id="d92e299a1310">
            <mixed-citation id="d92e303" publication-type="book">
de Merode, E. 2004 Protected areas and decentralisation in
the Democratic Republic of Congo: a case for devolving
responsibility to local institutions. In Rural resources and local
livelihoods in Africa (ed. K. Homewood), pp. 36-58. Oxford:
James Currey.<person-group>
                  <string-name>
                     <surname>de Merode</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Protected areas and decentralisation in the Democratic Republic of Congo: a case for devolving responsibility to local institutions</comment>
               <fpage>36</fpage>
               <source>Rural resources and local livelihoods in Africa</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e344a1310">
            <mixed-citation id="d92e348" publication-type="journal">
de Merode, E., Homewood, K. &amp; Cowlishaw, G. 2004 The
value of bushmeat and other wild foods to rural households
living in extreme poverty in Democratic Republic of Congo.
Biol. Conserv. 118, 583-592.<person-group>
                  <string-name>
                     <surname>de Merode</surname>
                  </string-name>
               </person-group>
               <fpage>583</fpage>
               <volume>118</volume>
               <source>Biol. Conserv.</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e386a1310">
            <mixed-citation id="d92e390" publication-type="book">
de Schlippé, P. 1954 Écocultures d'Afrique. Nivelles, Belgium:
Editions Terre et Vie, l'Harmattan.<person-group>
                  <string-name>
                     <surname>de Schlippé</surname>
                  </string-name>
               </person-group>
               <source>Écocultures d'Afrique</source>
               <year>1954</year>
            </mixed-citation>
         </ref>
         <ref id="d92e415a1310">
            <mixed-citation id="d92e419" publication-type="book">
DFID 2002 Wildlife and Poverty Study. London: Department
for International Development, Livestock and Wildlife
Advisory Group.<person-group>
                  <string-name>
                     <surname>DFID</surname>
                  </string-name>
               </person-group>
               <source>Wildlife and Poverty Study</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d92e448a1310">
            <mixed-citation id="d92e452" publication-type="book">
Evans-Pritchard, E. E. 1971 The Azande: history and political
institutions. Oxford: Clarendon Press.<person-group>
                  <string-name>
                     <surname>Evans-Pritchard</surname>
                  </string-name>
               </person-group>
               <source>The Azande: history and political institutions</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d92e477a1310">
            <mixed-citation id="d92e481" publication-type="book">
Goldstein, H. 2003 Multilevel statistical models. London: Hod-
der Arnold.<person-group>
                  <string-name>
                     <surname>Goldstein</surname>
                  </string-name>
               </person-group>
               <source>Multilevel statistical models</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e507a1310">
            <mixed-citation id="d92e511" publication-type="book">
Hawkes, K. 1990 Why do men hunt? Benefits for risky choi-
ces. In Risk and uncertainty in tribal and peasant economies
(ed. E. Cashdan), pp. 145-166. Boulder: Westview Press.<person-group>
                  <string-name>
                     <surname>Hawkes</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Why do men hunt? Benefits for risky choices</comment>
               <fpage>145</fpage>
               <source>Risk and uncertainty in tribal and peasant economies</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d92e546a1310">
            <mixed-citation id="d92e550" publication-type="journal">
Hawkes, K. 1991 Showing off-tests of an hypothesis about
men's foraging goals. Ethol. Sociobiol. 12, 29-54.<person-group>
                  <string-name>
                     <surname>Hawkes</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>12</volume>
               <source>Ethol. Sociobiol.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d92e582a1310">
            <mixed-citation id="d92e586" publication-type="journal">
Hawkes, K., Hill, K. &amp; O'Connell, J. F. 1982 Why hunters
gather-optimal foraging and the Ache of eastern Paraguay.
Am. Ethnologist 9, 379-398.<person-group>
                  <string-name>
                     <surname>Hawkes</surname>
                  </string-name>
               </person-group>
               <fpage>379</fpage>
               <volume>9</volume>
               <source>Am. Ethnologist</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d92e621a1310">
            <mixed-citation id="d92e625" publication-type="journal">
Jachmann, H. &amp; Billiouw, M. 1997 Elephant poaching and
law enforcement in the central Luangwa Valley, Zambia. J.
Appl. Ecol. 34, 233-244.<object-id pub-id-type="doi">10.2307/2404861</object-id>
               <fpage>233</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e644a1310">
            <mixed-citation id="d92e648" publication-type="journal">
Kuchikura, Y. 1988 Efficiency and focus of blowpipe hunting
among Semaq Beri hunter-gatherers of peninsular Malay-
sia. Hum. Ecol. 16, 271-305.<person-group>
                  <string-name>
                     <surname>Kuchikura</surname>
                  </string-name>
               </person-group>
               <fpage>271</fpage>
               <volume>16</volume>
               <source>Hum. Ecol.</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d92e683a1310">
            <mixed-citation id="d92e687" publication-type="journal">
Leader-Williams, N. &amp; Albon, S. D. 1988 Allocation of
resources for conservation. Nature 336, 533-535.<person-group>
                  <string-name>
                     <surname>Leader-Williams</surname>
                  </string-name>
               </person-group>
               <fpage>533</fpage>
               <volume>336</volume>
               <source>Nature</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d92e720a1310">
            <mixed-citation id="d92e724" publication-type="journal">
Leader-Williams, N. &amp; Milner-Gulland, E. J. 1993 Policies for
the enforcement of wildlife laws-the balance between
detection and penalties in Luangwa Valley, Zambia. Con-
serv. Biol. 7, 611-617.<object-id pub-id-type="jstor">10.2307/2386690</object-id>
               <fpage>611</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e747a1310">
            <mixed-citation id="d92e751" publication-type="journal">
Leader-Williams, N., Albon, S. D. &amp; Berry, P. S. M. 1990
Illegal exploitation of black rhinoceros and elephant popula-
tions-patterns of decline, law-enforcement and patrol effort
in Luangwa Valley, Zambia. J. Appl. Ecol. 27, 1055-1087.<object-id pub-id-type="doi">10.2307/2404395</object-id>
               <fpage>1055</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e774a1310">
            <mixed-citation id="d92e778" publication-type="journal">
McNamara, J. M. &amp; Houston, A. I. 1987 Partial preferences
and foraging. Anim. Behav. 35, 1084-1099.<person-group>
                  <string-name>
                     <surname>McNamara</surname>
                  </string-name>
               </person-group>
               <fpage>1084</fpage>
               <volume>35</volume>
               <source>Anim. Behav.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d92e810a1310">
            <mixed-citation id="d92e814" publication-type="journal">
Milner-Gulland, E. J. &amp; Leader-Williams, N. 1992 A model of
incentives for the illegal exploitation of black rhinos and ele-
phants-poaching pays in Luangwa Valley, Zambia. J. Appl.
Ecol. 29, 388-401.<object-id pub-id-type="doi">10.2307/2404508</object-id>
               <fpage>388</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e837a1310">
            <mixed-citation id="d92e841" publication-type="journal">
Milner-Gulland, E. J. &amp; Clayton, L. 2002 The trade in babir-
usas and wild pigs in North Sulawesi, Indonesia. Ecol. Eco-
nomics 42, 165-183.<person-group>
                  <string-name>
                     <surname>Milner-Gulland</surname>
                  </string-name>
               </person-group>
               <fpage>165</fpage>
               <volume>42</volume>
               <source>Ecol. Economics</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d92e876a1310">
            <mixed-citation id="d92e880" publication-type="journal">
Peres, C. A. &amp; Terborgh, J. W. 1995 Amazonian nature
reserves-an analysis of the defensibility status of existing
conservation units and design criteria for the future. Con-
serv. Biol. 9, 34-46.<object-id pub-id-type="jstor">10.2307/2386386</object-id>
               <fpage>34</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e904a1310">
            <mixed-citation id="d92e908" publication-type="journal">
Pulliam, H. R. 1974 On the theory of optimal diets. Am. Nat.
108, 59-75.<object-id pub-id-type="jstor">10.2307/2459736</object-id>
               <fpage>59</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e924a1310">
            <mixed-citation id="d92e928" publication-type="other">
Rasbash, J., Browne, W. J., Healy, M., Cameron, B. &amp; Charl-
ton, C. 2000 Mlwin software version 2.0. London: Institute
of Education, University of London.</mixed-citation>
         </ref>
         <ref id="d92e941a1310">
            <mixed-citation id="d92e945" publication-type="journal">
Rowcliffe, J. M., Cowlishaw, G. &amp; Long, J. 2003 A model of
human hunting impacts in multi-prey communities. J. Appl.
Ecol. 40, 872-889.<object-id pub-id-type="jstor">10.2307/3505770</object-id>
               <fpage>872</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e964a1310">
            <mixed-citation id="d92e968" publication-type="journal">
Sih, A. &amp; Christensen, B. 2001 Optimal diet theory: when
does it work, and when and why does it fail? Anim. Behav.
61, 379-390.<person-group>
                  <string-name>
                     <surname>Sih</surname>
                  </string-name>
               </person-group>
               <fpage>379</fpage>
               <volume>61</volume>
               <source>Anim. Behav.</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1003a1310">
            <mixed-citation id="d92e1007" publication-type="book">
Silva, M. &amp; Downing, J. A. 1995 CRC handbook of mammalian
body masses. Boca Raton: CRC Press.<person-group>
                  <string-name>
                     <surname>Silva</surname>
                  </string-name>
               </person-group>
               <source>CRC handbook of mammalian body masses</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1032a1310">
            <mixed-citation id="d92e1036" publication-type="book">
Smith, E. A. 1991 Inujjuamiut foraging strategies. New York:
Aldine de Gruyter.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <source>Inujjuamiut foraging strategies</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1062a1310">
            <mixed-citation id="d92e1066" publication-type="journal">
Wilkie, D. S., Sidle, J. G. &amp; Boundzanga, G. C. 1992 Mechan-
ized logging, market hunting, and a bank loan in Congo.
Conserv. Biol. 6, 570-580.<object-id pub-id-type="jstor">10.2307/2386367</object-id>
               <fpage>570</fpage>
            </mixed-citation>
         </ref>
         <ref id="d92e1085a1310">
            <mixed-citation id="d92e1089" publication-type="journal">
Wilkie, D. S., Carpenter, J. F. &amp; Zhang, Q. F. 2001 The under
financing of protected areas in the Congo Basin: so many
parks and so little willingness to pay. Biodiversity Conserv.
10, 691-709.<person-group>
                  <string-name>
                     <surname>Wilkie</surname>
                  </string-name>
               </person-group>
               <fpage>691</fpage>
               <volume>10</volume>
               <source>Biodiversity Conserv.</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
