<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41329832</article-id>
         <article-categories>
            <subj-group>
               <subject>MAJOR ARTICLES AND BRIEF REPORTS</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Human Transcriptome During Nontyphoid Salmonella and HIV Coinfection Reveals Attenuated NFκB-Mediated Inflammation and Persistent Cell Cycle Disruption</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Fernanda</given-names>
                  <surname>Schreiber</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David J.</given-names>
                  <surname>Lynn</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Angela</given-names>
                  <surname>Houston</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Joanna</given-names>
                  <surname>Peters</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gershom</given-names>
                  <surname>Mwafulirwa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Brett B.</given-names>
                  <surname>Finlay</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Fiona S. L.</given-names>
                  <surname>Brinkman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert E. W.</given-names>
                  <surname>Hancock</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert S.</given-names>
                  <surname>Heyderman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gordon</given-names>
                  <surname>Dougan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Melita A.</given-names>
                  <surname>Gordon</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>15</day>
            <month>10</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">204</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">8</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40061409</issue-id>
         <fpage>1237</fpage>
         <lpage>1245</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/infdis/jir512"
                   xlink:title="an external site"/>
         <abstract>
            <p>Background. Invasive nontyphoid Salmonella (iNTS) disease is common and severe in adults with human immunodeficiency virus (HIV) infection in Africa. We previously observed that ex vivo macrophages from HIV-infected subjects challenged with Salmonella Typhimurium exhibit dysregulated proinflammatory cytokine responses. Methods. We studied the transcriptional response in whole blood from HIV-positive patients during acute and convalescent iNTS disease compared to other invasive bacterial diseases, and to HIV-positive and -negative controls. Results. During iNTS disease, there was a remarkable lack of a coordinated inflammatory or innate immune signaling response. Few interferon λ (IFNy)-induced genes or Toll-like receptor/transcription factor nuclear factor KB (TLR/NFKB) gene pathways were upregulated in expression. Ex vivo lipopolysacharide (LPS) or flagellin stimulation of whole blood, however, showed that convalescent iNTS subjects and controls were competent to mount prominent TLR/NFKB-associated patterns of mRNA expression. In contrast, HIV-positive patients with other invasive bacterial infections (Escherichia coli and Streptococcus pneumoniae) displayed a pronounced proinflammatory innate immune transcriptional response. There was also upregulated mRNA expression in cell cycle, DNA replication, translation and repair, and viral replication pathways during iNTS. These patterns persisted for up to 2 months into convalescence. Conclusions. Attenuation of NFkB-mediated inflammation and dysregulation of cell cycle and DNA-function gene pathway expression are key features of the interplay between iNTS and HIV.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d653e307a1310">
            <label>1</label>
            <mixed-citation id="d653e314" publication-type="other">
Reddy EA, Shaw AV, Crump JA. Community-acquired bloodstream
infections in Africa: a systematic review and meta-analysis. Lancet
Infect Dis 2010; 10:417-32.</mixed-citation>
         </ref>
         <ref id="d653e327a1310">
            <label>2</label>
            <mixed-citation id="d653e334" publication-type="other">
Gordon MA, Walsh AL, Chaponda M, et al. Bacteraemia and
mortality among adult medical admissions in Malawi -predominance
of non-typhi salmonellae and Streptococcus pneumoniae. J Infect 2001;
42:44-9.</mixed-citation>
         </ref>
         <ref id="d653e350a1310">
            <label>3</label>
            <mixed-citation id="d653e357" publication-type="other">
Gordon MA, Banda HT, Gondwe M, et al. Non-typhoidal salmonella
bacteraemia among HIV-infected Malawian adults: high mortality and
frequent recrudescence. AIDS 2002; 16:1641.</mixed-citation>
         </ref>
         <ref id="d653e370a1310">
            <label>4</label>
            <mixed-citation id="d653e377" publication-type="other">
Gordon MA, Graham SM, Walsh AL, et al. Epidemics of invasive
Salmonella enterica serovar enteritidis and Salmonella enterica serovar
typhimurium infection associated with multidrug resistance among
adults and children in Malawi. Clin Infect Dis 2008; 46:963-9.</mixed-citation>
         </ref>
         <ref id="d653e394a1310">
            <label>5</label>
            <mixed-citation id="d653e401" publication-type="other">
Gordon MA. Salmonella infections in immunocompromised adults.
J Infect 2008; 56:413-22.</mixed-citation>
         </ref>
         <ref id="d653e411a1310">
            <label>6</label>
            <mixed-citation id="d653e418" publication-type="other">
Gordon MA, Kankwatira AM, Mwafulirwa G, et al. Invasive non-
typhoid salmonellae establish systemic intracellular infection in HIV-
infected adults: an emerging disease pathogenesis. Clin Infect Dis 2010;
50:953-62.</mixed-citation>
         </ref>
         <ref id="d653e434a1310">
            <label>7</label>
            <mixed-citation id="d653e441" publication-type="other">
Gordon MA, Gordon SB, Musaya L, Zijlstra EE, Molyneux ME, Read
RC. Primary macrophages from HIV-infected adults show dysregu-
lated cytokine responses to Salmonella, but normal internalization and
killing. AIDS 2007; 21:2399-408.</mixed-citation>
         </ref>
         <ref id="d653e457a1310">
            <label>8</label>
            <mixed-citation id="d653e464" publication-type="other">
Gardy JL, Lynn DJ, Brinkman FS, Hancock RE. Enabling a systems
biology approach to immunology: focus on innate immunity. Trends
Immunol 2009; 30:249-62.</mixed-citation>
         </ref>
         <ref id="d653e477a1310">
            <label>9</label>
            <mixed-citation id="d653e484" publication-type="other">
Eckmann L, Smith JR, Housley MP, Dwinell MB, Kagnoff MF. Analysis
by high density cDNA arrays of altered gene expression in human
intestinal epithelial cells in response to infection with the invasive
enteric bacteria Salmonella. J Biol Chem 2000; 275:14084-94.</mixed-citation>
         </ref>
         <ref id="d653e500a1310">
            <label>10</label>
            <mixed-citation id="d653e507" publication-type="other">
Rosenberger CM, Scott MG, Gold MR, Hancock RE, Finlay BB. Sal-
monella typhimurium infection and lipopolysaccharide stimulation
induce similar changes in macrophage gene expression. J Immunol
2000; 164:5894-904.</mixed-citation>
         </ref>
         <ref id="d653e524a1310">
            <label>11</label>
            <mixed-citation id="d653e531" publication-type="other">
Stockhammer OW, Zakrzewska A, Hegedus Z, Spaink HP, Meijer AH.
Transcriptome profiling and functional analyses of the zebrafish em-
bryonic innate immune response to Salmonella infection. J Immunol
2009; 182:5641-53.</mixed-citation>
         </ref>
         <ref id="d653e547a1310">
            <label>12</label>
            <mixed-citation id="d653e554" publication-type="other">
Thompson LJ, Dunstan S J, Doleček С, et al. Transcriptional response in
the peripheral blood of patients infected with Salmonella enterica se-
rovar typhi. Proc Natl Acad Sci USA 2009; 106:22433-8.</mixed-citation>
         </ref>
         <ref id="d653e567a1310">
            <label>13</label>
            <mixed-citation id="d653e574" publication-type="other">
Detweiler CS, Cunanan DB, Falkow S. Host microarray analysis reveals
a role for the Salmonella response regulator phoP in human macro-
phage cell death. Proc Natl Acad Sci USA 2001; 98:5850-5.</mixed-citation>
         </ref>
         <ref id="d653e587a1310">
            <label>14</label>
            <mixed-citation id="d653e594" publication-type="other">
Hochberg Y, Benjamini Y. More powerful procedures for multiple
significance testing. Stat Med 1990; 9:811-8.</mixed-citation>
         </ref>
         <ref id="d653e604a1310">
            <label>15</label>
            <mixed-citation id="d653e611" publication-type="other">
Lynn DJ, Winsor GL, Chan C, et al. InnateDB: facilitating systems-level
analyses of the mammalian innate immune response. Mol Syst Biol
2008; 4:218.</mixed-citation>
         </ref>
         <ref id="d653e624a1310">
            <label>16</label>
            <mixed-citation id="d653e631" publication-type="other">
Lin CY, Chin CH, Wu HH, Chen SH, Ho CW, Ko MT. Hubba: hub
objects analyzer -a framework of interactome hubs identification for
network biology. Nucleic Acids Res 2008; 36:W438-43.</mixed-citation>
         </ref>
         <ref id="d653e645a1310">
            <label>17</label>
            <mixed-citation id="d653e652" publication-type="other">
Galati D, Bocchino M. New insights on the perturbations of T cell cycle
during HIV infection. Curr Med Chem 2007; 14:1920-4.</mixed-citation>
         </ref>
         <ref id="d653e662a1310">
            <label>18</label>
            <mixed-citation id="d653e669" publication-type="other">
Kino T, Gragerov A, Valentin A, et al. Vpr protein of human immu-
nodeficiency virus type 1 binds to 14-3-3 proteins and facilitates
complex formation with Cdc25C: implications for cell cycle arrest.
J Virol 2005; 79:2780-7.</mixed-citation>
         </ref>
         <ref id="d653e685a1310">
            <label>19</label>
            <mixed-citation id="d653e692" publication-type="other">
Izumi T, Takaori-Kondo A, Shirakawa K, et al. MDM2 is a novel E3
ligase for HIV-1 Vif. Retrovirology 2009; 6:1.</mixed-citation>
         </ref>
         <ref id="d653e702a1310">
            <label>20</label>
            <mixed-citation id="d653e709" publication-type="other">
Verstak B, Nagpal K, Bottomley SP, Golenbock DT, Hertzog PJ,
Mansell A. MyD88 adapter-like (Mal)/TIRAP interaction with TRAF6
is critical for TLR2-and TLR4-mediated NF-кВ proinflammatory re-
sponses. J Biol Chem 2009; 284:24192-203.</mixed-citation>
         </ref>
         <ref id="d653e725a1310">
            <label>21</label>
            <mixed-citation id="d653e732" publication-type="other">
Ikeda O, Sekine Y, Yasui T, et al. STAP-2 negatively regulates both
canonical and noncanonical NF-kappaB activation induced by Epstein-
Barr virus-derived latent membrane protein 1. Mol Cell Biol 2008;
28:5027-42.</mixed-citation>
         </ref>
         <ref id="d653e748a1310">
            <label>22</label>
            <mixed-citation id="d653e755" publication-type="other">
Sekine Y, Yumioka T, Yamamoto T, et al. Modulation of TLR4 sig-
naling by a novel adaptor protein signal-transducing adaptor protein-2
in macrophages. J Immunol 2006; 176:380-9.</mixed-citation>
         </ref>
         <ref id="d653e769a1310">
            <label>23</label>
            <mixed-citation id="d653e776" publication-type="other">
Lee SY, Lee SY, Choi Y. TRAF-interacting protein (TRIP): a novel
component of the tumor necrosis factor receptor (TNFR)-and CD30-
TRAF signaling complexes that inhibits TRAF2-mediated NF-kappaB
activation. J Exp Med 1997; 185:1275-85.</mixed-citation>
         </ref>
         <ref id="d653e792a1310">
            <label>24</label>
            <mixed-citation id="d653e799" publication-type="other">
Hosmalin A, Lebon P. Type I interferon production in HIV-infected
patients. J Leukoc Biol 2006; 80:984-93.</mixed-citation>
         </ref>
         <ref id="d653e809a1310">
            <label>25</label>
            <mixed-citation id="d653e816" publication-type="other">
Jacquelin B, Mayau V, Targat B, et al. Nonpathogenic SIV infection of
African green monkeys induces a strong but rapidly controlled type I
IFN response. J Clin Invest 2009; 119:3544-55.</mixed-citation>
         </ref>
         <ref id="d653e829a1310">
            <label>26</label>
            <mixed-citation id="d653e836" publication-type="other">
Tegner J, Nilsson R, Bajic VB, Bjorkegren J, Ravasi T. Systems biology
of innate immunity. Cell Immunol 2006; 244:105-9.</mixed-citation>
         </ref>
         <ref id="d653e846a1310">
            <label>27</label>
            <mixed-citation id="d653e853" publication-type="other">
Coley W, Kehn-Hall K, Van DR, Kashanchi F. Novel HIV-1 thera-
peutics through targeting altered host cell pathways. Expert Opin Biol
Ther 2009; 9:1369-82.</mixed-citation>
         </ref>
         <ref id="d653e866a1310">
            <label>28</label>
            <mixed-citation id="d653e873" publication-type="other">
Van den BR, Florence E, Vlieghe E, et al. Transcriptome analysis of
monocyte-HIV interactions. Retrovirology 2010; 7:53.</mixed-citation>
         </ref>
         <ref id="d653e884a1310">
            <label>29</label>
            <mixed-citation id="d653e891" publication-type="other">
Biswas SK, Lopez-Collazo E. Endotoxin tolerance: new mechanisms,
molecules and clinical significance. Trends Immunol 2009; 30:475-87.</mixed-citation>
         </ref>
         <ref id="d653e901a1310">
            <label>30</label>
            <mixed-citation id="d653e908" publication-type="other">
Hiscott J, Kwon H, Genin P. Hostile takeovers: viral appropriation of
the NF-kappa В pathway. J Clin Invest 2001; 107:143-51.</mixed-citation>
         </ref>
         <ref id="d653e918a1310">
            <label>31</label>
            <mixed-citation id="d653e925" publication-type="other">
Noursadeghi M, Tsang J, Miller RF, et al. Genome-wide innate immune
responses in HIV-1 -infected macrophages are preserved despite at-
tenuation of the NF-kappa В activation pathway. J Immunol 2009; 182:
319-28.</mixed-citation>
         </ref>
         <ref id="d653e941a1310">
            <label>32</label>
            <mixed-citation id="d653e948" publication-type="other">
Vassilev LT, Vu BT, Graves В, et al. In vivo activation of the p53
pathway by small-molecule antagonists of MDM2. Science 2004; 303:
844-8.</mixed-citation>
         </ref>
         <ref id="d653e961a1310">
            <label>33</label>
            <mixed-citation id="d653e968" publication-type="other">
Andreana A, Gollapudi S, Gupta S. Salmonella typhimurium induces
expression of P glycoprotein (multidrug resistance 1 gene product)
in a promonocytic cell line chronically infected with human im-
munodeficiency virus type 1. J Infect Dis 1994; 169:760-5.</mixed-citation>
         </ref>
         <ref id="d653e984a1310">
            <label>34</label>
            <mixed-citation id="d653e991" publication-type="other">
Gollapudi S, Gupta S, Thadepalli H. Salmonella typhimurium-induced
reactivation of latent HIV-1 in promonocytic U1 cells is inhibited by
trovafloxacin. Int J Mol Med 2000; 5:615-8.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
