<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id journal-id-type="jstor">j100014</journal-id>
         <journal-title-group>
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences of the United States of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00278424</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40514</article-id>
         <title-group>
            <article-title>Major Histocompatibility Class I Presentation of Soluble Antigen Facilitated by Mycobacterium tuberculosis Infection</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Richard J.</given-names>
                  <surname>Mazzaccaro</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Margaret</given-names>
                  <surname>Gedde</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Eric R.</given-names>
                  <surname>Jensen</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Hisse M.</given-names>
                  <surname>Van Santen</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Hidde L.</given-names>
                  <surname>Ploegh</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Kenneth L.</given-names>
                  <surname>Rock</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Barry R.</given-names>
                  <surname>Bloom</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>10</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">93</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">21</issue>
         <issue-id>i202860</issue-id>
         <fpage>11786</fpage>
         <lpage>11791</lpage>
         <page-range>11786-11791</page-range>
         <permissions>
            <copyright-statement>Copyright 1996 National Academy of Sciences</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/40514"/>
         <abstract>
            <p>Cell-mediated immune responses are essential for protection against many intracellular pathogens. For Mycobacterium tuberculosis (MTB), protection requires the activity of T cells that recognize antigens presented in the context of both major histocompatibility complex (MHC) class II and I molecules. Since MHC class I presentation generally requires antigen to be localized to the cytoplasmic compartment of antigen-presenting cells, it remains unclear how pathogens that reside primarily within endocytic vesicles of infected macrophages, such as MTB, can elicit specific MHC class I-restricted T cells. A mechanism is described for virulent MTB that allows soluble antigens ordinarily unable to enter the cytoplasm, such as ovalbumin, to be presented through the MHC class I pathway to T cells. The mechanism is selective for MHC class I presentation, since MTB infection inhibited MHC class II presentation of ovalbumin. The MHC class I presentation requires the tubercle bacilli to be viable, and it is dependent upon the transporter associated with antigen processing (TAP), which translocates antigenic peptides from the cytoplasm into the endoplasmic reticulum. The process is mimicked by Listeria monocytogenes and soluble listeriolysin, a pore-forming hemolysin derived from it, suggesting that virulent MTB may have evolved a comparable mechanism that allows molecules in a vacuolar compartment to enter the cytoplasmic presentation pathway for the generation of protective MHC class I-restricted T cells.</p>
         </abstract>
         <kwd-group>
            <kwd>Immunology</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Endnotes]</title>
         <ref id="d302e199a1310">
            <label>1</label>
            <mixed-citation id="d302e206" publication-type="book">
Murray, C. J. L. &amp; Lopez, A. D. (1996) Burden of Disease and
Injury (Harvard University Press, Cambridge, MA), in press.<person-group>
                  <string-name>
                     <surname>Murray</surname>
                  </string-name>
               </person-group>
               <source>Burden of Disease and Injury</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d302e231a1310">
            <label>2</label>
            <mixed-citation id="d302e238" publication-type="journal">
Orme, I. M. (1987) J. Immunol.138, 293-298.<person-group>
                  <string-name>
                     <surname>Orme</surname>
                  </string-name>
               </person-group>
               <fpage>293</fpage>
               <volume>138</volume>
               <source>J. Immunol.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d302e267a1310">
            <label>3</label>
            <mixed-citation id="d302e274" publication-type="journal">
Flynn, J. L., Chan, J., Triebold, K. J., Dalton, D. K., Stewart, T.
A. &amp; Bloom, B. R. (1993) J. Exp. Med.178, 2249-2254.<person-group>
                  <string-name>
                     <surname>Flynn</surname>
                  </string-name>
               </person-group>
               <fpage>2249</fpage>
               <volume>178</volume>
               <source>J. Exp. Med.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d302e306a1310">
            <label>4</label>
            <mixed-citation id="d302e313" publication-type="journal">
Cooper, A. M., Dalton, D. K., Stewart, T. A., Griffin, J. P.,
Russell, D. G. &amp; Orme, I. M. (1993)J. Exp. Med.178, 2243-2247.<person-group>
                  <string-name>
                     <surname>Cooper</surname>
                  </string-name>
               </person-group>
               <fpage>2243</fpage>
               <volume>178</volume>
               <source>J. Exp. Med.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d302e346a1310">
            <label>5</label>
            <mixed-citation id="d302e353" publication-type="journal">
Flynn, J. L., Goldstein, M. M., Chan, J., Triebold, K. J., Pfeffer,
K., Lowenstein, C. J., Schreiber, R., Mak, T. W. &amp; Bloom, B. R.
(1995) Immunity2, 561-572.<person-group>
                  <string-name>
                     <surname>Flynn</surname>
                  </string-name>
               </person-group>
               <fpage>561</fpage>
               <volume>2</volume>
               <source>Immunity</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d302e388a1310">
            <label>6</label>
            <mixed-citation id="d302e395" publication-type="journal">
Flynn, J. L., Goldstein, M. M., Triebold, K. J., Koller, B. &amp;
Bloom, B. R. (1992) Proc. Natl. Acad. Sci. USA89, 12013-12017.<object-id pub-id-type="jstor">10.2307/2360847</object-id>
               <fpage>12013</fpage>
            </mixed-citation>
         </ref>
         <ref id="d302e411a1310">
            <label>7</label>
            <mixed-citation id="d302e418" publication-type="journal">
Clemens, D. L. &amp; Horwitz, M.A. (1995) J. Exp. Med.181,
257-270.<person-group>
                  <string-name>
                     <surname>Clemens</surname>
                  </string-name>
               </person-group>
               <fpage>257</fpage>
               <volume>181</volume>
               <source>J. Exp. Med.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d302e450a1310">
            <label>8</label>
            <mixed-citation id="d302e457" publication-type="journal">
Xu, S., Cooper, A., Sturgill-Koszycki, S., van Heyningen, T.,
Chatterjee, D., Orme, I., Allen, P. &amp; Russell, D. G. (1994) J.
Immunol.153, 2568-2578.<person-group>
                  <string-name>
                     <surname>Xu</surname>
                  </string-name>
               </person-group>
               <fpage>2568</fpage>
               <volume>153</volume>
               <source>J. Immunol.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d302e492a1310">
            <label>9</label>
            <mixed-citation id="d302e499" publication-type="journal">
McDonough, K. A., Kress, Y. &amp; Bloom, B. R. (1993) Infect.
Immun.61, 2763-2773.<person-group>
                  <string-name>
                     <surname>McDonough</surname>
                  </string-name>
               </person-group>
               <fpage>2763</fpage>
               <volume>61</volume>
               <source>Infect. Immun.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d302e531a1310">
            <label>10</label>
            <mixed-citation id="d302e538" publication-type="journal">
Myrvik, Q. N., Leake, E. S. &amp; Wright, M. J. (1984) Am. Rev.
Respir. Dis.129, 322-328.<person-group>
                  <string-name>
                     <surname>Myrvik</surname>
                  </string-name>
               </person-group>
               <fpage>322</fpage>
               <volume>129</volume>
               <source>Am. Rev. Respir. Dis.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d302e571a1310">
            <label>11</label>
            <mixed-citation id="d302e578" publication-type="journal">
Armstrong, J. A. &amp; Hart, P. D. (1971) J. Exp. Med.134, 713-740.<person-group>
                  <string-name>
                     <surname>Armstrong</surname>
                  </string-name>
               </person-group>
               <fpage>713</fpage>
               <volume>134</volume>
               <source>J. Exp. Med.</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d302e607a1310">
            <label>12</label>
            <mixed-citation id="d302e614" publication-type="journal">
Shen, H., Slifka, M. K., Matloubian, M., Jensen, E. R., Ahmed,
R. &amp; Miller, J. F. (1995) Proc. Natl. Acad. Sci. USA92, 3987-
3991.<object-id pub-id-type="jstor">10.2307/2367472</object-id>
               <fpage>3987</fpage>
            </mixed-citation>
         </ref>
         <ref id="d302e633a1310">
            <label>13</label>
            <mixed-citation id="d302e640" publication-type="journal">
Kovacsovics-Bankowski, M., Clark, K., Benacerraf, B. &amp; Rock,
K. L. (1993) Proc. Natl. Acad. Sci. USA90, 4942-4946.<object-id pub-id-type="jstor">10.2307/2362197</object-id>
               <fpage>4942</fpage>
            </mixed-citation>
         </ref>
         <ref id="d302e656a1310">
            <label>14</label>
            <mixed-citation id="d302e663" publication-type="journal">
Rock, K. L., Rothstein, L., Gamble, S. &amp; Fleischacker, C. (1993)
J. Immunol.150, 438-436.<person-group>
                  <string-name>
                     <surname>Rock</surname>
                  </string-name>
               </person-group>
               <fpage>438</fpage>
               <volume>150</volume>
               <source>J. Immunol.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d302e695a1310">
            <label>15</label>
            <mixed-citation id="d302e702" publication-type="journal">
Kaer, L. V., Ashton-Rickardt, P. G., Ploegh, H. L. &amp; Tonegawa,
S. (1992) Cell71, 1205-1214.<person-group>
                  <string-name>
                     <surname>Kaer</surname>
                  </string-name>
               </person-group>
               <fpage>1205</fpage>
               <volume>71</volume>
               <source>Cell</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d302e734a1310">
            <label>16</label>
            <mixed-citation id="d302e741" publication-type="journal">
Lee, K. D., Oh, Y. K., Portnoy, D. A. &amp; Swanson, J. A. (1996)
J. Biol. Chem.271, 7249-7252.<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <fpage>7249</fpage>
               <volume>271</volume>
               <source>J. Biol. Chem.</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d302e774a1310">
            <label>17</label>
            <mixed-citation id="d302e781" publication-type="journal">
Pancholi, P., Mirza, A., Bhardwaj, N. &amp; Steinman, R. M. (1993)
Science260, 984-986.<object-id pub-id-type="jstor">10.2307/2885630</object-id>
               <fpage>984</fpage>
            </mixed-citation>
         </ref>
         <ref id="d302e797a1310">
            <label>18</label>
            <mixed-citation id="d302e804" publication-type="journal">
Gercken, J., Pryjma, J., Ernst, M. &amp; Flad, H. D. (1994) Infect.
Immun.62, 3472- 3478.<person-group>
                  <string-name>
                     <surname>Gercken</surname>
                  </string-name>
               </person-group>
               <fpage>3472</fpage>
               <volume>62</volume>
               <source>Infect. Immun.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d302e836a1310">
            <label>19</label>
            <mixed-citation id="d302e843" publication-type="journal">
Heemels, M. T. &amp; Ploegh, H. (1995) Annu. Rev. Biochem.64,
463-491.<person-group>
                  <string-name>
                     <surname>Heemels</surname>
                  </string-name>
               </person-group>
               <fpage>463</fpage>
               <volume>64</volume>
               <source>Annu. Rev. Biochem.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d302e875a1310">
            <label>20</label>
            <mixed-citation id="d302e882" publication-type="journal">
Kovacsovics-Bankowski, M. &amp; Rock, K. L. (1995) Science267,
243-246.<object-id pub-id-type="jstor">10.2307/2885903</object-id>
               <fpage>243</fpage>
            </mixed-citation>
         </ref>
         <ref id="d302e898a1310">
            <label>21</label>
            <mixed-citation id="d302e905" publication-type="journal">
Reis e Sousa, C. &amp; Germain, R. N. (1995) J. Exp. Med.182,
841-851.<person-group>
                  <string-name>
                     <surname>Reis e Sousa</surname>
                  </string-name>
               </person-group>
               <fpage>841</fpage>
               <volume>182</volume>
               <source>J. Exp. Med.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d302e937a1310">
            <label>22</label>
            <mixed-citation id="d302e944" publication-type="journal">
Harding, C. V. &amp; Song, R. (1994) J. Immunol.153, 4925-4933.<person-group>
                  <string-name>
                     <surname>Harding</surname>
                  </string-name>
               </person-group>
               <fpage>4925</fpage>
               <volume>153</volume>
               <source>J. Immunol.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d302e974a1310">
            <label>23</label>
            <mixed-citation id="d302e981" publication-type="journal">
Pfeifer, J. D., Wick, M. M., Roberts, R. L., Findlay, K., Normark,
S. J. &amp; Harding, C. V. (1993) Nature (London)361, 359-362.<person-group>
                  <string-name>
                     <surname>Pfeifer</surname>
                  </string-name>
               </person-group>
               <fpage>359</fpage>
               <volume>361</volume>
               <source>Nature (London)</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1013a1310">
            <label>24</label>
            <mixed-citation id="d302e1020" publication-type="journal">
Day, P. M., Esquivel, F., Lukszo, J., Bennink, J. R. &amp; Yewdell,
J. W. (1995) Immunity2, 137-147.<person-group>
                  <string-name>
                     <surname>Day</surname>
                  </string-name>
               </person-group>
               <fpage>137</fpage>
               <volume>2</volume>
               <source>Immunity</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1052a1310">
            <label>25</label>
            <mixed-citation id="d302e1059" publication-type="journal">
Song, R. &amp; Harding, C. V. (1996) J. Immunol.156, 4182-4190.<person-group>
                  <string-name>
                     <surname>Song</surname>
                  </string-name>
               </person-group>
               <fpage>4182</fpage>
               <volume>156</volume>
               <source>J. Immunol.</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1088a1310">
            <label>26</label>
            <mixed-citation id="d302e1095" publication-type="journal">
Tilney, L. G. &amp; Portnoy, D. A. (1989) J. Cell Biol.109, 1597-1608.<object-id pub-id-type="jstor">10.2307/1613660</object-id>
               <fpage>1597</fpage>
            </mixed-citation>
         </ref>
         <ref id="d302e1108a1310">
            <label>27</label>
            <mixed-citation id="d302e1115" publication-type="journal">
Brunt, L. M., Portnoy, D. A. &amp; Unanue, E. R. (1990) J. Immunol.
145, 3540- 3546.<person-group>
                  <string-name>
                     <surname>Brunt</surname>
                  </string-name>
               </person-group>
               <fpage>3540</fpage>
               <volume>145</volume>
               <source>J. Immunol.</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1147a1310">
            <label>28</label>
            <mixed-citation id="d302e1154" publication-type="journal">
Barry, R. A., Bouwer, H. G. A., Portnoy, D. A. &amp; Hinrichs, D. J.
(1992) Infect. Immun.60, 1625-1632.<person-group>
                  <string-name>
                     <surname>Barry</surname>
                  </string-name>
               </person-group>
               <fpage>1625</fpage>
               <volume>60</volume>
               <source>Infect. Immun.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1187a1310">
            <label>29</label>
            <mixed-citation id="d302e1194" publication-type="journal">
Darji, A., Chakroborty, T., Wehland, J. &amp; Weiss, S. (1995) Eur.
J. Immunol.25, 2967-2971.<person-group>
                  <string-name>
                     <surname>Darji</surname>
                  </string-name>
               </person-group>
               <fpage>2967</fpage>
               <volume>25</volume>
               <source>Eur. J. Immunol.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1226a1310">
            <label>30</label>
            <mixed-citation id="d302e1233" publication-type="journal">
Hart, P. D. &amp; Sutherland, I. (1977) Br. Med. J.22, 293-295.<person-group>
                  <string-name>
                     <surname>Hart</surname>
                  </string-name>
               </person-group>
               <fpage>293</fpage>
               <volume>22</volume>
               <source>Br. Med. J.</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1262a1310">
            <label>31</label>
            <mixed-citation id="d302e1269" publication-type="journal">
Tuberculosis Prevention Trial, Madras. (1980) Indian J. Med. Res.
72, 1-74.<person-group>
                  <string-name>
                     <surname>Tuberculosis Prevention Trial, Madras</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>72</volume>
               <source>Indian J. Med. Res.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1301a1310">
            <label>32</label>
            <mixed-citation id="d302e1308" publication-type="journal">
Ponnighaus, J. M., et al. (1992) Lancet339, 636-639.<person-group>
                  <string-name>
                     <surname>Ponnighaus</surname>
                  </string-name>
               </person-group>
               <fpage>636</fpage>
               <volume>339</volume>
               <source>Lancet</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1337a1310">
            <label>33</label>
            <mixed-citation id="d302e1344" publication-type="journal">
Silva, C. L., Silva, M. F., Pietro, R. C. L. R. &amp; Lowrie, D. B.
(1994) Immunology83, 341-346.<person-group>
                  <string-name>
                     <surname>Silva</surname>
                  </string-name>
               </person-group>
               <fpage>341</fpage>
               <volume>83</volume>
               <source>Immunology</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1376a1310">
            <label>34</label>
            <mixed-citation id="d302e1383" publication-type="journal">
Turner, J. &amp; Dockrell, H. M. (1996) Immunology87, 339-342.<person-group>
                  <string-name>
                     <surname>Turner</surname>
                  </string-name>
               </person-group>
               <fpage>339</fpage>
               <volume>87</volume>
               <source>Immunology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1413a1310">
            <label>35</label>
            <mixed-citation id="d302e1420" publication-type="journal">
Stover, C. K., de la Cruz, V. F., Fuerst, T. R., Burlein, J. E.,
Benson, L. A., Bennett, L. T., Bansal, G. P., Young, J. F., M. H.,
L., Hatfull, G. F., Snapper, S. B., Barletta, R. G., Jacobs, W. R.
&amp; Bloom, B. R. (1991) Nature (London)351, 456-460.<person-group>
                  <string-name>
                     <surname>Stover</surname>
                  </string-name>
               </person-group>
               <fpage>456</fpage>
               <volume>351</volume>
               <source>Nature (London)</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1458a1310">
            <label>36</label>
            <mixed-citation id="d302e1465" publication-type="journal">
High, N., Mounier, J., Prevost, M. C. &amp; Sansonetti, P. J. (1992)
EMBO J.11, 1991-1999.<person-group>
                  <string-name>
                     <surname>High</surname>
                  </string-name>
               </person-group>
               <fpage>1991</fpage>
               <volume>11</volume>
               <source>EMBO J.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1497a1310">
            <label>37</label>
            <mixed-citation id="d302e1504" publication-type="journal">
Ley, V., Robbins, E. S., Nussenzweig, V. &amp; Andrews, N. W.
(1990) J. Exp. Med.171, 401-413.<person-group>
                  <string-name>
                     <surname>Ley</surname>
                  </string-name>
               </person-group>
               <fpage>401</fpage>
               <volume>171</volume>
               <source>J. Exp. Med.</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d302e1536a1310">
            <label>38</label>
            <mixed-citation id="d302e1543" publication-type="journal">
Schwab, J. C., Beckers, C. J. M. &amp; Joiner, K. A. (1994) Proc. Natl.
Acad. Sci. USA91, 509-513.<object-id pub-id-type="jstor">10.2307/2363913</object-id>
               <fpage>509</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
