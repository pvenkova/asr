<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26269590</article-id>
         <article-categories>
            <subj-group>
               <subject>Synthesis</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Livelisystems</article-title>
            <subtitle>a conceptual framework integrating social, ecosystem, development, and evolutionary theory</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Dorward</surname>
                  <given-names>Andrew R.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>SOAS, University of London</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2014</year>
            <string-date>Jun 2014</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">19</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26269516</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2014 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26269590"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>Human activity poses multiple environmental challenges for ecosystems that have intrinsic value and also support that activity. Our ability to address these challenges is constrained by, among other things, weaknesses in cross-disciplinary understandings of interactive processes of change in social–ecological systems. This paper draws on complementary insights from social and biological sciences to propose a “livelisystems” framework of multiscale, dynamic change across social and biological systems. This describes how material, informational, and relational assets, asset services, and asset pathways interact in systems with embedded and emergent properties undergoing a variety of structural transformations. Related characteristics of “higher” (notably human) livelisystems and change processes are identified as the greater relative importance of (a) informational, relational, and extrinsic (as opposed to material and intrinsic) assets, (b) teleological (as opposed to natural) selection, and (c) innovational (as opposed to mutational) change. The framework provides valuable insights into social and environmental challenges posed by global and local change, globalization, poverty, modernization, and growth in the anthropocene. Its potential for improving interdisciplinary and multiscale understanding is discussed, notably by examination of human adaptation to biodiversity and ecosystem service change following the spread of Lantana camera in the Western Ghats, India.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>environmental change</kwd>
            <kwd>livelisystems</kwd>
            <kwd>social–ecological systems</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Alwang, J., and P. B. Siegel. 1999. An asset-based approach to social risk management: a conceptual approach. Social Protection Discussion paper 9926. World Bank, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">An, L. 2012. Modeling human decisions in coupled human and natural systems: review of agent-based models. Ecological Modelling 229:25–36.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Anand, M., A. Gonzalez, F. Guichard, J. Kolasa, and L. Parrott. 2010. Ecological systems as complex systems: challenges for an emerging science. Diversity 2:395–410. http://dx.doi.org/10.3390/d2030395</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Anderies, J. M., M. A. Janssen, and E. Ostrom. 2004. A framework to analyze the robustness of social–ecological systems from an institutional perspective. Ecology and Society 9(1): 18. [online] URL: http://www.ecologyandsociety.org/vol9/iss1/art18/</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Bodin, Ö., and M. Tengö. 2012. Disentangling intangible social–ecological systems. Global Environmental Change 22:430–439.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Boyd, J., and S. Banzhaf. 2007. What are ecosystem services? The need for standardized environmental accounting units. Ecological Economics 63:616–626.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Carney, D., editor. 1998. Sustainable livelihoods: what contribution can we make? Department for International Development (DFID), London, UK.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Carter, M. R., and C. B. Barrett. 2006. The economics of poverty traps and persistent poverty: an asset-based approach. Journal of Development Studies 42:178–199. http://dx.doi.org/10.1080/00220380500405261</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Chambers, R., and G. R. Conway. 1992. Sustainable rural livelihoods: practical concepts for the 21st century. IDSDiscussion Paper No 296. IDS, Brighton, UK.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Chirwa, E., and A. R. Dorward. 2013. Agricultural input subsidies: the recent Malawi experience. Oxford University Press, Oxford, UK. http://dx.doi.org/10.1093/acprof:oso/9780199683529.001.0001</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Costanza, R., and H. Daly. 1992. Natural capital and sustainable development. Conservation Biology 6:37–46.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Dorward, A. R., S. Anderson, Y. Nava, J. Pattison, R. Paz, J. Rushton, and E. Sanchez Vera. 2005. A guide to indicators and methods for assessing the contribution of livestock keeping to livelihoods of the poor. Department of Agricultural Sciences, Imperial College, London, UK.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Dorward, A. R., S. Anderson, Y. Nava, J. Pattison, R. Paz, J. Rushton, and E. Sanchez Vera. 2009a. Hanging in, stepping up and stepping out : livelihood aspirations and strategies of the poor. Development in Practice 19:240–247.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Dorward, A. R., J. G. Kydd, C. D. Poulton, and D. Bezemer. 2009b. Coordination risk and cost impacts on economic development in poor rural areas. Journal of Development Studies 45:1093–1112.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Dorward, A. R., N. D. Poole, J. A. Morrison, J. G. Kydd, and I. Urey. 2003. Markets, institutions and technology: missing links in livelihoods analysis. Development Policy Review 21:319–332.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Fisher, J. A., G. Patenaude, P. Meir, A. Nightingale, M. Rounsevell, M. Williams, and I. H. Woodhouse. 2013.Strengthening conceptual foundations: analysing frameworks for ecosystem services and poverty alleviation research. Global Environmental Change 23:1098–1111.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Foresight. 2011. The future of food and farming: challenges and choices for global sustainability. Final project report, The Government Office for Science, London, UK.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Gallopin, G. 1991. Human dimensions of global change: linking the global and the local processes. International Social Science Journal 130:707–718.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Gintis, H. 2007. A framework for the unification of the behavioral sciences. Behavioral and Brain Sciences 30:1–16.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Gómez-Baggethun, E., R. De Groot, P. L. Lomas, and C. Montes. 2010. The history of ecosystem services in economic theory and practice: from early notions to markets and payment schemes. Ecological Economics 69:1209–1218.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Gual, M. A., and R. B. Norgaard. 2010. Bridging ecological and social systems coevolution: a review and proposal. Ecological Economics 69:707–717.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Hird, M. J. 2010. Coevolution, symbiosis and sociology. Ecological Economics 69:737–42.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Hodgson, G. M., and T. Knudsen. 2010a. Darwin’s conjecture: the search for general principles of social and economic evolution. University of Chicago Press, London, UK.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Hodgson, G. M., and T. Knudsen. 2010b. Generative replication and the evolution of complexity. Journal of Economic Behavior and Organization 75:12–24.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Holdo, R. M., K. A. Galvin, E. Knapp, S. Polasky, R. Hilborn, and R. D. Holt. 2010. Responses to alternative rainfall regimes and antipoaching in a migratory system. Ecological Applications 20:381–397.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Holling, C. S., F. Berkes, and C. Folke. 1998. Science, sustainability, and resource management. Pages 63–102 in F. Berkes and C. Folke, editors. Linking social and ecological systems. Cambridge University Press, Cambridge, UK.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Holling, C. S., L. H. Gunderson, and G. D. Peterson 2002.Sustainability and panarchies. In L. H. Gunderson and C. S. Holling, editors. Panarchy: understanding transformations in human and natural systems. Island Press, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Hummel, D., S. Adamo, A. De Sherbinin, L. Murphy, R. Aggarwal, L. Zulu, J. Liu, and K. Knight. 2013. Inter- and transdisciplinary approaches to population–environment research for sustainability aims: a review and appraisal. Population and Environment 34(4):481–509.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Jablonka, E., and M. J. Lamb. 2005. Evolution in four dimensions: genetic, epigenetic, behavioral, and symbolic variation in the history of life. MIT Press, Cambridge, Massachussetts, USA.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Janssen, M. A., Ö. Bodin, J. M. Anderies, T. Elmqvist, H. Ernstson, R. R. J. Mcallister, P. Olsson, and P. Ryan. 2006. Toward a network perspective on the resilience of social–ecological systems. Ecology and Society 11(1): 15. [online] URL: http://www.ecologyandsociety.org/vol11/iss1/art15/</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Jax, K. 2010. Ecosystem functioning. Cambridge University Press, Cambridge, UK.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Kallis, G. 2007. When is it coevolution? Ecological Economics 62:1–6.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Kent, R., and A. Dorward. 2012a. Biodiversity change and livelihood responses: ecosystem asset functions in southern India. Working paper, Centre for Development, Environment and Policy. School of Oriental and African Studies (SOAS), University of London, London, UK.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Kent, R., and A. R. Dorward. 2012b. Conceptualising assets and asset services in livelihoods and ecosystem analyses: a framework for considering livelihood responses to biodiversity change. Working paper, Centre for Development, Environment and Policy. School of Oriental and African Studies (SOAS), University of London, London, UK.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Laland, K. N., and N. J. Boogert. 2010. Niche construction, coevolution and biodiversity. Ecological Economics 69:731–736.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Liu, J., T. Dietz, S. R. Carpenter, C. Folke, M. Alberti, C. L. Redman, S. H. Schneider, E. Ostrom, A. N. Pell, J. Lubchenco, W. W. Taylor, Z. Ouyang, P. Deadman, T. Kratz, and W. Provencher. 2007. Coupled human and natural systems. Ambio 36:639–649.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Liverpool-Tasie, L. S. O., and A. Winter-Nelson. 2011. Asset versus consumption poverty and poverty dynamics in rural Ethiopia. Agricultural Economics 42:221–233.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Macmynowski, D. P. 2007. Pausing at the brink of interdisciplinarity: power and knowledge at the meeting of social and biophysical science. Ecology and Society 12(1): 20. [online] URL: http://www.ecologyandsociety.org/vol12/iss1/art20/</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Maestre André, S., L. Calvet Mir, J. C. J. M. van Den Bergh, I. Ring, and P. H. Verburg. 2012. Ineffective biodiversity policy due to five rebound effects. Ecosystem Services 1:101–110.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">McAfee, K., and E. Shapiro. 2010. Payments for ecosystem services in Mexico: nature, neoliberalism, social movements, and the state. Annals of the Association of American Geographers 100:579–599.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Mcginnis, M. D. 2011. An introduction to IAD and the language of the Ostrom workshop: a simple guide to a complex framework. Policy Studies Journal 39:169–183.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Millennium Ecosystem Assessment. 2005. Ecosystems and human well-being: synthesis. Island Press, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Miller, J. G. 1978. Living systems. McGraw-Hill, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Miller, J. L., and J. G. Miller. 1992. Greater than the sum of its parts: subsystems which process both matter-energy and information. Behavioral Science 37:1–38.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Milner-Gulland, E. J. 2012. Interactions between human behaviour and ecological systems. Philosophical Transactions of the Royal Society B, Biological Sciences 367:270–278. http://dx. doi.org/10.1098/rstb.2011.0175</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Mollinga, P. P. 2010. Boundary work and the complexity of natural resources management. Crop Science 50:S1–S9.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Muradian, R., and L. Rival. 2012. Between markets and hierarchies: the challenge of governing ecosystem services. Ecosystem Services 1:93–100.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Nelson, R. R. 2011. Economic development as an evolutionary process. Innovation and Development 1:39–49.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Norgaard, R. B. 1984. Coevolutionary development potential. Land Economics 60:160–173.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Norgaard, R. B. 2008. Finding hope in the Millennium Ecosystem Assessment. Conservation Biology 22:862–869.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Norgaard, R. B. 2010. Ecosystem services: from eye-opening metaphor to complexity blinder. Ecological Economics 69:1219–1227.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Odling-Smee, J. 2007. Niche inheritance: a possible basis for classifying multiple inheritance systems in evolution. Biological Theory 2:276–289.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Odling-Smee, J. 2010. Niche inheritance. In M. Pigliucci and G.B. Müller, editors. Evolution—the extended synthesis. MIT Press, Cambridge, Massachussetts, USA.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Odling-Smee, J., K. N. Laland, and M. Feldman 2003. Niche construction. The neglected process in evolution. Princeton University Press, Princeton, New Jersey, USA.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Olson, M. 1993. Dictatorship, democracy, and development. The American Political Science Review 87:567–576. http://dx.doi.org/10.2307/2938736</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Ostrom, E. 2005. Understanding Institutional Diversity. Princeton University Press.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Ostrom, E. 2007. A diagnostic approach for going beyond panaceas. Proceedings of the National Academy of Sciences 104:5181–15187. http://dx.doi.org/10.1073/pnas.0610178104</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Ostrom, E. 2009. A general framework for analyzing sustainability of social–ecological systems. Science 325:419–422.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Otu, M., P. Ramlal, P. Wilkinson, R. I. Hall, and R. E. Hecky. 2011. Paleolimnological evidence of the effects of recent cultural eutrophication during the last 200 years in Lake Malawi, East Africa. Journal of Great Lakes Research 37:61–74.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Perrings, C. 2007. Future challenges. Proceedings of the National Academy of Sciences 104:15179–15180. http://dx.doi.org/10.1073/pnas.0701957104</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Perrings, C., C. Folke, and K. G. Mäler. 1992. The ecology and economics of biodiversity loss: the research agenda. Ambio 21:201–211.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Rammel, C., S. Stagl, and H. Wilfing. 2007. Managing complex adaptive systems—a co-evolutionary perspective on natural resource management. Ecological Economics 63:9–21.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Raworth, K. 2012. A safe and just space for humanity. Oxfam Discussion Paper, Oxfam, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Rockström, J., W. Steffen, K. Noone, Å. Persson, F. S. Chapin, E. Lambin, T. M. Lenton, M. Scheffer, C. Folke, H. Schellnhuber, B. Nykvist, C. A. de Wit, T. Hughes, S. van der Leeuw, H. Rodhe, S. Sörlin, P. K. Snyder, R. Costanza, U. Svedin, M. Falkenmark, L. Karlberg, R. W. Corell, V. J. Fabry, J. Hansen, B. Walker, D. Liverman, K. Richardson, P. Crutzen, and J. Foley. 2009.Planetary boundaries: exploring the safe operating space for humanity. Ecology and Society 14(2): 32. [online] URL: http://www.ecologyandsociety.org/vol14/iss2/art32/</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Rounsevell, M., T. Dawson, and P. Harrison. 2010. A conceptual framework to assess the effects of environmental change on ecosystem services. Biodiversity and Conservation 19:2823–2842.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Rounsevell, M., D. T. Robinson, and D. Murray-Rust. 2012. From actors to agents in socio-ecological systems models. Philosophical Transactions of the Royal Society B, Biological Sciences 367:259–269. http://dx.doi.org/10.1098/rstb.2011.0187</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Schlüter, M., R. R. J. Mcallister, R. Arlinghaus, N. Bunnefeld, K. Eisenack, F. Hölker, E. J. Milner-Gulland, B. Müller, E. Nicholson, M. Quaas, and M. Stöven. 2012. New horizons for managing the environment: a review of coupled social–ecological systems modeling. Natural Resource Modeling 25:219–272.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Scoones, I. 2009. Livelihoods perspectives and rural development. The Journal of Peasant Studies 36:171–196. http://dx.doi.org/10.1080/03066150902820503</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Taylor, P., editor. 2007. Integrative science for society and environment: a strategic research initiative. Research Initiatives Subcommittee of the LTER Planning Process Conference Committee and the Cyberinfrastructure Core Team, U.S. Long Term Ecological Research Network, Albuquerque, New Mexico, USA.</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">Vollmer, M. K., H. A. Bootsma, R. E. Hecky, G. Patterson, J. D. Halfman, J. M. Edmond, D. H. Eccles, and R. F. Weiss. 2005.Deep-water warming trend in Lake Malawi, East Africa. Limnology and Oceanography 50:727–732. http://dx.doi.org/10.4319/lo.2005.50.2.0727</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Waage, J., R. Banerji, O. Campbell, E. Chirwa, G. Collender, V. Dieltiens, A. Dorward, P. Godfrey-Faussett, P. Hanvoravongchai, G. Kingdon, A. Little, A. Mills, K. Mulholland, A. Mwinga, A. North, W. Patcharanarumol, C. Poulton, V. Tangcharoensathien, and E. Unterhalter. 2010. The Millennium Development Goals: a cross-sectoral analysis and principles for goal setting after 2015.The Lancet 376:991–1023. http://dx.doi.org/10.1016/S0140-6736(10)61196-8</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Wallace, K. J. 2007. Classification of ecosystem services: problems and solutions. Biological Conservation 139:235–246.</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Waring, T. M., and P. J. Richerson. 2011. Towards unification of the socio-ecological sciences: the value of coupled models. Geografiska Annaler: Series B, Human Geography 93:301–314.</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">Widlok, T., A. Aufgebauer, M. Bradtmöller, R. Dikau, T. Hoffmann, I. Kretschmer, K. Panagiotopoulos, A. Pastoors, R. Peters, F. Schäbitz, M. Schlummer, M. Solich, B. Wagner, G.-C. Weniger, and A. Zimmermann. 2012. Towards a theoretical framework for analyzing integrated socio-environmental systems. Quaternary International 274:259–272.</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">Winder, N., B. S. Mcintosh, and P. Jeffrey. 2005. The origin, diagnostic ttributes and practical application of co-evolutionary theory. Ecological Economics 54:347–361.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
