<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40482808</article-id>
         <article-id pub-id-type="pub-doi">10.1073/pnas.0901415106</article-id>
         <title-group>
            <article-title>Competitive Facilitation of Drug-Resistant Plasmodium Falciparum Malaria Parasites in Pregnant Women Who Receive Preventive Treatment</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>W. E.</given-names>
                  <surname>Harrington</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>T. K.</given-names>
                  <surname>Mutabingwa</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>A.</given-names>
                  <surname>Muehlenbachs</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>B.</given-names>
                  <surname>Sorensen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>M. C.</given-names>
                  <surname>Bolla</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>M.</given-names>
                  <surname>Fried</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>P. E.</given-names>
                  <surname>Duffy</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel L.</given-names>
                  <surname>Hartl</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>2</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">106</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">22</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40021223</issue-id>
         <fpage>9027</fpage>
         <lpage>9032</lpage>
         <permissions>
            <copyright-statement>Copyright 1993-2008 National Academy of Sciences of the United States of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40482808"/>
         <abstract>
            <p>Intermittent preventive treatment in pregnancy (IPTp) is used to prevent Plasmodium falciparum malaria. However, parasites resistant to the IPTp drug sulfadoxine-pyrimethamine (SP) have emerged worldwide, and infections with mixed resistant and susceptible parasites are exacerbated by pyrimethamine in mice. In a prospective delivery cohort in Muheza, Tanzania, we examined the effects of SP IPTp on parasite resistance alleles, parasite diversity, level of parasitemia, and inflammation in the placenta. IPTp use was associated with an increased fraction of parasites carrying the resistance allele at DHPS codon 581, an increase in the level of parasitemia, and more intense placental inflammation. The lowest mean level of parasite diversity and highest mean level of parasitemia occurred in women after recent IPTp use. These findings support a model of parasite release and facilitation, whereby the most highly resistant parasites out-compete less fit parasite populations and overgrow under drug pressure. Use of partially effective anti-malarial agents for IPTp may exacerbate malaria infections in the setting of widespread drug resistance.</p>
         </abstract>
         <kwd-group>
            <kwd>evolution of drug resistance</kwd>
            <kwd>intermittent preventive treatment in pregnancy</kwd>
            <kwd>malaria drug resistance</kwd>
            <kwd>pregnancy malaria</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d6601e254a1310">
            <label>1</label>
            <mixed-citation id="d6601e261" publication-type="other">
Parise ME, et al. (1998) Efficacy of sulfadoxine-pyrimethamine for prevention of
placental malaria in an area of Kenya with a high prevalence of malaria and human
immunodeficiency virus infection. Am J Trop Med Hyg 59:813-822.</mixed-citation>
         </ref>
         <ref id="d6601e274a1310">
            <label>2</label>
            <mixed-citation id="d6601e281" publication-type="other">
Rogerson SJ, et al. (2000) Intermittent sulfadoxine-pyrimethamine in pregnancy: Ef-
fectiveness against malaria morbidity in Blantyre, Malawi, in 1997-99. Trans RSoc Trop
Med Hyg 94:549-553.</mixed-citation>
         </ref>
         <ref id="d6601e294a1310">
            <label>3</label>
            <mixed-citation id="d6601e301" publication-type="other">
Schultz U, et al. (1994) The efficacy of antimalanal regimens containing surraaoxme-
pyrimethamine and/or chloroquine in preventing peripheral and placental Plasmo-
dium falciparum infection among pregnant women in Malawi. Am J Trop Med Hyg
51:515-522.</mixed-citation>
         </ref>
         <ref id="d6601e317a1310">
            <label>4</label>
            <mixed-citation id="d6601e324" publication-type="other">
Shulman CE, et al. (1999) Intermittent sulphadoxine-pyrimethamine to prevent severe
anaemia secondary to malaria in pregnancy: A randomised placebo-controlled trial.
Lancet 353:632-636.</mixed-citation>
         </ref>
         <ref id="d6601e338a1310">
            <label>5</label>
            <mixed-citation id="d6601e345" publication-type="other">
Verhoeff FH, et al. (1998) An evaluation of the effects of intermittent sulfadoxine-
pyrimethamine treatment in pregnancy on parasite clearance and risk of low birth-
weight in rural Malawi. Ann Trop Med Parasitol 92:141-150.</mixed-citation>
         </ref>
         <ref id="d6601e358a1310">
            <label>6</label>
            <mixed-citation id="d6601e365" publication-type="other">
ter Kuile FO, van Eijk AM, Filler SJ (2007) Effect of sulfadoxine-pyrimetnamine resis-
tance on the efficacy of intermittent preventive therapy for malaria control during
pregnancy: A systematic review. J Am Med Assoc 297:2603-2616.</mixed-citation>
         </ref>
         <ref id="d6601e378a1310">
            <label>7</label>
            <mixed-citation id="d6601e385" publication-type="other">
Gregson A, Plowe CV (2005) Mechanisms of resistance of malaria parasites to antifo-
lates. Pharmacol Rev 57:1 17-145.</mixed-citation>
         </ref>
         <ref id="d6601e395a1310">
            <label>8</label>
            <mixed-citation id="d6601e402" publication-type="other">
Brooks DR, et al. (1994) Sequence variation of the hydroxymetnyidinyaropienn pyro-
phosphokinase: Dihydropteroate synthase gene in lines of the human malaria parasite,
Plasmodium falciparum, with differing resistance to sulfadoxine. Eur J Biochem
224:397-405.</mixed-citation>
         </ref>
         <ref id="d6601e418a1310">
            <label>9</label>
            <mixed-citation id="d6601e425" publication-type="other">
Gesase S, et al. (2009) High resistance of Plasmodium falciparum to sulphadoxine/
pyrimethamine in northern Tanzania and the emergence of dhps resistance mutation
at Codon 581. PLoS ONE 4:e4569.</mixed-citation>
         </ref>
         <ref id="d6601e438a1310">
            <label>10</label>
            <mixed-citation id="d6601e445" publication-type="other">
Jelinek T, Kilian AH, Kabagambe G, von Sonnenburg F (1999) Plasmodium taiciparum
resistance to sulfadoxine/pyrimethamine in Uganda: Correlation with polymorphisms
in the dihydrofolate reducíase and dihydropteroate synthetase genes. Am J Trop Med
Hyg 61:463-466.</mixed-citation>
         </ref>
         <ref id="d6601e462a1310">
            <label>11</label>
            <mixed-citation id="d6601e469" publication-type="other">
Marks F, et al. (2005) Parasitological rebound effect and emergence of pyrimetnamme
resistance in Plasmodium falciparum after single-dose sulfadoxine-pyrimethamine.
J Infect Dis 192:1962-1965.</mixed-citation>
         </ref>
         <ref id="d6601e482a1310">
            <label>12</label>
            <mixed-citation id="d6601e489" publication-type="other">
Wang P, et al. (1997) Resistance to antifolates in Plasmodium falciparum monitored by
sequence analysis of dihydropteroate synthetase and dihydrofolate reductase alleles
in a large number of field samples of diverse origins. Mol Biochem Parasitol 89:161-
177.</mixed-citation>
         </ref>
         <ref id="d6601e505a1310">
            <label>13</label>
            <mixed-citation id="d6601e512" publication-type="other">
Mockenhaupt FP, et al. (2007) Markers of sulfadoxine-pyrimethamine-resistant Plas-
modium falciparum in placenta and circulation of pregnant women. Antimicrob
Agents Chemother 51:332-334.</mixed-citation>
         </ref>
         <ref id="d6601e525a1310">
            <label>14</label>
            <mixed-citation id="d6601e532" publication-type="other">
Mockenhaupt FP, et al. (2008) Rapid increase in the prevalence of sulfadoxine-
pyrimethamine resistance among Plasmodium falciparum isolated from pregnant
women in Ghana. J Infect Dis 198:1545-1549.</mixed-citation>
         </ref>
         <ref id="d6601e545a1310">
            <label>15</label>
            <mixed-citation id="d6601e552" publication-type="other">
O'Meara WP, Smith DL, McKenzie FE (2006) Potential impact of intermittent preven-
tive treatment (IPT) on spread of drug-resistant malaria. PLoS Med 3:e141.</mixed-citation>
         </ref>
         <ref id="d6601e562a1310">
            <label>16</label>
            <mixed-citation id="d6601e569" publication-type="other">
Fried M, Nosten F, Brockman A, Brabin BJ, Duffy PE (1998) Maternal antibodies block
malaria. Nature 395:851-852.</mixed-citation>
         </ref>
         <ref id="d6601e580a1310">
            <label>17</label>
            <mixed-citation id="d6601e587" publication-type="other">
Wargo AR, Huijben S, de Roode JC, Shepherd J, Read AF (2007) Competitive release and
facilitation of drug-resistant parasites after therapeutic chemotherapy in a rodent
malaria model. Proc Nati Acad Sci USA 104:19914-19919.</mixed-citation>
         </ref>
         <ref id="d6601e600a1310">
            <label>18</label>
            <mixed-citation id="d6601e607" publication-type="other">
Mutabingwa TK, et al. (2005) Maternal malaria and gravidity interact to modify infant
susceptibility to malaria. PLoS Med 2:e407.</mixed-citation>
         </ref>
         <ref id="d6601e617a1310">
            <label>19</label>
            <mixed-citation id="d6601e624" publication-type="other">
Fried M, Duffy PE (1998) Maternal malaria and parasite adhesion. J Mol Med 76: 162-171.</mixed-citation>
         </ref>
         <ref id="d6601e631a1310">
            <label>20</label>
            <mixed-citation id="d6601e638" publication-type="other">
Snounou G, et al. (1999) Biased distribution of msp1 and msp2 allelic variants in Plasmo-
dium falciparum populations in Thailand. Trans R Soc Trop Med Hyg 93:369-374.</mixed-citation>
         </ref>
         <ref id="d6601e648a1310">
            <label>21</label>
            <mixed-citation id="d6601e655" publication-type="other">
Bulmer JN, Rasheed FN, Francis N, Morrison L, Greenwood BM (1993) Placental malaria.
I. Pathological classification. Histopathology 22:21 1-218.</mixed-citation>
         </ref>
         <ref id="d6601e665a1310">
            <label>22</label>
            <mixed-citation id="d6601e672" publication-type="other">
Garnham PCC (1938) The placenta in malaria with special reference to reticulo-
endothelial immunity. Trans R Soc Trop Med Hyg 13-48.</mixed-citation>
         </ref>
         <ref id="d6601e683a1310">
            <label>23</label>
            <mixed-citation id="d6601e690" publication-type="other">
Menendez C, et al. (2000) The impact of placental malaria on gestational age and birth
weight. J Infect Dis 181:1740-1745.</mixed-citation>
         </ref>
         <ref id="d6601e700a1310">
            <label>24</label>
            <mixed-citation id="d6601e707" publication-type="other">
Zhou Z, et al. (2006) Pyrosequencing, a high-throughput method for detecting single
nucleotide polymorphisms in the dihydrofolate reducíase and dihydropteroate syn-
thetase genes of Plasmodium falciparum. J Clin Microbio! 44:3900-3910.</mixed-citation>
         </ref>
         <ref id="d6601e720a1310">
            <label>25</label>
            <mixed-citation id="d6601e727" publication-type="other">
de Roode JC, Helinski ME, Anwar MA, Read AF (2005) Dynamics of multiple infection
and within-host competition in genetically diverse malaria infections. Am Nat 166:531-
542.</mixed-citation>
         </ref>
         <ref id="d6601e740a1310">
            <label>26</label>
            <mixed-citation id="d6601e747" publication-type="other">
Kublin JG, et al. (2002) Molecular markers for failure of sulfadoxine-pyrimethamine
and chlorproguanil-dapsone treatment of Plasmodium falciparum malaria. J Infect Dis
185:380-388.</mixed-citation>
         </ref>
         <ref id="d6601e760a1310">
            <label>27</label>
            <mixed-citation id="d6601e767" publication-type="other">
Schneider P, Chan BH, Reece SE, Read AF (2008) Does the drug sensitivity of malaria
parasites depend on their virulence? Malar J 7:257.</mixed-citation>
         </ref>
         <ref id="d6601e777a1310">
            <label>28</label>
            <mixed-citation id="d6601e784" publication-type="other">
Akim NI, et al. (2000) Dynamics of P. falciparum gametocytemia in symptomatic
patients in an area of intense perennial transmission in Tanzania. Am J Trop Med Hyg
63:199-203.</mixed-citation>
         </ref>
         <ref id="d6601e798a1310">
            <label>29</label>
            <mixed-citation id="d6601e805" publication-type="other">
Fried M, Duffy PE (1996) Adherence of Plasmodium falciparum to chondroitin sulfate
A in the human placenta. Science 272:1502-1504.</mixed-citation>
         </ref>
         <ref id="d6601e815a1310">
            <label>30</label>
            <mixed-citation id="d6601e822" publication-type="other">
Morris C (1941) The determination of sulphanilamide and its derivatives. Biochem J
35:952-959.</mixed-citation>
         </ref>
         <ref id="d6601e832a1310">
            <label>31</label>
            <mixed-citation id="d6601e839" publication-type="other">
Fansidar Product Information. Roche Pharmaceuticals. Accessed August 16, 2004.</mixed-citation>
         </ref>
         <ref id="d6601e846a1310">
            <label>32</label>
            <mixed-citation id="d6601e853" publication-type="other">
Green MD, et al. (2007) Pharmacokinetics of sulfadoxine-pyrimethamine in HIV-
infected and uninfected pregnant women in Western Kenya. J Infect Dis 196:1403-
1408.</mixed-citation>
         </ref>
         <ref id="d6601e866a1310">
            <label>33</label>
            <mixed-citation id="d6601e873" publication-type="other">
Sarikabhuti B, et al. (1 988) Plasma concentrations of sulf adoxine in healthy and malaria
infected Thai subjects. Acta Trop 45:217-224.</mixed-citation>
         </ref>
         <ref id="d6601e883a1310">
            <label>34</label>
            <mixed-citation id="d6601e890" publication-type="other">
Duraisingh MT, Curtis J, Warhurst DC (1998) Plasmodium falciparum: Detection of
polymorphisms in the dihydrofolate reducíase and dihydropteroate synthetase genes
by PCR and restriction digestion. Exp Parasitol 89:1-8.</mixed-citation>
         </ref>
         <ref id="d6601e904a1310">
            <label>35</label>
            <mixed-citation id="d6601e911" publication-type="other">
Paul RE, et al. (1995) Mating patterns in malaria parasite populations of Papua New
Guinea. Science 269:1709-1711.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
