<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">scanjecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100661</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Scandinavian Journal of Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03470520</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14679442</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40783307</article-id>
         <title-group>
            <article-title>Measurement Error in Education and Growth Regressions</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Miguel</given-names>
                  <surname>Portela</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rob</given-names>
                  <surname>Alessie</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Coen</given-names>
                  <surname>Teulings</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">112</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40035338</issue-id>
         <fpage>618</fpage>
         <lpage>639</lpage>
         <permissions>
            <copyright-statement>© 2010 The editors of the Scandinavian Journal of Economics</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40783307"/>
         <abstract>
            <p>The use of the perpetual inventory method for the construction of education data per country leads to systematic measurement error. This paper analyzes its effect on growth regressions. We suggest a methodology for correcting this error. The standard attenuation bias suggests that using these corrected data would lead to a higher coefficient. Our regressions reveal the opposite. We discuss why this is the case.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d186e241a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d186e248" publication-type="other">
Kyriacou (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d186e254" publication-type="other">
Nehru et al. (1995).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d186e260" publication-type="other">
Psacharopoulos and Amagada (1986),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d186e267" publication-type="other">
De la Fuente and Doménech (2002,
p. 6)</mixed-citation>
            </p>
         </fn>
         <fn id="d186e277a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d186e284" publication-type="other">
httpv/www.eeg.uminho.pt/economia/mangelo/education/.</mixed-citation>
            </p>
         </fn>
         <fn id="d186e291a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d186e298" publication-type="other">
Ferreira et al. (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d186e304" publication-type="other">
Pritchett
(2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d186e314a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d186e321" publication-type="other">
Topel (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d186e327" publication-type="other">
Krueger and
Lindahl (2001).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>&lt;bold&gt;References&lt;/bold&gt;</title>
         <ref id="d186e346a1310">
            <mixed-citation id="d186e350" publication-type="other">
Barro, R., and Lee, J.-W. (1993), International Comparisons of Educational Attainment,
Journal of Monetary Economics 32 (3), 363-394.</mixed-citation>
         </ref>
         <ref id="d186e360a1310">
            <mixed-citation id="d186e364" publication-type="other">
Barro, R., and Lee, J.-W. (1996), International Measures of Schooling Years and Schooling
Quality, American Economic Review 86 (2), 218-223.</mixed-citation>
         </ref>
         <ref id="d186e374a1310">
            <mixed-citation id="d186e378" publication-type="other">
Barro, R., and Lee, J.-W. (2001), International Data on Educational Attainment: Updates and
Implications, Oxford Economic Papers 53 (3), 541-563.</mixed-citation>
         </ref>
         <ref id="d186e388a1310">
            <mixed-citation id="d186e392" publication-type="other">
Barro, R., and Sala-i-Martin, X. (1999), Economic Growth, MIT Press, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="d186e400a1310">
            <mixed-citation id="d186e404" publication-type="other">
Benhabib, J., and Spiegel, M. (1994), The Role of Human Capital in Economic Development:
Evidence from Aggregate Cross-country Data, Journal of Monetary Economics 34 (2),
143-173.</mixed-citation>
         </ref>
         <ref id="d186e417a1310">
            <mixed-citation id="d186e421" publication-type="other">
Cohen, D., and Soto, M. (2007), Growth and Human Capital: Good Data, Good Results,
Journal of Economic Growth 12 (1), 51-76.</mixed-citation>
         </ref>
         <ref id="d186e431a1310">
            <mixed-citation id="d186e435" publication-type="other">
de la Fuente, A., and Doménech, R. (2002), Human Capital in Growth Regressions: How
Much Difference Does Data Quality Make? An Update and Further Results, CEPR Dis-
cussion Paper no. 3587, London.</mixed-citation>
         </ref>
         <ref id="d186e448a1310">
            <mixed-citation id="d186e452" publication-type="other">
de la Fuente, A., and Doménech, R. (2006), Human Capital in Growth Regressions: How
Much Difference Does Data Quality Make?, Journal of the European Economic Associa-
tion 4 (1) 1-36</mixed-citation>
         </ref>
         <ref id="d186e465a1310">
            <mixed-citation id="d186e469" publication-type="other">
Ferreira, P., Issler, J., and de Abreu Pessôa, S. (2004), Testing Production Functions Used in
Empirical Growth Studies. Economics Letters 83 (1) 29-35.</mixed-citation>
         </ref>
         <ref id="d186e479a1310">
            <mixed-citation id="d186e483" publication-type="other">
Heston, A., Summers, R., and Aten, B. (2002), Penn World Table Version 6.1, Center for
International Comparisons at the University of Pennsylvania (CICUP), October, Philadel-
phia.</mixed-citation>
         </ref>
         <ref id="d186e497a1310">
            <mixed-citation id="d186e501" publication-type="other">
Krueger, A., and Lindahl, M. (2001), Education for Growth: Why and For Whom?, Journal
of Economic Literature 39 (4), 1101-1136.</mixed-citation>
         </ref>
         <ref id="d186e511a1310">
            <mixed-citation id="d186e515" publication-type="other">
Kyriacou, G. (1991), Level and Growth Effects of Human Capital: A Cross-country Study of
the Convergence Hypothesis, New York University Economic Research Report no. 91-26.</mixed-citation>
         </ref>
         <ref id="d186e525a1310">
            <mixed-citation id="d186e529" publication-type="other">
Lucas, R. (1988), On the Mechanics of Economic Development, Journal of Monetary Eco-
nomics 22 (1) 3-42.</mixed-citation>
         </ref>
         <ref id="d186e539a1310">
            <mixed-citation id="d186e543" publication-type="other">
Mankiw, N., Romer, D., and Weil, D. (1992), A Contribution to the Empirics of Economic
Growth, Quarterly Journal of Economics 107 (2), 407-438.</mixed-citation>
         </ref>
         <ref id="d186e553a1310">
            <mixed-citation id="d186e557" publication-type="other">
Nehru, V, Swanson, E., and Dubey, A. (1995), A New Database on Human Capital Stocks
in Developing and Industrial Countries: Sources, Methodology and Results, Journal of
Development Economics 46 (2), 379-401.</mixed-citation>
         </ref>
         <ref id="d186e570a1310">
            <mixed-citation id="d186e574" publication-type="other">
Nelson, R., and Phelps, E. (1966), Investment in Humans, Technology Diffusion and Eco-
nomic Growth, American Economic Review 56 (2), 69-75.</mixed-citation>
         </ref>
         <ref id="d186e585a1310">
            <mixed-citation id="d186e589" publication-type="other">
Pritchett, L. (2001), Where Has All the Education Gone?, World Bank Economic Review 15
(3), 367-391.</mixed-citation>
         </ref>
         <ref id="d186e599a1310">
            <mixed-citation id="d186e603" publication-type="other">
Pritchett, L. (2006), Does Learning to Add Up Add Up? The Returns to Schooling in
Aggregate Data, in E. Hanushek and F. Welch (eds.), Handbook of Education Economics,
Vol. 1, North-Holland, Amsterdam, 637-695.</mixed-citation>
         </ref>
         <ref id="d186e616a1310">
            <mixed-citation id="d186e620" publication-type="other">
Psacharopoulos, G., and Amagada, A. M. (1986), The Educational Composition of the
Labour Force: An International Comparison, International Labour Review 125 (5),
561-574.</mixed-citation>
         </ref>
         <ref id="d186e633a1310">
            <mixed-citation id="d186e637" publication-type="other">
Summers, R., and Heston, A. (1991), The Penn World Table (Mark 5): An Expanded Set of
International Comparisons, 1950-1988, Quarterly Journal of Economics 106 (2), 280-315.</mixed-citation>
         </ref>
         <ref id="d186e647a1310">
            <mixed-citation id="d186e651" publication-type="other">
Teulings, C, and van Rens, T. (2008), Education, Growth, and Income Inequality, Review of
Economics and Statistics 90 (1), 89-104.</mixed-citation>
         </ref>
         <ref id="d186e661a1310">
            <mixed-citation id="d186e665" publication-type="other">
Topel, R. (1999), Labor Markets and Economic Growth, in O. Ashenfelter and D. Card
(cds.), Handbook of Labor Economics, Vol. 3C, North-Holland, Amsterdam, 2943-2984.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
