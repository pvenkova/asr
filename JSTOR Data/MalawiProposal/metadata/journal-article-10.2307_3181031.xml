<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">intefamiplanper2</journal-id>
         <journal-id journal-id-type="jstor">j100566</journal-id>
         <journal-title-group>
            <journal-title>International Family Planning Perspectives</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Alan Guttmacher Institute</publisher-name>
         </publisher>
         <issn pub-type="ppub">01903187</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3181031</article-id>
         <title-group>
            <article-title>Does Discussion of Family Planning Improve Knowledge of Partner's Attitude toward Contraceptives?</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Laurie F.</given-names>
                  <surname>DeRose</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>F. Nii-Amoo</given-names>
                  <surname>Dodoo</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Alex C.</given-names>
                  <surname>Ezeh</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Tom O.</given-names>
                  <surname>Owuor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">30</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i359320</issue-id>
         <fpage>87</fpage>
         <lpage>93</lpage>
         <page-range>87-93</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The Alan Guttmacher Institute</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3181031"/>
         <abstract>
            <p> Context: Results from an analysis of 1998 Demographic and Health Survey (DHS) data from Kenya, where the approval rate of family planning is 90%, have cast doubt on the assumption that spousal discussion improves knowledge of partner's attitude toward family planning. However, it is not known whether this finding also applies to contexts more typical of Sub-Saharan Africa, where approval is not as high. Methods: DHS data from 21 Sub-Saharan African countries were used to assess the relationship between spousal discussion and correct reporting of partner's attitude toward family planning. Multivariate analyses of data from Chad were conducted to further examine this relationship in a setting where contraceptive approval was not high. Results: In every country, the proportion of women correctly reporting their spouse's disapproval of contraception was smaller among those who had discussed family planning with their husband than among those who had never done so. However, in an analysis of Chad data that included women who did not know their husband's attitude toward contraception, proportions of women correctly citing their husband's attitude were larger if discussion had occurred than if it had not, regardless of the husband's actual approval status. In multivariate analyses of Chad data that controlled for women's demographic characteristics, discussion was positively associated with correct reporting of husband's approval, but negatively associated with correct reporting of his disapproval. Conclusions: Partner discussion does not necessarily mean an increase in knowledge of a partner's contraceptive attitudes. Therefore, anticipated reductions in unmet need for contraception through improvements in spousal discussion may be overstated. /// [Spanish] Contexto: Los resultados obtenidos de un análisis de los datos de la Encuesta Demográfica y de Salud (EDS) de 1998, en Kenya, donde la tasa de aprobación de los servicios de planificación familiar asciende al 90%, han planteado ciertas dudas sobre el supuesto de que el intercambio de opinión entre los cónyuges mejora el conocimiento de la actitud de la pareja con respecto a la planificación familiar. Sin embargo, no se sabe si este resultado también se aplica a otros contextos más típicos del África Subsahariana, donde la aprobación de la planificación familiar no es tan elevada. Métodos: Se usaron los datos de las EDS realizadas en 21 países del África Subsahariana para evaluar la relación que existe entre el intercambio de opinión entre los cónyuges y la información correcta sobre la actitud de la pareja con respecto a la planificación familiar. Se hicieron análisis multivariados con datos de la EDS realizada en el Chad para examinar a fondo esta relación en un entorno donde la aprobación del uso de anticonceptivos no era elevada. Resultados: En todos los países, el porcentaje de mujeres que informaron correctamente acerca del rechazo manifestado por su cónyuge con respecto al uso de anticonceptivos fue menor entre aquellas que habían intercambiado opinión con su pareja que entre las que nunca lo habían hecho. Sin embargo, en el análisis de datos de Chad, que incluía las respuestas de las mujeres que contestaban "no sé", el porcentaje de mujeres que mencionaban correctamente la actitud de su cónyuge era mayor entre las que habían intercambiado puntos de vista, fuere cual fuere la actitud del cónyuge. Al realizar un análisis multivariado con los datos de Chad, en el cual se controlaron las características demográficas de las mujeres, el intercambio de opinión de la pareja estuvo positivamente relacionado con la información correcta sobre la aprobación del hombre, aunque negativamente asociada con la información correcta sobre su rechazo. Conclusiones: El intercambio de opinión entre los cónyuges no significa necesariamente un aumento del conocimiento de la actitud de la pareja con respecto a la anticoncepción. Por lo tanto, quizá es exagerada la conclusión de que se puede lograr descensos en la necesidad insatisfecha de anticoncepción si hay un intercambio de opinión entre los cónyuges. /// [French] Contexte: Les résultats d'une analyse des données de l'Enquête démographique et de santé (EDS) de 1998 du Kenya, où le taux d'approbation de la planification familiale atteint 90%, remettent en question l'hypothèse selon laquelle la discussion entre époux améliore la connaissance de l'attitude du conjoint à l'égard du planning familial. On ignore toutefois si cette observation s'applique aussi aux contextes plus typiques d'Afrique subsaharienne, où les taux d'approbation ne sont pas aussi élevés. Méthodes: Les données EDS de 21 pays d'Afrique subsaharienne ont servi à évaluer le rapport entre la discussion conjugale et la déclaration correcte de l'attitude du partenaire vis-à-vis du planning familial. Les données du Tchad ont été soumises à des analyses multivariées afin d'examiner plus avant ce rapport dans un contexte caractérisé par la faible approbation de la contraception. Résultats: Dans chaque pays, la proportion de femmes ayant déclaré, correctement, que leur époux n'approuvait pas la contraception s'est avérée moindre parmi celles qui avaient parlé du planning familial avec leur conjoint que parmi celles qui n'avaient jamais abordé la question. Cependant, dans une analyse des données tchadiennes incluant les femmes ayant répondu qu'elles ne savaient pas, les proportions de celles ayant correctement rapporté l'attitude de leur mari étaient supérieures si la discussion avait eu lieu, indépendamment de l'approbation effective ou non du mari. Dans les analyses multivariées des données tchadiennes tenant compte des caractéristiques démographiques des femmes, la discussion s'est avérée positivement associée à la déclaration correcte de l'approbation du conjoint, mais négativement associée à la déclaration correcte de sa désapprobation. Conclusions: La discussion avec le partenaire ne mène pas nécessairement à une meilleure connaissance de l'attitude de ce dernier à l'égard de la contraception. Les réductions anticipées du besoin de contraception non satisfait à travers l'amélioration de la discussion conjugale risquent dès lors d'être exagérées. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d491e208a1310">
            <label>1</label>
            <mixed-citation id="d491e217" publication-type="journal">
Chaudhury RH, Female labor force status and fertility behavior in
Bangladesh: search for policy interventions, Bangladesh Development
Studies, 1983, 11(3):59-102<person-group>
                  <string-name>
                     <surname>Chaudhury</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>59</fpage>
               <volume>11</volume>
               <source>Bangladesh Development Studies</source>
               <year>1983</year>
            </mixed-citation>
            <mixed-citation id="d491e254" publication-type="journal">
Lasee A and Becker S, Husband-wife
communication about family planning and contraceptive use in Kenya,
International Family Planning Perspectives, 1997, 23(1):15-20 &amp; 33<object-id pub-id-type="doi">10.2307/2950781</object-id>
               <fpage>15</fpage>
            </mixed-citation>
            <mixed-citation id="d491e273" publication-type="journal">
Mahmood N and Ringheim K, Knowledge, approval and communica-
tion about family planning as correlates of desired fertility among spous-
es in Pakistan, International Family Planning Perspectives, 1997, 23(3):
122-129 &amp; 145<object-id pub-id-type="doi">10.2307/2950768</object-id>
               <fpage>122</fpage>
            </mixed-citation>
            <mixed-citation id="d491e295" publication-type="journal">
Ngom P, Men's unmet need for family planning:
implications for African fertility transitions, Studies in Family Planning,
1997,28(3):192-200<object-id pub-id-type="doi">10.2307/2137887</object-id>
               <fpage>192</fpage>
            </mixed-citation>
            <mixed-citation id="d491e313" publication-type="journal">
Omondi-Odhiambo, Men's participation in family
planning decisions in Kenya, Population Studies, 1997, 51(1):29-40<object-id pub-id-type="jstor">10.2307/2175071</object-id>
               <fpage>29</fpage>
            </mixed-citation>
            <mixed-citation id="d491e328" publication-type="journal">
Oni GA and McCarthyJ, Family planning knowledge, attitudes and
practices of males in Ilorin, Nigeria, International Family Planning
Perspectives, 1991, 17(2):50-54 &amp; 64<person-group>
                  <string-name>
                     <surname>McCarthy</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>50</fpage>
               <volume>17</volume>
               <source>International Family Planning Perspectives</source>
               <year>1991</year>
            </mixed-citation>
            <mixed-citation id="d491e365" publication-type="journal">
Piotrow PT et al., Changing men's
attitude and behavior: the Zimbabwe male motivation project, Studies
in Family Planning, 1992, 23(6):365-375<object-id pub-id-type="doi">10.2307/1966894</object-id>
               <fpage>365</fpage>
            </mixed-citation>
            <mixed-citation id="d491e383" publication-type="journal">
Podhisita C, Gender and
decision making in family formation and planning: achievement and
future direction,Journal of Population and Social Studies, 1998, 6(1):1-27;<person-group>
                  <string-name>
                     <surname>Podhisita</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>1</fpage>
               <volume>6</volume>
               <source>Journal of Population and Social Studies</source>
               <year>1998</year>
            </mixed-citation>
            <mixed-citation id="d491e421" publication-type="journal">
Salway S, How attitudes toward family planning and discussion between
wives and husbands affect contraceptive use in Ghana, International
Family Planning Perspectives, 1994, 20(2):44-47 &amp; 74<object-id pub-id-type="doi">10.2307/2133433</object-id>
               <fpage>44</fpage>
            </mixed-citation>
            <mixed-citation id="d491e439" publication-type="book">
Toure L, Male
Involvement in Family Planning: A Review of the Literature and Selected
Program Initiatives in Africa, Washington, DC: Support for Analysis and
Research in Africa, and U.S. Agency for International Development, 1996.<person-group>
                  <string-name>
                     <surname>Toure</surname>
                  </string-name>
               </person-group>
               <source>Male Involvement in Family Planning: A Review of the Literature and Selected Program Initiatives in Africa</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d491e471a1310">
            <label>2</label>
            <mixed-citation id="d491e478" publication-type="journal">
BongaartsJ and BruceJ, The causes of unmet need for contracep-
tion and the social content of services, Studies in Family Planning, 1995,
26(2):57-75.<object-id pub-id-type="doi">10.2307/2137932</object-id>
               <fpage>57</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e497a1310">
            <label>3</label>
            <mixed-citation id="d491e504" publication-type="journal">
Ibid.  </mixed-citation>
            <mixed-citation id="d491e512" publication-type="journal">
Hollerbach PE, Power in families, communication, and fertili-
ty decision-making, Population and Environment, 1980, 3(2):146-173<person-group>
                  <string-name>
                     <surname>Hollerbach</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>146</fpage>
               <volume>3</volume>
               <source>Population and Environment</source>
               <year>1980</year>
            </mixed-citation>
            <mixed-citation id="d491e546" publication-type="journal">
Sathar ZA and CasterlineJB, The onset of fertility transition in Pakistan,
Population and Development Review, 1998, 24(4):773-796<object-id pub-id-type="doi">10.2307/2808024</object-id>
               <fpage>773</fpage>
            </mixed-citation>
            <mixed-citation id="d491e562" publication-type="journal">
Casterline
JB, Perez AE and Biddlecom AE, Factors underlying unmet need for
family planning in the Philippines, Studies in Family Planning, 1997,
28(3):173-191<person-group>
                  <string-name>
                     <surname>Perez</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>173</fpage>
               <volume>28</volume>
               <source>Studies in Family Planning</source>
               <year>1997</year>
            </mixed-citation>
            <mixed-citation id="d491e602" publication-type="book">
Touré L, 1996, op. cit. (see reference 1)  </mixed-citation>
            <mixed-citation id="d491e610" publication-type="journal">
van de
Walle F and Maiga M, Family planning in Bamako, Mali, International
Family Planning Perspectives, 1991, 17(3):84-90 &amp; 99.<person-group>
                  <string-name>
                     <surname>Maiga</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>84</fpage>
               <volume>17</volume>
               <source>International Family Planning Perspectives</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d491e648a1310">
            <label>4</label>
            <mixed-citation id="d491e655" publication-type="journal">
Lasee A and Becker S, 1997, op. cit. (see reference 1)  </mixed-citation>
            <mixed-citation id="d491e663" publication-type="journal">
Oheneba-Sakyi
Y and Takyi BK, Effects of couples' characteristics on contraceptive use
in sub-SaharanAfrica: the Ghanaian example,Journal of Biosocial Science,
1997, 29(1):33-49<person-group>
                  <string-name>
                     <surname>Oheneba</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>33</fpage>
               <volume>29</volume>
               <source>Journal of Biosocial Science</source>
               <year>1997</year>
            </mixed-citation>
            <mixed-citation id="d491e703" publication-type="journal">
Oni GA and McCarthyJ, 1991, op. cit. (see refer-
ence 1)  </mixed-citation>
            <mixed-citation id="d491e715" publication-type="journal">
Salway S, 1994, op. cit. (see reference 1)  </mixed-citation>
            <mixed-citation id="d491e723" publication-type="journal">
Sharan M and
Valente TW, Spousal communication and family planning adoption:
effects of a radio drama series in Nepal, International Family Planning
Perspectives, 2002, 28(1):16-25.<object-id pub-id-type="doi">10.2307/3088271</object-id>
               <fpage>16</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e747a1310">
            <label>5</label>
            <mixed-citation id="d491e754" publication-type="journal">
Beckman LJ, Communication, power, and the influence of social net-
works in couple decisions on fertility, in: Bulatao RA and Lee RD, eds.,
Determinants of Fertility in Developing Countries, New York: Academic
Press, 1983, pp. 415-443<person-group>
                  <string-name>
                     <surname>Beckman</surname>
                  </string-name>
               </person-group>
               <fpage>415</fpage>
               <source>Determinants of Fertility in Developing Countries</source>
               <year>1983</year>
            </mixed-citation>
            <mixed-citation id="d491e788" publication-type="book">
Hill R, StycosJM and Back KW, The Family
and Population Control: A Puerto Rican Experiment in Social Change,
Chapel Hill, NC, USA: University of North Carolina Press, 1959<person-group>
                  <string-name>
                     <surname>Back</surname>
                  </string-name>
               </person-group>
               <source>The Family and Population Control: A Puerto Rican Experiment in Social Change</source>
               <year>1959</year>
            </mixed-citation>
            <mixed-citation id="d491e816" publication-type="journal">
Yaukey D, Griffiths W and Roberts BJ, Couple concurrence and empathy
on birth control motivation in Dacca, East Pakistan, American Sociological
Review, 1967, 32(5):716-726.<object-id pub-id-type="doi">10.2307/2092020</object-id>
               <fpage>716</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e835a1310">
            <label>6</label>
            <mixed-citation id="d491e842" publication-type="book">
BlancAK et al., Negotiating Reproductive Outcomes in Uganda, Calverton,
MD, USA and Kampala, Uganda: Macro International and Institute of
Statistics and Applied Economics, 1996<source>Negotiating Reproductive Outcomes in Uganda</source>
               <year>1996</year>
            </mixed-citation>
            <mixed-citation id="d491e860" publication-type="journal">
Castle S et al., A qualitative
study of clandestine contraceptive use in urban Mali, Studies in Family
Planning, 1999, 30(3):231-248<object-id pub-id-type="jstor">10.2307/172199</object-id>
               <fpage>231</fpage>
            </mixed-citation>
            <mixed-citation id="d491e878" publication-type="journal">
Ezeh A, The influence of spouses
over each other's contraceptive attitudes in Ghana, Studies in Family
Planning, 1993, 24(3):163-174.<object-id pub-id-type="doi">10.2307/2939231</object-id>
               <fpage>163</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e897a1310">
            <label>7</label>
            <mixed-citation id="d491e904" publication-type="conference">
Tagoe-Darko E, Adolescent reproductive health: the neglected role
of traditional teachings and practices in Ghana, paper presented at the
annual meeting of the Population Association of America, Washington,
DC, Mar. 27-29, 1997.<person-group>
                  <string-name>
                     <surname>Tagoe</surname>
                  </string-name>
               </person-group>
               <fpage>27</fpage>
               <source>Population Association of America</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d491e939a1310">
            <label>8</label>
            <mixed-citation id="d491e946" publication-type="journal">
Dodoo FN-A, Ezeh AC and Owuor TO, Some evidence against the
assumption that approval of family planning is associated with frequency
of spouses' discussion of the subject, Population Studies, 2001,
55(2):195-198.<object-id pub-id-type="jstor">10.2307/3092963</object-id>
               <fpage>195</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e969a1310">
            <label>9</label>
            <mixed-citation id="d491e976" publication-type="journal">
Sharan M and Valente IW, 2002, op. cit. (see reference 4).  </mixed-citation>
         </ref>
         <ref id="d491e985a1310">
            <label>10</label>
            <mixed-citation id="d491e994" publication-type="journal">
Beckman LJ, 1983, op. cit. (see reference 5)  </mixed-citation>
            <mixed-citation id="d491e1002" publication-type="book">
Caldwell B, Female ed-
ucation, autonomy and fertility in Sri Lanka, in:Jeffery R and Basu AM,
eds., Girls' Schooling, Women's Autonomy and Fertility Change in South
Asia, New Delhi: Sage Publications, 1996, pp. 269-321<person-group>
                  <string-name>
                     <surname>Caldwell</surname>
                  </string-name>
               </person-group>
               <fpage>269</fpage>
               <source>Women's Autonomy and Fertility Change in South Asia</source>
               <year>1996</year>
            </mixed-citation>
            <mixed-citation id="d491e1037" publication-type="book">
ClelandJ and
Jejeebhoy S, Maternal schooling and fertility: evidence from censuses
and surveys, in: ibid., pp. 72-106<person-group>
                  <string-name>
                     <surname>Cleland</surname>
                  </string-name>
               </person-group>
               <fpage>72</fpage>
               <source>Women's Autonomy and Fertility Change in South Asia</source>
               <year>1996</year>
            </mixed-citation>
            <mixed-citation id="d491e1068" publication-type="journal">
HollosM and Larsen U, From lineage to conjugality:
the social context of fertility decisions among the Pare of northern
Tanzania, Social Science &amp; Medicine, 1997453361<person-group>
                  <string-name>
                     <surname>Hollos</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>361</fpage>
               <volume>45</volume>
               <source>Social Science &amp; Medicine,</source>
               <year>1997</year>
            </mixed-citation>
            <mixed-citation id="d491e1105" publication-type="book">
Oheneba-
Sakyi Y et al., Female Autonomy, Decision Making, and Demographic
Behavior Among Couples in Ghana, Potsdam, NY, USA, and Accra, Ghana:
State University of NewYork at Potsdam and University of Ghana, 1995<person-group>
                  <string-name>
                     <surname>Oheneba</surname>
                  </string-name>
               </person-group>
               <source>Female Autonomy, Decision Making, and Demographic Behavior Among Couples in Ghana</source>
               <year>1995</year>
            </mixed-citation>
            <mixed-citation id="d491e1136" publication-type="journal">
TuladharJM, Determinants of contraceptive use in NepalJournal
of Biosocial Science, 1985, 17(2):185-193.<person-group>
                  <string-name>
                     <surname>Tuladhar</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>185</fpage>
               <volume>17</volume>
               <source>Journal of Biosocial Science</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d491e1172a1310">
            <label>11</label>
            <mixed-citation id="d491e1179" publication-type="journal">
Dodoo FN-A, Ezeh AC and Owuor TO, 2001, op. cit. (see reference
8).  </mixed-citation>
         </ref>
         <ref id="d491e1191a1310">
            <label>12</label>
            <mixed-citation id="d491e1198" publication-type="other">
Bureau Central du Recensement, Chad, and Macro International:
Chad Demographic and Health Survey 1996/97, N'Djamena, Chad, and
Calverton, MD, USA: Bureau Central du Recensement and Macro
International, 1998.</mixed-citation>
         </ref>
         <ref id="d491e1214a1310">
            <label>13</label>
            <mixed-citation id="d491e1221" publication-type="journal">
Dodoo FN-A, Luo Y and Panayotova E, Do male reproductive
preferences really point to a need to refocus fertility policy? Population
Research and Policy Review, 1997, 16(5):447-455.<person-group>
                  <string-name>
                     <surname>Dodoo</surname>
                  </string-name>
               </person-group>
               <issue>5</issue>
               <fpage>447</fpage>
               <volume>16</volume>
               <source>Population Research and Policy Review</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d491e1259a1310">
            <label>14</label>
            <mixed-citation id="d491e1266" publication-type="journal">
Bongaarts J and BruceJ, 1995, op. cit. (see reference 2).  </mixed-citation>
         </ref>
         <ref id="d491e1275a1310">
            <label>15</label>
            <mixed-citation id="d491e1282" publication-type="journal">
Dodoo FN-A, Ezeh AC and Owuor TO, 2001, op. cit. (see reference
8).  </mixed-citation>
         </ref>
         <ref id="d491e1294a1310">
            <label>16</label>
            <mixed-citation id="d491e1301" publication-type="book">
Hill R, StycosJM and Back KW, 1959, op. cit. (see reference 5).  </mixed-citation>
         </ref>
         <ref id="d491e1311a1310">
            <label>17</label>
            <mixed-citation id="d491e1318" publication-type="journal">
Ibid.  </mixed-citation>
         </ref>
         <ref id="d491e1327a1310">
            <label>18</label>
            <mixed-citation id="d491e1334" publication-type="journal">
Bureau Central du Recensement, Chad, and Macro International,
1998, op. cit. (see reference 12).  </mixed-citation>
         </ref>
         <ref id="d491e1346a1310">
            <label>19</label>
            <mixed-citation id="d491e1353" publication-type="journal">
Bawah AA et al., Women's fears and men's anxieties: the impact of
family planning on gender relations in northern Ghana, Studies in Family
Planning, 1999, 30(1):54-66.<issue>1</issue>
               <fpage>54</fpage>
               <volume>30</volume>
               <source>Family Planning</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d491e1382a1310">
            <label>20</label>
            <mixed-citation id="d491e1389" publication-type="book">
Blanc AK et al., 1996, op. cit. (see reference 6).  </mixed-citation>
         </ref>
         <ref id="d491e1398a1310">
            <label>21</label>
            <mixed-citation id="d491e1405" publication-type="journal">
CaldwellJC, The delayed western fertility decline: an examination
of English-speaking countries, Population and Development Review, 1999,
25(3):479-513.<object-id pub-id-type="jstor">10.2307/172344</object-id>
               <fpage>479</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e1424a1310">
            <label>22</label>
            <mixed-citation id="d491e1431" publication-type="journal">
Castle S et al., A qualitative study of clandestine contraceptive use
in urban Mali, Studies in Family Planning, 1999, 30(2):231-248.<issue>2</issue>
               <fpage>231</fpage>
               <volume>30</volume>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d491e1455a1310">
            <label>23</label>
            <mixed-citation id="d491e1462" publication-type="journal">
Hollos M and Larsen U, 1997, op. cit. (see reference 10).  </mixed-citation>
         </ref>
         <ref id="d491e1471a1310">
            <label>24</label>
            <mixed-citation id="d491e1478" publication-type="journal">
Boulay M and Valente TW, The relationship of social affiliation and
intrapersonal discussion to family planning knowledge, attitudes and
practice, International Family Planning Perspectives, 1999, 25(3):112-118.<object-id pub-id-type="doi">10.2307/2991959</object-id>
               <fpage>112</fpage>
            </mixed-citation>
         </ref>
         <ref id="d491e1497a1310">
            <label>25</label>
            <mixed-citation id="d491e1504" publication-type="book">
PhillipsJF et al., The determinants of contraceptive innovation: a
case-control study of family planning acceptance in a traditional African
society, Research Division Working Paper, New York: Population Coun-
cil, 1997, No, 93.<person-group>
                  <string-name>
                     <surname>Phillips</surname>
                  </string-name>
               </person-group>
               <source>Research Division Working Paper</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d491e1536a1310">
            <label>26</label>
            <mixed-citation id="d491e1543" publication-type="journal">
Sharan M and Valente TW, 2002, op. cit. (see reference 4)  </mixed-citation>
            <mixed-citation id="d491e1551" publication-type="journal">
Rogers
EM et al., Effects of an entertainment-education radio soap opera on
family planning behavior in Tanzania, Studies in Family Planning, 1999,
30(3): 193-211.<object-id pub-id-type="jstor">10.2307/172196</object-id>
               <fpage>193</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
