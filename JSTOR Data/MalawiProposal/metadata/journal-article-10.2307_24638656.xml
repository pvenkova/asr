<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">popudeverevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100511</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population and Development Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Subscription Services, Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00987921</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284457</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24638656</article-id>
         <title-group>
            <article-title>The Demographic Promise of Expanded Female Education: Trends in the Age at First Birth in Malawi</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Monica J.</given-names>
                  <surname>Grant</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2015</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">41</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24638305</issue-id>
         <fpage>409</fpage>
         <lpage>438</lpage>
         <permissions>
            <copyright-statement>© 2015 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24638656"/>
         <abstract>
            <p>The expansion of female education has been promoted as a way to postpone the age at first birth. In sub-Saharan Africa, the first cohorts to benefit from policies that expanded access to education are now reaching adulthood and beginning childbearing. I investigate whether the expansion of education in Malawi, which implemented a free primary education policy in 1994 and subsequently expanded secondary schooling, has led to a later age at first birth and whether the education gradient in fertility timing has remained stable over time. Despite increases in female grade attainment over the past twenty years, the age at first birth has not changed. Using instrumental variables analysis, I find a significant negative association between grade attainment and age at first birth, suggesting that the deterioration of school quality and the shift in the age pattern of enrollment that accompanied educational expansion may have compromised the transformative potential of education.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d813e224a1310">
            <label>3</label>
            <mixed-citation id="d813e231" publication-type="other">
Cleves et al. 2013</mixed-citation>
            <mixed-citation id="d813e237" publication-type="other">
StataCorp 2013</mixed-citation>
         </ref>
         <ref id="d813e244a1310">
            <label>4</label>
            <mixed-citation id="d813e251" publication-type="other">
StataCorp 2013</mixed-citation>
         </ref>
         <ref id="d813e258a1310">
            <label>5</label>
            <mixed-citation id="d813e267" publication-type="other">
Kalipeni 1997</mixed-citation>
            <mixed-citation id="d813e273" publication-type="other">
Kendall 2007</mixed-citation>
         </ref>
         <ref id="d813e280a1310">
            <label>7</label>
            <mixed-citation id="d813e287" publication-type="other">
Blanc and
Rutenberg 1990</mixed-citation>
            <mixed-citation id="d813e296" publication-type="other">
Gage 1995</mixed-citation>
            <mixed-citation id="d813e302" publication-type="other">
National
Statistical Office and ICF Macro 2011</mixed-citation>
            <mixed-citation id="d813e312" publication-type="other">
Fortson
2008</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d813e331a1310">
            <mixed-citation id="d813e335" publication-type="other">
Aikman, Sheila, Elaine Unterhalter, and Chloe Challender. 2005. "The education MDGs:
Achieving gender equality through curriculum and pedagogy change," Gender and De-
velopment 13(1): 44-55.</mixed-citation>
         </ref>
         <ref id="d813e348a1310">
            <mixed-citation id="d813e352" publication-type="other">
Ainsworth, M„ K. Beegle, and A. Nyamete. 1996. "The impact of women's schooling on fertil-
ity and contraceptive use: A study of fourteen sub-Saharan African countries," The World
Bank Economic Review 10(1): 85-122.</mixed-citation>
         </ref>
         <ref id="d813e365a1310">
            <mixed-citation id="d813e369" publication-type="other">
Al-Samarrai, Samer and Paul Bennell. 2007. "Where has all the education gone in sub-Saharan
Africa? Employment and other outcomes among secondary school and university leav-
ers," Journal of Development Studies 43(7): 1270-1300.</mixed-citation>
         </ref>
         <ref id="d813e382a1310">
            <mixed-citation id="d813e386" publication-type="other">
Al-Samarrai, Samer and Hassan Zaman. 2007. "Abolishing school fees in Malawi: The impact
on education access and equity," Education Economics 15(3): 359-375.</mixed-citation>
         </ref>
         <ref id="d813e397a1310">
            <mixed-citation id="d813e401" publication-type="other">
Anderson-Levitt, K.M., M. Bloch, and A.M. Soumare. 1998. "Inside classrooms in Guinea:
Girls' experiences," in Marianne Bloch, Josephine A. Beoku-Betts, and B. Robert Tabach-
nick (eds.), Women and Education in Sub-Saharan Africa: Power, Opportunities, and Constraints.
Boulder, CO: Lynne Rienner Publishers, pp. 99-130.</mixed-citation>
         </ref>
         <ref id="d813e417a1310">
            <mixed-citation id="d813e421" publication-type="other">
Angeles, Gustavo, David K. Guilkey, and Thomas A. Mroz. 2005. "The effects of education and
family planning programs on fertility in Indonesia," Economic Development and Cultural
Change 54(1): 165-201.</mixed-citation>
         </ref>
         <ref id="d813e434a1310">
            <mixed-citation id="d813e438" publication-type="other">
Axinn, William G. and Jennifer S Barber. 2001. "Mass education and fertility transition,"
American Sociological Review 66(4): 481-505.</mixed-citation>
         </ref>
         <ref id="d813e448a1310">
            <mixed-citation id="d813e452" publication-type="other">
Behrman, Julia Andrea. 2015a. "The effect of increased primary schooling on adult women's
HIV status in Malawi and Uganda: Universal primary education as a natural experiment,"
Social Science &amp; Medicine 127: 108-115.</mixed-citation>
         </ref>
         <ref id="d813e465a1310">
            <mixed-citation id="d813e469" publication-type="other">
-. 2015b. "Does schooling affect women's desired fertility? Evidence from Malawi,
Uganda, and Ethiopia," Demography 52(3): 787-809.</mixed-citation>
         </ref>
         <ref id="d813e479a1310">
            <mixed-citation id="d813e483" publication-type="other">
Blanc, Ann K. and Naomi Rutenberg. 1990. "Assessment of the quality of data on age at first
sexual intercourse, age at first marriage, and age at first birth in the Demographic and
Health Surveys," in An Assessment of DHS-I Data Quality. Columbia, MD: Institute of Re-
source Development/Macro System, pp. 41-79.</mixed-citation>
         </ref>
         <ref id="d813e500a1310">
            <mixed-citation id="d813e504" publication-type="other">
Bledsoe, Caroline H. and Barney Cohen (eds.) 1993. Social Dynamics of Adolescent Fertility in
Sub-Saharan Africa. Washington, DC: National Academies Press.</mixed-citation>
         </ref>
         <ref id="d813e514a1310">
            <mixed-citation id="d813e518" publication-type="other">
Caldwell, John C. 1980. "Mass education as a determinant of the timing of fertility decline,"
Population and Development Review 6(2): 225-255.</mixed-citation>
         </ref>
         <ref id="d813e528a1310">
            <mixed-citation id="d813e532" publication-type="other">
-. 1982. Theory of Fertility Decline. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="d813e539a1310">
            <mixed-citation id="d813e543" publication-type="other">
Caldwell, John C., I.O. Orubuloye, and Pat Caldwell. 1992. "Fertility decline in Africa: A new
type of transition?" Population and Development Review 18(2): 211-242.</mixed-citation>
         </ref>
         <ref id="d813e553a1310">
            <mixed-citation id="d813e557" publication-type="other">
Chimombo, Joseph Patrick Godson. 2005. "Quantity versus quality in education: Case studies
in Malawi," International Review of Education 51(2-3): 155-172.</mixed-citation>
         </ref>
         <ref id="d813e567a1310">
            <mixed-citation id="d813e571" publication-type="other">
Chimombo, Joseph, Elizabeth Meke, Benjamin Zeitlyn, and Keith M. Lewin. 2014. "Increasing
access to secondary school: Education in Malawi—Does private schooling deliver on its
promises?" ESP Working Paper No. 61. Sussex.</mixed-citation>
         </ref>
         <ref id="d813e585a1310">
            <mixed-citation id="d813e589" publication-type="other">
Chisamya, Grace, Joan DeJaeghere, Nancy Kendall, and Marufa Aziz Khan. 2012. "Gender
and education for all: Progress and problems in achieving gender equity," International
Journal of Educational Development 32(6): 743-755.</mixed-citation>
         </ref>
         <ref id="d813e602a1310">
            <mixed-citation id="d813e606" publication-type="other">
Cleland, John. 2002. "Education and future fertility trends with special reference to mid-
transitional countries," Population Bulletin of the United Nations Special Issue, pp. 183-194.</mixed-citation>
         </ref>
         <ref id="d813e616a1310">
            <mixed-citation id="d813e620" publication-type="other">
Cleves, Mario, Roberto G. Gutierrez, William Gould, and Yulia V. Marchenko. 2013. An Intro-
duction to Survival Analysis Using Stata. Third Edition. College Station, TX: StataCorp LP.</mixed-citation>
         </ref>
         <ref id="d813e630a1310">
            <mixed-citation id="d813e634" publication-type="other">
Colclough, Christopher, Pauline Rose, and Mercy Tembon. 2000. "Gender inequalities in pri-
mary schooling: The roles of poverty and adverse cultural practice," International Journal
of Educational Development 20(1): 5-27.</mixed-citation>
         </ref>
         <ref id="d813e647a1310">
            <mixed-citation id="d813e651" publication-type="other">
De Paoli, Anna. 2011. "Education, teenage fertility and labour market participation: Evidence
from Ecuador," Centra Studi Luca D'Agliano Development Studies Working Papers 319.
University of Milan Bicocca, Milan, Italy.</mixed-citation>
         </ref>
         <ref id="d813e664a1310">
            <mixed-citation id="d813e668" publication-type="other">
Diamond, Ian, Margaret Newby, and Sarah Varie. 1999. "Female education and fertility: Exam-
ining the links," in Caroline H. Bledsoe, John Casterline, Jennifer A. Johnson-Kuhn, and
John G. Haaga (eds.), Critical Perspectives on Schooling and Fertility in the Developing World.
Washington, DC: National Academies Press, pp. 23-48.</mixed-citation>
         </ref>
         <ref id="d813e685a1310">
            <mixed-citation id="d813e689" publication-type="other">
Dunne, Mâiréad, Sara Humphreys, and Fiona Leach. 2006. "Gender violence in schools in the
developing world," Gender and Education 18(1): 75-98.</mixed-citation>
         </ref>
         <ref id="d813e699a1310">
            <mixed-citation id="d813e703" publication-type="other">
EMIS. 1994. "Basic education statistics: Malawi 1994." Lilongwe, Malawi: Department of
Education Planning, Ministry of Education, Science, and Technology.</mixed-citation>
         </ref>
         <ref id="d813e713a1310">
            <mixed-citation id="d813e717" publication-type="other">
-. 1995. "Basic education statistics: Malawi 1995." Lilongwe, Malawi: Statistics Unit,
Ministry of Education, Science, and Technology.</mixed-citation>
         </ref>
         <ref id="d813e727a1310">
            <mixed-citation id="d813e731" publication-type="other">
-. 2000. "Education basic statistics: Malawi 2000." Vol. 2000. Lilongwe, Malawi.</mixed-citation>
         </ref>
         <ref id="d813e738a1310">
            <mixed-citation id="d813e742" publication-type="other">
-. 2012. "Education statistics 2012." Lilongwe, Malawi.</mixed-citation>
         </ref>
         <ref id="d813e749a1310">
            <mixed-citation id="d813e753" publication-type="other">
Feeney, Griffith. 2014. "Literacy and gender: Development success stories," Population and
Development Review 40(3): 545-552.</mixed-citation>
         </ref>
         <ref id="d813e764a1310">
            <mixed-citation id="d813e768" publication-type="other">
Ferré, Céline. 2009. "Age at first child: Does education delay fertility timing?—The case of
Kenya," Policy Research Working Paper 4833, The World Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d813e778a1310">
            <mixed-citation id="d813e782" publication-type="other">
Filmer, Deon and Louise Fox. 2014. "Youth employment in sub-Saharan Africa," Africa De-
velopment Series, World Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d813e792a1310">
            <mixed-citation id="d813e796" publication-type="other">
Filmer, Deon and Norbert Schady. 2009. "School enrollment, selection and test scores," Policy
Research Working Paper 4998, World Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d813e806a1310">
            <mixed-citation id="d813e810" publication-type="other">
Fortson, Jane G. 2008. "The gradient in sub-Saharan Africa: Socioeconomic status and HIV/
AIDS," Demography 45(2): 303-322.</mixed-citation>
         </ref>
         <ref id="d813e820a1310">
            <mixed-citation id="d813e824" publication-type="other">
Frye, Margaret. 2012. "Bright futures in Malawi's new dawn: Educational aspirations as asser-
tions of identity," American Journal of Sociology 117(6): 1565-1624.</mixed-citation>
         </ref>
         <ref id="d813e834a1310">
            <mixed-citation id="d813e838" publication-type="other">
Gage, Anastasia J. 1995. "An assessment of the quality of data on age at first union, first birth,
and first sexual intercourse for Phase II of the Demographic and Health Surveys Pro-
gram," Occasional Papers No. 4, Calverton, MD.</mixed-citation>
         </ref>
         <ref id="d813e852a1310">
            <mixed-citation id="d813e856" publication-type="other">
Gakidou, Emmanuela et al. 2009. "Increased educational attainment and its effect on child
mortality in 175 countries between 1970 and 2009: A systematic analysis," The Lancet
376(9745): 959-974.</mixed-citation>
         </ref>
         <ref id="d813e869a1310">
            <mixed-citation id="d813e873" publication-type="other">
Gerver, Mollie. 2013. "'Sinigurisha! (You are not for sale!)': Exploring the relationship between
access to school, school fees, and sexual abuse in Rwanda," Gender and Education 25(2):
220-235.</mixed-citation>
         </ref>
         <ref id="d813e886a1310">
            <mixed-citation id="d813e890" publication-type="other">
Government of Malawi. 2014. "Global AIDS response progress report: Malawi progress report
for 2013." Lilongwe, Malawi, http://www.unaids.org/sites/default/files/country/docu-
ments/MWI_narrative_report_2 014.pdf.</mixed-citation>
         </ref>
         <ref id="d813e903a1310">
            <mixed-citation id="d813e907" publication-type="other">
Grant, Monica J. and Erica Soler-Hampejsek. 2014. "HIV risk perceptions, the transition to
marriage, and divorce in southern Malawi," Studies in Family Planning 45(3): 315-337.</mixed-citation>
         </ref>
         <ref id="d813e917a1310">
            <mixed-citation id="d813e921" publication-type="other">
Grant, Monica J., Erica Soler-Hampejsek, Barbara S. Mensch, and Paul C. Hewett. 2011.
"Gender differences in school effects on learning and enrollment outcomes in rural Ma-
lawi," paper presented at the Annual Meeting of the Population Association of America,
Washington, DC, 31 March-2 April.</mixed-citation>
         </ref>
         <ref id="d813e937a1310">
            <mixed-citation id="d813e941" publication-type="other">
Grogan, Louise. 2006. "Who benefits from universal primary education in Uganda?" Vol. 1.
Department of Economics, University of Guelph, Ontario.</mixed-citation>
         </ref>
         <ref id="d813e952a1310">
            <mixed-citation id="d813e956" publication-type="other">
Gupta, Neeru and Mary Mahy. 2003. "Adolescent childbearing in sub-Saharan Africa: Can
increased schooling alone raise ages at first birth?" Demographic Research 8(4): 93-106.</mixed-citation>
         </ref>
         <ref id="d813e966a1310">
            <mixed-citation id="d813e970" publication-type="other">
Hayford, Sarah R., Victor Agadjanian, and Luciana Luz. 2012. "Now or never: Perceived HIV
status and fertility intentions in rural Mozambique," Studies in Family Planning 43(3):
191-199.</mixed-citation>
         </ref>
         <ref id="d813e983a1310">
            <mixed-citation id="d813e987" publication-type="other">
Herz, Barbara and Gene B. Sperling. 2004. "What works in girls' education: Evidence and poli-
cies from the developing world," Council on Foreign Relations, New York.</mixed-citation>
         </ref>
         <ref id="d813e997a1310">
            <mixed-citation id="d813e1001" publication-type="other">
Heyneman, Stephen P. and Jonathan M.B. Stern. 2014. "Low cost private schools for the
poor: What public policy is appropriate?" International Journal of Educational Development
35(March): 3-15.</mixed-citation>
         </ref>
         <ref id="d813e1014a1310">
            <mixed-citation id="d813e1018" publication-type="other">
Hoop, Jacobus De. 2010. "Selective secondary education and school participation in sub-Saha-
ran Africa: Evidence from Malawi," Tinbergen Institute Discussion Paper TI2010-041/2.
VU University Amsterdam and Tinbergen Institute, Amsterdam.</mixed-citation>
         </ref>
         <ref id="d813e1031a1310">
            <mixed-citation id="d813e1035" publication-type="other">
Jejeebhoy, Shireen. 1995. Women's Education, Autonomy, and Reproductive Behavior: Experience
from Developing Countries. Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d813e1046a1310">
            <mixed-citation id="d813e1050" publication-type="other">
Johnson-Hanks, Jennifer. 2006. Uncertain Honor: Modern Motherhood in an African Crisis. Chicago:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d813e1060a1310">
            <mixed-citation id="d813e1064" publication-type="other">
Kabeer, Naila. 2005. "Gender equality and women's empowerment: A critical analysis of the
third Millennium Development Goal, " Gender and Development 13(1): 13-24.</mixed-citation>
         </ref>
         <ref id="d813e1074a1310">
            <mixed-citation id="d813e1078" publication-type="other">
Kadzamira, Esme and Pauline Rose. 2003. "Can free primary education meet the needs of
the poor? Evidence from Malawi," International Journal of Educational Development 23 (5):
501-516.</mixed-citation>
         </ref>
         <ref id="d813e1091a1310">
            <mixed-citation id="d813e1095" publication-type="other">
Kalipeni, Ezekiel. 1997. "Gender and regional differences in schooling between boys and girls
in Malawi," The East African Geographical Review 19(1): 14-32.</mixed-citation>
         </ref>
         <ref id="d813e1105a1310">
            <mixed-citation id="d813e1109" publication-type="other">
Kendall, Nancy. 2007. "Education for all meets political democratization: Free primary educa-
tion and the neoliberalization of the Malawian school and state," Comparative Education
Review 51(3): 281-305.</mixed-citation>
         </ref>
         <ref id="d813e1122a1310">
            <mixed-citation id="d813e1126" publication-type="other">
Kirdar, Murat G., Meltem Dayioglu, and Ismet Koc. 2009. "The impact of schooling on the
timing of marriage and fertility: Evidence from a change in compulsory schooling law,"
MPRA Paper No. 13410. Munich Personal RePEc Archive.</mixed-citation>
         </ref>
         <ref id="d813e1140a1310">
            <mixed-citation id="d813e1144" publication-type="other">
Klugman, Jeni et al. 2014. Voice and Agency: Empowering Women and Girls for Shared Prosperity.
Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d813e1154a1310">
            <mixed-citation id="d813e1158" publication-type="other">
Kravdal, 0ystein. 2002. "Education and fertility in sub-Saharan Africa: Individual and com-
munity effects," Demography 39(2): 233-250.</mixed-citation>
         </ref>
         <ref id="d813e1168a1310">
            <mixed-citation id="d813e1172" publication-type="other">
Le Vine, Robert A., Sarah E. Le Vine, and Beatrice Schnell. 2001. '"Improve the women': Mass
schooling, female literacy, and worldwide social change," Harvard Educational Review
71(1): 1-51.</mixed-citation>
         </ref>
         <ref id="d813e1185a1310">
            <mixed-citation id="d813e1189" publication-type="other">
LeVine, Robert A., Sarah E. LeVine, Beatrice Schnell-Anzola, Meredith L. Rowe, and Emily
Dexter. 2012. Literacy and Mothering: How Women's Schooling Changes the Lives of the World's
Children. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d813e1202a1310">
            <mixed-citation id="d813e1206" publication-type="other">
Levine, Ruth, Cynthia Lloyd, Margaret Greene, and Caren Grown. 2008. Girls Count: A Global
Investment &amp; Action Agenda. Washington, DC: Center for Global Development.</mixed-citation>
         </ref>
         <ref id="d813e1216a1310">
            <mixed-citation id="d813e1220" publication-type="other">
Lloyd, Cynthia B. (ed.). 2005. Growing Up Global: The Changing Transitions to Adulthood in Devel-
oping Countries. Washington, DC: National Academies Press.</mixed-citation>
         </ref>
         <ref id="d813e1231a1310">
            <mixed-citation id="d813e1235" publication-type="other">
-. 2009. New Lessons: The Power of Educating Adolescent Girls, A Girls Count Report on Adolescent
Girls. New York: Population Council.</mixed-citation>
         </ref>
         <ref id="d813e1245a1310">
            <mixed-citation id="d813e1249" publication-type="other">
Lucas, Adrienne M. and Isaac M. Mbiti. 2012. "Does free primary education narrow gender dif-
ferences in schooling? Evidence from Kenya," Journal of African Economies 21(5): 691-722.</mixed-citation>
         </ref>
         <ref id="d813e1259a1310">
            <mixed-citation id="d813e1263" publication-type="other">
Luke, Nancy. 2003. "Age and economic asymmetries in the sexual relationships of adolescent
girls in sub-Saharan Africa," Studies in Family Planning 34(2): 67-86.</mixed-citation>
         </ref>
         <ref id="d813e1273a1310">
            <mixed-citation id="d813e1277" publication-type="other">
Mcintosh, C. Alison and Jason L. Finkle. 1995. "The Cairo conference on population and de-
velopment: A new paradigm?" Population and Development Review 21(2): 223-260.</mixed-citation>
         </ref>
         <ref id="d813e1287a1310">
            <mixed-citation id="d813e1291" publication-type="other">
Mensch, Barbara S„ Judith Bruce, and Margaret E. Greene. 1998. The Uncharted Passage: Girls'
Adolescence in the Developing World. New York: Population Council.</mixed-citation>
         </ref>
         <ref id="d813e1301a1310">
            <mixed-citation id="d813e1305" publication-type="other">
Mensch, Barbara S. and Cynthia B. Lloyd. 1998. "Gender differences in the schooling expe-
riences of adolescents in low-income countries: The case of Kenya," Studies in Family
Planning 29(2): 167-184.</mixed-citation>
         </ref>
         <ref id="d813e1319a1310">
            <mixed-citation id="d813e1323" publication-type="other">
Milner, G., D. Mulera, and T. Chimuzu. 2011. "The SACMEQ III Project in Malawi: A study of
the conditions of schooling and the quality of education." www.sacmeq.org.</mixed-citation>
         </ref>
         <ref id="d813e1333a1310">
            <mixed-citation id="d813e1337" publication-type="other">
Moeller, Kathryn. 2013. "Proving 'the girl effect': Corporate knowledge production and edu-
cational intervention," International Journal of Educational Development 33(6): 612-621.</mixed-citation>
         </ref>
         <ref id="d813e1347a1310">
            <mixed-citation id="d813e1351" publication-type="other">
National Statistical Office and Macro International Inc. 1992. "Malawi Demographic and Health
Survey." Calverton, MD.</mixed-citation>
         </ref>
         <ref id="d813e1361a1310">
            <mixed-citation id="d813e1365" publication-type="other">
National Statistical Office. 2010. Malawi Integrated Household Survey III. Zomba, Malawi:
National Statistics Office.</mixed-citation>
         </ref>
         <ref id="d813e1375a1310">
            <mixed-citation id="d813e1379" publication-type="other">
National Statistical Office and ICF Macro. 2011. "Malawi Demographic and Health Survey
2010." Zomba, Malawi, and Calverton, MD.</mixed-citation>
         </ref>
         <ref id="d813e1389a1310">
            <mixed-citation id="d813e1393" publication-type="other">
Pritchett, Lant. 2013. The Rebirth of Education: Schooling Ain't Learning. New York: Center for
Global Development.</mixed-citation>
         </ref>
         <ref id="d813e1404a1310">
            <mixed-citation id="d813e1408" publication-type="other">
Smith-Greenaway, Emily. 2013. "Maternal reading skills and child mortality in Nigeria: A reas-
sessment of why education matters," Demography 50(5): 1551-1561.</mixed-citation>
         </ref>
         <ref id="d813e1418a1310">
            <mixed-citation id="d813e1422" publication-type="other">
Soler-Hampejsek, Erica et al. 2014. "Gender differences in the retention of literacy and nu-
meracy in Malawi," paper presented at the Annual Meeting of the Population Association
of America, Boston, MA, 1-3 May.</mixed-citation>
         </ref>
         <ref id="d813e1435a1310">
            <mixed-citation id="d813e1439" publication-type="other">
Stromquist, Nelly P. 2003. "Education as a means of empowering women," in Jane L. Parpart,
Shirin M. Rai, and Kathleen A. Staudt (eds. ), Rethinking Empowerment: Gender and Develop-
ment in a Global/Local World. London: Routledge, pp. 22-38.</mixed-citation>
         </ref>
         <ref id="d813e1452a1310">
            <mixed-citation id="d813e1456" publication-type="other">
Taylor, Stephen and Nicholas Spaull. 2015. "Measuring access to learning over a period of
increased access to schooling: The case of southern and eastern Africa since 2000," Inter-
national Journal of Educational Development 41 (March): 47-59.</mixed-citation>
         </ref>
         <ref id="d813e1469a1310">
            <mixed-citation id="d813e1473" publication-type="other">
Tembon, Mercy and Lucia Fort. 2008. Girls' Education in the 21st Century: Gender Equality, Em-
powerment, and Economic Growth. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d813e1483a1310">
            <mixed-citation id="d813e1487" publication-type="other">
Thornton, Arland. 2005. Reading History Sideways: The Fallacy and Enduring Impact of the Devel-
opmental Paradigm on Family Life. University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d813e1498a1310">
            <mixed-citation id="d813e1502" publication-type="other">
Trinitapoli, Jenny and Sara E. Yeatman. 2011. "Uncertainty and fertility in a generalized AIDS
epidemic," American Sociological Review 76(6): 935-954. doi: 10.1177/0003122411427672.</mixed-citation>
         </ref>
         <ref id="d813e1512a1310">
            <mixed-citation id="d813e1516" publication-type="other">
Ueyama, Mika and Futoshi Yamauchi. 2009. "Marriage behavior response to prime-age adult
mortality: Evidence from Malawi," Demography 46(1) : 43-63.</mixed-citation>
         </ref>
         <ref id="d813e1526a1310">
            <mixed-citation id="d813e1530" publication-type="other">
UNESCO. 1990. "Meeting basic learning needs—A vision for the 1990s: World Conference on
Education for All Meeting Basic Learning Needs," Jomtien, Thailand.</mixed-citation>
         </ref>
         <ref id="d813e1540a1310">
            <mixed-citation id="d813e1544" publication-type="other">
Vavrus, Frances. 2003. "'The acquired income deficiency syndrome': School fees and sexual
risk in northern Tanzania," Compare: A Journal of Comparative and International Education
33(2): 235-250.</mixed-citation>
         </ref>
         <ref id="d813e1557a1310">
            <mixed-citation id="d813e1561" publication-type="other">
World Bank. 2001. Engendering Development: Through Gender Equality in Rights, Resources, and
Voice. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d813e1571a1310">
            <mixed-citation id="d813e1575" publication-type="other">
-. 2004. "Cost, financing and school effectiveness of education in Malawi: A future of
limited choices and endless opportunities," Africa Region Human Development Working
Paper Series, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d813e1589a1310">
            <mixed-citation id="d813e1593" publication-type="other">
-. 2006. World Development Report 2007: Development and the Next Generation. Washington,
DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d813e1603a1310">
            <mixed-citation id="d813e1607" publication-type="other">
-. 2010. "The education system in Malawi," Working Paper No. 182, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d813e1614a1310">
            <mixed-citation id="d813e1618" publication-type="other">
World Bank and UNICEF. 2009. Abolishing School Fees in Africa: Lessons from Ethiopia, Ghana,
Kenya, Malawi, and Mozambique. Washington, DC: The World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
