<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26349327</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Correlates of infant and childhood mortality</article-title>
            <subtitle>A theoretical overview and new evidence from the analysis of longitudinal data of the Bejsce (Poland) parish register reconstitution study of the 18th-20th centuries</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Tymicki</surname>
                  <given-names>Krzysztof</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Institute of Statistics and Demography, Warsaw School of Economics-SGH. Al. Niepodległości 162, 02-554 Warsaw, Poland.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2009</year>
            <string-date>JANUARY - JUNE 2009</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">20</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26349304</issue-id>
         <fpage>559</fpage>
         <lpage>594</lpage>
         <permissions>
            <copyright-statement>© 2009 Krzysztof Tymicki</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26349327"/>
         <abstract xml:lang="eng">
            <p>This paper has two main goals. The first is to review the context for studying infant mortality, which includes a review of the theoretical framework, the covariates used to examine mortality over the first 60 months of life, and the major findings of empirical studies. Second, the paper adds some new empirical evidence that comes from the longitudinal reconstitution of church registers of Bejsce parish, located in the south of Poland. This rich database allows for an analysis of mortality trends of cohorts born between the 18<sup>th</sup>and 20<sup>th</sup>centuries in the parish. The analysis includes a reconstruction of descriptive measures of infant and childhood mortality, and a hazard model of mortality over the first 60 months of life. The hazard model has been calculated for each cohort separately in order to demonstrate the change in the relative importance of analyzed factors during the process of mortality decline in the parish. Obtained mortality patterns are discussed with reference to the theoretical context presented in the first part of the paper.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Beise, J. and Voland, E. (2002). A multilevel event history analysis of the effects of grandmothers on child mortality in a historical German population (Krummhörn, Ostfriesland, 1720-1847). Demographic Research 7(13): 470-494. doi:10.4054/DemRes.2002.7.13.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Bideau, A., Desjardins, B., and Brignoli, H.P. (1997). Infant and Child Mortality in the Past. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Bijur, P.E., Golding, J., and Kurzon, M. (1988). Childhood Accidents, Family Size and Birth Order. Social Science and Medicine 26(8): 839-843. doi:10.1016/0277-9536(88)90176-1.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Blake, J. (1981). Family Size and the Quality of Children. Demography 18(4): 421-442. doi:10.2307/2060941.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Blossfeld, H-P., Golsh, K., and Rohwer, G. (2007). Event History Analysis with STATA. London: Lawrence Erlbaum Associates.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Boerma, J.T. and Bicego, G.T. (1992). Preceding Birth Intervals and Child Survival: Searching for Pathways of Influence. Studies in Family Planning 23(4): 243-256. doi:10.2307/1966886.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Breschi, M. and Livi-Bacci, M. (1997). Month of Birth As a Factor in Children's Survival. In: Bideau, A., Desjardins, B., and Brignoli, H.P. (eds.). Infant and Child Mortality in the Past. Oxford: Clarendon Press: 157-173.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Burnett, J. (1991). Housing and the Decline of Mortality. In: Schofield, R., Reher, D., and Bideau, A. (eds.). The Decline of Mortality in Europe. Oxford: Clarendon Press: 158-176.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Cohen, J.E. (1975). Childhood Mortality, Family Size and Birth Order in Pre-Industrial Europe. Demography 12(1): 35-55. doi:10.2307/2060731.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Cotts Watkins, S. and Menken, J. (1985). Famines in Historical Perspective. Population and Development Review 11(4): 647-675.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Desjardins, B. (1995). Bias in Age at Marriage in Family Reconstitutions: Evidence from French- Canadian Data. Population Studies 49(1): 165-169. doi:10.1080/0032472031000148306.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Dupaquier, J. (1997). For a History of Prematurity. In: Bideau, A., Desjardins, B., and Brignoli, H.P. (eds.). Infant and Child Mortality in the Past. Oxford: Clarendon Press: 188-202.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Forste, R. (1994). The Effects of Breastfeeding and Birth Spacing on Infant and Child Mortality in Bolivia. Population Studies 48(3): 497-511. doi:10.1080/0032472031000147996.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Galloway, P.R., Lee, R.D., and Hammel, E.A. (1998). Infant Mortality and the Fertility Transition: Macro Evidence from Europe and New Findings From Prussia. In: Montgomery, M.R. and Cohen, B. (eds.). From Death to Birth. Mortality Decline and Reproductive Change. Washington, D.C.: National Academy Press: 182-226.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Grafen, A. (1982). How to Measure Inclusive Fitness. Nature 298: 425-426. doi:10.1038/298425a0.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Gray, R., Leridon, H., and Spira, A. (1993). Biomedical and Demographic Determinants of Reproduction. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Hamilton, W.D. (1964). The Genetical Evolution of Social Behaviour: I and II. Journal of Theoretical Biology 7(1): 1-52.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Hawkes, K. (2003). Grandmothers and the Evolution of Human Longevity. American Journal of Human Biology 15(3): 380-400. doi:10.1002/ajhb.10156.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Hobcraft, J.N., McDonald, J.W., and Rutstein, S.O. (1985). Demographic Determinants of Infant and Early Child Mortality: A Comparative Analysis. Population Studies 39(3): 363-385. doi:10.1080/0032472031000141576.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Jelliffe, D.B. and Maddocks, I. (1964). Notes on Ecologic Malnutrition in the New Guinea Highlands. Clinical Pediatrics 3: 432-438. doi: 10.1177/000992286400300710.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Kasakoff, A.B. and Adams, J.W. (1995). The effect of migration on ages at vital rates events: a critique of family reconstitution in historical demography. European Journal of Population 11(3): 199-242. doi:10.1007/BF01264948.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Knodel, J. and Hermalin, A.I. (1984). Effects of Birth Rank, Maternal Age, Birth Interval, and Sibship Size on Infant and Child Mortality: Evidence From 18th and 19th Century Reproductive Histories. American Journal of Public Health 74: 1098-1106.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Knodel, J. and Kintner, H. (1977). The Impact of Breast Feeding Patterns on the Biometric Analysis of Infant Mortality. Demography 14(4): 391-409. doi:10.2307/2060586.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Kuate-Defo, B. (1997). Effects of Infant Feeding Practices and Birth Spacing on Infant and Child Survival: a Reassessment From Retrospective and Prospective Data. Journal of Biosocial Science 29(3): 303-326. doi:10.1017/S0021932097003039.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Kumm, J., Laland, K.N., and Feldman, M.W. (1994). Gene-Culture Co-evolution and Sex-Ratios: The Effects of Infanticide, Sex-Selective Abortion, Sex Selection, and Sex-Biased Parental Investment on the Evolution of Sex Ratios. Theoretical Population Biology 46(3): 249-278.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Kumor, B. (1976). Przepisy Prawne w Sprawie Chrztu Dzieci w XVI-XVIIIw. Przeszłość Demograficzna Polski 9: 41-56.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Lalou, R. (1997). Endogenous Mortality in New France: At the Crossroads of Natural and Social Selection. In: Bideau, A., Desjardins, B., and Brignoli, H.P., (eds.). Infant and Child Mortality in the Past. Oxford: Clarendon Press: 203-215.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Livi-Bacci, M. (1991). Population and Nutrition. An Essay on European Demographic History. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Lummaa, V. (2003). Early Developmental Conditions and Reproductive Success in Humans: Downstream Effects of Prenatal Famine, Birthweight, and Timing of Birth. American Journal of Human Biology 15 (3): 370-379. doi:10.1002/ajhb.10155.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Manda, S. (1999). Birth Intervals, Breastfeeding and Determinants of Childhood Mortality in Malawi. Social Science and Medicine 48: 301-312. doi:10.1016/S0277-9536(98)00359-1.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Matthiessen, P.C. and McCann, J.C. (1978). The Role of Mortality in the European Fertility Transition: Aggregate-Level Relations. In: Preston, S.H. (ed.). The Effects of Infant and Child Mortality on Fertility. New York : Academic Press: 47-68.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Miller, J.E., Trussell, J., Pebley, A.R., and Vaughan, B. (1992). Birth Spacing and Child Mortality in Bangladesh and the Philippines. Demography 29(2): 305-318. doi:10.2307/2061733.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Modin, B. (2002). Birth Order and Mortality: a Life-Long Follow-Up of 14,200 Boys and Girls Born in Early 20th Century Sweden. Social Science &amp; Medicine 54(7): 1051-1064. doi:10.1016/S0277-9536(01)00080-6.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Morel, M-F. (1991). The Care of Children: the Influence of Medical Innovation and Medical Institutions on Infant Mortality, 1750-1914. In: Schofield, R., Reher,D., and Bideau, A., (eds.). The Decline of Mortality in Europe. Oxford: Clarendon Press: 196-219.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Palloni, A. and Millman, S. (1986). Effects of Inter-Birth Intervals and Breastfeeding on Infant and Early Childhood Mortality. Population Studies 40(2): 215-236.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Pebley, A.R., Hermalin, A.I., and Knodel, J. (1991). Birth Spacing and Infant Mortality: Evidence for Eighteenth and Nineteenth Century German Villages. Journal of Biosocial Science 23(4): 445-459. doi:10.1017/S0021932000019556.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Peccei, J.S. (1995). A Hypothesis for the Origin and Evolution of Menopause. Maturitas 21: 83-89.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Piasecki, E. (1990). Ludnosc Parafii Bejskiej w Swietle Ksiag Metryklanych z XVIII-XX W. Studium Demograficzne. Population of the Bejsce Parish (Kielce voivodeship, Poland) in the light of parish registers of the 18th-20th centuries. A demographic study. Warszawa: PWN.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Retel-Laurentin, A. and Benoit, D. (1976). Infant Mortality and Birth Intervals. Population Studies 30(2): 279-293. doi:10.2307/2173610.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Rosenberg, M. (1989). Breast-Feeding and Infant Mortality in Norway 1860-1930. Journal of Biosocial Science 21(3): 335-348. doi:10.1017/S0021932000018034.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Royston, P. (2004). Multiple Imputation of Missing Values. The Stata Journal 4(3): 227-241.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Saito, O. (1996). Historical Demography: Achievements and Prospects. Population Studies 50(3): 537-553. doi:10.1080/0032472031000149606.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Schofield, R., Reher, D., and Bideau, A. (1991). The Decline of Mortality in Europe. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Scott, S., Duncan, S.R., and Duncan, C.J. (1995). Infant Mortality and Famine: a Study in Historical Epidemiology in Northern England. Journal of Epidemiology and Community Health 49(3): 245-252. doi:10.1136/jech.49.3.245.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Sear, R. (2001). Evolutionary Demography of Rural Gambian Population. PhD thesis, University College London.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Sear, R., Mace, R., and McGregor, I.A. (2000). Maternal Grandmothers Improve Nutritional Status and Survival of Children in Rural Gambia. Proceedings of the Royal Society of London B 267: 1641-1647. doi:10.1098/rspb.2000.1190.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Shanley, D.P. and Kirkwood, T. (2001). Evolution of Human Menopause. BioEssays 23: 282-287. doi:10.1002/1521-1878(200103)23:3&lt;282::AID-BIES1038&gt;3.0. CO;2-9.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Styś, W. (1957). The Influence of Economic Conditions on the Fertility of Peasant Women. Population Studies 11: 136-148. doi:10.2307/2172109.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Styś, W. (1959). Współzależność Rozwoju Rodziny Chłopskiej i Jej Gospodarstwa. Wrocław: Wrocławskie Towarzystwo Naukowe.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Voland, E. (1998). Evolutionary Ecology of Human Reproduction. Annual Review of Anthropology 27: 347-374. doi:10.1146/annurev.anthro.27.1.347.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Voland, E. (2000). Contributions of Family Reconstitution Studies to Evolutionary Reproductive Ecology. Evolutionary Anthropology 9(3): 134-146. doi:10.1002/1520-6505(2000)9:3&lt;134::AID-EVAN3&gt;3.0.CO;2-M.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Voland, E. and Beise, J. (2001). Opposite Effects of Maternal and Paternal Grandmothers on Infant Survival in Historical Krummhörn. Rostock: Max Planck Institute for Demographic Research (MPIDR working paper; WP-2001-026).</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Weinstein, M., Wood, J.W., and Chang, M.-C. (1993). Age Patterns of Fecundability. In: Gray, R., Leridon, H., and Spira, A., (eds.). Biomedical and Demographic Determinants of Reproduction. Oxford: Clarendon Press: 209-227.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Wood, J.W. (1994). Dynamics of Human Reproduction: Biology, Biometry, Demography. New York: Aldine de Gruyter.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Worthman, C. (1996). Biosocial Determinants of Sex Ratios: Survivorship, Selection and Socialization in the Early Environment. In: Henry, C.J.K. and Ulijaszek, S.J. (eds.). Long-Term Consequences of Early Environment: Growth, Development and the Lifespan developmental Perspective. Cambridge: Cambridge University Press: 44-68.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
