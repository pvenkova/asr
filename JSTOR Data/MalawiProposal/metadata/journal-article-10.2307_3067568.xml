<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3067568</article-id>
         <title-group>
            <article-title>Reaction Norms with Bifurcations Shaped by Evolution</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Tom J. M. van Dooren</string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>07</day>
            <month>2</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">268</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1464</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i354847</issue-id>
         <fpage>279</fpage>
         <lpage>287</lpage>
         <page-range>279-287</page-range>
         <permissions>
            <copyright-statement>Copyright 2001 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3067568"/>
         <abstract>
            <p>Two versions of a model for the evolution of seasonal polyphenism investigate the evolution of reaction norm bifurcation and branching. The first version is without a specific submodel for morphological development and the second has an explicit developmental map. Version 1 is evolutionarily relatively unconstrained: (i) reaction norms are specified by matrices containing the probabilities of occurrence of environment-phenotype combinations, (ii) all conceivable reaction norm matrices are reachable through a sequence of mutations, and (iii) small as well as large mutational effects occur. This version is used to find the evolutionarily stable strategy favoured by the population ecology that is characterized by stabilizing viability selection with a cyclically fluctuating selection optimum. When the strength of selection is large and when the lag between initiation of development and selection on mature phenotype is not a multiple of half the period of the environmental cycle, a branching reaction norm evolves. In the second model version, branching reaction norms occur for certain parameter combinations of the developmental submodel, but the evolution of this pattern is often constrained. The evolutionary trajectory becomes trapped in a local selective optimum for the parameters of the developmental system. Substantial developmental noise evolves, but mutations that produce a selectively advantageous branching pattern do not occur from there.</p>
         </abstract>
         <kwd-group>
            <kwd>Evolution of Reaction Norms</kwd>
            <kwd>Fluctuating Environments</kwd>
            <kwd>Developmental Dynamics</kwd>
            <kwd>Evolutionary Constraint</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d274e181a1310">
            <mixed-citation id="d274e185" publication-type="journal">
Barkal, N. &amp; Leibler, S. 1997 Robustness in simple biochemical
networks. Nature387, 913-917<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Barkal</surname>
                  </string-name>
               </person-group>
               <fpage>913</fpage>
               <volume>387</volume>
               <source>Nature</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d274e217a1310">
            <mixed-citation id="d274e221" publication-type="journal">
Brakefield, P. M. &amp; Reitsma, N. 1991 Phenotypic plasticity,
seasonal climate and the population biology of Bicylus butter-
flies in Malawi. Ecol. Entomol.10, 291-303<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Brakefield</surname>
                  </string-name>
               </person-group>
               <fpage>291</fpage>
               <volume>10</volume>
               <source>Ecol. Entomol.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d274e256a1310">
            <mixed-citation id="d274e260" publication-type="journal">
Brakefield, P. M., Gates, J., Keys, D., Kesbeke, E, Wijngaarden,
P, Monteiro, A., French, V. &amp; Carroll, S. B. 1996
Development, plasticity and evolution of butterfly eyespot
patterns. Nature384, 236-242<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Brakefield</surname>
                  </string-name>
               </person-group>
               <fpage>236</fpage>
               <volume>384</volume>
               <source>Nature</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d274e298a1310">
            <mixed-citation id="d274e302" publication-type="journal">
de Jong, G.1990 Quantitative genetics of reaction norms. J.
Evol. Biol.3, 447-468<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>de Jong</surname>
                  </string-name>
               </person-group>
               <fpage>447</fpage>
               <volume>3</volume>
               <source>J. Evol. Biol.</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d274e335a1310">
            <mixed-citation id="d274e339" publication-type="journal">
Gavrilets, S. &amp; Scheiner, S. M. 1993 The genetics of phenotypic
plasticity. V. Evolution of reaction norm shape. J. Evol. Biol.6,
49-68<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Gavrilets</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>6</volume>
               <source>J. Evol. Biol.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d274e374a1310">
            <mixed-citation id="d274e378" publication-type="journal">
Gianola, D.1982 Theory and analysis of threshold characters. J.
Anim. Sci.54, 1079-1096<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Gianola</surname>
                  </string-name>
               </person-group>
               <fpage>1079</fpage>
               <volume>54</volume>
               <source>J. Anim. Sci.</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d274e410a1310">
            <mixed-citation id="d274e414" publication-type="journal">
Haccou, P. &amp; Iwasa, Y. 1995 Optimal mixed strategies in
stochastic environments. Theor. Pop. Biol.47, 212-243<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Haccou</surname>
                  </string-name>
               </person-group>
               <fpage>212</fpage>
               <volume>47</volume>
               <source>Theor. Pop. Biol.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d274e446a1310">
            <mixed-citation id="d274e450" publication-type="journal">
Kaplan, R. H. &amp; Cooper, W. S. 1984 The evolution of develop¬
mental plasticity in reproductive characteristics: an application
of the 'adaptive coin-fhpping' principle. Am. Nat.123, 393-410<object-id pub-id-type="jstor">10.2307/2461103</object-id>
               <fpage>393</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e469a1310">
            <mixed-citation id="d274e473" publication-type="journal">
Kirkpatrick, M. &amp; Heckman, N. 1989 A quantitative genetic
model for growth, shape, reaction norms, and other infinite-
dimensional characters. J. Math. Biol.27, 429-450<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kirkpatrick</surname>
                  </string-name>
               </person-group>
               <fpage>429</fpage>
               <volume>27</volume>
               <source>J. Math. Biol.</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d274e508a1310">
            <mixed-citation id="d274e512" publication-type="book">
Kuznetsov, Y. A.1998Elements of applied bifurcation theory, 2nd
edn. New\brk: Springer<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kuznetsov</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Elements of applied bifurcation theory</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d274e542a1310">
            <mixed-citation id="d274e546" publication-type="journal">
Lewis, J., Slack, M. W. &amp; Wolpert, L. 1977 Thresholds in
development. J. Theor. Biol.65, 579-590<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lewis</surname>
                  </string-name>
               </person-group>
               <fpage>579</fpage>
               <volume>65</volume>
               <source>J. Theor. Biol.</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d274e578a1310">
            <mixed-citation id="d274e582" publication-type="book">
Maynard Smith, J.1982Evolution and the theory of games.
Cambridge University Press<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <source>Evolution and the theory of games</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d274e607a1310">
            <mixed-citation id="d274e613" publication-type="journal">
Maynard Smith, J., Burian, R., Kauffman, S., Alberch, P,
Campbell, J., Goodwin, B., Lande, R., Raup, D. &amp; Wolpert,
L. 1985 Developmental constraints and evolution. Quart. Rev.
Biol.60, 265-287<object-id pub-id-type="jstor">10.2307/2828504</object-id>
               <fpage>265</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e636a1310">
            <mixed-citation id="d274e640" publication-type="book">
Metz, J. A. J. &amp; Mylius, S. 2001When does evolution optimize?
In Adaptive dynamics in context (ed. J. A. J. Metz &amp;
U. Dieckmann). Cambridge University Press. (In the press.)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Metz</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">When does evolution optimize?</comment>
               <source>Adaptive dynamics in context</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d274e672a1310">
            <mixed-citation id="d274e676" publication-type="journal">
Metz, J. A. J., Nisbet, R. M. &amp; Geritz, S. A. H. 1992 How
should we define fitness for general ecological scenarios?
Trends Ecol. Evol.7, 198-202<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Metz</surname>
                  </string-name>
               </person-group>
               <fpage>198</fpage>
               <volume>7</volume>
               <source>Trends Ecol. Evol.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d274e711a1310">
            <mixed-citation id="d274e715" publication-type="book">
Metz, J. A. J., Geritz, S. A. H., Meszena, G, Jacobs, F. J. A. &amp;
Van Heerwaarden, J. S. 1996Adaptive dynamics: a geome-
trical study of the consequences of nearly faithful
reproduction. In Stochastic and spatial structures of dynamical
systems (ed. S. J. van Strien &amp; S. M. Verduyn Lunel), pp. 183-
231. Amsterdam: KNAW Verhandelingen, North Holland<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Metz</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Adaptive dynamics: a geometrical study of the consequences of nearly faithful reproduction</comment>
               <fpage>183</fpage>
               <source>Stochastic and spatial structures of dynamical systems</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d274e760a1310">
            <mixed-citation id="d274e764" publication-type="book">
Møller, A. P. &amp; Swaddle, J. P 1997Asymmetry, developmental stabi-
lity, and evolution. Oxford University Press<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Møller</surname>
                  </string-name>
               </person-group>
               <source>Asymmetry, developmental stability, and evolution</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d274e789a1310">
            <mixed-citation id="d274e793" publication-type="journal">
Rand, D. A., Wilson, H. B. &amp; McGlade, J. M. 1994 Dynamics
and evolution: evolutionary stable attractors, invasion expo-
nents and phenotype dynamics. Phil. Trans. R. Soc. Lond. B343,
261-283<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rand</surname>
                  </string-name>
               </person-group>
               <fpage>261</fpage>
               <volume>343</volume>
               <source>Phil. Trans. R. Soc. Lond. B</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d274e831a1310">
            <mixed-citation id="d274e835" publication-type="journal">
Richardson, S. &amp; Green, P. 1998 On Bayesian analysis of
mixtures with an unknown number of components. J. R. Stat.
Soc. B59, 731-792<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Richardson</surname>
                  </string-name>
               </person-group>
               <fpage>731</fpage>
               <volume>59</volume>
               <source>J. R. Stat. Soc. B</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d274e870a1310">
            <mixed-citation id="d274e874" publication-type="journal">
Roff, D. A.1996 The evolution of threshold traits. Quart. Rev.
Biol.71, 3-35<object-id pub-id-type="jstor">10.2307/3037828</object-id>
               <fpage>3</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e890a1310">
            <mixed-citation id="d274e894" publication-type="journal">
Sasaki, A. &amp; de Jong, G 1999 Density dependence and un-
predictable selection in a heterogeneous environment:
compromise and polymorphism in the ESS reaction norm.
Evolution53,1329-1342<object-id pub-id-type="doi">10.2307/2640880</object-id>
               <fpage>1329</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e917a1310">
            <mixed-citation id="d274e921" publication-type="journal">
Sasaki, A. &amp; Ellner, S. 1995 The evolutionarily stable pheno¬
type distribution in a random environment. Evolution49,
337-350<object-id pub-id-type="doi">10.2307/2410344</object-id>
               <fpage>337</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e941a1310">
            <mixed-citation id="d274e945" publication-type="book">
Schlichting, C. D. &amp; Pigliucci, M. 1998Phenotypic evolution: a
reaction norm perspective. Sunderland, MA: Sinauer Associates<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schlichting</surname>
                  </string-name>
               </person-group>
               <source>Phenotypic evolution: a reaction norm perspective</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d274e970a1310">
            <mixed-citation id="d274e974" publication-type="journal">
Seger, J. &amp; Brockmann, H. J. 1987 What is bet-hedging? Oxford
Surv. Evol. Biol.4, 182-211<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Seger</surname>
                  </string-name>
               </person-group>
               <fpage>182</fpage>
               <volume>4</volume>
               <source>Oxford Surv. Evol. Biol.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d274e1006a1310">
            <mixed-citation id="d274e1010" publication-type="journal">
Simons, A. M. &amp; Johnston, M. O. 1997 Developmental stability
as a bet-hedging strategy. Oikos80, 401-406<object-id pub-id-type="doi">10.2307/3546608</object-id>
               <fpage>401</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e1026a1310">
            <mixed-citation id="d274e1030" publication-type="book">
Thom, R.1972Stabilité structurelle et morphogénèse. New York:
Benjamin<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Thom</surname>
                  </string-name>
               </person-group>
               <source>Stabilité structurelle et morphogénèse</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d274e1055a1310">
            <mixed-citation id="d274e1059" publication-type="journal">
Walker, T. J.1986 Stochastic polyphenism: coping with uncer-
tainty. Florida Entomol.69, 46-62<object-id pub-id-type="doi">10.2307/3494744</object-id>
               <fpage>46</fpage>
            </mixed-citation>
         </ref>
         <ref id="d274e1075a1310">
            <mixed-citation id="d274e1079" publication-type="journal">
Woltereck, R.1909 Weiterer experimentelle Untersuchungen
iiber Artveranderung, Speziell iiber das Wessen Quantitativer
Artunterschiede bei Daphniden. Versuch. Deutsch. Tsch. Zool.
Gesellschaft19, 110-172<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Woltereck</surname>
                  </string-name>
               </person-group>
               <fpage>110</fpage>
               <volume>19</volume>
               <source>Versuch. Deutsch. Tsch. Zool. Gesellschaft</source>
               <year>1909</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
