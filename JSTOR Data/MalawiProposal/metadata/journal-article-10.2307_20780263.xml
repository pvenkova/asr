<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20780263</article-id>
         <title-group>
            <article-title>The Detection of Acute HIV Infection</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Myron S.</given-names>
                  <surname>Cohen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Cynthia L.</given-names>
                  <surname>Gay</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael P.</given-names>
                  <surname>Busch</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Frederick M.</given-names>
                  <surname>Hecht</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>15</day>
            <month>10</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">202</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20780260</issue-id>
         <fpage>S270</fpage>
         <lpage>S277</lpage>
         <permissions>
            <copyright-statement>© 2010 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20780263"/>
         <abstract>
            <p>Acute human immunodeficiency virus (HIV) infection (AHI) can be defined as the time from HIV acquisition until seroconversion. Incident HIV infection is less well defined but comprises the time from the acquisition of HIV (acute infection) through seroconversion (early or primary HIV infection) and the following months until infection has been well established, as characterized by a stable HIV viral load (viral load set point) and evolution of antibodies with increased concentration and affinity for HIV antigens. During AHI, a viral latent pool reservoir develops, the immune system suffers irreparable damage, and the infected (often unsuspecting) host may be most contagious. It has proved very difficult to find individuals with AHI either in longitudinal cohorts of subjects at high risk for acquiring the virus or through cross-sectional screening, and the opportunity for diagnosis is generally missed during this phase. We review the technical strategies for identifying individuals with acute or incident HIV infection. We conclude that further technical advances are essential to allow more widespread detection of patients with AHI and to affect HIV treatment outcomes and transmission prevention.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d91e198a1310">
            <label>1</label>
            <mixed-citation id="d91e205" publication-type="other">
McMichael A, Borrow P, Tomaras GD, Goonetilleke N, Haynes BE
The immune response during acute HIV-1 infection: clues for HIV-1
vaccine development. Nat Rev Immunol 2010; 10:11-23.</mixed-citation>
         </ref>
         <ref id="d91e218a1310">
            <label>2</label>
            <mixed-citation id="d91e225" publication-type="other">
Spira AI, Marx PA, Patterson BK, et al. Cellular targets of infection
and route of viral dissemination after an intravaginal inoculation of
simian immunodeficiency virus into rhesus macaques. J Exp Med
1996;183(1):215-225.</mixed-citation>
         </ref>
         <ref id="d91e241a1310">
            <label>3</label>
            <mixed-citation id="d91e248" publication-type="other">
Zhang Z, Schuler T, Zupancic M, et al. Sexual transmission and prop-
agation of SIV and HIV in resting and activated CD4+ T cells. Science
1999;286(5443):1353-1357.</mixed-citation>
         </ref>
         <ref id="d91e261a1310">
            <label>4</label>
            <mixed-citation id="d91e268" publication-type="other">
Bowman MC, Archin NM, Margolis DM. Pharmaceutical approaches
to eradication of persistent HIV infection. Expert Rev Mol Med
2009;11:e6.</mixed-citation>
         </ref>
         <ref id="d91e282a1310">
            <label>5</label>
            <mixed-citation id="d91e289" publication-type="other">
Gianella S, von Wyl V, Fischer M, et al. Impact of early ART on proviral
HIV-1 DNA and plasma viremia in acutely infected patients. In: Pro-
gram and abstracts of the 17th Conference on Retroviruses and Op-
portunistic Infections; 2010; San Francisco, CA.</mixed-citation>
         </ref>
         <ref id="d91e305a1310">
            <label>6</label>
            <mixed-citation id="d91e312" publication-type="other">
Wawer MJ, Gray RH, Sewankambo NK, et al. Rates of HIV-1 trans-
mission per coital act, by stage of HIV-1 infection, in Rakai, Uganda.
J Infect Dis 2005; 191(9):1403-1409.</mixed-citation>
         </ref>
         <ref id="d91e325a1310">
            <label>7</label>
            <mixed-citation id="d91e332" publication-type="other">
Pilcher CD, Tien HC, Eron JJ Jr, et al. Brief but efficient: acute HIV
infection and the sexual transmission of HIV J Infect Dis 2004; 189(10):
1785-1792.</mixed-citation>
         </ref>
         <ref id="d91e345a1310">
            <label>8</label>
            <mixed-citation id="d91e352" publication-type="other">
Ma ZM, Stone M, Piatak M Jr, et al. High specific infectivity of plasma
virus from the pre-ramp-up and ramp-up stages of acute simian im-
munodeficiency virus infection. J Virol 2009;83(7):3288-3297.</mixed-citation>
         </ref>
         <ref id="d91e365a1310">
            <label>9</label>
            <mixed-citation id="d91e372" publication-type="other">
Gray RH, Li X, Kigozi G, et al. Increased risk of incident HIV during
pregnancy in Rakai, Uganda: a prospective study. Lancet 2005;
366(9492):1182-1188.</mixed-citation>
         </ref>
         <ref id="d91e385a1310">
            <label>10</label>
            <mixed-citation id="d91e392" publication-type="other">
Taha TE, Hoover DR, Dallabetta GA, et al. Bacterial vaginosis and
disturbances of vaginal flora: association with increased acquisition of
HIV AIDS 1998; 12(13):1699-1706.</mixed-citation>
         </ref>
         <ref id="d91e406a1310">
            <label>11</label>
            <mixed-citation id="d91e413" publication-type="other">
Gay CL, Mwapasa V, Murdoch DM, et al. Acute HIV infection among
pregnant women in Malawi. Diagn Microbiol Infect Dis 2010; 66(4):
356-360.</mixed-citation>
         </ref>
         <ref id="d91e426a1310">
            <label>12</label>
            <mixed-citation id="d91e433" publication-type="other">
Keele BF, Giorgi EE, Salazar-Gonzalez JF, et al. Identification and char-
acterization of transmitted and early founder virus envelopes in pri-
mary HIV-1 infection. Proc Natl Acad Sci U S A 2008; 105(21):
7552-7557.</mixed-citation>
         </ref>
         <ref id="d91e449a1310">
            <label>13</label>
            <mixed-citation id="d91e456" publication-type="other">
Fiebig EW, Wright DJ, Rawal BD, et al. Dynamics of HIV viremia and
antibody seroconversion in plasma donors: implications for diagnosis
and staging of primary HIV infection. AIDS 2003; 17(13):1871-1879.</mixed-citation>
         </ref>
         <ref id="d91e469a1310">
            <label>14</label>
            <mixed-citation id="d91e476" publication-type="other">
Li Q, Skinner PJ, Ha SJ, et al. Visualizing antigen-specific and infected
cells in situ predicts outcomes in early viral infection. Science 2009;
323(5922):1726-1729.</mixed-citation>
         </ref>
         <ref id="d91e489a1310">
            <label>15</label>
            <mixed-citation id="d91e496" publication-type="other">
Stacey AR, Norris PJ, Qin L, et al. Induction of a striking systemic
cytokine cascade prior to peak viremia in acute human immunode-
ficiency virus type 1 infection, in contrast to more modest and delayed
responses in acute hepatitis and C virus infections. J Virol 2009;
83(8):3719-3733.</mixed-citation>
         </ref>
         <ref id="d91e515a1310">
            <label>16</label>
            <mixed-citation id="d91e522" publication-type="other">
Goonetilleke N, Liu MK, Salazar-Gonzalez JF, et al. The first cell
response to transmitted/founder virus contributes to the control of
acute viremia in HIV-1 infection. J Exp Med 2009; 206(6): 1253-1272.</mixed-citation>
         </ref>
         <ref id="d91e536a1310">
            <label>17</label>
            <mixed-citation id="d91e543" publication-type="other">
Lindback S, Thorstensson R, Karlsson AC, et al, for the Karolinska
Institute Primary HIV Infection Study Group. Diagnosis of primary
HIV-1 infection and duration of follow-up after HIV exposure. AIDS
2000;14(15):2333-2339.</mixed-citation>
         </ref>
         <ref id="d91e559a1310">
            <label>18</label>
            <mixed-citation id="d91e566" publication-type="other">
van Loggerenberg F, Mlisana K, Williamson C, et al. Establishing a
cohort at high risk of HIV infection in South Africa: challenges and
experiences of the CAPRISA 002 acute infection study. PLoS One
2008;3(4):el954.</mixed-citation>
         </ref>
         <ref id="d91e582a1310">
            <label>19</label>
            <mixed-citation id="d91e589" publication-type="other">
Vermeulen M, Lelie N, Sykes W, et al. Impact of individual-donation
nucleic acid testing on risk of human immunodeficiency virus, hepatitis
B virus, and hepatitis C virus transmission by blood transfusion in
South Africa. Transfusion 2009; 49(6):1115-1125.</mixed-citation>
         </ref>
         <ref id="d91e605a1310">
            <label>20</label>
            <mixed-citation id="d91e612" publication-type="other">
Quinn TC, Brookmeyer R, Kline R, et al. Feasibility of pooling sera
for HIV-1 viral RNA to diagnose acute primary HIV-1 infection and
estimate HIV incidence. AIDS 2000; 14(17):2751-2757.</mixed-citation>
         </ref>
         <ref id="d91e625a1310">
            <label>21</label>
            <mixed-citation id="d91e632" publication-type="other">
Pilcher CD, Eron JJ Jr, Galvin S, Gay C, Cohen MS. Acute HIV revisited:
new opportunities for treatment and prevention. J Clin Invest 2004;
113(7):937-945.</mixed-citation>
         </ref>
         <ref id="d91e645a1310">
            <label>22</label>
            <mixed-citation id="d91e652" publication-type="other">
Pilcher CD, Fiscus SA, Nguyen TQ, et al. Detection of acute infections
during HIV testing in North Carolina. N Engl J Med 2005; 352(18):
1873-1883.</mixed-citation>
         </ref>
         <ref id="d91e666a1310">
            <label>23</label>
            <mixed-citation id="d91e673" publication-type="other">
Patel P, Klausner JD, Bacon OM, et al. Detection of acute HIV infec-
tions in high-risk patients in California. J Acquir Immune Defic Syndr
2006;42(l):75-79.</mixed-citation>
         </ref>
         <ref id="d91e686a1310">
            <label>24</label>
            <mixed-citation id="d91e693" publication-type="other">
Priddy FH, Pilcher CD, Moore RH, et al. Detection of acute HIV
infections in an urban HIV counseling and testing population in the
United States. J Acquir Immune Defic Syndr 2007; 44(2): 196-202.</mixed-citation>
         </ref>
         <ref id="d91e706a1310">
            <label>25</label>
            <mixed-citation id="d91e713" publication-type="other">
Pilcher CD, Price MA, Hoffman IF, et al. Frequent detection of acute
primary HIV infection in men in Malawi. AIDS 2004; 18(3):517-524.</mixed-citation>
         </ref>
         <ref id="d91e723a1310">
            <label>26</label>
            <mixed-citation id="d91e730" publication-type="other">
Fiscus SA, Pilcher CD, Miller WC, et al. Rapid, real-time detection of
acute HIV infection in patients in Africa. J Infect Dis 2007; 195(3):
416-424.</mixed-citation>
         </ref>
         <ref id="d91e743a1310">
            <label>27</label>
            <mixed-citation id="d91e750" publication-type="other">
Stekler J, Swenson PD, Wood RW, Handsfield HH, Golden MR. Tar-
geted screening for primary HIV infection through pooled HIV-RNA
testing in men who have sex with men. AIDS 2005; 19(12): 1323-1325.</mixed-citation>
         </ref>
         <ref id="d91e763a1310">
            <label>28</label>
            <mixed-citation id="d91e770" publication-type="other">
Stevens W, Akkers E, Myers M, Motlboung T, Pilcher C, Venter F. High
prevalence of undetected, acute HIV infection in a South African pri-
mary care clinic. Paper presented at: Third IAS Conference on HIV
Pathogenesis and Treatment; 2005; Rio de Janeiro, Brazil.</mixed-citation>
         </ref>
         <ref id="d91e787a1310">
            <label>29</label>
            <mixed-citation id="d91e794" publication-type="other">
De Souza R, Pilcher C, Fiscus S, et al. Rapid and efficient acute HIV
detection by 4th generation Ag/Ab ELISA. Paper presented at: Inter-
national Conference on AIDS; 2006; Toronto, Canada.</mixed-citation>
         </ref>
         <ref id="d91e807a1310">
            <label>30</label>
            <mixed-citation id="d91e814" publication-type="other">
Patel P, Mackellar D, Simmons P, et al. Detecting acute human im-
munodeficiency virus infection using 3 different screening immuno-
assays and nucleic acid amplification testing for human immunode-
ficiency virus RNA, 2006-2008. Arch Intern Med 2010; 170(l):66-74.</mixed-citation>
         </ref>
         <ref id="d91e830a1310">
            <label>31</label>
            <mixed-citation id="d91e837" publication-type="other">
Powers KA, Miller WC, Pilcher CD, et al. Improved detection of acute
HIV-1 infection in sub-Saharan Africa: development of a risk score
algorithm. AIDS 2007;21(16):2237-2242.</mixed-citation>
         </ref>
         <ref id="d91e850a1310">
            <label>32</label>
            <mixed-citation id="d91e857" publication-type="other">
Kozlov AP, Shaboltas AV, Toussova OV, et al. HIV incidence and factors
associated with HIV acquisition among injection drug users in St Pe-
tersburg, Russia. AIDS 2006;20(6):901-906.</mixed-citation>
         </ref>
         <ref id="d91e870a1310">
            <label>33</label>
            <mixed-citation id="d91e877" publication-type="other">
Hall HI, Song R, Rhodes P, et al. Estimation of HIV incidence in the
United States. JAMA 2008;300(5):520-529.</mixed-citation>
         </ref>
         <ref id="d91e887a1310">
            <label>34</label>
            <mixed-citation id="d91e894" publication-type="other">
Le Vu S, Le Strat Y, Cazein F, et al. Population-based HIV Incidence
in France, 2003 to 2008. In: Program and abstracts of the 17th Con-
ference on Retroviruses and Opportunistic Infections; 2010; San Fran-
cisco, CA. Abstract 36LB.</mixed-citation>
         </ref>
         <ref id="d91e911a1310">
            <label>35</label>
            <mixed-citation id="d91e918" publication-type="other">
Riedesel M, Laeyendecker O, Quinn T. Comparison of global HIV
incidence: longitudinal and cross-sectional estimates. In: Program and
abstracts of the 17th Conference on Retroviruses and Opportunistic
Infections; 2010; San Francisco, CA. Abstract 526.</mixed-citation>
         </ref>
         <ref id="d91e934a1310">
            <label>36</label>
            <mixed-citation id="d91e941" publication-type="other">
Barkan SE, Melnick SL, Preston-Martin S, et al, for the WIHS Col-
laborative Study Group. The Women's Interagency HIV Study. Epi-
demiology 1998; 9(2):117-125.</mixed-citation>
         </ref>
         <ref id="d91e954a1310">
            <label>37</label>
            <mixed-citation id="d91e961" publication-type="other">
Ostrow DG, Plankey MW, Cox C, et al. Specific sex drug combinations
contribute to the majority of recent HIV seroconversions among MSM
in the MACS. J Acquir Immune Defic Syndr 2009;51(3):349-55.</mixed-citation>
         </ref>
         <ref id="d91e974a1310">
            <label>38</label>
            <mixed-citation id="d91e981" publication-type="other">
Rehle T, Shisana O, Simbayi L, et al. Trends in HIV prevalence, in-
cidence, and risk behaviors among children, youth, and adults in South
Africa, 2002 to 2008. In: Program and abstracts of the 17th Conference
on Retroviruses and Opportunistic Infections; 2010; San Francisco, CA.
Abstract 37.</mixed-citation>
         </ref>
         <ref id="d91e1000a1310">
            <label>39</label>
            <mixed-citation id="d91e1009" publication-type="other">
Tomaras GD, Yates NL, Liu P, et al. Initial B-cell responses to trans-
mitted human immunodeficiency virus type 1: virion-binding Im-
munoglobulin M (IgM) and IgG antibodies followed by plasma anti-
gp41 antibodies with ineffective control of initial viremia. J Virol
2008; 82(24):12449-12463.</mixed-citation>
         </ref>
         <ref id="d91e1028a1310">
            <label>40</label>
            <mixed-citation id="d91e1035" publication-type="other">
Janssen RS, Satten GA, Stramer SL, et al. New testing strategy to detect
early HIV-1 infection for use in incidence estimates and for clinical
and prevention purposes. JAMA 1998; 280(1):42-48.</mixed-citation>
         </ref>
         <ref id="d91e1049a1310">
            <label>41</label>
            <mixed-citation id="d91e1056" publication-type="other">
Murphy G, Parry JV Assays for the detection of recent infections with
human immunodeficiency virus type 1. Euro Surveill 2008; 13(36):pii:
18966.</mixed-citation>
         </ref>
         <ref id="d91e1069a1310">
            <label>42</label>
            <mixed-citation id="d91e1076" publication-type="other">
Guy R, Gold J, Calleja JM, et al. Accuracy of serological assays for
detection of recent infection with HIV and estimation of population
incidence: a systematic review. Lancet Infect Dis 2009;9(12):747-759.</mixed-citation>
         </ref>
         <ref id="d91e1089a1310">
            <label>43</label>
            <mixed-citation id="d91e1096" publication-type="other">
Laeyendecker O, Rothman RE, Henson C, et al. The effect of viral
suppression on cross-sectional incidence testing in the Johns Hopkins
Hospital emergency department. J Acquir Immune Defic Syndr
2008; 48(2):211-215.</mixed-citation>
         </ref>
         <ref id="d91e1112a1310">
            <label>44</label>
            <mixed-citation id="d91e1119" publication-type="other">
Salazar-Gonzalez JF, Bailes E, Pham KT, et al. Deciphering human
immunodeficiency virus type 1 transmission and early envelope di-
versification by single-genome amplification and sequencing. J Virol
2008;82(8):3952-3970.</mixed-citation>
         </ref>
         <ref id="d91e1135a1310">
            <label>45</label>
            <mixed-citation id="d91e1142" publication-type="other">
Masharsky A, Dukhovlinova E, Verevochkin S, et al. A substantial
transmission bottleneck among newly and recently HIV-1-infected in-
jection drug users in St Petersburg, Russia. J Infect Dis 2010; 201(11):
1697-1702.</mixed-citation>
         </ref>
         <ref id="d91e1158a1310">
            <label>46</label>
            <mixed-citation id="d91e1165" publication-type="other">
Gasper-Smith N, Crossman DM, Whitesides JF, et al. Induction of
plasma (TRAIL), TNFR-2, Fas ligand, and plasma microparticles after
human immunodeficiency virus type 1 (HIV-1) transmission: impli-
cations for HIV-1 vaccine design. J Virol 2008; 82(15):7700-7710.</mixed-citation>
         </ref>
         <ref id="d91e1182a1310">
            <label>47</label>
            <mixed-citation id="d91e1189" publication-type="other">
Weber B, Orazi B, Raineri A, et al. Multicenter evaluation of a new
4th generation HIV screening assay Elecsys HIV combi. Clin Lab
2006;52:463-473.</mixed-citation>
         </ref>
         <ref id="d91e1202a1310">
            <label>48</label>
            <mixed-citation id="d91e1209" publication-type="other">
Weber B, Fall EH, Berger A, Doerr HW. Reduction of diagnostic win-
dow by new fourth-generation human immunodeficiency virus screen-
ing assays. J Clin Microbiol 1998;36(8):2235-2239.</mixed-citation>
         </ref>
         <ref id="d91e1222a1310">
            <label>49</label>
            <mixed-citation id="d91e1229" publication-type="other">
van Binsbergen J, Keur W, Siebelink A, et al. Strongly enhanced sen-
sitivity of a direct anti-HIV-1/-2 assay in seroconversion by incorpo-
ration of HIV p24 Ag detection: a new generation Vironostika HIV
Uni-Form II. J Virol Methods 1998;76:59-71.</mixed-citation>
         </ref>
         <ref id="d91e1245a1310">
            <label>50</label>
            <mixed-citation id="d91e1252" publication-type="other">
Ly TD, Laperche S, Brennan C, et al. Evaluation of the sensitivity and
specificity of six HIV combined p24 antigen and antibody assays. J
Virol Methods 2004; 122(2):185-194.</mixed-citation>
         </ref>
         <ref id="d91e1265a1310">
            <label>51</label>
            <mixed-citation id="d91e1272" publication-type="other">
Owen M, Patel P, Wesolowski L, et al. Evaluation of the Abbott AR-
CHITECT Ag/Ab Combo assay, an antigen/antibody combination test:
implications for US HIV testing programs. In: Program and abstracts
of the 16th Conference on Retroviruses and Opportunistic Infections;
2009; Montreal, Canada. Abstract 991.</mixed-citation>
         </ref>
         <ref id="d91e1291a1310">
            <label>52</label>
            <mixed-citation id="d91e1298" publication-type="other">
New rapid 4th generation HIV test now available from Inverness Med-
ical. PRLog. 6 October 2009.</mixed-citation>
         </ref>
         <ref id="d91e1309a1310">
            <label>53</label>
            <mixed-citation id="d91e1316" publication-type="other">
Nargessi D, Ou CY. MagaZorb: a simple tool for rapid isolation of
viral nucleic acids. J Infect Dis 2010;201(Suppl 1):S37-S41.</mixed-citation>
         </ref>
         <ref id="d91e1326a1310">
            <label>54</label>
            <mixed-citation id="d91e1333" publication-type="other">
Zhang N, Appella DH. Advantages of peptide nucleic acids as diagnostic
platforms for detection of nucleic acids in resource-limited settings. J
Infect Dis 2010;201(Suppl 1):S42-S5.</mixed-citation>
         </ref>
         <ref id="d91e1346a1310">
            <label>55</label>
            <mixed-citation id="d91e1353" publication-type="other">
Tang W, Chow WH, Li Y, Kong H, Tang YW, Lemieux B. Nucleic acid
assay system for tier II laboratories and moderately complex clinics to
detect HIV in low-resource settings. J Infect Dis 2010;201(Suppl 1):
S46-S51.</mixed-citation>
         </ref>
         <ref id="d91e1369a1310">
            <label>56</label>
            <mixed-citation id="d91e1376" publication-type="other">
Tang S, Hewlett I. Nanoparticle-based immunoassays for sensitive and
early detection of HIV-1 capsid (p24) antigen. J Infect Dis 2010;
201(Suppl 1):S59-S64.</mixed-citation>
         </ref>
         <ref id="d91e1389a1310">
            <label>57</label>
            <mixed-citation id="d91e1396" publication-type="other">
Lee HH, Dineva MA, Chua YL, Ritchie AV, Ushiro-Lumb I, Wisniewski
CA. Simple amplification-based assay: a nucleic acid-based point-of-
care platform for HIV-1 testing. J Infect Dis 2010; 201 (Suppl 1):
S65-S72.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
