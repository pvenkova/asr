<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">plantecology</journal-id>
         <journal-id journal-id-type="jstor">j50000076</journal-id>
         <journal-title-group>
            <journal-title>Plant Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">13850237</issn>
         <issn pub-type="epub">15735052</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20050866</article-id>
         <title-group>
            <article-title>Contrasting Effects of Herbivory on Plant Size and Reproductive Performance in Two Populations of the Critically Endangered Species, Euphorbia clivicola R. A. Dyer</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>M. F.</given-names>
                  <surname>Pfab</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>E. T. F.</given-names>
                  <surname>Witkowski</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">145</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i20050853</issue-id>
         <fpage>317</fpage>
         <lpage>325</lpage>
         <permissions>
            <copyright-statement>Copyright 1999 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20050866"/>
         <abstract>
            <p>Euphorbia clivicola R. A. Dyer is a Critically Endangered Northern Province endemic confined to two populations that have declined drastically over the last decade. One population is protected within a Nature Reserve, while the other occurs in a peri-urban area. In order to determine the causes of the population declines, herbivory damage to plants in both the protected and urban populations was quantified and the effects of herbivory on various aspects of the population biology were assessed. Mountain reedbuck are believed to have been responsible for the herbivory in the protected population. Herbivory on the above-ground branches probably caused the small sizes of protected plants. These were on average less than half the size of urban plants. Herbivory caused a reduction in the number of flowers and fruit produced per protected plant and may have prevented the maturation of flowers into fruit, thereby reducing the total regenerative output of the population. An eight-year absence of fire had resulted in the build-up of a dense moribund grass layer. The selection of E. clivicola plants by mountain reedbuck may therefore have been due to their relatively high nutritional value and accessibility in the thick moribund grass layer. Fencing off the population to prevent entry of herbivores, and implementing a more suitable fire management programme is recommended, as is the adoption of a new IUCN status for the species of Critically Endangered (CR A1).</p>
         </abstract>
         <kwd-group>
            <kwd>Flowering</kwd>
            <kwd>Fruiting</kwd>
            <kwd>Palatable</kwd>
            <kwd>Rare</kwd>
            <kwd>Reedbuck</kwd>
            <kwd>Threatened</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d776e148a1310">
            <mixed-citation id="d776e152" publication-type="other">
Acocks, J. P. H. 1988. Veld Types of South Africa. Memoirs of the
Botanical Survey of South Africa No. 57. Botanical Research
Institute, South Africa.</mixed-citation>
         </ref>
         <ref id="d776e165a1310">
            <mixed-citation id="d776e169" publication-type="other">
Beukes, P. C. 1987. Responses of grey rhebuck and bontebok to
controlled fires in coastal renosterveld. South African J. Wildlife
Res. 17 (3): 103-108.</mixed-citation>
         </ref>
         <ref id="d776e182a1310">
            <mixed-citation id="d776e186" publication-type="other">
Bossard, C. C. &amp;amp; Rejmanek, M. 1994. Herbivory, growth, seed pro-
duction, and resprouting of an exotic invasive shrub. Biol. Cons.
67: 193-200.</mixed-citation>
         </ref>
         <ref id="d776e199a1310">
            <mixed-citation id="d776e203" publication-type="other">
Bruce, E. A., Brueckner, A., Dyer, R. A., Kies, P. &amp;amp; Verdoorn, J. C.
1951. Newly described species. Bothalia 6 (1)221: 213-248.</mixed-citation>
         </ref>
         <ref id="d776e214a1310">
            <mixed-citation id="d776e218" publication-type="other">
Caughley, G. 1994. Directions in conservation biology. J. An. Ecol.
63: 215-244.</mixed-citation>
         </ref>
         <ref id="d776e228a1310">
            <mixed-citation id="d776e232" publication-type="other">
Cowling, R. M. &amp;amp; Hilton-Taylor, C. 1994. Patterns of plant diver-
sity and endemism in southern Africa: an overview. Pp. 31-52.
In: Huntley, B. J. (ed.), Botanical diversity in southern Africa.
Strelitzia 1. National Botanical Institute, Pretoria.</mixed-citation>
         </ref>
         <ref id="d776e248a1310">
            <mixed-citation id="d776e252" publication-type="other">
Crawley, M. J. &amp;amp; Weiner, J. 1991. Plant size variation and vertebrate
herbivory: Winter wheat grazed by rabbits. J. Appl. Ecol. 28:
154-172.</mixed-citation>
         </ref>
         <ref id="d776e265a1310">
            <mixed-citation id="d776e269" publication-type="other">
de Jong, T. J. &amp;amp; Klinkhamer, P. G. L. 1989. Limiting factors for seed
production in Cynoglossum officinale. Oecologia 80: 167-172.</mixed-citation>
         </ref>
         <ref id="d776e279a1310">
            <mixed-citation id="d776e283" publication-type="other">
Doak, D. F. 1992. Lifetime impacts of herbivory for a perennial
plant. Ecology 73: 2086-2099.</mixed-citation>
         </ref>
         <ref id="d776e293a1310">
            <mixed-citation id="d776e297" publication-type="other">
Dudley, C. O. 1997. The candelabra tree (Euphorbia ingens): a
source of water for black rhinoceros in Liwonde National Park,
Malawi. Koedoe 40 (1): 57-62.</mixed-citation>
         </ref>
         <ref id="d776e311a1310">
            <mixed-citation id="d776e315" publication-type="other">
Ehrlén, J. 1995. Demography of the perennial herb Lathyrus vernus.
I. Herbivory and individual performance. J. Ecol. 83: 287-295.</mixed-citation>
         </ref>
         <ref id="d776e325a1310">
            <mixed-citation id="d776e329" publication-type="other">
Fiedler, P. L. 1987. Life history and population dynamics of rare and
common Mariposa lilies (Calochortus pursh: Liliaceae). J. Ecol.
75: 977-995.</mixed-citation>
         </ref>
         <ref id="d776e342a1310">
            <mixed-citation id="d776e346" publication-type="other">
Fourie, S. P. 1986. The Transvaal, South Africa, Threatened Plants
Programme. Biol. Cons. 37: 23-42.</mixed-citation>
         </ref>
         <ref id="d776e356a1310">
            <mixed-citation id="d776e360" publication-type="other">
Gange, A. C. &amp;amp; Brown, V. K. 1989. Insect herbivory affects size
variability in plant populations. OIKOS 56: 351-356.</mixed-citation>
         </ref>
         <ref id="d776e370a1310">
            <mixed-citation id="d776e374" publication-type="other">
Goodrich, J. M. &amp;amp; Buskirk, S. W. 1994. Control of abundant native
vertebrates for conservation of endangered species. Cons. Biol.
9: 1357-1364.</mixed-citation>
         </ref>
         <ref id="d776e387a1310">
            <mixed-citation id="d776e391" publication-type="other">
Hall, A. V., de Winter, M., de Winter, B. &amp;amp; van Oosterhout, S.A.M.
1980. Threatened plants of Southern Africa. South African
National Scientific Programmes Report No. 45.</mixed-citation>
         </ref>
         <ref id="d776e405a1310">
            <mixed-citation id="d776e409" publication-type="other">
Hendrix, S. D. 1988. Herbivory and its impact on plant reproduc-
tion. Pp. 246-263. In: Plant Reproductive Ecology. Patterns and
Strategies. Oxford University Press, New York.</mixed-citation>
         </ref>
         <ref id="d776e422a1310">
            <mixed-citation id="d776e426" publication-type="other">
Hilton-Taylor, C. 1996. Red Data List of Southern African Plants.
Strelitzia, 4. National Botanical Institute, Pretoria.</mixed-citation>
         </ref>
         <ref id="d776e436a1310">
            <mixed-citation id="d776e440" publication-type="other">
Hilton-Taylor, C. 1997. Red Data List of southern African plants. 2.
Corrections and additions. Bothalia 27 (2): 195-209.</mixed-citation>
         </ref>
         <ref id="d776e450a1310">
            <mixed-citation id="d776e456" publication-type="other">
Knowles, L. 1995. The ecology and population biology of Eu-
phorbia barnardii White, Dyer &amp;amp; Sloan, a vulnerable North-
ern Province endemic. Honours Thesis. Department of Botany.
University of the Witwatersrand, South Africa.</mixed-citation>
         </ref>
         <ref id="d776e472a1310">
            <mixed-citation id="d776e476" publication-type="other">
Moran, V. C. &amp;amp; Hoffmann, J. H. 1989. The effects of herbivory by
a weevil species, acting alone and unrestrained by natural ene-
mies, on growth and phenology of the weed Sesbania punicea. J.
Applied Ecol. 26: 967-977.</mixed-citation>
         </ref>
         <ref id="d776e492a1310">
            <mixed-citation id="d776e496" publication-type="other">
Pfab, M. F. 1997. Population biology and ecology of Euphor-
bia clivicola R. A. Dyer, an endangered plant endemic to the
Northern Province of South Africa. Master of Science Research
Report. The University of the Witwatersrand, South Africa.</mixed-citation>
         </ref>
         <ref id="d776e513a1310">
            <mixed-citation id="d776e517" publication-type="other">
Pfab, M. F. &amp;amp; Witkowski, E. T. F. 1999. Fire survival of the Criti-
cally Endangered succulent, Euphorbia clivicola R.A. Dyer-fire-
avoider or fire-tolerant. African J. Ecol., in press.</mixed-citation>
         </ref>
         <ref id="d776e530a1310">
            <mixed-citation id="d776e534" publication-type="other">
Raal, P. A. 1986. Conservation Plan.Euphorbia clivicola. Transvaal
Provincial Administration, Nature Conservation Division, South
Africa.</mixed-citation>
         </ref>
         <ref id="d776e547a1310">
            <mixed-citation id="d776e551" publication-type="other">
Raal, P. A. 1988. Ecology and population biology of Euphor-
bia perangusta (Euphorbiaceae) in the Transvaal, South Africa.
Bothalia 18: 105-110.</mixed-citation>
         </ref>
         <ref id="d776e564a1310">
            <mixed-citation id="d776e568" publication-type="other">
Rizk, A-F. M. 1987. The chemical constituents and economic plants
of the Euphorbiaceae. Bot. J. Linnean Soc. 94: 293-326.</mixed-citation>
         </ref>
         <ref id="d776e578a1310">
            <mixed-citation id="d776e582" publication-type="other">
Roundy, B. A. &amp;amp; Ruyle, G. B. 1989. Effects of herbivory on twig dy-
namics of a Sonoran Desert shrub Simmondsia chinensis (Link)
Schn. J. Applied Ecol. 26: 701-710.</mixed-citation>
         </ref>
         <ref id="d776e595a1310">
            <mixed-citation id="d776e599" publication-type="other">
Rowe-Rowe, D. T. 1982. Influence of fire on antelope distribu-
tion and abundance in the Natal Drakensberg. South African J.
Wildlife Res. 12: 124-127.</mixed-citation>
         </ref>
         <ref id="d776e613a1310">
            <mixed-citation id="d776e617" publication-type="other">
Schemske, D. W., Husband, B. C., Ruckelshaus, M. H., Goodwillie,
C., Parker, I. M. &amp;amp; Bishop, J. G. 1994. Evaluating approaches
to the conservation of rare and endangered plants. Ecology 75:
584-606.</mixed-citation>
         </ref>
         <ref id="d776e633a1310">
            <mixed-citation id="d776e637" publication-type="other">
Shackleton, C. M. 1992. Area and species selection by wild un-
gulates in coastal sour grasslands of Mkambati Game Reserve,
Transkei, southern Africa. African J. Ecol. 30: 189-202.</mixed-citation>
         </ref>
         <ref id="d776e650a1310">
            <mixed-citation id="d776e654" publication-type="other">
Smithers, R. H. N. 1996. Smithers' Mammals of Southern Africa.
A Field Guide. Apps, P. (ed), Third Edition. Southern Book
Publishers, Halfway House.</mixed-citation>
         </ref>
         <ref id="d776e667a1310">
            <mixed-citation id="d776e671" publication-type="other">
Stephenson, A. G. 1981. Flower and fruit abortion: Proximate
causes and ultimate functions. Ann. Rev. Ecol. Syst. 12: 253-
279.</mixed-citation>
         </ref>
         <ref id="d776e684a1310">
            <mixed-citation id="d776e688" publication-type="other">
Stuart, C. &amp;amp; Stuart, T. 1994. A Field Guide to the Tracks and
Signs of Southern and East African Wildlife. Southern Book
Publishers, Halfway House.</mixed-citation>
         </ref>
         <ref id="d776e701a1310">
            <mixed-citation id="d776e705" publication-type="other">
Watkinson, A. R. 1986. Plant population dynamics. Pp. 137-184.
In: Crawley, M. J. (ed.), Plant Ecology. Blackwell Scientific
publications, Oxford.</mixed-citation>
         </ref>
         <ref id="d776e719a1310">
            <mixed-citation id="d776e723" publication-type="other">
Wilsey, B. J. 1996. Variation in use of green flushes following burns
among African ungulate species: the importance of body size.
African J. Ecol. 34: 32-38.</mixed-citation>
         </ref>
         <ref id="d776e736a1310">
            <mixed-citation id="d776e740" publication-type="other">
Witkowski, E. T. F., Lamont, B. B. &amp;amp; Obbens, F. J. 1994. Com-
mercial picking of Banksia hookeriana in the wild reduces
subsequent shoot, flower and seed production. J. Applied Ecol.
31: 508-520.</mixed-citation>
         </ref>
         <ref id="d776e756a1310">
            <mixed-citation id="d776e760" publication-type="other">
Witkowski, E. T. F., Knowles, L. &amp;amp; Liston, R. J. 1997. Threat-
ened plants in the Northern Provinces of South Africa: Case
studies and future approaches. Pp. 446-451. In: Hale, P. (ed.),
Conservation Outside Nature Reserves. Surrey Beatty, Brisbane.</mixed-citation>
         </ref>
         <ref id="d776e776a1310">
            <mixed-citation id="d776e780" publication-type="other">
Zar, J. H. 1984. Biostatistical Analysis. 2nd edition. Prentice-Hall,
Englewood.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
