<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id journal-id-type="jstor">j100641</journal-id>
         <journal-title-group>
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">03057070</issn>
         <issn pub-type="epub">14653893</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2637346</article-id>
         <article-categories>
            <subj-group>
               <subject>Debate</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Has the Mfecane a Future? A Response to the Cobbing Critique</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. D.</given-names>
                  <surname>Omer-Cooper</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>1993</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">19</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i324955</issue-id>
         <fpage>273</fpage>
         <lpage>294</lpage>
         <page-range>273-294</page-range>
         <permissions>
            <copyright-statement>Copyright Oxford University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2637346"/>
         <abstract>
            <p>In recent years Julian Cobbing has advanced a wide-ranging critique of the concept of the Mfecane in southern, central and east African history. The Mfecane, he has maintained, was in origin a colonial myth to conceal white wrong-doing and justify land expropriation. Revived by well-intentioned `Africanist' historians in the 1960s the concept has subsequently been exploited to justify aspects of apartheid. Rather than being the accompaniments of institutional change in African societies, he maintains that the wars and upheavals of the period must be attributed to the effects of increased white demand for African labour expressed in the massive expansion of the Delagoa Bay slave trade and slave raiding for the Cape labour market by Griquas with missionary and official involvement in Trans Orangia, white traders in Natal, and British military forces in the Transkei. Examination of the evidence, however, shows that the expansion of the slave trade in Delagoa Bay came after the area had been affected by the spread of upheavals from the south and could not have been their cause. Evidence for large-scale slave raiding and trading by Griquas, missionaries, Natal traders and British military commanders likewise proves unsubstantiated. The bold new paradigm cannot be sustained. The debate has, however, raised important new questions, enlivened research in the area and ensured that the Mfecane will continue to occupy a prominent place in the developing historiography of southern Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1501e129a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d1501e136" publication-type="journal">
'The Mfecane Defended', Southern African
Review of Books, 4, 4 &amp;5 (1991), pp. 11-15.<issue>4</issue>
                  <fpage>11</fpage>
                  <volume>4</volume>
                  <source>Southern African Review of Books</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e162a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1501e169" publication-type="book">
J.D. Omer-Cooper, The Zulu Aftermath (Longmans, London, 1966).<person-group>
                     <string-name>
                        <surname>Omer-Cooper</surname>
                     </string-name>
                  </person-group>
                  <source>The Zulu Aftermath</source>
                  <year>1966</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e190" publication-type="book">
M.H. Wilson and L.M. Thompson
(eds), The Oxford History of South Africa, vol 1, (Oxford, 1969))<person-group>
                     <string-name>
                        <surname>Wilson</surname>
                     </string-name>
                  </person-group>
                  <volume>1</volume>
                  <source>The Oxford History of South Africa</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e218" publication-type="book">
Julian Cobbing, 'The Case against the Mfecane' unpublished seminar paper, University of
Cape Town, 1983)<person-group>
                     <string-name>
                        <surname>Cobbing</surname>
                     </string-name>
                  </person-group>
                  <source>The Case against the Mfecane</source>
                  <year>1983</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e243a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1501e250" publication-type="other">
The Zulu Aftermath, 1966, Chapter 2, pp.24-27.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e256" publication-type="book">
The Cambridge History of Africa, vol 5 (Cambridge
University Press, 1976), chapter 9, pp. 319-352<comment content-type="section">chapter 9</comment>
                  <fpage>319</fpage>
                  <source>The Cambridge History of Africa, vol 5</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e278" publication-type="book">
J.D. Omer-Cooper, A History of Southern Africa
(James Currey, London, 1987), ch.4, pp. 52-66.<person-group>
                     <string-name>
                        <surname>Omer-Cooper</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">ch.4</comment>
                  <fpage>52</fpage>
                  <source>A History of Southern Africa</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e310a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1501e317" publication-type="other">
Mfecane as I then understood it is set out in The Zulu Aftermath,
1966, ch.12, pp. 168-182.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e328a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1501e337" publication-type="other">
Julian Cobbing, 'The Case against the Mfecane'. University of Cape Town, 1983</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e343" publication-type="other">
'The Case
against the Mfecane', unpublished seminar paper, University of the Witwatersrand, 1984</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e353" publication-type="other">
'The Myth of
the Mfecane', unpublished seminar paper, University of Durban Westville, 1987</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e362" publication-type="journal">
'The Mfecane as
Alibi: thoughts on Dithakong and Mbolompo', Journal of African? History, 29 (1988), pp. 487-519<fpage>487</fpage>
                  <volume>29</volume>
                  <source>Journal of African? History</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e384" publication-type="book">
C. DeB. Webb and John Wright, eds, The James Stuart Archives, 4 vols
to date, University of Natal Press, 1976-<person-group>
                     <string-name>
                        <surname>Webb</surname>
                     </string-name>
                  </person-group>
                  <source>The James Stuart Archives</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e408" publication-type="journal">
Journal of Natal and Zulu History, XI, 1988, pp. 115-153<fpage>115</fpage>
                  <volume>XI</volume>
                  <source>Journal of Natal and Zulu History</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e426" publication-type="other">
'Grasping the Nettle: the Slave trade and the Early Zulu', unpublished seminar paper, University of
Natal, Pietermaritzburg, September 1990</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e435" publication-type="other">
'Rethinking the Roots of Violence in Southern Africa,
c1790-1840', unpublished seminar paper, June 1991</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e445" publication-type="book">
'Ousting the Mfecane', paper presented to the
colloquium on the Mfecane Aftermath, University of the Witwatersrand, 6-9 September 1991.<comment content-type="section">Ousting the Mfecane</comment>
                  <source>colloquium on the Mfecane Aftermath, University of the Witwatersrand, 6-9 September 1991</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e461a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1501e468" publication-type="journal">
J.B. Wright, 'Political Mythology and the Making of Natal's Mfecane', The Canadian Journal of
African Studies, 23, 2 (1989), pp. 272-291.<object-id pub-id-type="doi">10.2307/485525</object-id>
                  <fpage>272</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e484a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1501e491" publication-type="other">
John Wright, 'Political transformations in the Thukela - Mzimkhulu Region of Natal in the
late 18th and early 19th centuries', unpublished seminar paper, University of Antananarivo, July 1991,
pp. 8-12.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e504a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1501e511" publication-type="other">
Julian Cobbing, 'The Ndebele under the Khumalos, 1820-96', unpublished PhD thesis, London
University, 1976.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e521a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1501e528" publication-type="book">
J. Guy in 'Ecological Factors in the Rise of Shaka and the Zulu
Kingdom' in Shula Marks and Anthony Atmore (eds), Economy and Society in Pre-Industrial South
Africa (Longman, London and New York, 1980), pp. 102-119.<person-group>
                     <string-name>
                        <surname>Guy</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Ecological Factors in the Rise of Shaka and the Zulu Kingdom</comment>
                  <fpage>102</fpage>
                  <source>Economy and Society in Pre-Industrial South Africa</source>
                  <year>1980</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e563a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1501e570" publication-type="book">
Alan K. Smith, 'The Trade of Delagoa Bay as a Factor in Nguni politics 1750-1835', in Leonard
Thompson, ed., African Societies in Southern Africa (London, 1969)<person-group>
                     <string-name>
                        <surname>Smith</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The Trade of Delagoa Bay as a Factor in Nguni politics 1750-1835</comment>
                  <source>African Societies in Southern Africa</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e598" publication-type="other">
'The Struggle for the Control of
Southern Mozambique 1720-1835', unpublished PhD thesis, University of California, Los Angeles,
1970</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e610" publication-type="other">
D.W. Hedges, 'Trade and Politics in Southern Mozambique and Zululand in the 18th and early
19th centuries', unpublished PhD thesis, University of London, 1978</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e620" publication-type="book">
P. Bonner, 'The Dynamics of
late 18th Century Northern Nguni Society: Some Hypotheses', in J. Peires, ed, Before and After Shaka
(Grahamstown, 1981), pp. 74-81<person-group>
                     <string-name>
                        <surname>Bonner</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The Dynamics of late 18th Century Northern Nguni Society: Some Hypotheses</comment>
                  <fpage>74</fpage>
                  <source>Before and After Shaka</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e654" publication-type="book">
P. Bonner, Kings, Commoners and Concessionaires (Cambridge,
1982).<person-group>
                     <string-name>
                        <surname>Bonner</surname>
                     </string-name>
                  </person-group>
                  <source>Kings, Commoners and Concessionaires</source>
                  <year>1982</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e680a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1501e687" publication-type="journal">
p. Harries, 'Slavery, Social Incorporation and Surplus Extraction: the Nature of Free and Unfree
Labour in South-east Africa', Journal of African History, 22 (1981), pp. 309-330<object-id pub-id-type="jstor">10.2307/181806</object-id>
                  <fpage>309</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e702" publication-type="book">
G. Liesgang, 'A First
Look at the Import and Export Trade of Mozambique, 1800-1914', ch. v in G. Liesgang, H. Pasch, A.
Jones, eds, Figuring African Trade (Berlin, 1986).<person-group>
                     <string-name>
                        <surname>Liesgang</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">A First Look at the Import and Export Trade of Mozambique, 1800-1914</comment>
                  <source>Figuring African Trade</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e734a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1501e741" publication-type="other">
Julian Cobbing, 'Grasping the Nettle'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e747" publication-type="other">
John Wright, 'Political Transformations'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e754a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1501e761" publication-type="book">
Martin Hall, The Changing Past: Farmers, Kings and Traders in Southern
Africa, 200-1860 (Cape Town, 1987).<person-group>
                     <string-name>
                        <surname>Hall</surname>
                     </string-name>
                  </person-group>
                  <source>The Changing Past: Farmers, Kings and Traders in Southern Africa, 200-1860</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e786a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1501e793" publication-type="book">
Peter Delius, The Land Belongs to Us: Pedi Polity, the Boers and the British in the Nineteenth
Century Transvaal (London, 1983), pp. 11-19.<person-group>
                     <string-name>
                        <surname>Delius</surname>
                     </string-name>
                  </person-group>
                  <fpage>11</fpage>
                  <source>The Land Belongs to Us: Pedi Polity, the Boers and the British in the Nineteenth Century Transvaal</source>
                  <year>1983</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e822a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1501e829" publication-type="other">
P. Harries, 'Slavery, Social Incorporation and Surplus Extraction'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e836a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1501e843" publication-type="book">
Elizabeth A. Eldredge, 'Sources of Conflict in Southern Africa, ca1800-1830 — the Mfecane
reconsidered', paper submitted to the colloquium on the Mfecane Aftermath, University of the
Witwatersrand, 6-9 September 1991, especially pp. 4-21.<person-group>
                     <string-name>
                        <surname>Eldredge</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Sources of Conflict in Southern Africa, ca1800-1830 — the Mfecane reconsidered</comment>
                  <fpage>4</fpage>
                  <source>colloquium on the Mfecane Aftermath, University of the Witwatersrand, 6-9 September 1991</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e876a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1501e883" publication-type="other">
Martin Hall, The Changing Past, p. 126.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e890a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1501e897" publication-type="other">
J. Gump, 'Origins of the Mfecane: an Ecological Perspective', paper presented to the
colloquium on the Mfecane Aftermath.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e907a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1501e914" publication-type="book">
J. D. Omer-Cooper, 'Aspects of Political Change in the Nineteenth century
Mfecane', in C. M. Thompson, ed., African Societies in Southern Africa (Heinemann, 1969), pp. 207-
229.<person-group>
                     <string-name>
                        <surname>Omer-Cooper</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Aspects of Political Change in the Nineteenth century Mfecane</comment>
                  <fpage>207</fpage>
                  <source>African Societies in Southern Africa</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e949a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1501e956" publication-type="other">
John Wright, 'Political Transformations'.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e962" publication-type="other">
P. Bonner, Kings, Commoners
and Concessionaires.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e972a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1501e979" publication-type="other">
Julian Cobbing, 'Grasping the Nettle'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e986a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1501e993" publication-type="other">
Julian Cobbing's trenchantly expressed conviction on this point in 'Grasping the Nettle'
and 'Rethinking the Roots of Violence'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1002" publication-type="other">
John Wright's much more cautious argumentation in
'Political Transformations'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1013a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1501e1020" publication-type="other">
Julian Cobbing, 'Grasping the Nettle', p. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1027a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1501e1034" publication-type="other">
Ibid., p. 10.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1041a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1501e1048" publication-type="other">
J. Wright, 'Political Transformations'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1054" publication-type="other">
'A.T. Bryant and "The Wars of Shaka"", History in
Africa, 1991.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1064a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1501e1071" publication-type="other">
Julian Cobbing,
'The Mfecane as Alibi'</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1081a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1501e1088" publication-type="other">
John Wright, 'The Dynamics of Power and Conflict in the Thukela-Mzimkhulu region in the late
18th and early 19th centuries: a critical reconstruction', unpublished PhD thesis, University of the
Witwatersrand, 1990.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1100" publication-type="other">
Carolyn Hamilton, 'Ideology, Oral Tradition and the Struggle for Power in the
early Zulu Kingdom', unpublished MA thesis, University of the Witwatersrand, 1986.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1110a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1501e1117" publication-type="other">
C.deB. Webb and J.B. Wright (eds), The James Stuart Archive, vol.II, p. 251.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1123" publication-type="other">
Julian
Cobbing, 'Grasping the Nettle', p. 11.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1134a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1501e1141" publication-type="other">
P. Harries, 'Slavery, Social Incorporation and Surplus Extraction'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1148a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1501e1155" publication-type="other">
Peter Delius, The Land Belongs to Us, pp. 22-
23.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1165a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1501e1172" publication-type="other">
John Wright, 'Political Transformations', p. 13.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1179a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1501e1186" publication-type="journal">
Julian Cobbing, 'The evolution of Ndebele Amabutho', Journal of African History, XV, 4 (1974),
pp. 607-631.<object-id pub-id-type="jstor">10.2307/180993</object-id>
                  <fpage>607</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1202a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1501e1209" publication-type="other">
The Zulu Aftermath, pp. 148-149</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1215" publication-type="other">
Julian Cobbing in his thesis on 'The Ndebele under the
Khumalos'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1225a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1501e1232" publication-type="other">
The Zulu Aftermath, pp. 148-149</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1240a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1501e1247" publication-type="other">
John Wright, 'Political Transformations'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1254a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1501e1261" publication-type="other">
Julian Cobbing, 'The Mfecane as Alibi'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1268a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1501e1275" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1282a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1501e1289" publication-type="other">
Julian Cobbing, 'Grasping the Nettle', p. 13</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1295" publication-type="other">
'Rethinking the Roots of Violence', pp. 25-26.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1302a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1501e1309" publication-type="other">
Julian Cobbing, 'The Mfecane as Alibi'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1316a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1501e1323" publication-type="other">
Julian Cobbing, 'The Case against the Mfecane'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1331a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1501e1338" publication-type="other">
his 'Political Transformations'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1344" publication-type="book">
his paper, 'A.T. Bryant and "The Wars of
Shaka"', paper presented to workshop on pre-colonial Natal and Zululand, Pietermaritzburg, October
1990.<comment content-type="section">A.T. Bryant and "The Wars of Shaka"</comment>
                  <source>workshop on pre-colonial Natal and Zululand, Pietermaritzburg, October 1990</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1363a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1501e1370" publication-type="book">
J.B. Peires, 'Matiwane's Road to Mbholompo', paper presented to the colloquium on the
Mfecane Aftermath, University of Witwatersrand, 6-9 September, 1991, pp. 336-37.<person-group>
                     <string-name>
                        <surname>Peires</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Matiwane's Road to Mbholompo</comment>
                  <fpage>336</fpage>
                  <source>colloquium on the Mfecane Aftermath, University of Witwatersrand, 6-9 September, 1991</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1399a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1501e1406" publication-type="book">
Margaret Kinsman, 'Resketching the Mfecane: the Impact of Violence on Rolong life, 1823-
1836', paper presented to the colloquium on the Mfecane Aftermath, 6-9 September 1991, pp. 1-3.<person-group>
                     <string-name>
                        <surname>Kinsman</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Resketching the Mfecane: the Impact of Violence on Rolong life, 1823-1836</comment>
                  <fpage>1</fpage>
                  <source>colloquium on the Mfecane Aftermath, 6-9 September 1991</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1434" publication-type="book">
Guy
Hartley, 'The Battle of Dithakong and Mfecane Theory' paper presented to the colloquium on the
Mfecane Aftermath, pp. 9-11.<person-group>
                     <string-name>
                        <surname>Hartley</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The Battle of Dithakong and Mfecane Theory</comment>
                  <fpage>9</fpage>
                  <source>colloquium on the Mfecane Aftermath</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1466a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1501e1473" publication-type="book">
R. Kent Rasmussen, Migrant Kingdom: Mzilikazis Ndebele in South Africa (London
and Cape Town, 1978), pp. 27-132.<person-group>
                     <string-name>
                        <surname>Rasmussen</surname>
                     </string-name>
                  </person-group>
                  <fpage>27</fpage>
                  <source>Migrant Kingdom: Mzilikazis Ndebele in South Africa</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1502a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1501e1509" publication-type="other">
Guy Hartley, 'The Battle of Dithakong', pp. 10-11</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1515" publication-type="other">
Margaret Kinsman, 'Resketching the
Mfecane'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1525a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d1501e1532" publication-type="other">
Guy Hartley, 'The Battle of Dithakong', pp. 9-15</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1501e1538" publication-type="other">
Elizabeth Eldredge, 'Sources of
Conflict in Southern Africa', pp. 25-26.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1549a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1501e1556" publication-type="other">
J.B. Peires, 'Matiwane's Road to Mbholompo', pp. 9-25.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1563a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1501e1570" publication-type="other">
Julian Cobbing, 'The Mfecane as Alibi'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1577a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1501e1584" publication-type="other">
Peter Delius, The Land Belongs to Us, pp. 34-37, 136-147.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1591a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1501e1598" publication-type="other">
Julian Cobbing, 'The Mfecane as Alibi', p. 513.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1605a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1501e1612" publication-type="other">
Julian Cobbing, 'The Mfecane as Alibi', p. 492.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1619a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1501e1626" publication-type="other">
footnote 47 above.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1634a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d1501e1641" publication-type="other">
Julian Cobbing, 'The Mfecane as Alibi', pp. 492-493.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1648a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d1501e1655" publication-type="other">
Hartley, 'The Battle of Dithakong', pp. 1-9.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1662a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d1501e1669" publication-type="book">
Peter Sanders, Moshoeshoe: Chief of the Sotho, London, 1975, pp. 60-74.<person-group>
                     <string-name>
                        <surname>Sanders</surname>
                     </string-name>
                  </person-group>
                  <fpage>60</fpage>
                  <source>Moshoeshoe: Chief of the Sotho</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1694a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d1501e1701" publication-type="other">
'The Mfecane as Alibi', pp. 502-503.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1708a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d1501e1715" publication-type="other">
J.B. Peires, 'Matiwane's Road to Mbholompo', pp. 24-44.</mixed-citation>
            </p>
         </fn>
         <fn id="d1501e1722a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d1501e1729" publication-type="other">
Swazi and Lesotho kingdoms given in The Zulu Aftermath</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
