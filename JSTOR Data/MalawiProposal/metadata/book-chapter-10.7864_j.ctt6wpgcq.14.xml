<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wpgcq</book-id>
      <subj-group>
         <subject content-type="call-number">QC903.C545 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Climatic changes</subject>
         <subj-group>
            <subject content-type="lcsh">Economic aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Global environmental change</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Global temperature changes</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Poverty</subject>
         <subj-group>
            <subject content-type="lcsh">Government policy</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic geography</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">International cooperation</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Climate Change and Global Poverty</book-title>
         <subtitle>A Billion Lives in the Balance?</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>BRAINARD</surname>
               <given-names>LAEL</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>JONES</surname>
               <given-names>ABIGAIL</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>PURVIS</surname>
               <given-names>NIGEL</given-names>
            </name>
         </contrib>
         <role content-type="editor">Editors</role>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>10</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="epub">9780815703815</isbn>
      <isbn content-type="epub">0815703813</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wpgcq"/>
      <abstract abstract-type="short">
         <p>Climate change threatens all people, but its adverse effects will be felt most acutely by the world's poor. Absent urgent action, new threats to food security, public health, and other societal needs may reverse hard-fought human development gains.<italic>Climate Change and Global Poverty</italic>makes concrete recommendations to integrate international development and climate protection strategies. It demonstrates that effective climate solutions must empower global development, while poverty alleviation itself must become a central strategy for both mitigating emissions and reducing global vulnerability to adverse climate impacts.</p>
      </abstract>
      <counts>
         <page-count count="307"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Talbott</surname>
                           <given-names>Strobe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>Although climate change is a global threat, it is especially menacing to the world’s poor. As the mean temperature of the Earth rises, the impact of climate change on sources of water and food, and on health and living standards, will be greater in those regions that are already struggling. Waves of “climate refugees,” damage to traditional cultures, increasingly frequent and severe floods and droughts—these and other results of global warming will constitute a humanitarian disaster on top of the environmental one.</p>
                     <p>The costs of climate change will be political—and geopolitical—as well. Weak states will become failed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Left unchecked, climate change will compromise human development. This book sets out to determine how, and critically, what can be done. The volume brings together scholars from both the climate change and global development communities whose engagement ranges from activists and practitioners to academics involved in rigorous analysis. Building on scientific modeling of the effects of climate change in the developing world, in the chapters that follow these contributors offer sensible solutions to help the poor mitigate their own greenhouse gas emissions, adapt to the changing global environment, and benefit from international carbon markets and other innovative financial mechanisms. Though</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Double Jeopardy:</title>
                     <subtitle>What the Climate Crisis Means for the Poor</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jones</surname>
                           <given-names>Abigail</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>LaFleur</surname>
                           <given-names>Vinca</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Purvis</surname>
                           <given-names>Nigel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>10</fpage>
                  <abstract>
                     <p>As the twenty-first century unfolds, humanity faces two defining challenges: lifting the lives of the global poor and stabilizing the Earth’s climate. Our success or failure in meeting these challenges will shape the future for our children and successive generations, and many choices we make today will drive consequences for years to come.</p>
                     <p>Around the world, extreme poverty fuels a volatile mix of desperation and instability—exhausting governing institutions, depleting resources, weakening leaders, and crushing hope. Conscience demands that we confront the facts that 10 million children under five years old still perish each year of largely preventable causes; that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Climate Change Impacts in the Developing World:</title>
                     <subtitle>Implications for Sustainable Development</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nyong</surname>
                           <given-names>Anthony</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>The Fourth Assessment Report of the Intergovernmental Panel on Climate Change noted that between 1906 and 2005, the global average surface temperature increased by about 0.74°C.¹ The panel’s climate models have conservatively estimated that global average surface temperature could increase by up to 5.8 °C by 2100. Most of the warming observed over the last fifty years is attributable to anthropogenic activities, through the emission of greenhouse gases—mainly carbon dioxide, methane, and nitrous oxides, with carbon dioxide being the most important contributor. Though the industrial countries are largely responsible for a greater proportion of the carbon dioxide emissions through</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Toward a New International Climate Change Agreement</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Diringer</surname>
                           <given-names>Elliot</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>For most of the past decade, even as the effects of global warming swung from largely theoretical to distressingly real, international climate change negotiations have barely inched forward. But two high-profile events seem likely to ensure that in 2009 the search for a global solution to the growing climate crisis will shift into high gear.</p>
                     <p>The first is the inauguration in January of a new U.S. president. Quite arguably, a lack of will on the part of the United States has been the single greatest constraint on the international climate effort, no more so than in the recent past. But</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Greenhouse Gas Mitigation Efforts in China:</title>
                     <subtitle>Progress and Opportunities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Helme</surname>
                           <given-names>Ned</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>Most of the world recognizes that the climate is changing. Not as noticeable is the change in the international climate policy world. At the UN climate conference held in Bali in 2007, for example, developing countries such as Brazil, China, Mexico, and South Africa exerted leadership within the international negotiations, and they are now taking aggressive unilateral actions to reduce greenhouse gas (GHG) emissions at home. These contributions will shape the post-2012 international climate change treaty and are critical to protecting the planet from the worst effects of climate change.</p>
                     <p>One major developing country that is front and center in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Linking Communities, Forests, and Carbon</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jenkins</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>A large share of greenhouse gas emissions comes from deforestation and forest degradation in tropical countries, which continue at an alarming rate despite decades of global advocacy and policy initiatives. Between 1990 and 2005, the rate of deforestation averaged more than 13 million hectares a year. The highest emission rates are associated with industrial agriculture (soybeans, cotton) and cattle ranching in the Amazon River Basin, the palm oil plantations of Southeast Asia, and household slash-and-burn agriculture in Africa.</p>
                     <p>Tropical deforestation happens, very simply, because it is more profitable to cut down trees and forests than to look after them. This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Integrating Climate Change into Development:</title>
                     <subtitle>Multiple Benefits of Mitigation and Adaptation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rahman</surname>
                           <given-names>Atiq</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>104</fpage>
                  <abstract>
                     <p>Climate change is the greatest threat to human security and civilization, and it poses an enormous challenge for sustainable development. This problem has largely been created by the industrial countries, but the world’s poor are the main victims of the negative effects of climate change. These various effects are being felt locally in developing countries. Climate change will increase global food insecurity, hunger, and poverty, and it may exacerbate migration and social conflicts. The extreme climatic events across the world have reconfirmed the assertion by the Intergovernmental Panel on Climate Change (IPCC) that poor people in the developing countries are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Development in the Balance:</title>
                     <subtitle>Agriculture and Water</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mendelsohn</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>120</fpage>
                  <abstract>
                     <p>The nost RECENT report by the Intergovernmental Panel on Climate Change (IPCC) provides ever more conclusive evidence that Earth’s climate will continue to gradually warm into the future because of accumulating greenhouse gases. Expected projections of global temperatures imply continued warming by 2100 of between 2º and 4ºC, depending on future greenhouse gas emission levels.¹ This chapter examines the risk these climate changes suggest for the agriculture and water sectors and what can be done to help reduce the potential harm. The focus of the chapter is especially upon the risks to the global poor. There are three key risks</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Public Health Adaptation to Climate Change in Low-Income Countries</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ebi</surname>
                           <given-names>Kristie L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>130</fpage>
                  <abstract>
                     <p>Climate change is challenging the effectiveness of public health policymakers and practitioners to promote physical and mental health, and prevent disease, injury, and disability. The potential health effects of climate change are diverse and wide-ranging. Heat waves, floods, droughts, windstorms, and fires annually affect millions of people and cause billions of dollars of damage, with the largest impacts in low- and middle-income countries. The frequency and intensity of these extreme events are expected to increase over the coming decades as a consequence of climate change, suggesting that the associated health effects also could increase. Climate change also could alter or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Linking Adaptation and Disaster Risk Reduction</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Huq</surname>
                           <given-names>Saleemul</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ayers</surname>
                           <given-names>Jessica</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>142</fpage>
                  <abstract>
                     <p>There are obvious synergies and overlaps between adaptation to climate change and disaster risk reduction (DRR). In the first place, both adaptation and DRR aim to build the resilience of vulnerable communities in the face of hazards (often, but not always in the case of DRR, climatic hazards). Both seek to do so in the context of sustainable development.¹ Further, the Fourth Assessment Report of the Intergovernmental Panel on Climate Change (IPCC) highlights that climate change is set to increase the magnitude of extreme climatic events and generate new threats of climate-related disasters in previously lower-risk regions, making action on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Climate-Security Connection:</title>
                     <subtitle>What It Means for the Poor</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Busby</surname>
                           <given-names>Joshua W.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>When Cyclone Nargis struck Myanmar’s Irrawaddy River Delta in May 2008, the world received yet another reminder of nature’s sometimes terrifying capacity for physical destruction. In its wake, the storm left more than 80,000 dead, another 50,000 missing, and some 2.4 million survivors.¹ The storm also brought unprecedented scrutiny to Myanmar’s military government, which was seen as negligent in ignoring early warnings from Indian meteorologists and dilatory in responding to the pleas for help from its country’s displaced population.² A few isolated voices called for military intervention to provide aid to the Burmese people.³ More serious wrangling occurred over visas</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Financing Adaptation to a Warmer World:</title>
                     <subtitle>Opportunities for Innovation and Experimentation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bapna</surname>
                           <given-names>Manish</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>McGray</surname>
                           <given-names>Heather</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>181</fpage>
                  <abstract>
                     <p>Climate change is upon us. The Earth is warming, seasons are shifting, species are migrating, and water is flowing in new patterns. The accelerating and deepening effects of climate change will touch everyone on Earth, but those who stand to suffer most are the poor. People and governments must find the will and the means to slow, stop, and reverse the buildup of greenhouse gases in the atmosphere to avert catastrophic warming. But it is already too late to avert some serious consequences. We must also learn to adapt to a warmer world.</p>
                     <p>This question of adaptation is a particularly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Exploring the Potential for Public-Private Insurance to Help the World’s Poor to Adapt and Thrive as the Climate Changes</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Burton</surname>
                           <given-names>Ian</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dickinson</surname>
                           <given-names>Thea</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>207</fpage>
                  <abstract>
                     <p>Climate change is upon us and, even under the most optimistic emission scenarios from the Intergovernmental Panel on Climate Change, will continue for many decades to come. In these most authoritative projections, even if greenhouse gas emissions peak in 2040, the global mean temperature will continue to rise beyond the end of the twenty-first century. Due to inertia in the climate system, ice melting and sea-level rise are set to continue for much longer. The mitigation of climate change itself (the control of greenhouse gas emissions, and more specifically, the reduction of carbon dioxide emissions to a fraction of their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Corporate Action on Climate Adaptation and Development:</title>
                     <subtitle>Mobilizing New Partnerships to Build Climate Change Resilience in Developing Countries and Communities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nelson</surname>
                           <given-names>Jane</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>223</fpage>
                  <abstract>
                     <p>In the fifteen years since the Rio de Janeiro Earth Summit, the private sector’s response to sustainable development, and to climate change more specifically, has become increasingly strategic and constructive at the levels of both individual companies and collective initiatives. Denial and obstruction still occur, but many major corporations and business associations have started to address the complex, interrelated challenges of sustaining growth and wealth creation in the face of a changing climate, growing water stress, and emerging threats to ecosystems, biodiversity, human health, food security, and the resilience of economic and social systems.</p>
                     <p>The business community has played an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Mobilizing Action for Climate Change Adaptation in the North and South</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Coleman</surname>
                           <given-names>Heather K.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Offenheiser</surname>
                           <given-names>Raymond C.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Waskow</surname>
                           <given-names>David</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>260</fpage>
                  <abstract>
                     <p>The challenge that climate change poses for global poverty reduction and development is both broad and deep. The consequences of climate change will have ramifications through the entire economic, political, and social fabric of developing countries in ways that extend far beyond the environmental arena, and climate change will affect those countries in profound ways that will alter their development paths and place substantial obstacles in the way of their ability to meet the UN’s Millennium Development Goals. The task before the international community, and particularly advocates for global civil society, is to recognize and act on the full breadth</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.19</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>277</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>287</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgcq.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>301</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
