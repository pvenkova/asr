<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">oikos</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100306</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Oikos</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Munksgaard International Publishers, Ltd.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00301299</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16000706</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2307/3546480</article-id>
         <title-group>
            <article-title>Resource Patch Density and Larval Aggregation in Mushroom-Breeding Flies</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Stephen B.</given-names>
                  <surname>Heard</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">81</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i283210</issue-id>
         <fpage>187</fpage>
         <lpage>195</lpage>
         <page-range>187-195</page-range>
         <permissions>
            <copyright-statement>Copyright 1998 Oikos</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3546480"/>
         <abstract>
            <p>For organisms exploiting patchy resource landscapes, the degree of aggregation of individuals across patches has important implications for population and community ecology. For insects breeding in mushrooms, carrion, or fallen fruit, larval aggregation has previously been shown to be sensitive to the density of ovipositing females and to variation in patch quality and detectability. However, effects of resource patch density (interpatch spacing) have not been examined. I tested for an effect of patch density on larval aggregation in natural populations of mushroom-breeding flies. Larval aggregation increased strongly and consistently with declining patch density (increasing patch spacing). This effect could be due to increased aggregation of ovipositing females, but is more likely due to increased clutch sizes laid by females facing higher travel costs for movement among patches (when those patches are more distantly spaced).</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1716e129a1310">
            <mixed-citation id="d1716e133" publication-type="journal">
Adamson, M. and Ludwig, D.1993. Oedipal mating as a
factor in sex allocation in haplodiploids. - Philos. Trans.
R. Soc. Lond. B341: 195-202.<object-id pub-id-type="jstor">10.2307/55811</object-id>
               <fpage>195</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e152a1310">
            <mixed-citation id="d1716e156" publication-type="journal">
Anderson, P. and Ldfqvist, J.1996. Asymmetric oviposition
behaviour and the influence of larval competition in the
two pyralid moths, Ephestia kuehniella and Plodia inter-
punctella. - Oikos76: 47-56.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Anderson</surname>
                  </string-name>
               </person-group>
               <fpage>47</fpage>
               <volume>76</volume>
               <source>Oikos</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e194a1310">
            <mixed-citation id="d1716e198" publication-type="journal">
Atkinson, W. D.1983. Gregarious oviposition in Drosophila
melanogaster is explained by surface texture. - Aust. J.
Zool.31: 925-929.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Atkinson</surname>
                  </string-name>
               </person-group>
               <fpage>925</fpage>
               <volume>31</volume>
               <source>Aust. J. Zool.</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e233a1310">
            <mixed-citation id="d1716e237" publication-type="journal">
- and Shorrocks, B.1981. Competition on a divided and
ephemeral resource: a simulation study. - J. Anim. Ecol.
50: 461-471.<object-id pub-id-type="doi">10.2307/4067</object-id>
               <fpage>461</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e257a1310">
            <mixed-citation id="d1716e261" publication-type="journal">
- and Shorrocks, B.1984. Aggregation of larval diptera over
discrete and ephemeral breeding sites: the implications for
coexistence. - Am. Nat.124: 336-351.<object-id pub-id-type="jstor">10.2307/2461462</object-id>
               <fpage>336</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e280a1310">
            <mixed-citation id="d1716e284" publication-type="journal">
Barclay, H. J.1992. Modelling the effects of population aggre-
gation on the efficiency of insect pest control. - Res.
Popul. Ecol.34: 131-141.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Barclay</surname>
                  </string-name>
               </person-group>
               <fpage>131</fpage>
               <volume>34</volume>
               <source>Res. Popul. Ecol.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e319a1310">
            <mixed-citation id="d1716e323" publication-type="journal">
Benson, W. W., Brown, K. S., Jr. and Gilbert, L. E.1975.
Coevolution of plants and herbivores: passion flower but-
terflies. - Evolution29: 659-680.<object-id pub-id-type="doi">10.2307/2407076</object-id>
               <fpage>659</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e342a1310">
            <mixed-citation id="d1716e346" publication-type="journal">
Charnov, E. L. and Skinner, S. W.1985. Complementary
approaches to the understanding of parasitoid oviposition
decisions. - Environ. Entomol.14: 383-391.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Charnov</surname>
                  </string-name>
               </person-group>
               <fpage>383</fpage>
               <volume>14</volume>
               <source>Environ. Entomol.</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e381a1310">
            <mixed-citation id="d1716e385" publication-type="journal">
Courtney, S. P.1986. The ecology of pierid butterflies: dynam-
ics and interactions. - Adv. Ecol. Res.15: 51-131.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Courtney</surname>
                  </string-name>
               </person-group>
               <fpage>51</fpage>
               <volume>15</volume>
               <source>Adv. Ecol. Res.</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e417a1310">
            <mixed-citation id="d1716e421" publication-type="journal">
— , Kibota, T. T. and Singleton, T. A. 1990. Ecology of
mushroom-feeding Drosophilidae. - Adv. Ecol. Res.20:
225-274.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Courtney</surname>
                  </string-name>
               </person-group>
               <fpage>225</fpage>
               <volume>20</volume>
               <source>Adv. Ecol. Res.</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e457a1310">
            <mixed-citation id="d1716e461" publication-type="journal">
Dunham, A. E.1980. An experimental study of interspecific
competition between the iguanid lizards Sceloporus merri-
ami and Urosaurus ornatus. - Ecol. Monogr.50: 309-330.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Dunham</surname>
                  </string-name>
               </person-group>
               <fpage>309</fpage>
               <volume>50</volume>
               <source>Ecol. Monogr.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e496a1310">
            <mixed-citation id="d1716e500" publication-type="journal">
Dytham, C. and Shorrocks, B.1995. Aggregation and the
maintenance of genetic diversity: an individual-based cellu-
lar model. - Evol. Ecol.9: 508-519.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Dytham</surname>
                  </string-name>
               </person-group>
               <fpage>508</fpage>
               <volume>9</volume>
               <source>Evol. Ecol.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e535a1310">
            <mixed-citation id="d1716e539" publication-type="journal">
Feijen, H. R. and Schulten, G. G. M.1981. Egg parasitoids
(Hymenoptera: Trichogrammatidae) of Diopsis macroph-
thalma (Diptera: Diopsidae) in Malawi. - Neth. J. Zool.
31: 381-417.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Feijen</surname>
                  </string-name>
               </person-group>
               <fpage>381</fpage>
               <volume>31</volume>
               <source>Neth. J. Zool.</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e577a1310">
            <mixed-citation id="d1716e581" publication-type="book">
Fox, J.1984. Linear statistical models and related methods
with applications to social research. - Wiley, New York.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Fox</surname>
                  </string-name>
               </person-group>
               <source>Linear statistical models and related methods with applications to social research</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e606a1310">
            <mixed-citation id="d1716e610" publication-type="journal">
Godfray, H. C. J. and Parker, G. A.1992. Sibling competi-
tion, parent-offspring conflict and clutch size. - Anim.
Behav.43: 473-490.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Godfray</surname>
                  </string-name>
               </person-group>
               <fpage>473</fpage>
               <volume>43</volume>
               <source>Anim. Behav.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e645a1310">
            <mixed-citation id="d1716e649" publication-type="book">
Grant, P. R.1986. Interspecific competition in fluctuating
environments. - In: Diamond, J. and Case, T. J. (eds),
Community ecology. Harper and Row, New York, pp.
173- 191.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Grant</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Interspecific competition in fluctuating environments</comment>
               <fpage>173</fpage>
               <source>Community ecology</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e688a1310">
            <mixed-citation id="d1716e692" publication-type="journal">
Green, R. F.1986. Does aggregation prevent competitive
exclusion? A response to Atkinson and Shorrocks. - Am.
Nat.128: 301-304.<object-id pub-id-type="jstor">10.2307/2461553</object-id>
               <fpage>301</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e711a1310">
            <mixed-citation id="d1716e715" publication-type="journal">
-1988. Reply to Shorrocks and Rosewell. - Am. Nat.131:
772-773.<object-id pub-id-type="jstor">10.2307/2461679</object-id>
               <fpage>772</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e731a1310">
            <mixed-citation id="d1716e735" publication-type="journal">
Grimaldi, D. and Jaenike, J.1984. Competition in natural
populations of mycophagous Drosophila. - Ecology65:
1113-1120.<object-id pub-id-type="doi">10.2307/1938319</object-id>
               <fpage>1113</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e754a1310">
            <mixed-citation id="d1716e758" publication-type="book">
Hanski, I.1987. Colonization of ephemeral habitats. - In:
Gray, A. J., Crawley, M. J. and Edwards, P. J. (eds),
Colonization, succession, and stability. Blackwell Scientific,
Oxford, pp. 155-185.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hanski</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Colonization of ephemeral habitats</comment>
               <fpage>155</fpage>
               <source>Colonization, succession, and stability</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e796a1310">
            <mixed-citation id="d1716e800" publication-type="journal">
Heard, S. B. and Remer, L. C.1997. Clutch size behavior and
coexistence in ephemeral-patch competition models. - Am.
Nat.150 (in press).<object-id pub-id-type="jstor">10.2307/2463512</object-id>
            </mixed-citation>
         </ref>
         <ref id="d1716e816a1310">
            <mixed-citation id="d1716e820" publication-type="journal">
Ives, A. R.1988a. Aggregation and the coexistence of com-
petitors. - Ann. Zool. Fenn.25: 75-88.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ives</surname>
                  </string-name>
               </person-group>
               <fpage>75</fpage>
               <volume>25</volume>
               <source>Ann. Zool. Fenn.</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e853a1310">
            <mixed-citation id="d1716e857" publication-type="journal">
—1988b. Covariance, coexistence, and the population dy-
namics of two competitors using a patchy resource. - J.
Theor. Biol.133: 345-361.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ives</surname>
                  </string-name>
               </person-group>
               <fpage>345</fpage>
               <volume>133</volume>
               <source>J. Theor. Biol.</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e892a1310">
            <mixed-citation id="d1716e896" publication-type="journal">
-1989. The optimal clutch size of insects when many females
oviposit per patch. - Am. Nat.133: 671-687.<object-id pub-id-type="jstor">10.2307/2462074</object-id>
               <fpage>671</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e912a1310">
            <mixed-citation id="d1716e916" publication-type="journal">
—1991. Aggregation and coexistence in a carrion fly commu-
nity. - Ecol. Monogr.61: 75-94.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ives</surname>
                  </string-name>
               </person-group>
               <fpage>75</fpage>
               <volume>61</volume>
               <source>Ecol. Monogr.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e948a1310">
            <mixed-citation id="d1716e952" publication-type="journal">
-1992. Density-dependent and density-independent para-
sitoid aggregation in model host-parasitoid systems. - Am.
Nat.140: 912-937.<object-id pub-id-type="jstor">10.2307/2462926</object-id>
               <fpage>912</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e971a1310">
            <mixed-citation id="d1716e975" publication-type="journal">
Iwasa, Y., Suzuki, Y. and Matsuda, H.1984. Theory of
oviposition strategy of parasitoids. I. Effect of mortality
and limited egg number. - Theor. Popul. Biol.26: 205-
227.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Iwasa</surname>
                  </string-name>
               </person-group>
               <fpage>205</fpage>
               <volume>26</volume>
               <source>Theor. Popul. Biol.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1013a1310">
            <mixed-citation id="d1716e1017" publication-type="journal">
Jackson, D. J.1966. Observations on the biology of Cara-
phractus cinctus Walker (Hymenoptera: Mymaridae), a
parasitoid of the eggs of Dytiscidae (Coleoptera). - Trans.
R. Entomol. Soc. Lond.118: 23-49.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Jackson</surname>
                  </string-name>
               </person-group>
               <fpage>23</fpage>
               <volume>118</volume>
               <source>Trans. R. Entomol. Soc. Lond.</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1056a1310">
            <mixed-citation id="d1716e1060" publication-type="journal">
Jaenike, J.1996. Population-level consequences of parasite
aggregation. - Oikos76: 155-160.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Jaenike</surname>
                  </string-name>
               </person-group>
               <fpage>155</fpage>
               <volume>76</volume>
               <source>Oikos</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1092a1310">
            <mixed-citation id="d1716e1096" publication-type="journal">
- and James, A. C.1991. Aggregation and the coexistence of
mycophagous Drosophila. - J. Anim. Ecol.60: 913-928.<object-id pub-id-type="doi">10.2307/5421</object-id>
               <fpage>913</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1112a1310">
            <mixed-citation id="d1716e1116" publication-type="journal">
Kato, M.1994. Structure, organization, and response of a
species-rich parasitoid community to host leafminer popu-
lation dynamics. - Oecologia97: 17-25.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kato</surname>
                  </string-name>
               </person-group>
               <fpage>17</fpage>
               <volume>97</volume>
               <source>Oecologia</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1151a1310">
            <mixed-citation id="d1716e1155" publication-type="journal">
Kouki, J. and Hanski, I.1995. Population aggregation facili-
tates coexistence of many competing carrion fly species. -
Oikos72: 223-227.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kouki</surname>
                  </string-name>
               </person-group>
               <fpage>223</fpage>
               <volume>72</volume>
               <source>Oikos</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1190a1310">
            <mixed-citation id="d1716e1194" publication-type="journal">
Mangel, M.1987. Oviposition site selection and clutch size in
insects. - J. Math. Biol.25: 1-22.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mangel</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>25</volume>
               <source>J. Math. Biol.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1226a1310">
            <mixed-citation id="d1716e1230" publication-type="journal">
McCauley, D. E.1994. Intrademic group selection imposed by
a parasitoid-host interaction. - Am. Nat.144: 1-13.<object-id pub-id-type="jstor">10.2307/2462797</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1247a1310">
            <mixed-citation id="d1716e1251" publication-type="journal">
Messina, F. J.1991. Life-history variation in a seed beetle:
adult egg-laying vs. larval competitive ability. - Oecologia
85: 447-455.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Messina</surname>
                  </string-name>
               </person-group>
               <fpage>447</fpage>
               <volume>85</volume>
               <source>Oecologia</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1286a1310">
            <mixed-citation id="d1716e1290" publication-type="journal">
—, Kemp, J. L. and Dickinson, J. A.1992. Plasticity in the
egg-spacing behavior of a seed beetle: effects of host depri-
vation and seed patchiness (Coleoptera: Bruchidae). - J.
Insect Behav.5: 609-621.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Messina</surname>
                  </string-name>
               </person-group>
               <fpage>609</fpage>
               <volume>5</volume>
               <source>J. Insect Behav.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1328a1310">
            <mixed-citation id="d1716e1332" publication-type="journal">
Montague, J. R.1985. Body size, reproductive biology, and
dispersal behavior among artificial baits in D. falleni. -
Drosophila Information Service61: 123-126.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Montague</surname>
                  </string-name>
               </person-group>
               <fpage>123</fpage>
               <volume>61</volume>
               <source>Drosophila Information Service</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1367a1310">
            <mixed-citation id="d1716e1371" publication-type="journal">
Morris, W. F., Wiser, S. D. and Klepetka, B.1992. Causes
and consequences of spatial aggregation in the phy-
tophagous beetle Altrica tombacina. - J. Anim. Ecol.61:
49-58.<object-id pub-id-type="doi">10.2307/5508</object-id>
               <fpage>49</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1394a1310">
            <mixed-citation id="d1716e1398" publication-type="journal">
Nagelkerke, C. J.1994. Simultaneous optimization of egg
distribution and sex allocation in a patch-structured popu-
lation. - Am. Nat.144: 262-284.<object-id pub-id-type="jstor">10.2307/2463160</object-id>
               <fpage>262</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1417a1310">
            <mixed-citation id="d1716e1421" publication-type="journal">
Pak, G. A. and Oatman, E. R.1982. Biology of Trichogramma
brevicapillum. - Entomol. Exp. Appl.32: 61-67.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pak</surname>
                  </string-name>
               </person-group>
               <fpage>61</fpage>
               <volume>32</volume>
               <source>Entomol. Exp. Appl.</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1454a1310">
            <mixed-citation id="d1716e1458" publication-type="journal">
Parker, G. A. and Courtney, S. P.1984. Models of clutch size
in insect oviposition. - Theor. Popul. Biol.26: 27-48.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Parker</surname>
                  </string-name>
               </person-group>
               <fpage>27</fpage>
               <volume>26</volume>
               <source>Theor. Popul. Biol.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1490a1310">
            <mixed-citation id="d1716e1494" publication-type="journal">
Podoler, H., Rosen, D. and Sharoni, M.1978. Ovipositional
responses to host density in Aphytis holoxanthus (Hy-
menoptera: Aphelinidae), an efficient gregarious parasite.
- Ecol. Entomol.3: 305-311.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Podoler</surname>
                  </string-name>
               </person-group>
               <fpage>305</fpage>
               <volume>3</volume>
               <source>Ecol. Entomol.</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1532a1310">
            <mixed-citation id="d1716e1536" publication-type="journal">
Rosewell, J., Shorrocks, B. and Edwards, K.1990. Competi-
tion on a divided and ephemeral resource: testing the
assumptions. I. Aggregation. - J. Anim. Ecol.59: 977-
1001.<object-id pub-id-type="doi">10.2307/5026</object-id>
               <fpage>977</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1559a1310">
            <mixed-citation id="d1716e1563" publication-type="journal">
Ross, D. W. and Daterman, G. E.1994. Reduction of Dou-
glas-fir beetle infestation of high-risk stands by antiaggre-
gation and aggregation pheromones. - Can. J. For. Res.
24: 2184-2190.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ross</surname>
                  </string-name>
               </person-group>
               <fpage>2184</fpage>
               <volume>24</volume>
               <source>Can. J. For. Res.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1601a1310">
            <mixed-citation id="d1716e1605" publication-type="book">
SAS Institute Inc.1988. SAS/STAT User's Guide, Release
6.03 Ed. - SAS Institute Inc., Cary, NC.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Sas Institute Inc.</surname>
                  </string-name>
               </person-group>
               <source>SAS/STAT User's Guide</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1630a1310">
            <mixed-citation id="d1716e1634" publication-type="journal">
Sevenster, J. G. and van Alphen, J. J. M.1996. Aggregation
and coexistence. 2. A Neotropical Drosophila community.
- J. Anim. Ecol.65: 308-324.<object-id pub-id-type="doi">10.2307/5877</object-id>
               <fpage>308</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1654a1310">
            <mixed-citation id="d1716e1658" publication-type="journal">
Shorrocks, B. and Rosewell, J.1988. Aggregation does prevent
competitive exclusion: a response to Green. - Am. Nat.
131: 765-771.<object-id pub-id-type="jstor">10.2307/2461678</object-id>
               <fpage>765</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1677a1310">
            <mixed-citation id="d1716e1681" publication-type="journal">
Skinner, S. W.1985. Clutch size as an optimal foraging
problem for insects. - Behav. Ecol. Sociobiol.17: 231-
238.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Skinner</surname>
                  </string-name>
               </person-group>
               <fpage>231</fpage>
               <volume>17</volume>
               <source>Behav. Ecol. Sociobiol.</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1716a1310">
            <mixed-citation id="d1716e1720" publication-type="book">
Smith, R. H. and Lessells, C. M.1985. Oviposition, ovicide,
and larval competition in granivorous insects. - In: Sibly,
R. M. and Smith, R. H. (eds), Behavioural ecology: ecolog-
ical consequences of adaptive behaviour. Blackwell Scien-
tific, Oxford, pp. 423-448.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Oviposition, ovicide, and larval competition in granivorous insects</comment>
               <fpage>423</fpage>
               <source>Behavioural ecology: ecological consequences of adaptive behaviour</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1761a1310">
            <mixed-citation id="d1716e1765" publication-type="journal">
Stähls, G., Ribeiro, E. and Hanski, I.1989. Fungivorous
Pegomya flies: spatial and temporal variation in a guild of
competitors. - Ann. Zool. Fenn.26: 103-112.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Stähls</surname>
                  </string-name>
               </person-group>
               <fpage>103</fpage>
               <volume>26</volume>
               <source>Ann. Zool. Fenn.</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1800a1310">
            <mixed-citation id="d1716e1804" publication-type="journal">
Taylor, L. R., Woiwod, I. P. and Perry, J. N.1978. The
density-dependence of spatial behaviour and the rarity of
randomness. - J. Anim. Ecol.47: 383-406.<object-id pub-id-type="doi">10.2307/3790</object-id>
               <fpage>383</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1823a1310">
            <mixed-citation id="d1716e1827" publication-type="journal">
- , Woiwod, I. P. and Perry, J. N. 1979. The negative
binomial as a dynamic ecological model for aggregation,
and the density dependence of k. - J. Anim. Ecol.48:
289-304.<object-id pub-id-type="doi">10.2307/4114</object-id>
               <fpage>289</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1851a1310">
            <mixed-citation id="d1716e1855" publication-type="journal">
Waage, J. K. and Ng, S. M.1984. The reproductive strategy of
a parasitic wasp. I. Optimal progeny and sex allocation in
Trichogramma evanescens. - J. Anim. Ecol.53: 401-415.<object-id pub-id-type="doi">10.2307/4524</object-id>
               <fpage>401</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1874a1310">
            <mixed-citation id="d1716e1878" publication-type="journal">
Weis, A. E., Price, P. W. and Lynch, M.1983. Selective
pressures on clutch size in the gall maker Asteromyia
carbonifera. - Ecology64: 688-695.<object-id pub-id-type="doi">10.2307/1937190</object-id>
               <fpage>688</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1716e1897a1310">
            <mixed-citation id="d1716e1901" publication-type="journal">
Wiens, J. A.1977. On competition and variable environments.
- Am. Sci.65: 590-597.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Wiens</surname>
                  </string-name>
               </person-group>
               <fpage>590</fpage>
               <volume>65</volume>
               <source>Am. Sci.</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1933a1310">
            <mixed-citation id="d1716e1937" publication-type="journal">
Worthen, W. B.1988. Slugs (Arion spp.) facilitate my-
cophagous drosophilids in laboratory and field experi-
ments. - Oikos53: 161-166.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Worthen</surname>
                  </string-name>
               </person-group>
               <fpage>161</fpage>
               <volume>53</volume>
               <source>Oikos</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e1972a1310">
            <mixed-citation id="d1716e1976" publication-type="journal">
—1989. Effects of resource density on mycophagous fly dis-
persal and community structure. - Oikos54: 145-153.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Worthen</surname>
                  </string-name>
               </person-group>
               <fpage>145</fpage>
               <volume>54</volume>
               <source>Oikos</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1716e2008a1310">
            <mixed-citation id="d1716e2012" publication-type="journal">
—1993. Effects of ant predation and larval density on my-
cophagous fly communities. - Oikos66: 526-532.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Worthen</surname>
                  </string-name>
               </person-group>
               <fpage>526</fpage>
               <volume>66</volume>
               <source>Oikos</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
