<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7nwsm</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">9781469632728_dworkin</book-id>
      <subj-group>
         <subject content-type="call-number">E185.625 .D95 2017</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African Americans</subject>
         <subj-group>
            <subject content-type="lcsh">Relations with Africans</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African Americans</subject>
         <subj-group>
            <subject content-type="lcsh">Intellectual life</subject>
            <subj-group>
               <subject content-type="lcsh">19th century</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African Americans</subject>
         <subj-group>
            <subject content-type="lcsh">Intellectual life</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Anti-imperialist movements</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Black nationalism</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Congo Love Song</book-title>
         <subtitle>African American Culture and the Crisis of the Colonial State</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>DWORKIN</surname>
               <given-names>IRA</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>12</day>
         <month>06</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="epub">9781469632735</isbn>
      <isbn content-type="epub">146963273X</isbn>
      <publisher>
         <publisher-name>The University of North Carolina Press</publisher-name>
         <publisher-loc>Chapel Hill</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>The University of North Carolina Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.5149/9781469632728_dworkin"/>
      <abstract abstract-type="short">
         <p>In his 1903 hit "Congo Love Song," James Weldon Johnson recounts a sweet if seemingly generic romance between two young Africans. While the song's title may appear consistent with that narrative, it also invokes the site of King Leopold II of Belgium's brutal colonial regime at a time when African Americans were playing a central role in a growing Congo reform movement. In an era when popular vaudeville music frequently trafficked in racist language and imagery, "Congo Love Song" emerges as one example of the many ways that African American activists, intellectuals, and artists called attention to colonialism in Africa.In this book, Ira Dworkin examines black Americans' long cultural and political engagement with the Congo and its people. Through studies of George Washington Williams, Booker T. Washington, Pauline Hopkins, Langston Hughes, Malcolm X, and other figures, he brings to light a long-standing relationship that challenges familiar presumptions about African American commitments to Africa. Dworkin offers compelling new ways to understand how African American involvement in the Congo has helped shape anticolonialism, black aesthetics, and modern black nationalism.</p>
      </abstract>
      <counts>
         <page-count count="472"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.4</book-part-id>
                  <title-group>
                     <title>Abbreviations Used in the Text</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.5</book-part-id>
                  <title-group>
                     <title>[Map]</title>
                  </title-group>
                  <fpage>xx</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                     <subtitle>James Weldon Johnson’s Transnational Vaudeville</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Expressions of love for the Congo take a range of forms within African American culture. In the first years of the twentieth century, James Weldon Johnson wrote the lyrics to the hugely popular “Congo Love Song,” which invokes the fictitious “wilds of Umbagooda” in a manner that may seem commonplace among the era’s representations of Africa. On first glance, Johnson’s song might appear to reproduce the kinds of stock representations that are the subject of Malcolm X’s brilliant commentary on the role played by primitivist images of the Congo not only in the debasement of African Americans, but also as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.7</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>George Washington Williams’s Stern Duty of History</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>On December 12, 1889, the eminent African American historian George Washington Williams eagerly visited the campus of Hampton Normal and Agricultural Institute.¹ He had returned less than two weeks earlier from an antislavery conference in Brussels, where he also had a personal meeting with King Leopold II of Belgium, the sovereign ruler of l’État Indépendant du Congo. Williams parlayed his royal audience, which he had actively sought since 1884, into a commission from the Belgian Commercial Companies to recruit educated black Americans to work in the Congo State. Soon thereafter, he traveled to Hampton with a letter of introduction to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.8</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>William Henry Sheppard’s Country of My Forefathers</title>
                  </title-group>
                  <fpage>49</fpage>
                  <abstract>
                     <p>On December 9, 1889, three days before George Washington Williams traveled to Hampton Institute to recruit students for his Congo expedition, William Henry Sheppard, a twenty-four-year-old Virginian who attended Hampton, was appointed to serve as a missionary. At a time when many African Americans had a regnant desire to go to Africa, Sheppard, a graduate of Stillman Institute in Alabama (then known as Tuscaloosa Theological Institute), distinctively pursued his dream. Trailing Williams by a few months, Sheppard left New York for Liverpool on February 26, 1890, arriving in Africa on May 9.¹ Soon after landing in the port of Boma,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.9</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Booker T. Washington’s African at Home</title>
                  </title-group>
                  <fpage>78</fpage>
                  <abstract>
                     <p>On January 17, 1890, Booker T. Washington addressed “a large audience of colored students and citizens” at Alabama State Normal School one month after George Washington Williams’s address at his alma mater and his former student William Henry Sheppard’s appointment to the American Presbyterian Congo Mission (APCM). Migration to the Congo was quite real and Washington took the opportunity to dismiss the entire prospect as misguided:</p>
                     <p>You may talk of our disadvantages because of our color, because of race prejudice, because of friction in enjoying civil and political rights, may advise emigration to the West or the Congo, but I</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.10</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Missionary Cultures</title>
                     <subtitle>The American Presbyterian Congo Mission, Althea Brown Edmiston, and the Languages of the Congo</subtitle>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract>
                     <p>The American Presbyterian Congo Mission (APCM) was extraordinarily successful in recruiting African Americans to its ranks. For every William Sheppard who served in Africa, there were many more Booker T. Washingtons who dreamed of doing so but found themselves without the opportunity. Among the latter was a young Mary McLeod (Bethune) who, in 1894, submitted her application to the Northern Presbyterian Church (PCUSA), which, on the advice of white Lincoln University theology professor Robert Laird Stewart, decided against racially integrating its mission.¹ Her application is indicative of the popular appeal of Africa to accomplished young African Americans, and the unrealized</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>Literary Cultures</title>
                     <subtitle>The Black Press, Pauline E. Hopkins, and the Rewriting of Africa</subtitle>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract>
                     <p>In 1902–3, the<italic>Colored American Magazine</italic>published<italic>Of One Blood; Or, the Hidden Self</italic>, Pauline E. Hopkins’s novel about Reuel Briggs, an African American doctor, who, like the African American missionaries of his day, travels to Africa, making it, according to John Cullen Gruesser, “one of the earliest known fictional accounts of the continent by a black American writer.”¹ The novel, which was being serialized at the precise time when Althea Brown Edmiston was traveling to the Congo, features as its heroine Dianthe Lusk, a former Fisk University student and Jubilee Singer who uses African American spirituals as a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.12</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>Visual Cultures</title>
                     <subtitle>Hampton Institute, William Sheppard’s Kuba Collection, and African American Art</subtitle>
                  </title-group>
                  <fpage>163</fpage>
                  <abstract>
                     <p>In 1916, more than a decade removed from her tenure at the<italic>Colored American Magazine</italic>and<italic>Voice of the Negro</italic>, Pauline Hopkins made one final foray into publishing with her<italic>New Era Magazine</italic>, whose two issues featured decidedly international coverage of Ethiopia, Liberia, Haiti, and Puerto Rico. One feature that appeared in both issues was Meta Vaux Warrick Fuller’s column, “Helpful Suggestions for Young Artists,” as part of its art department.¹ As early as 1915, Fuller, who married Liberian doctor Solomon Fuller in 1909, had begun corresponding with the pioneering art historian Freeman Murray about “her interest in the theme</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.13</book-part-id>
                  <title-group>
                     <title>A section of color plates</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.14</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Near the Congo</title>
                     <subtitle>Langston Hughes and the Geopolitics of Internationalist Poetry</subtitle>
                  </title-group>
                  <fpage>203</fpage>
                  <abstract>
                     <p>Langston Hughes’s “The Negro Speaks of Rivers,” which has become a signature meditation on African American heritage and ancestry, includes one of the most famous invocations of the Congo in American letters: “I built my hut near the Congo and it lulled me to sleep.”¹ The poem, which was originally published in the June 1921 issue of the NAACP’s<italic>Crisis</italic>, has become an iconic representative of Hughes and the Harlem Renaissance. With an opening couplet whose long, reflective second line begins by repeating the first line—“I’ve known rivers: / I’ve known rivers ancient as the world and older than</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.15</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Another Black Magazine with a Lumumba Poem</title>
                     <subtitle>Patrice Lumumba and African American Poetry</subtitle>
                  </title-group>
                  <fpage>224</fpage>
                  <abstract>
                     <p>In John Oliver Killens’s novel,<italic>The Cotillion, or, One Good Bull Is Half the Herd</italic>(1971), a young woman named Yoruba is forced to negotiate her mother’s bourgeois aspirations with her own rising nationalist sensibilities. Yoruba’s burgeoning racial pride is entangled with her romantic relationship with a young poet, Ben Ali Lumumba: “It seemed like every day Yoruba came into the house with another Black magazine with a Lumumba poem and face somewhere inside.”¹ In this scene, Yoruba is carrying a magazine with a “Lumumba poem,” a verse written by her prolific and increasingly successful boyfriend, who represents the new</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.16</book-part-id>
                  <title-group>
                     <label>Chapter 9</label>
                     <title>The Chickens Coming Home to Roost</title>
                     <subtitle>Malcolm X, the Congo, and Modern Black Nationalism</subtitle>
                  </title-group>
                  <fpage>257</fpage>
                  <abstract>
                     <p>While the Black Arts movement was engaging the postcolonial politics of the Congo, Malcolm X took Patrice Lumumba as the patron saint for the Organization of Afro-American Unity (OAAU). Overall, Malcolm X took a longer historical view on the Congo. On November 28, 1964, a few days after returning to the United States from several months abroad, he appeared as a guest on the Barry Gray Show on WMCA radio in New York to discuss the Congo, a timely subject following the disastrous attempt by U.S. and Belgian forces four days earlier to rescue white hostages from Stanleyville (Kisangani), a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.17</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>288</fpage>
                  <abstract>
                     <p>Malcolm X was deeply disturbed by media coverage of the Congo. In an address at the London School of Economics less than two weeks before his assassination, he explained the utter dehumanization he experienced: “If you recall reading in the paper, they never talked about the Congolese who were being slaughtered. But as soon as a few whites, the lives of a few whites were at stake, they began to speak of ‘white hostages,’ ‘white missionaries,’ ‘white priests,’ ‘white nuns’—as if a white life, one white life, was of such greater value than a Black life, than a thousand</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.18</book-part-id>
                  <title-group>
                     <title>Appendix</title>
                     <subtitle>Malcolm X on the Congo, February 14, 1965, Detroit</subtitle>
                  </title-group>
                  <fpage>295</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.19</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>299</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.20</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>363</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.21</book-part-id>
                  <title-group>
                     <title>Credits</title>
                  </title-group>
                  <fpage>417</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781469632728_dworkin.22</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>419</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
