<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="doi">10.5612</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">slavicreview</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Slavic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Association for Slavic East European and Eurasian Studies</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00376779</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">slavicreview.72.3.0505</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.5612/slavicreview.72.3.0505</article-id>
         <article-categories>
            <subj-group>
               <subject>MOSCOW: A GLOBAL CITY?</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>In Search of the Fourth Rome: Visions of a New Russian Capital City</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Vadim Rossman</string-name>
               <bio>
                  <p>V<sc>adim</sc>R<sc>ossman</sc>is lecturer at the International College for Sustainability Studies, Srinakharinwirot University, Bangkok, Thailand. Rossman received a PhD in philosophy and government from the University of Texas, Austin, in 1998. He is the author of<italic>Russian Intellectual Antisemitism in the Post-Communist Era</italic>(Nebraska University Press, 2002),<italic>Stolitsy: Ikh mnogoobraznie i zakonomernosti razvitiia i peremeshcheniia</italic>(Moscow, 2013), and<italic>V poiskakh Chetvertogo Rima: Rossiiskie debaty o perenose stolitsy</italic>(Moscow, 2013). His recent research focuses on capital cities, their iconography, and their relocations in a global perspective.</p>
               </bio>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>10</month>
            <year>2013</year>
            <string-date>FALL 2013</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">72</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">slavicreview.72.issue-3</issue-id>
         <fpage>505</fpage>
         <lpage>527</lpage>
         <permissions>
            <copyright-statement>Copyright 2013 Association for Slavic East European and Eurasian Studies</copyright-statement>
            <copyright-year>2013</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/10.5612/slavicreview.72.3.0505"/>
         <abstract>
            <p>Over the last twenty years Russia has experienced significant fluctuations in sentiments regarding the prospects and urgency of relocating the Russian capital city. In this article, Vadim Rossman examines the public debates on this topic, which have involved important Russian politicians, intellectuals, and members of various expert communities. In these debates, one can recognize several distinct new visions of society that emerged in the post-Soviet period. This article provides an overview and a critique of these debates and suggests that they should be viewed in the context of nation building, the slow emergence of the nation that was historically suppressed under the weight of the imperial ambitions of Russian statehood. In the background of these debates, the concept of self-identity looms large. National capitals can serve as catalysts for nation building and an instrument of the nation as it constitutes and constructs itself.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>Footnotes</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="other">More detailed analysis can be found in my book<source>
                  <italic>V poiskakh Chetvertogo Rima: Rossiiskie debaty o perenose stolitsy</italic>
               </source>(Moscow,<year>2013</year>).</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="journal">
               <string-name>Nikolaus von Twickel</string-name>,<article-title>“Miserable Moscow Ranks Low,”</article-title>
               <source>
                  <italic>St. Petersburg Times</italic>
               </source>
               <volume>1381</volume>(<issue>45</issue>), 12 June<year>2008</year>.</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="other">“IBM Global Commuter Pain Study Reveals Traffic Crisis in Key International Cities,” Press Release, 30 June 2010, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www-03.ibm.com/press/us/en/pressrelease/32017.wss">www-03.ibm.com/press/us/en/pressrelease/32017.wss</ext-link>(last accessed 31 May<year>2013</year>).</mixed-citation>
         </ref>
         <ref id="ref4">
            <label>4.</label>
            <mixed-citation publication-type="journal">
               <article-title>“Bad Weather, Bad-Tempered People Scare Foreigners away from Moscow,”</article-title>
               <source>
                  <italic>Kommersant</italic>
               </source>, 30 January<year>2007</year>; “What the Russian Papers Say,”<source>
                  <italic>RIA Novosti</italic>
               </source>, 14 June 2007, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://n.rian.ru/analysis/20070614/67235589.html">n.rian.ru/analysis/20070614/67235589.html</ext-link>(last accessed 31 May<year>2013</year>); WCIOM Survey, “Gorodupuschennykhvozmozhnostei,” 21 April 2005,<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://atwciom.ru/index.php?id=269&amp;amp;uid=1203">atwciom.ru/index.php?id=269&amp;uid=1203</ext-link>(last accessed 31 May 2013).</mixed-citation>
         </ref>
         <ref id="ref5">
            <label>5.</label>
            <mixed-citation publication-type="other">
               <string-name>B. Nemtsov</string-name>,<source>
                  <italic>Nezavisimyi ekspertnyi doklad “Luzhkov. Itogi”</italic>
               </source>(Moscow,<year>2009</year>);<string-name>Bill Bowring</string-name>,<source>“Moscow: Third Rome, Model Communist City, Eurasian Antagonist—and Power as No-Power?”</source>
               <publisher-name>Metropolitan University Human Rights and Social Justice Research Institute</publisher-name>,<source>
                  <italic>Polis</italic>
               </source>,<year>2005</year>/<year>2006</year>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <label>6.</label>
            <mixed-citation publication-type="journal">
               <string-name>M. Shvydkoi</string-name>,<article-title>“Rukopisi v virtual'nom mire,”</article-title>
               <source>
                  <italic>Rossiiskaia gazeta</italic>
               </source>, 13 October<year>2010</year>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <label>7.</label>
            <mixed-citation publication-type="journal">
               <string-name>V. Rossman</string-name>,<article-title>“Perenos stolitsy: Skhema analiza,”</article-title>
               <source>
                  <italic>Predstavitel'naia vlast': Zhurnal gosudarstvennoi Dumi</italic>
               </source>(March<year>2010</year>).</mixed-citation>
         </ref>
         <ref id="ref8">
            <label>8.</label>
            <mixed-citation publication-type="journal">Many people living in Siberia have never been to their capital city and cannot afford to go there; during the recent population census many of them referred to this fact to explain their choice of identity as Siberians rather than Russians.<string-name>V. Antipin</string-name>,<article-title>“Grazhdanin Sibiri,”</article-title>
               <source>
                  <italic>Russkii reporter</italic>
               </source>, 22 February<year>2011</year>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <label>9.</label>
            <mixed-citation publication-type="journal">
               <article-title>“Moskvichei v Rossii nenavidiat uzhe dve treti sograzhdan,”</article-title>
               <source>
                  <italic>Newsland</italic>
               </source>, 18 February<year>2009</year>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <label>10.</label>
            <mixed-citation publication-type="other">“Kuda by stolitsu perenesti?”<source>
                  <italic>Kommersant vlast'</italic>
               </source>, no. 17 (4 May<year>1999</year>); “Chetvertii Rim,”<source>
                  <italic>Itogi</italic>
               </source>, no. 1 (10 January<year>2011</year>).</mixed-citation>
         </ref>
         <ref id="ref11">
            <label>11.</label>
            <mixed-citation publication-type="other">
               <string-name>A. Dugin</string-name>,<article-title>“Tret'ia stolitsa Evrazii,”</article-title>in<string-name>N. Kirabaev, A. Semushkin, and S. Nizhnikov</string-name>, eds.,<source>
                  <italic>Evraziiskaia ideia i sovremennost'</italic>
               </source>(Moscow,<year>2002</year>),<fpage>228</fpage>–<lpage>44</lpage>.</mixed-citation>
         </ref>
         <ref id="ref12">
            <label>12.</label>
            <mixed-citation publication-type="other">Since its discovery, Arkaim has attracted significant public and media attention. It is said to be the most enigmatic archaeological site within the territory of Russia, and, as with many archaeological discoveries, many conflicting interpretations have emerged. In order to gain publicity, the early investigators described Arkaim as “Swastika City,” “Mandala City,” and the ancient capital of early Aryan civilization, as described in the<source>
                  <italic>Avesta</italic>
               </source>and<source>
                  <italic>Vedas</italic>
               </source>. For more details on the myth of Arkaim in Russian nationalist ideology, see<string-name>V. Shnirelman</string-name>,<article-title>“Strasti po Arkaimu: Ariiskaia ideia i natsionalizm,”</article-title>in<string-name>M. B. Olkott [Olcott] and Il'ia Semenov</string-name>, eds.,<source>
                  <italic>Iazyk i etnicheskii konflikt</italic>
               </source>(Moscow,<year>2001</year>), 58–85.</mixed-citation>
         </ref>
         <ref id="ref13">
            <label>13.</label>
            <mixed-citation publication-type="other">Both cited in “Ekspert: V sluchae perenosa stolitsy Rossii Kazakhstan pridetsia prisoedinit',”<source>
                  <italic>Rosbalt</italic>
               </source>, 26 February<year>2009</year>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <label>14.</label>
            <mixed-citation publication-type="journal">
               <string-name>V. Tsymburskii</string-name>,<article-title>“Zaural'skii Peterburg: Al'ternativa dlia rossiiskoi tsivilizatsii,”</article-title>
               <source>
                  <italic>Biznes i politika</italic>
               </source>,<year>1995</year>, no. 1:<fpage>57</fpage>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <label>15.</label>
            <mixed-citation publication-type="journal">
               <string-name>V. Tret'iakov</string-name>,<article-title>“Nuzhno li, a esli nuzhno, to kuda perenosit' stolitsu Rossii?”</article-title>
               <source>
                  <italic>Chto delat'?</italic>
               </source>on the TV channel Kultura), 24 June<year>2007</year>. The guests on the program included Maksim Dianov, Dmitrii Andreev, Iurii Krupnov, Vladimir Iurovitskii, and Mikhail Deliagin.<string-name>V. Iurovitskii</string-name>,<source>
                  <italic>Rossii nuzhna novaia stolitsa</italic>
               </source>(Moscow,<year>2005</year>).</mixed-citation>
         </ref>
         <ref id="ref16">
            <label>16.</label>
            <mixed-citation publication-type="other">
               <string-name>Iurovitskii</string-name>,<source>
                  <italic>Rossii nuzhna novaia stolitsa</italic>
               </source>,<fpage>67</fpage>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <label>17.</label>
            <mixed-citation publication-type="journal">
               <string-name>S. Korableva</string-name>,<article-title>“Eduard Limonov: ‘Rossii nuzhen novyi natsproekt—perenos stolitsy strany v Omsk,’”</article-title>
               <source>
                  <italic>Novyi region</italic>
               </source>, 21 January<year>2008</year>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <label>18.</label>
            <mixed-citation publication-type="other">“Zhirinovskii: Sdelaem iz Orenburga Washington,”<source>
                  <italic>Novyi region</italic>
               </source>, 28 September 2010, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www.nr2.ru/moskow/302349.html">www.nr2.ru/moskow/302349.html</ext-link>(last accessed 31 May<year>2013</year>).</mixed-citation>
         </ref>
         <ref id="ref19">
            <label>19.</label>
            <mixed-citation publication-type="other">
               <article-title>“V poiskakh imperskoi perspektivy: Suzhdeno li Kievu stat tsentrom post-vizantiiskoi tsivilizatsii,”</article-title>
               <source>
                  <italic>Nezavisimaia gazeta</italic>
               </source>, 25 October<year>2000</year>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <label>20.</label>
            <mixed-citation publication-type="journal">
               <string-name>V. Tretiakov</string-name>,<article-title>“Rossia: Poslednii pryzhok v budushchee,”</article-title>
               <source>
                  <italic>Nezavisimaia gazeta</italic>
               </source>, 24 February<year>2000</year>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <label>21.</label>
            <mixed-citation publication-type="other">
               <string-name>I. Krupnov</string-name>,<source>
                  <italic>Solntse v Rossii voskhodit s Vostoka</italic>
               </source>(Moscow,<year>2006</year>).</mixed-citation>
         </ref>
         <ref id="ref22">
            <label>22.</label>
            <mixed-citation publication-type="other">
               <source>“Chetvertii Rim.”</source>.</mixed-citation>
         </ref>
         <ref id="ref23">
            <label>23.</label>
            <mixed-citation publication-type="other">
               <string-name>V. Milov</string-name>, interview, Moscow, February<year>2011</year>.</mixed-citation>
         </ref>
         <ref id="ref24">
            <label>24.</label>
            <mixed-citation publication-type="other">
               <string-name>A. Shiropaev</string-name>,<source>
                  <italic>Ot Rossii k Rusi: Stat'i, ocherki, esse</italic>
               </source>(Moscow,<year>2003</year>);<string-name>Vladimir Mozhegov</string-name>,<article-title>“O novoi gardarike i moskovskikh pirogakh,”</article-title>
               <source>
                  <italic>APN</italic>
               </source>, 17 August<year>2006</year>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <label>25.</label>
            <mixed-citation publication-type="other">
               <string-name>Mozhegov</string-name>,<source>“O novoi gardarike i moskovskikh pirogakh.”</source>.</mixed-citation>
         </ref>
         <ref id="ref26">
            <label>26.</label>
            <mixed-citation publication-type="other">
               <string-name>A. Shiropaev</string-name>,<source>
                  <italic>Tiurma naroda: Russkii vzgliad na Rossiiu</italic>
               </source>(Moscow,<year>2001</year>).</mixed-citation>
         </ref>
         <ref id="ref27">
            <label>27.</label>
            <mixed-citation publication-type="journal">
               <string-name>S. Kornev and V. Shtepa</string-name>,<article-title>“Posle Tret'ego Rima—Kitezh (Otryvok iz futurologicheskogo proekta ‘Evropa ot Kitezha do Aliaski‘),”</article-title>
               <source>
                  <italic>Zavtra</italic>
               </source>, no. 1–2 (18 January<year>2000</year>).</mixed-citation>
         </ref>
         <ref id="ref28">
            <label>28.</label>
            <mixed-citation publication-type="other">
               <string-name>Mozhegov</string-name>,<source>“O novoi gardarike i moskovskikh pirogakh.”</source>.</mixed-citation>
         </ref>
         <ref id="ref29">
            <label>29.</label>
            <mixed-citation publication-type="journal">
               <string-name>E. Bychkova</string-name>,<article-title>“Gavriil Popov: Korruptsiiu v rossiisskoi stolitse mozhno pobedit',”</article-title>
               <source>
                  <italic>Argumenty i fakti</italic>
               </source>, no. 40 (6 October<year>2010</year>).</mixed-citation>
         </ref>
         <ref id="ref30">
            <label>30.</label>
            <mixed-citation publication-type="journal">
               <string-name>I. Lezhava and M. Shubenkov</string-name>,<article-title>“Kontseptsiia lineinoi sistemy rasseleniia Rossii v 21 veke,”</article-title>
               <source>
                  <italic>IV Rome</italic>
               </source>(online journal), 14 April<year>2010</year>, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www.ivrome.ru/2010/04/koncepciya-linejnoj-sistemy-rasseleniya-rossii-v-21-veke/">www.ivrome.ru/2010/04/koncepciya-linejnoj-sistemy-rasseleniya-rossii-v-21-veke/</ext-link>(last accessed 31 May<year>2013</year>).</mixed-citation>
         </ref>
         <ref id="ref31">
            <label>31.</label>
            <mixed-citation publication-type="other">Ibid.</mixed-citation>
         </ref>
         <ref id="ref32">
            <label>32.</label>
            <mixed-citation publication-type="journal">
               <string-name>A. Trifonov</string-name>,<article-title>“Perenos stolitsy neobkhodim, ia eshche v 1986-m predlagal stroit' gorod mezhdu Moskvoi i Piterom',”</article-title>
               <source>
                  <italic>Izvestiia</italic>
               </source>, 30 June<year>2011</year>.</mixed-citation>
         </ref>
         <ref id="ref33">
            <label>33.</label>
            <mixed-citation publication-type="journal">
               <string-name>S. Belkovskii</string-name>,<article-title>“Dorogaia moia stolitsa, zolotaia moia N'iu-Moskva …: Komu vygoden perenos federal'noi vlasti v gorod-sputnik na pustyre?”</article-title>
               <source>
                  <italic>Moskovskii komsomolets</italic>
               </source>, no. 25686 (7 July<year>2011</year>).</mixed-citation>
         </ref>
         <ref id="ref34">
            <label>34.</label>
            <mixed-citation publication-type="other">Ibid.</mixed-citation>
         </ref>
         <ref id="ref35">
            <label>35.</label>
            <mixed-citation publication-type="other">“Mikhail Deliagin: Stolitsu nuzhno perenosit' v zakholust'e,” 19 April 2012, on<source>
                  <italic>Priamaia rech</italic>
               </source>, Mir TV, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www.mirtv.ru/programms/4390776/episode/4886262">www.mirtv.ru/programms/4390776/episode/4886262</ext-link>(last accessed 31 May<year>2013</year>).</mixed-citation>
         </ref>
         <ref id="ref36">
            <label>36.</label>
            <mixed-citation publication-type="other">“Perenos stolytsy,” 22 January 2011, on<source>
                  <italic>Pravo golosa</italic>
               </source>, hosted by Roman Babaian, Channel 3. The participants included Iurii Krupnov, Sergei Stankevich, Aleksandr Tsypko, Marat Gel'man, Iosif Diskin, Igor' Volgin, Evgenii Minchenko, and Boris Mezhuev.</mixed-citation>
         </ref>
         <ref id="ref37">
            <label>37.</label>
            <mixed-citation publication-type="journal">
               <string-name>O. Wite</string-name>,<article-title>“Stolitsa versus okraina: Cherez 10 let Moskva perestanet byt' edinstvennym gorodom v strane,” Interview with Petr Miroshnik,”</article-title>
               <source>
                  <italic>IV Rome</italic>
               </source>(online journal), 24 August<year>2010</year>, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www.ivrome.ru/2010/08/stolica-vs-okraina/">www.ivrome.ru/2010/08/stolica-vs-okraina/</ext-link>(last accessed 31 May 2013).</mixed-citation>
         </ref>
         <ref id="ref38">
            <label>38.</label>
            <mixed-citation publication-type="journal">“Perenos stolytsy”;<string-name>V. Kurennoi</string-name>,<article-title>“Tiagotenie k tsentru: Razvitie makroregional'nykh stolits,”</article-title>
               <source>
                  <italic>Politicheskii zhurnal</italic>
               </source>, no. 43 (19 December<year>2005</year>).</mixed-citation>
         </ref>
         <ref id="ref39">
            <label>39.</label>
            <mixed-citation publication-type="other">
               <source>“Perenos stolytsy.”</source>.</mixed-citation>
         </ref>
         <ref id="ref40">
            <label>40.</label>
            <mixed-citation publication-type="journal">
               <string-name>P. Prianikov</string-name>,<article-title>“Poka rossiiane bedny, demokratii v strane ne budet,”</article-title>
               <source>
                  <italic>Tolkovatel'</italic>
               </source>, 3 March 2012, at<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://ttolk.ru/?p=9884">ttolk.ru/?p=9884</ext-link>(last accessed 31 May<year>2013</year>).</mixed-citation>
         </ref>
         <ref id="ref41">
            <label>41.</label>
            <mixed-citation publication-type="other">Irina Hakamada cited in “Kuda by stolitsu perenesti?”</mixed-citation>
         </ref>
         <ref id="ref42">
            <label>42.</label>
            <mixed-citation publication-type="other">
               <string-name>R. Rakhmatullin</string-name>,<source>
                  <italic>Dve Moskvi, ili Metafizika stolitsi</italic>
               </source>(Moscow<year>2008</year>),<fpage>15</fpage>–<lpage>16</lpage>.</mixed-citation>
         </ref>
         <ref id="ref43">
            <label>43.</label>
            <mixed-citation publication-type="journal">Many capitals of the medieval European kingdoms modeled themselves on Rome or Jerusalem. Nuremberg, one of the capitals of the Holy Roman Empire, was considered the reincarnation of Jerusalem and had topological similarities to Jerusalem (e.g., Olive Hill and the like). The same holds for the medieval Russian capitals Moscow and Kiev. See<string-name>R. Stupperrich</string-name>,<article-title>“Kiev—das zweite Jerusalem,”</article-title>
               <source>
                  <italic>Zeitschrift für slavische Philologie</italic>
               </source>
               <volume>12</volume>(<year>1935</year>). Rakhmatullin is primarily interested in the parallel landscape features of Moscow and Rome (seven hills and the like).</mixed-citation>
         </ref>
         <ref id="ref44">
            <label>44.</label>
            <mixed-citation publication-type="journal">
               <string-name>A. Maler</string-name>,<article-title>“Otstupat'c nekuda—pozadi Moskva!”</article-title>
               <source>
                  <italic>Russkii zhurnal</italic>
               </source>, 25 January<year>2007</year>.</mixed-citation>
         </ref>
         <ref id="ref45">
            <label>45.</label>
            <mixed-citation publication-type="journal">
               <string-name>S. Turner and R. Turner</string-name>,<article-title>“Capital Cities: A Special Case in Urban Development,”</article-title>
               <source>
                  <italic>Annals of Regional Science</italic>
               </source>
               <volume>46</volume>,<issue>no. 1</issue>(February<year>2011</year>):<fpage>19</fpage>–<lpage>35</lpage>;<string-name>Ronald L. Moomaw and Mohammad A. Alwosabi</string-name>,<article-title>“An Empirical Analysis of Competing Explanations of Urban Primacy: Evidence from Asia and the Americas,”</article-title>
               <source>
                  <italic>Annals of Regional Science</italic>
               </source>
               <volume>38</volume>,<issue>no. 1</issue>(March<year>2004</year>);<string-name>Horst Zimmermann</string-name>,<article-title>“Do Different Types of Capital Cities Make a Difference for Economic Dynamism?”</article-title>
               <source>
                  <italic>Environment and Planning C: Government and Policy</italic>
               </source>
               <volume>28</volume>,<issue>no. 5</issue>(<year>2010</year>):<fpage>761</fpage>–<lpage>67</lpage>.</mixed-citation>
         </ref>
         <ref id="ref46">
            <label>46.</label>
            <mixed-citation publication-type="other">
               <string-name>Jean Gottman</string-name>,<article-title>“Capital Cities,”</article-title>in<string-name>Jean Gottman and Robert Harper</string-name>, eds.,<source>
                  <italic>Since Megapolis: The Urban Writings of Jean Gottman</italic>
               </source>(Baltimore,<year>1990</year>),<fpage>63</fpage>–<lpage>82</lpage>;<string-name>David L. A. Gordon and Mark L. Seasons</string-name>,<article-title>“Administrative and Financial Strategies for Implementing Plans in Political Capitals,”</article-title>
               <source>
                  <italic>Canadian Journal of Urban Research</italic>
               </source>
               <volume>18</volume>,<issue>no. 1</issue>(Summer<year>2009</year>);<string-name>Peter Hall</string-name>,<article-title>“The Changing Role of Capital Cities: Six Types of Capital City,”</article-title>in<string-name>John H. Taylor, Jean G. Lengellé, and C. Andrew</string-name>, eds.,<source>
                  <italic>Capital Cities International Perspectives/Les Capitales Perspectives Internationales</italic>
               </source>(Ottawa,<year>1993</year>).</mixed-citation>
         </ref>
         <ref id="ref47">
            <label>47.</label>
            <mixed-citation publication-type="journal">On the dynamic systems approach, see<string-name>Man-Hyung Lee, Nam-Hee Choi, and Moonseo Park</string-name>,<article-title>“A Systems Thinking Approach to the New Administrative Capital in Korea: Balanced Development or Not?”</article-title>
               <source>
                  <italic>System Dynamics Review</italic>
               </source>
               <volume>21</volume>,<issue>no. 1</issue>(Spring<year>2005</year>):<fpage>69</fpage>–<lpage>85</lpage>.</mixed-citation>
         </ref>
         <ref id="ref48">
            <label>48.</label>
            <mixed-citation publication-type="other">One curious example will suffice. V. P. Patrakov produced a whole book, where he proposed a numerological way to study the genealogy of new capital cities and their relocation based on the poetic metaphors of Velimir Khlebnikov, a Russian avant-garde poet. See<string-name>V. P. Patrakov</string-name>,<source>
                  <italic>Rozhdenie stolits: Ot proshlogo k budushchemu</italic>
               </source>(Kharkov,<year>2008</year>).</mixed-citation>
         </ref>
         <ref id="ref49">
            <label>49.</label>
            <mixed-citation publication-type="other">
               <string-name>Geoffrey Hosking</string-name>,<source>
                  <italic>Russia and the Russians: A History from Rus to the Russian Federation</italic>
               </source>(London,<year>2001</year>).</mixed-citation>
         </ref>
         <ref id="ref50">
            <label>50.</label>
            <mixed-citation publication-type="other">
               <string-name>Ksenia Kas'ianova</string-name>,<article-title>“Rossiia perezhivaet period perekhoda k natsional'nomu gosudarstvu,”</article-title>in<string-name>Kas'ianova</string-name>,<source>
                  <italic>O russkom natsional'nom kharaktere</italic>
               </source>(Moscow<year>1994</year>),<fpage>16</fpage>.</mixed-citation>
         </ref>
         <ref id="ref51">
            <label>51.</label>
            <mixed-citation publication-type="journal">
               <string-name>Deborah Potts</string-name>,<article-title>“Capital Relocation in Africa: The Case of Lilongwe in Malawi,”</article-title>
               <source>
                  <italic>Geographical Journal</italic>
               </source>
               <volume>151</volume>,<issue>no. 2</issue>(July<year>1985</year>):<fpage>182</fpage>–<lpage>96</lpage>.</mixed-citation>
         </ref>
         <ref id="ref52">
            <label>52.</label>
            <mixed-citation publication-type="journal">
               <string-name>Edward Schatz</string-name>,<article-title>“What Capital Cities Say about State and Nation Building,”</article-title>
               <source>
                  <italic>Nationalism and Ethnic Politics</italic>
               </source>
               <volume>9</volume>,<issue>no. 4</issue>(<year>2004</year>):<fpage>111</fpage>–<lpage>40</lpage>.</mixed-citation>
         </ref>
         <ref id="ref53">
            <label>53.</label>
            <mixed-citation publication-type="journal">
               <string-name>Rhoads Murphey</string-name>,<article-title>“New Capitals of Asia,”</article-title>
               <source>
                  <italic>Economic Development and Cultural Change</italic>
               </source>
               <volume>5</volume>,<issue>no. 3</issue>(April<year>1957</year>):<fpage>216</fpage>–<lpage>43</lpage>; (<year>1957</year>);<string-name>J. C. Nwafor</string-name>,<article-title>“The Relocation of Nigeria's Federal Capital: A Device for Greater Territorial Integration and National Unity,”</article-title>
               <source>
                  <italic>GeoJournal</italic>
               </source>
               <volume>4</volume>,<issue>no. 4</issue>(<year>1980</year>):<fpage>359</fpage>–<lpage>66</lpage>; Potts, “Capital Relocation in Africa,” 182–96.</mixed-citation>
         </ref>
         <ref id="ref54">
            <label>54.</label>
            <mixed-citation publication-type="journal">
               <article-title>“The Pros and Cons of Capital Flight,”</article-title>
               <source>
                  <italic>The Economist</italic>
               </source>, 13 August<year>2004</year>;<string-name>Simon Akam</string-name>,<article-title>“Not Such a Capital Idea after All,”</article-title>
               <source>
                  <italic>Independent</italic>
               </source>, 24 February<year>2011</year>.</mixed-citation>
         </ref>
         <ref id="ref55">
            <label>55.</label>
            <mixed-citation publication-type="other">
               <string-name>Harry W. Richardson</string-name>,<article-title>“The Location and Relocation of National and State Capitals in North America and the Rest of the World,”</article-title>in<string-name>Korea Planners Association</string-name>,<source>
                  <italic>A Planning Policy for Korea's New Capital City</italic>
               </source>(Seoul,<year>2003</year>),<fpage>119</fpage>–<lpage>30</lpage>.</mixed-citation>
         </ref>
         <ref id="ref56">
            <label>56.</label>
            <mixed-citation publication-type="other">
               <string-name>N. Karamzin</string-name>,<source>
                  <italic>Zapiska o drevnei i novoi Rossii v ee politicheskom i grazhdanskom otnosheniiakh</italic>
               </source>(Moscow,<year>1991</year>),<fpage>159</fpage>.</mixed-citation>
         </ref>
         <ref id="ref57">
            <label>57.</label>
            <mixed-citation publication-type="other">
               <string-name>Kenneth E. Corey</string-name>,<source>“Relocation of National Capitals: Implication for Korea,”</source>
               <source>
                  <italic>International Symposium on the Capital Relocation</italic>
               </source>(Seoul,<year>2004</year>),<fpage>42</fpage>–<lpage>127</lpage>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
