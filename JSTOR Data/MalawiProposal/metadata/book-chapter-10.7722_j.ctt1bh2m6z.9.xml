<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1bh2m6z</book-id>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Mozambique</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Mozambique</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
            <subj-group>
               <subject content-type="lcsh">1975-</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Mozambique</subject>
         <subj-group>
            <subject content-type="lcsh">Economic policy</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Language &amp; Literature</subject>
      </subj-group>
      <book-title-group>
         <book-title>Do Bicycles Equal Development in Mozambique?</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>HANLON</surname>
               <given-names>JOSEPH</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>SMART</surname>
               <given-names>TERESA</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>25</day>
         <month>03</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="ppub">9781847013194</isbn>
      <isbn content-type="epub">9781846156281</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk, UK; Rochester, NY, USA</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2008</copyright-year>
         <copyright-holder>Joseph Hanlon</copyright-holder>
         <copyright-holder>Teresa Smart</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt1bh2m6z"/>
      <abstract abstract-type="short">
         <p>Is Mozambique an African success story? It has 7 percent a year growth rate and substantial foreign investment. Fifteen years after the war of destabilisation, the peace has held. Mozambique is the donors' model pupil, carefully following their prescriptions and receiving more than a billion dollars a year in aid. The number of bicycles has doubled and this is often cited as the symbol of development. In this book the authors challenge some key assumptions of both the donors and the government and ask questions such as whether there has been too much stress on the Millennium Development Goals and too little support for economic development; if it makes sense to target the poorest of the poor, or would it be better to target those who create the jobs which will employ the poor; whether there has been too much emphasis on foreign investment and too little on developing domestic capital; and if the private sector really will end poverty, or must there be a stronger role for the state in the economy? This book is about more than Mozambique. Mozambique is an apparent success story that is used to justify the present 'post-Washington consensus' development model. Here, the case of Mozambique is situated within the broader development debate. Joseph Hanlon is Senior Lecturer at the Open University and the author of Beggar Your Neighbours; Mozambique: Who Calls the Shots?; and Peace without Profit (all published by James Currey) which have all made influential interventions in the development debate; Teresa Smart is Director of the London Mathematics Centre, Institute of Education.</p>
      </abstract>
      <counts>
         <page-count count="256"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.3</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hanlon</surname>
                           <given-names>Joseph</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Smart</surname>
                           <given-names>Teresa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.4</book-part-id>
                  <title-group>
                     <title>The authors</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.5</book-part-id>
                  <title-group>
                     <title>Money &amp; measurements</title>
                  </title-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.6</book-part-id>
                  <title-group>
                     <title>Abbreviations, acronyms &amp; glossary</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction:</title>
                     <subtitle>more bicycles, but …</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Felito Julião is carrying two bundles of sugar cane on his bicycle. This bright young man earns a living with his bicycle, and has found a way to carry two bundles of cane instead of just one, like the others But the two bundles of tall cane are unstable and he has to walk slowly up the many hills. It takes more than half a day to travel the 23 km from Rapale to Nampula and, in practice, he has to travel with a friend who carries only one bundle, and who can help him if a bundle comes loose</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A brief history:</title>
                     <subtitle>war, peace &amp; slow recovery</subtitle>
                  </title-group>
                  <fpage>6</fpage>
                  <abstract>
                     <p>In the 33 years since independence Mozambique has gone though a bewildering series of rapid changes, reflecting not only local events but those in southern Africa and the wider world. It has seen great hope and immense suffering. Too often, its fate has not been in its own hands. And outsiders still play an overwhelming and overweening role, as will be clear in this book. But Mozambicans have proved to be a remarkable people, adapting to the changes and building a nation against fearsome odds.</p>
                     <p>President Armando Guebuza fought in the decade-long liberation war which not only brought independence to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Can peasants pull Nampula out of poverty?</title>
                  </title-group>
                  <fpage>16</fpage>
                  <abstract>
                     <p>We were in Iapala as part of extended visits to two provinces, Nampula and Manica, where everyone said there were more bicycles, and so it was believed that development was taking place. We also chose these places because they seemed very different. Nampula is a province with small producers and associations, while Manica was reportedly embarking on a boom driven by medium-sized commercial farmers. This chapter and the next report on our investigations.</p>
                     <p>Associação de Ehiquite-Iapala is a model association: the sort we had heard about and were strongly encouraged to visit by the Ribáuè¹ district administrator. Members are accustomed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Manica miracle is over</title>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>If Nampula seems likely to grow on the basis of small-scale peasant agriculture, the engine of growth for Manica province was set to be larger commercial agriculture.¹ The 2001–4 Manica boom was triggered by the crisis in Zimbabwe. Tobacco companies encouraged Zimbabwean farmers to migrate to Manica by offering credit and technical assistance; paprika companies also provided support. In the end 42 Zimbabwean individuals and groups settled in Manica and a similar number of Mozambicans (and foreigners already permanently resident in Mozambique) received support. The Zimbabwean farmers alone created 4,385 jobs in 2003 and estimated that they were putting</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Cashew:</title>
                     <subtitle>from disaster to export model</subtitle>
                  </title-group>
                  <fpage>36</fpage>
                  <abstract>
                     <p>Warren, who runs the nut stall in our local street market in London, sells Mozambican cashew nuts. ‘Better quality than the ones from other countries,’ he told us. And therein lies a tale of an industry destroyed and resurrected, with perhaps some lessons as to how Mozambican peasants can survive in a globalised world. Mozambique was the world’s largest producer of cashew nuts until 1977, and the nut seems ideal for a poor country since it is grown by millions of peasants on poor land and processing employs thousands of people. In this chapter we look at three overlapping phases</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Tobacco:</title>
                     <subtitle>hard choices</subtitle>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>Tobacco is Mozambique’s largest agricultural export, rising from $11 m. in 2003 to $109 m. in 2006, and now exceeding sugar, prawns, cotton or cashew (see Table 6.1). Mozambique has become the world’s fifth largest producer of burley tobacco.¹ Promotion of tobacco growing by international companies has done more to raise peasant incomes than any similar post-war action. But tobacco is highly controversial, on grounds of health and long-term potential, because of the way it also squeezes peasant households, and because of a complex local trade-off between industrial and agricultural development.</p>
                     <p>‘Tobacco is the second major cause of death in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Has poverty decreased?</title>
                  </title-group>
                  <fpage>57</fpage>
                  <abstract>
                     <p>Poverty is said to be falling, but malnutrition is rising. How can that be? This chapter explores what UNICEF calls ‘a central paradox in the Mozambican development trend’ (Dupraz et al. 2006: 18). Two key reports agree on the depth of poverty, but disagree on the trend. Poverty ‘levels remain high. More than half the population fails to attain even the very basic standard of living represented by poverty lines,’ according to a 2004 report (Massingarela et al. 2004a: v). ‘Children have poor nutrition in the overwhelming majority of Mozambican households,’ reports UNICEF in its 2006 report (Dupraz et al.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Is there development in Mozambique?</title>
                  </title-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>Most Mozambicans are still desperately poor, and raising incomes is the most important developmental objective. But in this chapter we want to look beyond cash, at wider issues of poverty and development, and how Mozambicans are doing against those standards. First, we consider two approaches, one based on rights and the other on children and the Millennium Development Goals (MDGs). Next, we look more broadly at development as a process of change. That leads to considerations of social services such as health, education and water supply, and to questions about power and the extent to which Mozambicans are taking control</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Frelimo &amp; the democratic one-party state</title>
                  </title-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>When Sweden’s Social Democratic Party lost the election in September 2006, it had been in power for all but nine years since 1932. Sweden is one of many democratic countries with a ‘natural’ party of government which is repeatedly elected over decades.</p>
                     <p>In many cases, a liberation movement becomes the dominant party, such as Congress in India and the PRI in Mexico (where PRI actually stands for the Institutionalised Revolutionary Party). Similarly, in southern Africa, it looks increasingly as if the African National Congress (ANC) in South Africa, Chama Cha Mapinduzi (CCM, Party of the Revolution) in Tanzania, and Frelimo</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.16</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Corruption, rent-seeking, reform &amp; a divided elite</title>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>At independence in 1975, the new government did not replace the old colonial banknotes and coins.¹ Then on Sunday night 15 June 1980, President Samora Machel announced on the radio that the old ‘escudo’ would be replaced, one for one, by a new ‘metical’, over the following three days. Money in bank accounts was converted automatically, but cash had to be changed at banks or, in rural areas, mobile change centres.</p>
                     <p>We were working in Mozambique then and have strong memories of the change-over. One of us, Teresa Smart, was then a teacher at the Industrial Institute in Maputo. She</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.17</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Aid dependence &amp; subservience:</title>
                     <subtitle>carrots &amp; sticks</subtitle>
                  </title-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>In early 2007 the IMF reported (IMF, 2007c: 4) ‘Mozambique is a success story in sub-Saharan Africa, benefiting from sustained large foreign aid inflows, strong and broad-based growth and deep poverty reduction.’ A joint donor-government study in early 2007 stated ‘Mozambique is generally considered an aid success story.’ But this study also ‘revealed a widespread perception that Government leadership and ownership of the aid agenda has left donors in the driver’s seat.’² In a 2004 study, Tony Hodges and Roberto Tibana stated ‘high aid dependence means that the budget process essentially involves only two actors, the executive and foreign donors.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.18</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>On the edge of the world</title>
                  </title-group>
                  <fpage>137</fpage>
                  <abstract>
                     <p>As a small country in a large continent and larger world, the actions of others impose heavy constraints on the choices Mozambique can make, but also point to some of the opportunities. Mozambique has a number of advantages – land, people, and being a donor darling – but to the larger world it is marginal and peripheral, buffeted by decisions taken by people who may not even know it exists.</p>
                     <p>In this chapter we look in more detail at the global forces affecting Mozambique. We start with an embarrassing and damning independent evaluation of World Bank research – by evaluators</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.19</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Questioning the cargo cult</title>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>‘Our policy is to attract foreign investors who bring capital and do not need Mozambican banks in order to invest. The investor has access to markets and brings know-how and even staff in order to train Mozambicans,’ explained Roberto Albino, director of the government’s Commercial Agriculture Promotion Centre (CEPAGRI, Centro de Promoção Agrária).¹ And he went on to cite sugar, which now accounts for more than 60% of Mozambique’s irrigated agriculture, and has been developed entirely by foreign investors.</p>
                     <p>In an interview on 26 October 2006, Aiuba Cuereneia, Planning and Development Minister and a member of Frelimo’s Political Commission, set</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.20</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Increase demand to kick-start the economy</title>
                  </title-group>
                  <fpage>154</fpage>
                  <abstract>
                     <p>The poor are poor because they have no money. That simple statement goes a long way to explaining the economic problems in Mozambique. As Chapter 7 showed, the large majority of the rural poor are too poor to make even the most basic investments needed to help lift themselves out of poverty. Chapter 8 ended with a picture of Lichinga market, but it could have been in hundreds of different places, where market women sit behind tiny piles of vegetables or dried fish, which cost only pennies, but most people are too poor to buy. There are more bicycles, but</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.21</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Agriculture &amp; the new role for the state</title>
                  </title-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>For three decades, the centrality of agriculture to Mozambican development has been recognised. Agriculture still supports 80% of the Mozambican population (World Bank 2005a: xiii). There was growth in production after the end of the war, but it came largely through displaced people returning home and re-opening land that had not been used for many years. The World Bank (2006a) warns that such growth is not sustainable because there has been no modernisation; more than 30 years after independence, most peasants farm in the same way as their grandparents.</p>
                     <p>Of Mozambican farm families, 99.6% have small holdings averaging only 1.2</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.22</book-part-id>
                  <title-group>
                     <label>16</label>
                     <title>Finance &amp; a development bank</title>
                  </title-group>
                  <fpage>175</fpage>
                  <abstract>
                     <p>‘The lack of a development bank and the lack of interest shown by the recently privatised commercial banks in providing credit to the rural sector have calamitous effects in the financing of development, especially for commercial agriculture,’ warned the report<italic>Agenda 2025</italic>(Committee of Counsellors 2003: 159).¹ Released in 2003, it was an attempt by a very broad group of 14 Mozambican opinion leaders to create a development vision for the coming two decades. Emanating from a group that represented both political parties as well as academics and bankers, it was a statement of the obvious which clearly had broad</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.23</book-part-id>
                  <title-group>
                     <label>17</label>
                     <title>The developmental state builds capitalism</title>
                  </title-group>
                  <fpage>188</fpage>
                  <abstract>
                     <p>Colonialism and a brief dabble with socialism mean that, for strong historical reasons, Mozambique does not have an entrepreneurial tradition. If it is to build a productive national bourgeoisie and entrepreneurial class that will create jobs, then the state will need to take an active role in building domestic capitalism – just as states in the now industrialised countries did in the nineteenth and twentieth centuries. In contemporary clichés, in the ‘free market’ the ‘playing field’ is tilted in favour of those who already have money and experience; in Mozambique’s case these are largely foreigners. Only the state can level</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.24</book-part-id>
                  <title-group>
                     <label>18</label>
                     <title>Can Mozambique stop putting its hand out &amp; become a developmental state?</title>
                  </title-group>
                  <fpage>200</fpage>
                  <abstract>
                     <p>The picture above is proof of development. The girl and boy are coming home from a rural school, beneficiaries of a huge expansion in education. And she is riding a relatively new bicycle. So, yes, there are more bicycles and more school places. But the boy, like most people, is walking. And they are unlikely to find jobs when they leave school. There has been development, but not enough. Sixteen years after the end of the war, most people remain desperately poor; even those moving out of poverty remain precarious, with a strong chance of falling back into poverty, as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.25</book-part-id>
                  <title-group>
                     <title>Appendix 1:</title>
                     <subtitle>Aid</subtitle>
                  </title-group>
                  <fpage>209</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.26</book-part-id>
                  <title-group>
                     <title>Appendix 2:</title>
                     <subtitle>Investment &amp; other tables</subtitle>
                  </title-group>
                  <fpage>215</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.27</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>217</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.28</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>233</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1bh2m6z.29</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
