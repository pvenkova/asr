<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">revieconstat</journal-id>
         <journal-id journal-id-type="jstor">j100341</journal-id>
         <journal-title-group>
            <journal-title>The Review of Economics and Statistics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>MIT Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00346535</issn>
         <issn pub-type="epub">15309142</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2646838</article-id>
         <title-group>
            <article-title>Slowdowns and Meltdowns: Postwar Growth Evidence from 74 Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Dan</given-names>
                  <surname>Ben-David</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>David H.</given-names>
                  <surname>Papell</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>11</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">80</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i345513</issue-id>
         <fpage>561</fpage>
         <lpage>571</lpage>
         <page-range>561-571</page-range>
         <permissions>
            <copyright-statement>Copyright 1998 The President and Fellows of Harvard College and the Massachusetts Institute of Technology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2646838"/>
         <abstract>
            <p>This paper proposes an explicit test for determining the significance and the timing of slowdowns in economic growth. We examine a large sample of countries and find that a majority-though not all-exhibit a significant structural break in their postwar growth rates. We find that (a) most industrialized countries experienced postwar growth slowdowns in the early 1970s, though (b) the United States, Canada, and the United Kingdom did not, and (c) developing countries (and in particular, Latin American countries) tended to experience much more severe slowdowns which, in contrast with the more developed countries, began nearly a decade later.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d379e132a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d379e139" publication-type="journal">
Griliches (1980)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e147" publication-type="journal">
Nordhaus (1982)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e155" publication-type="journal">
Bruno
(1984)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e167" publication-type="journal">
Darby (1984)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e175" publication-type="journal">
Romer (1987)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e183" publication-type="book">
Baumol et al. (1989)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e191" publication-type="book">
De Long
and Summers (1992)  </mixed-citation>
            </p>
         </fn>
         <fn id="d379e203a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d379e210" publication-type="journal">
Azariadis and Drazen
(1990)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e221" publication-type="journal">
Baumol (1986)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e229" publication-type="book">
Baumol et al. (1989)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e238" publication-type="book">
Ben-David (1994)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e246" publication-type="journal">
Quah (1993)  </mixed-citation>
            </p>
         </fn>
         <fn id="d379e255a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d379e262" publication-type="journal">
Ng and Perron (1995)  </mixed-citation>
            </p>
         </fn>
         <fn id="d379e271a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d379e278" publication-type="journal">
Vogelsang (1997)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e286" publication-type="journal">
Andrews (1993)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e294" publication-type="journal">
Andrews and Ploberger (1994)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e303" publication-type="journal">
Bai et al. (1997)  </mixed-citation>
            </p>
         </fn>
         <fn id="d379e313a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d379e320" publication-type="partial">
Bai and Perron (1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d379e327a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d379e334" publication-type="journal">
Dale Jorgenson, Mancur Olson,
Zvi Griliches, and Michael Boskin at a Journal of Economic Perspectives<person-group>
                     <string-name>
                        <surname>Jorgenson</surname>
                     </string-name>
                  </person-group>
                  <source>Journal of Economic Perspectives</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d379e355" publication-type="journal">
Fischer (1988)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d379e373a1310">
            <mixed-citation id="d379e377" publication-type="journal">
Andrews, Donald, "Tests for Parameter Instability and Structural Change
with Unknown Change Point," Econometrica61 (1993), 821-856.<object-id pub-id-type="doi">10.2307/2951764</object-id>
               <fpage>821</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e393a1310">
            <mixed-citation id="d379e397" publication-type="journal">
Andrews, Donald, and Wemer Ploberger, "Optimal Tests when a Nuisance
Parameter Is Present Only under the Alternative," Econometrica62
(1994), 1383-1414.<object-id pub-id-type="doi">10.2307/2951753</object-id>
               <fpage>1383</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e416a1310">
            <mixed-citation id="d379e420" publication-type="journal">
Azariadis, Costas, and Alan Drazen, "Threshold Externalities in Economic
Development," Quarterly Journal of Economics105 (1990), 501-
526.<object-id pub-id-type="doi">10.2307/2937797</object-id>
               <fpage>501</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e439a1310">
            <mixed-citation id="d379e443" publication-type="journal">
Bai, Jushan, "Estimation of a Change Point in Multiple Regression
Models," Review of Economics and Statistics97 (1997), 551-563.<object-id pub-id-type="jstor">10.2307/2951407</object-id>
               <fpage>551</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e460a1310">
            <mixed-citation id="d379e464" publication-type="book">
Bai, Jushan, Robin Lumsdaine, and James Stock, "Testing for and Dating
Breaks in Integrated and Cointegrated Time Series," mimeo, MIT
(1991).<person-group>
                  <string-name>
                     <surname>Bai</surname>
                  </string-name>
               </person-group>
               <source>Testing for and Dating Breaks in Integrated and Cointegrated Time Series</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d379e493a1310">
            <mixed-citation id="d379e497" publication-type="journal">
—Testing for and Dating Common Breaks in Multivariate Time
Series," forthcoming, Review of Economic Studies (1997).<person-group>
                  <string-name>
                     <surname>Bai</surname>
                  </string-name>
               </person-group>
               <source>Review of Economic Studies</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d379e522a1310">
            <mixed-citation id="d379e526" publication-type="journal">
Bai, Jushan, and Pierre Perron, "Testing for and Estimation of Multiple
Structural Changes," Econometrica66 (1998), 817-858.<object-id pub-id-type="doi">10.2307/2998540</object-id>
               <fpage>817</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e542a1310">
            <mixed-citation id="d379e546" publication-type="journal">
Banerjee, Anindya, Robin Lumsdaine, and James Stock, "Recursive and
Sequential Tests of the Unit Root and Trend Break Hypotheses:
Theory and International Evidence," Journal of Business and
Economic Statistics10 (1992), 271-287.<object-id pub-id-type="doi">10.2307/1391542</object-id>
               <fpage>271</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e569a1310">
            <mixed-citation id="d379e573" publication-type="journal">
Baumol, William J., "Productivity Growth, Convergence, and Welfare:
What the Long-Run Data Show," American Economic Review76
(1986), 1072-1085.<object-id pub-id-type="jstor">10.2307/1816469</object-id>
               <fpage>1072</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e592a1310">
            <mixed-citation id="d379e596" publication-type="book">
Baumol, William J., Sue Ann B. Blackman, and Edward N. Wolff,
Productivity and American Leadership: The Long View (Cam-
bridge, MA: MIT Press, 1989).<person-group>
                  <string-name>
                     <surname>Baumol</surname>
                  </string-name>
               </person-group>
               <source>Productivity and American Leadership: The Long View</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d379e626a1310">
            <mixed-citation id="d379e630" publication-type="book">
Ben-David, Dan, "Convergence Clubs and Diverging Economics," CEPR
Working Paper 922 (1994).<person-group>
                  <string-name>
                     <surname>Ben-David</surname>
                  </string-name>
               </person-group>
               <source>Convergence Clubs and Diverging Economics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d379e655a1310">
            <mixed-citation id="d379e659" publication-type="journal">
Bruno, Michael, "Raw Materials, Profits, and the Productivity Slow-
down," Quarterly Journal of Economics99 (1984), 1-12.<object-id pub-id-type="doi">10.2307/1885718</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e675a1310">
            <mixed-citation id="d379e679" publication-type="journal">
Campbell, John Y, and Pierre Perron, "Pitfalls and Opportunities: What
Macroeconomists Should Know about Unit Roots," NBER Macro-
economic Annual (1991), 141-201.<person-group>
                  <string-name>
                     <surname>Campbell</surname>
                  </string-name>
               </person-group>
               <fpage>141</fpage>
               <source>NBER Macroeconomic Annual</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d379e711a1310">
            <mixed-citation id="d379e715" publication-type="book">
Cardoso, Eliana A., and Rudiger Dornbusch, "Brazilian Debt Crises: Past
and Present," in Barry Eichengreen and Peter H. Lindert (eds.), The
International Debt Crisis in Historical Perspective (Cambridge,
MA: MIT Press, 1989), 106-139<person-group>
                  <string-name>
                     <surname>Cardoso</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Brazilian Debt Crises: Past and Present</comment>
               <fpage>106</fpage>
               <source>The International Debt Crisis in Historical Perspective</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d379e753a1310">
            <mixed-citation id="d379e757" publication-type="journal">
Darby, Michael R., "The U.S. Productivity Slowdown: A Case of
Statistical Myopia," American Economic Review74 (1984), 301-
322.<object-id pub-id-type="jstor">10.2307/1804009</object-id>
               <fpage>301</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e776a1310">
            <mixed-citation id="d379e780" publication-type="book">
De Long, J. Bradford, and Lawrence H. Summers, "Macroeconomic
Policy and Long-Run Growth," in Policies for Long-Run Economic
Growth (Federal Reserve Bank of Kansas City, 1992), 93-128.<person-group>
                  <string-name>
                     <surname>De Long</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Macroeconomic Policy and Long-Run Growth</comment>
               <fpage>93</fpage>
               <source>Policies for Long-Run Economic Growth</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d379e816a1310">
            <mixed-citation id="d379e820" publication-type="book">
Dornbusch, Rudiger, and Stanley Fischer, "The World Debt Problem," in
Sydney Dell (ed.), The International Monetary System and Its
Reform (Amsterdam: North-Holland, 1987), 907-959.<person-group>
                  <string-name>
                     <surname>Dornbusch</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The World Debt Problem</comment>
               <fpage>907</fpage>
               <source>The International Monetary System and Its Reform</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d379e855a1310">
            <mixed-citation id="d379e859" publication-type="journal">
Fischer, Stanley, "Symposium on the Slowdown in Productivity Growth,"
Journal of Economic Perspectives2 (1988), 3-7.<object-id pub-id-type="jstor">10.2307/1942774</object-id>
               <fpage>3</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e875a1310">
            <mixed-citation id="d379e879" publication-type="journal">
Griliches, Zvi, "R &amp; D and the Productivity Slowdown," American
Economic Review70 (1980), 343-348.<object-id pub-id-type="jstor">10.2307/1815495</object-id>
               <fpage>343</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e895a1310">
            <mixed-citation id="d379e899" publication-type="book">
International Monetary Fund, International Financial Statistics Yearbook
(Washington, DC, various editions).<person-group>
                  <string-name>
                     <surname>International Monetary Fund</surname>
                  </string-name>
               </person-group>
               <source>International Financial Statistics Yearbook</source>
            </mixed-citation>
         </ref>
         <ref id="d379e921a1310">
            <mixed-citation id="d379e925" publication-type="book">
Kahn, George A., "Symposium Summary," in Policies for Long-Run
Economic Growth (Federal Reserve Bank of Kansas City, (1992),
17-33.<person-group>
                  <string-name>
                     <surname>Kahn</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Symposium Summary</comment>
               <fpage>17</fpage>
               <source>Policies for Long-Run Economic Growth</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d379e960a1310">
            <mixed-citation id="d379e964" publication-type="book">
Lindert, Peter H., "Response to Debt Crisis: What is Different about the
1980s?" in Barry Eichengreen and Peter H. Lindert (eds.), The
International Debt Crisis in Historical Perspective (Cambridge,
MA: MIT Press, 1989), 227-275.<person-group>
                  <string-name>
                     <surname>Lindert</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Response to Debt Crisis: What is Different about the 1980s?</comment>
               <fpage>227</fpage>
               <source>The International Debt Crisis in Historical Perspective</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1003a1310">
            <mixed-citation id="d379e1007" publication-type="journal">
Ng, Serena, and Pierre Perron, "Unit Root Tests in ARMA Models with
Data Dependent Methods for the Selection of the Truncation Lag,"
Journal of the American Statistical Association90 (1995), 268-
281.<object-id pub-id-type="doi">10.2307/2291151</object-id>
               <fpage>268</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e1030a1310">
            <mixed-citation id="d379e1034" publication-type="journal">
Nordhaus, William D., "Economic Policy in the Face of Declining
Productivity Growth," European Economic Review18 (1982),
131-157.<person-group>
                  <string-name>
                     <surname>Nordhaus</surname>
                  </string-name>
               </person-group>
               <fpage>131</fpage>
               <volume>18</volume>
               <source>European Economic Review</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1069a1310">
            <mixed-citation id="d379e1073" publication-type="journal">
Perron, Pierre, "The Great Crash, the Oil Price Shock, and the Unit Root
Hypothesis," Econometrica57 (1989), 1361-1401.<object-id pub-id-type="doi">10.2307/1913712</object-id>
               <fpage>1361</fpage>
            </mixed-citation>
         </ref>
         <ref id="d379e1089a1310">
            <mixed-citation id="d379e1093" publication-type="book">
—Further Evidence on Breaking Trend Functions in Macroeco-
nomic Variables," mimeo, University of Montreal (1994).<person-group>
                  <string-name>
                     <surname>Perron</surname>
                  </string-name>
               </person-group>
               <source>Further Evidence on Breaking Trend Functions in Macroeconomic Variables</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1118a1310">
            <mixed-citation id="d379e1122" publication-type="journal">
Quah, Danny, "Empirical Cross-Section Dynamics in Economic Growth,"
European Economic Review37 (1993), 426-434.<person-group>
                  <string-name>
                     <surname>Quah</surname>
                  </string-name>
               </person-group>
               <fpage>426</fpage>
               <volume>37</volume>
               <source>European Economic Review</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1154a1310">
            <mixed-citation id="d379e1158" publication-type="journal">
Romer, Paul M., "Crazy Explanations for the Productivity Slowdown,"
NBER Macroeconomics Annual (1987), 163-202.<person-group>
                  <string-name>
                     <surname>Romer</surname>
                  </string-name>
               </person-group>
               <fpage>163</fpage>
               <source>NBER Macroeconomics Annual</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1188a1310">
            <mixed-citation id="d379e1192" publication-type="book">
Shigehara, Kumiharu, "Causes of Declining Growth in Industrialized
Countries," in Policies for Long-Run Economic Growth (Federal
Reserve Bank of Kansas City, 1992), 15-39.<person-group>
                  <string-name>
                     <surname>Shigehara</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Causes of Declining Growth in Industrialized Countries</comment>
               <fpage>15</fpage>
               <source>Policies for Long-Run Economic Growth</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1227a1310">
            <mixed-citation id="d379e1231" publication-type="other">
Summers, Robert, and Alan Heston, "The Penn World Table (Mark 5.5)"
(1993).</mixed-citation>
         </ref>
         <ref id="d379e1241a1310">
            <mixed-citation id="d379e1245" publication-type="journal">
Vogelsang, Timothy, "Wald-Type Tests for Detecting Shifts in the Trend
Function of a Dynamic Time Series," Econometric Theory13
(1997), 818-849.<person-group>
                  <string-name>
                     <surname>Vogelsang</surname>
                  </string-name>
               </person-group>
               <fpage>818</fpage>
               <volume>13</volume>
               <source>Econometric Theory</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d379e1280a1310">
            <mixed-citation id="d379e1284" publication-type="journal">
Zivot, Eric, and Donald Andrews, "Further Evidence on the Great Crash,
the Oil Price Shock, and the Unit Root Hypothesis," Journal of
Business and Economic Statistics10 (1992), 251-270.<object-id pub-id-type="doi">10.2307/1391541</object-id>
               <fpage>251</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
