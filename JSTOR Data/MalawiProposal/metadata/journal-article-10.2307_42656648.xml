<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42656648</article-id>
         <title-group>
            <article-title>Provincialization of terrestrial faunas following the end-Permian mass extinction</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Christian A.</given-names>
                  <surname>Sidor</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daril A.</given-names>
                  <surname>Vilhena</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kenneth D.</given-names>
                  <surname>Angielczyk</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Adam K.</given-names>
                  <surname>Huttenlocker</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sterling J.</given-names>
                  <surname>Nesbitt</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Brandon R.</given-names>
                  <surname>Peecook</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>J. Sébastien</given-names>
                  <surname>Steyer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Roger M. H.</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Linda A.</given-names>
                  <surname>Tsuji</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>14</day>
            <month>5</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">110</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">20</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40099488</issue-id>
         <fpage>8129</fpage>
         <lpage>8133</lpage>
         <permissions>
            <copyright-statement>copyright © 1993-2008 National Academy of Sciences of the United States of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42656648"/>
         <abstract>
            <p>In addition to their devastating effects on global biodiversity, mass extinctions have had a long-term influence on the history of life by eliminating dominant lineages that suppressed ecological change. Here, we test whether the end-Permian mass extinction (252.3 Ma) affected the distribution of tetrapod faunas within the southern hemisphere and apply quantitative methods to analyze four components of biogeographic structure: connectedness, clustering, range size, and endemism. For all four components, we detected increased provincialism between our Permian and Triassic datasets. In southern Pangea, a more homogeneous and broadly distributed fauna in the Late Permian (Wuchiapingian, ~257 Ma) was replaced by a provincial and biogeographically fragmented fauna by Middle Triassic times (Anisian, ~242 Ma). Importantly in the Triassic, lower latitude basins in Tanzania and Zambia included dinosaur predecessors and other archosaurs unknown elsewhere. The recognition of heterogeneous tetrapod communities in the Triassic implies that the end-Permian mass extinction afforded ecologically marginalized lineages the ecospace to diversify, and that biotic controls (i.e., evolutionary incumbency) were fundamentally reset. Archosaurs, which began diversifying in the Early Triassic, were likely beneficiaries of this ecological release and remained dominant for much of the later Mesozoic.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d195e245a1310">
            <label>1</label>
            <mixed-citation id="d195e252" publication-type="other">
Jablonski D (1986) Background and mass extinctions: The alternation of macroevo-
lutionary regimes. Science 231(4734):129-133.</mixed-citation>
         </ref>
         <ref id="d195e262a1310">
            <label>2</label>
            <mixed-citation id="d195e269" publication-type="other">
Jablonski D (2005) Mass extinctions and macroevolution. Paleobiology 31 (Suppl 2):
192-210.</mixed-citation>
         </ref>
         <ref id="d195e279a1310">
            <label>3</label>
            <mixed-citation id="d195e286" publication-type="other">
Benton MJ, Tverdokhlebov VP, Surkov MV (2004) Ecosystem remodelling among
vertebrates at the Permian-Triassic boundary in Russia. Nature 432(7013):97-100.</mixed-citation>
         </ref>
         <ref id="d195e296a1310">
            <label>4</label>
            <mixed-citation id="d195e303" publication-type="other">
Sahney S, Benton MJ (2008) Recovery from the most profound mass extinction of all
time. Proc Biol Sci 275(1636):759-765.</mixed-citation>
         </ref>
         <ref id="d195e314a1310">
            <label>5</label>
            <mixed-citation id="d195e321" publication-type="other">
Ward PD, et al. (2005) Abrupt and gradual extinction among Late Permian land
vertebrates in the Karoo basin, South Africa. Science 307(5710):709-714.</mixed-citation>
         </ref>
         <ref id="d195e331a1310">
            <label>6</label>
            <mixed-citation id="d195e338" publication-type="other">
Smith RMH, Botha J (2005) The recovery of terrestrial vertebrate diversity in the
South African Karoo Basin after the end-Permian extinction. C R Palevol 4(6-7):
623-636.</mixed-citation>
         </ref>
         <ref id="d195e351a1310">
            <label>7</label>
            <mixed-citation id="d195e358" publication-type="other">
Botha J, Smith RMH (2006) Rapid vertebrate recuperation in the Karoo Basin of South
Africa following the end-Permian extinction. J Afr Earth Sci 45(4-5):502-514.</mixed-citation>
         </ref>
         <ref id="d195e368a1310">
            <label>8</label>
            <mixed-citation id="d195e375" publication-type="other">
Smith RMH (1995) Changing fluvial environments across the Permian-Triassic
boundary in the Karoo Basin, South Africa and possible causes of tetrapod ex-
tinctions. Palaeogeogr Palaeoclimatol Palaeoecol 117(1-2):81-104.</mixed-citation>
         </ref>
         <ref id="d195e388a1310">
            <label>9</label>
            <mixed-citation id="d195e395" publication-type="other">
Ruta M, Benton MJ (2008) Calibrated diversity, tree topology and the mother of mass
extinctions: The lesson of temnospondyls. Palaeontology 51(6):1261-1288.</mixed-citation>
         </ref>
         <ref id="d195e405a1310">
            <label>10</label>
            <mixed-citation id="d195e412" publication-type="other">
Shi GR (1993) Multivariate data analysis in palaeoecology and palaeobiogeography —
A review. Palaeogeogr Palaeoclimatol Palaeoecol 105(3-4):199-234.</mixed-citation>
         </ref>
         <ref id="d195e423a1310">
            <label>11</label>
            <mixed-citation id="d195e430" publication-type="other">
Kreft H, Jetz W (2010) A framework for delineating biogeographical regions based on
species distributions. J Biogeogr 37(11):2029-2053.</mixed-citation>
         </ref>
         <ref id="d195e440a1310">
            <label>12</label>
            <mixed-citation id="d195e447" publication-type="other">
Irmis RB, Whiteside JH (2012) Delayed recovery of non-marine tetrapods after the
end-Permian mass extinction tracks global carbon cycle. Proc Biol Sci 279(1732):
1310-1318.</mixed-citation>
         </ref>
         <ref id="d195e460a1310">
            <label>13</label>
            <mixed-citation id="d195e467" publication-type="other">
Payne JL, et al. (2004) Large perturbations of the carbon cycle during recovery from
the end-permian extinction. Science 305(5683):506-509.</mixed-citation>
         </ref>
         <ref id="d195e477a1310">
            <label>14</label>
            <mixed-citation id="d195e484" publication-type="other">
Sidor CA, Damiani R, Hammer WR (2008) A new Triassic temnospondyl from Ant-
arctica and a review of Fremouw Formation biostratigraphy. J Vertebr Paleontol
28(3):656-663.</mixed-citation>
         </ref>
         <ref id="d195e497a1310">
            <label>15</label>
            <mixed-citation id="d195e504" publication-type="other">
Sidor CA, et al. (2010) Tetrapod fauna of the lowermost Usili Formation (Songea
Group, Ruhuhu Basin) of southern Tanzania, with a new burnetiid record. J Vertebr
Paleontol 30(3):696-703.</mixed-citation>
         </ref>
         <ref id="d195e517a1310">
            <label>16</label>
            <mixed-citation id="d195e524" publication-type="other">
Nesbitt SJ, et al. (2010) Ecologically distinct dinosaurian sister group shows early di-
versification of Ornithodira. Nature 464(7285):95-98.</mixed-citation>
         </ref>
         <ref id="d195e535a1310">
            <label>17</label>
            <mixed-citation id="d195e542" publication-type="other">
Angielczyk KD, et al. Permian and Triassic dicynodont (Therapsida: Anomodontia)
faunas of the Luangwa Basin, Zambia: Taxonomie update and implications for di-
cynodont biogeography and biostratigraphy. The Early Evolutionary History of Syn-
apsida, eds Kammerer CF, Angielczyk KD, Fröbisch J (Springer, Dordrecht), in press.</mixed-citation>
         </ref>
         <ref id="d195e558a1310">
            <label>18</label>
            <mixed-citation id="d195e565" publication-type="other">
Peecook BR, et al. (2013) A new silesaurid from the upper Ntawere Formation of
Zambia (Middle Triassic) demonstrates the rapid diversification of Silesauridae (Ave-
metarsalia: Dinosauriformes). J Vertebr Paleontol 33, in press.</mixed-citation>
         </ref>
         <ref id="d195e578a1310">
            <label>19</label>
            <mixed-citation id="d195e585" publication-type="other">
Dunne JA, Williams RJ, Martinez ND (2002) Food-web structure and network theory:
The role of connectance and size. Proc Natl Acad Sci USA 99(20):12917-12922.</mixed-citation>
         </ref>
         <ref id="d195e595a1310">
            <label>20</label>
            <mixed-citation id="d195e602" publication-type="other">
Fortunato S (2010) Community detection in graphs. Phys Rep 486(3-5):75-174.</mixed-citation>
         </ref>
         <ref id="d195e609a1310">
            <label>21</label>
            <mixed-citation id="d195e616" publication-type="other">
Lancichinetti A, Fortunato S (2009) Community detection algorithms: A comparative
analysis. Phys Rev E Stat Nonlin Soň Matter Phys 80(5 Pt 2):056117.</mixed-citation>
         </ref>
         <ref id="d195e626a1310">
            <label>22</label>
            <mixed-citation id="d195e633" publication-type="other">
Rosvall M, Bergstrom CT (2008) Maps of random walks on complex networks reveal
community structure. Proc Natl Acad Sci USA 105(4):1118-1123.</mixed-citation>
         </ref>
         <ref id="d195e644a1310">
            <label>23</label>
            <mixed-citation id="d195e651" publication-type="other">
Rubidge BS (2005) Re-uniting lost continents—Fossil reptiles from the ancient Karoo
and their wanderlust. 5 Afr J Geol 108(1):135-172.</mixed-citation>
         </ref>
         <ref id="d195e661a1310">
            <label>24</label>
            <mixed-citation id="d195e668" publication-type="other">
Smith R, Rubidge B, van der Walt M (2011) Therapsid biodiversity patterns and
paleoenvironments of the Karoo Basin, South Africa. Forerunners of Mammals:
Radiation, Histology, Biology, ed Chinsamy-Turan A (Indiana Univ Press, Indianapolis),
pp 30-62.</mixed-citation>
         </ref>
         <ref id="d195e684a1310">
            <label>25</label>
            <mixed-citation id="d195e691" publication-type="other">
Hancox PJ, Rubidge BS (2001) Breakthroughs in the biodiversity, biogeography,
biostratigraphy, and basin analysis of the Beaufort Group. J Afr Earth Sci 33(3-4):
563-577.</mixed-citation>
         </ref>
         <ref id="d195e704a1310">
            <label>26</label>
            <mixed-citation id="d195e711" publication-type="other">
Jin YG, et al. (2000) Pattern of marine mass extinction near the Permian-Triassic
boundary in South China. Science 289(5478):432-436.</mixed-citation>
         </ref>
         <ref id="d195e721a1310">
            <label>27</label>
            <mixed-citation id="d195e728" publication-type="other">
King GM (1991) Terrestrial tetrapods and the end Permian event: A comparison of
analyses. Hist Biol 5(2-4):239-255.</mixed-citation>
         </ref>
         <ref id="d195e738a1310">
            <label>28</label>
            <mixed-citation id="d195e745" publication-type="other">
McElwain JC, Punyasena SW (2007) Mass extinction events and the plant fossil record.
Trends Ecol Evol 22(10):548-557.</mixed-citation>
         </ref>
         <ref id="d195e756a1310">
            <label>29</label>
            <mixed-citation id="d195e763" publication-type="other">
Shen S-Z, et al. (2011) Calibrating the end-Permian mass extinction. Science 334(6061):
1367-1372.</mixed-citation>
         </ref>
         <ref id="d195e773a1310">
            <label>30</label>
            <mixed-citation id="d195e780" publication-type="other">
Roopnarine PD, Angielczyk KD, Wang SC, Hertog R (2007) Trophic network models
explain instability of Early Triassic terrestrial communities. Proc Biol Sci 274(1622):
2077-2086.</mixed-citation>
         </ref>
         <ref id="d195e793a1310">
            <label>31</label>
            <mixed-citation id="d195e800" publication-type="other">
Chen Z-Q, Benton MJ (2012) The timing and pattern of biotic recovery following the
end-Permian mass extinction. Nat Geosci 5:375-383.</mixed-citation>
         </ref>
         <ref id="d195e810a1310">
            <label>32</label>
            <mixed-citation id="d195e817" publication-type="other">
Huttenlocker AK, Sidor CA, Smith RMH (2011) A new specimen of Promoschorhynchus
(Therapsida: Therocephalia: Akidnognathidae) from the Early Triassic of South Africa
and its implications for theriodont survivorship across the Permian-Triassic boundary.
J Vertebr Paleontol 31(2):405-421.</mixed-citation>
         </ref>
         <ref id="d195e833a1310">
            <label>33</label>
            <mixed-citation id="d195e840" publication-type="other">
Fröbisch J, Angielczyk KD, Sidor CA (2010) The Triassic dicynodont Kombuisia (Syn-
apsida, Anomodontia) from Antarctica, a refuge from the terrestrial Permian-Triassic
mass extinction. Naturwissenschaňen 97(2):187-196.</mixed-citation>
         </ref>
         <ref id="d195e853a1310">
            <label>34</label>
            <mixed-citation id="d195e860" publication-type="other">
Hammer WR (1990) Triassic terrestrial vertebrate faunas of Antarctica. Antarctic
Paleobiology: Its role in the Reconstruction of Gondwana, eds Taylor TN, Taylor EL
(Springer, New York), pp 42-50.</mixed-citation>
         </ref>
         <ref id="d195e874a1310">
            <label>35</label>
            <mixed-citation id="d195e881" publication-type="other">
Roopnarine PD, Angielczyk KD (2012) The evolutionary palaeoecology of species and
the tragedy of the commons. Biol Lett 8(1):147-150.</mixed-citation>
         </ref>
         <ref id="d195e891a1310">
            <label>36</label>
            <mixed-citation id="d195e898" publication-type="other">
Nesbitt SJ, Barrett PM, Werning S, Sidor CA, Charig AJ (2013) The oldest dinosaur? A
Middle Triassic dinosauriform from Tanzania. Biol Lett 9(1):20120949.</mixed-citation>
         </ref>
         <ref id="d195e908a1310">
            <label>37</label>
            <mixed-citation id="d195e915" publication-type="other">
Brusatte SL, Niedžwiedzki G, Butler RJ (2011) Footprints pull origin and diversification
of dinosaur stem lineage deep into Early Triassic. Proc Biol Sci 278(1708):1107-1113.</mixed-citation>
         </ref>
         <ref id="d195e925a1310">
            <label>38</label>
            <mixed-citation id="d195e932" publication-type="other">
Butler RJ, et al. (2011) The sail-backed reptile Ctenosauriscus from the latest Early
Triassic of Germany and the timing and biogeography of the early archosaur radia-
tion. PLoS ONE 6(10):e25693.</mixed-citation>
         </ref>
         <ref id="d195e945a1310">
            <label>39</label>
            <mixed-citation id="d195e952" publication-type="other">
Nesbitt SJ (2011) The early evolution of archosaurs: Relationships and the origin of
major clades. Bull Am Mus Nat Hist 352:1-292.</mixed-citation>
         </ref>
         <ref id="d195e962a1310">
            <label>40</label>
            <mixed-citation id="d195e969" publication-type="other">
Lawver LA, Dalziel IWD, Norton IO, Ganagan LM (2009) The PLATES 2009 atlas of
plate reconstructions (750 Ma to present day), PLATES Progress Report No. 325-0509.
Univ of Texas Technical Report 196:1-32.</mixed-citation>
         </ref>
         <ref id="d195e983a1310">
            <label>41</label>
            <mixed-citation id="d195e990" publication-type="other">
Rubidge BS, Erwin DH, Ramezani J, Bowring SA, de Klerk WJ (2013) High-precision
temporal calibration of Late Permian vertebrate biostratigraphy: U-Pb zircon con-
straints from the Karoo Supergroup, South Africa. Geology, 10.1130/G33622.1.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
