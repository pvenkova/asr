<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">sociofocus</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50001093</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Sociological Focus</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>North Central Sociological Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00380237</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">21621128</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20831212</article-id>
         <title-group>
            <article-title>Industrialization and the Decline of the Aged</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>JAMES J.</given-names>
                  <surname>DOWD</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>1981</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">14</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20831210</issue-id>
         <fpage>255</fpage>
         <lpage>269</lpage>
         <permissions>
            <copyright-statement>© 1981 The North Central Sociological Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20831212"/>
         <abstract>
            <p>This paper reexamines the proposition that old people lose prestige with societal modernization. I argue that while the basic notion of a decline in the relative position of old people with industrialization is supportable the focus on industrialization or modernization is restrictive and misleading. It is the interaction between historically variable exchange rates and individual possession of valued resources that determines the ability of an age-group member to participate in satisfying and profitable social exchange. The differential allocation of valued resources across age strata establishes an age-relevent exchange rate for that particular period. The rate favors the aged in agrarian societies because of their control over land and disfavors the superannuated in industrial societies because of their exclusion from labor markets. As a result, old people in modern society incur greater costs to achieve comparable levels of reward (prestige and privilege) as their age-mates in non-modern societies.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>FOOTNOTES</title>
         <ref id="d568e114a1310">
            <label>1</label>
            <mixed-citation id="d568e121" publication-type="other">
J-curve (1976)</mixed-citation>
         </ref>
         <ref id="d568e128a1310">
            <label>3</label>
            <mixed-citation id="d568e135" publication-type="other">
Simmons (1945)</mixed-citation>
            <mixed-citation id="d568e141" publication-type="other">
Hallpike (1972: 46)</mixed-citation>
            <mixed-citation id="d568e147" publication-type="other">
Gulliver, 1963</mixed-citation>
            <mixed-citation id="d568e154" publication-type="other">
Dyson-Hudson, 1966</mixed-citation>
         </ref>
         <ref id="d568e161a1310">
            <label>4</label>
            <mixed-citation id="d568e168" publication-type="other">
Wilensky, 1975</mixed-citation>
         </ref>
         <ref id="d568e175a1310">
            <label>5</label>
            <mixed-citation id="d568e182" publication-type="other">
Wallerstein (1974)</mixed-citation>
            <mixed-citation id="d568e188" publication-type="other">
Chase-Dunn, 1978:174</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d568e204a1310">
            <mixed-citation id="d568e208" publication-type="other">
Abrahamson, Mark, Ephraim H. Mizruchi and
Carlton A. Hornung
1976 Stratification and Mobility. New York:
Macmillan.</mixed-citation>
         </ref>
         <ref id="d568e224a1310">
            <mixed-citation id="d568e228" publication-type="other">
Baxter, P. T. W. and Vri Almagor
1978 Age, Generation, and Time: Some Features
of East African Age Organizations. New*
York: St. Martin's.</mixed-citation>
         </ref>
         <ref id="d568e244a1310">
            <mixed-citation id="d568e248" publication-type="other">
Beauvoir, Simone de
1972 The Coming of Age (trans. Patrick
O'Brian). New York: G. P. Putman's Sons.</mixed-citation>
         </ref>
         <ref id="d568e261a1310">
            <mixed-citation id="d568e265" publication-type="other">
Beck, E. M. Patrick M. Horan, and Charles M.
Tolbert II
1978 "Stratification in a dual economy. A
sectoral model of earnings determination."
American Sociological Review 43: 704-20.</mixed-citation>
         </ref>
         <ref id="d568e285a1310">
            <mixed-citation id="d568e289" publication-type="other">
Bell, Daniel
1973 The Coming of Post-Industrial Society.
New York: Basic Books.</mixed-citation>
         </ref>
         <ref id="d568e302a1310">
            <mixed-citation id="d568e306" publication-type="other">
Chase-Dunn, Christopher
1978 "Core-periphery relations: The effects of
core competition." Pp. 159-176 in Barbara
H. Kaplan, ed., Social Change in the
Capitalist World Economy. Beverly Hills:
Sage.</mixed-citation>
         </ref>
         <ref id="d568e329a1310">
            <mixed-citation id="d568e333" publication-type="other">
demente, Frank and Gene F. Summers
1973 " Industrial development and the elderly: A
longitudinal analysis." Journal of
Gerontology 28: 479-83.</mixed-citation>
         </ref>
         <ref id="d568e349a1310">
            <mixed-citation id="d568e353" publication-type="other">
Colson, Elizabeth
1958 Marriage and the Family Among the
Plateau Tonga of Northern Rhodesia.
Manchester: Manchester University Press.</mixed-citation>
         </ref>
         <ref id="d568e369a1310">
            <mixed-citation id="d568e373" publication-type="other">
Cowgill, Donald O.
1974 "Aging and modernization: A revision of
the theory." Pp. 123-146 in J. F. Gubrium,
ed., Late Life: Communities and
Environmental Policy. Springfield, 111:
Charles C. Thomas.</mixed-citation>
         </ref>
         <ref id="d568e396a1310">
            <mixed-citation id="d568e400" publication-type="other">
Cowgill, Donald and Lowell D. Holmes
1972 Aging and Modernization. New York:
Appleton-Century-Crofts.</mixed-citation>
         </ref>
         <ref id="d568e414a1310">
            <mixed-citation id="d568e418" publication-type="other">
Douglas, Mary
1954 African Worlds: Studies in the
Cosmological Ideas and Social Values of
African Peoples. New York: Oxford
University.</mixed-citation>
         </ref>
         <ref id="d568e437a1310">
            <mixed-citation id="d568e441" publication-type="other">
1963 The Lele of the Kasai. London: Oxford
University.</mixed-citation>
         </ref>
         <ref id="d568e451a1310">
            <mixed-citation id="d568e455" publication-type="other">
Dowd, James J.
1981 "Age and inequality: A critique of the age
stratification model." Human
Development 24: 157-71.</mixed-citation>
         </ref>
         <ref id="d568e471a1310">
            <mixed-citation id="d568e475" publication-type="other">
1980 Stratification Among the Aged. Monterey:
Brooks/Cole.</mixed-citation>
         </ref>
         <ref id="d568e485a1310">
            <mixed-citation id="d568e489" publication-type="other">
Dyson-Hudson, Neville
1963 "The Karimojong age systems."
Ethnology 2: 353-401.</mixed-citation>
         </ref>
         <ref id="d568e502a1310">
            <mixed-citation id="d568e506" publication-type="other">
1966 Karimojong Politics. Oxford: Clarendon.</mixed-citation>
         </ref>
         <ref id="d568e514a1310">
            <mixed-citation id="d568e518" publication-type="other">
Eisenstadt, S. N.
1956 From Generation to Generation: Age
Groups and Social Structure. Glencoe, 111.:
The Free Press.</mixed-citation>
         </ref>
         <ref id="d568e534a1310">
            <mixed-citation id="d568e538" publication-type="other">
Fischer, David Hackett
1977 Growing Old in America. New York:
Oxford.</mixed-citation>
         </ref>
         <ref id="d568e551a1310">
            <mixed-citation id="d568e555" publication-type="other">
Foner, Anne
1974 "Age stratification and age conflict in
political life." American Sociological
Review 39: 187-96.</mixed-citation>
         </ref>
         <ref id="d568e571a1310">
            <mixed-citation id="d568e575" publication-type="other">
Foner, Anne and David Kertzer
1978 "Transitions over the life course: Lessons
from age-set societies." American Journal
of Sociology 83: 1081-1104.</mixed-citation>
         </ref>
         <ref id="d568e591a1310">
            <mixed-citation id="d568e595" publication-type="other">
Gulliver, P. H.
1955 The Family Herds: A Study of Two
Pastoral Peoples in East Africa, the Jie
and Turkana. New York: Humanities
Press.</mixed-citation>
         </ref>
         <ref id="d568e614a1310">
            <mixed-citation id="d568e618" publication-type="other">
1963 Social Control in an African Society.
Boston: Boston University.</mixed-citation>
         </ref>
         <ref id="d568e629a1310">
            <mixed-citation id="d568e633" publication-type="other">
1974 "The Jie of Uganda." Pp. 323-345 in Y. A.
Cohen, ed., Man in Adaptation: The
Cultural Present (2nd ed.). Chicago: Aldine.</mixed-citation>
         </ref>
         <ref id="d568e646a1310">
            <mixed-citation id="d568e650" publication-type="other">
Hallpike, C. R.
1972 The Konso of Ethiopia. London: Oxford,
Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d568e663a1310">
            <mixed-citation id="d568e667" publication-type="other">
Hamer, John H.
1972 "Aging in a gerontocratie society: The
Sidamo of southwest Ethiopia." Pp. 15-30
in D. Cowgill and L. Holmes, eds., Aging
and Modernization. New York: Appleton-
Century-Crofts.</mixed-citation>
         </ref>
         <ref id="d568e690a1310">
            <mixed-citation id="d568e694" publication-type="other">
Harlan, W. H.
1964 "Social status of the aged in three Indian
villages." Vita Humanae 7: 239-52.</mixed-citation>
         </ref>
         <ref id="d568e707a1310">
            <mixed-citation id="d568e711" publication-type="other">
Harris, Marvin
1979 Cultural Materialism. New York: Random
House.</mixed-citation>
         </ref>
         <ref id="d568e724a1310">
            <mixed-citation id="d568e728" publication-type="other">
Hempel, Carl G.
1968 "The logic of functional analysis." Pp.
179-210 in M. Brodbeck, ed., Readings in
the Philosophy of the Social Sciences. New
York: Macmillan.</mixed-citation>
         </ref>
         <ref id="d568e748a1310">
            <mixed-citation id="d568e752" publication-type="other">
Henretta, John C. and Richard T. Campbell
1978 "Net worth as an aspect of status."
American Jouranl of Sociology 83:
1204-23.</mixed-citation>
         </ref>
         <ref id="d568e768a1310">
            <mixed-citation id="d568e772" publication-type="other">
Inkeles, Alex
1973 "A model of modern man: Theoretical and
methodological issues." In N. Hammond,
ed., Social Science and the New Societies:
Problems in Crosscultural Research and
Theory Building. Lansing: Social Science
Research Bureau, Michigan State
University.</mixed-citation>
         </ref>
         <ref id="d568e801a1310">
            <mixed-citation id="d568e805" publication-type="other">
Kottak, Conrad P.
1978 Anthropology: The Exploration of Human
Diversity (2nd ed.). New York: Random
House.</mixed-citation>
         </ref>
         <ref id="d568e821a1310">
            <mixed-citation id="d568e825" publication-type="other">
Lamphear, John
1976 The Traditional History of the Jie of
Uganda. Oxford: Clarendon.</mixed-citation>
         </ref>
         <ref id="d568e838a1310">
            <mixed-citation id="d568e842" publication-type="other">
Laslett, Peter
1976 "Societal development and aging." Pp.
87-116 in R. Binstock and E. Shanas, eds.,
Handbook of Aging and the Social
Sciences. New York: Van Nostrand
Reinhold.</mixed-citation>
         </ref>
         <ref id="d568e865a1310">
            <mixed-citation id="d568e869" publication-type="other">
Lee, Richard B.
1974 "What hunters do for a living, or how to
make out on scarce resources." Pp. 87-100
in Y. Cohen, ed., Man in Adaptation: The
Cultural Present. Chicago: Aldine.</mixed-citation>
         </ref>
         <ref id="d568e889a1310">
            <mixed-citation id="d568e893" publication-type="other">
1979 The ! Kung San: Men, Women, and Work in
a Foraging Society. Cambridge:
Cambridge University.</mixed-citation>
         </ref>
         <ref id="d568e906a1310">
            <mixed-citation id="d568e910" publication-type="other">
Lipman, A.
1970 " Prestige of the aged in Portugal: Realistic
appraisal of ritualistic deference." Aging
and Human Development 1: 127-36.</mixed-citation>
         </ref>
         <ref id="d568e926a1310">
            <mixed-citation id="d568e930" publication-type="other">
Marshall, Victor W.
1979 "No exit: A symbolic interactionist
perspective on aging." International
Journal of Aging and Human
Development 9: 345-58.</mixed-citation>
         </ref>
         <ref id="d568e949a1310">
            <mixed-citation id="d568e953" publication-type="other">
Marx, Karl
1970 A Contribution of the Critique of Political
[1859] Economy. New York: International
Publishers.</mixed-citation>
         </ref>
         <ref id="d568e969a1310">
            <mixed-citation id="d568e973" publication-type="other">
Murdock, G. P.
1967 Ethnographic Atlas. Pittsburgh:
University of Pittsburgh.</mixed-citation>
         </ref>
         <ref id="d568e986a1310">
            <mixed-citation id="d568e990" publication-type="other">
Neugarten, Bernice L. and Gunhild O. Hagestad
1976 "Age and the life course." Pp. 35-55 in H.
Binstock and E. Shanes, eds., Handbook of
Aging and the Social Sciences. New York:
Van Nostrand Reinhold.</mixed-citation>
         </ref>
         <ref id="d568e1010a1310">
            <mixed-citation id="d568e1014" publication-type="other">
Ng, Wing-Cheung
1977 "The dual labor market theory: An
evaluation of its status in the field of
ethnic studies." Paper presented at the
Annual Meetings of the American
Sociological Association, Chicago.</mixed-citation>
         </ref>
         <ref id="d568e1037a1310">
            <mixed-citation id="d568e1041" publication-type="other">
Palmore, Erdman
1976 "The future status of the aged." The
Gerontologist 17: 297-302.</mixed-citation>
         </ref>
         <ref id="d568e1054a1310">
            <mixed-citation id="d568e1058" publication-type="other">
Palmore, Erdman B. and Kenneth Manton
1974 "Modernization and status of the aged:
International correlations." Journal of
Gerontology 29: 205-10.</mixed-citation>
         </ref>
         <ref id="d568e1074a1310">
            <mixed-citation id="d568e1078" publication-type="other">
Palmore, Erdman and Frank Whittington
1971 "Trends in the relative status of the aged."
Social Forces 50: 84-91.</mixed-citation>
         </ref>
         <ref id="d568e1091a1310">
            <mixed-citation id="d568e1095" publication-type="other">
Prins, A. H. J.
1970 East African Age-Class Systems.
[1953] Westport, Conn.: Negro Universities
Press.</mixed-citation>
         </ref>
         <ref id="d568e1111a1310">
            <mixed-citation id="d568e1115" publication-type="other">
Riley, Matilda W.
1976 "Age strata in social systems." Pp.
189-217 in R. Binstock and E. Shanas
(eds.) Handbook of Aging and the Social
Sciences. New York: Van Nostrand
Rinehold.</mixed-citation>
         </ref>
         <ref id="d568e1139a1310">
            <mixed-citation id="d568e1143" publication-type="other">
Rosow, Irving
1964 "And then we were old." Trans-Action 2:
22-5.</mixed-citation>
         </ref>
         <ref id="d568e1156a1310">
            <mixed-citation id="d568e1160" publication-type="other">
Sangree Walter H.
1966 Age, Prayer and Politics in Tiriki, Kenya.
London: Oxford University.</mixed-citation>
         </ref>
         <ref id="d568e1173a1310">
            <mixed-citation id="d568e1177" publication-type="other">
Shelton, Austin J.
1972 "The aged and eldership among the Igbo."
Pp. 31-49 in D. Cowgill and L. Holmes,
eds., Aging and Modernization. New York:
Appleton-Century-Crofts.</mixed-citation>
         </ref>
         <ref id="d568e1196a1310">
            <mixed-citation id="d568e1200" publication-type="other">
Simmons, Leo W.
1945 The Role of the Aged in Primitive Society.
New Haven: Yale University.</mixed-citation>
         </ref>
         <ref id="d568e1213a1310">
            <mixed-citation id="d568e1217" publication-type="other">
1959 "Aging in modern society." In Toward
Better Understanding of the Aging. New
York: Council on Social Work Education.</mixed-citation>
         </ref>
         <ref id="d568e1230a1310">
            <mixed-citation id="d568e1234" publication-type="other">
1960 "Aging in preindustrial societies." Pp.
62-91 in C. Tibbets, ed., Handbook of
Social Gerontology. Chicago: University
of Chicago.</mixed-citation>
         </ref>
         <ref id="d568e1251a1310">
            <mixed-citation id="d568e1255" publication-type="other">
Spencer, Paul
1965 The Samburu: A Study of Gerontology in a
Nomadic Tribe. Berkeley: University of
California.</mixed-citation>
         </ref>
         <ref id="d568e1271a1310">
            <mixed-citation id="d568e1275" publication-type="other">
Stewart, Frank H.
1977 Fundamentals of Age-Group Systems.
New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="d568e1288a1310">
            <mixed-citation id="d568e1292" publication-type="other">
Stolzenberg, Ross M.
1975a "Education, occupation, and wage
differences between White and Black
Men." American Journal of Sociology 81:
299-323.</mixed-citation>
         </ref>
         <ref id="d568e1311a1310">
            <mixed-citation id="d568e1315" publication-type="other">
1975b "Occupations, labor markets, and the
process of wage attainment." American
Sociological Review 40: 645-65.</mixed-citation>
         </ref>
         <ref id="d568e1328a1310">
            <mixed-citation id="d568e1332" publication-type="other">
Tolbert, Charles, Patrick M. Horan and Elwood Beck
1980 "The structure of economic segmentation:
A dual economy approach." American
Journal of Sociology 85: 1095-1116.</mixed-citation>
         </ref>
         <ref id="d568e1348a1310">
            <mixed-citation id="d568e1352" publication-type="other">
Turnbull, Colin M.
1962 The Forest People. New York: Simon and
Schuster.</mixed-citation>
         </ref>
         <ref id="d568e1366a1310">
            <mixed-citation id="d568e1370" publication-type="other">
Wallerstein, Immanuel
1974 The Modern World System, Vol. I:
Capitalist Agricaulture and the Origins of
the European World Economy in the
Sixteenth Century. New York: Academic
Press.</mixed-citation>
         </ref>
         <ref id="d568e1393a1310">
            <mixed-citation id="d568e1397" publication-type="other">
Wilensky, Harold L.
1975 The Welfare State and Equality. Berkeley:
University of California.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
