<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt12606d</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/dine08450</book-id>
      <subj-group>
         <subject content-type="call-number">QL737.U63 D56 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Indian rhinoceros</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Endangered species</subject>
         <subj-group>
            <subject content-type="lcsh">Asia</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Wildlife conservation</subject>
         <subj-group>
            <subject content-type="lcsh">Nepal</subject>
            <subj-group>
               <subject content-type="lcsh">Royal Chitwan National Park</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Environmental Science</subject>
         <subject>Biological Sciences</subject>
         <subject>Ecology &amp; Evolutionary Biology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Return of the Unicorns</book-title>
         <subtitle>Natural History and Conservation of the Greater-One Horned Rhinoceros</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Dinerstein</surname>
               <given-names>Eric</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>02</day>
         <month>07</month>
         <year>2003</year>
      </pub-date>
      <isbn content-type="epub">9780231501309</isbn>
      <isbn content-type="epub">0231501307</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2003</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/dine08450"/>
      <abstract abstract-type="short">
         <p>Since 1984, Eric Dinerstein has led the team directly responsible for the recovery of the greater one-horned rhinoceros in the Royal Chitwan National Park in Nepal, where the population had once declined to as few as 100 rhinos. The Return of the Unicorns is an account of what it takes to save endangered large mammals. Dinerstein outlines the multifaceted recovery program -- structured around targeted fieldwork and scientific research, effective protective measures, habitat planning and management, public-awareness campaigns, economic incentives to promote local guardianship, and bold, uncompromising leadership -- that brought these extraordinary animals back from the brink of extinction. In an age when scientists must also become politicians, educators, fund-raisers, and activists in order to safeguard the subjects they study, Dinerstein's inspiring story offers a successful model for large-mammal conservation applicable throughout Asia and across the globe.</p>
      </abstract>
      <counts>
         <page-count count="384"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.3</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Schaller</surname>
                           <given-names>George B.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract abstract-type="extract">
                     <p>AS I SAT ON THE ELEPHANT, I looked past its bristly skull through a bower of grass arching over our trail in Chitwan. The only sound was the swish of grass against the animal’s body as it glided along. We emerged abruptly into a clearing where a gray hulking creature with skin folds like armor plate faced us, its nostrils flared, its ears twitching. The curved horn on its nose looked menacing. We halted. The scene was anachronistic, antediluvian, two giants of the earth meeting as they have for eons. It was 1972, but I felt adrift in time, a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.4</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>ON A MISTY FEBRUARY MORNING in 1986, I sat patiently in the fork of a kadam tree in Royal Bardia National Park in southwestern Nepal. The forest echoed with the raucous calls of peacocks and wild jungle fowl, but my attention was fixed on two enormous crates at the edge of the grassland. Inside the crates were two greater one-horned rhinoceros, a pair captured a day earlier in Royal Chitwan National Park, Nepal, 250 km to the east. I was waiting to photograph their release, marking the end of two years of intensive research and planning that had culminated in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.6</book-part-id>
                  <title-group>
                     <title>[PART I Introduction]</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract abstract-type="extract">
                     <p>TWENTY-FIVE MILLION YEARS AGO, on a warm, calm day in the Oligocene, a male giraffe-rhinoceros (<italic>Paraceratherium grangeri</italic>) ambled through a shaded grove at the base of a low range of hills in South Asia, clipping the leaves of tree branches with his massive teeth. Twenty feet long and as tall as a double-decker bus, the giraffe-rhinoceros was the largest land mammal to tread the planet, dwarfing even prehistoric elephants and mammoths. Nearby, a female giraffe-rhinoceros and her two-year-old calf nibbled on some low-hanging foliage. The mother stopped to listen, alert for giant hyena-like predators that might attack her offspring. Soon</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>VANISHING MAMMALS:</title>
                     <subtitle>THE RISE AND FALL OF THE RHINOCEROS</subtitle>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract abstract-type="extract">
                     <p>MY FIRST VIEW OF A GREATER one-horned rhinoceros was at the age of seven in the Hall of Asian Mammals at the American Museum of Natural History in New York. My classmates and I, a group of boisterous fourth-graders from the Toms River, New Jersey, Elementary School, had just finished writing and illustrating a book on the ecology of dinosaurs. We planned to seek critical review of our manuscript from the museum’s own Dr. Roy Chapman Andrews, the dean of American paleontologists, and our hero. After wandering through the dinosaur exhibits all morning, I broke off from the group to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>CULTURE, CONSERVATION, AND THE DEMAND FOR RHINOCEROS HORN</title>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract abstract-type="extract">
                     <p>A CONVENTION OF INDIAN GRIFFON VULTURES perched in the treetops at the edge of the Rapti River in Chitwan National Park, Nepal. Clearly, they were hoping for a feast of rhinoceros, but they would have no such luck this time. For the moment, I was first in the pecking order, allowed by park officials to measure the dead male rhinoceros—which we had identified as M039—that was sprawled on the floodplain. Meanwhile, they conducted the official inquest. This animal had died from wounds suffered in a fight with the resident male. Thankfully, the horn was still attached when an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>VANISHING LANDSCAPES:</title>
                     <subtitle>THE FLOODPLAIN ECOSYSTEM OF THE TERAI</subtitle>
                  </title-group>
                  <fpage>34</fpage>
                  <abstract abstract-type="extract">
                     <p>THE THUNDER OF THE RIVER woke me from a fitful sleep. Torrents of rain had engulfed the Chitwan Valley for three solid days, and now in September, the last month of the monsoon, the earth could hold no more. I rushed down to the banks of the Rapti River and joined villagers who stood gaping at the brown floodwaters surging toward India that were sweeping along uprooted trees as if they were toothpicks. The scene was almost biblical or, more appropriately, something from the Hindu scriptures, in the display of the raw power of nature. Yet this scene plays out</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.10</book-part-id>
                  <title-group>
                     <title>[PART II Introduction]</title>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract abstract-type="extract">
                     <p>THE EMERGING SCIENCE OF conservation biology—the science of scarcity and diversity—has created a framework for assessing the demographic, genetic, and environmental risks facing small isolated populations of endangered species. These, along with landscape-scale issues such as habitat fragmentation, reserve size, core areas, connectivity of habitats, and maintenance of ecological processes, constitute the substance of research in this emerging field of science. Part II introduces some of these central issues of conservation biology as they apply to rhinoceros and examines the evidence that rhinoceros are more prone to extinction than are smaller species of herbivorous mammals.</p>
                     <p>Seven basic questions</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>SIZE AND SEXUAL DIMORPHISM IN GREATER ONE-HORNED RHINOCEROS</title>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract abstract-type="extract">
                     <p>WE DARTED OUR FIRST GREATER one-horned rhino, a large male, in the winter of 1985. Within a few minutes, my colleagues and I saw him ease to the ground as the powerful morphine derivative worked its magic. We descended from our elephants to measure this magnificent animal and to attach a radio collar. Like Lilliputians next to Gulliver, we stared in wonder at the behemoth that lay before us (figure 4.1). Our hearts stopped suddenly as the supposedly sedated rhino stood up and rumbled off into the forest. Our textbook on animal capture omitted mention of this behavior. As the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>THE BIOLOGY OF AN EXTINCTION-PRONE SPECIES:</title>
                     <subtitle>FACING DEMOGRAPHIC, GENETIC, AND ENVIRONMENTAL THREATS</subtitle>
                  </title-group>
                  <fpage>81</fpage>
                  <abstract abstract-type="extract">
                     <p>ON A HOT SPRING AFTERNOON in late March, I was riding elephant-back to a grassland known as Pipariya in the eastern part of Chitwan. With me were three Malaysian biologists who had devoted six years to studying and capturing the elusive Sumatran rhinoceros. Despite extensive tracking, they had yet to see a single Sumatran rhino in the wild other than those that had been captured in pit traps. As we emerged from the forest, I pointed out the gray, boulderlike objects dotting the grassland. Fires had burned through two weeks earlier, and the abundance of new shoots had attracted a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>LIFE ON THE FLOODPLAIN:</title>
                     <subtitle>SPACING AND RANGING BEHAVIOR, FEEDING ECOLOGY, AND ACTIVITY PATTERNS</subtitle>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract abstract-type="extract">
                     <p>I SAW MY FIRST LIVING greater one-horned rhinoceros in the company of three other Peace Corps volunteers on a sweltering afternoon in May 1975. We had been sent to Chitwan as part of our training program but with scant introduction to the wildlife of the park or warnings about the dangers of walking in the jungle. Naively, we tried to get a close-up photograph of a male rhino immersed in a wallow. The male charged without warning, chasing us into the nearest climbable tree. For the next few minutes, while the male snorted and circled menacingly below us, I had</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>MALE DOMINANCE, REPRODUCTIVE SUCCESS, AND THE “INCISOR-SIZE HYPOTHESIS”</title>
                  </title-group>
                  <fpage>134</fpage>
                  <abstract abstract-type="extract">
                     <p>ON A STEAMY JULY EVENING during my first monsoon season in Chitwan, I had just sat down to dinner when I heard tremendous crashing sounds in the riverine forest next to my camp. Galloping across the lawn at full tilt were two male rhinos, one chasing the other. Ignoring us completely, the males thundered past the dining area and straight through a barbed-wire fence, snapping the strands like party ribbon. This unexpected commotion right in the middle of my camp lawn initiated my investigation into the link between male dominance and reproductive success. The relationship between these is fully relevant</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>ENDANGERED PHENOMENA:</title>
                     <subtitle>RHINOCEROS AS LANDSCAPE ARCHITECTS</subtitle>
                  </title-group>
                  <fpage>153</fpage>
                  <abstract abstract-type="extract">
                     <p>WHEN I FIRST ARRIVED IN Chitwan, I wondered how any herbivore could make a dent in the mass of green vegetation created by monsoon floods and rains. One day I asked an elephant driver why a common tree species, <italic>Trewia nudiflora</italic> (known locally as <italic>bhellur</italic>), grows in copses, like an archipelago of trees in the midst of a tall grassland (figure 8.1). “Oh, it’s the work of <italic>gaida,</italic>” he said, using the Nepali term for greater one-horned rhinoceros and gesturing toward the tree islands in the sea of grass. “Those are old rhino latrines.” Rhinoceros, like many other ungulates and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.16</book-part-id>
                  <title-group>
                     <title>[PART III Introduction]</title>
                  </title-group>
                  <fpage>179</fpage>
                  <abstract abstract-type="extract">
                     <p>A FORMER WARDEN IN CHITWAN recalls how angry villagers often confronted him during periodic meetings with local communities. They demanded greater access to the park to satisfy their need for firewood and grazing areas for their livestock. Eventually, they would always ask, “Who is more important, people or rhinoceros?” And he would reply, “There are only four hundred rhinoceros in Chitwan, and there are over eighty thousand human residents. I am sorry, but it is my duty to protect the rights of the minority.”</p>
                     <p>This was a courageous response, but the situation exemplifies the conundrum that exists in all communities</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.17</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>DOES PRIVATELY OWNED ECOTOURISM SUPPORT CONSERVATION OF CHARISMATIC LARGE MAMMALS?</title>
                  </title-group>
                  <fpage>181</fpage>
                  <abstract abstract-type="extract">
                     <p>I WAS SITTING ON THE BACK of my elephant, Mel Kali, watching a rhino graze a short distance away. Above us, a flock of black ibis was flying in tight formation as it returned to Chitwan from the south, the birds’ melancholy cries signaling the end of the monsoon. The rain clouds had dispersed, unveiling the summits of the towering Himalayan chain that were gleaming in a coat of fresh snow. Annapurna, Mount Dhaulagiri, Himalchuli—even the names of these great mountains are inspiring, not to mention their extraordinary heights. I experienced a profound sense of exhilaration that October day</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.18</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>MAKING ROOM FOR MEGAFAUNA:</title>
                     <subtitle>PROMOTING LOCAL GUARDIANSHIP OF ENDANGERED SPECIES AND LANDSCAPE-SCALE CONSERVATION</subtitle>
                  </title-group>
                  <fpage>192</fpage>
                  <abstract abstract-type="extract">
                     <p>IN THE SPRING OF 1988, I took one last walk along the Rapti River, the boundary between Chitwan National Park and the settlements that partly surround it. In two days I would be on a plane, heading back to the United States to finish my postdoctoral fellowship with the National Zoological Park. As I approached the village of Bagmara, I watched as a forlorn pair of emaciated cows crossed the Rapti and ambled toward home (figure 10.1). They had obviously spent the morning grazing inside the national park. The scene disturbed me. The livestock’s unchecked grazing of habitat set aside</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.19</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>THE RECOVERY OF RHINOCEROS AND OTHER LARGE ASIAN MAMMALS</title>
                  </title-group>
                  <fpage>226</fpage>
                  <abstract abstract-type="extract">
                     <p>THE PHANTOMLIKE JAVAN RHINOCEROS, perhaps the rarest large mammal on Earth (figure 11.1), lives in Ujung Kulon National Park on the island of Java; a population of fewer than ten individuals also hangs on in Vietnam. In 1989, I tried to photograph one of these elusive animals by placing an infrared camera trap along trails that the rhinos were thought to frequent in the Java park. In a few days, I had amassed intimate nocturnal portraits of rusa (deer), birds, and tree branches but no rhinoceros. Trudging around in the mud of Ujung Kulon in search of Javan rhinos rivaled</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.20</book-part-id>
                  <title-group>
                     <title>APPENDIX A:</title>
                     <subtitle>METHODS</subtitle>
                  </title-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.21</book-part-id>
                  <title-group>
                     <title>APPENDIX B:</title>
                     <subtitle>MEASUREMENTS AND OTHER PHYSICAL FEATURES OF GREATER ONE-HORNED RHINOCEROS CAPTURED IN ROYAL CHITWAN NATIONAL PARK</subtitle>
                  </title-group>
                  <fpage>271</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.22</book-part-id>
                  <title-group>
                     <title>APPENDIX C:</title>
                     <subtitle>DEMOGRAPHIC AND GENETIC DATA</subtitle>
                  </title-group>
                  <fpage>275</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.23</book-part-id>
                  <title-group>
                     <title>APPENDIX D:</title>
                     <subtitle>SEASONAL HOME RANGE AND DAILY MOVEMENTS</subtitle>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.24</book-part-id>
                  <title-group>
                     <title>APPENDIX E:</title>
                     <subtitle>A PROFILE OF RHINOCEROS BEHAVIOR</subtitle>
                  </title-group>
                  <fpage>283</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.25</book-part-id>
                  <title-group>
                     <title>APPENDIX F:</title>
                     <subtitle>REPRODUCTIVE HISTORIES OF ADULT FEMALE RHINOCEROS</subtitle>
                  </title-group>
                  <fpage>287</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.26</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>291</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/dine08450.27</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>303</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
