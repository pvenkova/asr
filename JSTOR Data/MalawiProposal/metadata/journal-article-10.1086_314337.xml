<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">jgeology</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Geology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221376</issn>
         <issn pub-type="epub">15375269</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor-stable">30079927</article-id>
         <article-id pub-id-type="doi">10.1086/314337</article-id>
         <article-id pub-id-type="msid">JG990010</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>Articles</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>U‐Pb Geochronology and Isotope Geochemistry of the Archean and Proterozoic Rocks of North‐Central Madagascar</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>R. D. </given-names>
                  <x xml:space="preserve"/>
                  <surname>Tucker</surname>
               </string-name>
               <x xml:space="preserve">,</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>L. D.</given-names>
                  <x xml:space="preserve"/>
                  <surname>Ashwal</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="fn" rid="fn1">1</xref>
               <x xml:space="preserve"/>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>M. J.</given-names>
                  <x xml:space="preserve"/>
                  <surname>Handke</surname>
               </string-name>
               <x xml:space="preserve">,</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>M. A.</given-names>
                  <x xml:space="preserve"/>
                  <surname>Hamilton</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="fn" rid="fn2">2</xref>
            </contrib>
         </contrib-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>M.</given-names>
                  <x xml:space="preserve"/>
                  <surname>Le Grange</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="fn" rid="fn1">1</xref>
               <x xml:space="preserve">and</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>R. A.</given-names>
                  <x xml:space="preserve"/>
                  <surname>Rambeloson</surname>
               </string-name>
               <xref ref-type="fn" rid="fn3">3</xref>
            </contrib>
            <aff id="aff_1">Department of Earth and Planetary Sciences, Washington University, 1 Brookings Hall, St. Louis, Missouri 63130‐4899, U.S.A.(e‐mail:<email xlink:href="mailto:tucker@zircon.wustl.edu" xlink:type="simple">tucker@zircon.wustl.edu</email>)</aff>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>03</month>
            <year>1999</year>
            <string-date>March 1999</string-date>
         </pub-date>
         <volume>107</volume>
         <issue>2</issue>
         <issue-id>jg.1999.107.issue-2</issue-id>
         <fpage>135</fpage>
         <lpage>153</lpage>
         <permissions>
            <copyright-statement>© 1999 by The University of Chicago. All rights reserved.</copyright-statement>
            <copyright-year>1999</copyright-year>
            <copyright-holder>The University of Chicago</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/314337"/>
         <abstract>
            <label>Abstract</label>
            <p>New U‐Pb zircon ages and Sm‐Nd and Rb‐Sr isotopic data are presented for orthogneisses from north‐central Madagascar, including Île Sainte Marie, Alaotra‐Beforona, Maevatanana, and Ambatolampy‐Ambatomarina. A migmatite tonalite gneiss from Île Sainte Marie is dated precisely at<inline-formula>
                  <tex-math notation="LaTeX">\documentclass{aastex} \usepackage{amsbsy} \usepackage{amsfonts} \usepackage{amssymb} \usepackage{bm} \usepackage{mathrsfs} \usepackage{pifont} \usepackage{stmaryrd} \usepackage{textcomp} \usepackage{portland,xspace} \usepackage{amsmath,amsxtra} \usepackage[OT2,OT1]{fontenc} \newcommand\cyr{ \renewcommand\rmdefault{wncyr} \renewcommand\sfdefault{wncyss} \renewcommand\encodingdefault{OT2} \normalfont \selectfont} \DeclareTextFontCommand{\textcyr}{\cyr} \pagestyle{empty} \DeclareMathSizes{10}{9}{7}{6} \begin{document} \landscape $3187\pm 2$ \end{document}</tex-math>
               </inline-formula>Ma and has a Sm‐Nd model age (<italic>T</italic>
               <sub>DM</sub>) of 3204 Ma, thereby establishing a Middle Archean age for the oldest, juvenile gneisses in northeast Madagascar. Dated orthogneisses, intrusive into the schist/paragneiss sequences, range in age between 2522 and 2494 Ma and have Sm‐Nd model ages (<italic>T</italic>
               <sub>DM</sub>) between 3207 Ma and 2541 Ma. These data establish a Late Archean or older age for two of the schist/paragneiss sequences of Madagascar and suggest that the cratonal regions of north‐central Madagascar and south India were once contiguous. Strontium and neodymium isotopic data from the Late Archean rocks are interpreted to reflect mixing between depleted mantle magmas and evolving Middle Archean crust. U‐Pb geochronology of other plutonic igneous rocks demonstrates that the Middle Neoproterozoic (800–640 Ma) represents an important period of igneous activity throughout north‐central Madagascar. In addition, a latest Neoproterozoic–Early Cambrian (580–520 Ma) period of high‐grade metamorphism and intrusive igneous activity is recorded in western and central parts of north Madagascar. We attribute this later activity to the effects of continental collision between East and West Gondwana.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="fn1">
               <label>1</label>
               <p>Department of Geology, Rand Afrikaans University, P.O. Box 524, Auckland Park, 2006, South Africa.</p>
            </fn>
            <fn id="fn2">
               <label>2</label>
               <p>Continental Geoscience Division, Geological Survey of Canada, 601 Booth Street, Ottawa K1A OE8, Canada.</p>
            </fn>
            <fn id="fn3">
               <label>3</label>
               <p>Laboratoire de Géologie, Campus Universitaire d'Ambohitsarina, BP 906, Antananarivo, Madagascar.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
   <back>
      <ack>
         <sec>
            <title>Acknowledgments</title>
            <p>The U‐Pb analytical work at Washington University was supported by National Science Foundation grants EAR‐9418538 and EAR‐9506693 to R. D. Tucker. L. D. Ashwal acknowledges the South African Foundation for Research and Development for financial support of his work in Madagascar. The article benefited from several readings and helpful suggestions by J. D. Morris, as well as a thoughtful review by R. J. Stern.</p>
         </sec>
      </ack>
      <ref-list content-type="unparsed">
         <title>References Cited</title>
         <ref id="rf1">
            <mixed-citation xlink:type="simple" publication-type="">Ackermand, D.; Windley, B. F.; and Razafiniparany, A. 1989. The Precambrian mobile belt of southern Madagascar.<italic>In</italic>Daly, J. S.; Cliff, R. A.; and Yardley, B. W. D., eds. Evolution of metamorphic belts. Geol. Soc. Spec. Publ., no. 14, p. 293–296.</mixed-citation>
         </ref>
         <ref id="rf2">
            <mixed-citation xlink:type="simple" publication-type="">Amelin, Y. V.; Larin, A. M.; and Tucker, R. D. 1997. Chronology of multiphase emplacement of the Salmi rapakivi granite‐anorthosite complex, Baltic Shield: implications for magmatic evolution. Contrib. Mineral. Petrol. 127:353–368.</mixed-citation>
         </ref>
         <ref id="rf3">
            <mixed-citation xlink:type="simple" publication-type="">Ashwal, L. D., ed. 1997. Guidebook to field excursions. UNESCO‐IUGS‐IGCP 348/368, International Field Workshop on Proterozoic Geology of Madagascar, Gondwana Research Group Miscellaneous Publication no. 6, 53 p.</mixed-citation>
         </ref>
         <ref id="rf4">
            <mixed-citation xlink:type="simple" publication-type="">Ashwal, L. D., and Tucker, R. D. 1997. Archean to Neoproterozoic events in Madagascar: implications for the assembly of Gondwana. Terra Nova 9(abstr. suppl.):163–164.</mixed-citation>
         </ref>
         <ref id="rf6">
            <mixed-citation xlink:type="simple" publication-type="">Bazot, G.; Bousteyak, L.; Hottin, G.; and Razafiniparany, A. H. 1971. Carte du metamorphisme de Madagascar. Doc. Bur. Madagascar, no. 183.</mixed-citation>
         </ref>
         <ref id="rf7">
            <mixed-citation xlink:type="simple" publication-type="">Beckinsale, R. D.; Drury, S. A.; and Holt, R. W. 1980. 3360‐Myr old gneisses from the South Indian Craton. Nature 283:469–470.</mixed-citation>
         </ref>
         <ref id="rf8">
            <mixed-citation xlink:type="simple" publication-type="">Beckinsale, R. D.; Reeves‐Smith, G.; Gale, N. H.; Holt, R. W.; and Thompson, B. 1982. Rb‐Sr and Pb‐Pb isochron ages and REE data for Archaean gneisses and granites, Karnataka state, South India.<italic>In</italic>Indo‐U.S. Workshop on the Precambrian of South India: abstracts of papers. National Geophysical Institute, Hyderabad, p. 35–36.</mixed-citation>
         </ref>
         <ref id="rf9">
            <mixed-citation xlink:type="simple" publication-type="">Besairie, H. 1964. Carte géologique de Madagascar, Service Géologique de Madagascar, Antananarivo, scale, 1 : 1,000,000 (color).</mixed-citation>
         </ref>
         <ref id="rf10">
            <mixed-citation xlink:type="simple" publication-type="">———. 1973. Précis de géologie Malgache. Annal. Géol. Madagascar 36:93–113.</mixed-citation>
         </ref>
         <ref id="rf11">
            <mixed-citation xlink:type="simple" publication-type="">Bhaskar Rao, Y. J.; Sivaraman, T. V.; Pantulu, G. V. C.; Gopalan, K.; and Naqvi, S. M. 1992. Rb‐Sr ages of Late Archean metavolcanics and granites, Dharwar craton, South India and evidence for Early Proterozoic thermotectonic event(s). Precambrian Res. 59:145–170.</mixed-citation>
         </ref>
         <ref id="rf12">
            <mixed-citation xlink:type="simple" publication-type="">Choudhary, A. K.; Harris, N. B. W.; van Calsteren, P.; and Hawkesworth, C. J. 1992. Pan‐African charnockite formation in Kerala, South India. Geol. Mag. 129:257–264.</mixed-citation>
         </ref>
         <ref id="rf13">
            <mixed-citation xlink:type="simple" publication-type="">Condie, K. C., ed. 1994. Archean crustal evolution. Elsevier, Amsterdam, 528 p.</mixed-citation>
         </ref>
         <ref id="rf14">
            <mixed-citation xlink:type="simple" publication-type="">Cox, R.; Armstrong, R. A.; Ashwal, L. D.; and de Wit, M. J. 1995. Sedimentology, tectonics, and geochronology of Proterozoic shelf sediments of the Itremo Group, central Madagascar: implications for the assembly of East Gondwana. Geol. Soc. Am. Abstr. Prog., 27, no. 6, A‐161.</mixed-citation>
         </ref>
         <ref id="rf15">
            <mixed-citation xlink:type="simple" publication-type="">Dalziel, I. W. D. 1997. Neoproterozoic‐Paleozoic geography and tectonics: review, hypothesis, environmental speculation. Geol. Soc. Am. Bull. 109:16–42.</mixed-citation>
         </ref>
         <ref id="rf16">
            <mixed-citation xlink:type="simple" publication-type="">Davis, D. W. 1982. Optimum linear regression and error estimation applied to U‐Pb data. Can. J. Earth Sci. 19:2141–2149.</mixed-citation>
         </ref>
         <ref id="rf17">
            <mixed-citation xlink:type="simple" publication-type="">Delbos, L.; Giraudon, R.; and Rantoanina, M. 1962. Geological map or the Moramanga‐Brickaville area. Service Géologique de Madagascar, Tananarive, scale, 1 : 200,000 (color).</mixed-citation>
         </ref>
         <ref id="rf18">
            <mixed-citation xlink:type="simple" publication-type="">DePaolo, D. J. 1981. Neodymium isotopes in the Colorado Front Range and crust‐mantle evolution in the Proterozoic. Nature 291:193–196.</mixed-citation>
         </ref>
         <ref id="rf19">
            <mixed-citation xlink:type="simple" publication-type="">de Wit, M. J., and Ashwal, L. D. 1995. Greenstone belts: what are they? S. Afr. J. Geol. 98:505–520.</mixed-citation>
         </ref>
         <ref id="rf21">
            <mixed-citation xlink:type="simple" publication-type="">Dudan, R.; Boulanger, J.; Bertucat, M.; and Clair, G. 1959. Geologic map of the lac Alaotra area. Service Géologique de Madagascar, Tanarive, scale, 1 : 200,000 (color).</mixed-citation>
         </ref>
         <ref id="rf22">
            <mixed-citation xlink:type="simple" publication-type="">Friend, C. R. L., and Nutman, A. P. 1991. SHRIMP U‐Pb geochronology of the Closepet granite and Peninsular gneiss, Karnataka, South India. J. Geol. Soc. India 38:357–368.</mixed-citation>
         </ref>
         <ref id="rf23">
            <mixed-citation xlink:type="simple" publication-type="">Guerrot, C.; Codherie, A.; and Ohnenstetter, N. 1993. Origin and evolution of the West Andriamena Pan‐African mafic‐ultramafic complex in Madagascar as shown by U‐Pb, Nd isotopes and trace element constraints. Terra Abstr. 5:387.</mixed-citation>
         </ref>
         <ref id="rf24">
            <mixed-citation xlink:type="simple" publication-type="">Guigues, J.; Rakotoarimanana, A.; and Alsac, C. 1963. Geological map of Antsirabe‐Ambatolampy area. Service Géologique de Madagascar, Antananarivo, scale, 1 : 200,000 (color).</mixed-citation>
         </ref>
         <ref id="rf25">
            <mixed-citation xlink:type="simple" publication-type="">Handke, M. J.; Tucker, R. D.; and Hamilton, M. A. 1997. Age, geochemistry, and petrogenesis of the Early Neoproterozoic (800–790 Ma) intrusive igneous rocks of the Itremo Region, central Madagascar.<italic>In</italic>Cox, R. M., and Ashwal, L. D., eds. Proceedings of the UNESCO‐IUGS‐IGCP‐348/368 International Field Workshop on Proterozoic Geology of Madagascar, Gondwana Misc. Publ. no. 5, p. 28–29.</mixed-citation>
         </ref>
         <ref id="rf26">
            <mixed-citation xlink:type="simple" publication-type="">Hoffman, P. F. 1991 Did the breakout of Laurentia turn Gondwanaland inside out? Science 252:1409–1412.</mixed-citation>
         </ref>
         <ref id="rf27">
            <mixed-citation xlink:type="simple" publication-type="">Hottin, G. 1976. Presentation et essai d'Interpretation du Precambrien de Madagascar. Bulletin 2d series 4, no. 2, Paris, Bureau des Recherche Géologique et Miniere, p. 17–53.</mixed-citation>
         </ref>
         <ref id="rf28">
            <mixed-citation xlink:type="simple" publication-type="">Hottin, G., and Randriamanantena, Z. 1965. Geological map of Ambatolampy, p. 48, Service Géologique de Madagascar, Antananarivo, scale, 1 : 100,000 (color).</mixed-citation>
         </ref>
         <ref id="rf29">
            <mixed-citation xlink:type="simple" publication-type="">Jaffey, A. H.; Flynn, K. F.; Glendenin, L. E.; Bentley, W. C.; and Essling, A. M. 1971. Precision measurement of half‐lives and specific activities of<sup>235</sup>U and<sup>238</sup>U. Phys. Rev. C 4:1889–1906.</mixed-citation>
         </ref>
         <ref id="rf30">
            <mixed-citation xlink:type="simple" publication-type="">Krogh, T. E. 1973. A low‐contamination method for hydrothermal decomposition of zircon and extraction of U and Pb for isotopic age determinations. Geochim. Cosmochim. Acta 37:485–494.</mixed-citation>
         </ref>
         <ref id="rf31">
            <mixed-citation xlink:type="simple" publication-type="">———. 1982. Improved accuracy of U‐Pb zircon dating by the creation of more concordant systems using air abrasion technique. Geochim. Cosmochim. Acta 46:637–649.</mixed-citation>
         </ref>
         <ref id="rf32">
            <mixed-citation xlink:type="simple" publication-type="">Krogstad, E. J.; Hanson, G. N.; and Rajamani, V. 1991. U‐Pb ages of zircon and sphene for two gneiss terranes adjacent to the Kolar Schist Belt, South India: evidence for separate crustal evolution histories. J. Geol. 99:801–816.</mixed-citation>
         </ref>
         <ref id="rf33">
            <mixed-citation xlink:type="simple" publication-type="">Kröner, A.; Braun, I.; and Jaeckel, P. 1996. Zircon geochronology of anatectic melts and residues from a high‐grade pelitic assemblage at Ihosy, southern Madagascar: evidence for Pan‐African granulite metamorphism. Geol. Mag. 133:311–323.</mixed-citation>
         </ref>
         <ref id="rf34">
            <mixed-citation xlink:type="simple" publication-type="">Kröner, A.; Pidgeon, R. J.; Sacchi, R.; and Windley, B. F. 1997. Single zircon ages from high‐grade gneisses of the Mozambique Belt in Malawi, northern Mozambique and Madagascar: evidence for Pan‐African metamorphism and implications Gondwana assembly. Terra Nova 9(abstr. suppl.):163.</mixed-citation>
         </ref>
         <ref id="rf35">
            <mixed-citation xlink:type="simple" publication-type="">Kröner, A., and Williams, I. S. 1993. Age of metamorphism in the high‐grade rocks of Sri Lanka. J. Geol. 101:513–521.</mixed-citation>
         </ref>
         <ref id="rf36">
            <mixed-citation xlink:type="simple" publication-type="">Lardeaux, J.‐M.; Martelat, J.‐E.; Nicollet, C.; Pili, E.; and Sheppard, S. 1997. The deep continental crust in southern Madagascar: strain pattern and related fluid and heat transfers.<italic>In</italic>Cox, R. M., and Ashwal, L. D., eds. Proceedings of the UNESCO‐IUGS‐IGCP‐348/368 International Field Workshop on Proterozoic Geology of Madagascar, Gondwana Misc. Publ. no. 5, p. 44.</mixed-citation>
         </ref>
         <ref id="rf37">
            <mixed-citation xlink:type="simple" publication-type="">Le Grange, M.; Ashwal, L. D.; Tucker, R. D.; de Wit, M. J.; and Rambeloson, R. A. 1998. The Maevatanana schistose‐gneissic terrain, north‐central Madagascar, general geology and economic prospects. Geol. Soc. S. Afr. Geocongress 98, Pretoria, p. 55–59.</mixed-citation>
         </ref>
         <ref id="rf38">
            <mixed-citation xlink:type="simple" publication-type="">Ludwig, K. R. 1980. Calculation of uncertainties of U‐Pb isotope data. Earth Planet. Sci. Lett. 46:212–220.</mixed-citation>
         </ref>
         <ref id="rf39">
            <mixed-citation xlink:type="simple" publication-type="">———. 1992. ISOPLOT—a plotting and regression program for radiogenie isotope data, version 2.57. U.S. Geol. Surv. Open File Rep. 91–445.</mixed-citation>
         </ref>
         <ref id="rf40">
            <mixed-citation xlink:type="simple" publication-type="">Meen, J. K.; Rogers, J. J. W.; and Fullagar, P. D. 1992. Lead isotopic compositions of the Western Dharwar Craton, southern India: evidence for distinct Middle Archean terranes in a Late Archean craton. Geochim. Cosmochim. Acta 56:2455–2470.</mixed-citation>
         </ref>
         <ref id="rf41">
            <mixed-citation xlink:type="simple" publication-type="">Moine, B. 1968. Massif schisto‐quartzo‐dolomitique: region d'Ambatofinandrahana centre‐ouest du socle cristallin Precambrien de Madagascar. Service Géologique de Madagasikara, Tananarive, scale, 1 : 100,000.</mixed-citation>
         </ref>
         <ref id="rf42">
            <mixed-citation xlink:type="simple" publication-type="">———. 1974. Carctères de sédimentation et de métamorphism des séries précambriennes épizonales à catasonales du centre de Madagascar (région d'Ambatofinandrahana). Sci. de la Terra Mém. 31:293.</mixed-citation>
         </ref>
         <ref id="rf43">
            <mixed-citation xlink:type="simple" publication-type="">Nédeléc, A.; Stephens, W. E.; and Fallick, A. E. 1995. The Panafrican stratoid granites of Madagascar: alkaline magmatism in a post‐collisional extensional setting. J. Petrol. 36:1367–1391.</mixed-citation>
         </ref>
         <ref id="rf44">
            <mixed-citation xlink:type="simple" publication-type="">Nicollet, C. 1990. Crustal evolution of the granulites of Madagascar.<italic>In</italic>Vielzeuf, D., and Vidal, P., eds. Granulites and crustal evolution. NATO ASI Series C, 311. Dordrecht, Kluwer, p. 291–310.</mixed-citation>
         </ref>
         <ref id="rf45">
            <mixed-citation xlink:type="simple" publication-type="">Paquette, J.‐L., and Nédélec, A. 1998. A new insight into Pan‐African tectonics in the East‐West Gondwana collision zone by U‐Pb zircon dating of granites from central Madagascar. Earth Planet. Sci. Lett. 155:45–56.</mixed-citation>
         </ref>
         <ref id="rf46">
            <mixed-citation xlink:type="simple" publication-type="">Paquette, J. L.; Nédélec, A.; Moine, B.; and Rakotondrazafy, M. 1994. U‐Pb, single zircon Pb‐evaporation, and Sm‐Nd isotopic study of a granulite domain in SE Madagascar. J. Geol. 102:523–538.</mixed-citation>
         </ref>
         <ref id="rf47">
            <mixed-citation xlink:type="simple" publication-type="">Peucat, J. J.; Bouhallier, H.; Fanning, C. M.; and Jayananda, M. 1995. Age of the Holenarsipur greenstone belt: relationships with the surrounding gneisses (Karnataka, South India). J. Geol. 103:701–710.</mixed-citation>
         </ref>
         <ref id="rf48">
            <mixed-citation xlink:type="simple" publication-type="">Peucat, J. J.; Mahabaleswar, B.; and Jayananda, M. 1993. Age of younger tonalitic magmatism and granulitic metamorphism in the South Indian transition zone (Krishnagiri area): comparison with older Peninsular gneisses from the Gorur‐Hassan area. J. Metamorph. Geol. 11:879–888.</mixed-citation>
         </ref>
         <ref id="rf49">
            <mixed-citation xlink:type="simple" publication-type="">Richard, P.; Shimizu, N.; and Allègre, C. J. 1976.<sup>143</sup>Nd/<sup>144</sup>Nd, a natural tracer: an application to oceanic basalts. Earth Planet. Sci. Lett. 31:269–278.</mixed-citation>
         </ref>
         <ref id="rf50">
            <mixed-citation xlink:type="simple" publication-type="">Rogers, J. J. W. 1996. A history of continents in the past three billion years. J. Geol. 104:91–107.</mixed-citation>
         </ref>
         <ref id="rf51">
            <mixed-citation xlink:type="simple" publication-type="">Rogers, J. J. W., and Mauldin, L. C. 1994. A review of the terranes of southern India.<italic>In</italic>Subbarao, K. V., ed. Volcanism. New Delhi, Wiley Eastern, p. 157–171.</mixed-citation>
         </ref>
         <ref id="rf52">
            <mixed-citation xlink:type="simple" publication-type="">Stacey, J. S., and Kramers, J. D. 1975. Approximation of terrestrial lead isotope evolution by a two‐stage model. Earth Planet. Sci. Lett. 26:207–221.</mixed-citation>
         </ref>
         <ref id="rf53">
            <mixed-citation xlink:type="simple" publication-type="">Stern, R. J. 1994. Arc assembly and continental collision in the Neoproterozoic East African Orogen. Annu. Rev. Earth Planet. Sci. 22:319–351.</mixed-citation>
         </ref>
         <ref id="rf54">
            <mixed-citation xlink:type="simple" publication-type="">Taylor, P. N.; Chadwick, B.; Moorbath, S.; Ramakrishna, M.; and Viswanatha, M. N. 1984. Petrography, chemistry and isotopic ages of Peninsular gneiss, Dharwar acid volcanic rocks and the Chitradurga Granite with special reference to the Late Archaean evolution of the Karnataka Craton, Southern India. Precambrian Res. 23:349–375.</mixed-citation>
         </ref>
         <ref id="rf55">
            <mixed-citation xlink:type="simple" publication-type="">Tucker, R. D.; Ashwal, L. D.; Handke, M. J.; and Hamilton, M. A. 1997. A geochronologic overview of the Precambrian rocks of Madagascar: a record from the Middle Archean to the Late Neoproterozoic.<italic>In</italic>Cox, R. M., and Ashwal, L. D., eds. Proceedings of the UNESCO‐IUGS‐IGCP‐348/368 International Field Workshop on Proterozoic Geology of Madagascar, Gondwana Misc. Pub. 5, 99 p.</mixed-citation>
         </ref>
         <ref id="rf56">
            <mixed-citation xlink:type="simple" publication-type="">Tucker, R. D.; Handke, M. J.; and Ashwal, L. D. 1996. New isotopic ages and geological perspectives of the Precambrian rocks of north and north‐central Madagascar. Geol. Soc. Am. Abstr. Prog. 28, no. 7, p. A‐230.</mixed-citation>
         </ref>
         <ref id="rf57">
            <mixed-citation xlink:type="simple" publication-type="">Tucker, R. D.; Krogh, T. E.; and Råheim, A. 1991. Proterozoic evolution and age‐province boundaries in the central part of the Western Gneiss Region, Norway: results of U‐Pb dating of accessory minerals from Trondheimsfjord to Geiranger.<italic>In</italic>Gower, C. F.; Rivers, T.; and Ryan, B., eds. Mid‐Proterozoic Laurentia‐Baltica. Geol. Assoc. Can. Spec. Pap. 38:149–173.</mixed-citation>
         </ref>
         <ref id="rf58">
            <mixed-citation xlink:type="simple" publication-type="">Vachette, M. 1979. Le Précambrien de Madagascar: radiochronométrie par isochrones Rb/Sr sur roches totales. Rev. Géol. Dynam. Géog. Phys. 21:331–338.</mixed-citation>
         </ref>
         <ref id="rf59">
            <mixed-citation xlink:type="simple" publication-type="">Vachette, M., and Hottin, G. 1970. Age au strontium des granites d'Antongil et de l'Androna (Nord‐Est et Centre‐Nord de Madagascar). C. R. Semaine Géol. Madagascar, p. 73–76.</mixed-citation>
         </ref>
         <ref id="rf60">
            <mixed-citation xlink:type="simple" publication-type="">———. 1979. The Precambrian of Madagascar through whole‐rock Rb/Sr isochron data.<italic>In</italic>Histoire du Gondwana vue de Madagascar, Antanarivo.</mixed-citation>
         </ref>
         <ref id="rf61">
            <mixed-citation xlink:type="simple" publication-type="">Wiedenbeck, M., and Goswami, J. N. 1994. High precision<sup>207</sup>Pb/<sup>206</sup>Pb zircon geochronology using a small ion microprobe. Geochim. Cosmochim. Acta 58:2135–2145.</mixed-citation>
         </ref>
         <ref id="rf62">
            <mixed-citation xlink:type="simple" publication-type="">Wiedenbeck, M.; Goswami, J. N.; and Roy, A. B. 1996. Stabilization of the Aravalli Craton of northwestern India at 2.5 Ga: an ion microprobe zircon study. Chem. Geol. 129:325–340.</mixed-citation>
         </ref>
         <ref id="rf63">
            <mixed-citation xlink:type="simple" publication-type="">Windley, B. F.; Razafiniparany, A.; Razakamanana, T.; and Ackermand, D. 1994. Tectonic framework of the Precambrian of Madagascar and its Gondwana connections: a review and reappraisal. Geol. Rundsch. 83:642–659.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
