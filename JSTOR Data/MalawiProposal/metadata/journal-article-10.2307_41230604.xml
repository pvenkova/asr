<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jinfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00221899</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41230604</article-id>
         <article-categories>
            <subj-group>
               <subject>MAJOR ARTICLES AND BRIEF REPORTS</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>HIV-1 RNA Rectal Shedding Is Reduced in Men With Low Plasma HIV-1 RNA Viral Loads and Is Not Enhanced by Sexually Transmitted Bacterial Infections of the Rectum</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Colleen F.</given-names>
                  <surname>Kelley</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Richard E.</given-names>
                  <surname>Haaland</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pragna</given-names>
                  <surname>Patel</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Tammy</given-names>
                  <surname>Evans-Strickfaden</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Carol</given-names>
                  <surname>Farshy</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Debra</given-names>
                  <surname>Hanson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kenneth</given-names>
                  <surname>Mayer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeffrey L.</given-names>
                  <surname>Lennox</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John T.</given-names>
                  <surname>Brooks</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Clyde E.</given-names>
                  <surname>Hart</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">204</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40055460</issue-id>
         <fpage>761</fpage>
         <lpage>767</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/infdis/jir400"
                   xlink:title="an external site"/>
         <abstract>
            <p>Background. Among human immunodeficiency virus (HIV)—infected men who have sex with men (MSM) taking combination antiretroviral therapy (cART), the impact of rectal sexually transmitted infections (STIs) on rectal HIV-1 shedding is unknown. Methods. Human immunodeficiency virus type 1 (HIV-1) RNA was quantified from rectal swabs collected for Neisseria gonorrhoeae (GC) and Chlamydia trachomatis (CT) screening of HIV-1-infected MSM. Correlations of STIs with rectal viral load were explored using multinomial regression modeling. HIV-1 coreceptor tropism was predicted from sequencing in a subset of men. Results. Thirty-one (39%) of 80 men (59 prescribed combination antiretroviral therapy [cART]) had HIV detected in 38 (42%) of 91 rectal swabs. Rectal HIV detection was associated with plasma virus loads above 3.15 log₁₀ copies/mL (95% confidence limit [CL] 2.73, 3.55) and paired rectal viral loads and plasma viral loads were correlated (Kendall's tau [τ] 0.68, Spearman rho [P] = .77). Rectal STIs and abnormal anal cytology were not associated with rectal viral load. HIV coreceptor distribution was very similar between the plasma and rectum in 3 of 4 men. Conclusions. Plasma and rectal viral load were correlated, and rectal STIs did not increase the likelihood of detecting HIV in the rectal secretions in MSM, including those with low or undetectable plasma viral load. Suppressing plasma viral load is likely to reduce risk of HIV transmission to insertive partners.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d310e267a1310">
            <label>1</label>
            <mixed-citation id="d310e274" publication-type="other">
Trends in HIV/AIDS diagnoses among men who have sex with
men—33 stales, 2001-2006. MMWR Morb Mortal Wkly Rep 2008;
57:681-6.</mixed-citation>
         </ref>
         <ref id="d310e287a1310">
            <label>2</label>
            <mixed-citation id="d310e294" publication-type="other">
Centers for Disease Control and Prevention. HIV Surveillance Report,
2008; 20. http://www.cdc.gov/hiv/topics/surveillance/resources/reports/.
Accessed 1 December 1.</mixed-citation>
         </ref>
         <ref id="d310e307a1310">
            <label>3</label>
            <mixed-citation id="d310e314" publication-type="other">
Jaffe HW, Valdiserri RO, De Cock KM. The reemerging HIV/AIDS
epidemic in men who have sex with men. JAMA 2007; 298:2412-4.</mixed-citation>
         </ref>
         <ref id="d310e324a1310">
            <label>4</label>
            <mixed-citation id="d310e331" publication-type="other">
Hall HI, Song R, Rhodes P, et al. Estimation of HIV incidence in the
United States. JAMA 2008; 300:520-9.</mixed-citation>
         </ref>
         <ref id="d310e342a1310">
            <label>5</label>
            <mixed-citation id="d310e349" publication-type="other">
Sullivan PS, Salazar L, Buchbinder S, Sanchez TH. Estimating the
proportion of HIV transmissions from main sex partners among men
who have sex with men in five US cities. AIDS 2009; 23:1153-62.</mixed-citation>
         </ref>
         <ref id="d310e362a1310">
            <label>6</label>
            <mixed-citation id="d310e369" publication-type="other">
Cohen MS, Hoffman IF, Royce RA, et al. Reduction of concentration of
HIV-1 in semen after treatment of urethritis: implications for pre-
vention of sexual transmission of HIV-1. AIDSCAP Malawi Research
Group. Lancet 1997; 349:1868-73.</mixed-citation>
         </ref>
         <ref id="d310e385a1310">
            <label>7</label>
            <mixed-citation id="d310e392" publication-type="other">
Kent CK, Chaw JK, Wong W, et al. Prevalence of rectal, urethral, and
pharyngeal chlamydia and gonorrhea detected in 2 clinical settings
among men who have sex with men: San Francisco, California, 2003.
Clin Infect Dis 2005; 41:67-74.</mixed-citation>
         </ref>
         <ref id="d310e408a1310">
            <label>8</label>
            <mixed-citation id="d310e415" publication-type="other">
Scott КС, Philip S, Ahrens K, Kent CK, Klausner JD. High prevalence of
gonococcal and chlamydial infection in men who have sex with men with
newly diagnosed HIV infection: an opportunity for same-day pre-
sumptive treatment. J Acquir Immune Defic Syndr 2008; 48:109-12.</mixed-citation>
         </ref>
         <ref id="d310e431a1310">
            <label>9</label>
            <mixed-citation id="d310e438" publication-type="other">
van't Wout AB, Kootstra NA, Mulder-Kamp inga GA, et al. Macrophage-
tropic variants initiate human immunodeficiency virus type 1 infection
after sexual, parenteral, and vertical transmission. J Clin Invest 1994;
94:2060-7.</mixed-citation>
         </ref>
         <ref id="d310e454a1310">
            <label>10</label>
            <mixed-citation id="d310e461" publication-type="other">
Zhu T, Mo H, Wang N, et al. Genotypic and phenotypic character-
ization of HIV-1 patients with primary infection. Science 1993;
261:1179-81.</mixed-citation>
         </ref>
         <ref id="d310e475a1310">
            <label>11</label>
            <mixed-citation id="d310e482" publication-type="other">
Moore JP, Kitchen SG, Pugach P, Zack JA. The CCR5 and CXCR4
coreceptors—central to understanding the transmission and patho-
genesis of human immunodeficiency virus type 1 infection. AIDS Res
Hum Retroviruses 2004; 20:111-26.</mixed-citation>
         </ref>
         <ref id="d310e498a1310">
            <label>12</label>
            <mixed-citation id="d310e505" publication-type="other">
van der Hoek L, Sol CJ, Snijders F, Bartelsman JF, Boom R, Goudsmit J.
Human immunodeficiency virus type 1 RNA populations in faeces
with higher homology to intestinal populations than to blood pop-
ulations. J Gen Virol 1996; 77:2415-25.</mixed-citation>
         </ref>
         <ref id="d310e521a1310">
            <label>13</label>
            <mixed-citation id="d310e528" publication-type="other">
van Marie G, Gill MJ, Kolodka D, McManus L, Grant T, Church DL.
Compartmentalization of the gut viral reservoir in HIV-1 infected
patients. Retroviroloey 2007; 4:87.</mixed-citation>
         </ref>
         <ref id="d310e541a1310">
            <label>14</label>
            <mixed-citation id="d310e548" publication-type="other">
Poles MA, Elliott J, Vingerhoets J, et al. Despite high concordance,
distinct mutational and phenotypic drug resistance profiles in human
immunodeficiency virus type 1 RNA are observed in gastrointestinal
mucosal biopsy specimens and peripheral blood mononuclear cells
compared with plasma. J Infect Dis 2001; 183:143-8.</mixed-citation>
         </ref>
         <ref id="d310e567a1310">
            <label>15</label>
            <mixed-citation id="d310e574" publication-type="other">
Monno L, Punzi G, Scarabaggio T, et al. Mutational patterns of paired
blood and rectal biopsies in HIV-infected patients on HAART. J Med
Virol 2003; 70:1-9.</mixed-citation>
         </ref>
         <ref id="d310e587a1310">
            <label>16</label>
            <mixed-citation id="d310e594" publication-type="other">
Kiviat NB, Critchlow CW, Hawes SE, et al. Determinants of human
immunodeficiency virus DNA and RNA shedding in the anal-rectal
canal of homosexual men. J Infect Dis 1998; 177:571-8.</mixed-citation>
         </ref>
         <ref id="d310e608a1310">
            <label>17</label>
            <mixed-citation id="d310e615" publication-type="other">
Lampinen TM, Critchlow CW, Kuypers JM, et al. Association of an-
tiretroviral therapy with detection of HIV-1 RNA and DNA in the
anorectal mucosa of homosexual men. AIDS 2000; 14:F69-75.</mixed-citation>
         </ref>
         <ref id="d310e628a1310">
            <label>18</label>
            <mixed-citation id="d310e635" publication-type="other">
Zuckerman RA, Lucchetti A, Whittington WL, et al. Herpes simplex virus
(HSV) suppression with valacyclovir reduces rectal and blood plasma
HIV-1 levels in HIV-1/HSV-2-seropositive men: a randomized, double-
blind, placebo-controlled crossover trial. J Infect Dis 2007; 196:1500-8.</mixed-citation>
         </ref>
         <ref id="d310e651a1310">
            <label>19</label>
            <mixed-citation id="d310e658" publication-type="other">
Zuckerman RA, Whittington WL, Celum CL, et al. Higher concen-
tration of HIV RNA in rectal mucosa secretions than in blood and
seminal plasma, among men who have sex with men, independent of
antiretro viral therapy. J Infect Dis 2004; 190:156-61.</mixed-citation>
         </ref>
         <ref id="d310e674a1310">
            <label>20</label>
            <mixed-citation id="d310e681" publication-type="other">
Vellozzi C, Brooks JT, Bush TJ, et al. The study to understand the
natural history of HIV and AIDS in the era of effective therapy (SUN
Study). Am J Epidemiol 2009; 169:642-52.</mixed-citation>
         </ref>
         <ref id="d310e694a1310">
            <label>21</label>
            <mixed-citation id="d310e703" publication-type="other">
Sullivan ST, Mandava U, Evans-Strickfaden T, Lennox JL, Ellerbrock
TV, Hart CE. Diversity, divergence, and evolution of cell-free human
immunodeficiency virus type 1 in vaginal secretions and blood of
chronically infected women: associations with immune status. J Virol
2005; 79:9799-809.</mixed-citation>
         </ref>
         <ref id="d310e722a1310">
            <label>22</label>
            <mixed-citation id="d310e729" publication-type="other">
Chen J, Young NL, Subbarao S, et al. HIV type 1 subtypes in Guangxi
Province, China, 1996. AIDS Res Hum Retrov 1999; 15:81-4.</mixed-citation>
         </ref>
         <ref id="d310e740a1310">
            <label>23</label>
            <mixed-citation id="d310e747" publication-type="other">
MCCullagh P. Regression models for ordinal data (with discussion).
J R Statist Soc Ser B 1980; 42:109-42.</mixed-citation>
         </ref>
         <ref id="d310e757a1310">
            <label>24</label>
            <mixed-citation id="d310e764" publication-type="other">
Johnson LF, Lewis DA. The effect of genital tract infections on HIV-1
shedding in the genital tract: a systematic review and meta-analysis. Sex
Transm Dis 2008; 35:946-59.</mixed-citation>
         </ref>
         <ref id="d310e777a1310">
            <label>25</label>
            <mixed-citation id="d310e784" publication-type="other">
Sadiq ST, Taylor S, Kaye S, et al. The effects of antiretroviral therapy on
HIV-1 RNA loads in seminal plasma in HIV-positive patients with and
without urethritis. AIDS 2002; 16:219-25.</mixed-citation>
         </ref>
         <ref id="d310e797a1310">
            <label>26</label>
            <mixed-citation id="d310e804" publication-type="other">
Bernstein KT, Marcus JL, Nieri G, Philip SS, Klausner JD. Rectal
gonorrhea and chlamydia reinfection is associated with increased risk
of HIV seroconversion. J Acquir Immune Defic Syndr 2010; 53:537-43.</mixed-citation>
         </ref>
         <ref id="d310e817a1310">
            <label>27</label>
            <mixed-citation id="d310e824" publication-type="other">
Granich RM, Gilks CF, Dye C, De Cock KM, Williams BG. Universal
voluntary HIV testing with immediate antiretroviral therapy as
a strategy for elimination of HIV transmission: a mathematical model.
Lancet 2009; 373:48-57.</mixed-citation>
         </ref>
         <ref id="d310e840a1310">
            <label>28</label>
            <mixed-citation id="d310e847" publication-type="other">
Dodd PJ, Garnett GP, Hallett ТВ. Examining the promise of HIV elimi-
nation by 'test and treat' in hyperendemic settings. AIDS 2010; 24:729-35.</mixed-citation>
         </ref>
         <ref id="d310e858a1310">
            <label>29</label>
            <mixed-citation id="d310e865" publication-type="other">
Cohen MS, Gay C, Kashuba AD, Blower S, Paxton L. Narrative review:
antiretroviral therapy to prevent the sexual transmission of HIV-1. Ann
Intern Med 2007; 146:591-601.</mixed-citation>
         </ref>
         <ref id="d310e878a1310">
            <label>30</label>
            <mixed-citation id="d310e885" publication-type="other">
Neely MN, Benning L, Xu J, et al. Cervical shedding of HIV-1 RNA
among women with low levels of viremia while receiving highly ac-
tive antiretroviral therapy. J Acquir Immune Defic Syndr 2007; 44:
38-42.</mixed-citation>
         </ref>
         <ref id="d310e901a1310">
            <label>31</label>
            <mixed-citation id="d310e908" publication-type="other">
Sheth PM, Kovacs C, Kemal KS, et al. Persistent HIV RNA shedding
in semen despite effective antiretroviral therapy. AIDS 2009; 23:
2050-4.</mixed-citation>
         </ref>
         <ref id="d310e921a1310">
            <label>32</label>
            <mixed-citation id="d310e928" publication-type="other">
Attia S, Egger M, Müller M, Zwahlen M, Low N. Sexual transmission
of HIV according to viral load and antiretroviral therapy: systematic
review and meta-analysis. AIDS 2009; 23:1397-404.</mixed-citation>
         </ref>
         <ref id="d310e941a1310">
            <label>33</label>
            <mixed-citation id="d310e948" publication-type="other">
Yeaman GR, Asin S, Weldon S, et al. Chemokine receptor expression
in the human ectocervix: implications for infection by the human
immunodeficiency virus-type I. Immunology 2004; 113:524-33.</mixed-citation>
         </ref>
         <ref id="d310e961a1310">
            <label>34</label>
            <mixed-citation id="d310e968" publication-type="other">
Hladik F, Lentz G, Delpit E, McElroy A, McElrath MJ. Coexpression
of CCR5 and IL-2 in human genital but not blood T cells: implications
for the ontogeny of the CCR5 + Thl phenotype. J Immunol 1999; 163:
2306-13.</mixed-citation>
         </ref>
         <ref id="d310e985a1310">
            <label>35</label>
            <mixed-citation id="d310e992" publication-type="other">
Patterson BK, Landay A, Andersson J, et al. Repertoire of chemokine
receptor expression in the female genital tract: implications for human
immunodeficiency virus transmission. Am J Pathol 1998; 153:481-90.</mixed-citation>
         </ref>
         <ref id="d310e1005a1310">
            <label>36</label>
            <mixed-citation id="d310e1012" publication-type="other">
Barroso PF, Schechter M, Gupta P, et al. Effect of antiretroviral therapy
on HIV shedding in semen. Ann Intern Med 2000; 133:280-4.</mixed-citation>
         </ref>
         <ref id="d310e1022a1310">
            <label>37</label>
            <mixed-citation id="d310e1029" publication-type="other">
Hart CE, Lennox JL, Pratt-Palmore M, et al. Correlation of human
immunodeficiency virus type 1 RNA levels in blood and the female
genital tract. J Infect Dis 1999; 179:871-82.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
