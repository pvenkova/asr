<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt1825qtx</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1qqhfzd</book-id>
      <subj-group>
         <subject content-type="call-number">QH75 .I355 2017</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Nature conservation</subject>
         <subj-group>
            <subject content-type="lcsh">Economic aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ecotourism</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Spectacular, The</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Capitalism</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>Anthropology</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Nature of Spectacle</book-title>
         <subtitle>On Images, Money, and Conserving Capitalism</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>IGOE</surname>
               <given-names>JIM</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>12</day>
         <month>09</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9780816530441</isbn>
      <isbn content-type="epub">9780816537549</isbn>
      <publisher>
         <publisher-name>University of Arizona Press</publisher-name>
         <publisher-loc>TUCSON</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>The Arizona Board of Regents</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1qqhfzd"/>
      <abstract abstract-type="short">
         <p>Today crisis appears to be the normal order of things. We seem to be turning in widening gyres of economic failure, species extinction, resource scarcity, war, and climate change. These crises are interconnected ecologically, economically, and politically. Just as importantly, they are connected-and disconnected-in our imaginations. Public imaginations are possibly the most important stage on which crises are played out, for these views determine how the problems are perceived and what solutions are offered.In<italic>The Nature of Spectacle</italic>, Jim Igoe embarks on multifaceted explorations of how we imagine nature and how nature shapes our imaginations. The book traces spectacular productions of imagined nature across time and space-from African nature tourism to transnational policy events to green consumer appeals in which the push of a virtual button appears to initiate a chain of events resulting in the protection of polar bears in the Arctic or jaguars in the Amazon rainforest. These explorations illuminate the often surprising intersections of consumerism, entertainment, and environmental policy. They show how these intersections figure in a strengthening and problematic policy consensus in which economic growth and ecosystem health are cast as mutually necessitating conditions. They also take seriously the potential of these intersections and how they may facilitate other alignments and imaginings that may become the basis of alternatives to our current socioecological predicaments.</p>
      </abstract>
      <counts>
         <page-count count="176"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>I</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>VII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>IX</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.4</book-part-id>
                  <title-group>
                     <title>INTRODUCTION:</title>
                     <subtitle>The Spectacle of Nature and Circuits of Capitalism</subtitle>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>Nature is a remarkable thing. It does not exactly exist, yet it is all around us, shaping our realities and giving meaning to our lives. Raymond Williams (1976: 219) found that one of the most common senses in which the word “nature” is used in English is in reference to “the material world itself, taken as including or not including human beings.” For human beings, however, there is no material world that precedes meaningmaking. “If we can talk about nature,” writes Eduardo Kohn (2015: 315), “it is only as culture.” Perceptions that material realities constitute a gigantic object called nature</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>MAKING, MANAGING, AND MARKETING EAST AFRICAN NATURE</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>In september 2007, Tanzanian President Jakaya Kikwete attended a gala reception at the palatial Tavern on the Green in New York City’s Central Park. The gala launched a new slogan for the East African nation—“The Land of Kilimanjaro, Zanzibar, and the Serengeti”—supported by a television ad campaign targeting upper-middle-class and wealthy Americans. Just over a year later I was interviewed by filmmakers producing a documentary on conservation conflict in Tanzania. The resulting film,<italic>A Place Without People</italic>, includes footage of Kikwete speaking at a similar gala event in Tanzania. “As you know,” says the president, “tourism, which is very</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A LANDSCAPE THAT FUNCTIONS ECOLOGICALLY AND ECONOMICALLY?</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>My wife, Gladness, grew up near a place called Msitu wa Tembo (Forest of Elephants). For her, that was only a name: there were no elephants or forest. But as a child Gladness listened to elders recalling the days when elephants actually inhabited that forest. One needed to be careful around those animals. They were so big that if you came upon one at night, you might mistake it for a house and try to knock on the door. For Gladness this was all very amusing, but also improbable. She was certain they were exaggerating, or “ adding salt,” as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>SEEING THE WORLD TO SAVE THE WORLD</title>
                  </title-group>
                  <fpage>54</fpage>
                  <abstract>
                     <p>This chapter continues to elaborate on the ways capitalism, conservation, and tourism are materially and symbolically intertwined in northern Tanzania. In chapter 1 we saw that postcolonial conservation in Tanzania emerged alongside both tourism and a story wrapped around nature: that nature is a mainstay of national economic growth and that development will be achieved through the putative nonuse of protected landscapes. We also began to see how more complex versions of this story were being brought forth, and this is a central concern of the present chapter. In chapter 2 we explored the dynamics of the colonial-era geographies underpinning</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>WISE EXCHANGE, CONVENTION SPACE, AND TRANSNATIONAL TOURNAMENTS OF VALUE</title>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>This chapter is concerned with the wider institutional and policy transformations of which the Maasai Steppe story is part. It begins with the “neoliberalization” of transnational conservation at the end of the twentieth century and the rise of conservation BINGOs (big nongovernmental organizations). This first phase involves the transformation in U.S. support for transnational conservation and its connections to the iconic figure of Teddy Roosevelt, who is remembered as—among other things—the original proponent of the “wise use” of nature. This philosophy, which will be ad dressed in detail below, invokes possibilities for using nature in ways that will</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>CONSUME, CONNECT, CONSERVE</title>
                  </title-group>
                  <fpage>90</fpage>
                  <abstract>
                     <p>Years after my wife gladness saw her first elephant in Tarangire National Park, we visited New York’s Central Park. On a beautiful Sunday in June 2010, we strolled up Broadway from Times Square with our sons. As we approached the southwest corner of the park, a group of young Malian men on pedal rickshaws began vying for our custom. Following a brief negotiation, we found ourselves travelling north on Central Park West. We passed the Tavern on the Green, where President Kikwete of Tanzania had launched his country’s ecotourism slogan: “The Land of Kilimanjaro, Zanzibar, and the Serengeti.” We then</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>SOME LAST(ING) STORIES AND REFLECTIONS</title>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract>
                     <p>As a television-infatuated child of the 1970s, my view of the world was thoroughly mediated by images. By the age of ten, I had developed a penchant for corporate-sponsored programming about national parks, theme parks, and real-action wildlife safaris. Each Sunday evening, across the dinner table, my family would tune into<italic>Walt Disney Presents</italic>, followed by<italic>Mutual of Omaha’s Wild Kingdom</italic>. Occasionally, when my mom was putting my younger siblings to bed, I would sneak in some extra, post-primetime viewing. It was on one such occasion that I caught the musical special<italic>Old Faithful</italic>, which was essentially an extended Chevrolet</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.11</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>119</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.12</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>143</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.13</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>155</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1qqhfzd.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>162</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
