<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">stafpapeintemone</journal-id>
         <journal-id journal-id-type="jstor">j100190</journal-id>
         <journal-title-group>
            <journal-title>Staff Papers (International Monetary Fund)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Monetary Fund</publisher-name>
         </publisher>
         <issn pub-type="ppub">00208027</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/3866773</article-id>
         <title-group>
            <article-title> Fiscal Adjustment and Fund-Supported Programs, 1971-80 (Ajustement budgétaire et programmes bénéficiant de l'aide du FMI, 1971-80) (El ajuste fiscal y los programas Ilevados a cabo con apoyo del Fondo, 1971-80) </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Margaret R.</given-names>
                  <surname>Kelly</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1982</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i294964</issue-id>
         <fpage>561</fpage>
         <lpage>602</lpage>
         <page-range>561-602</page-range>
         <permissions>
            <copyright-statement/>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3866773"/>
         <abstract>
            <p> A recent staff paper concluded that countries that undertook Fund-supported programs in the 1970s achieved significant absolute and relative reductions in their external deficits and a relative reduction in average domestic inflation rates, compared with other non-oil developing countries. These adjustments generally were not achieved at a cost of lower real rates of growth and consumption. The paper investigates the extent to which reductions in external deficits resulted from adjustments in the financial position of the government (as opposed to that of the rest of the economy). The analysis utilizes national income and monetary survey identities to specify relationships between fiscal and balance of payments variables. The results reinforce the importance attached to fiscal targets and performance clauses in Fund programs. They also show that (i) external imbalances in preprogram years tended to be associated with large fiscal imbalances; (ii) reductions/increases (relative to gross national product (GNP)) in the deficit on the current account of the balance of payments in program years were associated with reductions/increases (relative to GNP) in the overall government deficit (on average, reductions in fiscal deficits accounted for 40 per cent of the reduction in current account deficits); and (iii) the overall balance of payments and domestically financed budget deficit also tended to move in the same direction. Large absolute (and relative) reductions in financial deficits were also observed in the rest of the economy in a number of programs. The paper also examines policies for reducing fiscal deficits in terms of their effects on growth and medium-term balance of payments performance. It argues that policies based on disaggregated analysis are likely to be more useful than those based on aggregative analysis; it stresses the desirability of designing revenue and expenditure policies in consultation with tax specialists and project specialists in major development agencies. /// Une récente étude des services du Fonds aboutit à la conclusion que les pays qui, dans la décennie 70, ont appliqué des programmes bénéficiant de l'aide du Fonds ont sensiblement réduit leur déficit extérieur, en termes absolus et en termes relatifs, et abaissé leurs taux moyens d'inflation par rapport à d'autres pays en développement non pétroliers. Ces ajustements n'ont, en général, pas été opérés au prix d'une baisse des taux réels de croissance et de consommation. La présente étude a pour objet de déterminer la mesure dans laquelle l'ajustement de la position financière du secteur public (par opposition à celle du reste de l'économie) a contribué à la réduction des déficits extérieurs. Pour analyser les relations entre les variables budgétaires et les variables de la balance des paiements, l'auteur utilise deux identités, celle du revenu national et celle de la situation monétaire. Les résultats soulignent l'importance qui est attachée aux objectifs budgétaires et aux critères de réalisation dans les programmes du Fonds. Ils montrent aussi que: i) les déséquilibres extérieurs enregistrés dans les années précédant l'application des programmes sont souvent liés à d'importants déséquilibres budgétaires; ii) les diminutions ou augmentations (par rapport au produit national brut -- PNB) du déficit courant de la balance des paiements pendant la durée d'exécution des programmes sont liées aux diminutions ou augmentations (par rapport au PNB) du déficit global du secteur public (en moyenne, la réduction des déficits budgétaires a contribué pour 40% à la réduction des déficits courants); iii) la balance globale des paiements et le déficit budgétaire financé par des ressources intérieures tendent aussi à évoluer dans le même sens. On a observé en outre que, dans plusieurs cas, les programmes avaient permis aussi de réduire sensiblement les déficits financiers dans le reste de l'économie en termes absolus (et relatifs). L'étude examine, par ailleurs, les mesures qui, par leurs effets sur la croissance et sur les résultats à moyen terme de la balance des paiements, contribuent à réduire les déficits budgétaires. L'auteur fait valoir que les mesures qui se fondent sur une analyse détaillée des agrégats seront sans doute plus efficaces que celles qui reposent sur une analyse globale des agrégats; il est bon, souligne l'auteur, que les décisions en matière de recettes et de dépenses soient prises après consultation des experts en fiscalité et des spécialistes des projets travaillant dans des principaux organismes de développement. /// En un estudio reciente efectuado por funcionarios del Fondo se llegaba a la conclusión de que los países que siguieron programas del Fondo en el decenio de los setenta lograron reducciones significativas, tanto absolutas como relativas, en su déficit externo y una reducción relativa en la tasa media de inflación interna, en comparación con otros países en desarrollo no petroleros. Estos ajustes generalmente no se consiguieron a costa de tasas reales de crecimiento y de consumo más bajas. En el presente trabajo se investiga el grado en que las reducciones del déficit externo fueron resultado de ajustes en la situación financiera del Estado (a diferencia de las del resto de la economía). En el análisis se utilizan las identidades del ingreso nacional y del panorama monetario para especificar las relaciones entre la variable fiscal y la de balanza de pagos. Los resultados concuerdan con la importancia atribuida en los programas del Fondo a las metas fiscales y a las cláusulas de ejecución. Indican también que i) los desequilibrios externos de los años anteriores al programa tendían a ir acompañados de grandes desequilibrios fiscales; ii) las reducciones/aumentos (en relación con el producto nacional bruto (PNB)) de los déficit en la balanza en cuenta corriente durante los años del programa fueron acompañados de reducciones/aumentos (en relación con el PNB) en el déficit global del Estado (en promedio, las reducciones del déficit fiscal representaron el 40 por ciento de la reducción de los déficit en cuenta corriente), y iii) la balanza de pagos global y el déficit presupuestario financiado internamente registraron también una tendencia a variar en la misma dirección. En varios programas se observaron también grandes reducciones absolutas (y relativas) en los déficit financieros del resto de la economía. En el presente trabajo se examinan también las medidas para reducir los déficit fiscales en cuanto a sus efectos en el crecimiento económico y el comportamiento de la balanza de pagos a plazo medio. Se mantiene que las medidas basadas en un análisis desagregado probablemente sean más útiles que otras basadas en un análisis agregativo; se subraya la conveniencia de idear políticas de ingresos y de gastos en consulta con especialistas tributarios y expertos en proyectos en los principales organismos de desarrollo. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1680e149a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1680e156" publication-type="other">
Donovan (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e163a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1680e170" publication-type="book">
Fund's Draft Manual on Government Finance Statistics (Washington, June
1974)<person-group>
                     <string-name>
                        <surname>Fund</surname>
                     </string-name>
                  </person-group>
                  <source>Draft Manual on Government Finance Statistics</source>
                  <year>1974</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1680e195a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1680e202" publication-type="other">
Khan and Knight (1982)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1680e208" publication-type="other">
Milne (1977)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e215a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1680e222" publication-type="other">
Tanzi (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e230a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1680e237" publication-type="other">
IMF (1976)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e244a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1680e251" publication-type="other">
Tanzi (1976)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e258a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1680e265" publication-type="other">
Tanzi (1981)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e272a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1680e279" publication-type="book">
International Bank for Reconstruction and Development, World Devel-
opment Report, 1980 (Oxford University Press, 1980), p. 8 and pp. 48-64<person-group>
                     <string-name>
                        <surname>International Bank for Reconstruction and Development</surname>
                     </string-name>
                  </person-group>
                  <fpage>8</fpage>
                  <source>World Development Report, 1980</source>
                  <year>1980</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1680e308a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1680e315" publication-type="other">
Heller (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e322a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1680e329" publication-type="other">
Donovan
(1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e340a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1680e347" publication-type="other">
Donovan (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e354a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1680e361" publication-type="other">
Donovan (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e368a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1680e375" publication-type="other">
Kelly (1980)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1680e381" publication-type="other">
Beveridge (1981)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e388a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1680e395" publication-type="other">
Donovan (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e402a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1680e409" publication-type="other">
Zaïre, 1977</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1680e415" publication-type="other">
Togo, 1979</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e422a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1680e429" publication-type="other">
Donovan (1982), p. 184, Table 5</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e437a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1680e444" publication-type="other">
Beveridge and Kelly (1980)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1680e450" publication-type="other">
Beveridge (1981)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e457a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1680e464" publication-type="other">
Donovan (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d1680e471a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1680e478" publication-type="other">
Donovan (1982), p. 199</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1680e494a1310">
            <mixed-citation id="d1680e498" publication-type="book">
Beveridge, W.A., "Fiscal Adjustment in Financial Programs Supported by
Stand-By Arrangements in the Upper Credit Tranches, 1978-79" (unpub¬
lished, International Monetary Fund, July 1, 1981)<person-group>
                  <string-name>
                     <surname>Beveridge</surname>
                  </string-name>
               </person-group>
               <source>Fiscal Adjustment in Financial Programs Supported by Stand-By Arrangements in the Upper Credit Tranches, 1978-79</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e527a1310">
            <mixed-citation id="d1680e531" publication-type="journal">
—, and Margaret R. Kelly, "Fiscal Content of Financial Programs Sup¬
ported by Stand-By Arrangements in the Upper Credit Tranches,
1969-78," Staff Papers, Vol. 27 (June1980), pp. 205-49<person-group>
                  <string-name>
                     <surname>Beveridge</surname>
                  </string-name>
               </person-group>
               <issue>June</issue>
               <fpage>205</fpage>
               <volume>27</volume>
               <source>Staff Papers</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e569a1310">
            <mixed-citation id="d1680e573" publication-type="journal">
Donovan, Donal J., "Macroeconomic Performance and Adjustment Under
Fund-Supported Programs: The Experience of the Seventies," Staff Papers,
Vol. 29 (June1982), pp. 171-203<person-group>
                  <string-name>
                     <surname>Donovan</surname>
                  </string-name>
               </person-group>
               <issue>June</issue>
               <fpage>171</fpage>
               <volume>29</volume>
               <source>Staff Papers</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e611a1310">
            <mixed-citation id="d1680e615" publication-type="book">
Heller, Peter S., "The Problem of Recurrent Costs in the Budgeting and
Planning Process" (unpublished, International Monetary Fund, June 1,
1982)<person-group>
                  <string-name>
                     <surname>Heller</surname>
                  </string-name>
               </person-group>
               <source>The Problem of Recurrent Costs in the Budgeting and Planning Process</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e645a1310">
            <mixed-citation id="d1680e649" publication-type="other">
International Monetary Fund (IMF), "The Concept of the Current Balance in
Government Accounts" (unpublished, June 11, 1976)</mixed-citation>
         </ref>
         <ref id="d1680e659a1310">
            <mixed-citation id="d1680e663" publication-type="book">
Khan, Mohsin S., and Malcolm D. Knight, "Some Theoretical and Empirical
Issues Relating to Economic Stabilization in Developing Countries"
(unpublished, International Monetary Fund, June 21, 1982)<person-group>
                  <string-name>
                     <surname>Khan</surname>
                  </string-name>
               </person-group>
               <source>Some Theoretical and Empirical Issues Relating to Economic Stabilization in Developing Countries</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e692a1310">
            <mixed-citation id="d1680e696" publication-type="journal">
Milne, Elizabeth, "The Fiscal Approach to the Balance of Payments," Eco-
nomic Notes, Monte dei Paschi di Siena, Vol. 6 (No. 1, 1977), pp. 89-106<person-group>
                  <string-name>
                     <surname>Milne</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>89</fpage>
               <volume>6</volume>
               <source>Economic Notes</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e731a1310">
            <mixed-citation id="d1680e735" publication-type="journal">
Reichmann, Thomas M., and Richard T. Stillson, "Experience with Programs
of Balance of Payments Adjustment: Stand-By Arrangements in the Higher
Tranches, 1963-72," Staff Papers, Vol. 25 (June1978), pp. 293-309<person-group>
                  <string-name>
                     <surname>Reichmann</surname>
                  </string-name>
               </person-group>
               <issue>June</issue>
               <fpage>293</fpage>
               <volume>25</volume>
               <source>Staff Papers</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e773a1310">
            <mixed-citation id="d1680e777" publication-type="journal">
Tanzi, Vito (1976), "Fiscal Policy, Keynesian Economics and the Mobilization
of Savings in Developing Countries," World Development, Vol. 4 (No. 10
and 11, 1976), pp. 907-17<person-group>
                  <string-name>
                     <surname>Tanzi</surname>
                  </string-name>
               </person-group>
               <issue>10</issue>
               <fpage>907</fpage>
               <volume>4</volume>
               <source>World Development</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e815a1310">
            <mixed-citation id="d1680e819" publication-type="book">
— (1981), "Taxation and Price Stabilization" (unpublished, International
Monetary Fund, October 30, 1981)<person-group>
                  <string-name>
                     <surname>Tanzi</surname>
                  </string-name>
               </person-group>
               <source>Taxation and Price Stabilization</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1680e845a1310">
            <mixed-citation id="d1680e849" publication-type="book">
—1982), "Fiscal Disequilibrium in Developing Countries" (unpublished,
International Monetary Fund, June 23, 1982)<person-group>
                  <string-name>
                     <surname>Tanzi</surname>
                  </string-name>
               </person-group>
               <source>Fiscal Disequilibrium in Developing Countries</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
