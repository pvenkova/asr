<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wph4s</book-id>
      <subj-group>
         <subject content-type="call-number">HC79.P6G65 2008</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Poverty</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
            <subj-group>
               <subject content-type="lcsh">Prevention</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Globalization</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Economics</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Global Development 2.0</book-title>
         <subtitle>Can Philanthropists, the Public, and the Poor Make Poverty History?</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>BRAINARD</surname>
               <given-names>LAEL</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>CHOLLET</surname>
               <given-names>DEREK</given-names>
            </name>
         </contrib>
         <role content-type="editor">Editors</role>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>10</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="epub">9780815701569</isbn>
      <isbn content-type="epub">081570156X</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2008</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wph4s"/>
      <abstract abstract-type="short">
         <p>The fight against global poverty has quickly become one of the hottest tickets on the global agenda -with rock stars, world leaders, and multibillionaires calling attention to the plight of the poor at international confabs such as the World Economic Forum and the Clinton Global Initiative. The cozy, all-of-a-kind club of rich country officials who for decades dominated the development agenda has given way to a profusion of mega-philanthropists, "celanthropists," and super-charged advocacy networks vying to solve the world's toughest problems. Supporting the development glitterati is a sizable rank and file made up of the mass public -as evidenced by the abundance of "Make Poverty History" wristbands, an Internet-enabled spike in charitable giving at all income levels, and record involvement in overseas volunteering. While philanthropic foundations and celebrity goodwill ambassadors have been part of the charitable landscape for many years, the unprecedented explosion of development players heralds a new era of global action on poverty. Global Development 2.0 celebrates this transformative trend within international aid and offers lessons to ensure that this wave of generosity yields lasting and widespread improvements to the lives and prospects of the world's poorest. Contributors include Matthew Bishop (Economist), Joshua Busby (University of Texas-Austin), J. Gregory Dees (Duke University), Vinca LaFleur (Vinca LaFleur Communications), Homi Kharas (Brookings Institution),Ashok Khosla (Development Alternatives Group), Mark Kramer (FSG Social Impact Advisors), Jane Nelson (Harvard University), Joseph O'Keefe (Brookings Institution), Ngozi Okonjo-Iweala (Brookings Institution), Darrell M.West (Brown University), and Simon Zadek (AccountAbility).</p>
      </abstract>
      <counts>
         <page-count count="244"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Talbott</surname>
                           <given-names>Strobe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>The effort to defeat global poverty is one of the defining causes of the twenty-first century. Victory will require robust alliances waging the good fight on multiple fronts. Thus, venture capitalists, celebrity activists, technologists, and emerging global powers must join forces with traditional bilateral and multilateral donors. Representatives of all these fields and more gathered in August 2007 in Aspen, Colorado, under the auspices of the annual Brookings Blum Roundtable. For three days, they held a focused, forward-looking, action-oriented discussion about how to convert their sizable resources and shared sense of ingenuity and optimism into new ways to alleviate poverty.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The world of development is changing, and this book sets out to examine how and why—and to what end. This volume brings together the perspectives of new players with those of leaders in the public sector and academics involved in cross-cutting analysis. By examining the common challenges faced by all development players—accountability, the effective deployment of resources, agenda setting, and achieving scale and sustainability—these contributors’ chapters aim to spur a debate as well as build a consensus on effective practices and, thus, establish foundations for collaboration among the growing number of people and organizations committed to lifting</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Making Poverty History?</title>
                     <subtitle>How Activists, Philanthropists, and the Public Are Changing Global Development</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Brainard</surname>
                           <given-names>Lael</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>LaFleur</surname>
                           <given-names>Vinca</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>The international development community as we have known it for sixty years is undergoing an extreme makeover. If its roots go back to the Marshall Plan and the founding of the Bretton Woods institutions, its modern incarnation has branched both up and out—dramatically altering the landscape of humanity’s efforts to alleviate poverty.</p>
                     <p>During the postcolonial era of giving in the 1960s and 1970s, roughly thirty-eight official bilateral and multilateral donors annually disbursed an average of $43 billion in assistance (in 2005 dollars). Today, hundreds of development entities are spread across a larger group of countries, annually disbursing $158 billion</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Fighting Global Poverty:</title>
                     <subtitle>Who Will Be Relevant in 2020?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bishop</surname>
                           <given-names>Matthew</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>42</fpage>
                  <abstract>
                     <p>For a White house summit, it was quite a party. The president was late arriving to give his concluding remarks, so his place at the podium was taken by Yvonne Chaka Chaka, an African business woman, motivational speaker, UN goodwill ambassador, and singer. As she started to sing her latest campaign song, her fellow summiteers, in their formal business attire, seemed unclear how to behave. Then an elderly lady stood up and started to dance. But would anyone join her?</p>
                     <p>Finally, a middle-aged man in a suit got up and started to waltz with the elderly lady. Then Eyitayo Lambo,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The New Reality of Aid</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kharas</surname>
                           <given-names>Homi</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>With great fanfare, the leaders of the Group of Eight (G-8) countries reaffirmed at Heiligendamm in June 2007 that they would meet their commitments to increase aid by $50 billion by 2010, with half going to Africa. When this pledge was first announced at Gleneagles in 2005, it was hailed as a breakthrough by aid advocates. Bono gushed that “I would not say this is the end of extreme poverty, but it is the beginning of the end.” Now, two years later, with the same commitments on the table, the tone is different. Bono has accused the rich countries of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Angelina, Mia, and Bono:</title>
                     <subtitle>Celebrities and International Development</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>West</surname>
                           <given-names>Darrell M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>74</fpage>
                  <abstract>
                     <p>It is the Age of Celebrity in the United States. Glamorous movie stars run for elective office and win. Former politicians play fictional characters on television shows. Rock stars and actresses raise money for a variety of humanitarian causes. Musicians, athletes, and artists speak out on issues of hunger, stem cell research, international development, and foreign policy.</p>
                     <p>Although the contemporary era is not the first time celebrities have spoken out on questions of public policy, a number of factors in the current period have given celebrities a far greater voice in civic affairs. The culture has changed in ways that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Is There a Constituency for Global Poverty?</title>
                     <subtitle>Jubilee 2000 and the Future of Development Advocacy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Busby</surname>
                           <given-names>Joshua</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>In September 2000, the Irish rock star Bono met with North Carolina’s Senator Jesse Helms and urged the conservative head of the Senate Foreign Relations Committee to support developing country debt relief on behalf of Jubilee 2000, a London-based transnational campaign that sought to eliminate the debts of the world’s poorest countries in time for the new millennium. Helms was known for equating foreign aid with throwing money down “ratholes.” Bono claimed that Helms wept when they spoke: “I talked to him about the biblical origin of the idea of Jubilee Year…. He was genuinely moved by the story of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Nigeria’s Fight for Debt Relief:</title>
                     <subtitle>Tracing the Path</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Okonjo-Iweala</surname>
                           <given-names>Ngozi</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>On June 29, 2005, Nigeria and the Paris Club reached a historic agreement on an $18 billion (or 60 percent) write-off of Nigeria’s Paris Club debt. The write-off was implemented from October 2005 to March 2006, bringing to a close Nigeria’s long quest for a Paris Club debt agreement. This chapter traces the background, issues, and elements that led to this success, including the implicit and explicit roles played by various parties, such as civil society. What were the ingredients for success, and how can the lessons learned be applied to other countries or other similar campaigns? The story of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Philanthropy and Enterprise:</title>
                     <subtitle>Harnessing the Power of Business and Social Entrepreneurship for Development</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dees</surname>
                           <given-names>J. Gregory</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>120</fpage>
                  <abstract>
                     <p>More than two hundred years after Adam Smith penned these words, his wisdom on this matter is being challenged by social entrepreneurs and enterprising philanthropists who are deliberately using business ventures to serve the public good. George Soros candidly describes his own change of heart on this matter: “Where I have modified my stance is with regard to social entrepreneurship. I used to be negative toward it because of my innate aversion to mixing business with philanthropy. Experience has taught me that I was wrong. As a philanthropist, I saw a number of successful social enterprises, and I became engaged</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Leveraging Knowledge to End Poverty</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Khosla</surname>
                           <given-names>Ashok</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>Over the past couple of centuries, the world as a whole has made undeniable, and often quite dramatic, “progress” on many fronts. People in scores of countries have attained unprecedented levels of health, wealth, and knowledge. Diseases that were for millennia the scourges of whole nations have been conquered. Food production has grown to levels inconceivable even a few decades ago. An ever-growing range of products from industry is accessible to an ever growing range of customers. In addition, cheap sources of energy have made possible facilities for travel and communication that enable large numbers of people to live a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Effecting Change through Accountable Channels</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nelson</surname>
                           <given-names>Jane</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>During the 1970s, 70 percent of resource flows from the United States to developing countries originated from the U.S. government in the form of official development assistance. Today, private capital from American citizens, residents, and companies accounts for more than 80 percent of such flows.¹ These private resources are being channeled to developing countries through a combination of foreign direct investment and portfolio investment, commercial bank loans, remittances, nongovernmental organizations (NGOs), religious groups, universities and colleges, foundations, and corporate philanthropy. Together with new approaches to official development assistance, they are changing the face of America’s engagement in international development. Similar</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Collaborative Governance:</title>
                     <subtitle>The New Multilateralism for the Twenty-First Century</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zadek</surname>
                           <given-names>Simon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>An era of globalization, international development, and multilateralism as practiced for over half a century is over, for both better and worse. As we bid it a fond farewell, we might reflect for a moment on four emergent challenges we face in reinventing our global community to effectively address the imperative of sustainable development:</p>
                     <p>—<italic>Fractured multilateralism:</italic>a fracturing, perhaps fatally, of an inherited twentieth-century multilateralism and associated institutional arrangements. Visible is the decline in effectiveness of the United Nations, including its once-powerful cousins, the World Bank and the International Monetary Fund, in sustaining a reasonable universal consent over what constitutes</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Aid:</title>
                     <subtitle>From Consensus to Competition?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>OʹKeefe</surname>
                           <given-names>Joseph</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>201</fpage>
                  <abstract>
                     <p>Aid is more fragmented today than ever before. Bilateral and multilateral aid agencies have proliferated in recent years: The average number of donors per recipient country rose from about 12 in the 1960s to about 33 in the 2001–5 period, according to World Bank research. In addition, there are currently more than 230 international aid organizations, funds, and programs. This growth has been accompanied by ever more earmarking of aid resources for specific, narrow uses or special purpose organizations, including global programs or so-called vertical funds. In fact, about half the official development assistance (ODA) channeled through multilateral channels</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Philanthropy, Aid, and Investment:</title>
                     <subtitle>Alignment for Impact</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kramer</surname>
                           <given-names>Mark R.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>216</fpage>
                  <abstract>
                     <p>The recently released United Nations 2007<italic>Update on Africa offers</italic>a sobering conclusion: “At the midway point between their adoption in 2000 and the 2015 target date for achieving the Millennium Development Goals, Sub-Saharan Africa is not on track to achieve any of the goals.”¹</p>
                     <p>This conclusion is especially discouraging in light of the unprecedented level of attention that major philanthropists, celebrities, academics, corporations, and governments have in recent years focused on Africa’s urgent and crushing needs. Why has so much money, publicity, and goodwill produced such limited effects? One reason, I suggest, is the lack of collaboration, or even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.17</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>225</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>231</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wph4s.19</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
