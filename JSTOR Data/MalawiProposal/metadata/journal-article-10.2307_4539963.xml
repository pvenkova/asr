<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">divedist</journal-id>
         <journal-id journal-id-type="jstor">j100916</journal-id>
         <journal-title-group>
            <journal-title>Diversity and Distributions</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Science</publisher-name>
         </publisher>
         <issn pub-type="ppub">13669516</issn>
         <issn pub-type="epub">14724642</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4539963</article-id>
         <article-categories>
            <subj-group>
               <subject>Biodiversity Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title> Bryophyte Diversity and Distribution along an Altitudinal Gradient on a Lava Flow in La Réunion </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Claudine Ah-Peng</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Min Chuah-Petiot</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Blandine</given-names>
                  <surname>Descamps-Julien</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Jacques</given-names>
                  <surname>Bardat</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Pierre Stamenoff</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Dominique</given-names>
                  <surname>Strasberg</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">13</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id>i405969</issue-id>
         <fpage>654</fpage>
         <lpage>662</lpage>
         <page-range>654-662</page-range>
         <permissions>
            <copyright-statement>Copyright 2007 Blackwell Publishing</copyright-statement>
         </permissions>
         <self-uri xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1111/j.1472-4642.2007.00393.x"
                   xlink:title="an external site"/>
         <abstract>
            <p> Non-vascular plant distribution patterns were examined in three microhabitats along an altitudinal gradient on a recent lava flow of the Piton de la Fournaise volcano (La Réunion, Mascarene archipelago). The uniform nature of the lava flow provides an excellent system to study the relationship between altitude and species diversity and distribution, and at the same time avoiding confusing multiple effects of substrate and vegetation heterogeneity. Non-vascular plants were surveyed with quadrats within an altitudinal range from 250 m to 850 m a.s.l. Fine-scale variations in bryophyte communities between three ecological microhabitats (the ground and on the rachises of two fern species) were investigated. Three specific questions were addressed: (1) What is the species diversity of bryophyte communities on a 19-year-old lava flow? (2) How does altitude influence the diversity and distribution of bryophytes on a lava flow? (3) Does microhabitat variation control bryophyte diversity? In our study, bryophyte diversity increased with altitude. Unexpectedly, species richness was very high; 70 species of bryophytes were recorded including nine new records for the island. Diversity was also controlled by ecological microhabitats. Bryophyte species were structured into six categories according to altitude and microhabitat preferences. Results suggested that the high diversity of these cryptic organisms on this lava flow is fostered in part by their host substrate and their adaptative strategies on new substrates. On a broader scale, it was concluded that lava flows as primary mineral environments are important to conserve, as they support a high diversity of pioneer organisms that constitute the early stages of the development of La Réunion's remnant lowland rainforest. </p>
         </abstract>
         <kwd-group>
            <kwd>Altitude</kwd>
            <kwd>Liverwort</kwd>
            <kwd>Microhabitat</kwd>
            <kwd>Moss</kwd>
            <kwd>Oceanic Islands</kwd>
            <kwd>Piton de la Fournaise Volcano</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d120e311a1310">
            <mixed-citation id="d120e315" publication-type="journal">
Acebey, A., Gradstein, S.R. &amp;amp; Krömer, T. (2003) Species richness
and habitat diversification of bryophytes in submontane rain forest
and fallows of Bolivia. Journal of Tropical Ecology, 19, 9-18.<person-group>
                  <string-name>
                     <surname>Acebey</surname>
                  </string-name>
               </person-group>
               <fpage>9</fpage>
               <volume>19</volume>
               <source>Journal of Tropical Ecology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d120e350a1310">
            <mixed-citation id="d120e354" publication-type="journal">
Ah-Peng, C. &amp;amp; Bardat, J. (2005) Check list of the bryophytes of
Réunion Island (France). Tropical Bryology, 26, 89-118.<person-group>
                  <string-name>
                     <surname>Ah-Peng</surname>
                  </string-name>
               </person-group>
               <fpage>89</fpage>
               <volume>26</volume>
               <source>Tropical Bryology</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e386a1310">
            <mixed-citation id="d120e390" publication-type="journal">
Ah-Peng, C., Bardat, J. &amp;amp; Ellis, L.T. (2005) Additions to the bryo-
flora of Réunion Island (France). Lindbergia, 30, 43-45.<person-group>
                  <string-name>
                     <surname>Ah-Peng</surname>
                  </string-name>
               </person-group>
               <fpage>43</fpage>
               <volume>30</volume>
               <source>Lindbergia</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e422a1310">
            <mixed-citation id="d120e426" publication-type="journal">
Andrew, N.R., Rodgerson, L. &amp;amp; Dunlop, M. (2003) Variation in
invertebrate-bryophyte community structure at different
spatial scales along altitudinal gradients. Journal of Biogeo-
graphy, 30, 731-746.<object-id pub-id-type="jstor">10.2307/3554484</object-id>
               <fpage>731</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e450a1310">
            <mixed-citation id="d120e454" publication-type="journal">
Axmacher, J.C., Holtmann, G., Scheuermann, L., Brehm, G.,
Müller-Hohenstein, K. &amp;amp; Fiedler, K. (2004) Diversity of
geometrid moths (Lepidoptera: Geometridae) along an
Afrotropical elevational rainforest transect. Diversity and
Distribution, 10, 293-302.<person-group>
                  <string-name>
                     <surname>Axmacher</surname>
                  </string-name>
               </person-group>
               <fpage>293</fpage>
               <volume>10</volume>
               <source>Diversity and Distribution</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d120e495a1310">
            <mixed-citation id="d120e499" publication-type="book">
Barcelo, A. (1996) Analyse des mécanismes hydrologiques sur
domaine volcanique insulaire tropical à relief jeune. Apports à
la connaissance du bilan hydrique. Massif du Piton de la
Fournaise (île de la Réunion). Université de Montpellier II,
Montpellier, 270p.<person-group>
                  <string-name>
                     <surname>Barcelo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Analyse des mécanismes hydrologiques sur domaine volcanique insulaire tropical à relief jeune</comment>
               <fpage>270</fpage>
               <source>Apports à la connaissance du bilan hydrique. Massif du Piton de la Fournaise (île de la Réunion)</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d120e540a1310">
            <mixed-citation id="d120e544" publication-type="journal">
Bhattarai, K.R., Vetaas, O.R. &amp;amp; Grytnes, J.A. (2004) Fern species
richness along a central Himalayan elevational gradient, Nepal.
Journal of Biogeography, 31, 389-400.<object-id pub-id-type="jstor">10.2307/3554728</object-id>
               <fpage>389</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e563a1310">
            <mixed-citation id="d120e567" publication-type="journal">
Botes, A., McGeoch, M.A., Robertson, H.G., van Niekerk, A.,
Davids, H.P. &amp;amp; Chown, S.L. (2006) Ants, altitude and change
in the northern Cape Floristic Region. Journal of Biogeography,
33, 71-90.<object-id pub-id-type="jstor">10.2307/3554862</object-id>
               <fpage>71</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e590a1310">
            <mixed-citation id="d120e594" publication-type="journal">
Bruun, H.H., Moen, J., Risto, V., Grytnes, J.-A., Oksanen, L. &amp;amp;
Angerbjörn, A. (2006) Effects of altitude and topography on
species richness of vascular plants, bryophytes and lichens in
alpine communities. Journal of Vegetation, 17, 37-46.<person-group>
                  <string-name>
                     <surname>Bruun</surname>
                  </string-name>
               </person-group>
               <fpage>37</fpage>
               <volume>17</volume>
               <source>Journal of Vegetation</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d120e632a1310">
            <mixed-citation id="d120e636" publication-type="journal">
Delorme, H., Bachèlery, P., Blum, P.A., Cheminée, J.L., Delarue,
J.F., Delmond, J.C., Hirn, A., Lepine, J.C., Vincent, P.M. &amp;amp;
Zlotnicki, J. (1989) March 1986 eruptive episodes at Piton de la
Fournaise volcano (Réunion Island). Journal of Volcanology
and Geothermal Research, 36, 199-208.<person-group>
                  <string-name>
                     <surname>Delorme</surname>
                  </string-name>
               </person-group>
               <fpage>199</fpage>
               <volume>36</volume>
               <source>Journal of Volcanology and Geothermal Research</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d120e678a1310">
            <mixed-citation id="d120e682" publication-type="journal">
Frahm, J.-P. (1987) Struktur und Zusammensetzung der
Epiphytishen Moosvegetation in Regenwäldern NO-Perus.
Beiheft Nova Hedwigia, 88, 115-141.<person-group>
                  <string-name>
                     <surname>Frahm</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>88</volume>
               <source>Beiheft Nova Hedwigia</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d120e717a1310">
            <mixed-citation id="d120e721" publication-type="journal">
Frahm, J.-P. (1990) The altitudinal zonation of bryophytes on
Mt. Kinabalu. Nova Hedwigia, 51, 133-149.<person-group>
                  <string-name>
                     <surname>Frahm</surname>
                  </string-name>
               </person-group>
               <fpage>133</fpage>
               <volume>51</volume>
               <source>Nova Hedwigia</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d120e753a1310">
            <mixed-citation id="d120e757" publication-type="journal">
Frahm, J.-P. (1994) Scientific results of the BRYOTROP Expedi-
tion to Zaire and Rwanda 2. The altitudinal zonation of the
bryophytes on Mt. Kahuzi, Zaïre. Tropical Bryology, 9, 153-167.<person-group>
                  <string-name>
                     <surname>Frahm</surname>
                  </string-name>
               </person-group>
               <fpage>153</fpage>
               <volume>9</volume>
               <source>Tropical Bryology</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d120e792a1310">
            <mixed-citation id="d120e796" publication-type="journal">
Frahm, J.-P. &amp;amp; Ohlemüller, R. (2001) Ecology of bryophytes
along altitudinal and latitudinal gradients in New Zealand.
Studies in austral temperate rain forest bryophytes 15. Tropical
Bryology, 20, 117-137.<person-group>
                  <string-name>
                     <surname>Frahm</surname>
                  </string-name>
               </person-group>
               <fpage>117</fpage>
               <volume>20</volume>
               <source>Tropical Bryology</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d120e834a1310">
            <mixed-citation id="d120e838" publication-type="journal">
Fulford, M., Crandall, B. &amp;amp; R.S. (1971) The ecology of an elfin
forest in Puerto Rico, 15. A study of the leafy hepatic flora of
the Luquillo Mountains. Journal of the Arnold Arboretum, 52,
435-458.<person-group>
                  <string-name>
                     <surname>Fulford</surname>
                  </string-name>
               </person-group>
               <fpage>435</fpage>
               <volume>52</volume>
               <source>Journal of the Arnold Arboretum</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d120e876a1310">
            <mixed-citation id="d120e880" publication-type="journal">
Gradstein, S.R. &amp;amp; Frahm, J.-P. (1987) Die Floristische Höheng-
liederung der Moose entlang des BRYOTROP-transektes in
NO Perus. Beiheft Nova Hedwigia, 88, 195-113.<person-group>
                  <string-name>
                     <surname>Gradstein</surname>
                  </string-name>
               </person-group>
               <fpage>195</fpage>
               <volume>88</volume>
               <source>Beiheft Nova Hedwigia</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d120e916a1310">
            <mixed-citation id="d120e920" publication-type="book">
Gradstein, S.R. &amp;amp; Pócs, T. (1989) Bryophytes. Tropical rainforest
ecosystems: biogeographical and ecological studies (ed. by World
Eot), pp. 311-325. Elsevier, Amsterdam.<person-group>
                  <string-name>
                     <surname>Gradstein</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Bryophytes</comment>
               <fpage>311</fpage>
               <source>Tropical rainforest ecosystems: biogeographical and ecological studies</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d120e955a1310">
            <mixed-citation id="d120e959" publication-type="journal">
Kaboli, M., Guillaumet, A. &amp;amp; Prodon, R. (2006) Avifaunal gradi-
ents in two arid zones of central Iran in relation to vegetation,
climate, and topography. Journal of Biogeography, 33, 133-144.<object-id pub-id-type="jstor">10.2307/3554867</object-id>
               <fpage>133</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e978a1310">
            <mixed-citation id="d120e982" publication-type="journal">
Kessler, M. (2000) Altitudinal zonation of Andean cryptogam
communities. Journal of Biogeography, 27, 275-282.<object-id pub-id-type="jstor">10.2307/2656259</object-id>
               <fpage>275</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e998a1310">
            <mixed-citation id="d120e1002" publication-type="journal">
Lassau, S.C., Hochuli, D.F., Cassis, G. &amp;amp; Reid, C.A.M. (2005)
Effects on habitat complexity on forest beetle diversity: do
functional groups respond consistently? Diversity and Distri-
bution, 11, 73-82.<person-group>
                  <string-name>
                     <surname>Lassau</surname>
                  </string-name>
               </person-group>
               <fpage>73</fpage>
               <volume>11</volume>
               <source>Diversity and Distribution</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1040a1310">
            <mixed-citation id="d120e1044" publication-type="journal">
Lomolino, M.V. (2001) Elevation gradients of species richness,
historical and prospective views. Global Ecology and Biogeography,
10, 3-13.<object-id pub-id-type="jstor">10.2307/2665396</object-id>
               <fpage>3</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1063a1310">
            <mixed-citation id="d120e1067" publication-type="book">
Magurran, A.E. (1988) Ecological diversity and its measurement.
Croom-Helm, Cambridge, MA.<person-group>
                  <string-name>
                     <surname>Magurran</surname>
                  </string-name>
               </person-group>
               <source>Ecological diversity and its measurement</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1093a1310">
            <mixed-citation id="d120e1097" publication-type="journal">
McCain, C.M. (2004) The mid-domain effect applied to eleva-
tional gradients: species richness of small mammals in Costa
Rica. Journal of Biogeography, 31, 19-31.<object-id pub-id-type="jstor">10.2307/3554687</object-id>
               <fpage>19</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1116a1310">
            <mixed-citation id="d120e1120" publication-type="journal">
Mishler, B.D. (2001) The biology of bryophytes - Bryophytes
aren&amp;apos;t just small tracheophytes. American Journal of Botany, 88,
2129-2131.<object-id pub-id-type="doi">10.2307/3558438</object-id>
               <fpage>2129</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1139a1310">
            <mixed-citation id="d120e1143" publication-type="journal">
Myers, N., Mittermeier, R.A., Mittermeier, C.G., da Fonseca,
G.A.B. &amp;amp; Kent, J. (2000) Biodiversity hotspots conservation
priorities. Nature, 403, 853-858.<person-group>
                  <string-name>
                     <surname>Myers</surname>
                  </string-name>
               </person-group>
               <fpage>853</fpage>
               <volume>403</volume>
               <source>Nature</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1178a1310">
            <mixed-citation id="d120e1182" publication-type="journal">
O&amp;apos;Shea, B.J. (2003) Checklist of the mosses of sub-Saharan Africa
(version 4, 12/03). Tropical Bryology Research Reports, 4, 1-176.<person-group>
                  <string-name>
                     <surname>O&amp;apos;Shea</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>4</volume>
               <source>Tropical Bryology Research Reports</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1214a1310">
            <mixed-citation id="d120e1218" publication-type="journal">
Pfeiffer, T. (2003) Terricolous bryophyte vegetation of New Zealand
temperate rain forests - Communities, adaptative strategies
and divergence patterns. Bryophyterum Bibliotheca, 59, 1-147.<person-group>
                  <string-name>
                     <surname>Pfeiffer</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>59</volume>
               <source>Bryophyterum Bibliotheca</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1253a1310">
            <mixed-citation id="d120e1257" publication-type="journal">
Pharo, E.J. &amp;amp; Beattie, A.J. (2002) The association between sub-
strate variability and bryophyte and lichen diversity in eastern
Australian forests. Bryologist, 105, 11-26.<object-id pub-id-type="jstor">10.2307/3244817</object-id>
               <fpage>11</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1277a1310">
            <mixed-citation id="d120e1281" publication-type="journal">
Pharo, E.J., Beattie, A.J. &amp;amp; Binns, D. (1999) Vascular plants
Diversity as a surrogate for bryophyte and lichen diversity.
Conservation Biology, 13, 282-292.<object-id pub-id-type="jstor">10.2307/2641470</object-id>
               <fpage>282</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1300a1310">
            <mixed-citation id="d120e1304" publication-type="book">
Pócs, T. (1994) The altitudinal distribution of Kilimanjaro bryo-
phytes. XIIIth plenary meeting AETFAT (ed. by J.H. Seyani &amp;amp;
A.C. Chikuni), pp. 797-812. Association for the taxonomic
study of the flora of Tropical Africa, Zomba, Malawi.<person-group>
                  <string-name>
                     <surname>Pócs</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The altitudinal distribution of Kilimanjaro bryophytes</comment>
               <fpage>797</fpage>
               <source>XIIIth plenary meeting AETFAT</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1342a1310">
            <mixed-citation id="d120e1346" publication-type="journal">
Rahbek, C. (2005) The role of spatial scale and the perception of
large-scale species-richness pattern. Ecology Letters, 8, 224-239.<person-group>
                  <string-name>
                     <surname>Rahbek</surname>
                  </string-name>
               </person-group>
               <fpage>224</fpage>
               <volume>8</volume>
               <source>Ecology Letters</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1378a1310">
            <mixed-citation id="d120e1384" publication-type="journal">
Raj Bhattarai, K. &amp;amp; Vetaas, O.R. (2003) Variation in plant species
richness of different life forms along a subtropical gradient in
the Himalayas, east Nepal. Global Ecology and Biodiversity, 12,
327-340.<person-group>
                  <string-name>
                     <surname>Raj Bhattarai</surname>
                  </string-name>
               </person-group>
               <fpage>327</fpage>
               <volume>12</volume>
               <source>Global Ecology and Biodiversity</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1422a1310">
            <mixed-citation id="d120e1426" publication-type="journal">
Robyns, W. (1932) La colonisation végétale des laves récentes
du volcan Rumoka. Institut Royal Colonial Belge, Section Des
Sciences Naturelle et Médicale, 1, 3-33.<person-group>
                  <string-name>
                     <surname>Robyns</surname>
                  </string-name>
               </person-group>
               <fpage>3</fpage>
               <volume>1</volume>
               <source>Institut Royal Colonial Belge, Section Des Sciences Naturelle et Médicale</source>
               <year>1932</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1461a1310">
            <mixed-citation id="d120e1465" publication-type="journal">
Romero, C., Putz, F.E. &amp;amp; Kitajima, K. (2006) Ecophysiology in
relation to exposure of pendant epiphytic bryophytes in the
canopy of a tropical montane oak forest. Biotropica, 38, 35-41.<person-group>
                  <string-name>
                     <surname>Romero</surname>
                  </string-name>
               </person-group>
               <fpage>35</fpage>
               <volume>38</volume>
               <source>Biotropica</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1501a1310">
            <mixed-citation id="d120e1505" publication-type="journal">
Sánchez-González, A. &amp;amp; Löpez-Mata, L. (2005) Plant species
richness and diversity along an altitudinal gradient in the Sierra
Nevada, Mexico. Diversity and Distribution, 11, 567-575.<person-group>
                  <string-name>
                     <surname>Sánchez-González</surname>
                  </string-name>
               </person-group>
               <fpage>567</fpage>
               <volume>11</volume>
               <source>Diversity and Distribution</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1540a1310">
            <mixed-citation id="d120e1544" publication-type="journal">
Senbeta, F., Schmitt, C., Denich, M., Demissew, S., Vlek, P.L.G.,
Preisinger, H., Woldemariam, T. &amp;amp; Teketay, D. (2005) The
diversity and distribution of lianas in the Afromontane rain
forests of Ethiopia. Diversity and Distribution, 11, 443-452.<person-group>
                  <string-name>
                     <surname>Senbeta</surname>
                  </string-name>
               </person-group>
               <fpage>443</fpage>
               <volume>11</volume>
               <source>Diversity and Distribution</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1582a1310">
            <mixed-citation id="d120e1586" publication-type="journal">
Skottsberg, C. (1941) Plant succession on recent lava flows in the
island of Hawaii. Vetenskap-och Vitterhetssamhalles Handlinger
Sjatte Fojden Series B. Bd.1, 8, 32.<person-group>
                  <string-name>
                     <surname>Skottsberg</surname>
                  </string-name>
               </person-group>
               <fpage>8</fpage>
               <volume>1</volume>
               <source>Vetenskap-och Vitterhetssamhalles Handlinger Sjatte Fojden Series B</source>
               <year>1941</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1621a1310">
            <mixed-citation id="d120e1625" publication-type="journal">
Smathers, G.A. &amp;amp; Mueller-Dombois, D. (1974) Invasion and
recovery of vegetation after a volcanic eruption in Hawaii.
National Park Service Scientific Monograph Series, 5, 1-129.<person-group>
                  <string-name>
                     <surname>Smathers</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>5</volume>
               <source>National Park Service Scientific Monograph Series</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1660a1310">
            <mixed-citation id="d120e1664" publication-type="journal">
Stevens, G.C. (1989) The latitudinal gradient in geographical
range: how so many species coexist in the tropics. American
Naturalist, 133, 240-256.<object-id pub-id-type="jstor">10.2307/2462300</object-id>
               <fpage>240</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1683a1310">
            <mixed-citation id="d120e1687" publication-type="journal">
Stevens, G.C. (1992) The elevational gradient in altitudinal
range an extension of Rapoport&amp;apos;s latitudinal rule to altitude.
American Naturalist, 140, 893-911.<object-id pub-id-type="jstor">10.2307/2462925</object-id>
               <fpage>893</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1707a1310">
            <mixed-citation id="d120e1711" publication-type="journal">
Strasberg, D., Rouget, M., Richardson, D.M., Baret, S., Dupont, J.
&amp;amp; Cowling, R.M. (2005) An assessment of habitat diversity
and transformation on la Réunion Island (Mascarene Islands,
Indian Ocean) as a basis for identifying broad scale con-
servation priorities. Biodiversity and Conservation, 14,
3015-3032.<person-group>
                  <string-name>
                     <surname>Strasberg</surname>
                  </string-name>
               </person-group>
               <fpage>3015</fpage>
               <volume>14</volume>
               <source>Biodiversity and Conservation</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1755a1310">
            <mixed-citation id="d120e1759" publication-type="book">
Team, R.D.C (2006) R: A language and environment for statistical
computing. R.2.2.1, R Foundation for Statistical Computing,
Vienna, Austria.<person-group>
                  <string-name>
                     <surname>Team</surname>
                  </string-name>
               </person-group>
               <source>R: A language and environment for statistical computing</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1788a1310">
            <mixed-citation id="d120e1792" publication-type="journal">
Van Reenen, G.B.A. &amp;amp; Gradstein, S.R. (1983) Studies on Colom-
bian Cryptogams XX. A transect analysis of the bryophytes vegeta-
tion along an altitudinal gradient on the Sierra Nevada de Santa
Marta, Colombia. Acta Botanica Neerlandica, 32, 163-175.<person-group>
                  <string-name>
                     <surname>Van Reenen</surname>
                  </string-name>
               </person-group>
               <fpage>163</fpage>
               <volume>32</volume>
               <source>Acta Botanica Neerlandica</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1830a1310">
            <mixed-citation id="d120e1834" publication-type="journal">
Van Reenen, G.B.A. &amp;amp; Gradstein, S.R. (1984) An investigation of
bryophytes distribution and ecology along a altitudinal gradient
in the Andes of Colombia. Journal of the Hattori Botanical
Laboratory, 56, 79-84.<person-group>
                  <string-name>
                     <surname>Van Reenen</surname>
                  </string-name>
               </person-group>
               <fpage>79</fpage>
               <volume>56</volume>
               <source>Journal of the Hattori Botanical Laboratory</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1872a1310">
            <mixed-citation id="d120e1876" publication-type="journal">
Vitousek, P.M. (1993) Mauna Loa volcano: the &amp;apos;white rat&amp;apos; of eco-
systems studies. Stanford News, http://news-service.stanford.edu/
pr/93/931206Arc3012.html<person-group>
                  <string-name>
                     <surname>Vitousek</surname>
                  </string-name>
               </person-group>
               <source>Stanford News</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1905a1310">
            <mixed-citation id="d120e1909" publication-type="journal">
Whittaker, R.J., Willis, K.J. &amp;amp; Field, R. (2001) Scale and species
richness: towards a general, hierarchical theory of species
diversity. Journal of Biogeography, 28, 453-470.<object-id pub-id-type="jstor">10.2307/827394</object-id>
               <fpage>453</fpage>
            </mixed-citation>
         </ref>
         <ref id="d120e1929a1310">
            <mixed-citation id="d120e1933" publication-type="book">
Wigginton, M.J. (2004) Checklist and distribution of the liver-
worts and hornworts of Sub-Saharan Africa, including the East
African Islands (edition 2, September 2004). Tropical Bryology
Research Reports, 5, 1-102.<person-group>
                  <string-name>
                     <surname>Wigginton</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <fpage>1</fpage>
               <volume>5</volume>
               <source>Checklist and distribution of the liverworts and hornworts of Sub-Saharan Africa, including the East African Islands</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d120e1974a1310">
            <mixed-citation id="d120e1978" publication-type="book">
Zar, J.H. (1999) Biostatistical analysis, 4th edn. Prentice Hall,
Englewood Cliffs, NJ.<person-group>
                  <string-name>
                     <surname>Zar</surname>
                  </string-name>
               </person-group>
               <edition>4</edition>
               <source>Biostatistical analysis</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
