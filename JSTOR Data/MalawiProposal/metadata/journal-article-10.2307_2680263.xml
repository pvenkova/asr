<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">evolution</journal-id>
         <journal-id journal-id-type="jstor">j100004</journal-id>
         <journal-title-group>
            <journal-title>Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society for the Study of Evolution</publisher-name>
         </publisher>
         <issn pub-type="ppub">00143820</issn>
         <issn pub-type="epub">15585646</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2680263</article-id>
         <title-group>
            <article-title>A Single Ancient Origin of Brood Parasitism in African Finches: Implications for Host-Parasite Coevolution</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Michael D.</given-names>
                  <surname>Sorenson</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Robert B.</given-names>
                  <surname>Payne</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">55</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id>i200280</issue-id>
         <fpage>2550</fpage>
         <lpage>2567</lpage>
         <page-range>2550-2567</page-range>
         <permissions>
            <copyright-statement>Copyright 2001 The Society for the Study of Evolution</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2680263"/>
         <abstract>
            <p>Robust phylogenies for brood-parasitic birds, their hosts, and nearest nesting relatives provide the framework to address historical questions about host-parasite coevolution and the origins of parasitic behavior. We tested phylogenetic hypotheses for the two genera of African brood-parasitic finches, Anomalospiza and Vidua, using mitochondrial DNA sequence data from 43 passeriform species. Our analyses strongly support a sister relationship between Vidua and Anomalospiza, leading to the conclusion that obligate brood parasitism evolved only once in African finches rather than twice, as has been the conventional view. In addition, the parasitic finches (Viduidae) are not recently derived from either weavers (Ploceidae) or grassfinches (Estrildidae), but represent a third distinct lineage. Among these three groups, the parasitic finches and estrildids, which includes the hosts of all 19 Vidua species, are sister taxa in all analyses of our full dataset. Many characters shared by Vidua and estrildids, including elaborate mouth markings in nestlings, unusual begging behavior, and immaculate white eggs, can therefore be attributed to common ancestry rather than convergent evolution. The host-specificity of mouth mimicry in Vidua species, however, is clearly the product of subsequent host-parasite coevolution. The lineage leading to Anomalospiza switched to parasitizing more distantly related Old World warblers (Sylviidae) and subsequently lost these characteristics. Substantial sequence divergence between Vidua and Anomalospiza indicates that the origin of parasitic behavior in this clade is ancient (∼20 million years ago), a striking contrast to the recent radiation of extant Vidua. We suggest that the parasitic finch lineage has experienced repeated cycles of host colonization, speciation, and extinction through their long history as brood parasites and that extant Vidua species represent only the latest iterations of this process. This dynamic process may account for a significantly faster rate of DNA sequence evolution in parasitic finches as compared to estrildids and other passerines. Our study reduces by one the tally of avian lineages in which obligate brood parasitism has evolved and suggests an origin of parasitism that involved relatively closely related species likely to accept and provide appropriate care to parasitic young. Given the ancient origin of parasitism in African finches, ancestral estrildids must have been parasitized well before the diversification of extant Vidua, suggesting a long history of coevolution between these lineages preceding more recent interactions between specific hosts and parasites.</p>
         </abstract>
         <kwd-group>
            <kwd>Base Composition</kwd>
            <kwd>Brood Parasitism</kwd>
            <kwd>Mitochondrial DNA</kwd>
            <kwd>Optimization Alignment</kwd>
            <kwd>Passeriformes</kwd>
            <kwd>Phylogeny</kwd>
            <kwd>Vidua</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
</article>
