<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plantandsoil</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50010412</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant and Soil</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0032079X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15735036</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24125098</article-id>
         <title-group>
            <article-title>Enhanced nitrogen mineralization in mowed or glyphosate treated cover crops compared to direct incorporation</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>S.S.</given-names>
                  <surname>Snapp</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>H.</given-names>
                  <surname>Borden</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">270</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24115781</issue-id>
         <fpage>101</fpage>
         <lpage>112</lpage>
         <permissions>
            <copyright-statement>© 2005 Springer</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24125098"/>
         <abstract>
            <p>Information on how management by mowing and herbicide alter residue quality and nitrogen (N) inputs would be valuable to improve prediction of N availability. Mowing and glyphosate application are widely used by growers to limit cover crop growth and facilitate incorporation. A mixture of cover crops, hairy vetch (Vicia villosa L.), oriental mustard (Brassica juncea L.) and cereal rye (Secale cereale L.), was investigated as a means to improve soil quality and optimize N availability. There is limited information on how mowing or glyphosate application influence cover crop decomposition and N mineralization from these heterogeneous residues. A rye cover crop was grown in the field over the winter and transferred to containers as an intact soil profile to conduct a greenhouse study. Management treatments (mowing and glyphosate) were imposed eight days before incorporation. Plant and soil N dynamics were monitored. The experiment was repeated with the addition of a tri-mixture cover crop. Inorganic $\mathrm{N}{\mathrm{O}}_{3}^{-}$ in bare soil ranged from 6 to 10 μg N g soil-1 over 39 days. Similar or lower levels of soil $\mathrm{N}{\mathrm{O}}_{3}^{-}$ were observed after rye residue incorporation, from 2 to 6 μg N g-1; consistent with N-immobilization. Application of untreated, mixed cover crop residues generally was associated with higher levels of soil inorganic $\mathrm{N}{\mathrm{O}}_{3}^{-}$, from 3 to 11 μg N g-1. For both rye and mixed residues, management by mowing or glyphosate enhanced N mineralization by 10 to 100%, compared to untreated residues. At the same time, application of mowing or glyphosate 8 days before cover crop incorporation seemed to reduce the amount of residues by about half compared to untreated controls. Belowground biomass was reduced more than aboveground, although recovery of senescent roots may have been incomplete. Management by glyphosate or mowing enhanced soil inorganic N availability in the short-term while simultaneously reducing carbon and N inputs.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1e214a1310">
            <mixed-citation id="d1e218" publication-type="other">
Biederbeck V O, Bouman O T, Campbell C A, Bailey L D and
Winkleman G E 1996 Nitrogen benefits from four green-manure
legumes in dryland cropping systems. Can. J. Plant Sei. 76,
307-315.</mixed-citation>
         </ref>
         <ref id="d1e234a1310">
            <mixed-citation id="d1e238" publication-type="other">
Bremner, J M and Mulvaney C S 1982 Total nitrogen. Methods of
soil analysis. Part 2. Chemical and microbiological properties.
Am. Soc. Agronomy- Soil Sei. Soc. of Am., Madison, WI.</mixed-citation>
         </ref>
         <ref id="d1e251a1310">
            <mixed-citation id="d1e255" publication-type="other">
Cabrera M L, Vigil M F and Kissel D E 1994 Potential nitro-
gen mineralization: Laboratory and field evaluation. Soil testing:
Prospects for Improving Nutrient Recommendations. Ed. Havlin
J Spec Publ. 40, 15-30 Soil Sei. Soc. of Am., Madison, WI.</mixed-citation>
         </ref>
         <ref id="d1e271a1310">
            <mixed-citation id="d1e275" publication-type="other">
Carlson S J and Donald W 1988 Glyphosate effects on Canada this-
tle (Cirisium Aravense) roots, root buds and shoots. Weed Res.
28, 37—45.</mixed-citation>
         </ref>
         <ref id="d1e289a1310">
            <mixed-citation id="d1e293" publication-type="other">
Clark A J, Decker A M and Meisinger J J 1994 Seeding rate and
kill date effects on hairy vetch-cereal rye cover crop mixtures for
corn production. Agron. J. 86, 1065-1070.</mixed-citation>
         </ref>
         <ref id="d1e306a1310">
            <mixed-citation id="d1e310" publication-type="other">
Drinkwater L D, Janke R R and Rossoni-Longnecker L 2000 Ef-
fects of tillage intensity on nitrogen dynamics and productivity
in legume-based grain systems. Plant Soil 227, 99-113.</mixed-citation>
         </ref>
         <ref id="d1e323a1310">
            <mixed-citation id="d1e327" publication-type="other">
Franzluebbers K, Weaver R W and Juo A S R 1994 Mineralization
of labelled-N from cowpea (Vigna unguiculatal L. Walp) plant
parts at two growth stages in sandy soil. Plant Soil 174,255-277.</mixed-citation>
         </ref>
         <ref id="d1e340a1310">
            <mixed-citation id="d1e344" publication-type="other">
Goins G D and Russelle M P 1996 Fine root demography in alfalfa
(Medicago sativa L.). Plant Soil 185, 281-291.</mixed-citation>
         </ref>
         <ref id="d1e354a1310">
            <mixed-citation id="d1e358" publication-type="other">
Harris G H and Hesterman O B 1990 Quantifying the nitrogen
contribution from alfalfa to soil and two succeeding crops using
nitrogen-15. Agron. J. 82, 129-134.</mixed-citation>
         </ref>
         <ref id="d1e371a1310">
            <mixed-citation id="d1e375" publication-type="other">
Honeycutt C W 1999 Nitrogen mineralization from soil organic
matter and crop residues: field validation of laboratory predic-
tions. Soil Sei Soc. Am. J. 63, 134-141.</mixed-citation>
         </ref>
         <ref id="d1e389a1310">
            <mixed-citation id="d1e393" publication-type="other">
Kenney D R and Nelson D W 1982 Nitrogen-Inorganic Forms. In
Methods of Soil Analysis. Part 2, 2nd edition. Eds. A L Page, R
H Miller and D R Kenney. pp. 643-698. Agron. Soc. Am. and
Soil Sei. Soc. Am., Madison, WI.</mixed-citation>
         </ref>
         <ref id="d1e409a1310">
            <mixed-citation id="d1e413" publication-type="other">
Kiute A 1986 Water retention: Laboratory methods In Methods of
Soil Analysis. Part 1, 2nd edition. Ed. A Klute. pp. 635-662.
Agron. Soc. Am. and Soil Sei. Soc. Am., Madison, WI.</mixed-citation>
         </ref>
         <ref id="d1e426a1310">
            <mixed-citation id="d1e430" publication-type="other">
Mackie-Dawson L A 1999 Nitrogen uptake and root morphological
responses of defoliated Lolium perenne (L.) to a heterogeneous
nitrogen supply. Plant Soil 209, 111-118.</mixed-citation>
         </ref>
         <ref id="d1e443a1310">
            <mixed-citation id="d1e447" publication-type="other">
Malpassi R N, Kaspar T C, Parkin T B, Cambardella and Nubel
N A 2000 Oat and rye root decomposition effects on nitrogen
mineralization. Soil Sei. Soc. Am. J. 64, 208-215.</mixed-citation>
         </ref>
         <ref id="d1e460a1310">
            <mixed-citation id="d1e464" publication-type="other">
McMichael B L and Burke I J 1996 Temperature effects on root
growth. In Plant Roots: The Hidden Half, 2nd Ed.</mixed-citation>
         </ref>
         <ref id="d1e474a1310">
            <mixed-citation id="d1e478" publication-type="other">
Mutch D R and Snapp S S 2003 Cover crop choices for Michigan.
Michigan State University Extension Bulletin E2884.</mixed-citation>
         </ref>
         <ref id="d1e489a1310">
            <mixed-citation id="d1e493" publication-type="other">
Puget P and Drinkwater L E 2001 Short-term dynamics of root and
shoot-derived carbon from a leguminous green manure. Soil Sei.
Soc. Am. J. 65, 771-779.</mixed-citation>
         </ref>
         <ref id="d1e506a1310">
            <mixed-citation id="d1e510" publication-type="other">
Rasse P D, Smucker A J M and Schabenberger O 1999 Modifica-
tions of soil nitrogen pools in response to alfalfa root systems
and shoot mulch. Agron. J. 91, 471-477.</mixed-citation>
         </ref>
         <ref id="d1e523a1310">
            <mixed-citation id="d1e527" publication-type="other">
Rosencrance R C, McCarty G W, Shelton D R and Teasdale J R 2000
Denitrification and N mineralization from hairy vetch (Vicia vil-
losa Roth) and rye (Secale cereale L.) cover crop monocultures
and bicultures. Plant Soil 227, 283-290.</mixed-citation>
         </ref>
         <ref id="d1e543a1310">
            <mixed-citation id="d1e547" publication-type="other">
Sanchez J E, Paul E A, Willson T C, Smeenk J and Harwood R R
2002 Corn root effects on the nitrogen-supplying capacity of a
conditioned soil. Agronomy J. 94, 391-396.</mixed-citation>
         </ref>
         <ref id="d1e560a1310">
            <mixed-citation id="d1e564" publication-type="other">
Sanchez J E, Willson T C, Kizilkaya K, Parker E and Harwood R
R 2001 Enhancing the mineralizable nitrogen pool through sub-
strate diversity in long term cropping systems. Soil Sei. Soc. Am.
J. 65, 1442-1447.</mixed-citation>
         </ref>
         <ref id="d1e580a1310">
            <mixed-citation id="d1e584" publication-type="other">
Schmidt W E, Myers D K and Van Keuren R W 2001 Value of
Legumes for Plowdown Nitrogen. The Ohio State University
Extension Factsheet. AGF111-01.</mixed-citation>
         </ref>
         <ref id="d1e598a1310">
            <mixed-citation id="d1e602" publication-type="other">
Snapp S S, Borden H and Rohrbach D D 2001 Improving nitro-
gen efficiency: lessons from Malawi and Michigan, pp. 42-
48. In Optimizing nitrogen management in food and energy
production and environmental protection. Eds. J Galloway, E
Cowling, J W Erisman, J Wisnieswki and C Jordan. Second
International Nitrogen Conference Papers, A.A. Balkema Pub.
Lisse/Abingdon/Exton/Tokyo.</mixed-citation>
         </ref>
         <ref id="d1e628a1310">
            <mixed-citation id="d1e632" publication-type="other">
Snapp S S, Koide R and Lynch J P 1995 Exploitation of localized
P-patches by common bean roots. Plant Soil 177, 211-218.</mixed-citation>
         </ref>
         <ref id="d1e642a1310">
            <mixed-citation id="d1e646" publication-type="other">
Stanford G and Smith S J 1972 Nitrogen mineralization potentials
of soils. Soil Sei. Soc. Am. Proc. 36, 465—471.</mixed-citation>
         </ref>
         <ref id="d1e656a1310">
            <mixed-citation id="d1e660" publication-type="other">
Swift M J, Heal O W and Anderson J M 1979 Decomposing in ter-
restrial ecosystems. Studies in Ecology, 5. Blackwell Sei. Publ.,
Oxford.</mixed-citation>
         </ref>
         <ref id="d1e673a1310">
            <mixed-citation id="d1e677" publication-type="other">
Thorup-Kristensen K and Nielsen N E 1998 Modelling and measur-
ing the effect of nitrogen catch crops on the nitrogen supply for
succeeding crops. Plant Soil 203, 79-89.</mixed-citation>
         </ref>
         <ref id="d1e690a1310">
            <mixed-citation id="d1e694" publication-type="other">
Trinsoutrot I, Recous S, Bentz B, Linères M, Chèneby D and Nico-
lardot B 2000 Biochemical quality of crop residues and carbon
and nitrogen mineralization kinetics under non-limiting nitrogen
conditions. Soil Sei Soc. Am. J. 64, 918-926.</mixed-citation>
         </ref>
         <ref id="d1e711a1310">
            <mixed-citation id="d1e715" publication-type="other">
Vaughan J D and Evanylo G K 1998 Com response to cover
crop species, spring desiccation time and residue management.
Agron. J. 90, 536-544.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
