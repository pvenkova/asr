<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envicons</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020266</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environmental Conservation</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CAMBRIDGE UNIVERSITY PRESS</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03768929</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694387</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44519372</article-id>
         <title-group>
            <article-title>Forced migration, environmental change and woodfuel issues in the Senegal River Valley</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>RICHARD</given-names>
                  <surname>BLACK</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>MOHAMED F.</given-names>
                  <surname>SESSAY</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">24</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40189755</issue-id>
         <fpage>251</fpage>
         <lpage>260</lpage>
         <permissions>
            <copyright-statement>© Foundation for Environmental Conservation 1997</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44519372"/>
         <abstract>
            <p>There is increasing international concern about the environmental impacts of refugees on host areas, with governments calling for compensation for environmental damage, particularly concerning the loss of woodland resources as a result of demand for wood for fuel. In addition to an obvious increase in the population of host areas, concern about refugees' woodfueluse centres on the notion that they are 'exceptional resource degraders'. Since they view their stay as temporary, it is argued, they therefore do not have any incentive to use resources in a way that is sustainable in the long-term. This study examined refugee migration to the middle valley of the Senegal River, and compared woodfuel use by refugee and local populations. Drawing on a household survey and direct measurement of woodfuel use, little or no evidence is found to support the expectation that refugees use more wood for fuel than local people, or that they are more destructive in their collection or use of wood. This is important since it suggests that policy measures developed to reduce what is perceived as excess demand by refugees, notably through the introduction of fuel-efficient stoves, are unlikely to be successful. Reforestation schemes have been relatively unsuccessful in addressing supply or demand for wood.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1267e181a1310">
            <mixed-citation id="d1267e185" publication-type="other">
Adams, A. (1977) Le Long Voyage des Gens du Fleuve. Paris, France:
L'Harmattan: 222 pp.</mixed-citation>
         </ref>
         <ref id="d1267e195a1310">
            <mixed-citation id="d1267e199" publication-type="other">
Allan, N. (1987) Impact of Afghan refugees on the vegetation re-
sources of Pakistan's Hindukush-Himalaya. Mountain Research
and Development 7: 200-4.</mixed-citation>
         </ref>
         <ref id="d1267e212a1310">
            <mixed-citation id="d1267e216" publication-type="other">
Agnew, C. (1995) Desertification, drought and development in the
Sahel. In: People and Environment in Africa, ed. T. Binns, pp.
137-50. Chichester, UK: John Wiley &amp; Sons.</mixed-citation>
         </ref>
         <ref id="d1267e229a1310">
            <mixed-citation id="d1267e233" publication-type="other">
Barnes, D.F. (1993) The design and diffusion of improved cooking
stoves. World Bank Research Observer 8: 119-41.</mixed-citation>
         </ref>
         <ref id="d1267e244a1310">
            <mixed-citation id="d1267e248" publication-type="other">
Bernard, C. (1993) Les débuts de la politique de reboisement dans
la vallée du fleuve Sénégal. Revue Française d'Histoire d'Outre-Mer
80: 49-82.</mixed-citation>
         </ref>
         <ref id="d1267e261a1310">
            <mixed-citation id="d1267e265" publication-type="other">
Betlem, J. (1988) Le changement de la couverture arborée des forêts
de Gonakier sur l'Ile à Morphil entre 1954 and 1986. Notes
Techniques No. 7. République Sénégal, MPN, DCSR, Pavs-Bas,
DGCI.</mixed-citation>
         </ref>
         <ref id="d1267e281a1310">
            <mixed-citation id="d1267e285" publication-type="other">
Black, R. (1994a) Forced migration and environmental change: the
impact of refugees on host environments. Journal of
Environmental Management 24: 261-77.</mixed-citation>
         </ref>
         <ref id="d1267e298a1310">
            <mixed-citation id="d1267e302" publication-type="other">
Black, R. (1994 b) Environmental change in refugee-affected areas of
the Third World: the role of policy and research. Disasters 18:
107-16.</mixed-citation>
         </ref>
         <ref id="d1267e315a1310">
            <mixed-citation id="d1267e319" publication-type="other">
Black, R. &amp; Sessav, M.F. (1995) Refugees and environmental
change: the case of the Senegal River Valley. Unpublished report.
London, UK: King's College London: 38 pp.</mixed-citation>
         </ref>
         <ref id="d1267e332a1310">
            <mixed-citation id="d1267e336" publication-type="other">
Caminada, L. (1992) Nepal: Consultancy on Environmental Protection
and Rehabilitation in Refugee-hosting Areas. PTSS Mission
Report, 92/57. Geneva, Switzerland: UNHCR: 83 pp.</mixed-citation>
         </ref>
         <ref id="d1267e350a1310">
            <mixed-citation id="d1267e354" publication-type="other">
CARE/ODA (1994) Refugee inflow into Ngara and Karagwe
districts, Kagera Region, Tanzania. Environmental impact
assessment. Unpublished report. London, UK: CARE
International/Overseas Development Administration: 73 pp.</mixed-citation>
         </ref>
         <ref id="d1267e370a1310">
            <mixed-citation id="d1267e374" publication-type="other">
Conde, J. &amp; Diagne, P. (1986) South-north international migrations:
a case study. Malian, Mauritanian and Senegalese migrants from
the Senegal River Valley to France. Development Centre Papers.
Paris, France: OECD: 148 pp.</mixed-citation>
         </ref>
         <ref id="d1267e390a1310">
            <mixed-citation id="d1267e394" publication-type="other">
Crousse, B., Mathieu, P. &amp; Seck, S.M. (1991) La Vallée du Fleuve
Sénégal: Evaluations et Perspectives d'une Décenie d'Aménagements.
Paris, France: Karthala: 380 pp.</mixed-citation>
         </ref>
         <ref id="d1267e407a1310">
            <mixed-citation id="d1267e411" publication-type="other">
Daffe, M., Laura, P. &amp; Cisse, S. (1991) Étude de la problématique
du bois combustible dans le département de Podor. Rapport
Final. Unpublished report. SIC/SAS/ABF.MDRH, DEFCCS
and DCCE, Dakar, Senegal: 78 pp.</mixed-citation>
         </ref>
         <ref id="d1267e427a1310">
            <mixed-citation id="d1267e431" publication-type="other">
Dewees, P.A. (1989) The woodfuel crisis reconsidered: observations
on the dynamics of abundance and scarcity. World Development
17:1159-72.</mixed-citation>
         </ref>
         <ref id="d1267e444a1310">
            <mixed-citation id="d1267e448" publication-type="other">
Dregne, H.E. &amp; Tucker, C.J. (1988) Desert encroachment.
Desertification Control Bulletin 16: 16-19.</mixed-citation>
         </ref>
         <ref id="d1267e459a1310">
            <mixed-citation id="d1267e465" publication-type="other">
Dufournaud, C.M., Quinn, J.T. &amp; Harrington, J.J. (1994) A partial
equilibrium analysis of the impact of introducing more efficient
wood-burning stoves into households in the Sahelian region.
Environment and Planning A 26: 407-14</mixed-citation>
         </ref>
         <ref id="d1267e481a1310">
            <mixed-citation id="d1267e485" publication-type="other">
Eckholm, E.P. (1975) The other energy crisis: firewood.
Worldwatch Paper 1. Washington DC, USA: Worldwatch
Institute: 22 pp.</mixed-citation>
         </ref>
         <ref id="d1267e498a1310">
            <mixed-citation id="d1267e502" publication-type="other">
Foley, G., Moss, P. &amp; Timberlake, L. (1984) Stoves and Trees.
London, UK: Earthscan and International Institute for
Environment and Development: 85 pp.</mixed-citation>
         </ref>
         <ref id="d1267e515a1310">
            <mixed-citation id="d1267e519" publication-type="other">
French, D. (1986) Confronting an unsolvable problem: deforesta-
tion in Malawi. World Development 14: 531-40.</mixed-citation>
         </ref>
         <ref id="d1267e529a1310">
            <mixed-citation id="d1267e533" publication-type="other">
Hoerz, T. (1995a) The environment of refugee camps: a challenge
for refugees, local populations and aid agencies. Refugee
Participation Network 18: 17-19.</mixed-citation>
         </ref>
         <ref id="d1267e546a1310">
            <mixed-citation id="d1267e550" publication-type="other">
Hoerz, T. (1995 b) Refugees and host environments: a review of cur-
rent and related literature. Unpublished report. Oxford, UK:
Refugee Studies Programme: 121 pp.</mixed-citation>
         </ref>
         <ref id="d1267e564a1310">
            <mixed-citation id="d1267e568" publication-type="other">
Horowitz, M. (1991) Victims upstream and down. Journal of
Refugee Studies 4: 164-81.</mixed-citation>
         </ref>
         <ref id="d1267e578a1310">
            <mixed-citation id="d1267e582" publication-type="other">
Jacobsen, K. (1997) Refugees' environmental impact: the effect of
patterns of settlement. Journal of Refugee Studies 10: 19-36.</mixed-citation>
         </ref>
         <ref id="d1267e592a1310">
            <mixed-citation id="d1267e596" publication-type="other">
Ketel, H. (1994a) Tanzania: Environmental assessment report of
the Rwandese refugee camps and the affected local communities
in Kagera region. PTSS Mission Report, 94/29/N. Geneva,
Switzerland: UNHCR: 50 pp.</mixed-citation>
         </ref>
         <ref id="d1267e612a1310">
            <mixed-citation id="d1267e616" publication-type="other">
Ketel, H. (1994 b) Zaire: Environmental assessment report of the
Rwandese refugee camps and the immediate surrounding in
North and South Kivu. PTSS Mission Report, 94/62/N.
Geneva, Switzerland: UNHCR: 30 pp.</mixed-citation>
         </ref>
         <ref id="d1267e632a1310">
            <mixed-citation id="d1267e636" publication-type="other">
Kharoufi, M. (1994) Forced migration in the Senegalese-
Mauritanian conflict: consequences for the Senegal River valley.
In: Population Displacement and Resettlement: Development and
Conflict in the Middle East, ed. S. Shami, pp. 140-55. New York,
USA: Center for Migration Studies: xi + 326 pp.</mixed-citation>
         </ref>
         <ref id="d1267e655a1310">
            <mixed-citation id="d1267e659" publication-type="other">
Lamont-Gregory, E. (1995) The environment, cooking fuel and UN
Resolution 46/182. Refugee Participation Network 18: 14-16.</mixed-citation>
         </ref>
         <ref id="d1267e670a1310">
            <mixed-citation id="d1267e674" publication-type="other">
Le Breton, G. (1995) Stoves, trees and refugees: the fuelwood crisis
consortium in Zimbabwe. Refugee Participation Network 18: 9-
12.</mixed-citation>
         </ref>
         <ref id="d1267e687a1310">
            <mixed-citation id="d1267e691" publication-type="other">
Leach, G. &amp; Mearns, R. (1988) Beyond the Woodfuel Crisis: People,
Land and Trees in Africa. London: Earthscan, UK: vii + 309 pp.</mixed-citation>
         </ref>
         <ref id="d1267e701a1310">
            <mixed-citation id="d1267e705" publication-type="other">
Leach, M. (1992) Dealing with displacement: refugee-host re-
lations, food and forest resources in Sierra Leonean Mende com-
munities during the Liberian influx, 1990-91. IDS research
report, no. 22. Brighton, UK: Institute of Development Studies:
51 pp.</mixed-citation>
         </ref>
         <ref id="d1267e724a1310">
            <mixed-citation id="d1267e728" publication-type="other">
Lericollais, A. (1975) Peuplement et migrations dans la vallée du
Sénégal. Cahiers ORSTOM série Sciences Humaines 12: 123-36</mixed-citation>
         </ref>
         <ref id="d1267e738a1310">
            <mixed-citation id="d1267e742" publication-type="other">
Mbaye, S.M., Sv, F. &amp; Tandian, O. (1994) Familles réfugiés mau-
ritaniennes dans la vallée du fleuve Sénégal: stratégies de survie et
d'adaptation. Unpublished report. Dakar, Senegal: Haut
Comissariat des Nations-Unies pour les réfugiés, Délégation ré-
gionale pour l'Afrique Occidentale: 37 pp.</mixed-citation>
         </ref>
         <ref id="d1267e761a1310">
            <mixed-citation id="d1267e765" publication-type="other">
Mercer, D.E. &amp; Soussan, J. (1992) Fuelwood problems and
solutions. In: Managing the World's Forests: Looking for Balance
between Conservation and Development, ed. N.P. Sharma, pp.
177-213. Washington, DC, USA: World Bank: xxi + 605 pp.</mixed-citation>
         </ref>
         <ref id="d1267e782a1310">
            <mixed-citation id="d1267e786" publication-type="other">
Morgan, W.B. &amp; Moss, R.P. (1981) Woodfuel and Rural Energy
Production and Supply in the Humid Tropics. A Report for the
United Nations University with Special Reference to Tropical Africa
and South-East Asia. Dublin, Ireland: Tycoolv, for the United
Nations University: 224 pp.</mixed-citation>
         </ref>
         <ref id="d1267e805a1310">
            <mixed-citation id="d1267e809" publication-type="other">
Mortimore, M. (1989) Adapting to Drought: Farmers, Famines and
Desertification in West Africa. Cambridge, UK: Cambridge
University Press: xxii + 299 pp.</mixed-citation>
         </ref>
         <ref id="d1267e822a1310">
            <mixed-citation id="d1267e826" publication-type="other">
Oxfam (1995) The Oxfam Handbook of Development and Relief
Volumes 1-2. Oxford, UK: Oxfam: 1028 pp.</mixed-citation>
         </ref>
         <ref id="d1267e836a1310">
            <mixed-citation id="d1267e840" publication-type="other">
RDS (1992) Recensement général de la population et de l'habitat de
1988: rapport régional: Saint-Louis. Dakar, Senegal: République
du Sénégal, Ministère de l'Economie, de Finances et du Plan,
Direction de la Prévision et de la Statistique: 50 pp.</mixed-citation>
         </ref>
         <ref id="d1267e856a1310">
            <mixed-citation id="d1267e860" publication-type="other">
Sevestre, M-F., Thiadens, R. &amp; Guigou, P. (1989) Mission
HCR/BIT effectuée au Sénégal auprès des réfugiés mauritaniens
en vue de l'identification d'activités generatrices de revenus.
Unpublished report. Geneva, Switzerland: UNHCR: 37 pp.</mixed-citation>
         </ref>
         <ref id="d1267e876a1310">
            <mixed-citation id="d1267e880" publication-type="other">
Stiles, D. (1990) Ecological monitoring: the Senegal model.
Technical Publication Series No. 1. New York, USA: United
Nations Sudano-Sahelian Office: 28 pp.</mixed-citation>
         </ref>
         <ref id="d1267e894a1310">
            <mixed-citation id="d1267e898" publication-type="other">
Sylva, E. (1992) Reboisement: leurres et lueurs de la participative
populaire. Environnement Africain 31-32: 171-84.</mixed-citation>
         </ref>
         <ref id="d1267e908a1310">
            <mixed-citation id="d1267e912" publication-type="other">
Thiadens, R. (1992) Senegal: self-sufficiency programme for
Mauritanian refugees, PTSS Mission Report, 92/54. Geneva,
Switzerland: UNHCR: 42 pp.</mixed-citation>
         </ref>
         <ref id="d1267e925a1310">
            <mixed-citation id="d1267e931" publication-type="other">
Thomas, D.S. G. &amp; Middleton, N.J. (1994) Desertification:
Exploding the Myth. Chichester, UK: Wiley &amp; Sons: xiii +194 pp.</mixed-citation>
         </ref>
         <ref id="d1267e941a1310">
            <mixed-citation id="d1267e945" publication-type="other">
Toussaint, A., Ducenne, Q. &amp; Roulette, G. (1994) Projet de
restauration du milieu naturel dans le département de Podor,
République de Sénégal. Rapport de la première phase,
Unpublished report. Feldkirchen, Germany: Deutsche
Forstservice GmbH, for Direction des Eaux, Forêts et de la
Conservation des Sols, République de Sénégal, Dakar: 165 pp.</mixed-citation>
         </ref>
         <ref id="d1267e968a1310">
            <mixed-citation id="d1267e972" publication-type="other">
Tucker, C.J., Dregne, H.E. &amp; Newcomb, W.W. (1991) Expansion
and contraction of the Sahara desert from 1980 to 1990. Science
253: 299-301</mixed-citation>
         </ref>
         <ref id="d1267e985a1310">
            <mixed-citation id="d1267e989" publication-type="other">
United Nations High Commissioner for Refugees [UNHCR] (1995)
The State of the World's Refugees: in Search of Solutions. Oxford,
UK: Oxford University Press: 264 pp.</mixed-citation>
         </ref>
         <ref id="d1267e1003a1310">
            <mixed-citation id="d1267e1007" publication-type="other">
van Lavieren, B. &amp; van Wetten, J. (1990) Profil de l'Environnement
de la Vallée du Fleuve Sénégal. Dakar, Senegal: Euroconsult/
Institut National de Recherche pour la Conservation de la Nature:
68 pp.</mixed-citation>
         </ref>
         <ref id="d1267e1023a1310">
            <mixed-citation id="d1267e1027" publication-type="other">
von Maydell, H.-J. (1992) Arbres et Arbustes du Sahel: leurs
Caractéristiques et leurs Utilisations. Weikersheim, Germany:
GTZ/Verlag Josef Margraf Scientific Books: 531 pp.</mixed-citation>
         </ref>
         <ref id="d1267e1040a1310">
            <mixed-citation id="d1267e1044" publication-type="other">
Williams, M.A.J. &amp; Balling Jr., R.C. (1996) Interactions of
Desertification and Climate. London, UK: Arnold: xiv + 270pp.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
