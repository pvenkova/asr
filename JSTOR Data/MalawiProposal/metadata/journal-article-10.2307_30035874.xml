<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">imfstaffpapers</journal-id>
         <journal-id journal-id-type="jstor">j100845</journal-id>
         <journal-title-group>
            <journal-title>IMF Staff Papers</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Monetary Fund</publisher-name>
         </publisher>
         <issn pub-type="ppub">10207635</issn>
         <issn pub-type="epub">15645150</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30035874</article-id>
         <title-group>
            <article-title>Capital Account Liberalization and Economic Performance: Survey and Synthesis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Hali J.</given-names>
                  <surname>Edison</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Michael W.</given-names>
                  <surname>Klein</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Luca Antonio</given-names>
                  <surname>Ricci</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Torsten</given-names>
                  <surname>Sløk</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i30035871</issue-id>
         <fpage>220</fpage>
         <lpage>256</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 International Monetary Fund</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30035874"/>
         <abstract>
            <p>This paper surveys the literature on the effects of capital account openness and stock market liberalization on economic growth and provides a synthesis in which we reconcile some of the different results presented in the literature. Various empirical measures used to gauge the presence of controls on capital account transactions and the liberalization of equity markets are discussed. We compare detailed measures of capital account controls that attempt to capture the intensity of enforcement with other indicators that simply capture whether controls are present. A detailed review of the literature is followed by an empirical section in which we trace the divergence in published results to differences in country coverage, sample periods, indicators of liberalization, and control variables across studies. Specifically, we show that when an institutional variable such as government reputation is added to the specification, the significance of capital account openness vanishes. Also, we demonstrate that enriching the specification by allowing for nonlinearities helps explain why different studies that ignore the nonlinear nature of the relationship find different results.</p>
         </abstract>
         <kwd-group>
            <kwd>F32</kwd>
            <kwd>F33</kwd>
            <kwd>F36</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d176e299a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d176e306" publication-type="other">
Report of the Managing Director to the International Monetary and Financial
Committee on Progress in Strengthening the Architecture of the International Financial System and Reform
of the IMF (IMF, September 19, 2000; available via the Internet at: http://www.imf org/external/np/omd/
2000/02/report.htm)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e322a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d176e329" publication-type="other">
Foreign Affairs, Bhagwati (1998, p. 7)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e336a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d176e343" publication-type="other">
Annual Report on Exchange Arrangements and Exchange Restrictions</mixed-citation>
            </p>
         </fn>
         <fn id="d176e350a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d176e357" publication-type="other">
Grilli and Milesi-Ferretti (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e363" publication-type="other">
Rodrik (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e369" publication-type="other">
Klein
and Olivei (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e380a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d176e387" publication-type="other">
Montiel (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e393" publication-type="other">
Montiel and Reinhart (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e400a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d176e407" publication-type="other">
Montiel and Reinhart (1999, notes to Table 3, p. 628).</mixed-citation>
            </p>
         </fn>
         <fn id="d176e414a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d176e421" publication-type="other">
page 533 of Henry (2000a)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e427" publication-type="other">
Economist Intelligence Unit's Quarterly Economic Report, "Appendix 1: Chronological
Listing of Major Policy Events in Developing Countries." Available via the Internet at: http://faculty-
gsb.stanford.edu/henry/personal/homepage.htm.</mixed-citation>
            </p>
         </fn>
         <fn id="d176e440a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d176e447" publication-type="other">
Henry (2000a)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e454a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d176e461" publication-type="other">
Montiel (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e468a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d176e475" publication-type="other">
Obstfeld (1986)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e481" publication-type="other">
Bayoumi (1990)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e489a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d176e496" publication-type="other">
IMF's October 2001 World Economic Outlook</mixed-citation>
            </p>
         </fn>
         <fn id="d176e503a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d176e510" publication-type="other">
Prasad and others (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e516" publication-type="other">
Lane and Milesi-Ferretti (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d176e523a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d176e530" publication-type="other">
Table 4, p. 537</mixed-citation>
            </p>
         </fn>
         <fn id="d176e537a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d176e544" publication-type="other">
Acemoglu and others (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e550" publication-type="other">
Easterly and Levine (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e556" publication-type="other">
Rodrik, Subramanian,
and Trebbi (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d176e566" publication-type="other">
IMF (2003)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d176e582a1310">
            <mixed-citation id="d176e586" publication-type="other">
Acemoglu, Daron, Simon Johnson, James A. Robinson, Yunyong Thaicharoen, 2003, "Institutional
Causes, Macroeconomic Symptoms: Volatility, Crises, and Growth," Journal of Monetary
Economics, Vol. 50 (January), pp. 49-123.</mixed-citation>
         </ref>
         <ref id="d176e599a1310">
            <mixed-citation id="d176e603" publication-type="other">
Ahearne, Alan, William Griever, and Francis Warnock, 2004, "Information Costs and Home Bias:
An Analysis of U.S. Holdings of Foreign Equities," Journal of International Economics,
Vol. 62 (March), pp. 313-36.</mixed-citation>
         </ref>
         <ref id="d176e616a1310">
            <mixed-citation id="d176e620" publication-type="other">
Arteta, Carlos, Barry Eichengreen, and Charles Wyplosz, 2001, "On the Growth Effects of Capital
Account Liberalization" (unpublished; Berkeley, California: University of California).</mixed-citation>
         </ref>
         <ref id="d176e630a1310">
            <mixed-citation id="d176e634" publication-type="other">
Bailliu, Jeannine, 2000, "Private Capital Flows, Financial Development, and Economic Growth
in Developing Countries," Bank of Canada Working Paper No. 2000-15 (Ontario: Bank of
Canada).</mixed-citation>
         </ref>
         <ref id="d176e648a1310">
            <mixed-citation id="d176e652" publication-type="other">
Bayoumi, Tamim, 1990, "Saving-Investment Correlations: Immobile Capital, Government Policy,
or Endogenous Behavior," Staff Papers, International Monetary Fund, Vol. 37, No. 2 (June),
pp. 360-87.</mixed-citation>
         </ref>
         <ref id="d176e665a1310">
            <mixed-citation id="d176e669" publication-type="other">
Bekaert, Geert, 1995, "Market Integration and Investment Barriers in Emerging Equity Markets,"
World Bank Economic Review, Vol. 9 (January), pp. 75-107.</mixed-citation>
         </ref>
         <ref id="d176e679a1310">
            <mixed-citation id="d176e683" publication-type="other">
---, and Campbell Harvey, 1995, "Time-Varying World Market Integration," Journal of
Finance, Vol. 50 (June), pp. 403-44.</mixed-citation>
         </ref>
         <ref id="d176e693a1310">
            <mixed-citation id="d176e697" publication-type="other">
---, 2000, "Foreign Speculators and Emerging Equity Markets," Journal of Finance,
Vol. 55 (April), pp. 565-613.</mixed-citation>
         </ref>
         <ref id="d176e707a1310">
            <mixed-citation id="d176e711" publication-type="other">
---, and Christian Lundblad, 2001, "Does Financial Liberalization Spur Growth?" NBER
Working Paper No. 8245 (Cambridge, Massachusetts: National Bureau of Economic
Research).</mixed-citation>
         </ref>
         <ref id="d176e724a1310">
            <mixed-citation id="d176e728" publication-type="other">
Bhagwati, Jagdish, 1998, "The Capital Myth: The Difference between Trade in Widgets and
Trade in Dollars," Foreign Affairs, Vol. 77 (May/June), pp. 7-12.</mixed-citation>
         </ref>
         <ref id="d176e739a1310">
            <mixed-citation id="d176e743" publication-type="other">
Chanda, Areendam, 2001, "The Influence of Capital Controls on Long-Run Growth: Where
and How Much?" (unpublished; Providence, Rhode Island: Brown University).</mixed-citation>
         </ref>
         <ref id="d176e753a1310">
            <mixed-citation id="d176e757" publication-type="other">
Easterly, William, and Ross Levine, 2003, "Tropics, Germs, and Crops: How Endowments
Influence Economic Development," Journal of Monetary Economics, Vol. 50 (January),
pp. 3-39.</mixed-citation>
         </ref>
         <ref id="d176e770a1310">
            <mixed-citation id="d176e774" publication-type="other">
Edison, Hali J., and Francis E. Warnock, 2003, "A Simple Measure of the Intensity of Capital
Controls," Journal of Empirical Finance, Vol. 10, pp. 81-103.</mixed-citation>
         </ref>
         <ref id="d176e784a1310">
            <mixed-citation id="d176e788" publication-type="other">
Edison, Hali J., Ross Levine, Luca Ricci, and Torsten Slck, 2002, "International Financial
Integration and Economic Growth," Journal of International Money and Finance, Vol. 21
(November), pp. 749-76.</mixed-citation>
         </ref>
         <ref id="d176e801a1310">
            <mixed-citation id="d176e805" publication-type="other">
Edwards, Sebastian, 2001, "Capital Mobility and Economic Performance: Are Emerging
Economies Different?" NBER Working Paper No. 8076 (Cambridge, Massachusetts:
National Bureau of Economic Research).</mixed-citation>
         </ref>
         <ref id="d176e818a1310">
            <mixed-citation id="d176e822" publication-type="other">
Eichengreen, Barry, 2001, "Capital Account Liberalization: What Do Cross-Country Studies
Tell Us?" World Bank Economic Review, Vol. 15, No. 3, pp. 341-65.</mixed-citation>
         </ref>
         <ref id="d176e833a1310">
            <mixed-citation id="d176e837" publication-type="other">
Feldstein, Martin, and Charles Horioka, 1980, "Domestic Saving and International Capital
Flows," Economic Journal, Vol. 90, No. 358, pp. 314-29.</mixed-citation>
         </ref>
         <ref id="d176e847a1310">
            <mixed-citation id="d176e851" publication-type="other">
Grilli, Vittorio, and Gian Maria Milesi-Ferretti, 1995, "Economic Effects and Structural
Determinants of Capital Controls," Staff Papers, International Monetary Fund, Vol. 42,
No. 3, pp. 517-51.</mixed-citation>
         </ref>
         <ref id="d176e864a1310">
            <mixed-citation id="d176e868" publication-type="other">
Henry, Peter Blair, 2000a, "Stock Market Liberalization, Economic Reform, and Emerging
Market Equity Prices," Journal of Finance, Vol. 55, No. 2 (April), pp. 529-64.</mixed-citation>
         </ref>
         <ref id="d176e878a1310">
            <mixed-citation id="d176e882" publication-type="other">
---, 2000b, "Do Stock Market Liberalizations Cause Investment Booms?" Journal of
Financial Economics, Vol. 58, Nos. 1-2 (October), pp. 301-34.</mixed-citation>
         </ref>
         <ref id="d176e892a1310">
            <mixed-citation id="d176e896" publication-type="other">
International Monetary Fund, 2001, World Economic Outlook, October 2001: A Survey by the
Staff of the International Monetary Fund, World Economic and Financial Surveys, Chapter 4
(Washington: International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d176e909a1310">
            <mixed-citation id="d176e913" publication-type="other">
---, 2003, World Economic Outlook, April 2003: A Survey by the Staff of the International
Monetary Fund, World Economic and Financial Surveys, Chapter 3 (Washington: Inter-
national Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d176e927a1310">
            <mixed-citation id="d176e931" publication-type="other">
---, Annual Report on Exchange Arrangements and Exchange Restrictions (Washington:
International Monetary Fund), various issues.</mixed-citation>
         </ref>
         <ref id="d176e941a1310">
            <mixed-citation id="d176e945" publication-type="other">
Klein, Michael W., 2003, "Capital Account Openness and the Varieties of Growth Experience,"
NBER Working Paper No. 9500 (Cambridge, Massachusetts: National Bureau of Economic
Research).</mixed-citation>
         </ref>
         <ref id="d176e958a1310">
            <mixed-citation id="d176e962" publication-type="other">
---, and Giovanni Olivei, 1999, "Capital Account Liberalization, Financial Depth and
Economic Growth," NBER Working Paper No. 7384 (Cambridge, Massachusetts: National
Bureau of Economic Research).</mixed-citation>
         </ref>
         <ref id="d176e975a1310">
            <mixed-citation id="d176e979" publication-type="other">
Knack, Stephen, and Philip Keefer, 1995, "Institutions and Economic Performance: Cross-
Country Tests Using Alternative Institutional Measures," Economics and Politics, Vol. 7,
No. 3, pp. 207-27.</mixed-citation>
         </ref>
         <ref id="d176e992a1310">
            <mixed-citation id="d176e996" publication-type="other">
Kraay, Aart, 1998, "In Search of the Macroeconomic Effects of Capital Account Liberalization"
(unpublished; Washington: World Bank).</mixed-citation>
         </ref>
         <ref id="d176e1006a1310">
            <mixed-citation id="d176e1010" publication-type="other">
Lane, Philip, and Gian Maria Milesi-Ferretti, 2001, "The External Wealth of Nations: Measures
of Foreign Assets and Liabilities for Industrial and Developing Nations," Journal of Inter-
national Economics, Vol. 55, No. 2, pp. 263-94.</mixed-citation>
         </ref>
         <ref id="d176e1024a1310">
            <mixed-citation id="d176e1028" publication-type="other">
---, 2003, "International Financial Integration," IMF Staff Papers, Vol. 50 (Special Issue),
pp. 82-113.</mixed-citation>
         </ref>
         <ref id="d176e1038a1310">
            <mixed-citation id="d176e1042" publication-type="other">
Levine, Ross, and Sara Zervos, 1998, "Stock Markets, Banks, and Economic Growth," American
Economic Review, Vol. 88 (June), pp. 537-58.</mixed-citation>
         </ref>
         <ref id="d176e1052a1310">
            <mixed-citation id="d176e1056" publication-type="other">
Montiel, Peter, 1996, "Managing Economic Policy in the Face of Large Capital Inflows: What
Have We Learned?" in Private Capital Flows to Emerging Markets After the Mexican Crisis,
ed. by G. Calvo and M. Goldstein (Washington: Institute for International Economics),
pp. 189-218.</mixed-citation>
         </ref>
         <ref id="d176e1072a1310">
            <mixed-citation id="d176e1076" publication-type="other">
---, and Carmen Reinhart, 1999, "Do Capital Controls and Macroeconomic Policies
Influence the Volume and Composition of Capital Flows? Evidence from the 1990s," Journal
of International Money and Finance, Vol. 18, No. 4, pp. 619-35.</mixed-citation>
         </ref>
         <ref id="d176e1089a1310">
            <mixed-citation id="d176e1093" publication-type="other">
Obstfeld, Maurice, 1986, "Capital Mobility in the World Economy: Theory and Measurement,"
in Carnegie-Rochester Conference Series on Public Policy, Vol. 24, ed. by K. Brunner and
A. Meltzer (Spring), pp. 55-103.</mixed-citation>
         </ref>
         <ref id="d176e1106a1310">
            <mixed-citation id="d176e1110" publication-type="other">
O'Donnell, Barry, 2001, "Financial Openness and Economic Performance" (unpublished;
Dublin, Ireland: Trinity College).</mixed-citation>
         </ref>
         <ref id="d176e1121a1310">
            <mixed-citation id="d176e1125" publication-type="other">
Prasad, Eswar, Kenneth Rogoff, Shang-Jin Wei, and Ayhan Kose, 2003, Effects of Financial
Globalization on Developing Countries: Some Empirical Evidence, IMF Occasional Paper
No. 220 (Washington: International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d176e1138a1310">
            <mixed-citation id="d176e1142" publication-type="other">
Quinn, Dennis, 1997, "The Correlates of Change in International Financial Regulation,"
American Political Science Review, Vol. 91, No. 3 (September), pp. 531-51.</mixed-citation>
         </ref>
         <ref id="d176e1152a1310">
            <mixed-citation id="d176e1156" publication-type="other">
Rodrik, Dani, 1998, "Who Needs Capital-Account Convertibility?" in Should the IMF Pursue
Capital Account Convertibility ? Essays in International Finance No. 207, May, ed. by Stanley
Fischer and others (Princeton, New Jersey: Princeton University, Department of Economics,
International Finance Section).</mixed-citation>
         </ref>
         <ref id="d176e1172a1310">
            <mixed-citation id="d176e1176" publication-type="other">
---, 1999, The New Global Economy and Developing Countries: Making Openness
Work, Overseas Development Council, Policy Essay No. 24 (Washington: Johns Hopkins
University Press).</mixed-citation>
         </ref>
         <ref id="d176e1189a1310">
            <mixed-citation id="d176e1193" publication-type="other">
---, Arvind Subramanian, and Francesco Trebbi, 2002, "Institutions Rule: The Primacy of
Institutions Over Geography and Integration in Economic Development," IMF Working
Paper 02/189 (Washington: International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d176e1206a1310">
            <mixed-citation id="d176e1210" publication-type="other">
Sachs, Jeffrey, and Andrew Warner, 1995, "Economic Reform and the Process of Global
Integration," Brookings Papers on Economic Activity: 1 (Washington: Brookings Institution).</mixed-citation>
         </ref>
         <ref id="d176e1221a1310">
            <mixed-citation id="d176e1225" publication-type="other">
Summers, Lawrence, 2000, "International Financial Crises: Causes, Prevention, and Cures,"
American Economic Review, Vol. 90, No. 2 (May), pp. 1-16.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
