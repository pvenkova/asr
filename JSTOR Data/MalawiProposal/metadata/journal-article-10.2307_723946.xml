<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">africanaffairs</journal-id>
         <journal-id journal-id-type="jstor">j100046</journal-id>
         <journal-title-group>
            <journal-title>African Affairs</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal African Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">00019909</issn>
         <issn pub-type="epub">14682621</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">723946</article-id>
         <title-group>
            <article-title>From Neglect to 'Virtual Engagement': The United States and Its New Paradigm for Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Chris</given-names>
                  <surname>Alden</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">99</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">396</issue>
         <issue-id>i228974</issue-id>
         <fpage>355</fpage>
         <lpage>371</lpage>
         <page-range>355-371</page-range>
         <permissions>
            <copyright-statement/>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/723946"/>
         <abstract>
            <p>The promotion of a new American relationship with Africa is one of the Clinton Administration's most visible initiatives in the foreign policy sphere. From the White House summit on Africa to the highly publicized presidential tour of the continent, the US government has sought to demonstrate its commitment to reversing the pattern of neglect that characterized relations in the post-colonial period. The new interest in Africa expressed by Washington is complemented by the concurrent launching of a domestic initiative that targets the development of a 'new constituency for Africa' aimed at linking local American interest groups with foreign policy concerns. Taken together, these initiatives purport to lay the foundation for an enduring partnership with commercial and political depth that will ensure that African issues become a perennial feature of the American political landscape. However, in spite of this overt commitment to activism, there is much in the formation of the new relationship that is suggestive of the continued limits of American interests in the continent.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d116e121a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d116e128" publication-type="book">
Peter Schraeder, United States Foreign Policy Toward Africa: Incrementalism, crisis and
change (Cambridge University Press, Cambridge, 1994), pp. 5-8<person-group>
                     <string-name>
                        <surname>Schraeder</surname>
                     </string-name>
                  </person-group>
                  <fpage>5</fpage>
                  <source>United States Foreign Policy Toward Africa: Incrementalism, crisis and change</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e156" publication-type="book">
Sanford Ungar, 'US policy
toward Africa', in Robert Art and Seyom Brown (eds), US Foreign Policy: The search for a new
role (Macmillan, Basingstoke, 1993), pp. 383-4<person-group>
                     <string-name>
                        <surname>Ungar</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">US policy toward Africa</comment>
                  <fpage>383</fpage>
                  <source>US Foreign Policy: The search for a new role</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e191a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d116e198" publication-type="journal">
Bill Martin, 'Waiting for Oprah and the new US constituency for Africa', Review ofAfrican
Political Economy 75, 25 (1998), pp. 10-11<person-group>
                     <string-name>
                        <surname>Martin</surname>
                     </string-name>
                  </person-group>
                  <issue>25</issue>
                  <fpage>10</fpage>
                  <volume>75</volume>
                  <source>Review ofAfrican Political Economy</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e233a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d116e240" publication-type="book">
Steve Hurst, The Foreign Policy of the Bush Administration: In search of a new world order
(Pinter, London, 1999)<person-group>
                     <string-name>
                        <surname>Hurst</surname>
                     </string-name>
                  </person-group>
                  <source>The Foreign Policy of the Bush Administration: In search of a new world order</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e265a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d116e272" publication-type="journal">
Michael Mandelbaum, 'Foreign policy as social work', Foreign Affairs, January/February
(1996), p. 20<person-group>
                     <string-name>
                        <surname>Mandelbaum</surname>
                     </string-name>
                  </person-group>
                  <issue>January</issue>
                  <fpage>20</fpage>
                  <source>Foreign Affairs</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e305a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d116e312" publication-type="journal">
Jim Cason, 'The US: backing out of Africa', Review of African Political Economy 76, 24
(1997), p. 152<person-group>
                     <string-name>
                        <surname>Cason</surname>
                     </string-name>
                  </person-group>
                  <issue>24</issue>
                  <fpage>152</fpage>
                  <volume>76</volume>
                  <source>Review of African Political Economy</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e346" publication-type="book">
David Newman, 'After the Cold War: US interests in sub-Saharan Africa',
in Brad Roberts (ed.), US Foreign Policy after the Cold War (MIT Press, Cambridge, MA,
1992), p. 143<person-group>
                     <string-name>
                        <surname>Newman</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">After the Cold War: US interests in sub-Saharan Africa</comment>
                  <fpage>143</fpage>
                  <source>US Foreign Policy after the Cold War</source>
                  <year>1992</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e381a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d116e388" publication-type="book">
Michael Hunt, Ideology and U.S. Foreign Policy (Yale University Press, New Haven,
CT, 1987)<person-group>
                     <string-name>
                        <surname>Hunt</surname>
                     </string-name>
                  </person-group>
                  <source>Ideology and U.S. Foreign Policy</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e413a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d116e420" publication-type="book">
Michael Cox, US Foreign Policy after the Cold War: Superpower without a mission?
(Pinter, London, 1995)<person-group>
                     <string-name>
                        <surname>Cox</surname>
                     </string-name>
                  </person-group>
                  <source>US Foreign Policy after the Cold War: Superpower without a mission?</source>
                  <year>1995</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e445a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d116e452" publication-type="journal">
Charles W. Maynes, 'A workable Clinton doctrine', Foreign Policy 93 (1993-4)<object-id pub-id-type="doi">10.2307/1149017</object-id>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e461" publication-type="journal">
David
Brinkley, 'Democratic enlargement: The Clinton Doctrine', Foreign Policy 106 (1997)<object-id pub-id-type="doi">10.2307/1149177</object-id>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e474a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d116e481" publication-type="journal">
Brinkley, 'Democratic enlargement .. .', p. 116  <fpage>116</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e493a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d116e500" publication-type="book">
Jim Broderick, The United States and South Africa in the 1990s, SAIIA Report No. 8,
(South African Institute of International Affairs, Braamfontein, 1998), p. 2<person-group>
                     <string-name>
                        <surname>Broderick</surname>
                     </string-name>
                  </person-group>
                  <fpage>2</fpage>
                  <source>The United States and South Africa in the 1990s</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e528" publication-type="journal">
John
Stremlau, 'Clinton's dollar diplomacy', Foreign Policy 97 (1994-5)<object-id pub-id-type="doi">10.2307/1149437</object-id>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e542a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d116e549" publication-type="other">
Susan Rice, Assistant Secretary for African Affairs, 22 October 1997, Africa News
Service, http://www.africanews.org</mixed-citation>
            </p>
         </fn>
         <fn id="d116e559a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d116e566" publication-type="book">
Salih Booker, Africa: Thinking
regionally-priorities for US policy toward Africa, APIC Background Paper No. 5 (Africa Policy
Information Centre, Washington, DC, March 1996)<person-group>
                     <string-name>
                        <surname>Booker</surname>
                     </string-name>
                  </person-group>
                  <source>Africa: Thinking regionally-priorities for US policy toward Africa</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e594" publication-type="journal">
Robert Chase, Emily Hill and Paul
Kennedy, 'The pivotal states', Foreign Affairs, January/February (1996)<person-group>
                     <string-name>
                        <surname>Chase</surname>
                     </string-name>
                  </person-group>
                  <issue>January</issue>
                  <source>Foreign Affairs</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e622" publication-type="journal">
Dan Connell and
Frank Smyth, 'Africa's new bloc', Foreign Affairs, March/April (1998)<person-group>
                     <string-name>
                        <surname>Connell</surname>
                     </string-name>
                  </person-group>
                  <issue>March</issue>
                  <source>Foreign Affairs</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e651" publication-type="book">
Marina Ottaway,
Africa's New Leaders: Democracy or state reconstruction (Carnegie Endowment for International
Peace, Washington, DC, 1999)<person-group>
                     <string-name>
                        <surname>Ottaway</surname>
                     </string-name>
                  </person-group>
                  <source>Africa's New Leaders: Democracy or state reconstruction</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e680a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d116e687" publication-type="journal">
Princeton Lyman, 'South Africa's promise', Foreign Policy 102 (1996), p. 118<object-id pub-id-type="doi">10.2307/1149262</object-id>
                  <fpage>118</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e700a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d116e707" publication-type="book">
Broderick, The United States and South Africa, p. 35  <fpage>35</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e719a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d116e726" publication-type="book">
William Robinson, Promot-
ing Polyarchy: Globalization, US intervention and hegemony (Cambridge University Press,
Cambridge, 1996), pp. 86-116<person-group>
                     <string-name>
                        <surname>Robinson</surname>
                     </string-name>
                  </person-group>
                  <fpage>86</fpage>
                  <source>Promoting Polyarchy: Globalization, US intervention and hegemony</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e757" publication-type="journal">
Thomas Carothers, 'The NED at 10', Foreign
Policy 95 (1994)<object-id pub-id-type="doi">10.2307/1149427</object-id>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e770a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d116e777" publication-type="book">
Robinson, Promoting Polyarchy ..., 330-1  <fpage>330</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e790a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d116e797" publication-type="book">
'The African Renaissance', Occasional Papers (Konrad Adenauer Stiftung, Bonn, May 1998)<source>The African Renaissance</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e810a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d116e817" publication-type="journal">
Robert Kaplan, 'The coming anarchy', Atlantic Monthly 273 (1994)<person-group>
                     <string-name>
                        <surname>Kaplan</surname>
                     </string-name>
                  </person-group>
                  <volume>273</volume>
                  <source>Atlantic Monthly</source>
                  <year>1994</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e842a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d116e849" publication-type="book">
Booker, Africa: Thinking regionally, p. 13  <fpage>13</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e861a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d116e868" publication-type="journal">
The Star (Johannesburg), 26 March 1998<issue>26 March</issue>
                  <source>The Star</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e884a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d116e891" publication-type="book">
Claude Kabemba and Chris Landsberg, 'Opportunity or Opportunism? Behind the Africa
Growth and Opportunity Act', Policy Brief 6, September (Centre for Policy Studies, London,
1998), p. 1<person-group>
                     <string-name>
                        <surname>Kabemba</surname>
                     </string-name>
                  </person-group>
                  <fpage>1</fpage>
                  <source>Opportunity or Opportunism? Behind the Africa Growth and Opportunity Act</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e923a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d116e930" publication-type="book">
J. F. Torres, New Directions for Development in Third World Countries: The failure
of US foreign policy (Aldershot, Avebury, 1993), p. 95<person-group>
                     <string-name>
                        <surname>Torres</surname>
                     </string-name>
                  </person-group>
                  <fpage>95</fpage>
                  <source>New Directions for Development in Third World Countries: The failure of US foreign policy</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e960a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d116e967" publication-type="book">
Homi Kharas and Hisanobu Shishido, 'The transition from aid to
private capital flow', in Uma Lele and Ijaz Nabi (eds), Transitions in Development: The role of
aid and commercial flows (International Center for Economic Growth, San Francisco, 1991),
pp. 406-9<person-group>
                     <string-name>
                        <surname>Kharas</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The transition from aid to private capital flow</comment>
                  <fpage>406</fpage>
                  <source>Transitions in Development: The role of aid and commercial flows</source>
                  <year>1991</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1005a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d116e1012" publication-type="journal">
Glenn Brigaldino, 'Africa's economic renewal under the spell of globalisation', Review of
African Political Economy 69, 23 (1996), pp. 437-42<person-group>
                     <string-name>
                        <surname>Brigaldino</surname>
                     </string-name>
                  </person-group>
                  <issue>23</issue>
                  <fpage>437</fpage>
                  <volume>69</volume>
                  <source>Review of African Political Economy</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1047a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d116e1054" publication-type="other">
House of Representatives on 3 November 1998 as HR 1432
Crane/Lugar 'African Growth and Opportunity Act', US House of Representatives,
Washington, DC</mixed-citation>
            </p>
         </fn>
         <fn id="d116e1067a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d116e1074" publication-type="journal">
Roger Riddell, 'The end of foreign aid to Africa? Concerns about donor policies', African
Affairs 98, 392 (1999), p. 320<object-id pub-id-type="jstor">10.2307/723522</object-id>
                  <fpage>320</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1090a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d116e1097" publication-type="journal">
The Star (Johannesburg), 28 March 1998<issue>28 March</issue>
                  <source>The Star</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1113a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d116e1120" publication-type="journal">
Greg Mills, 'South Africa, the United States and Africa', South African Journal
of International Affairs 6, 1 (Summer, 1998), p. 37<person-group>
                     <string-name>
                        <surname>Mills</surname>
                     </string-name>
                  </person-group>
                  <issue>1</issue>
                  <fpage>37</fpage>
                  <volume>6</volume>
                  <source>South African Journal of International Affairs</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1156a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d116e1163" publication-type="journal">
Mills, 'South Africa, the United States and Africa', pp. 36-7  <fpage>36</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1175a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d116e1182" publication-type="journal">
Alfred Zack-Williams, 'Peacekeeping and an African High Command: plus ca change,
plus c'est la mime chose', Review of African Political Economy 71, 24 (1997)<person-group>
                     <string-name>
                        <surname>Zack-Williams</surname>
                     </string-name>
                  </person-group>
                  <issue>24</issue>
                  <volume>71</volume>
                  <source>Review of African Political Economy</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1214a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d116e1221" publication-type="book">
Jeffrey Herbst, 'South Africa and ACRI', in South African Yearbook for International
Affairs (South African Institute of International Affairs, Braamfontein, 1998), p. 222<person-group>
                     <string-name>
                        <surname>Herbst</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">South Africa and ACRI</comment>
                  <fpage>222</fpage>
                  <source>South African Yearbook for International Affairs</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e1252" publication-type="journal">
Paul Omach, 'The African Crisis Response Initiative: domestic politics and convergence
of national interests', African Affairs 99, 394 (2000), pp. 73-95<object-id pub-id-type="jstor">10.2307/723548</object-id>
                  <fpage>73</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1268a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d116e1275" publication-type="journal">
Jeremy Levitt, 'The African Crisis Response Initiative: a general survey', Africa Insight
28, 3/4 (1998), p. 101<person-group>
                     <string-name>
                        <surname>Levitt</surname>
                     </string-name>
                  </person-group>
                  <issue>3</issue>
                  <fpage>101</fpage>
                  <volume>28</volume>
                  <source>Africa Insight</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1310a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d116e1317" publication-type="journal">
Levitt, 'The African Crisis Response Initiative', p. 102  <fpage>102</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1329a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d116e1336" publication-type="book">
Eric Berman and Katie Sams, Constructive Disengagement: Western efforts to develop
African peacekeeping, ISS Monograph Series No. 33, December (Institute for Security Studies,
Midrand, 1998), p. 3<person-group>
                     <string-name>
                        <surname>Berman</surname>
                     </string-name>
                  </person-group>
                  <fpage>3</fpage>
                  <source>Constructive Disengagement: Western efforts to develop African peacekeeping</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1369a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d116e1376" publication-type="journal">
Levitt, 'The African Crisis Response Initiative', p. 102  <fpage>102</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1388a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d116e1395" publication-type="journal">
Africa Confidential 38, 16, 1 August 1997<issue>16</issue>
                  <volume>38</volume>
                  <source>Africa Confidential</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1414a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d116e1421" publication-type="journal">
Mills, 'South Africa, the United States and Africa', p. 40  <fpage>40</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1433a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d116e1440" publication-type="book">
David Gordon et al., The United States and Africa: a post-Cold War perspective (Norton,
New York, 1998), pp. 77-80<person-group>
                     <string-name>
                        <surname>Gordon</surname>
                     </string-name>
                  </person-group>
                  <fpage>77</fpage>
                  <source>The United States and Africa: a post-Cold War perspective</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1469a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d116e1476" publication-type="journal">
Adrian Guelke, 'The US, Irish American and Northern
Ireland peace process,' International Affairs 72, 3 (1996)<person-group>
                     <string-name>
                        <surname>Guelke</surname>
                     </string-name>
                  </person-group>
                  <issue>3</issue>
                  <volume>72</volume>
                  <source>International Affairs</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e1507" publication-type="journal">
Ali Mazrui, 'Between
the crescent and the star-spangled banner: American Muslims and US foreign policy',
International Affairs 72, 3 (1996)<person-group>
                     <string-name>
                        <surname>Mazrui</surname>
                     </string-name>
                  </person-group>
                  <issue>3</issue>
                  <volume>72</volume>
                  <source>International Affairs</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1542a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d116e1549" publication-type="other">
National Summit on Africa, http://www.africasummit.org/
backgrnd.htm</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e1558" publication-type="book">
Ronald Walters, 'African American influence on US foreign policy toward South Africa',
in Mohammed Ahrari (ed.) Ethnic Groups in US Foreign Policy (Greenwood, Westport, CT,
1987)<person-group>
                     <string-name>
                        <surname>Walters</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">African American influence on US foreign policy toward South Africa</comment>
                  <source>Ethnic Groups in US Foreign Policy</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e1589" publication-type="book">
Michael Krenn (ed.) The African American Voice in US Foreign Policy
Since World War II (Garland, New York, 1998)<person-group>
                     <string-name>
                        <surname>Krenn</surname>
                     </string-name>
                  </person-group>
                  <source>The African American Voice in US Foreign Policy Since World War II</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1615a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d116e1622" publication-type="journal">
William Martin, 'Constructive engagement II or catching the fourth wave: who and
where are the "constituents" for Africa?' The Black Scholar 24, 1 (1999), p. 24<person-group>
                     <string-name>
                        <surname>Martin</surname>
                     </string-name>
                  </person-group>
                  <issue>1</issue>
                  <fpage>24</fpage>
                  <volume>24</volume>
                  <source>The Black Scholar</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1657a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d116e1664" publication-type="journal">
Martin, 'Constructive engagement II', p. 18  <fpage>18</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d116e1675" publication-type="journal">
Gerald Home, 'Looking forward/looking backward: the
black constituency for Africa past and present', The Black Scholar 29, 1 (1999), pp. 30-33<person-group>
                     <string-name>
                        <surname>Home</surname>
                     </string-name>
                  </person-group>
                  <issue>1</issue>
                  <fpage>30</fpage>
                  <volume>29</volume>
                  <source>The Black Scholar</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1710a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d116e1717" publication-type="other">
'Final communique issued by US-SADC Forum', 21 April 1999, Africa News Service,
http://www.africanews.org/usaf</mixed-citation>
            </p>
         </fn>
         <fn id="d116e1727a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d116e1734" publication-type="other">
'Assistant Secretary Rice addresses CCA capital to Africa conference', 28 April 1999,
Africa News Service, http://africanews.org/usaf</mixed-citation>
            </p>
         </fn>
         <fn id="d116e1744a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d116e1751" publication-type="journal">
Mills, 'South Africa, the United States and Africa', p. 36  <fpage>36</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1763a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d116e1770" publication-type="journal">
Salih Booker, 'The Clinton Administration's new partnership for economic growth and
opportunity in Africa: a pivotal moment?', Global Dialogue 2, 2, (Foundation for Global
Dialogue, Braamfontein, 1998), p. 13<person-group>
                     <string-name>
                        <surname>Booker</surname>
                     </string-name>
                  </person-group>
                  <issue>2</issue>
                  <fpage>13</fpage>
                  <volume>2</volume>
                  <source>Global Dialogue</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1809a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d116e1816" publication-type="journal">
Gorm Rye Olssen, 'Europe and the promotion of democracy in post-Cold War Africa:
how serious is Europe and for what reason?', African Affairs 97, 388 (1998), p. 350<person-group>
                     <string-name>
                        <surname>Olssen</surname>
                     </string-name>
                  </person-group>
                  <issue>388</issue>
                  <fpage>350</fpage>
                  <volume>97</volume>
                  <source>African Affairs</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1851a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d116e1858" publication-type="book">
Kabemba and Landsberg, Opportunity or Opportunism, p. 1  <fpage>1</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1870a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d116e1877" publication-type="conference">
Susan Rice, Assistant Secretary for African Affairs, US State
Department, at an address to the Corporate Council on Africa, 'Assistant Secretary Rice
addresses CCA capital to Africa conference', 28 April 1999, Africa News Service, http://
www. africanews.org/usaf<person-group>
                     <string-name>
                        <surname>Rice</surname>
                     </string-name>
                  </person-group>
                  <source>Corporate Council on Africa, 'Assistant Secretary Rice addresses CCA capital to Africa conference', 28 April 1999</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1909a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d116e1916" publication-type="conference">
Rice, 'Capital to Africa conference', http://www.africanews.org/usaf  </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1925a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d116e1932" publication-type="journal">
Connell and Smyth, 'Africa's new bloc', p. 93  <fpage>93</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1944a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d116e1951" publication-type="other">
Africa News Service, 12 May 2000, http://www.africanews.org</mixed-citation>
            </p>
         </fn>
         <fn id="d116e1959a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d116e1966" publication-type="book">
Hunt, Ideology and U.S. Foreign Policy, p. 174  <fpage>174</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e1978a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d116e1985" publication-type="other">
Africa News Service, 23 September 1999, http://www.africanews.org</mixed-citation>
            </p>
         </fn>
         <fn id="d116e1992a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d116e1999" publication-type="journal">
Marguerite Michaels, 'Retreat from Africa', Foreign Affairs 72, 1 (1993), p. 107<person-group>
                     <string-name>
                        <surname>Michaels</surname>
                     </string-name>
                  </person-group>
                  <issue>1</issue>
                  <fpage>107</fpage>
                  <volume>72</volume>
                  <source>Foreign Affairs</source>
                  <year>1993</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d116e2031a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d116e2038" publication-type="other">
Africa News Service, 11 May 2000, http://www.africanews.org</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
