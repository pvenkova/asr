<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mycologia</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100297</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Mycologia</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Mycological Society of America</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00275514</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15572536</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3762306</article-id>
         <article-categories>
            <subj-group>
               <subject>Molecular Evolution and Systematics</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Molecular Systematics of Helicoma, Helicomyces and Helicosporium and Their Teleomorphs Inferred from rDNA Sequences</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Clement K. M. Tsui</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Somsak</given-names>
                  <surname>Sivichai</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mary L.</given-names>
                  <surname>Berbee</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2006</year>
      
            <day>1</day>
            <month>2</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">98</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i290201</issue-id>
         <fpage>94</fpage>
         <lpage>104</lpage>
         <page-range>94-104</page-range>
         <permissions>
            <copyright-statement>Copyright 2006 The Mycological Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3762306"/>
         <abstract>
            <p>Three genera of asexual, helical-spored fungi, Helicoma, Helicomyces and Helicosporium traditionally have been differentiated by the morphology of their conidia and conidiophores. In this paper we assessed their phylogenetic relationships from ribosomal sequences from ITS, 5.8S and partial LSU regions using maximum parsimony, maximum likelihood and Bayesian analysis. Forty-five isolates from the three genera were closely related and were within the teleomorphic genus Tubeufia sensu Barr (Tubeufiaceae, Ascomycota). Most of the species could be placed in one of the seven clades that each received 78% or greater bootstrap support. However none of the anamorphic genera were monophyletic and all but one of the clades contained species from more than one genus. The 15 isolates of Helicoma were scattered through the phylogeny and appeared in five of the clades. None of the four sections within the genus were monophyletic, although species from Helicoma sect. helicoma were concentrated in Clade A. The Helicosporium species also appeared in five clades. The four Helicomyces species were distributed among three clades. Most of the clades supported by sequence data lacked unifying morphological characters. Traditional characters such as the thickness of the conidial filament and whether conidiophores were conspicuous or reduced proved to be poor predictors of phylogenetic relationships. However some combinations of characters including conidium colour and the presence of lateral, tooth-like conidiogenous cells did appear to be predictive of genetic relationships.</p>
         </abstract>
         <kwd-group>
            <kwd>Acanthostigma</kwd>
            <kwd>Aero-aquatic</kwd>
            <kwd>Dothideomycetes</kwd>
            <kwd>Drepanospora</kwd>
            <kwd>Freshwater fungi</kwd>
            <kwd>Mitosporic fungi</kwd>
            <kwd>Thaxteriella</kwd>
            <kwd>Tubeufia</kwd>
            <kwd>Phylogeny</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d1496e272a1310">
            <mixed-citation id="d1496e276" publication-type="journal">
Barr ME. 1979. A classification of loculoascomycetes.
Mycologia71:935-957<object-id pub-id-type="doi">10.2307/3759283</object-id>
               <fpage>935</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e292a1310">
            <mixed-citation id="d1496e296" publication-type="journal">
—. 1980. On the family Tubeufiaceae. Mycotaxon
12:137-167<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Barr</surname>
                  </string-name>
               </person-group>
               <fpage>137</fpage>
               <volume>12</volume>
               <source>Mycotaxon</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e328a1310">
            <mixed-citation id="d1496e332" publication-type="journal">
Bucher WC, Pointing SB, Hyde KD, Reddy CA. 2004.
Production of wood decay enzymes, loss of mass, and
lignin solubilization in woody, diverse tropical freshwa-
ter fungi. Microb Ecol48:331-337<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bucher</surname>
                  </string-name>
               </person-group>
               <fpage>331</fpage>
               <volume>48</volume>
               <source>Microb Ecol</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e370a1310">
            <mixed-citation id="d1496e374" publication-type="journal">
Carris LM. 1989. Vaccinium fungi: Helicoma vaccinii sp. nov.
Mycotaxon36:29-34<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Carris</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>36</volume>
               <source>Mycotaxon</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e407a1310">
            <mixed-citation id="d1496e411" publication-type="journal">
Casteñeda Ruíz RF, Kendrick B, Guarro J, Mayayo E. 1998.
New species of Dictyochaeta and Helicoma from rain-
forests in Cuba. Mycol Res102:58-62<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Casteñeda</surname>
                  </string-name>
               </person-group>
               <fpage>58</fpage>
               <volume>102</volume>
               <source>Mycol Res</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e446a1310">
            <mixed-citation id="d1496e450" publication-type="journal">
Crane JL, Shearer CA, Barr ME. 1998. A revision of
Boerlagiomyces with notes and a key to the saprobic
genera of Tubeufiaceae. Can J Bot76:602-612<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Crane</surname>
                  </string-name>
               </person-group>
               <fpage>602</fpage>
               <volume>76</volume>
               <source>Can J Bot</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e485a1310">
            <mixed-citation id="d1496e489" publication-type="journal">
Crous PW, Aptroot A, Kang J-C, Braun U, Wingfield MJ.
2000. The genus Mycosphaerella and its anamorph. Stud
Mycol45:107-121<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Crous</surname>
                  </string-name>
               </person-group>
               <fpage>107</fpage>
               <volume>45</volume>
               <source>Stud Mycol</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e524a1310">
            <mixed-citation id="d1496e530" publication-type="journal">
—, Kang J-C, Braun U. 2001. A phylogenetic re-
definition of anamorph genera in Mycosphaerella based
on ITS rDNA sequence and morphology. Mycologia
93:1081-1101<object-id pub-id-type="doi">10.2307/3761670</object-id>
               <fpage>1081</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e553a1310">
            <mixed-citation id="d1496e557" publication-type="journal">
Goh TK, Hyde KD. 1996. Helicoon gigantisporum sp. nov.,
and an amended key to the genus. Mycol Res100:
1485-1488<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Goh</surname>
                  </string-name>
               </person-group>
               <fpage>1485</fpage>
               <volume>100</volume>
               <source>Mycol Res</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e592a1310">
            <mixed-citation id="d1496e596" publication-type="journal">
Goos RD. 1985. A review of the anamorph genus
Helicomyces. Mycologia77:606-618<object-id pub-id-type="doi">10.2307/3793359</object-id>
               <fpage>606</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e613a1310">
            <mixed-citation id="d1496e617" publication-type="journal">
-. 1986. A review of the anamorph genus Helicoma.
Mycologia78:744-761<object-id pub-id-type="doi">10.2307/3807519</object-id>
               <fpage>744</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e633a1310">
            <mixed-citation id="d1496e637" publication-type="journal">
-. 1987. Fungi with a twist: the helicosporous
hyphomycetes. Mycologia79:1-22<object-id pub-id-type="doi">10.2307/3807740</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e653a1310">
            <mixed-citation id="d1496e657" publication-type="journal">
-. 1989. On the anamorph genera Helicosporium and
Drepanospora. Mycologia81:356-374<object-id pub-id-type="doi">10.2307/3760074</object-id>
               <fpage>356</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e673a1310">
            <mixed-citation id="d1496e677" publication-type="journal">
Hanada T, Sato T, Arioka M, Uramoto M, Yamasaki M. 1996.
Purification and characterization of a 15 kDa protein
(pl5) produced by Helicosporium that exhibits distinct
effects on neurite outgrowth from cortical neurons
and pcl2 cells. Biochem Biophys Res Commun
228:209-215<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hanada</surname>
                  </string-name>
               </person-group>
               <fpage>209</fpage>
               <volume>228</volume>
               <source>Biochem Biophys Res Commun</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e721a1310">
            <mixed-citation id="d1496e725" publication-type="journal">
Hausner G, Reid J, Klassen GR. 2000. On the phylogeny of
members of Ceratocystis s.s. and Ophiostoma that possess
different anamorphic states, with emphasis on the
anamorph genus Leptographium, based on partial
ribosomal DNA sequences. Can J Bot78:903-916<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hausner</surname>
                  </string-name>
               </person-group>
               <fpage>903</fpage>
               <volume>78</volume>
               <source>Can J Bot</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e766a1310">
            <mixed-citation id="d1496e770" publication-type="journal">
Huelsenbeck JP, Ronquist F. 2001. MrBayes: Bayesian
inference of phylogenetic trees. Bioinformatics17:
754-755<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Huelsenbeck</surname>
                  </string-name>
               </person-group>
               <fpage>754</fpage>
               <volume>17</volume>
               <source>Bioinformatics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e806a1310">
            <mixed-citation id="d1496e810" publication-type="journal">
Kendrick B. 2003. Analysis of morphogenesis in hyphomy¬
cetes: new characters derived from considering some
conidiophores and conidia as condensed hyphal
systems. Can J Bot81:75-100<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kendrick</surname>
                  </string-name>
               </person-group>
               <fpage>75</fpage>
               <volume>81</volume>
               <source>Can J Bot</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e848a1310">
            <mixed-citation id="d1496e852" publication-type="book">
Kirk PM, Cannon PF, David JC, StalpersJA. 2001. Ainsworth
and Bisby's Dictionary of the Fungi. Oxon, UK CABI
Publishing<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kirk</surname>
                  </string-name>
               </person-group>
               <source>Ainsworth and Bisby's Dictionary of the Fungi</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e881a1310">
            <mixed-citation id="d1496e885" publication-type="journal">
Kodsueb R, Lumyong S, Lumyong P, McKenzie EHC, Ho
WH, Hyde KD. 2004. Acanthostigma and Tubeufia
species, including T. claspisphaeria sp. nov., from
submerged wood in. Mycologia, 96:667-674<object-id pub-id-type="doi">10.2307/3762184</object-id>
               <fpage>667</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e908a1310">
            <mixed-citation id="d1496e912" publication-type="journal">
Lane LC, Shearer CA. 1984. Helicomyces torquatus, new
species, a new hyphomycete from Panama. Mycotaxon
19:291-298<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lane</surname>
                  </string-name>
               </person-group>
               <fpage>291</fpage>
               <volume>19</volume>
               <source>Mycotaxon</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e947a1310">
            <mixed-citation id="d1496e951" publication-type="journal">
Linder DH. 1929. A monograph ofthe helicosporous fungi.
Ann Mo Bot Gard16:227-338<object-id pub-id-type="doi">10.2307/2394038</object-id>
               <fpage>227</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e967a1310">
            <mixed-citation id="d1496e971" publication-type="journal">
Moore RT. 1954. Three new species of helicosporae.
Mycologia46:89-92<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Moore</surname>
                  </string-name>
               </person-group>
               <fpage>89</fpage>
               <volume>46</volume>
               <source>Mycologia</source>
               <year>1954</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1004a1310">
            <mixed-citation id="d1496e1008" publication-type="journal">
Nakagiri A, Ito T. 1995. Some dematiaceous hyphomycetes
on decomposing leaves of Satakentia liukiuensis from
Ishigaki Island, Japan. Institute for Fermentation
Research Communications17:75-98<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Nakagiri</surname>
                  </string-name>
               </person-group>
               <fpage>75</fpage>
               <volume>17</volume>
               <source>Institute for Fermentation Research Communications</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1046a1310">
            <mixed-citation id="d1496e1050" publication-type="journal">
Ohtsu Y, Sasamura H, Tsurumi Y, Yoshimura S, Takase S,
Hashimoto M, Shibata T, Hino M, Fujii T. 2003. The
novel gluconeogenesis inhibitors FR225659 and related
compounds that originate from Helicomyces sp.
no. 19353. J Antibiot56:682-688<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ohtsu</surname>
                  </string-name>
               </person-group>
               <fpage>682</fpage>
               <volume>56</volume>
               <source>J Antibiot</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1091a1310">
            <mixed-citation id="d1496e1095" publication-type="journal">
Pirozynski KA. 1972. Micro Fungi of Tanzania Part 1:
miscellaneous fungi on oil palm. Commonwealth
Mycological Institute Mycol Pap129:1-39<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pirozynski</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>129</volume>
               <source>Commonwealth Mycological Institute Mycol Pap</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1130a1310">
            <mixed-citation id="d1496e1136" publication-type="book">
Rambaut A. 1999. Se-Al: Sequence Alignment Editor.
Available at &lt;http://evolve.zoo.ox.ac.uk/&gt;. Oxford,
U.K: Department of Zoology, University of Oxford<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rambaut</surname>
                  </string-name>
               </person-group>
               <source>Se-Al: Sequence Alignment Editor</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1165a1310">
            <mixed-citation id="d1496e1169" publication-type="journal">
Réblová M, Barr ME. 2000. The genus Acanthostigma
(Tubeufiaceae, Pleosporales). Sydowia52:258-285<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Réblová</surname>
                  </string-name>
               </person-group>
               <fpage>258</fpage>
               <volume>52</volume>
               <source>Sydowia</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1201a1310">
            <mixed-citation id="d1496e1205" publication-type="journal">
Rossman AY. 1987. The Tubeufiaceae and similar loculoas-
comycetes. Mycol Pap157:1-71<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rossman</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>157</volume>
               <source>Mycol Pap</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1238a1310">
            <mixed-citation id="d1496e1242" publication-type="journal">
—. 2000. Towards monophyletic genera in the holo-
morphic Hypocreales. Stud Mycol45:27-34<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rossman</surname>
                  </string-name>
               </person-group>
               <fpage>27</fpage>
               <volume>45</volume>
               <source>Stud Mycol</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1274a1310">
            <mixed-citation id="d1496e1278" publication-type="journal">
—, Samuels GJ, Rogerson CT, Lowen R. 1999. Genera
of Bionectriaceae, Hypocreaceae and Nectriaceae
(Hypocreales, Ascomycetes). Stud Mycol42:1-248<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rossman</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>42</volume>
               <source>Stud Mycol</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1313a1310">
            <mixed-citation id="d1496e1317" publication-type="journal">
Samuels GJ, Rossman AY, Muller E. 1979. Life history of
Brazilian ascomycetes 6. Sydowia31:180-193<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Samuels</surname>
                  </string-name>
               </person-group>
               <fpage>180</fpage>
               <volume>31</volume>
               <source>Sydowia</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1349a1310">
            <mixed-citation id="d1496e1353" publication-type="journal">
Schoch CL, Crous PW, Wingfield MJ, Wingfield BD. 2000.
Phylogeny of Calonectria and selected hypocrealean
genera with cylindrical macroconidia. Stud Mycol
45:45-62<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schoch</surname>
                  </string-name>
               </person-group>
               <fpage>45</fpage>
               <volume>45</volume>
               <source>Stud Mycol</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1391a1310">
            <mixed-citation id="d1496e1395" publication-type="journal">
Schroers H-J. 2000. Generic delimitation of Bionectria
(Bionectriaceae, Hypocreales) based on holomorph
characters and rDNA sequences. Stud Mycol45:63-82<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schroers</surname>
                  </string-name>
               </person-group>
               <fpage>63</fpage>
               <volume>45</volume>
               <source>Stud Mycol</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1430a1310">
            <mixed-citation id="d1496e1434" publication-type="book">
Seifert KA. 1993. Integrating anamorphic fungi into the
fungal system. In: Reynolds DR, Taylor JW, eds. The
fungal holomorph: mitotic, meiotic and pleomorphic
speciation in fungal systematics. Newport, Oregon:
CABI. p 79-86<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Seifert</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Integrating anamorphic fungi into the fungal system</comment>
               <fpage>79</fpage>
               <source>The fungal holomorph: mitotic, meiotic and pleomorphic speciation in fungal systematics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1476a1310">
            <mixed-citation id="d1496e1480" publication-type="journal">
—, Samuels GJ. 2000. How should we look at
anamorphs? Stud Mycol45:5-18<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Seifert</surname>
                  </string-name>
               </person-group>
               <fpage>5</fpage>
               <volume>45</volume>
               <source>Stud Mycol</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1512a1310">
            <mixed-citation id="d1496e1516" publication-type="journal">
Shearer CA. 1987. Helicoma chlamydosporum, a new hypho-
mycetes from submerged wood in Panama. Mycologia
79:468-472<object-id pub-id-type="doi">10.2307/3807474</object-id>
               <fpage>468</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e1535a1310">
            <mixed-citation id="d1496e1539" publication-type="journal">
Sivichai S, Jones EBG, Hywel-Jones N. 2002. Fungal
colonisation of wood in a freshwater stream at Tad Ta
Phu, Khao Yai National Park, Thailand. Fungal Di-
versity June10:113-129<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Sivichai</surname>
                  </string-name>
               </person-group>
               <fpage>113</fpage>
               <volume>10</volume>
               <source>Fungal Diversity June</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1577a1310">
            <mixed-citation id="d1496e1581" publication-type="book">
Swofford DL. 2003. PAUP*: phylogenetic analysis using
parsimony (*and other methods). Sunderland, Massa-
chusetts: Sinauer Associates<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swofford</surname>
                  </string-name>
               </person-group>
               <source>PAUP*: phylogenetic analysis using parsimony (*and other methods)</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1610a1310">
            <mixed-citation id="d1496e1614" publication-type="journal">
Thompson JD, Gibson TJ, Plewniak F, Jeanmougin F,
Higgins DG. 1997. The Clustal X windows interface:
flexible strategies for multiple sequence alignment
aided by quality analysis tools. Nucleic Acids Res
25:4876-4882<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Thompson</surname>
                  </string-name>
               </person-group>
               <fpage>4876</fpage>
               <volume>25</volume>
               <source>Nucleic Acids Res</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1655a1310">
            <mixed-citation id="d1496e1659" publication-type="other">
Tsui CKM, Berbee ML. 2006. Phylogenetic relationships and
convergence of helicosporous fungi inferred from
ribosomal DNA sequences. Mol Phyl Evol (In press)</mixed-citation>
         </ref>
         <ref id="d1496e1673a1310">
            <mixed-citation id="d1496e1677" publication-type="journal">
-, Goh TK, Hyde KD, Hodgkiss IJ. 2001a. New species
or records of Cacumisporium, Helicosporium, Monoto-
sporella and Bahusutrabeeja (Hyphomycetes) on sub¬
merged wood in Hong Kong streams. Mycologia
93:389-397<object-id pub-id-type="doi">10.2307/3761660</object-id>
               <fpage>389</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1496e1703a1310">
            <mixed-citation id="d1496e1707" publication-type="journal">
-, Hyde KD, Hodgkiss IJ. 2001b. Longitudinal and
temporal distribution of freshwater ascomycetes and
dematiaceous hyphomycetes on submerged wood in
the Lam Tsuen River, Hong Kong. J N Am Benthol
Soc20:533-549<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Tsui</surname>
                  </string-name>
               </person-group>
               <fpage>533</fpage>
               <volume>20</volume>
               <source>J N Am Benthol Soc</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1496e1748a1310">
            <mixed-citation id="d1496e1752" publication-type="journal">
Yuen TK, Hyde KD, Hodgkiss IJ. 1998. Physiological growth
parameters and enzyme production in tropical fresh¬
water fungi. Material und Organismen (Berlin)
32:1-16<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Yuen</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>32</volume>
               <source>Material und Organismen (Berlin)</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
