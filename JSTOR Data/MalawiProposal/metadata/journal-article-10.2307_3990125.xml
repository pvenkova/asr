<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id journal-id-type="jstor">j101235</journal-id>
         <journal-title-group>
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>World Bank</publisher-name>
         </publisher>
         <issn pub-type="ppub">02586770</issn>
         <issn pub-type="epub">1564698X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3990125</article-id>
         <title-group>
            <article-title>Cross-Country Evidence on Public Sector Retrenchment</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>John</given-names>
                  <surname>Haltiwanger</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Manisha</given-names>
                  <surname>Singh</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">13</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i382802</issue-id>
         <fpage>23</fpage>
         <lpage>66</lpage>
         <page-range>23-66</page-range>
         <permissions>
            <copyright-statement>Copyright 1999 The International Bank for Reconstruction and Development/The World Bank</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3990125"/>
         <abstract>
            <p>This article reports the results from a survey of public sector employment retrenchment episodes across a wide variety of developing and transition economies. The information collected and analyzed is primarily from internal World Bank documents and in-depth interviews with World Bank staff having operational information about experiences in specific countries. Using the information collected on 41 retrenchment programs across 37 countries, the article analyzes the relationships between the factors leading to retrenchment, the scope and nature of retrenchment, and the methods used to accomplish the retrenchment. The discussion of methods includes an analysis of the mix of involuntary and voluntary employment reduction programs, the compensation schemes offered, and the extent of targeting of specific types of workers. Although relevant quantitative information is limited, the article also attempts to evaluate the outcome of the programs on several dimensions. The most striking findings relate to analysis of the factors leading a significant fraction of programs to rehire workers separated from the public sector (thereby defeating the programs' objective). In addition, the article relates program characteristics to calculated summary financial payback indicators and to the nature of the labor market adjustment.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d847e132a1310">
            <mixed-citation id="d847e136" publication-type="journal">
Alderman, Harold, Roy S. Canagarajah, and Stephen Younger. 1996. "A Comparison
of Ghanaian Civil Servants' Earnings before and after Retrenchment." Journal of
African Economies4(2):259-88.<person-group>
                  <string-name>
                     <surname>Alderman</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>259</fpage>
               <volume>4</volume>
               <source>Journal of African Economies</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d847e174a1310">
            <mixed-citation id="d847e178" publication-type="book">
Basu, Kaushik, Gary Fields, and Shub Debgupta. 1996. "Retrenchment, Labor Laws,
and Government Policy: An Analysis with Special Reference to India." Paper pre-
sented at the conference on Public Sector Retrenchment and Efficient Compensation
Schemes, World Bank, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Basu</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Retrenchment, Labor Laws, and Government Policy: An Analysis with Special Reference to India</comment>
               <source>conference on Public Sector Retrenchment and Efficient Compensation Schemes, World Bank, Washington, D.C.</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d847e213a1310">
            <mixed-citation id="d847e217" publication-type="journal">
Bertola, Giuseppe. 1990. "Job Security, Employment, and Wages." European Economic
Review34(4, June):381-402.<person-group>
                  <string-name>
                     <surname>Bertola</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>381</fpage>
               <volume>34</volume>
               <source>European Economic Review</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d847e252a1310">
            <mixed-citation id="d847e256" publication-type="journal">
Bertola, Giuseppe, and Richard Rogerson. 1997. "Institutions and Labor Reallocation."
European Economic Review41(6, June):1147-71.<person-group>
                  <string-name>
                     <surname>Bertola</surname>
                  </string-name>
               </person-group>
               <issue>6</issue>
               <fpage>1147</fpage>
               <volume>41</volume>
               <source>European Economic Review</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d847e292a1310">
            <mixed-citation id="d847e296" publication-type="journal">
Blanchard, Olivier, and Peter Diamond. 1989. "The Beveridge Curve." Brookings Pa-
pers on Economic Activity1:1-60.<object-id pub-id-type="doi">10.2307/2534495</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d847e312a1310">
            <mixed-citation id="d847e316" publication-type="journal">
.1990. "The Cyclical Behavior of Gross Flows of Workers in the United States."
Brookings Papers on Economic Activity2:85-155.<object-id pub-id-type="doi">10.2307/2534505</object-id>
               <fpage>85</fpage>
            </mixed-citation>
         </ref>
         <ref id="d847e332a1310">
            <mixed-citation id="d847e336" publication-type="book">
Blanchard, Olivier, Simon Commander, and Fabrizio Coricelli. 1994. "Unemployment
and Restructuring in Eastern Europe." In Simon Commander and Fabrizio Coricelli,
eds., Unemployment, Restructuring, and the Labor Market in Eastern Europe and
Russia. EDI Development Studies. Washington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Blanchard</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Unemployment and Restructuring in Eastern Europe</comment>
               <source>Unemployment, Restructuring, and the Labor Market in Eastern Europe and Russia</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e371a1310">
            <mixed-citation id="d847e375" publication-type="book">
Commander, Simon, and Farbrizio Coricelli, eds. 1994. Unemployment, Restructuring,
and the Labor Market in Eastern Europe and Russia. EDI Development Studies. Wash-
ington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Commander</surname>
                  </string-name>
               </person-group>
               <source>Unemployment, Restructuring, and the Labor Market in Eastern Europe and Russia</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e404a1310">
            <mixed-citation id="d847e408" publication-type="book">
Commander, Simon, Janos Kollo, Cecilia Ugaz, and Balacs Vilagi. 1994. "Hungary." In
Simon Commander and Fabrizio Coricelli, eds., Unemployment, Restructuring, and
the Labor Market in Eastern Europe and Russia. EDI Development Studies. Washing-
ton, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Commander</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Hungary</comment>
               <source>Unemployment, Restructuring, and the Labor Market in Eastern Europe and Russia</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e443a1310">
            <mixed-citation id="d847e447" publication-type="journal">
Cooper, Russell, and John Haltiwanger. 1996. "Evidence on Macroeconomic Comple-
mentarities." Review of Economics and Statistics78(1, February):78-93.<object-id pub-id-type="doi">10.2307/2109849</object-id>
               <fpage>78</fpage>
            </mixed-citation>
         </ref>
         <ref id="d847e464a1310">
            <mixed-citation id="d847e468" publication-type="book">
Davis, Steven J., John Haltiwanger, and Scott Schuh. 1996. Job Creation and Destruc-
tion. Cambridge, Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Davis</surname>
                  </string-name>
               </person-group>
               <source>Job Creation and Destruction</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d847e493a1310">
            <mixed-citation id="d847e497" publication-type="book">
Diwan, Ishac. 1993a. "Public Sector Retrenchment and Efficient Severance Pay Schemes."
World Bank, Policy Research Department, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Diwan</surname>
                  </string-name>
               </person-group>
               <source>Public Sector Retrenchment and Efficient Severance Pay Schemes</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d847e522a1310">
            <mixed-citation id="d847e526" publication-type="book">
.1993b. "Public Sector Retrenchment and Severance Pay: Nine Propositions."
World Bank, Policy Research Department, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Diwan</surname>
                  </string-name>
               </person-group>
               <source>Public Sector Retrenchment and Severance Pay: Nine Propositions</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d847e551a1310">
            <mixed-citation id="d847e555" publication-type="book">
Griliches, Zvi, ed. 1992. Output Measurement in the Service Sectors. NBER Studies in
Income and Wealth 56. Chicago: University of Chicago Press.<person-group>
                  <string-name>
                     <surname>Griliches</surname>
                  </string-name>
               </person-group>
               <source>Output Measurement in the Service Sectors</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d847e580a1310">
            <mixed-citation id="d847e584" publication-type="journal">
Hall, Robert E.1995. "Lost Jobs." Brookings Papers on Economic Activity1:221-
56.<object-id pub-id-type="doi">10.2307/2534575</object-id>
               <fpage>221</fpage>
            </mixed-citation>
         </ref>
         <ref id="d847e600a1310">
            <mixed-citation id="d847e604" publication-type="book">
International Monetary Fund. Various issues. International Finance Statistics. Washing-
ton, D.C.<person-group>
                  <string-name>
                     <surname>International Monetary Fund</surname>
                  </string-name>
               </person-group>
               <source>International Finance Statistics</source>
            </mixed-citation>
         </ref>
         <ref id="d847e627a1310">
            <mixed-citation id="d847e631" publication-type="journal">
Lazear, Edward. 1990. "Job Security Provisions and Employment." Quarterly Journal
of Economics105(3, August):699-726.<object-id pub-id-type="doi">10.2307/2937895</object-id>
               <fpage>699</fpage>
            </mixed-citation>
         </ref>
         <ref id="d847e647a1310">
            <mixed-citation id="d847e651" publication-type="book">
Levy, Anat, and Richard McLean. 1996. "Optimal and Suboptimal Retrenchment
Schemes: An Analytical Framework." Paper presented at the conference on Public
Sector Retrenchment and Efficient Compensation Schemes, World Bank, Washing-
ton, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Levy</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Optimal and Suboptimal Retrenchment Schemes: An Analytical Framework</comment>
               <source>conference on Public Sector Retrenchment and Efficient Compensation Schemes, World Bank, Washington, D.C.</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d847e686a1310">
            <mixed-citation id="d847e690" publication-type="book">
Lindauer, David, and Barbara Nunberg, eds. 1994. Rehabilitating Government: Pay
and Employment Reform in Africa. A World Bank Regional and Sectoral Study.
Washington, D.C.: World Bank.<person-group>
                  <string-name>
                     <surname>Lindauer</surname>
                  </string-name>
               </person-group>
               <source>Rehabilitating Government: Pay and Employment Reform in Africa</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e719a1310">
            <mixed-citation id="d847e723" publication-type="book">
Mazumdar, Dipak. 1989. Microeconomic Issues of Labor Markets in Developing Coun-
tries: Analysis and Policy Implications. EDI Seminar Paper 40. Washington, D.C.:
World Bank.<person-group>
                  <string-name>
                     <surname>Mazumdar</surname>
                  </string-name>
               </person-group>
               <source>Microeconomic Issues of Labor Markets in Developing Countries: Analysis and Policy Implications</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d847e752a1310">
            <mixed-citation id="d847e756" publication-type="book">
Mills, Bradford, David E. Sahn, Edward E. Walden, and Stephen D. Younger. 1993.
"Public Finance and Employment: An Analysis of Public Sector Retrenchment Pro-
grams in Ghana and Guinea." World Bank, Policy Research Department, Washing-
ton, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Mills</surname>
                  </string-name>
               </person-group>
               <source>Public Finance and Employment: An Analysis of Public Sector Retrenchment Programs in Ghana and Guinea</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d847e788a1310">
            <mixed-citation id="d847e792" publication-type="book">
OECD (Organisation for Economic Co-operation and Development). 1993. Employment
Outlook (July). Paris.<person-group>
                  <string-name>
                     <surname>OECD (Organisation for Economic Co-operation and Development)</surname>
                  </string-name>
               </person-group>
               <source>Employment Outlook</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d847e818a1310">
            <mixed-citation id="d847e822" publication-type="book">
.1996. Job Creation and Loss: Analysis, Policy and Data Development. Paris.<person-group>
                  <string-name>
                     <surname>OECD (Organisation for Economic Co-operation and Development)</surname>
                  </string-name>
               </person-group>
               <source>Job Creation and Loss: Analysis, Policy and Data Development</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d847e844a1310">
            <mixed-citation id="d847e848" publication-type="book">
PA Consulting Group. 1993. "Social Safety Net Adjustment Credit Technical Assis-
tance—Final Report." Report submitted to the Government of India's Industrial De-
velopment Ministry and the World Bank. Auckland, New Zealand. Processed.<person-group>
                  <string-name>
                     <surname>PA Consulting Group</surname>
                  </string-name>
               </person-group>
               <source>Social Safety Net Adjustment Credit Technical Assistance—Final Report</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d847e877a1310">
            <mixed-citation id="d847e881" publication-type="book">
Rama, Martin. 1994. "Flexibility in Sri Lanka's Labor Market." Policy Research Work-
ing Paper 1262. World Bank, Policy Research Department, Washington, D.C. Pro-
cessed.<person-group>
                  <string-name>
                     <surname>Rama</surname>
                  </string-name>
               </person-group>
               <source>Flexibility in Sri Lanka's Labor Market</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e910a1310">
            <mixed-citation id="d847e914" publication-type="book">
.1995. "Labor Market Cross-Country Database: Glossary." World Bank, Policy
Research Department, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Rama</surname>
                  </string-name>
               </person-group>
               <source>Labor Market Cross-Country Database: Glossary</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d847e939a1310">
            <mixed-citation id="d847e943" publication-type="book">
.1997. "Efficient Public Sector Downsizing." World Bank, Policy Research De-
partment, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Rama</surname>
                  </string-name>
               </person-group>
               <source>Efficient Public Sector Downsizing</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d847e968a1310">
            <mixed-citation id="d847e972" publication-type="book">
Robbins, Donald. 1996. "Public Sector Retrenchment: A Case Study of Argentina."
Paper presented at the conference on Public Sector Retrenchment and Efficient Com-
pensation Schemes, World Bank, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Public Sector Retrenchment: A Case Study of Argentina</comment>
               <source>conference on Public Sector Retrenchment and Efficient Compensation Schemes, World Bank, Washington, D.C.</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1005a1310">
            <mixed-citation id="d847e1009" publication-type="journal">
Summers, Robert, and Alan Heston. 1991. "The Penn World Table (Mark 5): An Ex-
panded Set of International Comparisons, 1950-1988." Quarterly Journal of Eco-
nomics106(2, May):327-68.<object-id pub-id-type="doi">10.2307/2937941</object-id>
               <fpage>327</fpage>
            </mixed-citation>
         </ref>
         <ref id="d847e1028a1310">
            <mixed-citation id="d847e1032" publication-type="book">
Svejnar, Jan, and Katherine Terrell. 1991. "Reducing Labor Redundancy in State-Owned
Enterprises." Policy Research Working Paper 792. World Bank, Policy Research De-
partment, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Svejnar</surname>
                  </string-name>
               </person-group>
               <source>Reducing Labor Redundancy in State-Owned Enterprises</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1061a1310">
            <mixed-citation id="d847e1065" publication-type="book">
Uganda Ministry of Public Service. 1994. Management of Change, Context, Vision,
Objectives, Strategy, and Plan: Civil Service Reform. Kampala.<person-group>
                  <string-name>
                     <surname>Uganda Ministry of Public Service</surname>
                  </string-name>
               </person-group>
               <source>Management of Change, Context, Vision, Objectives, Strategy, and Plan: Civil Service Reform</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1090a1310">
            <mixed-citation id="d847e1094" publication-type="book">
World Bank. 1994a. "Bolivia: Structural Reforms, Fiscal Impact, and Economic Growth."
Report 13067-B). Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Bolivia: Structural Reforms, Fiscal Impact, and Economic Growth</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1119a1310">
            <mixed-citation id="d847e1123" publication-type="book">
.1994b. "Peru Public Expenditure Review." Report 13190-PE. Washington, D.C.
Processed.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Peru Public Expenditure Review</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1148a1310">
            <mixed-citation id="d847e1152" publication-type="book">
.1994c. "Shenyang Industrial Reform Project Appraisal Report." Report 12415-
CHA. Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Shenyang Industrial Reform Project Appraisal Report</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1178a1310">
            <mixed-citation id="d847e1182" publication-type="book">
.1994d. World Development Report 1994: Infrastructure for Development. New
York: Oxford University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report 1994: Infrastructure for Development</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1207a1310">
            <mixed-citation id="d847e1211" publication-type="book">
.1995a. "Argentina Public Sector Reform Project Completion Report." Report
14781. Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Argentina Public Sector Reform Project Completion Report</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1236a1310">
            <mixed-citation id="d847e1240" publication-type="book">
.1995b. "From Swords to Ploughshares: The Uganda Demobilization and Rein-
tegration of Ex-combatants in Uganda." Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>From Swords to Ploughshares: The Uganda Demobilization and Reintegration of Ex-combatants in Uganda</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1265a1310">
            <mixed-citation id="d847e1269" publication-type="book">
.1995c. "Public Enterprise Adjustment Loan Audit Report." Report 14809.
Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Public Enterprise Adjustment Loan Audit Report</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d847e1294a1310">
            <mixed-citation id="d847e1298" publication-type="book">
. Various issues. World Tables. Baltimore, Md.: The Johns Hopkins University
Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Tables</source>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
