<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">inteaffaroyainst</journal-id>
         <journal-id journal-id-type="jstor">j100185</journal-id>
         <journal-title-group>
            <journal-title>International Affairs (Royal Institute of International Affairs 1944-)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00205850</issn>
         <issn pub-type="epub">14682346</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3568888</article-id>
         <title-group>
            <article-title>British Government Policy in Sub-Saharan Africa under New Labour</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Tom</given-names>
                  <surname>Porteous</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">81</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i282854</issue-id>
         <fpage>281</fpage>
         <lpage>297</lpage>
         <page-range>281-297</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The Royal Institute of International Affairs</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3568888"/>
         <abstract>
            <p>British government policy in Africa under Labour has been motivated by a combination of humanitarianism and self-interest. The policy has been shaped principally by the Department for International Development (DFID), but also by the Foreign and Commonwealth Office (FCO) and by Prime Minister Tony Blair himself, as he has become more interested in Africa issues. The main focus of the policy has been on poverty reduction and development. The approach has been multi-dimensional, aiming to tackle the principal obstacles to development such as conflict, HIV, debt, governance and trade barriers. The UK has sought to increase its leverage in Africa by working multilaterally with its allies and through the UN, the World Bank and the EU. But the policy has been hampered by the inherent difficulty of promoting sound development policies in weak states, by a lack of UK leverage to affect change, and by a UK preference for statist solutions. Strategic and commercial objectives pursued by the FCO and the prime minister have sometimes appeared as being at odds with the developmental objectives of DFID. Post-9/11 concerns have reinforced the UK's motivation for dealing with Africa's problems, particularly the problems of weak and failed states. But western policies related to the war on terror may give rise to new contradictions and complicate the UK's developmental efforts in Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d482e122a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d482e129" publication-type="book">
John Kampfner, Blair's wars (London: Free Press, 2004), p. 64.<person-group>
                     <string-name>
                        <surname>Kampfner</surname>
                     </string-name>
                  </person-group>
                  <fpage>64</fpage>
                  <source>Blair's wars</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e154a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d482e161" publication-type="journal">
John Vereker, 'Blazing the trail:
eight years of change in handling international development', Development Policy Review 20: 2, 2002,
pp. 133-40 .<person-group>
                     <string-name>
                        <surname>Vereker</surname>
                     </string-name>
                  </person-group>
                  <issue>2</issue>
                  <fpage>133</fpage>
                  <volume>20</volume>
                  <source>Development Policy Review</source>
                  <year>2002</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e199a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d482e206" publication-type="book">
OECD Development Assistance Committee, Shaping the 21st century: the contribution of development co-
operation (Paris: OECD, 1996)<person-group>
                     <string-name>
                        <surname>OECD Development Assistance Committee</surname>
                     </string-name>
                  </person-group>
                  <source>Shaping the 21st century: the contribution of development cooperation</source>
                  <year>1996</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d482e230" publication-type="book">
DFID, International Development White Paper, Eliminating poverty: a
challenge for the 21st century (London: DFID, Nov. 1997).<person-group>
                     <string-name>
                        <surname>DFID, International Development White Paper</surname>
                     </string-name>
                  </person-group>
                  <source>Eliminating poverty: a challenge for the 21st century</source>
                  <year>1997</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e255a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d482e262" publication-type="book">
Clare Short, An honourable deception? New Labour, Iraq and the misuse of power (London: Free Press, 2004),
pp. 85-8.<person-group>
                     <string-name>
                        <surname>Short</surname>
                     </string-name>
                  </person-group>
                  <fpage>85</fpage>
                  <source>An honourable deception? New Labour, Iraq and the misuse of power</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e292a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d482e299" publication-type="other">
Vereker, 'Blazing the trail</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d482e305" publication-type="other">
Clare Short, interview with author, Nov. 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e312a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d482e319" publication-type="other">
Short, Honourable deception?, p. 78.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e326a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d482e333" publication-type="book">
Short exaggerates when she says in Honourable deception? (p. 78)<person-group>
                     <string-name>
                        <surname>Short</surname>
                     </string-name>
                  </person-group>
                  <fpage>78</fpage>
                  <source>Honourable deception</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e355a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d482e362" publication-type="book">
Peter Unwin, Hearts, minds and interests: Britain's place in the world (London: Profile, 1998), pp. 167-8.<person-group>
                     <string-name>
                        <surname>Unwin</surname>
                     </string-name>
                  </person-group>
                  <fpage>167</fpage>
                  <source>Hearts, minds and interests: Britain's place in the world</source>
                  <year>1998</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e387a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d482e394" publication-type="journal">
BBC News, 9 March 2001<issue>9 March</issue>
                  <source>BBC News</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e410a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d482e417" publication-type="other">
Robin Cook, FCO mission statement, May 1997</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d482e423" publication-type="other">
interview with author, Nov. 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e431a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d482e438" publication-type="other">
Cook, FCO mission statement.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e445a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d482e452" publication-type="other">
Kampfner, Blair's wars, p. 15.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e459a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d482e466" publication-type="journal">
Rita Abrahamsen and Paul Williams, 'The antinomies of New Labour's third way in sub-Saharan
Africa', Political Studies 49, 2001, pp. 249-64.<person-group>
                     <string-name>
                        <surname>Abrahamsen</surname>
                     </string-name>
                  </person-group>
                  <fpage>249</fpage>
                  <volume>49</volume>
                  <source>Political Studies</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e498a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d482e505" publication-type="book">
Antony Goldman, 'Nigeria: many problems, few solutions', in Richard Haas, ed.,
Transatlantic tensions (Washington DC: Brookings, 1999), p. 217.<person-group>
                     <string-name>
                        <surname>Goldman</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Nigeria: many problems, few solutions</comment>
                  <fpage>217</fpage>
                  <source>Transatlantic tensions</source>
                  <year>1999</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e537a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d482e544" publication-type="other">
Africa Conflict Prevention Pool in
2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d482e553" publication-type="book">
Greg Austin et al., Evaluation
of Conflict Prevention Pools (London: DFID, 2004).<person-group>
                     <string-name>
                        <surname>Austin</surname>
                     </string-name>
                  </person-group>
                  <source>Evaluation of Conflict Prevention Pools</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e578a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d482e585" publication-type="other">
interview with author, Oct. 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e593a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d482e600" publication-type="other">
interview with author, Nov.
2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e610a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d482e617" publication-type="other">
Kampfner, Blair's wars, pp. 10-15.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e624a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d482e631" publication-type="other">
Short, An honourable deception?, p. 77.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e638a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d482e645" publication-type="other">
Tony Blair, speech delivered to Lord Mayor's Banquet, Nov. 1997.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e652a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d482e659" publication-type="other">
Short, interview with author, Nov. 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e666a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d482e673" publication-type="other">
Sir Thomas Legg and Sir Robin Ibbs, Report of the Sierra Leone arms investigation (London: Stationery
Office, July 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d482e684a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d482e691" publication-type="other">
Cook blamed FCO staff in the Africa Directorate: interview with author, Nov. 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e698a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d482e705" publication-type="other">
Kampfner, Blair's wars, p. 68.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e712a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d482e719" publication-type="other">
Tony Blair, speech delivered to Chicago Economics Club, 22 April 1999</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d482e725" publication-type="journal">
BBC Radio 4, Start the Week, 18 Oct. 2004.<issue>18 Oct</issue>
                  <volume>4</volume>
                  <source>Start the Week</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e744a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d482e751" publication-type="other">
Kampfner says (Blair's wars, p. 53)</mixed-citation>
            </p>
         </fn>
         <fn id="d482e758a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d482e765" publication-type="other">
Short, interview with author, Nov. 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d482e771" publication-type="other">
Hansard (Commons), 7 March 2001, col. 294.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e778a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d482e785" publication-type="other">
Kampfner, Blair's wars, p. 73.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e793a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d482e800" publication-type="other">
interview with author, Dec. 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e807a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d482e814" publication-type="book">
Tony Blair, speech delivered to Labour Party conference, Sept. 2001.<person-group>
                     <string-name>
                        <surname>Blair</surname>
                     </string-name>
                  </person-group>
                  <source>Speech delivered to Labour Party conference, Sept. 2001</source>
                  <year>2001</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e836a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d482e843" publication-type="book">
Peter Stothard, 30 days: a month at the heart of Blair's war (London: Harper Collins, 2003), p. 42.<person-group>
                     <string-name>
                        <surname>Stothard</surname>
                     </string-name>
                  </person-group>
                  <fpage>42</fpage>
                  <source>30 days: a month at the heart of Blair's war</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e868a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d482e875" publication-type="book">
International Crisis Group, Liberia and Sierra Leone: rebuilding failed states (Brussels and Dakar: ICG,
2004).<person-group>
                     <string-name>
                        <surname>International Crisis Group</surname>
                     </string-name>
                  </person-group>
                  <source>Liberia and Sierra Leone: rebuilding failed states</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d482e900a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d482e907" publication-type="other">
Austin et al., Evaluation of Conflict Prevention Pools.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e914a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d482e921" publication-type="other">
UN expert panel on the illegal exploitation of the natural resources of the Democratic Republic of Congo, Oct.
2002.</mixed-citation>
            </p>
         </fn>
         <fn id="d482e932a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d482e939" publication-type="book">
Abrahamsen and Williams, The antinomies of New Labour's third way, pp. 254-8.<person-group>
                     <string-name>
                        <surname>Abrahamsen</surname>
                     </string-name>
                  </person-group>
                  <fpage>254</fpage>
                  <source>The antinomies of New Labour's third way</source>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
