<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt1df4hs4</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Africa and the Gulf Region</book-title>
         <subtitle>Blurred Boundaries and Shifting Ties</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Abusharaf</surname>
               <given-names>Rogaia Mustafa</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Eickelman</surname>
               <given-names>Dale F.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>09</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9783940924704</isbn>
      <isbn content-type="epub">9783940924711</isbn>
      <publisher>
         <publisher-name>Gerlach Press</publisher-name>
         <publisher-loc>Berlin, Germany</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Gulf Research Center Cambridge</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1df4hs4"/>
      <abstract abstract-type="short">
         <p>The ties that bind Africa and the Gulf region have deep historical roots that influence both what Braudel called the longue durée and the short-term events of current policy shifts, market-based economic fluctuations, and global and local political vicissitudes. This book, a collaboration of historians, political scientists, development planners, and a biomedical engineer, explores Arabian-African relationships in their many overlapping dimensions. Thus histories constructed from the “bottom up" - records of the everyday activities of commerce, intermarriage, and gender roles - offer an incisive complement to the “top down" histories of dynasties and the elite. Topics such as migration, collective memory, scriptural and oral narratives, and contemporary notions of food security and “soft" power pose new questions about the ties that bind Africa to the Gulf.</p>
      </abstract>
      <counts>
         <page-count count="175"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>[i]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>[v]</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.3</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                     <subtitle>Africa and the Gulf Region Blurred Boundaries and Shifting Ties</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Eickelman</surname>
                           <given-names>Dale F.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Abusharaf</surname>
                           <given-names>Rogaia Mustafa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>A paradox marks understanding the historical and contemporary relationships of Africa with the Arab Gulf region. Geographic propinquity and economic and cultural exchanges over millennia notwithstanding, both modern and medieval scholars have often conceived of the Gulf region and Africa as largely separate. The two regions have deep historical roots that influence both the<italic>longue durée</italic>and the short-term contemporary events of policy shifts, market-based economic fluctuations, and global and local political vicissitudes. The Gulf region is one of mixed populations and “creole” societies, in which both spoken languages and syntax—to mention only one dimension of the societies in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Gender and Geography in the Land of Punt</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sonbol</surname>
                           <given-names>Amira El-Azhary</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>6</fpage>
                  <abstract>
                     <p>The Temple of Hatchepsut, Dayr al-Bahri, lies at the base of the mountainous Western shore of the town of Luxor in Egypt; standing as a monument to gender struggle dating back to the 15<sup>th</sup>century, built by a queen who dressed as a man, ruled with a firm hand and seemingly loved by the people. Followed by her brother Tuthmose to the throne, he attempted to erase all evidence of his sister’s greatness as Egypt’s ruler, deforming the faces of her statues, hiding her grave and literally covering many depictions of her accomplishments. This defacement is unique in Ancient Egypt,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Queen of Sheba in Yemeni and Ethiopian Mythology</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Al-Thani</surname>
                           <given-names>Al-Johara Hassan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>The narrative of the Queen of Sheba is a myth intrinsically linked with the creation of distinct histories and identities of Yemenis and Ethiopians. As a character, the Queen of Sheba (Bilqis, as she is known in the Arab/Islamic tradition) has been subject to various forms of appropriations ranging from the national to the dynastic, revolving around issues of identity and hegemonic rule.</p>
                     <p>This chapter explores the significance of the narrative of the Queen of Sheba upon conceptions of identity and consciousness amongst the populations of Yemen and Ethiopia. The perception of the divide between these two regions shall be</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Diasporic Routes:</title>
                     <subtitle>African Passages to the Gulf</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hopper</surname>
                           <given-names>Matthew S.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>41</fpage>
                  <abstract>
                     <p>A substantial African presence is visible in the Gulf today, but the question of the origins of the Gulf’s sizable African diaspora is a complicated one. Africa has a long and historical connection with the Gulf: Africans are widely known to have participated in the Zanj slave revolt of Basra in the ninth century CE. Yet most of the official literature on African musical and dance traditions in the Gulf is vague on how African traditions found their way to the Gulf, highlighting the Gulf ’ s cosmopolitanism and its role as a “cultural crossroads” without reference to the people</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The 1964 Zanzibar Genocide:</title>
                     <subtitle>The Politics of Denial</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ibrahim</surname>
                           <given-names>Abdullahi Ali</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>On the fiftieth anniversary of the killing and raping of the Arabs of Zanzibar in the wake of the 1964 Revolution, this chapter seeks to establish that the revolution constituted genocide. In light of the almost complete failure of historians and commentators to foreground this tragedy, we argue that tragedy in Zanzibar has been a “denied” genocide. No reference whatsoever is made to it in the fast growing literature on genocide published in the last two decades,¹ except for a passing reference in a 2002 reader edited by Alexander Hinton.² The chapter argues that this genocide is commonly ignored even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Gulfrica:</title>
                     <subtitle>Blowing the Horn of Light into Afrabia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ghassany</surname>
                           <given-names>Harith</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>74</fpage>
                  <abstract>
                     <p>This chapter has two aims. The first is to clarify the contemporary history of Zanzibar by separating the association between East Africa slave trade and the 1964 revolution and to demonstrate the positive accelerating effects of the dissociation in terms of the current and future relationship between the Gulf and sub-Saharan Africa. The second is to renew ties between Arabia and Africa by removing barriers that erected in the last half century.</p>
                     <p>For thousands of years the East African coast and the Arabian Peninsula have been natural trading partners. They were part and parcel of the Indian Ocean civilization, which</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Neoliberal Challenges and Transnational Lives of Cameroonian Migrants in Dubai</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pelican</surname>
                           <given-names>Michaela</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>Dubai—“the kingdom of bling,” as one journalist called it²— may count as an example of a neoliberal economy in which government regulations are aimed at facilitating the rule of the free market. With its aspirations to world excellence in all economic sectors, including tourism, banking, media, education, and industries, the Dubai government cultivates exalted expectations of enrichment and profit maximization among local Emirati as well as foreign investors and labor force. The latter account for more than 90 percent of the city’s population and constitute the backbone of Dubai’s economy. At the same time, their stay is generally transitory</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Inclusive Growth, Governance of Natural Resources and Sustainable Development in Africa from a Qatari Perspective</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McSparren</surname>
                           <given-names>Jason J.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tok</surname>
                           <given-names>Mohamed Evren</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shaw</surname>
                           <given-names>Timothy M.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Besada</surname>
                           <given-names>Hany</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>Inclusive growth, governance of natural resources and sustainable development are elements of a vision for a prosperous, thriving African future. African states with established, as well as, nascent natural resource-based economies are increasingly supporting and implementing new governance mechanisms designed to capture greater wealth for the state that in part may be invested into local societies. The African Mining Vision (AMV) and Extractive Industry Transparency Initiative (EITI) are two examples among several.¹ African states with resource endowments must be strategic when developing policy and should look to other states for positive lessons. This paper will analyze Qatar as a partner</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Qatar’s Food and Water Security:</title>
                     <subtitle>An Evolving Strategy</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stoll</surname>
                           <given-names>Daniel C.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>Qatar’s quest for viable food and water security programs has its origins in the substantial increases in global food prices in 2007 and 2008. As with many countries at that time, Qatar fund itself whipsawed by the cumulative effects of bad harvests around the world in 2005 and 2006, as well as the impact of increasingly higher energy costs. Other factors contributing to this surge in food prices included: a rising demand across the globe for food; efforts to counter higher energy prices through increased use of biofuels; and population growth rates that caused policy makers to brace for even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Healthy Relationships?</title>
                     <subtitle>GCC Global Health Engagement in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bhacker</surname>
                           <given-names>Mariam M. R.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zaman</surname>
                           <given-names>Muhammad Hamid</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>The Gulf states have witnessed rapid and unprecedented growth in the last four decades. Attainment of oil wealth funded the expansion of infrastructure and has led to improvements in the quality of living. The Gulf’s development has had a major impact on domestic health, with increased access to quality healthcare and reductions in infectious disease and maternal and child deaths.</p>
                     <p>During the same period of growth, the Gulf states have engaged in development activities around the world. Saudi Arabia, the United Arab Emirates (UAE) and Kuwait are among the biggest donors of aid in the world. These countries’ contributions to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.13</book-part-id>
                  <title-group>
                     <title>About the Contributors</title>
                  </title-group>
                  <fpage>165</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1df4hs4.14</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>167</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
