<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">imfstaffpapers</journal-id>
         <journal-id journal-id-type="jstor">j100845</journal-id>
         <journal-title-group>
            <journal-title>IMF Staff Papers</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Monetary Fund</publisher-name>
         </publisher>
         <issn pub-type="ppub">10207635</issn>
         <issn pub-type="epub">15645150</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30035946</article-id>
         <title-group>
            <article-title>How Much Do Trading Partners Matter for Economic Growth?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Vivek</given-names>
                  <surname>Arora</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Athanasios</given-names>
                  <surname>Vamvakidis</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">52</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i30035943</issue-id>
         <fpage>24</fpage>
         <lpage>40</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 International Monetary Fund</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30035946"/>
         <abstract>
            <p>This paper empirically examines the extent to which a country's economic growth is influenced by the economies of its trading partners. Panel estimation results based on four decades of data for more than 100 countries show that trading partners' growth has a strong effect on domestic growth, even after controlling for the influence of common global and regional trends. The results are robust to instrumental variable estimation and other robustness tests. Trading partners' relative income levels are also positively correlated with growth, suggesting that the richer a country's trading partners, the stronger is conditional convergence. A general implication of the results is that countries benefit from trading with fast-growing and relatively more developed countries.</p>
         </abstract>
         <kwd-group>
            <kwd>F43</kwd>
            <kwd>F15</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d876e199a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d876e206" publication-type="other">
The Economist (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e213a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d876e220" publication-type="other">
Baldwin (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e227a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d876e234" publication-type="other">
Arora and Vamvakidis (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e240" publication-type="other">
Ahmed and Loungani (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e247a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d876e254" publication-type="other">
Spilimbergo (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e262a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d876e269" publication-type="other">
IMF, 2002</mixed-citation>
            </p>
         </fn>
         <fn id="d876e276a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d876e283" publication-type="other">
Bhagwati
and Srinivasan (1985)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e292" publication-type="other">
Greenaway, Morgan, and Wright (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e298" publication-type="other">
Bhagwati and Srinivasan (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e305" publication-type="other">
Baldwin (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e312a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d876e319" publication-type="other">
Barro and Sala-i-Martin (1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e326a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d876e333" publication-type="other">
Sachs and Warner (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e339" publication-type="other">
Rodriguez and
Rodrik (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e349a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d876e356" publication-type="other">
Barro and Sala-i-Martin (1995)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e363a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d876e370" publication-type="other">
Caselli, Esquivel, and Lefort (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e378a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d876e385" publication-type="other">
Levine and Renelt, 1992</mixed-citation>
            </p>
         </fn>
         <fn id="d876e392a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d876e399" publication-type="other">
Navaretti and Tarr (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d876e406a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d876e413" publication-type="other">
Sachs and Warner (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e419" publication-type="other">
Rodriguez and Rodrik (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d876e425" publication-type="other">
Warner (2002)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d876e441a1310">
            <mixed-citation id="d876e445" publication-type="other">
Agenor, Pierre-Richard, John McDermott, and Eswar Prasad, 1999, "Macroeconomic Fluctuations
in Developing Countries: Some Stylized Facts," IMF Working Paper 99/35 (Washington:
International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d876e458a1310">
            <mixed-citation id="d876e462" publication-type="other">
Ahmed, Shaghil, and Prakash Loungani, 1999, "Business Cycles in Emerging Market
Economies," Monetaria, Volume XXII (October-December), Centro de Estudios Monetarios
Latinoamericanos (CEMLA).</mixed-citation>
         </ref>
         <ref id="d876e475a1310">
            <mixed-citation id="d876e479" publication-type="other">
Arora, Vivek, and Athanasios Vamvakidis, 2004, "The Impact of U.S. Economic Growth on the
Rest of the World: How Much Does It Matter?" Journal of Economic Integration, Vol. 19
(March), pp. 1-18.</mixed-citation>
         </ref>
         <ref id="d876e492a1310">
            <mixed-citation id="d876e496" publication-type="other">
Baldwin, Robert E., 2003, "Openness and Growth: What's the Empirical Relationship?" NBER
Working Paper No. 9578 (Cambridge, Massachusetts: National Bureau of Economic
Research).</mixed-citation>
         </ref>
         <ref id="d876e510a1310">
            <mixed-citation id="d876e514" publication-type="other">
Barro, Robert, and Xavier Sala-i-Martin, 1995, Economic Growth (New York: McGraw Hill).</mixed-citation>
         </ref>
         <ref id="d876e521a1310">
            <mixed-citation id="d876e525" publication-type="other">
Ben-David, Dan, 1993, "Equalizing Exchange: Trade Liberalization and Income Convergence,"
Quarterly Journal of Economics, Vol. 108 (August), pp. 653-79.</mixed-citation>
         </ref>
         <ref id="d876e535a1310">
            <mixed-citation id="d876e539" publication-type="other">
Bhagwati, Jagdish N., and T. N. Srinivasan, 1985, "Trade Policy and Development," in
Dependence and Interdependence, Essays in Development Economics, Vol. 2, ed. by Gene
Grossman (Cambridge, Massachusetts: MIT Press).</mixed-citation>
         </ref>
         <ref id="d876e552a1310">
            <mixed-citation id="d876e556" publication-type="other">
---, 2002, "Trade and Poverty in Poor Countries," American Economic Review, Vol. 92
(May), pp. 180-83.</mixed-citation>
         </ref>
         <ref id="d876e566a1310">
            <mixed-citation id="d876e570" publication-type="other">
Brunner, Allan D., 2003, "The Long-Run Effects of Trade on Income and Income Growth,"
IMF Working Paper 03/37 (Washington: International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d876e580a1310">
            <mixed-citation id="d876e584" publication-type="other">
Caselli, Francesco, Gerardo Esquivel, and Fernando Lefort, 1996, "Reopening the Convergence
Debate: A New Look at Cross-Country Growth Empirics," Journal of Economic Growth,
Vol. 1 (September), pp. 363-89.</mixed-citation>
         </ref>
         <ref id="d876e598a1310">
            <mixed-citation id="d876e602" publication-type="other">
Clemens, Michael, and Jeffrey Williamson, 2004, "Why Did the Tariff-Growth Correlation
Reverse After 1950?" Journal of Economic Growth, Vol. 9, No. 1, pp. 5-46.</mixed-citation>
         </ref>
         <ref id="d876e612a1310">
            <mixed-citation id="d876e616" publication-type="other">
Coe, David, and Elhanan Helpman, 1995, "International R&amp;D Spillovers," European Economic
Review, Vol. 39, No. 5, pp. 859-87.</mixed-citation>
         </ref>
         <ref id="d876e626a1310">
            <mixed-citation id="d876e630" publication-type="other">
---, and Alexander Hoffmaister, 1997, "North-South R&amp;D Spillovers," Economic Journal,
Vol. 107 (January), pp. 134-49.</mixed-citation>
         </ref>
         <ref id="d876e640a1310">
            <mixed-citation id="d876e644" publication-type="other">
Dollar, David, 1992, "Outward-Oriented Developing Economies Really Do Grow More Rapidly:
Evidence from 95 LDCs, 1976-1985," Economic Development and Cultural Change,
Vol. 40 (April), pp. 523-44.</mixed-citation>
         </ref>
         <ref id="d876e657a1310">
            <mixed-citation id="d876e661" publication-type="other">
Economist, The, 2002, "United We Fall," September 26.</mixed-citation>
         </ref>
         <ref id="d876e668a1310">
            <mixed-citation id="d876e672" publication-type="other">
Edwards, Sebastian, 1998, "Openness, Productivity and Growth: What Do We Really Know?"
Economic Journal, Vol. 108 (March), pp. 383-98.</mixed-citation>
         </ref>
         <ref id="d876e683a1310">
            <mixed-citation id="d876e687" publication-type="other">
Frankel, Jeffrey A., and David Romer, 1999, "Does Trade Cause Growth?" American Economic
Review, Vol. 89, No. 3, pp. 379-99.</mixed-citation>
         </ref>
         <ref id="d876e697a1310">
            <mixed-citation id="d876e701" publication-type="other">
Greenaway, David, Wyn Morgan, and Peter Wright, 1998, "Trade Reform, Adjustment and
Growth: What Does the Evidence Tell Us?" Economic Journal, Vol. 108 (September),
pp. 1547-61.</mixed-citation>
         </ref>
         <ref id="d876e714a1310">
            <mixed-citation id="d876e718" publication-type="other">
Harrison, Ann, 1996, "Openness and Growth: A Time-Series, Cross-Country Analysis for
Developing Countries," Journal of Development Economics, Vol. 48 (March), pp. 419-47.</mixed-citation>
         </ref>
         <ref id="d876e728a1310">
            <mixed-citation id="d876e732" publication-type="other">
International Monetary Fund, 2002, Direction of Trade Statistics Yearbook (Washington:
International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d876e742a1310">
            <mixed-citation id="d876e746" publication-type="other">
Levine, Ross, and David Renelt, 1992, "A Sensitivity Analysis of Cross-Country Growth
Regressions," American Economic Review, Vol. 82 (September), pp. 942-63.</mixed-citation>
         </ref>
         <ref id="d876e756a1310">
            <mixed-citation id="d876e760" publication-type="other">
Navaretti, Giorgio Barba, and David Tarr, 2000, "International Knowledge Flows and Economic
Performance: A Review of the Evidence," World Bank Economic Review, Vol. 14 (January),
pp. 1-15.</mixed-citation>
         </ref>
         <ref id="d876e774a1310">
            <mixed-citation id="d876e778" publication-type="other">
Rodriguez, Francisco, and Dani Rodrik, 1999, "Trade Policy and Economic Growth: A Skeptic's
Guide to the Cross-National Evidence," NBER Working Paper No. 7081 (Cambridge,
Massachusetts: National Bureau of Economic Research).</mixed-citation>
         </ref>
         <ref id="d876e791a1310">
            <mixed-citation id="d876e795" publication-type="other">
Sachs, Jeffrey D., and Andrew Warner, 1995, "Economic Reform and the Process of Global
Integration," Brookings Papers on Economic Activity: 1, Brookings Institution, pp. 1-95.</mixed-citation>
         </ref>
         <ref id="d876e805a1310">
            <mixed-citation id="d876e809" publication-type="other">
Spilimbergo, Antonio, 2000, "Growth and Trade: The North Can Lose," Journal of Economic
Growth, Vol. 5 (June), pp. 131-46.</mixed-citation>
         </ref>
         <ref id="d876e819a1310">
            <mixed-citation id="d876e823" publication-type="other">
Vamvakidis, Athanasios, 1998, "Regional Integration and Economic Growth," World Bank
Economic Review, Vol. 12 (May), pp. 251-70.</mixed-citation>
         </ref>
         <ref id="d876e833a1310">
            <mixed-citation id="d876e837" publication-type="other">
---, 1999, "Regional Trade Agreements or Broad Liberalization: Which Path Leads to
Faster Growth?" IMF Staff Papers, International Monetary Fund, Vol. 46, No. 1, pp. 42-68.</mixed-citation>
         </ref>
         <ref id="d876e847a1310">
            <mixed-citation id="d876e851" publication-type="other">
---, 2002, "How Robust Is the Growth-Openness Connection? Historical Evidence," Journal
of Economic Growth, Vol. 7, No. 1, pp. 57-80.</mixed-citation>
         </ref>
         <ref id="d876e862a1310">
            <mixed-citation id="d876e866" publication-type="other">
Warner, Andrew, 2002, "Once More into the Breach: Economic Growth and Global Integration"
(unpublished; Cambridge, Massachusetts: Harvard University).</mixed-citation>
         </ref>
         <ref id="d876e876a1310">
            <mixed-citation id="d876e880" publication-type="other">
World Bank, 2002, World Development Indicators (Washington: World Bank).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
