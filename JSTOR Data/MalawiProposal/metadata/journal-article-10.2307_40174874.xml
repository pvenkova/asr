<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">africaspec</journal-id>
         <journal-id journal-id-type="jstor">j50000361</journal-id>
         <journal-title-group>
            <journal-title>Africa Spectrum</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>German Institute of Global and Area Studies</publisher-name>
         </publisher>
         <issn pub-type="ppub">00020397</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40174874</article-id>
         <title-group>
            <article-title>'Living PositHIVely in Tanzania'. The Global Dynamics of AIDS and the Meaning of Religion for International and Local AIDS Work</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Hansjörg</given-names>
                  <surname>Dilger</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i40005059</issue-id>
         <fpage>73</fpage>
         <lpage>90</lpage>
         <self-uri xlink:href="https://www.jstor.org/stable/40174874"/>
         <abstract>
            <p>AIDS work in Africa is to a great extent based on models which originated in North America or Europe and may be, for this reason, culturally inappropriate for the international setting. The global dynamics of AIDS work are explored in the paper with regard to a neglected topic of research: the care and support for people living with HIV/AIDS in sub-Saharan Africa. Referring to the concept of living positHIVely, the author describes where and how the model came into use, how it has later been appropriated by Tanzanian NGOs and how it shapes today the lives of PWHAs in the country's major town, Dar es Salaam. It is argued that, as religion plays an important role for the conceptions of illness and healing in Tanzania, religious-spiritual elements have contributed strongly to the understanding of a 'positive' life with HIV (Swahili: kuishi kwa matumaini = living with hope). The paper concludes by calling for an integrating approach in international and local AIDS work which takes into account both the working experiences from the North, as well as the cultural conceptions and circumstances that shape the respective contexts in which AIDS work takes place. /// AIDS-Programme in Afrika basieren zu einem großen Teil auf Arbeitsmodellen, die in Nordamerika Oder Europa entstanden sind und daher für die Anwendung in anderen internationalen Kontexten ungeeignet sein können. Diese globale Dynamik der AIDS-Arbeit wird hinsichtlich eines bisher vernachlässigten Forschungsthemas untersucht: der Versorgung und sozialen Einbindung von Menschen mit HIV/AIDS im sub-saharischen Afrika. Ausgehend vom Konzept 'PositHIV leben' zeigt der Autor, wo und wie dieses Arbeitsmodell zuerst zur Anwendung kam, wie es später von tanzanischen Nicht-Regierungs-Organisationen übernommen wurde und wie es heute von HIV-lnfizierten in Dar es Salaam, der größten Stadt des Landes, in ihr Leben integriert wird. Insofern Religion eine wichtige Rolle für Vorstellungen von Krankheit und Heilung in Tanzania spielt, wird ein 'positives' Leben mit HIV/AIDS (Swahili: kuishi kwa matumaini = Leben mit Hoffnung) stark von religiös-spirituellen Konzepten bestimmt. Abschließend plädiert der Autor für einen integrierenden Ansatz in der AIDS-Arbeit, der sowohl Arbeitserfahrungen aus dem Norden, als auch kulturelle Konzepte und Bedingungen, die den jeweiligen Kontext der AIDS-Arbeit prägen, mit einbezieht. /// Les programmes contre le SIDA en Afrique reposent en grande partie sur des modèles de travail conçus en Amérique du Nord ou en Europe et qui, done, peuvent ne pas être appropriés à d'autres contextes internationaux. Cette dynamique globale de l'action contre le SIDA est ici examinée à partir d'un thème de recherche jusqu'ici négligé : les soins et l'intégration sociale de personnes positives ou malades du SIDA en Afrique subsaharienne. A partir du concept « vivre positif», l'auteur montre oú et comment ce modèle de travail a d'abord été mis en pratique, comment il a été adopté par des ONG tanzaniennes et comment il est intégré aujourd'hui dans la vie de personnes positives à Dar es-Salaam, la plus grande ville du pays. Dans la mesure où la religion joue en Tanzanie un grand rêle dans l'idée de maladie et de guérison, une vie « positive » avec le VIH ou le SIDA (souahéli : kuishi kwa matumaini = vivre avec espoir) est fortement déterminée par les concepts religieux et spirituels. En conclusion, l'auteur plaide pour une approche intégrative dans l'action contre le SIDA, qui prenne en compte aussi bien les expériences du Nord que les concepts culturels et les conditions respectives caractérisant l'action contre le SIDA.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1548e107a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1548e114" publication-type="other">
Medical Anthropology (Vienna, October 2000),</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e121a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1548e128" publication-type="other">
(Giffin 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e135a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1548e142" publication-type="other">
Dilger 2000: 169ff.</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e149a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1548e156" publication-type="other">
Ochsner 1997</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e164a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1548e171" publication-type="other">
(Hampton 1992: 5).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e178a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1548e185" publication-type="other">
Kaleeba/Ray/Willmore (1991: 101).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e191" publication-type="other">
Durban (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e198a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1548e205" publication-type="other">
(Madsen, K., e-mail:
16 Aug 2000).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e214" publication-type="other">
Mulindwa (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e220" publication-type="other">
Bujra/Mokake
(2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e230a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1548e237" publication-type="other">
(Last
1992).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e246" publication-type="other">
(Pool 1994a: 259f).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e252" publication-type="other">
(Pool 1994b).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e259a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1548e268" publication-type="other">
(Hardy 1998).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1548e284a1310">
            <mixed-citation id="d1548e288" publication-type="other">
Abrahams, R.D. (1970), "Positively black", Prentice-Hall, N.J.</mixed-citation>
         </ref>
         <ref id="d1548e295a1310">
            <mixed-citation id="d1548e299" publication-type="other">
Act Up New York, http://www.actupny.org/reports/.</mixed-citation>
         </ref>
         <ref id="d1548e306a1310">
            <mixed-citation id="d1548e310" publication-type="other">
The AIDS Channel, http://www.theaidschannel.com/.</mixed-citation>
         </ref>
         <ref id="d1548e317a1310">
            <mixed-citation id="d1548e321" publication-type="other">
Akaike, J. (1998), "Leben mit HIV/AIDS in Sanpatong. Zum Umgang mit AIDS bei HIV-infizierten
Frauen in einer ländlichen Region Nordthailands", Unpublished Master Thesis, Department of Social
Anthropology, Free University of Berlin.</mixed-citation>
         </ref>
         <ref id="d1548e335a1310">
            <mixed-citation id="d1548e339" publication-type="other">
Bauer, R.W. (1997), "Discovering impediments to successful HIV infection prevention efforts in East
Africa", Unpublished Manuscript, Maryknoll Institute of African Studies of St. Marys University,
Minnesota.</mixed-citation>
         </ref>
         <ref id="d1548e352a1310">
            <mixed-citation id="d1548e356" publication-type="other">
Berzon, B./Leighton, R. (eds.) (1979), "Positively gay", Millbrae/Califomia, Celestial Arts.</mixed-citation>
         </ref>
         <ref id="d1548e363a1310">
            <mixed-citation id="d1548e367" publication-type="other">
Bujra, J./Mokake, S.N. (2000), "AIDS activism in Dar es Salaam: many struggles; a single goal",
in: Baylies, C./Bujra, J. (eds.), "AIDS, sexuality, and gender", London, Routledge,154-174.</mixed-citation>
         </ref>
         <ref id="d1548e377a1310">
            <mixed-citation id="d1548e381" publication-type="other">
Crimp, D./Rolston, A. (1990), "AIDS Demo Graphics", Seattle.</mixed-citation>
         </ref>
         <ref id="d1548e388a1310">
            <mixed-citation id="d1548e392" publication-type="other">
Deutsche AIDS-Hilfe (1995), "Positiv leben - gut leben", Berlin.</mixed-citation>
         </ref>
         <ref id="d1548e399a1310">
            <mixed-citation id="d1548e403" publication-type="other">
Dilger, H. (2000), "AIDS ist ein Unfair: Metaphem und Bildlichkeit in AIDS-Diskursen Tanzanias",
afrika spectrum, 35 (2000) 2: 165-82.</mixed-citation>
         </ref>
         <ref id="d1548e414a1310">
            <mixed-citation id="d1548e418" publication-type="other">
Dilger, H. (2001), "Sexuality, morals and the lures of modernity: inconsistency and self-reflection in
young people's discourses on AIDS in rural Tanzania", unpublished manuscript, Department for
Social Anthropology, Free University of Berlin.</mixed-citation>
         </ref>
         <ref id="d1548e431a1310">
            <mixed-citation id="d1548e435" publication-type="other">
The Express (Dar es Salaam) 26/11 - 2/12/1998.</mixed-citation>
         </ref>
         <ref id="d1548e442a1310">
            <mixed-citation id="d1548e446" publication-type="other">
"Finances for Positive Living", http://www1.surfsouth.com/-uinsureu/contents.htm</mixed-citation>
         </ref>
         <ref id="d1548e453a1310">
            <mixed-citation id="d1548e457" publication-type="other">
Foster, P. (1996), "Cosmological aspects of Aids", in: Isabel, P./Ross, K./Cox, J. (eds.), "The role of
Christianity in development, peace and reconstruction, Nairobi, 191-200.</mixed-citation>
         </ref>
         <ref id="d1548e467a1310">
            <mixed-citation id="d1548e471" publication-type="other">
Garner, R.C. (2000), "Safe sects? Dynamic Religion and AIDS in South Africa", The Journal of
Modern African Studies, 38 (March 2000) 1 , 41 -69.</mixed-citation>
         </ref>
         <ref id="d1548e481a1310">
            <mixed-citation id="d1548e485" publication-type="other">
Giffin, K. (1998), "Beyond empowerment: heterosexualities and the prevention of AIDS", Social
Science and Medicine, 46 (1998) 2, 151-156.</mixed-citation>
         </ref>
         <ref id="d1548e496a1310">
            <mixed-citation id="d1548e500" publication-type="other">
Gruénais, M. (1999), "La religion préserve-t-elle du Sida?", Cahiers d'Études Africaines, 154,
39(2), 1999,253-270.</mixed-citation>
         </ref>
         <ref id="d1548e510a1310">
            <mixed-citation id="d1548e514" publication-type="other">
Hampton, J. (1992) (1st publ.: 1990), "Living positively with AIDS. The AIDS support organization
(TASO), Uganda", Strategies for Hope Series, No. 2, Actionaid, London et al.</mixed-citation>
         </ref>
         <ref id="d1548e524a1310">
            <mixed-citation id="d1548e528" publication-type="other">
Hardy, R.P. (1998), "Loving Men: Gay partners, Spirituality, and AIDS", New York.</mixed-citation>
         </ref>
         <ref id="d1548e535a1310">
            <mixed-citation id="d1548e539" publication-type="other">
"Hindu and Buddhist AIDS Memoriam", http://home.connexus.net.au/~vicente/</mixed-citation>
         </ref>
         <ref id="d1548e546a1310">
            <mixed-citation id="d1548e550" publication-type="other">
Islamic medical association of Uganda (1998), "AIDS education through Imams: A spiritually
motivated community effort in Uganda", UN AIDS case study (October 1998), Genf.</mixed-citation>
         </ref>
         <ref id="d1548e560a1310">
            <mixed-citation id="d1548e564" publication-type="other">
Kaleeba, N./Ray, S./Willmore, B. (1991), "We Miss You All. Noerine Kaleeba: AIDS in the Family",
Harare.</mixed-citation>
         </ref>
         <ref id="d1548e575a1310">
            <mixed-citation id="d1548e579" publication-type="other">
Klaits, F. (1998), "Making a Good Death; AIDS and Social Belonging in an Independent Church in
Gabarone", Botswana Notes and Records, 30 (1998), 101-119.</mixed-citation>
         </ref>
         <ref id="d1548e589a1310">
            <mixed-citation id="d1548e593" publication-type="other">
Last, M. (1992), "The importance of knowing about not knowing: observations from Hausaland", in:
Feierman, S./Janzen, J.M. (eds.), "The social basis of health and healing", Berkely, 393-406.</mixed-citation>
         </ref>
         <ref id="d1548e603a1310">
            <mixed-citation id="d1548e607" publication-type="other">
Luig, U. (1999), "Constructing local worlds. Spirit possession in the Gwembe Valley, Zambia", in:
Behrend, H./Luig, U. (eds.), "Spirit possession. Modernity and power in Africa", Oxford, 124-141.</mixed-citation>
         </ref>
         <ref id="d1548e617a1310">
            <mixed-citation id="d1548e621" publication-type="other">
Marshall- Fratani, R. (1998), "Mediating the global and the local in Nigerian Pentecostal ism",
Journal of Religion in Africa, 28 (1998) 3, 278-315.</mixed-citation>
         </ref>
         <ref id="d1548e631a1310">
            <mixed-citation id="d1548e635" publication-type="other">
Media-AIDS, http://www.hivnet.ch:8000/global/media-aids/.</mixed-citation>
         </ref>
         <ref id="d1548e642a1310">
            <mixed-citation id="d1548e646" publication-type="other">
Meyer, B./Geschiere, P. (1999), "Globalization and identity: dialectics of flow and closure.
Introduction", in: Meyer, B./Geschiere, P. (eds.): "Globalization and identity: dialectics of flow and
closure", Oxford, 1-15.</mixed-citation>
         </ref>
         <ref id="d1548e660a1310">
            <mixed-citation id="d1548e664" publication-type="other">
Mulindwa, J.M. (1994), "Living positively with AIDS", Dar es Salaam.</mixed-citation>
         </ref>
         <ref id="d1548e671a1310">
            <mixed-citation id="d1548e675" publication-type="other">
Ochsner, M. (1997) "Positiv leben mit Parkinson. Handbuch für ein gutes Leben mit der
Parkinsonkrankheit", Roche Pharma (Schweiz) AG.</mixed-citation>
         </ref>
         <ref id="d1548e685a1310">
            <mixed-citation id="d1548e689" publication-type="other">
Pool, R. (1994a) "Dialogue and the interpretation of illness: Conversations in a Cameroon village",
Oxford.</mixed-citation>
         </ref>
         <ref id="d1548e699a1310">
            <mixed-citation id="d1548e703" publication-type="other">
Pool, R. (1994b) "On the creation and dissolution of ethnomedical systems in the medical
ethnography of Africa", Africa 64 (1994) 1 , 1-20.</mixed-citation>
         </ref>
         <ref id="d1548e713a1310">
            <mixed-citation id="d1548e717" publication-type="other">
Public Health, Seattle &amp; King County (2000), "Positive living. A practical guide for people with HIV",
http://www.metrokc.gov/health/apu/workbook/toc.htm.</mixed-citation>
         </ref>
         <ref id="d1548e727a1310">
            <mixed-citation id="d1548e731" publication-type="other">
Pwha-Net, http://www.hivnet.ch:8000/topics/pwha-net/.</mixed-citation>
         </ref>
         <ref id="d1548e739a1310">
            <mixed-citation id="d1548e743" publication-type="other">
Schwartzberg, S. (1996), "A crisis of meaning. How gay men are making sense of AIDS", New York.</mixed-citation>
         </ref>
         <ref id="d1548e750a1310">
            <mixed-citation id="d1548e754" publication-type="other">
UNAIDS (1999) "From principle to Practice. Greater involvement of people living with or affected by
HIV/AIDS (GIPA) ", Genf.</mixed-citation>
         </ref>
         <ref id="d1548e764a1310">
            <mixed-citation id="d1548e768" publication-type="other">
UNAIDS (2000), "Report on the global HIV/AIDS epidemic", Genf.</mixed-citation>
         </ref>
         <ref id="d1548e775a1310">
            <mixed-citation id="d1548e779" publication-type="other">
United Republic of Tanzania, Ministry of Health (1999), "National AIDS Control Programme.
HIV/AIDS/STD surveillance, Report No.13, December 1998", Dar es Salaam.</mixed-citation>
         </ref>
         <ref id="d1548e789a1310">
            <mixed-citation id="d1548e793" publication-type="other">
U.S. Department of State (2000), "Preventing AIDS: an investment in global prosperity",
http://www.thebody.com/state_dept/summers.html.</mixed-citation>
         </ref>
         <ref id="d1548e803a1310">
            <mixed-citation id="d1548e807" publication-type="other">
Wimberley, K. (1995), "Becoming saved1 as a strategy of control. The role of religion in combating
HIV/AIDS in a Ugandan community. (From backsliding to manoeuvring: adolescent girls, salvation
and AIDS in Ankole, Uganda)"; in: Thirty-eighth annual meeting of the African Studies Association:
Orlando, Florida, November 3-6, 1995; Orlando / Florida: S.1-21 .</mixed-citation>
         </ref>
         <ref id="d1548e824a1310">
            <mixed-citation id="d1548e828" publication-type="other">
Wolf, Angelika (2000), "AIDS in Malawi. Lokale Diskurse eines globalen Phänomens", unpublished
master thesis, Department for Social Anthropology, Free University of Berlin.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
