<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">philtranbiolscie</journal-id>
         <journal-id journal-id-type="jstor">j100835</journal-id>
         <journal-title-group>
            <journal-title>Philosophical Transactions: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">09628436</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30040903</article-id>
         <article-id pub-id-type="pub-doi">10.1098/rstb.2004.1595</article-id>
         <title-group>
            <article-title>Prospects for Monitoring Freshwater Ecosystems towards the 2010 Targets</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>C.</given-names>
                  <surname>Revenga</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>I.</given-names>
                  <surname>Campbell</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>R.</given-names>
                  <surname>Abell</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>P.</given-names>
                  <surname>de Villiers</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>M.</given-names>
                  <surname>Bryer</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>28</day>
            <month>2</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">360</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1454</issue>
         <issue-id>i30040888</issue-id>
         <fpage>397</fpage>
         <lpage>413</lpage>
         <permissions>
            <copyright-statement>Copyright 2005 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30040903"/>
         <abstract>
            <p>Human activities have severely affected the condition of freshwater ecosystems worldwide. Physical alteration, habitat loss, water withdrawal, pollution, overexploitation and the introduction of nonnative species all contribute to the decline in freshwater species. Today, freshwater species are, in general, at higher risk of extinction than those in forests, grasslands and coastal ecosystems. For North America alone, the projected extinction rate for freshwater fauna is five times greater than that for terrestrial fauna-a rate comparable to the species loss in tropical rainforest. Because many of these extinctions go unseen, the level of assessment and knowledge of the status and trends of freshwater species are still very poor, with species going extinct before they are even taxonomically classified. Increasing human population growth and achieving the sustainable development targets set forth in 2002 will place even higher demands on the already stressed freshwater ecosystems, unless an integrated approach to managing water for people and ecosystems is implemented by a broad constituency. To inform and implement policies that support an integrated approach to water management, as well as to measure progress in halting the rapid decline in freshwater species, basinlevel indicators describing the condition and threats to freshwater ecosystems and species are required. This paper discusses the extent and quality of data available on the number and size of populations of freshwater species, as well as the change in the extent and condition of natural freshwater habitats. The paper presents indicators that can be applied at multiple scales, highlighting the usefulness of using remote sensing and geographical information systems technologies to fill some of the existing information gaps. Finally, the paper includes an analysis of major data gaps and information needs with respect to freshwater species to measure progress towards the 2010 biodiversity targets.</p>
         </abstract>
         <kwd-group>
            <kwd>Freshwater species</kwd>
            <kwd>Freshwater ecosystems</kwd>
            <kwd>Ecosystem condition indicators</kwd>
            <kwd>Freshwater monitoring</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d71e226a1310">
            <mixed-citation id="d71e230" publication-type="other">
Abramovitz, J. N. 1996 Imperiled waters, impoverished future:
the decline of freshwater ecosystems. Worldwatch paper 128.
Washington, DC: Worldwatch Institute.</mixed-citation>
         </ref>
         <ref id="d71e243a1310">
            <mixed-citation id="d71e247" publication-type="other">
Alcamo, J., Döll, P., Henrichs, T., Kaspar, F., Lehner, B.,
R6sch, T. &amp; Siebert, S. In press. Global estimates of water
withdrawals and availability under current and future
'business-as-usual' conditions, Hydrol. Sci. J. 48, 339-349.</mixed-citation>
         </ref>
         <ref id="d71e263a1310">
            <mixed-citation id="d71e267" publication-type="other">
Ballester, M. V. R, Victoria, D.de C., Krusche, A. V., Coburn,
R., Victoria, R. L., Richey, J. E., Logsdon, M. G., Mayorga,
E. &amp; Matricardi, E. 2003 A remote sensing/GIS-based
physical template to understand the biogeochemistry of the
Ji-Parana river basin (Western Amazonia). Remote Sens.
Environ. 87, 429-445.</mixed-citation>
         </ref>
         <ref id="d71e290a1310">
            <mixed-citation id="d71e294" publication-type="other">
Baumgartner, A. &amp; Reichel, E. 1975 The world water balance:
mean annual global, continental, and maritime precipitation,
evaporation, and runoff. Amsterdam, The Netherlands:
Elsevier Science Publishers.</mixed-citation>
         </ref>
         <ref id="d71e311a1310">
            <mixed-citation id="d71e315" publication-type="other">
Bilge, F., Yazici, B., Dogeroglu, T. &amp; Ayday, C. 2003
Statistical evaluation of remotely sensed data for water
quality monitoring. Int. J. Remote Sens. 24, 5317-5326.</mixed-citation>
         </ref>
         <ref id="d71e328a1310">
            <mixed-citation id="d71e332" publication-type="other">
BirdLife International 2004 State of the world's birds 2004:
indicators for our changing world. Cambridge, UK: BirdLife
International.</mixed-citation>
         </ref>
         <ref id="d71e345a1310">
            <mixed-citation id="d71e349" publication-type="other">
Butchart, S. H. M., Stattersfield, A. J., Bennun, L. A.,
Akgakaya, H. R., Baillie, J. E. M., Stuart, S. N., Hilton-
Taylor, C. &amp; Mace, G. M. 2005 Using Red List Indices to
measure progress towards the 2010 target and beyond.
Phil. Trans. R. Soc. B 360.</mixed-citation>
         </ref>
         <ref id="d71e368a1310">
            <mixed-citation id="d71e372" publication-type="other">
Campbell, I. C. 2002 Biological monitoring and assessment
using invertebrates. Environmental monitoring handbook
(ed. F. R. Burden, I. McKelvie, U. Föstner &amp; A. Guenther).
pp. 5.0-5.1, New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d71e388a1310">
            <mixed-citation id="d71e392" publication-type="other">
Chessman, B. C. 1995 Rapid assessment of rivers using
macroinvertebrates: a procedure based on habitat specific
sampling, family level identification and a biotic index.
Aust. J. Ecol. 20, 122-129.</mixed-citation>
         </ref>
         <ref id="d71e408a1310">
            <mixed-citation id="d71e412" publication-type="other">
Chutter, F M. 1972 An empirical biotic index of the quality of
water in South African streams and rivers. Water Res. 6, 19-30.</mixed-citation>
         </ref>
         <ref id="d71e423a1310">
            <mixed-citation id="d71e427" publication-type="other">
CIESIN (Center For International Earth Science Infor-
mation Network), International Food Policy Research
Institute and World Resources Institute 2000 Gridded
Population of the World (GPWI), Version 2. Palisades, NY:
CIESIN/Columbia University.</mixed-citation>
         </ref>
         <ref id="d71e446a1310">
            <mixed-citation id="d71e450" publication-type="other">
Costa, M. P. F. 2004 Use of SAR satellites for mapping
zonation of vegetation communities in the Amazon flood-
plain. Int. J. Remote Sens. 25, 1817-1835.</mixed-citation>
         </ref>
         <ref id="d71e463a1310">
            <mixed-citation id="d71e467" publication-type="other">
Dahl, T. E. 2000 Status and trends of wetlands in the
conterminous United States 1986 to 1997. Washington: US
Department of the Interior, Fish and Wildlife Service.</mixed-citation>
         </ref>
         <ref id="d71e480a1310">
            <mixed-citation id="d71e484" publication-type="other">
Dahl, T. E. &amp; Johnson, C. E. 1991 Status and trends of wetlands
in the conterminous United States, Mid-1970s to Mid-1980s.
Washington, DC: US Department of the Interior, Fish
and Wildlife Service.</mixed-citation>
         </ref>
         <ref id="d71e500a1310">
            <mixed-citation id="d71e504" publication-type="other">
Dallas, H. F. &amp; Day, J. A. 1993 The effect ofwater quality variables
on riverine ecosystems: a review. Technical Report Series No.
TT 61/93. South Africa: Water Research Commission.</mixed-citation>
         </ref>
         <ref id="d71e517a1310">
            <mixed-citation id="d71e521" publication-type="other">
Darwall, W. &amp; Revenga, C. In press. Wetlands. In The World's
Protected Areas (ed. M. Spalding, M. Jenkins &amp; S. Chape).
California University Press.</mixed-citation>
         </ref>
         <ref id="d71e535a1310">
            <mixed-citation id="d71e539" publication-type="other">
Darwall, W., Smith. K., Lowe, T. &amp; Vié, J.-C. In press. The
Status and Distribution of Freshwater Biodiversity in Eastern
Africa. Gland, Switzerland and Cambridge, UK: IUCN.</mixed-citation>
         </ref>
         <ref id="d71e552a1310">
            <mixed-citation id="d71e556" publication-type="other">
Davies, P. E. 2000 Development of a national river bioassess-
ment system (AUSRIVAS) in Australia. Assessing the
biological quality offresh waters: RIVPACS and other techniques
(ed. J. F. Wright, D. W Sutcliffe &amp; M. T. Furse). Cumbria,
UK: Freshwater Biological Association.</mixed-citation>
         </ref>
         <ref id="d71e575a1310">
            <mixed-citation id="d71e579" publication-type="other">
Davies, S. P. &amp; Tsomides, L. 1997 Methods for biological
sampling and analysis of Maine's inland waters. DEPLW-1 07-
A97. Augusta, ME: Maine Department of Environmental
Protection.</mixed-citation>
         </ref>
         <ref id="d71e595a1310">
            <mixed-citation id="d71e599" publication-type="other">
Day, J. A., Stewart, B. A., de Moor, F. J. &amp; Louw, A. E. 2000
Guides to the freshwater invertebrates of Southern
Africa. Crustacea I. Water Research Commission, Pre-
toria. Stats and trends of wetlands in the conterminous
United States 1986-1997 (ed. T. E. Dahl). Washington,
DC: US Department of the Interior, Fish and Wildlife
Service.</mixed-citation>
         </ref>
         <ref id="d71e625a1310">
            <mixed-citation id="d71e629" publication-type="other">
Delany, S. N., Reyes, C., Hubert, E., Pihl, S., Rees, E.,
Haanstra, L. &amp; van Strien, A. 1999 Results from the
International Waterbird Census in the western Palearctic and
Southwest Asia, 1995 and 1996. Wetlands International
Publication 54. Wageningen, The Netherlands: Wetlands
International.</mixed-citation>
         </ref>
         <ref id="d71e652a1310">
            <mixed-citation id="d71e656" publication-type="other">
Döll, P., Kaspar, F. &amp; Lehner, B. 2003 A global hydrological
model for deriving water availability indicators: model
tuning and validation. J. Hydrol. 270, 105-134.</mixed-citation>
         </ref>
         <ref id="d71e670a1310">
            <mixed-citation id="d71e674" publication-type="other">
Ellison, A. M. 2004 Wetlands of Central America. Wetlands
Ecol. Mngmt 12, 3-55.</mixed-citation>
         </ref>
         <ref id="d71e684a1310">
            <mixed-citation id="d71e688" publication-type="other">
European Union 2000. The EU Water Framework Directive -
Integrated River Basin Management in Europe. Official
Journal of the European Community (OJL 327).</mixed-citation>
         </ref>
         <ref id="d71e701a1310">
            <mixed-citation id="d71e705" publication-type="other">
Fekete, B.M., Vörösmarty, C.J. &amp; Grabs, W. 2000
UNH/GRDC Composite Runoff Fields, Version 1.0.
Durham, NH: Complex Systems Research Center,
University of New Hampshire; Koblenz, Germany: global
Runoff Data Center (GRDC). See http://www.grdc.sr
unh.edu/.</mixed-citation>
         </ref>
         <ref id="d71e728a1310">
            <mixed-citation id="d71e732" publication-type="other">
Finlayson, C. M. &amp; Davidson, N. C. 1999 Global review of
wetland resources and priorities for wetland inventory:
summary report. Global review of wetland resources and
priorities for wetland inventory (ed. C. M. Finlayson &amp; A. G.
Spiers), 2nd edn. pp. 1-14, Supervising Scientist Report
144. Canberra, Australia: Environmental Research Insti-
tute of the Supervising Scientist.</mixed-citation>
         </ref>
         <ref id="d71e758a1310">
            <mixed-citation id="d71e762" publication-type="other">
Finlayson, C. M. &amp; Spiers, A. G., 1999 Global review of
wetland resources and priorities for wetland inventory, 2nd
edn. Supervising Scientist Report 144. Canberra, Aus-
tralia: Environmental Research Institute of the Super-
vising Scientist.</mixed-citation>
         </ref>
         <ref id="d71e781a1310">
            <mixed-citation id="d71e785" publication-type="other">
Finlayson, C. M., Davidson, N. C. &amp; Stevenson, N. J. (eds)
2001 Wetland inventory, assessment and monitoring: practical
techniques and identification of major issues. Report from
Workshop 4, 2nd International Conference on Wetlands
and Development, 8-14 November, 1998, Dakar, Sene-
gal. Supervising Scientist Report 161, Darwin, Australia:
Supervising Scientist. Wageningen, The Netherlands:
Wetlands International.</mixed-citation>
         </ref>
         <ref id="d71e815a1310">
            <mixed-citation id="d71e819" publication-type="other">
Fraser, A. S., Meybeck, M. &amp; Ongley, E. D. 1995 Global
environment monitoring system (GEMS) water quality of
world river basins. UNEP Environment Library No. 14.
Nairobi: UNEP.</mixed-citation>
         </ref>
         <ref id="d71e835a1310">
            <mixed-citation id="d71e839" publication-type="other">
Glasgow, H. B., Burkholder, J. M., Reed, R. E., Lewitus, A. J.
&amp; Kleinman, J. E. 2004 Real-time remote monitoring of
water quality: a review of current applications, and
advancements in sensor, telemetry, and computing tech-
nologies. J. Exp. Mar. Biol. Ecol. 300, 409-448.</mixed-citation>
         </ref>
         <ref id="d71e858a1310">
            <mixed-citation id="d71e862" publication-type="other">
Gleick, P. H. 1993 Part II: freshwater data. Water in crisis: a
guide to the world's fresh water resources (ed. P. H. Gleick).
New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d71e875a1310">
            <mixed-citation id="d71e879" publication-type="other">
Groombridge, B. &amp; Jenkins, M. 1998 Freshwater biodiversity: a
preliminary global assessment. Cambridge, UK: WCMC-
World Conservation Press.</mixed-citation>
         </ref>
         <ref id="d71e892a1310">
            <mixed-citation id="d71e896" publication-type="other">
Groombridge, B. &amp; Jenkins, M. D. 2000 Global biodiversity:
Earth's living resources in the 21st century. Cambridge, UK:
WCMC-World Conservation Press.</mixed-citation>
         </ref>
         <ref id="d71e909a1310">
            <mixed-citation id="d71e913" publication-type="other">
Harding, J. S., Benfield, E. F., Bolstad, P. V., Helfman, G. S.
&amp; Jones, E. B.D, III 1998 Stream biodiversity: the ghost of
land use past. Proc. Natl Acad. Sci. USA 95, 14 843-
14 847.</mixed-citation>
         </ref>
         <ref id="d71e930a1310">
            <mixed-citation id="d71e934" publication-type="other">
Harrison, I. J. &amp; Stiassny, M. J. 1999 The quiet crisis: a
preliminary listing of the freshwater fishes of the world that
are extinct or 'Missing in Action'. In Extinctions in near
time: causes, contexts, and consequences (ed. R. D. E.
MacPhee), pp. 271-332, New York: Kluwer Academic/-
Plenum Publishers.</mixed-citation>
         </ref>
         <ref id="d71e957a1310">
            <mixed-citation id="d71e961" publication-type="other">
Hering, D., Moog, O., Sandin, L. &amp; Verdonschot, P. F. M.
2004 Overview and application of the AQEM assessment
system. Hydrobiologia 516, 1-20.</mixed-citation>
         </ref>
         <ref id="d71e974a1310">
            <mixed-citation id="d71e978" publication-type="other">
Hess, L. L., Melack, J. M., Novo, E. M. L., Barbosa, C. C. F. &amp;
Gastil, M. 2003 Dual-season mapping of wetland inunda-
tion and vegetation for the central Amazon basin. Remote
Sens. Environ 87, 404-428.</mixed-citation>
         </ref>
         <ref id="d71e994a1310">
            <mixed-citation id="d71e998" publication-type="other">
ILEC (International Lake Environment Committee). 2002
See http://www.ilec.or.jp/database/database.html.</mixed-citation>
         </ref>
         <ref id="d71e1008a1310">
            <mixed-citation id="d71e1012" publication-type="other">
International Wader Study Group. 2003 Waders are declining
worldwide. Conclusions from the 2003 International
Wader study group Conference, Cadiz, Spain. Wader
Stud. Group Bull. 101, 8-12.</mixed-citation>
         </ref>
         <ref id="d71e1028a1310">
            <mixed-citation id="d71e1032" publication-type="other">
IUCN 2003 2003 IUCN Red List of threatened species.
Gland, Switzerland: The IUCN Species Survival
Commission.</mixed-citation>
         </ref>
         <ref id="d71e1046a1310">
            <mixed-citation id="d71e1050" publication-type="other">
IUCN 2004 2004 IUCN Red List of threatened species. IUCN,
Gland, Switzerland.</mixed-citation>
         </ref>
         <ref id="d71e1060a1310">
            <mixed-citation id="d71e1064" publication-type="other">
IUCN Species Survival Commission, Conservation Inter-
national Center for Applied Biodiversity Science and
NatureServe 2004 IUCN Global Amphibian Assessment.
See http://www.globalamphibians.org.</mixed-citation>
         </ref>
         <ref id="d71e1080a1310">
            <mixed-citation id="d71e1084" publication-type="other">
IUCN/SSC Tortoise and Freshwater Turtle Specialist Group
1991 Tortoises and freshwater turtles: an action plan for their
conservation. IUCN/SSC tortoise and freshwater turtle
specialist group. Gland, Switzerland: IUCN.</mixed-citation>
         </ref>
         <ref id="d71e1100a1310">
            <mixed-citation id="d71e1104" publication-type="other">
Jorgensen, S.E., de Bernardi, R. Ballatore, T. &amp; Muhandiki,
V. 2001 Lake Watch 25: changes in the world's lakes. Draft
report. Kusatsu, Japan: International Lake Environment
Committee.</mixed-citation>
         </ref>
         <ref id="d71e1120a1310">
            <mixed-citation id="d71e1124" publication-type="other">
Karr, J. R. 1999 Defining and measuring river health.
Freshwat. Biol. 41, 221-234.</mixed-citation>
         </ref>
         <ref id="d71e1134a1310">
            <mixed-citation id="d71e1138" publication-type="other">
Kasischke, E. S., Smith, K. B., Bourgeau-Chavez, L. L.,
Romanowicz, Edwin A., Brunzell, S. &amp; Richardson, C. J.
2003 Effects of seasonal hydrologic patterns in
south Florida wetlands on radar backscatter measured
from ERS-2 SAR imagery. Remote Sens. Environ. 88, 423-
441.</mixed-citation>
         </ref>
         <ref id="d71e1162a1310">
            <mixed-citation id="d71e1166" publication-type="other">
King, J. M. &amp; Louw, D. 1998 Instream flow assessments
for regulated rivers in South Africa using the building
block methodology. Aquat. Ecosyst. Health Mngmt 1, 109-
124.</mixed-citation>
         </ref>
         <ref id="d71e1182a1310">
            <mixed-citation id="d71e1186" publication-type="other">
King, J. M., Tharme, R.E. &amp; de Villiers, M. 2000
Environmental flow assessments for rivers: manual for
the building block methodology. Technology Transfer
Report TT131/100. Pretoria, South Africa: Water
Research Commission.</mixed-citation>
         </ref>
         <ref id="d71e1205a1310">
            <mixed-citation id="d71e1209" publication-type="other">
King, J. M., Brown, C. A. &amp; Sabet, H. 2003 A scenario-based
holistic approach for environmental flow assessments.
Rivers Res. Appl. 19, 619-639.</mixed-citation>
         </ref>
         <ref id="d71e1222a1310">
            <mixed-citation id="d71e1226" publication-type="other">
Kira, T. 1997 Survey of the state of world lakes. In The world
lakes in crisis: guidelines of lake management, vol. 8 (ed. S. E.
Jorgensen &amp; S. Matsui), pp. 147-155. Kusatsu, Shiga,
Japan: International Lake Environment Committee and
United Nations Environment Programme.</mixed-citation>
         </ref>
         <ref id="d71e1245a1310">
            <mixed-citation id="d71e1249" publication-type="other">
Kolkwitz, R. &amp; Marsson, M. 1908 Ökologie der pflanzlichen
Saprobien. Ber. Deutsch Botan. Ges. 26a, 505-519.</mixed-citation>
         </ref>
         <ref id="d71e1259a1310">
            <mixed-citation id="d71e1263" publication-type="other">
Kolkwitz, R. &amp; Marsson, M. 1909 Ökologie der tierischen
Saprobien. Int. Rev. Hydrobiol. 2, 126-152.</mixed-citation>
         </ref>
         <ref id="d71e1274a1310">
            <mixed-citation id="d71e1278" publication-type="other">
Kurata, A. 1994 Data book of world lake environments: a survey
of the state of world lakes. (Five volumes.) Kusatsu, Japan
and Nairobi, Kenya: International Lake Environment
Committee and United Nations Environment Programme
(UNEP).</mixed-citation>
         </ref>
         <ref id="d71e1297a1310">
            <mixed-citation id="d71e1301" publication-type="other">
Ladson, A. R., White, L. J., Doolan, J. A., Finlayson, B. L.,
Hart, B. T., Lake, P. S. &amp; Tilleard, J. W. 1999 Develop-
ment and testing of an index of stream condition for
waterway management in Australia. Freshw. Biol. 41, 453.</mixed-citation>
         </ref>
         <ref id="d71e1317a1310">
            <mixed-citation id="d71e1321" publication-type="other">
Lehner, B. &amp; Dö11, P. 2004 Development and validation of a
global database of lakes, reservoirs and wetlands.
J. Hydrol. 296, 1-22.</mixed-citation>
         </ref>
         <ref id="d71e1334a1310">
            <mixed-citation id="d71e1338" publication-type="other">
Loh et al. In preparation. The living planet Report 2004.</mixed-citation>
         </ref>
         <ref id="d71e1345a1310">
            <mixed-citation id="d71e1349" publication-type="other">
Lundberg, J. G., Kottelat, M., Smith, G. R., Stiassny, M. L. J.
&amp; Gill, A. C. 2000 So many fishes, so little time: an
overview of recent ichthyological discovery in continental
waters. Ann. Mo. Bot. Gard. 87, 26-62.</mixed-citation>
         </ref>
         <ref id="d71e1365a1310">
            <mixed-citation id="d71e1369" publication-type="other">
Malmqvist, B. &amp; Rundle, S. 2002 Threats to the running water
ecosystems of the world. Environ. Conserv. 29, 134-153.</mixed-citation>
         </ref>
         <ref id="d71e1380a1310">
            <mixed-citation id="d71e1384" publication-type="other">
Master, L. L., Flack, S. R. &amp; Stein, B. A. 1998 Rivers of life:
critical watersheds for protecting freshwater biodiversity.
Arlington, VA, USA: The Nature Conservancy.</mixed-citation>
         </ref>
         <ref id="d71e1397a1310">
            <mixed-citation id="d71e1401" publication-type="other">
Matthews, E. &amp; Fung, I. 1987 Methane emissions from natural
wetlands: global distribution, area, and environmental
characteristics of sources. Global Biogeochem. Cycles 1, 61-
86.</mixed-citation>
         </ref>
         <ref id="d71e1417a1310">
            <mixed-citation id="d71e1421" publication-type="other">
McAllister, D. E., Hamilton, A. L. &amp; Harvey, B. 1997 Global
freshwater biodiversity: striving for the integrity of
freshwater ecosystems. Sea Wind Bull. Ocean Voice Int.
11, 1-140.</mixed-citation>
         </ref>
         <ref id="d71e1437a1310">
            <mixed-citation id="d71e1441" publication-type="other">
Miller, R. R., Williams, J. D. &amp; Williams, J. E. 1989
Extinctions of North American fishes during the past
century. Fisheries 14, 22-38.</mixed-citation>
         </ref>
         <ref id="d71e1454a1310">
            <mixed-citation id="d71e1458" publication-type="other">
MRC 2003 State of the Basin report. Phnom Penh, Mekong:
River Commission.</mixed-citation>
         </ref>
         <ref id="d71e1468a1310">
            <mixed-citation id="d71e1472" publication-type="other">
Myers, N. 1997 The rich diversity of biodiversity issues. In
Biodiversity II: understanding and protecting our biological
resources (ed. M. L. Reaka-Kudla, D. E. Wilson &amp; E. O.
Wilson), pp. 125-138, Washington, DC: Joseph Henry
Press.</mixed-citation>
         </ref>
         <ref id="d71e1492a1310">
            <mixed-citation id="d71e1496" publication-type="other">
National Water Council 1981 River quality: the 1980 survey
and future outlook. London: NWC.</mixed-citation>
         </ref>
         <ref id="d71e1506a1310">
            <mixed-citation id="d71e1510" publication-type="other">
Platts, W. S. et al. 1987 Methods for evaluating riparian habitats
with applications to management. US Department of
Agriculture, Forest Service, General Technical Report
INT-221. Ogden, UT: USDA.</mixed-citation>
         </ref>
         <ref id="d71e1526a1310">
            <mixed-citation id="d71e1530" publication-type="other">
Reaka-Kudla, M. L. 1997 The global biodiversity of coral
reefs: a comparison with rain forests. In Biodiversity II:
understanding and protecting our biological resources (ed.
M. L. Reaka-Kudla, D. E. Wilson &amp; E. O. Wilson),
pp. 83-108, Washington, DC: Joseph Henry Press.</mixed-citation>
         </ref>
         <ref id="d71e1549a1310">
            <mixed-citation id="d71e1553" publication-type="other">
Red List Consortium. 2004 Red List Indices. Gland, Switzer-
land and Cambridge, UK: IUCN and SSC, BirdLife
International, CI and CABS and NatureServe.</mixed-citation>
         </ref>
         <ref id="d71e1566a1310">
            <mixed-citation id="d71e1570" publication-type="other">
Revenga, C. &amp; Kura, Y. 2003 Status and trends ofbiodiversity of
inland water ecosystems. Technical Series no. 11. Montreal,
Canada: Secretariat of the Convention on Biological
Diversity.</mixed-citation>
         </ref>
         <ref id="d71e1586a1310">
            <mixed-citation id="d71e1590" publication-type="other">
Revenga, C., Brunner, J., Henninger, N., Kassem, K. &amp;
Payne, R. 2000 Pilot analysis of global ecosystems: freshwater
systems. Washington, DC: World Resources Institute.</mixed-citation>
         </ref>
         <ref id="d71e1604a1310">
            <mixed-citation id="d71e1608" publication-type="other">
Ricciardi, A. &amp; Rasmussen, J. B. 1999 Extinction rates of
North American freshwater fauna. Conserv. Biol. 13,
1220-1222.</mixed-citation>
         </ref>
         <ref id="d71e1621a1310">
            <mixed-citation id="d71e1625" publication-type="other">
RHP 2003 State-of-rivers report: free state province region river
systems. Pretoria, South Africa: Department of Water
Affairs and Forestry.</mixed-citation>
         </ref>
         <ref id="d71e1638a1310">
            <mixed-citation id="d71e1642" publication-type="other">
Rolauffs, P., Stubauer, I., Zahrádkhová, S., Brabec, K. &amp;
Moog, O. 2004 Integration of the saprobic system into the
European Water Framework Directive. Hydrobiologia 516,
285-298.</mixed-citation>
         </ref>
         <ref id="d71e1658a1310">
            <mixed-citation id="d71e1662" publication-type="other">
Rosenberg, D. M. &amp; Resh, V. H. 1996 Use of aquatic insects
in biomonitoring. In An introduction to the aquatic insects of
North America (ed. R. W. Merritt &amp; K. W. Cummins), 3rd
edn, pp. 87-97, Dubuque, IA, USA: Kendall/Hunt
Publishing Company.</mixed-citation>
         </ref>
         <ref id="d71e1681a1310">
            <mixed-citation id="d71e1685" publication-type="other">
Sawaya, K. E., Olmanson, L. G., Heinert, N. J., Brezonik,
P. L. &amp; Bauer, M. E. 2003 Extending satellite remote
sensing to local scales: land and water resource monitoring
using high-resolution imagery. Remote Sens. Environ. 88,
144-156.</mixed-citation>
         </ref>
         <ref id="d71e1704a1310">
            <mixed-citation id="d71e1708" publication-type="other">
Scott, D. A. &amp; Rose, P. M. 1996 Atlas of Anatidae populations
in Africa and western Eurasia. Wetlands International
Publication No. 41. Wageningen, The Netherlands: Wet-
lands International.</mixed-citation>
         </ref>
         <ref id="d71e1725a1310">
            <mixed-citation id="d71e1729" publication-type="other">
Shiklomanov, I. A. 1997 Comprehensive assessment of the
freshwater resources of the world: assessment of water resource
and water availability in the world. Stockholm, Sweden:
World Meteorological Organization and Stockholm
Environment Institute.</mixed-citation>
         </ref>
         <ref id="d71e1748a1310">
            <mixed-citation id="d71e1752" publication-type="other">
Shuman, C. S. &amp; Ambrose, R. F. 2003 A comparison of
remote sensing and ground-based methods for monitoring
wetland restoration success. Restor. Ecol. 11, 325-333.</mixed-citation>
         </ref>
         <ref id="d71e1765a1310">
            <mixed-citation id="d71e1769" publication-type="other">
Sladecek, V. 1973 System of water quality from the biological
point of view. Arch. Hydrobiol. Beih. Ergebnisse Limnol. 7,
1-218.</mixed-citation>
         </ref>
         <ref id="d71e1782a1310">
            <mixed-citation id="d71e1786" publication-type="other">
Smakhtin, V., Revenga, C. &amp; Döll, P. 2004 Taking into
account environmental water requirements in global-scale
water resources assessments. Comprehensive assessment
of water management in Agriculture. Research Report No.
2. Colombo, Sri Lanka: International Water Management
Institute.</mixed-citation>
         </ref>
         <ref id="d71e1809a1310">
            <mixed-citation id="d71e1813" publication-type="other">
Spiers, A. G. 1999 Global review of wetland resources and
priorities for wetland inventory. In Global review of wetland
resources and priorities for wetland inventory, Supervising
Scientist Report 144 (ed. C.M. Finlayson &amp; A.G.Spiers),
pp. 63-104. Canberra, Australia: Environmental Research
Institute of the Supervising Scientist.</mixed-citation>
         </ref>
         <ref id="d71e1836a1310">
            <mixed-citation id="d71e1840" publication-type="other">
The Heinz Center 2002 Global review of wetland resources and
priorities for wetland inventory, Supervising Scientist Report
144, pp. 63-104. Canberra, Australia: Environmental
Research Institute of the Supervising Scientist.</mixed-citation>
         </ref>
         <ref id="d71e1857a1310">
            <mixed-citation id="d71e1861" publication-type="other">
UNEP/DEWA 2001. The Mesopotamian marshlands:
demise of an ecosystem. Early warning and assessment.
(46 pages.) Technical Report No. TR.01-3. Nairobi,
Kenya: UNEP.</mixed-citation>
         </ref>
         <ref id="d71e1877a1310">
            <mixed-citation id="d71e1881" publication-type="other">
van Dijk, P. P., Stuart, B. L. &amp; Rhodin, A. G. J. 2000 Asian
turtle trade: proceedings of a workshop on conservation
and trade of freshwater turtles and tortoises in Asia. In
Chelonian research monographs No. 2. Lunenburg, MA:
Chelonian Research Foundation in association with WCS,
TRAFFIC, WWF, Kadoorie Farm and Botanic Gardens
and the US Fish and Wildlife Service.</mixed-citation>
         </ref>
         <ref id="d71e1907a1310">
            <mixed-citation id="d71e1911" publication-type="other">
Verma, R., Singh, S. P. &amp; Raj, K. G. 2003 Assessment of
changes in water-hyacinth coverage of water bodies in
northern part of Bangalore city using temporal remote
sensing data. Curr. Sci. 84, 795-804.</mixed-citation>
         </ref>
         <ref id="d71e1927a1310">
            <mixed-citation id="d71e1931" publication-type="other">
Vis, C., Hudon, C. &amp; Carignan, R. 2003 An evaluation of
approaches used to determine the distribution and
biomass of emergent and submerged aquatic macrophytes
over large spatial scales. Aquat. Bot. 77, 187-201.</mixed-citation>
         </ref>
         <ref id="d71e1947a1310">
            <mixed-citation id="d71e1951" publication-type="other">
Wetlands International 2002 Waterbird population estimates.
Wetlands International Global Series No. 12, 3rd edn.
Wageningen, The Netherlands: Wetlands International.</mixed-citation>
         </ref>
         <ref id="d71e1964a1310">
            <mixed-citation id="d71e1968" publication-type="other">
Wetlands International 2004 Global peatlands initiative
Wetlands International, Wageningen, The Netherlands.</mixed-citation>
         </ref>
         <ref id="d71e1979a1310">
            <mixed-citation id="d71e1983" publication-type="other">
Whited, D., Stanford, J. A. &amp; Kimball, J. S. 2002 Application
of airborne multispectral digital imagery to quantify
riverine habitats at different base flows. River Res. Appl.
18, 583-594.</mixed-citation>
         </ref>
         <ref id="d71e1999a1310">
            <mixed-citation id="d71e2003" publication-type="other">
Williams, D. J., Rybicki, N. B., Lombana, A. V., O'Brien,
T. M. &amp; Gomez, R. B. 2003 Preliminary investigation of
submerged aquatic vegetation mapping using hyperspec-
tral remote sensing. Environ. Monit. Assess. 81, 383-392.</mixed-citation>
         </ref>
         <ref id="d71e2019a1310">
            <mixed-citation id="d71e2023" publication-type="other">
WRI 1995 Africa data sampler user's guide; a georeferenced data
base for all African countries. Washington, DC: WRI.</mixed-citation>
         </ref>
         <ref id="d71e2033a1310">
            <mixed-citation id="d71e2037" publication-type="other">
WRI in collaboration with United Nations Development
Programme, United Nations Environment Programme
and the World Bank 2000 World Resources 2000-2001:
people and ecosystems: the fraying web of life. Washington,
DC: World Resources Institute.</mixed-citation>
         </ref>
         <ref id="d71e2056a1310">
            <mixed-citation id="d71e2060" publication-type="other">
Wright, J. F. 2000 An introduction to RIVPACS. Assessing the
biological quality of fresh waters: RIVPACS and other
techniques (ed. J. F. Wright, D. W. Sutcliffe &amp; M. T.
Furse), pp. 1-24, Cumbria, UK: Freshwater Biological
Association.</mixed-citation>
         </ref>
         <ref id="d71e2079a1310">
            <mixed-citation id="d71e2083" publication-type="other">
Yu, X. &amp; Jiang, N. 2003 Analyzing lake area change in Ebinur
by integration of RS and GIS techniques. Hupo Kexue 15,
81-84.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
