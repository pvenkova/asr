<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">imfstaffpapers</journal-id>
         <journal-id journal-id-type="jstor">j100845</journal-id>
         <journal-title-group>
            <journal-title>IMF Staff Papers</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Monetary Fund</publisher-name>
         </publisher>
         <issn pub-type="ppub">10207635</issn>
         <issn pub-type="epub">15645150</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30035873</article-id>
         <title-group>
            <article-title>The International Monetary Fund and the Global Spread of Privatization</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Nancy</given-names>
                  <surname>Brune</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Geoffrey</given-names>
                  <surname>Garrett</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Bruce</given-names>
                  <surname>Kogut</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i30035871</issue-id>
         <fpage>195</fpage>
         <lpage>219</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 International Monetary Fund</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30035873"/>
         <abstract>
            <p>Well over a trillion dollars worth of state-owned firms have been privatized since 1980. The traditional argument is that governments choose to privatize in response to fiscal pressures. In this study, the authors examine the impact oflFI conditionality on privatization andfind that IMF conditionality, in particular, has an important indirect economic benefit. Investors are willing to pay more for privatized assets in countries that owe the IMF money (and hence that are subject to the policy constraints attached to the loans). The reason for this is that investors view IMF conditionality as a signal of credible policy reform. The magnitude of this effect is striking. For every dollar a developing country owed the IMF in the early 1980s, it subsequently privatized state-owned assets worth roughly 50c. Admittedly, this "credibility bonus" of IMF lending may not justify the policy conditions typically imposed by the IME However, the additional capital drawn into developing countries as a result of the IMF-privatization nexus is no doubt helpful to these economies.</p>
         </abstract>
         <kwd-group>
            <kwd>L3</kwd>
            <kwd>F33</kwd>
            <kwd>F34</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d204e251a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d204e258" publication-type="other">
The Progress of Policy Reform in Latin America (1990).</mixed-citation>
            </p>
         </fn>
         <fn id="d204e265a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d204e272" publication-type="other">
http://www.privatizationlink.org.</mixed-citation>
            </p>
         </fn>
         <fn id="d204e279a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d204e286" publication-type="other">
Central Bank of Nigeria: http://www.cenbank.org./extern_debt/htm.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d204e302a1310">
            <mixed-citation id="d204e306" publication-type="other">
Abouharb, Rodwan, 2001, "World Bank Structural Adjustment Loans and Their Impact on
Economic Growth" (unpublished; Binghamton, New York: State University of New York).</mixed-citation>
         </ref>
         <ref id="d204e316a1310">
            <mixed-citation id="d204e320" publication-type="other">
Bevan, Alan, Saul Estrin, Boris Kuznetsov, Mark Schaffer, Manuela Angelucci, Julian Fennema,
and Giovanni Mangiarotti, 2001, "The Determinants of Privatized Enterprise Performance
in Russia," William Davidson Institute Working Paper Series No. 452 (Ann Arbor: University
of Michigan).</mixed-citation>
         </ref>
         <ref id="d204e336a1310">
            <mixed-citation id="d204e340" publication-type="other">
Black, Bernard, Reinier Kraakman, and Anna Tarassova, 2000, "Russian Privatization and
Corporate Governance: What Went Wrong?" Stanford Law Review, Vol. 52, pp. 1731-1808.</mixed-citation>
         </ref>
         <ref id="d204e350a1310">
            <mixed-citation id="d204e354" publication-type="other">
Bird, Graham, and Dane Rowlands, 1997, "The Catalytic Effect of Lending by the International
Financial Institutions," The World Economy, Vol. 20 (November), pp. 967-91.</mixed-citation>
         </ref>
         <ref id="d204e365a1310">
            <mixed-citation id="d204e369" publication-type="other">
---, forthcoming, "Do IMF Programmes Have a Catalytic Effect on Other International
Capital Flows?" Oxford Development Studies (Oxford, U.K.: Oxford University).</mixed-citation>
         </ref>
         <ref id="d204e379a1310">
            <mixed-citation id="d204e383" publication-type="other">
Bollen, Kenneth A., and Robert W. Jackman, 1985, "Regression Diagnostics: An Expository
Treatment of Outliers and Influential Cases," Sociological Methods and Research, Vol. 13,
No. 4, pp. 510-42.</mixed-citation>
         </ref>
         <ref id="d204e396a1310">
            <mixed-citation id="d204e400" publication-type="other">
Bortolotti, Bernardo, Marcella Fantini, and Domenico Siniscalco, 2001, "Privatization:
Politics, Institutions, and Financial Markets," Emerging Market Review, Netherlands, Vol. 2
(June), pp. 109-36.</mixed-citation>
         </ref>
         <ref id="d204e413a1310">
            <mixed-citation id="d204e417" publication-type="other">
---, 1998, "Privatizations and Institutions: A Cross-Country Analysis" (unpublished; Milan:
FEEM).</mixed-citation>
         </ref>
         <ref id="d204e427a1310">
            <mixed-citation id="d204e431" publication-type="other">
Brune, Nancy, 2004, "Privatization Around the World" (Ph.D. dissertation; New Haven,
Connecticut: Yale University).</mixed-citation>
         </ref>
         <ref id="d204e441a1310">
            <mixed-citation id="d204e445" publication-type="other">
Davis, Jeffrey, Rolando Ossowski, Thomas Richardson, and Steven Barnett, 2000, Fiscal and
Macroeconomic Impact of Privatization, Occasional Paper No. 194 (Washington: Inter-
national Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d204e459a1310">
            <mixed-citation id="d204e463" publication-type="other">
Dewenter, Kathryn, and Paul H. Malatesta, 2001, "State-Owned and Privately Owned Firms:
An Empirical Analysis of Profitability, Leverage, and Labor Intensity," American
Economic Review Vol. 91, No. 1, pp. 320-34.</mixed-citation>
         </ref>
         <ref id="d204e476a1310">
            <mixed-citation id="d204e480" publication-type="other">
Easterly, William, and Hairong Yu, 1999, Global Development Network Growth Database
(Washington: World Bank).</mixed-citation>
         </ref>
         <ref id="d204e490a1310">
            <mixed-citation id="d204e494" publication-type="other">
Galal, Ahmed, Leroy Jones, Pankay Tandon, and Ongo Vogelsang, 1994, Welfare Consequences
of Selling Public Enterprises: An Empirical Analysis (New York: Oxford University Press).</mixed-citation>
         </ref>
         <ref id="d204e504a1310">
            <mixed-citation id="d204e508" publication-type="other">
Galal, Ahmed, and Mary Shirley, 1994, Does Privatization Deliver? Highlights from a World
Bank Conference (Washington: World Bank).</mixed-citation>
         </ref>
         <ref id="d204e518a1310">
            <mixed-citation id="d204e522" publication-type="other">
Gwartney, James, Robert Lawson, and Walter Block, 1996, Economic Freedom of the World:
1975-1995 (Toronto: Fraser Institute).</mixed-citation>
         </ref>
         <ref id="d204e532a1310">
            <mixed-citation id="d204e536" publication-type="other">
Holmström, Bengt, and Jean Tirole, 1993, "Market Liquidity and Performance Monitoring,"
Journal of Political Economy, Vol. 101, No. 4, pp. 678-709.</mixed-citation>
         </ref>
         <ref id="d204e547a1310">
            <mixed-citation id="d204e551" publication-type="other">
Knack, Stephen, and Philip Keefer, 1995, "Institutions and Economic Performance: Cross-
Country Tests Using Alternative Institutional Measures," Economics and Politics, Vol. 7,
No. 3, pp. 207-27.</mixed-citation>
         </ref>
         <ref id="d204e564a1310">
            <mixed-citation id="d204e568" publication-type="other">
La Porta, Rafael, Florencio Lopez-de-Silanes, Andrei Shleifer, and Robert Vishny, 1999, "The
Quality of Government," Journal of Law, Economics, and Organization, Vol. 15, No. 1,
pp. 222-79.</mixed-citation>
         </ref>
         <ref id="d204e581a1310">
            <mixed-citation id="d204e585" publication-type="other">
Levine, Ross, 1997, "Financial Development and Economic Growth: Views and Agenda,"
Journal of Economic Literature, Vol. 35 (June), pp. 688-726.</mixed-citation>
         </ref>
         <ref id="d204e595a1310">
            <mixed-citation id="d204e599" publication-type="other">
Megginson, William, Robert Nash, and Matthias van Randenborgh, 1994, "The Financial and
Operating Performance of Newly Privatized Firms: An International Empirical Analysis,"
Journal of Finance, Vol. 49 (June), pp. 403-52.</mixed-citation>
         </ref>
         <ref id="d204e612a1310">
            <mixed-citation id="d204e616" publication-type="other">
Megginson, William, and Jeffrey Netter, 2001, "From State to Market: A Survey of Empirical
Studies on Privatization," Journal of Economic Literature, Vol. 39 (June) pp. 321-89.</mixed-citation>
         </ref>
         <ref id="d204e626a1310">
            <mixed-citation id="d204e630" publication-type="other">
Mody, Ashoka, and Diego Saravia, 2003, "Catalyzing Capital Flows: Do IMF Programs Work
as Commitment Devices?" IMF Working Paper 03/100 (Washington: International
Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d204e644a1310">
            <mixed-citation id="d204e648" publication-type="other">
Perotti, Enrico, and Pieter van Oijen, 2001, "Privatization, Political Risk, and Stock Market
Development in Emerging Economies," Journal of International Money and Finance, Vol.
20, No. 1, pp. 43-69.</mixed-citation>
         </ref>
         <ref id="d204e661a1310">
            <mixed-citation id="d204e665" publication-type="other">
Polak, Jacques, 1994, "The World Bank and the IMF: A Changing Relationship," The World
Bank. Its First Half Century. Volume 2: Perspectives, ed. by Devish Kapur, John P. Lewis,
and Richard Webb (Washington: Brookings Institution Press).</mixed-citation>
         </ref>
         <ref id="d204e678a1310">
            <mixed-citation id="d204e682" publication-type="other">
Przeworski, Adam, Michael E. Alvarez, Jose Antonio Cheibub, Fernando Limongi, 2000,
Democracy and Development: Political Institutions and Well-Being in the World,
1950-1990 (Cambridge: Cambridge University Press).</mixed-citation>
         </ref>
         <ref id="d204e695a1310">
            <mixed-citation id="d204e699" publication-type="other">
Przeworski, Adam, and James Vreeland, 2000, "The Effect of IMF Programs on Economic
Growth," Journal of Development Economics, Vol. 62 (August), pp. 385-421.</mixed-citation>
         </ref>
         <ref id="d204e709a1310">
            <mixed-citation id="d204e713" publication-type="other">
Ramamurti, Ravi, 1996, Privatizing Monopolies: Lessons from the Telecommunications and
Transport Sectors in Latin America (Baltimore: Johns Hopkins University Press).</mixed-citation>
         </ref>
         <ref id="d204e723a1310">
            <mixed-citation id="d204e727" publication-type="other">
---, 1997, "Testing the Limits of Privatization: Argentine Railroads," World Development,
Vol. 25 (December), 1793-93.</mixed-citation>
         </ref>
         <ref id="d204e738a1310">
            <mixed-citation id="d204e742" publication-type="other">
Vickers, John, and George Yarrow, 1988, Privatization: An Economic Analysis (Cambridge,
Massachusetts: MIT Press).</mixed-citation>
         </ref>
         <ref id="d204e752a1310">
            <mixed-citation id="d204e756" publication-type="other">
Vreeland, James Raymond, 2003, The IMF and Economic Development (New York:
Cambridge University Press).</mixed-citation>
         </ref>
         <ref id="d204e766a1310">
            <mixed-citation id="d204e770" publication-type="other">
Williamson, John, 1993, "Democracy and the 'Washington Consensus'," World Development,
Vol. 21 (August), pp. 1329-36.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
