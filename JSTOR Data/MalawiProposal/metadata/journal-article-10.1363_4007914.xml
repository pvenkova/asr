<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">intsexrephea</journal-id>
         <journal-title-group>
            <journal-title content-type="full">International Perspectives on Sexual and Reproductive Health</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Guttmacher Institute</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19440391</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19440405</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">intsexrephea.40.2.79</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.1363/4007914</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>ARTICLES</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Knowledge and Use of Emergency Contraception: A Multicountry Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Palermo</surname>
                  <given-names>Tia</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Bleck</surname>
                  <given-names>Jennifer</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Westley</surname>
                  <given-names>Elizabeth</given-names>
               </string-name>
            </contrib>
            <bio>
               <p>Tia Palermo is assistant professor, Program in Public Health, Department of Preventive Medicine, Stony Brook University, State University of New York. Jennifer Bleck is doctoral candidate, Department of Community and Family Health, College of Public Health, University of South Florida. Elizabeth Westley is coordinator, International Consortium for Emergency Contraception, Family Care International, New York.</p>
               <p>
                  <bold>Author contact:</bold>
                  <email xmlns:xlink="http://www.w3.org/1999/xlink"
                         xlink:href="mailto:tia.palermo@stonybrook.edu">tia.palermo@stonybrook.edu</email>
               </p>
            </bio>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2014</year>
            <string-date>June 2014</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">intsexrephea.40.2.issue-2</issue-id>
         <fpage>79</fpage>
         <lpage>86</lpage>
         <permissions>
            <copyright-statement>© 2014 Guttmacher Institute</copyright-statement>
            <copyright-year>2014</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/10.1363/4007914"/>
         <abstract>
            <p>
               <bold>CONTEXT:</bold>Globally, evidence on knowledge and use of emergency contraception from population-based data is limited, though such information would be helpful in increasing access to the method. We examined knowledge and use of emergency contraception in 45 countries using population-based survey data.</p>
            <p>
               <bold>METHODS:</bold>Demographic and Health Survey (DHS) data on women aged 15–49 were analyzed by country in logistic regressions to identify associations between women's characteristics and their having heard of emergency contraception or having ever used it. Trends were examined, by region and globally, according to individual, household and community descriptors, including women's age, education, marital status, socioeconomic status, and urban or rural location.</p>
            <p>
               <bold>RESULTS:</bold>The proportion of women who had heard of emergency contraception ranged from 2% in Chad to 66% in Colombia, and the proportion of sexually experienced women who had used it ranged from less than 0.1% in Chad to 12% in Colombia. The odds of having heard of or used the method generally increased with wealth, and although the relationship between marital status and knowing of the method varied by region, never-married women were more likely than married women to have used emergency contraception in countries where significant differences existed. In some countries, urban residence was associated with having heard of the method, but in only three countries were women from urban areas more likely to have used it.</p>
            <p>
               <bold>CONCLUSIONS:</bold>Our findings support the need for broader dissemination of information on emergency contraception, particularly among low-income individuals. Variations in use and knowledge within regions suggest a need for programs to be tailored to country-level characteristics.</p>
         </abstract>
         <trans-abstract xml:lang="es">
            <p>
               <bold>RESUMEN</bold>
            </p>
            <p>
               <bold>Contexto:</bold>A nivel mundial, la evidencia acerca de los conocimientos y el uso de anticoncepción de emergencia a partir de datos poblacionales es limitada, a pesar de que dicha información sería útil para aumentar el acceso al método. Examinamos datos sobre los conocimientos y el uso de anticoncepción de emergencia en 45 países, usando como base encuestas de población.</p>
            <p>
               <bold>Métodos:</bold>Mediante regresiones logísticas, se analizaron datos por país obtenidos de Encuestas Demográficas y de Salud (EDS), referentes a mujeres en edades entre 15 y 49 años con el fin de identificar las asociaciones entre las características de las mujeres y el hecho de que hubieran escuchado acerca de la anticoncepción de emergencia o que la hubieran usado alguna vez. Se examinaron tendencias, con un enfoque regional y mundial, basándose en descriptores a nivel de individuos, hogares y comunidades, incluyendo la edad, la educación, el estado conyugal, la condición socioeconómica, así como la ubicación rural o urbana de las mujeres.</p>
            <p>
               <bold>Resultados:</bold>La proporción de mujeres que habían escuchado acerca de la anticoncepción de emergencia varió entre un 2% en Chad a un 66% en Colombia, y la proporción de mujeres con experiencia sexual que la habían usado varió de un 0.04% en Chad a un 12% en Colombia. La probabilidad de haber escuchado acerca del método o de haberlo usado generalmente aumentó con el nivel de riqueza; aunque la relación entre el estado conyugal y el conocimiento sobre el método varió por región, las mujeres que nunca habían estado casadas tuvieron más probabilidades que las mujeres casadas de haber usado anticoncepción de emergencia en países donde existieron diferencias significativas. En algunos países, residir en una zona urbana se asoció con el hecho de haber escuchado sobre el método, pero solamente en tres países las mujeres que residían en zonas urbanas tuvieron mayor probabilidad de haberlo usado.</p>
            <p>
               <bold>Conclusiones:</bold>Nuestros hallazgos apoyan la necesidad de una mayor difusión de la información sobre la anticoncepción de emergencia, dirigida particularmente a personas de más bajos ingresos. Las variaciones en uso y conocimientos dentro de las regiones sugieren la necesidad de que los programas se diseñen de acuerdo con las características de cada país; y las investigaciones futuras deberían aportar estudios de caso de países con los más altos niveles de aceptación del método, para destacar las mejores prácticas.</p>
         </trans-abstract>
         <trans-abstract xml:lang="fr">
            <p>
               <bold>RÉSUMÉ</bold>
            </p>
            <p>
               <bold>Contexte:</bold>À l'échelle mondiale, l'évidence obtenue de données de population concernant la connaissance et l'usage de la contraception d'urgence est limitée. Cette information serait pourtant utile à l'amélioration de l'accès à la méthode. Nous avons examiné ces facteurs dans 45 pays sur la base de données d'enquêtes en population.</p>
            <p>
               <bold>Méthodes:</bold>Les données d'EDS (Enquête démographique et de santé) relatives aux femmes âgées de 15 à 49 ans ont été analysées par pays, par régression logistique, afin d'identifier les associations entre les caractéristiques des femmes et leur sensibilisation ou recours éventuel à la contraception d'urgence. Les tendances ont été examinées, par région et à l'échelle mondiale, en fonction de descripteurs individuels, de ménage et de communauté, notamment l'âge, le niveau d'éducation, l'état matrimonial, le statut socioéconomique et le lieu de résidence urbain ou rural des femmes.</p>
            <p>
               <bold>Résultats:</bold>La proportion des femmes qui avaient entendu parler de la contraception d'urgence varie entre 2% au Tchad et 66% en Colombie et celle des femmes sexuellement expérimentées qui y avaient eu recours, entre 0,04% au Tchad et 12% en Colombie. La probabilité d'avoir entendu parler de la méthode ou d'y avoir eu recours augmente généralement avec la richesse et, bien que le rapport entre l'état matrimonial et la sensibilisation à la méthode varie d'une région à l'autre, les femmes qui n'avaient jamais été mariées sont plus susceptibles que celles mariées d'avoir utilisé la contraception d'urgence dans les pays présentant une différence significative. Dans certains pays, le lieu de résidence urbain est associé à la sensibilisation à la méthode, mais les femmes urbaines ne sont plus susceptibles d'y avoir eu recours que dans trois pays.</p>
            <p>
               <bold>Conclusions:</bold>Nos observations s'inscrivent au soutien d'une plus large dissémination de l'information sur la contraception d'urgence, en particulier auprès des personnes à moindres revenus. Les variations du recours et de la connaissance au sein des régions laissent entendre la nécessité d'adaptation des programmes aux caractéristiques de chaque pays.</p>
         </trans-abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Coeytaux F</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Pillsbury B</string-name>
               </person-group>,<article-title>Bringing emergency contraception to American women: the history and remaining challenges</article-title>,<source>
                  <italic>Women's Health Issues</italic>
               </source>,<year>2001</year>,<volume>11</volume>(<issue>2</issue>):<fpage>80</fpage>–86.</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>L'Engle KL</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Hinson L</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Chin-Quee D</string-name>
               </person-group>,<article-title>“I love my ECPs:” challenges to bridging emergency contraceptive users to more effective contraceptive methods in Ghana</article-title>,<source>
                  <italic>Journal of Family Planning and Reproductive Health Care</italic>
               </source>,<year>2011</year>,<volume>37</volume>(<issue>3</issue>):<fpage>146</fpage>–<lpage>151</lpage>.</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Chin-Quee DS</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Cuthbertson C</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Janowitz B</string-name>
               </person-group>,<article-title>Over-the-counter pill provision: evidence from Jamaica</article-title>,<source>
                  <italic>Studies in Family Planning</italic>
               </source>,<year>2006</year>,<volume>37</volume>(<issue>2</issue>):<fpage>99</fpage>–<lpage>110</lpage>.</mixed-citation>
         </ref>
         <ref id="ref4">
            <label>4.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Keesbury J</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Morgan G</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Owino B</string-name>
               </person-group>,<article-title>Is repeat use of emergency contraception common among pharmacy clients? Evidence from Kenya</article-title>,<source>
                  <italic>Contraception</italic>
               </source>,<year>2011</year>,<volume>83</volume>(<issue>4</issue>):<fpage>346</fpage>–<lpage>351</lpage>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <label>5.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Smit J</string-name>
               </person-group>et al.,<article-title>Emergency contraception in South Africa: knowledge, attitudes, and use among public sector primary healthcare clients</article-title>,<source>
                  <italic>Contraception</italic>
               </source>,<year>2001</year>,<volume>64</volume>(<issue>6</issue>):<fpage>333</fpage>–<lpage>337</lpage>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <label>6.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Myer L</string-name>
               </person-group>et al.,<article-title>Knowledge and use of emergency contraception among women in the Western Cape province of South Africa: a crosssectional study</article-title>,<source>
                  <italic>BMC Women's Health</italic>
               </source>,<year>2007</year>,<volume>7</volume>(<issue>1</issue>):<fpage>14</fpage>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <label>7.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Langer A</string-name>
               </person-group>et al.,<article-title>Emergency contraception in Mexico City: what do health care providers and potential users know and think about it?</article-title>
               <source>
                  <italic>Contraception</italic>
               </source>,<year>1999</year>,<volume>60</volume>(<issue>4</issue>):<fpage>233</fpage>–<lpage>241</lpage>.</mixed-citation>
         </ref>
         <ref id="ref8">
            <label>8.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>García SG</string-name>
               </person-group>et al.,<article-title>Emergency contraception in Honduras: knowledge, attitudes, and practice among urban family planning clients</article-title>,<source>
                  <italic>Studies in Family Planning</italic>
               </source>,<year>2006</year>,<volume>37</volume>(<issue>3</issue>):<fpage>187</fpage>–<lpage>196</lpage>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <label>9.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Shaaban OM</string-name>
               </person-group>et al.,<article-title>Emergency contraception in the context of marriage in Upper Egypt</article-title>,<source>
                  <italic>International Journal of Gynaecology &amp; Obstetrics</italic>
               </source>,<year>2011</year>,<volume>112</volume>(<issue>3</issue>):<fpage>195</fpage>–<lpage>199</lpage>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <label>10.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Meng CX</string-name>
               </person-group>et al.,<article-title>Emergency contraceptive use among 5,677 women seeking abortion in Shanghai, China</article-title>,<source>
                  <italic>Human Reproduction</italic>
               </source>,<year>2009</year>,<volume>24</volume>(<issue>7</issue>):<fpage>1612</fpage>–<lpage>1618</lpage>.</mixed-citation>
         </ref>
         <ref id="ref11">
            <label>11.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Obi SN</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Ozumba BC</string-name>
               </person-group>,<article-title>Emergency contraceptive knowledge and practice among unmarried women in Enugu, southeast Nigeria</article-title>,<source>
                  <italic>Nigerian Journal of Clinical Practice</italic>
               </source>,<year>2008</year>,<volume>11</volume>(<issue>4</issue>):<fpage>296</fpage>–<lpage>299</lpage>.</mixed-citation>
         </ref>
         <ref id="ref12">
            <label>12.</label>
            <mixed-citation publication-type="confproc">
               <person-group person-group-type="author">
                  <string-name>Chin-Quee DS</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>L'Engle KL</string-name>
               </person-group>,<article-title>Frequency and context of EC use in urban Kenya and Nigeria</article-title>,<conf-name>paper presented at EC Jamboree 2012</conf-name>,<conf-loc>New York</conf-loc>,<conf-date>Oct. 5, 2012</conf-date>.</mixed-citation>
         </ref>
         <ref id="ref13">
            <label>13.</label>
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Jena M</string-name>
               </person-group>,<article-title>India: rising use of emergency contraceptives raises alarm</article-title>,<source>
                  <italic>Inter Press Service News Agency</italic>
               </source>,<month>May</month>
               <day>21</day>,<year>2010</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.ipsnews.net/2010/05/india-rising-use-of-emergency-contraceptives-raisesalarm/">http://www.ipsnews.net/2010/05/india-rising-use-of-emergency-contraceptives-raisesalarm/</uri>&gt;,<comment>accessed Jan. 24, 2014</comment>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <label>14.</label>
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Mawathe A</string-name>
               </person-group>,<article-title>Kenya concern over pill popping</article-title>,<source>
                  <italic>BBC News</italic>
               </source>,<month>July</month>
               <day>14</day>,<year>2009</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.news.bbc.co.uk/2/hi/africa/8145418.stm">http://news.bbc.co.uk/2/hi/africa/8145418.stm</uri>&gt;,<comment>accessed Jan. 24, 2014</comment>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <label>15.</label>
            <mixed-citation publication-type="web">
               <person-group person-group-type="author">
                  <string-name>Rai S</string-name>
               </person-group>,<article-title>India: popping morning after pills like candy</article-title>,<source>
                  <italic>MinnPost</italic>
               </source>,<month>June</month>
               <day>29</day>,<year>2010</year>, &lt;<uri xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="http://www.minnpost.com/global-post/2010/06/indiapopping-morning-after-pills-candy">http://www.minnpost.com/global-post/2010/06/indiapopping-morning-after-pills-candy</uri>&gt;,<comment>accessed Jan. 24, 2014</comment>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <label>16.</label>
            <mixed-citation>
               <person-group person-group-type="author">
                  <string-name>Daniels K</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Jones J</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Abma J</string-name>
               </person-group>,<article-title>Use of emergency contraception among women aged 15–44: United States, 2006–2010</article-title>,<source>
                  <italic>NCHS Data Brief</italic>
               </source>,<year>2013</year>, No.<issue>112</issue>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <label>17.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Moreau C</string-name>
               </person-group>,<person-group person-group-type="author">
                  <string-name>Bajos N</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Trussell J</string-name>
               </person-group>,<article-title>The impact of pharmacy access to emergency contraceptive pills in France</article-title>,<source>
                  <italic>Contraception</italic>
               </source>,<year>2006</year>,<volume>73</volume>(<issue>6</issue>):<fpage>602</fpage>–<lpage>608</lpage>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <label>18.</label>
            <mixed-citation publication-type="book">
               <collab>PRB</collab>,<source>
                  <italic>2013 World Population Data Sheet</italic>
               </source>,:<publisher-name>PRB</publisher-name>,<year>2013</year>.</mixed-citation>
         </ref>
         <ref id="ref19">
            <label>19.</label>
            <mixed-citation publication-type="book">
               <collab>United Nations Development Programme (UNDP)</collab>,<source>
                  <italic>Human Development Report 2013: The Rise of the South: Human Progress in a Diverse World</italic>
               </source>,:<publisher-name>UNDP</publisher-name>,<year>2013</year>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <label>20.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Padilla K</string-name>
               </person-group>,<publisher-name>Ipas Central America</publisher-name>,<comment>personal communication</comment>,<month>Jan.</month>
               <day>27</day>,<year>2014</year>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <label>21.</label>
            <mixed-citation publication-type="book">
               <collab>DKT International</collab>,<source>
                  <italic>2012 Contraceptive Social Marketing Statistics</italic>
               </source>,:<publisher-name>DKT International</publisher-name>,<year>2013</year>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <label>22.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Foreman M</string-name>
               </person-group>and<person-group person-group-type="author">
                  <string-name>Spieler J</string-name>
               </person-group>,<source>
                  <italic>Contraceptive Evidence: Questions and Answers</italic>
               </source>,:<publisher-name>PRB</publisher-name>,<year>2013</year>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
