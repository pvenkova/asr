<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jmedethics</journal-id>
         <journal-id journal-id-type="jstor">j50000494</journal-id>
         <journal-title-group>
            <journal-title>Journal of Medical Ethics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>British Medical Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">03066800</issn>
         <issn pub-type="epub">14734257</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">27717226</article-id>
         <article-categories>
            <subj-group>
               <subject>Symposium on Ethics and Public Health</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ethical Dilemmas in Malaria Drug and Vaccine Trials: A Bioethical Perspective</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Michele</given-names>
                  <surname>Barry</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Malcolm</given-names>
                  <surname>Molyneux</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">18</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i27717217</issue-id>
         <fpage>189</fpage>
         <lpage>192</lpage>
         <permissions>
            <copyright-statement>Copyright 1992 Journal of Medical Ethics</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/27717226"/>
         <abstract>
            <p>Malaria is a disease of developing countries whose local health services do not have the time, resources or personnel to mount studies of drugs or vaccines without the collaboration and technology of western investigators. This investigative collaboration requres a unique bridging of cultural differences with respect to human investigation. The following debate, sponsored by The Institute of Medicine and The American Society of Tropical Medicine and Hygiene, raises questions concerning the conduct of trans-cultural clinical malaria research. Specific questions are raised about the difficulties of informed consent in different cultural settings and whether there is any role for community involvement. Discussants debate whether drug and vaccine trials not approved in an industrialised country are ever defensible if performed in a third-world setting. Potential conflicting priorities between investigators are discussed and ideas regarding conflict resolution are offered.</p>
         </abstract>
         <kwd-group>
            <kwd>Malaria vaccine trials</kwd>
            <kwd>ethics</kwd>
            <kwd>developing countries</kwd>
            <kwd>informed consent</kwd>
            <kwd>malaria drug trails</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d574e153a1310">
            <label>1</label>
            <mixed-citation id="d574e160" publication-type="other">
Beauchamp T L, Childress J F, eds. Principles of
biomedical ethics. (2nd ed.) Oxford: Oxford University
Press, 1983; 339–343.</mixed-citation>
         </ref>
         <ref id="d574e173a1310">
            <label>2</label>
            <mixed-citation id="d574e180" publication-type="other">
Proposed international guidelines for biomedical research
involving human subjects. Geneva: World Health
Organisation 1981: 1–49.</mixed-citation>
         </ref>
         <ref id="d574e193a1310">
            <label>3</label>
            <mixed-citation id="d574e200" publication-type="other">
CIOMS. International guidelines for ethical review of
epidemiological studies. Law, medicine and health care
1991; 19,3–4: 247–258.</mixed-citation>
         </ref>
         <ref id="d574e213a1310">
            <label>4</label>
            <mixed-citation id="d574e220" publication-type="other">
World Health Organisation. World malaria situation,
1983. World health status quarterly 1985; 38: 193–231.</mixed-citation>
         </ref>
         <ref id="d574e231a1310">
            <label>5</label>
            <mixed-citation id="d574e238" publication-type="other">
Blum A L, Chalmers T C, Deutsch J et al. The Lugano
statement on controlled clinical trials. Journal of
international medical research 1987; 15: 2–22.</mixed-citation>
         </ref>
         <ref id="d574e251a1310">
            <label>6</label>
            <mixed-citation id="d574e258" publication-type="other">
Barry M. Ethical considerations of human investigation
in developing countries: the AIDS dilemma. New
England journal of medicine 1988; 319: 1083–1086.</mixed-citation>
         </ref>
         <ref id="d574e271a1310">
            <label>7</label>
            <mixed-citation id="d574e278" publication-type="other">
Ajayi O O. Taboos and clinical research in West Africa.
Journal of medical ethics 1980; 6: 61–63.</mixed-citation>
         </ref>
         <ref id="d574e288a1310">
            <label>8</label>
            <mixed-citation id="d574e295" publication-type="other">
De Craemer W. A cross-cultural perspective on
personhood. Milbank memorial fund quarterly 1983; 63:
19–34.</mixed-citation>
         </ref>
         <ref id="d574e308a1310">
            <label>9</label>
            <mixed-citation id="d574e315" publication-type="other">
Henderson D A et al. Assessment of vaccination
coverage, vaccine scar rates and smallpox scarring in five
areas of West Africa. Bulletin of the World Health
Organisation 1973; 84: 183–194.</mixed-citation>
         </ref>
         <ref id="d574e331a1310">
            <label>10</label>
            <mixed-citation id="d574e338" publication-type="other">
Hall A J. Public health trials in West Africa: logistics and
ethics. Investigative review board 1989; 11: 8–10.</mixed-citation>
         </ref>
         <ref id="d574e349a1310">
            <label>11</label>
            <mixed-citation id="d574e356" publication-type="other">
Steketee R W, Wirima J J, Heymann D C, Khoromana
C, Bremen J G. Efficacy of mefloquine and chloroquine
prophylaxis in pregnancy. Centers for Disease Control,
Atlanta, Georgia; Ministry of Health, Lilongwe,
Malawi. Presented at the 38th Annual Meeting of the
American Society of Tropical Medicine and Hygiene,
1989 Dec 10–14: Abstract 283: 222.</mixed-citation>
         </ref>
         <ref id="d574e382a1310">
            <label>12</label>
            <mixed-citation id="d574e389" publication-type="other">
Warrell D A, Looareesuwan S, Warrell M J et al.
Dexamethasone proves deleterious in cerebral malaria: a
double blind controlled trial in 100 comatose patients.
New England journal of medicine 1982; 306: 313–319.</mixed-citation>
         </ref>
         <ref id="d574e405a1310">
            <label>13</label>
            <mixed-citation id="d574e412" publication-type="other">
Hoffman S L, Rustama D, Punjabi NH^i al. High dose
dexamethasone in quinine-treated patients with cerebral
malaria: a double-blind, placebo-controlled trial.
Journal of infectious diseases 1988; 158,2: 325–331.</mixed-citation>
         </ref>
         <ref id="d574e428a1310">
            <label>14</label>
            <mixed-citation id="d574e435" publication-type="other">
Molyneux M E, Taylor T E, Wirima J J, Borgstein A.
Clinical features and prognostic indicators in paediatric
cerebral malaria: a study of 131 comatose Malawian
children. Quarterly journal of medicine 1989; 71,265:
441–459.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
