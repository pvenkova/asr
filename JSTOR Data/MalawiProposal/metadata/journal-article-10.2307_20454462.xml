<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">studfamiplan</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100383</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Studies in Family Planning</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley-Blackwell</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00393665</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284465</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20454462</article-id>
         <title-group>
            <article-title>Reproductive Inequality in Sub-Saharan Africa: Differentials versus Concentration</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sarah C.</given-names>
                  <surname>Giroux</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Parfait M.</given-names>
                  <surname>Eloundou-Enyegue</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel T.</given-names>
                  <surname>Lichter</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">39</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20454458</issue-id>
         <fpage>187</fpage>
         <lpage>198</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20454462"/>
         <abstract>
            <p>Within developing countries, our understanding of reproductive inequality--how fertility is distributed within a population--has been shaped largely by studies of fertility differentials, a practical but partial-information measure. In this study, we examine whether exclusive reliance on differentials biases this understanding. Findings based on recent data from sub-Saharan Africa show bias. We find that historical and especially cross-country comparisons can yield substantially different conclusions about the magnitude and even the direction of inequality patterns and trends, depending on whether differentials or fuller-information measures are used. For instance, the fertility differentials associated with education have remained relatively stable as national fertility has fallen, but inequality (as calculated by a fuller measure) has increased. Such results underscore the value of complementing existing studies of fertility differentials with analyses based on fuller-information measures. The analyses also show how change in differential fertility behavior and in the educational composition of national populations has shaped recent variations in reproductive inequality in the region.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d36e174a1310">
            <label>3</label>
            <mixed-citation id="d36e183" publication-type="other">
Firebaugh 1999</mixed-citation>
         </ref>
         <ref id="d36e190a1310">
            <label>7</label>
            <mixed-citation id="d36e197" publication-type="other">
Carmines and Zeller 1979</mixed-citation>
         </ref>
         <ref id="d36e204a1310">
            <label>8</label>
            <mixed-citation id="d36e211" publication-type="other">
Jejeebhoy 1995</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d36e227a1310">
            <mixed-citation id="d36e231" publication-type="other">
Ainsworth, Martha, Kathleen Beegle, and Andrew Nyamete. 1996. "The
impact of women's schooling on fertility and contraceptive use: A
study of 14 sub-Saharan countries." World Bank Economic Review
10(1): 85-122.</mixed-citation>
         </ref>
         <ref id="d36e247a1310">
            <mixed-citation id="d36e251" publication-type="other">
Bongaarts, John. 2003. "Completing the Fertility Transition in the De-
veloping World: The Role of Educational Differences and Fertility
Preferences." Policy Research Division Working Paper No. 177. New
York: Population Council.</mixed-citation>
         </ref>
         <ref id="d36e267a1310">
            <mixed-citation id="d36e271" publication-type="other">
—. 2006. "The Causes of Stalling Fertility Transitions." Studies in
Family Planning 37(1): 1-16.</mixed-citation>
         </ref>
         <ref id="d36e281a1310">
            <mixed-citation id="d36e285" publication-type="other">
Boyle, P.M. 1996. "Parents, private schools, and the politics of an emerg-
ing civil society in Cameroon." The Journal of Modern African Studies
34(4): 609-622.</mixed-citation>
         </ref>
         <ref id="d36e299a1310">
            <mixed-citation id="d36e303" publication-type="other">
Buchmann, Claudia. 1999. "Educational inequality and poverty in sub-
Saharan Africa." Prospects: Quarterly Review of Comparative Educa-
tion 29(4): 503-515.</mixed-citation>
         </ref>
         <ref id="d36e316a1310">
            <mixed-citation id="d36e320" publication-type="other">
Carmines, Edward G. and Richard A. Zeller. 1979. Reliability and Validity
Assessment. Beverly Hills, CA: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d36e330a1310">
            <mixed-citation id="d36e334" publication-type="other">
Case, Anne, Christina Paxson, and Joseph Ableidinger. 2004. "Orphans
in Africa: Parental death, poverty, and school enrollment." Demog-
raphy 41(3): 483-508.</mixed-citation>
         </ref>
         <ref id="d36e347a1310">
            <mixed-citation id="d36e351" publication-type="other">
Castro Martin, Teresa. 1995. "Women's education and fertility: Results
from 26 demographic and health surveys." Studies in Family Plan-
ning 26(4): 187-202.</mixed-citation>
         </ref>
         <ref id="d36e364a1310">
            <mixed-citation id="d36e368" publication-type="other">
Demographic and Health Surveys (DHS). 2006. Statcompiler, ORC Macro.
&amp;amp;lt;http://www.statcompiler.com&amp;amp;gt;. Accessed 1 January 2006.</mixed-citation>
         </ref>
         <ref id="d36e378a1310">
            <mixed-citation id="d36e382" publication-type="other">
DeRose, Laurie F. and Oyestein Kravdal. 2007. "Education reversals and
first-birth timing in sub-Saharan Africa: A dynamic multi-level ap-
proach." Demography 44(1): 59-77.</mixed-citation>
         </ref>
         <ref id="d36e396a1310">
            <mixed-citation id="d36e400" publication-type="other">
Desai, Sonalde and Soumya Alva. 1998. "Maternal education and child
health: Is there a strong causal relationship?" Demography 35(1):
71-81.</mixed-citation>
         </ref>
         <ref id="d36e413a1310">
            <mixed-citation id="d36e417" publication-type="other">
Diamond, Ian, Margaret Newby, and Sarah Varie. 1999. "Female edu-
cation and fertility: Examining the links." In Critical Perspectives
on Schooling and Fertility in the Developing World. Eds. Caroline H.
Bledsoe, John B. Casterline, Jennifer A. Johnson-Kuhn, and John G.
Haaga. Washington, DC: National Academy Press. Pp. 23-48.</mixed-citation>
         </ref>
         <ref id="d36e436a1310">
            <mixed-citation id="d36e440" publication-type="other">
Eloundou-Enyegue, Parfait M. and C. Shannon Stokes. 2007. "Demo-
graphic transitions and children's resources: Bonus or divergence."
Demographic Research 16(7): 195-218.</mixed-citation>
         </ref>
         <ref id="d36e453a1310">
            <mixed-citation id="d36e457" publication-type="other">
Firebaugh, Glenn. 1999. "Empirics of world income inequality." Ameri-
can Journal of Sociology 104(6): 1,597-1,631.</mixed-citation>
         </ref>
         <ref id="d36e467a1310">
            <mixed-citation id="d36e471" publication-type="other">
Giroux, Sarah. 2007. "Rural parentage and labor market disadvantage in
a sub-Saharan setting: Source and trends." Unpublished.</mixed-citation>
         </ref>
         <ref id="d36e481a1310">
            <mixed-citation id="d36e485" publication-type="other">
Jejeebhoy, Shireen J. 1995. Women's Education, Autonomy and Reproduc-
tive Behavior. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d36e496a1310">
            <mixed-citation id="d36e500" publication-type="other">
Kirk, Dudley and Bernard Pillet. 1998. "Fertility levels, trends, and dif-
ferentials in sub-Saharan Africa in the 1980s and 1990s." Studies in
Family Planning 29(1): 1-22.</mixed-citation>
         </ref>
         <ref id="d36e513a1310">
            <mixed-citation id="d36e517" publication-type="other">
Kremer, Michael and Daniel Chen. 2002. "Income distribution dynamics
with endogenous fertility." Journal of Economic Growth 7(3): 227-258.</mixed-citation>
         </ref>
         <ref id="d36e527a1310">
            <mixed-citation id="d36e531" publication-type="other">
Lam, David. 1986. "The dynamics of population growth, differential
fertility, and inequality." The American Economic Review 76(5):
1,103-1,116.</mixed-citation>
         </ref>
         <ref id="d36e544a1310">
            <mixed-citation id="d36e548" publication-type="other">
Lichter, Daniel T. and Julian Wooton. 2005. "The concentration of re-
production in low-fertility societies: The case of the United States."
In The New Population Problem: Why Families Are Shrinking in Devel-
oped Countries and What It Means. Eds. Alan Booth and Ann Crouter.
Mahwah, NJ: Lawrence Erlbaum Associates. Pp. 213-224.</mixed-citation>
         </ref>
         <ref id="d36e567a1310">
            <mixed-citation id="d36e571" publication-type="other">
Lloyd, Cynthia and Paul Hewett. 2003. "Primary Schooling in Sub-Sa-
haran Africa: Recent Trends and Current Challenges." Population
Council Working Paper No. 176. New York: Population Council.</mixed-citation>
         </ref>
         <ref id="d36e584a1310">
            <mixed-citation id="d36e588" publication-type="other">
Lutz, Wolfgang. 1987. "The Concentration of Reproduction: A Global
Perspective." International Institute for Applied Systems Analysis Work-
ing Paper No. 87-51. Laxenberg, Austria.</mixed-citation>
         </ref>
         <ref id="d36e602a1310">
            <mixed-citation id="d36e606" publication-type="other">
Maralani, Vida and Robert D. Mare. 2006. "The intergenerational effects
of changes in women's educational attainments." American Socio-
logical Review 71(4): 542-565.</mixed-citation>
         </ref>
         <ref id="d36e619a1310">
            <mixed-citation id="d36e623" publication-type="other">
National Research Council (NRC). 1986. Population Growth and Eco-
nomic Development: Policy Questions. Washington, DC: National
Academy Press.</mixed-citation>
         </ref>
         <ref id="d36e636a1310">
            <mixed-citation id="d36e640" publication-type="other">
Shapiro, David and B. Oleko Tambashe. 2001. "Fertility transition in ur-
ban and rural sub-Saharan Africa: Preliminary evidence of a three-
stage process." The Journal of African Policy Studies 7(2-3): 111-136.</mixed-citation>
         </ref>
         <ref id="d36e653a1310">
            <mixed-citation id="d36e657" publication-type="other">
Shkolnikov, Vladimir, Evgueni M. Andreev, René Houle, and James
W. Vaupel. 2007. "The concentration of reproduction in cohorts of
women in Europe and the United States." Population and Develop-
ment Review 33(1): 67-100.</mixed-citation>
         </ref>
         <ref id="d36e673a1310">
            <mixed-citation id="d36e677" publication-type="other">
Spielauer, Martin. 2005. "Concentration of Reproduction in Austria:
General Trends and Differentials by Educational Attainment and
Urban-rural Setting." Max Planck Institute for Demographic Research
Working Paper No. 2005-012. Rostock, Germany.</mixed-citation>
         </ref>
         <ref id="d36e693a1310">
            <mixed-citation id="d36e697" publication-type="other">
Tabutin, Dominique and Bruno Schoumaker. 2004. "The demography of
sub-Saharan Africa from the 1950s to the 2000s: A survey of changes
and a statistical assessment." Population 59(3): 457-554.</mixed-citation>
         </ref>
         <ref id="d36e711a1310">
            <mixed-citation id="d36e715" publication-type="other">
Thornton, Arland. 2001. "The developmental paradigm, reading history
sideways, and family change." Demography 38(4): 449-465.</mixed-citation>
         </ref>
         <ref id="d36e725a1310">
            <mixed-citation id="d36e729" publication-type="other">
Vaupel, James W. and Dianne Goodwin. 1987. "The concentration of
reproduction among U.S. women, 1917-1980." Population and De-
velopment Review 13(4): 723-730.</mixed-citation>
         </ref>
         <ref id="d36e742a1310">
            <mixed-citation id="d36e746" publication-type="other">
World Bank. 2007. Gender Stats Online. &amp;amp;lt;http://devdata.worldbank.
org/home.asp.&amp;amp;gt; Accessed 21 September 2007.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
