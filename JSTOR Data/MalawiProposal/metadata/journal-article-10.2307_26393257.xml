<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">conssoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019729</journal-id>
         <journal-title-group>
            <journal-title>Conservation and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>WOLTERS KLUWER INDIA PRIVATE LIMITED</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09724923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">09753133</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26393257</article-id>
         <article-categories>
            <subj-group>
               <subject>Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Conceptualising ‘Core’ Medicinal Floras</article-title>
            <subtitle>A Comparative and Methodological Study of Phytomedical Resources in Related Indonesian Populations</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Ellen</surname>
                  <given-names>Roy</given-names>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Puri</surname>
                  <given-names>Rajindra</given-names>
               </string-name>
            </contrib>
            <aff>Centre for Biocultural Diversity, School of Anthropology and Conservation, University of Kent, Canterbury, Kent, UK</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2016</year>
            <string-date>2016</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">14</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26393251</issue-id>
         <fpage>345</fpage>
         <lpage>358</lpage>
         <permissions>
            <copyright-statement>Copyright: © Ellen and Puri 2016</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26393257"/>
         <abstract xml:lang="eng">
            <p>Although there have been comparative studies of the botanical content and patterning of ethnopharmacopoeias across different ecological zones, there are few attempts to compare these systematically within the same ethnographic and ecological areas in Southeast Asia. This paper undertakes a simple quantitative survey of the medicinal plant resources of three populations on the island of Seram in the Moluccas, and then compares the results with those from five populations on Borneo. The comparison reveals some surprising omissions and patterns, given what is known about the medicinal use of plants in island Southeast Asia from other sources. The paper discusses ecological, cultural, and methodological reasons for the lack of expected congruence. A model of medicinal plant resource pools is developed to aid comparison, and it is suggested that we need to examine carefully what might be understood by a ‘core’ medicinal flora. While biodiversity loss is evident in the areas where the studies have been conducted, and this may impact some actual and more potential medicinal plants, it is less likely to erode core ethno pharmacopoeias.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>Indonesia</kwd>
            <kwd>Borneo</kwd>
            <kwd>Moluccas</kwd>
            <kwd>medicinal plants</kwd>
            <kwd>core ethnopharmacopoeias</kwd>
            <kwd>resource pools</kwd>
            <kwd>biodiversity loss</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Afdhal, A.F. and R.L. Welsch. 1988. The rise of the modern jamu industry in Indonesia: a preliminary overview. In: The context of medicines in developing countries: studies in pharmaceutical anthropology. (eds. S. van der Geest and S.R. Whyte) Pp. 149-172. Dordrecht: Kluwer.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Amiguet, V.T., J.T. Arnason, P. Maquin, V. Cal, P. Sanchez-Vindas, L.P. Alvarez. 2006. A regression analysis of Q’eqchi Maya medicinal plants from Southern Belize. Economic Botany 60(1): 24-38.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Atlas bahasa tanah Maluku. 1996. Pusat Pengkajian dan Pengembangan Maluku Universitas Pattimura and Summer Institute of Linguistics, Ambon.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Bates, D.M. 1985. Plant utilization: patterns and prospects. Economic Botany 39(3): 241-265.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Bell, J.M. and A.S. van Houten. 1993. The medicinal plants of central Seram. In: Natural history of Seram, Maluku, Indonesia (eds. Edwards, I.D., A.A. Macdonald, and J. Proctor). Pp. 207-230. Andover: Intercept.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Berlin, E.A. and B. Berlin. 2005. Some field methods in medical ethnobiology. Field Methods 17(3): 235-268.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Borgatti, S. 1996. ANTHROPAC 4. Natick, Mass: Analytic Technologies.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Burkill, I.H. 1935. A dictionary of economic products of the Malay peninsula. 2 vols. London: Crown Agents for the Colonies.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Caniago, I. and S.F. Siebert. 1998. Medicinal plant economy, knowledge and conservation in Kalimantan, Indonesia. Economic Botany 52(3): 229-250.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Cronquist, A. 1981. An integrated system of classification of flowering plants. New York: Columbia University Press.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Edwards, I.D. 1993. Introduction. In: Natural history of Seram, Maluku, Indonesia (eds. Edwards, J.D., A.A. Macdonald, and J. Proctor). Pp. 1-12. Andover: Intercept.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Ellen, R.F. 1991. Nuaulu betel chewing: ethnobotany, technique and cultural significance. Cakalele: Maluku Research Journal 2(2): 97-122.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Ellen, R. 2006. Introduction. In: Ethnobiology and the science of humankind (ed. Ellen, R.). Special Issue of the Journal of the Royal Anthropological Institute 12: 1-22. Oxford: Blackwell.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Ellen, R.F. 2007. Plots, typologies and ethnoecology: local and scientific understandings of forest diversity on Seram. In: Global vs local knowledge (ed. Sillitoe, P.). Pp. 41-74. Oxford: Berghahn.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Ellen, R. 2009. A modular approach to understanding the transmission of technical knowledge: Nuaulu basket-making from Seram, eastern Indonesia. Journal ofMaterial Culture 14(2): 243-277.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Ellen, R.F., J. Bernstein, and Bantong Antaran. 1997. The use of plot surveys for the study of ethnobotanical knowledge: a Brunei Dusun example. Journal of Ethnobiology 17(1): 69-96.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Ellen, R. and M.D. Fischer. 2013. Introduction: on the concept of cultural transmission. In: Understanding cultural transmission in anthropology: a critical synthesis. (eds. Ellen, R., S.J. Lycett, and S.E. Johns). Pp. 1-54. London: Berghahn.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Etkin, N.L. 1993. Anthropological methods in ethnopharmacology. Journal of Ethnopharmacology 38: 93-104.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Etkin, N.L. 1994. Consuming a therapeutic landscape: a multicontextual framework for assessing the health significance of human-plant interactions. In: People-plant relationships: setting research priorities (eds. Flagler, J. and R.P. Poincelot). Pp. 61-81. Philadelphia: Haworth Press.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Etkin, N.L., L.X. Gollin, D. Binnendyk, and H. Soselisa. 1996. Ethnomedicine in Maluku, eastern Indonesia. Cakalele: Maluku Research Journal (7): 33-50.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Florey, M.J. 2001. Threats to indigenous knowledge: a case study from eastern Indonesia. In: On biocultural diversity: linking language, knowledge and the environment (ed. Maffi, L.). Pp. 325-342. Washington DC and London: Smithsonian Institution Press.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Florey, M.J. and X.Y. Wolff. 1998. Incantations and herbal medicines: Alune ethnomedical knowledge in a context of change. Journal ofEthnobiology 18(1): 39-67.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Gollin, L.X. 2001. The taste and smell of Taban Kenyah (Kenyah Medicine): an exploration of the chemosensory selection criteria for medicinal plants among the Leppo’ Ke of East Kalimantan, Borneo, Indonesia. Ph.D.thesis. University of Hawai’i, Honolulu, USA.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Hamilton, A.C. 2004. Medicinal plants, conservation and livelihoods. Biodiversity and Conservation 13: 1477-1517.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Heinrich, M., A. Ankli, B. Frei, C. Weimann, and O. Sticher. 1998. Medicinal plants in Mexico: healers’ consensus and cultural importance. Social Science and Medicine 47(11): 1859-1871.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Heinrich, M., S.E. Edwards, D.E. Moerman, and M. Leonti. 2009. Ethnopharmacological field studies: a critical assessment of their conceptual basis on methods. Journal ofEthnopharmacology 124(1): 1-17.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Heyne, K. 1913-17. De nuttige planten van Nederlandsch-Indie. Batavia: Ruygrok, Departement van Landbouw, Nijverheid en Handel.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Hsu, E. 2010. Introduction: plants in medical practice and common sense. In: Plants, health and healing: on the interface of ethnobotany and medical anthropology (eds. E. E. Hsu and S. Harris). Pp.1-48. Berghahn, Oxford, UK.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Koizumi, M. and K. Momose. 2007. Penan Benalui wildplant use, classification, and nomenclature. CurrentAnthropology 48(3): 454-459.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Leaman, D. 1997. The medical ethnobotany of the Kenyah of East Kalimantan (Indonesian Borneo). Ph.D. thesis. University of Ottawa, Ottawa, Canada.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Lemmens, R.H.M.J. and N. Bunyapraphatsara (eds.). 2003. Plant resources of South-East Asia: medicinal and poisonous plants. Volume 12(3). Pp. 23-28. Bogor: Prosea Foundation.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Mabberley, D.J. 1987. The plant-book: a portable dictionary of the higher plants. Cambridge: CUP.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">MacKinnon, K., Gusti Hatta, Hakimah Halim, and A. Mangalik. 1997. The ecology of Kalimantan. Oxford: OUP.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Moerman, D.E., R.W. Pemberton, D. Kiefer, and B. Berlin. 1999. A comparative analysis of five medicinal floras. Journal ofEthnobiology 19(1): 49-67.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Monk, K.A., Y. De Fretes, and G. Reksodiharjo-Lilley. 1997. The ecology of Nusa Tenggara and Maluku. Oxford: OUP.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Morris, B. 1996. Chewa medical botany: a study of herbalism in southern Malawi, International African Institute, Hamburg: Lit.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Padoch, C., T.C. Jessup, H. Soedjito, and K. Kartawinata. 1991. Complexity and conservation of medicinal plants: anthropological cases from Peru and Indonesia. In: The conservation of medicinal plants (eds. Akerele, O., V. Heywood, and H. Synge). Pp. 321-328. Cambridge: CUP.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Padua, L.S. de, N. Bunyapraphatsara and R.H.M.J. Lemmens (eds.). 1999. Plant resources of South-East Asia: medicinal and poisonous plants. Volume 12(1). Leiden: Backhuys.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Puri, R.K. 2001. Bulungan ethnobiology handbook. Bogor, Indonesia: Centre for International Forestry Research.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Puri, R.K. 2005. Deadly dances in the Bornean rainforest: hunting knowledge of the Penan Benalui. Leiden: KITLV Press.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Rifai, M.A. and K. Kartawinata. 1991. Germplasm, genetic erosion and the conservation of Indonesian medicinal plants. In: Conservation of medicinal plants (eds. Akerele, O., V. Heywood, and H. Synge). Pp. 281-292. Cambridge: CUP.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Rumphius, G.E. 2011 [1741-1750]. The Ambonese herbal. Volume 2. (Trans. E. M. Beekman). New Haven and London: Yale University Press.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Slikkerveer, L.J. 1987. Apotik hidup: indigenous Indonesia medicine for selfreliance. Paper for the LIPI workshop on Jamu and PHC development. Jakarta: LIPI, pp. 25.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Slikkerveer, L.J. and M.K.L. Slikkerveer. 1995. Taman Obat Keluarga (TOGA): indigenous Indonesian medicine for self-reliance. In: The cultural dimension of development: indigenous knowledge systems (eds. Warren, D.M., L.J. Slikkerveer, and D. Brokensha). Pp. 13-34. London: Intermediate Technology Publications.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Stepp, J.R. 2004. The role of weeds as sources of pharmaceuticals. Journal ofEthnopharmacology 92: 163-166.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">The Plant List 2013. Version 1.1. Published on the Internet; http://www.theplantlist.org/(accessed 20 December 2014).</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Van Valkenburg, J.L.C.H. and N. Bunyapraphatsara (eds.). 2002. Plant resources of South-East Asia: medicinal and poisonous plants. Volume 12(2). Leiden: Backhuys.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Voeks, R.A. 1996. Tropical forest healers and habitat preference. Economic Botany 50: 381-400.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Voeks, R. 2004. Disturbance pharmacopoeias: medicine and myth from the humid tropics. Annals of the Association of American Geographers 94: 868-888.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Voeks, R.A. and Nyawa, S. bin 2001. Healing Flora of the Brunei Dusun. Borneo Research Bulletin 32: 178-195.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Waldstein, A. and C. Adams. 2006. The interface between medical anthropology and medical ethnobiology. In: Ethnobiology and the science of humankind (ed. Ellen, R.). Special Issue of the Journal of the Royal Anthropological Institute 12: 95-117. Oxford: Blackwell.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Wijayakusuma, H.M, A. Hembing, Setiawan Wirian, T. Yaputra, Setiawan Dalimartha, and Bambang Wibowo. 1992-94. Tanaman berkhasiat obat di Indonesia. Jakarta: Pustaka Kartini.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Wolff, X.Y. and M.J. Florey. 1998. Foraging, agricultural, and culinary practices among the Alune of west Seram, with implications for the changing significance of cultivated plants as foodstuffs. In: Old world places, new world problems: exploring resource management issues in eastern Indonesia (eds. Pannell, S. and F. von Benda-Beckmann). Pp. 267-321. Canberra: Centre for Resource and Environmental Studies, Australian National University.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
