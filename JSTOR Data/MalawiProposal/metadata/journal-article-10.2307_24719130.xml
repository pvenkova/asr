<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">sociindirese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000231</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Social Indicators Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03038300</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15730921</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24719130</article-id>
         <title-group>
            <article-title>Political Regime and Human Capital: A Cross-Country Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeroen</given-names>
                  <surname>Klomp</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jakob</given-names>
                  <surname>de Haan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">111</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24719123</issue-id>
         <fpage>45</fpage>
         <lpage>73</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24719130"/>
         <abstract>
            <p>We examine the relationship between different dimensions of the political regime in place and human capital using a two-step structural equation model. In the first step, we employ factor analysis on 16 human capital indicators to construct two new human capital measures (basic and advanced human capital). In the second step, we estimate the impact of our political variables on human capital, using a cross-sectional structural model for some 100 countries. We conclude that democracy is positively related to basic human capital, while regime instability has a negative link with basic human capital. Governance has a positive relationship with advanced human capital, while government instability has a negative link with advanced human capital. Finally, we also find an indirect positive effect of governance and democracy on both types of human capital through their effect on income.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d71e211a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d71e218" publication-type="other">
As Adams-Kane and Lim (2009) point out, there is a strong theoretical case that human capital can drive
growth in both neoclassical and endogenous growth models, although there is also the possibility of reverse
causality (Bils and Klenow 2000). Using panel data from 120 developing countries from 1975 to 2000,
Baldacci et al. (2008) explore the direct and indirect channels linking social spending, human capital, and
growth in a system of equations. These authors find that both education and health spending have a positive
and significant direct impact on the accumulation of education and health capital, and thus can lead to higher
economic growth. However, some papers do not report that human capital is significantly related to eco-
nomic growth (see, for instance, Pritchett 2001). Recently, Sunde and Vischer (2011) replicated earlier
results from the literature showing that both initial levels and changes in human capital have positive growth
effects. They also report that the effects are heterogeneous across countries with different levels of
development.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e256a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d71e263" publication-type="other">
The same holds for most studies explaining cross-country growth differences. See Wößmann (2000) for a
critical discussion of human capital measures used in growth regressions.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e273a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d71e280" publication-type="other">
See also Pinto and Timmons (2005), Becker and Tomes (1979), Aghion and Bolton (1990), Castello-
Climent and Domenech (2008), and Saint-Paul and Verdier (1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d71e290a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d71e297" publication-type="other">
The first step in this analysis is to check whether the data used is suitable for an EFA using the Kaiser-
Meyer-Olkin measure of sampling adequacy testing whether the partial correlation among variables is low.
A test statistic that is higher than 0.6 indicates that the data is suitable for an EFA (Kaiser 1970). An
alternative test is Bartlett's test of sphericity, that checks whether the correlation matrix is an identity matrix
in which case the factor model is inappropriate. Both tests indicate that the human capital data and political
data used in the present paper are suitable for an EFA (Lattin et al. 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d71e321a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d71e328" publication-type="other">
E(r.) = 0 and E(£e') = 0.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e335a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d71e342" publication-type="other">
We tested each specification for multivariate normality. In those cases where transformations have been
necessary in order for the data to satisfy the multivariate normality assumption, the non-transformed data
produced similar results.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e355a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d71e362" publication-type="other">
Various indicators of governance also include information on the decision-making process within gov-
ernment. As this dimension is captured by our regime measure, we do not include it here.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e372a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d71e379" publication-type="other">
The EM algorithm is an iterative method, which involves forming a log-likelihood function for the latent
data as if they were observed and taking its expectation, while in the maximization step the resulting
expected log-likelihood is maximized.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e392a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d71e399" publication-type="other">
Arguably, democracy and governance are closely related. The correlation between the two is about 0.6.
We performed a factor analysis on all indicators of governance and democracy. However, the results did not
make much sense. This is also the case if we perform a factor analysis on all political indicators.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e412a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d71e419" publication-type="other">
See also Klomp and De Haan (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d71e427a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d71e434" publication-type="other">
Experience is calculated as follows [share of labour force with education level /] x [ 1 - unemployment
rate of education level j] x [average age of labour with education level i - years of education).</mixed-citation>
            </p>
         </fn>
         <fn id="d71e444a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d71e451" publication-type="other">
Using the factor scores as a regressor in a regression model would lead to inconsistent estimation results
because the latent variables are imperfectly measured. In particular, the coefficients of the latent variables
will be biased and inconsistent (Wansbeek and Meijer 2000; Bollen and Lennox 1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d71e464a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d71e471" publication-type="other">
Causality between income and human capital may ran in both directions (Narayan and Smith 2004).
In Sect. 6 we will examine this in more detail.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e481a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d71e488" publication-type="other">
Checchi (2000) reports a negative effect of income inequality on secondary education enrolment rates.
However, Castello-Climent and Domenech (2008) find an insignificant result for income inequality when
accounting for life expectancy.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e501a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d71e508" publication-type="other">
We identified the following country groups: Western Europe, Eastern Europe, North and Central Asia,
South Asia, Middle East, North America, Central America and the Caribbean, Latin America, North and
Central Africa, Southern Africa, Australia and Oceania.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e521a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d71e528" publication-type="other">
If we directly include our political variables into the general-to-specific method, our main findings do not
change. Results are available on request.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e539a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d71e546" publication-type="other">
The model is estimated with AMOS 7.0 using the maximum likelihood estimation.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e553a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d71e560" publication-type="other">
We also included all six dimensions of the political system in one model. However, the results do not
change substantially. The results are available on request.</mixed-citation>
            </p>
         </fn>
         <fn id="d71e570a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d71e577" publication-type="other">
The effect of democracy and governance on income is higher in developing countries. The indirect
effects are confirmed by the outcomes of a bootstrapping analysis and rolling regressions. The results are
available upon request.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d71e599a1310">
            <mixed-citation id="d71e603" publication-type="other">
Acemoglu, D., Johnson, S., Robinson, J. A., &amp; Yared, P. (2005). From education to democracy? American
Economic Review, 95, 44-49.</mixed-citation>
         </ref>
         <ref id="d71e613a1310">
            <mixed-citation id="d71e617" publication-type="other">
Adams-Kane, J., &amp; Lim, J. J. (2009). Institutions, education, and economic performance. Paper presented at
the 2009 Silvaplana workshop on political economy.</mixed-citation>
         </ref>
         <ref id="d71e627a1310">
            <mixed-citation id="d71e631" publication-type="other">
Aghion, P., &amp; Bolton, P. (1990). Government domestic debt and the risk of default: A political economic
model of strategic role of debt. In R. Dornbusch &amp; M. Draghi (Eds.), Political debt management.
Theory and history (pp. 315-345). Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d71e644a1310">
            <mixed-citation id="d71e648" publication-type="other">
Ahrend, R. (2002). Press freedom, human capital and corruption. Delta working paper, 2002-11, Paris.</mixed-citation>
         </ref>
         <ref id="d71e656a1310">
            <mixed-citation id="d71e660" publication-type="other">
Altinok, N., &amp; Murseli, H. (2007). International database on human capital quality. Economics Letters, 96,
237-244.</mixed-citation>
         </ref>
         <ref id="d71e670a1310">
            <mixed-citation id="d71e674" publication-type="other">
Bai, J., &amp; Ng, S. (2002). Determining the number of factors in approximate factor models. Econometrica,
70, 191-221.</mixed-citation>
         </ref>
         <ref id="d71e684a1310">
            <mixed-citation id="d71e688" publication-type="other">
Baldacci, E., Clements, B., Gupta, S., &amp; Cui, Q. (2008). Social spending, human capital, and growth in
developing countries. World Development, 36, 1317-1341.</mixed-citation>
         </ref>
         <ref id="d71e698a1310">
            <mixed-citation id="d71e702" publication-type="other">
Barro, R. (1999). Human capital and growth in cross country regressions. Swedish Economic Policy Review,
6, 237-277.</mixed-citation>
         </ref>
         <ref id="d71e712a1310">
            <mixed-citation id="d71e716" publication-type="other">
Barro, R., &amp; Lee, J. (2001). International data on educational attainment, updates and implications. Oxford
Economic Papers, 53, 541-563.</mixed-citation>
         </ref>
         <ref id="d71e726a1310">
            <mixed-citation id="d71e730" publication-type="other">
Barro, R., &amp; Lee, J. (2010). A new data set of educational attainment in the world, 1950-2010. NBER
working paper 15902.</mixed-citation>
         </ref>
         <ref id="d71e741a1310">
            <mixed-citation id="d71e745" publication-type="other">
Baum, M. A., &amp; Lake, D. A. (2003). The political economy of growth, democracy and human capital.
American Journal of Political Science, 47, 333-347.</mixed-citation>
         </ref>
         <ref id="d71e755a1310">
            <mixed-citation id="d71e759" publication-type="other">
Beck, T., Clarke, G., Groff, A., Keefer, P., &amp; Walsh, P. (2001). New tools in comparative political economy.
The database of political institutions. World Bank Economic Review, 15, 165-176.</mixed-citation>
         </ref>
         <ref id="d71e769a1310">
            <mixed-citation id="d71e773" publication-type="other">
Becker, G. S„ Murphy, K. M., &amp; Tamura, R. (1990). Human capital, fertility, and economic growth. Journal
of Political Economy, 98, 12-37.</mixed-citation>
         </ref>
         <ref id="d71e783a1310">
            <mixed-citation id="d71e787" publication-type="other">
Becker, G. S., &amp; Tomes, N. (1979). An equilibrium theory of the distribution of income and intergenera-
tional mobility. Journal of Political Economy, 87, 1153-1189.</mixed-citation>
         </ref>
         <ref id="d71e797a1310">
            <mixed-citation id="d71e801" publication-type="other">
Benhabib, J., &amp; Spiegel, M. (1994). The role of human capital in economic development evidence from
aggregate cross-country data. Journal of Monetary Economics, 34, 143-173.</mixed-citation>
         </ref>
         <ref id="d71e811a1310">
            <mixed-citation id="d71e815" publication-type="other">
Bhattacharyya, S. (2009). Unbundled institutions, human capital and growth. Journal of Comparative
Economics, 37, 106-120.</mixed-citation>
         </ref>
         <ref id="d71e826a1310">
            <mixed-citation id="d71e830" publication-type="other">
Bils, M., &amp; Klenow, P. (2000). Does schooling cause growth? American Economic Review, 90, 1160-1183.</mixed-citation>
         </ref>
         <ref id="d71e837a1310">
            <mixed-citation id="d71e841" publication-type="other">
Bollen, K., &amp; Lennox, R. (1991). Conventional wisdom on measurement: A structural equation perspective.
Psychological Bulletin, 110, 305-314.</mixed-citation>
         </ref>
         <ref id="d71e851a1310">
            <mixed-citation id="d71e855" publication-type="other">
Brown, D. (1999). Reading, writing, and regime type: Democracy impact on primary school enrollment.
Political Research Quarterly, 52, 681-707.</mixed-citation>
         </ref>
         <ref id="d71e865a1310">
            <mixed-citation id="d71e869" publication-type="other">
Brown, D„ &amp; Hunter, W. (2000). World Bank directives, domestic interests and politics of human capital
investment in Latin America. Comparative Political Studies, 37, 842-864.</mixed-citation>
         </ref>
         <ref id="d71e879a1310">
            <mixed-citation id="d71e883" publication-type="other">
Brown, D., &amp; Hunter, W. (2004). Democracy and human capital formation. Comparative Political Studies,
33, 113-143.</mixed-citation>
         </ref>
         <ref id="d71e893a1310">
            <mixed-citation id="d71e897" publication-type="other">
Campos, J., Ericsson, N. R., &amp; Hendry, D. F. (2004). General to specific modeling. Cheltenham: Edward
Elgar.</mixed-citation>
         </ref>
         <ref id="d71e908a1310">
            <mixed-citation id="d71e912" publication-type="other">
Castello-Climent, A. (2008). On the distribution of education and democracy. Journal of Development
Economics, 87, 179-190.</mixed-citation>
         </ref>
         <ref id="d71e922a1310">
            <mixed-citation id="d71e926" publication-type="other">
Castello-Climent, A., &amp; Domenech, R. (2008). Human capital inequality, life expectancy and economic
growth. The Economic Journal, 118, 653-677.</mixed-citation>
         </ref>
         <ref id="d71e936a1310">
            <mixed-citation id="d71e940" publication-type="other">
Checchi, D. (2000). Does educational achievement help to explain income inequality. Working paper
University of Milan 11.2000.</mixed-citation>
         </ref>
         <ref id="d71e950a1310">
            <mixed-citation id="d71e954" publication-type="other">
Cohen, D., &amp; Soto, M. (2007). Growth and human capital: Good data, good results. Journal of Economic
Growth, 12, 51-76.</mixed-citation>
         </ref>
         <ref id="d71e964a1310">
            <mixed-citation id="d71e968" publication-type="other">
Databanks International. (2005). Cross-national time-series data archive. Binghamton, NY.
http://www.databanksinternational.com/32.html.</mixed-citation>
         </ref>
         <ref id="d71e978a1310">
            <mixed-citation id="d71e982" publication-type="other">
de Haan, J. (2007). Political institutions and economic growth reconsidered. Public Choice, 127, 281-292.</mixed-citation>
         </ref>
         <ref id="d71e990a1310">
            <mixed-citation id="d71e994" publication-type="other">
Dempster, A., Laird, N., &amp; Rubin, D. (1977). Maximum likelihood from incomplete data via the EM
algorithm. Journal of the Royal Statistical Society: Series B, 39, 1-38.</mixed-citation>
         </ref>
         <ref id="d71e1004a1310">
            <mixed-citation id="d71e1008" publication-type="other">
Dreher, A., Kotsogiannis, C., &amp; McCorriston, S. (2007). Corruption around the world: Evidence from a
structural model. Journal of Comparative Economics, 35, 443^166.</mixed-citation>
         </ref>
         <ref id="d71e1018a1310">
            <mixed-citation id="d71e1022" publication-type="other">
Fedderke, J., &amp; Klitgaard, R. (1998). Economic growth and social indicators. An exploratory analysis.
Economic Development and Cultural Change, 46, 455^189.</mixed-citation>
         </ref>
         <ref id="d71e1032a1310">
            <mixed-citation id="d71e1036" publication-type="other">
Feng, Y. (2003). Democracy, governance, and economic performance. Theory and evidence. Cambridge,
MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d71e1046a1310">
            <mixed-citation id="d71e1050" publication-type="other">
Francis, A. (2007). The human capital peace, economic development, democracy and international conflict.
Working paper University of Chicago.</mixed-citation>
         </ref>
         <ref id="d71e1060a1310">
            <mixed-citation id="d71e1064" publication-type="other">
Gwartney, J., &amp; Lawson, R. (2006). Economic freedom of the world. Vancouver, BC: Fraser Institute.</mixed-citation>
         </ref>
         <ref id="d71e1072a1310">
            <mixed-citation id="d71e1076" publication-type="other">
Haque, N. U., &amp; Kim, S. (1994). Human capital flight, impact of migration on income and growth. IMF
working paper 155.</mixed-citation>
         </ref>
         <ref id="d71e1086a1310">
            <mixed-citation id="d71e1090" publication-type="other">
Helliwell, J. F. (1994). Empirical linkages between democracy and economic growth. British Journal of
Political Science, 24, 225-248.</mixed-citation>
         </ref>
         <ref id="d71e1100a1310">
            <mixed-citation id="d71e1104" publication-type="other">
International Country Risk Guide. (2003). International Country Risk Guide. New York: PRS Group.</mixed-citation>
         </ref>
         <ref id="d71e1111a1310">
            <mixed-citation id="d71e1115" publication-type="other">
Jaggers, K., &amp; Gurr. R. T. (2006). Polity IV, political regime characteristics and transitions, 1800-2006.
Ann Arbor, MI: Inter-University Consortium for Political and Social Research.</mixed-citation>
         </ref>
         <ref id="d71e1125a1310">
            <mixed-citation id="d71e1129" publication-type="other">
Jong-A-Pin, R. (2009). On the measurement of political instability and its impact on economic growth.
European Journal of Political Economy, 25, 15-29.</mixed-citation>
         </ref>
         <ref id="d71e1139a1310">
            <mixed-citation id="d71e1143" publication-type="other">
Jöreskog, K. (2000). Latent variable scores and their uses. Downloadable from: http://www.ssicentral.
com/lisrel.</mixed-citation>
         </ref>
         <ref id="d71e1154a1310">
            <mixed-citation id="d71e1158" publication-type="other">
Kaiser, H. (1970). A second generation little jiffy. Pschychometrika, 35, 401—415.</mixed-citation>
         </ref>
         <ref id="d71e1165a1310">
            <mixed-citation id="d71e1169" publication-type="other">
Katona, G. (1980). How expectations are really formed. Challenge, 23, 32-35.</mixed-citation>
         </ref>
         <ref id="d71e1176a1310">
            <mixed-citation id="d71e1180" publication-type="other">
Klomp, J., &amp; de Haan, J. (2008). Effects of governance on health: A cross-national analysis of 101 countries.
Kyklos, 61, 599-614.</mixed-citation>
         </ref>
         <ref id="d71e1190a1310">
            <mixed-citation id="d71e1194" publication-type="other">
Krueger, A., &amp; Lindahl, M. (2001). Education for growth: Why and for whom? Journal of Economic
Literature, 39, 1101-1136.</mixed-citation>
         </ref>
         <ref id="d71e1204a1310">
            <mixed-citation id="d71e1208" publication-type="other">
Lake, D., &amp; Baum, D. (2001). The Invisible hand of democracy: Political control and the provision of public
services. Comparative Political Studies, 34, 587-621.</mixed-citation>
         </ref>
         <ref id="d71e1218a1310">
            <mixed-citation id="d71e1222" publication-type="other">
Lattin, J., Carroll, D., &amp; Green, P. (2003). Analyzing multivariate data. Pacific Grove, CA: Duxbury Press.</mixed-citation>
         </ref>
         <ref id="d71e1230a1310">
            <mixed-citation id="d71e1234" publication-type="other">
Lipset, S. M. (1959). Some social requisites of democracy: Economic development and political legitimacy.
American Political Science Review, 53, 69-105.</mixed-citation>
         </ref>
         <ref id="d71e1244a1310">
            <mixed-citation id="d71e1248" publication-type="other">
Maloney, W. (2002). Missed opportunities, innovation and resource-based growth in Latin America. Eco-
nomi'a, 3, 111-167.</mixed-citation>
         </ref>
         <ref id="d71e1258a1310">
            <mixed-citation id="d71e1262" publication-type="other">
Mankiw, G. N., Romer, D., &amp; Weil, D. N. (1992). A contribution to the empirics of economic growth.
Quarterly Journal of Economics, 107, 407—437.</mixed-citation>
         </ref>
         <ref id="d71e1272a1310">
            <mixed-citation id="d71e1276" publication-type="other">
McMahon, W. (1987). Student labor market expectations. In G. Psacharopoulos (Ed.), Economics of edu-
cation, research and studies (pp. 182-186). Oxford: Pergamon Press.</mixed-citation>
         </ref>
         <ref id="d71e1286a1310">
            <mixed-citation id="d71e1290" publication-type="other">
Munck, G., &amp; Verkuilen, J. (2002). Conceptualizing and measuring democracy: Evaluating alternative
indices. Comparative Political Studies, 35, 5-34.</mixed-citation>
         </ref>
         <ref id="d71e1300a1310">
            <mixed-citation id="d71e1304" publication-type="other">
Narayan, P. K., &amp; Smith, R. (2004). Temporal causality and the dynamics of exports, human capital and real
income in China. International Journal of Applied Economics, 1, 24—45.</mixed-citation>
         </ref>
         <ref id="d71e1315a1310">
            <mixed-citation id="d71e1319" publication-type="other">
Noorbakhsh, F., Paloni, A., &amp; Youseff, A. (2001). Human capital and FDI inflows to developing countries.
New empirical evidence. World Development, 29, 1593-1610.</mixed-citation>
         </ref>
         <ref id="d71e1329a1310">
            <mixed-citation id="d71e1333" publication-type="other">
Pinto, P., &amp; Timmons, J. (2005). The political determinants of economic performance, political competition
and the sources of growth. Comparative Political Studies, 38, 26-50.</mixed-citation>
         </ref>
         <ref id="d71e1343a1310">
            <mixed-citation id="d71e1347" publication-type="other">
Pritchett, L. (2001). Where has all the education gone? World Bank Economic Review, 15, 367-391.</mixed-citation>
         </ref>
         <ref id="d71e1354a1310">
            <mixed-citation id="d71e1358" publication-type="other">
Ross, M. (2006). Does democracy reduce infant mortality? American Journal of Political Science, 50,
860-874.</mixed-citation>
         </ref>
         <ref id="d71e1368a1310">
            <mixed-citation id="d71e1372" publication-type="other">
Saint-Paul, G„ &amp; Verdier, T. (1993). Education, democracy and growth. Journal of Development Eco-
nomics, 42, 406-407.</mixed-citation>
         </ref>
         <ref id="d71e1382a1310">
            <mixed-citation id="d71e1386" publication-type="other">
Sunde, U., &amp; Vischer, T. (2011). Human capital and growth: Specification matters. IZA discussion paper
5991.</mixed-citation>
         </ref>
         <ref id="d71e1397a1310">
            <mixed-citation id="d71e1401" publication-type="other">
Treier, S., &amp; Jackman, S. (2008). Democracy as a latent variable. American Journal of Political Science, 52,
201-217.</mixed-citation>
         </ref>
         <ref id="d71e1411a1310">
            <mixed-citation id="d71e1415" publication-type="other">
University of Texas. (2006). University of Texas inequality project, http://utip.gov.utexas.edu.</mixed-citation>
         </ref>
         <ref id="d71e1422a1310">
            <mixed-citation id="d71e1426" publication-type="other">
Veira, P. C., &amp; Teixeira, A. C. (2006). Human capital and corruption: A microeconomic model of the bribes
market with democratic contestability. FEP working papers Faculdade de Economia do Porto No. 212,
Universidade do Porto.</mixed-citation>
         </ref>
         <ref id="d71e1439a1310">
            <mixed-citation id="d71e1443" publication-type="other">
Wansbeek, T. J„ &amp; Meijer, E. (2000). Measurement error and latent variables in econometrics. Amsterdam:
Elsevier.</mixed-citation>
         </ref>
         <ref id="d71e1453a1310">
            <mixed-citation id="d71e1457" publication-type="other">
Wößmann, L. (2000). Specifying human capital: A review, some extensions, and development effects. Kiel
working paper no. 1007.</mixed-citation>
         </ref>
         <ref id="d71e1467a1310">
            <mixed-citation id="d71e1471" publication-type="other">
Woodhall, M. (1987). Earnings and education. In G. Psacharopoulos (Ed.), Economics of education,
research and studies (pp. 209-217). New York: Pergamon Press.</mixed-citation>
         </ref>
         <ref id="d71e1482a1310">
            <mixed-citation id="d71e1486" publication-type="other">
World Bank. (2009). World Bank development indicators 2006. CD-Rom.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
