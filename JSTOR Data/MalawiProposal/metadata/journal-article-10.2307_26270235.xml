<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26270235</article-id>
         <article-categories>
            <subj-group>
               <subject>Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The role of strong-tie social networks in mediating food security of fish resources by a traditional riverine community in the Brazilian Amazon</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Mertens</surname>
                  <given-names>Frédéric</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
               <xref ref-type="aff" rid="af2">²</xref>
               <xref ref-type="aff" rid="af3">³</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Fillion</surname>
                  <given-names>Myriam</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
               <xref ref-type="aff" rid="af3">³</xref>
               <xref ref-type="aff" rid="af4">⁴</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Saint-Charles</surname>
                  <given-names>Johanne</given-names>
               </string-name>
               <xref ref-type="aff" rid="af3">³</xref>
               <xref ref-type="aff" rid="af5">⁵</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Mongeau</surname>
                  <given-names>Pierre</given-names>
               </string-name>
               <xref ref-type="aff" rid="af5">⁵</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Távora</surname>
                  <given-names>Renata</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Passos</surname>
                  <given-names>Carlos José Sousa</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
               <xref ref-type="aff" rid="af3">³</xref>
               <xref ref-type="aff" rid="af6">⁶</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Mergler</surname>
                  <given-names>Donna</given-names>
               </string-name>
               <xref ref-type="aff" rid="af3">³</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Centro de Desenvolvimento Sustentável, Universidade de Brasília,</aff>
            <aff id="af2">
               <label>²</label>Community of Practice in Ecosystem Approaches to Health in Latin America and the Caribbean (CoPEH-LAC),</aff>
            <aff id="af3">
               <label>³</label>Centre de recherche interdisciplinaire sur le bien-être, la santé, la société et l’environnement (CINBIOSE), Université du Québec à Montréal,</aff>
            <aff id="af4">
               <label>⁴</label>Department of Biology, University of Ottawa,</aff>
            <aff id="af5">
               <label>⁵</label>Faculté de communication, Université du Québec à Montréal,</aff>
            <aff id="af6">
               <label>⁶</label>Faculty UnB Planaltina, University of Brasília</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>9</month>
            <year>2015</year>
            <string-date>Sep 2015</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">20</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26270221</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2015 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26270235"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>Social networks are a significant way through which rural communities that manage resources under common property regimes obtain food resources. Previous research on food security and social network analysis has mostly focused on egocentric network data or proxy variables for social networks to explain how social relations contribute to the different dimensions of food security. Whole-network approaches have the potential to contribute to former studies by revealing how individual social ties aggregate into complex structures that create opportunities or constraints to the sharing and distribution of food resources. We used a whole-network approach to investigate the role of network structure in contributing to the four dimensions of food security: food availability, access, utilization, and stability. For a case study of a riparian community from the Brazilian Amazon that is dependent on fish as a key element of food security, we mapped the community strong-tie network among 97% of the village population over 14 years old (n = 336) by integrating reciprocated friendship and occupational ties, as well as close kinship relationships. We explored how different structural properties of the community network contribute to the understanding of (1) the availability of fish as a community resource, (2) community access to fish as a dietary resource, (3) the utilization of fish for consumption in a way that allows the villagers to maximize nutrition while at the same time minimizing toxic risks associated with mercury exposure, and (4) the stability of the fish resources in local ecosystems as a result of cooperative behaviors and community-based management. The contribution of whole-network approaches to the study of the links between community-based natural resource management and food security were discussed in the context of recent social-ecological changes in the Amazonian region.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>Amazon</kwd>
            <kwd>common property regimes</kwd>
            <kwd>community-based management</kwd>
            <kwd>fish consumption</kwd>
            <kwd>food security</kwd>
            <kwd>mercury</kwd>
            <kwd>natural resource management</kwd>
            <kwd>social networks</kwd>
            <kwd>strong ties</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Adams, C., R. S. S. Murrieta, and R. A. Sanches. 2005.Agriculture and diet among riverine populations of the Amazonian floodplains: new perspectives (in Portuguese).Ambiente &amp; Sociedade 8:1-22.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Agrawal, A. 2001. Common property institutions and sustainable governance of resources. World Development 29:1649-1672. http://dx.doi.org/10.1016/S0305-750X(01)00063-8</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Ahluwalia, I. B., J. M. Dodds, and M. Baligh. 1998. Social support and coping behaviors of low-income families experiencing food insufficiency in North Carolina. Health Education &amp; Behavior 25:599-612. http://dx.doi.org/10.1177/109019819802500507</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Almeida, O. T., K. Lorenzen, and D. G. McGrath. 2009. Fishing agreements in the lower Amazon: for gain and restraint. Fisheries Management and Ecology 16:61-67. http://dx.doi.org/10.1111/j.1365-2400.2008.00647.x</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Barrett, C. B. 2010. Measuring food insecurity. Science 327:825-828. http://dx.doi.org/10.1126/science.1182768</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Beaumier, M. C., and J. D. Ford. 2010. Food insecurity among Inuit women exacerbated by socio-economic stresses and climate change. Canadian Journal of Public Health 101:196-201.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Bebbington, A., and T. Perreault. 1999. Social capital, development, and access to resources in highland Ecuador. Economic Geography 75:395-418. http://dx.doi.org/10.2307/144478</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Becquey, E., F. Delpeuch, A. M. Konaté, H. Delsol, M. Lange, M. Zoungrana, and Y. Martin-Prevel. 2012. Seasonality of the dietary dimension of household food security in urban Burkina Faso. British Journal of Nutrition 107:1860-1870. http://dx.doi.org/10.1017/S0007114511005071</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Berkes, F. 2003. Alternatives to conventional management: lessons from small-scale fisheries. Environments 31:5-19.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Bodin, Ö., and B. I. Crona. 2008. Management of natural resources at the community level: exploring the role of social capital and leadership in a rural fishing community. World Development 36:2763-2779. http://dx.doi.org/10.1016/j.worlddev.2007.12.002</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Bodin, Ö., and B. I. Crona. 2009. The role of social networks in natural resource governance: what relational patterns make a difference? Global Environmental Change 19:366-374. http://dx.doi.org/10.1016/j.gloenvcha.2009.05.002</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Bodin, Ö., B. Crona, and H. Ernstson. 2006. Social networks in natural resource management: what is there to learn from a structural perspective? Ecology and Society 11(2): r2. [online] URL: http://www.ecologyandsociety.org/vol11/iss2/resp2/</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Bodin, Ö., and C. Prell. 2011. Social networks and natural resource management: uncovering the social fabric of environmental governance. Cambridge University Press, Cambridge, UK. http://dx.doi.org/10.1017/CBO9780511894985</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Borgatti, S. P. 2002. NetDraw: graph visualization software. Analytic Technologies, Harvard, Massachusetts, USA. [online] URL: http://www.analytictech.com/ucinet.htm</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Borgatti, S. P., M. G. Everett, and L. C. Freeman. 2002. Ucinet for Windows: software for social network analysis. Analytic Technologies, Harvard, Massachusetts, USA. [online] URL: https://sites.google.com/site/ucinetsoftware/home</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Crona, B., and Ö. Bodin. 2010. Power asymmetries in small-scale fisheries: a barrier to governance transformability? Ecology and Society 15(4): 32. [online] URL: http://www.ecologyandsociety.org/vol15/iss4/art32/</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Dale, A., and L. Newman. 2010. Social capital: a necessary and sufficient condition for sustainable community development? Community Development Journal 45:5-21. http://dx.doi.org/10.1093/cdj/bsn028</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Demeda, K. 2010. Quanto vale uma “onça”? Os significados das relações entre os brasilienses e as suas paisagens na região do Tapajós, oeste do Pará. Dissertation. Universidade Federal do Pará, Belém, Pará, Brazil. [online] URL: http://ppgcs.ufpa.br/arquivos/dissertacoes/dissertacaoTurma2008-KatiaDemeda.pdf</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Dhokarh, R., D. A. Himmelgreen, Y.-K. Peng, S. Segura-Pérez, A. Hromi-Fiedler, and R. Pérez-Escamilla. 2011. Food insecurity is associated with acculturation and social networks in Puerto Rican households. Journal of Nutrition Education and Behavior 43:288-294. http://dx.doi.org/10.1016/j.jneb.2009.11.004</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Díaz, H. L., R. D. Drumm, J. Ramírez-Johnson, and H. Oidjarv. 2002. Social capital, economic development and food security in Peru’s mountain region. International Social Work 45:481-405. http://dx.doi.org/10.1177/00208728020450040601</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Dodds, P. S., R. Muhamad, and D. J. Watts. 2003. An experimental study of search in global social networks. Science 301:827-829. http://dx.doi.org/10.1126/science.1081058</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Dorea, J. D. 2003. Fish are central in the diet of Amazonian riparians: should we worry about their mercury concentrations? Environmental Research 92:232-244. http://dx.doi.org/10.1016/s0013-9351(02)00092-0</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Fillion, M., A. Philibert, F. Mertens, M. Lemire, C. J. S. Passos, B. Frenette, J. R. D. Guimarães, and D. Mergler. 2011. Neurotoxic sequelae of mercury exposure: an intervention and follow-up study in the Brazilian Amazon. EcoHealth 8:210-222. http://dx.doi.org/10.1007/s10393-011-0710-1</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Food and Agriculture Organization of the United Nations (FAO). 2010. The state of food insecurity in the world. FAO, Rome, Italy.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Friedkin, N. 1980. A test of structural features of Granovetter’s strength of weak ties theory. Social Networks 2:411-422. http://dx.doi.org/10.1016/0378-8733(80)90006-4</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Gilmour, P. W., P. D. Dwyer, and R. W. Day. 2011. Beyond individual quotas: the role of trust and cooperation in promoting stewardship of five Australian abalone fisheries. Marine Policy 35:692-702. http://dx.doi.org/10.1016/j.marpol.2011.02.010</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Gochfeld, M., and J. Burger. 2005. Good fish/bad fish: a composite benefit–risk by dose curve. NeuroToxicology 26:511-520. http://dx.doi.org/10.1016/j.neuro.2004.12.010</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Grafton, R. Q. 2005 Social capital and fisheries governance. Ocean &amp; Coastal Management 48:753-766. http://dx.doi.org/10.1016/j.ocecoaman.2005.08.003</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Granovetter, M. S. 1973. The strength of weak ties. American Journal of Sociology 78:1360-1380. http://dx.doi.org/10.1086/225469</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Hanazaki, N., F. Berkes, C. S. Seixas, and N. Peroni. 2013.Livelihood diversity, food security and resilience among the Caiçara of coastal Brazil. Human Ecology 41:153-164. http://dx.doi.org/10.1007/s10745-012-9553-9</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Haythornthwaite, C. 1996. Social network analysis: an approach and technique for the study of information exchange. Library &amp;Information Science Research 18:323-342. http://dx.doi.org/10.1016/S0740-8188(96)90003-1</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Jentoft, S. 2000. The community: a missing link of fisheries management. Marine Policy 24:53-60. http://dx.doi.org/10.1016/S0308-597X(99)00009-3</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Kossinets, G., and D. J. Watts. 2006. Empirical analysis of an evolving social network. Science 311:88-90. http://dx.doi.org/10.1126/science.1116869</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Lebel, J., D. Mergler, F. Branches, M. Lucotte, M. Amorim, F. Larribe, and J. Dolbec. 1998. Neurotoxic effects of low-level methylmercury contamination in the Amazonian Basin. Environmental Research 79:20-32. http://dx.doi.org/10.1006/enrs.1998.3846</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Lebel, J., M. Roulet, D. Mergler, M. Lucotte, and F. Larribe. 1997.Fish diet and mercury exposure in a riparian Amazonian population. Water, Air, &amp; Soil Pollution 97:31-44. http://dx.doi.org/10.1023/A:1018378207932</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Luke, D. A., and J. K. Harris. 2007. Network analysis in public health: history, methods, and applications. Annual Review of Public Health 28:69-93. http://dx.doi.org/10.1146/annurev. publhealth.28.021406.144132</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Mahaffey, K. R. 2004. Fish and shellfish as dietary sources of methylmercury and the ω-3 fatty acids, eicosahexaenoic acid and docosahexaenoic acid: risks and benefits. Environmental Research 95:414-428. http://dx.doi.org/10.1016/j.envres.2004.02.006</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Marín, A., S. Gelcich, J. C. Castilla, and F. Berkes. 2012. Exploring social capital in Chile’s coastal benthic comanagement system using a network approach. Ecology and Society 17(1): 13. http://dx.doi.org/10.5751/ES-04562-170113</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Marin, A., and B. Wellman. 2011. Social network analysis: an introduction. Pages 11-25 in J. Scott and P. J. Carrington, editors. The SAGE handbook of social network analysis. SAGE, Thousand Oaks, California, USA.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Marsden, P. V., and K. E. Campbell. 1984. Measuring tie strength. Social Forces 63:482-501. http://dx.doi.org/10.1093/sf/63.2.482</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">McGrath, D. G., O. T. Almeida, and F. D. Merry. 2007. The influence of community management agreements on household economic strategies: cattle grazing and fishing agreements on the Lower Amazon floodplain. International Journal of the Commons 1:67-87.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Mergler, D., H. A. Anderson, L. H. M. Chan, K. R. Mahaffey, M. Murray, M. Sakamoto, and A. H. Stern. 2007. Methylmercury exposure and health effects in humans: a worldwide concern.AMBIO: A Journal of the Human Environment 36:3-11. http://dx.doi.org/10.1579/0044-7447(2007)36[3:MEAHEI]2.0.CO;2</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Mertens, F., J. Saint-Charles, M. Lucotte, and D. Mergler. 2008.Emergence and robustness of a community discussion network on mercury contamination and health in the Brazilian Amazon. Health Education &amp; Behavior 35:509-521. http://dx.doi.org/10.1177/1090198108320357</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Mertens, F., J. Saint-Charles, and D. Mergler. 2012. Social communication network analysis of the role of participatory research in the adoption of new fish consumption behaviors. Social Science &amp; Medicine 75:643-650. http://dx.doi.org/10.1016/j.socscimed.2011.10.016</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Mertens, F., J. Saint-Charles, D. Mergler, C. J. Passos, and M. Lucotte. 2005. Network approach for analyzing and promoting equity in participatory ecohealth research. Ecohealth 2:113-126. http://dx.doi.org/10.1007/s10393-004-0162-y</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Mertens, F., R. Távora, I. F. da Fonseca, R. Grando, M. Castro, and K. Demeda. 2011. Social networks, social capital and environmental governance in the Amazonian Gateway Territory. Acta Amazonica 41:481-492. http://dx.doi.org/10.1590/S0044-59672011000400006</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Mtika, M. M. 2001. The AIDS epidemic in Malawi and its threat to household food security. Human Organization 60:178-188. http://dx.doi.org/10.17730/humo.60.2.cnlpmfrrg8hay9xh</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Murrieta, R. S. S. 2001. Dialética do sabor: alimentação, ecologia e vida cotidiana em comunidades ribeirinhas da Ilha de Ituqui, Baixo Amazonas, Pará. Revista de Antropologia 44:39-88. http://dx.doi.org/10.1590/S0034-77012001000200002</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Newman, L., and A. Dale. 2007. Homophily and agency: creating effective sustainable development networks. Environment, Development and Sustainability 9:79-90. http://dx.doi.org/10.1007/s10668-005-9004-5</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Ostrom, E., and C. Hess. 2007. Private and common property rights. School of Public and Environmental Affairs Research Paper No. 2008-11-01. Indiana University, Bloomington, Indiana, USA. http://dx.doi.org/10.2139/ssrn.1936062</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Passos, C. J. S., and D. Mergler. 2008. Human mercury exposure and adverse health effects in the Amazon: a review. Cadernos de Saúde Pública 24(supplement 4):S503-S520. http://dx.doi.org/10.1590/S0102-311X2008001600004</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Passos, C. J. S., D. Mergler, M. Fillion, M. Lemire, F. Mertens, J.R. D. Guimarães, and A. Philibert. 2007a. Epidemiologic confirmation that fruit consumption influences mercury exposure in riparian communities in the Brazilian Amazon. Environmental Research 105:183-193. http://dx.doi.org/10.1016/j.envres.2007.01.012</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Passos, C. J. S., D. Mergler, E. Gaspar, S. Morais, M. Lucotte, F. Larribe, R. Davidson, and S. de Grosbois. 2001. Caracterização do consumo alimentar de uma população ribeirinha na Amazônia Brasileira. Revista Saúde e Ambiente 4:72-84.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Passos, C. J., D. Mergler, E. Gaspar, S. Morais, M. Lucotte, F. Larribe, R. Davidson, and S. de Grosbois. 2003. Eating tropical fruit reduces mercury exposure from fish consumption in the Brazilian Amazon. Environmental Research 93:123-130. http://dx.doi.org/10.1016/S0013-9351(03)00019-7</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Passos, C. J. S., D. Mergler, M. Lemire, M. Fillion, J. R. D. Guimarães. 2007b. Fish consumption and bioindicators of inorganic mercury exposure. Science of the Total Environment 373:68-76. http://dx.doi.org/10.1016/j.scitotenv.2006.11.015</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Portes, A. 1998. Social capital: its origins and applications in modern sociology. Annual Review of Sociology 24:1-24. http://dx.doi.org/10.1146/annurev.soc.24.1.1</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Prell, C., C. Hubacek, and M. Reed. 2009. Stakeholder analysis and social network analysis in natural resource management. Society &amp; Natural Resources: An International Journal 22:501-518. http://dx.doi.org/10.1080/08941920802199202</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Pretty, J., and H. Ward. 2001. Social capital and the environment. World Development 29:209-227. http://dx.doi.org/10.1016/S0305-750X(00)00098-X</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Rogers, E. 2003. Diffusion of innovations. Fifth edition. Free, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Roulet, M., M. Lucotte, R. Canuel, N. Farella, M. Courcelles, J.-R. D. Guimarães, D. Mergler, and M. Amorim. 2000. Increase in mercury contamination recorded in lacustrine sediments following deforestation in the central Amazon. Chemical Geology 165:243-266. http://dx.doi.org/10.1016/S0009-2541(99)00172-2</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Ruef, M. 2002. Strong ties, weak ties and islands: structural and cultural predictors of organizational innovation. Industrial and Corporate Change 11:427-449. http://dx.doi.org/10.1093/icc/11.3.427</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Saint-Charles, J., M.-E. Rioux-Pelletier, P. Mongeau, and F. Mertens. 2012. Diffusion of environmental health information: the role of sex- and gender-differentiated pathways. Pages 69-76 in Institute of Gender and Health, Canadian Institutes of Health Research, editors. What a difference sex and gender make: a gender, sex and health research casebook. Institute of Gender and Health, Canadian Institutes of Health Research, Vancouver, British Columbia, Canada. [online] URL: http://www.cihr-irsc.gc.ca/e/documents/What_a_Difference_Sex_and_Gender_Make-en.pdf</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Sampaio da Silva, D. 2008. Ressources halieutiques du Tapajós en Amazonie brésilienne: une étude écosystémique reliant les pratiques de pêche, les caractéristiques des bassins versants et la contamination au mercure. Dissertation. Université du Québec àMontréal, Montréal, Québec, Canada. [online] URL: http://www.archipel.uqam.ca/1060/1/D1657.pdf</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Sampaio da Silva, D., M. Lucotte, and S. Paquet. 2011. Les connaissances au quotidien: perceptions et savoirs des populations riveraines de l’Amazonie sur leurs ressources halieutiques. Confins 13. [online] URL: http://confins.revues.org/7334</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Sampaio da Silva, D., M. Lucotte, M. Roulet, H. Poirier, D. Mergler, E. Oliveira Santos, and M. Crossa. 2005. Trophic structure and bioaccumulation of mercury in fish of 3 natural lakes of the Brazilian Amazon. Water, Air, &amp; Soil Pollution 165:77-94. http://dx.doi.org/10.1007/s11270-005-4811-8</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Sandström, A., and C. Rova. 2010. Adaptive co-management networks: a comparative analysis of two fishery conservation areas in Sweden. Ecology and Society 15(3): 14. [online] URL: http://www.ecologyandsociety.org/vol15/iss3/art14/</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Scott, J. 2000. Social network analysis: a handbook. SAGE, Thousand Oaks, California, USA.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Turner, R. A., N. V. C. Polunin, and S. M. Stead. 2014. Social networks and fishers’ behavior: exploring the links between information flow and fishing success in the Northumberland lobster fishery. Ecology and Society 19(2): 38. http://dx.doi.org/10.5751/ES-06456-190238</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Walker, J. L., D. H. Holben, M. L. Kropf, J. P. Holcomb Jr., and H. Anderson. 2007. Household food insecurity is inversely associated with social capital and health in females from special supplemental nutrition program for women, infants, and children households in Appalachian Ohio. Journal of the American Dietetic Association 107:1989-1993. http://dx.doi.org/10.1016/j.jada.2007.08.004</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">Wasserman, S., and K. Faust. 1994. Social network analysis: methods and applications. Cambridge University Press, New York, New York, USA. http://dx.doi.org/10.1017/cbo9780511815478</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Watts, D. J. 1999. Networks, dynamics, and the small-world phenomenon. American Journal of Sociology 105:493-527. http://dx.doi.org/10.1086/210318</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Watts, D. J. 2004. The “new” science of networks. Annual Review of Sociology 30:243-270. http://dx.doi.org/10.1146/annurev. soc.30.020404.104342</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Wellman, B., and S. Wortley, 1990. Different strokes from different folks: community ties and social support. American Journal of Sociology 96:558-588. http://dx.doi.org/10.1086/229572</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">White, D. R., and M. Houseman. 2002. The navigability of strong ties: small worlds, tie strength, and network topology. Complexity 8:72-81. http://dx.doi.org/10.1002/cplx.10053</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">Woolcock, M. 2001. The place of social capital in understanding social and economic outcomes Canadian Journal of Policy Research 2:11-17.</mixed-citation>
         </ref>
         <ref id="ref76">
            <mixed-citation publication-type="other">Woolcock, M., and D. Narayan. 2000. Social capital: implications for development theory, research, and policy. World Bank Research Observer 15:225-249. http://dx.doi.org/10.1093/wbro/15.2.225</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
