<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt13x0dmp</book-id>
      <subj-group>
         <subject content-type="call-number">P115.5.A35G46 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Multilingualism</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Native language and education</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Language policy</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African languages</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Congresses</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Linguistics</subject>
      </subj-group>
      <book-title-group>
         <book-title>Languages in Africa</book-title>
         <subtitle>Multilingualism, Language Policy, and Education</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Zsiga</surname>
               <given-names>Elizabeth C.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Boyer</surname>
               <given-names>One Tlale</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Kramer</surname>
               <given-names>Ruth</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>03</day>
         <month>02</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9781626161528</isbn>
      <isbn content-type="ppub">1626161526</isbn>
      <isbn content-type="epub">9781626161535</isbn>
      <isbn content-type="epub">1626161534</isbn>
      <publisher>
         <publisher-name>Georgetown University Press</publisher-name>
         <publisher-loc>Washington, DC</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Georgetown University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt13x0dmp"/>
      <abstract abstract-type="short">
         <p>People in many African communities live within a series of concentric circles when it comes to language. In a small group, a speaker uses an often unwritten and endangered mother tongue that is rarely used in school. A national indigenous language-written, widespread, sometimes used in school-surrounds it. An international language like French or English, a vestige of colonialism, carries prestige, is used in higher education, and promises mobility-and yet it will not be well known by its users.The essays in<italic>Languages in Africa</italic>explore the layers of African multilingualism as they affect language policy and education. Through case studies ranging across the continent, the contributors consider multilingualism in the classroom as well as in domains ranging from music and film to politics and figurative language. The contributors report on the widespread devaluing and even death of indigenous languages. They also investigate how poor teacher training leads to language-related failures in education. At the same time, they demonstrate that education in a mother tongue can work, linguists can use their expertise to provoke changes in language policies, and linguistic creativity thrives in these multilingual communities.</p>
      </abstract>
      <counts>
         <page-count count="160"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.3</book-part-id>
                  <title-group>
                     <title>List of Figures and Tables</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.4</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Layers of Language—Some Bad News and Some Good News on Multilingualism, Language Policy, and Education in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ZSIGA</surname>
                           <given-names>ELIZABETH C.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOYER</surname>
                           <given-names>ONE TLALE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>KRAMER</surname>
                           <given-names>RUTH</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>THE CHAPTERS IN THIS VOLUME examine the phenomenon of multilingualism in Africa as it affects individuals, communities, ethnic groups, nations, and the larger interconnected world. The perspectives and case studies presented here cover the whole of the continent, from Morocco to South Africa, from Côte d’Ivoire to Kenya. Some chapters present a discouraging picture; others, a more hopeful one. There is both bad and good news to report.</p>
                     <p>The contributors to this volume came together to discuss the problems and promise of African multilingualism at a joint meeting of the Annual Conference on African Linguistics and the Georgetown University Round</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Early Reading Success in Africa:</title>
                     <subtitle>The Language Factor</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>TRUDELL</surname>
                           <given-names>BARBARA</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>ADGER</surname>
                           <given-names>CAROLYN TEMPLE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>12</fpage>
                  <abstract>
                     <p>TO THE CASUAL EYE, the two domains of literacy and linguistics may not seem closely linked. In fact, however, concerns about reading and writing competencies do intersect with the field of linguistics, particularly where unwritten or recently written languages are being developed for written use in learning and communication. This situation is particularly common in the language-rich regions of Africa, Latin America, Asia, and the Pacific. For this reason, understanding the role of linguistics in developing written-language competencies is becoming increasingly critical to successful reading and writing acquisition across the two-thirds (or developing) world.</p>
                     <p>Reading competencies have become central in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Multilingualism as a Sociolinguistic Phenomenon:</title>
                     <subtitle>Evidence from Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOKAMBA</surname>
                           <given-names>EYAMBA G,</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>There are 6,909 spoken languages in the world (<italic>Ethnologue</italic>2009), but there are only approximately 196 to 200 countries. In spite of the realization in the twentyfirst century that societal and individual multilingualism must therefore be common, rather than exotic, phenomena, multilingualism remains grossly understudied and is therefore highly misunderstood</p>
                     <p>This misunderstanding ranges from incorrect answers to the fundamental question of “what constitutes multilingualism” to flawed evaluations and characterizations of multilingual competence and practices. Except for a few recent studies published since the beginning of the last decade—including those by Cook (2009); Cenoz, Hufeisen, and Jessner (2001); Aronin and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Classroom Discourse in Bilingual and Multilingual Kenyan Primary Schools</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KIRAMBA</surname>
                           <given-names>LYDIAH KANANU</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>49</fpage>
                  <abstract>
                     <p>LANGUAGE PLAYS A MAJOR ROLE in education. It is a means of communication in the classroom and serves as an expression of identity and power. A need to connect the populations within different countries and around the globe has led to language policies being put in place in order to guide language practices in individual nation-states. These policies outline the use of languages and specify the national and official languages that the nation will adopt for education and general communication (Corson 1990). The central purpose of this chapter is to provide a critical review of studies of classroom discourse in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Investigating Teacher Effects in Mother-Tongue-Based Multilingual Education Programs</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WALTER</surname>
                           <given-names>STEPHEN L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>ALTHOUGH A LARGE CORPUS of research has accumulated documenting and investigating the existence of large teacher effects in education, especially in low-income countries (e.g., UNESCO 1998), there remains considerable uncertainty as to the specific nature of this effect. Much of the literature has focused on general issues, such as the training of pedagogues, compensation, certification, motivation and commitment, community support (or the lack thereof), and deployment. Fortunately, some researchers are beginning to explore the teacher effect issue in greater depth. For example, Mullens, Murnane, and Willett (1996) investigated teacher effectiveness in teaching mathematics concepts to grade 3 students in Belize.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Ghana’s Complementary Education Program</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ARKORFUL</surname>
                           <given-names>KINGSLEY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>ALTHOUGH THE GOVERNMENT OF GHANA has been investing significantly in formal education, not every child is able to attend a government school. In remote areas schools may not be located near enough for children to reach them safely. Some children may be precluded from attending school in the morning because families need their participation in farming or economic activities. Thus, in Ghana’s Northern Region, the Complementary Education Program was introduced in 1995 to provide learning opportunities for out-of-school children between the ages of eight and fourteen years. The program, called School for Life (SfL), offers a nine-month program cycle in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Language Contact and Language Attitudes in Two Dagara-Speaking Border Communities in Burkina Faso and Ghana</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BEYOGLE</surname>
                           <given-names>RICHARD</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>81</fpage>
                  <abstract>
                     <p>DAGARA IS A GUR LANGUAGE spoken in the northwestern corner of Ghana, the southwestern part of Burkina Faso, and the northeastern part of Côte d’Ivoire (Bodomo 1986). An arbitrary division of the Dagara people into three neighboring West African countries is due to European colonization. From 1920 until today, this arbitrary demarcation has created an officially bilingual anglophone and francophone Dagara community, respectively, in Ghana and Burkina Faso. These political boundaries today represent a major divide and a source of mutual contact influence for the Dagara, who continue to communicate across the borders despite the differences between the official state</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Language and Education Policy in Botswana:</title>
                     <subtitle>The Case of Sebirwa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BOYER</surname>
                           <given-names>ONE TLALE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>ZSIGA</surname>
                           <given-names>ELIZABETH C.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>THIS CHAPTER ADDRESSES the status of the Sebirwa language in Botswana in relation to the country’s language and education policy. We begin with an overview of linguistic demographics in Botswana and then turn to the specific situation of Sebirwa, presenting evidence for its marginalized, and indeed endangered, status. We then discuss how current policies on language and education in Botswana are contributing to the marginalization of all minority languages, including Sebirwa. We conclude with some success stories of language revitalization through education for other minority languages in Botswana and argue for some national-level policy changes that could aid communities in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Ethnic Language Shift among the Nao People of Ethiopia</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SEID</surname>
                           <given-names>SAMSON</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>102</fpage>
                  <abstract>
                     <p>ETHIOPIA IS THE HOME of more than eighty-five linguistically and ethnically distinct groups, most of which have not been scientifically studied or systematically documented. As is true in every multilingual situation, some Ethiopian languages, such as Amharic and Oromiffa, are highly developed and widespread, but some minority languages, such as Nao, are confined to a specific place.</p>
                     <p>The majority of the ethnic groups are almost completely oral cultures with no written literature in their own respective languages. This means that the ethnic groups transmit their indigenous knowledge, wisdom accumulated through ages, core values, and principles underlying their cultures only orally</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>The Role of Language and Culture in Ethnic Identity Maintenance:</title>
                     <subtitle>The Case of the Gujarati Community in South Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SHAH</surname>
                           <given-names>SHEENA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>110</fpage>
                  <abstract>
                     <p>THIS CHAPTER FOCUSES on the importance of the Gujarati language and culture for the ethnic identity development and maintenance of young people living in the Diaspora context of South Africa. Thirty-seven current and former students of a heritagelanguage school in South Africa completed surveys, semistructured interviews, and language tests. Using the participants’ own words, this chapter reports on the views they hold on aspects of their Gujarati identity in a multiethnic and multiracial South Africa. Findings show that they generally identified strongly as Indian. Although proficiency in Gujarati is for the most part low among the participants and they increasingly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>“The Palm Oil with Which Words Are Eaten”:</title>
                     <subtitle>Proverbs from Cameroon’s Endangered Indigenous Languages</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>NJWE</surname>
                           <given-names>EYOVI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>118</fpage>
                  <abstract>
                     <p>CHINUA ACHEBE, a renowned novelist of African descent, figuratively made reference to the undisputed fact of the relevance of proverbs in language when he said that they were “the palm oil with which words are eaten” (Achebe 1958, 14). The importance of proverbs, especially in spoken language as well as written texts, cannot be overemphasized for the color and flavor they introduce and sustain in language. In our study of some endangered Cameroonian languages, we saw the need to investigate and preserve these forms because they tell the stories, experiences, and cultural heritage more vividly than other forms of language</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Linguistic “Glocal” in Nigeria’s Urban Popular Music</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ODEBUNMI</surname>
                           <given-names>TOLULOPE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>THE PHENOMENON OF “WORLD BEAT” has been aptly described as the “Africanization of world pop music and the Americanization of African pop” (Feld 1994, 245). Among other things, it signposts one aspect of the larger flows and interminglings among cultures that are increasingly aware of themselves and of others. Transcultural influences in the realm of World Music are, for the most part, understood as manifested in style, mode, and identity-performance. In this chapter I look at the centrality of language for this hybrid musical culture by considering the linguistic features of pop music of Nigeria, especially the hip-hop music produced</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Language Use in Advertisements as a Reflection of Speakers’ Language Habits</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MUAKA</surname>
                           <given-names>LEONARD</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>137</fpage>
                  <abstract>
                     <p>ADVERTISING AS A FIELD transcends several disciplines. This chapter contributes to this interdisciplinary field from a sociolinguistic perspective by focusing on billboards that form the linguistic landscape of Kenyan and Tanzanian urban centers. Although the field of linguistic landscapes has made major progress in other parts of the world, the East African region and Africa in general are only beginning to see studies on linguistic landscapes emerge. Examples from the region include Higgins (2009), who deals with Tanzania and Kenya; Stroud and Mpendukana (2009) on South Africa; Bwenge (2009) on Tanzania; and Legère (2012) on Tanzania as well. The purpose</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>The Persuasive Nature of Metaphors in Kenya’s Political Discourse</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MUAKA</surname>
                           <given-names>LEONARD</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>158</fpage>
                  <abstract>
                     <p>THE AIM OF THIS CHAPTER is to analyze Kenya’s political discourse with an emphasis on the metaphors politicians use. The chapter uses Raila Arnollo Odinga, a controversial and influential figure in Kenyan politics, as the main focal point in a discussion of Kenya’s political discourse. By focusing on Odinga’s political parables in his campaigns leading to the 2007 and 2013 presidential elections, the chapter identifies the stylistic strategies utilized by politicians to engage and identify with their audiences from different ethnic groups. Quite often, the (mis)interpretations of these metaphors lead to further debates.</p>
                     <p>To examine these debates I invoke a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>African Languages on Film:</title>
                     <subtitle>Visualizations of Pathologized Polyglossia</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PANDEY</surname>
                           <given-names>ANJALI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>171</fpage>
                  <abstract>
                     <p>THIS CHAPTER PRESENTS generous visual evidence of the extent to which cinematic accounts persist in conflating African polyglossia with pathologized indecipherability, linguistic cacophony, and unintelligibility in twenty-first-century transnational encounters keen on privileging linguistic homogeneity. Such linguistic juxtapositions reflect the astute marketing strategies of accrued linguistic capital—a hierarchy of linguistic potential emerging in the world’s linguistic resources (Pandey 2015). In such constructed clines of linguistic “portability” (Canagarajah 2013, 2), African languages remain relegated to scales of mitigated linguistic worth in comparison, for example, with the saliency accorded other spotlighted languages of transactional power and influence.</p>
                     <p>The noted hyperpolyglot Helen Abadzi</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.19</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>193</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt13x0dmp.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>195</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
