<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">bmjbritmedj</journal-id>
         <journal-id journal-id-type="jstor">j50000213</journal-id>
         <journal-title-group>
            <journal-title>BMJ: British Medical Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>British Medical Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">09598138</issn>
         <issn pub-type="epub">17561833</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">29714326</article-id>
         <article-categories>
            <subj-group>
               <subject>Papers</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Stability Of Essential Drugs During Shipment To The Tropics</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>H. V.</given-names>
                  <surname>Hogerzeil</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>A.</given-names>
                  <surname>Battersby</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>V.</given-names>
                  <surname>Srdanovic</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>N. E.</given-names>
                  <surname>Stjernstrom</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>25</day>
            <month>1</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">304</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6821</issue>
         <issue-id>i29714303</issue-id>
         <fpage>210</fpage>
         <lpage>212</lpage>
         <permissions>
            <copyright-statement>Copyright 1992 British Medical Journal</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/29714326"/>
         <abstract>
            <p>Objective—To determine whether present methods of international transport of essential drugs by sea adversely affect their quality. Design—Controlled longitudinal study of drug shipments sent by sea from Unicef in Copenhagen to Lagos; to Mombasa and by land to Kampala; and to Bangkok. 11 essential drugs were stored in four locations on board the ships. Setting—Main shipping routes from Unicef, Copenhagen, to tropical countries. Main outcome measures—Temperature and relative humidity in the test packs during the journey. Amount of active ingredient in the drugs before and after shipment. Results—Temperatures recorded within the test packs range from −3.5°C to 42.4°C and were 3-12°C higher than the ambient temperature. Relative humidity within the packs ranged from 20% to 88%. Differences between the locations on board were negligible. Ergometrine injection, methylergometrine injection, and retinol capsules lost 1.5-5.8% of their activity. Ampoules of ergometrine showed a large variation in the amount of active ingredient after shipment, with three of 80 samples having concentrations 60% below those stated. Ampicillin, benzylpenicillin, phenoxymethylpenicillin, and tetracycline were not affected by transport. Conclusions—Drugs were exposed to a much higher temperature and humidity than is recommended by the manufacturer, especially in tropical harbours and during inland transport. Except for ergometrine and methylergometrine the transport would not affect clinical effectiveness.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d456e168a1310">
            <label>1</label>
            <mixed-citation id="d456e175" publication-type="other">
Essential drugs price list, July-December 1990. Copenhagen: Unicef Supply
Division, 1990.</mixed-citation>
         </ref>
         <ref id="d456e185a1310">
            <label>2</label>
            <mixed-citation id="d456e192" publication-type="other">
WHO Expert Committee on Specifications for Pharmaceutical Preparations.
Thirty first report. WHO Tech Rep Ser 1990:790.</mixed-citation>
         </ref>
         <ref id="d456e202a1310">
            <label>3</label>
            <mixed-citation id="d456e209" publication-type="other">
Smallenbroek H. Stability of drugs in the tropics. Groningen, Netherlands:
Wetenschapswinkel voor Geneesmiddelen, 1983.</mixed-citation>
         </ref>
         <ref id="d456e219a1310">
            <label>4</label>
            <mixed-citation id="d456e226" publication-type="other">
World Health Organisation. Accelerated stability studies of widely used pharma-
ceutical substances under simulated tropical conditions. Geneva: WHO, 1986.
(WHO/PHARM/86.529.)</mixed-citation>
         </ref>
         <ref id="d456e240a1310">
            <label>5</label>
            <mixed-citation id="d456e247" publication-type="other">
York P. The shelf-life of some antibiotic preparations stored under tropical
conditions. Pharmazie 1977;32:101-4.</mixed-citation>
         </ref>
         <ref id="d456e257a1310">
            <label>6</label>
            <mixed-citation id="d456e264" publication-type="other">
Walker GJA, Hogerzeil HV, Hillgren U. Potency of ergometrine in tropical
countries. Lancet 1988;ii:393.</mixed-citation>
         </ref>
         <ref id="d456e274a1310">
            <label>7</label>
            <mixed-citation id="d456e281" publication-type="other">
Abu-Reid IO, El-Samani SA, Hag Orner AI, Khalil NY, Mahgoub KM,
Everitt G, et al. Stability of drugs in the tropics. A study in Sudan.
International Pharmaceutical Journal 1990;4:6-10.</mixed-citation>
         </ref>
         <ref id="d456e294a1310">
            <label>8</label>
            <mixed-citation id="d456e301" publication-type="other">
Longland PW, Rowbotham PC Stability at room temperature of medicines
normally recommended for cold storage. Pharmaceutical Journal 1987;
January: 147-51.</mixed-citation>
         </ref>
         <ref id="d456e314a1310">
            <label>9</label>
            <mixed-citation id="d456e321" publication-type="other">
Hogerzeil HV, Battersby A, Srdanovic V, Hansen LV, Boye O, Lindgren B,
et al. WHO/Unicef study on the stability of drugs during international
transport. Geneva: WHO, 1991. (WHO/DAP/91.1.)</mixed-citation>
         </ref>
         <ref id="d456e334a1310">
            <label>10</label>
            <mixed-citation id="d456e341" publication-type="other">
Becker G, Fritz HE, Mennicke W, Mobers F, Witt M. Temperaturebelastung
von Arzneimitteln an Bord. Pharmazeutische Zeitung 1983;15:794-7.</mixed-citation>
         </ref>
         <ref id="d456e352a1310">
            <label>11</label>
            <mixed-citation id="d456e359" publication-type="other">
World Health Organisation Technical Working Group. The prevention and
management of postpartum haemorrhage. Geneva: WHO, 1990. (WHO/MCH/
90.7.)</mixed-citation>
         </ref>
         <ref id="d456e372a1310">
            <label>12</label>
            <mixed-citation id="d456e379" publication-type="other">
Pawelczyk E, Hermann T, Knitter B, Smilowski B. Kinetics of drug
decomposition. 61. Kinetics of various ampicillin forms degradation in solid
phase. Pol J Pharmacol Pharm 1980;32:47-54.</mixed-citation>
         </ref>
         <ref id="d456e392a1310">
            <label>13</label>
            <mixed-citation id="d456e399" publication-type="other">
Pawelczyk E, Plotkowiak Z, Knitter K, Kozakiewicz-Wegner B. Kinetics of
drug decomposition. 62. Kinetics of penicillin G potassium salt (PGP)
thermal degradation in solid phase. Pol J Pharmacol Pharm 1980;32:55-62.</mixed-citation>
         </ref>
         <ref id="d456e412a1310">
            <label>14</label>
            <mixed-citation id="d456e419" publication-type="other">
Pikal MJ, Lukes AL, Lang JE. Thermal decomposition of amorphous
β-lactam antibacterials. J Pharm Sci 1977;66:1312-6.</mixed-citation>
         </ref>
         <ref id="d456e429a1310">
            <label>15</label>
            <mixed-citation id="d456e436" publication-type="other">
Hogerzeil HV, De Goeje M, Abu-Reid IO. Stability of essential drugs in
Sudan. Lancet 199l;ii:754-5.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
