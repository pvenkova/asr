<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">polity</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100328</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Polity</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Palgrave Macmillan</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00323497</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17441684</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41684503</article-id>
         <article-categories>
            <subj-group>
               <subject>Polity Symposium: Deepening Democracy</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>A Human Rights-Based Approach (HRBA) in Practice: Evaluating NGO Development Efforts</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Hans Peter</given-names>
                  <surname>Schmitz</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">44</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40079209</issue-id>
         <fpage>523</fpage>
         <lpage>541</lpage>
         <permissions>
            <copyright-statement>© 2012 Northeastern Political Science Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1057/pol.2012.18"
                   xlink:title="an external site"/>
         <abstract>
            <p>Human rights-based approaches (HRBAs) promise greater alignment of development efforts with universal norms, as well as a focus on the root causes of poverty. While HRBAs have been widely adopted across the development sector, there is little systematic evidence about the actual impact of this strategic shift. Evaluating the effectiveness of HRBAs is challenging because various non-governmental and other organizations have developed very different understandings of how to apply a rights-based framework in the development context. This essay takes a step toward the rigorous evaluation of HRBAs by offering a comprehensive review of rightsbased programming implemented by Plan International, a child-centered organization. It shows that Plans adoption of HRBA-inspired strategies has transformed its interactions with local communities and added an explicit focus on the state as the primary duty bearer. There is evidence for a systematic increase in individual rights awareness, greater ownership exercised by community organizations, and the application of evidence-based advocacy aimed at scaling up proven program activities. But Plans peculiar brand of HRBA neglects collaboration with domestic social movements and civil society, largely avoids a more confrontational approach towards the state, and has yet to produce evidence for regular successful rights claims by disadvantaged communities against governmental representatives at local, regional, or national levels. The study also reveals a limited ability of Plan to address disparities and discrimination within local communities, as well as a need to define clearly the organizations own accountability and duties deriving from its presence in local communities across more than fifty developing nations.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1606e134a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1606e141" publication-type="other">
William R. Easterly, The White Man's Burden: Why the West's Efforts to Aid the Rest Have Done So
Much III and So Little Good (New York: Penguin Press, 2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e151a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1606e158" publication-type="other">
Sam Hickey and Diana Mitlin, eds., Rights-Based Approaches to Development: Exploring the
Potential and ñtfalls (Sterling, VA: Kumarian Press, 2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e168a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1606e175" publication-type="other">
Peter Uvin, Human Rights and Development (Bloomfield, CT Kumanan Press, 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e182a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1606e189" publication-type="other">
Paul J. Nelson and Ellen Dorsey, New Rights Advocacy: Changing Strategies of Development and
Human Rights NGOs (Washington, DC: Georgetown University Press, 2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e200a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1606e207" publication-type="other">
http://hrbaportal.org/</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e214a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1606e221" publication-type="other">
Laure-Hélène Piron, Learning from the UK Department for International Developments Rights-
Based Approach to Development Assistance (London: Overseas Development Institute, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e231a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1606e238" publication-type="other">
Ellen Dorsey, Mayra Gómez, Bret Thiele, and Paul Nelson, "Millennium Development Rights,"
Monday Developments 29 (2011): 17-18,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e247" publication-type="other">
Malcolm Langford, "A Poverty of Rights: Six Ways to Fix the
MDGs," IDS Bulletin 41 (2010): 83-91.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e257a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1606e264" publication-type="other">
George E. Mitchell, Watchdog Study: Reframing the Discussion about
Nonprofit Effectiveness (Washington, DC: DMA Nonprofit Federation, 2010),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e273" publication-type="other">
Ann Goggins Gregory
and Don Howard, "The Nonprofit Starvation Cycle," Stanford Social Innovation Review 7 (2009): 48-53.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e282" publication-type="other">
Charity Navigator's
methodology section at www.charitynavigator.org and Ken Berger's blog at www.kenscommentary.org/.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e292a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1606e299" publication-type="other">
Paul Gready, "Reasons to Be Cautious about Evidence and Evaluation: Rights-based Approaches
to Development and the Emerging Culture of Evaluation," Journal of Human Rights Practice 1 (2009):
380-401.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e312a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1606e319" publication-type="other">
Habib Mohammad Zafarullah and Mohammad Habibur Rahman, "Human Rights, Civil Society
and Nongovernmental Organizations: The Nexus in Bangladesh," Human Rights Quarterly 24 (2002):
1011-34;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e331" publication-type="other">
Mac Darrow and Amparo Tomas, "Power, Capture, and Conflict: A Call for Human Rights
Accountability in Development Cooperation," Human Rights Quarterly 27 (2005): 471-538;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e340" publication-type="other">
Alessandra
Lundström Sarelin, "Human Rights-Based Approaches to Development Cooperation, HIV/AIDS, and
Food Security," Human Rights Quarterly 29 (2007): 460-88;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e353" publication-type="other">
Benjamin Mason Meier and Ashley M. Fox,
"Development as Health: Employing the Collective Right to Development to Achieve the Goals of the
Individual Right to Health," Human Rights Quarterly 30 (2008): 259-355.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e367a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1606e374" publication-type="other">
Shannon Kindornay, James Ron, and Charli Carpenter, "Rights-Based
Approaches to Development: Implications for NGOs," Human Rights Quarterly 34 (2012): 472-506;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e383" publication-type="other">
Sheena Crawford, The Impact of Rights-Based Approaches to Development: Bangladesh, Malawi and Peru
(London: UK Interagency Group on Human Rights Based Approaches, 2007);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e392" publication-type="other">
Gready, "Reasons to Be
Cautious."</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e402a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1606e409" publication-type="other">
Tosca Bruno-van Vijfeijken, Uwe Gneiting, Hans Beter Schmitz, and Otto Valle, Rights-Based
Approach to Development: Learning from Guatemala (Syracuse, NY: Moynihan Institute of Global Affairs,
2009);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1606e421" publication-type="other">
Tosca Bruno-van Vijfeijken, Uwe Gneiting, and Hans Peter Schmitz, How does CCCD Affect
Program Effectiveness and Sustainability? A Meta Review of Plan's Evaluations (Syracuse, NY: Moynihan
Institute of Global Affairs, 2011).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e434a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1606e441" publication-type="other">
Craig Johnson and Timothy Forsyth, "In the Eyes of the State: Negotiating a 'Rights-Based
Approach' to Forest Conservation in Thailand," World Development 30 (2002): 1591-1605.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e451a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1606e458" publication-type="other">
John Gaventa and Rosemary McGee, Citizen Action and National Policy Reform: Making Change
Happen (London: Zed Books, 2010).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e468a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1606e475" publication-type="other">
Uvin, Human Rights and Development.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e482a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1606e489" publication-type="other">
Ibid., 179.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e497a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1606e504" publication-type="other">
Jennifer Chapman. Rights-based Development: The Challenge of Change and Power (London:
ActionAid International, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e514a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1606e521" publication-type="other">
Päul Gready, "Rights-Based Approaches to Development: What is the Value-Added?" Develop-
ment in Practice 18 (2008): 735-47.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e531a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1606e538" publication-type="other">
Srirak Plipat, Developmentizing Human Rights: How Development NGOs Interpret and Implement
a Human Rights-based Approach to Development Policy (University of Pittsburgh, Doctoral Thesis, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e548a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1606e555" publication-type="other">
Jonathan Menkos, Ignacio Saiz, and Maria Jose Eva, Rights or Privileges? Fiscal Commitment to
the Rights to Health, Education and Food in Guatemala (Madrid/Guatemala City: Instituto Centroamer-
icano de Estudios Fiscales and The Center for Economic and Social Rights, 2009).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e568a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1606e575" publication-type="other">
Irko Zuurmond, ed., Promoting Child Rights to End Child Poverty (Woking, UK: Plan International,
2010).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e585a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1606e592" publication-type="other">
Bruno-van Vijfeijken, Gneiting, and Schmitz, Meta Review, 6.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e600a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1606e607" publication-type="other">
Plan International, Global Effectiveness Framework (Woking, UK: Plan International, 2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e614a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1606e621" publication-type="other">
Hickey and Mitlin, Rights-Based Approaches to Development, 225.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e628a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1606e635" publication-type="other">
Bruno-van Vijfeijken, Gneiting, and Schmitz, Meta Review, 11.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e642a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1606e649" publication-type="other">
Ibid., 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e656a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1606e663" publication-type="other">
Plipat, Deuelopmentizing Human Rights.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e670a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1606e677" publication-type="other">
Sarah Bradshaw, "Is the Rights Focus the Right Focus? Nicaraguan Responses to the Rights
Agenda," Third World Quarterly 27 (2006): 1329-41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e688a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1606e695" publication-type="other">
Emily B. Rodio and Hans Peter Schmitz, "Beyond Norms and Interests: Understanding the
Evolution of Transnational Human Rights Activism," International Journal of Human Rights 14 (2010):
442-59.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e708a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1606e715" publication-type="other">
Plipat, Developmentizing Human Rights.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e722a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1606e729" publication-type="other">
Bruno-van Vijfeijken, Gneiting, Schmitz, and Valle, Learning from Guatemala.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e736a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1606e743" publication-type="other">
Tosca Bruno-van Vijfeijken and Hans Beter Schmitz, "A Gap between Ambition and Effectiveness,"
Journal of Civil Society 7 (2011): 287-92.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e753a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1606e760" publication-type="other">
Jude Rand and Gabrielle Watson, Rights-Based Approaches: Learning Project (Boston/Atlanta:
Oxfam America/CARE USA, 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e770a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1606e777" publication-type="other">
Plan International, Global Effectiveness Framework.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e785a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1606e792" publication-type="other">
Bruno-van Vijfeijken, Gneiting, and Schmitz, Meta Review, 23.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e799a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1606e806" publication-type="other">
Ibid., 26.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e813a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1606e820" publication-type="other">
Anthony Bebbington, "Donor-NGO Relations and Representations of Livelihood in Nongovern-
mental Aid Chains," World Development 33 (2005): 937-50.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e830a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1606e837" publication-type="other">
Bruno-van Vijfeijken, Gneiting, and Schmitz, Meta Review, 40.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e844a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1606e851" publication-type="other">
Crawford, Bangladesh.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e858a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1606e865" publication-type="other">
Bruno-van Vijfeijken, Gneiting, and Schmitz, Meta Review, 32.</mixed-citation>
            </p>
         </fn>
         <fn id="d1606e873a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1606e880" publication-type="other">
Gready, "Reasons to Be Cautious."</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
