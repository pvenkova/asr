<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1hj5858</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Gender and Health</book-title>
         <subtitle>Policy and Practice</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>van der Kwaak</surname>
               <given-names>Anke</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Wegelin-Schuringa</surname>
               <given-names>Madeleen</given-names>
            </name>
         </contrib>
         <role content-type="editor">Guest Editors</role>
      </contrib-group>
      <contrib-group>
         <role content-type="series-editor">Series editors:</role>
         <contrib contrib-type="series-editor" id="contrib3">
            <name name-style="western">
               <surname>Cummings</surname>
               <given-names>Sarah</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib4">
            <name name-style="western">
               <surname>van Dam</surname>
               <given-names>Henk</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib5">
            <name name-style="western">
               <surname>Valk</surname>
               <given-names>Minke</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>12</month>
         <year>2006</year>
      </pub-date>
      <isbn content-type="ppub">9780855985714</isbn>
      <isbn content-type="epub">9780855987268</isbn>
      <publisher>
         <publisher-name>Oxfam</publisher-name>
         <publisher-loc>Oxford, United Kingdom</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2006</copyright-year>
         <copyright-holder>KIT (Royal Tropical Institute)</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1hj5858"/>
      <abstract abstract-type="short">
         <p>An extensive and up-to-date annotated bibliography of international resources (print and online) makes this a truly global sourcebook on strategies, approaches, and tools to mainstream gender-equality concerns in the formulation of health policy and practice.</p>
      </abstract>
      <counts>
         <page-count count="150"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>1</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>5</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.3</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="other" id="contrib1" xlink:type="simple">
                        <string-name>Henk</string-name>
                     </contrib>
                     <contrib contrib-type="other" id="contrib2" xlink:type="simple">
                        <string-name>Sarah</string-name>
                     </contrib>
                     <contrib contrib-type="other" id="contrib3" xlink:type="simple">
                        <string-name>Minke</string-name>
                     </contrib>
                     <role content-type="other">Series Editors</role>
                  </contrib-group>
                  <fpage>7</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.4</book-part-id>
                  <title-group>
                     <title>Acronyms</title>
                  </title-group>
                  <fpage>9</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.5</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Gender and health</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>van der Kwaak</surname>
                           <given-names>Anke</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dasgupta</surname>
                           <given-names>Jashodhara</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>This introduction to the sourcebook ‘Gender and health’ discusses the international context and recent developments in the broad field of gender and health. Gender and health refers to all gender issues in the field of health, health care, suffering and wellbeing. Thus, this book not only refers to public health or reproductive health alone but genuine gender and clinical health issues are also presented. In this introduction, gaps and challenges in this field will be discussed. New approaches to gender and health, taking the complex realities into account, are crucial. Moreover, the combination of a gender perspective and a rights’</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Engendering communicable disease policy and practice:</title>
                     <subtitle>experiences from Malawi</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nhlema</surname>
                           <given-names>Bertha Simwaka</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Makwiza</surname>
                           <given-names>Ireen</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sanudi</surname>
                           <given-names>Lifah</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nkhonjera</surname>
                           <given-names>Patnice</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib5" xlink:type="simple">
                        <name name-style="western">
                           <surname>Theobald</surname>
                           <given-names>Sally</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>This paper synthesizes the findings of research in Malawi to explore how gender roles and relations affect the way in which key diseases of poverty are experienced at the community level. The paper also documents strategies for translating gendered research findings into policy and practice. First, it highlights background information on Malawi and gives an overview of the three key diseases of poverty. Next, it introduces the Research on Equity and Community Health (REACH) Trust and documents its research work on gender and equity in diseases of poverty. This is followed by a conceptual framework on how gender shapes vulnerability</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Tackling tradition:</title>
                     <subtitle>examining successful strategies in the mitigation of female genital mutilation in Ethiopian communities</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Teferra</surname>
                           <given-names>Sehin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>A<italic>seratuma</italic>is a traditional form of meeting by which the Oromo people of Ethiopia determine major decisions affecting the community. In these meetings, the elders of the clan pronounce joint decisions that are often questioned and debated by all present until a general consensus is reached. During one<italic>seratuma</italic>in Arsi Negelle in April 2001, clan elders asked a group of girls who had come to attend the meeting if they thought the practice of female genital mutilation (FGM) that had been practised in the community for generations should be continued. When the question was met with a resounding</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Reaching young people on health and reproductive and sexual rights:</title>
                     <subtitle>the experience of Argentina</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bianco</surname>
                           <given-names>Mabel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>Argentina is a country with a strong Catholic tradition although other churches and religions are recognized. From the early 1970s, family planning activities were forbidden, both in public health services and social security health services. This prohibition was restricted in 1986 by a National Government Decree but, with the change of Government, strong opposition developed once again from 1989 onwards so that such services were once again curtailed (Bianco 1996). In addition, no sex education was provided at school to teenage girls and boys. As a consequence, sex and sexual issues remained taboo in schools and often within families too.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Men, gender equity and HIV/AIDS prevention, with case studies from South Africa and Brazil</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Zoll</surname>
                           <given-names>Miriam</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>At a seminar at New York City’s public library a few years ago, André de Zanger of the Creativity Institute mesmerized his audience by discussing a Soviet-devised problem-solving model called the Theory of Inventive Problem Solving, otherwise known as TRIZ. TRIZ, developed in a resource-poor nation, invites practitioners to invent new ways of using limited resources to solve problems. It encourages analysts and inventors to look for obvious but often hidden solutions within existing systems and structures. The management adage – the problem is part of the solution and the solution is part of the problem – encapsulates the TRIZ</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Local responses to HIV/AIDS from a gender perspective</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wegelin-Schuringa</surname>
                           <given-names>Madeleen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>Between 2002 and 2004, the Royal Tropical Institute (KIT) in Amsterdam has managed the development of a toolkit for local responses to HIV and AIDS for the Joint United Nations Programme on HIV/AIDS (UNAIDS). The aim of the toolkit is to strengthen the capacity of different actors to address HIV/AIDS at local level, to take concrete measures to reduce vulnerability and risk, and to mitigate the impact of the epidemic on affected households and communities.</p>
                     <p>The process of developing the toolkit started with a workshop with partners from Brazil, Burkina Faso, Trinidad and Tobago, Thailand, Uganda and Zambia to design</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Stigma, discrimination and violence amongst female sex workers and men who have sex with men in Andhra Pradesh, India</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Samuels</surname>
                           <given-names>Fiona</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Verma</surname>
                           <given-names>Ravi K.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>George</surname>
                           <given-names>C.K.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>One-third of people living with the human immune deficiency virus (HIV) are in countries that do not yet have generalized epidemics. Even a small increase in infection rates in countries without generalized epidemics, especially populous ones, would represent a dramatic worsening of the global burden of the acquired immune deficiency syndrome (AIDS). Thus, there is an urgent need to reduce HIV infections in relatively low-prevalence countries that are put at risk by the growing HIV pandemic (Ainsworth and Over 1999).</p>
                     <p>In the late 1990s, there were an estimated 4 million HIV-infected individuals in India. By the end of 2003, this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.12</book-part-id>
                  <title-group>
                     <title>Annotated bibliography</title>
                  </title-group>
                  <fpage>111</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.13</book-part-id>
                  <title-group>
                     <title>Author index</title>
                  </title-group>
                  <fpage>165</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.14</book-part-id>
                  <title-group>
                     <title>Subject index</title>
                  </title-group>
                  <fpage>169</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.15</book-part-id>
                  <title-group>
                     <title>Geographical index</title>
                  </title-group>
                  <fpage>171</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.16</book-part-id>
                  <title-group>
                     <title>Web resources</title>
                  </title-group>
                  <fpage>173</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hj5858.17</book-part-id>
                  <title-group>
                     <title>About the authors</title>
                  </title-group>
                  <fpage>180</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
