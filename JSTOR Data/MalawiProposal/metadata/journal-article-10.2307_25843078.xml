<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">zeitethn</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50002948</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Zeitschrift für Ethnologie</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Dietrich Reimer Verlag</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00442666</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25843078</article-id>
         <title-group>
            <article-title>Umstrittene Traditionen in Marokko und Indonesien</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Franz</given-names>
                  <surname>von Benda-Beckmann</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Keebet</given-names>
                  <surname>von Benda-Beckmann</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bertram</given-names>
                  <surname>Turner</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">132</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25843074</issue-id>
         <fpage>15</fpage>
         <lpage>35</lpage>
         <permissions>
            <copyright-statement>© 2007 Dietrich Reimer Verlag GmbH</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25843078"/>
         <abstract>
            <p>In the current era of globalization and transnationalisation of law, the continued vitality and revitalisation of tradition in legal systems seems paradoxical. Common explanations consider the renewed emphasis on tradition in law and religion as an indication of the failure or the imminent breakdown of the state or see it primarily as a rejection of dominant globalisation processes. In both approaches, tradition primarily figures as a relic from the past and attempts to revitalise it are seen as inward and backward looking. We argue that these explanations are unsatisfactory. Based on our research findings in Indonesia and Morocco, we argue, that a wide range of actors — from global players to local people — refers to tradition as a strategic tool in the legal repertoire in order to bring forward their respective interests in very different fields ranging from development goals such as sustainability, natural protection and good governance, to absolute hegemony in the religious field. Not conservatism but discontent with the distribution of resources and power motivate actors to look for alternative legitimisation of their claims.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>ger</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d323e138a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d323e145" publication-type="other">
F. von Benda-Beckmann 1994</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e151" publication-type="other">
2002a</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e157" publication-type="other">
K. von Benda-Beckmann
2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e167" publication-type="other">
Turner 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e174a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d323e181" publication-type="other">
Kuppe 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e188a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d323e195" publication-type="other">
van Rouveroy van Nieuwaal und Ray 1996</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e201" publication-type="other">
Oomen 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e208a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d323e215" publication-type="other">
van Klinken 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d323e223a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d323e230" publication-type="other">
Pirie 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e237a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d323e244" publication-type="other">
Esposito und Watson 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e250" publication-type="other">
Ahdar 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e256" publication-type="other">
Turner 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e263a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d323e270" publication-type="other">
F. von Benda-Beckmann, K. von Benda-Beckmann und A. Griffiths 2005.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e276" publication-type="other">
Li 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e282" publication-type="other">
Randeria 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e289" publication-type="other">
2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e295" publication-type="other">
Weilenmann 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e302a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d323e309" publication-type="other">
Rata 2005</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e315" publication-type="other">
Rdsel und von Tro-
tha 1999.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e325a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d323e332" publication-type="other">
F. von Benda-Beckmann et al. 2003a</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e338" publication-type="other">
2003b</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e344" publication-type="other">
F. von
Benda-Beckmann, K. von Benda-Beckmann and Turner 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e354a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d323e361" publication-type="other">
Asad 1973</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e367" publication-type="other">
Said 1978</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e373" publication-type="other">
Hobsbawm und Ranger 1983.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e380" publication-type="other">
Gammer 1973</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e386" publication-type="other">
Spittler 1980</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e392" publication-type="other">
Roberts 1984</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e398" publication-type="other">
F. and K. von Benda-Beckmann 1985</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e404" publication-type="other">
Chanock 1985</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e410" publication-type="other">
Zerner 1994</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e417" publication-type="other">
F.
and K. von Benda-Beckmann and Brouwer 1995</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e426" publication-type="other">
Turner 2005a.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e434a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d323e441" publication-type="other">
Spittler 1980</mixed-citation>
            </p>
         </fn>
         <fn id="d323e448a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d323e455" publication-type="other">
van Vollenhoven 1909</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e461" publication-type="other">
Holleman 1938.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e467" publication-type="other">
Woodman's (1987)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e474" publication-type="other">
Lafuente 1999.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e481a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d323e488" publication-type="other">
Rosen 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e494" publication-type="other">
Turner 2001.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e501a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d323e508" publication-type="other">
Wuerth 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e515a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d323e522" publication-type="other">
F. and K. von Benda-Beckmann 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e528" publication-type="other">
2006a</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e534" publication-type="other">
b</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e541" publication-type="other">
F. von Benda-Beckmann 2002b.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e548a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d323e555" publication-type="other">
Acciaioli 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e561" publication-type="other">
Li 2001.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e567" publication-type="other">
F. von Benda-Beckmann 1979</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e574" publication-type="other">
K. von Benda-Beckmann 1984</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e580" publication-type="other">
Kahn 1993.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e588a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d323e595" publication-type="other">
Beyer 2005</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e601" publication-type="other">
2006</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e607" publication-type="other">
Eckert 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e614" publication-type="other">
Klute 2004.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e621a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d323e628" publication-type="other">
Zerner 1994</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e634" publication-type="other">
F. and
K. von Benda-Beckmann and Brouwer 1995.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e644a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d323e651" publication-type="other">
Eckert 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d323e658a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d323e665" publication-type="other">
Kuper 2003</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e671" publication-type="other">
Zips 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d323e678a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d323e685" publication-type="other">
Konigs, Mohammed VI. von Juli 2003: http://
www.mincom.gov.ma/french/generalites/samajeste/mohammedVI/discours/2003/Discours%20-
du%20Trone%202003.htm</mixed-citation>
            </p>
         </fn>
         <fn id="d323e698a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d323e705" publication-type="other">
Esposito und Watson 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d323e711" publication-type="other">
Ahdar 2000.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>Literatur</title>
         <ref id="d323e727a1310">
            <mixed-citation id="d323e731" publication-type="other">
Acciaioli, Greg 2000: The Re-emergence of Customary Claims to Land among the To Lindu of Cent-
ral Sulawesi: The Revitalisation of Adat in the Era of Reformasi in Indonesia. Paper, Annual Con-
ference of the Australian Anthropological Society, The University of Western Australia, Perth,
19-23 September.</mixed-citation>
         </ref>
         <ref id="d323e747a1310">
            <mixed-citation id="d323e751" publication-type="other">
Ahdar, Rex (ed.) 2000: Law and Religion. Aldershot: Ashgate.</mixed-citation>
         </ref>
         <ref id="d323e758a1310">
            <mixed-citation id="d323e762" publication-type="other">
Asad, Talal (ed.) 1973: Anthropology and the Colonial Encounter. Atlantic Highlands: Humanities Press.</mixed-citation>
         </ref>
         <ref id="d323e769a1310">
            <mixed-citation id="d323e773" publication-type="other">
Benda-Beckmann, Franz von 1994: Rechtspluralismus: Analytische Begriffsbildung oder politisch-
ideologisches Programm? Zeitschrift fur Ethnologie 119:1 -16.</mixed-citation>
         </ref>
         <ref id="d323e784a1310">
            <mixed-citation id="d323e788" publication-type="other">
Benda-Beckmann, Franz von 2002a: Who's afraid of legal pluralism? journal of Legal Pluralism
47:37-82.</mixed-citation>
         </ref>
         <ref id="d323e798a1310">
            <mixed-citation id="d323e802" publication-type="other">
Benda-Beckmann, Franz von 2002b: Verfassungsrechtspluralismus in West Sumatra: Veranderungen
in staatlicher und dorflicher Verfassung im Zuge der Dezentralisierung in Indonesien. Verfassung
und Recht in Ubersee 35:494-512.</mixed-citation>
         </ref>
         <ref id="d323e815a1310">
            <mixed-citation id="d323e819" publication-type="other">
Benda-Beckmann, Keebet von 1984: The Broken Stairways to Consensus: Village Justice and State
Courts in Minangkahau. Dordrecht, Leiden, Cinnaminson: Foris Publications, KITLV Press.</mixed-citation>
         </ref>
         <ref id="d323e829a1310">
            <mixed-citation id="d323e833" publication-type="other">
Benda-Beckmann, Keebet von 1997: Environmental Protection and Human Rights of Indigenous
Peoples: A Tricky Alliance. In: Franz von Benda-Beckmann, Keebet von Benda-Beckmann and
Andre Hoekema (eds.), Natural Resources, Environment and Legal Pluralism. Special issue of Year-
hook Law and Anthropology 9:302-323.</mixed-citation>
         </ref>
         <ref id="d323e849a1310">
            <mixed-citation id="d323e853" publication-type="other">
Benda-Beckmann, Keebet von 2001: Transnational Dimensions of Legal Pluralism. In: Wolfgang Fi-
kentscher (ed.), Begegnung und Konflikt — Eine kulturanthropologische Bestandsaufhahme. Miin-
chen: C.H. Beck Verlag, pp. 33-48.</mixed-citation>
         </ref>
         <ref id="d323e866a1310">
            <mixed-citation id="d323e870" publication-type="other">
Benda-Beckmann, Franz von and Benda-Beckmann, Keebet von 1985: Transformation and Change
in Minangkabau. In: Lynn L. Thomas and Franz von Benda-Beckmann (eds.), Change and Con-
tinuity in Minangkabau. Monographs in International Studies, Southeast Asia Series Number 71.
Athens, Ohio: Ohio University Center for International Studies, Center for Southeast Asian
Studies, pp. 235-278.</mixed-citation>
         </ref>
         <ref id="d323e890a1310">
            <mixed-citation id="d323e894" publication-type="other">
Benda-Beckmann, Franz von and Benda-Beckmann, Keebet von 2001: Actualising History for Bin-
ding the Future: Decentralisation in Minangkabau. In: Paul Hebinck and Gerard Verschoor
(eds.), Resonances and Dissonances in Development: Actors, Networks and Cultural Repertoires. As-
sen: Van Gorcum, pp. 33-47.</mixed-citation>
         </ref>
         <ref id="d323e910a1310">
            <mixed-citation id="d323e914" publication-type="other">
Benda-Beckmann, Franz von and Benda-Beckmann, Keebet von 2006a: How Communal is Commu-
nal and Whose Communal is it? Lessons from Minangkabau. In: Franz von Benda-Beckmann,
Keebet von Benda-Beckmann and Melanie G. Wiber (eds.), Changing Properties of Property. Ox-
ford: Berghahn, pp. 194-217.</mixed-citation>
         </ref>
         <ref id="d323e930a1310">
            <mixed-citation id="d323e934" publication-type="other">
Benda-Beckmann, Franz and Keebet von 2006b: Changing one is changing all: Dynamics in the
Adat-Islam-State triangle. In: F. and K. von Benda-Beckmann eds. Dynamics of Plural Legal
Orders. Special Issue of the Journal of Legal Pluralism 53/54, pp. 239-270.</mixed-citation>
         </ref>
         <ref id="d323e947a1310">
            <mixed-citation id="d323e951" publication-type="other">
Benda-Beckmann, Franz von, Benda-Beckmann, Keebet von and Brouwer, Arie 1995: Changing 'In-
digenous Environmental Law' in the Central Moluccas: Communal Regulation and Privatization
of Sasi. Ekonesia 2 (March 1995):l-38.</mixed-citation>
         </ref>
         <ref id="d323e964a1310">
            <mixed-citation id="d323e968" publication-type="other">
Benda-Beckmann, Franz von, Benda-Beckmann, Keebet von and Turner, Bertram 2003a: Die Revita-
lisierung von Tradition im Recht: Riickfall in die Vergangenheit oder zeitgemafie Entwicklung?
In: Max Planck Gesellschaft (ed.), Jahrbuch 2003. Miinchen: K.G. Saur, pp. 299-207.</mixed-citation>
         </ref>
         <ref id="d323e981a1310">
            <mixed-citation id="d323e985" publication-type="other">
Benda-Beckmann, Franz von, Benda-Beckmann, Keebet von, Eckert, Julia, Pirie, Fernanda and Tur-
ner, Bertram 2003b: Vitality and Revitalisation of Tradition in Law: Going back to the Past or
Future-oriented Development? In: Max Planck Institute for Social Anthropology (ed.), Report
2002-2003. Halle/Saale: druck-medienverlag, pp. 296-306.</mixed-citation>
         </ref>
         <ref id="d323e1002a1310">
            <mixed-citation id="d323e1006" publication-type="other">
Benda-Beckmann, Franz von, Benda-Beckmann, Keebet von and Griffiths, Anne (eds.) 2005: Mobile
People, Mobile Law: Expanding Legal Relations in a Contracting World. Aldershot: Ashgate.</mixed-citation>
         </ref>
         <ref id="d323e1016a1310">
            <mixed-citation id="d323e1020" publication-type="other">
Benda-Beckmann, Franz von, Benda-Beckmann, Keebet von and Turner, Bertram 2005: Die Revita-
lisierung von Tradition im Recht: Riickfall in die Vergangenheit oder zeitgemafie Entwicklung?
Juridikum. Zeitschrift im Rechtsstaat 4:197 -201.</mixed-citation>
         </ref>
         <ref id="d323e1033a1310">
            <mixed-citation id="d323e1037" publication-type="other">
Beyer, Judith 2005: Die Aksakal-Gerichte in Kirgistan: Entwicklung und aktuelle Situation einer tra-
ditionellen Rechtsinstitution. In: Michael Kemper and Maurus Reinkowski (eds.), Rechtspluralis-
mus in der Islamischen Welt. Gewohnheitsrecht zwischen Staat und Gesellschaft. Berlin und New
York: Walter de Gruyter, pp. 343-358.</mixed-citation>
         </ref>
         <ref id="d323e1053a1310">
            <mixed-citation id="d323e1057" publication-type="other">
Beyer, Judith 2006: Revitalisation, Invention and Continued Existence of the Kyrgyz Aksakal courts.
In: F. and K. von Benda-Beckmann (eds.), Dynamics of Plural Legal Orders. Special Issue of the
Journal of Legal Pluralism 53/54, pp. 141-176.</mixed-citation>
         </ref>
         <ref id="d323e1070a1310">
            <mixed-citation id="d323e1074" publication-type="other">
Chanock, Martin 1985: Law, Custom and Social Order: The Colonial Experience in Malawi and Zam-
bia. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d323e1084a1310">
            <mixed-citation id="d323e1088" publication-type="other">
Clammer, John 1973: Colonialism and the Perception of Tradition in Fiji. In: Talal Asad (ed.), Anth-
ropology and the Colonial Encounter. Atlantic Highlands, N.J.: Humanities Press, pp. 199-220.</mixed-citation>
         </ref>
         <ref id="d323e1099a1310">
            <mixed-citation id="d323e1103" publication-type="other">
Eckert, Julia 2003: The Charisma of Direct Action: Power, Politics and the Shiv Sena. Delhi: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d323e1113a1310">
            <mixed-citation id="d323e1117" publication-type="other">
Esposito, John and Watson, Michael (eds.) 2000: Religion and Global Order. Cardiff: University of
Wales Press.</mixed-citation>
         </ref>
         <ref id="d323e1127a1310">
            <mixed-citation id="d323e1131" publication-type="other">
Giordano, Christian 1996: The Past in the Present: Actualised History in the Social Construction of
Reality. In: Don Kalb, Hans Marks and Herman Tak (eds.), Historical Anthropology: The Un-
waged Debate. Utrecht: Stichting Focaal, vol. 25/27pp. 97-107.</mixed-citation>
         </ref>
         <ref id="d323e1144a1310">
            <mixed-citation id="d323e1148" publication-type="other">
Hobsbawm, Eric J. and Ranger, Terence O. 1983: The Invention of Tradition. Cambridge: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d323e1158a1310">
            <mixed-citation id="d323e1162" publication-type="other">
Holleman, Frederik D. 1938: Mr. B. ter Haar Bzn. s Rede 'Het Adatprivaatrecht van Nederlandsch-
Indie in Wetenschap, Practijk en Onderwijs', besproken door Holleman. Indisch Tijdschrift voor
het Recht 147(3):428-440.</mixed-citation>
         </ref>
         <ref id="d323e1175a1310">
            <mixed-citation id="d323e1179" publication-type="other">
Kahn, Joel S. 1993: Constituting the Minangkabau: Peasants, Culture, and Modernity in Colonial Indo-
nesia. Providence, Oxford: Berg.</mixed-citation>
         </ref>
         <ref id="d323e1190a1310">
            <mixed-citation id="d323e1194" publication-type="other">
Klinken, Gerry van 2007: Return of the Sultans: The Communitarian Turn in local Politics. In: Jamie
S. Davidson and David Henley (eds.), The Revival of Tradition in Indonesian Politics: The Deve-
lopment of Adat from Colonialism to Indigenism. London: Roudedge, pp. 149-169.</mixed-citation>
         </ref>
         <ref id="d323e1207a1310">
            <mixed-citation id="d323e1211" publication-type="other">
Klute, Georg 2004: Formen der Streitregelung jenseits des Staates. In: Julia Eckert (ed.), Anthropologie
der Konflikte: Georg Elwerts konflikttheoretische Thesen in der Diskussion. Bielefeld: transcript,
pp. 298-314.</mixed-citation>
         </ref>
         <ref id="d323e1224a1310">
            <mixed-citation id="d323e1228" publication-type="other">
Kuper, Adam 2003: The Return of the Native. Current Anthropology 44(3):389-402.</mixed-citation>
         </ref>
         <ref id="d323e1235a1310">
            <mixed-citation id="d323e1239" publication-type="other">
Kuppe, Rene* (ed.) 2005: Indigenous Peoples, Constitutional States and Treaties and other Constructive
Arrangements between Indigenous Peoples and States. Leiden: Martinus Nijhoff.</mixed-citation>
         </ref>
         <ref id="d323e1249a1310">
            <mixed-citation id="d323e1253" publication-type="other">
Lafuente, Gilles 1999: La Politique Berbfre de la France et le Nationalisme Marocain. Paris: L'Harmattan.</mixed-citation>
         </ref>
         <ref id="d323e1260a1310">
            <mixed-citation id="d323e1264" publication-type="other">
Li, Tania Murray 2001: Masyarakat Adat, Difference, and the Limits of Recognition in Indonesia's
Forest Zone. Modern Asian Studies 35:645-76.</mixed-citation>
         </ref>
         <ref id="d323e1275a1310">
            <mixed-citation id="d323e1279" publication-type="other">
Marcy, Georges 1954: Le Probleme du Droit Coutumier. Revue Alger.y Tunis, et Maroc. de Legisl. et de
Jurispr. :1 -44.</mixed-citation>
         </ref>
         <ref id="d323e1289a1310">
            <mixed-citation id="d323e1293" publication-type="other">
Merry, Sally E. 1982: The Social Organization of Mediation in Non-industrial Societies: Implications
for Informal Community Justice in America. In: Richard L. Abel (ed.), The Politics of Informal
Justice. Vol. 2. New York: Academic Press, pp. 17-45.</mixed-citation>
         </ref>
         <ref id="d323e1306a1310">
            <mixed-citation id="d323e1310" publication-type="other">
Nader, Laura 1996: Civilization and its Negotiations. In: Pat Caplan (ed.), Understanding Disputes:
The Politics of Argument. Oxford, New York: Berg, pp. 39—63.</mixed-citation>
         </ref>
         <ref id="d323e1320a1310">
            <mixed-citation id="d323e1324" publication-type="other">
Oomen, Barbara 2005: McTradition in the New South Africa: Commodifled Custom and Rights
Talk with the Bafokeng and the Bapedi. In: Franz von Benda-Beckmann, Keebet von Benda-
Beckmann and Anne Griffiths (eds.), Mobile People, Mobile Law: Expanding Legal Relations in a
Contracting World. Aldershot: Ashgate, pp. 91-109.</mixed-citation>
         </ref>
         <ref id="d323e1340a1310">
            <mixed-citation id="d323e1344" publication-type="other">
Pirie, Fernanda 2006: Legal Autonomy a Political Engagement: The Ladakhi Village in the Wider
World. Law &amp; Society Review 40(1):77-103.</mixed-citation>
         </ref>
         <ref id="d323e1354a1310">
            <mixed-citation id="d323e1358" publication-type="other">
Randeria, Shalini 2001: Local Refractions of Global Governance: Legal Plurality, International Institu-
tions, the Post-colonial State and NGOs in India. Habilitationsschrift, FB Politik und Sozialwissen-
schaften, FU Berlin.</mixed-citation>
         </ref>
         <ref id="d323e1372a1310">
            <mixed-citation id="d323e1376" publication-type="other">
Randeria, Shalini 2003: Glocalization of Law: Environmental Justice, World Bank, NGOs and the
Cunning State in India. Special issue of Current Sociology 51:305-328.</mixed-citation>
         </ref>
         <ref id="d323e1386a1310">
            <mixed-citation id="d323e1390" publication-type="other">
Rata, Elisabeth 2005: Rethinking Biculturalism. Anthropological Theory 5(3):267-284.</mixed-citation>
         </ref>
         <ref id="d323e1397a1310">
            <mixed-citation id="d323e1401" publication-type="other">
Roberts, Simon 1984: Some Notes on African Customary Law. In: Simon Roberts (ed.), The Construc-
tion and Transformation of African Customary Law. Special issue of the Journal of African Law 28:1 - 5.</mixed-citation>
         </ref>
         <ref id="d323e1411a1310">
            <mixed-citation id="d323e1415" publication-type="other">
Roque, Maria-Angels (ed.) 2004: La Sociiti Civile au Maroc. Barcelona: IEMed/Publisud.</mixed-citation>
         </ref>
         <ref id="d323e1422a1310">
            <mixed-citation id="d323e1426" publication-type="other">
Rosel, Jakob and Trotha, Trutz von (eds.) 1999: Dezentralisierung, Demokratisierung und die lokale
Representation des Staates. Koln: Rudiger Koppe Verlag.</mixed-citation>
         </ref>
         <ref id="d323e1436a1310">
            <mixed-citation id="d323e1440" publication-type="other">
Rosen, Lawrence 2000: The Justice of Islam. Comparative Perspectives on Islamic Law and Society. Ox-
ford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d323e1451a1310">
            <mixed-citation id="d323e1455" publication-type="other">
Rouveroy van Nieuwaal, Emile A.B. van and Ray, Donald I. (eds.) 1996: The New Relevance of Tradi-
tional Authorities to Africds Future. Special Issue of the Journal of Legal Pluralism No. 37-38.</mixed-citation>
         </ref>
         <ref id="d323e1465a1310">
            <mixed-citation id="d323e1469" publication-type="other">
Said, Edward W. 1978: Orientalism: Western Conceptions of the Orient. Harmondsworth: Penguin Books.</mixed-citation>
         </ref>
         <ref id="d323e1476a1310">
            <mixed-citation id="d323e1480" publication-type="other">
Spittler, Gerd 1980: Streitregelung im Schatten des Leviathans. Zeitschriftfur Rechtssoziologie I A-32.</mixed-citation>
         </ref>
         <ref id="d323e1487a1310">
            <mixed-citation id="d323e1491" publication-type="other">
Tsing, Anna L. 2000: The Global Situation. Cultural Anthropology 15:327-360.</mixed-citation>
         </ref>
         <ref id="d323e1498a1310">
            <mixed-citation id="d323e1502" publication-type="other">
Turner, Bertram 2001: Die Persistenz traditioneller Konfliktregelungsverfahren im Souss; Marokko.
In: Wolfgang Fikentscher (ed.), Begegnung und Konflikt — Eine kulturanthropologische Bestandsauf-
nahme. Munchen: C.H. Beck Verlag, pp. 187-202.</mixed-citation>
         </ref>
         <ref id="d323e1515a1310">
            <mixed-citation id="d323e1519" publication-type="other">
Turner, Bertram 2004: Rechtspluralismus in Deutschland - Das Dilemma von “6ffentlicher Wahr-
nehmung" und alltaglicher Rechtspraxis. In: Ursula Bertels, Birgit Baumann, Silke Dinkel and
Irmgard Hellmann (eds.), Aus der Feme in die Nahe - Neue Wege der Ethnologie in die Offentlich-
keit. Minister: Waxmann, pp. 155-183.</mixed-citation>
         </ref>
         <ref id="d323e1536a1310">
            <mixed-citation id="d323e1540" publication-type="other">
Turner, Bertram 2005a: Asyl und Konflikt. Von der Antike bis heute. Berlin: Reimer.</mixed-citation>
         </ref>
         <ref id="d323e1547a1310">
            <mixed-citation id="d323e1551" publication-type="other">
Turner, Bertram 2005b: Der Wald im Dickicht der Gesetze: Transnationales Recht und lokale Rechts-
praxis im Arganwald (Marokko). Entwicklungsethnologie 14(1+2): 97-117.</mixed-citation>
         </ref>
         <ref id="d323e1561a1310">
            <mixed-citation id="d323e1565" publication-type="other">
Turner, Bertram 2006a: The Legal Arena as a Battlefield: Salaflyya Intervention and Local Response
in Rural Morocco. In: Hans-Jorg Albrecht, Jan Simon, Hassan Rezaei, Holger-Christoph Rohne
and Ernesto Kiza (eds.), Conflicts and Conflict Resolution in Middle Eastern Societies - Between
Tradition and Modernity. Berlin: Duncker &amp; Humblot, pp. 185-196.</mixed-citation>
         </ref>
         <ref id="d323e1581a1310">
            <mixed-citation id="d323e1585" publication-type="other">
Turner, Bertram 2006b: Competing Global Players in Rural Morocco: Upgrading Legal Arenas. In: F.
and K. von Benda-Beckmann (eds.), Dynamics of Plural Legal Orders. Special Issue of the Journal
of Legal Pluralism 53/54, pp. 101-139.</mixed-citation>
         </ref>
         <ref id="d323e1598a1310">
            <mixed-citation id="d323e1602" publication-type="other">
UNDP. 2001. Good Governance for the People Involving the People: Partnership for Governance Reform.
Jakarta: Partnership for Governance Reform Secretariat.</mixed-citation>
         </ref>
         <ref id="d323e1612a1310">
            <mixed-citation id="d323e1616" publication-type="other">
Vollenhoven, Cornelis van 1909: Miskenningen van het Adatrecht. Leiden: Brill.</mixed-citation>
         </ref>
         <ref id="d323e1624a1310">
            <mixed-citation id="d323e1628" publication-type="other">
Weilenmann, Markus 2005: Project Law - Normative Orders of Bilateral Development Cooperation
and Social Change: A Case Study from the German Agency for Technical Cooperation. In: Franz
von Benda-Beckmann, Keebet von Benda-Beckmann and Anne Griffiths (eds.), Mobile People,
Mobile Law. Expanding Legal Relations in a Contracting World. Aldershot: Ashgate, pp. 233-255.</mixed-citation>
         </ref>
         <ref id="d323e1644a1310">
            <mixed-citation id="d323e1648" publication-type="other">
Wiener, Jarrod 1999: Globalization and the Harmonization of Law. London: Pinter.</mixed-citation>
         </ref>
         <ref id="d323e1655a1310">
            <mixed-citation id="d323e1659" publication-type="other">
Woodman, Gordon R. 1987: How State Courts Create Customary Law in Ghana and Nigeria. In:
Bradford W. Morse and Gordon R. Woodman (eds.), Indigenous Law and the State. Dordrecht:
Foris, pp. 181-220.</mixed-citation>
         </ref>
         <ref id="d323e1672a1310">
            <mixed-citation id="d323e1676" publication-type="other">
Wuerth, Oriana 2005: The Reform of the Moudawana: The Role of Women's Civil Society Organi-
zations in Changing the Personal Status Code in Morocco. Hawwa 3(3):309-333.</mixed-citation>
         </ref>
         <ref id="d323e1686a1310">
            <mixed-citation id="d323e1690" publication-type="other">
Zerner, Charles 1994: Through a Green Lens: The Construction of Customary Environmental Law
and Community in Indonesia's Maluku Islands. Special issue of Law and Society Review
28(5):1079-1122.</mixed-citation>
         </ref>
         <ref id="d323e1703a1310">
            <mixed-citation id="d323e1707" publication-type="other">
Zips, Werner 2006: Discussion: The Concept of Indigeneity: Social Anthropology l4(l):27-29.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
