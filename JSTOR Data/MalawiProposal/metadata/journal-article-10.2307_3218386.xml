<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">brittonia</journal-id>
         <journal-id journal-id-type="jstor">j100006</journal-id>
         <journal-title-group>
            <journal-title>Brittonia</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>New York Botanical Garden</publisher-name>
         </publisher>
         <issn pub-type="ppub">0007196X</issn>
         <issn pub-type="epub">1938436X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3218386</article-id>
         <title-group>
            <article-title>Lipochaeta and Melanthera (Asteraceae: Heliantheae Subtribe Ecliptinae): Establishing Their Natural Limits and a Synopsis</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Warren L.</given-names>
                  <surname>Wagner</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Harold</given-names>
                  <surname>Robinson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>2001</year>
         
            <day>1</day>
            <month>12</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">53</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i200986</issue-id>
         <fpage>539</fpage>
         <lpage>561</lpage>
         <page-range>539-561</page-range>
         <permissions>
            <copyright-statement>Copyright 2001 The New York Botanical Garden</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3218386"/>
         <abstract>
            <p>We restrict the genus Lipochaeta to the allopolyploid species of the typical section. Lipochaeta s.str. is interpreted to be the result of an intergeneric hybridization between Melanthera and a presently unknown taxon, perhaps of the genus Wedelia. Lipochaeta is characterized, in addition to its allopolyploidy (n = 26), by having both flavonols and flavones, disk corollas with 4 lobes, achenes tuberculate at maturity, the disk achenes flattened to slightly biconvex, and ray achenes ob-compressed. Lipochaeta sect. Aphanopappus and Wollastonia are here reduced to synonymy under Melanthera. We transfer 14 Hawaiian Lipochaeta and one New Caledonian species as well as the Asian Wedelia prostrata to Melanthera. These transfers, along with the species in Africa and North America, bring the number of species in the genus to 35. Melanthera is delimited by an abruptly narrowed to truncate and flattened top of the achene, (0-)1-15(-20) often unequal, ciliate or barbellate, caducous pappus bristles immediately surrounding the corolla, involucral bracts and receptacular paleae with many veins forming longitudinal striations, and n = 15. The florets are 5-merous, the corollas are yellow or white, and rays are absent (in white-flowered species) or present and neutral or fertile. In dealing with species formerly placed in Lipochaeta, the Galapagos L. laricifolia is here transferred from the illegitimate generic name Macraea to Trigonopterum and the Brazilian L. goyazensis is transferred to Angelphytum. We maintain the earlier reduction of Echinocephalum under Melanthera and reduce all three taxa originally described in it to one, M. latifolia.</p>
         </abstract>
         <kwd-group>
            <kwd>Asteraceae</kwd>
            <kwd>Compositae</kwd>
            <kwd>Heliantheae</kwd>
            <kwd>Ecliptinae</kwd>
            <kwd>Echinocephalum</kwd>
            <kwd>Lipochaeta</kwd>
            <kwd>Macraea</kwd>
            <kwd>Melanthera</kwd>
            <kwd>Trigonopterum</kwd>
            <kwd>Wedelia</kwd>
            <kwd>Wollastonia</kwd>
            <kwd>Hawaiian Islands</kwd>
            <kwd>Pacific Islands</kwd>
            <kwd>New World</kwd>
            <kwd>Africa</kwd>
            <kwd>Asia</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d64e231a1310">
            <mixed-citation id="d64e235" publication-type="journal">
Becker, K. M.1979. A monograph of the genus Las-
ianthaea (Asteraceae). Mem. New York Bot. Gard.
31(2): 1-64.<person-group>
                  <string-name>
                     <surname>Becker</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>1</fpage>
               <volume>31</volume>
               <source>Mem. New York Bot. Gard.</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d64e273a1310">
            <mixed-citation id="d64e277" publication-type="journal">
Bentham, G.1873. Compositae. In: Benthanm, G. and
J. D. Hooker, Genera Plantarum. 2(1): 163-533.<person-group>
                  <string-name>
                     <surname>Bentham</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>163</fpage>
               <volume>2</volume>
               <source>Genera Plantarum</source>
               <year>1873</year>
            </mixed-citation>
         </ref>
         <ref id="d64e312a1310">
            <mixed-citation id="d64e316" publication-type="journal">
Cabrera, A. L.1970. Novedades sinanterol6gicas en-
trerrianas. Darwiniana16: 409-411.<person-group>
                  <string-name>
                     <surname>Cabrera</surname>
                  </string-name>
               </person-group>
               <fpage>409</fpage>
               <volume>16</volume>
               <source>Darwiniana</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d64e348a1310">
            <mixed-citation id="d64e352" publication-type="journal">
—1974. 158. Compositae. Flora Ilustrada de En-
tre Rios (Argentina)6: 106-538.<person-group>
                  <string-name>
                     <surname>Cabrera</surname>
                  </string-name>
               </person-group>
               <fpage>106</fpage>
               <volume>6</volume>
               <source>Flora Ilustrada de Entre Rios (Argentina)</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d64e385a1310">
            <mixed-citation id="d64e389" publication-type="journal">
Candolle, A. P. de. 1836. Compositae. Prodr.5: 4-
706.<person-group>
                  <string-name>
                     <surname>Candolle</surname>
                  </string-name>
               </person-group>
               <fpage>4</fpage>
               <volume>5</volume>
               <source>Compositae. Prodr.</source>
               <year>1836</year>
            </mixed-citation>
         </ref>
         <ref id="d64e421a1310">
            <mixed-citation id="d64e425" publication-type="journal">
Carr, G.1978. Chromosome numbers of Hawaiian
flowering plants and the significance of cytology in
selected taxa. Amer. J. Bot.65: 236-242.<object-id pub-id-type="doi">10.2307/2442457</object-id>
               <fpage>236</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e444a1310">
            <mixed-citation id="d64e448" publication-type="journal">
Chumley, T. W., J. L. Panero, S. C. Keeley &amp; R.
K. Jansen. 2000. A phylogeny of the Ecliptinae
(Asteraceae: Heliantheae) as inferred from internal
transcriber spacer (ITS) sequences, and the origin
of Lipochaeta. Suppl. Amer. J. Bot.87(6): 119.<person-group>
                  <string-name>
                     <surname>Chumley</surname>
                  </string-name>
               </person-group>
               <issue>6</issue>
               <fpage>119</fpage>
               <volume>87</volume>
               <source>Suppl. Amer. J. Bot.</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d64e492a1310">
            <mixed-citation id="d64e496" publication-type="journal">
Decaisne, J.1834. Herbarii timorensis descripto.
Nouv. Ann. Mus. Hist. Nat.3: 333-501.<person-group>
                  <string-name>
                     <surname>Decaisne</surname>
                  </string-name>
               </person-group>
               <fpage>333</fpage>
               <volume>3</volume>
               <source>Nouv. Ann. Mus. Hist. Nat.</source>
               <year>1834</year>
            </mixed-citation>
         </ref>
         <ref id="d64e528a1310">
            <mixed-citation id="d64e532" publication-type="journal">
Eliasson, U. H.1984. Chromosome number of Ma-
craea laricifolia Hook. fil. (Compositae) and its
bearing on the taxonomic affinity of the genus. Bot.
J. Linn. Soc.88: 253-256.<person-group>
                  <string-name>
                     <surname>Eliasson</surname>
                  </string-name>
               </person-group>
               <fpage>253</fpage>
               <volume>88</volume>
               <source>Bot. J. Linn. Soc.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d64e570a1310">
            <mixed-citation id="d64e574" publication-type="journal">
Fay, J. J.1979. Revision of Perymenium (Asteraceae-
Heliantheae) in Mexico and Central America. Al-
lertonia1: 235-296.<person-group>
                  <string-name>
                     <surname>Fay</surname>
                  </string-name>
               </person-group>
               <fpage>235</fpage>
               <volume>1</volume>
               <source>Allertonia</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d64e610a1310">
            <mixed-citation id="d64e614" publication-type="journal">
Fosberg, F. R.1993. The Forster Pacific islands col-
lections from Captain Cook's resolution voyage.
Allertonia7: 41-86.<person-group>
                  <string-name>
                     <surname>Fosberg</surname>
                  </string-name>
               </person-group>
               <fpage>41</fpage>
               <volume>7</volume>
               <source>Allertonia</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d64e649a1310">
            <mixed-citation id="d64e653" publication-type="journal">
— &amp; M.-H. Sachet. 1980. Systematic studies of
Micronesian plants. Smithsonian Contr. Bot.45: 1-
40.<person-group>
                  <string-name>
                     <surname>Fosberg</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>45</volume>
               <source>Smithsonian Contr. Bot.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d64e688a1310">
            <mixed-citation id="d64e692" publication-type="journal">
Gardner, R. C.1976. Evolution and adaptive radia-
tion in Lipochaeta (Compositae) of the Hawaiian
Islands. Syst. Bot.1: 383-391.<object-id pub-id-type="doi">10.2307/2418707</object-id>
               <fpage>383</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e711a1310">
            <mixed-citation id="d64e717" publication-type="journal">
.1977a. Chromosome numbers and their sys-
tematic implications in Lipochaeta (Compositae:
Heliantheae). Amer. J. Bot.64: 810-813.<object-id pub-id-type="doi">10.2307/2442373</object-id>
               <fpage>810</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e736a1310">
            <mixed-citation id="d64e740" publication-type="journal">
—1977b. Observations on tetramerous disc flo-
rets in the Compositae. Rhodora79: 139-146.<person-group>
                  <string-name>
                     <surname>Gardner</surname>
                  </string-name>
               </person-group>
               <fpage>139</fpage>
               <volume>79</volume>
               <source>Rhodora</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d64e772a1310">
            <mixed-citation id="d64e776" publication-type="journal">
—1979. Revision of Lipochaeta (Compositae:
Heliantheae) of the Hawaiian Islands. Rhodora81:
291-343.<person-group>
                  <string-name>
                     <surname>Gardner</surname>
                  </string-name>
               </person-group>
               <fpage>291</fpage>
               <volume>81</volume>
               <source>Rhodora</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d64e812a1310">
            <mixed-citation id="d64e816" publication-type="journal">
&amp; J. C. La Duke. 1978. Phyletic and cladistic
relationships in Lipochaeta (Compositae). Syst.
Bot.3: 197-207.<object-id pub-id-type="doi">10.2307/2418313</object-id>
               <fpage>197</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e835a1310">
            <mixed-citation id="d64e839" publication-type="journal">
Gill, L. S. &amp; D. I. Omoigui. 1992. Chromosome num-
bers in some Nigerian Compositae. Comp. Newsl.
20/21: 12-15.<person-group>
                  <string-name>
                     <surname>Gill</surname>
                  </string-name>
               </person-group>
               <fpage>12</fpage>
               <volume>20</volume>
               <source>Comp. Newsl.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d64e874a1310">
            <mixed-citation id="d64e878" publication-type="journal">
Harling, G.1962. On some Compositae endemic to
the Galapagos Islands. Acta Horti Bergiani20: 63-
120.<person-group>
                  <string-name>
                     <surname>Harling</surname>
                  </string-name>
               </person-group>
               <fpage>63</fpage>
               <volume>20</volume>
               <source>Acta Horti Bergiani</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d64e913a1310">
            <mixed-citation id="d64e917" publication-type="journal">
Heller, A. A.1897. Observations on the ferns and
flowering plants of the Hawaiian Islands. Minne-
sota Bot. Stud.1: 760-922.<person-group>
                  <string-name>
                     <surname>Heller</surname>
                  </string-name>
               </person-group>
               <fpage>760</fpage>
               <volume>1</volume>
               <source>Minnesota Bot. Stud.</source>
               <year>1897</year>
            </mixed-citation>
         </ref>
         <ref id="d64e952a1310">
            <mixed-citation id="d64e956" publication-type="journal">
Lauener, L. A.1980. Faurie's Hawaiian types at the
British Museum. Notes Roy. Bot. Gard. Edinburgh
38: 495-497.<person-group>
                  <string-name>
                     <surname>Lauener</surname>
                  </string-name>
               </person-group>
               <fpage>495</fpage>
               <volume>38</volume>
               <source>Notes Roy. Bot. Gard. Edinburgh</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d64e991a1310">
            <mixed-citation id="d64e995" publication-type="journal">
Lessing, C. F.1831. Synanthereae rich. In: A. de
Chamisso &amp; D. de Schlechtendal, editors. De plan-
tis in expeditione Romanzoffiana. Linnaea6: 83-
170, 209-258 (addenda), 510-526 (continuatio).<person-group>
                  <string-name>
                     <surname>Lessing</surname>
                  </string-name>
               </person-group>
               <fpage>83</fpage>
               <volume>6</volume>
               <source>Linnaea</source>
               <year>1831</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1034a1310">
            <mixed-citation id="d64e1038" publication-type="journal">
Mathew, A. &amp; P. M. Mathew. 1978. In IOPB chro-
mosome reports LX. Taxon27: 223-231.<person-group>
                  <string-name>
                     <surname>Mathew</surname>
                  </string-name>
               </person-group>
               <fpage>223</fpage>
               <volume>27</volume>
               <source>Taxon</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1070a1310">
            <mixed-citation id="d64e1074" publication-type="journal">
Nicolson, D. H. &amp; F. R. Fosberg. In press. Forsters
and botany of the second Cook expedition (1772-
1775). Reg. Veg.<person-group>
                  <string-name>
                     <surname>Nicolson</surname>
                  </string-name>
               </person-group>
               <source>Reg. Veg.</source>
            </mixed-citation>
         </ref>
         <ref id="d64e1099a1310">
            <mixed-citation id="d64e1103" publication-type="journal">
Ono, M.1975. Chromosome numbers of some endem-
ic species of the Bonin Islands I. Bot. Mag. Tokyo
88: 323-328.<person-group>
                  <string-name>
                     <surname>Ono</surname>
                  </string-name>
               </person-group>
               <fpage>323</fpage>
               <volume>88</volume>
               <source>Bot. Mag. Tokyo</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1138a1310">
            <mixed-citation id="d64e1142" publication-type="journal">
—1977. Cytotaxonomic studies on the flowering
plants endemic species of the Bonin Islands I.
Mem. Nat. Sci. Mus.10: 63-80.<person-group>
                  <string-name>
                     <surname>Ono</surname>
                  </string-name>
               </person-group>
               <fpage>63</fpage>
               <volume>10</volume>
               <source>Mem. Nat. Sci. Mus.</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1177a1310">
            <mixed-citation id="d64e1181" publication-type="journal">
Panero, J. L., R. K. Jansen &amp; J. A. Clevinger. 1999.
Phylogenetic relationships of subtribe Ecliptinae
(Asteraceae: Heliantheae) based on chloroplast
DNA restriction site data. Amer. J. Bot.86: 413-
427.<object-id pub-id-type="doi">10.2307/2656762</object-id>
               <fpage>413</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e1207a1310">
            <mixed-citation id="d64e1211" publication-type="journal">
Parks, J. C.1973. A revision of North American and
Caribbean Melanthera (Compositae). Rhodora
75(802): 169-210.<person-group>
                  <string-name>
                     <surname>Parks</surname>
                  </string-name>
               </person-group>
               <issue>802</issue>
               <fpage>169</fpage>
               <volume>75</volume>
               <source>Rhodora</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1250a1310">
            <mixed-citation id="d64e1254" publication-type="book">
Peng, C.-I., K.-F. Chung &amp; H.-L. Li. 1998. 144.
Compositae. Flora of Taiwan, Ed. 2, 4: 807-1102.<person-group>
                  <string-name>
                     <surname>Peng</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <fpage>807</fpage>
               <volume>4</volume>
               <source>Compositae</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1289a1310">
            <mixed-citation id="d64e1293" publication-type="journal">
Rabakonandrianina, E.1980. Infrageneric relation-
ships and the origin of the Hawaiian endemic genus
Lipochaeta (Compositae). Pacific Sci.34: 29-39.<person-group>
                  <string-name>
                     <surname>Rabakonandrianina</surname>
                  </string-name>
               </person-group>
               <fpage>29</fpage>
               <volume>34</volume>
               <source>Pacific Sci.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1328a1310">
            <mixed-citation id="d64e1332" publication-type="journal">
&amp; G. D. Carr. 1981. Intergeneric hybridiza-
tion, induced polyploidy, and the origin of the Ha-
waiian endemic Lipochaeta from Wedelia (Com-
positae). Amer. J. Bot.68: 206-215.<object-id pub-id-type="doi">10.2307/2442852</object-id>
               <fpage>206</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e1355a1310">
            <mixed-citation id="d64e1359" publication-type="journal">
Robinson, H.1981. A revision of the tribal and sub-
tribal limits of the Heliantheae (Asteraceae). Smith-
sonian Contr. Bot.51: 1-102.<person-group>
                  <string-name>
                     <surname>Robinson</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>51</volume>
               <source>Smithsonian Contr. Bot.</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1394a1310">
            <mixed-citation id="d64e1398" publication-type="journal">
—, A. M. Powell, R. M. King &amp; J. F. Weedin.
1981. Chromosome numbers in Compositae, XII:
Heliantheae. Smithsonian Contr. Bot.52: 1-28.<person-group>
                  <string-name>
                     <surname>Robinson</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>52</volume>
               <source>Smithsonian Contr. Bot.</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1433a1310">
            <mixed-citation id="d64e1437" publication-type="journal">
Rohr, J. von. 1792. Plantae-Slaegter poa St. Croix.
med tilfoiede Anmaerkninger af Vahl. Skriv. Nat.
Selsk (Kiobenhaven)2: 205-227.<person-group>
                  <string-name>
                     <surname>Rohr</surname>
                  </string-name>
               </person-group>
               <fpage>205</fpage>
               <volume>2</volume>
               <source>Skriv. Nat. Selsk (Kiobenhaven)</source>
               <year>1792</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1473a1310">
            <mixed-citation id="d64e1479" publication-type="journal">
St. John, H. 1954. Review of Mrs. Sinclair's "Indig-
enous flowers of the Hawaiian Islands." Hawaiian
Plant Studies 23. Pacific Sci8: 140-146.<person-group>
                  <string-name>
                     <surname>St. John</surname>
                  </string-name>
               </person-group>
               <fpage>140</fpage>
               <volume>8</volume>
               <source>Pacific Sci</source>
               <year>1954</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1514a1310">
            <mixed-citation id="d64e1518" publication-type="journal">
—1984. Novelties in Lipochaeta (Compositae).
Hawaiian Plant Studies 119. Pacific Sci.38: 253-
282.<person-group>
                  <string-name>
                     <surname>St. John</surname>
                  </string-name>
               </person-group>
               <fpage>253</fpage>
               <volume>38</volume>
               <source>Pacific Sci.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1553a1310">
            <mixed-citation id="d64e1557" publication-type="journal">
Sherff, E. E.1935. Revision of Tetramolopium, Li-
pochaeta, Dubautia, and Railliardia. Bernice P.
Bishop Mus. Bull.135: 1-136.<person-group>
                  <string-name>
                     <surname>Sherff</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>135</volume>
               <source>Bernice P. Bishop Mus. Bull.</source>
               <year>1935</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1592a1310">
            <mixed-citation id="d64e1596" publication-type="book">
Smith, A. C. &amp; G. Carr. 1991. Asteraceae. In: A. C.
Smith, Flora Vitiensis Nova: a new flora of Fiji
(Spermatophytes only)5: 254-320. National Trop-
ical Botanical Garden, Lawai, Hawaii.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Asteraceae</comment>
               <fpage>254</fpage>
               <volume>5</volume>
               <source>Flora Vitiensis Nova: a new flora of Fiji (Spermatophytes only)</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1637a1310">
            <mixed-citation id="d64e1643" publication-type="journal">
Strother, J. L.1991. Taxonomy of Complaya, Ela-
phandra, Iogeton, Jefea, Wamalchitamia, Wedelia,
Zexmenia, and Zyzyxia (Compositae-Heliantheae-
Ecliptinae). Syst. Bot. Monogr.33: 1-111.<person-group>
                  <string-name>
                     <surname>Strother</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>33</volume>
               <source>Syst. Bot. Monogr.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1681a1310">
            <mixed-citation id="d64e1685" publication-type="book">
Stuessy, T.1977. Heliantheae: systematic review. Pag-
es 673-697 In: V. H. Heywood, J. B. Harborne &amp;
B. L. Turner, editors. The biology and chemistry of
the Compositae. Academic Press, London.<person-group>
                  <string-name>
                     <surname>Stuessy</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Heliantheae: systematic review</comment>
               <fpage>673</fpage>
               <source>The biology and chemistry of the Compositae</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1724a1310">
            <mixed-citation id="d64e1728" publication-type="journal">
Wagner, W. L. &amp; R. K. Shannon. 1999. Angiosperm
types from the Hawaiian collections of A. A. Hell-
er. Brittonia51: 422-438.<object-id pub-id-type="doi">10.2307/2666526</object-id>
               <fpage>422</fpage>
            </mixed-citation>
         </ref>
         <ref id="d64e1747a1310">
            <mixed-citation id="d64e1751" publication-type="book">
—, D. R. Herbst &amp; S. H. Sohmer. 1990. Manual
of the flowering plants of Hawai'i. Bishop Museum
Special Publication 83. University of Hawaii Press
and Bishop Museum Press, Honolulu.<person-group>
                  <string-name>
                     <surname>Wagner</surname>
                  </string-name>
               </person-group>
               <source>Manual of the flowering plants of Hawai'i</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d64e1783a1310">
            <mixed-citation id="d64e1787" publication-type="journal">
Wild, H.1965. The African species of the genus Me-
lanthera Rohr. Kirkia5: 1-17.<person-group>
                  <string-name>
                     <surname>Wild</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>5</volume>
               <source>Kirkia</source>
               <year>1965</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
