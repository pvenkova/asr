<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1jktrp3</book-id>
      <subj-group>
         <subject content-type="call-number">QH345 .S74 2002</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Biochemistry</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Stoichiometry</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Molecular ecology</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Biological Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Ecological Stoichiometry</book-title>
         <subtitle>The Biology of Elements from Molecules to the Biosphere</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Sterner</surname>
               <given-names>Robert W.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Elser</surname>
               <given-names>James J.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib3">
            <role>WITH A FOREWORD BY</role>
            <name name-style="western">
               <surname>VITOUSEK</surname>
               <given-names>PETER</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>02</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="epub">9781400885695</isbn>
      <isbn content-type="epub">1400885698</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2002</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1jktrp3"/>
      <abstract abstract-type="short">
         <p>All life is chemical. That fact underpins the developing field of ecological stoichiometry, the study of the balance of chemical elements in ecological interactions. This long-awaited book brings this field into its own as a unifying force in ecology and evolution. Synthesizing a wide range of knowledge, Robert Sterner and Jim Elser show how an understanding of the biochemical deployment of elements in organisms from microbes to metazoa provides the key to making sense of both aquatic and terrestrial ecosystems. After summarizing the chemistry of elements and their relative abundance in Earth's environment, the authors proceed along a line of increasing complexity and scale from molecules to cells, individuals, populations, communities, and ecosystems. The book examines fundamental chemical constraints on ecological phenomena such as competition, herbivory, symbiosis, energy flow in food webs, and organic matter sequestration. In accessible prose and with clear mathematical models, the authors show how ecological stoichiometry can illuminate diverse fields of study, from metabolism to global change. Set to be a classic in the field, Ecological Stoichiometry is an indispensable resource for researchers, instructors, and students of ecology, evolution, physiology, and biogeochemistry. From the foreword by Peter Vitousek: "[T]his book represents a significant milestone in the history of ecology. . . . Love it or argue with it--and I do both--most ecologists will be influenced by the framework developed in this book. . . . There are points to question here, and many more to test . . . And if we are both lucky and good, this questioning and testing will advance our field beyond the level achieved in this book. I can't wait to get on with it."</p>
      </abstract>
      <counts>
         <page-count count="464"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.3</book-part-id>
                  <title-group>
                     <title>List of Figures</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.4</book-part-id>
                  <title-group>
                     <title>List of Tables</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.5</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Vitousek</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xv</fpage>
                  <abstract>
                     <p>Stoichiometry has had a long and honorable history in ecology. Redfield’s pioneering analysis in 1958 is a beautiful and widely known example of an explicitly stoichiometric analysis; Reiners’ (1986) too-little-noted discussion of stoichiometry as a major underlying principle in ecosystem-level ecology is another example. Earlier work by Sterner and Elser, their students and colleagues, and increasingly by others, has built substantially on these beginnings. Stoichiometric concepts have been applied implicitly even more widely, for even longer. They underlie discussions of light-nitrogen and light-phosphorus interactions in terrestrial and aquatic ecosystems, of critical ratios of carbon to nitrogen during decomposition/mineralization, and of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.6</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Stoichiometry and Homeostasis</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>What are some of the most powerful explanatory ideas in all of science? Here are a few of our favorites: natural selection, the periodic table of elements, conservation of matter and energy, positive and negative feedback, the central dogma of molecular biology, and the ecosystem. This book you have just opened involves all of these. It is a book about how chemical elements come together to form evolved, living species in ecosystems. It is a book that takes very seriously the constraints of matter and energy. These are among the most powerful forces in nature, and a good understanding of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Biological Chemistry:</title>
                     <subtitle>Building Cells from Elements</subtitle>
                  </title-group>
                  <fpage>44</fpage>
                  <abstract>
                     <p>The fact that the chemical composition of organisms differs in many ways from that of the nonliving world (Table 1.1) implies a “natural selection of the elements” (Williams and Fraústo da Silva 1996). In other words, the elements used extensively by biological systems are not an unbiased sample from the periodic table or from the abiotic world. Indeed, a distinct stoichiometric signature of living systems is one tool that has been used to separate life from nonlife in ancient fossilized material (Watanabe et al. 2000). The physical chemistry of an element directly determines the types of interactions it has with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Stoichiometry of Autotroph Growth:</title>
                     <subtitle>Variation at the Base of Food Webs</subtitle>
                  </title-group>
                  <fpage>80</fpage>
                  <abstract>
                     <p>Photoautotrophs are the globally dominant interface between nonliving and living systems, where elements are combined to form living biomass. The elemental stoichiometry at the base of food webs is established when primary producers use light to fix carbon dioxide and simultaneously assimilate inorganic nutrients, thus creating biological systems with a biochemical and elemental mixture related to structure and function as we discussed in the previous chapter.</p>
                     <p>Autotroph elemental content is determined by the net difference between uptake and losses due to such processes as respiration, exudation, and (in higher plants) leaf and root excision. A key concept in this chapter</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>How to Build an Animal:</title>
                     <subtitle>The Stoichiometry of Metazoans</subtitle>
                  </title-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>In the history of biology, the dominant way of analyzing animal growth is with biochemical (Lehninger 1971), physiological (Pandian and Vernberg 1987; Jobling 1994), or ecological (Wiegert 1976) energetics. Within ecological energetics, animals are abstracted as local thermodynamic perturbations—repositories of chemical energy—and whose feeding, metabolism, growth, and reproduction are analyzed by application of the energy concepts of thermodynamics: calories, free energy, work, entropy, heat, efficiency, and productivity. The basic assumption is that flows of energy organize thermodynamic systems, including biological ones (Morowitz 1968). Calories of food are ingested and expended via metabolism or captured in covalent bonds during</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Imbalanced Resources and Animal Growth</title>
                  </title-group>
                  <fpage>179</fpage>
                  <abstract>
                     <p>Our book now shifts focus from the chemical composition of individuals and their component parts and we start to ask questions about how ecological stoichiometry regulates or constrains ecological processes. This chapter considers how consumer growth is affected by the chemical composition of resources. As we will see, there are many ways that consumers react to foods of differing composition, including changes in feeding rate, food selection, production efficiency, biomass growth rate, and population growth rate. Living consumers and prey are complex biological systems constantly undergoing ontogenetic and physiological adjustments that may influence their stoichiometry. One of the most important</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>The Stoichiometry of Consumer-Driven Nutrient Recycling</title>
                  </title-group>
                  <fpage>231</fpage>
                  <abstract>
                     <p>Consumers separate what has been mingled in their food and return some of those ingested nutrients to their surroundings; in this chapter we will refer to this process as consumer-driven nutrient recycling (CNR). In Chapter 3 we explored the stoichiometrically flexible lifestyle of autotrophs, whose elemental composition is variable in response to nutrient supply and abiotic factors such as temperature and light intensity. In Chapter 4 we considered major aspects determining the elemental composition of metazoan animals, emphasizing patterns of cellular allocation that determine the characteristic C:N:P of various species. In strong contrast to the situation with autotrophs, our main</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Stoichiometry in Communities:</title>
                     <subtitle>Dynamics and Interactions</subtitle>
                  </title-group>
                  <fpage>262</fpage>
                  <abstract>
                     <p>In this chapter, we will see what stoichiometry has to say about collections of interacting species, in other words, communities. Some of the critical aspects of stoichiometry at lower levels of organization—namely, homeostatic regulation of chemical content, linkages between elemental content and the physical and chemical environment, and interspecific stoichiometric variability—will reappear. Some new patterns and feedbacks also will arise.</p>
                     <p>Our operational definition of a community will be a dynamic, biological system with a small set of interacting species or other players (some of which, like soil nutrient pools, may be abiotic). By “small set” we distinguish communities</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Big-Scale Stoichiometry:</title>
                     <subtitle>Ecosystems in Space and Time</subtitle>
                  </title-group>
                  <fpage>313</fpage>
                  <abstract>
                     <p>Our journey from molecules to ecosystems is almost complete. What remains is to examine the highest levels of biological organization at the largest spatial and longest temporal scales. Here, we are returning to the intellectual roots of ecological stoichiometry. Ecosystems were the subject of many of the seminal studies we have already relied heavily upon, including those of Lotka and Redfield and others. Even these historical figures had predecessors pointing to the importance of consideration of multiple substances in biomass. For example, consider this quotation dating from the first decade of the twentieth century, years before Redfield’s and even Lotka’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Recapitulation and Integration</title>
                  </title-group>
                  <fpage>370</fpage>
                  <abstract>
                     <p>So, how far have we come since the time of the ancient Greeks in understanding the “coming into being of composite things” in the living world? As we have now seen, we know quite a bit about the composite chemical nature of the living world. The preceding pages have taken us from the mundane (food ingestion, excretion, and egestion) to the esoteric (mathematical nullclines), from the staggeringly small (atom-to-atom interactions in biochemicals) to the numbingly large (material cycling at the global scale). Mass conservation applies at all these scales and all also are amenable to stoichiometric analysis. The fundamental nature</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.16</book-part-id>
                  <title-group>
                     <title>Appendix</title>
                  </title-group>
                  <fpage>382</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.17</book-part-id>
                  <title-group>
                     <title>Literature Cited</title>
                  </title-group>
                  <fpage>385</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1jktrp3.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>431</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
