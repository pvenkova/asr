<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">greabasinatu</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50005090</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Great Basin Naturalist</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Brigham Young University</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00173614</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41712921</article-id>
         <title-group>
            <article-title>EXCEPTIONAL FISH YIELD IN A MID-ELEVATION UTAH TROUT RESERVOIR: EFFECTS OF ANGLING REGULATIONS</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Wayne A.</given-names>
                  <surname>Wurtsbaugh</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David</given-names>
                  <surname>Barnard</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thomas</given-names>
                  <surname>Pettengill</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>1996</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">56</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40080445</issue-id>
         <fpage>12</fpage>
         <lpage>21</lpage>
         <permissions>
            <copyright-statement>Copyright © 1996 Brigham Young University</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41712921"/>
         <abstract>
            <p>We used creel surveys to evaluate how a change from a 6-mon to a year-round fishing season affected the sport fish harvest in East Canyon Reservoir (Utah), a 277-ha mesoeutrophic system. Under the year-round season, fishing effort was 840 angler-h·ha⁻¹·yr⁻¹, and 360 trout ha⁻¹ were captured. Catch rates were proportional to estimated trout densities in the reservoir, ranging from 1.06 during the winter ice fishery, to 0.18 fish angler⁻¹·h⁻¹ in July. Ninety-nine percent of fish harvested were rainbow trout (Oncorhynchus mykiss). Thirty-two percent of the 300,000 75-mm fingerling trout stocked annually were captured by anglers within 2.5 yr, but return rates varied with the strain and/or size of trout stocked. Annual fish yield was 102 kg/ha, among the highest yet reported for a temperate zone, lacustrine system. Extending fishing from a 6-mon season to year-round increased the number of fish captured and provided almost twice as many hours of recreational fishing in the reservoir. The harvest period was changed from traditional spring-summer months to primarily a winter-spring fishery because relatively few trout survived for more than 6 mon after reaching harvestable size. Although salmonid production in East Canyon Reservoir is very high, the fishery is in a precarious state because high primary productivity driven, in part, by cultural eutrophication, makes water quality suboptimal during midsummer.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d562e214a1310">
            <mixed-citation id="d562e218" publication-type="other">
Babey, G. J., and C. R. Berry. 1989. Post-stocking perfor-
mance of three strains of rainbow trout in a reser-
voir. North American Journal of Fisheries Manage-
ment 9: 309-315.</mixed-citation>
         </ref>
         <ref id="d562e234a1310">
            <mixed-citation id="d562e238" publication-type="other">
Berry, C. R. Jr., G. J. Babey, and T. Schräder. 1991.
Effect of Lernaea cyprinacea (Crustacea: Copepoda)
on stocked rainbow trout (Oncorhynchus mykiss).
Journal of Wildlife Diseases 27: 206-213.</mixed-citation>
         </ref>
         <ref id="d562e254a1310">
            <mixed-citation id="d562e258" publication-type="other">
Brauhn, J. L., and H. L. Kincaid. 1982. Survival, growth,
and catchability of rainbow trout of four strains.
North American Journal of Fisheries Management 2:
1-10.</mixed-citation>
         </ref>
         <ref id="d562e274a1310">
            <mixed-citation id="d562e278" publication-type="other">
Brett, J. R. 1979. Environmental factors and growth.
Pages 599-675 in W. S. Hoar, D. J. Randall, and J. R.
Brett, editors, Fish physiology. Volume III, Bioener-
getics and growth. Academic Press, NY.</mixed-citation>
         </ref>
         <ref id="d562e295a1310">
            <mixed-citation id="d562e299" publication-type="other">
Carline, R. F 1986. Indices as predictors of fish commu-
nity traits. Pages 46-56 in G. E. Hall and M. J. Van
Den Avyle, editors, Reservoir fisheries management:
strategies for the 80s. American Fisheries Society,
Bethesda, MD.</mixed-citation>
         </ref>
         <ref id="d562e318a1310">
            <mixed-citation id="d562e322" publication-type="other">
Carlton, F E. 1975. Optimum sustainable yield as a man-
agement concept in recreational fisheries. American
Fisheries Society Special Publication 9: 45-49.</mixed-citation>
         </ref>
         <ref id="d562e335a1310">
            <mixed-citation id="d562e339" publication-type="other">
Colby, P J., G. R. Spangler, D. A. Hurley, and A. M.
McCombie. 1972. Effects of eutrophication on
salmonid communities in oligotrophic lakes. Journal
of the Fisheries Research Board of Canada 29:
975-983.</mixed-citation>
         </ref>
         <ref id="d562e358a1310">
            <mixed-citation id="d562e362" publication-type="other">
Downing, J. A., and C. Plante. 1993. Production of fish
populations in lakes. Canadian Journal of Fisheries
and Aquatic Sciences 50: 110-120.</mixed-citation>
         </ref>
         <ref id="d562e375a1310">
            <mixed-citation id="d562e379" publication-type="other">
EPA (U.S. Environmental Protection Agency). 1986.
Quality criteria for water. EPA report 440/5-86-001.
Washington, DC.</mixed-citation>
         </ref>
         <ref id="d562e392a1310">
            <mixed-citation id="d562e396" publication-type="other">
Hall, G. E., and M. J. Van Den Avyle. 1986. Reservoir
fisheries management: strategies for the 80s. Ameri-
can Fisheries Society, Bethesda, MD. 327 pp.</mixed-citation>
         </ref>
         <ref id="d562e410a1310">
            <mixed-citation id="d562e414" publication-type="other">
Hokanson, K. E. F., C. F. Kleiner, and T. W. Thors-
lund. 1977. Effects of constant temperatures and
diel temperature fluctuations on specific growth and
mortality rates and yield of juvenile rainbow trout,
Salmo gairdneri. Journal of the Fisheries Research
Board of Canada 34: 639-648.</mixed-citation>
         </ref>
         <ref id="d562e437a1310">
            <mixed-citation id="d562e441" publication-type="other">
Jones, J. R., and M. V. Hoyer. 1982. Sportfish harvest
predicted by summer chlorophyll-a concentration in
midwestern lakes and reservoirs. Transactions of the
American Fisheries Society 111: 176-179.</mixed-citation>
         </ref>
         <ref id="d562e457a1310">
            <mixed-citation id="d562e461" publication-type="other">
Malvestito, S. P. 1983. Sampling the recreational fishery.
Pages 397-419 in L. A. Nielsen and D. L. Johnson,
editors, Fishery techniques. American Fisheries
Society, Bethesda, MD.</mixed-citation>
         </ref>
         <ref id="d562e477a1310">
            <mixed-citation id="d562e481" publication-type="other">
Matkowski, S. M. D. 1989. Differential susceptibility of
three species of stocked trout to bird prédation.
North American Journal of Fisheries Management 9:
184-187.</mixed-citation>
         </ref>
         <ref id="d562e497a1310">
            <mixed-citation id="d562e501" publication-type="other">
Merritt, L. B., A. W. Miller, R. N. Winget, S. R. Rush-
forth, and W. H. Brimhall. 1980. East Canyon
Reservoir water quality assessment. Mountainland
Association of Governments, Provo, UT. 193 pp.</mixed-citation>
         </ref>
         <ref id="d562e517a1310">
            <mixed-citation id="d562e521" publication-type="other">
Morgan, N. C., et al. 1980. Secondary production. Pages
247-340 in E. D. LeCren and R. H. Lowe-McConnell,
editors, The functioning of freshwater ecosystems.
Cambridge University Press, London.</mixed-citation>
         </ref>
         <ref id="d562e538a1310">
            <mixed-citation id="d562e542" publication-type="other">
Ogelsby, R. T. 1977. Relationships of fish yield to lake
phytoplankton standing crop, production and morpho-
edaphic factors. Journal of the Fisheries Research
Board of Canada 34: 2271-2279.</mixed-citation>
         </ref>
         <ref id="d562e558a1310">
            <mixed-citation id="d562e562" publication-type="other">
Phinney, E. E., D. M. Miller, and M. L. Dahlberg.
1967. Mass-marking young salmonids with fluores-
cent pigment. Transactions of the American Fish-
eries Society 96: 157-162.</mixed-citation>
         </ref>
         <ref id="d562e578a1310">
            <mixed-citation id="d562e582" publication-type="other">
Plante, C., and J. A. Downing. 1993. Relationship of
salmonine production to lake trophic status and tem-
perature. Canadian Journal of Fisheries and Aquatic
Science 50: 1324-1328.</mixed-citation>
         </ref>
         <ref id="d562e598a1310">
            <mixed-citation id="d562e602" publication-type="other">
Schlesinger, D. A., and H. A. Regier. 1982. Climatic
and morphoedaphic indices of fish yields from nat-
ural lakes. Transactions of the American Fisheries
Society 111: 141-150.</mixed-citation>
         </ref>
         <ref id="d562e618a1310">
            <mixed-citation id="d562e622" publication-type="other">
Schrader, T. M. 1988. Performance of three strains of
rainbow trout in East Canyon Reservoir. Unpub-
lished master's thesis, Utah State University, Logan.</mixed-citation>
         </ref>
         <ref id="d562e635a1310">
            <mixed-citation id="d562e639" publication-type="other">
Stockner, J. G. 1992. Lake fertilization: the enrichment
cycle and lake sockeye salmon (Oncorhynchus nerka)
production. Pages 199-214 in H. D. Smith, L. Mar-
golis, and C. C. Wood, editors, Sockeye salmon (Onco-
rhynchus nerka) population biology and future man-
agement. Canadian Special Publication in Fisheries
and Aquatic Science 96: 198-215.</mixed-citation>
         </ref>
         <ref id="d562e666a1310">
            <mixed-citation id="d562e670" publication-type="other">
Tabor, R. A., and W. A. Wurtsbaugh. 1991. Prédation risk
and the importance of cover for juvenile rainbow
trout in lentie systems. Transactions of the American
Fisheries Society 120: 728-738.</mixed-citation>
         </ref>
         <ref id="d562e686a1310">
            <mixed-citation id="d562e690" publication-type="other">
Tabor, R. A., C. Luecke, and W. A. Wurtsbaugh. In
press. Effects of Daphnia availability on the growth
and food consumption rates of rainbow trout in two
Utah Reservoirs. Transactions of the American Fish-
eries Society.</mixed-citation>
         </ref>
         <ref id="d562e709a1310">
            <mixed-citation id="d562e713" publication-type="other">
Taylor, M. J., and K. R. White. 1992. A meta-analysis of
hooking mortality of nonanadromous trout. North
American Journal of Fisheries Management 12:
760-767.</mixed-citation>
         </ref>
         <ref id="d562e729a1310">
            <mixed-citation id="d562e735" publication-type="other">
Trendall, J. 1988. Recruitment of juvenile mbuna (Pisces:
Cichlidae) to experimental rock shelters in Lake
Malawi, Africa. Environmental Biology of Fishes 22:
117-132.</mixed-citation>
         </ref>
         <ref id="d562e751a1310">
            <mixed-citation id="d562e755" publication-type="other">
Utah Department of Health. 1982. State of Utah clean
lakes inventory and classification, volume 1. Depart-
ment of Health, Salt Lake City, UT. 519 pp.</mixed-citation>
         </ref>
         <ref id="d562e768a1310">
            <mixed-citation id="d562e772" publication-type="other">
Vondracek, B., W. Wurtsbaugh, and J. Cech. 1980.
Mass marking of Gambusia. California Mosquito and
Vector Control Association 48: 42-44.</mixed-citation>
         </ref>
         <ref id="d562e786a1310">
            <mixed-citation id="d562e790" publication-type="other">
Wasowicz, A. F 1991. Influence of fish and avian preda-
tors upon the trout population of Minersville Reser-
voir. Unpublished masters thesis, Utah State Univer-
sity, Logan.</mixed-citation>
         </ref>
         <ref id="d562e806a1310">
            <mixed-citation id="d562e810" publication-type="other">
Wurtsbaugh, W. A. 1987. Importance of prédation by
adult trout on mortality rates of fingerling rainbow
trout stocked in East Canyon Reservoir, Utah. Pages
14-17 in Proceedings of the Bonneville Chapter of
the American Fisheries Society, Salt Lake City, UT,
1987.</mixed-citation>
         </ref>
         <ref id="d562e833a1310">
            <mixed-citation id="d562e837" publication-type="other">
---. 1988. Iron, molybdenum and phosphorus limita-
tion of N2 fixation maintains nitrogen deficiency of
plankton in the Great Salt Lake drainage (Utah,
USA). Verhandlungen Internationale Vereinigung
für Theoretische und Angewandte Limnologie 23:
121-130.</mixed-citation>
         </ref>
         <ref id="d562e860a1310">
            <mixed-citation id="d562e864" publication-type="other">
Wurtsbaugh, W. A., and G. E. Davis. 1977. Effects of
temperature and ration level on the growth and food
conversion efficiency of Salmo gairdneri, Richard-
son. Journal of Fish Biology 11: 87-98.</mixed-citation>
         </ref>
         <ref id="d562e880a1310">
            <mixed-citation id="d562e884" publication-type="other">
Wurtsbaugh, W. A., R. W. Brocksen, and C. R. Gold-
man. 1975. Food and distribution of underyearling
brook and rainbow trout in Castle Lake, California.
Transactions of the American Fisheries Society 104:
88-95.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
