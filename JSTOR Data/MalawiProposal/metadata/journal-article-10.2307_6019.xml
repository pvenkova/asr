<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">janimalecology</journal-id>
         <journal-id journal-id-type="jstor">j100032</journal-id>
         <journal-title-group>
            <journal-title>Journal of Animal Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>British Ecological Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">00218790</issn>
         <issn pub-type="epub">13652656</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/6019</article-id>
         <title-group>
            <article-title>Long-Term Effects of Brood Size Manipulation on Morphological Development and Sex-Specific Mortality of Offspring</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>C.H.</given-names>
                  <surname>De Kogel</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">66</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i201723</issue-id>
         <fpage>167</fpage>
         <lpage>178</lpage>
         <page-range>167-178</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 British Ecological Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/6019"/>
         <abstract>
            <p>1. Little is known about long-term effects of brood size on fitness components of offspring. This is unfortunate because such information is needed to predict optimal brood size. Furthermore, ontogenetic circumstances are potentially important in explaining the large individual differences in lifetime reproductive success documented in many species. 2. In a laboratory study the long-term effect of brood size manipulation on mortality and morphological development of offspring was investigated. Young zebra finches Taeniopygia guttata were reared in small or large broods. Young were exchanged in such a way that natural siblings from different rearing conditions could be compared. 3. Manipulated brood size affected offspring morphology permanently (measurements were taken up to the age of 12 months). Individuals reared in small broods were heavier, had longer tarsi and wings, higher beaks, were in better condition, and males had redder beaks. The experiment did not affect beak length or female beak redness. Individuals from large broods had caught up on wing length and males from large broods on beak redness by 6 months of age, which may reflect priority of investment in traits important to fitness. 4. Mortality of offspring raised in large broods was higher both before and after independence. After independence the effect of manipulated brood size on mortality was sex specific; females were most likely to die. This suggests that the optimal sex ratio of offspring may depend on brood size. 5. The effect of brood size on mortality after independence was probably partly mediated by size and condition during the nestling phase and possibly by peer aggression among adult offspring. Sex-linked mortality could not be explained by a sex difference in morphological development. Why females are more vulnerable remains to be discovered.</p>
         </abstract>
         <kwd-group>
            <kwd>clutch size optimization</kwd>
            <kwd>growth strategies</kwd>
            <kwd>longevity</kwd>
            <kwd>offspring quality</kwd>
            <kwd>sex ratio</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
</article>
