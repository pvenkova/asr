<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mounresedeve</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100622</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Mountain Research and Development</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>United Nations University and International Mountain Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02764741</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19947151</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3674504</article-id>
         <article-categories>
            <subj-group>
               <subject>Development</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Linking Smallholder Farmers to Markets in East Africa: Empowering Mountain Communities to Identify Market Opportunities and Develop Rural Agroenterprises</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pascal C.</given-names>
                  <surname>Sanginga</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rupert</given-names>
                  <surname>Best</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Colletah</given-names>
                  <surname>Chitsike</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert</given-names>
                  <surname>Delve</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Susan</given-names>
                  <surname>Kaaria</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Roger</given-names>
                  <surname>Kirkby</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">24</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i287232</issue-id>
         <fpage>288</fpage>
         <lpage>291</lpage>
         <page-range>288-291</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 International Mountain Society and United Nations University</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3674504"/>
         <abstract>
            <p>The livelihoods of mountain farmers are often constrained by poor access to markets and limited entrepreneurial skills for adding value to produce. Research and development organizations have now recognized that improving market access and enhancing the ability of resource-poor mountain farmers to diversify their links with markets are among the most pressing challenges in mountain agriculture. What is not so obvious is how to link small-scale farmers in marginal areas to growth markets, and how to develop methods and approaches that effectively integrate research, market access and development of community agroenterprise. The present article highlights the key steps and procedures in building capacity among farmers, farmers' groups, and communities to identify and evaluate market opportunities, develop profitable agroenterprise, and intensify production, while sustaining the resources upon which livelihoods depend. This approach, known as Participatory Market Research (PMR)-a component of the Enabling Rural Innovation (ERI) initiative-is being implemented and further refined by the International Centre for Tropical Agriculture (CIAT) in collaboration with research and development partners in Uganda, Malawi, and Tanzania.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Further Reading</title>
         <ref id="d57e220a1310">
            <mixed-citation id="d57e224" publication-type="book">
Bernet T, Devaux A, Ortiz O, Thiele G.
2004. Participatory Market Chain
Approach. In: Participatory Research
and Development for Sustainable Agri-
culture and Natural Resources Man-
agement. A Sourcebook. Lima, Peru:
International Potato Centre
CIP-UPWARD.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bernet</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Participatory Market Chain Approach</comment>
               <source>Participatory Research and Development for Sustainable Agriculture and Natural Resources Management</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d57e272a1310">
            <mixed-citation id="d57e276" publication-type="other">
Best R. 2002. Farmer participation in
market research to identify income-
generating opportunities. CIAT Africa
Highlights. International Centre for
Tropical Agriculture. Available at:
www.ciat.cgiar.org/africa; accessed in
August 2004.</mixed-citation>
         </ref>
         <ref id="d57e302a1310">
            <mixed-citation id="d57e306" publication-type="book">
Hellin J, Higmann S. 2002. Smallhold-
ers and Niche Markets. Lessons from
the Andes. Agricultural Research and
Extension Network Paper no 118. Lon-
don: Overseas Development Institute
(ODI).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hellin</surname>
                  </string-name>
               </person-group>
               <source>Smallholders and Niche Markets. Lessons from the Andes</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d57e344a1310">
            <mixed-citation id="d57e348" publication-type="book">
Lundy M, Gottret MV, Cifuentes W,
Ostertag CF, Best R. 2003. Design of
Strategies to Increase the Competitive-
ness of Smallholder Production
Chains. Field Manual. Cali, Colombia:
Centro Internacional de Agricultura
Tropical (CIAT).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lundy</surname>
                  </string-name>
               </person-group>
               <source>Design of Strategies to Increase the Competitiveness of Smallholder Production Chains</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d57e390a1310">
            <mixed-citation id="d57e394" publication-type="book">
Ostertag Galvez CF. 1999. Identifying
and Assessing Market Opportunities
for Small Rural Producers. Tools for
Decision-Making in NRM. Cali, Colom-
bia: Centro Internacional de Agricul-
tura Tropical (CIAT).<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Galvez</surname>
                  </string-name>
               </person-group>
               <source>Identifying and Assessing Market Opportunities for Small Rural Producers. Tools for Decision-Making in NRM</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d57e432a1310">
            <mixed-citation id="d57e436" publication-type="book">
Sanginga P, Best R, Chitsike C, Delve
R, Kaaria S, Kirkby R. 2004. Enabling
Rural Innovation in Africa: An Approach
for Integrating Farmer Participatory
Research and Market Orientation for
Building the Assets of Rural Poor.
Report. Kampala: International Centre
for Tropical Agriculture (CIAT) Africa.
Available from the authors.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Sanginga</surname>
                  </string-name>
               </person-group>
               <source>Enabling Rural Innovation in Africa: An Approach for Integrating Farmer Participatory Research and Market Orientation for Building the Assets of Rural Poor</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
