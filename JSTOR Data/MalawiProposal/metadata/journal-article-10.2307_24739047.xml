<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">aejapplecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50001040</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Economic Journal: Applied Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Economic Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">19457782</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19457790</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24739047</article-id>
         <title-group>
            <article-title>Incentives, Commitments, and Habit Formation in Exercise: Evidence from a Field Experiment with Workers at a Fortune-500 Company</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Heather</given-names>
                  <surname>Royer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mark</given-names>
                  <surname>Stehr</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Justin</given-names>
                  <surname>Sydnor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2015</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24739043</issue-id>
         <fpage>51</fpage>
         <lpage>84</lpage>
         <permissions>
            <copyright-statement>Copyright © 2015 American Economic Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24739047"/>
         <abstract>
            <p>Financial incentives have shown strong positive short-run effects for problematic health behaviors that likely stem from time inconsistency. However, the effects often disappear once incentive programs end. This paper analyzes the results of a large-scale workplace field experiment to examine whether self-funded commitment contracts can improve the long-run effects of an incentive program. A four-week incentive program targeting use of the company gym generated only small lasting effects on behavior. Those that also offered a commitment contract at the end of the program, however, showed demand for commitment and significant long-run changes, detectable even several years after the incentive ended.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1840e220a1310">
            <label>†</label>
            <p>
               <mixed-citation id="d1840e227" publication-type="other">
Go to http://dx.doi.org/10.1257/app.20130327 to visit the article page for additional materials and author
disclosure statement(s) or to comment in the online discussion forum.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e237a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1840e244" publication-type="other">
In other applications, such as StickK.com, commitment contracts are often given with the option for forfeited
money to go to an undesirable source, what StickK.com calls an "anti-charity." The relative effectiveness of differ-
ent forfeiture options remains an unstudied area.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e257a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1840e264" publication-type="other">
Consistent with the existing literature we refer to these long-run effects as "habit formation" effects. However,
we recognize that long-run effects may be due to channels such as learning about exercise that not all would refer
to as "habit formation.'</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e277a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1840e284" publication-type="other">
Bryan, Karlan, and Nelson (2010) argue that those with mild overoptimism may still see commitments as
desirable but will likely believe that weak commitments be more effective than they are.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e295a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1840e302" publication-type="other">
Goldhaber-Feibert, Blumenkranz, and Garber (2010) explore whether the commitment contracts people
design for exercise can be influenced by anchoring and nudges but do not observe the outcomes of those contracts.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e312a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1840e319" publication-type="other">
Brune et al. (2013), however, observed that most used an uncommitted account over a commitment account
when both were available. Karlan and Linden (2014) also find that a savings account with a full commitment to
education was less effective than an account that was merely intended for educational expense.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e332a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1840e339" publication-type="other">
There are no start-up fees or contracts and employees can cancel their membership at any time with no penalty.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e346a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1840e353" publication-type="other">
The gym is open Monday through Friday from 6:00 a.m. to 7:00 P.M.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e360a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1840e367" publication-type="other">
We observe a difference of less than one noontime visit during the time when our incentive program was in
place.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e377a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1840e384" publication-type="other">
Response rates do vary some across cohorts, although, in a regression of whether or not an individual responded
on cohort fixed effects, we are unable to reject the hypothesis that the cohort fixed effects are jointly equal to each
other. Moreover, the fraction of responders who are gym members is not changing systematically over time. If word
spread rapidly through the company about the details of our experiment and this affected participation, we would
expect that response rates and the fraction who are gym members would vary across cohorts.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e404a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1840e411" publication-type="other">
Our data on employees are limited (essentially departmental unit, position, and gym membership status).
Gym members responded to the initial survey at a somewhat higher rate than nonmembers—74 percent versus 57
percent.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e424a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1840e431" publication-type="other">
Charness and Gneezy (2009), however, do not use per-visit incentives and instead base incentives on whether
individuals met or exceeded an eight visit threshold.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e441a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1840e448" publication-type="other">
In pilot experiments at the company prior to this experiment, there was essentially zero response to a treat-
ment offering only a free membership.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e458a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1840e465" publication-type="other">
In order to ensure balance between the incentive-only and the incentive-(-commit groups, we rerandomized
during this step until a p-value on the test of the equality of the in-treatment effects between the two incentive
groups exceeded 0.10. For the first few cohorts, we made these random sub-treatment assignments prior to observ-
ing exercise behavior from the incentive period. Given the relatively small sample size of cohorts, we observed
some imbalance in gym visits between the incentive-only and incentive+commitment groups during the treatment
period for the first few cohorts. For that reason we decided to change the protocol and conduct the randomization
after the incentive period for later cohorts.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e491a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1840e498" publication-type="other">
Note the sample sizes are not balanced across the three groups: control, incentive, and incentive+commit.
We wanted the largest samples in the incentive and incentive+commit groups, which are approximately equal in
size, because their differences would be most difficult to detect. The size of each treatment group for each cohort
was dictated by number of new possible members the wellness center could sign up. Treatment probabilities ranged
between 0.2 and 0.4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e517a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1840e524" publication-type="other">
These estimated mean differences come from simple regressions that include strata fixed effects (a combina-
tion of gym membership, exercise relative to target and cohort), which are included in all regressions throughout.
Including strata fixed effects ensures that results are not biased by fluctuations across cohorts in the shares of
employees randomly sorted into control and treatment groups.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e541a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1840e548" publication-type="other">
Baseline statistics for this and previous sentence based on authors' calculations using the 2010 census.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e555a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1840e562" publication-type="other">
Source of statistic is http://sda.berkeley.edu/cgi-bin/hsda?harcsda+gsslO.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e569a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1840e576" publication-type="other">
There was a week between the week of the initial survey and the start of the incentives that new members
could use to sign up. Visits for that week are excluded from this graph. Also, for some cohorts the commitment
period ran to week 14 due to holidays, so month 4 in the graph sometimes includes one week (week 13) that was
within the commitment period.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e592a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1840e599" publication-type="other">
The fraction attending falls over time for the control group, which is not surprising in this subsample because
(i) restricting to existing members naturally results in some reversion to the mean and (ii) high percentages of sub-
jects had incentive periods in the fall and spring, so that the posttreatment periods are composed somewhat heavily
of summer months when attendance tends to be lower.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e615a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1840e622" publication-type="other">
For ease of interpretation, we present OLS estimates of these regressions. We also estimated probit models to
take into account the binary nature of the dependent variable, "any visit," and these models produced similar results.
The weekly visits measure is also bounded between 0 and 5 and in principle it would be appropriate to use a mode!
that takes into account the censored nature of that dependent variable. Again for ease of interpretation we present
OLS results. Tobit estimates yield very similar conclusions to the OLS regressions.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e641a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1840e648" publication-type="other">
Of course, a lack of success in fulfilling the contract, the fact that many of the people partaking in these con-
tracts are already exercising at the company gym, and the encouragement the contract may provide individuals to
exercise beyond its minimal requirements will cause these estimates to stray from 0.5.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e662a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1840e669" publication-type="other">
The estimates come from a regression specification following the format described in Section IVB. Period 1
in the regression is weeks 1-4, period 2 is weeks 5-13 (commitment-contract period), and then periods 3 and on
are the subsequent 8 week groupings.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e682a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1840e689" publication-type="other">
We have also estimated the average treatment effect over the third year on the fraction attending, which is
0.032 with a p-value of 0.105.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e699a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1840e706" publication-type="other">
We observe too few commitment contracts to present any meaningful analysis of the size of the commitment
individuals made, and focus instead simply on the take up decision.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e716a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1840e723" publication-type="other">
The survey with these measures was conducted before subjects learned about the commitment option.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e730a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1840e737" publication-type="other">
We see this as primarily a result of the difficulty of assessing time inconsistency using a series of hypothetical
questions related to time-dated monetary receipts.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e747a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1840e754" publication-type="other">
Giné et al. (2012) present another method for identifying time inconsistency through reversals in previous
time-dated monetary allocations. Consistent with the discussion in Augenblick, Niederle, and Sprenger (2014), they
find preference reversals but not strong indication of present-biased time inconsistency.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e768a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1840e775" publication-type="other">
There are at least two possible problems with this measure. First, "personal targets" reported in surveys may
not conform to the theoretically relevant construct of the frequency of exercise a person would do in the absence
of time-inconsistency problems. Second, those who have found other sources to overcome a time inconsistency
problem already may report being in line with their personal target yet still have an underlying time inconsistency
problem.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e794a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1840e801" publication-type="other">
This question was asked prior to subjects learning about the incentive program.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e808a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1840e815" publication-type="other">
At least one other study in this area has attempted to measure substitution, but the conclusions are unclear.
Charness and Gneezy (2009) ask participants to fill out an exercise log. The log includes questions about overall
exercise, exercise at a gym, and exercise outside of a gym. As they state, the self-reported data in their case do
not seem to be reliable. For example, the effect on gym use for the main incentive group is 0.04 gym visits/week
whereas that measured via administrative data is 1.22 university gym visits/week. The difference in these estimates
could reflect considerable measurement error in the exercise logs or significant substitution (i.e., substitution of
other gyms for the university gym).</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e841a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1840e848" publication-type="other">
Sample sizes differ across regressions because of nonresponse to the follow-up survey; regressions estimated
using the computerized gym data on just the sample of follow-up survey responders give similar estimates.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e858a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1840e865" publication-type="other">
Survey gym visits do not appear to be a good measure of actual gym visits for members at or above their
target. While the reason for this mismatch is not clear, there is little evidence that this group increased their overall
exercise in response to the incentives.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e878a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1840e885" publication-type="other">
Of course, this conclusion assumes that company gym attendance is equivalent to exercise at the company
gym (i.e., individuals are not going to the gym without exercising).</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e896a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1840e903" publication-type="other">
The standard treatment effects literature assumes the existence of the stable-unit-treatment-value (SUTVA)
assumption (Cox 1958) and such cross-contamination effects would be a violation of this assumption.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e913a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1840e920" publication-type="other">
The cuts are 0-0.5 days, t to 3 days, and more than 3 days per week.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e927a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1840e934" publication-type="other">
For this table we split subjects based on tertiles of prestudy exercise using gym attendance for existing mem-
bers of the gym and self-reported exercise frequency in the initial survey for nonmembers.</mixed-citation>
            </p>
         </fn>
         <fn id="d1840e944a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1840e951" publication-type="other">
The targeting of incentives (e.g., payments for smokers to quit smoking) may be seen as inequitable and thus,
while cost-effective, targeting may be rather infeasible.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1840e970a1310">
            <mixed-citation id="d1840e974" publication-type="other">
Acland, Dan, and Matthew Levy. 2015. "Naiveté, Projection Bias, and Habit Formation in Gym Atten-
dance." Management Science 61 (1): 146-60.</mixed-citation>
         </ref>
         <ref id="d1840e984a1310">
            <mixed-citation id="d1840e988" publication-type="other">
Ashraf, Nava, Dean Karlan, and Wesley Yin. 2006. "Tying Odysseus to the Mast: Evidence from a
Commitment Savings Product in the Philippines." Quarterly Journal of Economics 121 (2): 635-72.</mixed-citation>
         </ref>
         <ref id="d1840e998a1310">
            <mixed-citation id="d1840e1002" publication-type="other">
Augenblick, Ned, Muriel Niederle, and Charles Sprenger. 2013. "Working Over Time: Dynamic
Inconsistency in Real Effort Tasks." National Bureau of Economic Research (NBER) Working
Paper 18734.</mixed-citation>
         </ref>
         <ref id="d1840e1015a1310">
            <mixed-citation id="d1840e1019" publication-type="other">
Babcock, Philip, Kelly Bedard, Gary Charness, John Hartman, and Heather Royer. 2011. "Letting
Down the Team? Evidence of Social Effects of Team Incentives." National Bureau of Economic
Research (NBER) Working Paper 16687.</mixed-citation>
         </ref>
         <ref id="d1840e1033a1310">
            <mixed-citation id="d1840e1037" publication-type="other">
Babcock, Philip S., and John L. Hartman. 2010. "Networks and Workouts: Treatment Size and Status
Specific Peer Effects in a Randomized Field Experiment." National Bureau of Economic Research
(NBER) Working Paper 16581.</mixed-citation>
         </ref>
         <ref id="d1840e1050a1310">
            <mixed-citation id="d1840e1054" publication-type="other">
Baicker, Katherine, David Cutler, and Zirui Song. 2010. "Workplace Wellness Programs Can Generate
Savings." Health Affairs 29 (2): 304-11.</mixed-citation>
         </ref>
         <ref id="d1840e1064a1310">
            <mixed-citation id="d1840e1068" publication-type="other">
Baumeister, Roy F., Ellen Bratslavsky, Mark Muraven, and Dianne M. Tice. 1998. "Ego Depletion: Is
the Active Self a Limited Resource?" Journal of Personality and Social Psychology 74 (5): 1252-65.</mixed-citation>
         </ref>
         <ref id="d1840e1078a1310">
            <mixed-citation id="d1840e1082" publication-type="other">
Baumeister, Roy F., Mark Muraven, and Dianne M. Tice. 2000. "Ego Depletion: A Resource Model of
Volition, Self-Regulation, and Controlled Processing." Social Cognition 18 (2): 130-50.</mixed-citation>
         </ref>
         <ref id="d1840e1092a1310">
            <mixed-citation id="d1840e1096" publication-type="other">
Benartzi, Shiomo, and Richard Thaler. 2004. "Save More Tomorrow: Using Behavioral Economics to
Increase Employee Saving." Journal of Political Economy 112(1): S164—87.</mixed-citation>
         </ref>
         <ref id="d1840e1106a1310">
            <mixed-citation id="d1840e1110" publication-type="other">
Beshears, John, James J. Choi, David Laibson, Brigitte C. Madrian, and Jung Sakong. 2011. "Self
Control and Liquidity: How to Design a Commitment Contract." RAND Working Paper Series
WR-895-SSA.</mixed-citation>
         </ref>
         <ref id="d1840e1124a1310">
            <mixed-citation id="d1840e1128" publication-type="other">
Brune, Lasse, Xavier Giné, Jessica Goldberg, and Dean Yang. 2013. "Commitments to Save: A Field
Experiment in Rural Malawi." World Bank Policy Research Working Paper 5748.</mixed-citation>
         </ref>
         <ref id="d1840e1138a1310">
            <mixed-citation id="d1840e1142" publication-type="other">
Bryan, Gharad, Dean Karlan, and Scott Nelson. 2010. "Commitment Devices." Annual Review of Eco-
nomics 2: 671-98.</mixed-citation>
         </ref>
         <ref id="d1840e1152a1310">
            <mixed-citation id="d1840e1156" publication-type="other">
Card, David, Alexandre Mas, Enrico Moretti, and Emmanuel Saez. 2012. "Inequality at Work: The
Effect of Peer Salaries on Job Satisfaction." American Economic Review 102 (6): 2981-3003.</mixed-citation>
         </ref>
         <ref id="d1840e1166a1310">
            <mixed-citation id="d1840e1170" publication-type="other">
Cawley, John, and Joshua A. Price. 2013. "A Case Study of a Workplace Wellness Program That Offers
Financial Incentives for Weight Loss." Journal of Health Economics 32 (5): 794-803.</mixed-citation>
         </ref>
         <ref id="d1840e1180a1310">
            <mixed-citation id="d1840e1184" publication-type="other">
Charness, Gary, and Uri Gneezy. 2009. "Incentives to Exercise." Econometrics 77 (3): 909-31.</mixed-citation>
         </ref>
         <ref id="d1840e1191a1310">
            <mixed-citation id="d1840e1195" publication-type="other">
Cox, D. R. 1958. Planning of Experiments. New York: John Wiley and Sons.</mixed-citation>
         </ref>
         <ref id="d1840e1203a1310">
            <mixed-citation id="d1840e1207" publication-type="other">
DeilaVigna, Stefano, and Ulrike Malmendier. 2006. "Paying Not to Go to the Gym." American Eco-
nomic Review 96 (3): 694-719.</mixed-citation>
         </ref>
         <ref id="d1840e1217a1310">
            <mixed-citation id="d1840e1221" publication-type="other">
Finkelstein, Eric A., Laura A. Linnan, Deborah F. Tate, and Ben E. Birken. 2007. "A Pilot Study
Testing the Effect of Different Levels of Financial Incentives on Weight Loss among Overweight
Employees." Journal of Occupational and Environmental Medicine 49 (9): 981-89.</mixed-citation>
         </ref>
         <ref id="d1840e1234a1310">
            <mixed-citation id="d1840e1238" publication-type="other">
Finkelstein, Eric A., Justin G. Trogdon, Joel W. Cohen and William Dietz. 2009. "Annual Medical Spend-
ing Attributable To Obesity: Payer-And Service-Specific Estimates." Health Affairs 28 (5) : w822-31.</mixed-citation>
         </ref>
         <ref id="d1840e1248a1310">
            <mixed-citation id="d1840e1252" publication-type="other">
Fryer, Jr., Roland. 2011. "Financial Incentives and Student Achievement: Evidence from Randomized
Trials." Quarterly Journal of Economics 126 (4): 1755-98.</mixed-citation>
         </ref>
         <ref id="d1840e1262a1310">
            <mixed-citation id="d1840e1266" publication-type="other">
Giné, Xavier, Jessica Goldberg, Dan Silverman, and Dean Yang. 2012. "Revising Commitments: Field
Evidence on the Adjustment of Prior Choices." National Bureau of Economic Research (NBER)
Working Paper 18065.</mixed-citation>
         </ref>
         <ref id="d1840e1279a1310">
            <mixed-citation id="d1840e1283" publication-type="other">
Giné, Xavier, Dean Karlan, and Jonathan Zinman. 2010. "Put Your Money Where Your Butt Is: A
Commitment Contract for Smoking Cessation." American Economic Journal: Applied Economics
2 (4): 213-35.</mixed-citation>
         </ref>
         <ref id="d1840e1297a1310">
            <mixed-citation id="d1840e1301" publication-type="other">
Gneezy, Uri, Stephen Meier, and Pedro Rey-Biel. 2011. "When and Why Incentives (Don't) Work to
Modify Behavior." Journal of Economic Perspectives 25 (4): 191-209.</mixed-citation>
         </ref>
         <ref id="d1840e1311a1310">
            <mixed-citation id="d1840e1315" publication-type="other">
Goldhaber-Feibert, Jeremy, Erik Blumenkranz, and Alan M. Garber. 2010. "Committing to Exercise:
Contract Design for Virtuous Habit Formation." National Bureau of Economic Research (NBER)
Working Paper 16624.</mixed-citation>
         </ref>
         <ref id="d1840e1328a1310">
            <mixed-citation id="d1840e1332" publication-type="other">
Jacobson, Bert H., and Steven G. Aldana. 2001. "Relationship Between Frequency of Aerobic Activity
and Illness-Related Absenteeism in a Large Employee Sample." Journal of Occupational and Envi-
ronmental Medicine 43 (12): 1019-25.</mixed-citation>
         </ref>
         <ref id="d1840e1345a1310">
            <mixed-citation id="d1840e1349" publication-type="other">
Jeffery, Robert W., Wendy L. Hellerstedt, and Thomas L. Schmid. 1990. "Correspondence programs
for smoking cessation and weight control: A comparison of two strategies in the Minnesota Heart
Health Program." Health Psychology 9 (5): 585-98.</mixed-citation>
         </ref>
         <ref id="d1840e1362a1310">
            <mixed-citation id="d1840e1366" publication-type="other">
John, Leslie K., George Loewenstein, Andrea B. Troxel, Laurie Norton, Jennifer E. Fassbender, and
Kevin G. Volpp. 2011. "Financial Incentives for Extended Weight Loss: A Randomized, Controlled
Trial." Journal of General Internal Medicine 26 (6): 621-26.</mixed-citation>
         </ref>
         <ref id="d1840e1379a1310">
            <mixed-citation id="d1840e1383" publication-type="other">
Just, David R., and Joseph Price. 2013. "Using Incentives to Encourage Healthy Eating in Children."
Journal of Human Resources 48 (4): 855-72.</mixed-citation>
         </ref>
         <ref id="d1840e1394a1310">
            <mixed-citation id="d1840e1398" publication-type="other">
Karlan, Dean, and Leigh L. Linden. 2014. "Loose Knots: Strong versus Weak Commitments to Save
for Education in Uganda." National Bureau of Economic Research (NBER) Working Paper 19863.</mixed-citation>
         </ref>
         <ref id="d1840e1408a1310">
            <mixed-citation id="d1840e1412" publication-type="other">
Kaur, Supreet, Michael Kremer, and Sendhil Mullainathan. Forthcoming. "Self-Control at Work."
Journal of Political Economy.</mixed-citation>
         </ref>
         <ref id="d1840e1422a1310">
            <mixed-citation id="d1840e1426" publication-type="other">
Laibson, David. 1997. "Golden Eggs and Hyperbolic Discounting." Quarterly Journal of Economics
112 (2): 443-78.</mixed-citation>
         </ref>
         <ref id="d1840e1436a1310">
            <mixed-citation id="d1840e1440" publication-type="other">
List, John A. 2009. "An introduction to field experiments in economics." Journal of Economic Behav-
ior and Organization 70 (3): 439-42.</mixed-citation>
         </ref>
         <ref id="d1840e1450a1310">
            <mixed-citation id="d1840e1454" publication-type="other">
Loewenstein, George, Ted O'Donoghue, and Matthew Rabin. 2003. "Projection Bias in Predicting
Future Utility." Quarterly Journal of Economics 118 (4): 1209^48.</mixed-citation>
         </ref>
         <ref id="d1840e1464a1310">
            <mixed-citation id="d1840e1468" publication-type="other">
Milkman, Katherine L., Julia A. Minson, and Kevin G. M. Volpp. 2014. "Holding the Hunger Games
Hostage at the Gym: An Evaluation of Temptation Bundling." Management Science 60 (2): 283-99.</mixed-citation>
         </ref>
         <ref id="d1840e1479a1310">
            <mixed-citation id="d1840e1483" publication-type="other">
O'Donoghue, Ted, and Matthew Rabin. 1999. "Doing It Now or Later." American Economic Review
89 (1): 103-24.</mixed-citation>
         </ref>
         <ref id="d1840e1493a1310">
            <mixed-citation id="d1840e1497" publication-type="other">
O'Donoghue, Ted, and Matthew Rabin. 2001. "Choice and Procrastination." Quarterly Journal of Eco-
nomics 116 (1): 121-60.</mixed-citation>
         </ref>
         <ref id="d1840e1507a1310">
            <mixed-citation id="d1840e1511" publication-type="other">
Ozdenoren, Emre, Stephen W. Salant, and Daniel Silverman. 2012. "Willpower and the Optimal Con-
trol of Visceral Urges." Journal of the European Economic Association 10 (2): 342-68.</mixed-citation>
         </ref>
         <ref id="d1840e1521a1310">
            <mixed-citation id="d1840e1525" publication-type="other">
Royer, Heather, Mark Stehr, and Justin Sydnor. 2015. "Incentives, Commitments, and Habit Forma-
tion in Exercise: Evidence from a Field Experiment with Workers at a Fortune-500 Company: Data-
set." American Economic Journal: Applied Economics, http://dx.doi.org/10.1257/app.20130327.</mixed-citation>
         </ref>
         <ref id="d1840e1538a1310">
            <mixed-citation id="d1840e1542" publication-type="other">
Strotz, R. H. 1955-56. "Myopia and Inconsistency in Dynamic Utility Maximization." Review of Eco-
nomic Studies 23 (3): 165-80.</mixed-citation>
         </ref>
         <ref id="d1840e1552a1310">
            <mixed-citation id="d1840e1556" publication-type="other">
Volpp, Kevin G., Leslie K. John, Andrea B. Troxel, Laurie Norton, Jennifer Fassbender, and George
Loewenstein. 2008. "Financial Incentive-Based Approaches for Weight Loss: A Randomized Trial."
Journal of American Medical Association 300 (22): 2631-37.</mixed-citation>
         </ref>
         <ref id="d1840e1570a1310">
            <mixed-citation id="d1840e1574" publication-type="other">
Volpp, Kevin G., Mark V. Pauly, George Loewenstein, and David Bangsberg. 2009a. "An Agenda for
Research on Pay-For-Performance For Patients." Health Affairs 28 (1): 206-14.</mixed-citation>
         </ref>
         <ref id="d1840e1584a1310">
            <mixed-citation id="d1840e1588" publication-type="other">
Volpp, Kevin G., Andrea B. Troxel, Mark V. Pauly, Henry A. Click, Andrea Puig, David A. Asch, Rob-
ert Galvin, et al. 2009b. "A Randomized, Controlled Trial of Financial Incentives for Smoking Ces-
sation." New England Journal of Medicine 360 (7): 699-709.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
