<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">curranth</journal-id>
         <journal-title-group>
            <journal-title>Current Anthropology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00113204</issn>
         <issn pub-type="epub">15375382</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor-stable">2744363</article-id>
         <article-id pub-id-type="doi">10.1086/204641</article-id>
         <article-id pub-id-type="msid">CAv38p477</article-id>
         <title-group>
            <article-title>The World's Crop Genetic Resources and the Rights of Indigenous Farmers<xref ref-type="fn" rid="FN1">1</xref>
            </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>David A.</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Cleveland</surname>
               </string-name>
               <x xml:space="preserve"/>
            </contrib>
            <aff id="aff_1">Assistant Professor in the Department of Anthropology and the Environmental Studies Program at the University of California, Santa Barbara (Santa Barbara, Calif. 93106–3210), and also Co‐Director of the Center for People, Food, and Environment in Tucson, Ariz. He has a Ph.D. in anthropology and an M.S. in genetics from the University of Arizona. His main interests are cultural and biological diversity and their relationship to stability and productivity in the development of small‐scale sustainable farming. He has done research with Kusasi farmers in northeastern Ghana, with Hopi and Zuni farmers in the U.S. Southwest, and with farmers on an irrigation project in northern Pakistan. He is currently beginning research with small‐scale farmers in Oaxaca, Mexico, on the role of yield stability in farmer management of maize varieties and household risk, with the goal of identifying ways for farmers to work with formal plant breeders in developing improved varieties.</aff>
            <x xml:space="preserve"/>
         </contrib-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Stephen C.</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Murray</surname>
               </string-name>
               <x xml:space="preserve"/>
            </contrib>
            <aff id="aff_2">Ph.D. candidate in the Department of Anthropology at the University of California, Santa Barbara. He holds an M.A. in international relations from the Fletcher School of Law and Diplomacy and received his B.A. from Amherst College. He has taught and served in educational administration in Micronesia and the Middle East. His interests include the feasibility of sustainable tourism in small island societies, traditional navigational systems of the Pacific, and the role of anthropology in development.</aff>
            <x xml:space="preserve"/>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>08</month>
            <year>1997</year>
            <string-date>August/October 1997</string-date>
         </pub-date>
         <volume>38</volume>
         <issue>4</issue>
         <issue-id>ca.1997.38.issue-4</issue-id>
         <fpage>477</fpage>
         <lpage>516</lpage>
         <permissions>
            <copyright-statement>© 1997 by The Wenner‐Gren Foundation for Anthropological Research. All rights reserved</copyright-statement>
            <copyright-year>1997</copyright-year>
            <copyright-holder>The Wenner‐Gren Foundation for Anthropological Research</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/204641"/>
         <abstract>
            <p>Farmer or folk crop varieties developed over many generations by indigenous farmers are an important component of global crop genetic resources for use by both industrial and indigenous agriculture. Currently there is a debate between advocates of indigenous farmers' rights in their folk varieties and the dominant world system, which vests intellectual property rights to crop genetic resources only in users of those resources for industrial agriculture. While indigenous peoples at the individual and group levels do have a broad range of intellectual property rights in their folk varieties, they define and use them differently than does the industrial world. Therefore, industrial‐world intellectual propery rights mechanisms are generally inappropriate for protecting the intellectual property rights of indigenous farmers, but some could be used effectively. To meet indigenous farmers' need for protection, new approaches are being developed that embed indigenous farmers' rights in folk varieties in cultural, human, and environmental rights. More research on the cultural, social, and agronomic roles of folk varieties, ongoing negotiation of the meaning of key concepts such as “crop genetic resources,” “rights,” and “indigenous,” and an emphasis on a common goal of sustainability will help to resolve the debate.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="FN1">
               <label>1</label>
               <p>We thank Eve Darian‐Smith, Hirmina Murray, Hope Shand, Daniela Soleri, Philip Walker, and especially Brian Haley, as well as Richard Fox and four anonymous referees for this journal, for helpful comments on drafts of this paper; Brenda Bowser, Mac Chapin, Patricia Cummings, David Downes, Hurst Hannum, Thomas Harding, and Janet McGowan for discussions of intellectual property rights and indigenous peoples and/or sharing of references; and Melanie Rhodehamel and Amy Sabbadini for research assistance. Cleveland thanks Zuni and Hopi farmers and religious and tribal leaders for discussions about and insights into rights in folk varieties and their roles in agriculture. Research was funded in part by a grant to Cleveland from the University of California‐Santa Barbara Committee on Research of the Faculty Senate. We as authors are fully responsible for this paper.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
</article>
