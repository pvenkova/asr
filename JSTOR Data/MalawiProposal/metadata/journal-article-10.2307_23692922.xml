<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jexperbota</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009630</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Experimental Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00220957</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14602431</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23692922</article-id>
         <title-group>
            <article-title>An Intermediate Category of Seed Storage Behaviour? II. EFFECTS OF PROVENANCE, IMMATURITY, AND IMBIBITION ON DESICCATION-TOLERANCE IN COFFEE</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>R. H.</given-names>
                  <surname>ELLIS</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>T. D.</given-names>
                  <surname>HONG</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>E. H.</given-names>
                  <surname>ROBERTS</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>1991</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">42</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">238</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23691997</issue-id>
         <fpage>653</fpage>
         <lpage>657</lpage>
         <permissions>
            <copyright-statement>© Oxford University Press 1991</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23692922"/>
         <abstract>
            <p>Seeds of four cultivars of arabica coffee (Coffea arabica L.) received from three continents survived desiccation to between 7.2% and 11.3% moisture content (wet basis), i.e. to seed water potentials of -90 MPa to -150 MPa, but further desiccation reduced germination (criterion, normal seedling development) in all seed lots. Only a few individuals from four of the lots germinated after being dried to 4—5% moisture content. Differences in desiccation sensitivity were apparent among lots within each cultivar. Desiccation sensitivity in these lots was similar to that observed in seeds of orthodox species which have begun to germinate. Seeds extracted from fruits of intermediate maturity (yellow) were able to tolerate greater desiccation than those from either ripe (red) or immature (green) fruits. Imbibed storage increased desiccation sensitivity. The results are compatible with the view that arabica coffee seeds are unable to tolerate extreme desiccation because germination has been initiated before harvest.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d232e174a1310">
            <mixed-citation id="d232e178" publication-type="other">
Berjak, P., Farrant, J. M., Mycock, D. J., and Pammenter,
N. W., 1990a. Recalcitrant (homoiohydrous) seeds: the en-
igma of their desiccation-sensitivity. Seed Science and Techno-
logy, 18, 297-310.</mixed-citation>
         </ref>
         <ref id="d232e194a1310">
            <mixed-citation id="d232e198" publication-type="other">
- - and Pammenter, N. W., 1990b. The basis of recalcit-
rant seed behaviour. In Recent advances in the development
and germination of seeds. Ed. R. B. Taylorson. Plenum Press,
New York. Pp. 89-108.</mixed-citation>
         </ref>
         <ref id="d232e214a1310">
            <mixed-citation id="d232e218" publication-type="other">
Dasgupta, J., Bewley, J. D., and Yeung, E. C., 1982. Desicca-
tion-tolerant and desiccation-intolerant stages during the de-
velopment and germination of Phaseolus vulgaris seeds.
Journal of Experimental Botany, 136, 1045-57.</mixed-citation>
         </ref>
         <ref id="d232e234a1310">
            <mixed-citation id="d232e238" publication-type="other">
Ellis, R. H., Hong, T. D., and Roberts, E. H., 1989. A
comparison of the low-moisture-content limit to the logarith-
mic relation between seed moisture and longevity in twelve
species. Annals of Botany, 63, 601-11.</mixed-citation>
         </ref>
         <ref id="d232e255a1310">
            <mixed-citation id="d232e259" publication-type="other">
- - - 1990. An intermediate category of seed storage
behaviour? I. Coffee. Journal of Experimental Botany, 41,
1167-74.</mixed-citation>
         </ref>
         <ref id="d232e272a1310">
            <mixed-citation id="d232e278" publication-type="other">
- - - 1991. Effect of storage temperature and moisture
on the germination of papaya seeds. Seed Science Research
(in press).</mixed-citation>
         </ref>
         <ref id="d232e291a1310">
            <mixed-citation id="d232e295" publication-type="other">
Farrant, J. M., Pammenter, N. W., and Berjak, P., 1986. The
increasing desiccation sensitivity of recalcitrant Avicennia
marina seeds. Physiologia plantarum, 67, 291-8.</mixed-citation>
         </ref>
         <ref id="d232e308a1310">
            <mixed-citation id="d232e312" publication-type="other">
- - - 1988. Recalcitrance—a current assessment. Seed
Science and Technology, 16, 155-66.</mixed-citation>
         </ref>
         <ref id="d232e322a1310">
            <mixed-citation id="d232e326" publication-type="other">
Finch-Savage, W. E., and McKee, J. M. T., 1989. Viability of
rape (Brassica napus L.) seeds following selection on the basis
of newly-emerged radicles then subsequent drying and storage.
Annals of Applied Biology, 114, 587-95.</mixed-citation>
         </ref>
         <ref id="d232e342a1310">
            <mixed-citation id="d232e346" publication-type="other">
Hong, T. D., and Ellis, R. H., 1990. A comparison of
maturation-drying, germination, and desiccation-tolerance
between developing seeds of Acer pseudoplatanus L. and Acer
platanoides L. New Phytologist, 116, 589-96.</mixed-citation>
         </ref>
         <ref id="d232e363a1310">
            <mixed-citation id="d232e367" publication-type="other">
International Seed Testing Association, 1985a. International
rules for seed testing. Rules 1985. Seed Science and Techno-
logy, 13, 299-355.</mixed-citation>
         </ref>
         <ref id="d232e380a1310">
            <mixed-citation id="d232e384" publication-type="other">
- 1985¿?. International rules for seed testing. Annexes 1985.
Ibid. 13, 356-513.</mixed-citation>
         </ref>
         <ref id="d232e394a1310">
            <mixed-citation id="d232e398" publication-type="other">
McKersie, B. D., and Stinson, R. H., 1980. Effect of dehydra-
tion on leakage and membrane structure in Lotus corniculatus
L. seeds. Plant Physiology, 66, 316-20.</mixed-citation>
         </ref>
         <ref id="d232e411a1310">
            <mixed-citation id="d232e415" publication-type="other">
- and Tomes, D. T., 1980. Effects of dehydration treatments
on germination, seedling vigour, and cytoplasmic leakage in
wild oats and birdsfoot trefoil. Canadian Journal of Botany,
58, 471-6.</mixed-citation>
         </ref>
         <ref id="d232e431a1310">
            <mixed-citation id="d232e435" publication-type="other">
Nemmer, M. W., and Luyet, B. J., 1954. Survival of dehydrated
pea seedlings. Biodynamica, 7, 193-211.</mixed-citation>
         </ref>
         <ref id="d232e445a1310">
            <mixed-citation id="d232e449" publication-type="other">
Osei-Bonsu, K., Opuku-Ameyaw, K., Amoah, F. M., and
Acheampong, K., 1989. Coffee seed germination. I. Effect of
ripening and processing on coffee (Cojfea canephora) seed
germination. Café Cacao Thé, 33, 219-22.</mixed-citation>
         </ref>
         <ref id="d232e466a1310">
            <mixed-citation id="d232e470" publication-type="other">
Roberts, E. H., 1973. Predicting the storage life of seeds. Seed
Science and Technology, 1, 499-514.</mixed-citation>
         </ref>
         <ref id="d232e480a1310">
            <mixed-citation id="d232e484" publication-type="other">
- and Ellis, R. H., 1989. Water and seed survival. Annals
of Botany, 63, 39-52.</mixed-citation>
         </ref>
         <ref id="d232e494a1310">
            <mixed-citation id="d232e498" publication-type="other">
Senaratna, T., and McKersie, B. D., 1983. Dehydration injury
in germinating soybean (Glycine max L. Merr.) seeds. Plant
Physiology, 72, 620-4.</mixed-citation>
         </ref>
         <ref id="d232e511a1310">
            <mixed-citation id="d232e515" publication-type="other">
Sun, C. N., 1958. The survival of excised pea seedlings after
drying and freezing in liquid nitrogen. Botanical Gazette, 119,
234-6.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
