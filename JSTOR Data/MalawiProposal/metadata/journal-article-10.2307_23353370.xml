<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23353370</article-id>
         <title-group>
            <article-title>Polygny among the Tsimane of Bolivia: an improved method for testing the polygyny—fertility hypothesis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeffrey</given-names>
                  <surname>Winking</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jonathan</given-names>
                  <surname>Stieglitz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jenna</given-names>
                  <surname>Kurten</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Hillard</given-names>
                  <surname>Kaplan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael</given-names>
                  <surname>Gurven</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>7</day>
            <month>4</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">280</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1756</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23353340</issue-id>
         <fpage>1</fpage>
         <lpage>7</lpage>
         <permissions>
            <copyright-statement>Copyright © 2013 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1098/rspb.2012.3078"
                   xlink:title="an external site"/>
         <abstract>
            <p>The polygyny—fertility hypothesis states that polygyny is associated with reduced fertility for women and is supported by a large body of literature. This finding is important, because theoretical models of polygyny often differentiate systems based on the degree to which women are forced or willingly choose to enter polygynous marriages. The fact that polygyny tends to be associated with reduced fertility has been presented as evidence that polygyny is often less favourable for women, and that women must, therefore, be pressured into accepting such arrangements. Previous studies, however, have been hampered by the non-random assignment of women into monogamous and polygynous unions (i.e. self-selection), as differences between these groups of women might explain some of the effects. Furthermore, the vast majority of such studies focus on sub-Saharan populations. We address these problems in our analysis of women's fertility in polygynous marriages among the Tsimane of Bolivia. We offer a more robust method for assessing the impact of polygynous marriage on reproductive outcomes by testing for intra-individual fertility effects among first wives as they transition from monogamous to polygynous marriage. We report a significant link between polygyny and reduced fertility when including all cases of polygyny; however, this association disappears when testing only for intra-individual effects.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d420e193a1310">
            <label>1</label>
            <mixed-citation id="d420e200" publication-type="other">
Gibson MA, Mace R. 2007 Polygyny, reproductive
success and child health in rural Ethiopia: why
marry a married man? J. Biosoc. Sci. 39, 287–300.
( d oi : 10.1017/s0021932006001441 )</mixed-citation>
         </ref>
         <ref id="d420e216a1310">
            <label>2</label>
            <mixed-citation id="d420e223" publication-type="other">
Josephson S. 2000 Polygyny, fertility, and human
reproductive strategies. Utah, PhD dissertation,
Department of Anthropology, University of Utah,
Salt Lake City.</mixed-citation>
         </ref>
         <ref id="d420e239a1310">
            <label>3</label>
            <mixed-citation id="d420e246" publication-type="other">
Ellison PT. 1994 Advances in human reproductive
ecology. Annu. Rev. Anthropol. 23, 255–275.
(doi:10.1146/annurev.anthro.23.1.255)</mixed-citation>
         </ref>
         <ref id="d420e259a1310">
            <label>4</label>
            <mixed-citation id="d420e266" publication-type="other">
Strassman B. 1997 Polygyny as a risk factor for child
mortality among the Dogon. Curr. Anthropol. 38,
688–695. (doi:10.1086/204657)</mixed-citation>
         </ref>
         <ref id="d420e280a1310">
            <label>5</label>
            <mixed-citation id="d420e287" publication-type="other">
Hern WM. 1992 Polygyny and fertility among the
Shipibo of the Peruvian Amazon. Popul.
Stud. J. Demogr. 46, 53 – 64. (doi:10.1080/
0032472031000146006)</mixed-citation>
         </ref>
         <ref id="d420e303a1310">
            <label>6</label>
            <mixed-citation id="d420e310" publication-type="other">
White DR. 1988 Rethinking polygyny: co-wives,
codes and cultural systems. Curr. Anthropol. 29,
529–558. (doi:10.1086/203674)</mixed-citation>
         </ref>
         <ref id="d420e323a1310">
            <label>7</label>
            <mixed-citation id="d420e330" publication-type="other">
Chisolm JS, Burbank VK. 1991 Monogamy and
polygyny in southeast Arnhem Land: male cercion
and female choice. Ethol. Socioblol. 12,291–313.
(doi:10.1016/0162-3095(91 )90022-l)</mixed-citation>
         </ref>
         <ref id="d420e346a1310">
            <label>8</label>
            <mixed-citation id="d420e353" publication-type="other">
Mulder MB. 1990 Kipsigis women's preferences for
weatlhy men: evidence for female choice in
mammals. Behav. Ecol. Sociobiol. 27, 255 – 264.
(doi:10.1007/BF00164S97)</mixed-citation>
         </ref>
         <ref id="d420e369a1310">
            <label>9</label>
            <mixed-citation id="d420e376" publication-type="other">
Mulder MB, 1985 Polygyny threshold: a Kipsigis
case study. Natl. Geogr. 21,33–39.</mixed-citation>
         </ref>
         <ref id="d420e386a1310">
            <label>10</label>
            <mixed-citation id="d420e393" publication-type="other">
Orians GH. 1969 On the evolution of mating systems
in birds and mammals. Am. Nat. 103, 589–603.
(doi:10.1086/282628)</mixed-citation>
         </ref>
         <ref id="d420e407a1310">
            <label>11</label>
            <mixed-citation id="d420e414" publication-type="other">
Verner J, Willson MF. 1966 The influence of habitats
on mating systems of North American birds. Ecology
47, 143–147. (doi:10.2307/1935753)</mixed-citation>
         </ref>
         <ref id="d420e427a1310">
            <label>12</label>
            <mixed-citation id="d420e434" publication-type="other">
Sellen DW, Hruschka DJ. 2004 Extracted-food
resource-defense polygyny in native western North
American societies at contact. Curr. Anthropol. 45,
707 – 714. (doi:10.1086/425637)</mixed-citation>
         </ref>
         <ref id="d420e450a1310">
            <label>13</label>
            <mixed-citation id="d420e457" publication-type="other">
White DR, Burton ML. 1988 Causes of polygyny:
ecology, economy, kinship, and warfare. Am.
Anthropol. 90, 871–887. (doi:10.1525/aa.1988.90.
4.02a00060)</mixed-citation>
         </ref>
         <ref id="d420e473a1310">
            <label>14</label>
            <mixed-citation id="d420e482" publication-type="other">
Sellen DW, Mulder MB, Sieff DF. 2000 Fertility,
offspring quality, and wealth in Datoga pastoralists:
testing evolutionary models of intersexual selection.
In Adaptation and human behavior: an
anthropological perspective (eds L Cronk,
Ν Chagnon, W Irons), pp. 91–114. New York, NY:
Aldien de Gruyter.</mixed-citation>
         </ref>
         <ref id="d420e508a1310">
            <label>15</label>
            <mixed-citation id="d420e515" publication-type="other">
Reniers 6, Tfaily R. 2008 Polygyny and HIV in
Malawi. Demogr. Res. 10, 1811–1830. (doi:10.
4054/Dem Res.2008.19.53)</mixed-citation>
         </ref>
         <ref id="d420e528a1310">
            <label>16</label>
            <mixed-citation id="d420e535" publication-type="other">
Canadian Press. 2010 Abuse not unique to
polygamy. See http://www.cbc.ca/news/canada/
british-columbia/story/2010/12/15/bc-polygamy-
hearing.html.</mixed-citation>
         </ref>
         <ref id="d420e552a1310">
            <label>17</label>
            <mixed-citation id="d420e559" publication-type="other">
Bean LL, Mineau GP. 1986 The polygyny fertility
hypothesis: a réévaluation. Popul. Stud. 40, 67–81.
(doi :10.1080/0032472031000141846)</mixed-citation>
         </ref>
         <ref id="d420e572a1310">
            <label>18</label>
            <mixed-citation id="d420e579" publication-type="other">
Hadley C. 2005 Is polygyny a risk factor for
poor growth performance among Tanzanian
agropastoralists? Am. J. Phys. Anthropol. 126,
471–480. (doi:10.1002/ajpa.20068)</mixed-citation>
         </ref>
         <ref id="d420e595a1310">
            <label>19</label>
            <mixed-citation id="d420e602" publication-type="other">
Amey FK. 2002 Polygyny and child survival in
sub-Saharan Africa. Soc. Biol. 49, 74– 89.</mixed-citation>
         </ref>
         <ref id="d420e612a1310">
            <label>20</label>
            <mixed-citation id="d420e619" publication-type="other">
Lardoux S, Van de Walle E. 2003 Polygyny and
fertility in rural Senegal. Population 58, 807 – 835.
(doi:10.2307/3271283)</mixed-citation>
         </ref>
         <ref id="d420e632a1310">
            <label>21</label>
            <mixed-citation id="d420e639" publication-type="other">
Sueyoshi S, Ohtsuka R. 2003 Effects of polygyny and
consanguinity on high fertility in the rural Arab
population in South Jordan. J. Biosoc. Sci. 35,
513–526. (doi:10.1017/s0021932003005911)</mixed-citation>
         </ref>
         <ref id="d420e655a1310">
            <label>22</label>
            <mixed-citation id="d420e662" publication-type="other">
Effah KB. 1999 A reformulation of the polygyny—
fertility hypothesis. J. Comp. Fam. Stud. 30, 381.</mixed-citation>
         </ref>
         <ref id="d420e673a1310">
            <label>23</label>
            <mixed-citation id="d420e680" publication-type="other">
Sear R, Mace R, McGregor IA. 2003 The effects of kin
on female fertility in rural Gambia. Evol. Hum. Behav.
24, 99–112. (doi:10.1016/S1090-5138(02) 00122-8)</mixed-citation>
         </ref>
         <ref id="d420e693a1310">
            <label>24</label>
            <mixed-citation id="d420e700" publication-type="other">
Musham HV. 1956 The fertility of polygynous
marriages. Popul. Stud. 10,3–16.</mixed-citation>
         </ref>
         <ref id="d420e710a1310">
            <label>25</label>
            <mixed-citation id="d420e717" publication-type="other">
Bongaarts J, Frank 0, Lesthaeghe R. 1984 The
proximate determinants of fertility in sub-Saharan
Africa. Popul. Dev. Rev. 10, 511–537. (doi:10.2307/
1973518)</mixed-citation>
         </ref>
         <ref id="d420e733a1310">
            <label>26</label>
            <mixed-citation id="d420e740" publication-type="other">
Brabin L. 1984 Polygyny: an indicator of nutritional
stress in African agricultural societies. Africa 54,
31–45. (doi:10.2307/1160142)</mixed-citation>
         </ref>
         <ref id="d420e753a1310">
            <label>27</label>
            <mixed-citation id="d420e760" publication-type="other">
Mulder MB. 1989 Marital-status and reproductive-
performance in Kipsigis women: re-evaluating the
polygyny fertility hypothesis. Popul. Stud. 43,
285–304. (doi:10.1080/0032472031000144126)</mixed-citation>
         </ref>
         <ref id="d420e776a1310">
            <label>28</label>
            <mixed-citation id="d420e783" publication-type="other">
Josephson S. 2002 Does polygyny reduce fertility? Am.
J. Hum. Biol. 14, 222–232. (doi:10.1002/ajhb,10045)</mixed-citation>
         </ref>
         <ref id="d420e794a1310">
            <label>29</label>
            <mixed-citation id="d420e801" publication-type="other">
Olusanya PO. 1971 Problem of multiple causation
in population analysis, with particular reference
to the polygamy—fertility hypothesis. Socio!. Rev. 19,
165 –178. (doi:10.1111/j.1467-954X.1971,tb00625.x)</mixed-citation>
         </ref>
         <ref id="d420e817a1310">
            <label>30</label>
            <mixed-citation id="d420e826" publication-type="other">
Kalick SM, Zebrowitz LA, Langlois JH, Johnson RM.
1998 Does human facial attractiveness honestly
advertise health? Longitudinal data on an
evolutionary question. Psychol. Sci. 9, 8–13.
(doi:10.1111/1467-9280.00002)</mixed-citation>
         </ref>
         <ref id="d420e845a1310">
            <label>31</label>
            <mixed-citation id="d420e852" publication-type="other">
Udry JR, Eckland BK. 1984 Benefits of being
attractive: differential payoffs for men and
women. Psychol. Rep. 54, 47 –56. (doi:10.2466/
prO. 1984.54.1.47)</mixed-citation>
         </ref>
         <ref id="d420e868a1310">
            <label>32</label>
            <mixed-citation id="d420e875" publication-type="other">
Jaeger MM. 2011 Ά thing of beauty is a joy
forever'? Returns to physical attractiveness over the
life course. Soc. Forces 89, 983–1003.</mixed-citation>
         </ref>
         <ref id="d420e888a1310">
            <label>33</label>
            <mixed-citation id="d420e895" publication-type="other">
Riley AP. 1994 Determinants of adolescent fertility
and its consequences for maternal health, with
special reference to rural Bangladesh. Ann. NY.
Acad. Sci. 709, 86–100.</mixed-citation>
         </ref>
         <ref id="d420e911a1310">
            <label>34</label>
            <mixed-citation id="d420e918" publication-type="other">
Komlos J. 1989 The age of menarche and age at first
birth in an undernourished population. Ann. Hum. Biol.
16, 463 – 466. (doi:10.1080/030144689 00000592)</mixed-citation>
         </ref>
         <ref id="d420e932a1310">
            <label>35</label>
            <mixed-citation id="d420e939" publication-type="other">
Hill K, Hurtado AM. 1996 Ache life history: the
ecology and demography of a foraging people.
New York, NY: Aldine.</mixed-citation>
         </ref>
         <ref id="d420e952a1310">
            <label>36</label>
            <mixed-citation id="d420e959" publication-type="other">
Sear R, Mace R, McGregor IA. 2003 A life-history
analysis of fertility rates in rural Gambia: evidence
for trade-offs or phenotypic correlations? In The
biodemography of human reproduction and fertility
(eds J Rodgers, HP Kohler), pp. 135–160. Boston,
MA: Kluwer Press.</mixed-citation>
         </ref>
         <ref id="d420e982a1310">
            <label>37</label>
            <mixed-citation id="d420e991" publication-type="other">
Mulder MB. 1989 Early maturing Kipsigis women
have higher reproductive success than late maturing
women and cost more to marry. Behav. Ecol.
Sociobiol. 24, 145–153. (doi:10.1007/BF00292097)</mixed-citation>
         </ref>
         <ref id="d420e1007a1310">
            <label>38</label>
            <mixed-citation id="d420e1014" publication-type="other">
Hoogeveen J, van der Klaauw B, van Lomwel G.
2011 On the timing of marriage, cattle, and shocks.
Econ. Dev. Cult Change 60, 121–154. (doi:10.
1086/661215)</mixed-citation>
         </ref>
         <ref id="d420e1030a1310">
            <label>39</label>
            <mixed-citation id="d420e1037" publication-type="other">
Gurven M, Kaplan H, Supa AZ. 2007 Mortality
experience of Tsimane Amerindians: regional
variation and temporal trends. Am. J. Hum. Biol. 19,
376 – 398. (doi:10.1002/ajhb.20600)</mixed-citation>
         </ref>
         <ref id="d420e1053a1310">
            <label>40</label>
            <mixed-citation id="d420e1060" publication-type="other">
Winking J, Gurven M, Kaplan H. 2011 Father death
and adult success among the Tsimane: implications
for marriage and divorce. Evol. Hum. Behav. 32,
79– 89. (doi:10.1016/j.evolhumbehav.2010.08.002)</mixed-citation>
         </ref>
         <ref id="d420e1077a1310">
            <label>41</label>
            <mixed-citation id="d420e1084" publication-type="other">
Gurven M, Winking J, Kaplan H, von Rueden C,
McAllister L. 2009 A bioeconomic approach to
marriage and the sexual division of labor. Hum. Nat.
20, 151–183. (doi:10.1007/s12110-009-9062-8)</mixed-citation>
         </ref>
         <ref id="d420e1100a1310">
            <label>42</label>
            <mixed-citation id="d420e1107" publication-type="other">
Bateman AJ. 1948 Intra-sexual selection in
Drosophila. Heredity 2, 349–368. (doi:10.1038/
hdy.1948.21)</mixed-citation>
         </ref>
         <ref id="d420e1120a1310">
            <label>43</label>
            <mixed-citation id="d420e1127" publication-type="other">
Moorad JA, Promislow DEL, Smith KR, Wade MJ.
2011 Mating system change reduces the strength
of sexual selection in an American frontier
population of the 19th century. Evol. Hum.
Behav. 32, 147–155. (doi:10.1016/j.evolhumbehav.
2010.10.004)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
