<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">poliresequar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100886</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Political Research Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10659129</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23563146</article-id>
         <title-group>
            <article-title>The Effects of Political Institutions on Women's Political Representation: A Comparative Analysis of 168 Countries from 1992 to 2010</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jennifer</given-names>
                  <surname>Rosen</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">66</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23562431</issue-id>
         <fpage>306</fpage>
         <lpage>321</lpage>
         <permissions>
            <copyright-statement>Copyright © 2013 The University of Utah</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23563146"/>
         <abstract>
            <p>Women's political representation exhibits substantial cross-national variation. While mechanisms shaping these variations are well understood for Western democracies, there is little consensus on how these same factors operate in less developed countries. Effects of two political institutions—electoral systems and gender quotas—are tested across 168 countries from 1992 to 2010. Findings indicate that key causal factors interact with a country's socioeconomic development, shifting their importance and possibly even direction at various development thresholds. Generalizing broadly across countries, therefore, does not adequately represent the effects of these political institutions. Rather, different institutional changes are advised to increase women's presence in national governments.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d71e159a1310">
            <label>1</label>
            <mixed-citation id="d71e166" publication-type="other">
Hughes (2009)</mixed-citation>
            <mixed-citation id="d71e172" publication-type="other">
Inglehart
and Norris (2003)</mixed-citation>
         </ref>
         <ref id="d71e182a1310">
            <label>2</label>
            <mixed-citation id="d71e189" publication-type="other">
Diamond 2002</mixed-citation>
            <mixed-citation id="d71e195" publication-type="other">
Hughes 2009</mixed-citation>
         </ref>
         <ref id="d71e202a1310">
            <label>4</label>
            <mixed-citation id="d71e209" publication-type="other">
Bauer and
Britton 2006</mixed-citation>
            <mixed-citation id="d71e218" publication-type="other">
Dahlerup 2006</mixed-citation>
            <mixed-citation id="d71e224" publication-type="other">
Krook 2010</mixed-citation>
         </ref>
         <ref id="d71e231a1310">
            <label>7</label>
            <mixed-citation id="d71e238" publication-type="other">
Paxton and Kunovich (2003).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d71e254a1310">
            <mixed-citation id="d71e258" publication-type="other">
Bauer, Gretchen, and Hannah Britton, eds. 2006. Women in
African parliaments. Boulder, CO: Lynne Rienner.</mixed-citation>
         </ref>
         <ref id="d71e268a1310">
            <mixed-citation id="d71e272" publication-type="other">
Carroll, Susan. 2001. Representing women: State legislators as
agents of policy-related change. In The impact of women in
public office, edited by Susan Carrol, 3-21. Bloomington:
University of Indiana Press.</mixed-citation>
         </ref>
         <ref id="d71e288a1310">
            <mixed-citation id="d71e292" publication-type="other">
Celis, Karen, Sarah Childs, Johanna Kantola, and Mona Lena
Krook. 2008. Rethinking women's substantive representa-
tion. Representation 44 (2): 99-110.</mixed-citation>
         </ref>
         <ref id="d71e305a1310">
            <mixed-citation id="d71e309" publication-type="other">
Central Intelligence Agency. 1992-2010. The World Fact book.
Washington, DC: Central Intelligence Agency.</mixed-citation>
         </ref>
         <ref id="d71e320a1310">
            <mixed-citation id="d71e324" publication-type="other">
Chafetz, Janet Saltzman. 1990. Gender equity: An integrated
theory of stability and change. Newbury Park, CA: Sage.</mixed-citation>
         </ref>
         <ref id="d71e334a1310">
            <mixed-citation id="d71e338" publication-type="other">
Dahlerup, Drude, ed. 2006. Women, quotas and politics. New
York: Routledge Research in Comparative Politics.</mixed-citation>
         </ref>
         <ref id="d71e348a1310">
            <mixed-citation id="d71e352" publication-type="other">
Dahlerup, Drude. 2010. From a small to a large minority:
Women in Scandinavian politics. In Women, gender, and
politics: A reader, edited by Mona Krook and Sarah Childs,
225-30. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d71e368a1310">
            <mixed-citation id="d71e372" publication-type="other">
Dahlerup, Drude, and Lenita Friedenvall. 2005. Quotas as a
"fast track" to equal representation for women: Why Scan-
dinavia is no longer the model. International Feminist Jour-
nal of Politics 7 (1): 26-48.</mixed-citation>
         </ref>
         <ref id="d71e388a1310">
            <mixed-citation id="d71e392" publication-type="other">
Del Campo, Esther. 2005. Women and politics in Latin America:
Perspectives and limits of the institutional aspects of women's
political representation. Social Forces 83 (4): 1697-1725.</mixed-citation>
         </ref>
         <ref id="d71e405a1310">
            <mixed-citation id="d71e409" publication-type="other">
Diamond, Larry Jay. 2002. Thinking about hybrid regimes.
Journal of Democracy 13 (2): 21-35.</mixed-citation>
         </ref>
         <ref id="d71e420a1310">
            <mixed-citation id="d71e424" publication-type="other">
Freedom House. 2012. Freedom in the world country ratings:
1972-2011. Washington, DC: Freedom House.</mixed-citation>
         </ref>
         <ref id="d71e434a1310">
            <mixed-citation id="d71e438" publication-type="other">
Hughes, Melanie. 2009. Armed conflict, international linkages,
and women's parliamentary representation in developing
nations. Social Problems 56 (1): 174-204.</mixed-citation>
         </ref>
         <ref id="d71e451a1310">
            <mixed-citation id="d71e455" publication-type="other">
Hughes, Melanie, and Pamela Paxton. 2007. Familiar theories
from a new perspective: The implications of a longitudinal
approach to women in politics research. Politics and Gender
3 (3): 370-78.</mixed-citation>
         </ref>
         <ref id="d71e471a1310">
            <mixed-citation id="d71e475" publication-type="other">
Inglehart, Ronald, and Pippa Norris. 2003. Rising tide: Gender
equality and cultural change around the world. New York:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d71e488a1310">
            <mixed-citation id="d71e492" publication-type="other">
International IDEA. 2006. Electoral system design. Stockholm,
Sweden: International IDEA.</mixed-citation>
         </ref>
         <ref id="d71e502a1310">
            <mixed-citation id="d71e506" publication-type="other">
Kenworthy, Lane, and Melissa Malami. 1999. Gender inequal-
ity in political representation: A worldwide comparative
analysis. Social Forces 78 (1): 235-68.</mixed-citation>
         </ref>
         <ref id="d71e520a1310">
            <mixed-citation id="d71e524" publication-type="other">
Kittilson, Miki Caul. 2006. Challenging parties, changing
parliaments: Women and elected office in contemporary
Western Europe. Columbus: Ohio State University Press.</mixed-citation>
         </ref>
         <ref id="d71e537a1310">
            <mixed-citation id="d71e541" publication-type="other">
Krook, Mona Lena. 2006. Reforming representation: The dif-
fusion of candidate gender quotas worldwide. Politics and
Gender 2 (3): 303-27.</mixed-citation>
         </ref>
         <ref id="d71e554a1310">
            <mixed-citation id="d71e558" publication-type="other">
Krook, Mona Lena. 2010. Women's representation in parlia-
ment: A qualitative comparative analysis. Political Studies
58 (5): 886-908.</mixed-citation>
         </ref>
         <ref id="d71e571a1310">
            <mixed-citation id="d71e575" publication-type="other">
Lovenduski, Joni, and Pippa Norris. 2004. Westminster women:
The politics of presence. Political Studies 51 (1): 84-102.</mixed-citation>
         </ref>
         <ref id="d71e585a1310">
            <mixed-citation id="d71e589" publication-type="other">
Mahoney, James, and Gary Goertz. 2004. The possibility prin-
ciple: Choosing negative cases in comparative research.
American Political Science Review 98 (4): 653-69.</mixed-citation>
         </ref>
         <ref id="d71e602a1310">
            <mixed-citation id="d71e606" publication-type="other">
Matland, Richard. 1998. Women's representation in national
legislatures: Developed and developing countries. Legisla-
tive Studies Quarterly 23 (1): 109-25.</mixed-citation>
         </ref>
         <ref id="d71e620a1310">
            <mixed-citation id="d71e624" publication-type="other">
Matland, Richard. 2006. Electoral quotas: Frequency and
effectiveness. In Women, quotas and politics, edited by
Drude Dahlerup, 275-92. New York: Routledge Research
in Comparative Politics.</mixed-citation>
         </ref>
         <ref id="d71e640a1310">
            <mixed-citation id="d71e644" publication-type="other">
McConnaughy, Corrine M. 2007. Seeing gender over the short
and long haul. Gender and Politics 3 (3): 378-86.</mixed-citation>
         </ref>
         <ref id="d71e654a1310">
            <mixed-citation id="d71e658" publication-type="other">
Moore, Gwen, and Gene Shackman. 1996. Gender and author-
ity: A cross-national study. Social Science Quarterly 11 (2):
273-88.</mixed-citation>
         </ref>
         <ref id="d71e671a1310">
            <mixed-citation id="d71e675" publication-type="other">
Oakes, Ann, and Elizabeth Almquist. 1993. Women in national
legislature: A cross-national test of macrostructural gender
theories. Population Research Policy and Review 12:71-81.</mixed-citation>
         </ref>
         <ref id="d71e688a1310">
            <mixed-citation id="d71e692" publication-type="other">
Paxton, Pamela. 1997. Women in national legislatures: A cross-
national analysis. Social Science Research 26:442-64.</mixed-citation>
         </ref>
         <ref id="d71e702a1310">
            <mixed-citation id="d71e706" publication-type="other">
Paxton, Pamela, Jennifer Green, and Melanie Hughes. 2008.
Women in parliament, 1945-2003: Cross-national dataset
[Computer file]. ICPSR24340-vl. Ann Arbor: Inter-Univer-
sity Consortium for Political and Social Research.</mixed-citation>
         </ref>
         <ref id="d71e723a1310">
            <mixed-citation id="d71e727" publication-type="other">
Paxton, Pamela, and Melanie Hughes. 2007. Women, politics,
and power: A global perspective. Thousand Oaks, CA: Pine
Forge Press.</mixed-citation>
         </ref>
         <ref id="d71e740a1310">
            <mixed-citation id="d71e744" publication-type="other">
Paxton, Pamela, Melanie M. Hughes, and Matthew A. Painter
II. 2009. Growth in women's political representation: A lon-
gitudinal exploration of democracy, electoral system, and
gender quotas. Working Paper: 09-01. Center for the Study
of Democracy.</mixed-citation>
         </ref>
         <ref id="d71e763a1310">
            <mixed-citation id="d71e767" publication-type="other">
Paxton, Pamela, and Sheri Kunovich. 2003. Women's political
representation: The importance of ideology. Social Forces
82(1): 87-113.</mixed-citation>
         </ref>
         <ref id="d71e780a1310">
            <mixed-citation id="d71e784" publication-type="other">
Paxton, Pamela, and Sheri Kunovich. 2005. Pathways to power:
The role of political parties in women's national political rep-
resentation. American Journal of Sociology 111 (2): 505-52.</mixed-citation>
         </ref>
         <ref id="d71e797a1310">
            <mixed-citation id="d71e801" publication-type="other">
Phillips, Anne. 1995. The politics of presence. Oxford: Claren-
don Press.</mixed-citation>
         </ref>
         <ref id="d71e811a1310">
            <mixed-citation id="d71e815" publication-type="other">
Reynolds, Andrew. 1999. Women in the legislatures and execu-
tives of the world: Knocking at the highest glass ceiling.
World Politics 51 (4): 547-72.</mixed-citation>
         </ref>
         <ref id="d71e829a1310">
            <mixed-citation id="d71e833" publication-type="other">
Ritter, Gretchen. 2007. Gender and politics over time. Politics
and Gender 3 (3): 386-97.</mixed-citation>
         </ref>
         <ref id="d71e843a1310">
            <mixed-citation id="d71e847" publication-type="other">
Rule, Wilma. 1987. Electoral systems, contextual factors, and
women's opportunities for election to parliament in twenty-
three democracies. Western Political Quarterly 40:477-98.</mixed-citation>
         </ref>
         <ref id="d71e860a1310">
            <mixed-citation id="d71e864" publication-type="other">
Stevens, Anne. 2007. Women, power and politics. New York:
Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d71e874a1310">
            <mixed-citation id="d71e878" publication-type="other">
Tolleson-Rinehart. 2001. Do women leaders make a difference?
In The impact of women in public office, edited by Susan
Carrol, 149-65. Bloomington: University of Indiana Press.</mixed-citation>
         </ref>
         <ref id="d71e891a1310">
            <mixed-citation id="d71e895" publication-type="other">
Tremblay, Manon. 2008. Women and legislative representation:
Electoral systems, political parties, and sex quotas. New
York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d71e908a1310">
            <mixed-citation id="d71e912" publication-type="other">
Tripp, Aili Mari, and Alice Kang. 2008. The global impact of
quotas: On the fast track to increased female legislative rep-
resentation. Comparative Political Studies 41 (3): 338-61.</mixed-citation>
         </ref>
         <ref id="d71e926a1310">
            <mixed-citation id="d71e930" publication-type="other">
Tripp, Aili, Dior Konate, and Colleen Lowe-Morna. 2006.
Sub-Saharan Africa: On the fast track to women's political
representation. In Women, quotas and politics, edited by
Drude Dahlerup, 112-37. New York: Routledge Research
in Comparative Politics.</mixed-citation>
         </ref>
         <ref id="d71e949a1310">
            <mixed-citation id="d71e953" publication-type="other">
United Nations. 2008. Human Development Indices. New York:
United Nations Statistics Division.</mixed-citation>
         </ref>
         <ref id="d71e963a1310">
            <mixed-citation id="d71e967" publication-type="other">
United Nations. 2010. The world's women 2010. New York:
United Nations Statistics Division.</mixed-citation>
         </ref>
         <ref id="d71e977a1310">
            <mixed-citation id="d71e981" publication-type="other">
Viterna, Jocelyn, Kathleen Fallon, and Jason Beckfield. 2008.
How development matters: A research note on the relationship
between development, democracy and women's political repre-
sentation. International Journal of Comparative Sociology 49
(6): 455-77.</mixed-citation>
         </ref>
         <ref id="d71e1000a1310">
            <mixed-citation id="d71e1004" publication-type="other">
Wângnerud, Lena. 2009. Women in parliaments: Descriptive
and substantive representation. Annual Review of Political
Science 12:51-69.</mixed-citation>
         </ref>
         <ref id="d71e1017a1310">
            <mixed-citation id="d71e1021" publication-type="other">
World Bank. 2010. Worldwide governance indicators. Wash-
ington, DC: World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
