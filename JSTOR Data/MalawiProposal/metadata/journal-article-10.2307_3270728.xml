<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jmonecredbank</journal-id>
         <journal-id journal-id-type="jstor">j100238</journal-id>
         <journal-title-group>
            <journal-title>Journal of Money, Credit and Banking</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Ohio State University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00222879</issn>
         <issn pub-type="epub">15384616</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3270728</article-id>
         <title-group>
            <article-title>Currency Unions and International Integration</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Andrew K.</given-names>
                  <surname>Rose</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Charles</given-names>
                  <surname>Engel</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>11</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">34</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i364874</issue-id>
         <fpage>1067</fpage>
         <lpage>1089</lpage>
         <page-range>1067-1089</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 The Ohio State University</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3270728"/>
         <abstract>
            <p>This paper characterizes the integration patterns of international currency unions (such as the CFA Franc Zone). We empirically explore different features of currency unions, and compare them to countries with sovereign monies by examining the criteria for Mundell's concept of an optimum currency area. We find that members of currency unions are more integrated than countries with their own currencies. For instance, we find that currency union members have more trade and less volatile real exchange rates than countries with their own monies; business cycles are more highly synchronized across currency union countries than across countries with sovereign monies.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d588e226a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d588e233" publication-type="journal">
McKinnon (1963)  </mixed-citation>
            </p>
         </fn>
         <fn id="d588e242a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d588e249" publication-type="book">
Obstfeld and Rogoff (2000)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d588e257" publication-type="journal">
Frankel and Rose (1998)  </mixed-citation>
            </p>
         </fn>
         <fn id="d588e266a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d588e273" publication-type="journal">
Rose (2000)  </mixed-citation>
            </p>
         </fn>
         <fn id="d588e282a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d588e289" publication-type="book">
Imbs and
Wacziarg (2000).  </mixed-citation>
            </p>
         </fn>
         <fn id="d588e302a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d588e309" publication-type="other">
The 1998 World Factbook available at http://www.odci.gov/cia/publications/factbook/index.html.</mixed-citation>
            </p>
         </fn>
         <fn id="d588e316a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d588e323" publication-type="other">
http://www.wto.org/wto/
develop/webrtas.htm</mixed-citation>
            </p>
         </fn>
         <fn id="d588e333a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d588e340" publication-type="journal">
Rudebusch (1993)  </mixed-citation>
            </p>
         </fn>
         <fn id="d588e349a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d588e356" publication-type="journal">
Frankel and Rose (1998).  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d588e374a1310">
            <mixed-citation id="d588e378" publication-type="journal">
Bachetta, Philippe, Andrew K. Rose, and Eric van Wincoop (2001) "Intranational Economics
and International Economics." Journal of International Economics 55:1 (2001), 1.<person-group>
                  <string-name>
                     <surname>Bachetta</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>1</fpage>
               <volume>55</volume>
               <source>Journal of International Economics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d588e413a1310">
            <mixed-citation id="d588e417" publication-type="journal">
Cecchetti, Stephen, Nelson C. Mark, and Robert J. Sonora. "Price Level Convergence among
United States Cities: Lessons for the European Central Bank." International Economic Re-
view, forthcoming.<person-group>
                  <string-name>
                     <surname>Cecchetti</surname>
                  </string-name>
               </person-group>
               <source>International Economic Review</source>
            </mixed-citation>
         </ref>
         <ref id="d588e442a1310">
            <mixed-citation id="d588e446" publication-type="journal">
Clark, Todd E., and Eric van Wincoop. "Borders and Business Cycles." Journal of Interna-
tional Economics 55:1 (2001), 59-85.<person-group>
                  <string-name>
                     <surname>Clark</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>59</fpage>
               <volume>55</volume>
               <source>Journal of International Economics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d588e481a1310">
            <mixed-citation id="d588e485" publication-type="journal">
Engel, Charles, Michael K. Hendrickson, and John H. Rogers. "Intranational, Intracontinen-
tal, and Intraplanetary PPP." Journal of the Japanese and International Economies 11
(1997), 480-501.<person-group>
                  <string-name>
                     <surname>Engel</surname>
                  </string-name>
               </person-group>
               <fpage>480</fpage>
               <volume>11</volume>
               <source>Journal of the Japanese and International Economies</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d588e521a1310">
            <mixed-citation id="d588e525" publication-type="journal">
Engel, Charles, and John H. Rogers. "How Wide Is the Border?" American Economic Review
86 (1996), 1112-25.<object-id pub-id-type="jstor">10.2307/2118281</object-id>
               <fpage>1112</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e541a1310">
            <mixed-citation id="d588e545" publication-type="book">
— (2000) "Relative Price Volatility: What Role Does the Border Play?" In Intranational
Macroeconomics, edited by Gregory D. Hess and Eric van Wincoop. Cambridge, 2000.<person-group>
                  <string-name>
                     <surname>Engel</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Relative Price Volatility: What Role Does the Border Play?</comment>
               <source>Intranational Macroeconomics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d588e574a1310">
            <mixed-citation id="d588e578" publication-type="journal">
— "Deviations from Purchasing Power Parity: Causes and Welfare Costs." Journal of
International Economics 55:1 (2001), 29-57.<person-group>
                  <string-name>
                     <surname>Engel</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>29</fpage>
               <volume>55</volume>
               <source>Journal of International Economics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d588e613a1310">
            <mixed-citation id="d588e617" publication-type="book">
Feenstra, Robert C., Robert E. Lipsey, and Harry P. Bowen. "World Trade Flows, 1970-1992,
with Production and Tariff Data." NBER Working Paper no. 5910, 1997.<person-group>
                  <string-name>
                     <surname>Feenstra</surname>
                  </string-name>
               </person-group>
               <source>World Trade Flows, 1970-1992, with Production and Tariff Data</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d588e642a1310">
            <mixed-citation id="d588e646" publication-type="journal">
Frankel, Jeffrey A., and Andrew K. Rose. "The Endogeneity of the Optimum Currency Area
Criteria." Economic Journal 108 (1998), 1009-25.<object-id pub-id-type="jstor">10.2307/2565665</object-id>
               <fpage>1009</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e662a1310">
            <mixed-citation id="d588e666" publication-type="book">
Helliwell, John F. How Much Do National Borders Matter? Washington: Brookings, 1998.<person-group>
                  <string-name>
                     <surname>Helliwell</surname>
                  </string-name>
               </person-group>
               <source>How Much Do National Borders Matter?</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d588e689a1310">
            <mixed-citation id="d588e693" publication-type="book">
Hess, Gregory D., and Eric van Wincoop. Intranational Macroeconomics. Cambridge: Uni-
versity Press, 2000.<person-group>
                  <string-name>
                     <surname>Hess</surname>
                  </string-name>
               </person-group>
               <source>Intranational Macroeconomics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d588e718a1310">
            <mixed-citation id="d588e722" publication-type="book">
Imbs, Jean, and Romain Wacziarg. "Stages of Diversification." London Business School man-
uscript, 2000.<person-group>
                  <string-name>
                     <surname>Imbs</surname>
                  </string-name>
               </person-group>
               <source>Stages of Diversification</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d588e747a1310">
            <mixed-citation id="d588e751" publication-type="journal">
Kalemi-Ozcan, Sebnem Bent S0rensen, and Oved Yosha. "Economic Integration, Industrial
Specialization, and the Asymmetry of Shocks across Regions." Journal of International
Economics 55:1 (2000), 107-37. a<person-group>
                  <string-name>
                     <surname>Kalemi-Ozcan</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>107</fpage>
               <volume>55</volume>
               <source>Journal of International Economics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d588e789a1310">
            <mixed-citation id="d588e793" publication-type="book">
— "Risk-Sharing and Specialization: Regional and International Evidence" CEPR Dis-
cussion Paper 2295, 2000. b<person-group>
                  <string-name>
                     <surname>Kalemi-Ozcan</surname>
                  </string-name>
               </person-group>
               <source>Risk-Sharing and Specialization: Regional and International Evidence</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d588e818a1310">
            <mixed-citation id="d588e822" publication-type="book">
Kenen, Peter B. "The Theory of Optimum Currency Areas: An Eclectic View" In Monetary
Problems of the International Economy, edited by R. A. Mundell and A. K. Swoboda.
Chicago: University Press, 1969.<person-group>
                  <string-name>
                     <surname>Kenen</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Theory of Optimum Currency Areas: An Eclectic View</comment>
               <source>Monetary Problems of the International Economy</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d588e854a1310">
            <mixed-citation id="d588e858" publication-type="journal">
McCallum, John. "National Borders Matter: Canada-U.S. Regional Trade Patterns." American
Economic Review 85:3 (1995), 615-23.<object-id pub-id-type="jstor">10.2307/2118191</object-id>
               <fpage>615</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e875a1310">
            <mixed-citation id="d588e879" publication-type="journal">
McKinnon, Ronald I. "Optimum Currency Areas." American Economic Review 53 (1963),
717-25.<object-id pub-id-type="jstor">10.2307/1811021</object-id>
               <fpage>717</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e895a1310">
            <mixed-citation id="d588e899" publication-type="journal">
Mundell, Robert A. "A Theory of Optimum Currency Areas." American Economic Review 51
(1961), 657-65.<object-id pub-id-type="jstor">10.2307/1812792</object-id>
               <fpage>657</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e915a1310">
            <mixed-citation id="d588e919" publication-type="book">
Obstfeld, Maurice, and Kenneth Rogoff. Foundations of International Macroeconomics.
Cambridge: MIT Press, 1996.<person-group>
                  <string-name>
                     <surname>Obstfeld</surname>
                  </string-name>
               </person-group>
               <source>Foundations of International Macroeconomics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d588e944a1310">
            <mixed-citation id="d588e948" publication-type="book">
— "The Six Major Puzzles in International Macroeconomics: Is There a Common
Cause?" NBER Macroeconomics Annual 2000, 339-90.<person-group>
                  <string-name>
                     <surname>Obstfeld</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Six Major Puzzles in International Macroeconomics: Is There a Common Cause?</comment>
               <fpage>339</fpage>
               <source>NBER Macroeconomics Annual 2000</source>
            </mixed-citation>
         </ref>
         <ref id="d588e977a1310">
            <mixed-citation id="d588e981" publication-type="journal">
Parsley, David C., and Shang-Jin Wei. "Convergence to the Law of One Price without Trade
Barriers or Currency Fluctuations." Quarterly Journal of Economics 111 (1996), 1211-36.<object-id pub-id-type="doi">10.2307/2946713</object-id>
               <fpage>1211</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e997a1310">
            <mixed-citation id="d588e1001" publication-type="journal">
Rogers, John H., and Michael Jenkins. "Haircuts or Hysteresis? Sources of Movements in
Real Exchange Rates." Journal of International Economics 38 (1995), 339-60.<person-group>
                  <string-name>
                     <surname>Rogers</surname>
                  </string-name>
               </person-group>
               <fpage>339</fpage>
               <volume>38</volume>
               <source>Journal of International Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d588e1034a1310">
            <mixed-citation id="d588e1038" publication-type="journal">
Rogoff, Kenneth. "The Purchasing Power Parity Puzzle." Journal of Economic Literature 34
(1996), 647-68.<object-id pub-id-type="jstor">10.2307/2729217</object-id>
               <fpage>647</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e1054a1310">
            <mixed-citation id="d588e1058" publication-type="journal">
Rose, Andrew K. "One Money One Market." Economic Policy 15-30 (2000), 7-46.<object-id pub-id-type="jstor">10.2307/1344722</object-id>
               <fpage>7</fpage>
            </mixed-citation>
         </ref>
         <ref id="d588e1071a1310">
            <mixed-citation id="d588e1075" publication-type="journal">
Rudebusch, Glenn. "The Uncertain Unit Root in Real GDP." American Economic Review 83
(1993), 264-72.<object-id pub-id-type="jstor">10.2307/2117509</object-id>
               <fpage>264</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
