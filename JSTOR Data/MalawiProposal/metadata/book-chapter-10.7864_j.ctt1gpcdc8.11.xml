<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gpcdc8</book-id>
      <subj-group>
         <subject content-type="call-number">HG178.3.R66 2011</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Microfinance</subject>
         <subj-group>
            <subject content-type="lcsh">Evaluation</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Due Diligence</book-title>
         <subtitle>An Impertinent Inquiry into Microfinance</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Roodman</surname>
               <given-names>David</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>01</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9781933286488</isbn>
      <isbn content-type="epub">9781933286532</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>CENTER FOR GLOBAL DEVELOPMENT</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1gpcdc8"/>
      <abstract abstract-type="short">
         <p>The idea that small loans can help poor families build businesses and exit poverty has blossomed into a global movement. The concept has captured the public imagination, drawn in billions of dollars, reached millions of customers, and garnered a Nobel Prize. Radical in its suggestion that the poor are creditworthy and conservative in its insistence on individual accountability, the idea has expanded beyond credit into savings, insurance, and money transfers, earning the name microfinance. But is it the boon so many think it is?</p>
         <p>Readers of David Roodman's openbook blog will immediately recognize his thorough, straightforward, and trenchant analysis.<italic>Due Diligence</italic>, written entirely in public with input from readers, probes the truth about microfinance to guide governments, foundations, investors, and private citizens who support financial services for poor people. In particular, it explains the need to deemphasize microcredit in favor of other financial services for the poor.</p>
      </abstract>
      <counts>
         <page-count count="275"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Birdsall</surname>
                           <given-names>Nancy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.5</book-part-id>
                  <title-group>
                     <label>one</label>
                     <title>What’s the Story?</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Most accounts of microfinance—the large-scale, businesslike provision of financial services to poor people—begin with a story. So I think it is fitting to start my inquiry into microfinance with two, about the most well-known kind of microfinance, microcredit, in which loans of $50–$1,000 are given to people typically earning no more than $2 per day. Both stories, I assume, are true. The first story is told by Muhammad Yunus, who won the Nobel Peace Prize along with the Grameen Bank he founded:</p>
                     <p>Murshida was born into a poor family of eight children. Neither her father nor grandfather</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.6</book-part-id>
                  <title-group>
                     <label>two</label>
                     <title>How the Other Half Finances</title>
                  </title-group>
                  <fpage>15</fpage>
                  <abstract>
                     <p>Do you remember when you first heard about microcredit? Did it surprise you that one could help poor people by putting them in debt? The root of such surprise is the microfinance movement’s successful replacement of the old story of lending to the poor—a story of usury—with a new one about microenterprise. After all, it seems, poor people can be masters of their fates, incipient entrepreneurs who lack only credit to bloom.</p>
                     <p>On the one hand, this upbeat story is rightly animated by the belief that people are no less ingenious or resourceful for being poor. On the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.7</book-part-id>
                  <title-group>
                     <label>three</label>
                     <title>Credit History</title>
                  </title-group>
                  <fpage>36</fpage>
                  <abstract>
                     <p>You’ve heard how microcredit was born. In a nation long shackled by British rule and wracked by famine, a brilliant man was seized with a desire to strike a blow against the poverty all about him. Defying common sense and the skepticism of his colleagues, he began lending tiny sums out of his own pocket to poor people, which they were to invest in tiny businesses. He demanded no collateral, only the vouchsafe of the borrowers’ peers. The borrowers rewarded his faith with punctual repayment. In time, his experiment spawned a national movement that delivered millions of loans to poor</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.8</book-part-id>
                  <title-group>
                     <label>four</label>
                     <title>Background Check</title>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>When biographer Thomas Sheridan likened Jonathan Swift’s lending to a spring that “watered and enriched the humble vale through which it ran, still extending and widening its course,” he probably did not foresee the spring tumbling on for two centuries, extending into the mighty rivers of South America, the deltas of South Asia, and the plains of Africa.² But so it did, intermingling with like streams from Priscilla Wakefield’s Benefit Club and the German credit cooperatives. As we continue to follow the water’s many courses, we arrive at the subject of this book, the financial services for the poor that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.9</book-part-id>
                  <title-group>
                     <label>five</label>
                     <title>Business Plan</title>
                  </title-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>Microfinance is manufactured by macro-sized organizations. In 2009, thirteen microfinance institutions (MFIs) in Bangladesh, India, Indonesia, and Mexico had more than a million borrowers, giving them 48 percent of all microcredit loans worldwide. Worldwide, 122 lenders—or 11 percent of the total—had more than 100,000 customers and accounted for fully 79 percent of outstanding microloans (see table 5-1). To cut the data another way, the “average microcredit client” was served by a lender with 2.2 million borrowers, a national market share of 22 percent, 9,000 employees, $730 million in assets, and operating profits equal to 16 percent of revenue.²</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.10</book-part-id>
                  <title-group>
                     <label>six</label>
                     <title>Development as Escape from Poverty</title>
                  </title-group>
                  <fpage>138</fpage>
                  <abstract>
                     <p>In January 2008 I visited Cairo, Egypt, for a few days. My official purpose was to speak at a United Nations conference whose premise I did not understand and did not try very hard to understand. It took place just off Tahrir Square, in a high-rise hotel with burly security guards and a clientele of Saudis who came to pursue pastimes more effectively proscribed in their own capital.</p>
                     <p>Outside the conference hall, I devoted most of my waking hours to two contradictory activities. On a laptop computer in the hotel room, I worked to reconstruct and scrutinize what was then</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.11</book-part-id>
                  <title-group>
                     <label>seven</label>
                     <title>Development as Freedom</title>
                  </title-group>
                  <fpage>175</fpage>
                  <abstract>
                     <p>The microfinance movement cuts against old ideas about credit. In ancient texts on religion and philosophy, most references to lending are stern, especially when it comes to charging interest.³ In the Koran it is written: “O believers, fear God, and give up the interest that remains outstanding if you are believers. If you do not do so, then be sure of being at war with God and His messenger. But, if you repent, you can have your principal.”⁴ In Leviticus, God spoke through Moses with lawyerly thoroughness, instructing, “When your brother-Israelite is reduced to poverty and cannot support himself in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.12</book-part-id>
                  <title-group>
                     <label>eight</label>
                     <title>Development as Industry Building</title>
                  </title-group>
                  <fpage>221</fpage>
                  <abstract>
                     <p>When I think of vaccination in developing countries, I see a baby getting a shot. I hardly see the nurse who gives the shot, much less the health ministry she works for. When I think of the provision of clean water, I see a mother working a hand pump to fill a bucket. I do not see the nonprofit that drilled the well.</p>
                     <p>When I think of the delivery of financial services to poor people, I see a cluster of women in colorful saris seated on the ground, gathered to do their weekly financial business. But I also see a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.13</book-part-id>
                  <title-group>
                     <label>nine</label>
                     <title>Billions Served</title>
                  </title-group>
                  <fpage>268</fpage>
                  <abstract>
                     <p>In the last eight chapters, I have examined microfinance from more angles than ever before in one place. I have placed modern microfinance in the flow of history. I have shared the viewpoints of the user at the metaphorical teller window and the manager behind it. I have surveyed the diversity of microfinance and investigated the strongest claims for its virtues: microfinance as abolisher of poverty, enhancer of freedom, and builder of industry. Now I sum up the lessons, ponder what lies ahead, and advise how to influence it.</p>
                     <p>You know the popular image of microfinance: it was invented by</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.14</book-part-id>
                  <title-group>
                     <title>appendix.</title>
                     <subtitle>A Sampling of Blog Posts</subtitle>
                  </title-group>
                  <fpage>293</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.15</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>337</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>353</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gpcdc8.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>367</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
