<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jsoutafristud</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100641</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Southern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge, Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03057070</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14653893</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20790053</article-id>
         <article-categories>
            <subj-group>
               <subject>Contemporary Struggles: Citizenship, (Dis)Empowerment, Livelihoods, Survival</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Growing up Without Parents: Socialisation and Gender Relations in Orphaned-Child-Headed Households in Rural Zimbabwe</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Monica</given-names>
                  <surname>Francis-Chizororo</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20790038</issue-id>
         <fpage>711</fpage>
         <lpage>727</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 The Editorial Board of the Journal of Southern African Studies</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/03057070.2010.507578"
                   xlink:title="an external site"/>
         <abstract>
            <p>The most distressing consequences of the HIV/AIDS pandemic's impact on children has been the development of child-headed households (CHHs). Child 'only' households challenge notions of the ideal home, family, and 'normal' childhood, as well as undermining international attempts to institute children's rights. The development of these households raises practical questions about how the children will cope without parental guidance during their childhood and how this experience will affect their adulthood. Drawing on ethnographic research with five child heads and their siblings, this article explores how orphaned children living in 'child only' households organise themselves in terms of household domestic and paid work roles, explores the socialisation of children by children and the negotiation of teenage girls' movement. Further, it examines whether the orphaned children are in some way attempting to 'mimic' previously existing family/household gender relations after parental death. The study showed that all members in the CHHs irrespective of age and gender are an integral part of household labour including food production. Although there is masculinisation of domestic chores in boys 'only' households, roles are distributed by age. On the other hand, households with a gender mix tended to follow traditional gender norms. Conflict often arose when boys controlled teenage girls' movement and sexuality. There is a need for further research on CHHs to better understand orphans' experiences, and to inform policy interventions.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d389e143a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d389e150" publication-type="other">
UNICEF/UNAIDS/USPEPFAR, Africa's Orphaned and Vulnerable Generations: Children Affected by AIDS
(New York, UNICEF, 2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e160a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d389e167" publication-type="other">
Central Statistical Office (Zimbabwe), The 2002 Zimbabwe National Population Census, Preliminary Findings
(Zimbabwe, CSO, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e177a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d389e184" publication-type="other">
PRF/IDS/UNDP, Zimbabwe Human Development Report 2003: Redirecting our Responses to HIV and AIDS
(Harare, Poverty Reduction Forum and Institute of Development Studies, 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e194a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d389e201" publication-type="other">
G. Foster, C. Makufa, R. Drew and E. Kralovec, 'Factors Leading to the Establishment of Child-headed
Households', Health Transition Review, Supplement, 7, 2 (1997), pp. 157-70.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e212a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d389e219" publication-type="other">
A. Barnett and T. Whiteside, AIDS in the Twenty-First Century: Disease and Globalisation (New York,
Palgrave, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e229a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d389e236" publication-type="other">
S. Chant, Women-headed Households: Diversity and Dynamics in the Developing World (London, Macmillan,
1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e246a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d389e253" publication-type="other">
S. Punch, 'Household Division of Labour: Generation, Gender, Age, Birth Order and Sibling Composition',
Work, Employment and Society, 15, 4 (2001), pp. 803-23.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e263a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d389e270" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e277a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d389e284" publication-type="other">
M.F.C. Bourdillon, Earning a Life: Working Children in Zimbabwe (Harare, Weaver Press, 2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e290" publication-type="other">
M.F.C. Bourdillon, Where are the Ancestors? Changing Culture in Zimbabwe (Harare, University of Zimbabwe
Publications, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e300a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d389e307" publication-type="other">
Foster
et al, 'Factors Leading to the Establishment of Child-headed Households'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e318a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d389e325" publication-type="other">
M. Francis-Chizororo, Orphanhood, Childhood and Identity Dilemma of Child-headed Households in Rural
Zimbabwe in the Context of HIV/AIDS Pandemic', Journal of African Population Studies, forthcoming.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e335a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d389e342" publication-type="other">
S. Hunter, 'Orphans as a Window on the AIDS Epidemic in Sub-Saharan Africa: Initial Results and Implications
of a Study in Uganda', Social Science and Medicine, 31, 3 (1990), pp. 681-90</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e351" publication-type="other">
UNICEF, AIDS, and Children
Victims: The State of the World's Children (Oxford, Oxford University Press, 1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e360" publication-type="other">
G. Foster, R. Shakespear,
F. Chinemana, 'Orphan Prevalence and Extended Family Care in a Peri-urban Community in Zimbabwe',
AIDS Care, 7, 1 (1995), pp. 3-18.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e373a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d389e380" publication-type="other">
L. Young, and N. Ansell, 'Fluid Households, Complex Families: The Impact of Children's Migration as a
Response to HIV/AIDS in Southern Africa', The Professional Geographer, 554, 4 (2003), pp. 464-79</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e389" publication-type="other">
E. Robson, 'Invisible Carers: Young People in Zimbabwe's Home-based Healthcare', Area, 32 (2000),
pp. 59-69</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e398" publication-type="other">
S.E. Germann, 'An Exploratory Study of Quality of Life and Coping Strategies of Orphans Living in
Child-headed Households in the High HIV/AIDS Prevalent City of Bulawayo, Zimbabwe' (D.Phil. Thesis,
University of South Africa, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e411a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d389e418" publication-type="other">
S. Madhavan, 'Fosterage Patterns in the age of AIDS: Continuity and Change', Social Science and Medicine, 58
(2004), pp. 1,443-54</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e427" publication-type="other">
N.R. Matshalaga, and G. Powell, 'Mass Orphanhood in the Era of HIV/AIDS',
British Medical Journal, 324 (2004), pp. 185-6.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e437a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d389e444" publication-type="other">
Foster et al, 'Factors Leading to the
Establishment of Child-headed Households'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e454a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d389e461" publication-type="other">
E. Robson, 'Hidden Child Workers: Young Carers in Zimbabwe', Antipode, 36, 2 (2004), pp. 227-48.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e469a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d389e476" publication-type="other">
M. Francis-Chizororo, The Formation,
Constitution and Social Dynamics of Orphaned Child-headed Households in Rural Zimbabwe in the Era of
HIV/AIDS Pandemic' (Ph.D. thesis, University of St Andrews, 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e489a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d389e496" publication-type="other">
M. Kesby, F. Gwanzura-Ottemoller and M. Chizororo, 'Theorising Other "Other Childhoods": Issues Emerging
from Work on HIV/AIDS in Urban and Rural Zimbabwe', Children's Geographies, 4, 2 (2006), pp. 185-202.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e506a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d389e513" publication-type="other">
Bourdillon, The Shona Peoples.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e520a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d389e527" publication-type="other">
M. Gelfand, The Spiritual Beliefs of the Shona: A Study Based on Field Research Among the East Central Shona
(Gweru, Mambo Press, 1977).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e537a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d389e544" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e551a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d389e558" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e566a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d389e573" publication-type="other">
E. Schmidt, Peasants, Traders and Wives: Shona Women in the History of Zimbabwe, 1970-1939
(Harare, Baobab, 1992).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e583a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d389e590" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e597a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d389e604" publication-type="other">
Foster et al., 'Factors Leading to the Establishment of Child-headed Households'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e611a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d389e618" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e625a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d389e632" publication-type="other">
M. Gelfand, The Genuine Shona: Survival Values of an African Culture (Gweru, Mambo Press, 1973).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e639a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d389e646" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e654a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d389e661" publication-type="other">
M. Gelfand, Growing up in Shona Society: From Birth to Marriage (Gweru, Mambo Press, 1975).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e668a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d389e675" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e682a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d389e689" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e696a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d389e703" publication-type="other">
Gelfand, Growing up in Shona Society.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e710a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d389e717" publication-type="other">
Gelfand, Growing up in Shona Society.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e724a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d389e731" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e739a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d389e746" publication-type="other">
Ibid.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e752" publication-type="other">
P. Reynolds, Dance Civet Cat: Child Labour in the Zambezi Valley (Ohio, Ohio University Press,
1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e762a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d389e769" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e776a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d389e783" publication-type="other">
B. Grier, 'Invisible Hands: The Political Economy of Child Labour in Colonial Zimbabwe, 1890-1930',
Journal of Southern African Studies, 20, 1 (1994), pp. 27-52.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e793a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d389e800" publication-type="other">
Bourdillon, Earning a Life.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e807a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d389e814" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e821a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d389e828" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e836a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d389e843" publication-type="other">
Bourdillon, Earning a Life.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e850a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d389e857" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e864a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d389e871" publication-type="other">
T.Barnes, 'The Fight for Control of African Women's Mobility in Colonial Zimbabwe, 1900-1939', Signs, 17,3
(1992), pp. 586-608.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e881a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d389e888" publication-type="other">
Barnes, 'The Fight for Control of African Women's Mobility', p. 598.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e895a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d389e902" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e909a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d389e916" publication-type="other">
M. Mhloyi, Women 's Participation in Development: The Role of Family Planning (Harare, Friedrich Ebert
Stiftung, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e927a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d389e934" publication-type="other">
M. Francis-Chizororo,N. Wekwete and M. Matshaka, 'Family Planning, Women's Participation in
Development: The Mediating Effects of Gender', in M. Mhloyi (ed.), Women's Participation in Development:
The Role of Family Planning (Harare, Friedrich Ebert Stiftung, 1998), pp. 71-89.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e947a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d389e954" publication-type="other">
M. Kesby, 'Arenas for Control, Terrains of Gender Contestation: Guerrilla Struggle and Counter-insurgency
Warfare in Zimbabwe 1972-1980', JSAS, 22, 4 (1996), pp. 561-84.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e964a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d389e971" publication-type="other">
P. Reynolds, 'Children of Tribulation: The Need to Heal and the Means to Heal War Trauma', Africa, 60, 1
(1990), pp. 1-38.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e981a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d389e988" publication-type="other">
P. Reynolds, Traditional Healers and Childhood in Zimbabwe (Athens, OH, Ohio University Press, 1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e995a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d389e1002" publication-type="other">
Ibid., p. 7.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1009a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d389e1016" publication-type="other">
Kesby, 'Arenas for Control'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1024a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d389e1031" publication-type="other">
Reynolds, 'Children of Tribulation'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1038a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d389e1045" publication-type="other">
Reynolds, 'Children of Tribulation'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1052a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d389e1059" publication-type="other">
M. Kesby, 'Locating and Dislocating Gender in Rural Zimbabwe: The Making of Space and the Texturing of
Bodies', Gender, Place and Culture, 6, 1 (1999), pp. 27-47.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1069a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d389e1076" publication-type="other">
Legal Age of Majority Act (LAMA) (No. 15. of 1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1083a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d389e1090" publication-type="other">
Kesby et al, 'Theorising Other "Other Childhoods'".</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1097a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d389e1104" publication-type="other">
Francis-Chizororo, 'Orphanhood, Childhood and Identity Dilemma of Child-headed Households'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1112a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d389e1119" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1126a1310">
            <label>65</label>
            <p>
               <mixed-citation id="d389e1133" publication-type="other">
Bourdillon, Where are the Ancestors?</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1140a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d389e1147" publication-type="other">
Foster et al, 'Orphan Prevalence and Extended Family Care'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1154a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d389e1161" publication-type="other">
L.C. Young and H.R. Barrett, 'Adapting Visual Methods: Action Research with Kampala Street
Children', Ethics, Place and Environment, 4, 2 (2001a), pp. 141-52</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e1170" publication-type="other">
L.C.N. Ansell, and L.C. Young, 'Enabling
Households to Support Successful Migration of AIDS Orphans in Southern Africa', AIDS Care, 16, 1 (2004),
pp. 3-10.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1183a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d389e1190" publication-type="other">
P. Alderson, Listening to Children: Children, Ethics and Social Research (Barnados, Barkingside, 1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1197a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d389e1204" publication-type="other">
Robson, 'Interviews Worth the Tears?'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e1210" publication-type="other">
Robson, 'Invisible Carers'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1218a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d389e1225" publication-type="other">
A. Strauss, Qualitative Analysis for Social Scientists (New York, Cambridge University Press, 1987).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1232a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d389e1239" publication-type="other">
S. Jones, 'The Analysis of Depth Interviews', in R. Walker (ed.), Applied Qualitative Research
(Aldershot, Gower Press, 1985), pp. 80-87.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1249a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d389e1256" publication-type="other">
Focus Group Discussion, Chemai, [female head of household], 29 June 2005, Tsungirirai Offices, Mhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1263a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d389e1270" publication-type="other">
In-depth Interview, Mufumi [male head of household], 22 June 2005, Bure Village, Nhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1277a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d389e1284" publication-type="other">
Focus Group Discussion, Chenjerai [male head of household], 29 June 2005, Tsungirirai Offices.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1291a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d389e1298" publication-type="other">
PRF/IDS/UNDP, 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1306a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d389e1313" publication-type="other">
Chant, Women-headed Households.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1320a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d389e1327" publication-type="other">
Focus Group Discussion, Tendai, 29 June 2005, Tsungirirai Offices, Mhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1334a1310">
            <label>84</label>
            <p>
               <mixed-citation id="d389e1341" publication-type="other">
Chemai, 29 June, 2005, Tsungirirai Offices, Mhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1348a1310">
            <label>85</label>
            <p>
               <mixed-citation id="d389e1355" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1362a1310">
            <label>87</label>
            <p>
               <mixed-citation id="d389e1369" publication-type="other">
F. Gwanzura-Ottemoller, 'They Tell Us We Are Still Children! HIV/AIDS-Related Knowledge and the Extent
and Nature of the Sexual Knowledge and Behaviour of Primary School Children in Zimbabwe' (Ph.D. thesis,
University of St Andrews, 2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1382a1310">
            <label>88</label>
            <p>
               <mixed-citation id="d389e1389" publication-type="other">
In-depth interview, Mbadzu [older brother to Chenjerai not head], 12 June 2005, Kawanzaruwa, Mhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1397a1310">
            <label>89</label>
            <p>
               <mixed-citation id="d389e1404" publication-type="other">
Conversation with, Chemai, 3 July 2005, Kavero Village, Mhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1411a1310">
            <label>91</label>
            <p>
               <mixed-citation id="d389e1418" publication-type="other">
Field diary, 3 July 2005, Chemai household, Kavero Village, Mhondoro.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1425a1310">
            <label>99</label>
            <p>
               <mixed-citation id="d389e1432" publication-type="other">
M. Francis-Chizororo and G. Malunga, 'Gender Roles and Wage Earnings: Women Seasonal Labour Migrants
to Large Scale Commercial Farms in Zimbabwe' (Kampala, Centre for Basic Research, 2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1442a1310">
            <label>100</label>
            <p>
               <mixed-citation id="d389e1449" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1456a1310">
            <label>101</label>
            <p>
               <mixed-citation id="d389e1463" publication-type="other">
Gelfand, Growing up in Shona Society</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d389e1469" publication-type="other">
Gelfand, The Genuine Shona.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1476a1310">
            <label>102</label>
            <p>
               <mixed-citation id="d389e1483" publication-type="other">
Kesby, Locating and Dislocating Gender in Rural Zimbabwe.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1491a1310">
            <label>103</label>
            <p>
               <mixed-citation id="d389e1498" publication-type="other">
Gelfand, The Genuine Shona.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1505a1310">
            <label>104</label>
            <p>
               <mixed-citation id="d389e1512" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1519a1310">
            <label>105</label>
            <p>
               <mixed-citation id="d389e1526" publication-type="other">
Chant, Women-headed Households.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1533a1310">
            <label>106</label>
            <p>
               <mixed-citation id="d389e1540" publication-type="other">
Germann, 'An Exploratory Study of Quality of Life'.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1547a1310">
            <label>107</label>
            <p>
               <mixed-citation id="d389e1554" publication-type="other">
Schmidt, Peasants, Traders and Wives.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1561a1310">
            <label>110</label>
            <p>
               <mixed-citation id="d389e1568" publication-type="other">
L. Brydon, 'Gender, Households and Rural Communities', in L. Brydon and S. Chant (eds), Women in the Third
World: Gender Issues in Rural and Urban Areas (Aldershot, Edward Elgar, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1579a1310">
            <label>111</label>
            <p>
               <mixed-citation id="d389e1586" publication-type="other">
Chant, Women-headed Households.</mixed-citation>
            </p>
         </fn>
         <fn id="d389e1593a1310">
            <label>112</label>
            <p>
               <mixed-citation id="d389e1600" publication-type="other">
P. Christensen and A. James, Research with Children: Perspectives and Practices (London, Falmer Press,
2000).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
