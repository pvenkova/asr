<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">afrdevafrdev</journal-id>
         <journal-title-group>
            <journal-title>Africa Development / Afrique et Développement</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CODESRIA</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08503907</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">90000042</article-id>
         <title-group>
            <article-title>Knowledge is Power and Power Affects Knowledge:</article-title>
            <subtitle>Challenges for Research Collaboration in and with Africa</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Melber</surname>
                  <given-names>Henning</given-names>
               </string-name>
               <xref rid="a1" ref-type="aff">*</xref>
               <aff id="a1">
                  <label>*</label>Director emeritus/Senior Adviser, Dag Hammarskjöld Foundation, Uppsala; Extraordinary Professor, Department of Political Sciences/University of Pretoria and Centre for Africa Studies/University of the Free State, Bloemfontein.</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2015</year>
            <string-date>2015</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e90000039</issue-id>
         <fpage>21</fpage>
         <lpage>42</lpage>
         <permissions>
            <copyright-statement>© Council for the Development of Social Science Research in Africa, 2015</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/90000042"/>
         <abstract>
            <label>Abstract</label>
            <p>This article engages with the challenges facing genuine research collaboration and knowledge production in a North–South interaction. It maps the asymmetries in global knowledge production in general and revisits African realities in particular. Using the experiences of the Norwegian research programme NORGLOBAL as an empirical reference point, it critically explores the limitations of partnerships and identifies some challenges resulting from the centuries of Northern hegemony established in all spheres related to global affairs and interactions. It presents some thoughts and suggestions as to how these limitations might be reduced or eliminated in favour of a truly joint effort to meet the challenges on the way towards equal relations and mutual respect.</p>
         </abstract>
         <trans-abstract xml:lang="fre">
            <label>Résumé</label>
            <p>Le présent article se penche sur les défis en matière de collaboration de recherche véritable et de production de connaissances, dans le cadre d’une interaction Nord-Sud. Il dresse les asymétries dans la production mondiale de connaissances en général et revisite les réalités africaines en particulier. En utilisant les expériences du programme de recherche norvégien NORGLOBAL comme point de référence empirique, il explore de manière critique les limites des partenariats et identifie quelques défis résultant des siècles d’hégémonie du Nord établie dans tous les domaines liés aux affaires et interactions à l’échelle mondiale. Il présente quelques réflexions et suggestions quant à la façon dont ces limites pourraient être réduites ou éliminées en faveur d’un véritable effort conjoint pour relever les défis et arriver à des relations d’égalité et de respect mutuel.</p>
         </trans-abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group content-type="unparsed-citations">
         <title>Notes</title>
         <fn id="fn1">
            <label>1.</label>
            <p>An earlier version of the paper was prepared for a meeting on ‘Enhancing collaboration between the development aid and the global environmental change communities to achieve development goals in Africa’, organized by the International Council for Science (ICSU) and the Swedish Development Agency (Sida) on 5 and 6 May 2014 at the Soweto Campus of the University of Johannesburg. I thank Inger-Ann Ullstein, Jan Monteverde Haakonsen and their colleagues from the NRC and Kevin Noone and other members on the Programme Board of NORGLOBAL for the fruitful cooperation during the last years and all the indirect inputs to this paper, for which I bear the full and sole responsibility. Its final revisions also benefitted from the observations shared by the reviewers.</p>
         </fn>
         <fn id="fn2">
            <label>2.</label>
            <p>
               <mixed-citation publication-type="other">Gilles Carbonnier/Tiina Kontinen, North–South Research Partnerships: Academia Meets Development? Bonn: EADI (EADI Policy Paper Series), June 2014, 3</mixed-citation>. For a considerably modified later version see<mixed-citation publication-type="other">Gilles Carbonnier/Tiina Kontinen, ‘Institutional learning in North-South research partnerships’, Revue Tiers Monde, 221, January–March 2015, 149–62</mixed-citation>.</p>
         </fn>
         <fn id="fn3">
            <label>3.</label>
            <p>
               <mixed-citation publication-type="other">Maria Eriksson Baaz, The Paternalism of Partnership: A Postcolonial Reading of Identity in Development Aid, London: Zed 2005</mixed-citation>.</p>
         </fn>
         <fn id="fn4">
            <label>4.</label>
            <p>See the review by<mixed-citation publication-type="other">Jonathan Goodhand, Journal of Agrarian Change, 7 (3): 418–20</mixed-citation>. For a general, critical assessment of the developmental discourses in current forms of cooperation see<mixed-citation publication-type="other">inter alia the special issue of Progress in Development Studies 6 (1), 2006, guest edited by Uma Kothari as well as chapters in Mark Duffield/Vernon Hewitt, Empire, Development &amp; Colonialism. The Past in the Present, Woodbridge: James Currey, 2009</mixed-citation>.</p>
         </fn>
         <fn id="fn5">
            <label>5.</label>
            <p>See the challenge to<mixed-citation publication-type="other">Slavo Zizek by Hamid Dabashi, ‘The discrete charm of European intellectuals’, International Journal of Zizek Studies 3 (4), 2009. Accessible at http://zizekstudies.org/index.php/ijzs/article/view/220/314</mixed-citation>. See also<mixed-citation publication-type="other">Hamid Dabashi, Can Non-Europeans Think?, London: Zed, 2015</mixed-citation>.</p>
         </fn>
         <fn id="fn6">
            <label>6.</label>
            <p>
               <mixed-citation publication-type="other">World Social Science Report 2010. Knowledge Divides. Paris: United Nations Educational, Scientific and Cultural Organization (UNESCO) and International Social Science Council (ISSC) 2010</mixed-citation>.</p>
         </fn>
         <fn id="fn7">
            <label>7.</label>
            <p>
               <mixed-citation publication-type="other">Paulin J. Houtondji, ‘Scientific dependence in Africa today’, Research in African Literature 21 (3): 5–15</mixed-citation>.</p>
         </fn>
         <fn id="fn8">
            <label>8.</label>
            <p>
               <mixed-citation publication-type="other">Cf. Peter Weingart, ‘Knowledge and Inequality’, in Göran Therborn, ed., Inequalities of the World. New theoretical frameworks, multiple empirical approaches. London: Verso, 2006, 163–90</mixed-citation>.</p>
         </fn>
         <fn id="fn9">
            <label>9.</label>
            <p>Published on 17 March 2015 as ’Africa Focus, Africa: Higher Education Must be Higher Priority’, http://allafrica.com/stories/201503190891.html.</p>
         </fn>
         <fn id="fn10">
            <label>10.</label>
            <p>
               <mixed-citation publication-type="other">Lewis R. Gordon, ’Disciplinary decadence and the decolonisation of knowledge’, Africa Development 39 (1), 2014, pp. 81–92 (here: p. 81; italics in the original)</mixed-citation>.</p>
         </fn>
         <fn id="fn11">
            <label>11.</label>
            <p>See on this the revised PhD thesis by<mixed-citation publication-type="other">Wiebke Keim, Vermessene Disziplin. Zum konterhegemonialen Potential afrikanischer und lateinamerikanischer Soziologien. Bielefeld: transcript 2008</mixed-citation>.</p>
         </fn>
         <fn id="fn12">
            <label>12.</label>
            <p>Revealing for this anomy have already been the early insights presented in the chapter ‘The Pitfalls of National Consciousness’ in the manifesto by<mixed-citation publication-type="other">Frantz Fanon, The Wretched of the Earth, Harmondsworth: Penguin, 1967 (French original 1961)</mixed-citation>; but also the mimicry he disclosed in his earlier work,<mixed-citation publication-type="other">Black Skin, White Masks, New York: Grove Press, 1967 (French original 1952)</mixed-citation>.</p>
         </fn>
         <fn id="fn13">
            <label>13.</label>
            <p>
               <mixed-citation publication-type="other">Thandika Mkandawire, ‘Running while others walk: knowledge and the challenge of Africa’s development’, Africa Development, 36 (2), 2011, pp. 1–36 (here: p. 15)</mixed-citation>.</p>
         </fn>
         <fn id="fn14">
            <label>14.</label>
            <p>ibid.</p>
         </fn>
         <fn id="fn15">
            <label>15.</label>
            <p>See among the numerous noteworthy efforts some of the results facilitated by CODESRIA, such as the contributions to<mixed-citation publication-type="other">Paul Tiyambe Zeleza/Adebayo Olukoshi, eds, African Universities in the Twenty-First Century. Volume II: Knowledge and Society, Dakar: CODESRIA, 2004</mixed-citation>;<mixed-citation publication-type="other">Paul Tiyambe Zeleza, ed., The Study of Africa. Volume I: Disciplinary and Interdisciplinary Encounters, Dakar: CODESRIA, 2006</mixed-citation>; as well as<mixed-citation publication-type="other">Alois Mlambo, ed., African Scholarly Publishing, Oxford: African Books Collective, Dag Hammarskjöld Foundation and the International Network for the Availability of Scientific Publications, 2006</mixed-citation>. For a summary overview on the impact of African intellectuals challenging the restrictive system see<mixed-citation publication-type="other">Thandika Mkandawire, ‘African intellectuals, political culture and development’, Austrian Journal of Development Studies 18 (1) 2002, pp. 31–47</mixed-citation>.</p>
         </fn>
         <fn id="fn16">
            <label>16.</label>
            <p>
               <mixed-citation publication-type="other">Ibrahim Oanda Ogachi, ‘Neo-liberalism and the subversion of academic freedom from within: money, corporate cultures and “captured” intellectuals in African public universities’, Journal of Higher Education in Africa 9 (1&amp;2), 2011, pp. 25–47 (here: p. 44)</mixed-citation>.</p>
         </fn>
         <fn id="fn17">
            <label>17.</label>
            <p>Recommendations for Reinvigorating the Humanities in Africa. Submitted by the Forum on the Humanities in Africa of the African Humanities Program. University of South Africa, 7 June 2014. For consideration by the African Higher Education Summit.<mixed-citation publication-type="other">Dakar, Senegal, 10–12 March 2015, p. 11. Released on 5 December 2014 at: http://www.acls.org/Publications/Programs/Reinvigorating_the_Humanities_in_Africa.pdf</mixed-citation>.</p>
         </fn>
         <fn id="fn18">
            <label>18.</label>
            <p>
               <mixed-citation publication-type="other">World Social Science Report 2010, pp. 44ff</mixed-citation>.</p>
         </fn>
         <fn id="fn19">
            <label>19.</label>
            <p>
               <mixed-citation publication-type="other">Adebayo Olukoshi, ‘African Scholars and African Studies’, in Henning Melber, ed., On Africa. Scholars and African Studies. Contributions in Honour of Lennart Wohlgemuth. Uppsala: Nordic Africa Institute 2007, pp. 7–22 (here: p. 17)</mixed-citation>.</p>
         </fn>
         <fn id="fn20">
            <label>20.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 15</mixed-citation>.</p>
         </fn>
         <fn id="fn21">
            <label>21.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 18</mixed-citation>. Cf.<mixed-citation publication-type="other">Mahmood Mamdani, ‘African studies made in USA’, CODESRIA Bulletin 2, 1990</mixed-citation>; and<mixed-citation publication-type="other">‘The Challenge of the Social Sciences in the 21st Century’, in Ruth Mukama and Murindwa Rutange, eds, Confronting 21st Century Challenges: Analyses and Re-dedications by National and International Scholars. Volume 1, Kampala: Faculty of Social Sciences and Makerere University, 2004</mixed-citation>.</p>
         </fn>
         <fn id="fn22">
            <label>22.</label>
            <p>For some critical reflections on the contested notion of African Studies from the perspectives of an European scholar see<mixed-citation publication-type="other">Henning Melber, ‘The relevance of African Studies’, Stichproben. Vienna Journal of African Studies 9 (16), 2009, pp. 183–200</mixed-citation>, and<mixed-citation publication-type="other">Henning Melber, ‘What is African in Africa(n) studies? Confronting the (mystifying) power of ideology and identity’, Africa Bibliography 2013, November 2014, pp. vii–xvii</mixed-citation>.</p>
         </fn>
         <fn id="fn23">
            <label>23.</label>
            <p>See note 2.</p>
         </fn>
         <fn id="fn24">
            <label>24.</label>
            <p>
               <mixed-citation publication-type="other">Carbonnier/Kontinen, ’Institutional learning’, p. 159</mixed-citation>.</p>
         </fn>
         <fn id="fn25">
            <label>25.</label>
            <p>Further details at: «http://www.forskningsradet.no/prognettnorglobal/Home_page/1224698160055» http://www.forskningsradet.no/prognettnorglobal/Home_page/1224698160055. The designed first chairperson of the newly established Programme Board, Carl-Erik Schulz, died in a tragic accident while hiking on Table Mountain on 30 November 2008 and so could not take up this function. I dedicate this article to his memory.</p>
         </fn>
         <fn id="fn26">
            <label>26.</label>
            <p>In addition, NORGLOBAL cooperated occasionally with other programmes in the Research Council, such as INDNOR on India. The programme board also handled a limited call sponsored by the Norwegian embassy in Malawi, which allocated funds to research projects aimed at presenting findings to strengthen political governance.</p>
         </fn>
         <fn id="fn27">
            <label>27.</label>
            <p>In 2013, NORGLOBAL had a disposable budget of some NOK 153.5 million (around US$ 25 million), of which for the first three months some NOK 51.5 million (around US$ 8.5 million) were used.</p>
         </fn>
         <fn id="fn28">
            <label>28.</label>
            <p>The report was drafted in August/September 2013 by the board’s chairperson Helge Hveem, based on the inputs of the other board members.</p>
         </fn>
         <fn id="fn29">
            <label>29.</label>
            <p>Both Helge Hveem, the chairperson of NORGLOBAL 1, and Øyvind Østeryd, the chairperson of NORGLOBAL 2, are among the top senior scholars in Development Studies in Norway, and have published critical opinion articles in established Norwegian print media, appealing to correct this situation. For another critical intervention see the opinion article by<mixed-citation publication-type="other">John Y. Jones/Henning Melber in Development Today 8, August 2015</mixed-citation>.</p>
         </fn>
         <fn id="fn30">
            <label>30.</label>
            <p>
               <mixed-citation publication-type="other">Erna Solberg/Børge Brende, ‘No Education, No Development’, published on 6 July 2015 in Project Syndicate. Accessible at «https://www.regjeringen.no/en/aktuelt/no-education-no-development/id2426818/» https://www.regjeringen.no/en/aktuelt/no-education-no-development/id2426818/</mixed-citation>.</p>
         </fn>
         <fn id="fn31">
            <label>31.</label>
            <p>The Prime Minister’s Opening Speech at the Oslo Summit of Education for Development, 7 July 2015. Accessible at: https://www.regjeringen.no/en/aktuelt/the-prime-ministers-opening-speech-at-the-oslo-summit-of-education-for-development/id2426937/.</p>
         </fn>
         <fn id="fn32">
            <label>32.</label>
            <p>See «http://www.icsu.org/future-earth» www.icsu.org/future-earth and «http://www.futureearth.info» www.futureearth.info.</p>
         </fn>
         <fn id="fn33">
            <label>33.</label>
            <p>
               <mixed-citation publication-type="other">Future Earth, Future Earth Initial Design: Report of the Transition Team, Paris: International Council for Science (ICSU), 2013, p. 11</mixed-citation>.</p>
         </fn>
         <fn id="fn34">
            <label>34.</label>
            <p>
               <mixed-citation publication-type="other">United Nations Educational, Scientific and Cultural Organization (UNESCO) and International Social Science Council (ISSC), World Social Science Report 2013. Changing Global Environments, Paris: OECD Publishing and UNESCO Publishing, 2013</mixed-citation>.</p>
         </fn>
         <fn id="fn35">
            <label>35.</label>
            <p>
               <mixed-citation publication-type="other">World Social Science Report 2013. Summary, p. 5</mixed-citation>.</p>
         </fn>
         <fn id="fn36">
            <label>36.</label>
            <p>See http://www.reflectiongroup.org/.</p>
         </fn>
         <fn id="fn37">
            <label>37.</label>
            <p>
               <mixed-citation publication-type="other">No future without justice. Report of the Civil Society Reflection Group on Global Development Perspectives, Uppsala: Dag Hammarskjöld Foundation, 2012 (Development Dialogue 59). Accessible at: http://www.dhf.uu.se/publications/development-dialogue/dd59/</mixed-citation>.</p>
         </fn>
         <fn id="fn38">
            <label>38.</label>
            <p>See for a summary on some of the contributions<mixed-citation publication-type="other">Henning Melber, ed., 50 Years Dag Hammarskjöld Foundation,, Uppsala: Dag Hammarskjöld Foundation 2012 (Development Dialogue 60). Accessible at: «http://www.dhf.uu.se/publications/development-dialogue/dd60/» http://www.dhf.uu.se/publications/development-dialogue/dd60/</mixed-citation>; and more specifically by<mixed-citation publication-type="other">Niclas Hällström with Robert Österbergh, eds, What Next Volume III: Climate, Development and Equity, Uppsala: Dag Hammarskjöld Foundation and What Next Forum 2012 (Development Dialogue 61). Accessible at: http://www.dhf.uu.se/publications/development-dialogue/dd61/. The Foundation’s web site offers access to several more related publications</mixed-citation>.</p>
         </fn>
         <fn id="fn39">
            <label>39.</label>
            <p>See<mixed-citation publication-type="other">Henning Melber, ‘Whose world? Development, civil society, development studies and (not only) scholar activists’, Third World Quarterly 35 (6), 2014, pp. 1082–97</mixed-citation>.</p>
         </fn>
         <fn id="fn40">
            <label>40.</label>
            <p>
               <mixed-citation publication-type="other">World Social Science Report 2013. Summary, p. 4</mixed-citation>.</p>
         </fn>
         <fn id="fn41">
            <label>41.</label>
            <p>This observation by no means suggests that NGO-interactions are immune from paternalistic forms of collaboration. Rather, they reproduce to a large extent similar problems and challenges as encountered in the collaboration among scholars and academic institutions. See as examples some of the articles in the special issue of the<mixed-citation publication-type="other">Austrian Journal of Development Studies 31 (1), 2015 on ‘Civil society, cooperation and development’</mixed-citation>.</p>
         </fn>
         <fn id="fn42">
            <label>42.</label>
            <p>
               <mixed-citation publication-type="other">Carbonnier/Kontinen, North-South Research Partnerships, p. 10</mixed-citation>.</p>
         </fn>
         <fn id="fn43">
            <label>43.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 9</mixed-citation>.</p>
         </fn>
         <fn id="fn44">
            <label>44.</label>
            <p>
               <mixed-citation publication-type="other">ibid., pp. 5 and 7</mixed-citation>.</p>
         </fn>
         <fn id="fn45">
            <label>45.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 16</mixed-citation>.</p>
         </fn>
         <fn id="fn46">
            <label>46.</label>
            <p>See for a critique of such discourses<mixed-citation publication-type="other">Henning Melber, ‘Africa and the middle class(es)’, Africa Spectrum, 48 (3), 2013, pp. 111–20</mixed-citation>and<mixed-citation publication-type="other">‘Where and what (for) is the middle? Africa and the middle class(es)’, European Journal of Development Research, 27 (2), 2015, pp. 246–54</mixed-citation>.</p>
         </fn>
         <fn id="fn47">
            <label>47.</label>
            <p>
               <mixed-citation publication-type="other">World Social Science Report 2013. Summary, p. 14</mixed-citation>.</p>
         </fn>
         <fn id="fn48">
            <label>48.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 12</mixed-citation>.</p>
         </fn>
         <fn id="fn49">
            <label>49.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 9</mixed-citation>.</p>
         </fn>
         <fn id="fn50">
            <label>50.</label>
            <p>
               <mixed-citation publication-type="other">Heide Hackmann/Asunción Lera St. Clair, Transformative Cornerstones of Social Science Research for Global Change, Paris: International Social Science Council, May 2012, p. 8</mixed-citation>.</p>
         </fn>
         <fn id="fn51">
            <label>51.</label>
            <p>
               <mixed-citation publication-type="other">‘Enhancing collaboration between the development aid and the global environmental change communities to achieve development goals in Africa’, conference held at the University of Johannesburg, Soweto campus, 5–6 May 2014</mixed-citation>.</p>
         </fn>
         <fn id="fn52">
            <label>52.</label>
            <p>
               <mixed-citation publication-type="other">World Social Science Report 2013. Summary, p. 7</mixed-citation>.</p>
         </fn>
         <fn id="fn53">
            <label>53.</label>
            <p>
               <mixed-citation publication-type="other">ibid., p. 4</mixed-citation>.</p>
         </fn>
         <fn id="fn54">
            <label>54.</label>
            <p>
               <mixed-citation publication-type="other">‘Future Earth’s “global” secretariat under fire’, SciDevNet, 23 July 2014. Available at: «http://www.scidev.net/global/sustainability/news/future-earth-global-secretariat.html» http://www.scidev.net/global/sustainability/news/future-earth-global-secretariat.html</mixed-citation>.</p>
         </fn>
         <fn id="fn55">
            <label>55.</label>
            <p>Core members include the ICSU, the ISSC, the IGFA/Belmont Forum, UNESCO, UNEP, UNU and the World Meteorological Organization (WMO).</p>
         </fn>
         <fn id="fn56">
            <label>56.</label>
            <p>
               <mixed-citation publication-type="other">‘Future Earth’s globally distributed secretariat designed to be flexible and diverse’, 7 August 2014. Available at «http://www.futureearth.info/news/future-earths-globally-distributed-secretariat-designed-be-flexible-and-diverse»http://www.futureearth.info/news/future-earths-globally-distributed-secretariat-designed-be-flexible-and-diverse</mixed-citation>.</p>
         </fn>
         <fn id="fn57">
            <label>57.</label>
            <p>China, for example, reportedly took an explicit decision not to bid to host the secretariat, while potential Indian partners did not respond to invitations to participate in the bidding process. Nor was there any African organization willing to act as a host for a hub or to coordinate a bid.</p>
         </fn>
         <fn id="fn58">
            <label>58.</label>
            <p>
               <mixed-citation publication-type="other">Carbonnier/Kontinen, ‘Institutional learning’, p. 160</mixed-citation>.</p>
         </fn>
         <fn id="fn59">
            <label>59.</label>
            <p>
               <mixed-citation publication-type="other">Mkandawire, ‘Running while others walk’, p. 25</mixed-citation>.</p>
         </fn>
         <fn id="fn60">
            <label>60.</label>
            <p>
               <mixed-citation publication-type="other">John Comaroff and Jean Comaroff, Theory from the South or, How Euro-America is Evolving toward Africa.,London: Paradigm 2012</mixed-citation>.</p>
         </fn>
         <fn id="fn61">
            <label>61.</label>
            <p>
               <mixed-citation publication-type="other">Ksenia Robbe, ‘African studies at a crossroads: producing theory across the disciplines in South Africa’, Social Dynamics 40 (2), 2014, pp. 255–73 (here: p. 259)</mixed-citation>.</p>
         </fn>
      </fn-group>
   </back>
</article>
