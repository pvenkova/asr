<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">kronos</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50001104</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Kronos</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Department of History and the Centre for Humanities Research at the University of the Western Cape</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02590190</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43859439</article-id>
         <title-group>
            <article-title>Urban Research in a Hostile Setting: Godfrey Wilson in Broken Hill, Northern Rhodesia, 1938-1940</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>KAREN TRANBERG</given-names>
                  <surname>HANSEN</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2015</year>
         </pub-date>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">41</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40158508</issue-id>
         <fpage>193</fpage>
         <lpage>214</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43859439"/>
         <abstract>
            <p>Acknowledged for his pioneering urban anthropological research in Broken Hill through the publication of An Essay on the Economics of Detribalization in Northern Rhodesia (Parts I and II, 1940 and 1941), Godfrey Wilson's professional career was cut short by his death during World War II. The late 1990s transfer and cataloguing at the University of Cape Town of the Monica and Godfrey Wilson papers has made an enormously rich research archive accessible to the public. For the first time. Godfrey Wilson's notes from his fieldwork in Broken Hill enable us to examine his research project through his own observations. Based on a preliminary overview of these records and a tentative analysis of some of their contents, this article revisits the Broken Hill research project against the background of the published essay on the economics of detribalisation. Wilson's argument about temporary urbanisation is demonstrated through migration histories, information about length of stay in towns, and an analysis of the economics of urban livelihoods that focuses on wages including rations, household expenditures, and urban-rural transfers. But the published essay barely explains how in fact he conducted his field research. Although there are very few direct indications, we can infer some of his fieldwork practices and field methodology from notes that occasionally evoke an immediate sense of the trials and tribulations of everyday African life in Broken Hill in the early World War II years. How might experiences in the field have influenced Wilsons analysis? Overall, I discuss his work from two angles, first in the context of a time and place characterised by conflicting agendas, and secondly, in retrospect as the conceptual space and time of early World War II colonial Northern Rhodesia have yielded to different explanatory perspectives.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d85e107a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d85e114" publication-type="other">
G. Wilson, An Essay on the Economics of Detribalization in Northern Rhodesia, Parts I and II, Rhodes-Livingstone Papers 5 and 6
(1941 and 1942) (Manchester: Manchester University Press, 1968).</mixed-citation>
            </p>
         </fn>
         <fn id="d85e124a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d85e131" publication-type="other">
J. Ferguson, 'Mobile Workers, Modernist Narratives:
A Critique of the Historiography of Transition on the Zambian Copperbelt', Parts 1 and 2, Journal of Southern African Studies,
16, 3, 1990, 385-412, and 16, 4, 1990, 603-21;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e143" publication-type="other">
H. Macmillan, 'The Historiography of Transition on the Zambian Copperbelt:
Another View', Journal of Southern African Studies, 19, 4, 1993, 681-712;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e152" publication-type="other">
J. Ferguson, 'Modernist Narratives, Conventional
Wisdoms, and Colonial Liberalism: Reply to a Straw Man, Journal of Southern African Southern African Studies , 20, 4, 1994,
633-40;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e165" publication-type="other">
H. Macmillan, 'More Thoughts on the Transition on the Zambian Copperbelt', Journal of Southern African Studies, 22,
2, 1996, 309-12;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e174" publication-type="other">
J. Ferguson, 'Urban Trends on the Zambian Copperbelt: A Short Bibliographic Note, Journal of Southern
African Studies, 22, 2, 1996, 313.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e184a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d85e191" publication-type="other">
A. Bank and L.J. Bank (eds), Inside African Anthropology: Monica Wilson and her Interpreters (London: The
International African Institute and Cambridge University Press, 2013), xiii. When Lyn Schumaker conducted research on the
Rhodes-Livingstone Institute, she had very limited access to Godfrey Wilsons papers. L. Schumaker, Africanizing Anthropology:
Fieldwork, Networks, and the Making of Cultural Knowledge in Central Africa (Durham and London: Duke University Press,
2001), 59-65, 280-2, nn 58-79.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e209" publication-type="other">
R. Brown, 'Anthropology and Colonial Rule: Godfrey Wilson and the Rhodes-
Livingstone Institute, Northern Rhodesia' in Talal Asad (ed), Anthropology and the Colonial Encounter (New York: Humanities,
1973), 173-98.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e222a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d85e229" publication-type="other">
A. Bank and L.J. Bank (eds), Inside African Anthropology.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e237a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d85e244" publication-type="other">
H. Heisler, Urbanisation and the Government of Migration: The Inter-Relation of Urban and
Rural Life in Zambia (New York: St. Martins, 1974), 127.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e254a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d85e261" publication-type="other">
E12.1 Rhodes-Livingstone Institute, Directors Report to the Trustees of the Rhodes-Livingstone Institute, On the Work of the
First Three Years (1938-40), 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e271a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d85e278" publication-type="other">
B5.1, Letters from Monica Wilson to her father, 24 February 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e285a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d85e292" publication-type="other">
J.F. Holleman and S. Biesheuvel, White Mine Workers in Northern Rhodesia 1959-60 (Leiden: Afrika-Studiencentrum, 1973)
77-82.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e302a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d85e309" publication-type="other">
G. Wilson, Essay, Part I, 35.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e316a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d85e323" publication-type="other">
Ibid, 22.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e329" publication-type="other">
G.
Chauncey Jr, 'The Locus of Reproduction: Women's Labour in the Zambian Copperbelt, 1927-1953', Journal of Southern African
Studies, 7, 2, 1981, 139.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e343a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d85e350" publication-type="other">
H. Macmillan, An African Trading Empire: The Story of Susman Brothers &amp; Wulfsohn, 1901-2005 (London: I.B. Taurus, 2005),
142.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e360a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d85e367" publication-type="other">
Ibid, Letters, 9 February 1939, 20 December 1939, 4 March 1940, 17 March 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e374a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d85e381" publication-type="other">
Ibid, Letters, 21 April 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e388a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d85e395" publication-type="other">
E12.1, Directors Report, On the Work of the First Three Years, 5.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e402a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d85e409" publication-type="other">
M. Wilson, 'The First Three Years, 1938-41', African Social Research, 24, 1977, 279.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e416a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d85e423" publication-type="other">
E 12.1, Director's Report, On the Work of the First Three Years, 4-5.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e431a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d85e438" publication-type="other">
S. Morrow, "'This Is from the Firm": The Anthropological Partnership of Monica and Godfrey Wilson, Unpublished paper
presented at the Monica Hunter Wilson Centenary Conference, Hunterstoun, South Africa, 24-26 June 2008.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e448a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d85e455" publication-type="other">
B4.11, Correspondence, Letter from Godfrey Wilson to B. Malinowski, 2 February 1938.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e462a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d85e469" publication-type="other">
B5.1, Letters, 3 July and 24 July 1938, 26 January 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e476a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d85e483" publication-type="other">
E9.4, Native Welfare, Lines of Investigation, 21 February 1939 and E9.15, Mine Compound and Mine Work, List of questions to
ask in interview, 10 July 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e493a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d85e500" publication-type="other">
G. Wilson, Essay, Part II, 75.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e507a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d85e514" publication-type="other">
Ibid, 20 March 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e522a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d85e529" publication-type="other">
Ibid, 10 December 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e536a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d85e543" publication-type="other">
Ibid, 16 January 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e550a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d85e557" publication-type="other">
Ibid, 9 February 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e564a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d85e571" publication-type="other">
E2.3, General Condition and Structure or Compounds: Includes Labour Returns and Notes or Interviews with Mine Managers,
27 November 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e581a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d85e588" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e595a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d85e602" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e610a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d85e617" publication-type="other">
G. Wilson, Essay, Part II, 36, 71, 72.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e624a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d85e631" publication-type="other">
Ibid.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e638a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d85e645" publication-type="other">
'Tribal Elders to Trade Unions' in
A.L. Epstein, Scenes from African Urban Life: Collected Copperbelt Essays (Edinburgh: Edinburgh University Press, 1992), 42-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e655a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d85e662" publication-type="other">
E6.1, Broken Hill, Railway Compound. Outline Biographies, 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e669a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d85e676" publication-type="other">
E7.1, Outline Biographies of Six People from Other Places in Broken Hill (including aerodrome), 20 February 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e683a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d85e690" publication-type="other">
E9.3, Churches and Schools, 25 January 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e698a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d85e705" publication-type="other">
E9.8, Houseboys, 11 December 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e712a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d85e719" publication-type="other">
E9.9. Dancing, 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e726a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d85e733" publication-type="other">
A. Bank, 'Family, Friends and Mentors: Monica Hunter at Lovedale and Cambridge' in A. Bank and L.J. Bank (eds), Inside African
Anthropology, 39-45.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e743a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d85e750" publication-type="other">
E9.3, Churches and Schools, 16 February 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e757a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d85e764" publication-type="other">
Ibid, 16 January 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e771a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d85e778" publication-type="other">
E9.14, Biographies about Various Individuals, 21 January 1940.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e784" publication-type="other">
Robert I. Rotberg, The Rise of Nationalism in Central Africa:
The Making of Malawi and Zambia 1873-1964 (Cambridge MA: Harvard University Press, 1965), 126-8. Hugh Macmillan drew
my attention to this.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e798a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d85e805" publication-type="other">
E7.1, Outline Biographies or Six People trom Other Places in Broken Hill (including aerodrome), 23 February 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e812a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d85e819" publication-type="other">
E9.5, Legal Proceedings, 21 March 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e826a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d85e833" publication-type="other">
A. Richards, 'The Rhodes-Livingstone Institute: An Experiment in Research, 1933-38', African Social Research, 24, 1977, 275-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e840a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d85e847" publication-type="other">
G. Wilson, Essay, Part II, 42.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e854a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d85e861" publication-type="other">
Ibid, Part I, 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e868a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d85e875" publication-type="other">
G. Wilson, Essay, Part II, 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e883a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d85e890" publication-type="other">
E1. 14, Notebooks of Assistants, Recording Hut Censuses, December 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e897a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d85e904" publication-type="other">
E3.3, Town Compound, General Conditions and Structure, 29 April 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e911a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d85e918" publication-type="other">
E9.9, Dancing, 8 February 1940, 4 March 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e925a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d85e932" publication-type="other">
G. Wilson, Essay, Part II, 49-73.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e939a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d85e946" publication-type="other">
Ibid, Part I, 21, Table III; 41, Table IV.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e953a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d85e960" publication-type="other">
G. Wilson, Essay, Part II, 79.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e968a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d85e975" publication-type="other">
Ibid, Part I, 36.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e982a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d85e989" publication-type="other">
Ibid, Part II, 41.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e996a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d85e1003" publication-type="other">
E7.1, Outline Biographies of Six People from Other Places in Broken Hill (including aerodrome), Wilson Mundula interview, 18
March 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1013a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d85e1020" publication-type="other">
I. Henderson, 'Early African Leadership: The Copperbelt Disturbances of 1935 and 1940', Journal of Southern African Studies, 2,
1, 1975, 83-97.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1030a1310">
            <label>80</label>
            <p>
               <mixed-citation id="d85e1037" publication-type="other">
G. Wilson, Essay, Part II, 75.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1044a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d85e1051" publication-type="other">
Ibid, 15.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1059a1310">
            <label>82</label>
            <p>
               <mixed-citation id="d85e1066" publication-type="other">
Ibid, 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1073a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d85e1080" publication-type="other">
A. Roberts, A History of Zambia (London: Heinemann, 1981 [1976]), 191.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1087a1310">
            <label>84</label>
            <p>
               <mixed-citation id="d85e1094" publication-type="other">
A. Richards. Land, Labour and Diet: An Economic Study of the Bemba Tribe (London: Oxford University Press, 1939).</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1101a1310">
            <label>86</label>
            <p>
               <mixed-citation id="d85e1108" publication-type="other">
R.L. Frain, the chairman ot the Roan Selected Trust, one or the two major mining groups on the Copperbelt, explained shins in
the mining policy towards African urbanization in "The Stabilisation of Labour in the Northern Rhodesian Copperbelt', African
Affairs, 55, 1956, 305-17.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1121a1310">
            <label>87</label>
            <p>
               <mixed-citation id="d85e1128" publication-type="other">
Heisler, Urbanisation, 103.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1135a1310">
            <label>88</label>
            <p>
               <mixed-citation id="d85e1142" publication-type="other">
G. Wilson, Essay, Part II, 14 n 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1150a1310">
            <label>89</label>
            <p>
               <mixed-citation id="d85e1157" publication-type="other">
Ibid, 80.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1164a1310">
            <label>90</label>
            <p>
               <mixed-citation id="d85e1171" publication-type="other">
G. Wilson, Essay, Part I, 46.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1178a1310">
            <label>92</label>
            <p>
               <mixed-citation id="d85e1185" publication-type="other">
bssay, Part II, Table
XIV, 10.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1195a1310">
            <label>93</label>
            <p>
               <mixed-citation id="d85e1202" publication-type="other">
G. Wilson. Essay, Part I, 47.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1209a1310">
            <label>94</label>
            <p>
               <mixed-citation id="d85e1216" publication-type="other">
K.T. Hansen, Salaula: The World of Secondhand Clothing and Zambia (Chicago: University of Chicago Press, 2000), 47.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1223a1310">
            <label>95</label>
            <p>
               <mixed-citation id="d85e1230" publication-type="other">
E1. 5, Notebooks of Assistants, Recording Hut Censuses.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1238a1310">
            <label>96</label>
            <p>
               <mixed-citation id="d85e1245" publication-type="other">
G. Wilson, Essay, Part II, 77.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1252a1310">
            <label>97</label>
            <p>
               <mixed-citation id="d85e1259" publication-type="other">
Ibid, 24, n 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1266a1310">
            <label>98</label>
            <p>
               <mixed-citation id="d85e1273" publication-type="other">
Ibid, 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1280a1310">
            <label>99</label>
            <p>
               <mixed-citation id="d85e1287" publication-type="other">
Ibid, 35.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1294a1310">
            <label>100</label>
            <p>
               <mixed-citation id="d85e1301" publication-type="other">
Hansen. Salaula, 35.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1308a1310">
            <label>101</label>
            <p>
               <mixed-citation id="d85e1315" publication-type="other">
R. Ross. Clothing: A Global History (Cambridge: Polity, 2008), 12</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1323a1310">
            <label>102</label>
            <p>
               <mixed-citation id="d85e1330" publication-type="other">
G. Wilson, Essay, Part II 15.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1337a1310">
            <label>103</label>
            <p>
               <mixed-citation id="d85e1344" publication-type="other">
Ibid, 19.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1351a1310">
            <label>104</label>
            <p>
               <mixed-citation id="d85e1358" publication-type="other">
E9.9, Dancing, Dance of the new Broken Hill dance club, Dance organized by the Native Welfare Association, 8 February 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1365a1310">
            <label>105</label>
            <p>
               <mixed-citation id="d85e1372" publication-type="other">
B5.1, Letters, 4 March 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1379a1310">
            <label>106</label>
            <p>
               <mixed-citation id="d85e1386" publication-type="other">
E9.9, Dancing, Typescript notes by Monica Wilson on frocks, African dance, 2 March 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1393a1310">
            <label>107</label>
            <p>
               <mixed-citation id="d85e1400" publication-type="other">
G. Wilson, Essay, Part II, 20. In footnote 3, he qualifies Japans role as a major clothing exporter at least in normal times.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1408a1310">
            <label>108</label>
            <p>
               <mixed-citation id="d85e1415" publication-type="other">
E9.16, Miscellaneous, Census of Broken Hill Mine Market on a Market Day, 31 January 1940.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1422a1310">
            <label>109</label>
            <p>
               <mixed-citation id="d85e1429" publication-type="other">
E 2.1.4, Broken Hill Mine Compound. Notes on Canda, 6 December 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1436a1310">
            <label>110</label>
            <p>
               <mixed-citation id="d85e1443" publication-type="other">
E4.1, Outline Biographies of People Living on the Mine Farms, William Busuka, 31 January 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1450a1310">
            <label>111</label>
            <p>
               <mixed-citation id="d85e1457" publication-type="other">
G. Wilson, Essay, Part II, 11, 36-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1464a1310">
            <label>112</label>
            <p>
               <mixed-citation id="d85e1471" publication-type="other">
Ibid, 20.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1478a1310">
            <label>114</label>
            <p>
               <mixed-citation id="d85e1485" publication-type="other">
E4.1, Outline Biographies of People Living on the Mine Farms, 31 January 1939, 2 February 1939.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1493a1310">
            <label>115</label>
            <p>
               <mixed-citation id="d85e1500" publication-type="other">
G. Wilson, Essay, Part II, 18.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1507a1310">
            <label>117</label>
            <p>
               <mixed-citation id="d85e1514" publication-type="other">
Ibid, E1.9, Notebook, Livingstone 1938, 18 May 1938;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e1520" publication-type="other">
Jabobs to District Commissioner, 29 June 1936.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1527a1310">
            <label>118</label>
            <p>
               <mixed-citation id="d85e1534" publication-type="other">
Rebecca Marsland, '"Fraternizing with the Africans": Godfrey Wilson and Masculine "Good Company" in 1930s Bunyakyusa.'
Paper in progress presented at the Royal Anthropological Institute seminar, London, on 19 June 2013.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1544a1310">
            <label>119</label>
            <p>
               <mixed-citation id="d85e1551" publication-type="other">
Godfrey Wilson and Monica Wilson, The Analysis of Social Change: Based on Observations in Central Africa (Cambridge:
Cambridge University Press, 1945).</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1561a1310">
            <label>120</label>
            <p>
               <mixed-citation id="d85e1568" publication-type="other">
Monica Hunter's (Wilson) study
in East London and Grahamstown in 1932, included in Reaction to Conquest: Effects of Contact with Europeans on the Pondo of
South Africa (London: Oxford University Press for the International Institute of African Languages and Cultures, 1936).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e1580" publication-type="other">
L.J.
Bank, 'City Dreams, Country Magic: Re-Reading Monica Hunters East London Fieldnotes' in A. Bank and L.J. Bank (eds), Inside
African Anthropology, 95-126.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e1592" publication-type="other">
E. Hellman, 'Rooiyard: A Sociological Survey of an Urban Native Slum Yard', Rhodes-Livingstone Papers 13 (1948).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e1599" publication-type="other">
R. Gordon, 'On Burning Ones Bridge: The Context of Gluckmans Zulu Fieldwork', History in Africa,
41, 2014, 177-8.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1609a1310">
            <label>121</label>
            <p>
               <mixed-citation id="d85e1616" publication-type="other">
R. Brown, 'Passages in the Life of a White Anthropologist: Max Gluckman in Northern Rhodesia, Journal of African History, 20,
4, 1979, 525-41.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1627a1310">
            <label>122</label>
            <p>
               <mixed-citation id="d85e1634" publication-type="other">
E.A.G. Robinson, 'The Economic Problem' in J. Merle Davis
(ed), Modern Industry and the African: An Enquiry into the Effect of the Copper Mines of Central Africa upon Native Society and
the Work of Christian Missions (New York: A.M. Kelley, 2nd edn 1986 [1933], 136-74.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d85e1646" publication-type="other">
H. Macmillan, "The Historiography of
Transition on the Zambian Copperbelt: Another View', Journal of Southern African Studies, 19, 4, 1993, 691-5</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1656a1310">
            <label>124</label>
            <p>
               <mixed-citation id="d85e1663" publication-type="other">
G. Wilson, Essay, Part I, 14-15.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1670a1310">
            <label>125</label>
            <p>
               <mixed-citation id="d85e1677" publication-type="other">
Ibid, Part II, 9.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1684a1310">
            <label>126</label>
            <p>
               <mixed-citation id="d85e1691" publication-type="other">
Hansen, Salaula, 35.</mixed-citation>
            </p>
         </fn>
         <fn id="d85e1698a1310">
            <label>127</label>
            <p>
               <mixed-citation id="d85e1705" publication-type="other">
Hansen, Salaula.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
