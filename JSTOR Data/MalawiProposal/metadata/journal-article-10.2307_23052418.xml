<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23052418</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Sex Differences in the Incidence of Peripheral Neuropathy Among Kenyans Initiating Antiretroviral Therapy</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sapna A.</given-names>
                  <surname>Mehta</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Aabid</given-names>
                  <surname>Ahmed</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Maura</given-names>
                  <surname>Laverty</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert S.</given-names>
                  <surname>Holzman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Fred</given-names>
                  <surname>Valentine</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sumathi</given-names>
                  <surname>Sivapalasingam</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">53</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23052360</issue-id>
         <fpage>490</fpage>
         <lpage>496</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/cid/cir432"
                   xlink:title="an external site"/>
         <abstract>
            <p>Background. Peripheral neuropathy (PN) is common among patients receiving antiretroviral therapy (ART) in resource-limited settings. We report the incidence of and risk factors for PN among human immunodeficiency virus (HIV)—infected Kenyan adults initiating ART. Methods. An inception cohort was formed of adults initiating ART. They were screened for PN at baseline and every 3 months for 1 year. We used the validated Brief Peripheral Neuropathy Screen (BPNS) that includes symptoms and signs (vibration perception and ankle reflexes) of PN. Results. Twenty-two (11%) of 199 patients had PN at baseline screening. One hundred fifty patients without evidence of PN at baseline were followed for a median of 366 days (interquartile range, 351—399). The incidence of PN was 11.9 per 100 person-years (95% confidence interval [CI], 6.9—19.1) and was higher in women than men (17.7 vs 1.9 per 100 person-years; rate ratio, 9.6; 95% CI, 1.27—72, P =.03). In stratified analyses, female sex remained statistically significant after adjustment for each of the following variables: age, CD4 cell count, body mass index, ART regimen, and tuberculosis treatment. Stratifying hemoglobin levels decreased the hazard ratio from 9.6 to 7.40 (P =.05), with higher levels corresponding to a lower risk of PN. Conclusions. HIV-infected Kenyan women were almost 10 times more likely than men to develop PN in the first year of ART. The risk decreased slightly at higher hemoglobin levels. Preventing or treating anemia in women before ART initiation and implementing BPNS during the first year of ART, the period of highest risk, could ameliorate the risk of PN.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d541e234a1310">
            <label>1</label>
            <mixed-citation id="d541e241" publication-type="other">
Boulle A, Orrel C, Kaplan R, et al. Substitutions due to antiretroviral
toxicity or contraindication in the first 3 years of antiretroviral therapy
in a large South African cohort. Antivir Ther 2007; 12:753-60.</mixed-citation>
         </ref>
         <ref id="d541e254a1310">
            <label>2</label>
            <mixed-citation id="d541e261" publication-type="other">
Forna F, Liechty CA, Solberg P, et al. Clinical toxicity of highly active
antiretroviral therapy in a home-based AIDS care program in rural
Uganda. J Acquir Immune Defic Syndr 2007; 44:456-62.</mixed-citation>
         </ref>
         <ref id="d541e274a1310">
            <label>3</label>
            <mixed-citation id="d541e281" publication-type="other">
Hawkins C, Achenbach C, Fryda W, Ngare D, Murphy R. Antiretroviral
durability and tolerability in HIV-infected adults living in urban
Kenya. J Acquir Immune Defic Syndr 2007; 45:304-10.</mixed-citation>
         </ref>
         <ref id="d541e294a1310">
            <label>4</label>
            <mixed-citation id="d541e301" publication-type="other">
Hoffmann CJ, Fielding KL, Charalambous S, et al. Antiretroviral
therapy using zidovudine, lamivudine, and efavirenz in South Africa:
tolerability and clinical events. AIDS 2008; 22:67-74.</mixed-citation>
         </ref>
         <ref id="d541e315a1310">
            <label>5</label>
            <mixed-citation id="d541e322" publication-type="other">
Millogo A, Lankoande D, Yameogo I, Yameogo AA, Sawadogo AB.
Polyneuropathies in patients treated with HAART in Bobo-Dioulasso
hospital, Burkina Faso [in French]. Bull SocPatholExot 2008; 101:11-3.</mixed-citation>
         </ref>
         <ref id="d541e335a1310">
            <label>6</label>
            <mixed-citation id="d541e342" publication-type="other">
van Oosterhout JJ, Bodasing N, Kumwenda JJ, et al. Evaluation of
antiretroviral therapy results in a resource-poor setting in Blantyre,
Malawi. Trop Med Int Health 2005; 10:464-70.</mixed-citation>
         </ref>
         <ref id="d541e355a1310">
            <label>7</label>
            <mixed-citation id="d541e362" publication-type="other">
Wester CW, Kim S, Bussmann H, et al. Initial response to highly active
antiretroviral therapy in HIV-lC-infected adults in a public sector
treatment program in Botswana. J Acquir Immune Defic Syndr 2005;
40:336-43.</mixed-citation>
         </ref>
         <ref id="d541e378a1310">
            <label>8</label>
            <mixed-citation id="d541e385" publication-type="other">
Ferrari S, Vento S, Monaco S, et al. Human immunodeficiency virus-
associated peripheral neuropathies. Mayo Clin Proc 2006; 81:213-9.</mixed-citation>
         </ref>
         <ref id="d541e395a1310">
            <label>9</label>
            <mixed-citation id="d541e402" publication-type="other">
Njoroge J, Reidy W, John-Stewart G, Attwa M, et al. Incidence of
peripheral neuropathy among patients receiving HAART regimens
containing stavudine vs. zidovudine in Kenya. In Abstracts of the Fifth
IAS Conference on HIV Pathogenesis, Treatment and Prevention. Cape
Town, South Africa: IAS, 2009.</mixed-citation>
         </ref>
         <ref id="d541e421a1310">
            <label>10</label>
            <mixed-citation id="d541e428" publication-type="other">
Sacktor N, Nakasujja N, Skolasky RL, et al. Benefits and risks of sta-
vudine therapy for HIV-associated neurologic complications in
Uganda. Neurology 2009; 72:165-70.</mixed-citation>
         </ref>
         <ref id="d541e442a1310">
            <label>11</label>
            <mixed-citation id="d541e449" publication-type="other">
van Griensven J, Zachariah R, Rasschaert F, Mugabo J, Att E, Reid T.
Stavudine- and nevirapine-related drug toxicity while on generic fixed-
dose antiretroviral treatment: incidence, timing and risk factors in
a three-year cohort in Kigali, Rwanda. Trans R Soc Trop Med Hyg
2010; 104:148-53.</mixed-citation>
         </ref>
         <ref id="d541e468a1310">
            <label>12</label>
            <mixed-citation id="d541e475" publication-type="other">
Cherry CL, Wesselingh SL, Lai L, McArthur JC. Evaluation of a clinical
screening tool for HIV-associated sensory neuropathies. Neurology
2005; 65:1778-81.</mixed-citation>
         </ref>
         <ref id="d541e488a1310">
            <label>13</label>
            <mixed-citation id="d541e495" publication-type="other">
Mehta SA, Ahmed A, Kariuki BW, et al. Implementation of a validated
peripheral neuropathy screening tool in patients receiving anti-
retroviral therapy in Mombasa, Kenya. Am J Trop Med Hyg 2010;
83:565-70.</mixed-citation>
         </ref>
         <ref id="d541e511a1310">
            <label>14</label>
            <mixed-citation id="d541e518" publication-type="other">
WHO case definitions of HIV for surveillance and revised clinical
staging and immunologic classification of HIV-related disease in adults
and children. Available at: http://www.who.int/hiv/pub/guidelines/
HIVstagingl50307.pdf. Accessed 25 April 2010.</mixed-citation>
         </ref>
         <ref id="d541e534a1310">
            <label>15</label>
            <mixed-citation id="d541e541" publication-type="other">
Peduzzi P, Guo Z, Marottoli RA, Gill TM, Araujo K, Allore HG.
Improved self-confidence was a mechanism of action in two geriatric
trials evaluating physical interventions. J Clin Epidemiol 2007; 60:94-102.</mixed-citation>
         </ref>
         <ref id="d541e554a1310">
            <label>16</label>
            <mixed-citation id="d541e561" publication-type="other">
Guarino P, Lamping DL, Elbourne D, Carpenter J, Peduzzi P. A brief
measure of perceived understanding of informed consent in a clinical
trial was validated. J Clin Epidemiol 2006; 59:608-14.</mixed-citation>
         </ref>
         <ref id="d541e575a1310">
            <label>17</label>
            <mixed-citation id="d541e582" publication-type="other">
Peduzzi P, Concato J, Feinstein AR, Holford TR. Importance of events
per independent variable in proportional hazards regression analysis.
II. Accuracy and precision of regression estimates. J Clin Epidemiol
1995; 48:1503-10.</mixed-citation>
         </ref>
         <ref id="d541e598a1310">
            <label>18</label>
            <mixed-citation id="d541e605" publication-type="other">
Nakamoto B, McMurtray A, Davis J, et al. Incident neuropathy in HIV-
infected patients on HAART. AIDS Res Hum Retroviruses 2010;
26:759-65.</mixed-citation>
         </ref>
         <ref id="d541e618a1310">
            <label>19</label>
            <mixed-citation id="d541e625" publication-type="other">
Ellis R, Rosario D, Clifford D, et al. Continued high prevalence and
adverse clinical impact of human immunodeficiency virus-associated
sensory neuropathy in the era of combination antiretroviral therapy:
the CHARTER Study. Arch Neurol 2010; 67:552-8.</mixed-citation>
         </ref>
         <ref id="d541e641a1310">
            <label>20</label>
            <mixed-citation id="d541e648" publication-type="other">
Simpson DM, Kitch D, Evans SR, et al. HIV neuropathy natural history
cohort study: assessment measures and risk factors. Neurology 2006;
66:1679-87.</mixed-citation>
         </ref>
         <ref id="d541e661a1310">
            <label>21</label>
            <mixed-citation id="d541e668" publication-type="other">
Cherry CL, Affandi JS, Imran D, et al. Age and height predict neu-
ropathy risk in patients with HIV prescribed stavudine. Neurology
2009; 73:315-20.</mixed-citation>
         </ref>
         <ref id="d541e681a1310">
            <label>22</label>
            <mixed-citation id="d541e688" publication-type="other">
Currier JS, Spino C, Grimes J, et al. Differences between women and
men in adverse events and CD4+ responses to nucleoside analogue
therapy for HIV infection. AIDS Clinical Trials Group 175 Team. J
Acquir Immune Defic Syndr 2000; 24:316-24.</mixed-citation>
         </ref>
         <ref id="d541e705a1310">
            <label>23</label>
            <mixed-citation id="d541e712" publication-type="other">
Moore RD, Fortgang I, Keruly J, Chaisson RE. Adverse events from
drug therapy for human immunodeficiency virus disease. Am J Med
1996; 101:34-40.</mixed-citation>
         </ref>
         <ref id="d541e725a1310">
            <label>24</label>
            <mixed-citation id="d541e732" publication-type="other">
Kempf MC, Pisu M, Dumcheva A, Westfall AO, Kilby JM, Saag MS.
Gender differences in discontinuation of antiretroviral treatment reg-
imens. J Acquir Immune Defic Syndr 2009; 52:336-41.</mixed-citation>
         </ref>
         <ref id="d541e745a1310">
            <label>25</label>
            <mixed-citation id="d541e752" publication-type="other">
Ofotokun I, Pomeroy C. Sex differences in adverse reactions to anti-
retroviral drugs. Top HIV Med 2003; 11:55-9.</mixed-citation>
         </ref>
         <ref id="d541e762a1310">
            <label>26</label>
            <mixed-citation id="d541e769" publication-type="other">
Antiretroviral therapy for HIV infection in adults and adolescents:
recommendations for a public health approach. Available at: http://
whqlibdoc.who.int/publications/2010/9789241599764_eng.pdf. Accessed
31 August 2010.</mixed-citation>
         </ref>
         <ref id="d541e785a1310">
            <label>27</label>
            <mixed-citation id="d541e792" publication-type="other">
Lichtenstein KA, Armon C, Baron A, Moorman AC, Wood KC,
Holmberg SD. Modification of the incidence of drug-associated sym-
metrical peripheral neuropathy by host and disease factors in the HIV
outpatient study cohort. Clin Infect Dis 2005; 40:148-57.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
