<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">epidinfe</journal-id>
         <journal-id journal-id-type="jstor">j100820</journal-id>
         <journal-title-group>
            <journal-title>Epidemiology and Infection</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">09502688</issn>
         <issn pub-type="epub">14694409</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3863689</article-id>
         <title-group>
            <article-title>Determinants of Drinking Water Quality in Rural Nicaragua</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>P.</given-names>
                  <surname>Sandiford</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>A. C.</given-names>
                  <surname>Gorter</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>G. Davey</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. P. C.</given-names>
                  <surname>Pauw</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>1989</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">102</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i294778</issue-id>
         <fpage>429</fpage>
         <lpage>438</lpage>
         <page-range>429-438</page-range>
         <permissions>
            <copyright-statement>Copyright 1989 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3863689"/>
         <abstract>
            <p>One hundred and fifty-three water samples from rural Nicaragua were examined for the presence of faecal coliforms during both wet and dry periods. A linear model was fitted by analysis of covariance with the logarithm of the faecal coliform count as the dependant variable. As expected, traditional water sources were grossly contaminated at all times whereas piped water sources were much cleaner. Hand-dug protected wells had significantly higher levels of faecal contamination than unprotected riverside wells and springs during the dry season. The possible reasons for this unexpected finding are discussed. A close association between rainfall and faecal contamination was demonstrated but the effect of rainfall depended on the type of water source. An association between water quality and the size of the community served by the source was also detected. The finding that stored water was usually more contaminated than fresh water samples is consistent with the results from other studies. Since it is unusual for water quality to be inversely correlated wth accessibility, this study site would be suitable for investigating the relative importance of water-borne versus water-washed transmission mechanisms in childhood diarrhoea.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1506e156a1310">
            <mixed-citation id="d1506e160" publication-type="book">
APHA (1981). Standard Methods for the Examination of Water and Waste-water. Washington:
American Public Health Association, American Water Works Association and Water
Pollution Control Federation.<source>Standard Methods for the Examination of Water and Waste-water</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e179a1310">
            <mixed-citation id="d1506e183" publication-type="journal">
Barrell, R. A. E. &amp; Rowland, M. G. M. (1979). The relationship between rainfall and well
water pollution in a West African (Gambian) village. Journal of Hygiene83, 143-150.<person-group>
                  <string-name>
                     <surname>Barrell</surname>
                  </string-name>
               </person-group>
               <fpage>143</fpage>
               <volume>83</volume>
               <source>Journal of Hygiene</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e215a1310">
            <mixed-citation id="d1506e219" publication-type="journal">
Blum, D. &amp; Feachem, R. G. (1983). Measuring the impact of water supply and sanitation
investments on diarrhoeal diseases: Problems of methodology. International Journal of
Epidemiology12, 357-365.<person-group>
                  <string-name>
                     <surname>Blum</surname>
                  </string-name>
               </person-group>
               <fpage>357</fpage>
               <volume>12</volume>
               <source>International Journal of Epidemiology</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e254a1310">
            <mixed-citation id="d1506e258" publication-type="journal">
Blum, D., Huttly, S. R. A., Okoro, J. I., Akujobi, C, Kirkwood, B. R. &amp; Feachem, R. G.
(1987). The bacteriological quality of traditional water sources in northeastern Imo State,
Nigeria. Epidemiology and Infection99, 429-437.<person-group>
                  <string-name>
                     <surname>Blum</surname>
                  </string-name>
               </person-group>
               <fpage>429</fpage>
               <volume>99</volume>
               <source>Epidemiology and Infection</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e294a1310">
            <mixed-citation id="d1506e298" publication-type="journal">
Bradley, D. J. &amp; Emurwon, P. (1968). Predicting the epidemiological affects of changing
water sources. Part I. A quantitative approach. East African Medical Journal45, 285-291.<person-group>
                  <string-name>
                     <surname>Bradley</surname>
                  </string-name>
               </person-group>
               <fpage>285</fpage>
               <volume>45</volume>
               <source>East African Medical Journal</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e330a1310">
            <mixed-citation id="d1506e334" publication-type="journal">
Briscoe, J. (1978). Role of water supply in improving health in poor countries (with special
reference to Bangladesh). American Journal of Clinical Nutrition31, 2100-2113.<person-group>
                  <string-name>
                     <surname>Briscoe</surname>
                  </string-name>
               </person-group>
               <fpage>2100</fpage>
               <volume>31</volume>
               <source>American Journal of Clinical Nutrition</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e366a1310">
            <mixed-citation id="d1506e370" publication-type="book">
Cairncross S. &amp; Feachem, R. G. (1983) Environmental Health Engineering in the Tropics: An
Introductory Text, pp. 4-9. Chichester: Wiley &amp; Sons.<person-group>
                  <string-name>
                     <surname>Cairncross</surname>
                  </string-name>
               </person-group>
               <fpage>4</fpage>
               <source>Environmental Health Engineering in the Tropics: An Introductory Text</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e399a1310">
            <mixed-citation id="d1506e403" publication-type="journal">
Einot, I. &amp; Gabriel, K. R. (1975). A study of the powers of several methods of multiple
comparisons. Journal of the American Statistical Association70, 574.<object-id pub-id-type="doi">10.2307/2285935</object-id>
               <fpage>574</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1506e419a1310">
            <mixed-citation id="d1506e423" publication-type="journal">
Esrey, S. A., Feachem, R. G. &amp; Hughes, J. M. (1985). Interventions for the control of
diarrhoeal diseases among young children: improving water supplies and excreta disposal
facilities. Bulletin of the World Health Organization63, 757-772.<person-group>
                  <string-name>
                     <surname>Esrey</surname>
                  </string-name>
               </person-group>
               <fpage>757</fpage>
               <volume>63</volume>
               <source>Bulletin of the World Health Organization</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e458a1310">
            <mixed-citation id="d1506e462" publication-type="journal">
Esrey, S. A. &amp; Habicht, J. (1986). Epidemiologic evidence for health benefits from improved
water and sanitation in developing countries. Epidemiologic Reviews8, 117-128.<person-group>
                  <string-name>
                     <surname>Esrey</surname>
                  </string-name>
               </person-group>
               <fpage>117</fpage>
               <volume>8</volume>
               <source>Epidemiologic Reviews</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e495a1310">
            <mixed-citation id="d1506e499" publication-type="book">
Feachem, R. G., Burns, E., Cairncross, A. M., Cronin, A., Cross, P., Curtis, D., Khalid
Khan, M., Lamb, D &amp; Southall, H. (1978). Water, Health and Development: An
Interdisciplinary Evaluation. London: Trimed.<person-group>
                  <string-name>
                     <surname>Feachem</surname>
                  </string-name>
               </person-group>
               <source>Water, Health and Development: An Interdisciplinary Evaluation</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e528a1310">
            <mixed-citation id="d1506e532" publication-type="journal">
Freij, L., Sterky, G., Wadstrom, T. &amp; Wall, S. (1978). Child Health and diarrhoeal disease in
relation to supply and use of water in African Communities. Progress in Water Technology11,
49-55.<person-group>
                  <string-name>
                     <surname>Freij</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>11</volume>
               <source>Progress in Water Technology</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e567a1310">
            <mixed-citation id="d1506e571" publication-type="journal">
Herbert, J. R. (1984). Water quality and water quantity and wasting in South India. Tropical
and Geographic Medicine36, 375-381.<person-group>
                  <string-name>
                     <surname>Herbert</surname>
                  </string-name>
               </person-group>
               <fpage>375</fpage>
               <volume>36</volume>
               <source>Tropical and Geographic Medicine</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e603a1310">
            <mixed-citation id="d1506e607" publication-type="journal">
Isley, R. B. (1978). A community organization approach to clean water and waste disposal in
Cameroonian villages. Progress in Water Technology11, 109-116.<person-group>
                  <string-name>
                     <surname>Isley</surname>
                  </string-name>
               </person-group>
               <fpage>109</fpage>
               <volume>11</volume>
               <source>Progress in Water Technology</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e639a1310">
            <mixed-citation id="d1506e643" publication-type="book">
Lehmusluoto, P. (1987). Survey on Contamination of Water Sources and Household Waters as an
Integrated Part of Impact Management, pp. 37-39. University of Tampere, Finland.<person-group>
                  <string-name>
                     <surname>Lehmusluoto</surname>
                  </string-name>
               </person-group>
               <fpage>37</fpage>
               <source>Survey on Contamination of Water Sources and Household Waters as an Integrated Part of Impact Management</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e672a1310">
            <mixed-citation id="d1506e676" publication-type="journal">
Moore, H. A., de la Cruz E. &amp; Vargas-Mendez, 0. (1966). Diarrhoeal disease studies in Costa
Rica. IV. The influence of sanitation upon the prevalence of intestinal infection and
diarrhoeal disease. American Journal of Public Health56, 276-286.<person-group>
                  <string-name>
                     <surname>Moore</surname>
                  </string-name>
               </person-group>
               <fpage>276</fpage>
               <volume>56</volume>
               <source>American Journal of Public Health</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e712a1310">
            <mixed-citation id="d1506e716" publication-type="journal">
Muhammed, S. I. &amp; Morrison, S. M. (1975). Water quality in Kiambu District, Kenya. East
African Medical Journal52, 269-276.<person-group>
                  <string-name>
                     <surname>Muhammed</surname>
                  </string-name>
               </person-group>
               <fpage>269</fpage>
               <volume>52</volume>
               <source>East African Medical Journal</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e748a1310">
            <mixed-citation id="d1506e752" publication-type="journal">
Schliessman, J. (1959). Diarrhoeal disease and environment. Bulletin of the World Health
Organization21, 381-386.<person-group>
                  <string-name>
                     <surname>Schliessman</surname>
                  </string-name>
               </person-group>
               <fpage>381</fpage>
               <volume>21</volume>
               <source>Bulletin of the World Health Organization</source>
               <year>1959</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e784a1310">
            <mixed-citation id="d1506e788" publication-type="journal">
Schneider, R. E., Shiffman, M. A. &amp; Faigenblum, J. M. (1978). The potential effect of water
on gastrointestinal infections prevalent in developing countries. American Journal of Clinical
Nutrition31, 2089-2099.<person-group>
                  <string-name>
                     <surname>Schneider</surname>
                  </string-name>
               </person-group>
               <fpage>2089</fpage>
               <volume>31</volume>
               <source>American Journal of Clinical Nutrition</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e823a1310">
            <mixed-citation id="d1506e827" publication-type="journal">
Shiffman, M. A., Schneider, R. E., Faigenblum, J. M., Helms, R. &amp; Turner, A. (1978).
Field studies on water sanitation and health education in relation to health status in Central
America. Progress in Water Technology11, 143-150.<person-group>
                  <string-name>
                     <surname>Shiffman</surname>
                  </string-name>
               </person-group>
               <fpage>143</fpage>
               <volume>11</volume>
               <source>Progress in Water Technology</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e862a1310">
            <mixed-citation id="d1506e866" publication-type="journal">
Tomkins, A. M., Drasar, B. S., Bradley, A. K. &amp; Williamson, W. A. (1978). Water supply
and nutritional status in rural Northern Nigeria. Transactions of the Royal Society of Tropical
Medicine and Hygiene72, 239-243.<person-group>
                  <string-name>
                     <surname>Tomkins</surname>
                  </string-name>
               </person-group>
               <fpage>239</fpage>
               <volume>72</volume>
               <source>Transactions of the Royal Society of Tropical Medicine and Hygiene</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e901a1310">
            <mixed-citation id="d1506e905" publication-type="book">
Torun, B. (1982). Environmental and educational interventions against diarrhea in Guatemala.
In Diarrhea and malnutrition: interactions management and interventions (ed. L. C. Chen and
N. S. Scrimshaw), pp. 235-266. New York: Plenum Press.<person-group>
                  <string-name>
                     <surname>Torun</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Environmental and educational interventions against diarrhea in Guatemala</comment>
               <fpage>235</fpage>
               <source>Diarrhea and malnutrition: interactions management and interventions</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e941a1310">
            <mixed-citation id="d1506e945" publication-type="journal">
Voelker, R. A. &amp; Heukelekian, H. (1960). Seasonal coliform variations in well waters.
American Journal of Public Health50, 1873-1881.<person-group>
                  <string-name>
                     <surname>Voelker</surname>
                  </string-name>
               </person-group>
               <fpage>1873</fpage>
               <volume>50</volume>
               <source>American Journal of Public Health</source>
               <year>1960</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e977a1310">
            <mixed-citation id="d1506e981" publication-type="journal">
Walsh, J. A. &amp; Warren, K. S. (1979). Selective primary health care: An interim strategy for
disease control in developing countries. New England Journal of Medicine301, 967-974.<person-group>
                  <string-name>
                     <surname>Walsh</surname>
                  </string-name>
               </person-group>
               <fpage>967</fpage>
               <volume>301</volume>
               <source>New England Journal of Medicine</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e1013a1310">
            <mixed-citation id="d1506e1017" publication-type="journal">
Wright, R. C. (1982). A comparison of the levels of faecal indicator bacteria in water and
human faeces in a rural area of a tropical developing country (Sierra Leone). Journal of
Hygiene89, 69-78.<person-group>
                  <string-name>
                     <surname>Wright</surname>
                  </string-name>
               </person-group>
               <fpage>69</fpage>
               <volume>89</volume>
               <source>Journal of Hygiene</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d1506e1052a1310">
            <mixed-citation id="d1506e1056" publication-type="journal">
Young, B. &amp; Briscoe, J. (1987). A case-control study ofthe effect of environmental sanitation
on diarrhoea morbidity in Malawi. Journal of Epidemiology and Community Health42,
33-88.<person-group>
                  <string-name>
                     <surname>Young</surname>
                  </string-name>
               </person-group>
               <fpage>33</fpage>
               <volume>42</volume>
               <source>Journal of Epidemiology and Community Health</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
