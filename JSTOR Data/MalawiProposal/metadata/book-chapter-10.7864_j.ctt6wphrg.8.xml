<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wphrg</book-id>
      <subj-group>
         <subject content-type="call-number">DS740.5.A34C45 2008</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">China</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">China</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">China</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
            <subj-group>
               <subject content-type="lcsh">China</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Economics</subject>
         <subject>Business</subject>
      </subj-group>
      <book-title-group>
         <book-title>China into Africa</book-title>
         <subtitle>Trade, Aid, and Influence</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>ROTBERG</surname>
               <given-names>ROBERT I.</given-names>
            </name>
            <role>Editor</role>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>10</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="epub">9780815701750</isbn>
      <isbn content-type="epub">0815701756</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press and World Peace Foundation</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2008</copyright-year>
         <copyright-holder>WORLD PEACE FOUNDATION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wphrg"/>
      <abstract abstract-type="short">
         <p>Africa has long attracted China. We can date their first certain involvement from the fourteenth century, but East African city-states may have been trading with southern China even earlier. In the mid-twentieth century, Maoist China funded and educated sub-Saharan African anticolonial liberation movements and leaders, and the PRC then assisted new sub-Saharan nations. Africa and China are now immersed in their third and most transformative era of heavy engagement, one that promises to do more for economic growth and poverty alleviation than anything attempted by Western colonialism or international aid programs. Robert Rotberg and his Chinese, African, and other colleagues discuss this important trend and specify its likely implications.</p>
         <p>Among the specific topics tackled here are China's interest in African oil; military and security relations; the influx and goals of Chinese aid to sub-Saharan Africa; human rights issues; and China's overall strategy in the region. China's insatiable demand for energy and raw materials responds to sub-Saharan Africa's relatively abundant supplies of unprocessed metals, diamonds, and gold, while offering a growing market for Africa's agriculture and light manufactures. As this book illustrates, this evolving symbiosis could be the making of Africa, the poorest and most troubled continent, while it further powers China's expansive economic machine.</p>
         <p>Contributors include Deborah Brautigam (American University), Harry Broadman (World Bank), Stephen Brown (University of Ottawa), Martyn J. Davies (Stellenbosch University), Joshua Eisenman (UCLA), Chin-Hao Huang (Stockholm International Peace Research Institute), Paul Hubbard (Australian Department of the Treasury),Wenran Jiang (University of Alberta), Darren Kew (University of Massachusetts- Boston), Henry Lee (Harvard University), Li Anshan (Peking University), Ndubisi Obiorah (Centre for Law and Social Action, Nigeria), Stephanie Rupp (National University of Singapore), Dan Shalmon (Georgetown University), David Shinn (GeorgeWashington University), Chandra Lekha Sriram (University of East London), and Yusuf Atang Tanko (University of Massachusetts-Boston)</p>
      </abstract>
      <counts>
         <page-count count="339"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rotberg</surname>
                           <given-names>Robert I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>China’s Quest for Resources, Opportunities, and Influence in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROTBERG</surname>
                           <given-names>ROBERT I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>China and Africa desperately need each other. China cannot easily grow without Africa. Nor can sub-Saharan Africa (a collection of forty-eight disparate countries) subsist, and now prosper, without China. Africa and China, in Auden’s terms, have at last met, and their recently forged and continuously reinvigorated, mutually reinforced, interactive relationship is already tight and will for decades grow ever stronger, more thoroughly intertwined.¹ Both benefit significantly from this remarkably symbiotic relationship.</p>
                     <p>China hardly wants to colonize, but it does have immense mercantilist ambitions. It ravenously seeks raw materials—petroleum, timber, ferrochrome, cobalt, platinum, copper, diamonds, and so on. Whatever primary</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>China’s New Policy toward Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ANSHAN</surname>
                           <given-names>LI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>As a dynamic part of China’s grand foreign policy, elements of China’s African policy have remained constant while others have changed. In 1992, Segal concluded that “there may be grounds for believing that as China grows strong, it will grow somewhat more important for Africans. But in the Chinese perspective, it seems that while Africa will attract attention from the writers of official policy statements, the continent will remain the least important area for Chinese foreign policy, whether of an expanding or a withdrawing kind.”¹</p>
                     <p>What is happening today obviously challenges Segal’s prophecy. In recent years, China’s rapid economic growth</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>China’s Emerging Strategic Partnerships in Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>JIANG</surname>
                           <given-names>WENRAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>50</fpage>
                  <abstract>
                     <p>Chinese President Hu Jintao wrapped up his eight-country, twelve-day African tour in February 2007 in the midst of controversy regarding China’s role on the continent. Government officials from the countries that received China’s leader expressed gratitude for their guest’s generous offers of aid, cancellations of debt, and promises of trade and investment. Critics, however, charge that China’s actions in Africa are no less than neocolonialism, as China seizes a new sphere of influence, grabs oil and other resources, props up repressive regimes, and leaves individual African countries on the losing end. Beijing has refuted such characterizations by identifying itself with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Africa and China:</title>
                     <subtitle>Engaging Postcolonial Interdependencies</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>RUPP</surname>
                           <given-names>STEPHANIE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>The alignment of Africa’s natural resource endowments with China’s core economic interests has placed Africa at the center of emerging geopolitical tensions. In the first decade of the twenty-first century, China is likely to succeed in securing economic and political ties to African nations that rival if not displace relations that Euro-American nations have dominated over 150 years of colonial rule and neocolonial influence.</p>
                     <p>This chapter analyzes the nature of relations between China and Africa, unpacking the frequent characterization of China’s recent activities in Africa as “colonial” or “neocolonial” and asking if there might be an alternative framework for making</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Chinese-African Trade and Investment:</title>
                     <subtitle>The Vanguard of South-South Commerce in the Twenty-First Century</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BROADMAN</surname>
                           <given-names>HARRY G.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>The recent explosion of commerce between China and the countries of sub-Saharan Africa is a striking hallmark of the new trend in South-South trade and investment. Indeed, this acceleration in exchange among developing countries is one of the most significant features of the current global economy. For decades, world trade has been dominated by commerce both among developed countries—the North—and between the North and the developing countries of the South. Today, South-South trade accounts for about 11 percent of global trade and is growing at about 10 percent a year; 43 percent of the South’s trade is with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Searching for Oil:</title>
                     <subtitle>China’s Oil Strategies in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LEE</surname>
                           <given-names>HENRY</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>SHALMON</surname>
                           <given-names>DAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>109</fpage>
                  <abstract>
                     <p>Pressured by skyrocketing demand, Chinese oil companies have branched out across the globe seeking new oil supplies to feed the country’s economic growth. By 2006, China had made oil investments in almost every part of the world, including Africa. These initiatives have not been without controversy. From the commercial perspective, Western companies complain that China’s ability to link its oil investments to government-to-government financial assistance gives its companies an unfair advantage. From the political perspective, Western nongovernmental organizations have accused China of using its investments to support some of the more abusive, corrupt, and violent governments in the world. The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Special Economic Zones:</title>
                     <subtitle>China’s Developmental Model Comes to Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>DAVIES</surname>
                           <given-names>MARTYN J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>137</fpage>
                  <abstract>
                     <p>A new developmental model is in the process of being rolled out in key African countries—Special Economic Zones (SEZs). They provide liberalized investment environments focused on strategic industries to attract foreign companies. The model of dedicated geographical zones where investing companies enjoy preferential economic policies is by no means unique. Numerous African governments have established or are establishing such zones in their countries in an attempt to attract foreign direct investment (FDI), especially in labor-intensive manufacturing industries. Kenya, Egypt, and Mauritius are the most proactive on the continent in establishing such zones.</p>
                     <p>What makes this new developmental model unique</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Military and Security Relations:</title>
                     <subtitle>China, Africa, and the Rest of the World</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SHINN</surname>
                           <given-names>DAVID H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>Chinese military and security relations with Africa have progressed from support for independence and revolutionary movements in the 1960s and 1970s to a more pragmatic relationship in the 1990s and the first decade of the twenty-first century. China’s national security interests focus over-whelmingly on its periphery: South Asia, Southeast Asia, Central Asia, West Asia (including the Middle East), Russia, Japan, Mongolia, and the Koreas. China is also much concerned about the ability of the United States and Europe to project military and economic power into Asia. Africa and Latin America, however, are not a critical part of China’s security policy.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>China’s Foreign Aid in Africa:</title>
                     <subtitle>What Do We Know?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BRAUTIGAM</surname>
                           <given-names>DEBORAH</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>197</fpage>
                  <abstract>
                     <p>Several years ago, China’s foreign aid program started to emerge from the shadows where it had been operating for close to five decades. Rumors of a huge new aid program ran through Western papers and magazines. By late 2006, concern about China’s role as a donor gained a place on the agendas of the major players in the global aid regime. Bilateral and multilateral agencies in Washington, Stockholm, Canada, London, and Oslo organized a number of meetings on China’s aid, where participants expressed worries about debt sustainability, the lack of conditions favoring governance and human rights, and environmental impacts.</p>
                     <p>Other</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Chinese Concessional Loans</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HUBBARD</surname>
                           <given-names>PAUL</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>217</fpage>
                  <abstract>
                     <p>China’s recent economic aid to Africa has the more traditional international aid donors worried. Outsiders fear that the Chinese government is giving away billions of dollars on the continent to buy political influence, thus supporting authoritarian regimes while undermining the governance and anticorruption efforts of traditional donors. But outsiders, and probably the Chinese themselves, know very little about how the Chinese aid system actually works.</p>
                     <p>China’s aid program is designed to promote China’s own foreign policy goals. In a recent study of China’s growing soft power, Kurlantzick describes aid as an integral part of the arsenal.¹ Not surprisingly, these foreign</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.14</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>China’s Political Outreach to Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>EISENMAN</surname>
                           <given-names>JOSHUA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>230</fpage>
                  <abstract>
                     <p>Over the last three decades the People’s Republic of China (PRC) has implemented an international outreach campaign designed to build lasting ties to African political parties and organizations. In the pursuit of improved official state-to-state relations and in support of Chinese domestic economic development, the Communist Party of China’s (CPC) International Department (CPC-ID) and its affiliated organizations have used their deep pockets and admirable diplomatic adroitness to engage African political parties.¹ Parliamentary delegations from the National People’s Congress (NPC) have also been a part of these efforts, albeit to a lesser degree.</p>
                     <p>CPC-ID and NPC delegations are valuable teams that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.15</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>China’s Role in Human Rights Abuses in Africa:</title>
                     <subtitle>Clarifying Issues of Culpability</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BROWN</surname>
                           <given-names>STEPHEN</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>SRIRAM</surname>
                           <given-names>CHANDRA LEKHA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>250</fpage>
                  <abstract>
                     <p>As other chapters in this volume demonstrate, China has extensive and rapidly increasing economic involvement in African countries, including trade, investment, and aid. How should one assess that involvement?</p>
                     <p>The authors of this chapter do not share many analysts’ and commentators’ alarm concerning China’s economic relations with Africa. Too much of the literature tends to enumerate individual Chinese investments in or agreements with African countries without situating them within a broader comparative context, assuming, rather than demonstrating, that they are significant. Why is China’s investment in an oil refinery in Nigeria a concern per se? To measure the impact of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.16</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>“Peaceful Rise” and Human Rights:</title>
                     <subtitle>China’s Expanding Relations with Nigeria</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>OBIORAH</surname>
                           <given-names>NDUBISI</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>KEW</surname>
                           <given-names>DARREN</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>TANKO</surname>
                           <given-names>YUSUF</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>272</fpage>
                  <abstract>
                     <p>The rapidly evolving Chinese-African relationship presents opportunities and challenges for Africa. Some African countries are benefiting from higher commodity prices, arising from China’s immense demand for natural resources for its manufacturing industries. Chinese trade, investment, and infrastructural aid are fundamentally reshaping Africa’s economies to the benefit of local consumers and businesses in some countries but also to the primary benefit of ruling elites in others, especially those with extraction-based economies.</p>
                     <p>Nigeria, as Africa’s sociopolitical giant, should have a leading role in shaping China’s involvement on the continent. In addition, Nigeria’s relative wealth and complexity should give it greater leverage than</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.17</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>China’s Renewed Partnership with Africa:</title>
                     <subtitle>Implications for the United States</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HUANG</surname>
                           <given-names>CHIN-HAO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>296</fpage>
                  <abstract>
                     <p>China’s expansive political, economic, and military engagement in Africa reflects an increasingly dynamic and accommodating approach toward the continent.¹ Launched at the November 2006 Forum on China-Africa Cooperation (FOCAC) summit, China’s renewed partnership with Africa marks a historic watershed in Chinese-African relations. Chinese activity in Africa promises future gains that benefit Africa in significant, constructive ways; at the same time, challenges are fast emerging as Beijing seeks to translate its vision of a strategic partnership with Africa into a sustainable reality.</p>
                     <p>This chapter illuminates the motivations and decisionmaking processes driving China’s evolving foreign and security policy in Africa, provides an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.18</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>313</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>319</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wphrg.20</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>340</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
