<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt24h3fz</book-id>
      <subj-group>
         <subject content-type="lcsh">Law</subject>
         <subj-group>
            <subject content-type="lcsh">Vanuatu</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Customary law</subject>
         <subj-group>
            <subject content-type="lcsh">Vanuatu</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Vanuatu</subject>
         <subj-group>
            <subject content-type="lcsh">Social life and customs</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Vanuatu</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Law</subject>
      </subj-group>
      <book-title-group>
         <book-title>A Bird That Flies With Two Wings</book-title>
         <subtitle>Kastom and state justice systems in Vanuatu</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Forsyth</surname>
               <given-names>Miranda</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>09</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="ppub">9781921536786</isbn>
      <isbn content-type="epub">9781921536793</isbn>
      <publisher>
         <publisher-name>ANU E Press</publisher-name>
         <publisher-loc>Canberra</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>ANU E Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt24h3fz"/>
      <abstract abstract-type="short">
         <p>This book investigates the problems and possibilities of plural legal orders through an in-depth study of the relationship between the state and customary justice systems in Vanuatu. It argues that there is a need to move away from the current state-centric approach to law reform in the South Pacific region, and instead include all state and non-state legal orders in development strategies and dialogue. The book also presents a typology of models of engagement between state and non-state legal systems, and describes a process for analysing which of these models would be most advantageous for any country in the South Pacific region, and beyond.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.4</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Braithwaite</surname>
                           <given-names>John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
                  <abstract abstract-type="extract">
                     <p>Miranda Forsyth researched and wrote this book while teaching at the University of the South Pacific Law School in Vanuatu for the past six years. During this time, she acquired depth and breadth of knowledge of law and <italic>kastom</italic> in the South Pacific beyond Vanuatu from students who attended her classes from island societies all over the South Pacific. She also enjoys a broad network of indigenous and non-indigenous friends in Vanuatu who are deep thinkers and repositories of great learning about Melanesian <italic>kastom</italic>. All this combines with the sharpness of Miranda Forsyth’s intellect to equip her to write an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.5</book-part-id>
                  <title-group>
                     <title>Prologue</title>
                  </title-group>
                  <fpage>xv</fpage>
                  <abstract abstract-type="extract">
                     <p>In 2001, I spent a year as a volunteer prosecutor in the Public Prosecutor’s Office in Vanuatu, a small country in the South Pacific spread over approximately 83 inhabited and uninhabited islands. On one occasion, relatively soon after I had arrived, I went with some members of the Vanuatu Police Force to serve summonses in remote villages on one of the outer islands. We took a four-wheel drive and set out, following barely marked tracks, through the dense bush. From time to time, we arrived at villages consisting of leaf houses and surrounded by gardens used for subsistence agriculture, with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.6</book-part-id>
                  <title-group>
                     <label>1.</label>
                     <title>‘Igat fulap rod blong hem’</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Vanuatu is situated geographically in Melanesia, an area of the world that has seen a recent succession of violent conflicts, deteriorating law and order and failing or fragile states.¹ The extent to which Vanuatu is at risk of going down the same road is a matter of some controversy, but it certainly shares some common Melanesian characteristics: a weak state, considerable poverty,² ethnic tensions, high youth unemployment in urban areas, poor governance, an exploding population and significant socioeconomic change.³ The focus of this chapter concerns another shared Melanesian characteristic: diversity.⁴ Using this theme, the current cultural, social and political setting</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.7</book-part-id>
                  <title-group>
                     <label>2.</label>
                     <title>The possibilities and limitations of legal pluralism</title>
                  </title-group>
                  <fpage>29</fpage>
                  <abstract abstract-type="extract">
                     <p>The existence of normative legal systems operating independently or semi-independently from the State, such as the <italic>kastom</italic> system in Vanuatu, is an empirical reality in almost every decolonised country in the world.¹ Despite their prevalent nature,² however, and the growing official and academic recognition of their existence, there is currently no widely accepted theoretical position for analysing the relationships between such legal systems, or between such systems and the State. This chapter discusses three possible theoretical approaches—legal positivism, legal anthropology and legal pluralism—and the possibilities and limitations of each for answering the questions posed in this study. It</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.8</book-part-id>
                  <title-group>
                     <label>3.</label>
                     <title>Tradition and transformation in leadership structures and conflict-management mechanisms</title>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter surveys the ethnography of Vanuatu from first contact with Europeans to the present day, drawing out the main themes in relation to conflict management and indigenous leadership structures. The motifs elicited will serve as an introductory framework within which the current debates about the <italic>kastom</italic> system and the role of chiefs can be more readily understood and appreciated. In addition, the extreme versatility of ni-Vanuatu in adapting to the enormous changes brought about by increasing influence and control by the outside world becomes apparent.</p>
                     <p>The discussion differentiates three periods: the period between when the first ethnographers arrived in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.9</book-part-id>
                  <title-group>
                     <label>4.</label>
                     <title>
                        <italic>Mat, kava, faol, pig, buluk, woman</italic>:</title>
                     <subtitle>the operation of the <italic>kastom</italic> system in Vanuatu today</subtitle>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter explores the current operation of the <italic>kastom</italic> system through the analysis of a number of initial hypotheses in light of later research.¹ Due to space constraints, it is not possible to provide a full ethnographic account of the whole <italic>kastom</italic> system throughout Vanuatu. Consequently, this chapter focuses on its most salient aspects, especially those that affect its relationship with the state system. It also draws out some of its main principles and fundamental concepts (what Chiba would call the <italic>legal postulates</italic> of the system),² including those of respect, reciprocity, shame, balance and the importance of the community. The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.10</book-part-id>
                  <title-group>
                     <label>5.</label>
                     <title>The relationship between the state and <italic>kastom</italic> systems</title>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter examines the current relationship between the <italic>kastom</italic> system and various state institutions—namely, the courts, police, prisons, Public Prosecutor’s Office, Office of the Public Solicitor and the Malvatumauri. The last is included although it is not part of the criminal justice system because it is the state body that, at least nominally, represents the <italic>kastom</italic> system at a state level. We shall see that adopting this broad ambit generates a far more nuanced account of the relationship between the two systems than has previously been produced by legal scholars who have focused solely on how substantive customary law</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.11</book-part-id>
                  <title-group>
                     <label>6.</label>
                     <title>The problems of the existing relationship between the state and <italic>kastom</italic> systems</title>
                  </title-group>
                  <fpage>175</fpage>
                  <abstract abstract-type="extract">
                     <p>The previous chapter characterised the relationship between the state and <italic>kastom</italic> systems as fluid, informal and largely involving the two operating in parallel with each other rather than meaningfully interacting. This chapter examines the problems flowing from such a relationship, elucidating additional imperatives for reform to those identified in Chapter 5. The main focus of the chapter is a case study, <italic>Public Prosecutor vs Agnes Kalo and Peter Obed</italic>,¹ which concerns a conflict in Vila dealt with by both the state and the <italic>kastom</italic> systems. This case illustrates many of the problems within, and most importantly, <italic>between</italic>, the two systems.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.12</book-part-id>
                  <title-group>
                     <label>7.</label>
                     <title>A typology of relationships between state and non-state justice systems</title>
                  </title-group>
                  <fpage>201</fpage>
                  <abstract abstract-type="extract">
                     <p>We have seen in the past three chapters that there are significant problems with the current relationship between the <italic>kastom</italic> and state systems in Vanuatu. Therefore, the final questions posed by this study concern an inquiry into other types of relationship that might enable the two systems to work together better, and how such a new relationship could be chosen and implemented. As discussed in Chapter 2, however, to date there has not been a serious attempt to create a framework for a comparative study of relationships between state and non-state justice systems. Further, although in much of the relevant</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.13</book-part-id>
                  <title-group>
                     <label>8.</label>
                     <title>A new method of legal pluralism</title>
                  </title-group>
                  <fpage>249</fpage>
                  <abstract abstract-type="extract">
                     <p>It has been pointed out by many scholars that in today’s Western world the State is becoming increasingly ‘hollowed-out’, meaning that more and more state functions are being devolved to non-state actors.¹ In Vanuatu and many other developing countries, however, the opposite of this has occurred: the State has engaged in a process of <italic>taking over</italic> more and more of the functions traditionally performed by community leaders in stateless communities. This has particularly been the case in countries emerging from periods of weak or scattered government (such as those emerging from civil war) to a stronger state. In these countries,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24h3fz.14</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>267</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
