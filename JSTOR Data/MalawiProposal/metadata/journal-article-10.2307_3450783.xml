<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">ecology</journal-id>
         <journal-id journal-id-type="jstor">j100138</journal-id>
         <journal-title-group>
            <journal-title>Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Ecology Society of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00129658</issn>
         <issn pub-type="epub">19399170</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3450783</article-id>
         <title-group>
            <article-title>Compensatory Larval Responses Shift Trade-offs Associated with Predator-Induced Hatching Plasticity</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>James R.</given-names>
                  <surname>Vonesh</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Benjamin M.</given-names>
                  <surname>Bolker</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">86</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id>i368046</issue-id>
         <fpage>1580</fpage>
         <lpage>1591</lpage>
         <page-range>1580-1591</page-range>
         <permissions>
            <copyright-statement>Copyright 2005 Ecological Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3450783"/>
         <abstract>
            <p>Many species with complex life histories can respond to risk by adaptively altering the timing of key life history switch points, including hatching. It is generally thought that such hatching plasticity involves a trade-off between embryonic and hatchling predation risk, e.g., hatching early to escape egg predation comes at the cost of increased vulnerability to hatchling predators. However, most empirical work has focused on simply detecting predator-induced hatching responses or on the short-term consequences of hatching plasticity. Short-term studies may not allow sufficient time for hatchlings to exhibit compensatory responses, which may extend to subsequent life stages and could alter the nature of the trade-offs associated with hatching plasticity. In this study, we examine the consequences of predator-induced hatching plasticity through the larval stage to metamorphosis in the East African reed frog, Hyperolius spinigularis. To do this we conducted an experiment in which we manipulated initial larval size and density (mimicking the effects of egg predators) and the presence of aquatic predators. We expected that predator-induced hatchlings (because they are less developed and smaller) would experience higher per capita predation rates and a longer larval period and thus would exhibit lower survival to metamorphosis in the presence of aquatic predators than larger, more developed, later hatched larvae. Surprisingly, we found that predator-induced hatchlings survived better, not worse, than hatchlings from undisturbed clutches. These results motivated us to develop a model parameterized from additional experiments to explore whether a combination of mechanisms, compensatory growth, and density- and size-specific predation, could give rise to this pattern. Predicted survival probabilities from the model with compensatory growth were consistent with those from the field experiment: early hatched larvae grew more rapidly through vulnerable size classes than later hatched larvae, resulting in higher survival at metamorphosis. Thus, in this system, there does not appear to be a trade-off in vulnerability between egg and larval predators. Instead, our results suggest that the cost that balances the survival benefit of hatching early to evade egg predators arises later in the life history, as a result of smaller size at metamorphosis.</p>
         </abstract>
         <kwd-group>
            <kwd>Compensatory Growth</kwd>
            <kwd>Density-Mediated Indirect Interaction (DMII)</kwd>
            <kwd>Functional Response</kwd>
            <kwd>Hatching Plasticity</kwd>
            <kwd>Hyperolius Spinigularis</kwd>
            <kwd>Multiple Predators</kwd>
            <kwd>Phenotypic Plasticity</kwd>
            <kwd>Size-Selective Predation</kwd>
            <kwd>Trait-Mediated Indirect Interaction (TMII)</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d568e233a1310">
            <mixed-citation id="d568e237" publication-type="book">
Alford, R. A. 1999. Ecology: resource use, competition, and
predation. Pages 240-278 in R. W. McDiarmid and R. Al-
tig, editors. Tadpoles: the biology of anuran larvae. Uni-
versity of Chicago Press, Chicago, Illinois, USA.<person-group>
                  <string-name>
                     <surname>Alford</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ecology: resource use, competition, and predation</comment>
               <fpage>240</fpage>
               <source>Tadpoles: the biology of anuran larvae</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d568e275a1310">
            <mixed-citation id="d568e279" publication-type="journal">
Alford, R. A., and R. N. Harris. 1988. Effects of larval growth
history on an anuran metamorphosis. American Naturalist
133:91-106.<object-id pub-id-type="jstor">10.2307/2461801</object-id>
               <fpage>91</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e298a1310">
            <mixed-citation id="d568e302" publication-type="journal">
Alford, R. A., and G. D. Jackson. 1993. Do cephalopods and
larvae of other taxa grow asymptotically? American Nat-
uralist 141:717-728.<object-id pub-id-type="jstor">10.2307/2462828</object-id>
               <fpage>717</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e321a1310">
            <mixed-citation id="d568e325" publication-type="journal">
Altwegg, R., and H. Reyer. 2003. Patterns of natural selection
on size at metamorphosis in water frogs. Evolution 57:872-
882.<object-id pub-id-type="jstor">10.2307/3094623</object-id>
               <fpage>872</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e345a1310">
            <mixed-citation id="d568e349" publication-type="journal">
Anholt, B. R., and E. E. Werner. 1998. Predictable changes
in predation mortality as a consequence of changes in food
availability and predation risk. Evolutionary Ecology 12:
729-738.<person-group>
                  <string-name>
                     <surname>Anholt</surname>
                  </string-name>
               </person-group>
               <fpage>729</fpage>
               <volume>12</volume>
               <source>Evolutionary Ecology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d568e387a1310">
            <mixed-citation id="d568e391" publication-type="journal">
Berven, K. A. 1990. Factors affecting population fluctuations
in larval and adult stages of the wood frog (Rana sylvatica).
Ecology 71:1599-1608.<object-id pub-id-type="doi">10.2307/1938295</object-id>
               <fpage>1599</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e410a1310">
            <mixed-citation id="d568e414" publication-type="journal">
Blaustein, L. 1997. Non-consumptive effects of larval Sal-
amandra on crustacean prey; can eggs detect predators?
Oecologia 110:212-217.<person-group>
                  <string-name>
                     <surname>Blaustein</surname>
                  </string-name>
               </person-group>
               <fpage>212</fpage>
               <volume>110</volume>
               <source>Oecologia</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d568e449a1310">
            <mixed-citation id="d568e453" publication-type="journal">
Chivers, D. P., J. M. Kiesecker, A. Marco, J. DeVito, M. T.
Anderson, and A. R. Blaustien. 2001. Predator-induced life
history changes in amphibians: egg predation induces
hatching. Oikos 92:135-142.<object-id pub-id-type="jstor">10.2307/3547689</object-id>
               <fpage>135</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e476a1310">
            <mixed-citation id="d568e480" publication-type="book">
Caswell, H. 2001. Matrix population models. Second edition.
Sinauer, Sunderland, Massachusetts, USA.<person-group>
                  <string-name>
                     <surname>Caswell</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Second edition</comment>
               <source>Matrix population models</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d568e509a1310">
            <mixed-citation id="d568e513" publication-type="journal">
DeWitt, T. J. 1998. Costs and limits of phenotypic plasticity:
tests with predator induced morphology and life history in
a freshwater snail. Journal of Evolutionary Biology 11:
465-480.<person-group>
                  <string-name>
                     <surname>DeWitt</surname>
                  </string-name>
               </person-group>
               <fpage>465</fpage>
               <volume>11</volume>
               <source>Journal of Evolutionary Biology</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d568e552a1310">
            <mixed-citation id="d568e556" publication-type="journal">
Downie, J. R., and A. Weir. 1997. Developmental arrest in
Leptodactylusfuscus tadpoles (Anura: Leptodactylidae). 3.
Effect of length of arrest period on growth potential. Her-
petological Journal 7(3):85-92.<person-group>
                  <string-name>
                     <surname>Downie</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>85</fpage>
               <volume>7</volume>
               <source>Herpetological Journal</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d568e597a1310">
            <mixed-citation id="d568e601" publication-type="journal">
Elliot, J. M. 2003. A comparative study of the functional
response of four species of carnivorous stoneflies. Fresh-
water Biology 48:191-202.<person-group>
                  <string-name>
                     <surname>Elliot</surname>
                  </string-name>
               </person-group>
               <fpage>191</fpage>
               <volume>48</volume>
               <source>Freshwater Biology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d568e636a1310">
            <mixed-citation id="d568e640" publication-type="journal">
Gosner, K. L. 1960. A simplified table for staging anuran
embryos and larvae with notes on identification. Herpe-
tologica 16:203-210.<person-group>
                  <string-name>
                     <surname>Gosner</surname>
                  </string-name>
               </person-group>
               <fpage>203</fpage>
               <volume>16</volume>
               <source>Herpetologica</source>
               <year>1960</year>
            </mixed-citation>
         </ref>
         <ref id="d568e675a1310">
            <mixed-citation id="d568e679" publication-type="book">
Hamilton, A. C., and R. Bensted-Smith, editors. 1989. Forest
conservation in the East Usambara Mountains Tanzania.
IUCN, Gland, Switzerland, and Cambridge, UK.<person-group>
                  <string-name>
                     <surname>Hamilton</surname>
                  </string-name>
               </person-group>
               <source>Forest conservation in the East Usambara Mountains Tanzania</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d568e708a1310">
            <mixed-citation id="d568e712" publication-type="journal">
Hayes, M. P. 1983. A technique for partitioning hatching and
mortality estimates in lead-breeding frogs. Herpetological
Review 14:115-116.<person-group>
                  <string-name>
                     <surname>Hayes</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>14</volume>
               <source>Herpetological Review</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d568e747a1310">
            <mixed-citation id="d568e751" publication-type="journal">
Holling, C. S. 1959. Some characteristics of simple types of
predation and parasitism. Canadian Entomologist 91:385-
398.<person-group>
                  <string-name>
                     <surname>Holling</surname>
                  </string-name>
               </person-group>
               <fpage>385</fpage>
               <volume>91</volume>
               <source>Canadian Entomologist</source>
               <year>1959</year>
            </mixed-citation>
         </ref>
         <ref id="d568e787a1310">
            <mixed-citation id="d568e791" publication-type="journal">
Ihaka, R., and R. Gentleman. 1996. R: a language for data
analysis and graphics. Journal of Computational and
Graphical Statistics 5:299-314.<object-id pub-id-type="doi">10.2307/1390807</object-id>
               <fpage>299</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e810a1310">
            <mixed-citation id="d568e814" publication-type="journal">
Jones, M., A. Laurila, N. Peuhkuri, J. Piironen, and T. Seppi.
2003. Timing an ontogenetic niche shift: response of
emerging salmon alevins to chemical cues from predators
and competitors. Oikos 122:155-163.<object-id pub-id-type="jstor">10.2307/3547868</object-id>
               <fpage>155</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e837a1310">
            <mixed-citation id="d568e841" publication-type="book">
Juliano, S. A. 1993. Nonlinear curve fitting: predation and
functional response curves. Pages 159-182 in S. M. Schei-
ner and J. Gurevitch, editors. Design and analysis of eco-
logical experiments. Chapman and Hall, New York, New
York, USA.<person-group>
                  <string-name>
                     <surname>Juliano</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Nonlinear curve fitting: predation and functional response curves</comment>
               <fpage>159</fpage>
               <source>Design and analysis of ecological experiments</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d568e882a1310">
            <mixed-citation id="d568e886" publication-type="journal">
Laurila, A., P. A. Crochet, and J. Merila. 2001. Predation-
induced effects on hatching morphology in the common
frog (Rana temporaria). Canadian Journal of Zoology 79:
926-930.<person-group>
                  <string-name>
                     <surname>Laurila</surname>
                  </string-name>
               </person-group>
               <fpage>926</fpage>
               <volume>79</volume>
               <source>Canadian Journal of Zoology</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d568e924a1310">
            <mixed-citation id="d568e928" publication-type="journal">
Laurila, A., S. Pakkasmaa, P. Crochet, and J. Merila. 2002.
Predator-induced plasticity in early life history and mor-
phology in two anuran amphibians. Oecologia 133:524-
530.<person-group>
                  <string-name>
                     <surname>Laurila</surname>
                  </string-name>
               </person-group>
               <fpage>524</fpage>
               <volume>133</volume>
               <source>Oecologia</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d568e966a1310">
            <mixed-citation id="d568e970" publication-type="book">
Li, D. Q. 2002. Hatching responses of subsocial spitting spi-
ders to predation risk. Proceedings of the Royal Society of
London, Series B 269:2155-2161.<person-group>
                  <string-name>
                     <surname>Li</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Hatching responses of subsocial spitting spiders to predation risk</comment>
               <fpage>2155</fpage>
               <volume>269</volume>
               <source>Proceedings of the Royal Society of London</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1009a1310">
            <mixed-citation id="d568e1013" publication-type="journal">
Lima, S. L., and L. M. Dill. 1990. Behavioral decisions made
under the risk of predation: a review and prospectus. Ca-
nadian Journal of Zoology 68:619-640.<person-group>
                  <string-name>
                     <surname>Lima</surname>
                  </string-name>
               </person-group>
               <fpage>619</fpage>
               <volume>68</volume>
               <source>Canadian Journal of Zoology</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1048a1310">
            <mixed-citation id="d568e1052" publication-type="book">
Lovett, J. C., and S. K. Wasser. 1993. Biogeography and
ecology of the rain forests of eastern Africa. Cambridge
University Press, Cambridge, UK.<person-group>
                  <string-name>
                     <surname>Lovett</surname>
                  </string-name>
               </person-group>
               <source>Biogeography and ecology of the rain forests of eastern Africa</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1081a1310">
            <mixed-citation id="d568e1085" publication-type="journal">
Luttbeg, B., L. Rowe, and M. Mangel. 2003. Prey state and
experimental design affect the relative size of trait- and
density-mediated indirect effects. Ecology 84:1140-1150.<object-id pub-id-type="jstor">10.2307/3107922</object-id>
               <fpage>1140</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1104a1310">
            <mixed-citation id="d568e1108" publication-type="book">
Lyons, L. 1991. A practical guide to data analysis for physical
science students. Cambridge University Press, Cambridge,
UK.<person-group>
                  <string-name>
                     <surname>Lyons</surname>
                  </string-name>
               </person-group>
               <source>A practical guide to data analysis for physical science students</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1137a1310">
            <mixed-citation id="d568e1141" publication-type="journal">
Martin, K. L. M. 1999. Ready and waiting: delayed hatching
and extended incubation of anamniotic vertebrate terrestrial
eggs. American Zoologist 39:279-288.<person-group>
                  <string-name>
                     <surname>Martin</surname>
                  </string-name>
               </person-group>
               <fpage>279</fpage>
               <volume>39</volume>
               <source>American Zoologist</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1176a1310">
            <mixed-citation id="d568e1180" publication-type="journal">
Matsuda, H., P. A. Abrams, and M. Hori. 1993. The effect
of adaptive antipredator behavior on exploitative compe-
tition and mutualism between predators. Oikos 68:549-
559.<object-id pub-id-type="doi">10.2307/3544924</object-id>
               <fpage>549</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1204a1310">
            <mixed-citation id="d568e1208" publication-type="journal">
Matsuda, H., M. Hori, and P. A. Abrams. 1994. Effects of
predator-specific defense on predator persistence and com-
munity complexity. Evolutionary Ecology 8:628-638.<person-group>
                  <string-name>
                     <surname>Matsuda</surname>
                  </string-name>
               </person-group>
               <fpage>628</fpage>
               <volume>8</volume>
               <source>Evolutionary Ecology</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1243a1310">
            <mixed-citation id="d568e1247" publication-type="journal">
Matsuda, H., M. Hori, and P. A. Abrams. 1996. Effects of
predator-specific defense on biodiversity and community
complexity in two-trophic-level communities. Evolution-
ary Ecology 10:13-28.<person-group>
                  <string-name>
                     <surname>Matsuda</surname>
                  </string-name>
               </person-group>
               <fpage>13</fpage>
               <volume>10</volume>
               <source>Evolutionary Ecology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1285a1310">
            <mixed-citation id="d568e1289" publication-type="journal">
Moore, R. D., B. Newton, and A. Sih. 1996. Delayed hatching
as a response of streamside salamander eggs to chemical
cues from predatory sunfish. Oikos 77:33-335.<object-id pub-id-type="doi">10.2307/3546073</object-id>
               <fpage>33</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1308a1310">
            <mixed-citation id="d568e1312" publication-type="journal">
Metcalfe, N. B., and P. Monaghan. 2001. Compensation for
a bad start: grow now, pay later? Trends in Ecology and
Evolution 16:254-260.<person-group>
                  <string-name>
                     <surname>Metcalfe</surname>
                  </string-name>
               </person-group>
               <fpage>254</fpage>
               <volume>16</volume>
               <source>Trends in Ecology and Evolution</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1347a1310">
            <mixed-citation id="d568e1351" publication-type="journal">
Myers, N., R. A. Mittermeir, C. G. Mittermeir, G. A. B. da
Fonseca, and J. Kent. 2000. Biodiversity hotspots for con-
servation priorities. Nature 403:853-858.<person-group>
                  <string-name>
                     <surname>Myers</surname>
                  </string-name>
               </person-group>
               <fpage>853</fpage>
               <volume>403</volume>
               <source>Nature</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1386a1310">
            <mixed-citation id="d568e1390" publication-type="journal">
Osenberg, C. W., and G. G. Mittelbach. 1989. Effects of body
size on the predator-prey interaction between pumpkinseed
sunfish and gastropods. Ecological Monographs 59:405-
432.<object-id pub-id-type="doi">10.2307/1943074</object-id>
               <fpage>405</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1414a1310">
            <mixed-citation id="d568e1418" publication-type="book">
Pinheiro, J. C., and D. Bates. 2000. Mixed-effects models in
S and S-plus. Springer-Verlag, New York, New York, USA.<person-group>
                  <string-name>
                     <surname>Pinheiro</surname>
                  </string-name>
               </person-group>
               <source>Mixed-effects models in S and S-plus</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1443a1310">
            <mixed-citation id="d568e1447" publication-type="journal">
Rhsdnen, K., A. Laurila, and J. Merila. 2002. Carry-over
effects of embryonic acid conditions on development and
growth of Rana temporaria tadpoles. Freshwater Biology
47:19-30.<person-group>
                  <string-name>
                     <surname>Rhsdnen</surname>
                  </string-name>
               </person-group>
               <fpage>19</fpage>
               <volume>47</volume>
               <source>Freshwater Biology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1485a1310">
            <mixed-citation id="d568e1489" publication-type="book">
R Development Core Team. 2004. R: a language and envi-
ronment for statistical computing. R Foundation for Sta-
tistical Computing, Vienna, Austria. (http://www.
R-project.org)<person-group>
                  <string-name>
                     <surname>R Development Core Team</surname>
                  </string-name>
               </person-group>
               <source>R Foundation for Statistical Computing, Vienna, Austria.</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1521a1310">
            <mixed-citation id="d568e1525" publication-type="journal">
Rogers, D. J. 1972. Random search and insect population
models. Journal of Animal Ecology 41:369-383.<object-id pub-id-type="doi">10.2307/3474</object-id>
               <fpage>369</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1541a1310">
            <mixed-citation id="d568e1545" publication-type="journal">
Schalk, G., M. R. Forbes, and P. J. Weatherhead. 2002. De-
velopmental plasticity and growth rates of green frog em-
bryos and tadpoles in relation to a leech (Macrobdella de-
cora) predator. Copeia 2002:445-449.<object-id pub-id-type="jstor">10.2307/1448060</object-id>
               <fpage>445</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1568a1310">
            <mixed-citation id="d568e1572" publication-type="book">
Schiøtz, A. 1999. Treefrogs of Africa. Chimara, Mecken-
heim, Germany.<person-group>
                  <string-name>
                     <surname>Schiøtz</surname>
                  </string-name>
               </person-group>
               <source>Treefrogs of Africa</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1598a1310">
            <mixed-citation id="d568e1602" publication-type="journal">
Sih, A., and R. D. Moore. 1993. Delayed hatching in sala-
mander eggs in response to enhanced larval predation.
American Naturalist 142:947-960.<object-id pub-id-type="jstor">10.2307/2462693</object-id>
               <fpage>947</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1621a1310">
            <mixed-citation id="d568e1625" publication-type="journal">
Skelly, D. K. 1992. Field evidence for a cost of behavioral
antipredator responses in a larval amphibian. Ecology 73:
704-708.<object-id pub-id-type="doi">10.2307/1940779</object-id>
               <fpage>704</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1644a1310">
            <mixed-citation id="d568e1648" publication-type="journal">
Smith, D. C. 1987. Adult recruitment in chorus frogs: effects
of size and date at metamorphosis. Ecology 68:344-350.<object-id pub-id-type="doi">10.2307/1939265</object-id>
               <fpage>344</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1664a1310">
            <mixed-citation id="d568e1668" publication-type="book">
Tollrian, R., and C. D. Harvell. 1999. The ecology and evo-
lution of inducible defenses. Princeton University Press,
Princeton, New Jersey, USA.<person-group>
                  <string-name>
                     <surname>Tollrian</surname>
                  </string-name>
               </person-group>
               <source>The ecology and evolution of inducible defenses</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1697a1310">
            <mixed-citation id="d568e1701" publication-type="book">
Venables, W. N., and B. D. Ripley. 2002. Modern applied
statistics with S. Fourth edition. Springer-Verlag, New
York, New York, USA.<person-group>
                  <string-name>
                     <surname>Venables</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Fourth edition</comment>
               <source>Modern applied statistics with S</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1733a1310">
            <mixed-citation id="d568e1737" publication-type="journal">
Vonesh, J. R. 2000. Dipteran predation on the eggs of four
Hyperolius frog species in western Uganda. Copeia 2000:
560-566.<object-id pub-id-type="jstor">10.2307/1448207</object-id>
               <fpage>560</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1757a1310">
            <mixed-citation id="d568e1761" publication-type="other">
Vonesh, J. R. 2003. Density- and trait-mediated effects of
predators across life history stages. Dissertation. University
of Florida, Gainesville, Florida, USA.</mixed-citation>
         </ref>
         <ref id="d568e1774a1310">
            <mixed-citation id="d568e1778" publication-type="journal">
Vonesh, J. R., and C. W. Osenberg. 2003. Multi-predator
effects across life-history stages: non-additivity of egg-and
larval-stage predation in an African treefrog. Ecology Let-
ters 6:503-508.<person-group>
                  <string-name>
                     <surname>Vonesh</surname>
                  </string-name>
               </person-group>
               <fpage>503</fpage>
               <volume>6</volume>
               <source>Ecology Letters</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1816a1310">
            <mixed-citation id="d568e1820" publication-type="journal">
Warkentin, K. M. 1995. Adaptive plasticity in hatching age:
a response to predation risk trade-offs. Proceedings of the
National Academy of Sciences (USA) 92:3507-3510.<person-group>
                  <string-name>
                     <surname>Warkentin</surname>
                  </string-name>
               </person-group>
               <fpage>3507</fpage>
               <volume>92</volume>
               <source>Proceedings of the National Academy of Sciences (USA)</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1855a1310">
            <mixed-citation id="d568e1859" publication-type="journal">
Warkentin, K. M. 1999a. Effects of hatching age on devel-
opment and hatchling morphology in the red-eyed treefrog,
Agalychnis callidryas. Biological Journal of the Linnaen
Society 68:443-470.<person-group>
                  <string-name>
                     <surname>Warkentin</surname>
                  </string-name>
               </person-group>
               <fpage>443</fpage>
               <volume>68</volume>
               <source>Biological Journal of the Linnaen Society</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1897a1310">
            <mixed-citation id="d568e1901" publication-type="journal">
Warkentin, K. M. 1999b. The development of behavioral de-
fenses: a mechanistic analysis of vulnerability in red-eyed
tree frog hatchlings. Behavioral Ecology 10:251-262.<person-group>
                  <string-name>
                     <surname>Warkentin</surname>
                  </string-name>
               </person-group>
               <fpage>251</fpage>
               <volume>10</volume>
               <source>Behavioral Ecology</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1936a1310">
            <mixed-citation id="d568e1940" publication-type="journal">
Warkentin, K. M. 2000. Wasp predation and wasp-induced
hatching of red-eyed treefrog eggs. Animal Behaviour 60:
503-510.<person-group>
                  <string-name>
                     <surname>Warkentin</surname>
                  </string-name>
               </person-group>
               <fpage>503</fpage>
               <volume>60</volume>
               <source>Animal Behaviour</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d568e1976a1310">
            <mixed-citation id="d568e1980" publication-type="journal">
Warkentin, K. M., C. R. Currie, and S. A. Rehner. 2001. Egg-
killing fungus induces early hatching of red-eyed treefrog
eggs. Ecology 82:2860-2869.<object-id pub-id-type="doi">10.2307/2679966</object-id>
               <fpage>2860</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e1999a1310">
            <mixed-citation id="d568e2003" publication-type="journal">
Wedekind, C. 2002. Induced hatching to avoid infectious egg
disease in whitefish. Current Biology 12:69-71.<person-group>
                  <string-name>
                     <surname>Wedekind</surname>
                  </string-name>
               </person-group>
               <fpage>69</fpage>
               <volume>12</volume>
               <source>Current Biology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d568e2035a1310">
            <mixed-citation id="d568e2039" publication-type="journal">
Werner, E. E. 1986. Amphibian metamorphosis: growth rate,
predation risk, and the optimal size at transformation.
American Naturalist 128:319-341.<object-id pub-id-type="jstor">10.2307/2461428</object-id>
               <fpage>319</fpage>
            </mixed-citation>
         </ref>
         <ref id="d568e2058a1310">
            <mixed-citation id="d568e2062" publication-type="journal">
Werner, E. E., and J. E Gilliam. 1984. The ontogenetic niche
and species interactions in size-structured populations. An-
nual Review of Ecology and Systematics 15:393-425.<object-id pub-id-type="jstor">10.2307/2096954</object-id>
               <fpage>393</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
