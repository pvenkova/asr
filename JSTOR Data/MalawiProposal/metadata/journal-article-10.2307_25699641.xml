<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">reviintepoliecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101400</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Review of International Political Economy</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Routledge</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09692290</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14664526</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25699641</article-id>
         <title-group>
            <article-title>The politics of stock market development</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Pablo M.</given-names>
                  <surname>Pinto</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Stephen</given-names>
                  <surname>Weymouth</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter</given-names>
                  <surname>Gourevitch</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">17</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25699631</issue-id>
         <fpage>378</fpage>
         <lpage>409</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 Taylor &amp; Francis</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1080/09692290903310424"
                   xlink:title="an external site"/>
         <abstract>
            <p>This article locates the political determinants of stock market development in the distributional cleavages among voters and interest groups. Our argument questions the prevailing explanation about the role of partisanship in the literature, where it is usually assumed that left governments frighten investors. To the extent that financial development is translated into higher levels of investment that increases labor demand, workers and the parties representing them will adopt policies and regulations that favor the capitalization of financial markets. We explore the empirical content of our hypothesis against several competing explanations: the legal origins school, which argues common law proxies stronger investor protections than civil law; the electoral law school, which argues proportional representation provides weaker protections than do majoritarian ones; the institutional economics view, which argues that checks on policy-making discretion such as veto gates protect the property rights of investors and encourage investment. We test the implications of the different arguments on the level of stock market capitalization in a panel of 85 countries over the period 1975–2004. We find preliminary evidence in favor of the partisanship hypothesis: left-leaning governments are more likely to be associated with higher stock market capitalization than their counterparts to the right and center of the political spectrum. These results are consistent with recent theories emphasizing an emerging coalition of workers and owners against managers in favor of greater transparency and shareholder protection.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1198e213a1310">
            <label>1</label>
            <mixed-citation id="d1198e220" publication-type="other">
Levine (2005: 867).</mixed-citation>
         </ref>
         <ref id="d1198e227a1310">
            <label>2</label>
            <mixed-citation id="d1198e234" publication-type="other">
Rajan and Zingales (2003a,</mixed-citation>
            <mixed-citation id="d1198e240" publication-type="other">
2003b)</mixed-citation>
            <mixed-citation id="d1198e246" publication-type="other">
Gourevitch
(2003)</mixed-citation>
            <mixed-citation id="d1198e256" publication-type="other">
Gourevitch and Shinn (2005)</mixed-citation>
            <mixed-citation id="d1198e262" publication-type="other">
Perotti and von Thadden (2006).</mixed-citation>
         </ref>
         <ref id="d1198e269a1310">
            <label>3</label>
            <mixed-citation id="d1198e278" publication-type="other">
Rajan and Zingales (2003a)</mixed-citation>
         </ref>
         <ref id="d1198e285a1310">
            <label>4</label>
            <mixed-citation id="d1198e292" publication-type="other">
Biais and Perotti (2002)</mixed-citation>
         </ref>
         <ref id="d1198e300a1310">
            <label>5</label>
            <mixed-citation id="d1198e307" publication-type="other">
Perotti and Volpin (2007: 9).</mixed-citation>
            <mixed-citation id="d1198e313" publication-type="other">
Rajan (2009)</mixed-citation>
            <mixed-citation id="d1198e319" publication-type="other">
Rajan and Zingales (2003a,</mixed-citation>
            <mixed-citation id="d1198e326" publication-type="other">
2003b).</mixed-citation>
         </ref>
         <ref id="d1198e333a1310">
            <label>6</label>
            <mixed-citation id="d1198e340" publication-type="other">
Rogowski, 1987</mixed-citation>
            <mixed-citation id="d1198e346" publication-type="other">
Hiscox, 2001</mixed-citation>
         </ref>
         <ref id="d1198e353a1310">
            <label>7</label>
            <mixed-citation id="d1198e360" publication-type="other">
Cox and McCub-
bins (2001).</mixed-citation>
         </ref>
         <ref id="d1198e370a1310">
            <label>8</label>
            <mixed-citation id="d1198e377" publication-type="other">
Pinto (2004)</mixed-citation>
            <mixed-citation id="d1198e383" publication-type="other">
Pinto and Pinto (2007,</mixed-citation>
            <mixed-citation id="d1198e389" publication-type="other">
2008a)</mixed-citation>
            <mixed-citation id="d1198e396" publication-type="other">
Pinto and Pinto, 2007,</mixed-citation>
            <mixed-citation id="d1198e402" publication-type="other">
2008a).</mixed-citation>
            <mixed-citation id="d1198e408" publication-type="other">
Pinto, 2004</mixed-citation>
            <mixed-citation id="d1198e414" publication-type="other">
Pinto and Pinto, 2008a,</mixed-citation>
            <mixed-citation id="d1198e420" publication-type="other">
2008b).</mixed-citation>
         </ref>
         <ref id="d1198e427a1310">
            <label>9</label>
            <mixed-citation id="d1198e434" publication-type="other">
Gourevitch and Shinn, 2005</mixed-citation>
            <mixed-citation id="d1198e440" publication-type="other">
Cioffi and Hoepner, 2006</mixed-citation>
         </ref>
         <ref id="d1198e447a1310">
            <label>10</label>
            <mixed-citation id="d1198e454" publication-type="other">
Alfaro (2004)</mixed-citation>
         </ref>
         <ref id="d1198e462a1310">
            <label>18</label>
            <mixed-citation id="d1198e469" publication-type="other">
Henisz, 2002</mixed-citation>
         </ref>
         <ref id="d1198e476a1310">
            <label>21</label>
            <mixed-citation id="d1198e483" publication-type="other">
Alfaro (2004)</mixed-citation>
            <mixed-citation id="d1198e489" publication-type="other">
Quinn (1997)</mixed-citation>
            <mixed-citation id="d1198e495" publication-type="other">
Quinn &amp; Inclan (1997)</mixed-citation>
            <mixed-citation id="d1198e502" publication-type="other">
Alesina
and Tabellini (1989)</mixed-citation>
            <mixed-citation id="d1198e511" publication-type="other">
Alesina et al (1994).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1198e527a1310">
            <mixed-citation id="d1198e531" publication-type="other">
Alesina, A. (1987) 'Macroeconomic Policy in a Two-party System as a Repeated
Game', Quarterly Journal of Economics, 102(3): 651-78.</mixed-citation>
         </ref>
         <ref id="d1198e541a1310">
            <mixed-citation id="d1198e545" publication-type="other">
Alesina, A. (1988) 'Macroeconomics and Polities', National Bureau of Economic Re-
search Macroeconomics Annual, 3:13-36.</mixed-citation>
         </ref>
         <ref id="d1198e555a1310">
            <mixed-citation id="d1198e559" publication-type="other">
Alesina, A., and Tabellini, G. (1989) 'External Debt, Capital Fight and Political Risk',
Journal of International Economics, 27 (November): 199-220.</mixed-citation>
         </ref>
         <ref id="d1198e569a1310">
            <mixed-citation id="d1198e573" publication-type="other">
Alesina, A., Grilli, V. and Milesi-Ferretti, G. M. (1994) The Political Economy of Capital
Controls, Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e584a1310">
            <mixed-citation id="d1198e588" publication-type="other">
Alfaro, L. (2004) 'Capital Controls: A Political Economy Approach', Review of In-
ternational Economics, 12(4): 571-90.</mixed-citation>
         </ref>
         <ref id="d1198e598a1310">
            <mixed-citation id="d1198e602" publication-type="other">
Alvarez, R. M., Garrett, G. and Lange, P. (1991) 'Government Partisanship, La-
bor Organization, and Macroeconomic Performance', American Political Science
Review, 85(2): 539-56.</mixed-citation>
         </ref>
         <ref id="d1198e615a1310">
            <mixed-citation id="d1198e619" publication-type="other">
Beck, T., Clarke, G., Groff, A., Keefer, P. and Walsh, P. (2001) 'New Tools in Com-
parative Political Economy: The Database of Political Institutions. World Bank
Economic'. World Bank Economic Review, 15(1): 165-76.</mixed-citation>
         </ref>
         <ref id="d1198e632a1310">
            <mixed-citation id="d1198e636" publication-type="other">
Benmelech, E. and Moskowitz, T. J. (2006) The Political Economy of Financial
Regulation: Evidence from U.S. State Usury Laws in the 18th and 19th Century',
AFA 2007 Chicago Meetings Paper.</mixed-citation>
         </ref>
         <ref id="d1198e649a1310">
            <mixed-citation id="d1198e653" publication-type="other">
Biais, B., and Perotti, E. (2002) 'Machiavellian Privatization', American Economic
Review, 92(1): 240-58.</mixed-citation>
         </ref>
         <ref id="d1198e663a1310">
            <mixed-citation id="d1198e667" publication-type="other">
Boix, C. (1997) 'Political Parties and the Supply Side of the Economy: The Provision
of Physical and Human Capital in Advanced Economies', American Journal of
Political Science, 41(3): 814-45.</mixed-citation>
         </ref>
         <ref id="d1198e681a1310">
            <mixed-citation id="d1198e685" publication-type="other">
Boix, C. (1998) Political Parties, Growth and Equality: Conservative and Social Demo-
cratic Economic Strategies in the World Economy, New York: Cambridge Univer-
sity Press.</mixed-citation>
         </ref>
         <ref id="d1198e698a1310">
            <mixed-citation id="d1198e702" publication-type="other">
Chinn, M. D. and Ito, H. (2006) 'What Matters for Financial Development? Capital
Controls, Institutions, and Interactions', Journal of Development Economics, 81(1):
163-92.</mixed-citation>
         </ref>
         <ref id="d1198e715a1310">
            <mixed-citation id="d1198e719" publication-type="other">
Cioffi, J. W. (forthcoming) Public Law and Private Power: Corporate Governance Reform
in the United States and Germany in the Age of Finance Capitalism. ,Ithaca, NY:
Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d1198e732a1310">
            <mixed-citation id="d1198e736" publication-type="other">
Cioffi, J. W, and Hoepner, M. (2006) The Political Paradox of Finance Capitalism:
Interests, Preferences, and Centre-Left Party Politics in Corporate Governance
Reform', Politics and Society,, 34(4): 463-502.</mixed-citation>
         </ref>
         <ref id="d1198e749a1310">
            <mixed-citation id="d1198e753" publication-type="other">
Claessens, S., Klingebiel, D. and Schmukler, S. L. (2006) 'Stock Market Development
and Internationalization: Do Economic Fundamentals Spur both Similarly?',
Journal of Empirical Finance, 13(3): 316-50.</mixed-citation>
         </ref>
         <ref id="d1198e766a1310">
            <mixed-citation id="d1198e770" publication-type="other">
Cox, G. W. (1997) Making Votes Count. Strategic Coordination in the World's Electoral
Systems, Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e781a1310">
            <mixed-citation id="d1198e785" publication-type="other">
Cox, G. W. and McCubbins, M. D. (2001) The Institutional Determinants of Eco-
nomic Policy Outcomes', in S. Haggard and M. D. McCubbins (eds)Presidents,
Parliaments, and Policy, Cambridge: Cambridge University Press, pp. 21-63.</mixed-citation>
         </ref>
         <ref id="d1198e798a1310">
            <mixed-citation id="d1198e802" publication-type="other">
Cox, G. W. and Mathew D. McCubbins, M.D. (2010, forthcoming) 'Managing
Plenary Time: The U.S. Congress in a Comparative Perspective' (with Gary
W. Cox), in L. Frances and E. Shickler (eds) Oxford Handbook of the American
Congress, Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1198e818a1310">
            <mixed-citation id="d1198e822" publication-type="other">
Culpepper, P. (2007) 'Eppure, non si muove: Legal Change, Institutional Sta-
bility and Italian Corporate Governance', West European Politics, 30(4): 784-
802.</mixed-citation>
         </ref>
         <ref id="d1198e835a1310">
            <mixed-citation id="d1198e839" publication-type="other">
Dutt, P. and Mitra, D. (2005) 'Political Ideology and Endogenous Trade Policy: An
Empirical Investigation', Review of Economics and Statistics, 87(1): 59-72.</mixed-citation>
         </ref>
         <ref id="d1198e849a1310">
            <mixed-citation id="d1198e853" publication-type="other">
Evans, P. (1978) Dependent Development: The Alliance of Multinational, State and Local
Capital in Brazil, Princeton: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1198e863a1310">
            <mixed-citation id="d1198e867" publication-type="other">
Fisman, R. (2001) 'Estimating the Value of Political Connections', The American
Economic Review, 91(4): 1095-102.</mixed-citation>
         </ref>
         <ref id="d1198e878a1310">
            <mixed-citation id="d1198e882" publication-type="other">
Franzese, R. J. Jr. (2002) Macroeconomic Policies of Developed Democracies, Cambridge
Studies in Comparative Politics, Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e892a1310">
            <mixed-citation id="d1198e896" publication-type="other">
Gastil, R. D. (1990) 'The Comparative Survey of Freedom: Experiences and Sug-
gestions', Studies in Comparative International Development, 25(1): 25-50.</mixed-citation>
         </ref>
         <ref id="d1198e906a1310">
            <mixed-citation id="d1198e910" publication-type="other">
Gourevitch, P. A. (2003) 'The Politics of Corporate Governance Regulation', The
Yale Law Journal, 112(7): 1829-80.</mixed-citation>
         </ref>
         <ref id="d1198e920a1310">
            <mixed-citation id="d1198e924" publication-type="other">
Gourevitch, P. (2006) World Bank Legal Review: Law Equity and Development, Vol. 2,
Washington, DC: World Bank Publications chapter Politics, Institutions and
Society: Seeking Better Results.</mixed-citation>
         </ref>
         <ref id="d1198e937a1310">
            <mixed-citation id="d1198e941" publication-type="other">
Gourevitch, P. and Hawes, M. (2002) The Politics of Choice among National Pro-
duction Systems', L'Annee de la Regulation 6, Paris: Presses de Sciences Po,
pp. 241-270.</mixed-citation>
         </ref>
         <ref id="d1198e954a1310">
            <mixed-citation id="d1198e958" publication-type="other">
Gourevitch, P. A. and Shinn, J. (2005) Political Power and Corporate Control, Princeton,
NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1198e969a1310">
            <mixed-citation id="d1198e973" publication-type="other">
Grossman, G. and Helpman, E. (1994) 'Protection for Sale', American Economic
Review, 84(4): 833-50.</mixed-citation>
         </ref>
         <ref id="d1198e983a1310">
            <mixed-citation id="d1198e987" publication-type="other">
Haggard, S. and McCubbins, M. D. (eds) (2001) Presidents, Parliaments, and Policy,
Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e997a1310">
            <mixed-citation id="d1198e1001" publication-type="other">
Henisz, W. J. (2000) 'The Institutional Environment for Economic Growth', Eco-
nomics and Politics, 12(1): 1-31.</mixed-citation>
         </ref>
         <ref id="d1198e1011a1310">
            <mixed-citation id="d1198e1015" publication-type="other">
Henisz, W. J. (2002) 'The Institutional Environment for Infrastructure Investment',
Industrial and Corporate Change, ll(2):355-89.</mixed-citation>
         </ref>
         <ref id="d1198e1025a1310">
            <mixed-citation id="d1198e1029" publication-type="other">
Hibbs, D. A., Jr. (1977) 'Political Parties and Macroeconomic Policy', American
Political Science Review, 71(4): 1467-87.</mixed-citation>
         </ref>
         <ref id="d1198e1039a1310">
            <mixed-citation id="d1198e1043" publication-type="other">
Hibbs, D. A., Jr. (1992) 'Partisan Theory After Fifteen Years', European Journal of
Political Economy, 8(3): 361-73.</mixed-citation>
         </ref>
         <ref id="d1198e1054a1310">
            <mixed-citation id="d1198e1058" publication-type="other">
Hiscox, M. J. (2001) 'Inter-Industry Factor Mobility and the Politics of Trade',
International Organization, 55(1): 1-46.</mixed-citation>
         </ref>
         <ref id="d1198e1068a1310">
            <mixed-citation id="d1198e1072" publication-type="other">
La Porta, R., Lopez-de-Silanes, F. and Shleifer, A. (1999) 'Corporate Ownership
around the World', Journal of Finance, 54(6): 471-517.</mixed-citation>
         </ref>
         <ref id="d1198e1082a1310">
            <mixed-citation id="d1198e1086" publication-type="other">
La Porta, R, Lopez-de Silanes, F., Shleifer, A. and Vishny, R. W. (1997) 'Legal
Determinants of External Finance', Journal of Finance, 52(2): 1131-50.</mixed-citation>
         </ref>
         <ref id="d1198e1096a1310">
            <mixed-citation id="d1198e1100" publication-type="other">
La Porta, R, Lopez-de Silanes, F., Shleifer, A. and Vishny, R. W. (1998) 'Law and
Finance', Journal of Political Economy, 106(3): 1113-55.</mixed-citation>
         </ref>
         <ref id="d1198e1110a1310">
            <mixed-citation id="d1198e1114" publication-type="other">
La Porta, R., Lopez-de-Silanes, F. and Shleifer, A. (2008) 'The Economic
Consequences of Legal Origins', Journal of Economic Literature, 46(2):
285-332.</mixed-citation>
         </ref>
         <ref id="d1198e1127a1310">
            <mixed-citation id="d1198e1131" publication-type="other">
Levine, R. (2005) 'Finance and Growth' in Theory and Evidence. Handbook of Economic
Growth,. The Netherlands: Elsevier Science.</mixed-citation>
         </ref>
         <ref id="d1198e1142a1310">
            <mixed-citation id="d1198e1146" publication-type="other">
Levine, R. Loayza, N. and Beck, T. (2000) 'Financial Intermediation and
Growth: Causality and Causes', Journal of Monetary Economics, 46(1): 31-
77.</mixed-citation>
         </ref>
         <ref id="d1198e1159a1310">
            <mixed-citation id="d1198e1163" publication-type="other">
Lijphart, A. (1999) Patterns of Democracy, New Haven, CT: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d1198e1170a1310">
            <mixed-citation id="d1198e1174" publication-type="other">
Lindblom, C. E. (1977) Politics and Markets. The World's Political-economic Systems,
New York: Basic Books.</mixed-citation>
         </ref>
         <ref id="d1198e1184a1310">
            <mixed-citation id="d1198e1188" publication-type="other">
Mackie, G. (2003) Democracy Defended, Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e1195a1310">
            <mixed-citation id="d1198e1199" publication-type="other">
Marshall, M. G., and Jaggers, K. (2004) Polity IV Project: Political Regime Character-
istics and Transitions, 1800 - 2004, Center for International Development and
Conflict Management, University of Maryland.</mixed-citation>
         </ref>
         <ref id="d1198e1212a1310">
            <mixed-citation id="d1198e1216" publication-type="other">
McNollgast. (1987) 'Administrative Procedures as Instruments of Political Control',
journal of Law, Economics and Organization, 3(2): 243-77.</mixed-citation>
         </ref>
         <ref id="d1198e1227a1310">
            <mixed-citation id="d1198e1231" publication-type="other">
McNollgast. (1989) 'Structure and Process, Politics and Policy: Administrative Ar-
rangements and the Political Control of Agencies', Virginia Law Review, 75(2):
431-82.</mixed-citation>
         </ref>
         <ref id="d1198e1244a1310">
            <mixed-citation id="d1198e1248" publication-type="other">
Milner, H. and Judkins, B. (2004) 'Partisanship, Trade Policy, and Globalization:
Is There a Left-Right Divide on Trade Policy?', International Studies Quarterly,
48(1): 95-119.</mixed-citation>
         </ref>
         <ref id="d1198e1261a1310">
            <mixed-citation id="d1198e1265" publication-type="other">
North, D. C. and Weingast, B. R. (1989) 'Constitutions and Commitment: The
Evolution of Institutional Governing Public Choice in Seventeenth-Century
England', The Journal of Economic History, 49(4): 803-32.</mixed-citation>
         </ref>
         <ref id="d1198e1278a1310">
            <mixed-citation id="d1198e1282" publication-type="other">
Pagano, M. and Volpin, P. F. (2005) 'The Political Economy of Corporate Gover-
nance', American Economic Review, 95(4): 1005-30.</mixed-citation>
         </ref>
         <ref id="d1198e1292a1310">
            <mixed-citation id="d1198e1296" publication-type="other">
Perotti, E. C. and Volpin, P. F. (2007) 'Politics, Investor Protection and Competition',
ECGI - Finance Working Paper No. 162/2007; Tinbergen Institute Discussion
Paper No. 07-006/2.</mixed-citation>
         </ref>
         <ref id="d1198e1309a1310">
            <mixed-citation id="d1198e1313" publication-type="other">
Perotti, E. C. and von Thadden, E.-L. (2006) The Political Economy of Corporate
Control and Labor Rents', Journal of Political Economy, 114(1): 145-74.</mixed-citation>
         </ref>
         <ref id="d1198e1324a1310">
            <mixed-citation id="d1198e1328" publication-type="other">
Pinto, P. M. (2004) 'Domestic Coalitions and the Political Economy of Foreign Direct
Investment', PhD dissertation, Department of Political Science, University of
California, San Diego, La Jolla, CA.</mixed-citation>
         </ref>
         <ref id="d1198e1341a1310">
            <mixed-citation id="d1198e1345" publication-type="other">
Pinto, P. (2005) 'Does Partisanship Affect the Regulation of Foreign Investment?
Download', Prepared for the Conference 'The Political Economy of Regulating
Multinational Corporations and Foreign Direct Investment', the Pennsylvania
State University, State College, 14-15 October, 2005.</mixed-citation>
         </ref>
         <ref id="d1198e1361a1310">
            <mixed-citation id="d1198e1365" publication-type="other">
Pinto, S. M. and Pinto, P. M. (2007) The Politics of Investment: Partisan Gov-
ernments, Wages and Employment', Paper presented at annual meeting of the
International Political Economy Society (IPES), 9-10 November, Palo Alto, CA.</mixed-citation>
         </ref>
         <ref id="d1198e1378a1310">
            <mixed-citation id="d1198e1382" publication-type="other">
Pinto, P. M., and Pinto, S. M. (2008a) 'The Politics of Investment: Partisanship and
Sectoral Allocation of Foreign Direct Investment', Economics &amp; Politics, 20(2):
216-54.</mixed-citation>
         </ref>
         <ref id="d1198e1395a1310">
            <mixed-citation id="d1198e1399" publication-type="other">
Pinto, P. M., and Pinto, S. M. (2008b) 'Partisan Commitment: A Dynamic Model
of the Politics of Investment', Paper prepared for presentation at the Annual
Meeting of the Midwest Political Science Association, Chicago, 3-6 April.</mixed-citation>
         </ref>
         <ref id="d1198e1412a1310">
            <mixed-citation id="d1198e1416" publication-type="other">
Quinn, D. (1997) 'The Correlates of Change in International Financial Regulation',
The American Political Science Review, 91(3): 531-51.</mixed-citation>
         </ref>
         <ref id="d1198e1427a1310">
            <mixed-citation id="d1198e1431" publication-type="other">
Quinn, D. and Inclan, C. (1997) 'The Origins of Financial Openness: A Study
of Current and Capital Account Liberalization', American Journal of Political
Science, 41(3): 771-813.</mixed-citation>
         </ref>
         <ref id="d1198e1444a1310">
            <mixed-citation id="d1198e1448" publication-type="other">
Rajan, R. G. (2009) 'Rent Preservation and the Persistence of Underdevelopment',
American Economic Journals: Macroeconomics, 1(1): 178-218.</mixed-citation>
         </ref>
         <ref id="d1198e1458a1310">
            <mixed-citation id="d1198e1462" publication-type="other">
Rajan, R. G., and Zingales, L. (2003a) The Great Reversals: The Politics of Financial
Development in the Twentieth Century', Journal of Financial Economics, 69(1):
5-50.</mixed-citation>
         </ref>
         <ref id="d1198e1475a1310">
            <mixed-citation id="d1198e1479" publication-type="other">
Rajan, R. G. and Zingales, L. (2003b) Saving Capitalism from the Capitalists: Unleashing
the Power of Financial Markets to Create Wealth and Spread Opportunity, New York,
NY: Crown Business.</mixed-citation>
         </ref>
         <ref id="d1198e1492a1310">
            <mixed-citation id="d1198e1496" publication-type="other">
Roe, M. (2003) Political Determinants of Corporate Governance: Political Context, Cor-
porate Impact, New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1198e1506a1310">
            <mixed-citation id="d1198e1510" publication-type="other">
Rogowski, R. (1987) 'Political Cleavages and Changing Exposure to Trade', Ameri-
can Political Science Review, 81(4): 1121-37.</mixed-citation>
         </ref>
         <ref id="d1198e1521a1310">
            <mixed-citation id="d1198e1525" publication-type="other">
Rogowski, R. and Kayser, M. (2002) 'Majoritarian Electoral Systems and Consumer
Power: Price-Level Evidence from the OECD Countries', American Journal of
Political Science, 46(3): 526-39.</mixed-citation>
         </ref>
         <ref id="d1198e1538a1310">
            <mixed-citation id="d1198e1542" publication-type="other">
Shugart, M. S., and Carey, J. M. (1982) Presidents and Assemblies. Constitutional Design
and Electoral Dynamics, New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e1552a1310">
            <mixed-citation id="d1198e1556" publication-type="other">
Stasavage, D. (2003) Public Debt and the Birth of the Democratic State: France and Great
Britain, 1688-1789, Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1198e1566a1310">
            <mixed-citation id="d1198e1570" publication-type="other">
Tiberghien, Y. (2007) Entrepreneurial States: Reforming Corporate Governance in France,
Japan, and Korea, Ithaca, NY: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d1198e1580a1310">
            <mixed-citation id="d1198e1584" publication-type="other">
Tsebelis, G. (2002) Veto Players: How Political Institutions Work, New York, NY:
Princeton University Press/Russell Sage.</mixed-citation>
         </ref>
         <ref id="d1198e1594a1310">
            <mixed-citation id="d1198e1598" publication-type="other">
Tufte, E. R. (1978) Political Control of the Economy, Princeton, NJ: Princeton University
Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
