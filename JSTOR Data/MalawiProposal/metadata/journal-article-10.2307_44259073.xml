<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">commdevej</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019784</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Community Development Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford Journals, Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00103802</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14682656</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44259073</article-id>
         <title-group>
            <article-title>Vignettes of Communities in Action: an exploration of participatory methodologies in promoting community development in Nigeria</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Oga Steve</given-names>
                  <surname>Abah</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">42</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40178558</issue-id>
         <fpage>435</fpage>
         <lpage>448</lpage>
         <permissions>
            <copyright-statement>© Oxford University Press and Community Development Journal 2007</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44259073"/>
         <abstract>
            <p>Three interconnected arguments are explored in this article. It begins by a reconsideration of community development, not from official and agency definitions but from what makes development real and satisfies not only physical needs, but also the spiritual as well as psychological. The second part looks at Theatre for Development (TFD) as a system of actualizing the participatory agenda so direly required, so often talked about and very consistently ignored in community development. I argue that TFD in its performative approach to discussing issues, forging alliances and community cohesion contributes to community development. In this instance, we witness TFD as a community art for instigating participation and change. I also talk about how in combination with other participatory methods, TFD can be empowering. This combination is what I call methodological conversation and the aesthetics that defines this conversation involves respect, dialogue, inclusion and flexibility. The case study section narrates the way in which this methodology has been applied in the turbulent environment of the Niger Delta in Nigeria. I argue that TFD, Participatory Learning and Action (PLA) and Questionnaire methods were the triumvirate of approaches that allowed us to understand issues and for communities to listen to us. The challenge remains how to define and develop an enduring relationship between researchers, community and government who may have the wherewithal for action.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d799e114a1310">
            <mixed-citation id="d799e118" publication-type="other">
Abah, O.S. (2002) Playing in the dust: women's health in Nigeria, in A. Cornwall and
A. Wellbourn, eds, Realizing Rights : Transforming Approaches to Sexual and Reproductive
Well-Being, Zed Books, London and New York, pp. 73-83.</mixed-citation>
         </ref>
         <ref id="d799e131a1310">
            <mixed-citation id="d799e135" publication-type="other">
Abah, O.S. (2003) Methodological conversations in researching citizenship: drama and
participatory learning and action in encountering citizens, in O.S. Abah, ed,
Geographies of Citizenship in Nigeria, Tamaza Publishing Company Ltd, Zaria,
pp. 114-143.</mixed-citation>
         </ref>
         <ref id="d799e151a1310">
            <mixed-citation id="d799e155" publication-type="other">
Abah, O.S. (2004) 'Beyond boundaries: theatre for development and social change in
Nigeria', paper presented at the Performing Africa Conference, Organized by Leeds
University Centre for African Studies (LUCAS), May 14.</mixed-citation>
         </ref>
         <ref id="d799e168a1310">
            <mixed-citation id="d799e172" publication-type="other">
Abah, O.S. (2005) Performing Life: The Practice of Theatre for Development, Tamaza
Publishing Company Limited, Zaria.</mixed-citation>
         </ref>
         <ref id="d799e183a1310">
            <mixed-citation id="d799e187" publication-type="other">
Abah, O.S. and Okwori, J.Z. (2005) A nation in search of citizens: problems of
citizenship in the Nigerian context, in N. Kabeer, ed, Inclusive Citizenship: Meanings
and Expressions, Zed Books, London, pp. 71-84.</mixed-citation>
         </ref>
         <ref id="d799e200a1310">
            <mixed-citation id="d799e204" publication-type="other">
Boal, A. (1985) Theatre of the Oppressed, Theatre Communication Group, New York.</mixed-citation>
         </ref>
         <ref id="d799e211a1310">
            <mixed-citation id="d799e215" publication-type="other">
Boal, A. (2002) Games for Actors and Non-Actors, Routledge, London.</mixed-citation>
         </ref>
         <ref id="d799e222a1310">
            <mixed-citation id="d799e226" publication-type="other">
Cornwall, A. and Pratt, G. (2003) Pathways to Participation: Reflections on PRA, ITDG
Publishing, London.</mixed-citation>
         </ref>
         <ref id="d799e236a1310">
            <mixed-citation id="d799e240" publication-type="other">
Coronel, S. and Dixit, K. (2006) The development debate thirty years after What Next,
Development Dialogue, 1 (47), 13-27.</mixed-citation>
         </ref>
         <ref id="d799e250a1310">
            <mixed-citation id="d799e254" publication-type="other">
Etherton, M. (1982) The Development of African Drama, Hutchinson University Library,
London.</mixed-citation>
         </ref>
         <ref id="d799e265a1310">
            <mixed-citation id="d799e269" publication-type="other">
Thandika Mkandawire. (2005) African Intellectuals: Rethinking Politics, Language, Gender
and Development, Zed Books, London.</mixed-citation>
         </ref>
         <ref id="d799e279a1310">
            <mixed-citation id="d799e283" publication-type="other">
Narayan, D. et al. (2000) Voices of the Poor: Crying Out for Change, The World Bank,
Washington.</mixed-citation>
         </ref>
         <ref id="d799e293a1310">
            <mixed-citation id="d799e297" publication-type="other">
Okon, E.J. (2003) Women's Issues and Women's Action in Claiming Citizenship Rights in the
Niger Delta, TFDC, Zaria.</mixed-citation>
         </ref>
         <ref id="d799e307a1310">
            <mixed-citation id="d799e311" publication-type="other">
Okwori, J.Z. (1998) Ije: The Performance Traditions of the Idoma, Instances
Communications, Zaria.</mixed-citation>
         </ref>
         <ref id="d799e321a1310">
            <mixed-citation id="d799e325" publication-type="other">
Osaghae, E.E. (1997) Exiting from the state in Nigeria, in J. Haynes, ed, Democracy and
Civil Society in the Third World: Politics and New Social Movements, Polity Press,
Cambridge.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
