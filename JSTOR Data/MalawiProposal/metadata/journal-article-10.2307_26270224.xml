<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26270224</article-id>
         <article-categories>
            <subj-group>
               <subject>Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Illusions of empowerment? Questioning policy and practice of community forestry in Kenya</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Chomba</surname>
                  <given-names>Susan W.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Nathan</surname>
                  <given-names>Iben</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Minang</surname>
                  <given-names>Peter A.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
               <xref ref-type="aff" rid="af3">³</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Sinclair</surname>
                  <given-names>Fergus</given-names>
               </string-name>
               <xref ref-type="aff" rid="af3">³</xref>
               <xref ref-type="aff" rid="af4">⁴</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>University of Copenhagen,</aff>
            <aff id="af2">
               <label>²</label>ASB Partnership for the Tropical Forest Margins,</aff>
            <aff id="af3">
               <label>³</label>World Agroforestry Centre (ICRAF),</aff>
            <aff id="af4">
               <label>⁴</label>Bangor University, Wales</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>9</month>
            <year>2015</year>
            <string-date>Sep 2015</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">20</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26270221</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2015 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26270224"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>The extent to which community forestry (CF) contributes to empowerment of local communities remains hotly contested. We develop a unified theory of empowerment at the intersection of asset-based agency and institution-based opportunity and apply it to examine the extent to which the implementation of CF has led to local empowerment. Our empirical data are drawn from review of national level policies and a field study of Ngare Ndare Community Forest Association (CFA) in Kenya. We investigated what types of powers were transferred to the local level, how representative the local institution was of the local community, and how its formation and composition affected the empowerment of socially and economically differentiated groups, with competing claims over the forest resource. We found that national forest policies and actors transferred minimal powers that enabled local communities to execute forest protection and conservation roles, while maintaining legislative powers and control of economic benefits centrally; and, that representation within the CFA was highly skewed in favor of small and already powerful local elites. We discuss the findings in the light of the literature on empowerment to develop insights about how to more effectively manage processes to empower local communities through appropriately representative institutions.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>community forestry</kwd>
            <kwd>empowerment</kwd>
            <kwd>Kenya</kwd>
            <kwd>representation</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Agrawal, A., A. Chhatre, and R. Hardin. 2008. Changing governance of the world’s forests. Science 320:1460-1462. http://dx.doi.org/10.1126/science.1155369</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Agrawal, A., and C. C. Gibson. 1999. Enchantment and disenchantment: the role of community in natural resource conservation. World Development 27:629-649. http://dx.doi.org/10.1016/S0305-750X(98)00161-2</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Agrawal, A., and E. Ostrom. 2008. Decentralization and community-based forestry: learning from experience. Pages 44-67 in E. L. Webb and G. Shivakoti, editors. Decentralization, forests and rural communities: policy outcomes in South and Southeast Asia Sage, London, UK.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Agrawal, A., and J. Ribot. 1999. Accountability in decentralization: a framework with South Asian and West African cases. Journal of Developing Areas 33:473-502.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Alkire, S. 2005. Subjective quantitative studies of human agency. Social Indicators Research 74(1):217-260. http://dx.doi.org/10.1007/s11205-005-6525-0</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Alsop, R., M. Bertelsen, and J. Holland. 2006. Empowerment in practice from analysis to implementation. World Bank, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Bandiaky-Badji, S. 2011. Gender equity in Senegal’s forest governance history: why policy and representation matter. International Forestry Review 13(2):177-194. http://dx.doi.org/10.1505/146554811797406624</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Berkes, F. 2009. Evolution of co-management: role of knowledge generation, bridging organizations and social learning. Journal of Environmental Management 90:1692-1702. http://dx.doi.org/10.1016/j.jenvman.2008.12.001</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Blaikie, P. 2006. Is small really beautiful? Community-based natural resource management in Malawi and Botswana. World Development 34(11):1942-1957. http://dx.doi.org/10.1016/j.worlddev.2005.11.023</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Borrini-Feyerabend, G., M. Pimbert, M. T. Farvar, A. Kothari, and Y. Renard. 2004. Sharing power: learning by doing in comanagement of natural resources throughout the world. IIED and IUCN/CEESP/CMWG, Cenesta, Tehran.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Brosius, J. P., A. L. Tsing, and C. Zerner. 1998. Representing communities: histories and politics of community-based natural resource management. Society &amp; Natural Resources 11(2):157-168. http://dx.doi.org/10.1080/08941929809381069</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Chomba, S., T. Treue, and F. Sinclair. In press. The political economy of forest entitlements: can community based forest management reduce vulnerability at the forest margin? Forest Policy and Economics. http://dx.doi.org/10.1016/j.forpol.2014.11.011</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Cleaver, F. 2005. The inequality of social capital and the reproduction of chronic poverty. World Development 33(6):893-906. http://dx.doi.org/10.1016/j.worlddev.2004.09.015</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Føllesdal, A. 1998. Survey article: subsidiarity. Journal of Political Philosophy 6:190-218. http://dx.doi.org/10.1111/1467-9760.00052</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Food and Agriculture Organisation of the United Nations (FAO). 2010. Global forest resource assessment 2010. FAO, Rome, Italy.[online] URL: http://foris.fao.org/static/data/fra2010/KeyFindingsen.pdf</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Glaser, B. G., and A. L. Strauss. 2009. The discovery of grounded theory: strategies for qualitative research. Transaction, New Brunswick, New Jersey, USA.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Government of Kenya (GoK). 2005. Laws of Kenya, the Kenya Forests Act, 2005. Government Printer, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Government of Kenya (GoK). 2012. Laws of Kenya, Trustee Act chapter 167, revised edition 2012 [1982]. Government Printer, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Hardin, G. 1968. The tragedy of the commons. Science 162(3859):1243-1248. http://dx.doi.org/10.1126/science.162.3859.1243</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Ibrahim, S., and S. Alkire. 2007. Agency and empowerment: a proposal for internationally comparable indicators. Oxford Development Studies 35(4):379-403. http://dx.doi.org/10.1080/13600810701701897</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Kellert, S. R., J. N. Mehta, S. A. Ebbin, and L. L. Lichtenfeld. 2000. Community natural resource management: promise, rhetoric, and reality. Society &amp; Natural Resources 13(8):705-715. http://dx.doi.org/10.1080/089419200750035575</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Klopp, J. M. 2012. Deforestation and democratization: patronage, politics and forests in Kenya. Journal of Eastern African Studies 6(2):351-370. http://dx.doi.org/10.1080/17531055.2012.669577</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Larson, A. M. 2005. Democratic decentralization in the forestry sector: lessons learned from Africa, Asia and Latin America. Pages 32-62 in C. J. Pierce Colfer and D. Capistrano, editors. The politics of decentralization: forests, power and people. Earthscan/James &amp; James, London, UK.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Larson, A. M., and J. C. Ribot. 2007. The poverty of forestry policy: double standards on an uneven playing field. Sustainability Science 2:189-204. http://dx.doi.org/10.1007/s11625-007-0030-0</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Leach, M., R. Mearns, and I. Scoones. 1999. Environmental entitlements: dynamics and institutions in community-based natural resource management. World Development 27(2):225-247. http://dx.doi.org/10.1016/s0305-750x(98)00141-7</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Ministry of Environment and Natural Resources (MENR). 2007a. The forests (participation in sustainable forest management) rules. Government Printer, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Ministry of Environment and Natural Resources (MENR). 2007b. Participatory forest management guidelines. Government Printer, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Mogoi, J., E. Obonyo, P. Ongugo, V. Oeba, and E. Mwangi. 2012.Communities, property rights and forest decentralisation in Kenya: early Lessons from participatory forestry management. Conservation &amp; Society 10(2):182-194. http://dx.doi.org/10.4103/0972-4923.97490</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Mugo, E., C. Nyandiga, and M. Gachanja. 2010. Development of forestry in Kenya (1900-2007): challenges and lessons learnt. Kenya Forests Working Group, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Narayan, D. 2005. Measuring empowerment: cross-disciplinary perspectives. World Bank, Washington, D.C., USA. http://dx.doi.org/10.1037/e597202012-001</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Njeru, J. 2010. ‘Defying’ democratization and environmental protection in Kenya: the case of Karura Forest Reserve in Nairobi. Political Geography 29:333-342. http://dx.doi.org/10.1016/j.polgeo.2010.07.003</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Nygren, A. 2000. Development discourses and peasant-forest relations: natural resource utilization as social process. Development and Change 31(1):11-34. http://dx.doi.org/10.1111/1467-7660.00145</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Nygren, A. 2005. Community-based forest management within the context of institutional decentralization in Honduras. World Development 33:639-655. http://dx.doi.org/10.1016/j.worlddev.2004.11.002</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Olowu, D. 2003. Local institutional and political structures and processes: recent experience in Africa. Public Administration and Development 23(1):41-52. http://dx.doi.org/10.1002/pad.258</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Ostrom, E. 1990. Governing the commons: the evolution of institutions for collective action. Cambridge University Press, Cambridge, UK. http://dx.doi.org/10.1017/cbo9780511807763</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Oyono, P. R. 2004. One step forward, two steps back? Paradoxes of natural resources management decentralisation in Cameroon. Journal of Modern African Studies 42:91-111. http://dx.doi.org/10.1017/S0022278X03004488</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Poteete, A. R., and J. C. Ribot. 2011. Repertoires of domination: decentralization as process in Botswana and Senegal. World Development 39(3):439-449. http://dx.doi.org/10.1016/j.worlddev.2010.09.013</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Ribot, J. C. 2003. Democratic decentralisation of natural resources: institutional choice and discretionary power transfers in Sub-Saharan Africa. Public Administration and Development 23:53-65 http://dx.doi.org/10.1002/pad.259</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Ribot, J. C. 2004. Waiting for democracy. World Resources Institute, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Ribot, J. C., A. Agrawal, and A. M. Larson. 2006. Recentralizing while decentralizing: how national governments reappropriate forest resources. World Development 34:1864-1886. http://dx.doi.org/10.1016/j.worlddev.2005.11.020</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Ribot, J. C., J. F. Lund, and T. Treue. 2010. Democratic decentralization in sub-Saharan Africa: its contribution to forest management, livelihoods, and enfranchisement. Environmental Conservation 37(01):35-44. http://dx.doi.org/10.1017/s0376892910000329</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Rowlands, J. 1997. Questioning empowerment. Oxfam, Oxford, UK. http://dx.doi.org/10.3362/9780855988364</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Saito-Jensen, M., I. Nathan, and T. Treue. 2010. Beyond elite capture? Community-based natural resource management and power in Mohammed Nagar village, Andhra Pradesh, India. Environmental Conservation 37:327-335. http://dx.doi.org/10.1017/S0376892910000664</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Sandbrook, C., F. Nelson, W. M. Adams, and A. Agrawal. 2010.Carbon, forests and the REDD paradox. Oryx 44:330-334. http://dx.doi.org/10.1017/S0030605310000475</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Standard Media. 2015. Kenya forest service signs 68 pacts with communities. 11 January. [online] URL: http://www.standardmedia.co.ke/business/article/2000147463/kenya-forest-servicesigns- 68-pacts-with-communities</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Standing, A., and M. Gachanja. 2014. The political economy of REDD+ in Kenya: identifying and responding to corruption challenges. U4 Issue 2014(3).</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Thenya, T., B. O. B. Wandago, E. T. Nahama, and M. Gachanja. 2008. Participatory forest management experiences in Kenya (1996-2007). Kenya Forests Working Group, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">United Nations (UN). 1992. The statement of forest principles. Report of the United Nations Conference on Environment and Development, Rio De Janeiro, Brazil.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">United Nations (UN). 2000. Millennium development goals (MDGs). UN, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">United Nations (UN). 2007. United Nations declaration on rights of indigenous people. UN, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">United Nations Economic Commission for Europe (UNECE). 1998. The UNECE convention on access to information, public participation in decision-making and access to justice in environmental matters. UNECE, Geneva, Switzerland.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Wängnerud, L. 2009. Women in parliaments: descriptive and substantive representation. Annual Review of Political Science 12:51-69. http://dx.doi.org/10.1146/annurev.polisci.11.053106.123839</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">White, A., and A. Martin. 2002. Who owns the world’s forests? Forest tenure and public forests in transition. Forest Trends, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">World Bank. 2007. Strategic environmental assessment of the Kenya Forest Act 2005. World Bank, Washington, D.C., USA.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
