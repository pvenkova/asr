<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mariecolprogseri</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50011610</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Marine Ecology Progress Series</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Inter-Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01718630</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16161599</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24827250</article-id>
         <article-categories>
            <subj-group>
               <subject>RESEARCH ARTICLES</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Microbial plankton response to resource limitation: insights from the community structure and seston stoichiometry in Florida Bay, USA</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter J.</given-names>
                  <surname>Lavrentyev</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Harvey A.</given-names>
                  <surname>Bootsma</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thomas H.</given-names>
                  <surname>Johengen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Joann F.</given-names>
                  <surname>Cavaletto</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Wayne S.</given-names>
                  <surname>Gardner</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>7</day>
            <month>5</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">165</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24825380</issue-id>
         <fpage>45</fpage>
         <lpage>57</lpage>
         <permissions>
            <copyright-statement>Copyright © Inter-Research 1998</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24827250"/>
         <abstract>
            <p>Concentrations of dissolved and particulate nutrients, chlorophyll, and microorganisms (0.01 to 200 μm) were simultaneously measured during a 1 d survey of 12 stations in Florida Bay, USA, to characterize the microbial plankton community with respect to resource limitation. Three distinct types of trophic conditions, reflected in seston elemental stoichiometry and community structure, were identified within the bay. The first type, characteristic of the isolated eastern region, had low nutrient concentrations, imbalanced stoichiometry, and small microbial biomass with a large proportion of bacteria. The microbial community in this region was characterized by weak relationships between microzooplankton and phytoplankton and the predominance of mixotrophic taxa and the autotrophic ciliate Mesodinium rubrum. The second type, found in the north-central region influenced by Taylor Slough inflow, had elevated nutrient concentrations, elemental stoichiometry skewed toward N, and high turbidity. Under these conditions, the picocyanobacterium Synechococcus formed a dense bloom and coincided with an abundant, multi-step microbial food web. Finally, at the boundary with the Gulf of Mexico, low concentrations of nutrients were balanced at approximately the Redfield ratio and supported nanophytoplankton that were tightly correlated with microzooplankton. These data are consistent with the notion of P limitation in Florida Bay but also demonstrate that Si, light, and N may be colimiting to phytoplankton in the eastern, north-central, and western boundary regions, respectively. Our findings suggest that multiple resource gradients, in conjunction with microbial food web processes, are important factors determining the plankton community structure in Florida Bay and should be considered in studies on ecological disturbances.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d36e256a1310">
            <mixed-citation id="d36e260" publication-type="other">
Arenowski AL, Lim EL, Caron DA (1995) Mixotrophic plank-
ton in oligotrophic surface waters of the Sargasso Sea may
employ phaqotrophy to obtain major nutrients. J Plankton
Res 17:801-820</mixed-citation>
         </ref>
         <ref id="d36e276a1310">
            <mixed-citation id="d36e280" publication-type="other">
Berdalet E, Marrase C, Estrada M, Arin L, MacLean ML
(1996) Microbial community response to nitrogen- and
phosphorus-deficient nutrient inputs: microplankton
dynamics and biochemical characterization. J Plankton
Res 18:1627-1641</mixed-citation>
         </ref>
         <ref id="d36e299a1310">
            <mixed-citation id="d36e303" publication-type="other">
Bjorkman K, Karl DM (1994) Bioavailability of inorganic and
organic phosphorus compounds to natural assemblages of
microorganisms in Hawaiian coastal waters. Mar Ecol
Prog Ser 111:265-273</mixed-citation>
         </ref>
         <ref id="d36e319a1310">
            <mixed-citation id="d36e323" publication-type="other">
Blomqvist P, Peterson A, Hyenstrand P (1994) Ammonium-
nitrogen: a key regulatory factor causing dominance of
non-nitrogen-fixing cyanobacteria in aquatic systems.
Arch Hydrobiol 132:141-164</mixed-citation>
         </ref>
         <ref id="d36e340a1310">
            <mixed-citation id="d36e344" publication-type="other">
Boesch DF, Armstrong NE, D'Elia CF, Maynard NG, Paerl
HN, Williams SL (1993) Deterioration of the Florida Bay
ecosystem: an evaluation of the scientific evidence. Report
to the Interagency Working Group on Florida Bay. Depart-
ment of the Interior, National Park Service, Washington,
DC</mixed-citation>
         </ref>
         <ref id="d36e367a1310">
            <mixed-citation id="d36e371" publication-type="other">
Bratbak G, Thingstad TF (1985) Phytoplankton-bacteria
interactions: an apparent paradox? Analysis of a model
system with both competition and commensalism. Mar
Ecol Prog Ser 25:23-30</mixed-citation>
         </ref>
         <ref id="d36e387a1310">
            <mixed-citation id="d36e391" publication-type="other">
Butler MJ IV, Hunt JH, Herrnkind WF, Childress MJ,
Bertelsen R, Sharp W, Matthews T, Field JM, Marshall
HG (1994) Cascading disturbances in Florida Bay, USA:
cyanobacteria bloom, sponge mortality, and implications
for juvenile spiny lobsters Panulirus argus. Mar Ecol Prog
Ser 129:119-125</mixed-citation>
         </ref>
         <ref id="d36e414a1310">
            <mixed-citation id="d36e418" publication-type="other">
Caron DA, Dam HG, Kremer P, Lessard EJ, Madin LP, Malone
TC, Napp JM, Peele ER, Roman MR, Youngbluth MJ
(1995) The contribution of microorganisms to particulate
carbon and nitrogen in surface waters of the Sargasso Sea
near Bermuda. Deep Sea Res (Part I) 42:943-972</mixed-citation>
         </ref>
         <ref id="d36e437a1310">
            <mixed-citation id="d36e441" publication-type="other">
Cloern JE, Cole BE, Hager SW (1994) Notes on a Mesodinium
rubrum red tide in San Francisco Bay (California, USA). J
Plankton Res 16:1269-1276</mixed-citation>
         </ref>
         <ref id="d36e454a1310">
            <mixed-citation id="d36e458" publication-type="other">
Cloern JE, Grenz C, Vidergar-Lucas L (1995) An empirical
model of the phytoplankton chlorophyll:carbon ratio—the
conversion factor between productivity and growth rate.
Limnol Oceanogr 40:1313-1321</mixed-citation>
         </ref>
         <ref id="d36e475a1310">
            <mixed-citation id="d36e479" publication-type="other">
Cotner JB Jr, Wetzel RG (1992) Uptake of dissolved inorganic
and organic phosphorus compounds by phytoplankton
and bacterioplankton. Limnol Oceanogr 37:232-243</mixed-citation>
         </ref>
         <ref id="d36e492a1310">
            <mixed-citation id="d36e496" publication-type="other">
Davis CO, Simmons MS (1979) Water chemistry and phyto-
plankton field and laboratory procedures. Report no. 70.
Great Lakes Research Division, Univ Michigan, Ann Arbor</mixed-citation>
         </ref>
         <ref id="d36e509a1310">
            <mixed-citation id="d36e513" publication-type="other">
Elser JJ, Chrzanowski TH, Sterner RW, Schampel JH, Foster
DK (1995) Elemental ratios and the uptake of nutrients by
phytoplankton and bacteria in three lakes of the Canadian
Shield. Microb Ecol 29:145-162</mixed-citation>
         </ref>
         <ref id="d36e529a1310">
            <mixed-citation id="d36e533" publication-type="other">
Elser JJ, Hasset PR (1994) A stoichiometric analysis of the
zooplankton-phytoplankton interactions in marine and
freshwater ecosystems. Nature 370:211-213</mixed-citation>
         </ref>
         <ref id="d36e546a1310">
            <mixed-citation id="d36e550" publication-type="other">
Fahnenstiel GL, Carrick HJ, Ituraga R (1991) Physiological
characteristics and food web dynamics of Synechococcus in
Lakes Huron and Michigan. Limnol Oceanogr 36:219-234</mixed-citation>
         </ref>
         <ref id="d36e563a1310">
            <mixed-citation id="d36e567" publication-type="other">
Fahnenstiel GL, McCormick MJ, Lang GA, Redalje DG,
Lohrenz SE, Markowitz M, Wagoner B, Carrick HJ (1995)
Taxon-specific growth and loss rates for dominant phyto-
plankton populations from the northern Gulf of Mexico.
Mar Ecol Prog Ser 117:229-239</mixed-citation>
         </ref>
         <ref id="d36e587a1310">
            <mixed-citation id="d36e591" publication-type="other">
Fourqurean JW, Jones RD, Zieman JC (1993) Processes influ-
encing water column nutrient characteristics and phos-
phorus limitation of phytoplankton biomass in Florida Bay,
FL, USA: inferences from spatial distributions. Estuar
Coast Shelf Sei 36:295-314</mixed-citation>
         </ref>
         <ref id="d36e610a1310">
            <mixed-citation id="d36e614" publication-type="other">
Fourqurean JW, Zieman JC, Powell GVN (1992) Phosphorus
limitation of primary production in Florida Bay: evidence
from C:N:P ratios of the dominant seagrass Thalassia tes-
tudinum. Limnol Oceanogr 37:162-171</mixed-citation>
         </ref>
         <ref id="d36e630a1310">
            <mixed-citation id="d36e634" publication-type="other">
Gardner WS, St John PA (1991) High-performance liquid
chromatographic method to determine ammonium ion
and primary amines in seawater. Anal Chem 63:537-540</mixed-citation>
         </ref>
         <ref id="d36e647a1310">
            <mixed-citation id="d36e651" publication-type="other">
Goldman JC, Glibert PM (1983) Kinetics of nitrogen uptake
by phytoplankton. In: Carpenter EJ, Capone DG (eds)
Nitrogen in the marine environment. Academic Press,
New York, p 233-274</mixed-citation>
         </ref>
         <ref id="d36e667a1310">
            <mixed-citation id="d36e671" publication-type="other">
Haigh R, Taylor FJR (1991) Mosaicism of microplankton
communities in the northern Straits of Georgia, British
Columbia. Mar Biol 110:301-314</mixed-citation>
         </ref>
         <ref id="d36e684a1310">
            <mixed-citation id="d36e688" publication-type="other">
Haigh R, Taylor FJR, Sutherland TF (1992) Phytoplankton
ecology of Sechelt Inlet, a fjord of the British Columbia
coast. I. General features of the nano- and microplankton.
Mar Ecol Prog Ser 89:117-134</mixed-citation>
         </ref>
         <ref id="d36e705a1310">
            <mixed-citation id="d36e709" publication-type="other">
Healey FP, Hendzel LL (1980) Physiological indicators of
nutrient deficiency in lake phytoplankton. Can J Fish
Aquat Sei 37:442-453</mixed-citation>
         </ref>
         <ref id="d36e722a1310">
            <mixed-citation id="d36e726" publication-type="other">
Hecky RE, Campbell P, Hendzel LL (1993) The stoichiometry
of carbon, nitrogen, and phosphorus in particulate matter
of lakes and oceans. Limnol Oceanogr 38:709-724</mixed-citation>
         </ref>
         <ref id="d36e739a1310">
            <mixed-citation id="d36e743" publication-type="other">
Hecky RE, Kilham P (1988) Nutrient limitation of phytoplank-
ton in freshwater and marine environments: a review of
recent evidence on the effects of enrichment. Limnol
Oceanogr 33:796-822</mixed-citation>
         </ref>
         <ref id="d36e759a1310">
            <mixed-citation id="d36e763" publication-type="other">
Hennes KP, Suttle CA (1994) Direct counts of viruses in
natural waters and laboratory cultures by epifluorescence
microscopy. Limnol Oceanogr 40:1050-1055</mixed-citation>
         </ref>
         <ref id="d36e776a1310">
            <mixed-citation id="d36e780" publication-type="other">
Jürgens K, Giide H (1994) The potential importance of
grazing-resistant bacteria in planktonic systems: a review.
Mar Ecol Prog Ser 112:169-188</mixed-citation>
         </ref>
         <ref id="d36e793a1310">
            <mixed-citation id="d36e797" publication-type="other">
Kilham P, Hecky RE (1988) Comparative ecology of marine and
freshwater phytoplankton. Limnol Oceanogr 33:776-795</mixed-citation>
         </ref>
         <ref id="d36e808a1310">
            <mixed-citation id="d36e812" publication-type="other">
Kulaev IS, Vagabov VM (1983) Polyphosphate metabolism
in microorganisms. Adv Microb Physiol 24:83-171</mixed-citation>
         </ref>
         <ref id="d36e822a1310">
            <mixed-citation id="d36e826" publication-type="other">
LaPointe BE (1989) Macroalgal production and nutrient rela-
tions in oligotrophic areas of Florida Bay. Bull Mar Sei
44:312-323</mixed-citation>
         </ref>
         <ref id="d36e839a1310">
            <mixed-citation id="d36e843" publication-type="other">
LaPointe BE, Clark MW (1992) Nutrient inputs from the
watershed and coastal eutrophication in the Florida Keys.
Estuaries 15:465-476</mixed-citation>
         </ref>
         <ref id="d36e856a1310">
            <mixed-citation id="d36e860" publication-type="other">
Lavrentyev PJ, Gardner WS, Johnson JR (1997) Cascading
trophic effects on aquatic nitrification: experimental evi-
dence and potential implications. Aquat Microb Ecol 13:
161-175</mixed-citation>
         </ref>
         <ref id="d36e876a1310">
            <mixed-citation id="d36e880" publication-type="other">
Legendre L, Rassoulzadegan F (1995) Plankton and nutrient
dynamics in marine waters. Ophelia 41:153-172</mixed-citation>
         </ref>
         <ref id="d36e890a1310">
            <mixed-citation id="d36e894" publication-type="other">
Luo YH, Mitsui A (1994) Hydrogen production from organic
substrates in an aerobic nitrogen-fixing marine unicellurar
cyanobacterium Synechococcus sp. strain Miami BG
043511. Biotech Bioeng 44:1255-1260</mixed-citation>
         </ref>
         <ref id="d36e911a1310">
            <mixed-citation id="d36e915" publication-type="other">
Menzel DW, Corwin N (1965) The measurement of total phos-
phorus seawater based on the liberation of organically
bound fractions by persulfate oxidation. Limnol Oceanogr
10:280-281</mixed-citation>
         </ref>
         <ref id="d36e931a1310">
            <mixed-citation id="d36e935" publication-type="other">
Miller CA, Penry DL, Glibert PM (1995) The impact of trophic
interactions on rates of nitrogen regeneration and grazing
in Chesapeake Bay. Limnol Oceanogr 40:1005-1011</mixed-citation>
         </ref>
         <ref id="d36e948a1310">
            <mixed-citation id="d36e952" publication-type="other">
Montagnes DJS (1995) Growth response of planktonic ciliates
in the genera Strobilidium and Stiombidium. Mar Ecol
Prog Ser 130:241-254</mixed-citation>
         </ref>
         <ref id="d36e965a1310">
            <mixed-citation id="d36e969" publication-type="other">
Montagnes DJS, Berges JA, Harrison PJ, Taylor FJR (1994)
Estimating carbon, protein, and chlorophyll a from volume
in marine phytoplankton. Limnol Oceanogr 39:1044-1060</mixed-citation>
         </ref>
         <ref id="d36e982a1310">
            <mixed-citation id="d36e986" publication-type="other">
Montagnes DJS, Lynn DH (1987) A quantitative Protargol
stain (QPS) for ciliates: method description and test of its
quantitative nature. Mar Microb Food Webs 2:83-93</mixed-citation>
         </ref>
         <ref id="d36e999a1310">
            <mixed-citation id="d36e1003" publication-type="other">
Montague CL, Ley JA (1993) A possible effect of salinity
fluctuation on abundance of benthic vegetation and asso-
ciated fauna in northeastern Florida Bay. Estuaries 16:
703-717</mixed-citation>
         </ref>
         <ref id="d36e1020a1310">
            <mixed-citation id="d36e1024" publication-type="other">
Nielsen TG, Kiorboe T (1991) Effects of a storm event on the
structure of the pelagic food web with special emphasis on
planktonic ciliates. J Plankton Res 13:35-51</mixed-citation>
         </ref>
         <ref id="d36e1037a1310">
            <mixed-citation id="d36e1041" publication-type="other">
Norland S (1993) The relationship between biomass and vol-
ume of bacteria. In: Kemp PF, Sherr BF, Sherr EB, Cole JJ
(eds) Handbook of methods in aquatic microbial ecology.
Lewis, Boca Raton, p 303-307</mixed-citation>
         </ref>
         <ref id="d36e1057a1310">
            <mixed-citation id="d36e1061" publication-type="other">
Paerl HW (1988) Nuisance phytoplankton blooms in coastal,
estuarine, and inland waters. Limnol Oceanogr 33:823-847</mixed-citation>
         </ref>
         <ref id="d36e1071a1310">
            <mixed-citation id="d36e1075" publication-type="other">
Paul JH, Rose JB, Jiang SC, Kellog CA, Dickson L (1993) Dis-
tribution of viral abundance in the reef environment of
Key Largo, Florida. Appl Environ Microbiol 59:718-724</mixed-citation>
         </ref>
         <ref id="d36e1088a1310">
            <mixed-citation id="d36e1092" publication-type="other">
Phlips EJ, Badylak S (1996) Spatial variability in phytoplank-
ton standing crop and composition in a shallow inner-shelf
lagoon, Florida Bay, Florida. Bull Mar Sei 58:203-216</mixed-citation>
         </ref>
         <ref id="d36e1105a1310">
            <mixed-citation id="d36e1109" publication-type="other">
Phlips EJ, Lynch TC, Badylak S (1995) Chlorophyll a, tripton,
color, and light availability in a shallow tropical inner-shelf
lagoon, Florida Bay, USA. Mar Ecol Prog Ser 127:223-234</mixed-citation>
         </ref>
         <ref id="d36e1123a1310">
            <mixed-citation id="d36e1127" publication-type="other">
Phlips EJ, Zeman C, Hansen P (1989) Growth, photosynthesis,
nitrogen fixation and carbohydrate production by a uni-
cellular cyanobacterium, Synechococcus sp. (Cyano-
phyta). J Appl Phycol 1:137-145</mixed-citation>
         </ref>
         <ref id="d36e1143a1310">
            <mixed-citation id="d36e1147" publication-type="other">
Porter KG, Feig YC (1980) The use of DAPI for identifying and
counting aquatic microflora. Limnol Oceanogr 25:943-948</mixed-citation>
         </ref>
         <ref id="d36e1157a1310">
            <mixed-citation id="d36e1161" publication-type="other">
Putt M, Stoecker DK (1989) An experimentally determined
carbon:volume ratio for marine oligotrichous ciliates from
estuarine and coastal waters. Limnol Oceanogr 34:177-183</mixed-citation>
         </ref>
         <ref id="d36e1174a1310">
            <mixed-citation id="d36e1178" publication-type="other">
Raven JA (1997) Phaqotrophy in phototrophs. Limnol Ocean-
ogr 42:198-205</mixed-citation>
         </ref>
         <ref id="d36e1188a1310">
            <mixed-citation id="d36e1192" publication-type="other">
Reyes E, Day JW Jr, Perez BC, Childers DL (1995) Nutrient
exchange between Florida Bay and the Everglades sal-
inity transition zone. Florida Bay Science Conference,
Gainesville, Florida, October 1995, p 20-22</mixed-citation>
         </ref>
         <ref id="d36e1208a1310">
            <mixed-citation id="d36e1212" publication-type="other">
Sherr BF, Sherr EB, Hopkinson CS (1988) Trophic interactions
within pelagic microbial communities: indications of feed-
back regulation of carbon flow. Hydrobiologia 159:19-26</mixed-citation>
         </ref>
         <ref id="d36e1226a1310">
            <mixed-citation id="d36e1230" publication-type="other">
Sherr EB, Caron DA, Sherr BF (1993) Staining of hetero-
trophic protists for visualization via epifluorescence micro-
scopy. In: Kemp PF, Sherr BF, Sherr EB, Cole JJ (eds)
Handbook of methods in aquatic microbial ecology. Lewis,
Boca Raton, p 213-228</mixed-citation>
         </ref>
         <ref id="d36e1249a1310">
            <mixed-citation id="d36e1253" publication-type="other">
Sherr EB, Rassoulzadegan F, Sherr BF (1989) Bacterivory by
pelagic choreotrichous ciliates in coastal waters of the NW
Mediterranean sea. Mar Ecol Prog Ser 55:235-240</mixed-citation>
         </ref>
         <ref id="d36e1266a1310">
            <mixed-citation id="d36e1270" publication-type="other">
Skibbe O (1994) An improved quantitative protargol stain for
ciliates and other planktonic protists. Arch Hydrobiol 132:
257-278</mixed-citation>
         </ref>
         <ref id="d36e1283a1310">
            <mixed-citation id="d36e1287" publication-type="other">
Sommer U (1993) Phytoplankton competition in Plußsee: a filed
test of the resource-ratio hypothesis. Limnol Oceanogr
38:838-845</mixed-citation>
         </ref>
         <ref id="d36e1300a1310">
            <mixed-citation id="d36e1304" publication-type="other">
Sorokin Yul, Sorokin PYu, Gnes A (1996) Structure and func-
tioning of the anthropogenically transformed Comacchino
lagoonal ecosystem (Ferrara, Italy). Mar Ecol Prog Ser 133:
57-71</mixed-citation>
         </ref>
         <ref id="d36e1320a1310">
            <mixed-citation id="d36e1324" publication-type="other">
Stockner JG (1988) Phototrophic picoplankton: an overview
from marine and freshwater ecosystems. Limnol Oceanogr
33:765-775</mixed-citation>
         </ref>
         <ref id="d36e1338a1310">
            <mixed-citation id="d36e1342" publication-type="other">
Stoecker DK, Li A, Coats DW, Gustafson DE, Nannen MK
(1997) Mixotrophy in the dinoflagellate Prorocentrum
minimum. Mar Ecol Prog Ser 152:1-12</mixed-citation>
         </ref>
         <ref id="d36e1355a1310">
            <mixed-citation id="d36e1359" publication-type="other">
Stoecker DK, Putt M, Davis LH, Michaels AE (1991) Photo-
syntheis in Mesodinium rubrum: species-specific mea-
surements and comparison to communiy rates. Mar Ecol
Prog Ser 73:245-252</mixed-citation>
         </ref>
         <ref id="d36e1375a1310">
            <mixed-citation id="d36e1379" publication-type="other">
Stoecker DK, Silver MW (1990) Replacement and aging of
chloroplasts in Strombidium capitatum (Ciliophora: Oligo-
trichida). Mar Biol 107:491-502</mixed-citation>
         </ref>
         <ref id="d36e1392a1310">
            <mixed-citation id="d36e1396" publication-type="other">
Strickland JDH, Parsons TR (1972) A practical handbook for
seawater analysis, 2nd edn. Bull Fish Res Bd Can 167</mixed-citation>
         </ref>
         <ref id="d36e1406a1310">
            <mixed-citation id="d36e1410" publication-type="other">
Strom SL, Strom MW (1996) Microplankton growth, grazing,
and community structure in the northern Gulf of Mexico.
Mar Ecol Prog Ser 130:229-240</mixed-citation>
         </ref>
         <ref id="d36e1423a1310">
            <mixed-citation id="d36e1427" publication-type="other">
Suttle CA (1994) The significance of viruses to mortality in
aquatic microbial communities. Microb Ecol 28:237-244</mixed-citation>
         </ref>
         <ref id="d36e1438a1310">
            <mixed-citation id="d36e1442" publication-type="other">
Suttle CA, Chan AM (1994) Dynamics and distribution of
cyanophages and their effects on marine Synechococcus
spp. Appl Environ Microbiol 60:3167-3174</mixed-citation>
         </ref>
         <ref id="d36e1455a1310">
            <mixed-citation id="d36e1459" publication-type="other">
Suzuki M, Sherr EB, Sherr BF (1996) Estimation of ammonium
regeneration efficiencies associated with bacterivory in
pelagic food webs via a 15N tracer method. J Plankton Res
18:411-428</mixed-citation>
         </ref>
         <ref id="d36e1475a1310">
            <mixed-citation id="d36e1479" publication-type="other">
SYSTAT for Windows: Statistics (1992) SYSTAT, Inc, Evans-
ton</mixed-citation>
         </ref>
         <ref id="d36e1489a1310">
            <mixed-citation id="d36e1493" publication-type="other">
Szmant AM, Forrester A (1996) Water column and sediment
nitrogen and phosphorus distribution patterns in the
Florida Keys, USA. Coral Reefs 15:21-24</mixed-citation>
         </ref>
         <ref id="d36e1506a1310">
            <mixed-citation id="d36e1510" publication-type="other">
Taylor AH, Geider RJ, Gilbert FJH (1997) Seasonal and latitu-
dinal dependencies of phytoplankton carbon-to-chloro-
phyll a ratios: results of a modelinq study. Mar Ecol Proq
Ser 152:51-66</mixed-citation>
         </ref>
         <ref id="d36e1526a1310">
            <mixed-citation id="d36e1530" publication-type="other">
Thayer GW, Murphey PL, Lacroix MW (1994) Responses of
plant communities in western Florida Bay to the die-off of
seagrasses. Bull Mar Sei 54:718-726</mixed-citation>
         </ref>
         <ref id="d36e1544a1310">
            <mixed-citation id="d36e1548" publication-type="other">
Tomas CR, Bendis B (1995) Phytoplankton dynamics studies
in Florida Bay. Florida Bay Science Conference. Gaines-
ville, Florida, October 1995, p 155-157</mixed-citation>
         </ref>
         <ref id="d36e1561a1310">
            <mixed-citation id="d36e1565" publication-type="other">
Verity PG, Paffenhofer GA, Wallace D, Sherr EB, Sherr BF
(1996) Composition and biomass of plankton in spring on
the Cape Hatteras shelf, with implications for carbon flux.
Cont Shelf Res 16:1087-1116</mixed-citation>
         </ref>
         <ref id="d36e1581a1310">
            <mixed-citation id="d36e1585" publication-type="other">
Wang JD, van de Kreeke J, Krishnan N, DeWitt S (1994) Wind
and tide response in Florida Bay. Bull Mar Sei 54:579-601</mixed-citation>
         </ref>
         <ref id="d36e1595a1310">
            <mixed-citation id="d36e1599" publication-type="other">
Waterbury JB, Valios FW (1993) Resistance to co-occurring
phages enables marine Synechococcus coomunities to co-
exist with cyanophages abundant in seawater. Appl Envi-
ron Microbiol 59:3393-3399</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
