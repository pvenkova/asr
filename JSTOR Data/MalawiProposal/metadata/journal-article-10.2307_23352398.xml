<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">interevieducinte</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100192</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>International Review of Education / Internationale Zeitschrift für Erziehungswissenschaft / Revue Internationale de l'Education</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00208566</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15730638</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23352398</article-id>
         <title-group>
            <article-title>Education reform for the expansion of mother-tongue education in Ghana</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kristin</given-names>
                  <surname>Rosekrans</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Arieh</given-names>
                  <surname>Sherris</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Marie</given-names>
                  <surname>Chatry-Komarek</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">58</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23352395</issue-id>
         <fpage>593</fpage>
         <lpage>618</lpage>
         <permissions>
            <copyright-statement>© 2013 Springer and UNESCO Institute for Lifelong Learning</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23352398"/>
         <abstract>
            <p>In 1957 Ghana was the first sub-Saharan colonial nation-state to achieve independence from British rule. The language of literacy instruction, however, remained English throughout most of Ghana's independence, effectively thwarting reading and writing in 11 major and 67 minor indigenous languages in use today. After years of policy shifts, including the intermittent use of mother tongue in early childhood schooling to facilitate English language and literacy instruction, prospects for a bold move towards multilingual education have emerged from a coalescence of forces inside and outside of Ghanaian education policy circles. This article discusses how the inertia of a dated language policy and a historic disregard for Ghana's multilingual landscape by the country's own policy makers are being overcome, at least partially, by progressive powers of change, albeit not without challenge. It undertakes an analysis of how a policy environment that supports bilingual education was created in order to implement a comprehensive and innovative multilingual programme, the National Literacy Acceleration Program (NALAP), which was rolled out across the nation's schools in early 2010. Having been involved in the process of designing NALAP, the authors describe the development of standards of learning and materials, as well as innovative aspects of a constructivist teacher education approach. The paper concludes with recommendations for further research, including combining a change process for key stakeholders and randomised language and literacy assessment with social marketing research in a unified approach. Réforme éducative au Ghana : développer l'enseignement en langue maternelle — En 1957, le Ghana était le premier État-nation colonial d'Afrique subsaharienne à obtenir son indépendance de l'autorité britannique. L'anglais est cependant resté la langue d'alphabétisation pendant la majeure partie de ces années d'indépendance, entravant ainsi de fait la lecture et l'écriture dans les langues autochtones en usage aujourd'hui, qui sont au nombre de 11 langues principales et de 67 secondaires. Les politiques éducatives ont été remaniées pendant des années, préconisant l'usage sporadique de la langue maternelle dans l'éducation de la petite enfance, en vue de faciliter l'enseignement de l'anglais et l'alphabétisation. Entre-temps, s'ouvrent les perspectives d'une évolution audacieuse en faveur de l'enseignement multilingue, suite à une action concertée de forces concernées directement ou indirectement par la conception des politiques éducatives. Cet article montre que l'inertie d'une politique linguistique dépassée et la négligence historique du paysage multilingue du Ghana par ses propres concepteurs de politiques sont actuellement surmontées, du moins partiellement, par les forces progressives du changement, bien que certains défis demeurent. Les auteurs analysent la création d'un cadre de politiques favorable à l'enseignement bilingue, en vue d'appliquer un programme multilingue global et innovant, le programme national d'accélération de l'alphabétisation (National Literacy Acceleration Program, NALAP), déployé dans les écoles du pays au début des années 2010. Ayant participé à la conception de ce programme, les auteurs décrivent la procédure d'élaboration de normes pour l'enseignement et les matériels, ainsi que les aspects innovants de l'approche constructiviste de la formation des enseignants. Ils concluent en recommandant une étude de recherche complémentaire, qui entre autres associerait au moyen d'une approche unifiée un processus de changement chez les parties prenantes concernées ainsi qu'une évaluation randomisée des connaissances de base et linguistiques, à une étude sur le marketing social. Eine Bildungsreform zur Ausweitung des muttersprachlichen Unterrichts in Ghana — 1957 erlangte Ghana als erster Nationalstaat südlich der Sahara die Unabhängigkeit von der britischen Kolonialherrschaft. Auch in der Zeit der Unabhängigkeit blieb die Unterrichtssprache jedoch meist Englisch, was die Alphabetisierung in den heute verwendeten 11 größeren und 67 kleineren indigenen Sprachen stark behinderte. Nach Jahren eines bildungspolitischen Schlingerkurses, mit gelegentlichen Ansätzen für eine muttersprachliche Früherziehung zur Erleichterung der nachfolgenden Alphabetisierung in der englischen Sprache, sind nun Anzeichen einer entschiedenen Hinwendung zu einem mehrsprachigen Bildungssystem zu erkennen, die von Akteuren innerhalb wie außerhalb der ghanaischen Bildungspolitik getragen wird. Dieser Beitrag behandelt das zumindest in Teilen erfolgreiche jüngere Bemühen fortschrittlicher Kräfte, die Trägheit einer überkommenen Sprachenpolitik und die fortgesetzte Missachtung der Vielsprachigkeit Ghanas durch die Politiker des Landes zu überwinden. Die Autoren zeigen, wie ein positives politisches Klima für zweisprachige Bildung geschaffen werden konnte, das schließlich die Umsetzung eines Anfang 2010 in allen Schulen des Landes eingeführten umfassenden und innovativen multilingualen Programms ermöglichte, des National Literacy Acceleration Program (NALAP). Aus eigener Erfahrung bei der Mitwirkung an der Konzeption von NALAP beschreiben die Autoren die Entwicklung von Lernstandards und Materialien sowie innovative Aspekte eines konstruktivistischen Ansatzes in der Lehrerausbildung. Der Beitrag schließt mit Empfehlungen für weitere Forschung, insbesondere zur Entwicklung eines einheitlichen Ansatzes, der einen Veränderungsprozess für die Hauptbeteiligten und eine randomisierte Sprach- und Alphabetisierungsbewertung mit Sozialmarketingforschung kombiniert. Reforma educativa para la expansión de una educación en lengua materna en Ghana — En 1957, Ghana fue el primer Estado-nación colonial subsahariano que logró su independencia de la dominación británica. Sin embargo, la lengua de enseñanza en alfabetización siguió siendo el inglés durante la mayor parte de la independencia de Ghana, lo cual efectivamente impidió el aprendizaje de la lectura y escritura en las 11 principales y 67 menores lenguas indígenas que están en uso en la actualidad. Después de años de cambios en las políticas, que incluyeron de vez en cuando el uso de la lengua materna en la temprana infancia para facilitar la alfabetización y la enseñanza en y del idioma inglés, aparecieron las perspectivas de un movimiento audaz hacia una educación multilingüe a partir de una coalición de fuerzas dentro y fuera de los círculos ghaneses de políticas de la educación. Este artículo se ocupa de cómo se está superando la inercia de una política de la lengua obsoleta y del menosprecio histórico del paisaje multilingüe de Ghana, aplicada por los mismos responsables del país; al menos parcialmente, mediante fuerzas progresistas del cambio, y no sin enfrentar desafíos. Los autores analizan cómo se ha creado un entorno político que apoya la educción bilingüe, con el fin de implementar un programa multilingüe integral e innovador, el Programa Nacional Ghanés de Aceleración de la Alfabetización (NALAP, por sus siglas en inglés), lanzado para las escuelas de toda la nación a principios del 2010. Los autores, que habían sido incluidos en el proceso de diseño del programa NALAP, describen el desarrollo de los estándares del aprendizaje y de los materiales, así como los aspectos innovadores de un enfoque de educación docente constructivista. El trabajo concluye con recomendaciones para una investigación subsiguiente, incluyendo la combinación de un proceso de cambio para grupos de interés clave y la evaluación de lenguas y alfabetismo por muestras representativas mediante métodos de investigación del marketing social y con un enfoque unificado. Реформа образования с целью распространения обучения на родном языке в Гане — В 1957 году Гана стала первым из колониальных национальных государств, расположенных к югу от Сахары, добившихся независимости от Великобритании. Однако в течение многих лет независимости страны языком обучения грамотности оставался английский, результатом чего стало разрушение навыков чтения и письма на существующих сегодня 11 основных языках и 67 языках малых народностей. Спустя годы изменений в концепциях, включая использование время от времени родного языка при обучении детей младшего возраста говорению и письму на английским языке, появилась перспектива более решительного перехода к многоязычному обучению благодаря слиянию сил внутри и извне общественных кругов, влияющих на образовательную политику Ганы. В данной статье рассматривается, как, пусть частично и не без проблем, прогрессивная сила перемен преодолевает инерцию устаревшей языковой политики и многолетнее неуважение к многоязычному ландшафту Ганы ее собственных чиновников. В статье проводится анализ того, как были разработаны концепции в поддержку двуязычного обучения с целью внедрения всесторонней прогрессивной многоязычной программы, Национальной программы акселерации грамотности (National Literacy Acceleration Program (NALАР)), которая была развернута в школах страны в начале 2010 года. Имея опыт участия в разработке NALAP, авторы описывают разработку стандартов обучения и материалов, а также новаторские аспекты конструктивистского подхода к подготовке учителей. В заключение статьи приведены рекомендации для последующих исследований, включая сочетание процесса изменений для ключевых заинтересованных лиц и оценку грамотности и знания языка с социальным маркетинговым исследованием в едином подходе.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1050e204a1310">
            <mixed-citation id="d1050e208" publication-type="other">
ADEA (Association of the Development of Education in Africa) Working Group. (1996). The role of
African languages in education and sustainable development. Association of the Development of
Education in Africa Newsletter, 8, 1-4.</mixed-citation>
         </ref>
         <ref id="d1050e221a1310">
            <mixed-citation id="d1050e225" publication-type="other">
August, D., &amp; Shanahan, T. (Eds.). (2006). Developing literacy in second-language learners: Report of
the national literacy panel on language minority children and youth. Mahwah, NJ: Lawrence
Erlbaum.</mixed-citation>
         </ref>
         <ref id="d1050e238a1310">
            <mixed-citation id="d1050e242" publication-type="other">
Chatry-Komarek, M. (1996). Tailor-made textbooks. A practical guide or the authors of textbooks for
primary schools in developing countries. Oxford: CODE Europe.</mixed-citation>
         </ref>
         <ref id="d1050e252a1310">
            <mixed-citation id="d1050e256" publication-type="other">
Dakubu, M. E. K. (2006). Ghana: Language situation. In K. Brown (Ed.), Encyclopedia of language and
linguistics (pp. 76-78). Oxford, UK: Elsevier.</mixed-citation>
         </ref>
         <ref id="d1050e267a1310">
            <mixed-citation id="d1050e271" publication-type="other">
Djité, P. G. (2008). The sociolinguistics of development in Africa. Clevedon, UK: Multilingual Matters.</mixed-citation>
         </ref>
         <ref id="d1050e278a1310">
            <mixed-citation id="d1050e282" publication-type="other">
Gal, S., &amp; Irving, J. (1995). The boundaries of languages and disciplines: How ideologies construct
difference. Social Research, 62(4), 966-1001.</mixed-citation>
         </ref>
         <ref id="d1050e292a1310">
            <mixed-citation id="d1050e296" publication-type="other">
Garcia, O. (2009). Bilingual education in the 21st century: A global perspective. West Sussex, UK:
Wiley-Blackwell.</mixed-citation>
         </ref>
         <ref id="d1050e306a1310">
            <mixed-citation id="d1050e310" publication-type="other">
Grindle, M. S., &amp; Thomas, J. W. (1991). Public choices and policy change: The political economy of
reform in developing counties. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d1050e320a1310">
            <mixed-citation id="d1050e324" publication-type="other">
Haddad. W. (1994). The dynamics of education policy making. World Bank, EDI Development Policy
Case Series. Washington, DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d1050e334a1310">
            <mixed-citation id="d1050e338" publication-type="other">
Hartwell. A., DeStefano, J., &amp; Benbow, J. (2004). The challenge of achieving Education for All: Quality
basic education for underserved children (EQUIP2 Issues Brief). Washington, DC: Education
Quality Improvement Program 2 (EQUIP2), Academy for Educational Development (AED).</mixed-citation>
         </ref>
         <ref id="d1050e352a1310">
            <mixed-citation id="d1050e356" publication-type="other">
Hornberger. N. H. (2008). Voices and biliteracy in indigenous language revitalization. In K. A. King,
et al. (Eds.), Sustaining linguistic diversity (pp. 95-109). Washington, DC: Georgetown University.</mixed-citation>
         </ref>
         <ref id="d1050e366a1310">
            <mixed-citation id="d1050e370" publication-type="other">
Hornberger, N. H. (2009). Multilingual education policy and practice: Ten certainties (grounded in
Indigenous experience. Language Teaching, 42(2), 197-211.</mixed-citation>
         </ref>
         <ref id="d1050e380a1310">
            <mixed-citation id="d1050e384" publication-type="other">
Kaplan, R., &amp; Baldauf, R. (1999). Language planning in Malawi, Mozambique and the Philippines.
Clevedon: Multilingual Matters.</mixed-citation>
         </ref>
         <ref id="d1050e394a1310">
            <mixed-citation id="d1050e398" publication-type="other">
Kegan, R.. &amp; Lahey, L. (2009). Immunity to change: How to overcome it and unlock the potential in
yourself and your organization. Cambridge, MA: Harvard Business Press.</mixed-citation>
         </ref>
         <ref id="d1050e408a1310">
            <mixed-citation id="d1050e412" publication-type="other">
Lewis, P. (Ed.) (2009). Ethnologue: Languages of the world, Sixteenth edition. Dallas, TX: SIL
International. Accessed 21, 2012 from http://www.ethnologue.com/.</mixed-citation>
         </ref>
         <ref id="d1050e422a1310">
            <mixed-citation id="d1050e426" publication-type="other">
Lipson, M., &amp; Wixon, K. (2004). Evaluation of the BTL and ASTEP Programs in the Northern, Eastern
and Volta Regions of Ghana. International Reading Association report to USAID/Ghana.</mixed-citation>
         </ref>
         <ref id="d1050e437a1310">
            <mixed-citation id="d1050e441" publication-type="other">
Long, M. H. (2007). Problems in SLA. Mahwah. NJ: Lawrence Erlbaum.</mixed-citation>
         </ref>
         <ref id="d1050e448a1310">
            <mixed-citation id="d1050e452" publication-type="other">
McGroarty, M. (2008). The political matrix of linguistic ideologies. In B. Spolsky &amp; F. Huit (Eds.),
Blackwell Handbook of Educational Linguistics (pp. 98-112). Maiden, MA: Blackwell Publishers.</mixed-citation>
         </ref>
         <ref id="d1050e462a1310">
            <mixed-citation id="d1050e466" publication-type="other">
Menken. K., &amp; Garcia, O. (Eds.). (2010). Negotiating language policies in schools: Educators as policy
makers. NY: Routledge.</mixed-citation>
         </ref>
         <ref id="d1050e476a1310">
            <mixed-citation id="d1050e480" publication-type="other">
Owu-Ewie, C. (2006). The language policy of education in Ghana: A critical look at the English-only
language policy of education. In J. Mugane, et al. (Eds.), Selected proceedings of the 35th annual
conference on African linguistics (pp. 76-85). Somerville, MA: Cascadilla Proceedings Project.</mixed-citation>
         </ref>
         <ref id="d1050e493a1310">
            <mixed-citation id="d1050e497" publication-type="other">
Phillipson, R. (1992). Linguistic imperialism. Oxford, UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1050e504a1310">
            <mixed-citation id="d1050e508" publication-type="other">
Reimers, F., &amp; McGinn, N. (1997). Informed dialogue: Using education research to shape education
policy around the world. Westport, CT : Praeger.</mixed-citation>
         </ref>
         <ref id="d1050e519a1310">
            <mixed-citation id="d1050e523" publication-type="other">
Ricento, T., &amp; Hornberger, N. (1996). Unpeeling the onion: Language planning and policy and the ELT
profession. TESOL Quarterly, 30{3), 401 —427.</mixed-citation>
         </ref>
         <ref id="d1050e533a1310">
            <mixed-citation id="d1050e537" publication-type="other">
Rosekrans, K. (2006). Using participatory research and informed dialogue to influence education policy:
Lessons learned from El Salvador. Journal of Education in International Development, 2(2), 12-28.</mixed-citation>
         </ref>
         <ref id="d1050e547a1310">
            <mixed-citation id="d1050e551" publication-type="other">
Seidu, A., et al. (2008). Report on teacher capacity for local language instruction. Winneba, Ghana:
National Center for Research in Basic Education.</mixed-citation>
         </ref>
         <ref id="d1050e561a1310">
            <mixed-citation id="d1050e565" publication-type="other">
Sherris, A. (2009). Talmudic pair work: A henneneutic approach to language teacher education. Paper
presented in the symposium entitled Responding to the needs of language teachers: Reconceptu-
alizing professional development at the Sixth International Conference on language Teacher
Education, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d1050e581a1310">
            <mixed-citation id="d1050e585" publication-type="other">
Skutnabb-Kangas, T. (2000). Linguistic genocide in education - or worldwide diversity and human
rights?. Mahwah, NJ: Erlbaum.</mixed-citation>
         </ref>
         <ref id="d1050e595a1310">
            <mixed-citation id="d1050e599" publication-type="other">
Snow, C. E., Burns. S., &amp; Griffin, P. (Eds.). (1998). Preventing reading difficulties in young children.
Washington, DC: National Academy Press.</mixed-citation>
         </ref>
         <ref id="d1050e610a1310">
            <mixed-citation id="d1050e614" publication-type="other">
Spolsky, B. (2004). Language policy. Cambridge. UK: Cambridge University Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
