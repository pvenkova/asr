<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26349286</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Children’s school participation and HIV/AIDS in rural Malawi</article-title>
            <subtitle>The role of parental knowledge and perceptions</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Grant</surname>
                  <given-names>Monica J.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Population Studies Center, University of Pennsylvania, 239 McNeil Building, 3718 Locust Walk, Philadelphia, PA 19104, U.S.A.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>7</month>
            <year>2008</year>
            <string-date>JULY - DECEMBER 2008</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>12</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">19</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26349241</issue-id>
         <fpage>1603</fpage>
         <lpage>1634</lpage>
         <permissions>
            <copyright-statement>© 2008 Grant</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26349286"/>
         <abstract xml:lang="eng">
            <p>Studies of the relationship between HIV/AIDS and children’s educational attainment largely focus on the direct impacts of parental illness and death, overlooking the potential indirect impact that parental knowledge and perceptions of their HIV status may have on children’s school enrollment. Drawing on both quantitative and qualitative evidence from Malawi, this paper finds that women’s real and perceived anticipation of future health shocks has a positive impact on their children’s educational attainment. Interventions that target health uncertainty, such as HIV testing programs, may make a significant contribution to maintaining children’s educational attainment in communities affected by HIV/AIDS.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Ainsworth, M., K. Beegle, and G. Koda (2005). The impact of adult mortality and parental deaths on primary schooling in northwestern Tanzania. Journal of Development Studies 41: 412-439.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Alderman, Harold, Jere R. Behrman, Victor Lavy, and Rekha Menon (2001). Child Health and School Enrollment: A Longitudinal Analysis. Journal of Human Resources 36(1): 185-205.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Anglewicz, Philip, jimi adams, Francis Obare-Onyango, Hans-Peter Kohler, and Susan Watkins (2006) The Malawi Diffusion and Ideational Change Project 2004-06: Data Collection, Data Quality, and Analysis of Attrition. SNP Working Paper No. 12. Philadelphia: University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Anglewicz, Philip (2007). Migration, risk perception, and HIV infection in Malawi. PhD dissertation. University of Pennsylvania. Philadelphia, PA.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Ansell, Nicola and Lorraine van Blerk (2004). Children’s Migration as a Household/Family Strategy: Coping with AIDS in Lesotho and Malawi. Journal of Southern African Studies 30(3): 673-690.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Auld, M. Christopher (2003). Choices, beliefs, and infectious disease dynamics. Journal of Health Economics 22: 361-277.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Barnett, Tony and Alan Whiteside (2002). AIDS in the Twenty-First Century. New York: Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Becker, Gary S. (1981). A Treatise on the Family. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Bignami-Van Assche, Simona, Li-Wei Chao, Philip Anglewicz, David Chilongozi, and Agatha Bula (2007). The Validity of Self-Reported Likelihood of HIV Infection Among the General Population in Rural Malawi. Sexually Transmitted Infections 83:35-40.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Boozer, M. and T. Philipson (2000). The impact of public testing for human immunodeficiency virus. Journal of Human Resources 35(3): 419-446.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Case, Anne and Cally Ardington (2006). The Impact of Parental Death on School Outcomes: Longitudinal Evidence from South Africa. Demography 43(3).</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Case, Anne, Christine Paxton, and Joseph Ableidinger (2004). Orphans in Africa: Parental Death, Poverty and School Enrollment. Demography 41(3): 483-508.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Cohen, Barney and Mark R. Montgomery (1998). Introduction. In: From Birth to Death: Mortality Decline and Reproductive Change. Washington, D.C.: National Academies Press.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Coates, T.J., O.A. Grinstead, S.E. Gregorich, D.C. Heilbron, W.P. Wolf, K.H. Choi, J. Schachter, P. Scheirer, A. van der Straten, M.C. Kamenga, M.D. Sweat, I. De Zoysa, G. Dallabetta, K.R. O’Reilly, E. van Praag, D. Miller, M. Ruiz, S. Kalibala, B. Nkowane, D. Balmer, F. Kihuho, S. Moses, F. Plummer, G. Sangiwa, M. Hogan, J. Killewo, D. Mwakigile, and C. Furlonge (2000). Efficacy of voluntary HIV-1 counseling and testing in individuals and couples in Kenya, Tanzania, and Trinidad: A randomized trial. Lancet 356(9224): 103-112.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Davidson, Jean and Martin Kanyuka (1992). Girls’ participation in basic education in southern Malawi. Comparative Education Review 36(4): 446-466.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">De Lannoy, Ariane (2005). “There is no other way out”: Educational decision-making in an era of AIDS: How do HIV-positive mothers value education? Centre for Social Science Research Working Paper No. 137. University of Cape Town.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Evans, David K. and Edward Miguel (2007). Orphans and Schooling in Africa: A Longitudinal Analysis. Demography 44(1): 35-57.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Fortson, Jane (2008). Mortality Risk and Human Capital Investment: The Impact of HIV/AIDS in Sub-Saharan Africa. Working Paper. Chicago: University of Chicago.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Gerland, Patrick (2006). Effects of social interactions on individual AIDS prevention attitudes and behaviors in rural Malawi. Ph.D. dissertation, Princeton University.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Glaser, B.G. and A. Strauss (1967). The discovery of grounded theory: Strategies for qualitative research. New York: Aldine.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Grant, Monica J. and Sara Yeatman (2008). Children’s Living Arrangments and Gender Differences in Parental Support. Paper presented at the Annual Meeting of the Population Association of America, April 17-19, New Orleans, LA.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Gregson, Simon, Basia Zaba, and Susan-Catherine Hunter (2003). The Impact of HIV-1 on Fertility in sub-Saharan Africa: Causes and Consequences. UN Population Bulletin, special issue on “Completing the fertility transition.”</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Heer, DM and DO Smith (1968). Mortality Level, Desired Family Size, and Population Increase. Demography 5(1): 104-121.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Helleringer, S. and H.P. Kohler (2005). Social Networks, Risk Perceptions, and Changing Attitudes Towards HIV/AIDS: New Evidence from a Longitudinal Study Using Fixed-Effects Estimation. Population Studies 59(3): 265-282.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Hewett, Paul (2006). Malawi School Survey Preparatory Report. New York: Population Council.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Kadzamira, Esme and Pauline Rose (2003). Can free primary education meet the needs of the poor? Evidence from Malawi. International Journal of Educational Development 23(5): 501-516.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Kalemli-Ozcan, S. (2006). AIDS, Reversal of the Demographic Transition, and Economic Development: Evidence from Africa. NBER Working Paper No. W12181.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Kaler, A. (2004). AIDS-talk in Everyday Life: The Presence of HIV/AIDS in Men’s Informal Conversation in Southern Malawi. Social Science and Medicine 59: 285-297.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Kohler, H.P, J.R. Behrman, and S.C. Watkins (2007). Social Networks and HIV/AIDS Risk Perceptions. Demography 44(1): 1-33.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Lloyd, Cynthia B. and Ann K. Blanc (1996). Children’s Schooling in Sub-Saharan Africa: The Role of Fathers, Mothers and Others. Population and Development Review 22(2): 265-298.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Macro International (2005). Malawi Demographic and Health Survey 2004. Calverton, MD: Macro International.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Macro International (1993). Malawi Demographic and Health Survey 1992. Calverton, MD: Macro International.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Mishra, Vinod, Simona Bignami, Robert Greener, Martin Vaessen, Rathavuth Hong, Peter Ghys, Ties Boerma, Ari Van Assche, Shane Khan, and Shea Rutstein (2007). A study of the association of HIV infection with wealth in sub-Saharan Africa. DHS Working Paper No. 31. Calverton, MD: Macro International.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Montgomery, Mark (2000). Perceiving Mortality Decline. Population and Development Review 26(4): 795-819.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Montgomery, Mark R. and John B. Casterline (1996). Social Influence, Social Learning, and New Models of Fertlity. Population and Development Review 22(Suppl.): 151-75.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">National Research Council-Institute of Medicine (NRC-IOM) (2005). Growing Up Global: The Transition to Adulthood in Less Developed Countries. Cynthia Lloyd (ed.). Washington, DC: National Academies Press.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Poullin, Michelle (2006). The Sexual and Social Relations of Youth in Rural Malawi: Strategies for AIDS Prevention. Unpublished Ph.D. Dissertation. Department of Sociology. Boston University.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Rutenberg, Naomi, Ann E. Biddlecom, and Frederick A.D. Kaona (2000). Reproductive Decision-Making in the Context of HIV and AIDS: A Qualitative Study in Ndola, Zambia. International Family Planning Perspectives 26(3): 124-130.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Setel, Philip (1995). The effects of HIV and AIDS on fertility in East and Central Africa. Health Transition Review 5(Suppl.): 179-189.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Smith, K.P. and S.C. Watkins (2005). Perceptions of Risk and Strategies for Prevention: Responses to HIV/AIDS in Rural Malawi. Social Science and Medicine 60: 649-660.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Soares, R. (2005). Mortality Reductions, Educational Attainment, and Fertility Choice. American Economic Review 95(3): 580-601.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Thornton, Rebecca (2005). The Demand for and Impact of Learning HIV Status: Evidence from a Field Experiment. Unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Timaeus, Ian M. and Mamadou Jasseh (2004). Adult Mortality in Sub-Saharan Africa. Demography 41(4): 757-772.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">van Blerk, Lorraine and Nicola Ansell (2006). Children’s experiences of migration: moving the wake of AIDS in southern Africa. Environment and Planning D: Society and Space 24: 449-471.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Watkins, S.C. (2004). Navigating the AIDS Epidemic in Rural Malawi. Population and Development Review 30(4): 673-705.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Yamano, T., Y. Shimamura, and D. Sserunkuuma (2006). Living Arrangements and Schooling of Orphaned Children and Adolescents in Uganda. Economic Development and Cultural Change. 54: 833-856.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Yeatman, Sara (2007). Fertility in Times of Crisis: The Case of the AIDS Epidemic. Paper presented at the Population Association of America Annual Meeting, New York, NY, March 29-31, 2007.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Young, A. (2005). The gift of the dying: The tragedy of AIDS and the welfare of future African generations. Quarterly Journal of Economics 120(2): 423-466.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Zulu, Eliya M. and Gloria Chepngeno (2003). Spousal communication about the risk of contracting HIV/AIDS in rural Malawi. Demographic Research Special Collection 1 (8): 247-278.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
