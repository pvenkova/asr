<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15376591</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40308711</article-id>
         <article-id pub-id-type="pub-doi">10.1086/600043</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>HIV Testing in a High-Incidence Population: Is Antibody Testing Alone Good Enough?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Joanne D.</given-names>
                  <surname>Stekler</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Paul D.</given-names>
                  <surname>Swenson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert W.</given-names>
                  <surname>Coombs</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Joan</given-names>
                  <surname>Dragavon</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Katherine K.</given-names>
                  <surname>Thomas</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Catherine A.</given-names>
                  <surname>Brennan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Sushil G.</given-names>
                  <surname>Devare</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert W.</given-names>
                  <surname>Wood</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Matthew R.</given-names>
                  <surname>Golden</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">49</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40012552</issue-id>
         <fpage>444</fpage>
         <lpage>453</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40308711"/>
         <abstract>
            <p>Background. The Centers for Disease Control and Prevention recently recommended the expansion of human immunodeficiency virus (HIV) antibody testing. However, antibody tests have longer "window periods" after HIV acquisition than do nucleic acid amplification tests (NAATs). Methods. Public Health-Seattle &amp; King County offered HIV antibody testing to men who have sex with men (MSM) using the OraQuick Advance Rapid HIV-1/2 Antibody Test (OraQuick; OraSure Technologies) on oral fluid or finger-stick blood specimens or using a first-or second-generation enzyme immunoassay. The enzyme immunoassay was also used to confirm reactive rapid test results and to screen specimens from OraQuick-negative MSM prior to pooling for HIV NAAT. Serum specimens obtained from subsets of HIV-infected persons were retrospectively evaluated by use of other HIV tests, including a fourth-generation antigen-antibody combination assay. Results. From September 2003 through June 2008, a total of 328 (2.3%) of 14,005 specimens were HIV antibody positive, and 36 (0.3%) of 13,677 antibody-negative specimens were NAAT positive (indicating acute HIV infection). Among 6811 specimens obtained from MSM who were initially screened by rapid testing, OraQuick detected only 153 (91%) of 169 antibody-positive MSM and 80% of the 192 HIV-infected MSM detected by the HIV NAAT program. HIV was detected in serum samples obtained from 15 of 16 MSM with acute HIV infection that were retrospectively tested using the antigen-antibody combination assay. Conclusions. OraQuick may be less sensitive than enzyme immunoassays during early HIV infection. NAAT should be integrated into HIV testing programs that serve populations that undergo frequent testing and that have high rates of HIV acquisition, particularly if rapid HIV antibody testing is employed. Antigen-antibody combination assays may be a reasonably sensitive alternative to HIV NAAT.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d3213e289a1310">
            <label>1</label>
            <mixed-citation id="d3213e296" publication-type="other">
Branson BM, Handsfield HH, Lampe MA, Janssen RS, Taylor AW, Lyss
SB, Clark JE. Revised recommendations for HIV testing of adults,
adolescents, and pregnant women in health-care settings. MMWR Re-
comm Rep 2006; 55:1-17; quiz CE11-14.</mixed-citation>
         </ref>
         <ref id="d3213e312a1310">
            <label>2</label>
            <mixed-citation id="d3213e319" publication-type="other">
Marks G, Crepaz N, Janssen RS. Estimating sexual transmission of HIV
from persons aware and unaware that they are infected with the virus
in the USA. AIDS 2006; 20:1447-50.</mixed-citation>
         </ref>
         <ref id="d3213e332a1310">
            <label>3</label>
            <mixed-citation id="d3213e339" publication-type="other">
Wawer MJ, Gray RH, Sewankambo NK, et al. Rates of HIV-1 trans-
mission per coital act, by stage of HIV-1 infection, in Rakai, Uganda.
J Infect Dis 2005; 191:1403-9.</mixed-citation>
         </ref>
         <ref id="d3213e352a1310">
            <label>4</label>
            <mixed-citation id="d3213e359" publication-type="other">
Stekler J, Maenza J, Stevens CE, et al. Screening for acute HIV infection:
lessons learned. Clin Infect Dis 2007; 44:459-61.</mixed-citation>
         </ref>
         <ref id="d3213e370a1310">
            <label>5</label>
            <mixed-citation id="d3213e377" publication-type="other">
Fiebig EW, Wright DJ, Rawal BD, et al. Dynamics of HIV viremia and
antibody seroconversion in plasma donors: implications for diagnosis
and staging of primary HIV infection. AIDS 2003; 17:1871-9.</mixed-citation>
         </ref>
         <ref id="d3213e390a1310">
            <label>6</label>
            <mixed-citation id="d3213e397" publication-type="other">
Golden MR, Stekler J, Hughes JP, Wood RW. HIV serosorting in men
who have sex with men: is it safe? J Acquir Immune Defic Syndr 2008;49:
212-8.</mixed-citation>
         </ref>
         <ref id="d3213e410a1310">
            <label>7</label>
            <mixed-citation id="d3213e417" publication-type="other">
Golden MR, Brewer DD, Kurth A, Holmes KK, Handsfield HH. Im-
portance of sex partner HIV status in HIV risk assessment among men
who have sex with men. J Acquir Immune Defic Syndr 2004; 36:734-42.</mixed-citation>
         </ref>
         <ref id="d3213e430a1310">
            <label>8</label>
            <mixed-citation id="d3213e437" publication-type="other">
Menza TW, Hughes JP, Celum CL, Golden MR. Prediction of HIV
acquisition among men who have sex with men. Sex Transm Dis (in
press).</mixed-citation>
         </ref>
         <ref id="d3213e450a1310">
            <label>9</label>
            <mixed-citation id="d3213e457" publication-type="other">
Stekler J, Wood RW, Swenson PD, Golden M. Negative rapid HIV an-
tibody testing during early HIV infection. Ann Intern Med 2007; 147:
147-8.</mixed-citation>
         </ref>
         <ref id="d3213e470a1310">
            <label>10</label>
            <mixed-citation id="d3213e477" publication-type="other">
McKinstry LA, Goldbaum GM, Meischke HW. Telephone notification
of HIV test results: impact in King County, Washington. Sex Transm
Dis 2007; 34:796-800.</mixed-citation>
         </ref>
         <ref id="d3213e491a1310">
            <label>11</label>
            <mixed-citation id="d3213e498" publication-type="other">
Stekler J, Swenson PD, Wood RW, Handsfield HH, Golden MR. Tar-
geted screening for primary HIV infection through pooled HIV-RNA
testing in men who have sex with men. AIDS 2005; 19:1323-5.</mixed-citation>
         </ref>
         <ref id="d3213e511a1310">
            <label>12</label>
            <mixed-citation id="d3213e518" publication-type="other">
Stekler J, Swenson PD, Coombs RW, Dragavon J, Wood RW, Golden
MR. Anonymous testing and rapid testing in screening for acute HIV
infection [abstract 340]. In: Program and abstracts of the 14th Con-
ference on Retroviruses and Opportunistic Infections (Los Angeles).
Alexandria, VA: Foundation for Retrovirology and Human Health,
2007:187.</mixed-citation>
         </ref>
         <ref id="d3213e541a1310">
            <label>13</label>
            <mixed-citation id="d3213e548" publication-type="other">
Li CC, Seidel KD, Coombs RW, Frenkel LM. Detection and quanti-
fication of human immunodeficiency virus type 1 p24 antigen in dried
whole blood and plasma on filter paper stored under various condi-
tions. J Clin Microbiol 2005; 43:3901-5.</mixed-citation>
         </ref>
         <ref id="d3213e564a1310">
            <label>14</label>
            <mixed-citation id="d3213e571" publication-type="other">
Centers for Disease Control and Prevention. Interpretation and use of
the Western blot assay for serodiagnosis of human immunodeficiency
virus type 1 infections. MMWR Morb Mortal Wkly Rep 1989; 38.</mixed-citation>
         </ref>
         <ref id="d3213e584a1310">
            <label>15</label>
            <mixed-citation id="d3213e591" publication-type="other">
Spielberg F, Branson BM, Goldbaum GM, et al. Overcoming barriers
to HIV testing: preferences for new strategies among clients of a needle
exchange, a sexually transmitted disease clinic, and sex venues for men
who have sex with men. J Acquir Immune Defic Syndr 2003; 32:318-27.</mixed-citation>
         </ref>
         <ref id="d3213e607a1310">
            <label>16</label>
            <mixed-citation id="d3213e614" publication-type="other">
Hutchinson AB, Branson BM, Kim A, Farnham PG. A meta-analysis
of the effectiveness of alternative HIV counseling and testing methods
to increase knowledge of HIV status. AIDS 2006; 20:1597-1604.</mixed-citation>
         </ref>
         <ref id="d3213e628a1310">
            <label>17</label>
            <mixed-citation id="d3213e635" publication-type="other">
Spielberg F, Branson BM, Goldbaum GM, et al. Choosing HIV coun-
seling and testing strategies for outreach settings: a randomized trial.
J Acquir Immune Defic Syndr 2005; 38:348-55.</mixed-citation>
         </ref>
         <ref id="d3213e648a1310">
            <label>18</label>
            <mixed-citation id="d3213e655" publication-type="other">
Kuun E, Brashaw M, Heyns AD. Sensitivity and specificity of standard
and rapid HFV-antibody tests evaluated by seroconversion and non-
seroconversion low-titre panels. Vox Sang 1997; 72:11-5.</mixed-citation>
         </ref>
         <ref id="d3213e668a1310">
            <label>19</label>
            <mixed-citation id="d3213e675" publication-type="other">
Samdal HH, Gutigard BG, Labay D, Wiik SI, Skaug K, Skar AG. Com-
parison of the sensitivity of four rapid assays for the detection of
antibodies to HIV-1/HIV-2 during seroconversion. Clin Diagn Virol
1996; 7:55-61.</mixed-citation>
         </ref>
         <ref id="d3213e691a1310">
            <label>20</label>
            <mixed-citation id="d3213e700" publication-type="other">
Beelaert G, Vercauteren G, Fransen K, et al. Comparative evaluation
of eight commercial enzyme linked immunosorbent assays and 14 sim-
pie assays for detection of antibodies to HIV. J Virol Methods 2002;
105:197-206.</mixed-citation>
         </ref>
         <ref id="d3213e716a1310">
            <label>21</label>
            <mixed-citation id="d3213e723" publication-type="other">
Brown P, Merline JR, Levine D, Minces LR. Repeatedly false-negative
rapid HIV test results in a patient with undiagnosed advanced AIDS.
Ann Intern Med 2008; 149:71-2.</mixed-citation>
         </ref>
         <ref id="d3213e736a1310">
            <label>22</label>
            <mixed-citation id="d3213e743" publication-type="other">
Ketema F, Zink HL, Kreisel KM, Croxton T, Constantine NT. A 10-
minute, US Food and Drug Administration-approved HIV test. Expert
Rev Mol Diagn 2005; 5:135-43.</mixed-citation>
         </ref>
         <ref id="d3213e757a1310">
            <label>23</label>
            <mixed-citation id="d3213e764" publication-type="other">
Louie B, Wong E, Klausner JD, et al. Assessment of rapid test perfor-
mances for HIV antibody detection in recently infected individuals. J
Clin Microbiol 2008; 46:1494-7.</mixed-citation>
         </ref>
         <ref id="d3213e777a1310">
            <label>24</label>
            <mixed-citation id="d3213e784" publication-type="other">
Owen SM, Yang C, Spira T, et al. Alternative algorithms for human
immunodeficiency virus infection diagnosis using tests that are licensed
in the United States. J Clin Microbiol 2008; 46:1588-95.</mixed-citation>
         </ref>
         <ref id="d3213e797a1310">
            <label>25</label>
            <mixed-citation id="d3213e804" publication-type="other">
Yerly S, Vora S, Rizzardi P, et al. Acute HIV infection: impact on the
spread of HIV and transmission of drug resistance. AIDS 2001; 15:
2287-92.</mixed-citation>
         </ref>
         <ref id="d3213e817a1310">
            <label>26</label>
            <mixed-citation id="d3213e824" publication-type="other">
Pao D, Fisher M, Hue S, et al. Transmission of HIV-1 during primary
infection: relationship to sexual risk and sexually transmitted infections.
AIDS 2005; 19:85-90.</mixed-citation>
         </ref>
         <ref id="d3213e837a1310">
            <label>27</label>
            <mixed-citation id="d3213e844" publication-type="other">
Brenner BG, Roger M, Routy JP, et al. High rates of forward trans-
mission events after acute/early HIV-1 infection. J Infect Dis 2007; 195:
951-9.</mixed-citation>
         </ref>
         <ref id="d3213e857a1310">
            <label>28</label>
            <mixed-citation id="d3213e864" publication-type="other">
Jacquez JA, Koopman JS, Simon CP, Longini IM Jr. Role of the primary
infection in epidemics of HIV infection in gay cohorts. J Acquir Im-
mune Defic Syndr 1994; 7:1169-84.</mixed-citation>
         </ref>
         <ref id="d3213e878a1310">
            <label>29</label>
            <mixed-citation id="d3213e885" publication-type="other">
Koopman JS, Jacquez JA, Welch GW, et al. The role of early HIV
infection in the spread of HIV through populations. J Acquir Immune
Defic Syndr Hum Retrovirol 1997; 14:249-58.</mixed-citation>
         </ref>
         <ref id="d3213e898a1310">
            <label>30</label>
            <mixed-citation id="d3213e905" publication-type="other">
Xiridou M, Geskus R, de Wit J, Coutinho R, Kretzschmar M. Primary
HIV infection as source of HIV transmission within steady and casual
partnerships among homosexual men. AIDS 2004; 18:1311-20.</mixed-citation>
         </ref>
         <ref id="d3213e918a1310">
            <label>31</label>
            <mixed-citation id="d3213e927" publication-type="other">
Hollingsworth D, Anderson R, Fraser C. Has the role of primary in-
fection in the epidemiology of HIV been overstated [abstract 913]? In:
Program and abstracts of the 13th Conference on Retroviruses and
Opportunistic Infections (Denver). Alexandria, VA: Foundation for
Retrovirology and Human Health, 2006:386.</mixed-citation>
         </ref>
         <ref id="d3213e946a1310">
            <label>32</label>
            <mixed-citation id="d3213e953" publication-type="other">
Pilcher CD, Fiscus SA, Nguyen TQ, et al. Detection of acute infections
during HIV testing in North Carolina. N Engl J Med 2005; 352:1873-83.</mixed-citation>
         </ref>
         <ref id="d3213e963a1310">
            <label>33</label>
            <mixed-citation id="d3213e970" publication-type="other">
Patel P, Klausner JD, Bacon OM, et al. Detection of acute HIV infec-
tions in high-risk patients in California. J Acquir Immune Defic Syndr
2006; 42:75-9.</mixed-citation>
         </ref>
         <ref id="d3213e983a1310">
            <label>34</label>
            <mixed-citation id="d3213e990" publication-type="other">
Priddy FH, Pilcher CD, Moore RH, et al. Detection of acute HIV
infections in an urban HIV counseling and testing population in the
United States. J Acquir Immune Defic Syndr 2006; 43(Suppl 1):S41-7.</mixed-citation>
         </ref>
         <ref id="d3213e1004a1310">
            <label>35</label>
            <mixed-citation id="d3213e1011" publication-type="other">
Pilcher CD, Price MA, Hoffman IF, et al. Frequent detection of acute
primary HIV infection in men in Malawi. AIDS 2004; 18:517-24.</mixed-citation>
         </ref>
         <ref id="d3213e1021a1310">
            <label>36</label>
            <mixed-citation id="d3213e1028" publication-type="other">
Vickstrom R, Hackett J, Swanson P, et al. Detection of acute and chronic
HIV infections by an HIV antigen/antibody combination assay [abstract
36]. In: Program and abstracts of the 2007 HIV Diagnostics Conference
(Atlanta). Centers for Disease Control and Prevention, Department of
Health and Human Services, and Association of Public Health Labora-
tories, 2007.</mixed-citation>
         </ref>
         <ref id="d3213e1051a1310">
            <label>37</label>
            <mixed-citation id="d3213e1058" publication-type="other">
Daar ES, Little S, Pitt J, Santangelo J, et al. Diagnosis of primary HIV-
1 infection. Los Angeles County Primary HIV Infection Recruitment
Network. Ann Intern Med 2001; 134:25-29.</mixed-citation>
         </ref>
         <ref id="d3213e1071a1310">
            <label>38</label>
            <mixed-citation id="d3213e1078" publication-type="other">
Hecht FM, Busch MP, Rawal B, et al. Use of laboratory tests and clinical
symptoms for identification of primary HIV infection. AIDS 2002; 16:
1119-29.</mixed-citation>
         </ref>
         <ref id="d3213e1091a1310">
            <label>39</label>
            <mixed-citation id="d3213e1098" publication-type="other">
Cunningham P, McNally L, Finlayson R, et al. Enhanced laboratory
surveillance of acute HIV infection in Sydney Australia [oral presen-
tation 22]. In: Proceedings of the 2007 HIV Diagnostics Conference
(Atlanta). Centers for Disease Control and Prevention, Department of
Health and Human Services, and Association of Public Health Lab-
oratories, 2007.</mixed-citation>
         </ref>
         <ref id="d3213e1121a1310">
            <label>40</label>
            <mixed-citation id="d3213e1128" publication-type="other">
Coco A, Kleinhans E. Prevalence of primary HIV infection in symp-
tomatic ambulatory patients. Ann Fam Med 2005; 3:400-4.</mixed-citation>
         </ref>
         <ref id="d3213e1139a1310">
            <label>41</label>
            <mixed-citation id="d3213e1146" publication-type="other">
Schacker T, Collier AC, Hughes J, Shea T, Corey L. Clinical and epide-
miologic features of primary HIV infection. Ann Intern Med 1996; 125:
257-64.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
