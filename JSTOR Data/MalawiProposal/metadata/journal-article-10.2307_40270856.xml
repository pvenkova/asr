<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">publchoi</journal-id>
         <journal-id journal-id-type="jstor">j50000024</journal-id>
         <journal-title-group>
            <journal-title>Public Choice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn pub-type="ppub">00485829</issn>
         <issn pub-type="epub">15737101</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40270856</article-id>
         <article-id pub-id-type="pub-doi">10.1007/s11127-008-9318-6</article-id>
         <title-group>
            <article-title>The Political Economy of IMF Forecasts</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Axel</given-names>
                  <surname>Dreher</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Silvia</given-names>
                  <surname>Marchesi</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>James Raymond</given-names>
                  <surname>Vreeland</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">137</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id>i40010640</issue-id>
         <fpage>145</fpage>
         <lpage>171</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Springer</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/40270856"/>
         <abstract>
            <p>We investigate the political economy of IMF forecasts with data for 157 countries (1999-2005). Generally, we find evidence of forecast bias in growth and inflation. Specifically, we find that countries voting with the United States in the UN General Assembly receive lower inflation forecasts as domestic elections approach. Countries with large loans outstanding from the IMF also receive lower inflation forecasts, suggesting that the IMF engages in "defensive forecasting." Finally, countries with fixed exchange rate regimes receive lower inflation forecasts, suggesting the IMF desires to preserve stability as inflation can have detrimental effects under such an exchange rate regime.</p>
         </abstract>
         <kwd-group>
            <kwd>IMF</kwd>
            <kwd>Economic forecasts</kwd>
            <kwd>Political influence</kwd>
            <kwd>C23</kwd>
            <kwd>D72</kwd>
            <kwd>F33</kwd>
            <kwd>F34</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d248e186a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d248e193" publication-type="other">
IMF (1998)</mixed-citation>
            </p>
         </fn>
         <fn id="d248e200a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d248e207" publication-type="other">
Stein wand and Stone (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d248e214a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d248e221" publication-type="other">
Fratianni and Pattison (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e227" publication-type="other">
Gisselquist (1981),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e233" publication-type="other">
Loxley (1986)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e240" publication-type="other">
Andersen
et al. (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d248e250a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d248e257" publication-type="other">
Broz and Hawes (2006),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e263" publication-type="other">
Faini and Grilli (2004),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e269" publication-type="other">
Rieffel (2003),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e276" publication-type="other">
Woods (2003).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e282" publication-type="other">
Frey et al. (1985),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e288" publication-type="other">
Frey and Schneider (1986),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e294" publication-type="other">
Weck-Hannemann
and Schneider (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e303" publication-type="other">
Dreheret al. (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d248e311a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d248e318" publication-type="other">
Vaubel (1996, 2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e324" publication-type="other">
Vreeland (2003, 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d248e331a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d248e338" publication-type="other">
Crawford and Sobel (1982)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e344" publication-type="other">
Ottaviani and SØrensen (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d248e351a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d248e358" publication-type="other">
Marchesi and Sabani (2007b),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e364" publication-type="other">
Przeworski
and Vreeland (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e373" publication-type="other">
Vreeland (2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d248e380a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d248e387" publication-type="other">
Reinhart et al. 2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d248e394a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d248e401" publication-type="other">
Dreher (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e407" publication-type="other">
Vreeland (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d248e414a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d248e421" publication-type="other">
Brambor et al. (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e427" publication-type="other">
Clark et al.
(2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d248e438a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d248e445" publication-type="other">
http://www.imf.org/external/np/exr/ib/2006/041806.htm (accessed
9 September 2007).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d248e454" publication-type="other">
Vreeland (2006)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d248e470a1310">
            <mixed-citation id="d248e474" publication-type="other">
Aldenhoff, F. O. (2007). Are economic forecasts of the International Monetary Fund politi-
cally biased? A public choice analysis. Review of International Organizations, 2(3), 239-260.
doi:10.1007/s11558-006-9010-x.</mixed-citation>
         </ref>
         <ref id="d248e487a1310">
            <mixed-citation id="d248e491" publication-type="other">
Andersen, T. B., Hansen, H., &amp; Markussen, T. (2006). US politics and World Bank IDA-lending. Journal of
Development Studies, 42(5), 772-794. doi: 10.1080/00220380600741946.</mixed-citation>
         </ref>
         <ref id="d248e501a1310">
            <mixed-citation id="d248e505" publication-type="other">
Arellano, M., &amp; Bond, S. (1991). Some tests of specification for panel data: Monte Carlo evidence and an
application to employment equations. Review of Economic Studies, 58, 277-297. doi: 10.2307/2297968.</mixed-citation>
         </ref>
         <ref id="d248e515a1310">
            <mixed-citation id="d248e519" publication-type="other">
Arellano, M., &amp; Bover, O. (1995). Another look at the instrumental variable estimation of error-components
models. Journal of Econometrics, 68(1), 29-51. doi: 10.1016/0304-4076(94)01642-D.</mixed-citation>
         </ref>
         <ref id="d248e530a1310">
            <mixed-citation id="d248e534" publication-type="other">
Artis, M. J. ( 1 988). How accurate is the world economic outlook? A post mortem on short-term forecasting at
the international Monetary Fund. Staff studies for the world economic outlook, International Monetary
Fund, Washington, 1-9.</mixed-citation>
         </ref>
         <ref id="d248e547a1310">
            <mixed-citation id="d248e551" publication-type="other">
Artis, M. J. (1997). How accurate are the WEO's short-term forecasts? An examination of the world économie-
outlook. Staff studies for the world economic outlook, International Monetary Fund, Washington.</mixed-citation>
         </ref>
         <ref id="d248e561a1310">
            <mixed-citation id="d248e565" publication-type="other">
Barro, R. J., &amp; Lee, J. W. (2005). IMF-programs: Who is chosen and what are the effects? Journal of Monetary
Economics, 52, 1245-1269. doi:10.1016/j.jmoneco.2005.04.003.</mixed-citation>
         </ref>
         <ref id="d248e575a1310">
            <mixed-citation id="d248e579" publication-type="other">
Barrionuevo, J. M. (1993). How accurate are the world economic outlook projections! Staff studies for the
world economic outlook, International Monetary Fund, Washington, pp. 28-46.</mixed-citation>
         </ref>
         <ref id="d248e589a1310">
            <mixed-citation id="d248e593" publication-type="other">
Batchelor, R. (2000). The IMF and OECD versus consensus forecasts. City University Business School,
London, August 2000.</mixed-citation>
         </ref>
         <ref id="d248e603a1310">
            <mixed-citation id="d248e607" publication-type="other">
Beach, W. W, Schavey, A. B., &amp; Isidro, I. M. (1999). How reliable are IMF economic forecasts? Heritage
Foundation CDA 99-05.</mixed-citation>
         </ref>
         <ref id="d248e618a1310">
            <mixed-citation id="d248e622" publication-type="other">
Beck, T., Clarke, G., Groff, A., Keefer, P., &amp; Walsh, P. (1999). New tools and new tests in comparative
political economy. The Database of Political Institutions, Development Research Group, The World
Bank, Groff: Federal Department of Foreign Affairs (Switzerland).</mixed-citation>
         </ref>
         <ref id="d248e635a1310">
            <mixed-citation id="d248e639" publication-type="other">
Bird, G., &amp; Rowlands, D. (2003). Political economy influences within the life-cycle of IMF programmes.
World Economy, 26, 1255-1278. doi :10.1046/j.1467-9701.2003.00572.x.</mixed-citation>
         </ref>
         <ref id="d248e649a1310">
            <mixed-citation id="d248e653" publication-type="other">
Blundell, R., &amp; Bond, S. (1998). Initial conditions and moment restrictions in dynamic panel data models.
Journal of Econometrics, 87(1), 1 15-143. doi:10.1016/S0304-4076(98)00009-8.</mixed-citation>
         </ref>
         <ref id="d248e663a1310">
            <mixed-citation id="d248e667" publication-type="other">
Brambor, T., Clark, W., &amp; Golder, M. (2005). Understanding interaction models: Improving empirical analy-
ses. Political Analysis, 14, 63-82. doi:10.1093/pan/mpi014.</mixed-citation>
         </ref>
         <ref id="d248e677a1310">
            <mixed-citation id="d248e681" publication-type="other">
Broz, J. L., &amp; Hawes, M. B. (2006). US domestic politics and International Monetary Fund Policy. In
D. Hawkins, D. A. Lake, D. Nielson, &amp; M. J. Tierney (Eds.), Delegation and agency in international
organizations (pp. 77-196). Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d248e694a1310">
            <mixed-citation id="d248e698" publication-type="other">
Clark, W., Gilligan, M., &amp; Golder, M. (2006). A simple multivariate test for asymmetric hypotheses. Political
Analysis, 14, 311-331. doi:10.1093/pan/mpj018</mixed-citation>
         </ref>
         <ref id="d248e709a1310">
            <mixed-citation id="d248e713" publication-type="other">
Copelovitch, M. (2007). Master or servant? Agency slack and the politics of IMF lending. Manuscript, De-
partment of Political Science, University of Wisconsin, Madison.</mixed-citation>
         </ref>
         <ref id="d248e723a1310">
            <mixed-citation id="d248e727" publication-type="other">
Crawford, V., &amp; Sobel, J. (1982). Strategic information transmission. Econometrica, 50, 1431-1451.
doi:10.2307/1913390.</mixed-citation>
         </ref>
         <ref id="d248e737a1310">
            <mixed-citation id="d248e741" publication-type="other">
Dewatripont, M., Jewitt, I., &amp; Tirole, J. (1999). The economics of career concern. The Review of Economic
Studies, 66(1), 183-217. doi:10.1 1 1 1/1 467-937X. 00084.</mixed-citation>
         </ref>
         <ref id="d248e751a1310">
            <mixed-citation id="d248e755" publication-type="other">
Dreher, A. (2004). A public choice perspective of IMF and World Bank lending and conditionality. Public
Choice, 119(3-4), 445-464. doi: 10.1023/B:PUCH.0000033326.19804.52.</mixed-citation>
         </ref>
         <ref id="d248e765a1310">
            <mixed-citation id="d248e769" publication-type="other">
Dreher, A. (2006). IMF and economic growth: The effects of programs, loans, and compliance with condi-
tionality. World Development, 34(5), 769-788. doi:10.1016/j.worlddev.2005.11.002.</mixed-citation>
         </ref>
         <ref id="d248e779a1310">
            <mixed-citation id="d248e783" publication-type="other">
Dreher, A., &amp; Jensen, N. M. (2007). Independent actor or agent? An empirical analysis of the impact of US
interests on IMF conditions. The Journal of Law and Economics, 50( 1 ),105-124. doi:10.1086/508311 .</mixed-citation>
         </ref>
         <ref id="d248e794a1310">
            <mixed-citation id="d248e798" publication-type="other">
Dreher, A., &amp; Sturm, J.-E. (2006). Do IMF and World Bank influence voting in the UN General Assembly?
KOF working paper 137. ETH Zürich.</mixed-citation>
         </ref>
         <ref id="d248e808a1310">
            <mixed-citation id="d248e812" publication-type="other">
Dreher, A., &amp; Vaubel, R. (2007 fortcoming). Foreign exchange intervention and the political business cycle:
A panel data analysis. Journal of International Money and Finance.</mixed-citation>
         </ref>
         <ref id="d248e822a1310">
            <mixed-citation id="d248e826" publication-type="other">
Dreher, A., Sturm, J.-E., &amp; Vreeland, J. R. (2006). Does membership on the UN Security Council influence
IMF decisions? Evidence from panel data. KOF working paper 151 . ETH Zürich.</mixed-citation>
         </ref>
         <ref id="d248e836a1310">
            <mixed-citation id="d248e840" publication-type="other">
Dreher, A., Sturm, J.-E., &amp; Vreeland, J.R. (2008, in press). Development aid and international politics: Does
membership on the UN Security Council influence World Bank decisions? Journal of Development
Economics.</mixed-citation>
         </ref>
         <ref id="d248e853a1310">
            <mixed-citation id="d248e857" publication-type="other">
Faini, R., &amp; Grilli, R. (2004). Who runs the IFIsl CEPR discussion paper No. 4666.</mixed-citation>
         </ref>
         <ref id="d248e864a1310">
            <mixed-citation id="d248e868" publication-type="other">
Fratianni, M, &amp; Pattison, J. (2005). Who is running the IMF: Critical shareholders or the staff? In P. Gijsel &amp;
H. Schenk (Eds.), Multidisciplinary economics: The birth of a new economics faculty in the Netherlands
(pp. 279-292). Berlin: Springer.</mixed-citation>
         </ref>
         <ref id="d248e882a1310">
            <mixed-citation id="d248e886" publication-type="other">
Frey, B. S., &amp; Schneider, F. (1986). Competing models of international lending activity. Journal of Develop-
ment Economics, 20(2), 225-245. doi: 10. 1016/0304-3878(86)90022-2.</mixed-citation>
         </ref>
         <ref id="d248e896a1310">
            <mixed-citation id="d248e900" publication-type="other">
Frey, B. S., Schneider, F., Horn, H., &amp; Persson, T. (1985). A formulation and test of a simple model of World
Bank behavior. Weltwirtschaftliches Archiv, 727(3), 438-47. doi:10.1007/BF02708182.</mixed-citation>
         </ref>
         <ref id="d248e910a1310">
            <mixed-citation id="d248e914" publication-type="other">
Gisselquist, D. (1981). The political economy of International Bank lending. New York: Praeger.</mixed-citation>
         </ref>
         <ref id="d248e921a1310">
            <mixed-citation id="d248e925" publication-type="other">
Goldsbrough, D., Barnes, K., Mateos y Lago, I., &amp; Tsikata, T. (2002). Prolonged use of IMF loans. Finance
and Development, 39, 1-7.</mixed-citation>
         </ref>
         <ref id="d248e935a1310">
            <mixed-citation id="d248e939" publication-type="other">
Gould, E. R. (2003). Money talks: Supplemental financiers and International Monetary Fund conditionality.
International Organization, 57(3), 551-586.</mixed-citation>
         </ref>
         <ref id="d248e949a1310">
            <mixed-citation id="d248e953" publication-type="other">
Gould, E. R. (2006). Money talks: The International Monetary Fund conditionality and supplemental fi-
nanciers. Stanford: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d248e964a1310">
            <mixed-citation id="d248e968" publication-type="other">
Hacche, G. (2007). A non-definitive guide to the IMF. World Economics, 8(2), 97-118.</mixed-citation>
         </ref>
         <ref id="d248e975a1310">
            <mixed-citation id="d248e979" publication-type="other">
Holden, K., &amp; Peel, D. A. (1990). On testing for unbiasedness and efficiency of forecasts. Manchester School
of Economic and Social Studies, 58, 120-127.</mixed-citation>
         </ref>
         <ref id="d248e989a1310">
            <mixed-citation id="d248e993" publication-type="other">
IMF (1998) Documentation MULTIMOD Mark III, the core dynamic and steady-state models. IMF occa-
sional paper No. 164.</mixed-citation>
         </ref>
         <ref id="d248e1003a1310">
            <mixed-citation id="d248e1007" publication-type="other">
IMF (various years) World economic outlook: Financial systems and economic cycles. Washington DC: In-
ternational Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d248e1017a1310">
            <mixed-citation id="d248e1021" publication-type="other">
IMF (2006). International financial statistics: CD-Rom. Washington DC: International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d248e1028a1310">
            <mixed-citation id="d248e1032" publication-type="other">
Kahler, M. (1990). The United States and the International Monetary Fund: Declining influence or declining
interest? In M. P. Karns &amp; K. A. Mingst (Eds.), The United States and Multilateral Institutions (pp.
91-114). Boston: Unwin Hyman.</mixed-citation>
         </ref>
         <ref id="d248e1046a1310">
            <mixed-citation id="d248e1050" publication-type="other">
Kenen, P. B., &amp; Schwartz, B. S. (1986). The assessment of macroeconomic forecasts in the International
Monetary Fund's world economic outlook. Working papers in international economics, No. G-86-40,
Princeton University.</mixed-citation>
         </ref>
         <ref id="d248e1063a1310">
            <mixed-citation id="d248e1067" publication-type="other">
Lagerspetz, E. (1999). Rationality and politics in long-term decisions. Biodiversity and Conservation, 8, 149-
164. doi:10.1023/A:1008821427812.</mixed-citation>
         </ref>
         <ref id="d248e1077a1310">
            <mixed-citation id="d248e1081" publication-type="other">
Levy-Yeyati, E., &amp; Sturzenegger, F. (2005). Classifying exchange rate regimes: Deeds vs. words. European
Economic Review, 49(6), 603- 1635 . doi: 10.1016/j.euroecorev.2004 .01.001 .</mixed-citation>
         </ref>
         <ref id="d248e1091a1310">
            <mixed-citation id="d248e1095" publication-type="other">
Loungani, P. (2000). How accurate are private sector forecasts? Cross-country evidence from consensus
forecasts of output growth. IMF working paper 00/77. International Monetary Fund, Washington, DC.</mixed-citation>
         </ref>
         <ref id="d248e1105a1310">
            <mixed-citation id="d248e1109" publication-type="other">
Loxley, J. (1986). Debt and disorder: External financing for development. Boulder: Westview Press.</mixed-citation>
         </ref>
         <ref id="d248e1116a1310">
            <mixed-citation id="d248e1120" publication-type="other">
Marchesi, S. (2003). Adoption of an IMF programme and debt rescheduling. An empirical analysis. Journal
of Development Economics, 70(2), 403-23. doi:10.1016/S0304-3878(02)00103-7.</mixed-citation>
         </ref>
         <ref id="d248e1131a1310">
            <mixed-citation id="d248e1135" publication-type="other">
Marchesi, S., &amp; Sabani, L. (2007a). Prolonged use and conditionality failure: Investigating the IMF responsi-
bility. In G. Mavrotas &amp; A. Shorrocks (Eds.), Advancing development: Core themes in global economics
(pp. 319-332). New York: Palgrave-Macmillan.</mixed-citation>
         </ref>
         <ref id="d248e1148a1310">
            <mixed-citation id="d248e1152" publication-type="other">
Marchesi, S., &amp; Sabani, L. (2007b). IMF concern for reputation and conditional lending failure: Theory and
empirics. Journal of Development Economics, 84, 640-666. doi: 10.1016/j.jdeveco.2007.01.001 .</mixed-citation>
         </ref>
         <ref id="d248e1162a1310">
            <mixed-citation id="d248e1166" publication-type="other">
Marchesi, S., &amp; Thomas, J. (1999). IMF conditionality as a screening device. The Economic Journal, 109,
111-125. doi:10.1111/1468-0297.00420.</mixed-citation>
         </ref>
         <ref id="d248e1176a1310">
            <mixed-citation id="d248e1180" publication-type="other">
Morris, S., &amp; Shin, H. S. (2006). Catalytic finance: When does it work? Journal of International Economics,
70, 161-177. doi:10.1016/j.jinteco.2005.06.014</mixed-citation>
         </ref>
         <ref id="d248e1190a1310">
            <mixed-citation id="d248e1194" publication-type="other">
Nickell, S. J. (1981). Biases in dynamic models with fixed effects. Econometrica, 49, 802-816.
doi:10.2307/1911408.</mixed-citation>
         </ref>
         <ref id="d248e1204a1310">
            <mixed-citation id="d248e1208" publication-type="other">
Oatley, T., &amp; Yackee, J. (2004). American interests and IMF lending. International Politics, 41(3), 415-429.
doi: 10.1057/palgrave.ip.8800085.</mixed-citation>
         </ref>
         <ref id="d248e1219a1310">
            <mixed-citation id="d248e1223" publication-type="other">
Ottaviani, M., &amp; S0rensen, P. N. (2006). The strategy of professional forecasting. Journal of Financial Eco-
nomics, 81, 441-466. doi:10.1016/j.jfineco.2005.08.002.</mixed-citation>
         </ref>
         <ref id="d248e1233a1310">
            <mixed-citation id="d248e1237" publication-type="other">
Przeworski, A., &amp; Vreeland, J. (2000). The effect of IMF programs on economic growth. Journal of Devel-
opment Economics, 62, 385-421. doi:10.1016/S0304-3878(00)00090-0.</mixed-citation>
         </ref>
         <ref id="d248e1247a1310">
            <mixed-citation id="d248e1251" publication-type="other">
Pons, J. (2000). The accuracy of IMF and OECD forecasts for G7 countries. Journal of Forecasting, 19(1),
53-63. doi:10.1002/(SICI)1099-131X(200001)19:l&lt;53::AID-FOR736&gt;3.0.CO;2-J.</mixed-citation>
         </ref>
         <ref id="d248e1261a1310">
            <mixed-citation id="d248e1265" publication-type="other">
Ramcharan, R. (2003). Reputation, debt and policy conditionality. IMF working paper No. 192.</mixed-citation>
         </ref>
         <ref id="d248e1272a1310">
            <mixed-citation id="d248e1276" publication-type="other">
Ramcharan, R. (2001). Just say no! {More often) IMF lending and policy reform. Mimeo.</mixed-citation>
         </ref>
         <ref id="d248e1283a1310">
            <mixed-citation id="d248e1287" publication-type="other">
Reinhart, C. M, Rogoff, K. S., &amp; Savastano, M. A. (2003). Debt intolerance. Brookings Papers on Economic
Activity, 2003(1), 1-74.</mixed-citation>
         </ref>
         <ref id="d248e1298a1310">
            <mixed-citation id="d248e1302" publication-type="other">
Rieffel, L. (2003). Restructuring sovereign debt: The case for ad-hoc machinery. Washington: Brookings
Institution Press.</mixed-citation>
         </ref>
         <ref id="d248e1312a1310">
            <mixed-citation id="d248e1316" publication-type="other">
Roodman, D. (2005). xtabondl: Stata module to extend xtabond dynamic panel data estimator. Center for
Global Development, Washington, DC. http://ideas.repec.Org/c/boc/bocode/s435901.html. Accessed 15
March 2007.</mixed-citation>
         </ref>
         <ref id="d248e1329a1310">
            <mixed-citation id="d248e1333" publication-type="other">
Russett, B. M. (1967). International regions and the international system. Chicago: Rand McNally.</mixed-citation>
         </ref>
         <ref id="d248e1340a1310">
            <mixed-citation id="d248e1344" publication-type="other">
Steinwand, M., &amp; Stone, R. W. (2008). The International Monetary Fund: A review of the recent evidence.
Review of International Organizations. doi:10.1007/sl 1558-007-9026-x.</mixed-citation>
         </ref>
         <ref id="d248e1354a1310">
            <mixed-citation id="d248e1358" publication-type="other">
Stone, R. W. (2002). Lending credibility: The International Monetary Fund and the post-communist transi-
tion. Princeton: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d248e1368a1310">
            <mixed-citation id="d248e1372" publication-type="other">
Stone, R. W. (2004). The political economy of IMF lending in Africa. American Political Science Review,
98(4), 577-592.</mixed-citation>
         </ref>
         <ref id="d248e1383a1310">
            <mixed-citation id="d248e1387" publication-type="other">
Thacker, S. C. (1999). The high politics of IMF lending. World Politics, 52, 38-75.</mixed-citation>
         </ref>
         <ref id="d248e1394a1310">
            <mixed-citation id="d248e1398" publication-type="other">
Timmermann, A. (2007). An evaluation of the world economic outlook forecasts. IMF Staff Papers, 54(1),
1-33. doi:10.1057/palgrave.imfsp.9450007.</mixed-citation>
         </ref>
         <ref id="d248e1408a1310">
            <mixed-citation id="d248e1412" publication-type="other">
Vaubel, R. (1986). A public choice approach to international organizations. Public Choice, 51, 39-57.
doi:10.1007/BF00141684.</mixed-citation>
         </ref>
         <ref id="d248e1422a1310">
            <mixed-citation id="d248e1426" publication-type="other">
Vaubel, R. (1991). Problems at IMF. Swiss Review of World Affairs, 40, 20-22.</mixed-citation>
         </ref>
         <ref id="d248e1433a1310">
            <mixed-citation id="d248e1437" publication-type="other">
Vaubel, R. (1996). Bureaucracy at the IMF and the World Bank: A comparison of the evidence. World Econ-
omy, 19, 195-210. doi:10.1111/j.1467-9701.1996.tb00672.x.</mixed-citation>
         </ref>
         <ref id="d248e1447a1310">
            <mixed-citation id="d248e1451" publication-type="other">
Vaubel, R. (2006). Principal-agent problems in international organizations. Review of International Organi-
zations, 1(2), 125-138.</mixed-citation>
         </ref>
         <ref id="d248e1462a1310">
            <mixed-citation id="d248e1466" publication-type="other">
Voeten, E. (2004). Documenting votes in the UN General Assembly. Political Science and International Af-
fairs, The George Washington University.</mixed-citation>
         </ref>
         <ref id="d248e1476a1310">
            <mixed-citation id="d248e1480" publication-type="other">
Vreeland, J. R. (2003). The IMF and economic development. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d248e1487a1310">
            <mixed-citation id="d248e1491" publication-type="other">
Vreeland, J. R. (2005). The international and domestic politics of IMF programs. Mimeo, Yale University.</mixed-citation>
         </ref>
         <ref id="d248e1498a1310">
            <mixed-citation id="d248e1502" publication-type="other">
Vreeland, J. R. (2006). Self reform: The IMF strategy. Prepared for the reinventing Bretton Woods Committee
and world economic forum conference, Cape Town, 29-30 May 2006.</mixed-citation>
         </ref>
         <ref id="d248e1512a1310">
            <mixed-citation id="d248e1516" publication-type="other">
Vreeland, J. R. (2007). The International Monetary Fund: Politics of conditional lending. New York: Rout-
ledge.</mixed-citation>
         </ref>
         <ref id="d248e1526a1310">
            <mixed-citation id="d248e1530" publication-type="other">
Weck-Hannemann, H., &amp; Schneider, F. (1991). Determinants of foreign aid under alternative institutional
arrangements. In R. Vaubel &amp; T. D. Willett (Eds.), The political economy of international organizations:
A public choice approach (pp. 245-266). Boulder: Westview Press.</mixed-citation>
         </ref>
         <ref id="d248e1544a1310">
            <mixed-citation id="d248e1548" publication-type="other">
Willett, T. D. (2000). A soft core public choice analysis of the International Monetary Fund., Claremont
Colleges working paper, 2000-56.</mixed-citation>
         </ref>
         <ref id="d248e1558a1310">
            <mixed-citation id="d248e1562" publication-type="other">
Windmeijer, F (2005). A finite sample correction for the variance of linear efficient two-step GMM estima-
tors. Journal of Econometrics, 126(1), 25-51. doi:10.1016/j.jeconom.2004.02.005.</mixed-citation>
         </ref>
         <ref id="d248e1572a1310">
            <mixed-citation id="d248e1576" publication-type="other">
Wittkopf, E. (1973). Foreign aid and United Nations votes: A comparative study. American Political Science
Review, 67(3), 868-888. doi:10.2307/1958630.</mixed-citation>
         </ref>
         <ref id="d248e1586a1310">
            <mixed-citation id="d248e1590" publication-type="other">
Woods, N. (2003). The United States and the international financial institutions: Power and influence within
the World Bank and the IMF. In R. Foot, N. McFarlane, &amp; M. Mastanduno (Eds.), US hegemony and
international organizations (pp. 92-114). Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d248e1603a1310">
            <mixed-citation id="d248e1607" publication-type="other">
World Bank (2006a). World development indicators. Washington: CD-Rom.</mixed-citation>
         </ref>
         <ref id="d248e1614a1310">
            <mixed-citation id="d248e1618" publication-type="other">
World Bank (2006b). Global development finance. Washington: CD-Rom.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
