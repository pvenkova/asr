<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30086104</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Articles and Brief Reports</subject>
               <subj-group>
                  <subject>Viruses</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Parvovirus B19 Infection Contributes to Severe Anemia in Young Children in Papua New Guinea</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>James</given-names>
                  <surname>Wildig</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Pascal</given-names>
                  <surname>Michon</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Peter</given-names>
                  <surname>Siba</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Mata</given-names>
                  <surname>Mellombo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Alice</given-names>
                  <surname>Ura</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Ivo</given-names>
                  <surname>Mueller</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Yvonne</given-names>
                  <surname>Cossart</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>7</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">194</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i30086100</issue-id>
         <fpage>146</fpage>
         <lpage>153</lpage>
         <permissions>
            <copyright-statement>Copyright 2006 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30086104"/>
         <abstract>
            <p>Background. Severe anemia (hemoglobin level, &lt;50 g/L) is a major cause of death among young children, and it arises from multiple factors, including malaria and iron deficiency. We sought to determine whether infection with parvovirus B19 (B19), which causes the cessation of erythropoiesis for 3-7 days, might precipitate some cases of severe anemia. Methods. Archival blood samples collected in the Wosera District of Papua New Guinea, from 169 children 6 months-5 years old with severe anemia and from 169 control subjects matched for age, sex, and time were tested for B19 immunoglobulin M (IgM) by enzyme immunoassay and for B19 DNA by nested polymerase chain reaction (PCR). A total of 168 separate samples from children in the Wosera District were tested for B19 IgG. Results. A strong association between acute B19 infection (positive by both IgM and PCR) and severe anemia was found (adjusted odds ratio, 5.61 [95% confidence interval, 1.93-16.3]). The prevalence of parvovirus B19 IgG reached &gt;90% in 6-year-olds. Conclusions. B19 infections play a significant role in the etiology of severe anemia in this area of malarial endemicity. Given the high levels of morbidity and mortality associated with severe anemia in such regions, the prevention of B19 infection with a vaccine might be a highly effective public health intervention.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1186e266a1310">
            <label>1</label>
            <mixed-citation id="d1186e273" publication-type="other">
Menendez C, Kahigwa E, Hirt R, et al. Randomised placebo-controlled
trial of iron supplementation and malaria chemoprophylaxis for pre-
vention of severe anaemia and malaria in Tanzanian infants. Lancet
1997; 350:844-50.</mixed-citation>
         </ref>
         <ref id="d1186e289a1310">
            <label>2</label>
            <mixed-citation id="d1186e296" publication-type="other">
Murphy SC, Breman JG. Gaps in the childhood malaria burden in
Africa: cerebral malaria, neurological sequelae, anemia, respiratory dis-
tress, hypoglycemia, and complications of pregnancy. Am J Trop Med
Hyg 2001; 64:57-67.</mixed-citation>
         </ref>
         <ref id="d1186e312a1310">
            <label>3</label>
            <mixed-citation id="d1186e319" publication-type="other">
Newton CR, Warn PA, Winstanley PA, et al. Severe anaemia in children
living in a malaria endemic area of Kenya. Trop Med Int Health 1997;2:
165-78.</mixed-citation>
         </ref>
         <ref id="d1186e332a1310">
            <label>4</label>
            <mixed-citation id="d1186e339" publication-type="other">
Jones PH, Pickett LC, Anderson MJ, Pasvol G. Human parvovirus
infection in children and severe anaemia seen in an area endemic for
malaria. J Trop Med Hyg 1990; 93:67-70.</mixed-citation>
         </ref>
         <ref id="d1186e353a1310">
            <label>5</label>
            <mixed-citation id="d1186e360" publication-type="other">
Kelly HA, Siebert D, Hammond R, Leydon J, Kiely P, Maskill W. The
age-specific prevalence of human parvovirus immunity in Victoria,
Australia compared with other parts of the world. Epidemiol Infect
2000; 124:449-57.</mixed-citation>
         </ref>
         <ref id="d1186e376a1310">
            <label>6</label>
            <mixed-citation id="d1186e383" publication-type="other">
Serjeant GR, Topley JM, Mason K, et al. Outbreak of aplastic crises in
sickle cell anaemia associated with parvovirus-like agent. Lancet 1981;2:
595-7.</mixed-citation>
         </ref>
         <ref id="d1186e396a1310">
            <label>7</label>
            <mixed-citation id="d1186e405" publication-type="other">
Potter CG, Potter AC, Hatton CS, et al. Variation of erythroid and
myeloid precursors in the marrow and peripheral blood of volunteer
subjects infected with human parvovirus (B19). J Clin Invest 1987; 79:
1486-92.</mixed-citation>
         </ref>
         <ref id="d1186e421a1310">
            <label>8</label>
            <mixed-citation id="d1186e428" publication-type="other">
Anderson MJ, Higgins PG, Davis LR, et al. Experimental parvoviral
infection in humans. J Infect Dis 1985; 152:257-65.</mixed-citation>
         </ref>
         <ref id="d1186e438a1310">
            <label>9</label>
            <mixed-citation id="d1186e445" publication-type="other">
Kudoh T, Yoto Y, Suzuki N, et al. Human parvovirus B19-induced
aplastic crisis in iron deficiency anemia. Acta Paediatr Jpn 1994; 36:
448-9.</mixed-citation>
         </ref>
         <ref id="d1186e458a1310">
            <label>10</label>
            <mixed-citation id="d1186e465" publication-type="other">
Lortholary O, Eliaszewicz M, Dupont B, Courouce AM. Parvovirus
B 19 infection during acute Plasmodium falciparum malaria. Eur J Hae-
matol 1992; 49:219.</mixed-citation>
         </ref>
         <ref id="d1186e479a1310">
            <label>11</label>
            <mixed-citation id="d1186e486" publication-type="other">
Urganci N, Arapoglu M, Kayaalp N. Plasmodium falciparum malaria with
coexisting parvovirus B19 infection. Indian Pediatr 2003; 40:369-70.</mixed-citation>
         </ref>
         <ref id="d1186e496a1310">
            <label>12</label>
            <mixed-citation id="d1186e503" publication-type="other">
Pattison JR, Jones SE, Hodgson J, et al. Parvovirus infections and
hypoplastic crisis in sickle-cell anaemia. Lancet 1981; 1:664-5.</mixed-citation>
         </ref>
         <ref id="d1186e513a1310">
            <label>13</label>
            <mixed-citation id="d1186e520" publication-type="other">
Patou G, Pillay D, Myint S, Pattison J. Characterization of a nested
polymerase chain reaction assay for detection of parvovirus B19. J Clin
Microbiol 1993; 31:540-6.</mixed-citation>
         </ref>
         <ref id="d1186e533a1310">
            <label>14</label>
            <mixed-citation id="d1186e540" publication-type="other">
Gallinella G, Zuffi E, Gentilomi G, et al. Relevance of B19 markers in
serum samples for a diagnosis of parvovirus B19-correlated diseases.
J Med Virol 2003; 71:135-9.</mixed-citation>
         </ref>
         <ref id="d1186e553a1310">
            <label>15</label>
            <mixed-citation id="d1186e560" publication-type="other">
Clewley JP. Polymerase chain reaction assay of parvovirus B19 DNA
in clinical specimens. J Clin Microbiol 1989; 27:2647-51.</mixed-citation>
         </ref>
         <ref id="d1186e570a1310">
            <label>16</label>
            <mixed-citation id="d1186e577" publication-type="other">
Searle K, Guilliard C, Wallat S, Schalasta G, Enders G. Acute parvovirus
B19 infection in pregnant women-an analysis of serial samples by
serological and semi-quantitative PCR techniques. Infection 1998; 26:
139-43.</mixed-citation>
         </ref>
         <ref id="d1186e594a1310">
            <label>17</label>
            <mixed-citation id="d1186e601" publication-type="other">
Lindblom A, Isa A, Norbeck A, et al. Slow clearance of human par-
vovirus B19 viremia following acute infection. Clin Infect Dis 2005;
41:1201-3.</mixed-citation>
         </ref>
         <ref id="d1186e614a1310">
            <label>18</label>
            <mixed-citation id="d1186e621" publication-type="other">
Yeats J, Daley H, Hardie D. Parvovirus B19 infection does not con-
tribute significantly to severe anaemia in children with malaria in Ma-
lawi. Eur J Haematol 1999; 63:276-7.</mixed-citation>
         </ref>
         <ref id="d1186e634a1310">
            <label>19</label>
            <mixed-citation id="d1186e641" publication-type="other">
Genton B, al-Yaman F, Beck HP, et al. The epidemiology of malaria
in the Wosera area, East Sepik Province, Papua New Guinea, in prep-
aration for vaccine trials. I. Malariometric indices and immunity. Ann
Trop Med Parasitol 1995; 89:359-76.</mixed-citation>
         </ref>
         <ref id="d1186e657a1310">
            <label>20</label>
            <mixed-citation id="d1186e664" publication-type="other">
Doyle S, Kerr S, O'Keeffe G, O'Carroll D, Daly P, Kilty C. Detection of
parvovirus B19 IgM by antibody capture enzyme immunoassay: receiver
operating characteristic analysis. J Virol Methods 2000; 90:143-52.</mixed-citation>
         </ref>
         <ref id="d1186e677a1310">
            <label>21</label>
            <mixed-citation id="d1186e684" publication-type="other">
Parvovirus B19 IgG enzyme immunoassay package insert. Dublin, Ire-
land: Biotrin International, 2003.</mixed-citation>
         </ref>
         <ref id="d1186e694a1310">
            <label>22</label>
            <mixed-citation id="d1186e701" publication-type="other">
Parvovirus B19 IgM enzyme immunoassay package insert. Dublin, Ire-
land: Biotrin International, 2003.</mixed-citation>
         </ref>
         <ref id="d1186e712a1310">
            <label>23</label>
            <mixed-citation id="d1186e719" publication-type="other">
Butchko A. Comparison of three commercially available serologic as-
says used to detect human parvovirus B19-specific immunoglobulin
M (IgM) and IgG antibodies in sera of pregnant women. J Clin Mi-
crobiol 2004; 42:3191-5.</mixed-citation>
         </ref>
         <ref id="d1186e735a1310">
            <label>24</label>
            <mixed-citation id="d1186e742" publication-type="other">
Stoltzfus RJ, Chwaya HM, Montresor A, Albonico M, Savioli L, Tielsch
JM. Malaria, hookworms and recent fever are related to anemia and
iron status indicators in 0-to 5-y old Zanzibari children and these
relationships change with age. J Nutr 2000; 130:1724-33.</mixed-citation>
         </ref>
         <ref id="d1186e758a1310">
            <label>25</label>
            <mixed-citation id="d1186e765" publication-type="other">
Young GP, Smith MB, Woodfield DG. Glucose-6-phosphate dehydro-
genase deficiency in Papua New Guinea using a simple methylene blue
reduction test. Med J Australia 1974; 1:876-8.</mixed-citation>
         </ref>
         <ref id="d1186e778a1310">
            <label>26</label>
            <mixed-citation id="d1186e785" publication-type="other">
Muller I, Bockarie M, Alpers M, Smith T. The epidemiology of malaria
in Papua New Guinea. Trends Parasitol 2003; 19:253-9.</mixed-citation>
         </ref>
         <ref id="d1186e795a1310">
            <label>27</label>
            <mixed-citation id="d1186e802" publication-type="other">
Candotti D, Etiz N, Parsyan A, Allain JP. Identification and charac-
terization of persistent human erythrovirus infection in blood donor
samples. J Virol 2004; 78:12169-78.</mixed-citation>
         </ref>
         <ref id="d1186e815a1310">
            <label>28</label>
            <mixed-citation id="d1186e822" publication-type="other">
Hoebe CJ, Claas EC, Steenbergen JE, Kroes AC. Confirmation of an
outbreak of parvovirus B19 in a primary school using IgM ELISA and
PCR on thumb prick blood samples. J Clin Virol 2002; 25:303-7.</mixed-citation>
         </ref>
         <ref id="d1186e836a1310">
            <label>29</label>
            <mixed-citation id="d1186e843" publication-type="other">
Mallouh AA, Qudah A. An epidemic of aplastic crisis caused by human
parvovirus B19. Pediatr Infect Dis J 1995; 14:31-4.</mixed-citation>
         </ref>
         <ref id="d1186e853a1310">
            <label>30</label>
            <mixed-citation id="d1186e860" publication-type="other">
Oliveira SA, Camacho LA, Pereira AC, Faillace TF, Setubal S, Nasci-
mento JP. Clinical and epidemiological aspects of human parvovirus
B19 infection in an urban area in Brazil (Niteroi city area, State of Rio
de Janeiro, Brazil). Mem Inst Oswaldo Cruz 2002; 97:965-70.</mixed-citation>
         </ref>
         <ref id="d1186e876a1310">
            <label>31</label>
            <mixed-citation id="d1186e883" publication-type="other">
Heegaard ED, Brown KE. Human parvovirus B19. Clin Microbiol Rev
2002; 15:485-505.</mixed-citation>
         </ref>
         <ref id="d1186e893a1310">
            <label>32</label>
            <mixed-citation id="d1186e900" publication-type="other">
McElroy PD, ter Kuile FO, Lal AA, et al. Effect of Plasmodium falci-
parum parasitemia density on hemoglobin concentrations among full-
term, normal birth weight children in western Kenya, IV. The Asembo
Bay Cohort Project. Am J Trop Med Hyg 2000; 62:504-12.</mixed-citation>
         </ref>
         <ref id="d1186e916a1310">
            <label>33</label>
            <mixed-citation id="d1186e923" publication-type="other">
Price RN, Simpson JA, Nosten F, et al. Factors contributing to anemia
after uncomplicated falciparum malaria. Am J Trop Med Hyg 2001;
65:614-22.</mixed-citation>
         </ref>
         <ref id="d1186e936a1310">
            <label>34</label>
            <mixed-citation id="d1186e943" publication-type="other">
Shaffer N, Hedberg K, Davachi F, et al. Trends and risk factors for
HIV-1 seropositivity among outpatient children, Kinshasa, Zaire. AIDS
1990; 4:1231-6.</mixed-citation>
         </ref>
         <ref id="d1186e957a1310">
            <label>35</label>
            <mixed-citation id="d1186e964" publication-type="other">
Breman JG. The ears of the hippopotamus: manifestations, determi-
nants, and estimates of the malaria burden. Am J Trop Med Hyg 2001;
64:1-11.</mixed-citation>
         </ref>
         <ref id="d1186e977a1310">
            <label>36</label>
            <mixed-citation id="d1186e984" publication-type="other">
Ballou WR, Reed JL, Noble W, Young NS, Koenig S. Safety and im-
munogenicity of a recombinant parvovirus B19 vaccine formulated
with MF59C.1. J Infect Dis 2003; 187:675-8.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
