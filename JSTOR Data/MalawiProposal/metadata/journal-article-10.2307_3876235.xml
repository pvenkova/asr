<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jmodeafristud</journal-id>
         <journal-id journal-id-type="jstor">j100235</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Modern African Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">0022278X</issn>
         <issn pub-type="epub">14697777</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3876235</article-id>
         <title-group>
            <article-title> African Military coups d'état, 1956-2001: Frequency, Trends and Distribution </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Patrick J.</given-names>
                  <surname>McGowan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">41</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i371697</issue-id>
         <fpage>339</fpage>
         <lpage>370</lpage>
         <page-range>339-370</page-range>
         <permissions>
            <copyright-statement>Copyright 2003 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3876235"/>
         <abstract>
            <p> Described here is a new data set including all successful coups d'é tat (80), failed coup attempts (108) and reported coup plots (139) for all 48 independent sub-Saharan African (SSA) states for the 46-year period from January 1956 until December 2001. Elite political instability (PI) in this form remains widespread in SSA, in contrast to other regions of the global South. Military-led PI has been shown to adversely affect economic growth and human development in SSA, and is a major cause of the current African 'crisis'. The frequency of these instability events is given for each state for all 46 years and for the two periods 1956-79 and 1980-2001. A Total Military Intervention Score (TMIS) for each state is calculated and examined over time to explore trends in coup behaviour. The distribution of these events among major African regions is presented. Appendix A lists all coups and failed coups by state and date. Major findings are that military interventions have continued to be pervasive in Africa, despite democratisation trends since 1990; that coups, failed coups and coup plots form a syndrome of military-led PI; that colonial heritage is unrelated to coup activity; that the chance of success when launching a coup attempt has averaged more than 40% since 1958; that once a successful coup has occurred, military factionalism often leads to more coup behaviour; that except for a declining rate of success once a coup is undertaken, there is no major difference between 1956-79 and 1980-2001; that no trends of increasing or decreasing coup behaviour are evident, except that up to around 1975 as decolonisation progressed, TMIS also increased; and that West Africa is the predominant centre of coup activity in SSA, although all African regions have experienced coups. States that have been free of significant PI since 1990 are examined and those with institutionalised democratic traditions appear less prone to coups. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1741e313a1310">
            <label>4</label>
            <mixed-citation id="d1741e320" publication-type="other">
Mkandawire (2001: 299-303)</mixed-citation>
         </ref>
         <ref id="d1741e327a1310">
            <label>6</label>
            <mixed-citation id="d1741e334" publication-type="other">
Morrison et al. 1989</mixed-citation>
         </ref>
         <ref id="d1741e341a1310">
            <label>7</label>
            <mixed-citation id="d1741e348" publication-type="other">
Namibia
(1990)</mixed-citation>
            <mixed-citation id="d1741e357" publication-type="other">
Eritrea (1993)</mixed-citation>
            <mixed-citation id="d1741e363" publication-type="other">
South Africa (1994)</mixed-citation>
         </ref>
         <ref id="d1741e370a1310">
            <label>9</label>
            <mixed-citation id="d1741e377" publication-type="other">
Russett et al. 1968</mixed-citation>
         </ref>
         <ref id="d1741e385a1310">
            <label>17</label>
            <mixed-citation id="d1741e394" publication-type="other">
Esterhuysen 2002: 7</mixed-citation>
         </ref>
         <ref id="d1741e401a1310">
            <label>18</label>
            <mixed-citation id="d1741e408" publication-type="other">
Morrison et al. 1989: 647</mixed-citation>
         </ref>
         <ref id="d1741e415a1310">
            <label>19</label>
            <mixed-citation id="d1741e422" publication-type="other">
Morrison et al. 1989: 647</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1741e438a1310">
            <mixed-citation id="d1741e442" publication-type="journal">
Arrighi, G. 2002. 'The African crisis', New Left Review 15 (May/June): 5-36.<person-group>
                  <string-name>
                     <surname>Arrighi</surname>
                  </string-name>
               </person-group>
               <issue>May</issue>
               <fpage>5</fpage>
               <volume>15</volume>
               <source>New Left Review</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e474a1310">
            <mixed-citation id="d1741e478" publication-type="book">
Bayart, J.-F. 1993. The State in Africa: the politics of the belly. Harlow: Longman.<person-group>
                  <string-name>
                     <surname>Bayart</surname>
                  </string-name>
               </person-group>
               <source>The State in Africa: the politics of the belly</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e500a1310">
            <mixed-citation id="d1741e504" publication-type="journal">
Bennett, V. P. 1975. 'Military government in Mali', Journal of Modern African Studies 13, 2: 249-66.<object-id pub-id-type="jstor">10.2307/160192</object-id>
               <fpage>249</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e517a1310">
            <mixed-citation id="d1741e521" publication-type="journal">
Bienen, J. L. &amp; J. Moore. 1987. 'The Sudan military economic corporations', Armed Forces &amp; Society
13, 4:489-516.<person-group>
                  <string-name>
                     <surname>Bienen</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>489</fpage>
               <volume>13</volume>
               <source>Armed Forces &amp; Society</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e557a1310">
            <mixed-citation id="d1741e561" publication-type="journal">
Birmingham, D. 1978. 'The twenty-seventh of May: an historical note on the abortive 1977 coup in
Angola', African Affairs 77, 309: 554-64.<object-id pub-id-type="jstor">10.2307/721965</object-id>
               <fpage>554</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e577a1310">
            <mixed-citation id="d1741e581" publication-type="journal">
Box-Steffensmeier, J. M. &amp; B. S.Jones. 1997. 'Time is of the essence: event history models in political
science', American Journal of Political Science 41, 4: 1414-61.<object-id pub-id-type="doi">10.2307/2960496</object-id>
               <fpage>1414</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e597a1310">
            <mixed-citation id="d1741e601" publication-type="book">
Callaghy, T. &amp; J. Ravenhill. eds. 1993. Hemmed in: responses to Africa's economic decline. New York:
Columbia University Press.<person-group>
                  <string-name>
                     <surname>Callaghy</surname>
                  </string-name>
               </person-group>
               <source>Hemmed in: responses to Africa's economic decline</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e626a1310">
            <mixed-citation id="d1741e630" publication-type="journal">
Clapham, C. 1968. 'The Ethiopian coup d'état of December 1960', Journal of Modern African Studies 6,
4: 495-5O7.<object-id pub-id-type="jstor">10.2307/159330</object-id>
               <fpage>495</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e646a1310">
            <mixed-citation id="d1741e650" publication-type="journal">
Decalo, S. 1973. 'Military coups and military regimes in Africa', Journal of Modern African Studies 11,
1: 105-27.<object-id pub-id-type="jstor">10.2307/159875</object-id>
               <fpage>105</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e666a1310">
            <mixed-citation id="d1741e670" publication-type="book">
Decalo, S. 1976. Coups and Army Rule in Africa: studies in military style. New Haven, CT: Yale University
Press.<person-group>
                  <string-name>
                     <surname>Decalo</surname>
                  </string-name>
               </person-group>
               <source>Coups and Army Rule in Africa: studies in military style</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e696a1310">
            <mixed-citation id="d1741e700" publication-type="journal">
Decalo, S. 1989. 'Modalities of civil-military stability in Africa', Journal of Modern African Studies 27,
4:547-78.<object-id pub-id-type="jstor">10.2307/161109</object-id>
               <fpage>547</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e716a1310">
            <mixed-citation id="d1741e720" publication-type="book">
Deger, S. 1986. Military Expenditure in Third World Countries. London: Routledge &amp; Kegan Paul.<person-group>
                  <string-name>
                     <surname>Deger</surname>
                  </string-name>
               </person-group>
               <source>Military Expenditure in Third World Countries</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e742a1310">
            <mixed-citation id="d1741e746" publication-type="book">
Diamond, L. 1995. 'Nigeria: the uncivic society and the descent into praetorianism', in L. Diamond,
J. J. Lintz &amp; S. M. Lipset, eds., Politics in Developing Countries: comparing experiences with democracy.
Boulder, CO: Lynne Rienner.<person-group>
                  <string-name>
                     <surname>Diamond</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Nigeria: the uncivic society and the descent into praetorianism</comment>
               <source>Politics in Developing Countries: comparing experiences with democracy</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e778a1310">
            <mixed-citation id="d1741e782" publication-type="journal">
The Economist (London). 2000. 'Africa: The hopeless continent', 13-19 May.<issue>13 May</issue>
               <source>The Economist (London)</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e798a1310">
            <mixed-citation id="d1741e802" publication-type="book">
Englebert, P. 2000. State Legitimacy and Development in Africa. Boulder, CO: Westview.<person-group>
                  <string-name>
                     <surname>Englebert</surname>
                  </string-name>
               </person-group>
               <source>State Legitimacy and Development in Africa</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e824a1310">
            <mixed-citation id="d1741e828" publication-type="book">
Esterhuysen, P. 2002. Africa at a Glance: facts and figures 2001/02. Pretoria: Africa Institute of South
Africa.<person-group>
                  <string-name>
                     <surname>Esterhuysen</surname>
                  </string-name>
               </person-group>
               <source>Africa at a Glance: facts and figures 2001/02</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e854a1310">
            <mixed-citation id="d1741e858" publication-type="book">
Finer, S. 1988. The Man on Horseback: the role of the military in politics. 2nd ed. London: Pinter.<person-group>
                  <string-name>
                     <surname>Finer</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>The Man on Horseback: the role of the military in politics</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e883a1310">
            <mixed-citation id="d1741e887" publication-type="book">
First, R. 1970. The Barrel of a Gun: political power in Africa and the coup d'état. London: Penguin.<person-group>
                  <string-name>
                     <surname>First</surname>
                  </string-name>
               </person-group>
               <source>The Barrel of a Gun: political power in Africa and the coup d'état</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e909a1310">
            <mixed-citation id="d1741e913" publication-type="journal">
Fischer, H.J. 1969. 'Elections and coups in Sierra Leone, 1967', Journal of Modern African Studies 7,
4: 611-36.<object-id pub-id-type="jstor">10.2307/159154</object-id>
               <fpage>611</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e929a1310">
            <mixed-citation id="d1741e933" publication-type="journal">
Fosu, A. K. 1992. 'Political instability and economic growth: evidence from Sub-Saharan Africa',
Economic Development and Cultural Change 40, 4: 829-41.<object-id pub-id-type="jstor">10.2307/1154636</object-id>
               <fpage>829</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e949a1310">
            <mixed-citation id="d1741e953" publication-type="journal">
Fosu, A. K. 2001. 'Political instability and economic growth in developing countries: some specifi-
cation empirics', Economic Letters 70: 289-94.<person-group>
                  <string-name>
                     <surname>Fosu</surname>
                  </string-name>
               </person-group>
               <fpage>289</fpage>
               <volume>70</volume>
               <source>Economic Letters</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e985a1310">
            <mixed-citation id="d1741e989" publication-type="journal">
Fosu, A. K. 2002a. ' Political instability and economic growth: implications of coup events in Sub-
Saharan Africa', American Journal of Economics and Sociology 61, 1: 329-48.<object-id pub-id-type="jstor">10.2307/3487750</object-id>
               <fpage>329</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1006a1310">
            <mixed-citation id="d1741e1010" publication-type="journal">
Fosu, A. K. 2002b. 'Transforming economic growth to human development in Sub-Saharan Africa:
the role of elite political instability', Oxford Development Studies 30, 1: 9-19.<person-group>
                  <string-name>
                     <surname>Fosu</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>9</fpage>
               <volume>30</volume>
               <source>Oxford Development Studies</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1045a1310">
            <mixed-citation id="d1741e1049" publication-type="journal">
Goldsworthy, D. 1981. 'Civilian control of the military in black Africa', African Affairs 80, 318: 49-74.<object-id pub-id-type="jstor">10.2307/721430</object-id>
               <fpage>49</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1062a1310">
            <mixed-citation id="d1741e1066" publication-type="journal">
Gutteridge, W. F. 1967. 'The political role of African armed forces: the impact of foreign military
assistance', African Affairs 66, 263: 93-103.<object-id pub-id-type="jstor">10.2307/720295</object-id>
               <fpage>93</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1082a1310">
            <mixed-citation id="d1741e1086" publication-type="journal">
Higgott, R. &amp; F. Fuglestad. 1975. 'The 1974 coup d'état in Niger: towards an explanation', Journal of
Modern African Studies 13, 3: 383-98.<object-id pub-id-type="jstor">10.2307/159846</object-id>
               <fpage>383</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1102a1310">
            <mixed-citation id="d1741e1106" publication-type="book">
Huntington, S. P. 1968. Political Order in Changing Societies. New Haven, CT: Yale University Press.<person-group>
                  <string-name>
                     <surname>Huntington</surname>
                  </string-name>
               </person-group>
               <source>Political Order in Changing Societies</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1128a1310">
            <mixed-citation id="d1741e1132" publication-type="book">
Huntington, S. P. 1991. The Third Wave: democratization in the late twentieth century. Norman, OK:
University of Oklahoma Press.<person-group>
                  <string-name>
                     <surname>Huntington</surname>
                  </string-name>
               </person-group>
               <source>The Third Wave: democratization in the late twentieth century</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1158a1310">
            <mixed-citation id="d1741e1162" publication-type="journal">
Ihonvbere, J. O. 1996. 'Are things falling apart? the military and the crisis of democratisation in
Nigeria', Journal of Modem African Studies 34, 2: 193-225.<person-group>
                  <string-name>
                     <surname>Ihonvbere</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>193</fpage>
               <volume>34</volume>
               <source>Journal of Modem African Studies</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1197a1310">
            <mixed-citation id="d1741e1201" publication-type="journal">
Jackman, R. W. 1978. 'The predictability of coups d'état, a model with African data', American Political
Science Review 72, 4: 1262-75.<object-id pub-id-type="doi">10.2307/1954538</object-id>
               <fpage>1262</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1217a1310">
            <mixed-citation id="d1741e1221" publication-type="journal">
Jenkins, J. C. &amp; A.J. Kposowa. 1990. 'Explaining military coups d'etat: black Africa, 1957-1984',
American Sociological Review 55: 861-75.<object-id pub-id-type="doi">10.2307/2095751</object-id>
               <fpage>861</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1237a1310">
            <mixed-citation id="d1741e1241" publication-type="journal">
Jenkins, J. C. &amp; A.J. Kposowa. 1992. 'The political origins of African military coups: ethnic compe-
tition, military centrality, and the struggle over the postcolonial state', International Studies Quarterly
36, 3: 271-92.<object-id pub-id-type="doi">10.2307/2600773</object-id>
               <fpage>271</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1260a1310">
            <mixed-citation id="d1741e1264" publication-type="journal">
Kebschull, H. G. 1994. 'Operation "just missed": lessons from failed coup attempts', Armed Forces &amp;
Society 20, 4: 565-79.<person-group>
                  <string-name>
                     <surname>Kebschull</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>565</fpage>
               <volume>20</volume>
               <source>Armed Forces &amp; Society</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1299a1310">
            <mixed-citation id="d1741e1303" publication-type="journal">
Kennedy, P. 1999. 'Sub-Saharan Africa's current plight and the threat or promise of globalisation',
Global Society 13, 4: 441-66.<person-group>
                  <string-name>
                     <surname>Kennedy</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>441</fpage>
               <volume>13</volume>
               <source>Global Society</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1339a1310">
            <mixed-citation id="d1741e1343" publication-type="journal">
Kposowa, A.J. &amp;J. C.Jenkins. 1993. 'The structural sources of military coups in post-colonial Africa',
American Journal of Sociology 99, 1: 126-63.<object-id pub-id-type="jstor">10.2307/2781957</object-id>
               <fpage>126</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1359a1310">
            <mixed-citation id="d1741e1363" publication-type="book">
Kuznets, S. S. 1996. Modern Economic Growth: rate, structure, and spread. New Haven, CT: Yale University
Press.<person-group>
                  <string-name>
                     <surname>Kuznets</surname>
                  </string-name>
               </person-group>
               <source>Modern Economic Growth: rate, structure, and spread</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1388a1310">
            <mixed-citation id="d1741e1392" publication-type="journal">
Lewis, I. M. 1972. 'The politics of the 1969 Somali coup', Journal of Modern African Studies 10, 3: 383-408.<object-id pub-id-type="jstor">10.2307/160127</object-id>
               <fpage>383</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1405a1310">
            <mixed-citation id="d1741e1409" publication-type="other">
Lexis-Nexis Academic Universe electronic archive (http://web.lexis-nexis.com/universe/).</mixed-citation>
         </ref>
         <ref id="d1741e1416a1310">
            <mixed-citation id="d1741e1420" publication-type="journal">
Leys, C. 1994. 'Confronting the African tragedy', New Left Review 204: 33-47.<person-group>
                  <string-name>
                     <surname>Leys</surname>
                  </string-name>
               </person-group>
               <fpage>33</fpage>
               <volume>204</volume>
               <source>New Left Review</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1449a1310">
            <mixed-citation id="d1741e1453" publication-type="journal">
Lofchie, M. F. 1972. 'The Uganda coup - class action by the military', Journal of Modern African Studies
10, 1: 19-35.<object-id pub-id-type="jstor">10.2307/159819</object-id>
               <fpage>19</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1470a1310">
            <mixed-citation id="d1741e1474" publication-type="journal">
Luckham, R. 1994. 'The military, militarization and democratization in Africa: a survey of literature
and issues', African Studies Review 37, 2: 13-76.<object-id pub-id-type="doi">10.2307/524766</object-id>
               <fpage>13</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1490a1310">
            <mixed-citation id="d1741e1494" publication-type="journal">
Lunde, T. K. 1991. 'Modernization and political instability: coups d'état in Africa 1955-1985', Acta
Sociologica 34, 1: 13-32.<person-group>
                  <string-name>
                     <surname>Lunde</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>13</fpage>
               <volume>34</volume>
               <source>Acta Sociologica</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1529a1310">
            <mixed-citation id="d1741e1533" publication-type="other">
McGowan, P. 1986. 'African military intervention events: January 1, 1956 to December 31, 1985'.
Tempe, AZ: unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="d1741e1543a1310">
            <mixed-citation id="d1741e1547" publication-type="journal">
McGowan, P. &amp; T. H.Johnson. 1984. 'African military coups d'état and under-development: a quan-
titative historical analysis', Journal of Modern African Studies 22, 4: 633-66.<object-id pub-id-type="jstor">10.2307/160389</object-id>
               <fpage>633</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1563a1310">
            <mixed-citation id="d1741e1567" publication-type="journal">
McGowan, P. &amp; T. H.Johnson. 1986. 'Sixty coups in thirty years: further evidence regarding African
military coups d'état', Journal of Modern African Studies 24, 3: 539-46.<object-id pub-id-type="jstor">10.2307/160357</object-id>
               <fpage>539</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1583a1310">
            <mixed-citation id="d1741e1587" publication-type="journal">
Mkandawire, T. 2001. 'Thinking about developmental states in Africa', Cambridge Journal of Economics
25, 3:289-313.<person-group>
                  <string-name>
                     <surname>Mkandawire</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>289</fpage>
               <volume>25</volume>
               <source>Cambridge Journal of Economics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1623a1310">
            <mixed-citation id="d1741e1627" publication-type="journal">
Morrison, D. G. &amp; H. M. Stevenson. 1971. 'Political instability in independent black Africa: more
dimensions of conflict behavior within nations', Journal of Conflict Resolution 15, 3: 347-68.<object-id pub-id-type="jstor">10.2307/173388</object-id>
               <fpage>347</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1643a1310">
            <mixed-citation id="d1741e1647" publication-type="book">
Morrison, D. G., R. C. Mitchell, J. N. Paden, H. M. Stevenson &amp; associates. 1972. Black Africa: a
comparative handbook. New York: Free Press.<person-group>
                  <string-name>
                     <surname>Morrison</surname>
                  </string-name>
               </person-group>
               <source>Black Africa: a comparative handbook</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1672a1310">
            <mixed-citation id="d1741e1676" publication-type="book">
Morrison, D. G., R. C. Mitchell &amp;J. N. Paden. 1989. Black Africa: a comparative handbook. 2nd ed. New
York: Paragon House &amp; Irvington Publishers.<person-group>
                  <string-name>
                     <surname>Morrison</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Black Africa: a comparative handbook</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1705a1310">
            <mixed-citation id="d1741e1709" publication-type="journal">
O'Connell, J. 1967. 'The inevitability of instability', Journal of Modern African Studies 5, 2: 181-91.<object-id pub-id-type="jstor">10.2307/159224</object-id>
               <fpage>181</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1722a1310">
            <mixed-citation id="d1741e1726" publication-type="journal">
Pachter, E. F. 1982. ' Contra-coup: civilian control of the military in Guinea, Tanzania, and
Mozambique', Journal of Modern African Studies 20, 4: 595-612.<object-id pub-id-type="jstor">10.2307/160340</object-id>
               <fpage>595</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1742a1310">
            <mixed-citation id="d1741e1746" publication-type="book">
Ramsay, E.J. 2001. Global Studies: Africa. Guilford, CT: McGraw-Hill/Dushkin.<person-group>
                  <string-name>
                     <surname>Ramsay</surname>
                  </string-name>
               </person-group>
               <source>Global Studies: Africa</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1769a1310">
            <mixed-citation id="d1741e1773" publication-type="book">
Reno, W. 1998. Warlord Politics and African States. Boulder, CO: Lynne Rienner.<person-group>
                  <string-name>
                     <surname>Reno</surname>
                  </string-name>
               </person-group>
               <source>Warlord Politics and African States</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1795a1310">
            <mixed-citation id="d1741e1799" publication-type="journal">
Renou, X. 2002. 'A new French policy for Africa?', Journal of Contemporary African Studies 20, 1: 5-27.<person-group>
                  <string-name>
                     <surname>Renou</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>5</fpage>
               <volume>20</volume>
               <source>Journal of Contemporary African Studies</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1831a1310">
            <mixed-citation id="d1741e1835" publication-type="book">
Rouquié, A. ed. 1981. Lapolitique deMars: lesprocessuspolitiques dans kspartis militaires contemporains. Paris:
Sycomore.<person-group>
                  <string-name>
                     <surname>Rouquié</surname>
                  </string-name>
               </person-group>
               <source>Lapolitique deMars: lesprocessuspolitiques dans kspartis militaires contemporains</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1860a1310">
            <mixed-citation id="d1741e1864" publication-type="journal">
Russet, B. M., J. D. Singer &amp; M. Small. 1968. 'National political units in the twentieth century: a
standardized list', American Political Science Review 63, 3: 932-51.<object-id pub-id-type="doi">10.2307/1953441</object-id>
               <fpage>932</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1880a1310">
            <mixed-citation id="d1741e1884" publication-type="book">
Sandbrook, R. 1985. The Politics of Africa's Economic Stagnation. Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Sandbrook</surname>
                  </string-name>
               </person-group>
               <source>The Politics of Africa's Economic Stagnation</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1906a1310">
            <mixed-citation id="d1741e1910" publication-type="journal">
Southall, A. 1975. 'General Amin and the coup: great man or historical inevitability?', Journal of
Modem Africa Studies 13, 1: 85-105.<person-group>
                  <string-name>
                     <surname>Southall</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>85</fpage>
               <volume>13</volume>
               <source>Journal of Modem Africa Studies</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e1946a1310">
            <mixed-citation id="d1741e1950" publication-type="journal">
Strang, D. 1990. 'From dependence to sovereignty: an event history analysis of decolonisation
1870-1987', American Sociological Review 55: 846-60.<object-id pub-id-type="doi">10.2307/2095750</object-id>
               <fpage>846</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e1966a1310">
            <mixed-citation id="d1741e1970" publication-type="other">
Wang, T. Y. 1996. 'African coup events data: 1986-1990', Interuniversity Consortium for Political
and Social Research, Archive Number 6869.</mixed-citation>
         </ref>
         <ref id="d1741e1980a1310">
            <mixed-citation id="d1741e1984" publication-type="journal">
Wang, T. Y. 1998. 'Arms transfers and coups d'état: a study of sub-Saharan Africa', Journal of Peace
Research 35, 6: 659-75.<object-id pub-id-type="jstor">10.2307/425409</object-id>
               <fpage>659</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e2000a1310">
            <mixed-citation id="d1741e2004" publication-type="journal">
Welch, C. E. 1967. 'Soldier and state in Africa', Journal of Modern African Studies 5, 3: 305-22.<object-id pub-id-type="jstor">10.2307/158726</object-id>
               <fpage>305</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e2017a1310">
            <mixed-citation id="d1741e2021" publication-type="journal">
Welch, C. E. 1972. 'Praetorianism in Commonwealth west Africa', Journal of Modern African Studies 10,
2: 203-21.<object-id pub-id-type="jstor">10.2307/159963</object-id>
               <fpage>203</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e2037a1310">
            <mixed-citation id="d1741e2041" publication-type="journal">
Welch, C. E. 1975. 'Continuity and discontinuity in African military organisation', Journal of Modern
African Studies 13, 2: 229-48.<object-id pub-id-type="jstor">10.2307/160191</object-id>
               <fpage>229</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e2058a1310">
            <mixed-citation id="d1741e2062" publication-type="book">
Whitaker, J. C. 1988. How can Africa Survive? New York: Council on Foreign Relations.<person-group>
                  <string-name>
                     <surname>Whitaker</surname>
                  </string-name>
               </person-group>
               <source>How can Africa Survive?</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e2084a1310">
            <mixed-citation id="d1741e2088" publication-type="journal">
Wolpin, M. D. 1980. 'Legitimizing state capitalism: Malian militarism in third world perspective',
Journal of Modern African Studies 18, 2: 281-95.<object-id pub-id-type="jstor">10.2307/160281</object-id>
               <fpage>281</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1741e2104a1310">
            <mixed-citation id="d1741e2108" publication-type="book">
World Bank. 1989. Sub-Saharan Africa: from crisis to sustainable growth. New York: Oxford University
Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Sub-Saharan Africa: from crisis to sustainable growth</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1741e2133a1310">
            <mixed-citation id="d1741e2137" publication-type="book">
World Bank. 1994. Adjustment in Africa: reforms, results, and the road ahead. New York: Oxford University
Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>Adjustment in Africa: reforms, results, and the road ahead</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
