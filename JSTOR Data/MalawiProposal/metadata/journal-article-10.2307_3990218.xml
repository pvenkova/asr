<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id journal-id-type="jstor">j101235</journal-id>
         <journal-title-group>
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>World Bank</publisher-name>
         </publisher>
         <issn pub-type="ppub">02586770</issn>
         <issn pub-type="epub">1564698X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3990218</article-id>
         <title-group>
            <article-title>Openness and Wage Inequality in Developing Countries: The Latin American Challenge to East Asian Conventional Wisdom</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Adrian</given-names>
                  <surname>Wood</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i382788</issue-id>
         <fpage>33</fpage>
         <lpage>57</lpage>
         <page-range>33-57</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 The International Bank for Reconstruction and Development/The World Bank</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3990218"/>
         <abstract>
            <p>The experience of East Asia in the 1960s and 1970s supports the theory that greater openness to trade tends to narrow the wage gap between skilled and unskilled workers in developing countries. In Latin America since the mid-1980s, however, increased openness has widened wage differentials. This conflict of evidence is probably not the result of differences between East Asia and Latin America. Instead, the conflict is probably the result of differences between the 1960s and the 1980s, specifically, the entry of China into the world market and, perhaps, the advent of new technology biased against unskilled workers.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d764e120a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d764e127" publication-type="other">
World Bank (1989: indicators table 17</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d764e133" publication-type="other">
1995: indicators table 16, with Indonesia
moved into the low-income group for consistency).</mixed-citation>
            </p>
         </fn>
         <fn id="d764e143a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d764e150" publication-type="other">
Sarkar and Singer (1991)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d764e166a1310">
            <mixed-citation id="d764e170" publication-type="book">
Bell, Linda A.1995. "The Impact of Minimum Wages in Mexico and Colombia." Policy
Research Working Paper 1514. Policy Research Department, World Bank, Wash-
ington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Bell</surname>
                  </string-name>
               </person-group>
               <source>The Impact of Minimum Wages in Mexico and Colombia</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e199a1310">
            <mixed-citation id="d764e203" publication-type="book">
Bourguignon, Francois, and Christian Morrisson. 1989. External Trade and Income
Distribution. Paris: Organization for Economic Cooperation and Development.<person-group>
                  <string-name>
                     <surname>Bourguignon</surname>
                  </string-name>
               </person-group>
               <source>External Trade and Income Distribution</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d764e228a1310">
            <mixed-citation id="d764e232" publication-type="journal">
Dornbusch, Rudiger, Stanley Fischer, and Paul Samuelson. 1980. "Heckscher-Ohlin Trade
Theory with a Continuum of Goods." Quarterly Journal of Economics95(2):203-24.<object-id pub-id-type="doi">10.2307/1885496</object-id>
               <fpage>203</fpage>
            </mixed-citation>
         </ref>
         <ref id="d764e248a1310">
            <mixed-citation id="d764e252" publication-type="book">
Edwards, Alejandra Cox, and Sebastian Edwards. 1995. "Trade Liberalization and
Unemployment: Evidence from Chile." California State University and University of
California, Los Angeles. Processed.<person-group>
                  <string-name>
                     <surname>Edwards</surname>
                  </string-name>
               </person-group>
               <source>Trade Liberalization and Unemployment: Evidence from Chile</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e282a1310">
            <mixed-citation id="d764e286" publication-type="book">
Feenstra, Robert, and Gordon Hanson. 1995. "Foreign Investment, Outsourcing, and
Relative Wages." In Robert Feenstra, Gene Grossman, and Douglas Irwin, eds., Po-
litical Economy of Trade Policy: Essays in Honor of Jagdish Bhagwati. Cambridge,
Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Feenstra</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Foreign Investment, Outsourcing, and Relative Wages</comment>
               <source>Political Economy of Trade Policy: Essays in Honor of Jagdish Bhagwati</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e321a1310">
            <mixed-citation id="d764e325" publication-type="journal">
Fields, Gary. 1994. "Changing Labor Market Conditions and Economic Development
in Hong Kong, the Republic of Korea, Singapore, and Taiwan, China." The World
Bank Economic Review8(3):395-414.<person-group>
                  <string-name>
                     <surname>Fields</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>395</fpage>
               <volume>8</volume>
               <source>The World Bank Economic Review</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e363a1310">
            <mixed-citation id="d764e367" publication-type="book">
Fischer, Bernhard, and Dean Spinanger. 1986. "Factor Market Distortions and Export
Performance: An Eclectic Review of the Evidence." Kiel Working Paper 259. Institut
fur Weltwirtschaft an der Universitat Kiel. Processed.<person-group>
                  <string-name>
                     <surname>Fischer</surname>
                  </string-name>
               </person-group>
               <source>Factor Market Distortions and Export Performance: An Eclectic Review of the Evidence</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d764e396a1310">
            <mixed-citation id="d764e400" publication-type="other">
Fontana, Marzia. 1994. "Trade Liberalisation and Income Distribution in Developing
Countries." M. Phil. diss., Institute of Development Studies, University of Sussex.
Processed.</mixed-citation>
         </ref>
         <ref id="d764e413a1310">
            <mixed-citation id="d764e417" publication-type="book">
Hanson, Gordon H., and Ann Harrison. 1995. "Trade, Technology, and Wage In-
equality." NBER Working Paper 5110. National Bureau of Economic Research, Cam-
bridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Hanson</surname>
                  </string-name>
               </person-group>
               <source>Trade, Technology, and Wage Inequality</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e446a1310">
            <mixed-citation id="d764e450" publication-type="journal">
Kaplinsky, Raphael. 1993. "Export Processing Zones in the Dominican Republic: Trans-
forming Manufactures into Commodities." World Development21(11):1851-65.<person-group>
                  <string-name>
                     <surname>Kaplinsky</surname>
                  </string-name>
               </person-group>
               <issue>11</issue>
               <fpage>1851</fpage>
               <volume>21</volume>
               <source>World Development</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d764e486a1310">
            <mixed-citation id="d764e490" publication-type="book">
Kim, K. S., and Pompen Vorasopontaviporn. 1989. "Foreign Trade and the Distribu-
tion of Income in Thailand." Working Paper 124. Helen Kellogg Institute for Inter-
national Studies, University of Notre Dame, Notre Dame, Ind. Processed.<person-group>
                  <string-name>
                     <surname>Kim</surname>
                  </string-name>
               </person-group>
               <source>Foreign Trade and the Distribution of Income in Thailand</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d764e519a1310">
            <mixed-citation id="d764e523" publication-type="book">
Krueger, Anne 0. 1983. Trade and Employment in Developing Countries. Vol. 3: Syn-
thesis and Conclusions. Chicago: University of Chicago Press.<person-group>
                  <string-name>
                     <surname>Krueger</surname>
                  </string-name>
               </person-group>
               <volume>3</volume>
               <source>Trade and Employment in Developing Countries</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d764e552a1310">
            <mixed-citation id="d764e556" publication-type="book">
Krueger, Anne O., Hal B. Lary, Terry D. Monson, and Narongchai Akrasanee, eds.
1981. Trade and Employment in Developing Countries. Vol. 1: Individual Studies.
Chicago: University of Chicago Press.<person-group>
                  <string-name>
                     <surname>Krueger</surname>
                  </string-name>
               </person-group>
               <volume>1</volume>
               <source>Trade and Employment in Developing Countries</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d764e588a1310">
            <mixed-citation id="d764e592" publication-type="book">
Leamer, Edward E.1995. "A Trade Economist's View of U.S. Wages and Globaliza-
tion." In Susan Collins, ed., Imports, Exports, and the American Worker. Washing-
ton, D.C.: Brookings Institution.<person-group>
                  <string-name>
                     <surname>Leamer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A Trade Economist's View of U.S. Wages and Globalization</comment>
               <source>Imports, Exports, and the American Worker</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e624a1310">
            <mixed-citation id="d764e628" publication-type="book">
Lee, T. H., and K. S. Liang. 1982. "Taiwan." In Bela Balassa, ed., Development
Strategies in Semi-Industrial Economies. Baltimore, Md.: Johns Hopkins University
Press.<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Taiwan</comment>
               <source>Development Strategies in Semi-Industrial Economies</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d764e660a1310">
            <mixed-citation id="d764e664" publication-type="journal">
Londero, Elio, and Simon Teitel. 1996. "Industrialisation and the Factor Content of Latin
American Exports of Manufactures."Journal of Development Studies32(4):581-601.<person-group>
                  <string-name>
                     <surname>Londero</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>581</fpage>
               <volume>32</volume>
               <source>Journal of Development Studies</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d764e700a1310">
            <mixed-citation id="d764e704" publication-type="book">
Minford, Patrick, Jonathan Riley, and Eric Nowell. 1995. "The Elixir of Growth: Trade,
Non-Traded Goods, and Development." CEPR Discussion Paper 1165. Centre for
Economic Policy Research, London. Processed.<person-group>
                  <string-name>
                     <surname>Minford</surname>
                  </string-name>
               </person-group>
               <source>The Elixir of Growth: Trade, Non-Traded Goods, and Development</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e733a1310">
            <mixed-citation id="d764e737" publication-type="journal">
Nambiar, R. G., and Gopal Tadas. 1994. "Is Trade Deindustrialising India?" Eco-
nomic and Political Weekly15(October):2741-46.<person-group>
                  <string-name>
                     <surname>Nambiar</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>2741</fpage>
               <volume>15</volume>
               <source>Economic and Political Weekly</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e772a1310">
            <mixed-citation id="d764e776" publication-type="book">
Owens, Trudy, and Adrian Wood. 1995. "Export-Oriented Industrialisation through
Primary Processing?" IDS Working Paper 19. Institute of Development Studies, Uni-
versity of Sussex. Processed.<person-group>
                  <string-name>
                     <surname>Owens</surname>
                  </string-name>
               </person-group>
               <source>Export-Oriented Industrialisation through Primary Processing?</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e805a1310">
            <mixed-citation id="d764e809" publication-type="book">
Revenga, Ana, and Claudio Montenegro. 1995. "North American Integration and Fac-
tor Price Equalization: Is There Evidence of Wage Convergence between Mexico and
the United States?" In Susan Collins, ed., Imports, Exports, and the American Worker.
Washington, D.C.: Brookings Institution.<person-group>
                  <string-name>
                     <surname>Revenga</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">North American Integration and Factor Price Equalization: Is There Evidence of Wage Convergence between Mexico and the United States?</comment>
               <source>Imports, Exports, and the American Worker</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e844a1310">
            <mixed-citation id="d764e848" publication-type="book">
Robinson, Sherman, and Karen Thierfelder. 1996. "The Trade-Wage Debate in a Model
with Nontraded Goods: Making Room for Labor Economists in Trade Theory."TMD
Discussion Paper 9. Trade and Macroeconomics Division, International Food Policy
Research Institute, Washington, D.C. Processed.<person-group>
                  <string-name>
                     <surname>Robinson</surname>
                  </string-name>
               </person-group>
               <source>The Trade-Wage Debate in a Model with Nontraded Goods: Making Room for Labor Economists in Trade Theory</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d764e880a1310">
            <mixed-citation id="d764e884" publication-type="book">
Robbins, Donald. 1994a. "Malaysian Wage Structure and Its Causes." Harvard Insti-
tute for International Development, Cambridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Malaysian Wage Structure and Its Causes</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e910a1310">
            <mixed-citation id="d764e914" publication-type="book">
.1994b. "Philippine Wage and Employment Structure, 1978-93." Harvard In-
stitute for International Development, Cambridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Philippine Wage and Employment Structure, 1978-93</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e939a1310">
            <mixed-citation id="d764e943" publication-type="book">
.1995a. "Earnings Dispersion in Chile after Trade Liberalization." Harvard
Institute for International Development, Cambridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Earnings Dispersion in Chile after Trade Liberalization</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e968a1310">
            <mixed-citation id="d764e972" publication-type="book">
.1995b. "Trade, Trade Liberalization, and Inequality in Latin America and East
Asia: Synthesis of Seven Country Studies." Harvard Institute for International De-
velopment, Cambridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Trade, Trade Liberalization, and Inequality in Latin America and East Asia: Synthesis of Seven Country Studies</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1001a1310">
            <mixed-citation id="d764e1005" publication-type="book">
.1996a. "Stolper-Samuelson (Lost) in the Tropics: Trade Liberalization and Wages
in Colombia 1976-94." Harvard Institute for International Development, Cambridge,
Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Stolper-Samuelson (Lost) in the Tropics: Trade Liberalization and Wages in Colombia 1976-94</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1034a1310">
            <mixed-citation id="d764e1038" publication-type="book">
.1996b. "HOS Hits Facts: Facts Win. Evidence on Trade and Wages in the Devel-
oping World." Harvard Institute for International Development, Cambridge, Mass.
Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>HOS Hits Facts: Facts Win. Evidence on Trade and Wages in the Developing World</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1067a1310">
            <mixed-citation id="d764e1071" publication-type="book">
Robbins, Donald, and Thomas Gindling. 1997. "Educational Expansion, Trade
Liberalisation, and Distribution in Costa Rica." In Albert Berry, ed., Poverty, Eco-
nomic Reform and Income Distribution in Latin America. Boulder, Colo.: Lynne
Rienner Publishers.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Educational Expansion, Trade Liberalisation, and Distribution in Costa Rica</comment>
               <source>Poverty, Economic Reform and Income Distribution in Latin America</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1107a1310">
            <mixed-citation id="d764e1111" publication-type="book">
Robbins, Donald, Martin Gonzales, and Alicia Menendez. 1995. "Wage Dispersion in
Argentina, 1976-93: Trade Liberalization amidst Inflation, Stabilization, and Overvalu-
ation." Harvard Institute for International Development, Cambridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Wage Dispersion in Argentina, 1976-93: Trade Liberalization amidst Inflation, Stabilization, and Overvaluation</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1140a1310">
            <mixed-citation id="d764e1144" publication-type="book">
Robbins, Donald, and Joseph Zveglich. 1995. "Skill-Bias in Recent Taiwanese Growth."
Harvard Institute for International Development, Cambridge, Mass. Processed.<person-group>
                  <string-name>
                     <surname>Robbins</surname>
                  </string-name>
               </person-group>
               <source>Skill-Bias in Recent Taiwanese Growth</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1169a1310">
            <mixed-citation id="d764e1173" publication-type="journal">
Sarkar, Prabirjit, and Hans Singer. 1991. "Manufactured Exports of Developing Coun-
tries and Their Terms of Trade since 1965." World Development19(4):333-40.<person-group>
                  <string-name>
                     <surname>Sarkar</surname>
                  </string-name>
               </person-group>
               <issue>4</issue>
               <fpage>333</fpage>
               <volume>19</volume>
               <source>World Development</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1208a1310">
            <mixed-citation id="d764e1212" publication-type="book">
Wood, Adrian. 1994. North-South Trade, Employment, and Inequality: Changing For-
tunes in a Skill-Driven World. Oxford: Clarendon Press.<person-group>
                  <string-name>
                     <surname>Wood</surname>
                  </string-name>
               </person-group>
               <source>North-South Trade, Employment, and Inequality: Changing Fortunes in a Skill-Driven World</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1237a1310">
            <mixed-citation id="d764e1241" publication-type="journal">
1995. "How Trade Hurt Unskilled Workers." Journal of Economic Perspec-
tives9(3):57-80.<object-id pub-id-type="jstor">10.2307/2138425</object-id>
               <fpage>57</fpage>
            </mixed-citation>
         </ref>
         <ref id="d764e1257a1310">
            <mixed-citation id="d764e1261" publication-type="book">
World Bank. 1989. World Development Report 1989: Financial Systems and Develop-
ment. New York: Oxford University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report 1989: Financial Systems and Development</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1287a1310">
            <mixed-citation id="d764e1291" publication-type="book">
.1995. World Development Report 1995: Workers in an Integrating World.
New York: Oxford University Press.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>World Development Report 1995: Workers in an Integrating World</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
