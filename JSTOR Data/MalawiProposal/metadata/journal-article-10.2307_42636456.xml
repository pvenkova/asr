<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">populationenviro</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000178</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population and Environment</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer Science+Business Media</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01990039</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737810</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42636456</article-id>
         <title-group>
            <article-title>Contribution of forest provisioning ecosystem services to rural livelihoods in the Miombo woodlands of Zambia</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Felix Kanungwe</given-names>
                  <surname>Kalaba</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Claire Helen</given-names>
                  <surname>Quinn</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrew John</given-names>
                  <surname>Dougill</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40098291</issue-id>
         <fpage>159</fpage>
         <lpage>182</lpage>
         <permissions>
            <copyright-statement>© 2013 Springer Science+Business Media</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s11111-013-0189-5"
                   xlink:title="an external site"/>
         <abstract>
            <p>This paper examines the contribution of forest provisioning ecosystem services (FPES) to rural households and assesses the contributions of forests to the annual incomes of households in Africa's Miombo woodlands. The study employed focus group meetings, in-depth interviews, and interviews of households, as stratified by wealth class and head of household gender in Copperbelt, Zambia. The results show that FPES are vitally important in providing food, medicine, fodder, and construction materials to rural livelihoods. FPES provided 43.9 % of the average household's income and contributed a 10 % income equalisation effect among households, as revealed by the Gini-coefficient analysis. Poorer households received a lower mean annual income from forests than did their intermediate and wealthy counterparts, but in relative terms, forest income made the greatest contribution to the total household incomes of poor households. When stratified by gender, forests contributed 44.4 and 41.8 % of the income of male-and femaleheaded households, respectively. The study indicates that wealth, rather than gender, was the key determinant of a household's engagement in the sale of FPES. The inter-and intra-community differentiation in the use and sale of FPES, as revealed in this study, enables more effective targeting of forest management interventions and informs efforts to reconcile the goals of poverty reduction and forest conservation.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1639e250a1310">
            <mixed-citation id="d1639e254" publication-type="other">
Adhikari, B., Di Falco, S., &amp; Lovett, J. C. (2004). Household characteristics and forest dependency:
Evidence from common property forest management in Nepal. Ecological Economics, 48(2),
245-257.</mixed-citation>
         </ref>
         <ref id="d1639e267a1310">
            <mixed-citation id="d1639e271" publication-type="other">
Alelign, A., Yemshaw, Y., Teketay, D., &amp; Edwards, S. (2011). Socio-economic factors affecting
sustainable utilization of woody species in Zegie Peninsula, northwestern Ethiopia. Tropical
Ecology, 52(1), 13-24.</mixed-citation>
         </ref>
         <ref id="d1639e284a1310">
            <mixed-citation id="d1639e288" publication-type="other">
Ambrose-Oji, B. (2003). The contribution of NTFPs to the livelihoods of the 'forest poor': Evidence from
the tropical forest zone of south-west Cameroon. International Forestry Review, 5(2), 106-117.</mixed-citation>
         </ref>
         <ref id="d1639e298a1310">
            <mixed-citation id="d1639e302" publication-type="other">
Babulo, B., Muys, B., Nega, F., Tollens, E., Nyssen, J., Deckers, J., et al. (2008). Household livelihood
strategies and forest dependence in the highlands of Tigray Northern Ethiopia. Agricultural Systems,
98(2), 147-155.</mixed-citation>
         </ref>
         <ref id="d1639e316a1310">
            <mixed-citation id="d1639e320" publication-type="other">
Belcher, B., Ruiz-Pérez, M., &amp; Achidawan, R. (2005). Global patterns and trends in the use and
management of commercial NTFPs: Implications for livelihoods and conservation. World
Development, 33(9), 1435-1452.</mixed-citation>
         </ref>
         <ref id="d1639e333a1310">
            <mixed-citation id="d1639e337" publication-type="other">
Blake, B., Kasanga, K., Adam, M., Nsiah-Gyabaah, K., Pender, J., Quashie-Sam, S. J., Warburton, K.,
Williams, K. (1997). Kumasi Natural Research Management Research Project Inception Report.
The University of Greenwich, p. 133.</mixed-citation>
         </ref>
         <ref id="d1639e350a1310">
            <mixed-citation id="d1639e354" publication-type="other">
Cavendish, W. (2000). Empirical regularities in the poverty -environment relationship of rural households:
Evidence from Zimbabwe. World Development, 28(11), 1979-2003.</mixed-citation>
         </ref>
         <ref id="d1639e364a1310">
            <mixed-citation id="d1639e368" publication-type="other">
Central Statistics Office. (2005). Living conditions monitoring survey report 2004. Lusaka: Central
Statistics Office.</mixed-citation>
         </ref>
         <ref id="d1639e378a1310">
            <mixed-citation id="d1639e382" publication-type="other">
Chidumayo, E. N. (1987). Woodland structure, destruction and conservation in the Copperbelt area of
Zambia. Biological Conservation, 40(2), 89-100.</mixed-citation>
         </ref>
         <ref id="d1639e392a1310">
            <mixed-citation id="d1639e396" publication-type="other">
Chirwa, P. W., Syampungani, S., &amp; Geldenhuys, C. J. (2008). The ecology and management of the
Miombo woodlands for sustainable livelihoods in southern Africa: the case for non-timber forest
products. Southern Forests, 70(3), 237-245.</mixed-citation>
         </ref>
         <ref id="d1639e410a1310">
            <mixed-citation id="d1639e414" publication-type="other">
Cocks, M. L., Bangay, L., Shackleton, C. M., &amp; Wiersum, K. F. (2008). 'Rich man poor man'-inter-
household and community factors influencing the use of wild plant resources amongst rural
households in South Africa. International Journal of Sustainable Development and World Ecology,
15(3), 198-210.</mixed-citation>
         </ref>
         <ref id="d1639e430a1310">
            <mixed-citation id="d1639e434" publication-type="other">
Davidar, P., Arjunan, M., &amp; Puyravaud, J.-P. (2008). Why do local households harvest forest products? A
case study from the southern Western Ghats India. Biological Conservation, 141(1), 1876-1884.</mixed-citation>
         </ref>
         <ref id="d1639e444a1310">
            <mixed-citation id="d1639e448" publication-type="other">
de Merode, E., Homewood, K., &amp; Cowlishaw, G. (2004). The value of bushmeat and other wild foods to
rural households living in extreme poverty in Democratic Republic of Congo. Biological
Conservation, 118(5), 573-581.</mixed-citation>
         </ref>
         <ref id="d1639e461a1310">
            <mixed-citation id="d1639e465" publication-type="other">
Dewees, P. A., Campbell, B. M., Katerere, Y., Sitoe, A., Cunninghams, A. B., Angelsen, A., et al. (2010).
Managing the Miombo woodlands of southern Africa: Policies, incentives and options for the rural
poor. Natural Resources Policy Research, 2(1), 57-73.</mixed-citation>
         </ref>
         <ref id="d1639e478a1310">
            <mixed-citation id="d1639e482" publication-type="other">
Ellis, F. (2000). The determinants of rural livelihood diversification in developing countries. Journal of
Agricultural Economics, 51(2), 289-302.</mixed-citation>
         </ref>
         <ref id="d1639e492a1310">
            <mixed-citation id="d1639e496" publication-type="other">
FAO. (2010). Global forest resource assessment. Rome: FAO.</mixed-citation>
         </ref>
         <ref id="d1639e504a1310">
            <mixed-citation id="d1639e508" publication-type="other">
Fisher, M. (2004). Household welfare and forest dependence in Southern Malawi. Environment and
Development Economics, 9(02), 135-154.</mixed-citation>
         </ref>
         <ref id="d1639e518a1310">
            <mixed-citation id="d1639e522" publication-type="other">
Fisher, B., Lewis, S. L., Burgess, N. D., Malimbwi, R. E., Munishi, P. K., Swetnam, R. D., et al. (2011).
Implementation and opportunity costs of reducing deforestation and forest degradation in Tanzania.
Nature Climate Change, 1(3), 161-164.</mixed-citation>
         </ref>
         <ref id="d1639e535a1310">
            <mixed-citation id="d1639e539" publication-type="other">
Fonjong, L. (2008). Gender roles and practices in natural resources management in the North West
Province of Cameroon. Local Environment, 13(5), 461-475.</mixed-citation>
         </ref>
         <ref id="d1639e549a1310">
            <mixed-citation id="d1639e553" publication-type="other">
Gbadegesin, A. (1996). Management of forest resources by women: A case study from the Olokemeji
forest reserve area, southwestern Nigeria. Environmental Conservation, 23(2), 115-119.</mixed-citation>
         </ref>
         <ref id="d1639e563a1310">
            <mixed-citation id="d1639e567" publication-type="other">
GRZ. (1998). An overview of Copperbelt Forestry Action Plan. Ndola: PFAP.</mixed-citation>
         </ref>
         <ref id="d1639e574a1310">
            <mixed-citation id="d1639e578" publication-type="other">
Hetherington, J. C. (1975). Samples? What shape? How large? How many? Scottish Forestry, 29,
260-267.</mixed-citation>
         </ref>
         <ref id="d1639e589a1310">
            <mixed-citation id="d1639e593" publication-type="other">
Heubach, K., Wittig, R., Nuppenau, E.-A., &amp; Hahn, K. (2011). The economic importance of non-timber
forest products (NTFPs) for livelihood maintenance of rural west African communities: A case study
from northern Benin. Ecological Economics, 70(11), 1991-2001.</mixed-citation>
         </ref>
         <ref id="d1639e606a1310">
            <mixed-citation id="d1639e610" publication-type="other">
Hill, P. (1986). Development economics on trial: the anthropological case for a prosecution. Cambridge:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1639e620a1310">
            <mixed-citation id="d1639e624" publication-type="other">
Jumbe, C. B. L., Bwalya, S. M., &amp; Husselman, M. (2009). Contribution of dry forests to rural livelihoods
and the national economy in Zambia. XIII World Forestry Congress (pp. 18-23). Argentina: Buenos
Aires.</mixed-citation>
         </ref>
         <ref id="d1639e637a1310">
            <mixed-citation id="d1639e641" publication-type="other">
Kalaba, F. K., Chirwa, P. W., Prozesky, H., &amp; Ham, C. (2009). The role of indigenous fruit trees in rural
livelihoods: The case of communities in Mwekera area, Copperbelt Province, Zambia. Acta Hort
(ISHS), 806, 129-136.</mixed-citation>
         </ref>
         <ref id="d1639e654a1310">
            <mixed-citation id="d1639e658" publication-type="other">
Kamanga, P., Vedeld, P., &amp; Sjaastad, E. (2009). Forest incomes and rural livelihoods in Chiradzulu
District, Malawi. Ecological Economics, 68(3), 613-624.</mixed-citation>
         </ref>
         <ref id="d1639e668a1310">
            <mixed-citation id="d1639e672" publication-type="other">
Kepe, T. (2008). Beyond the numbers: Understanding the value of vegetation to rural livelihoods in
Africa. Geoforum, 39(2), 958-968.</mixed-citation>
         </ref>
         <ref id="d1639e683a1310">
            <mixed-citation id="d1639e687" publication-type="other">
Kideghesho, J. R., &amp; Msuya, T. S. (2010). Gender and socio-economic factors influencing domestication
of indigenous medicinal plants in the West Usambara Mountains, northern Tanzania. International
Journal of Biodiversity Science Ecosystem Services and Management, 6(1-2), 3-12.</mixed-citation>
         </ref>
         <ref id="d1639e700a1310">
            <mixed-citation id="d1639e704" publication-type="other">
Kiptot, E., &amp; Franzel, S. (2012). Gender and agroforestry in Africa: A review of women's participation.
Agroforestry Systems, 84(1), 35-58.</mixed-citation>
         </ref>
         <ref id="d1639e714a1310">
            <mixed-citation id="d1639e718" publication-type="other">
MA. (2005). Ecosystems and human well-being: Synthesis Washington. DC: World Resource Institute.</mixed-citation>
         </ref>
         <ref id="d1639e725a1310">
            <mixed-citation id="d1639e729" publication-type="other">
Maass, J. M., Balvanera, P., Castillo, A., Daily, G. C., Mooney, H. A., Ehrlich, P., et al. (2005).
Ecosystem services of tropical dry forests: Insights from long-term ecological and social research on
the Pacific Coast of Mexico. Ecology and Society, 10(1), 1-17.</mixed-citation>
         </ref>
         <ref id="d1639e742a1310">
            <mixed-citation id="d1639e746" publication-type="other">
Malla, Y. B., Neupane, H. R., &amp; Branney, P. J. (2003). Why aren't poor people benefiting more from
community forestry? Journal of forest and livelihood , 3, 78-92.</mixed-citation>
         </ref>
         <ref id="d1639e756a1310">
            <mixed-citation id="d1639e760" publication-type="other">
Mamo, G., Sjaastad, E., &amp; Vedeld, P. (2007). Economic dependence on forest resources: A case from
Dendi district, Ethiopia. Forest Policy and Economics, 9(8), 916-927.</mixed-citation>
         </ref>
         <ref id="d1639e771a1310">
            <mixed-citation id="d1639e775" publication-type="other">
McSweeney, K. (2004). Forest product sale as natural insurance: the effects of household characteristics
and the nature of shock in eastern Honduras. Society and Natural Resources, 17(1), 39-56.</mixed-citation>
         </ref>
         <ref id="d1639e785a1310">
            <mixed-citation id="d1639e789" publication-type="other">
Mitchell, J. C., &amp; Barnes, J. A. (1950). The Lamba village: Report of a social survey University of Cape
Town.</mixed-citation>
         </ref>
         <ref id="d1639e799a1310">
            <mixed-citation id="d1639e803" publication-type="other">
Pardo, A., de Juan, J. A., &amp; Pardo, J. E. (2001). Post-harvest physiology, quality, and conservation of the
cultivated mushroom, Agaricus bisporus (Lange) Imbach. Alimentaria, 38(322), 107-117.</mixed-citation>
         </ref>
         <ref id="d1639e813a1310">
            <mixed-citation id="d1639e817" publication-type="other">
Paumgarten, F., &amp; Shackleton, C. M. (2009). Wealth differentiation in household use and trade in non-
timber forest products in South Africa. Ecological Economics, 68(12), 2950-2959.</mixed-citation>
         </ref>
         <ref id="d1639e827a1310">
            <mixed-citation id="d1639e831" publication-type="other">
Paumgarten, F., &amp; Shackleton, C. M. (2011). The role of non-timber forest products in household coping
strategies in South Africa: The influence of household wealth and gender. Population and
Environment, 33(1), 108-131.</mixed-citation>
         </ref>
         <ref id="d1639e844a1310">
            <mixed-citation id="d1639e848" publication-type="other">
Pearson, G. A. (1937). Conservation and use of forests in the southwest. Scientific Monthly, 45, 150-157.</mixed-citation>
         </ref>
         <ref id="d1639e856a1310">
            <mixed-citation id="d1639e860" publication-type="other">
Phillips, D., Williams, K., Andrews, G., Clarke, J., Carter, M., Kinsman, P., et al. (1999). Literature
review on peri urban natural resource conceptualisation and management approaches, Final
Technical Report (p. 210). University of Nottingham and University of Liverpool: DFID Natural
Resources Systems Programme.</mixed-citation>
         </ref>
         <ref id="d1639e876a1310">
            <mixed-citation id="d1639e880" publication-type="other">
PRSP. (2002). Poverty Reduction Strategic Paper. Lusaka: Government of the Republic of Zambia.</mixed-citation>
         </ref>
         <ref id="d1639e887a1310">
            <mixed-citation id="d1639e891" publication-type="other">
Reddy, S. R. C., &amp; Chakravarty, S. P. (1999). Forest dependence and income distribution in a subsistence
economy: evidence from India. World Development, 27(1), 1141-1149.</mixed-citation>
         </ref>
         <ref id="d1639e901a1310">
            <mixed-citation id="d1639e905" publication-type="other">
Rodgers, A., Salehe, J., Howard, G. (1996). The biodiversity of Miombo woodlands. In: B. Campbell
(Ed), The Miombo in transition: Woodlands and welfare in Africa (p. 12). Bogor: Center for
international forestry research (CIFOR).</mixed-citation>
         </ref>
         <ref id="d1639e918a1310">
            <mixed-citation id="d1639e922" publication-type="other">
Shackleton, C., &amp; Shackleton, S. (2004). The importance of non-timber forest products in rural livelihood
security and as safety nets: A review of evidence from South Africa. South African Journal of
Science, 100(11-12), 658-664.</mixed-citation>
         </ref>
         <ref id="d1639e935a1310">
            <mixed-citation id="d1639e939" publication-type="other">
Shackleton, C. M., &amp; Shackleton, S. E. (2006). Household wealth status and natural resource use in the
Kat River valley South Africa. Ecological Economics, 57(2), 306-317.</mixed-citation>
         </ref>
         <ref id="d1639e950a1310">
            <mixed-citation id="d1639e954" publication-type="other">
Shackleton, C. M., Shackleton, S. E., Buiten, E., &amp; Bird, N. (2007). The importance of dry woodlands and
forests in rural livelihoods and poverty alleviation in South Africa. Forest Policy and Economics,
9(5), 558-577.</mixed-citation>
         </ref>
         <ref id="d1639e967a1310">
            <mixed-citation id="d1639e971" publication-type="other">
Silverman, S. F. (1966). Ethnographic approach to social stratification-prestige in a central Italian
community. American Anthropologist, 68(4), 899-921.</mixed-citation>
         </ref>
         <ref id="d1639e981a1310">
            <mixed-citation id="d1639e985" publication-type="other">
Simon, D., McGregor, D., &amp; Nsiah-Gyabaah, K. (2004). The changing urban-rural interface of African
cities: Definitional issues and an application to Kumasi, Ghana. Environment and Urbanization,
16(2), 235-248.</mixed-citation>
         </ref>
         <ref id="d1639e998a1310">
            <mixed-citation id="d1639e1002" publication-type="other">
Strauss, A., &amp; Corbin, J. (1990). Basics of qualitative research: Grounded theory procedures and
techniques. Newbury Park: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d1639e1012a1310">
            <mixed-citation id="d1639e1016" publication-type="other">
Sunderlin, W. D., Angelsen, A., Belcher, B., Burgers, P., Nasi, R., Santoso, L., et al. (2005). Livelihoods,
forests, and conservation in developing countries: An overview. World Development, 33(9),
1383-1402.</mixed-citation>
         </ref>
         <ref id="d1639e1029a1310">
            <mixed-citation id="d1639e1033" publication-type="other">
Syampungani, S., Geldenhuys, C. J., &amp; Chirwa, P. W. (2010). The use of species-stem curves in sampling
the development of the Zambian Miombo woodland species in charcoal production and slash-and-
burn regrowth stands. Southern Forests, 72(2), 83-89.</mixed-citation>
         </ref>
         <ref id="d1639e1047a1310">
            <mixed-citation id="d1639e1051" publication-type="other">
Tesfaye, Y., Roos, A., Campbell, B. M., &amp; Bohlin, F. (201 1). Livelihood strategies and the role of forest
income in participatory-managed forests of Dodola area in the bale highlands, southern Ethiopia.
Forest Policy Economics, 13(4), 258-265.</mixed-citation>
         </ref>
         <ref id="d1639e1064a1310">
            <mixed-citation id="d1639e1068" publication-type="other">
Tschakert, P., Coomes, O. T., &amp; Potvin, C. (2007). Indigenous livelihoods, slash-and-burn agriculture,
and carbon stocks in Eastern Panama. Ecological Economics, 60(4), 807-820.</mixed-citation>
         </ref>
         <ref id="d1639e1078a1310">
            <mixed-citation id="d1639e1082" publication-type="other">
Twine, W., Moshe, D., Netshiluvhi, T., &amp; Siphugu, V. (2003). Consumption and direct-use values of
savanna bio-resources used by rural households in Mametja, a semi-arid area of Limpopo province,
South Africa. South African Journal of Science, 99, 467-473.</mixed-citation>
         </ref>
         <ref id="d1639e1095a1310">
            <mixed-citation id="d1639e1099" publication-type="other">
von der Heyden, C. J., &amp; New, M. G. (2004). Groundwater pollution on the Zambian Copperbelt:
deciphering the source and the risk. Science of the Total Environment, 327(1-3), 17-30.</mixed-citation>
         </ref>
         <ref id="d1639e1109a1310">
            <mixed-citation id="d1639e1113" publication-type="other">
Whitford, H. N. (1923). The use of tropical land and tropical forests. Scientific Monthly, 17, 135-145.</mixed-citation>
         </ref>
         <ref id="d1639e1120a1310">
            <mixed-citation id="d1639e1124" publication-type="other">
Yemiru, T., Roos, A., Campbell, B. M., &amp; Bohlin, F. (2010). Forest incomes and poverty alleviation
under participatory forest management in the Bale Highlands, Southern Ethiopia. International
Forestry Review, 72(1), 66-77.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
