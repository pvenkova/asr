<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amerpoliscierevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100077</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The American Political Science Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00030554</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15375943</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23275430</article-id>
         <title-group>
            <article-title>Inequality and Regime Change: Democratic Transitions and the Stability of Democratic Rule</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>STEPHAN</given-names>
                  <surname>HAGGARD</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>ROBERT R.</given-names>
                  <surname>KAUFMAN</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">106</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23275426</issue-id>
         <fpage>495</fpage>
         <lpage>516</lpage>
         <permissions>
            <copyright-statement>© American Political Science Association 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23275430"/>
         <abstract>
            <p>Recent work by Carles Boix and Daron Acemoglu and James Robinson has focused on the role of inequality and distributive conflict in transitions to and from democratic rule. We assess these claims through causal process observation, using an original qualitative dataset on democratic transitions and reversions during the "third wave" from 1980 to 2000. We show that distributive conflict, a key causal mechanism in these theories, is present in just over half of all transition cases. Against theoretical expectations, a substantial number of these transitions occur in countries with high levels of inequality. Less than a third of all reversions are driven by distributive conflicts between elites and masses. We suggest a variety of alternative causal pathways to both transitions and reversions.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1803e174a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1803e181" publication-type="other">
Romer (1975)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e187" publication-type="other">
Roberts (1977)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e194a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1803e201" publication-type="other">
Collier, Brady, and
Seawright 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e210" publication-type="other">
Bennett and
George 2005</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e219" publication-type="other">
George and McKeown 1985</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e226" publication-type="other">
Bates et. al. 1998</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e232" publication-type="other">
Falletti and Lynch 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e238" publication-type="other">
Gerring 2007b</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e244" publication-type="other">
2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e250" publication-type="other">
Hedstrom and Ylikoski 2010</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e257a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1803e264" publication-type="other">
Haggard, Kauf-
man, and Teo 2012</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e274a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1803e281" publication-type="other">
Acemoglu and Robinson 2000</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e287" publication-type="other">
2001</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e295a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1803e304" publication-type="other">
Haggard, Kaufman, and Teo (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e311a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1803e318" publication-type="other">
Haggard, Kaufman, and Teo
(2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e328a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1803e335" publication-type="other">
Armenia (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e341" publication-type="other">
Dominican Republic (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e347" publication-type="other">
Fiji (1987 and 2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e354" publication-type="other">
Haiti (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e360" publication-type="other">
Turkey (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e366" publication-type="other">
Ukraine (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e372" publication-type="other">
Zambia (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e379a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1803e386" publication-type="other">
Ghana (1981)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e392" publication-type="other">
Haiti (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e399a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1803e406" publication-type="other">
Ghana (1981)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e412" publication-type="other">
Armenia (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e418" publication-type="other">
Do-
minican Republic (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e428" publication-type="other">
Ukraine (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1803e434" publication-type="other">
Zambia (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e441a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1803e448" publication-type="other">
Houle (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d1803e456a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1803e463" publication-type="other">
Green and Shapiro (1994)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1803e479a1310">
            <mixed-citation id="d1803e483" publication-type="other">
Acemoglu, Daron, and James A. Robinson. 2000. "Democratization
or Repression?" European Economics Review 44 (4-6): 683-93.</mixed-citation>
         </ref>
         <ref id="d1803e493a1310">
            <mixed-citation id="d1803e497" publication-type="other">
Acemoglu, Daron, and James A. Robinson. 2001. "A Theory of
Political Transitions." American Economic Review 91(4): 938-63.</mixed-citation>
         </ref>
         <ref id="d1803e507a1310">
            <mixed-citation id="d1803e511" publication-type="other">
Acemoglu, Daron, and James A. Robinson. 2006. Economic Origins
of Dictatorship and Democracy. New York: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d1803e524a1310">
            <mixed-citation id="d1803e528" publication-type="other">
Ansell, Ben, and David Samuels. 2010. "Inequality and Democrati-
zation: A Contractarian Approach." Comparative Political Studies
43 (12): 1543-74.</mixed-citation>
         </ref>
         <ref id="d1803e542a1310">
            <mixed-citation id="d1803e546" publication-type="other">
Baker, Chris, and Pasuk Phongpaichit. 2002. Thailand: Economy and
Politics. 2nd ed. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1803e556a1310">
            <mixed-citation id="d1803e560" publication-type="other">
Bates, Robert H., Avner Greif, Margaret Levi, Jean-Laurent Rosen-
thal, and Barry R. Weingast. 1998. Analytic Narratives. Princeton,
NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1803e573a1310">
            <mixed-citation id="d1803e577" publication-type="other">
Beck, Nathaniel. 2006. "Is Causal-process Observation an Oxy-
moron?" Political Analysis 14 (3): 347-52.</mixed-citation>
         </ref>
         <ref id="d1803e587a1310">
            <mixed-citation id="d1803e591" publication-type="other">
Beck, Nathaniel. 2010. "Causal Process 'Observation': Oxymoron or
(Fine) Old Wine." Political Analysis 18 (4): 499-505.</mixed-citation>
         </ref>
         <ref id="d1803e601a1310">
            <mixed-citation id="d1803e605" publication-type="other">
Beissinger, Mark 2002. Nationalist Mobilization and the Collapse of
the Soviet State. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e615a1310">
            <mixed-citation id="d1803e619" publication-type="other">
Boix, Carles. 2003. Democracy and Redistribution. New York: Cam-
bridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e630a1310">
            <mixed-citation id="d1803e634" publication-type="other">
Boix, Carles. 2008. "Economic Roots and Civil Wars and Revolu-
tions in the Contemporary World." World Politics 60 (3): 390-
437.</mixed-citation>
         </ref>
         <ref id="d1803e647a1310">
            <mixed-citation id="d1803e651" publication-type="other">
Boix, Carles 2011. "Democracy, Development, and the International
System." American Political Science Review 105 (4): 809-28.</mixed-citation>
         </ref>
         <ref id="d1803e661a1310">
            <mixed-citation id="d1803e665" publication-type="other">
Brady, Henry E., and David Collier. 2004. Rethinking Social Inquiry,
Diverse Tools, Shared Standards. Lanham, MD: Rowman and Lit-
tlefield.</mixed-citation>
         </ref>
         <ref id="d1803e678a1310">
            <mixed-citation id="d1803e682" publication-type="other">
Bratton, Michael, and Nicolas van de Walle. 1997. Democratic Exper-
iments in Africa: Regime Transitions in Comparative Perspective.
New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e695a1310">
            <mixed-citation id="d1803e699" publication-type="other">
Burkhart, Ross E. 1997. "Comparative Democracy and Income Dis-
tribution: Shape and Direction of the Causal Arrow." Journal of
Politics 59 (1): 148-64.</mixed-citation>
         </ref>
         <ref id="d1803e712a1310">
            <mixed-citation id="d1803e716" publication-type="other">
Cheibub, Jose Antonio, and Jennifer Gandhi. 2004. "Classifying Po-
litical Regimes: A Six-fold Measure of Democracies and Dictator-
ships." Presented at the Annual Meeting of the American Political
Science Association, Chicago.</mixed-citation>
         </ref>
         <ref id="d1803e733a1310">
            <mixed-citation id="d1803e737" publication-type="other">
Cheibub, Jose Antonio, Jennifer Gandhi, and James Raymond
Vreeland. 2010. "Democracy and Dictatorship Revisited." Public
Choice 143 (1/2): 67-101.</mixed-citation>
         </ref>
         <ref id="d1803e750a1310">
            <mixed-citation id="d1803e754" publication-type="other">
Collier, David. 1979. The New Authoritarianism in Latin America.
Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1803e764a1310">
            <mixed-citation id="d1803e768" publication-type="other">
Collier, David, Henry E. Brady, and Jason Seawright. 2010. "Sources
of Leverage in Causal Inference: Toward an Alternative View of
Methodology." In Rethinking Social Inquiry: Diverse Tools, Shared
Standards, eds. Henry E. Brady and David Collier. Lanham, MD:
Rowman and Littlefield, 161-200.</mixed-citation>
         </ref>
         <ref id="d1803e787a1310">
            <mixed-citation id="d1803e791" publication-type="other">
Collier, Paul, and Anke Hoeffler. 2005. "Resource Rents, Gover-
nance, and Conflict." Journal of Conflict Resolution 49 (4): 625-33.</mixed-citation>
         </ref>
         <ref id="d1803e801a1310">
            <mixed-citation id="d1803e805" publication-type="other">
Collier, Ruth Berins. 1999. Paths toward Democracy; Working Class
and Elites in Western Europe and South America. Cambridge:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e818a1310">
            <mixed-citation id="d1803e822" publication-type="other">
Collier, Ruth Berins, and David Collier. 1991. Shaping the Politi-
cal Arena: Critical Junctures, the Labor Movement, and Regime
Dynamics in Latin America. Princeton, NJ: Princeton University
Press.</mixed-citation>
         </ref>
         <ref id="d1803e839a1310">
            <mixed-citation id="d1803e843" publication-type="other">
Diamond, Larry. 2008. The Spirit of Democracy: The Struggle to
Build Free Societies. New York: Times Books.</mixed-citation>
         </ref>
         <ref id="d1803e853a1310">
            <mixed-citation id="d1803e857" publication-type="other">
Drake, Paul W. 1998. Labor Movements and Dictatorships: The
Southern Cone in Comparative Perspective. Baltimore: Johns Hop-
kins University Press.</mixed-citation>
         </ref>
         <ref id="d1803e870a1310">
            <mixed-citation id="d1803e874" publication-type="other">
Dutt, Pushan, and Devashish Mitra. 2008. "Inequality and the Insta-
bility of Polity and Policy." Economic Journal 118 (531): 1285-1314.</mixed-citation>
         </ref>
         <ref id="d1803e884a1310">
            <mixed-citation id="d1803e888" publication-type="other">
Epstein, David L., Robert Bates, Jack Goldstone, Ida Kristensen,
and Sharyn O'Halloran. 2006. "Democratic Transitions." Ameri-
can Journal of Political Science 50 (3): 551-69.</mixed-citation>
         </ref>
         <ref id="d1803e901a1310">
            <mixed-citation id="d1803e905" publication-type="other">
Falleti, Tulia G., and Julia F. Lynch. 2010. "Context and Causal Mech-
anisms in Political Analysis." Comparative Political Studies 42 (9):
1143-66.</mixed-citation>
         </ref>
         <ref id="d1803e918a1310">
            <mixed-citation id="d1803e922" publication-type="other">
Fearon, James D., and David D. Laitin. 2008. "Integrating Quantita-
tive and Qualitative Methods." In Oxford Handbook of Political
Methodology, eds. Janet M. Box-Steffensmeier, Henry E. Brady,
and David Collier. Oxford: Oxford University Press, 756—78.</mixed-citation>
         </ref>
         <ref id="d1803e939a1310">
            <mixed-citation id="d1803e943" publication-type="other">
Freeman, John R., and Dennis P. Quinn. 2012. "The Economic Ori-
gins of Democracy Reconsidered." American Political Science Re-
view 106 (1): 58-80.</mixed-citation>
         </ref>
         <ref id="d1803e956a1310">
            <mixed-citation id="d1803e960" publication-type="other">
Gasiorowski, Mark J. 1995. "Economic Crisis and Political Regime
Change: An Event History Analysis." American Political Science
Review 89 (4): 882-97.</mixed-citation>
         </ref>
         <ref id="d1803e973a1310">
            <mixed-citation id="d1803e977" publication-type="other">
Geddes, Barbara 1999. "What Do We Know about Democratic Tran-
sitions after Twenty Years?" Annual Review of Political Science 2:
115-44.</mixed-citation>
         </ref>
         <ref id="d1803e990a1310">
            <mixed-citation id="d1803e994" publication-type="other">
George, Alexander L., and Andrew Bennett. 2005. Case Studies and
Theory Development in the Social Sciences. Cambridge, MA: MIT
Press.</mixed-citation>
         </ref>
         <ref id="d1803e1007a1310">
            <mixed-citation id="d1803e1011" publication-type="other">
George, Alexander L., and Timothy J. McKeown. 1985. "Case Stud-
ies and Theories of Organizational Decision Making." Advances
in Information Processing in Organizations 2: 21-58.</mixed-citation>
         </ref>
         <ref id="d1803e1024a1310">
            <mixed-citation id="d1803e1028" publication-type="other">
Gerring, John. 2006. "Single-outcome Studies: A Methodological
Primer." International Sociology 21 (5): 707-34.</mixed-citation>
         </ref>
         <ref id="d1803e1039a1310">
            <mixed-citation id="d1803e1043" publication-type="other">
Gerring, John. 2007a. Case Study Research: Principles and Practices.
New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1053a1310">
            <mixed-citation id="d1803e1057" publication-type="other">
Gerring, John. 2007b. "The Mechanismic Worldview: Thinking inside
the Box." British Journal of Political Science 38 (1): 161-79.</mixed-citation>
         </ref>
         <ref id="d1803e1067a1310">
            <mixed-citation id="d1803e1071" publication-type="other">
Gerring, John. 2010. "Causal Mechanisms: Yes, but " Comparative
Political Studies 42 (3): 327-59.</mixed-citation>
         </ref>
         <ref id="d1803e1081a1310">
            <mixed-citation id="d1803e1085" publication-type="other">
Gervais, Myriam. 1997. "Niger: Regime Change, Economic Crisis
and the Perpetuation of Privilege." In Political Reform in Franco-
phone Africa, eds. John F. Clark and David Gardinier. Boulder,
CO: Westview Press, 86-108.</mixed-citation>
         </ref>
         <ref id="d1803e1101a1310">
            <mixed-citation id="d1803e1105" publication-type="other">
Graham, Yao. 1985. "The Politics of Crisis in Ghana: Class Struggle
and Organization, 1981-1984." Review of African Political Econ-
omy 12 (34): 54-68.</mixed-citation>
         </ref>
         <ref id="d1803e1118a1310">
            <mixed-citation id="d1803e1122" publication-type="other">
Green, Donald P., and Ian Shapiro. 1994. Pathologies of Rational
Choice Theory. New Haven, CT: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1133a1310">
            <mixed-citation id="d1803e1137" publication-type="other">
Hafner-Burton, Emilie, and James Ron. 2009. "Seeing Double: Hu-
man Rights Impact through Qualitative and Quantitative Eyes?"
World Politics 61 (2): 360-401.</mixed-citation>
         </ref>
         <ref id="d1803e1150a1310">
            <mixed-citation id="d1803e1154" publication-type="other">
Haggard, Stephan, and Robert R. Kaufman. 1995. The Political
Economy of Democratic Transitions. Princeton, NJ: Princeton
University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1167a1310">
            <mixed-citation id="d1803e1171" publication-type="other">
Haggard, Stephan, Robert R. Kaufman, and Terence Teo. 2012. "Dis-
tributive Conflict and Regime Change: A Qualitative Dataset."
http://hdl.handle.net/1902.1/18276 VI (accessed June 1,2012).</mixed-citation>
         </ref>
         <ref id="d1803e1184a1310">
            <mixed-citation id="d1803e1188" publication-type="other">
Hedstrom, Peter, and Petri Ylikoski. 2010. "Causal Mechanisms in
the Social Sciences." Annual Review of Sociology 36: 49-67.</mixed-citation>
         </ref>
         <ref id="d1803e1198a1310">
            <mixed-citation id="d1803e1202" publication-type="other">
Houle, Christian 2009. "Inequality and Democracy: Why Inequal-
ity Harms Consolidation but Does Not Affect Democratization,"
World Politics 61 (4): 589-622.</mixed-citation>
         </ref>
         <ref id="d1803e1215a1310">
            <mixed-citation id="d1803e1219" publication-type="other">
Huntington, Samuel P. 1991. The Third Wave: Democratization in the
Late Twentieth Century. Norman: University of Oklahoma Press.</mixed-citation>
         </ref>
         <ref id="d1803e1230a1310">
            <mixed-citation id="d1803e1234" publication-type="other">
Hutchful, Eboe. 1997. "Military Policy and Reform in Ghana." Jour-
nal of Modern African Studies 35 (2): 251-78.</mixed-citation>
         </ref>
         <ref id="d1803e1244a1310">
            <mixed-citation id="d1803e1248" publication-type="other">
Im, Hyung Baeg. 1987. "The Rise of Bureaucratic Authoritarianism
in South Korea." World Politics 39 (2): 231-57.</mixed-citation>
         </ref>
         <ref id="d1803e1258a1310">
            <mixed-citation id="d1803e1262" publication-type="other">
King, Gary, Robert O. Keohane, and Sidney Verba. 1994. Designing
Social Inquiry: Scientific Inference in Qualitative Research. Prince-
ton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1275a1310">
            <mixed-citation id="d1803e1279" publication-type="other">
Kitschelt, Herbert, and Steven Wilkinson, eds. 2006. Patrons or Poli-
cies? Patterns of Democratic Accountability and Political Compe-
tition. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1292a1310">
            <mixed-citation id="d1803e1296" publication-type="other">
Kricheli, Ruth, and Yair Livne. 2011. "Mass Revolutions vs.
Elite Coups." Presented at the Annual Meeting of the Amer-
ican Political Science Association, Toronto, http://ssrn.com/
abstract=1449852 (accessed June 1,2012).</mixed-citation>
         </ref>
         <ref id="d1803e1312a1310">
            <mixed-citation id="d1803e1316" publication-type="other">
Kubik, Jan 1994. The Power of Symbols against the Symbols of Power:
The Rise of Solidarity and the Fall of State Socialism in Poland.
University Park: Pennsylvania University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1330a1310">
            <mixed-citation id="d1803e1334" publication-type="other">
Lambert, Peter. 2000. "A Decade of Electoral Democracy: Continu-
ity, Change, and Crisis in Paraguay." Bulletin of Latin American
Research 19 (3): 379-96.</mixed-citation>
         </ref>
         <ref id="d1803e1347a1310">
            <mixed-citation id="d1803e1351" publication-type="other">
Lehoucq, Fabrice, and Anibal Perez-Linan. 2009. "Regimes,
Competition, and Military Coups in Latin America." Pre-
sented at the Annual Meeting of the American Political
Science Association, Toronto, http://papers.ssrn.com/sol3/papers.
cfm?abstract_id=1449035 (accessed June 1,2012).</mixed-citation>
         </ref>
         <ref id="d1803e1370a1310">
            <mixed-citation id="d1803e1374" publication-type="other">
Lemarchand, Renee. 1996. Burundi: Ethnic Conflict and Genocide.
Washington, DC: Woodrow Wilson Press.</mixed-citation>
         </ref>
         <ref id="d1803e1384a1310">
            <mixed-citation id="d1803e1388" publication-type="other">
Levitsky, Steven, and Lucan A. Way. 2010. Competitive Authoritari-
anism: Hybrid Regimes after the Cold War. New York: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1401a1310">
            <mixed-citation id="d1803e1405" publication-type="other">
Linz, Juan, and Alfred Stepan. 1978. The Breakdown of Democratic
Regimes. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1415a1310">
            <mixed-citation id="d1803e1419" publication-type="other">
Linz, Juan, and Alfred Stepan. 1996. Problems of Democratic Tran-
sition and Consolidation. Baltimore: Johns Hopkins University
Press.</mixed-citation>
         </ref>
         <ref id="d1803e1433a1310">
            <mixed-citation id="d1803e1437" publication-type="other">
Lipset, Seymour M. 1960. Political Man: The Social Bases of Politics.
Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1447a1310">
            <mixed-citation id="d1803e1451" publication-type="other">
Londregan, John B., and Keith T. Poole. 1990. "Poverty, the Coup
Trap, and the Seizure of Executive Power." World Politics 42 (2):
151-83.</mixed-citation>
         </ref>
         <ref id="d1803e1464a1310">
            <mixed-citation id="d1803e1468" publication-type="other">
Magaloni, Beatriz, and Ruth Kricheli. 2010. "Political Order and
One-party Rule." Annual Review of Political Science 13:123—43.</mixed-citation>
         </ref>
         <ref id="d1803e1478a1310">
            <mixed-citation id="d1803e1482" publication-type="other">
Mainwaring, Scott, Daniel Brinks, and Anibal Perez-Linan. 2001.
"Classifying Political Regimes in Latin America, 1945-1999."
Studies in Comparative International Development 36 (1): 37-65.</mixed-citation>
         </ref>
         <ref id="d1803e1495a1310">
            <mixed-citation id="d1803e1499" publication-type="other">
Marshall, Thomas Humphrey. 1963. Class, Citizenship, and Social
Development: Essays. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1803e1509a1310">
            <mixed-citation id="d1803e1513" publication-type="other">
Marshall, Monty G., Ted Robert Gurr, and Keith Jaggers. 2010.
Polity IV Project: Political Regime Characteristics and Tran-
sitions, 1800-2009. Vienna, VA: Center for Systemic Peace.
http://www.systemicpeace.org/polity/polity4.htm (accessed June 1,
2012).</mixed-citation>
         </ref>
         <ref id="d1803e1533a1310">
            <mixed-citation id="d1803e1537" publication-type="other">
Marshall, Monty G., and Donna Ramsey Marshall. 2010. Coup d'etat
Events, 1946-2009. Vienna, VA: Center for Systemic Peace.</mixed-citation>
         </ref>
         <ref id="d1803e1547a1310">
            <mixed-citation id="d1803e1551" publication-type="other">
McAdam, Doug, Sidney Tarrow, and Charles Tilly 2003. Dynamics
of Contention. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1561a1310">
            <mixed-citation id="d1803e1565" publication-type="other">
McGowan, Patrick K. 2007. "African Military Intervention Events,
January 1,1955, to December 31,2006." Arizona State University.
Dataset.</mixed-citation>
         </ref>
         <ref id="d1803e1578a1310">
            <mixed-citation id="d1803e1582" publication-type="other">
Meltzer, Allan H., and Scott E Richard. 1981. "A Rational Theory
of the Size of Government." Journal of Political Economy 89 (5):
917-27.</mixed-citation>
         </ref>
         <ref id="d1803e1595a1310">
            <mixed-citation id="d1803e1599" publication-type="other">
Moore, Barrington, Jr. 1966. Social Origins of Dictatorship and
Democracy. Boston: Beacon Press.</mixed-citation>
         </ref>
         <ref id="d1803e1609a1310">
            <mixed-citation id="d1803e1613" publication-type="other">
O'Donnell, Guillermo A. 1973. Modernization and Bureaucratic-
authoritarianism: Studies in South American Politics. Berkeley:
University of California Press.</mixed-citation>
         </ref>
         <ref id="d1803e1627a1310">
            <mixed-citation id="d1803e1631" publication-type="other">
O'Donnell, Guillermo A., Philippe Schmitter, and Laurence
Whitehead, eds. 1986. Transitions from Authoritarian Rule: Tenta-
tive Conclusions about Uncertain Democracies. Baltimore: Johns
Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1647a1310">
            <mixed-citation id="d1803e1651" publication-type="other">
Olson, Mancur. 1965. The Logic of Collective Action: Public Goods
and the Theory of Groups. Cambridge, MA: Harvard University
Press.</mixed-citation>
         </ref>
         <ref id="d1803e1664a1310">
            <mixed-citation id="d1803e1668" publication-type="other">
Persson, Torsten, and Guido Tabellini. 1994. "Is Inequality Harmful
for Economic Growth?" American Economic Review 84: 600-21.</mixed-citation>
         </ref>
         <ref id="d1803e1678a1310">
            <mixed-citation id="d1803e1682" publication-type="other">
Pierson, Paul. 2004. Politics in Time: History, Institutions, and Social
Analysis. Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1692a1310">
            <mixed-citation id="d1803e1696" publication-type="other">
Przeworski, Adam. 2009. "Conquered or Granted? A History of
Suffrage Extensions." British Journal of Political Science 39 (2):
291-321.</mixed-citation>
         </ref>
         <ref id="d1803e1709a1310">
            <mixed-citation id="d1803e1713" publication-type="other">
Przeworski, Adam, Michael Alvarez, Jose Antonio Cheibub, and
Fernando Limongi. 2000. Democracy and Development: Political
Institutions and Well-being in the World, 1950-1990. Cambridge:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1730a1310">
            <mixed-citation id="d1803e1734" publication-type="other">
Reenock, Christopher, Michael Bernhard, and David Sobek. 2007.
"Regressive Socioeconomic Distribution and Democratic Sur-
vival." International Studies Quarterly 51 (3): 677-99.</mixed-citation>
         </ref>
         <ref id="d1803e1747a1310">
            <mixed-citation id="d1803e1751" publication-type="other">
Roberts, Kevin W. S. 1977. "Voting over Income Tax Schedules."
Journal of Public Economics 8 (3): 329^10.</mixed-citation>
         </ref>
         <ref id="d1803e1761a1310">
            <mixed-citation id="d1803e1765" publication-type="other">
Romer, Thomas. 1975. "Individual Welfare, Majority Voting, and the
Properties of a Linear Income Tax." Journal of Public Economics
4 (2): 163-85.</mixed-citation>
         </ref>
         <ref id="d1803e1778a1310">
            <mixed-citation id="d1803e1782" publication-type="other">
Rueschemeyer, Dietrich, Evelyne Huber Stephens, and John
Stephens. 1992. Capitalist Development and Democracy. Chicago:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1803e1795a1310">
            <mixed-citation id="d1803e1799" publication-type="other">
Schmitter, Philippe C. 1974. "Still the Century of Corporatism?"
Review of Politics 36 (1): 85-131.</mixed-citation>
         </ref>
         <ref id="d1803e1809a1310">
            <mixed-citation id="d1803e1813" publication-type="other">
Slater, Dan, and Benjamin Smith. 2012. "Economic Origins of Demo-
cratic Breakdown? The Redistributive Model and the Postcolonial
State." University of Chicago. Unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="d1803e1827a1310">
            <mixed-citation id="d1803e1831" publication-type="other">
Stepan, Alfred 2001. Arguing Comparative Politics. Oxford: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1841a1310">
            <mixed-citation id="d1803e1845" publication-type="other">
Teorell, Jan. 2010. Determinants of Democratization: Explaining
Regime Change in the World, 1972-2006. Cambridge: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1858a1310">
            <mixed-citation id="d1803e1862" publication-type="other">
Udo, Augustine. 1985. "Class, Party Politics, and the 1983 Coup in
Nigeria." Africa Spectrum 20 (3): 327-38.</mixed-citation>
         </ref>
         <ref id="d1803e1872a1310">
            <mixed-citation id="d1803e1876" publication-type="other">
University of Texas Inequality Project. 2008. Estimated House-
hold Income Inequality (EHII) Dataset. http://utip.gov.utexas.
edu/data.html (accessed June 1,2012).</mixed-citation>
         </ref>
         <ref id="d1803e1889a1310">
            <mixed-citation id="d1803e1893" publication-type="other">
Valenzuela, Arturo 1978. The Breakdown of Democratic Regimes:
Chile. Baltimore: Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d1803e1903a1310">
            <mixed-citation id="d1803e1907" publication-type="other">
Vanhanen, Tatu. 2003. "Democratization and Power Resources
1850-2000." FSD1216, version 1.0 (2003-03-10). University
of Tampere, Department of Political Science. Finnish So-
cial Science Data Archive, http://www.fsd.uta.fi/english/
data/catalogue/FSD1216/meF1216e.html (accessed June 1,
2012).</mixed-citation>
         </ref>
         <ref id="d1803e1931a1310">
            <mixed-citation id="d1803e1935" publication-type="other">
Weyland, Kurt. 1996. "Neoliberalism and Neopopulism in Latin
America: Unexpected Affinities." Studies in Comparative Inter-
national Development 31 (3): 3-31.</mixed-citation>
         </ref>
         <ref id="d1803e1948a1310">
            <mixed-citation id="d1803e1952" publication-type="other">
Whitehead, Lawrence, ed. 1996. The International Dimensions of
Democratization: Europe and the Americas. Oxford: Oxford Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d1803e1965a1310">
            <mixed-citation id="d1803e1969" publication-type="other">
World Bank. 2010. World Development Indicators 2010. Washington
DC: World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
