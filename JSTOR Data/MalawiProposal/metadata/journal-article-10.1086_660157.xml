<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">compeducrevi</journal-id>
         <journal-id journal-id-type="ucp-id">CER</journal-id>
         <journal-title-group>
            <journal-title>Comparative Education Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00104086</issn>
         <issn pub-type="epub">1545701X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">CER1287</article-id>
         <article-id pub-id-type="doi">10.1086/660157</article-id>
         <title-group>
            <article-title>School Resources and Academic Performance in Sub-Saharan Africa<xref ref-type="fn" rid="fn1"/>
            </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Valerie E.</given-names>
                  <x xml:space="preserve"/>
                  <surname>Lee</surname>
               </string-name>
               <x xml:space="preserve">and</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Tia Linda</given-names>
                  <x xml:space="preserve"/>
                  <surname>Zuze</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date pub-type="ppub">
                <day>01</day>
                <month>08</month>
                <year>2011</year>
                <string-date>August 2011</string-date>
            </pub-date>
         <volume>55</volume>
         <issue>3</issue>
         <issue-id>659994</issue-id>
         <fpage>369</fpage>
         <lpage>397</lpage>
         <permissions>
            <copyright-statement>© 2011 by the Comparative and International Education Society. All rights reserved.</copyright-statement>
            <copyright-year>2011</copyright-year>
            <copyright-holder>The Comparative and International Education Society.</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/660157"/>
         <abstract>
            <p>We investigate links between students’ achievement and several resource inputs in African primary schools, using data from the 2000 Southern and Eastern Africa Consortium for Monitoring Educational Quality (SACMEQ-II). We focused on four sub-Saharan countries that had in place legislation mandating free and universal primary schooling: Botswana, Malawi, Namibia, and Uganda. Using multilevel modeling (hierarchical linear models), we found strong links between material and human resources and grade 6 students’ achievement in reading and mathematics. Structural features such as school shifts and school size were negatively associated with achievement, although effects varied across countries. We discuss policy implications of our findings within each country’s educational context.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <app-group>
         <app id="app1">
            <label>Appendix</label>
            <title>Equations Used in the HLM Analysis</title>
            <p>The equations for the three steps involved in the HLM analysis are described below. In step 1, we partitioned the variances in mathematics and reading achievement into their within- and between-school components. The equation for the unconditional model is<disp-formula id="df1">
                  <inline-graphic mimetype="image" xlink:type="simple" xlink:href="df1.eps"/>
               </disp-formula>where<italic>Y<sub>ij</sub>
               </italic>is the estimated mathematics or reading score for student<italic>i</italic>in school<italic>j</italic>, γ<sub>00</sub>is the overall average value of mathematics and reading achievement,<italic>u</italic>
               <sub>0<italic>j</italic>
               </sub>is the estimated variation in achievement at the school level, and<italic>r<sub>ij</sub>
               </italic>is the estimated variation in achievement at the student level.</p>
            <p>The step 1 HLMs also included an estimate of the proportion of outcome variance that occurred between schools, or ICC. The higher the ICC, the larger are the systematic differences in achievement scores between schools. The school level variation<inline-formula id="if4">
                  <inline-graphic mimetype="image" xlink:type="simple" xlink:href="if4.eps"/>
               </inline-formula>is estimated as a proportion of total variation (student and school). Student variation is represented as σ<sup>2</sup>and the ICC (ρ), calculated as<disp-formula id="df2">
                  <inline-graphic mimetype="image" xlink:type="simple" xlink:href="df2.eps"/>
               </disp-formula>
            </p>
            <p>In step 2, mathematics and reading achievement for student<italic>i</italic>in school<italic>j</italic>(<italic>Y<sub>ij</sub>
               </italic>) was regressed on SES, gender, and grade-repetition history. The variance estimate (<italic>r<sub>ij</sub>
               </italic>) was now a residual after adjusting for student characteristics:<disp-formula id="df3">
                  <inline-graphic mimetype="image" xlink:type="simple" xlink:href="df3.eps"/>
               </disp-formula>
            </p>
            <p>In step 3, average academic achievement in reading and mathematics was estimated in two ways. The first method was unadjusted for school-level controls. In the second series of models, school-effect estimates were adjusted for school average SES, percentage of repeating students, and urban/rural school location. Both forms involved modeling school effects on mean achievement (the intercept). The unadjusted and adjusted models for school resources are shown below. The same equations apply to estimates of shift schooling, teacher quality, teacher attendance problems, average class size, and school size.</p>
            <p>School resources (unadjusted):<disp-formula id="df111">
                  <inline-graphic mimetype="image" xlink:type="simple" xlink:href="df111.eps"/>
               </disp-formula>
            </p>
            <p>School resources (adjusted):<disp-formula id="df222">
                  <inline-graphic mimetype="image" xlink:type="simple" xlink:href="df222.eps"/>
               </disp-formula>
            </p>
         </app>
      </app-group>
      <fn-group>
         <fn id="fn1">
            <p>We gratefully acknowledge support for this research from the Spencer Foundation, through a 2007 Small Grant. We appreciate our access to the Southern and Eastern Africa Consortium for Monitoring Educational Quality (SACMEQ) data and extensive information about it from our colleague Kenneth N. Ross of the International Institute for Educational Planning (IIEP), UNESCO.</p>
         </fn>
         <fn id="fn2">
            <label>
               <sup>1</sup>
            </label>
            <p>See Heyneman and Jamison (<xref ref-type="bibr" rid="rf21">1980</xref>), Jamison et al. (<xref ref-type="bibr" rid="rf23">1981</xref>), Lockheed et al. (<xref ref-type="bibr" rid="rf27">1986</xref>), Fuller (<xref ref-type="bibr" rid="rf9">1987</xref>), Velez et al. (<xref ref-type="bibr" rid="rf43">1993</xref>), and Fuller and Clarke (<xref ref-type="bibr" rid="rf10">1994</xref>).</p>
         </fn>
         <fn id="fn3">
            <label>
               <sup>2</sup>
            </label>
            <p>See Heyneman and Loxley (<xref ref-type="bibr" rid="rf22">1983</xref>), Fuller (<xref ref-type="bibr" rid="rf9">1987</xref>), Park and Hannum (<xref ref-type="bibr" rid="rf33">2001</xref>), Lee et al. (<xref ref-type="bibr" rid="rf26">2005</xref>), and Rivkin et al. (<xref ref-type="bibr" rid="rf35">2005</xref>).</p>
         </fn>
         <fn id="fn4">
            <label>
               <sup>3</sup>
            </label>
            <p>See Heyneman and Jamison (<xref ref-type="bibr" rid="rf21">1980</xref>), Mullens et al. (<xref ref-type="bibr" rid="rf30">1996</xref>), Fuller et al. (<xref ref-type="bibr" rid="rf11">1999</xref>), and Lee et al. (<xref ref-type="bibr" rid="rf26">2005</xref>).</p>
         </fn>
         <fn id="fn5">
            <label>
               <sup>4</sup>
            </label>
            <p>Before that, the infamous Bantu Education laws applied for the nonwhite majority, emphasizing a limited curriculum deemed “appropriate” for Africans.</p>
         </fn>
         <fn id="fn6">
            <label>
               <sup>5</sup>
            </label>
            <p>The 14 education systems included in SACMEQ-II are Botswana, Kenya, Lesotho, Malawi, Mauritius, Mozambique, Namibia, Seychelles, South Africa, Swaziland, Tanzania (mainland), Uganda, Zambia, and Zanzibar. Although Zanzibar forms part of the United Republic of Tanzania, educational issues are administered independently.</p>
         </fn>
         <fn id="fn7">
            <label>
               <sup>6</sup>
            </label>
            <p>South Africa and Mauritius refused to include achievement test scores for teachers. Ministry of Education officials in those countries felt that testing teachers was a politically sensitive action that might jeopardize the SACMEQ data-collection process.</p>
         </fn>
         <fn id="fn8">
            <label>
               <sup>7</sup>
            </label>
            <p>SACMEQ-II is a public-use data set available from IIEP. Interested researchers may obtain detailed information about IIEP and SACMEQ from their Web sites:<uri xlink:type="simple" xlink:href="http://www.iiep.unesco.org">http://www.iiep.unesco.org</uri>and<uri xlink:type="simple" xlink:href="http://www.sacmeq.org">http://www.sacmeq.org</uri>. Many studies about the included countries may also be downloaded from the sites. Before actually gaining access to the data, researchers typically must propose a project topic and specify which countries’ data they would use. Those using the data should have considerable analytic expertise, including knowledge of multilevel analytic methods (hierarchical linear models: HLMs).</p>
         </fn>
         <fn id="fn9">
            <label>
               <sup>8</sup>
            </label>
            <p>The domains were adjusted to match with what was actually being taught in schools across the SACMEQ countries. For mathematics, three domains were created out of an original five: number, measurement, and space data (a combination of the geometry and the space domains in the IEA framework). Another domain (algebra) was dropped because it did not apply to the grade 6 schools in SACMEQ. The reading tests covered narrative prose, expository prose, and documents. Narrative prose assessed a student’s grasp of a straightforward narrative. Expository prose tested the understanding of text with descriptions and explanations. The documents domain assessed whether students could deduce facts on the basis of different pieces of information within a text.</p>
         </fn>
         <fn id="fn10">
            <label>
               <sup>9</sup>
            </label>
            <p>Reenrollments at higher primary grades were greater for boys than for girls in Uganda after Universal Public Education was introduced in 1997, which resulted in more boys from low-income groups returning to higher grades of primary school (Appleton<xref ref-type="bibr" rid="rf2">2001</xref>).</p>
         </fn>
         <fn id="fn11">
            <label>
               <sup>10</sup>
            </label>
            <p>Some results that are not included in this article are available from the authors. These include the results of our level 1 HLM analysis and zero-order correlations among school-level variables.</p>
         </fn>
         <fn id="fn12">
            <label>
               <sup>11</sup>
            </label>
            <p>We converted HLM coefficients into effect sizes by dividing them by 100, the standard deviation of test scores in reading and mathematics.</p>
         </fn>
         <fn id="fn13">
            <label>
               <sup>12</sup>
            </label>
            <p>For the purposes of comparison, we note that typical ICCs in the United States are between .20 and .30 and somewhat higher (maybe .40) in more developed countries in Latin America. Using this yardstick, the ICCs in Namibia and Uganda are very high indeed.</p>
         </fn>
         <fn id="fn14">
            <label>
               <sup>13</sup>
            </label>
            <p>More specifically, we estimated full HLMs, including both level 1 and level 2 results. However, we have not included the level 1 results in this article, for the purpose of parsimony. They are available from the authors.</p>
         </fn>
         <fn id="fn15">
            <label>
               <sup>14</sup>
            </label>
            <p>Analysts using HLMs must decide how to center the independent variables in their level 1 models (Raudenbush and Bryk<xref ref-type="bibr" rid="rf777">2002</xref>). In this study, we were interested only in adjusting the dependent variables in each school for the academic and social backgrounds of their students (SES, repetition status, gender). Although it is possible to model these distributive parameters at level 2 as slopes-as-outcomes (e.g., the relationship between SES and achievement in each school), we chose not to do this. Thus, in all level 1 models in this study, the independent variables were centered around their grand means. The result is that variances in the distributive parameters were set to zero (i.e., “fixed”).</p>
         </fn>
      </fn-group>
      <ref-list content-type="unparsed">
         <title>References</title>
         <ref id="rf1">
            <mixed-citation xlink:type="simple" publication-type="">Alubisia, Alex A. 2005.<italic>UPE Myth or Reality? A Review of Experiences, Challenges, and Lessons from East Africa</italic>. London: OXFAM/ANCEFA.</mixed-citation>
         </ref>
         <ref id="rf2">
            <mixed-citation xlink:type="simple" publication-type="">Appleton, Simon. 2001. “What Can We Expect from Universal Primary Education?” In<italic>Uganda’s Recovery: The Role of Farms, Firms, and Government</italic>, ed. Ritva Reinikka and Paul Collier. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="rf111">
            <mixed-citation xlink:type="simple" publication-type="">Barr, Rebecca, and Robert Dreeben. 1983.<italic>How Schools Work</italic>. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="rf3">
            <mixed-citation xlink:type="simple" publication-type="">Bray, Mark. 1990. “The Quality of Education in Multiple-Shift Schools: How Far Does a Financial Saving Imply Educational Cost?”<italic>Comparative Education</italic>26 (1): 73–81.</mixed-citation>
         </ref>
         <ref id="rf4">
            <mixed-citation xlink:type="simple" publication-type="">Bray, Mark. 1999.<italic>The Shadow Education System: Private Tutoring and Its Implication for Planners</italic>. Paris: IIEP.</mixed-citation>
         </ref>
         <ref id="rf5">
            <mixed-citation xlink:type="simple" publication-type="">Bray, Mark. 2003.<italic>Adverse Effects of Private Supplementary Tutoring</italic>. Paris: IIEP.</mixed-citation>
         </ref>
         <ref id="rf444">
            <mixed-citation xlink:type="simple" publication-type="">Case, Anne, and Angus Deaton. 1999. “School Inputs and Educational Outcomes in South Africa.”<italic>Quarterly Journal of Economics</italic>114 (3): 1047–84.</mixed-citation>
         </ref>
         <ref id="rf6">
            <mixed-citation xlink:type="simple" publication-type="">Chimombo, J., D. Kunje, T. Chimuzu, and C. Mchikoma. 2005.<italic>The SACMEQ II Project in Malawi: A Study of the Conditions of Schooling and the Quality of Education</italic>. Harare: SACMEQ.</mixed-citation>
         </ref>
         <ref id="rf7">
            <mixed-citation xlink:type="simple" publication-type="">D’Agostino, Jerome, and Sonya J. Powers. 2009. “Predicting Teacher Performance with Test Scores and Grade Point Average: A Meta-analysis.”<italic>American Educational Research Journal</italic>46 (1): 146–82.</mixed-citation>
         </ref>
         <ref id="rf8">
            <mixed-citation xlink:type="simple" publication-type="">Fuller, Bruce. 1986. “Is Primary School Quality Eroding in the Third World?”<italic>Comparative Education Review</italic>30 (4): 491–507.</mixed-citation>
         </ref>
         <ref id="rf9">
            <mixed-citation xlink:type="simple" publication-type="">Fuller, Bruce. 1987. “What School Factors Raise Achievement in the Third World?”<italic>Review of Educational Research</italic>57 (3): 255–92.</mixed-citation>
         </ref>
         <ref id="rf10">
            <mixed-citation xlink:type="simple" publication-type="">Fuller, Bruce, and Prema Clarke. 1994. “Raising School Effects While Ignoring Culture? Local Conditions and the Influence of Classroom Tools, Rules, and Pedagogy.”<italic>Review of Educational Research</italic>64 (1): 119–57.</mixed-citation>
         </ref>
         <ref id="rf11">
            <mixed-citation xlink:type="simple" publication-type="">Fuller, Bruce, Lucia Dellagnelo, Annelie Strath, Eni Bastos, Maurício Maia, Kelma De Matos, Adelia Portela, and Sofia Vieira. 1999. “How to Raise Children’s Early Literacy? The Influence of Family, Teacher, and Classroom in Northeast Brazil.”<italic>Comparative Education Review</italic>43 (1): 1–35.</mixed-citation>
         </ref>
         <ref id="rf12">
            <mixed-citation xlink:type="simple" publication-type="">Fuller, Bruce, Haiyan Hua, and Conrad W. Snyder. 1994. “When Girls Learn More than Boys: The Influence of Time in School and Pedagogy in Botswana.”<italic>Comparative Education Review</italic>38 (3): 347–76.</mixed-citation>
         </ref>
         <ref id="rf222">
            <mixed-citation xlink:type="simple" publication-type="">Glass, Gene, and Mary Lee Smith. 1978.<italic>Meta-analysis of Research on the Relationship of Class Size and Achievement: The Class Size and Instruction Project</italic>. San Francisco: Far West Laboratory for Educational Research and Development.</mixed-citation>
         </ref>
         <ref id="rf13">
            <mixed-citation xlink:type="simple" publication-type="">Glewwe, Paul. 2002. “Schools and Skills in Developing Countries: Education Policies and Socioeconomic Outcomes.”<italic>Journal of Economic Literature</italic>40 (2): 436–82.</mixed-citation>
         </ref>
         <ref id="rf14">
            <mixed-citation xlink:type="simple" publication-type="">Glewwe, Paul, and Michael Kremer. 2006. “Schools, Teachers, and Education Outcomes in Developing Countries.” In<italic>Handbook of the Economics of Education</italic>, vol. 2, ed. Eric A. Hanushek and Finis Welch. Amsterdam: North-Holland.</mixed-citation>
         </ref>
         <ref id="rf15">
            <mixed-citation xlink:type="simple" publication-type="">Glewwe, Paul, Michael Kremer, and Sylvie Moulin. 1998.<italic>Textbooks and Test Scores: Evidence from a Prospective Evaluation in Kenya</italic>. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="rf16">
            <mixed-citation xlink:type="simple" publication-type="">Hanushek, Eric A., and Javier Luque. 2002. “Efficiency and Equity in Schools around the World.” NBER Working Paper no. 8949, National Bureau of Economic Research, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="rf17">
            <mixed-citation xlink:type="simple" publication-type="">Hedges, Larry V., and Rob Greenwald. 1996. “Have Times Changed? The Relation between School Resources and Student Performance.” In<italic>Does Money Matter? The Effect of School Resources on Student Achievement and Adult Success</italic>, ed. Gary Burtless. Washington, DC: Brookings.</mixed-citation>
         </ref>
         <ref id="rf18">
            <mixed-citation xlink:type="simple" publication-type="">Hedges, Larry V., Richard D. Laine, and Rob Greenwald. 1994. “Does Money Matter? A Meta-analysis of Studies of the Effects of Differential School Inputs on Student Outcomes.”<italic>Educational Researcher</italic>23 (3): 5–14.</mixed-citation>
         </ref>
         <ref id="rf19">
            <mixed-citation xlink:type="simple" publication-type="">Hedges, Larry V., and William Stock. 1983. “The Effects of Class Size: An Examination of Rival Hypotheses.”<italic>American Educational Research Journal</italic>20 (1): 63–85.</mixed-citation>
         </ref>
         <ref id="rf20">
            <mixed-citation xlink:type="simple" publication-type="">Heyneman, Stephen P., Joseph P. Farrell, and Manuel A. Sepulveda-Stuardo. 1981. “Textbooks and Achievement in Developing Countries: What We Know.”<italic>Journal of Curriculum Studies</italic>13 (3): 227–46.</mixed-citation>
         </ref>
         <ref id="rf21">
            <mixed-citation xlink:type="simple" publication-type="">Heyneman, Stephen P., and Dean T. Jamison. 1980. “Student Learning in Uganda: Textbook Availability and Other Factors.”<italic>Comparative Education Review</italic>24 (2): 206–20.</mixed-citation>
         </ref>
         <ref id="rf22">
            <mixed-citation xlink:type="simple" publication-type="">Heyneman, Stephen P., and William Loxley. 1983. “The Effect of Primary-School Quality on Academic Achievement across Twenty-nine High- and Low-Income Countries.”<italic>American Journal of Sociology</italic>8 (6): 1162–94.</mixed-citation>
         </ref>
         <ref id="rf23">
            <mixed-citation xlink:type="simple" publication-type="">Jamison, Dean T., Barbara Searle, Klaus Galda, and Stephen P. Heyneman. 1981. “Improving Elementary Math Education in Nicaragua: An Experimental Study of the Impact of Textbooks and Radio on Achievement.”<italic>Journal of Educational Psychology</italic>73 (4): 556.</mixed-citation>
         </ref>
         <ref id="rf24">
            <mixed-citation xlink:type="simple" publication-type="">Kukla-Acevedo, Sharon. 2009. “Do Teacher Characteristics Matter? New Results on the Effects of Teacher Preparation on Student Achievement.”<italic>Economics of Education Review</italic>28:49–57.</mixed-citation>
         </ref>
         <ref id="rf25">
            <mixed-citation xlink:type="simple" publication-type="">Lee, Jong-Wha, and Robert J. Barro. 1997.<italic>Schooling Quality in a Cross-Section of Countries</italic>. Cambridge, MA: National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="rf555">
            <mixed-citation xlink:type="simple" publication-type="">Lee, Valerie E. 2001.<italic>Restructuring High Schools for Equity and Excellence: What Works</italic>. New York: Teachers College Press.</mixed-citation>
         </ref>
         <ref id="rf26">
            <mixed-citation xlink:type="simple" publication-type="">Lee, Valerie E., Tia L. Zuze, and Kenneth R. Ross. 2005. “School Effectiveness in 14 Sub-Saharan African Countries: Links with 6th Graders’ Reading Achievement.”<italic>Studies in Educational Evaluation</italic>31:207–46.</mixed-citation>
         </ref>
         <ref id="rf666">
            <mixed-citation xlink:type="simple" publication-type="">Leith, J. Clark. 1999.<italic>Botswana: A Case Study of Economic Policy Prudence and Growth</italic>. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="rf27">
            <mixed-citation xlink:type="simple" publication-type="">Lockheed, Marlaine E., Stephen C. Vail, and Bruce Fuller. 1986. “How Textbooks Affect Achievement in Developing Countries: Evidence from Thailand.”<italic>Educational Evaluation and Policy Analysis</italic>8 (4): 379–92.</mixed-citation>
         </ref>
         <ref id="rf28">
            <mixed-citation xlink:type="simple" publication-type="">Lockheed, Marlaine E., and Adriaan M. Verspoor. 1991.<italic>Improving Primary Education in Developing Countries</italic>. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="rf29">
            <mixed-citation xlink:type="simple" publication-type="">Makuwa, Demus. 2005.<italic>The SACMEQ II Project in Namibia: A Study of the Conditions of Schooling and the Quality of Education</italic>. Windhoek: SACMEQ, Ministry of Basic Education, Sport, and Culture.</mixed-citation>
         </ref>
         <ref id="rf333">
            <mixed-citation xlink:type="simple" publication-type="">Michaelowa, Katharina. 2001. “Primary Education Quality in Francophone Sub-Saharan Africa: Determinants of Learning Achievement and Efficiency Considerations.”<italic>World Development</italic>29 (10): 1699–1716.</mixed-citation>
         </ref>
         <ref id="rf30">
            <mixed-citation xlink:type="simple" publication-type="">Mullens, John E., Richard J. Murnane, and John B. Willett. 1996. “The Contribution of Training and Subject Matter Knowledge to Teaching Effectiveness: A Multilevel Analysis of Longitudinal Evidence from Belize.”<italic>Comparative Education Review</italic>40 (2): 139–57.</mixed-citation>
         </ref>
         <ref id="rf31">
            <mixed-citation xlink:type="simple" publication-type="">Mushemeza, Elijah D. 2003. “Financial Management of Education in a Decentralized Setting: The Case of Uganda.” Paper presented at the CODESRIA Working Group on Finance and Education, Dakar.</mixed-citation>
         </ref>
         <ref id="rf32">
            <mixed-citation xlink:type="simple" publication-type="">Nishimura, Mikiko, Keiich Ogawa, Daniel N. Sifuna, Joseph Chimombo, Demis Kunje, Joseph G. Ampiah, Albert Byamugisha, Nobuhide Sawamura, and Shoko Yamada. 2009. “A Comparative Analysis of Universal Primary Education Policy in Ghana, Kenya, Malawi, and Uganda.”<italic>Journal of International Cooperation in Education</italic>12 (1): 143–58.</mixed-citation>
         </ref>
         <ref id="rf33">
            <mixed-citation xlink:type="simple" publication-type="">Park, Albert, and Emily Hannum. 2001. “Do Teachers Affect Learning in Developing Countries? Evidence from Matched Student-Teacher Data from China.” Paper presented at the Social Science Research Council conference Rethinking Social Science Research on the Developing World in the 21st Century, Salt Lake City, Utah.</mixed-citation>
         </ref>
         <ref id="rf777">
            <mixed-citation xlink:type="simple" publication-type="">Raudenbush, Stephen W., and Anthony S. Bryk. 2002.<italic>Hierarchical Linear Models: Applications and Data Analysis Methods</italic>. 2nd ed. Newbury Park, CA: Sage.</mixed-citation>
         </ref>
         <ref id="rf888">
            <mixed-citation xlink:type="simple" publication-type="">Raudenbush, Steven W., Anthony S. Bryk, Y. F. Cheong, and Richard T. Congdon. 2004.<italic>HLM 6: Hierarchical Linear and Nonlinear Modeling</italic>. Chicago: Scientific Software International.</mixed-citation>
         </ref>
         <ref id="rf34">
            <mixed-citation xlink:type="simple" publication-type="">Riddell, Abbey. 2003. “The Introduction of Free Primary Education in Sub-Saharan Africa.” Paper commissioned for the EFA Global Monitoring Report 2003–4, The Leap to Equality, UNESCO, Paris.</mixed-citation>
         </ref>
         <ref id="rf35">
            <mixed-citation xlink:type="simple" publication-type="">Rivkin, Steven G., Eric A. Hanushek, and John F. Kain. 2005. “Teachers, Schools, and Academic Achievement.”<italic>Econometrica</italic>73 (2): 417–58.</mixed-citation>
         </ref>
         <ref id="rf36">
            <mixed-citation xlink:type="simple" publication-type="">Robinson, Glen E., and James H. Wittebols. 1986.<italic>Class Size Research: A Related Cluster Analysis for Decision Making</italic>. Arlington, VA: Educational Research Service.</mixed-citation>
         </ref>
         <ref id="rf37">
            <mixed-citation xlink:type="simple" publication-type="">Rockoff, Jonah E., Brian A. Jacob, Thomas J. Kane, and Douglas O. Staiger. 2008. “Can You Recognize an Effective Teacher When You Recruit One?” NBER Working Paper no. 14485, National Bureau of Economic Research, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="rf38">
            <mixed-citation xlink:type="simple" publication-type="">Simmons, John, and Leigh Alexander. 1978. “The Determinants of School Achievement in Developing Countries: A Review of Research.”<italic>Economic Development and Cultural Change</italic>26 (2): 341–57.</mixed-citation>
         </ref>
         <ref id="rf39">
            <mixed-citation xlink:type="simple" publication-type="">Smith, Thomas M., and Albert Motivans. 2007. “Teacher Quality and Education for All in Sub-Saharan Africa.” In<italic>Education for All: Global Promises, National Challenges</italic>, ed. David P. Baker and Alexander W. Wiseman. Oxford: JAI.</mixed-citation>
         </ref>
         <ref id="rf42">
            <mixed-citation xlink:type="simple" publication-type="">UNESCO. 2005.<italic>EFA Global Monitoring Report, 2005</italic>. Paris: UNESCO.</mixed-citation>
         </ref>
         <ref id="rf40">
            <mixed-citation xlink:type="simple" publication-type="">U.S. Department of Education. 2004.<italic>No Child Left Behind: A Toolkit for Teachers</italic>. Washington, DC: Department of Education.</mixed-citation>
         </ref>
         <ref id="rf43">
            <mixed-citation xlink:type="simple" publication-type="">Velez, Eduardo, Ernesto Schiefelbein, and Jorge Valenzuela. 1993.<italic>Factors Affecting Achievement in Primary Education</italic>. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="rf44">
            <mixed-citation xlink:type="simple" publication-type="">Walberg, Herbert J. 1991. “Improving School Science in Advanced and Developing Countries.”<italic>Review of Educational Research</italic>61 (1): 25–69.</mixed-citation>
         </ref>
         <ref id="rf45">
            <mixed-citation xlink:type="simple" publication-type="">Willms, Jon D., and Marie Somers. 2001. “Family, Classroom, and School Effects on Children’s Educational Outcomes in Latin America.”<italic>School Effectiveness and School Improvement</italic>12 (4): 409–45.</mixed-citation>
         </ref>
         <ref id="rf47">
            <mixed-citation xlink:type="simple" publication-type="">Wößmann, Ludger. 2000.<italic>Schooling Resources, Educational Institutions, and Student Performance: The International Evidence</italic>. Kiel: Kiel Institute of World Economics.</mixed-citation>
         </ref>
         <ref id="rf48">
            <mixed-citation xlink:type="simple" publication-type="">Wößmann, Ludger, and Thomas Fuchs. 2005. “Families, Schools, and Primary-School Learning: Evidence for Argentina and Columbia in an International Perspective.” World Bank Policy Research Paper no. 3537, World Bank, Washington, DC.</mixed-citation>
         </ref>
         <ref id="rf49">
            <mixed-citation xlink:type="simple" publication-type="">Zuze, Tia L. 2008. “Equity and Effectiveness in East African Primary Schools.” PhD diss., Department of Economics, University of Cape Town.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
