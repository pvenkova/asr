<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plansystevol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009192</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant Systematics and Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03782697</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16156110</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23671355</article-id>
         <title-group>
            <article-title>Genetic differentiation and delimitation of Pugionium dolabratum and Pugionium cornutum (Brassicaceae)</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Qiushi</given-names>
                  <surname>Yu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Qian</given-names>
                  <surname>Wang</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Guili</given-names>
                  <surname>Wu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Yazheng</given-names>
                  <surname>Ma</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Xinyu</given-names>
                  <surname>He</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Xi</given-names>
                  <surname>Wang</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Penghui</given-names>
                  <surname>Xie</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lihua</given-names>
                  <surname>Hu</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jianquan</given-names>
                  <surname>Liu</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">299</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23662941</issue-id>
         <fpage>1355</fpage>
         <lpage>1365</lpage>
         <permissions>
            <copyright-statement>© Springer Verlag 2013</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s00606-013-0800-3"
                   xlink:title="an external site"/>
         <abstract>
            <p>Species delimitation has been a major research focus in evolutionary biology. However, the genetic delimitation of recently diverged species varies depending on the markers examined. In this study, we aimed to examine genetic differentiation and delimitations between only two species of Pugionium Gaertn (Brassicaceae)— Pugionium cornutum (L.) Gaertn and Pugionium dolabratum Maxim—that occur in the desert habitats of central Asia and have parapatric distributions. We genotyped 169 individuals from 25 populations, using two chloroplast (cp) DNA fragments (trnV-trnM and trnS-trnfM), seven simple repeated sequence (SSR) loci and the nuclear ribosomal internal transcribed spacer (ITS). Four cp haplotypes were identified, three of which commonly occur in the two species, suggesting incomplete species-specific lineage sorting. Between-species cpDNA differentiation (FCT) was low, even lower than among populations of the same species. However, we found higher than average SSR FCT values, and both Bayesian clustering of SSR variables and maximum-likelihood genetic analyses divided all sampled individuals into two groups, agreeing well with morphological separation, although gene flow between species was obvious according to the SSR loci data. However, two types of ITS sequences were highly consistent with the morphological delimitation of the two species in all sampled individuals. These results together suggest that these two species shared numerous ancestral cpDNA polymorphisms and point to the importance of nuclear DNA (ITS or genetic accumulation at multiple loci) in delimiting recently diverged species.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d286e282a1310">
            <mixed-citation id="d286e286" publication-type="other">
Avise JC (2000) Phylogeography: the history and formation of
species. Harvard University Press, London</mixed-citation>
         </ref>
         <ref id="d286e296a1310">
            <mixed-citation id="d286e300" publication-type="other">
Bai WN, Liao WJ, Zhang DY (2010) Nuclear and chloroplast DNA
phylogeography reveal two refuge areas with asymmetrical gene
flow in a temperate walnut tree from East Asia. New Phytol
188:892-901</mixed-citation>
         </ref>
         <ref id="d286e316a1310">
            <mixed-citation id="d286e320" publication-type="other">
Bandelt HJ, Forster P, Rohl A (1999) Median-joining networks for
inferring intraspecific phylogenies. Mol Biol Evol 16:37^18</mixed-citation>
         </ref>
         <ref id="d286e330a1310">
            <mixed-citation id="d286e334" publication-type="other">
Beerli P, Felsenstein J (1999) Maximum-likelihood estimation of
migration rates and effective population numbers in two
populations using a coalescent approach. Genetics 152:763-773</mixed-citation>
         </ref>
         <ref id="d286e348a1310">
            <mixed-citation id="d286e352" publication-type="other">
Beerli P, Palczewski M (2010) Unified framework to evaluate
panmixia and migration direction among multiple sampling
locations. Genetics 185:313-326</mixed-citation>
         </ref>
         <ref id="d286e365a1310">
            <mixed-citation id="d286e369" publication-type="other">
Cavender-Bares J, Pahlich A (2009) Molecular, morphological, and
ecological niche differentiation of sympatric sister oak species,
Quercus virginiana and Q. geminata (Fagaceae). Am J Bot
96:1690-1702</mixed-citation>
         </ref>
         <ref id="d286e385a1310">
            <mixed-citation id="d286e389" publication-type="other">
Chiang TY, Hung KH, Hsu TW, Wu WL (2004) Lineage sorting and
phylogeography in Lithocarpus formosanus and L. dodonaeifo-
lius (Fagaceae) from Taiwan. Ann Mo Bot Gard 91:207-222</mixed-citation>
         </ref>
         <ref id="d286e402a1310">
            <mixed-citation id="d286e406" publication-type="other">
Chung AM, Staub JE, Chen JF (2006) Molecular phylogeny of
Cucumis species as revealed by consensus chloroplast SSR
marker length and sequence variation. Genome 49:219-229</mixed-citation>
         </ref>
         <ref id="d286e419a1310">
            <mixed-citation id="d286e423" publication-type="other">
Currat M, Manuel R, Petit JR, Excoffier L (2008) The hidden side of
invasion: massive introgression by local genes. Evolution
62:1908-1920</mixed-citation>
         </ref>
         <ref id="d286e436a1310">
            <mixed-citation id="d286e440" publication-type="other">
de Queiroz K (2011) Branches in the lines of descent: Charles Darwin
and the evolution of the species concept. Biol J Linn Soc
103:19-35</mixed-citation>
         </ref>
         <ref id="d286e454a1310">
            <mixed-citation id="d286e458" publication-type="other">
Demesure B, Sodzi N, Petit RJ (1995) A set of universal primers for
amplification of polymorphic non-coding regions of mitochon-
drial and chloroplast DNA in plants. Mol Ecol 4:129-131</mixed-citation>
         </ref>
         <ref id="d286e471a1310">
            <mixed-citation id="d286e475" publication-type="other">
Doyle JJ, Doyle JL (1987) A rapid DNA isolation procedure for small
quantities of fresh leaf material. Phytochem Bull 19:11-15</mixed-citation>
         </ref>
         <ref id="d286e485a1310">
            <mixed-citation id="d286e489" publication-type="other">
Evanno G, Regnaut S, Goudet J (2005) Detecting the number of
clusters of individuals using the software STRUCTURE, a
simulation study. Mol Ecol 14:2611-2620</mixed-citation>
         </ref>
         <ref id="d286e502a1310">
            <mixed-citation id="d286e506" publication-type="other">
Excoffier L, Smouse PE, Quattro JM (1992) Analysis of molecular
variance inferred from metric distances among DNA haplotypes:
application to human mitochondrial DNA restriction sites.
Genetics 131:479^191</mixed-citation>
         </ref>
         <ref id="d286e522a1310">
            <mixed-citation id="d286e526" publication-type="other">
Felsenstein J (1985) Confidence limits on phylogenies: an approach
using the bootstrap. Evolution 39:783-791</mixed-citation>
         </ref>
         <ref id="d286e536a1310">
            <mixed-citation id="d286e540" publication-type="other">
Freeland JR (2005) Molecular ecology. John Wiley, Chichester,
pp 87-88</mixed-citation>
         </ref>
         <ref id="d286e551a1310">
            <mixed-citation id="d286e555" publication-type="other">
Hamilton MB (1999) Four primer pairs for the amplification of
chloroplast intergenic regions with intraspecific variation. Mol
Ecol 8:521-523</mixed-citation>
         </ref>
         <ref id="d286e568a1310">
            <mixed-citation id="d286e572" publication-type="other">
Hausdorf B (2011) Progress toward a general species concept.
Evolution 65:923-993</mixed-citation>
         </ref>
         <ref id="d286e582a1310">
            <mixed-citation id="d286e586" publication-type="other">
Hoelzer GA (1997) Inferring phylogenies from mtDNA variation:
mitochondrial-gene trees versus nuclear-gene trees revisited.
Evolution 51:622-626</mixed-citation>
         </ref>
         <ref id="d286e599a1310">
            <mixed-citation id="d286e603" publication-type="other">
Hollingsworth PM (2011) Refining the DNA barcode for land plants.
Proc Natl Acad Sci USA 108:19451-19452</mixed-citation>
         </ref>
         <ref id="d286e613a1310">
            <mixed-citation id="d286e617" publication-type="other">
Hollingsworth PM, Graham SW, Little DP (2011) Choosing and
using a plant DNA barcoding. PLoS One 6:e 19254</mixed-citation>
         </ref>
         <ref id="d286e627a1310">
            <mixed-citation id="d286e631" publication-type="other">
Huang YL, Tan FX, Su GH, Deng SL, He HH, Shi SH (2008)
Population genetic structure of three tree species in the
mangrove genus Ceriops (Rhizophoraceae) from the Indo West
Pacific. Genética 133:47-56</mixed-citation>
         </ref>
         <ref id="d286e648a1310">
            <mixed-citation id="d286e652" publication-type="other">
Huang XM, Hao LZ, Hu NB, Zhao QY, Zhang JW (2009) Studies on
the breeding system and pollination biology of China species—
Pugionium cornutum. Act Bot Bor-Occ Sin 29:2232-2237</mixed-citation>
         </ref>
         <ref id="d286e665a1310">
            <mixed-citation id="d286e669" publication-type="other">
Hubisz MJ, Falush D, Stephens M, Pritchard JK (2009) Inferring
weak population structure with the assistance of sample group
information. Mol Ecol Res 9:1322-1332</mixed-citation>
         </ref>
         <ref id="d286e682a1310">
            <mixed-citation id="d286e686" publication-type="other">
Illarionova ID (1999) Synopsis of the genus Pugionium Gaertn.
(Brassicaceae). Komarovia 1:35-37</mixed-citation>
         </ref>
         <ref id="d286e696a1310">
            <mixed-citation id="d286e700" publication-type="other">
Ji SL, Zhang FL, Hao LZ, Yang ZR (2011) Chromosome number and
karyotypes of Pugionium Gaertn. Acta Bot Boreal-Occident Sin
31:1988-1994</mixed-citation>
         </ref>
         <ref id="d286e713a1310">
            <mixed-citation id="d286e717" publication-type="other">
Kawasaki M, Ohnishi O (2006) Two distinct groups of natural
populations of Fagopyrum urophyllum (Bur. et Franch.) gross
revealed region in chloroplast DNA. Genes Genet Syst
81:323-332</mixed-citation>
         </ref>
         <ref id="d286e733a1310">
            <mixed-citation id="d286e737" publication-type="other">
Kikuchi R, Jae-Hong P, Takahashi H, Maki M (2010) Disjunct
distribution of chloroplast DNA haplotypes in the understory
perennial Veratrum album ssp. oxysepalum (Melanthiaceae) in
Japan as a result of ancient introgression. New Phytol 188:879-891</mixed-citation>
         </ref>
         <ref id="d286e754a1310">
            <mixed-citation id="d286e758" publication-type="other">
Kliman RM, Andolfatto P, Coyne JA, Depaulis F, Kreitman M, Berry
AJ, McCarter J, Wakeley J, Hey J (2000) The population
genetics of the origin and divergence of the Drosophila simulons
complex species. Genetics 156:1913-1931</mixed-citation>
         </ref>
         <ref id="d286e774a1310">
            <mixed-citation id="d286e778" publication-type="other">
Li DZ. Gao LM, Li HT, Wang H, Ge XJ, Liu JQ, Chen ZD, Zhou SL,
Chen SL, Yang JB, Fu CX, Zeng CX, Yan HF, Zhu YJ, Sun YS,
Chen SY, Zhao L, Wang K, Yang T, Duan GW (2011)
Comparative analysis of a large dataset indicates that internal
transcribed spacer (ITS) should be incorporated into the core
barcode for seed plants. Proc Natl Acad Sci USA
108:19641-19646</mixed-citation>
         </ref>
         <ref id="d286e804a1310">
            <mixed-citation id="d286e808" publication-type="other">
Maddison WP (1997) Gene trees in species trees. Syst Biol
46:523-536</mixed-citation>
         </ref>
         <ref id="d286e818a1310">
            <mixed-citation id="d286e822" publication-type="other">
Marriage TN, Hudman S, Mort ME, Orive ME, Shaw RG, Kelly JK
(2009) Direct estimation of the mutation rate at dinucleotide
microsatellite loci in Arabidopsis thaliana (Brassicaceae).
Heredity 103:310-317</mixed-citation>
         </ref>
         <ref id="d286e838a1310">
            <mixed-citation id="d286e842" publication-type="other">
Marshall JC, Arevalo E, Benavides E, Site JL, Site JW (2006)
Delimiting species: comparing methods for Mendelian charac-
ters using lizards of the Sceloporus grammicus (Squamata:
Phrynosomatidae) complex. Evolution 60:1050-1065</mixed-citation>
         </ref>
         <ref id="d286e858a1310">
            <mixed-citation id="d286e862" publication-type="other">
Mayr E (1963) Animal species and evolution. Harvard University
Press, Cambridge</mixed-citation>
         </ref>
         <ref id="d286e873a1310">
            <mixed-citation id="d286e877" publication-type="other">
Mims MC, Hulsey CD, Fitzpatrick BM, Streelman JT (2010)
Geography disentangles introgression from ancestral polymor-
phism in Lake Malawi cichlids. Mol Ecol 49:940-951</mixed-citation>
         </ref>
         <ref id="d286e890a1310">
            <mixed-citation id="d286e894" publication-type="other">
Mogensen HL ( 1996) The hows and whys of cytoplasmic inheritance
in seed plants. Am J Bot 83:383--404</mixed-citation>
         </ref>
         <ref id="d286e904a1310">
            <mixed-citation id="d286e908" publication-type="other">
Nadachowska K, Babik W (2009) Divergence in the face of gene
flow: the case of two newts (Amphibia: Salamandridae). Mol
Biol Evol 26:829-841</mixed-citation>
         </ref>
         <ref id="d286e921a1310">
            <mixed-citation id="d286e925" publication-type="other">
Nei M (1987) Molecular evolutionary genetics. Columbia University
Press, New York</mixed-citation>
         </ref>
         <ref id="d286e935a1310">
            <mixed-citation id="d286e939" publication-type="other">
Nichols R (2001) Gene trees and species trees are not the same.
Trends Ecol Evol 16:358-364</mixed-citation>
         </ref>
         <ref id="d286e949a1310">
            <mixed-citation id="d286e953" publication-type="other">
Palmé AE, Semerikov V, Lascoux M (2003) Absence of geographical
structure of chloroplast DNA variation in sallow, Salix caprea L.
Heredity 91:465^174</mixed-citation>
         </ref>
         <ref id="d286e967a1310">
            <mixed-citation id="d286e971" publication-type="other">
Petit RJ, Excoffier L (2009) Gene flow and species delimitation.
Trends Ecol Evol 24:386-393</mixed-citation>
         </ref>
         <ref id="d286e981a1310">
            <mixed-citation id="d286e985" publication-type="other">
Petit RJ, Vendramin GG (2007) Phylogeography of organelle DNA in
plants an introduction. In: Weiss S, Ferrand N Kluwer (eds)
Phylogeography of southern European Refugia, pp 46-82</mixed-citation>
         </ref>
         <ref id="d286e998a1310">
            <mixed-citation id="d286e1002" publication-type="other">
Petit RJ, Kremer A, Wagner DB (1993) Finite island model for
organelle and nuclear genes in plants. Heredity 71:630-641</mixed-citation>
         </ref>
         <ref id="d286e1012a1310">
            <mixed-citation id="d286e1016" publication-type="other">
Petit RJ, Duminil J, Fineschi S, Hampe A, Salvini D, Vendramin GG
(2005) Comparative organization of chloroplast, mitochondrial
and nuclear diversity in plant populations. Mol Ecol 14:689-701</mixed-citation>
         </ref>
         <ref id="d286e1029a1310">
            <mixed-citation id="d286e1033" publication-type="other">
Piry S, Alapetite A, Cornuet JM, Paetkau D, Baudouin L, Estoup A
(2004) GENECLASS2: a software for genetic assignment and
first-generation migrant detection. J Hered 95:536-539</mixed-citation>
         </ref>
         <ref id="d286e1046a1310">
            <mixed-citation id="d286e1050" publication-type="other">
Pons O, Petit RJ (1996) Measuring and testing genetic differentiation
with ordered versus unordered alleles. Genetics 144:1237-1245</mixed-citation>
         </ref>
         <ref id="d286e1061a1310">
            <mixed-citation id="d286e1065" publication-type="other">
Pritchard JK, Stephens M, Donnelly P (2000) Inference of population
structure using multilocus genotype data. Genetics 155:945-959</mixed-citation>
         </ref>
         <ref id="d286e1075a1310">
            <mixed-citation id="d286e1079" publication-type="other">
Raymond M, Rousset F (1995) An exact test for population
differentiation. Evolution 49:1280-1283</mixed-citation>
         </ref>
         <ref id="d286e1089a1310">
            <mixed-citation id="d286e1093" publication-type="other">
Schaal BA, Hayworth DA, Olsen KM, Rauscher JT, Smith WA
(1998) Phylogeographic studies in plants: problems and pros-
pects. Mol Ecol 7:465-474</mixed-citation>
         </ref>
         <ref id="d286e1106a1310">
            <mixed-citation id="d286e1110" publication-type="other">
Schemske DW (2010) Adaptation and the origin of species. Amer Nat
176:S4-S25</mixed-citation>
         </ref>
         <ref id="d286e1120a1310">
            <mixed-citation id="d286e1124" publication-type="other">
Schluter D (2009) Evidence for ecological speciation and its
alternative. Science 323:737-741</mixed-citation>
         </ref>
         <ref id="d286e1134a1310">
            <mixed-citation id="d286e1138" publication-type="other">
Schneider S, Roessli D, Excoffier L (2000) ARLEQUIN version
2.000: a software for population genetic data analysis. Genetics
and Biometry Laboratory, Department of Anthropology, Uni-
versity of Geneva, Switzerland</mixed-citation>
         </ref>
         <ref id="d286e1155a1310">
            <mixed-citation id="d286e1159" publication-type="other">
Soltis DE, Soltis PS, Thompson JN (1992) Chloroplast DNA variation
in Lithophragma (Saxifragaceae). Syst Bot 17:607-619</mixed-citation>
         </ref>
         <ref id="d286e1169a1310">
            <mixed-citation id="d286e1173" publication-type="other">
Soranzo N, Provan J, Powell W (1999) An example of microsatellite
length variation in the mitochondrial genome of conifers.
Genome 42:158-161</mixed-citation>
         </ref>
         <ref id="d286e1186a1310">
            <mixed-citation id="d286e1190" publication-type="other">
Sota T, Vogler AP (2001) Incongruence of mitochondrial and nuclear
gene trees in the carabid beetles Ohomopterus. Syst Biol
50:39-59</mixed-citation>
         </ref>
         <ref id="d286e1203a1310">
            <mixed-citation id="d286e1207" publication-type="other">
Taberlet P, Gielly L, Patou G, Bouvet J (1991) Universal primers for
amplification of three non-coding regions of chloroplast DNA.
Plant Mol Biol 17:1105-1109</mixed-citation>
         </ref>
         <ref id="d286e1220a1310">
            <mixed-citation id="d286e1224" publication-type="other">
Tamura K, Dudley J, Nei M, Kumar S (2007) MEGA4: molecular
evolutionary genetics analysis (GEGA) software version 4.0.
Mol Biol Evol 24:1596-1599</mixed-citation>
         </ref>
         <ref id="d286e1237a1310">
            <mixed-citation id="d286e1241" publication-type="other">
Thompson JD, Gibson TJ, Plewinak F, Jeanmougin F, Higgins DG
(1997) The Clustal-X windows interface: flexible strategies for
multiple sequence alignment aided by quality analysis tools.
Nucleic Acids Res 25:4876-4882</mixed-citation>
         </ref>
         <ref id="d286e1258a1310">
            <mixed-citation id="d286e1262" publication-type="other">
Wang ML, Barkley MA, Jenkins TM (2009) Microsatellite markers in
plants and insects. Part I: applications of biotechnology. Genes
Genom Genomics 3:54-67</mixed-citation>
         </ref>
         <ref id="d286e1275a1310">
            <mixed-citation id="d286e1279" publication-type="other">
Wang J, Wu YX, Ren GP, Guo QH, Liu JQ, Lascoux M (2011a)
Genetic differentiation and delimitation between ecologically
diverged Populus euphratica and P. pruinosa. PLoS One
6:e26530</mixed-citation>
         </ref>
         <ref id="d286e1295a1310">
            <mixed-citation id="d286e1299" publication-type="other">
Wang Q, Yu QS, Liu JQ (2011b) Are nuclear loci ideal to barcode
plants? A case study of genetic delimitation of two sister species
using multiple loci and multiple intraspecific individuals. J Syst
Evol 49:182-188</mixed-citation>
         </ref>
         <ref id="d286e1315a1310">
            <mixed-citation id="d286e1319" publication-type="other">
Weir BS (1996) Genetic data analysis II. Sinauer Associates,
Sunderland</mixed-citation>
         </ref>
         <ref id="d286e1329a1310">
            <mixed-citation id="d286e1333" publication-type="other">
White TJ, Bruns T, Lee S, Taylor J (1990) Amplification and direct
sequencing of fungal ribosomal RNA genes for phylogenetics.
In: Innis MA, Gelfand DH, Sninsky JJ, White TJ (eds) PCR
protocols: a guide to methods and application. Academic Press,
San Diego, pp 315-322</mixed-citation>
         </ref>
         <ref id="d286e1352a1310">
            <mixed-citation id="d286e1356" publication-type="other">
Wiens JJ (2007) Species delimitation: new approaches for discovering
diversity. Syst Biol 56:875-878</mixed-citation>
         </ref>
         <ref id="d286e1367a1310">
            <mixed-citation id="d286e1371" publication-type="other">
Wolfe KH, Li WH, Sharp PM (1987) Rates of nucleotide substitution
vary greatly among plant mitochondria, chloroplast, and nuclear
DNAs. Proc Natl Acad Sci USA 84:9054-9058</mixed-citation>
         </ref>
         <ref id="d286e1384a1310">
            <mixed-citation id="d286e1388" publication-type="other">
Wright S (1943) An analysis of local variability of flower color in
Linanthus parryae. Genetics 28:139-156</mixed-citation>
         </ref>
         <ref id="d286e1398a1310">
            <mixed-citation id="d286e1402" publication-type="other">
Yeh FC, Yang R, Boyle TJ, Ye Z, Xiyan JM (2000) PopGene32,
Microsoft Windows-based freeware for population genetic
analysis, version 1.32. Molecular Biology and Biotechnology
Centre, University of Alberta, Edmonton</mixed-citation>
         </ref>
         <ref id="d286e1418a1310">
            <mixed-citation id="d286e1422" publication-type="other">
Yu QS, Zhang DY, Wan DS, Xu HY, Zhao CM (2008) Development
of microsatellite DNA loci for Pugionium dolabratum (Brass-
icaceae), an endangered psammophyte. Conserv Genet
9:1019-1022</mixed-citation>
         </ref>
         <ref id="d286e1438a1310">
            <mixed-citation id="d286e1442" publication-type="other">
Yu QS, Wang Q, Wang AL, Wu GL, Liu JQ (2010) Interspecific
delimitation and phylogenetic origin of Pugionium (Brassica-
ceae). J Syst Evol 48:195-206</mixed-citation>
         </ref>
         <ref id="d286e1455a1310">
            <mixed-citation id="d286e1459" publication-type="other">
Zhao YZ (1999) A taxological revision and floristic analysis of the
genus Pugionium. J Inner Mong Univ 30:197-199</mixed-citation>
         </ref>
         <ref id="d286e1470a1310">
            <mixed-citation id="d286e1474" publication-type="other">
Zheng XM, Ge S (2010) Ecological divergence in the presence of
gene flow in two closely related Oryza species (Oryza rufipogon
and O. nivara). Mol Ecol 19:2439-2454</mixed-citation>
         </ref>
         <ref id="d286e1487a1310">
            <mixed-citation id="d286e1491" publication-type="other">
Zhou YF, Abbott RJ, Jiang ZY, Du FK, Milne RI, Liu JQ (2010)
Gene flow and species delimitation: a case study of Pinus
massoniana and P. hwangshanensis (Pinaceae) with overlapping
distributions. Evolution 64:2342-2352</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
