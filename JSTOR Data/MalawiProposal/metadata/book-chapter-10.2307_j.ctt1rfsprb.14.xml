<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1rfsprb</book-id>
      <subj-group>
         <subject content-type="call-number">BX9675.A43M388 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Brethren in Christ Church</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
            <subj-group>
               <subject content-type="lcsh">Matabeleland</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">20th century</subject>
                     <subj-group>
                        <subject content-type="lcsh">Case studies</subject>
                     </subj-group>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Brethren in Christ Church</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
            <subj-group>
               <subject content-type="lcsh">Matabeleland</subject>
               <subj-group>
                  <subject content-type="lcsh">Biography</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Sex role</subject>
         <subj-group>
            <subject content-type="lcsh">Religious aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Brethren in Christ Church</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Christianity and politics</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
            <subj-group>
               <subject content-type="lcsh">Matabeleland</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">20th century</subject>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Matabeleland (Zimbabwe)</subject>
         <subj-group>
            <subject content-type="lcsh">Church history</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Religion</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Gender of Piety</book-title>
         <subtitle>Family, Faith, and Colonial Rule in Matabeleland, Zimbabwe</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>URBAN-MEAD</surname>
               <given-names>WENDY</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>07</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9780821445273</isbn>
      <isbn content-type="epub">0821445278</isbn>
      <publisher>
         <publisher-name>Ohio University Press</publisher-name>
         <publisher-loc>Athens, Ohio</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Ohio University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1rfsprb"/>
      <abstract abstract-type="short">
         <p>
            <italic>The Gender of Piety</italic>is an intimate history of the Brethren in Christ Church in Zimbabwe, or BICC, as related through six individual life histories that extend from the early colonial years through the first decade after independence. Taken together, these six lives show how men and women of the BICC experienced and sequenced their piety in different ways. Women usually remained tied to the church throughout their lives, while men often had a more strained relationship with it. Church doctrine was not always flexible enough to accommodate expected masculine gender roles, particularly male membership in political and economic institutions or participation in important male communal practices.The study is based on more than fifteen years of extensive oral history research supported by archival work in Zimbabwe, the United Kingdom, and the United States. The oral accounts make it clear, official versions to the contrary, that the church was led by spiritually powerful women and that maleness and mission-church notions of piety were often incompatible.The life-history approach illustrates how the tension of gender roles both within and without the church manifested itself in sometimes unexpected ways: for example, how a single family could produce both a legendary woman pastor credited with mediating multiple miracles and a man - her son -  who joined the armed wing of the Zimbabwe African People's Union nationalist political party and fought in Zimbabwe's liberation war in the 1970s. Investigating the lives of men and women in equal measure,<italic>The Gender of Piety</italic>uses a gendered interpretive lens to analyze the complex relationship between the church and broader social change in this region of southern Africa.</p>
      </abstract>
      <counts>
         <page-count count="298"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.5</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.6</book-part-id>
                  <title-group>
                     <title>Map</title>
                  </title-group>
                  <fpage>xiv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.7</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                     <subtitle>The Gender of Piety in Matabeleland</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>A GROUP OF BOYS GATHERED AROUND the sickbed and sang a hymn called “Woza moni odangele” (Come, weary sinner).¹ Among them was nine-year-old Huggins Msimanga; they were learning the hymn from Huggins’s father, Zephania Msimanga, who lay dying.² It was 1955. This hymn speaks of the sinner who is depressed in spirit, who is broken or injured and announces that the Healer, the Savior, is here to provide comfort. Huggins recalled that his father, age thirty-seven, often sang this hymn in his “heavy times” and wanted to ensure the boys knew this hymn by heart before he passed on. “He</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.8</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>Matshuba Ndlovu</title>
                     <subtitle>Masculinity and Faith in Matabeleland, 1898–1930</subtitle>
                  </title-group>
                  <fpage>38</fpage>
                  <abstract>
                     <p>Nineteen-year-old Matshuba Ndlovu wrote this letter in 1904. It was addressed to missionary Frances Davidson, his teacher and spiritual mentor, who was on furlough in North America. While Davidson was on her yearlong tour of the congregations of the Brethren in Christ Church (BICC) in the United States and Canada, Matshuba was in charge of the school that Davidson and her colleague Alice Heise had started in 1898.</p>
                     <p>In the early years of the BICC’s mission to Rhodesia, the colonial state was in its infancy, while the very unworldly BICC missionaries were engaged in their first overseas venture and feeling</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.9</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>Maria Tshuma</title>
                     <subtitle>Chastity and Female Piety, 1920–70</subtitle>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>NTALI TSHUMA FACED A CRISIS.¹ HIS homestead in the Filabusi area of Matabeleland had fifteen wives and fifty-two children, and the crisis concerned his third wife’s second daughter. Her parents had named her Masalantombi, but lately she was known as Maria at the local BICC village school. She set the homestead into an uproar. On this day in 1931 she was refusing to be married. As a young woman of perhaps seventeen years whose menstrual flow had begun, it was time for Maria to be married—she was an<italic>intombi</italic>. The use of the passive voice is intentional: a girl</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.10</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>NakaSeyemephi Ngwenya</title>
                     <subtitle>A Church Planter eMaguswini, 1950–73</subtitle>
                  </title-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>A HANDWRITTEN POSTSCRIPT TO A LENGTHY, typed 1963 letter read, “My mother is still living. She is miles away and is a leader of a little church group in the remote areas of eMaguswini across the Shangani River.”¹ The author of the letter was raised from her infancy at Mtshabezi Mission School, and she was writing to her former mentor, retired missionary Walter O. Winger. At the time of writing she lived in Salisbury (present-day Harare) with her barrister husband and several children. Almost four decades later in an interview with me, she was very keen to tell all about</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.11</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Sandey Vundhla</title>
                     <subtitle>Being Fruitful for the Church, 1950–70</subtitle>
                  </title-group>
                  <fpage>134</fpage>
                  <abstract>
                     <p>TWO MEN RODE THEIR BICYCLES ALL THE way from Matopo Mission to the city of Bulawayo on the rough, unpaved road that passed northward out of the Matopo Hills and into town. The one-way trip was about thirty miles. One, the senior pastor, was Sandey Vundhla, in his midforties; the other was a young man in his twenties from the United States named Donald Zook. Vundhla’s rural home was just half a mile from the Matopo mission station, where Zook was teaching at the mission’s secondary school. They bicycled to Bulawayo many weekends in the years 1956–57. Their aim</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.12</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>MaNsimango (Sithembile Nkala)</title>
                     <subtitle>Sellouts, Comrades, and Christians in the Liberation War, 1969–78</subtitle>
                  </title-group>
                  <fpage>171</fpage>
                  <abstract>
                     <p>IN A DUSTY CLASSROOM AT THE WANEZI mission station in August 1997, an old woman named MaNsimango¹ and her middle-aged daughter, Musa Chidziva, leaned over a microphone and sang in Ndebele a song that translates as “Father we have been blessed by your word; we have been given light because of your light.” Musa later explained, as our interview drew to a close, that her mother chose this particular song to open the interview because “it has made her life better; even under difficulties, she manages to go through.”² Some of the difficulties she passed through in her lifetime, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.13</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>Stephen N. Ndlovu</title>
                     <subtitle>Gendered Piety, the New Zimbabwe, and the Gukurahundi, 1980–89</subtitle>
                  </title-group>
                  <fpage>203</fpage>
                  <abstract>
                     <p>A HIGH-CEILINGED ROOM EQUIPPED with an aging large wooden desk and metal file cabinets served as the office for former bishop Stephen N. Ndlovu during his years in the 1990s as a professor at the Theological College of Zimbabwe. Speaking in a strained, almost rasping voice, in worn penny loafers and large brown horn-rimmed glasses, Ndlovu had been fighting cancer. He was very tall, thinner than his former very imposing self, and exuded both immense dignity and caring. Our interview was interrupted several times by various people at the college needing to speak with him.</p>
                     <p>The 1990s were quieter years</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.14</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                     <subtitle>Gendered Lives of Piety</subtitle>
                  </title-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>TRACKING INDIVIDUALS THROUGH THEIR entire lives within the context of their families yields insights about the place of the church in African history. There is fruitfulness in considering the whole-life trajectory. In this study, I selected six members of the BICC-Zimbabwe for attention; their lives span the church’s inception, in the late 1890s, through to the postcolonial period, at the end of the twentieth century. Although each chapter features an individual, the web of family relationships around them is considered as fully as the evidence permits. All the stories have more power and subtlety because each life history does not</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.15</book-part-id>
                  <title-group>
                     <title>APPENDIX</title>
                     <subtitle>Ndlovu and Nsimango family tree</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <role>Created by</role>
                        <name name-style="western">
                           <surname>Urban-Mead</surname>
                           <given-names>Katherine</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>249</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>251</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.17</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>295</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.18</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>299</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsprb.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>317</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
