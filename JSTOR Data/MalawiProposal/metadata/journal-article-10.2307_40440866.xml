<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">weltarch</journal-id>
         <journal-id journal-id-type="jstor">j50000548</journal-id>
         <journal-title-group>
            <journal-title>Weltwirtschaftliches Archiv</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>J. C. B. Mohr (Paul Siebeck)</publisher-name>
         </publisher>
         <issn pub-type="ppub">00432636</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40440866</article-id>
         <title-group>
            <article-title>Technological Adaptation, Trade, and Growth</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Alberto</given-names>
                  <surname>Chong</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Luisa</given-names>
                  <surname>Zanforlin</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2001</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">137</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i40019667</issue-id>
         <fpage>565</fpage>
         <lpage>592</lpage>
         <permissions>
            <copyright-statement>Copyright 2001 Institut für Weltwirtschaft an der Universität Kiel</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/40440866"/>
         <abstract>
            <p>Based on Grossman and Helpman's 1991 seminal work, the authors provide a simple model extension where innovations created in the high-tech sector may be assimilated or adapted by the lowtech sector, thus generating nondecreasing returns in the production function of the latter. When applying a Heckscher-Ohlin framework the authors find that the effects of technological diffusion allow a country relatively scarce in human capital to benefit from nondecreasing rates of growth through its low-tech sector. They test this idea by using a dynamic panel data approach in order to deal with simultaneity and country heterogeneity. Their results are consistent with the predictions of the model and robust to a broad range of definitions of technological intensity.</p>
         </abstract>
         <kwd-group>
            <kwd>O40</kwd>
            <kwd>F14</kwd>
            <kwd>FI5</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1548e193a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1548e200" publication-type="other">
Grossman and Helpman (1991),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e206" publication-type="other">
Rivera-Batiz and Romer (1991),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e212" publication-type="other">
Barro and Sala-i-Martin (1995),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e219" publication-type="other">
Edwards (1995),</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e226a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1548e233" publication-type="other">
Ventura (1997)</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e240a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1548e247" publication-type="other">
Michaely (1977),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e253" publication-type="other">
Esfa-
hani (1991),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e262" publication-type="other">
Dollar (1992),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e269" publication-type="other">
Harrison (1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e276a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1548e283" publication-type="other">
Frankel and Romer (1999),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e289" publication-type="other">
(Frankel and Romer 1996: 31)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e295" publication-type="other">
Frankel
et al. (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e306a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1548e313" publication-type="other">
Frankel and Romer (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e320a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1548e327" publication-type="other">
Grossman and
Helpman (1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e337a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1548e344" publication-type="other">
Ethier 1982</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e351a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1548e358" publication-type="other">
Gomulka (1990)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e364" publication-type="other">
Edwards (1998)</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e371a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1548e378" publication-type="other">
Chong and Zanforlin 1999</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e385a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1548e394" publication-type="other">
Grossman and Helpman 1991</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e402a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1548e409" publication-type="other">
Grossman and Helpman (1991),</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e416a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1548e423" publication-type="other">
Arellano and Bond (1991),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e429" publication-type="other">
Arellano and Bover (1995),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e435" publication-type="other">
Blundell
and Bond (1997).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e445" publication-type="other">
Frankel and Romer (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e451" publication-type="other">
Frankel et al. (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e458a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1548e465" publication-type="other">
Easterly et al. 1997</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e472a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1548e479" publication-type="other">
Grossman and Helpman (1991).</mixed-citation>
            </p>
         </fn>
         <fn id="d1548e486a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1548e493" publication-type="other">
Holtz-Eakin et al. (1988),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e499" publication-type="other">
Arellano and Bond (1991),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e505" publication-type="other">
Kiviet (1995),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e512" publication-type="other">
Alonso-Borrego and Arellano (1999),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e518" publication-type="other">
Arellano and Bover (1995),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e524" publication-type="other">
Blundell and
Bond (1997),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e533" publication-type="other">
Ziliak (1997).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e539" publication-type="other">
Frankel and Romer (1996),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e545" publication-type="other">
Frankel et al. (1996),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1548e552" publication-type="other">
Harrison (1996)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1548e568a1310">
            <mixed-citation id="d1548e572" publication-type="other">
Aghion, P., and P. Howitt (1998). Endogenous Growth Theory. Cambridge: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1548e579a1310">
            <mixed-citation id="d1548e583" publication-type="other">
Alonso-Borrego, C, and M. Arellano (1999). Symmetrically Normalized Instrumental-
Variable Estimation Using Panel Data. Journal of Business and Economic Statistics
17(1): 36-49.</mixed-citation>
         </ref>
         <ref id="d1548e596a1310">
            <mixed-citation id="d1548e600" publication-type="other">
Arellano, M., and S. Bond (1991). Estimation of Dynamic Models with Error Compo-
nents. Journal of the American Statistical Association 76 (1): 598-606.</mixed-citation>
         </ref>
         <ref id="d1548e610a1310">
            <mixed-citation id="d1548e614" publication-type="other">
Arellano, M., and O. Bover (1995). Another Look at the Instrumental Variable Estima-
tion of Error-Component Models. Journal of Econometrics 68: 29-51.</mixed-citation>
         </ref>
         <ref id="d1548e625a1310">
            <mixed-citation id="d1548e629" publication-type="other">
Barro, R., and J. Lee (1993). International Comparisons of Educational Attainment.
Journal of Monetary Economics 32 (3): 363-394.</mixed-citation>
         </ref>
         <ref id="d1548e639a1310">
            <mixed-citation id="d1548e643" publication-type="other">
Barro, R. J., and X. Sala-i-Martin (1995). Economic Growth. London: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d1548e650a1310">
            <mixed-citation id="d1548e654" publication-type="other">
Blundell, R., and S. Bond (1997). Initial Conditions and Moment Restrictions in Dy-
namic Panel Data Models. Discussion Papers in Economics 97-07. University Col-
lege, London.</mixed-citation>
         </ref>
         <ref id="d1548e667a1310">
            <mixed-citation id="d1548e671" publication-type="other">
Chong, Α., and L. Zanforlin (1999). Technology and Epidemics. Working Paper. Forth-
coming in IMF Staff Papers.</mixed-citation>
         </ref>
         <ref id="d1548e681a1310">
            <mixed-citation id="d1548e685" publication-type="other">
De Long, B., and L. Summers (1991). Equipment Investment and Economic Growth.
Quarterly Journal of Economics 106 (2): 445-502.</mixed-citation>
         </ref>
         <ref id="d1548e695a1310">
            <mixed-citation id="d1548e699" publication-type="other">
Dollar, D. (1992). Outward-Oriented Developing Economies Really Do Grow More
Rapidly: Evidence from 95 LDCs, 1976-1985. Economic Development and Cultu-
ral Change 40 (3): 523-544.</mixed-citation>
         </ref>
         <ref id="d1548e713a1310">
            <mixed-citation id="d1548e717" publication-type="other">
Easterly, W., N. Loayza, and P. Montiel (1997). Why Has Growth in Latin America Been
Disappointing? Journal of International Economics 43: 287-311.</mixed-citation>
         </ref>
         <ref id="d1548e727a1310">
            <mixed-citation id="d1548e731" publication-type="other">
Edwards, S. (1995). Crisis and Reform in Latin America: From Despair to Hope. Ox-
ford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1548e741a1310">
            <mixed-citation id="d1548e745" publication-type="other">
Edwards, S. (1998). Openness, Productivity and Growth: What Do We Really Know?
Economic Journal 108 (March): 383-398.</mixed-citation>
         </ref>
         <ref id="d1548e755a1310">
            <mixed-citation id="d1548e759" publication-type="other">
Esfahani, H. (1991). Exports, Imports, and Economic Growth in Semi-Industrialized
Countries. Journal of Development Economics 35 (1): 93-116.</mixed-citation>
         </ref>
         <ref id="d1548e769a1310">
            <mixed-citation id="d1548e773" publication-type="other">
Ethier, W. (1982). National and International Returns to Scale in the Modern Theory of
International Trade. American Economic Review 72 (3): 389-405.</mixed-citation>
         </ref>
         <ref id="d1548e783a1310">
            <mixed-citation id="d1548e787" publication-type="other">
Frankel, J., and D. Romer (1996). Trade and Growth: An Empirical Investigation. NBER
Working Paper 5476. National Bureau of Economic Research, Cambridge, Mass.</mixed-citation>
         </ref>
         <ref id="d1548e798a1310">
            <mixed-citation id="d1548e802" publication-type="other">
Frankel, J., and D. Romer (1999). Does Trade Cause Growth? American Economic Re-
view 89 (3): 379-399.</mixed-citation>
         </ref>
         <ref id="d1548e812a1310">
            <mixed-citation id="d1548e816" publication-type="other">
Frankel, J., D. Romer, and T. Cyrus (1996). Trade and Growth in East Asian Countries:
Cause and Effect. NBER Working Paper 5732. National Bureau of Economic Re-
search, Cambridge, Mass.</mixed-citation>
         </ref>
         <ref id="d1548e829a1310">
            <mixed-citation id="d1548e833" publication-type="other">
Gomulka, S. (1990). The Theory of Technological Change and Economic Growth. Lon-
don: Routledge.</mixed-citation>
         </ref>
         <ref id="d1548e843a1310">
            <mixed-citation id="d1548e847" publication-type="other">
Grossman, G., and E. Helpman (1991). Innovation and Growth in the Global Economy.
Cambridge: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1548e857a1310">
            <mixed-citation id="d1548e861" publication-type="other">
Harrison, A. (1996). Openness and Growth: A Time-Series, Cross-Country Analysis for
Developing Countries. Journal of Development Economics 48 (2): 419-447.</mixed-citation>
         </ref>
         <ref id="d1548e871a1310">
            <mixed-citation id="d1548e875" publication-type="other">
Holtz-Eakin, D., W. Newey, and H. S. Rosen (1988). Estimating Vector Autoregressions
with Panel Data. Econométrica 56 (6): 1371-1395.</mixed-citation>
         </ref>
         <ref id="d1548e886a1310">
            <mixed-citation id="d1548e890" publication-type="other">
Kiviet, J. F. (1995). On Bias, Inconsistency, and Efficiency of Various Estimators in
Dynamic Panel Data Models. Journal of Econometrics 68 (1): 53-78.</mixed-citation>
         </ref>
         <ref id="d1548e900a1310">
            <mixed-citation id="d1548e904" publication-type="other">
Klodt, H. (1990). Technology Based Trade and Multinationals' Investment in Europe:
Structural Change and Competition in Schumpeterian Goods. Conference on Mul-
tinationals in Europe and Global Trade in the 1990's. Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1548e917a1310">
            <mixed-citation id="d1548e921" publication-type="other">
Learner, E. (1982). Sources of International Comparative Advantage. Cambridge: MIT
Press.</mixed-citation>
         </ref>
         <ref id="d1548e931a1310">
            <mixed-citation id="d1548e935" publication-type="other">
Levin, Α., and L. Raut (1997). Complementarities between Exports and Human Capi-
tal in Economic Growth: Evidence from the Semi-Industrialized Countries. Eco-
nomic Development and Cultural Change 46(1): 155-174.</mixed-citation>
         </ref>
         <ref id="d1548e948a1310">
            <mixed-citation id="d1548e952" publication-type="other">
Loayza, N., K. Schmidt-Hebbel, and L. Serven (2000). What Drives Saving across the
World? Review of Economics and Statistics 82 (2):165-181.</mixed-citation>
         </ref>
         <ref id="d1548e962a1310">
            <mixed-citation id="d1548e966" publication-type="other">
Mankiw, G., D. Romer, and D. Weil (1992). A Contribution to the Empirics of Growth.
Quarterly Journal of Economics 107 (2): 407-437.</mixed-citation>
         </ref>
         <ref id="d1548e977a1310">
            <mixed-citation id="d1548e981" publication-type="other">
Michaely, M. (1977). Exports and Growth: An Empirical Investigation. Journal of De-
velopment Economics 4(1): 49-53.</mixed-citation>
         </ref>
         <ref id="d1548e991a1310">
            <mixed-citation id="d1548e995" publication-type="other">
Pritchett, L. (1996). Measuring Outward Orientation in LDCs: Can It Be Done? Jour-
nal of Development Economics 49 (2): 307-335.</mixed-citation>
         </ref>
         <ref id="d1548e1005a1310">
            <mixed-citation id="d1548e1009" publication-type="other">
Quah, D. (1996). Empirics for Economic Growth and Convergence. European Econom-
ic Review 40 (6): 1353-1375.</mixed-citation>
         </ref>
         <ref id="d1548e1019a1310">
            <mixed-citation id="d1548e1023" publication-type="other">
Rivera-Batiz, L. Α., and P. Romer (1991). Economic Integration and Endogenous
Growth. Quarterly Journal of Economics 106 (2): 531-555.</mixed-citation>
         </ref>
         <ref id="d1548e1033a1310">
            <mixed-citation id="d1548e1037" publication-type="other">
Rodrik, D. (1993). Trade and Industrial Policy Reform in Developing Countries: A Re-
view of Recent Theory and Evidence. NBER Working Paper 4417. National Bureau
of Economic Research, Cambridge, Mass.</mixed-citation>
         </ref>
         <ref id="d1548e1050a1310">
            <mixed-citation id="d1548e1054" publication-type="other">
Romer, P. (1990). Endogenous Technological Change. Journal of Political Economy 98
(5):S71-S102.</mixed-citation>
         </ref>
         <ref id="d1548e1065a1310">
            <mixed-citation id="d1548e1069" publication-type="other">
Summers, R., and A. Heston (1991). The Penn World Table: An Expanded Set of Inter-
national Comparisons, 1950-1988. Quarterly Journal of Economics 106 (2): 327-368.</mixed-citation>
         </ref>
         <ref id="d1548e1079a1310">
            <mixed-citation id="d1548e1083" publication-type="other">
United Nations (various years). Yearbook of International Trade Statistics. Geneva: Uni-
ted Nations.</mixed-citation>
         </ref>
         <ref id="d1548e1093a1310">
            <mixed-citation id="d1548e1097" publication-type="other">
Ventura, J. (1997). Growth and Interdependence. Quarterly Journal of Economics 112
(1): 57-84.</mixed-citation>
         </ref>
         <ref id="d1548e1107a1310">
            <mixed-citation id="d1548e1111" publication-type="other">
World Bank (1996). The East Asian Miracle. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1548e1118a1310">
            <mixed-citation id="d1548e1122" publication-type="other">
World Bank (1997). World Development Indicators. CD ROM. Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1548e1129a1310">
            <mixed-citation id="d1548e1133" publication-type="other">
Ziliak, J. (1997). Efficient Estimation with Panel Data When Instruments Are Predeter-
mined: An Empirical Comparison of Moment-Condition Estimators. Journal of Busi-
ness and Economic Statistics 15 (4): 419-431.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
