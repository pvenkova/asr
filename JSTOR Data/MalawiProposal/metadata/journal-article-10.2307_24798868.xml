<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">jstudyreligion</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50012134</journal-id>
         <journal-title-group>
            <journal-title>Journal for the Study of Religion</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Association for the Study of Religion in Southern Africa(ASRSA)</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10117601</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">24133027</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24798868</article-id>
         <title-group>
            <article-title>Religion Education, Intercultural Education and Human Rights:</article-title>
            <subtitle>A Contribution for Cornelia Roux</subtitle>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Jackson</surname>
                  <given-names>Robert</given-names>
               </string-name>
               <aff>
                  <addr-line>Professor of Religions and Education</addr-line>
                  <addr-line>University of Warwick; and</addr-line>
                  <addr-line>Special Adviser</addr-line>
                  <addr-line>European Wergeland Centre</addr-line>
                  <addr-line>Oslo</addr-line>
               </aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2014</year>
            <string-date>2014</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">27</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e24798864</issue-id>
         <fpage>7</fpage>
         <lpage>22</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24798868"/>
         <abstract>
            <label>Abstract</label>
            <p>In this article, I draw on my own experience as a researcher, writer on theory and pedagogy of religion education¹ and contributor to European policy documents. This provides a basis to discuss some issues pertinent to Cornelia Roux’s personal and professional journey as a researcher in religion education and related fields, including intercultural education, human rights education and citizenship education. I refer to our meetings over the years, both in and beyond South Africa, especially in the context of the International Network for Interreligious and Intercultural Education, and to the development of Professor Roux’s ideas on Religion in Education (RiE), Religion and Education (RaE). An attempt is then made to articulate a view on the question of liberalism in relation to human rights, which connects to a stance on intercultural education and to religion education and values education more widely. The position developed is consistent with the approach to empirical research developed by Professor Roux and her team. The article concludes by relating Cornelia Roux’s personal journey to some of the themes considered above.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>religion education</kwd>
            <kwd>intercultural education</kwd>
            <kwd>human rights</kwd>
            <kwd>political liberalism</kwd>
            <kwd>comprehensive liberalism</kwd>
            <kwd>plurality</kwd>
            <kwd>pluralism</kwd>
            <kwd>identity formation</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editors">
                  <string-name>Andree, T.</string-name>,<string-name>C. Bakker</string-name>
               </person-group>&amp;<person-group person-group-type="editor">
                  <string-name>P. Schreiner</string-name>
               </person-group>(eds.)<year>1997</year>.<source>Crossing Boundaries: Contributions to Interreligious and Intercultural Education</source>.:<publisher-name>Comenius Institut</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Baumann, G.</string-name>
               </person-group>
               <year>1996</year>.<source>Contesting Culture: Discourses of Identity in Multi- Ethnic London</source>.:<publisher-name>Cambridge University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Baumann, G.</string-name>
               </person-group>
               <year>1999</year>.<source>The Multicultural Riddle: Rethinking National, Ethnic and Religious Identities</source>.:<publisher-name>Routledge</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Chidester, D.</string-name>
               </person-group>(ed.)<year>1992</year>.<source>Religion in Public Education: Policy Options for a New South Africa</source>.:<publisher-name>Institute for Comparative Religion in Southern Africa</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editors">
                  <string-name>Chidester, D.</string-name>,<string-name>J. Stonier</string-name>
               </person-group>&amp;<person-group person-group-type="editor">
                  <string-name>J. Tobler</string-name>
               </person-group>(eds.)<year>1999</year>.<source>Diversity as Ethos: Challenges for Interreligious and Intercultural Education</source>.:<publisher-name>Institute for Comparative Religion in Southern Africa</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <collab>Council of Europe</collab>
               </person-group>
               <year>2008</year>a.<source>White Paper on Intercultural Dialogue: ‘Living together as Equals with Dignity’</source>.:<publisher-name>Council of Europe Publishing</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <collab>Council of Europe</collab>
               </person-group>
               <year>2008</year>b.<source>Recommendation CM/Rec(2008)12 of the Committee of Ministers to Member States on the Dimension of Religions and Non-religious Convictions within Intercultural Education</source>.:<publisher-name>Council of Europe Publishing</publisher-name>. [Also available at:<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="https://wcd.coe.int//ViewDoc.jsp?Ref=CM/Rec(2008)12&amp;amp;Language=lan English&amp;amp;Ver=original&amp;amp;BackColorInternet=DBDCF2&amp;amp;BackColorIntranet=FDC864&amp;amp;BackColorLogged=FDC864">https://wcd.coe.int//ViewDoc.jsp?Ref=CM/Rec(2008)12&amp;Language=lan English&amp;Ver=original&amp;BackColorInternet=DBDCF2&amp;BackColorIntranet=FDC864&amp;BackColorLogged=FDC864</ext-link>. (Accessed on<date-in-citation>
                  <day>30</day>
                  <month>December</month>
                  <year>2013</year>
               </date-in-citation>.)]</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Hopgood, S.</string-name>
               </person-group>
               <year>2013</year>.<source>The Endtimes of Human Rights</source>.:<publisher-name>Cornell University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <collab>InterAction Council</collab>
               </person-group>
               <year>1997</year>.<source>Universal Declaration of Human Responsibilities</source>. Available at:<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://interactioncouncil.org/a-universal-declaration-ofhuman-responsibilities">http://interactioncouncil.org/a-universal-declaration-ofhuman-responsibilities</ext-link>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Jackson, R.</string-name>
               </person-group>(ed.)<year>2003</year>.<source>International Perspectives on Citizenship, Education and Religious Diversity</source>.:<publisher-name>RoutledgeFalmer</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Jackson, R.</string-name>
               </person-group>
               <year>2004</year>.<source>Rethinking Religious Education and Plurality: Issues in Diversity and Pedagogy</source>.:<publisher-name>RoutledgeFalmer</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Jackson, R.</string-name>
               </person-group>
               <year>2011</year>.<source>Studying Religions: The Interpretive Approach in Brief</source>. Available at:<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www.theewc.org/library/category/view/studying.religions.the.interpretive.approach.in.brief/">http://www.theewc.org/library/category/view/studying.religions.the.interpretive.approach.in.brief/</ext-link>.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <string-name>Jackson, R.</string-name>
               </person-group>
               <year>2014</year>a.<article-title>The Development and Dissemination of Council of Europe Policy on Education about Religions and Non-religious Convictions</article-title>.<source>Journal of Beliefs and Values: Studies in Religion &amp; Education</source>.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Jackson, R.</string-name>
               </person-group>
               <year>2014</year>b.<source>‘Signposts’: Policy and Practice for Teaching about Religions and Non-Religious Worldviews in Intercultural Education</source>.:<publisher-name>Council of Europe Publishing</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>MacIntyre, A.</string-name>
               </person-group>
               <year>1981</year>.<source>After Virtue</source>. Second Edition.:<publisher-name>Duckworth</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Meijer, W.A.J.</string-name>
               </person-group>
               <year>1995</year>.<article-title>The Plural Self: A Hermeneutical View on Identity and Plurality</article-title>.<source>British Journal of Religious Education</source>
               <volume>17</volume>,<issue>2</issue>:<fpage>92</fpage>–<lpage>99</lpage>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Morsink, J.</string-name>
               </person-group>
               <year>1999</year>.<source>The Universal Declaration of Human Rights: Origins, Drafting and Intent</source>.:<publisher-name>University of Pennsylvania Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Nesbitt, E.</string-name>
               </person-group>
               <year>2013</year>.<chapter-title>Ethnography, Religious Education and<italic>The Fifth Cup</italic>
               </chapter-title>. In<person-group person-group-type="editors">
                  <string-name>Miller, J.</string-name>,<string-name>K. O’Grady</string-name>
               </person-group>&amp;<person-group person-group-type="editor">
                  <string-name>U. McKenna</string-name>
               </person-group>(eds.):<source>Religion in Education: Innovation in International Research</source>.:<publisher-name>Routledge</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Parekh, B.</string-name>
               </person-group>
               <year>1994</year>.<chapter-title>Decolonising Liberalism</chapter-title>. In<person-group person-group-type="author">
                  <string-name>Shtromas, A.</string-name>
               </person-group>
               <source>The End of Isms? Reflections on the Fate of Ideological Politics after Communism's Collapse</source>.:<publisher-name>Blackwell</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>du Preez, P.</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>C. Roux</string-name>
               </person-group>
               <year>2010</year>.<article-title>Human Rights Values or Cultural Values? Pursuing Values to Maintain Positive Discipline in Multicultural Schools</article-title>.<source>South African Journal of Education</source>
               <volume>30</volume>:<fpage>13</fpage>-<lpage>26</lpage>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Rawls, J.</string-name>
               </person-group>
               <year>1993</year>.<source>Political Liberalism</source>.:<publisher-name>Columbia University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">
               <person-group person-group-type="editor">
                  <string-name>Roux, C.</string-name>
               </person-group>(ed.)<year>2005</year>.<source>International Network for Interreligious and Intercultural Education</source>. Special Issue of Scriptura 2005,<issue>2</issue>.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Roux, C.</string-name>
               </person-group>
               <year>2009</year>.<article-title>Religion in Education: Who is Responsible?</article-title>
               <source>Alternation Special Edition</source>
               <volume>3</volume>:<fpage>3</fpage>-<lpage>30</lpage>.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Roux, C.</string-name>
               </person-group>
               <year>2012</year>a.<chapter-title>Conflict or Cohesion? A Critical Discourse on Religion in Education (RiE) and Religion and Education (RaE)</chapter-title>. In<person-group person-group-type="editor">
                  <string-name>ter Avest, I.</string-name>
               </person-group>(ed.):<source>On the Edge: (Auto)biography and Pedagogical Theories on Religious Education</source>.:<publisher-name>Sense Publishers</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Roux, C.</string-name>
               </person-group>(ed.)<year>2012</year>b.<source>Safe Spaces: Human Rights Education in Diverse Contexts</source>.:<publisher-name>Sense Publishers</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Roux, C.</string-name>
               </person-group>
               <year>2012</year>c.<chapter-title>A Social Justice and Human Rights Education Project: A Search for Caring and Safe Spaces</chapter-title>. In<person-group person-group-type="editor">
                  <string-name>Roux, C.</string-name>
               </person-group>(ed.):<source>Safe Spaces: Human Rights Education in Diverse Contexts</source>.:<publisher-name>Sense Publishers</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Skeie, G.</string-name>
               </person-group>
               <year>2003</year>.<chapter-title>Nationalism, Religiosity and Citizenship in Norwegian Majority and Minority Discourses</chapter-title>. In<person-group person-group-type="editor">
                  <string-name>Jackson, R.</string-name>
               </person-group>(ed.):<source>International Perspectives on Citizenship, Education and Religious Diversity</source>.:<publisher-name>RoutledgeFalmer</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Tan, K-C.</string-name>
               </person-group>
               <year>2000</year>.<source>Toleration, Diversity, and Global Justice</source>.:<publisher-name>Pennsylvania State University Press</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>ter Avest, I.</string-name>
               </person-group>(ed.)<year>2011</year>.<source>Contrasting Colours: European and African Perspectives on Education in a Context of Diversity</source>.:<publisher-name>Gopher B.V.</publisher-name>
            </mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">
               <person-group person-group-type="author">
                  <collab>United Nations</collab>
               </person-group>
               <year>1948</year>.<source>Universal Declaration of Human Rights</source>. Available at:<ext-link xmlns:xlink="http://www.w3.org/1999/xlink"
                         ext-link-type="uri"
                         xlink:href="http://www.un.org/en/documents/udhr/">http://www.un.org/en/documents/udhr/</ext-link>.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="book">
               <person-group person-group-type="editor">
                  <string-name>Weisse, W.</string-name>
               </person-group>(ed.)<year>1996</year>.<source>Interreligious and Intercultural Education: Methodologies, Conceptions and Pilot Projects in South Africa, Namibia, Great Britain, the Netherlands and Germany</source>.:<publisher-name>Comenius Institut</publisher-name>.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="journal">
               <person-group person-group-type="authors">
                  <string-name>de Wet, A.</string-name>,<string-name>C. Roux</string-name>,<string-name>S. Simmonds</string-name>
               </person-group>&amp;<person-group person-group-type="author">
                  <string-name>I. ter Avest</string-name>
               </person-group>
               <year>2012</year>.<article-title>Girls’ and Boys’ Reasoning on Cultural and Religious Practices: A Human Rights Education Perspective</article-title>.<source>Gender and Education</source>
               <volume>24</volume>,<issue>6</issue>:<fpage>665</fpage>-<lpage>681</lpage>.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
