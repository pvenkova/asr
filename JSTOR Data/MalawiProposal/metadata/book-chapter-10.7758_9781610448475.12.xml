<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">9781610448475</book-id>
      <subj-group>
         <subject content-type="call-number">JV6347.D66 2016</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Women immigrants</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Emigration and immigration</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Gender and International Migration</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Donato</surname>
               <given-names>Katharine M.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Gabaccia</surname>
               <given-names>Donna</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>03</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780871545466</isbn>
      <isbn content-type="epub">9781610448475</isbn>
      <publisher>
         <publisher-name>Russell Sage Foundation</publisher-name>
         <publisher-loc>New York</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Russell Sage Foundation</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7758/9781610448475"/>
      <abstract abstract-type="short">
         <p>In 2006, the United Nations reported on the "feminization" of migration, noting that the number of female migrants had doubled over the last five decades. Likewise, global awareness of issues like human trafficking and the exploitation of immigrant domestic workers has increased attention to the gender makeup of migrants. But are women really more likely to migrate today than they were in earlier times? In<italic>Gender and International Migration</italic>, sociologist and demographer Katharine Donato and historian Donna Gabaccia evaluate the historical evidence to show that women have been a significant part of migration flows for centuries. The first scholarly analysis of gender and migration over the centuries,<italic>Gender and International Migration</italic>demonstrates that variation in the gender composition of migration reflect not only the movements of women relative to men, but larger shifts in immigration policies and gender relations in the changing global economy.</p>
         <p>While most research has focused on women migrants after 1960, Donato and Gabaccia begin their analysis with the fifteenth century, when European colonization and the transatlantic slave trade led to large-scale forced migration, including the transport of prisoners and indentured servants to the Americas and Australia from Africa and Europe. Contrary to the popular conception that most of these migrants were male, the authors show that a significant portion were women. The gender composition of migrants was driven by regional labor markets and local beliefs of the sending countries. For example, while coastal ports of western Africa traded mostly male slaves to Europeans, most slaves exiting east Africa for the Middle East were women due to this region's demand for female reproductive labor.</p>
         <p>Donato and Gabaccia show how the changing immigration policies of receiving countries affect the gender composition of global migration. Nineteenth-century immigration restrictions based on race, such as the Chinese Exclusion Act in the United States, limited male labor migration. But as these policies were replaced by regulated migration based on categories such as employment and marriage, the balance of men and women became more equal - both in large immigrant-receiving nations such as the United States, Canada, and Israel, and in nations with small immigrant populations such as South Africa, the Philippines, and Argentina. The gender composition of today's migrants reflects a much stronger demand for female labor than in the past. The authors conclude that gender imbalance in migration is most likely to occur when coercive systems of labor recruitment exist, whether in the slave trade of the early modern era or in recent guest-worker programs.</p>
         <p>Using methods and insights from history, gender studies, demography, and other social sciences,<italic>Gender and International Migration</italic>shows that feminization is better characterized as a gradual and ongoing shift toward gender balance in migrant populations worldwide. This groundbreaking demographic and historical analysis provides an important foundation for future migration research.</p>
      </abstract>
      <counts>
         <page-count count="270"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.3</book-part-id>
                  <title-group>
                     <title>List of Figures and Tables</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.4</book-part-id>
                  <title-group>
                     <title>About the Authors</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In 1984, demographers at the U.S. Department of Labor published data showing more women than men among U.S. immigrants. Appearing in the first special issue devoted to research on immigrant women in the flagship journal,<italic>International Migration Review,</italic>the authors documented a female majority of 52.3 percent among recent immigrants to the United States. In the same issue, scholars also examined migrant women’s work, lives, and experience in other parts of the world. For example, Saskia Sassen-Koob pointed to the feminization of the workforce in small-scale agricultural production in sending areas characterized by male out-migration, in global assembly plants in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.7</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Data and Discipline:</title>
                     <subtitle>Discovering the Feminization of Migration</subtitle>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>Scholars who discussed the feminization of migration at the end of the twentieth century suggested that it was a recent development even as they presented findings pointing toward much earlier change. In this book, we present considerable evidence of earlier periods of feminization and offer an explanation for a shift toward gender balance that stretched across the entire twentieth century. Before we present that historical evidence, however, it seems important to answer the question implicit in Hania Zlotnik’s acknowledgment that most feminization occurred before 1960. If migrations had begun to feminize already in the 1910s and 1920s (as we show</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.8</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Analyzing Migrant Gender Composition with Statistical Data on Sex</title>
                  </title-group>
                  <fpage>39</fpage>
                  <abstract>
                     <p>The sex ratio is the most widely used analytical category for the study of the relative numbers of males and females in populations.¹ Yet recent studies of the feminization of migration—including the 1984 and 2006 statistical studies previously discussed—do not measure the relative numbers of male and female migrants this way. Instead, they compare the percentage female among migrants over time and space. In this book, we too focus on the percentage female among migrants, and we analyze migrant gender composition rather than sex composition to explore how gender ideology and gender relations influenced migration over four centuries</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.9</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Gender and Early Modern Migrations, 1492–1867</title>
                  </title-group>
                  <fpage>55</fpage>
                  <abstract>
                     <p>If indeed the “past is a foreign country” where “they do things differently,” then the great vastness of that strange place poses a research problem as large as the spatial vastness of today’s interconnected and mobile world.¹ This volume slices up the past into segments to render it analytically manageable. We thus follow history’s disciplinary conventions, linking periods of stability and change in migrant gender composition to the periodization of global integration over four centuries.</p>
                     <p>This chapter analyzes variation in gender composition during the early modern era, a period that stretches from the late fifteenth century, when two formerly isolated</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.10</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Global Convergence:</title>
                     <subtitle>Gender and the Proletarian Mass Migrations, 1800–1924</subtitle>
                  </title-group>
                  <fpage>74</fpage>
                  <abstract>
                     <p>Writing the<italic>Communist Manifesto</italic>in the midst of the revolutions of 1848, Karl Marx and Friedrich Engels identified many phenomena that late twentieth-century theorists would label as globalization. In the eyes of the two dialectical materialists, “the need of a constantly expanding market for its products chases the bourgeoisie over the whole surface of the globe. It must nestle everywhere, settle everywhere, establish connections everywhere.”¹ Even more than the bourgeoisie, however, it was Marx’s “working men of all nations” who spread across the globe in the nineteenth century. Rates of migration surged in almost every part of the world after</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>The Twentieth Century:</title>
                     <subtitle>Migrant Gender Composition After 1918</subtitle>
                  </title-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>Convergence toward heavily male migrations was a distinguishing characteristic of nineteenth-century global economic integration. Feminization, by contrast, would characterize the entire twentieth century. Beginning with the unraveling of an earlier global economy around 1920, the feminization of migration finally attracted attention during a renewed period of global integration at century’s end. This made it easy for scholars and policymakers to assume that what they called globalization had caused feminization. The statistical record suggests otherwise, however. Instead, the much longer-term shift toward gender balance reflected the impact of new regulations and restrictions imposed on migrants by national states and international organizations</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.12</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>How and Why Migrant Gender Composition Varies, 1970–2010</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>In this chapter, we examine recent variation in migrant gender composition by analyzing stock data while taking into account the age structure of migrant populations. In previous chapters, we describe some of the relative strengths and weaknesses of stock data. Here we note several additional issues that may matter especially to scholars interested in gender and migration. First, because past estimates of migrant gender composition have not paid attention to population age structures, they may also have exaggerated both the degree of migrant feminization and the extent of female presence among recent migrants, especially for countries with long histories of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.13</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Consequences:</title>
                     <subtitle>A Story of Moderation and Convergence</subtitle>
                  </title-group>
                  <fpage>150</fpage>
                  <abstract>
                     <p>In this chapter, we aim to open a conversation about the effects of migrant gender composition. We examine variation in the gender composition of U.S. immigrants over the last 150 years and consider its possible consequences. We focus on the United States because extensive census data are available and permit us to examine feminization and some of its consequences over a long period. The findings here do not suggest widespread consequences of increased female presence among U.S. migrants. Instead, the investigation of consequences presented here suggests that the gender balance among U.S. immigrants observed since 1970 has led to a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.14</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>177</fpage>
                  <abstract>
                     <p>In this book, we analyze the gender composition of immigrant populations across time and space. We also emphasize how, why, and in what form data are created, showing that using different types of data and measurements may make women and feminization visible or invisible. In this way, we seek to “learn the language of statistics” by understanding the perspectives that underlie different data sets and their measurements and showing how they render (or do not render) gender invisible. In addition, we conceptualize feminization as a temporal process made up of significant variations in migrant gender composition defined as male-and female-predominant,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.15</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>185</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.16</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>204</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">9781610448475.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
