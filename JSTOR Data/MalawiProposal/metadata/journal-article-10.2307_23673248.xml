<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plansystevol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009192</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant Systematics and Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03782697</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16156110</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23673248</article-id>
         <title-group>
            <article-title>Genome sizes of Eucomis L'Hér. (Hyacinthaceae) and a description of the new species Eucomis grimshawii G.D.Duncan &amp; Zonneveld</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>B. J. M.</given-names>
                  <surname>Zonneveld</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>G. D.</given-names>
                  <surname>Duncan</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">284</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23672954</issue-id>
         <fpage>99</fpage>
         <lpage>109</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23673248"/>
         <abstract>
            <p>Nuclear genome size, as measured by flow cytometry with propidium iodide, was used to investigate the relationships within the genus Eucomis L'Hér. (Hyacinthaceae). Most species of Eucomis have the same basic chromosome number, x = 15. However, the somatic DNA 2C-value (2C) is shown to range from 21 to 31 pg for the diploids. The largest genome contains roughly 1010 more base pairs than the smallest. Genome sizes are evaluated here in combination with available morphological and geographical data. Therefore, the taxonomy proposed here is not based on genome size alone. The genus Eucomis, as here determined, has 12 species. These can be divided into two groups: mainly dwarf diploid species and large-sized, tetraploid species. A small diploid plant, Eucomis (autumnalis subsp.) amaryllidifolia, is restored to species status, as a diploid subspecies seems incongruent with an allotetraploid Eucomis autumnalis. Moreover, as a diploid it is separated reproductively from the allotetraploid E. autumnalis. A new diploid species that has the lowest C value, E. grimshawii, is described here. On the basis of DNA content and other morphological characters, possible parents are suggested for all tetraploid species. Nuclear DNA content as measured by using flow cytometry may conveniently be used to produce systematic data. It is applicable even in dormant bulbs or sterile plants for the monitoring of the trade in bulbous species.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d281e158a1310">
            <mixed-citation id="d281e162" publication-type="other">
Baker JG (1878) New garden plants: Eucomis amaryllidifolia and
Eucomis bicolor. Gard Chron 10:492</mixed-citation>
         </ref>
         <ref id="d281e172a1310">
            <mixed-citation id="d281e176" publication-type="other">
Baker JG (1886) Eucomis zambesiaca. Gard Chron 25:9</mixed-citation>
         </ref>
         <ref id="d281e183a1310">
            <mixed-citation id="d281e187" publication-type="other">
Baker JG (1887) New or noteworthy plants: Eucomis pallidiflora.
Gard Chron (2): 154</mixed-citation>
         </ref>
         <ref id="d281e197a1310">
            <mixed-citation id="d281e201" publication-type="other">
Baker JG (1895) Eucomis humilis. Bull Mise Inform Kew 152</mixed-citation>
         </ref>
         <ref id="d281e209a1310">
            <mixed-citation id="d281e213" publication-type="other">
Botschantzeva ZP (1962) Tulips: taxonomy, morphology, cytology,
phytogeography, and physiology. English translated edition by
HQ Varekamp (1982) p. 1-230. Balkema, Rotterdam</mixed-citation>
         </ref>
         <ref id="d281e226a1310">
            <mixed-citation id="d281e230" publication-type="other">
Brown NE (1918) New or noteworthy plants: Eucomis pole-evansii.
Gard Chron III 63:185</mixed-citation>
         </ref>
         <ref id="d281e240a1310">
            <mixed-citation id="d281e244" publication-type="other">
Chittenden FJ (1951) Eucomis autumnalis. Roy Hort Soc Diet Gard
2:787</mixed-citation>
         </ref>
         <ref id="d281e254a1310">
            <mixed-citation id="d281e258" publication-type="other">
Compton RH (1967) Plantae Novae Africanae: Eucomis montana. J S
Afr Bot 33:293-294</mixed-citation>
         </ref>
         <ref id="d281e268a1310">
            <mixed-citation id="d281e272" publication-type="other">
Compton J (1990) Eucomis L'Héritier. The Plantsman 12-3:129-139</mixed-citation>
         </ref>
         <ref id="d281e279a1310">
            <mixed-citation id="d281e283" publication-type="other">
Dillenius JJ (1732) Hortus Ethamensis. Sumptibus Auctoris, London</mixed-citation>
         </ref>
         <ref id="d281e291a1310">
            <mixed-citation id="d281e295" publication-type="other">
Dolezel J, Bartos J, Voglmayer H, Greilhuber J (2003) Nuclear DNA
content and genome size of trout and human. Cytometry
51a:127-128</mixed-citation>
         </ref>
         <ref id="d281e308a1310">
            <mixed-citation id="d281e312" publication-type="other">
Duncan GD (2007) Lesser known Eucomis. The Plantsman 6:98-103</mixed-citation>
         </ref>
         <ref id="d281e319a1310">
            <mixed-citation id="d281e323" publication-type="other">
Fabian A (1982) Transvaal Wild Flowers: 26 (plate 8b). Macmillan
Publication, USA</mixed-citation>
         </ref>
         <ref id="d281e333a1310">
            <mixed-citation id="d281e337" publication-type="other">
Govaerts R (2006) World checklist series RBG Kew, UK: genus
Eucomis. http://apps.kew.org/wcsp/qsearch.do77</mixed-citation>
         </ref>
         <ref id="d281e347a1310">
            <mixed-citation id="d281e351" publication-type="other">
Greilhuber J (1979) Evolutionary changes of DNA and Heterochro-
matin amounts in the Scilla bifolia Group (Liliaceae). Plant Syst
Evol Suppl 2:263-280</mixed-citation>
         </ref>
         <ref id="d281e364a1310">
            <mixed-citation id="d281e368" publication-type="other">
Greilhuber J (1998) Intraspecific variation in genome size: a critical
reassessment. Ann Bot 82:27-35</mixed-citation>
         </ref>
         <ref id="d281e379a1310">
            <mixed-citation id="d281e383" publication-type="other">
Greilhuber J (2005) Intraspecific variation in genome size in
angiosperms, identifying its existence. Ann Bot 95:91-98</mixed-citation>
         </ref>
         <ref id="d281e393a1310">
            <mixed-citation id="d281e397" publication-type="other">
Leitch I, Chase MW, Bennett MD (1998) Phylogenetic analysis of
DNA C-values provides evidence for a small ancestral genome
size in flowering plants. Ann Bot 82(Suppl. A):85-94</mixed-citation>
         </ref>
         <ref id="d281e410a1310">
            <mixed-citation id="d281e414" publication-type="other">
Ohri D (1998) Genome size variation and plant systematics. Ann Bot
82(Suppl. A):750-812</mixed-citation>
         </ref>
         <ref id="d281e424a1310">
            <mixed-citation id="d281e428" publication-type="other">
Reyneke WF (1972) 'n Monografiese studie van die genus Eucomis
L'Hérit. in Suid Afrika. Department of Botany, University of
Pretoria, unpublished Msc thesis</mixed-citation>
         </ref>
         <ref id="d281e441a1310">
            <mixed-citation id="d281e445" publication-type="other">
Reyneke WF (1976) A new species of Eucomis L'Hérit. (Liliaceae)
from South Africa. J S Afr Bot 42-4:361-364</mixed-citation>
         </ref>
         <ref id="d281e455a1310">
            <mixed-citation id="d281e459" publication-type="other">
Reyneke WF (1980) Three subspecies of Eucomis autumnalis.
Bothalia 13:140-142</mixed-citation>
         </ref>
         <ref id="d281e470a1310">
            <mixed-citation id="d281e474" publication-type="other">
Reyneke WF, Liebenberg H (1980) Karyotype analysis of the genus
Eucomis (Liliaceae). J S Afr Bot 46-4:355-360</mixed-citation>
         </ref>
         <ref id="d281e484a1310">
            <mixed-citation id="d281e488" publication-type="other">
Taylor JLS, van Staden J (2001) COX-1 inhibitory activity in extracts
from Eucomis L'Herif species. J Ethnopharm 75:257-265</mixed-citation>
         </ref>
         <ref id="d281e498a1310">
            <mixed-citation id="d281e502" publication-type="other">
Tiersch TR, Chandler RW, Wachtel SSM, Ellias S (1989) Reference
standards for flow cytometry and application in comparative
studies of nuclear DNA content. Cytometry 10:706-710</mixed-citation>
         </ref>
         <ref id="d281e515a1310">
            <mixed-citation id="d281e519" publication-type="other">
Verdoorn IC (1944) Eucomis vandermerwei. FI PI S Africa 24:t. 955</mixed-citation>
         </ref>
         <ref id="d281e526a1310">
            <mixed-citation id="d281e530" publication-type="other">
Woods MW, Bamford R (1937) Chromosome morphology and
number in Tulipa. Am J Bot 24:174-184</mixed-citation>
         </ref>
         <ref id="d281e540a1310">
            <mixed-citation id="d281e544" publication-type="other">
Zonneveld BJM (2001) Nuclear DNA contents of all species of
Helleborus discriminate between species and sectional divisions.
Plant Syst Evol 229:125-130</mixed-citation>
         </ref>
         <ref id="d281e558a1310">
            <mixed-citation id="d281e562" publication-type="other">
Zonneveld BJM (2003) The systematic value of nuclear DNA content
in Clivia. Herbertia 57:41-47</mixed-citation>
         </ref>
         <ref id="d281e572a1310">
            <mixed-citation id="d281e576" publication-type="other">
Zonneveld BJM (2008) The systematic value of nuclear DNA content
for all species of Narcissus L. (Amaryllidaceae). Plant Syst Evol
275:109-132</mixed-citation>
         </ref>
         <ref id="d281e589a1310">
            <mixed-citation id="d281e593" publication-type="other">
Zonneveld BJM (2009) The systematic value of nuclear genome size
for all species of Tulipa L. (Liliaceae). Plant Syst Evol (in press)</mixed-citation>
         </ref>
         <ref id="d281e603a1310">
            <mixed-citation id="d281e607" publication-type="other">
Zonneveld BJM, Duncan GD (2003) Taxonomic implications of
genome size and pollen color and vitality for species of
Agapanthus L'Héritier (Agapanthaceae). Plant Syst Evol
241:115-123</mixed-citation>
         </ref>
         <ref id="d281e623a1310">
            <mixed-citation id="d281e627" publication-type="other">
Zonneveld BJM, Duncan GD (2006) Genome size for the species of
Nerine Herb. (Amaryllidaceae) and its evident correlation with
growth cycle, leaf width and other morphological characters.
Plant Syst Evol 257:251-260</mixed-citation>
         </ref>
         <ref id="d281e643a1310">
            <mixed-citation id="d281e647" publication-type="other">
Zonneveld BJM, Van Iren F (2001) Genome size and pollen viability
as taxonomic criteria: application to the genus Hosta. Plant Biol
3:176-185</mixed-citation>
         </ref>
         <ref id="d281e661a1310">
            <mixed-citation id="d281e665" publication-type="other">
Zonneveld BJM, Van Jaarsveld EJ (2005) Taxonomic implications of
genome size for all species of the genus Gasteria Duval
(Aloaceae). Plant Syst Evol 251:217-227</mixed-citation>
         </ref>
         <ref id="d281e678a1310">
            <mixed-citation id="d281e682" publication-type="other">
Zonneveld BJM, Grimshaw JM, Davis AP (2003) The systematic
value of nuclear DNA content in Galanthus. Plant Syst Evol
241:89-102</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
