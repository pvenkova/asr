<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1hfr12q</book-id>
      <subj-group>
         <subject content-type="call-number">HC800.R33 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
            <subj-group>
               <subject content-type="lcsh">1960-</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Economic policy</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Emerging Africa</book-title>
         <subtitle>How 17 Countries Are Leading the Way</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Radelet</surname>
               <given-names>Steven</given-names>
            </name>
         </contrib>
         <contrib contrib-type="foreword-author" id="contrib2">
            <role>With an introduction by</role>
            <name name-style="western">
               <surname>Sirleaf</surname>
               <given-names>Ellen Johnson</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>10</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9781933286518</isbn>
      <isbn content-type="epub">9781933286525</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Baltimore, MD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>CENTER FOR GLOBAL DEVELOPMENT</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1hfr12q"/>
      <abstract abstract-type="short">
         <p>Emerging Africa describes the too-often-overlooked positive changes that have taken place in much of Africa since the mid-1990s. In 17 countries, five fundamental and sustained breakthroughs are making old assumptions increasingly untenable:</p>
         <p>• The rise of democracy brought on by the end of the Cold War and apartheid</p>
         <p>• Stronger economic management</p>
         <p>• The end of the debt crisis and a more constructive relationship with the international community</p>
         <p>• The introduction of new technologies, especially mobile phones and the Internet</p>
         <p>• The emergence of a new generation of leaders.</p>
         <p>With these significant changes, the countries of emerging Africa seem poised to lead the continent out of the conflict, stagnation, and dictatorships of the past. The countries discussed in the book are Botswana, Burkina Faso, Cape Verde, Ethiopia, Ghana, Lesotho, Mali Mauritius, Mozambique, Namibia, Rwanda, São Tomé and Principe, Seychelles, South Africa, Tanzania, Uganda, and Zambia.</p>
      </abstract>
      <counts>
         <page-count count="169"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.3</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Oppenheimer</surname>
                           <given-names>Jennifer Ward</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>To my surprise and pleasure, I found myself in 2003 seated next to an award-winning<italic>New York Times</italic>journalist on a flight from Gabarone to Johannesburg. My failure to win him over on the issue we subsequently discussed has bothered me ever since. The debate centered on a simple question: Is it more important to get the good or the bad news out of Africa?</p>
                     <p>Of course, most readers will immediately perceive that this is a false dichotomy; it is pointless to say just one or the other. A more balanced answer would be that it is important to get</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.5</book-part-id>
                  <title-group>
                     <title>ABOUT CGD</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.6</book-part-id>
                  <title-group>
                     <title>Map of Emerging Africa</title>
                     <subtitle>Average Growth Rates per Capita, 1996–2008</subtitle>
                  </title-group>
                  <fpage>xvi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.7</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sirleaf</surname>
                           <given-names>Ellen Johnson</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Twenty years ago, sub-Saharan Africa (SSA) was a region of despair. Outside of Botswana and Mauritius, democracy was but a distant dream. Unelected and unaccountable governments held power across the subcontinent. Dictators treated their countries as personal fiefdoms, taking what they wanted, doling out riches to a favored few, and sprinkling a handful of crumbs to the rest. They jailed or executed those who spoke out or whom they just didn’t like, and they ruled by force and intimidation. The terrible scar of apartheid made a mockery of justice and plunged the entire southern region into conflict and crisis. And</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.8</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>EMERGING AFRICA</title>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>There’s good news out of Africa. Not all of Africa. But from a large part of Africa that quietly, with little fanfare, is on the move.</p>
                     <p>Seventeen emerging African countries are putting behind them the conflict, stagnation, and dictatorships of the past. Since the mid-1990s—<italic>for fifteen years</italic>—they have achieved steady economic growth, deepening democracy, stronger leadership, and falling poverty. Six additional African countries are showing signs of following their lead. The old negative stereotypes of sub-Saharan Africa don’t apply to these countries. Not anymore.</p>
                     <p>Consider Ghana, where over the past 15 years the economy has grown by a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.9</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>EMERGING AFRICA’S RENAISSANCE</title>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>When Mozambique’s civil war ended in 1992, the country was in ruins. Armed rebellion against Portuguese colonial rule had started in the 1960s, but conflict had intensified significantly after the 1974 coup in Lisbon led to Portugal’s withdrawal. The new government in Maputo established one-party rule, aligned itself with the Soviet Union, and provided support to the liberation movements in South Africa and Rhodesia, while the governments of South Africa and Rhodesia countered by financing an armed rebellion movement to fight the Mozambican government. Sporadic conflict escalated into all-out war. More than 1 million people died in the conflict, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.10</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>EMERGING DEMOCRACIES</title>
                  </title-group>
                  <fpage>47</fpage>
                  <abstract>
                     <p>On November 8, 1989, the people of Namibia quietly started a revolution. For five days, they turned out in droves to line up for hours under the broiling sun to do something that they, their parents, and their grandparents had not been allowed to do: peacefully and openly choose their national leaders. An amazing 98 percent of registered voters turned out to elect 72 delegates to a constituent assembly that would draft a new constitution that would later lead to presidential elections and the formation of a new government.¹ Little did they know that their actions would reverberate far beyond</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.11</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>STRONGER ECONOMIC MANAGEMENT</title>
                  </title-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>By 1983, Ghana’s economy had hit rock bottom. The hope that followed Kwame Nkrumah’s declaration of independence in 1957 had long since evaporated. The 1966 coup d’état that overthrew Nkrumah had touched off 15 years of intense political instability and a series of coups and countercoups that led to eight different heads of state holding power between 1966 and 1981. The combination of sharply rising oil prices, falling cocoa prices, extreme political volatility, and poor economic management in the late 1970s left the country in disarray. As global economic shocks roiled the economy, successive governments responded with heavy intervention, making</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.12</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The END of the DEBT CRISIS and CHANGING RELATIONSHIPS with DONORS</title>
                  </title-group>
                  <fpage>91</fpage>
                  <abstract>
                     <p>On August 12, 1982, Mexican Finance Minister Jesús Silva Herzog shocked the financial world by announcing that Mexico could no longer service its debts. Within weeks, international banks began withdrawing lines of credit and cutting their exposure to developing countries around the world. Suddenly, many developing countries faced an enormous credit squeeze, making it enormously difficult to service older loans. What became known as the 1980s debt crisis was under way.</p>
                     <p>The debt crisis had its roots in the oil price shocks, volatile commodity prices, and deep global recession of the late 1970s and early 1980s. The impacts were worsened</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.13</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>THE TECHNOLOGY REVOLUTION</title>
                  </title-group>
                  <fpage>109</fpage>
                  <abstract>
                     <p>The Song-Taaba Yalgré organization in Burkina Faso had a problem. This vibrant group, made up mostly of women living in remote areas where poverty and illiteracy flourish, had begun to earn some welcome additional income by producing shea butter for export. But their efforts were being undermined by difficulties in getting the information they needed about production needs, deliveries, and prices. Communication was difficult, time-consuming, and costly. How could they better connect their 2,000 rural members with headquarters in the capital of Ouagadougou and exchange accurate information on what, where, and when to sell?</p>
                     <p>The answer came from something most</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>THE COMING OF THE “CHEETAHS”</title>
                  </title-group>
                  <fpage>125</fpage>
                  <abstract>
                     <p>Patrick Awuah knows that Africa’s future lies in the education of a new generation of leaders. Born in Ghana, Awuah went to the United States as a teenager to attend Swarthmore College. When Awuah returned to Ghana five years later, he was dismayed by what he saw: an autocratic government, economic ruin, and complete lack of basic services. This was certainly no place to stay and have a career.¹</p>
                     <p>Awuah returned to the United States, where he landed a position as a program manager at Microsoft and began to cultivate a successful career and start a family. However, with the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.15</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>CHALLENGES and OPPORTUNITIES on the ROAD AHEAD</title>
                  </title-group>
                  <fpage>141</fpage>
                  <abstract>
                     <p>Africa’s emerging countries are on the move. The 300 million people in these 17 countries have put behind them the years of conflict, stagnation, mismanagement, and poor governance. They are moving in a new direction. Economic growth has accelerated, investment is growing, and trade is expanding. Average incomes have increased by 50 percent in just 15 years. Infant mortality rates are down and primary school enrollment is up, especially for girls. Poverty rates have fallen from 59 percent to 48 percent. Democracy, imperfect as it may be, has become the norm rather than the exception. Governance has slowly but steadily</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.16</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>161</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1hfr12q.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>171</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
