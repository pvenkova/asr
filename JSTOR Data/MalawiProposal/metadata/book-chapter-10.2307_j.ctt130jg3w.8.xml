<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt130jg3w</book-id>
      <subj-group>
         <subject content-type="call-number">G155.U6F72 2003</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ecotourism</subject>
         <subj-group>
            <subject content-type="lcsh">Appalachian Mountains Region</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Environmental Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Ecotourism in Appalachia</book-title>
         <subtitle>Marketing the Mountains</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Fritsch</surname>
               <given-names>Al</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Johannsen</surname>
               <given-names>Kristin</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>05</day>
         <month>02</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780813122885</isbn>
      <isbn content-type="epub">9780813159225</isbn>
      <isbn content-type="epub">0813159229</isbn>
      <publisher>
         <publisher-name>The University Press of Kentucky</publisher-name>
         <publisher-loc>Lexington, Kentucky</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2004</copyright-year>
         <copyright-holder>The University Press of Kentucky</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt130jg3w"/>
      <abstract abstract-type="short">
         <p>Tourism is the world's largest industry, and ecotourism is rapidly emerging as its fastest growing segment. As interest in nature travel increases, so does concern for conservation of the environment and the well-being of local peoples and cultures. Appalachia seems an ideal destination for ecotourists, with its rugged mountains, uniquely diverse forests, wild rivers, and lively arts culture. And ecotourism promises much for the region: protecting the environment while bringing income to disadvantaged communities. But can these promises be kept?<italic>Ecotourism in Appalachia</italic>examines both the potential and the threats that tourism holds for Central Appalachia. The authors draw lessons from destinations that have suffered from the "tourist trap syndrome," including Nepal and Hawaii. They conclude that only carefully regulated and locally controlled tourism can play a positive role in Appalachia's economic development.</p>
      </abstract>
      <counts>
         <page-count count="320"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.3</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>To anyone who has flown on a jam-packed airplane or stood in an endless car rental line in recent years, it will come as no surprise that travel is a booming business. The shock is in learning just how big it is.</p>
                     <p>Tourism is now the world’s largest industry, creating $3.6 trillion of economic activity (directly and indirectly) annually, according to the World Travel and Tourism Council, an industry federation.¹ One out of every fifteen workers worldwide is employed in tourism, and tourism is the third largest household expenditure (after housing and food) in most industrialized nations.² Although the precise</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>The World’s Biggest Industry:</title>
                     <subtitle>The Rising Star of Tourism</subtitle>
                  </title-group>
                  <fpage>8</fpage>
                  <abstract>
                     <p>As with most smoothly functioning machines, the tourism industry received very little attention from its users—until it broke down. Following the attacks on the World Trade Center in New York City on September 11, 2001, most travel came to a sudden halt. Within the hour, tens of thousands of people found themselves pacing the floor in places they didn’t want to be, as airports were closed and airplanes were grounded. Even after air transport was operational again, many Americans felt reluctant to travel.</p>
                     <p>The impact of these events quickly rippled through the global economy to its furthest reaches. Airlines</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Mountain Mist:</title>
                     <subtitle>Appalachian Tourism Today</subtitle>
                  </title-group>
                  <fpage>29</fpage>
                  <abstract>
                     <p>In 1908, a wealthy young easterner accompanied his uncle on a trip to Harlan County, Kentucky. While the uncle spent long days researching land titles in the county courthouse, his nephew had plenty of time to write letters home, describing the area in rapturous terms: “The most beautiful country we have seen yet. The sides of the valley going up 2000 feet, heavily wooded with great poplars, chestnuts and a dozen or two other deciduous trees and every mile or so a fertile bottom with fine crops and a stream of splendid water.” He wrote about the “magnificent view” above</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>On the Wrong Track:</title>
                     <subtitle>ORV Tourism in Kentucky</subtitle>
                  </title-group>
                  <fpage>58</fpage>
                  <abstract>
                     <p>Too often, the attitude of both government officials and residents of Appalachia until now has been that tourism, all tourism, is an economic plus, and should be encouraged. As we have seen, Appalachia’s main attraction for visitors has always been nature—its majestic mountains, peaceful forests, and untamed rivers. And in contemplating future development, the focus is largely on these natural features. For example, in 2002, the Southern and Eastern Kentucky Tourism Development Association sponsored an all-day conference on “Nature-Based Tourism Opportunities” for actual and prospective tourism entrepreneurs. Their concept of nature tourism was broad enough to include everything from</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Going Green:</title>
                     <subtitle>Ecotourism as an Emerging Experience</subtitle>
                  </title-group>
                  <fpage>82</fpage>
                  <abstract>
                     <p>Given the difficulty of pinning down a definition of ecotourism, it’s hard to say who the first ecotourists were. Some likely candidates are the Sierra Club members who trooped off on the group’s High Trips starting in 1901. Every year, upwards of a hundred members would spend days trekking together in the Sierra Nevada, getting to know the land that their organization was fighting to preserve. On the first trip, participants (including women in long skirts) hiked twenty miles and climbed four thousand feet—in one day! This was hardly “walking lightly on the Earth,” though—porters, pack mules, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Lessons for Appalachia 1:</title>
                     <subtitle>Ecotourism in Developing Countries</subtitle>
                  </title-group>
                  <fpage>105</fpage>
                  <abstract>
                     <p>Though it seems that everyone in Appalachia has high hopes for the future of tourism, from state governors down to small-town dwellers, the fact remains that the industry is comparatively undeveloped here. The Appalachian states are relatively low on the list of U.S. tourist destinations. In 1998, the entire state of Virginia took in 2.8 percent of total tourist expenditures in the U.S., with only a part of that going to the mountain areas, and the rest to the suburbs of Washington, D.C., Williamsburg, and other major non-mountain areas. Other Appalachian states had much lower percentages of the total, down</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Lessons for Appalachia 2:</title>
                     <subtitle>Nature Tourism in the U.S.</subtitle>
                  </title-group>
                  <fpage>134</fpage>
                  <abstract>
                     <p>Ask Americans about their idea of a dream vacation and many of them have the same response—Hawai’i, the “tropical paradise,” or Alaska, “the last frontier.” In a nation filled with natural wonders and landscapes of great beauty, these two faraway places capture the imagination. The mystique of a lush rainforest climbing the slopes of a fiery volcano, or a glacier tumbling down to the icy sea, is an image that resonates, drawing travelers from great distances, sometimes at great expense.</p>
                     <p>In both Hawai’i and Alaska, nature tourism has been extensively developed over the past decades in very different environments</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.11</book-part-id>
                  <title-group>
                     <title>Photographs</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>The Bottom Line:</title>
                     <subtitle>Ecotourism’s Balance Sheet</subtitle>
                  </title-group>
                  <fpage>162</fpage>
                  <abstract>
                     <p>So far, we’ve looked at the development of ecotourism and considered some of the problems with nature-based tourism in specific locations. It’s time to consider the overall picture, weighing the pluses and minuses of ecotourism in order to assess its potential for Appalachia.</p>
                     <p>The existing literature on ecotourism is overwhelmingly positive. Ecotourism supporters and developers have written the bulk of it, ever since the concept first appeared. It is only in recent years that a few observers have cast a critical eye on the principle and on specific ecotourism projects. Since so much has been written on the benefits and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>2020 Visions:</title>
                     <subtitle>Two Alternative Futures for Appalachian Tourism</subtitle>
                  </title-group>
                  <fpage>186</fpage>
                  <abstract>
                     <p>As we have seen, tourism in Appalachia now stands at a crossroads. Travel in the United States is growing tremendously, and the region is well placed to benefit from the recent trends towards nature tourism, travel as a family, and the preference for destinations closer to home. The issue that faces us is: what kind of tourism do we want to develop? The choices we make now, whether consciously or unconsciously, will have a major impact on the region’s future, and we need to consider in detail what the results will be. What kind of Appalachia will they shape?</p>
                     <p>Let’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Our Own Backyard:</title>
                     <subtitle>An Ecotour Through Appalachia</subtitle>
                  </title-group>
                  <fpage>212</fpage>
                  <abstract>
                     <p>No discussion of ecotourism in Appalachia would be complete without considering how existing attractions can be part of such a vision. Can we bring the area’s ecology to the forefront in museum displays, tours, naturalist talks, and outdoor exhibits? Can this outlook be spread from public facilities to private businesses as well? And can we make ecotourism accessible to the less active half of the tourist population—the senior citizens, the families with small children, the disabled, the tour bus crowd, and those who would unhesitatingly label themselves couch potatoes? In short, can we make the existing Appalachian tourist experience</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>Beyond Ecotourism:</title>
                     <subtitle>Transforming Travel</subtitle>
                  </title-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>Throughout this book, we have seen how the current concept of ecotourism, though in some ways a promising idea, has serious draw-backs and limitations. It is inherently elitist, and because it is viewed as a specialized form of travel, it will only make up a limited segment of the commercial tourism market, with a correspondingly limited impact on the environment. We can’t count on it to save the global ecology, rescue the depressed economy of a particular community, or even transform the environmental consciousness of individual travelers. Instead, we must consciously work towards changing the shape and content of all</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.16</book-part-id>
                  <title-group>
                     <title>APPENDIX 1:</title>
                     <subtitle>Co–op America’s Travel Guidelines</subtitle>
                  </title-group>
                  <fpage>263</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.17</book-part-id>
                  <title-group>
                     <title>APPENDIX 2:</title>
                     <subtitle>99 Recreational Activities Ranked by Impact</subtitle>
                  </title-group>
                  <fpage>265</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.18</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>271</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt130jg3w.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
