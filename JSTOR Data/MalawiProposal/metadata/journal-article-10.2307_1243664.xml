<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">amerjagriecon</journal-id>
         <journal-id journal-id-type="jstor">j100057</journal-id>
         <journal-title-group>
            <journal-title>American Journal of Agricultural Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Agricultural Economics Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">00029092</issn>
         <issn pub-type="epub">14678276</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">1243664</article-id>
         <title-group>
            <article-title>Land Allocation in HYV Adoption Models: An Investigation of Alternative Explanations</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Melinda</given-names>
                  <surname>Smale</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Richard E.</given-names>
                  <surname>Just</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Howard D.</given-names>
                  <surname>Leathers</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">76</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i253148</issue-id>
         <fpage>535</fpage>
         <lpage>546</lpage>
         <page-range>535-546</page-range>
         <permissions>
            <copyright-statement>Copyright 1994 American Agricultural Economics Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1243664"/>
         <abstract>
            <p>Microeconomic theory provides four competing explanations for partial land allocation to new and traditional seed varieties in HYV adoption decisions: input fixity, portfolio selection, safety-first behavior, and learning. Testing a general model that contains each as a special case suggests that they are jointly most likely to explain land allocation in the HYV adoption decisions of Malawian smallholders. Yet when each explanation is tested to the exclusion of the others (as is usually the case in the literature), competing hypotheses are individually significant. Results suggest that employing approaches based on single explanations may lead to inappropriately narrow conclusions.</p>
         </abstract>
         <kwd-group>
            <kwd>HYV adoption</kwd>
            <kwd>land allocation</kwd>
            <kwd>seed-fertilizer technology</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d911e156a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d911e163" publication-type="journal">
Hiebert  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d911e171" publication-type="journal">
Tsur, Sternberg, and Hochman  </mixed-citation>
            </p>
         </fn>
         <fn id="d911e180a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d911e187" publication-type="book">
Leathers 1990  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d911e195" publication-type="book">
Roumasset et al.  </mixed-citation>
            </p>
         </fn>
         <fn id="d911e204a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d911e211" publication-type="journal">
Fafchamps  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d911e229a1310">
            <mixed-citation id="d911e233" publication-type="journal">
Amemiya, T. "The Estimation of a Simultaneous-
Equation Tobit Model." Int. Econ. Rev.
20(February 1979):169-81.<person-group>
                  <string-name>
                     <surname>Amemiya</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>169</fpage>
               <volume>20</volume>
               <source>Int. Econ. Rev.</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d911e271a1310">
            <mixed-citation id="d911e275" publication-type="journal">
Bell, C. "The Acquisition of Agricultural Technol-
ogy: Its Determinants and Effects." J. Develop.
Stud. 9(October 1972):123-59.<person-group>
                  <string-name>
                     <surname>Bell</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>123</fpage>
               <volume>9</volume>
               <source>J. Develop. Stud.</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d911e313a1310">
            <mixed-citation id="d911e317" publication-type="book">
Blaug, M. The Methodology of Economics or How
Economists Explain. Cambridge: Cambridge
University Press, 1980.<person-group>
                  <string-name>
                     <surname>Blaug</surname>
                  </string-name>
               </person-group>
               <source>The Methodology of Economics or How Economists Explain</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d911e346a1310">
            <mixed-citation id="d911e350" publication-type="journal">
Carter, M.R., and K.D. Wiebe. "Access to Capital
and Its Impact on Agrarian Structure and Pro-
ductivity in Kenya." Amer. J. Agr. Econ.
72(December 1990):1146-50.<person-group>
                  <string-name>
                     <surname>Carter</surname>
                  </string-name>
               </person-group>
               <issue>December</issue>
               <fpage>1146</fpage>
               <volume>72</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d911e392a1310">
            <mixed-citation id="d911e396" publication-type="journal">
Fafchamps, M. "Cash Crop Production, Food Price
Volatility, and Rural Market Integration in the
Third World." Amer. J. Agr. Econ. 74(February
1992):90-99.<person-group>
                  <string-name>
                     <surname>Fafchamps</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>90</fpage>
               <volume>74</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d911e437a1310">
            <mixed-citation id="d911e441" publication-type="journal">
Feder, G. "Farm Size, Risk Aversion and the Adop-
tion of New Technology Under Uncertainty."
Oxford Econ. Pap. 32(July 1980):263-83.<person-group>
                  <string-name>
                     <surname>Feder</surname>
                  </string-name>
               </person-group>
               <issue>July</issue>
               <fpage>263</fpage>
               <volume>32</volume>
               <source>Oxford Econ. Pap.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d911e479a1310">
            <mixed-citation id="d911e485" publication-type="journal">
Feder, G., and G. O'Mara. "Farm Size and the Diffu-
sion of Green Revolution Technology." Econ.
Develop. and Cultur. Change 30(October
1981):59-76.<person-group>
                  <string-name>
                     <surname>Feder</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>59</fpage>
               <volume>30</volume>
               <source>Econ. Develop. and Cultur. Change</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d911e526a1310">
            <mixed-citation id="d911e530" publication-type="journal">
Feder, G., and R. Slade. "The Acquisition of Infor-
mation and the Adoption of New Technology."
Amer. J. Agr. Econ. 66(August 1984):312-20.<person-group>
                  <string-name>
                     <surname>Feder</surname>
                  </string-name>
               </person-group>
               <issue>August</issue>
               <fpage>312</fpage>
               <volume>66</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d911e568a1310">
            <mixed-citation id="d911e572" publication-type="journal">
Hammer, J.S. "Subsistence First: Farm Allocation
Decisions in Senegal." J. Develop. Econ.
23(October 1986):357-69.<person-group>
                  <string-name>
                     <surname>Hammer</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>357</fpage>
               <volume>23</volume>
               <source>J. Develop. Econ.</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d911e610a1310">
            <mixed-citation id="d911e614" publication-type="book">
Heckman, J.J. "Statistical Models for Discrete Panel
Data." C.F. Manski and D. McFadden, eds.
Structural Analysis of Discrete Data with
Econometric Applications. Cambridge: MIT
Press, 1981.<person-group>
                  <string-name>
                     <surname>Heckman</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Statistical Models for Discrete Panel Data</comment>
               <source>Structural Analysis of Discrete Data with Econometric Applications</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d911e653a1310">
            <mixed-citation id="d911e657" publication-type="journal">
---. "Sample Selection Bias as a Specification Er-
ror" Econometrica 47(January 1979): 153-61.<person-group>
                  <string-name>
                     <surname>Heckman</surname>
                  </string-name>
               </person-group>
               <issue>January</issue>
               <fpage>153</fpage>
               <volume>47</volume>
               <source>Econometrica</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d911e692a1310">
            <mixed-citation id="d911e696" publication-type="journal">
Hiebert, D. "Risk, Learning and the Adoption of
Fertilizer Responsive Varieties." Amer. J. Agr.
Econ. 56(November 1974):764-68.<person-group>
                  <string-name>
                     <surname>Hiebert</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>764</fpage>
               <volume>56</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d911e734a1310">
            <mixed-citation id="d911e738" publication-type="journal">
Huffman, W.E. "Allocative Efficiency: The Role of
Human Capital." Quart. J. Econ. 91(February
1977):59-79.<person-group>
                  <string-name>
                     <surname>Huffman</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>59</fpage>
               <volume>91</volume>
               <source>Quart. J. Econ.</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d911e776a1310">
            <mixed-citation id="d911e780" publication-type="journal">
Just, R.E., and D. Zilberman. "Stochastic Structure,
Farm Size and Technology Adoption in Devel-
oping Agriculture." Oxford Econ. Pap.
35(August 1983):307-28.<person-group>
                  <string-name>
                     <surname>Just</surname>
                  </string-name>
               </person-group>
               <issue>August</issue>
               <fpage>307</fpage>
               <volume>35</volume>
               <source>Oxford Econ. Pap.</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d911e821a1310">
            <mixed-citation id="d911e825" publication-type="journal">
Kislev Y., and N. Shchori-Bachrach. "The Process of
an Innovation Cycle." Amer. J. Agr. Econ.
55(February 1973): 28-37.<person-group>
                  <string-name>
                     <surname>Kislev</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>28</fpage>
               <volume>55</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d911e863a1310">
            <mixed-citation id="d911e867" publication-type="book">
Kunreuther, H., and G. Wright. "Safety-First, Gam-
bling and the Subsistence Farmer." Risk,
Uncertainty and Agricultural Development. J.A.
Roumasset, J.M. Boussard, and I. Singh, eds.
New York and Laguna, Phillippines: Agricul-
tural Development Council (ADC) and
Southeast Asian Regional Center for Graduate
Study and Research in Agriculture (SEARCA),
1979.<person-group>
                  <string-name>
                     <surname>Kunreuther</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Safety-First, Gambling and the Subsistence Farmer</comment>
               <source>Risk, Uncertainty and Agricultural Development</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d911e919a1310">
            <mixed-citation id="d911e923" publication-type="journal">
Leathers, H.D. "Allocable Fixed Inputs as a Cause
of Joint Production: A Cost Function Ap-
proach." Amer. J. Agr. Econ. 73(November
1991):1083-90.<person-group>
                  <string-name>
                     <surname>Leathers</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>1083</fpage>
               <volume>73</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d911e964a1310">
            <mixed-citation id="d911e968" publication-type="book">
---. "The Concept of Safety in Economics." Depart-
ment of Agricultural and Resource Economics
Working Paper. College Park: University of
Maryland, 1990.<person-group>
                  <string-name>
                     <surname>Leathers</surname>
                  </string-name>
               </person-group>
               <source>The Concept of Safety in Economics</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1000a1310">
            <mixed-citation id="d911e1004" publication-type="journal">
Leathers, H.D., and M. Smale. "A Bayesian Ap-
proach to Explaining Sequential Adoption of
Components of a Technological Package."
Amer. J. Agr. Econ. 73(August 1991):734-42.<person-group>
                  <string-name>
                     <surname>Leathers</surname>
                  </string-name>
               </person-group>
               <issue>August</issue>
               <fpage>734</fpage>
               <volume>73</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1045a1310">
            <mixed-citation id="d911e1049" publication-type="book">
Lee, L.F. "Simultaneous Equations Models with Dis-
crete and Censored Dependent Variables."
Structural Analysis of Discrete Data with
Econometric Applications. C. Manski and D.
McFadden, eds. Cambridge: MIT Press, 1981.<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Simultaneous Equations Models with Discrete and Censored Dependent Variables</comment>
               <source>Structural Analysis of Discrete Data with Econometric Applications</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1087a1310">
            <mixed-citation id="d911e1093" publication-type="journal">
Lin, J.Y. "Education and Innovation Adoption in Ag-
riculture: Evidence from Hybrid Rice in China,"
Amer. J. Agr. Econ. 73(August 1991):713-23.<person-group>
                  <string-name>
                     <surname>Lin</surname>
                  </string-name>
               </person-group>
               <issue>August</issue>
               <fpage>713</fpage>
               <volume>73</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1131a1310">
            <mixed-citation id="d911e1135" publication-type="book">
Lindner, R.K. "Farm Size and the Time Lag to
Adoption of a Scale Neutral Innovation."
Mimeo. Adelaide, Australia: University of
Adelaide, 1980.<person-group>
                  <string-name>
                     <surname>Lindner</surname>
                  </string-name>
               </person-group>
               <source>Farm Size and the Time Lag to Adoption of a Scale Neutral Innovation</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1168a1310">
            <mixed-citation id="d911e1172" publication-type="journal">
Lindner, R.K., A.J. Fischer, and P. Pardey. "The
Time to Adoption." Econ. Letters 2(February
1979):187-90.<person-group>
                  <string-name>
                     <surname>Lindner</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>187</fpage>
               <volume>2</volume>
               <source>Econ. Letters</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1210a1310">
            <mixed-citation id="d911e1214" publication-type="journal">
Maddala, G.S., and L. Lee. "Recursive Models with
Qualitative Endogenous Variables." Ann. Econ.
and Soc. Measure. 5(Fall 1976):525-45.<person-group>
                  <string-name>
                     <surname>Maddala</surname>
                  </string-name>
               </person-group>
               <issue>Fall</issue>
               <fpage>525</fpage>
               <volume>5</volume>
               <source>Ann. Econ. and Soc. Measure</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1252a1310">
            <mixed-citation id="d911e1256" publication-type="journal">
Moscardi, E., and A. de Janvry. "Attitudes Toward
Risk among Peasants: An Econometric Ap-
proach." Amer. J. Agr. Econ. 59(November
1977):710-16.<person-group>
                  <string-name>
                     <surname>Moscardi</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>710</fpage>
               <volume>59</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1297a1310">
            <mixed-citation id="d911e1301" publication-type="book">
McGuirk, A.M., and Y. Mundlak. Incentives and
Constraints in the Transformation of Punjab
Agriculture. IFPRI Research Report 87. Wash-
ington, DC: International Food Policy Research
Institute (IFPRI), 1991.<person-group>
                  <string-name>
                     <surname>McGuirk</surname>
                  </string-name>
               </person-group>
               <source>Incentives and Constraints in the Transformation of Punjab Agriculture</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1336a1310">
            <mixed-citation id="d911e1340" publication-type="journal">
Nelson, F.D., and L. Olsen. "Specification and Esti-
mation of a Simultaneous Equation Model with
Limited Dependent Variables." Int. Econ. Rev.
3(October 1978):695-710.<person-group>
                  <string-name>
                     <surname>Nelson</surname>
                  </string-name>
               </person-group>
               <issue>October</issue>
               <fpage>695</fpage>
               <volume>3</volume>
               <source>Int. Econ. Rev.</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1381a1310">
            <mixed-citation id="d911e1387" publication-type="book">
O'Mara, G. "The Microeconomics of Technique
Adoption by Smallholding Mexican Farmers."
The Book of Chac: Programming Studies for
Mexican Agriculture. Roger D. Norton and
Leopoldo Solis, eds. Baltimore: Johns Hopkins
University Press and the World Bank, 1983.<person-group>
                  <string-name>
                     <surname>O'Mara</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Microeconomics of Technique Adoption by Smallholding Mexican Farmers</comment>
               <source>The Book of Chac: Programming Studies for Mexican Agriculture</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1429a1310">
            <mixed-citation id="d911e1433" publication-type="journal">
Pingali, P., and G.A. Carlson. "Human Capital, Ad-
justments in Subjective Probabilities, and the
Demand for Pest Controls." Amer. J. Agr. Econ.
67(November 1985):853-61.<person-group>
                  <string-name>
                     <surname>Pingali</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>853</fpage>
               <volume>67</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1474a1310">
            <mixed-citation id="d911e1478" publication-type="book">
Roumasset, J.A., J.M. Boussard, and I. Singh, eds.
Risk, Uncertainty and Agricultural Develop-
ment. New York and Laguna, Phillippines:
Agricultural Development Council (ADC) and
Southeast Asian Regional Center for Graduate
Study and Research in Agriculture (SEARCA),
1979.<person-group>
                  <string-name>
                     <surname>Roumasset</surname>
                  </string-name>
               </person-group>
               <source>Risk, Uncertainty and Agricultural Development</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1519a1310">
            <mixed-citation id="d911e1523" publication-type="journal">
Shumway, C.R., R.D. Pope, and E.K. Nash. "Allo-
catable Fixed Inputs and Jointness in
Agricultural Production: Implications for Eco-
nomic Modeling." Amer. J. Agr. Econ.
66(February 1984):72-78.<person-group>
                  <string-name>
                     <surname>Shumway</surname>
                  </string-name>
               </person-group>
               <issue>February</issue>
               <fpage>72</fpage>
               <volume>66</volume>
               <source>Amer. J. Agr. Econ.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d911e1567a1310">
            <mixed-citation id="d911e1571" publication-type="book">
Smale, M., with Z.H.W. Kaunda, H.L. Makina,
M.M.M.K. Mkandawire, M.N.S. Msowoya,
D.J.E.K. Mwale, and P.W. Heisey. "Chimanga
Cha Makolo, Hybrids and Composites: An
Analysis of Farmers' Adoption of Maize Tech-
nology in Malawi, 1989-1991." CIMMYT
Economics Working Paper 91/04. Mexico:
CIMMYT.<person-group>
                  <string-name>
                     <surname>Smale</surname>
                  </string-name>
               </person-group>
               <source>Chimanga Cha Makolo, Hybrids and Composites: An Analysis of Farmers' Adoption of Maize Technology in Malawi, 1989-1991</source>
            </mixed-citation>
         </ref>
         <ref id="d911e1612a1310">
            <mixed-citation id="d911e1616" publication-type="journal">
Tsur, Y., M. Sternberg, and E. Hochman. "Dynamic
Modelling of Innovation Process Adoption and
Learning." Oxford Econ. Pap. 42(April
1990):336-55.<person-group>
                  <string-name>
                     <surname>Tsur</surname>
                  </string-name>
               </person-group>
               <issue>April</issue>
               <fpage>336</fpage>
               <volume>42</volume>
               <source>Oxford Econ. Pap.</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
