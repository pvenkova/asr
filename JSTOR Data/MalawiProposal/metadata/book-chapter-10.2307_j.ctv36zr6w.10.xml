<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctv36zr6w</book-id>
      <subj-group>
         <subject content-type="call-number">BF1543.F73 2006</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Demonology</subject>
         <subj-group>
            <subject content-type="lcsh">Public opinion</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Conspiracies</subject>
         <subj-group>
            <subject content-type="lcsh">Public opinion</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Ritual abuse</subject>
         <subj-group>
            <subject content-type="lcsh">Public opinion</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Good and evil</subject>
         <subj-group>
            <subject content-type="lcsh">Public opinion</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Religion</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Evil Incarnate</book-title>
         <subtitle>Rumors of Demonic Conspiracy and Satanic Abuse in History</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>FRANKFURTER</surname>
               <given-names>DAVID</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>05</day>
         <month>06</month>
         <year>2018</year>
      </pub-date>
      <isbn content-type="epub">9780691186979</isbn>
      <isbn content-type="epub">0691186979</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>Princeton; Oxford</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2006</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctv36zr6w"/>
      <abstract abstract-type="short">
         <p>In the 1980s, America was gripped by widespread panics about Satanic cults. Conspiracy theories abounded about groups who were allegedly abusing children in day-care centers, impregnating girls for infant sacrifice, brainwashing adults, and even controlling the highest levels of government. As historian of religions David Frankfurter listened to these sinister theories, it occurred to him how strikingly similar they were to those that swept parts of the early Christian world, early modern Europe, and postcolonial Africa. He began to investigate the social and psychological patterns that give rise to these myths. Thus was born<italic>Evil Incarnate</italic>, a riveting analysis of the mythology of evilconspiracy.</p>
         <p>The first work to provide an in-depth analysis of the topic, the book uses anthropology, the history of religion, sociology, and psychoanalytic theory, to answer the questions "What causes people collectively to envision evil and seek to exterminate it?" and "Why does the representation of evil recur in such typical patterns?"</p>
         <p>Frankfurter guides the reader through such diverse subjects as witch-hunting, the origins of demonology, cannibalism, and the rumors of Jewish ritual murder, demonstrating how societies have long expanded upon their fears of such atrocities to address a collective anxiety. Thus, he maintains, panics over modern-day infant sacrifice are really not so different from rumors about early Christians engaging in infant feasts during the second and third centuries in Rome.</p>
         <p>
            <italic>In Evil Incarnate</italic>, Frankfurter deepens historical awareness that stories of Satanic atrocities are both inventions of the mind and perennial phenomena, not authentic criminal events. True evil, as he so artfully demonstrates, is not something organized and corrupting, but rather a social construction that inspires people to brutal acts in the name of moral order.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Basque villagers of the sixteenth century had always known that some people could be malevolent, dangerous; and they had long speculated on how such people perverted human custom and brought catastrophe down on their neighbors, how they got their powers to strike ill, what made them<italic>different</italic>. But as rumors filtered up the mountain valleys that French judges to the north were discovering organized<italic>groups</italic>of witches, and then when, around 1610, Franciscan friars arrived with the first, horrific details of a witches’<italic>aquellare</italic>, or Sabbat, with its orgies, feasts on infant flesh, desecration of Christian sacraments, and obscene contracts</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>An Architecture for Chaos</title>
                     <subtitle>THE NATURE AND FUNCTION OF DEMONOLOGY</subtitle>
                  </title-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>An eminent historian of religions once posed the question, “Why is it that the demonic, associated with the marginal, the liminal, the chaotic, the protean, the unstructured appears cross-culturally as so rigidly organized a realm?” 1 It is an intriguing paradox: the precise hierarchies that religious institutions construct, with their ranks and titles and weaponry, that consist of halfanimal, perverse beings bent on wreaking havoc in the world. And yet in the local landscapes where people really tangle with demons, in which demons are believed to pose concrete threats to health and social welfare, there is actually little to no</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Experts in the Identification of Evil</title>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>Chapter 2 exposed the difference between local understandings of ambiguously dangerous forces and the<italic>demonologies</italic>—systems—developed by religious experts: priests, exorcists, scribes, ritual specialists. Demonologies can be heady things, but we must understand them first as the innovations of certain self-defined experts, and also as the weapons of institutions. In this chapter we look at the expert in the discernment of evil, sometimes in cooperation with an institution, as a central force in transforming those unsystematic local understandings of capricious spirits and malevolent neighbors into an elaborate and coordinated assault on all aspects of life by a conspiracy of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Rites of Evil</title>
                     <subtitle>CONSTRUCTIONS OF MALEFICENT RELIGION AND RITUAL</subtitle>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>In the last chapter we saw the indispensability of the expert in evil for assembling and laying out evil conspiracies and for demonstrating the tangibility of evil in the immediate world of audiences. In early modern Europe, as in modern Africa, myths of evil conspiracy have to a great extent come down to the activities of such experts.</p>
                     <p>In this chapter we shift from the social world of experts and their audiences to the stories they convey: tableaux of the most monstrous behavior, condensations of absolute evil. To some degree these evils will reflect discrete cultural values; and yet much</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Imputations of Perversion</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>During the height of the recovered-memory disputes in the early 1990s, in which SRA often figured as an example of the horrors therapists could recover, SRA therapists often posed the question, how could people conceive of such perverse atrocities<italic>without</italic>their being true? How could people make up such horrors? Infanticide, cannibalism, torture, animal and human sacrifice, orgy, incest, bizarre sexual acts, pseudosacramental use of feces, urine, blood, and corpses—it all seemed quite beyond what these middle-class, married, suburban women or their therapists could concoct on their own.¹</p>
                     <p>Other rumors of perverse cults in history have likewise inspired scholars</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>The Performance of Evil</title>
                  </title-group>
                  <fpage>168</fpage>
                  <abstract>
                     <p>The images and myths of evil on which I have been dwelling are not the types people keep at a distance. Like the demons that permeate the landscape in chapter 2, evil forces and conspiracies emerge in the world of real men and women. Demonic forces become visible in the people who writhe and bellow in possession, as well as in the exorcist who seems to perceive exactly which creature he is commanding. Witches emerge in the individuals who finally come to confess, describing lurid Sabbats and horrifying conspiracies that they had (it seems) personally witnessed, and it is the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Mobilizing against Evil</title>
                  </title-group>
                  <fpage>208</fpage>
                  <abstract>
                     <p>The components of a myth of evil conspiracy lie deep in culture—“hard-wired,” as it were, to society and self, rather than produced independently at every discrete point in history. However, the<italic>activation</italic>of those components, to mobilize people to purge evil from their midst, is a process embedded in social and historical context. It is here that I want to discuss the interaction of these latent components and the contexts in which they arise and provoke.</p>
                     <p>Let us consider first the “depth” of those components that articulate evil in terms of inversion and perversion and yet allow a transgressive</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.13</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>225</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.14</book-part-id>
                  <title-group>
                     <title>Select Bibliography</title>
                  </title-group>
                  <fpage>259</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zr6w.15</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>281</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
