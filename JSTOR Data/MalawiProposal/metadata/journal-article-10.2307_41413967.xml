<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">ecosystems</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100928</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Ecosystems</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer Science+Business Media</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14329840</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14350629</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41413967</article-id>
         <title-group>
            <article-title>Impacts of Eutrophication on Carbon Burial in Freshwater Lakes in an Intensively Agricultural Landscape</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Adam J.</given-names>
                  <surname>Heathcote</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John A.</given-names>
                  <surname>Downing</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40068831</issue-id>
         <fpage>60</fpage>
         <lpage>70</lpage>
         <permissions>
            <copyright-statement>© 2012 Springer Science+Business Media, LLC</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41413967"/>
         <abstract>
            <p>The influence of inland water bodies on the global carbon cycle and the great potential for long-term carbon burial in them is an important component of global limnology. We used paleolimnological methods to estimate changes in carbon burial rates through time in a suite of natural lakes in the US state of Iowa which has watersheds that have been heavily modified over the last 150 years. Our results show increasing carbon burial for all lakes in our study as agriculture intensified. Our estimates of carbon burial rates, before land clearance, are similar to the published worldwide averages for nutrient-poor lakes. In nearly all the cases, burial rates increased to very high levels (up to 200 g C m⁻²y⁻¹) following agricultural development. These results support the idea that the increased autochthonous and allochthonous carbon flux, related to anthropogenic change, leads to higher rates of carbon burial. Further, these results imply that the fraction of global carbon buried by lakes will be increasingly important in the future if worldwide trends in anthropogenic eutrophication continue.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d701e229a1310">
            <mixed-citation id="d701e233" publication-type="other">
Anderson PF. 1997. GIS research to digitize maps of Iowa 1832-
1859 vegetation from General Land Office township plat
maps. Des Moines, IA: Iowa Department of Natural Resources.</mixed-citation>
         </ref>
         <ref id="d701e246a1310">
            <mixed-citation id="d701e250" publication-type="other">
Anderson NJ, D'Andrea W, Fritz SC. 2009. Holocene carbon
burial by lakes in SW Greenland. Glob Change Biol 1 5:2590-8.</mixed-citation>
         </ref>
         <ref id="d701e260a1310">
            <mixed-citation id="d701e264" publication-type="other">
Appleby PG, Oldfield F. 1978. The calculation of lead-210 dates
assuming a constant rate of supply of unsupported 210Pb to the
sediment. Catena 5:1-8.</mixed-citation>
         </ref>
         <ref id="d701e277a1310">
            <mixed-citation id="d701e281" publication-type="other">
Arbuckle KE, Downing JA. 2001. The influence of watershed
land use on lake N:P in a predominantly agricultural land-
scape. Limnol Oceanogr 46:970-5.</mixed-citation>
         </ref>
         <ref id="d701e295a1310">
            <mixed-citation id="d701e299" publication-type="other">
Auclair AN. 1976. Ecological factors in development of inten-
sive-management ecosystems in Midwestern United-States.
Ecology 57:431-44.</mixed-citation>
         </ref>
         <ref id="d701e312a1310">
            <mixed-citation id="d701e316" publication-type="other">
Bachmann RW, Jones JR. 1974. Water quality in the Iowa Great
Lakes: a report to the Iowa Great Lakes Water Quality Control
Plan. Ames, IA: Iowa Agricultural and Home Economics
Experiment Station Project.</mixed-citation>
         </ref>
         <ref id="d701e332a1310">
            <mixed-citation id="d701e336" publication-type="other">
Balmer MB, Downing JA. 2011. Carbon dioxide concentrations
in eutrophic lakes: undersaturation implies atmospheric up-
take. Inland Waters 1:125-32.</mixed-citation>
         </ref>
         <ref id="d701e349a1310">
            <mixed-citation id="d701e353" publication-type="other">
Battin TJ, Kaplan LA, Findlay S, Hopkinson CS, Marti E, Pack-
man AI, Newbold JD, Sabater F. 2008. Biophysical controls on
organic carbon fluxes in fluvial networks. Nat Geosci 1:95-
100.</mixed-citation>
         </ref>
         <ref id="d701e369a1310">
            <mixed-citation id="d701e373" publication-type="other">
Bennett EM, Carpenter SR, Caraco NF. 2001. Human impact on
erodable phosphorus and eutrophication: a global perspective.
Bioscience 51:227-34.</mixed-citation>
         </ref>
         <ref id="d701e386a1310">
            <mixed-citation id="d701e390" publication-type="other">
Carpenter SR, Caraco NF, Correli DL, Howarth RW, Sharpley
AN, Smith VH. 1998. Nonpoint pollution of surface waters
with phosphorus and nitrogen. Ecol Appi 8:559-68.</mixed-citation>
         </ref>
         <ref id="d701e404a1310">
            <mixed-citation id="d701e408" publication-type="other">
Cleveland WS, Grosse E, Shyu WM. 1992. Local regression
models. In: Chambers JM, Hastie TJ, Eds. Statistical models in
S. Boca Raton, FL: Chapman &amp; Hall, p 309-76.</mixed-citation>
         </ref>
         <ref id="d701e421a1310">
            <mixed-citation id="d701e425" publication-type="other">
Cole JJ, Prairie YT, Caraco NF, McDowell WH, Tranvik LJ, Striegl
RG, Duarte CM, Kortelainen P, Downing JA, Middelburg J,
Melack JM. 2007. Plumbing the global carbon cycle: inte-
grating inland waters into the terrestrial carbon cycle. Eco-
systems 10:171-84.</mixed-citation>
         </ref>
         <ref id="d701e444a1310">
            <mixed-citation id="d701e448" publication-type="other">
Cushing EJ, Wright HE Jr. 1965. Hand-operated piston corers for
lake sediments. Ecology 46:380-4.</mixed-citation>
         </ref>
         <ref id="d701e458a1310">
            <mixed-citation id="d701e462" publication-type="other">
Davis MB. 1976. Erosion rates and land-use history in southern
Michigan. Environ Conserv 3:139-48.</mixed-citation>
         </ref>
         <ref id="d701e472a1310">
            <mixed-citation id="d701e476" publication-type="other">
Dean WE. 1974. Determination of carbonate and organic-matter
in calcareous sediments and sedimentary-rocks by loss on
ignition -comparison with other methods. J Sediment Petrol
44:242-8.</mixed-citation>
         </ref>
         <ref id="d701e492a1310">
            <mixed-citation id="d701e496" publication-type="other">
Dean WE, Gorham E. 1998. Magnitude and significance of car-
bon burial in lakes, reservoirs, and peatlands. Geology 26:535.</mixed-citation>
         </ref>
         <ref id="d701e507a1310">
            <mixed-citation id="d701e511" publication-type="other">
Dearing JA, Flower RJ. 1982. The magnetic-susceptibility of sed-
imenting material trapped in Lough-Neagh, Northern-Ireland,
and its erosional significance. Limnol Oceanogr 27:969-75.</mixed-citation>
         </ref>
         <ref id="d701e524a1310">
            <mixed-citation id="d701e528" publication-type="other">
Dearing JA, Jones RT. 2003. Coupling temporal and spatial
dimensions of global sediment flux through lake and marine
sediment records. Glob Planet Change 39:147-68.</mixed-citation>
         </ref>
         <ref id="d701e541a1310">
            <mixed-citation id="d701e545" publication-type="other">
Del Giorgio PA, Cole JJ, Cimbleris A. 1997. Respiration rates in
bacteria exceed phytoplankton production in unproductive
aquatic systems. Nature 385:148-51.</mixed-citation>
         </ref>
         <ref id="d701e558a1310">
            <mixed-citation id="d701e562" publication-type="other">
Del Giorgio PA, Cole JJ, Caraco NF, Peters RH. 1999. Linking
planktonic biomass and metabolism to net gas fluxes in
northern temperate lakes. Ecology 80:1422-31.</mixed-citation>
         </ref>
         <ref id="d701e575a1310">
            <mixed-citation id="d701e579" publication-type="other">
Downing JA. 2003. Looking into Earth's eye: a watershed view
of clear lakes. Des Moines, IA: Iowa Natural Heritage Foun-
dation. pp 8-11.</mixed-citation>
         </ref>
         <ref id="d701e592a1310">
            <mixed-citation id="d701e596" publication-type="other">
Downing JA, Cole JJ, Middelburg JJ, Striegl RG, Duarte CM,
Kortelainen P, Prairie YT, Laube KA. 2008. Sediment organic
carbon burial in agriculturally eutrophic impoundments over
the last century. Glob Biogeochem Cycles 22:GB1018.</mixed-citation>
         </ref>
         <ref id="d701e613a1310">
            <mixed-citation id="d701e617" publication-type="other">
Duarte С, Prairie Y. 2005. Prevalence of heterotrophy and
atmospheric C02 emissions from aquatic ecosystems. Ecosys-
tems 8:862-70.</mixed-citation>
         </ref>
         <ref id="d701e630a1310">
            <mixed-citation id="d701e634" publication-type="other">
Engstrom DR, Swain EB. 1986. The chemistry of lake sediments
in time and space. Hydrobiologia 143:37-44.</mixed-citation>
         </ref>
         <ref id="d701e644a1310">
            <mixed-citation id="d701e648" publication-type="other">
ESRI. 2008. ArcMap 9.3. Redlands, CA: Environmental Research
Systems Institute.</mixed-citation>
         </ref>
         <ref id="d701e658a1310">
            <mixed-citation id="d701e662" publication-type="other">
Fuller CC, Van Geen A, Baskaran M, Anima R. 1999. Sediment
chronology in San Francisco Bay, California, defined by 210Pb,
234Th, 137Cs, and 239' 240Pu. Marine Chem 64:7-27.</mixed-citation>
         </ref>
         <ref id="d701e675a1310">
            <mixed-citation id="d701e679" publication-type="other">
Jobbágy EG, Jackson RB. 2000. The vertical distribution of soil
organic carbon and its relation to climate and vegetation. Ecol
Appi 10:423-36.</mixed-citation>
         </ref>
         <ref id="d701e692a1310">
            <mixed-citation id="d701e696" publication-type="other">
Johnson TC, Brown ET, Shi JM. 2010. Biogenic silica deposition
in Lake Malawi, East Africa over the past 150,000 years.
Palaeogeogr Palaeoclimatol Palaeoecol 303:103-9.</mixed-citation>
         </ref>
         <ref id="d701e710a1310">
            <mixed-citation id="d701e714" publication-type="other">
Jones R, Benson Evans K, Chambers FM. 1985. Human influ-
ence upon sedimentation in Llangorse Lake, Wales. Earth Surf
Proces Landf 10:227-35.</mixed-citation>
         </ref>
         <ref id="d701e727a1310">
            <mixed-citation id="d701e731" publication-type="other">
Lazzarino JK, Bachmann RW, Hoyer MV, Canfield DE Jr. 2009.
Carbon dioxide supersaturation in Florida lakes. Hydrobiolo-
gia 627:169-80.</mixed-citation>
         </ref>
         <ref id="d701e744a1310">
            <mixed-citation id="d701e748" publication-type="other">
Lehman JT. 1975. Reconstructing rate of accumulation of lake
sediment -effect of sediment focusing. Quat Res 5:541-50.</mixed-citation>
         </ref>
         <ref id="d701e758a1310">
            <mixed-citation id="d701e762" publication-type="other">
Lyle M, Mitchell N, Pisias N, Mix A, Martinez JI, Paytan A. 2005.
Do geochemical estimates of sediment focusing pass the sed-
iment test in the equatorial Pacific? Paleoceanography
20:PA1005.</mixed-citation>
         </ref>
         <ref id="d701e778a1310">
            <mixed-citation id="d701e782" publication-type="other">
Mulholland PJ, Elwood JW. 1982. The role of lake and reservoir
sediments as sinks in the perturbed global carbon-cycle. Tellus
34:490-9.</mixed-citation>
         </ref>
         <ref id="d701e795a1310">
            <mixed-citation id="d701e799" publication-type="other">
Mutel CF. 2008. The emerald horizon: the history of nature in
Iowa. Iowa City, IA: University of Iowa Press.</mixed-citation>
         </ref>
         <ref id="d701e810a1310">
            <mixed-citation id="d701e814" publication-type="other">
Ragueneau O, Leynaert A, Tréguer P, DeMaster DJ, Anderson
RF. 1996. Opal studied as a marker of paleoproductivity. EOS
Trans 77:491-491.</mixed-citation>
         </ref>
         <ref id="d701e827a1310">
            <mixed-citation id="d701e831" publication-type="other">
Rippey B, Anderson NJ, Renberg I, Korsman T. 2008. The
accuracy of methods used to estimate the whole-lake accu-
mulation rate of organic carbon, major cations, phosphorus
and heavy metals in sediment. J Paleolimnol 39:83-99.</mixed-citation>
         </ref>
         <ref id="d701e847a1310">
            <mixed-citation id="d701e851" publication-type="other">
Risser J. 1981. A renewed threat of soil-erosion -its worse than
the dust bowl. Smithsonian 11:120-31.</mixed-citation>
         </ref>
         <ref id="d701e861a1310">
            <mixed-citation id="d701e865" publication-type="other">
Rowan DJ, Cornett RJ, King K, Risto В. 1995. Sediment focusing
and Pb-210 dating -a new approach. J Paleolimnol 13:107-
18.</mixed-citation>
         </ref>
         <ref id="d701e878a1310">
            <mixed-citation id="d701e882" publication-type="other">
Schelske CL, Stoermer EF, Conley DJ, Robbins JA, Glover RM.
1983. Early eutrophication in the Lower Great-Lakes -new
evidence from biogenic silica in sediments. Science 222:320-2.</mixed-citation>
         </ref>
         <ref id="d701e895a1310">
            <mixed-citation id="d701e899" publication-type="other">
Sobek S, Durisch-Kaiser E, Zurbrugg R, Wongfun N, Wessels M,
Pasche N, Wehrli В. 2009. Organic carbon burial efficiency in
lake sediments controlled by oxygen exposure time and sed-
iment source. Limnol Oceanogr 54:2243-54.</mixed-citation>
         </ref>
         <ref id="d701e916a1310">
            <mixed-citation id="d701e920" publication-type="other">
Stumm W, Morgan JJ. 1996. Aquatic chemistry: chemical
equilibria and rates in natural waters. New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d701e930a1310">
            <mixed-citation id="d701e934" publication-type="other">
Thompson R, Battarbee RW, Osullivan PE, Oldfield F. 1975.
Magnetic-susceptibility of lake sediments. Limnol Oceanogr
20:687-98.</mixed-citation>
         </ref>
         <ref id="d701e947a1310">
            <mixed-citation id="d701e951" publication-type="other">
Tilman D, Fargione J, Wolff B, D'Antonio С, Dobson A, Howarth
R, Schindler D, Schlesinger WH, Simberloff D, Swackhamer D.
2001. Forecasting agriculturally driven global environmental
change. Science 292:281-4.</mixed-citation>
         </ref>
         <ref id="d701e967a1310">
            <mixed-citation id="d701e971" publication-type="other">
Tranvik LJ, Downing JA, Cotner JB, Loiselle SA, Striegl RG,
Ballatore TJ, Dillon P, Finlay K, Fortino К, Knoll LB, Korte-
lainen PL, Kutser T, Larsen S, Laurion I, Leech DM, McCall-
ister SL, McKnight DM, Melack JM, Overholt E, Porter JA,
Prairie Y, Renwick WH, Roland F, Sherman BS, Schindler
DW, Sobek S, Tremblay A, Vanni MJ, Verschoor AM, von
Wachenfeldt E, Weyhenmeyer GA. 2009. Lakes and reservoirs
as regulators of carbon cycling and climate. Limnol Oceanogr
54:2298-314.</mixed-citation>
         </ref>
         <ref id="d701e1003a1310">
            <mixed-citation id="d701e1007" publication-type="other">
Van Zant KL, Webb T, Peterson GM, Baker RG. 1979. Increased
Cannabis/Humulus pollen, an indicator of European agricul-
ture in Iowa. Palynology 3:227-33.</mixed-citation>
         </ref>
         <ref id="d701e1020a1310">
            <mixed-citation id="d701e1024" publication-type="other">
Watson SB, McCauley E, Downing JA. 1997. Patterns in phy-
toplankton taxonomie composition across temperate lakes of
differing nutrient status. Limnol Oceanogr 42:487-95.</mixed-citation>
         </ref>
         <ref id="d701e1038a1310">
            <mixed-citation id="d701e1042" publication-type="other">
Wong CS, Sanders G, Engstrom DR, Long DT, Swackhamer DL,
Eisenreich SJ. 1995. Accumulation, inventory, and diagenesis
of chlorinated hydrocarbons in Lake Ontario sediments.
Environ Sci Technol 29:2661-72.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
