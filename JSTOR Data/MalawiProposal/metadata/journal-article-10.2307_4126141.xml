<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">americanjbotany</journal-id>
         <journal-id journal-id-type="jstor">j100059</journal-id>
         <journal-title-group>
            <journal-title>American Journal of Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Botanical Society of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00029122</issn>
         <issn pub-type="epub">15372197</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4126141</article-id>
         <article-categories>
            <subj-group>
               <subject>Systematics and Phytogeography</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Life History and Systematics of the Aquatic Discomycete Mitrula (Helotiales, Ascomycota) Based on Cultural, Morphological, and Molecular Studies</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Zheng</given-names>
                  <surname>Wang</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Manfred</given-names>
                  <surname>Binder</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>David S.</given-names>
                  <surname>Hibbett</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">92</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">9</issue>
         <issue-id>i384100</issue-id>
         <fpage>1565</fpage>
         <lpage>1574</lpage>
         <page-range>1565-1574</page-range>
         <permissions>
            <copyright-statement>Copyright 2005 Botanical Society of America, Inc.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4126141"/>
         <abstract>
            <p>Mitrula species represent a group of aquatic discomycetes with uncertain position in the Helotiales and an unknown life history. Mitrula species were studied using a combination of cultural, morphological, and molecular techniques. Pure colonies were isolated from Mitrula elegans, and conidia were induced in vitro. Herbarium materials from Europe, Asia, and North America were studied. Sequences of rDNA, including partial small subunit rDNA, large subunit DNA and ITS, were used to infer phylogenetic relationships both within Mitrula and between Mitrula and other inoperculate discomycetes, with special attention to fungi that resemble Mitrula in morphology or ecology. Equally weighted parsimony analyses, likelihood analyses, constrained parsimony analyses, and Bayesian analyses were performed. Results suggest that (1) the anamorph of M. elegans produces brown bicellular conidia, (2) a new subalpine species M. brevispora is distinct, (3) more than six lineages and clades can be recognized in Mitrula, (4) the morphological species M. elegans is not monophyletic, (5) a close relationship between Mitrula and either Geoglossaceae or Sclerotiniaceae is not supported, (6) the Helotiaceae is paraphyletic, and (7) Mitrula belongs to a clade within the Helotiales that also includes other aero-aquatic genera, Cudoniella, Hydrocina, Vibrissea, Ombrophila, and Hymenoscyphus.</p>
         </abstract>
         <kwd-group>
            <kwd>Aquatic Fungi</kwd>
            <kwd>Decomposition</kwd>
            <kwd>Ecology</kwd>
            <kwd>Mitosporic Fungi</kwd>
            <kwd>Vernal Pools</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d661e295a1310">
            <mixed-citation id="d661e299" publication-type="book">
ALLAN, J. D. 1995. Stream ecology, structure and function of running water.
Chapman &amp; Hall, London, UK.<person-group>
                  <string-name>
                     <surname>Allan</surname>
                  </string-name>
               </person-group>
               <source>Stream ecology, structure and function of running water</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d661e324a1310">
            <mixed-citation id="d661e328" publication-type="journal">
ABDULLAH, S. K., E. DESCALS, AND J. WEBSTER. 1981. Teleomorphs of three
aquatic hyphomycetes. Transactions of the British Mycological Society
77: 475-483.<person-group>
                  <string-name>
                     <surname>Abdullah</surname>
                  </string-name>
               </person-group>
               <fpage>475</fpage>
               <volume>77</volume>
               <source>Transactions of the British Mycological Society</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d661e363a1310">
            <mixed-citation id="d661e367" publication-type="journal">
BLANCHETTE, R. A. 1994. Degradation of the lignocellulose complex in
wood. Canadian Journal of Botany 73 (Supplement 1): S999-S1010.<person-group>
                  <string-name>
                     <surname>Blanchette</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>999</fpage>
               <volume>73</volume>
               <source>Canadian Journal of Botany</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d661e402a1310">
            <mixed-citation id="d661e406" publication-type="journal">
BENKERT, D. 1983. Bemerkenswerte Ascomyceten der DDR VI. Die
weißsporigen Geoglossaceen. Gleditschia 10: 141-171.<person-group>
                  <string-name>
                     <surname>Benkert</surname>
                  </string-name>
               </person-group>
               <fpage>141</fpage>
               <volume>10</volume>
               <source>Gleditschia</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d661e439a1310">
            <mixed-citation id="d661e443" publication-type="book">
DENNIS, R. W. G. 1968. British ascomycetes. Cramer, Lehre, Germany.<person-group>
                  <string-name>
                     <surname>Dennis</surname>
                  </string-name>
               </person-group>
               <source>British ascomycetes</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d661e465a1310">
            <mixed-citation id="d661e469" publication-type="journal">
DESCALS, E., P. J. FISHER, AND J. WEBSTER. 1984. The Hymenoscyphus te-
leomorph of Geniculospora grandis. Transactions of the British Myco-
logical Society 83: 541-546.<person-group>
                  <string-name>
                     <surname>Descals</surname>
                  </string-name>
               </person-group>
               <fpage>541</fpage>
               <volume>83</volume>
               <source>Transactions of the British Mycological Society</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d661e504a1310">
            <mixed-citation id="d661e508" publication-type="journal">
DIGBY, S., AND R. D. Goos. 1987. Morphology, development and taxonomy
of Loramyces. Mycologia 79: 821-831.<object-id pub-id-type="doi">10.2307/3807683</object-id>
               <fpage>821</fpage>
            </mixed-citation>
         </ref>
         <ref id="d661e524a1310">
            <mixed-citation id="d661e528" publication-type="journal">
ERIKSSON, E., H.-O. BARAL, R. S. CURRAH, K. HANSEN, P. KURTZMAN,
G. RAMBOLD, AND T. LAESSØE [EDS.]. 2004. Outline of Ascomycota:
2004. Myconet 10: 1-99.<person-group>
                  <string-name>
                     <surname>Eriksson</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>10</volume>
               <source>Myconet</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d661e563a1310">
            <mixed-citation id="d661e567" publication-type="journal">
FISHER, P. J., AND B. SPOONER. 1987. Two new ascomycetes from Malawi.
Transactions of the British Mycological Society 88: 47-54.<person-group>
                  <string-name>
                     <surname>Fisher</surname>
                  </string-name>
               </person-group>
               <fpage>47</fpage>
               <volume>88</volume>
               <source>Transactions of the British Mycological Society</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d661e599a1310">
            <mixed-citation id="d661e603" publication-type="journal">
FISHER, P. J., AND J. WEBSTER. 1983. The teleomorphs of Helicodendron
gigante um and H. paradoxum. Transactions of the British Mycological
Society 81: 656-659.<person-group>
                  <string-name>
                     <surname>Fisher</surname>
                  </string-name>
               </person-group>
               <fpage>656</fpage>
               <volume>81</volume>
               <source>Transactions of the British Mycological Society</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d661e639a1310">
            <mixed-citation id="d661e643" publication-type="journal">
GERNANDT, D. S., F. J. CAMACHO, AND J. K. STONE. 1997. Meria laricis, an
anamoph of Rhabdocline. Mycologia 89: 735-744.<object-id pub-id-type="doi">10.2307/3761130</object-id>
               <fpage>735</fpage>
            </mixed-citation>
         </ref>
         <ref id="d661e659a1310">
            <mixed-citation id="d661e663" publication-type="journal">
GERNANDT, D. S., J. L. PLATT, J. K. STONE, J. W. SPATAFORA, A. HOLST-
JENSEN, R. HAMELIN, AND L. M. KOHN. 2001. Phylogenetics of Hel-
otiales and Rhytismatales based on partial small subunit nuclear ribo-
somal DNA sequences. Mycologia 93: 915-933.<object-id pub-id-type="doi">10.2307/3761757</object-id>
               <fpage>915</fpage>
            </mixed-citation>
         </ref>
         <ref id="d661e686a1310">
            <mixed-citation id="d661e690" publication-type="journal">
GOLDMAN, N., J. P. ANDERSON, AND A. G. RODRIGO. 2000. Likelihood-based
test of topologies in phylogenetics. Systematic Biology 49: 652-670.<object-id pub-id-type="jstor">10.2307/2585286</object-id>
               <fpage>652</fpage>
            </mixed-citation>
         </ref>
         <ref id="d661e706a1310">
            <mixed-citation id="d661e710" publication-type="journal">
GOODWIN, S. B. 2002. The barley scald pathogen Rhynchosporium secalis is
closely related to the discomycetes Tapesia and Pyrenopeziza. Mycolog-
ical Research 106: 645-654.<person-group>
                  <string-name>
                     <surname>Goodwin</surname>
                  </string-name>
               </person-group>
               <fpage>645</fpage>
               <volume>106</volume>
               <source>Mycological Research</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d661e745a1310">
            <mixed-citation id="d661e749" publication-type="journal">
Goos, R. D., S. K. ABDULLAH, P. J. FISHER, AND J. WEBSTER. 1986. The
anamorph genus Helicoon. Transactions of the British Mycological So-
ciety 87: 115-122.<person-group>
                  <string-name>
                     <surname>Goos</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>87</volume>
               <source>Transactions of the British Mycological Society</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d661e784a1310">
            <mixed-citation id="d661e788" publication-type="journal">
HAMAD, S. R., AND J. WEBSTER. 1988. Anavirga dendromorpha, anamorph
of Apostemidium torrenticola. Sydowia 40: 60-64.<person-group>
                  <string-name>
                     <surname>Hamad</surname>
                  </string-name>
               </person-group>
               <fpage>60</fpage>
               <volume>40</volume>
               <source>Sydowia</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d661e821a1310">
            <mixed-citation id="d661e827" publication-type="journal">
HARRINGTON, T. J. 1997. Aquatic hyphomycetes of 21 rivers in southern
Ireland. Biology and Enviroment: Proceedings of the Royal Irish Acad-
emy 97: 139-148.<person-group>
                  <string-name>
                     <surname>Harrington</surname>
                  </string-name>
               </person-group>
               <fpage>139</fpage>
               <volume>97</volume>
               <source>Biology and Enviroment: Proceedings of the Royal Irish Academy</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d661e862a1310">
            <mixed-citation id="d661e866" publication-type="journal">
HARRINGTON, AND D. L. MCNEW. 2003. Phylogenetic analysis places
the Phialophora-like anamorph genus Cadophora in the Helotiales. My-
cotaxon 87: 141-152.<person-group>
                  <string-name>
                     <surname>Harrington</surname>
                  </string-name>
               </person-group>
               <fpage>141</fpage>
               <volume>87</volume>
               <source>Mycotaxon</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d661e901a1310">
            <mixed-citation id="d661e905" publication-type="book">
HAWKSWORTH, D. L., P. M. KIRK, SUTTON, AND D. N. PEGLER. 1995.
Ainsworth and Bisby's dictionary of the Fungi, 8th ed. CAB Interna-
tional, Wallingford, UK.<person-group>
                  <string-name>
                     <surname>Hawksworth</surname>
                  </string-name>
               </person-group>
               <edition>8</edition>
               <source>Ainsworth and Bisby's dictionary of the Fungi</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d661e937a1310">
            <mixed-citation id="d661e941" publication-type="journal">
HIBBETT, D. S. 1996. Phylogenetic evidence for horizontal transmission of
group I introns in the nuclear ribosomal DNA of mushroom-forming
fungi. Molecular Biology and Evolution 13: 903-917.<person-group>
                  <string-name>
                     <surname>Hibbett</surname>
                  </string-name>
               </person-group>
               <fpage>903</fpage>
               <volume>13</volume>
               <source>Molecular Biology and Evolution</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d661e976a1310">
            <mixed-citation id="d661e980" publication-type="journal">
HUELSENBECK, J. P., AND F. RONQUIST. 2001. MrBayes: Bayesian inference
of phylogenetic trees. Bioinformatics 17: 754-755.<person-group>
                  <string-name>
                     <surname>Huelsenbeck</surname>
                  </string-name>
               </person-group>
               <fpage>754</fpage>
               <volume>17</volume>
               <source>Bioinformatics</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1012a1310">
            <mixed-citation id="d661e1016" publication-type="journal">
IMAI, S. 1941. Geoglossaceae Japoniae. Journal of the Faculty of Agriculture,
Hokkaido Imperial University 45: 155-264.<person-group>
                  <string-name>
                     <surname>Imai</surname>
                  </string-name>
               </person-group>
               <fpage>155</fpage>
               <volume>45</volume>
               <source>Journal of the Faculty of Agriculture, Hokkaido Imperial University</source>
               <year>1941</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1049a1310">
            <mixed-citation id="d661e1053" publication-type="journal">
INGOLD, T. 1942. Aquatic hyphomycetes of decaying alder leaves. Transac-
tions of the British Mycological Society 25: 339-417.<person-group>
                  <string-name>
                     <surname>Ingold</surname>
                  </string-name>
               </person-group>
               <fpage>339</fpage>
               <volume>25</volume>
               <source>Transactions of the British Mycological Society</source>
               <year>1942</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1085a1310">
            <mixed-citation id="d661e1089" publication-type="journal">
ITURRIAGA, T. 1997. Vibrissea pfisteri, a new species with an unusual ecol-
ogy. Mycotaxon 51: 215-221.<person-group>
                  <string-name>
                     <surname>Iturriaga</surname>
                  </string-name>
               </person-group>
               <fpage>215</fpage>
               <volume>51</volume>
               <source>Mycotaxon</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1121a1310">
            <mixed-citation id="d661e1125" publication-type="journal">
KANKAINEN, E. 1969. On the structure, ecology and distribution of the spe-
cies of Mitrula s. lat. (Ascomycetes, Geoglossaceae). Karstenia 9: 23-
34.<person-group>
                  <string-name>
                     <surname>Kankainen</surname>
                  </string-name>
               </person-group>
               <fpage>23</fpage>
               <volume>9</volume>
               <source>Karstenia</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1160a1310">
            <mixed-citation id="d661e1164" publication-type="book">
KEEKEY, J. E., AND P. H. ZEDLER. 1998. Characterization and global distri-
bution of vernal pools. In C. W. Witham, E. T. Bauder, D. Belk, W. R.
Ferren Jr., and R. Ornduff [eds.], Ecology, conservation, and manage-
ment of vernal pool ecosystems, proceedings from 1996 conference, 1-
14. California Native Plant Society, Sacramento, California, USA.<person-group>
                  <string-name>
                     <surname>Keekey</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Characterization and global distribution of vernal pools</comment>
               <fpage>1</fpage>
               <source>Ecology, conservation, and management of vernal pool ecosystems, proceedings from 1996 conference</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1205a1310">
            <mixed-citation id="d661e1209" publication-type="book">
KIRK, P. M., P. F. CANNON, J. DAVID, AND J. A. STALPERS. 2001. Ains-
worth and Bisby's dictionary of the Fungi, 9th ed. CAB International,
Wallingford, UK.<person-group>
                  <string-name>
                     <surname>Kirk</surname>
                  </string-name>
               </person-group>
               <edition>9</edition>
               <source>Ainsworth and Bisby's dictionary of the Fungi</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1241a1310">
            <mixed-citation id="d661e1245" publication-type="journal">
KIRK, K., AND R. L. FARREL. 1987. Enzymatic "combustion": the micro-
bial degradation of ligin. Annual Review of Microbiology 41: 465-505.<person-group>
                  <string-name>
                     <surname>Kirk</surname>
                  </string-name>
               </person-group>
               <fpage>465</fpage>
               <volume>41</volume>
               <source>Annual Review of Microbiology</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1278a1310">
            <mixed-citation id="d661e1282" publication-type="book">
KORF, R. P. 1973. Discomycetes and Tuberales. In G. C. Ainsworth, F. K.
Sparrow, and A. S. Sussman [eds.], The Fungi: an advanced treatise, vol.
4A, 249-319. Academic Press, New York, New York, USA.<person-group>
                  <string-name>
                     <surname>Korf</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Discomycetes and Tuberales</comment>
               <fpage>249</fpage>
               <volume>4A</volume>
               <source>The Fungi: an advanced treatise</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1320a1310">
            <mixed-citation id="d661e1324" publication-type="journal">
KORF, R. P. 1990. Discomycete systematics today: a look at some unanswered
questions in a group of unitunicate ascomycetes. Mycosystema 3: 19-27.<person-group>
                  <string-name>
                     <surname>Korf</surname>
                  </string-name>
               </person-group>
               <fpage>19</fpage>
               <volume>3</volume>
               <source>Mycosystema</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1356a1310">
            <mixed-citation id="d661e1360" publication-type="other">
LANDVIK, S. 1996. Phylogenetic rDNA studies of Discomycetes (Ascomy-
cota). Ph.D. dissertation, Department of Ecological Botany, Umeå Uni-
versity, Umea, Sweden.</mixed-citation>
         </ref>
         <ref id="d661e1373a1310">
            <mixed-citation id="d661e1377" publication-type="journal">
MAAS GEESTERANUS, R. A. 1964. On some white-spored Geoglossaceae.
Persoonia 3: 81-96.<person-group>
                  <string-name>
                     <surname>Geesteranus</surname>
                  </string-name>
               </person-group>
               <fpage>81</fpage>
               <volume>3</volume>
               <source>Persoonia</source>
               <year>1964</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1409a1310">
            <mixed-citation id="d661e1413" publication-type="book">
MARVANOVA, L. 1997. Freshwater hyphomycetes: a survey with remarks on
tropical taxa. In K. K. Janardhanan, C. Rajendran, K. Natarajan, and D.
L. Hawksworth [eds.], Tropical mycology, 169-226. Science Publishers,
Enfield, New Hampshire, USA.<person-group>
                  <string-name>
                     <surname>Marvanova</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Freshwater hyphomycetes: a survey with remarks on tropical taxa</comment>
               <fpage>169</fpage>
               <source>Tropical mycology</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1451a1310">
            <mixed-citation id="d661e1455" publication-type="journal">
MARVANOVA, L., AND E. DESCALS. 1996. Hyphomycetes from streams: new
taxa and new combination. Mycotaxon 50: 455-469.<person-group>
                  <string-name>
                     <surname>Marvanova</surname>
                  </string-name>
               </person-group>
               <fpage>455</fpage>
               <volume>50</volume>
               <source>Mycotaxon</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1488a1310">
            <mixed-citation id="d661e1492" publication-type="journal">
NIKOLCHEVA, L. G., A. M. COCKSHUTT, AND F. BÄRLOCHER. 2003. Deter-
mining diversity of freshwater fungi on decaying leaves: comparison of
traditional and molecular approaches. Applied and Environmental Mi-
crobiology 69: 2548-2554.<person-group>
                  <string-name>
                     <surname>Nikolcheva</surname>
                  </string-name>
               </person-group>
               <fpage>2548</fpage>
               <volume>69</volume>
               <source>Applied and Environmental Microbiology</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1530a1310">
            <mixed-citation id="d661e1534" publication-type="book">
NYLANDER, J. A. A. 2004. MrModeltest, version 2.0. Program distributed by
the author. Evolutionary Biology Centre, Uppsala University, Uppsala,
Sweden, website: http://www.ebc.uu.se/systzoo/staff/nylander.html.<person-group>
                  <string-name>
                     <surname>Nylander</surname>
                  </string-name>
               </person-group>
               <source>MrModeltest, version 2.0</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1563a1310">
            <mixed-citation id="d661e1567" publication-type="journal">
PFISTER, D. H. 1997. Castor, Pollux and life histories of fungi. Mycologia
89: 1-23.<object-id pub-id-type="doi">10.2307/3761168</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d661e1583a1310">
            <mixed-citation id="d661e1587" publication-type="book">
PFISTER, D. H., AND J. W. KIMBROUGH. 2001. Discomycetes. In D. J.
McLaughlin, E. G. McLaughlin, and P. A. Lemke [eds.], The Mycota
VII, part A, Systematics and evolution, 257-281. Springer-Verlag, Berlin,
Germany.<person-group>
                  <string-name>
                     <surname>Pfister</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Discomycetes</comment>
               <fpage>257</fpage>
               <source>The Mycota VII, part A, Systematics and evolution</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1625a1310">
            <mixed-citation id="d661e1629" publication-type="journal">
POINTING, S. A. L. PELLING, G. J. D. SMITH, K. D. HYDE, AND A.
REDDY. 2004. Screening of basidiomycetes and xylariaceous fungi for
lignin peroxidase and laccase gene-specific sequences. Mycological Re-
search 109: 115-124.<person-group>
                  <string-name>
                     <surname>Pointing</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>109</volume>
               <source>Mycological Research</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1667a1310">
            <mixed-citation id="d661e1671" publication-type="other">
PLATT, J. L. 2000. Lichens, earth tongues, and endophytes: evolutionary pat-
terns inferred from phylogenetic analyses of multiple loci. Ph.D. disser-
tation, Oregon State University, Corvallis, Oregon, USA.</mixed-citation>
         </ref>
         <ref id="d661e1685a1310">
            <mixed-citation id="d661e1689" publication-type="journal">
POSADA, D., AND K. A. CRANDALL. 1998. Modeltest: testing the model of
DNA substitution. Bioinformatics 14: 817-818.<person-group>
                  <string-name>
                     <surname>Posada</surname>
                  </string-name>
               </person-group>
               <fpage>817</fpage>
               <volume>14</volume>
               <source>Bioinformatics</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1721a1310">
            <mixed-citation id="d661e1725" publication-type="journal">
REDHEAD, S. A. 1977. The genus Mitrula in North America. Canadian Jour-
nal of Botany 55: 307-325.<person-group>
                  <string-name>
                     <surname>Redhead</surname>
                  </string-name>
               </person-group>
               <fpage>307</fpage>
               <volume>55</volume>
               <source>Canadian Journal of Botany</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1757a1310">
            <mixed-citation id="d661e1763" publication-type="journal">
SANCHEZ, A., AND R. P. KORF. 1966. The genus Vibrissea, and the generic
names Leptosporium, Apostremium, Apostemidium, Gorgoniceps and
Ophiogloea. Mycologia 58: 722-737.<object-id pub-id-type="doi">10.2307/3756847</object-id>
               <fpage>722</fpage>
            </mixed-citation>
         </ref>
         <ref id="d661e1782a1310">
            <mixed-citation id="d661e1786" publication-type="journal">
SCHUMACHER, T., AND A. HOLST-JENSEN. 1997. A synopsis of the genus
Scleromitrula (=Verpatinia) (Asomycotina: Helotiales: Sclerotiniaceae).
Mycoscience 38: 55-69.<person-group>
                  <string-name>
                     <surname>Schumacher</surname>
                  </string-name>
               </person-group>
               <fpage>55</fpage>
               <volume>38</volume>
               <source>Mycoscience</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1821a1310">
            <mixed-citation id="d661e1825" publication-type="book">
SEAVER, F. J. 1951. The North American cup-fungi (In-operculates). Seaver,
New York, New York, USA.<person-group>
                  <string-name>
                     <surname>Seaver</surname>
                  </string-name>
               </person-group>
               <source>The North American cup-fungi</source>
               <year>1951</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1850a1310">
            <mixed-citation id="d661e1854" publication-type="journal">
SHIMODAIRA, H., AND M. HASEGAWA. 1999. Multiple comparisons of log-
likelihoods with applications to phylogenetic inference. Molecular Bi-
ology and Evolution 13: 964-969.<person-group>
                  <string-name>
                     <surname>Shimodaira</surname>
                  </string-name>
               </person-group>
               <fpage>964</fpage>
               <volume>13</volume>
               <source>Molecular Biology and Evolution</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1890a1310">
            <mixed-citation id="d661e1894" publication-type="book">
SUTTON, AND G. L. HENNEBERT. 1994. Interconnections amongst an-
amorphs and their possible contribution to ascomycete systematics. In D.
L. Hawksworth [ed.], Ascomycete systematics: problems and perspec-
tives in the nineties, 77-100. Plenum Press, New York, New York, USA.<person-group>
                  <string-name>
                     <surname>Sutton</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Interconnections amongst anamorphs and their possible contribution to ascomycete systematics</comment>
               <fpage>77</fpage>
               <source>Ascomycete systematics: problems and perspectives in the nineties</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1932a1310">
            <mixed-citation id="d661e1936" publication-type="other">
SWOFFORD, D. L. 2002. PAUP*: phylogenetic analysis using parsimony
(* and other methods), version 4 beta. Sinauer, Sunderland, Massachu-
setts, USA.</mixed-citation>
         </ref>
         <ref id="d661e1949a1310">
            <mixed-citation id="d661e1953" publication-type="book">
TENG, S. 1996. Fungi of China. R. P. Korf [ed.]. Mycotaxon Ltd., Ithaca,
New York, USA.<person-group>
                  <string-name>
                     <surname>Teng</surname>
                  </string-name>
               </person-group>
               <source>Fungi of China</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d661e1978a1310">
            <mixed-citation id="d661e1982" publication-type="journal">
THOMPSON, J. D., T. J. GIBSON, F. JEANMOUGIN, AND D. G. HIGGINS. 1997.
The ClustalX windows interface: flexible strategies for multiple sequence
alignment aided by quality analysis tools. Nucleic Acids Research 24:
4876-4882.<person-group>
                  <string-name>
                     <surname>Thompson</surname>
                  </string-name>
               </person-group>
               <fpage>4876</fpage>
               <volume>24</volume>
               <source>Nucleic Acids Research</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d661e2020a1310">
            <mixed-citation id="d661e2024" publication-type="journal">
VERKLEY, G. J. M. 1994. Ultrastructure of the apical apparatus in Leotia
lubrica and some Geoglossaceae (Leotiales, Ascomycotina). Persoonia
15: 405-430.<person-group>
                  <string-name>
                     <surname>Verkley</surname>
                  </string-name>
               </person-group>
               <fpage>405</fpage>
               <volume>15</volume>
               <source>Persoonia</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d661e2059a1310">
            <mixed-citation id="d661e2063" publication-type="journal">
VILGALYS, R., AND M. HESTER. 1990. Rapid genetic identification and map-
ping of enzymatically amplified ribosomal DNA from several species of
Cryptococcus. Journal of Bacteriology 172: 4238-4246.<person-group>
                  <string-name>
                     <surname>Vilgalys</surname>
                  </string-name>
               </person-group>
               <fpage>4238</fpage>
               <volume>172</volume>
               <source>Journal of Bacteriology</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d661e2099a1310">
            <mixed-citation id="d661e2103" publication-type="journal">
WEBSTER, J., A. EICKER, AND M. SPOONER. 1995. Cudoniella indica sp.
nov. (Ascomycetes, leotiales), the teleomorph of Tricladium indicum, an
aquatic fungus isolated from a South African river. Nova Hedwigia 60:
493-498.<person-group>
                  <string-name>
                     <surname>Webster</surname>
                  </string-name>
               </person-group>
               <fpage>493</fpage>
               <volume>60</volume>
               <source>Nova Hedwigia</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d661e2141a1310">
            <mixed-citation id="d661e2145" publication-type="journal">
WEBSTER, J., SCHEUER, AND S. OM-KALTHOUM. 1991. Hydrocina
chaetocladia gen. et sp. nov., the teleomorph of Tricladium chaetoclad-
ium. Nova Hedwigia 51: 65-72.<person-group>
                  <string-name>
                     <surname>Webster</surname>
                  </string-name>
               </person-group>
               <fpage>65</fpage>
               <volume>51</volume>
               <source>Nova Hedwigia</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d661e2180a1310">
            <mixed-citation id="d661e2184" publication-type="journal">
WEYEMBERGH, G., S. GODEFROID, AND N. KOEDAM. 2004. Restoration of a
small-scale forest wetland in a Belgian nature reserve: a discussion of
factors determining wetland vegetation establishment. Aquatic Conser-
vation: Marine and Freshwater Ecosystem 14: 381-394.<person-group>
                  <string-name>
                     <surname>Weyembergh</surname>
                  </string-name>
               </person-group>
               <fpage>381</fpage>
               <volume>14</volume>
               <source>Aquatic Conservation: Marine and Freshwater Ecosystem</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d661e2222a1310">
            <mixed-citation id="d661e2226" publication-type="book">
WHITE, T. J., T. BRUNS, S. LEE, AND J. TAYLOR. 1990. Amplification and
direct sequencing of fungal ribosomal RNA genes for phylogenetics. In
M. A. Innis, D. H. Gelfand, J. J. Sninsky, and T. J. White [eds.], PCR
protocols, 315-322. Academic Press, San Diego, California, USA.<person-group>
                  <string-name>
                     <surname>White</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Amplification and direct sequencing of fungal ribosomal RNA genes for phylogenetics</comment>
               <fpage>315</fpage>
               <source>PCR protocols</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
