<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">demography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100446</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00703370</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15337790</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42920000</article-id>
         <title-group>
            <article-title>Is the Demographic Dividend an Education Dividend?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jesús Crespo</given-names>
                  <surname>Cuaresma</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Wolfgang</given-names>
                  <surname>Lutz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Warren</given-names>
                  <surname>Sanderson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40112076</issue-id>
         <fpage>299</fpage>
         <lpage>315</lpage>
         <permissions>
            <copyright-statement>© Population Association of America 2014</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1007/s13524-013-0245-x"
                   xlink:title="an external site"/>
         <abstract>
            <p>The effect of changes in age structure on economic growth has been widely studied in the demography and population economics literature. The beneficial effect of changes in age structure after a decrease in fertility has become known as the "demographic dividend." In this article, we reassess the empirical evidence on the associations among economic growth, changes in age structure, labor force participation, and educational attainment. Using a global panel of countries, we find that after the effect of human capital dynamics is controlled for, no evidence exists that changes in age structure affect labor productivity. Our results imply that improvements in educational attainment are the key to explaining productivity and income growth and that a substantial portion of the demographic dividend is an education dividend.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1043e176a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1043e183" publication-type="other">
Lee and Mason (2011)</mixed-citation>
            </p>
         </fn>
         <fn id="d1043e190a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1043e197" publication-type="other">
Lee and
Mason 2010</mixed-citation>
            </p>
         </fn>
         <fn id="d1043e207a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1043e214" publication-type="other">
Lutz et al. (2008),</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1043e230a1310">
            <mixed-citation id="d1043e234" publication-type="other">
Arellano, M., &amp; Bond, S. (1991). Some tests of specification for panel data: Monte Carlo evidence and an
application to employment equations. The Review of Economic Studies, 58, 277-297.</mixed-citation>
         </ref>
         <ref id="d1043e244a1310">
            <mixed-citation id="d1043e248" publication-type="other">
Arellano, M., &amp; Bover, O. (1995). Another look at the instrumental-variable estimation of error-
components models. Journal of Econometrics, 68, 29-51.</mixed-citation>
         </ref>
         <ref id="d1043e258a1310">
            <mixed-citation id="d1043e262" publication-type="other">
Barro, R. J., &amp; Lee, J. W. (1993). International comparison of educational attainment. Journal of Monetary
Economics, 32, 363-394.</mixed-citation>
         </ref>
         <ref id="d1043e272a1310">
            <mixed-citation id="d1043e276" publication-type="other">
Barro, R. J., &amp; Lee, J. W. (2001). International data on educational attainment: Updates and implications.
Oxford Economic Papers, 53, 541-563.</mixed-citation>
         </ref>
         <ref id="d1043e287a1310">
            <mixed-citation id="d1043e291" publication-type="other">
Benhabib, J., &amp; Spiegel, M. (1994). The role of human capital in economic development. Evidence from
aggregate cross-country data. Journal of Monetary Economics, 34, 143-173.</mixed-citation>
         </ref>
         <ref id="d1043e301a1310">
            <mixed-citation id="d1043e305" publication-type="other">
Benhabib, J., &amp; Spiegel, M. (2005). Human capital and technology diffusion. In P. Aghion &amp; S.
Durlauf (Eds.), Handbook of economic growth (Vol. 1, pp. 935-966). Amsterdam, The Netherlands:
Elsevier.</mixed-citation>
         </ref>
         <ref id="d1043e318a1310">
            <mixed-citation id="d1043e322" publication-type="other">
Bloom, D. E., Canning, D., Fink, G., &amp; Finlay, J. E. (2009). Fertility, female labor force participation, and
the demographic dividend. Journal of Economic Growth, 14, 79-101.</mixed-citation>
         </ref>
         <ref id="d1043e332a1310">
            <mixed-citation id="d1043e336" publication-type="other">
Bloom, D. E., &amp; Williamson, J. G. (1998). Demographic transitions and economic miracles in emerging
Asia. World Bank Economic Review, 12, 419-455.</mixed-citation>
         </ref>
         <ref id="d1043e346a1310">
            <mixed-citation id="d1043e350" publication-type="other">
Blundell, R., &amp; Bond, S. (1999). GMM estimation with persistent panel data : An application to production
functions (IFS Working Paper No. W99/04). London, UK: Institute for Fiscal Studies. Retrieved from
http://ideas.repec.org/p/ifs/ifsewp/99-04.html</mixed-citation>
         </ref>
         <ref id="d1043e363a1310">
            <mixed-citation id="d1043e367" publication-type="other">
Bongaarts, J. (2010). The causes of educational differences in fertility in sub-Saharan Africa. Vienna
Yearbook of Population Research, 8, 31-50.</mixed-citation>
         </ref>
         <ref id="d1043e378a1310">
            <mixed-citation id="d1043e382" publication-type="other">
Cochrane, S. H. (1979). Fertility and Education. What do we really know? Baltimore, MD: Johns Hopkins
University Press.</mixed-citation>
         </ref>
         <ref id="d1043e392a1310">
            <mixed-citation id="d1043e396" publication-type="other">
Cochrane, S. H., Khan, M. A., &amp; Osheba, I. K. T. (1990). Education, income, and desired fertility in Egypt:
A revised perspective. Economic Development and Cultural Change, 38, 313-339.</mixed-citation>
         </ref>
         <ref id="d1043e406a1310">
            <mixed-citation id="d1043e410" publication-type="other">
Cohen, D., &amp; Soto, M. (2007). Growth and human capital: Good data, good results. Journal of Economic
Growth, 12, 51-76.</mixed-citation>
         </ref>
         <ref id="d1043e420a1310">
            <mixed-citation id="d1043e424" publication-type="other">
Collins, S. M., &amp; Bosworth, B. P. (1996). Economic growth in East Asia: Accumulation versus assimila-
tion. Brookings Papers on Economic Activity, 2, 135-203.</mixed-citation>
         </ref>
         <ref id="d1043e434a1310">
            <mixed-citation id="d1043e438" publication-type="other">
Crespo Cuaresma, J., &amp; Mishra, T. (2011). The role of age-structured education data for economic growth
forecasts. Journal of Forecasting, 30, 249-267. doi:10.1002/for.1171</mixed-citation>
         </ref>
         <ref id="d1043e448a1310">
            <mixed-citation id="d1043e452" publication-type="other">
De la Fuente, A., &amp; Domenech, R. (2006). Human capital in growth regressions: How much difference
does data quality make? Journal of the European Economic Association, 4, 1-36.</mixed-citation>
         </ref>
         <ref id="d1043e463a1310">
            <mixed-citation id="d1043e467" publication-type="other">
Ehrlich, P. R. (1968). The population bomb. New York: Ballantine.</mixed-citation>
         </ref>
         <ref id="d1043e474a1310">
            <mixed-citation id="d1043e478" publication-type="other">
Ehrlich, P. R., &amp; Ehrlich, A. H. (1990). The population explosion. New York: Simon &amp; Schuster.</mixed-citation>
         </ref>
         <ref id="d1043e485a1310">
            <mixed-citation id="d1043e489" publication-type="other">
Hall, R. E., &amp; Jones, C. I. (1999). Why do some countries produce so much more output per worker than
others? The Quarterly Journal of Economics, 114, 83-116.</mixed-citation>
         </ref>
         <ref id="d1043e499a1310">
            <mixed-citation id="d1043e503" publication-type="other">
Hanushek, E. A., &amp; Woessmann, L. (2008). The role of cognitive skills in economic development. Journal
of Economic Literature, 46, 607-668.</mixed-citation>
         </ref>
         <ref id="d1043e513a1310">
            <mixed-citation id="d1043e517" publication-type="other">
Heston, A., Summers, R., &amp; Aten, B. (2009). Penn world table version 6.3. Philadelphia: Center for
International Comparisons of Production, Income and Prices, University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="d1043e527a1310">
            <mixed-citation id="d1043e531" publication-type="other">
Kelley, A., &amp; Schmidt, R. (2005). Evolution of recent economic-demographic modeling: A synthesis.
Journal of Population Economics, 18, 275-300.</mixed-citation>
         </ref>
         <ref id="d1043e542a1310">
            <mixed-citation id="d1043e546" publication-type="other">
Krueger, A., &amp; Lindahl, M. (2001). Education for growth: Why for whom? Journal of Economic Literature,
39, 1101-1136.</mixed-citation>
         </ref>
         <ref id="d1043e556a1310">
            <mixed-citation id="d1043e560" publication-type="other">
Lee, R., Lee, S.-H., &amp; Mason, A. (2008). Charting the economic life cycle. Population and Development
Review, 34(Suppl.), 208-237.</mixed-citation>
         </ref>
         <ref id="d1043e570a1310">
            <mixed-citation id="d1043e574" publication-type="other">
Lee, R., &amp; Mason, A. (2010). Fertility, human capital and economic growth over the demographic
transition. European Journal of Population, 26, 159-182.</mixed-citation>
         </ref>
         <ref id="d1043e584a1310">
            <mixed-citation id="d1043e588" publication-type="other">
Lee, R., &amp; Mason, A. (2011). Population aging and the generational economy: A global perspective.
Cheltenham, UK, and Ottawa, Canada: Edward Elgar and International Development Research Centre.</mixed-citation>
         </ref>
         <ref id="d1043e598a1310">
            <mixed-citation id="d1043e602" publication-type="other">
Lee, R., Mason, A., &amp; Miller, T. (2000). Life cycle saving and the demographic transition: The case of
Taiwan. Population and Development Review, 26(Suppl.), 194-219.</mixed-citation>
         </ref>
         <ref id="d1043e612a1310">
            <mixed-citation id="d1043e616" publication-type="other">
Lee, R., Mason, A., &amp; Miller, T. (2003). Saving, wealth and the transition from transfers to individual
responsibility: The cases of Taiwan and the United States. The Scandinavian Journal of Economics,
105, 339-357.</mixed-citation>
         </ref>
         <ref id="d1043e630a1310">
            <mixed-citation id="d1043e634" publication-type="other">
Lutz, W., Crespo Cuaresma, J., &amp; Abbasi-Shavazi, M. J. (2010). Demography, education, and democracy:
Global trends and the case of Iran. Population and Development Review, 36, 253-281.</mixed-citation>
         </ref>
         <ref id="d1043e644a1310">
            <mixed-citation id="d1043e648" publication-type="other">
Lutz, W., Crespo Cuaresma, J., &amp; Sanderson, W. C. (2008). The demography of educational attainment and
economic growth. Science, 319, 1047-1048.</mixed-citation>
         </ref>
         <ref id="d1043e658a1310">
            <mixed-citation id="d1043e662" publication-type="other">
Lutz, W., Goujon, A., Samir, K. C., &amp; Sanderson, W. C. (2007). Reconstruction of populations by age, sex
and level of educational attainment for 120 countries for 1970-2000. Vienna Yearbook of Population
Research, 5, 193-235.</mixed-citation>
         </ref>
         <ref id="d1043e675a1310">
            <mixed-citation id="d1043e679" publication-type="other">
Lutz, W., &amp; Samir, K. C. (2011). Global human capital: Integrating education and population. Science, 333,
587-592.</mixed-citation>
         </ref>
         <ref id="d1043e689a1310">
            <mixed-citation id="d1043e693" publication-type="other">
Mankiw, N. G., Romer, D., &amp; Weil, D. W. (1992). A contribution to the empirics of economic growth.
Quarterly Journal of Economics, 107, 407-437.</mixed-citation>
         </ref>
         <ref id="d1043e703a1310">
            <mixed-citation id="d1043e707" publication-type="other">
Mason, A., &amp; Lee, R. (2006). Reform and support systems for the elderly in developing countries:
Capturing the second demographic dividend. GENUS, LXII(2), 11-35.</mixed-citation>
         </ref>
         <ref id="d1043e718a1310">
            <mixed-citation id="d1043e722" publication-type="other">
National Research Council. (1986). Population growth and economic development: Policy questions.
Washington, DC: National Academy Press.</mixed-citation>
         </ref>
         <ref id="d1043e732a1310">
            <mixed-citation id="d1043e736" publication-type="other">
Nelson, R. R., &amp; Phelps, E. S. (1966). Investment in humans, technological diffusion, and economic
growth. The American Economic Review, 56, 69-75.</mixed-citation>
         </ref>
         <ref id="d1043e746a1310">
            <mixed-citation id="d1043e750" publication-type="other">
Pamuk, E. R., Fuchs, R., &amp; Lutz, W. (2011). Comparing relative effects of education and economic
resources on infant mortality in developing countries. Population and Development Review, 37,
637-664.</mixed-citation>
         </ref>
         <ref id="d1043e763a1310">
            <mixed-citation id="d1043e767" publication-type="other">
Pritchett, L. (2001). Where has all the education gone? World Bank Economic Review, 15, 367-391.</mixed-citation>
         </ref>
         <ref id="d1043e774a1310">
            <mixed-citation id="d1043e778" publication-type="other">
Rodrik, D. (1998). TFPG controversies, institutions and economic performance in East Asia. In Y. Hayami
&amp; M. Aoki (Eds.), The institutional foundation of economic development in East Asia (pp. 79-105).
London, UK: Macmillan.</mixed-citation>
         </ref>
         <ref id="d1043e791a1310">
            <mixed-citation id="d1043e795" publication-type="other">
Samir, K. C., &amp; Lentzner, H. (2010). The effect of education on adult mortality and disability: A global
perspective. Vienna Yearbook of Population Research, 8, 201-235.</mixed-citation>
         </ref>
         <ref id="d1043e806a1310">
            <mixed-citation id="d1043e810" publication-type="other">
Simon, J. L. (1981). The ultimate resource. Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d1043e817a1310">
            <mixed-citation id="d1043e821" publication-type="other">
Simon, J. L. (1982). Paul Ehrlich, saying it is so doesn't make it so. Social Science Quarterly, 63, 381-385.</mixed-citation>
         </ref>
         <ref id="d1043e828a1310">
            <mixed-citation id="d1043e832" publication-type="other">
Skirbekk, V., &amp; Samir, K. C. (2012). Fertility-reducing dynamics of women's social status and educational
attainment. Asian Population Studies, 8, 251-264.</mixed-citation>
         </ref>
         <ref id="d1043e842a1310">
            <mixed-citation id="d1043e846" publication-type="other">
Solow, R. M. (1956). A contribution to the theory of economic growth. Quarterly Journal of Economics,
70, 65-94. doi:10.2307/1884513</mixed-citation>
         </ref>
         <ref id="d1043e856a1310">
            <mixed-citation id="d1043e860" publication-type="other">
Temple, J. (1999). A positive effect of human capital on growth. Economics Letters, 65, 131-134.</mixed-citation>
         </ref>
         <ref id="d1043e867a1310">
            <mixed-citation id="d1043e871" publication-type="other">
UNFPA. (2011). Impact of demographic change in Thailand. Bangkok, Thailand: United Nations
Population Fund.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
