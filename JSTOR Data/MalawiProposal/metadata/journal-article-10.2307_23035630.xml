<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">philtranbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100835</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Philosophical Transactions: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628436</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23035630</article-id>
         <title-group>
            <article-title>Contribution of vaccines to our understanding of pneumococcal disease</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Keith P.</given-names>
                  <surname>Klugman</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>12</day>
            <month>10</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">366</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1579</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23035618</issue-id>
         <fpage>2790</fpage>
         <lpage>2798</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23035630"/>
         <abstract>
            <p>Pneumonia is the leading cause of mortality in children in developing countries and is also the leading infectious cause of death in adults. The most important cause of pneumonia is the Gram-positive bacterial pathogen, Streptococcus pneumoniae, also known as the pneumococcus. It has thus become the leading vaccine-preventable cause of death and is a successful and diverse human pathogen. The development of conjugate pneumococcal vaccines has made possible the prevention of pneumococcal disease in infants, but has also elucidated aspects of pneumococcal biology in a number of ways. Use of the vaccine as a probe has increased our understanding of the burden of pneumococcal disease in children globally. Vaccination has also elucidated the clinical spectrum of vaccine-preventable pneumococcal infections; the identification of a biological niche for multiple pneumococcal serotypes in carriage and the differential invasiveness of pneumococcal serotypes; the impact of pneumococcal transmission among children on disease burden in adults; the role of carriage as a precursor to pneumonia; the plasticity of a naturally transformable pathogen to respond to selective pressure through capsular switching and the accumulation of antibiotic-resistance determinants; and the role of pneumococcal infections in hospitalization and mortality associated with respiratory viral infections, including both seasonal and pandemic influenza. Finally, there has been a recent demonstration that pneumococcal pneumonia in children may be an important cause of hospitalization for those with underlying tuberculosis.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1605e143a1310">
            <label>1</label>
            <mixed-citation id="d1605e150" publication-type="other">
Rudan, I. et al. 2010 Causes of deaths in children
younger than 5 years in China in 2008. Lancet 375,
1083-1089. (doi:10.1016/S0140-6736(10)60060-8)</mixed-citation>
         </ref>
         <ref id="d1605e163a1310">
            <label>2</label>
            <mixed-citation id="d1605e170" publication-type="other">
O'Brien, K. L. et al. 2009 Burden of disease caused by
Streptococcus pneumoniae in children younger than 5
years: global estimates. Lancet 374, 893-902. (doi:10.
1016/S0140-6736(09)61204-6)</mixed-citation>
         </ref>
         <ref id="d1605e186a1310">
            <label>3</label>
            <mixed-citation id="d1605e193" publication-type="other">
Macleod, C. M., Hodges, R. G., Heidelberger, M. &amp;
Bernhard, W. G. 1945 Prevention of pneumococcal
pneumonia by immunization with specific capsular poly-
saccharides. J. Exp. Med. 82, 445-465. (doi:10.1084/
jem.82.6.445)</mixed-citation>
         </ref>
         <ref id="d1605e212a1310">
            <label>4</label>
            <mixed-citation id="d1605e219" publication-type="other">
Avery, O. T. &amp; Goebel, W. F. 1931 Chemo-immunological
studies on carbohydrate-proteins. V. The immunological
specificity of an antigen prepared by combining the capsu-
lar polysaccharide of type III pneumococcus with foreign
protein. J. Exp. Med. 54, 419-426.</mixed-citation>
         </ref>
         <ref id="d1605e239a1310">
            <label>5</label>
            <mixed-citation id="d1605e246" publication-type="other">
Berkelman, R., Cassell, G., Specter, S., Hamburg, M. &amp;
Klugman, K. 2006 The 'Achilles heel' of global efforts
to combat infectious diseases. Clin. Infect. Dis. 42,
1503-1504. (doi:10.1086/504494)</mixed-citation>
         </ref>
         <ref id="d1605e262a1310">
            <label>6</label>
            <mixed-citation id="d1605e271" publication-type="other">
Klugman, K. R, Madhi, S. A. &amp; Albrich, W. C. 2008
Novel approaches to the identification of Streptococcus
pneumoniae as the cause of community-acquired pneu-
monia. Clin. Infect. Dis. 47(Suppl. 3), S202-S206.
(doi:10.1086/591405)</mixed-citation>
         </ref>
         <ref id="d1605e290a1310">
            <label>7</label>
            <mixed-citation id="d1605e297" publication-type="other">
Williams, B. G., Gouws, E., Boschi-Pinto, C., Bryce, J. &amp;
Dye, C. 2002 Estimates of world-wide distribution of
child deaths from acute respiratory infections. Lancet
Infect. Dis. 2, 25-32. (doi:10.1016/S1473-3099(01)
00170-0)</mixed-citation>
         </ref>
         <ref id="d1605e316a1310">
            <label>8</label>
            <mixed-citation id="d1605e323" publication-type="other">
Cutts, F. T. et al. 2005 Efficacy of nine-valent pneumo-
coccal conjugate vaccine against pneumonia and
invasive pneumococcal disease in The Gambia: random-
ised, double-blind, placebo-controlled trial. Lancet 365,
1139-1146. (doi:10.1016/S0140-6736(05)71876-6)</mixed-citation>
         </ref>
         <ref id="d1605e342a1310">
            <label>9</label>
            <mixed-citation id="d1605e349" publication-type="other">
Klugman, K. P., Madhi, S. A., Huebner, R. E.,
Kohberger, R., Mbelle, N. &amp; Pierce, N. 2003 A trial of
a 9-valent pneumococcal conjugate vaccine in children
with and those without HIV infection. N. Engl. J. Med.
349, 1341-1348. (doi:10.1056/NEJMoa035060)</mixed-citation>
         </ref>
         <ref id="d1605e368a1310">
            <label>10</label>
            <mixed-citation id="d1605e375" publication-type="other">
Lucero, M. G. et al. 2009 Efficacy of an 11-valent pneu-
mococcal conjugate vaccine against radiologically
confirmed pneumonia among children less than 2 years
of age in the Philippines: a randomized, double-blind,
placebo-controlled trial. Pediatr. Infect. Dis. J. 28,
455-462. (doi:10.1097/INF.0b013e31819637af)</mixed-citation>
         </ref>
         <ref id="d1605e399a1310">
            <label>11</label>
            <mixed-citation id="d1605e406" publication-type="other">
Madhi, S. A., Kuwanda, L., Cutland, C. &amp; Klugman,
K. P. 2005 The impact of a 9-valent pneumococcal
conjugate vaccine on the public health burden
of pneumonia in HIV-infected and -uninfected chil-
dren. Clin. Infect. Dis. 40, 1511-1518. (doi:10.1086/
429828)</mixed-citation>
         </ref>
         <ref id="d1605e429a1310">
            <label>12</label>
            <mixed-citation id="d1605e436" publication-type="other">
Madhi, S. A. &amp; Klugman, K. P. 2007 World Health
Organisation definition of 'radiologically-confirmed
pneumonia' may under-estimate the true public health
value of conjugate pneumococcal vaccines. Vaccine 25,
2413-2419. (doi:10.1016/j.vaccine.2006.09.010)</mixed-citation>
         </ref>
         <ref id="d1605e455a1310">
            <label>13</label>
            <mixed-citation id="d1605e462" publication-type="other">
Grijalva, C. G., Nuorti, J. P., Arbogast, P. G., Martin, S.
W., Edwards, K. M. &amp; Griffin, M. R. 2007 Decline in
pneumonia admissions after routine childhood immun-
isation with pneumococcal conjugate vaccine in the USA:
a time-series analysis. Lancet 369, 1179-1186. (doi:10.
1016/S0140-6736(07)60564-9)</mixed-citation>
         </ref>
         <ref id="d1605e485a1310">
            <label>14</label>
            <mixed-citation id="d1605e492" publication-type="other">
Nelson, J. C., Jackson, M., Yu, O., Whitney, C. G.,
Bounds, L., Bittner, R., Zavitkovsky, A. &amp; Jackson,
L. A. 2008 Impact of the introduction of pneumococcal
conjugate vaccine on rates of community acquired pneu-
monia in children and adults. Vaccine 26, 4947-4954.
(doi:10.1016/j.vaccine.2008.07.016)</mixed-citation>
         </ref>
         <ref id="d1605e515a1310">
            <label>15</label>
            <mixed-citation id="d1605e522" publication-type="other">
De Wals, P., Robin, E., Fortin, E., Thibeault, R.,
Ouakki, M. &amp; Douville-Fradet, M. 2008 Pneumonia
after implementation of the pneumococcal conjugate
vaccine program in the province of Quebec, Canada.
Pediatr. Infect. Dis. J. 27, 963-968. (doi:10.1097/INF.
0b013e31817cf76f)</mixed-citation>
         </ref>
         <ref id="d1605e545a1310">
            <label>16</label>
            <mixed-citation id="d1605e552" publication-type="other">
Jardine, A., Menzies, R. I. &amp; Mclntyre, P. B. 2010
Reduction in hospitalizations for pneumonia associated
with the introduction of a pneumococcal conjugate vac-
cination schedule without a booster dose in Australia.
Pediatr. Infect. Dis. J. 29, 607-612. (doi:10.1097/INF.
0b013e3181d7d09c)</mixed-citation>
         </ref>
         <ref id="d1605e576a1310">
            <label>17</label>
            <mixed-citation id="d1605e583" publication-type="other">
Simonsen, L., Taylor, R. J., Young-Xu, Y., Haber, M.,
May, L. &amp; Klugman, K. P. 2011 Impact of pneumo-
coccal conjugate vaccination of infants on pneumonia
and influenza hospitalization and mortality in all age
groups in the United States. mBio 2, e00309-e00310.
(doi:10.1128/mBio.00309-10)</mixed-citation>
         </ref>
         <ref id="d1605e606a1310">
            <label>18</label>
            <mixed-citation id="d1605e613" publication-type="other">
French, N. et al. 2010 A trial of a 7-valent pneumococcal
conjugate vaccine in HIV-infected adults. N. Engl.
J. Med. 362, 812-822. (doi:10.1056/NEJMoa0903029)</mixed-citation>
         </ref>
         <ref id="d1605e626a1310">
            <label>19</label>
            <mixed-citation id="d1605e635" publication-type="other">
Grijalva, C. G., Nuorti, J. P., Zhu, Y. &amp; Griffin, M. R.
2010 Increasing incidence of empyema complicating
childhood community-acquired pneumonia in the
United States. Clin. Infect. Dis. 50, 805-813. (doi:10.
1086/650573)</mixed-citation>
         </ref>
         <ref id="d1605e654a1310">
            <label>20</label>
            <mixed-citation id="d1605e661" publication-type="other">
Blaschke, A. J. et al. 2011 Molecular analysis im-
proves pathogen identification and epidemiologic study
of pediatric parapneumonic empyema. Pediatr. Infect.
Dis. J. 30, 289-294. (doi:10.1097/INF.0b013e3182
002dl4)</mixed-citation>
         </ref>
         <ref id="d1605e680a1310">
            <label>21</label>
            <mixed-citation id="d1605e687" publication-type="other">
Resti, M. et al. 2010 Community-acquired bacteremic
pneumococcal pneumonia in children: diagnosis and sero-
typing by real-time polymerase chain reaction using blood
samples. Clin. Infect. Dis. 51, 1042-1049. (doi:10.1086/
656579)</mixed-citation>
         </ref>
         <ref id="d1605e706a1310">
            <label>22</label>
            <mixed-citation id="d1605e713" publication-type="other">
Nuorti, J. P., Butler, J. C., Farley, M. M., Harrison, L.
H., McGeer, A., Kolczak, M. S. &amp; Breiman, R. F.
2000 Cigarette smoking and invasive pneumococcal
disease. Active Bacterial Core Surveillance Team.
N. Engl. J. Med. 342, 681-689. (doi:10.1056/NEJM20
0003093421002)</mixed-citation>
         </ref>
         <ref id="d1605e737a1310">
            <label>23</label>
            <mixed-citation id="d1605e744" publication-type="other">
Breiman, R. F., Keller, D. W., Phelan, M. A., Sniadack,
D. H., Stephens, D. S., Rimland, D., Farley, M. M.,
Schuchat, A. &amp; Reingold, A. L. 2000 Evaluation of effect-
iveness of the 23-valent pneumococcal capsular
polysaccharide vaccine for HIV-infected patients. Arch.
Intern. Med. 160, 2633-2638. (doi:10.1001/archinte.
160.17.2633)</mixed-citation>
         </ref>
         <ref id="d1605e770a1310">
            <label>24</label>
            <mixed-citation id="d1605e777" publication-type="other">
Feikin, D. R., Klugman, K. P., Facklam, R. R., Zell,
E. R., Schuchat, A. &amp; Whitney, C. G. 2005 Increased
prevalence of pediatric pneumococcal serotypes in
elderly adults. Clin. Infect. Dis. 41, 481-487. (doi:10.
1086/432015)</mixed-citation>
         </ref>
         <ref id="d1605e796a1310">
            <label>25</label>
            <mixed-citation id="d1605e803" publication-type="other">
Walter, N. D., Taylor Jr, T. H., Dowell, S. F., Mathis, S. &amp;
Moore, M. R. 2009 Holiday spikes in pneumococcal
disease among older adults. N Engl. J. Med. 361,
2584-2585. (doi:10.1056/NEJMc0904844)</mixed-citation>
         </ref>
         <ref id="d1605e819a1310">
            <label>26</label>
            <mixed-citation id="d1605e826" publication-type="other">
Pilishvili, T. et al. 2010 Sustained reductions in invasive
pneumococcal disease in the era of conjugate vaccine.
J. Infect. Dis. 201, 32-41. (doi:10.1086/648593)</mixed-citation>
         </ref>
         <ref id="d1605e839a1310">
            <label>27</label>
            <mixed-citation id="d1605e846" publication-type="other">
Hsu, H. E. et al. 2009 Effect of pneumococcal conjugate
vaccine on pneumococcal meningitis. N. Engl. J. Med.
360, 244-256. (doi:10.1056/NEJMoa0800836)</mixed-citation>
         </ref>
         <ref id="d1605e859a1310">
            <label>28</label>
            <mixed-citation id="d1605e866" publication-type="other">
Metersky, M. L., Dransfield, M. T. &amp; Jackson, L. A.
2010 Determining the optimal pneumococcal vaccin-
ation strategy for adults: is there a role for the
pneumococcal conjugate vaccine? Chest 138, 486-490.
(doi:10.1378/chest.10-0738)</mixed-citation>
         </ref>
         <ref id="d1605e886a1310">
            <label>29</label>
            <mixed-citation id="d1605e893" publication-type="other">
Obaro, S. K, Adegbola, R. A., Banya, W. A. &amp;
Greenwood, B. M. 1996 Carriage of pneumococci after
pneumococcal vaccination. Lancet 348, 271-272.
(doi:10.1016/S0140-6736(05)65585-7)</mixed-citation>
         </ref>
         <ref id="d1605e909a1310">
            <label>30</label>
            <mixed-citation id="d1605e916" publication-type="other">
Mbelle, N., Huebner, R. E., Wasas, A. D., Kimura, A.,
Chang, I. &amp; Klugman, K. P. 1999 Immunogenicity and
impact on nasopharyngeal carriage of a nonavalent
pneumococcal conjugate vaccine. J. Infect. Dis. 180,
1171-1176. (doi:10.1086/315009)</mixed-citation>
         </ref>
         <ref id="d1605e935a1310">
            <label>31</label>
            <mixed-citation id="d1605e942" publication-type="other">
Cohen, A. L. et al. 2010 Prevention of invasive pneumo-
coccal disease among HIV-infected adults in the era of
childhood pneumococcal immunization. AIDS 24,
2253-2262. (doi 10.1097/QAD.0b013e32833d46fd)</mixed-citation>
         </ref>
         <ref id="d1605e958a1310">
            <label>32</label>
            <mixed-citation id="d1605e965" publication-type="other">
Singleton, R. J., Hennessy, T. W., Bulkow, L. R.,
Hammitt, L. L., Zulz, T., Hurlburt, D. A., Butler, J. C.,
Rudolph, K. &amp; Parkinson, A. 2007 Invasive pneumo-
coccal disease caused by nonvaccine serotypes among
Alaska native children with high levels of 7-valent
pneumococcal conjugate vaccine coverage. J. Am.
Med. Assoc. 297, 1784-1792. (doi:10.1001/jama.297.
16.1784)</mixed-citation>
         </ref>
         <ref id="d1605e994a1310">
            <label>33</label>
            <mixed-citation id="d1605e1003" publication-type="other">
Melegaro, A., Choi, Y. H., George, R., Edmunds, W. J.,
Miller, E. &amp; Gay, N. J. 2010 Dynamic models of pneu-
mococcal carriage and the impact of the heptavalent
pneumococcal conjugate vaccine on invasive pneumo-
coccal disease. BMC Infect. Dis. 10, 90. (doi:10.1186/
1471-2334-10-90)</mixed-citation>
         </ref>
         <ref id="d1605e1026a1310">
            <label>34</label>
            <mixed-citation id="d1605e1033" publication-type="other">
Gray, B. M., Converse III, G. M. &amp; Dillon Jr, H. C. 1980
Epidemiologic studies of Streptococcus pneumoniae in
infants: acquisition, carriage, and infection during the
first 24 months of life. J. Infect. Dis. 142, 923-933.
(doi:10.1093/infdis/142.6.923)</mixed-citation>
         </ref>
         <ref id="d1605e1053a1310">
            <label>35</label>
            <mixed-citation id="d1605e1060" publication-type="other">
Austrian, R., Douglas, R. M., Schiffman, G., Coetzee, A.
M., Koornhof, H. J., Hayden-Smith, S. &amp; Reid, R. D.
1976 Prevention of pneumococcal pneumonia by vaccin-
ation. Trans. Assoc. Am. Phys. 89, 184-194.</mixed-citation>
         </ref>
         <ref id="d1605e1076a1310">
            <label>36</label>
            <mixed-citation id="d1605e1083" publication-type="other">
O'Brien, K. L. et al. 2007 Effect of pneumococcal conju-
gate vaccine on nasopharyngeal colonization among
immunized and unimmunized children in a commu-
nity-randomized trial. J. Infect. Dis. 196, 1211-1220.
(doi:10.1086/521833)</mixed-citation>
         </ref>
         <ref id="d1605e1102a1310">
            <label>37</label>
            <mixed-citation id="d1605e1109" publication-type="other">
Yang, S. et al. 2005 Quantitative PCR assay using
sputum samples for rapid diagnosis of pneumococcal
pneumonia in adult emergency department patients.
J. Clin. Microbiol. 43, 3221-3226. (doi:10.1128/JCM.
43.7.3221-3226.2005)</mixed-citation>
         </ref>
         <ref id="d1605e1128a1310">
            <label>38</label>
            <mixed-citation id="d1605e1135" publication-type="other">
Vu, H. T. T. et al. 2011 Association between nasophar-
yngeal load of Streptococcus pneumoniae, viral infection,
and radiologically confirmed pneumonia in Vietnamese
children. Pediatr. Infect. Dis. J. 30, 11-18. (doi:10.
1097/INF.0b013e3181f111a2)</mixed-citation>
         </ref>
         <ref id="d1605e1154a1310">
            <label>39</label>
            <mixed-citation id="d1605e1161" publication-type="other">
Feikin, D. R. &amp; Klugman, K. P. 2002 Historical
changes in pneumococcal serogroup distribution:
implications for the era of pneumococcal conjugate
vaccines. Clin. Infect. Dis. 35, 547-555. (doi:10.1086/
341896)</mixed-citation>
         </ref>
         <ref id="d1605e1180a1310">
            <label>40</label>
            <mixed-citation id="d1605e1187" publication-type="other">
Klugman, K. P. &amp; Friedland, I. R. 1995 Antibiotic-
resistant pneumococci in pediatric disease. Microb.
Drug Resist. 1,5-8. (doi:10.1089/mdr.1995.1.5)</mixed-citation>
         </ref>
         <ref id="d1605e1201a1310">
            <label>41</label>
            <mixed-citation id="d1605e1208" publication-type="other">
Kyaw, M. H. et al. 2006 Effect of introduction of the
pneumococcal conjugate vaccine on drug-resistant
Streptococcus pneumoniae. N. Engl. J. Med. 354,
1455-1463. (doi:10.1056/NEJMoa051642)</mixed-citation>
         </ref>
         <ref id="d1605e1224a1310">
            <label>42</label>
            <mixed-citation id="d1605e1231" publication-type="other">
Stephens, D. S. et al. 2005 Incidence of macrolide resist-
ance in Streptococcus pneumoniae after introduction of the
pneumococcal conjugate vaccine: population-based
assessment. Lancet 365, 855-863. (doi:10.1016/S0140-
6736(05)71043-6)</mixed-citation>
         </ref>
         <ref id="d1605e1250a1310">
            <label>43</label>
            <mixed-citation id="d1605e1257" publication-type="other">
Black, S., Shinefield, H., Baxter, R., Austrian, R., Elvin,
L., Hansen, J., Lewis, E. &amp; Fireman, B. 2006 Impact of
the use of heptavalent pneumococcal conjugate vaccine
on disease epidemiology in children and adults. Vaccine
24(Suppl. 2), S279-S280.</mixed-citation>
         </ref>
         <ref id="d1605e1276a1310">
            <label>44</label>
            <mixed-citation id="d1605e1283" publication-type="other">
Moore, M. R. et al. 2008 Population snapshot of emer-
gent Streptococcus pneumoniae serotype 19A in the
United States, 2005. J. Infect. Dis. 197, 1016-1027.
(doi:10.1086/528996)</mixed-citation>
         </ref>
         <ref id="d1605e1299a1310">
            <label>45</label>
            <mixed-citation id="d1605e1306" publication-type="other">
Gertz Jr, R. E., Li, Z., Pimenta, F. C., Jackson, D.,
Juni, B. A., Lynfield, R., Jorgensen, J. H., Carvalho
Mda, G. &amp; Beall, B. W. 2010 Increased penicillin
nonsusceptibility of nonvaccine-serotype invasive
pneumococci other than serotypes 19A and 6A in post-
7-valent conjugate vaccine era. J. Infect. Dis. 201,
770-775.</mixed-citation>
         </ref>
         <ref id="d1605e1332a1310">
            <label>46</label>
            <mixed-citation id="d1605e1339" publication-type="other">
van Gils, E. J. et al. 2010 Pneumococcal conjugate vaccin-
ation and nasopharyngeal acquisition of pneumococcal
serotype 19A strains. J. Am. Med. Assoc. 304, 1099-
1106. (doi:10.1001/jama.2010.1290)</mixed-citation>
         </ref>
         <ref id="d1605e1356a1310">
            <label>47</label>
            <mixed-citation id="d1605e1363" publication-type="other">
Madhi, S. A. &amp; Klugman, K. P. 2004 A role for Strepto-
coccus pneumoniae in virus-associated pneumonia. Nat.
Med. 10, 811-813. (doi:10.1038/nm1077)</mixed-citation>
         </ref>
         <ref id="d1605e1376a1310">
            <label>48</label>
            <mixed-citation id="d1605e1385" publication-type="other">
Madhi, S. A., Ludewick, H., Kuwanda, L., Niekerk,
N., Cutland, C., Little, T. &amp; Klugman, K. P. 2006
Pneumococcal coinfection with human
metapneumovirus. J. Infect. Dis. 193, 1236-1243.
(doi:10.1086/503053)</mixed-citation>
         </ref>
         <ref id="d1605e1404a1310">
            <label>49</label>
            <mixed-citation id="d1605e1411" publication-type="other">
Moore, D. P., Klugman, K. P. &amp; Madhi, S. A. 2010 Role
of Streptococcus pneumoniae in hospitalisation for acute
community-acquired pneumonia associated with cul-
ture-confirmed Mycobacterium tuberculosis in children: a
pneumococcal conjugate vaccine probe study. Pediatr.
Infect. Dis. J. 29, 1099-1104. (doi:10.1097/INF.0b013
e3181eaefff)</mixed-citation>
         </ref>
         <ref id="d1605e1437a1310">
            <label>50</label>
            <mixed-citation id="d1605e1444" publication-type="other">
Berendt, R. F., Long, G. G. &amp; Walker, J. S. 1975
Influenza alone and in sequence with pneumonia due
to Streptococcus pneumoniae in the squirrel monkey.
J. Infect. Dis. 132, 689-693. (doi:10.1093/infdis/132.6.
689)</mixed-citation>
         </ref>
         <ref id="d1605e1463a1310">
            <label>51</label>
            <mixed-citation id="d1605e1470" publication-type="other">
McCullers, J. A. &amp; Rehg, J. E. 2002 Lethal synergism
between influenza virus and Streptococcus pneumoniae:
characterization of a mouse model and the role of
platelet-activating factor receptor. J. Infect. Dis. 186,
341-350. (doi:10.1086/341462)</mixed-citation>
         </ref>
         <ref id="d1605e1489a1310">
            <label>52</label>
            <mixed-citation id="d1605e1496" publication-type="other">
Sun, K. &amp; Metzger, D. W. 2008 Inhibition of pulmonary
antibacterial defense by interferon-gamma during recov-
ery from influenza infection. Nat. Med. 14, 558-564.
(doi:10.1038/nm1765)</mixed-citation>
         </ref>
         <ref id="d1605e1513a1310">
            <label>53</label>
            <mixed-citation id="d1605e1520" publication-type="other">
Shahangian, A., Chow, E. K., Tian, X., Kang, J. R.,
Ghaffari, A., Liu, S. Y., Belperio, J. A., Cheng, G. &amp;
Deng, J. C. 2009 Type I IFNs mediate development
of postinfluenza bacterial pneumonia in mice.
J. Clin. Invest. 119, 1910-1920. (doi:10.1172/JCI
35412)</mixed-citation>
         </ref>
         <ref id="d1605e1543a1310">
            <label>54</label>
            <mixed-citation id="d1605e1550" publication-type="other">
McCullers, J. A., McAuley, J. L., Browall, S., Iverson,
A. R., Boyd, K. L. &amp; Henriques Normark, B.
2010 Influenza enhances susceptibility to natural acqui-
sition of and disease due to Streptococcus pneumoniae
in ferrets. J. Infect. Dis. 202, 1287-1295. (doi:10.1086/
656333)</mixed-citation>
         </ref>
         <ref id="d1605e1573a1310">
            <label>55</label>
            <mixed-citation id="d1605e1580" publication-type="other">
Abdullahi, O., Nyiro, J., Lewa, P., Slack, M. &amp; Scott,
J. A. 2008 The descriptive epidemiology of Streptococcus
pneumoniae and Haemophilus influenzae nasopharyngeal
carriage in children and adults in Kilifi district, Kenya.
Pediatr. Infect. Dis. J. 27, 59-64. (doi:10.1097/INF.
0b013e31814da70c)</mixed-citation>
         </ref>
         <ref id="d1605e1603a1310">
            <label>56</label>
            <mixed-citation id="d1605e1610" publication-type="other">
Brimblecombe, F. S., Cruickshank, R., Masters, P. L.,
Reid, D. D. &amp; Stewart, G. T. 1958 Family studies
of respiratory infections. Br. Med. J. 1, 119-128.
(doi:10.1136/bmj.1.5063.119)</mixed-citation>
         </ref>
         <ref id="d1605e1626a1310">
            <label>57</label>
            <mixed-citation id="d1605e1633" publication-type="other">
Gwaltney Jr, J. M., Sande, M. A., Austrian, R. &amp;
Hendley, J. O. 1975 Spread of Streptococcus pneumoniae
in families. II. Relation of transfer of 5. pneumoniae to
incidence of colds and serum antibody. J. Infect. Dis.
132, 62-68. (doi:10.1093/infdis/132.1.62)</mixed-citation>
         </ref>
         <ref id="d1605e1652a1310">
            <label>58</label>
            <mixed-citation id="d1605e1659" publication-type="other">
Morens, D. M., Taubenberger, J. K. &amp; Fauci, A. S. 2008
Predominant role of bacterial pneumonia as a cause of
death in pandemic influenza: implications for pandemic
influenza preparedness. J. Infect. Dis. 198, 962-970.
(doi:10.1086/591708)</mixed-citation>
         </ref>
         <ref id="d1605e1679a1310">
            <label>59</label>
            <mixed-citation id="d1605e1686" publication-type="other">
Chien, Y. W„ Klugman, K. P &amp; Morens, D. M. 2009
Bacterial pathogens and death during the 1918 influenza
pandemic. N. Engl. J. Med. 361, 2582-2583. (doi:10.
1056/NEJMc0908216)</mixed-citation>
         </ref>
         <ref id="d1605e1702a1310">
            <label>60</label>
            <mixed-citation id="d1605e1709" publication-type="other">
Chien, Y. W., Klugman, K. P &amp; Morens, D. M. 2010
Efficacy of whole-cell killed bacterial vaccines in prevent-
ing pneumonia and death during the 1918 influenza
pandemic. J. Infect. Dis. 202, 1639-1648. (doi:10.
1086/657144)</mixed-citation>
         </ref>
         <ref id="d1605e1728a1310">
            <label>61</label>
            <mixed-citation id="d1605e1735" publication-type="other">
Chintu, C. et al. 2002 Lung diseases at necropsy in
African children dying from respiratory illnesses: a
descriptive necropsy study. Lancet 360, 985-990.
(doi:10.1016/S0140-6736(02)11082-8)</mixed-citation>
         </ref>
         <ref id="d1605e1751a1310">
            <label>62</label>
            <mixed-citation id="d1605e1760" publication-type="other">
Jeena, P M., Pillay, P, Pillay, T. &amp; Coovadia, H. M. 2002
Impact of HIV-1 co-infection on presentation and
hospital-related mortality in children with culture proven
pulmonary tuberculosis in Durban, South Africa.
Int. J. Tuberc. Lung Dis. 6, 672-678.</mixed-citation>
         </ref>
         <ref id="d1605e1779a1310">
            <label>63</label>
            <mixed-citation id="d1605e1786" publication-type="other">
Johnson, H. L., Deloria-Knoll, M., Levine, O. S.,
Stoszek, S. K., Freimanis Hance, L., Reithinger, R.,
Muenz, L. R. &amp; O'Brien, K. L. 2010 Systematic evalu-
ation of serotypes causing invasive pneumococcal
disease among children under five: the pneumococcal
global serotype project. PLoS Med. 7, e1000348.
(doi:10.1371/journal.pmed.1000348)</mixed-citation>
         </ref>
         <ref id="d1605e1812a1310">
            <label>64</label>
            <mixed-citation id="d1605e1819" publication-type="other">
Lu, Y. J. et al. 2010 Options for inactivation, adjuvant,
and route of topical administration of a killed,
unencapsulated pneumococcal whole-cell vaccine. Clin.
Vaccine Immunol. 17, 1005-1012. (doi:10.1128/CVI.
00036-10)</mixed-citation>
         </ref>
         <ref id="d1605e1839a1310">
            <label>65</label>
            <mixed-citation id="d1605e1846" publication-type="other">
Lu, Y. J. et al. 2010 GMP-grade pneumococcal whole-
cell vaccine injected subcutaneously protects mice
from nasopharyngeal colonization and fatal aspiration-
sepsis. Vaccine 28, 7468-7475. (doi:10.1016/j.vaccine.
2010.09.031)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
