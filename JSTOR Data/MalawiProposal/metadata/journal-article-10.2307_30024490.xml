<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">publchoi</journal-id>
         <journal-id journal-id-type="jstor">j50000024</journal-id>
         <journal-title-group>
            <journal-title>Public Choice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00485829</issn>
         <issn pub-type="epub">15737101</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30024490</article-id>
         <title-group>
            <article-title>Colonial Legacies and Economic Growth</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Robin M.</given-names>
                  <surname>Grier</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">98</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3/4</issue>
         <issue-id>i30024482</issue-id>
         <fpage>317</fpage>
         <lpage>335</lpage>
         <permissions>
            <copyright-statement>Copyright 1999 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30024490"/>
         <abstract>
            <p>Much of the work on colonialism has been theoretical or anecdotal. In this paper, I close the gap between the literature on development and new growth theory by testing the effect of colonization on subsequent growth and development. In a sample of 63 ex-colonial states from 1961-1990, I find that colonies that were held for longer periods of time than other countries tend to perform better, on average, after independence. Finally, I show that the level of education at the time of independence can help to explain much of the development gap between the former British and French colonies in Africa.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1630e159a1310">
            <label>1</label>
            <mixed-citation id="d1630e166" publication-type="other">
Easterlin (1981)</mixed-citation>
            <mixed-citation id="d1630e172" publication-type="other">
Hanson (1989)</mixed-citation>
         </ref>
         <ref id="d1630e179a1310">
            <label>2</label>
            <mixed-citation id="d1630e186" publication-type="other">
Quah (1993a</mixed-citation>
            <mixed-citation id="d1630e192" publication-type="other">
1993b)</mixed-citation>
         </ref>
         <ref id="d1630e199a1310">
            <label>6</label>
            <mixed-citation id="d1630e206" publication-type="other">
Wieschhoff (1944)</mixed-citation>
            <mixed-citation id="d1630e212" publication-type="other">
Lee (1967)</mixed-citation>
            <mixed-citation id="d1630e218" publication-type="other">
McInnes (1950)</mixed-citation>
            <mixed-citation id="d1630e225" publication-type="other">
Mayhew (1933)</mixed-citation>
         </ref>
         <ref id="d1630e232a1310">
            <label>7</label>
            <mixed-citation id="d1630e241" publication-type="other">
Fetter, 1979: 132</mixed-citation>
         </ref>
         <ref id="d1630e249a1310">
            <label>8</label>
            <mixed-citation id="d1630e256" publication-type="other">
Kelly (1986)</mixed-citation>
         </ref>
         <ref id="d1630e263a1310">
            <label>9</label>
            <mixed-citation id="d1630e270" publication-type="other">
Grier and Tullock (1989)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1630e286a1310">
            <mixed-citation id="d1630e290" publication-type="other">
Barro, R.J. (1991). Economic growth in a cross section of countries. The Quarterly Journal of
Economics 106(2): 407-443.</mixed-citation>
         </ref>
         <ref id="d1630e300a1310">
            <mixed-citation id="d1630e304" publication-type="other">
Bertocchi, A. and Canova, F (1996). Did colonization matter for growth? An empirical explo-
ration into the historical causes of Africa's underdevelopment. Unpublished.</mixed-citation>
         </ref>
         <ref id="d1630e314a1310">
            <mixed-citation id="d1630e318" publication-type="other">
Brading, D.A. (1971). Miners and merchants in Bourbon Mexico: 1763-1810. Cambridge:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1630e328a1310">
            <mixed-citation id="d1630e332" publication-type="other">
Corbett, E.M. (1972). The French presence in black Africa. Washington, DC: Black Orpheus
Press, Inc.</mixed-citation>
         </ref>
         <ref id="d1630e343a1310">
            <mixed-citation id="d1630e347" publication-type="other">
Dowrick, S. and Nguyen, D.-T. (1989). OECD comparative economic growth 1950-85: Catch-
up and convergence. American Economic Review 79: 1010-1030.</mixed-citation>
         </ref>
         <ref id="d1630e357a1310">
            <mixed-citation id="d1630e361" publication-type="other">
Easterlin, R.A. (1981). Why isn't the whole world developed? Journal of Economic History.
1: 1-19.</mixed-citation>
         </ref>
         <ref id="d1630e371a1310">
            <mixed-citation id="d1630e375" publication-type="other">
Fetter, B. (1979). Colonial rule in Africa: Readings from primary sources. Madison: The
University of Wisconsin Press.</mixed-citation>
         </ref>
         <ref id="d1630e385a1310">
            <mixed-citation id="d1630e389" publication-type="other">
Fieldhouse, D.K. (1966). The colonial empires from the 18th century. New York: Dell Pub-
lishing.</mixed-citation>
         </ref>
         <ref id="d1630e399a1310">
            <mixed-citation id="d1630e403" publication-type="other">
Friedman, M. (1977). Inflation and unemployment. Journal of Political Economy 85: 451-472.</mixed-citation>
         </ref>
         <ref id="d1630e410a1310">
            <mixed-citation id="d1630e414" publication-type="other">
Friedman, M. (1992). Do old fallacies ever die? Journal of Economic Literature 30: 2129-2132.</mixed-citation>
         </ref>
         <ref id="d1630e422a1310">
            <mixed-citation id="d1630e426" publication-type="other">
Grier, K.B. and Tullock, G. (1989). An empirical analysis of cross-national economic growth,
1951-80. Journal of Monetary Economics 24: 259-276.</mixed-citation>
         </ref>
         <ref id="d1630e436a1310">
            <mixed-citation id="d1630e440" publication-type="other">
Grier, R. (1997). The effect of religion on economic development: A cross national study of
63 former colonies. Kyklos (vol. 50, no.1, 47-62).</mixed-citation>
         </ref>
         <ref id="d1630e450a1310">
            <mixed-citation id="d1630e454" publication-type="other">
Hanson, J.R. (1989). Education, economic development, and technology transfer: A colonial
test. Journal of Economic History 4: 939-957.</mixed-citation>
         </ref>
         <ref id="d1630e464a1310">
            <mixed-citation id="d1630e468" publication-type="other">
Harrison, L. (1985). Underdevelopment is a state of mind: The Latin American case. Cam-
bridge, MA: Harvard University Press, and University Press of America, Lanham, MD.</mixed-citation>
         </ref>
         <ref id="d1630e478a1310">
            <mixed-citation id="d1630e482" publication-type="other">
Hayek, F.A. (1944). The road of serfdom. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1630e489a1310">
            <mixed-citation id="d1630e493" publication-type="other">
Kelly, G.P. (1986). Learning to be marginal: Schooling in interwar French West Africa. Journal
of Asian and African Studies 21: 171-184.</mixed-citation>
         </ref>
         <ref id="d1630e504a1310">
            <mixed-citation id="d1630e508" publication-type="other">
Keyfitz, N. and Flieger, W.C. (1990). World population growth and aging: Demographic trends
in the late twentieth century. Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1630e518a1310">
            <mixed-citation id="d1630e522" publication-type="other">
Kormendi, R.C. and Meguire, P.G. (1985). Macroeconomic determinants of growth: Cross-
country evidence. Journal of Monetary Economics 16: 141-163.</mixed-citation>
         </ref>
         <ref id="d1630e532a1310">
            <mixed-citation id="d1630e536" publication-type="other">
Lee, J.M. (1967). Colonial development and good government. Oxford: Clarendon Press.</mixed-citation>
         </ref>
         <ref id="d1630e543a1310">
            <mixed-citation id="d1630e547" publication-type="other">
Levi, M. and Makin, J.C. (1980). Inflation uncertainty and the Phillips curve: Some empirical
evidence. American Economic Review 70: 1022-1027.</mixed-citation>
         </ref>
         <ref id="d1630e557a1310">
            <mixed-citation id="d1630e561" publication-type="other">
Mayhew, A. (1933). Contemporary survey of educational aims and methods in India and
Africa. Africa 6: 172-186.</mixed-citation>
         </ref>
         <ref id="d1630e571a1310">
            <mixed-citation id="d1630e575" publication-type="other">
McInnes, C.M. (Ed.). (1950). Principles and methods of colonial administration. London:
Butterworths.</mixed-citation>
         </ref>
         <ref id="d1630e586a1310">
            <mixed-citation id="d1630e590" publication-type="other">
Mitchell, B.R. (1982). International historical statistics: Africa and Asia. New York: New
York University Press.</mixed-citation>
         </ref>
         <ref id="d1630e600a1310">
            <mixed-citation id="d1630e604" publication-type="other">
Mullineaux, D. (1980). Unemployment, industrial production and inflation uncertainty. Review
of Economics and Statistics 62: 163-168.</mixed-citation>
         </ref>
         <ref id="d1630e614a1310">
            <mixed-citation id="d1630e618" publication-type="other">
Quah, D. (1993a). Empirical cross-section dynamics in economic growth. European Economic
Review 37: 426-434.</mixed-citation>
         </ref>
         <ref id="d1630e628a1310">
            <mixed-citation id="d1630e632" publication-type="other">
Quah, D. (1993b). Galton's fallacy and tests of the convergence hypothesis. Scandinavian
Journal of Economics 95: 427-443.</mixed-citation>
         </ref>
         <ref id="d1630e642a1310">
            <mixed-citation id="d1630e646" publication-type="other">
Rebelo, S. (1991). Long-run policy analysis and long-run growth. Journal of Political Economy
99: 500-521.</mixed-citation>
         </ref>
         <ref id="d1630e656a1310">
            <mixed-citation id="d1630e660" publication-type="other">
Romer, P. (1987). Crazy explanations for the productivity slowdown. In S. Fischer (Ed.), NBER
macroeconomics annual, 163-202. Cambridge, Massachusets: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1630e671a1310">
            <mixed-citation id="d1630e675" publication-type="other">
Summers, R. and Heston, A. (1991). The Penn World Table (Mark 5). An Expanded Set of
International Comparisons, 1950-1988, Quarterly Journal of Economics 106(2): 327-368.</mixed-citation>
         </ref>
         <ref id="d1630e685a1310">
            <mixed-citation id="d1630e689" publication-type="other">
von der Mehden, FR. (1969). Politics of the developing nation. Englewood Cliffs: Prentice-
Hall.</mixed-citation>
         </ref>
         <ref id="d1630e699a1310">
            <mixed-citation id="d1630e703" publication-type="other">
Wieschhoff, H.A. (1944). Colonial policies in Africa. Westport, CT: Negro Universities Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
