<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socisciequar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50010312</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Social Science Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Texas Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00384941</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15406237</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42864398</article-id>
         <article-categories>
            <subj-group>
               <subject>Of General Interest</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Perilous Proxy: Human Rights and the Presence of National Elections</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David L.</given-names>
                  <surname>Richards</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>1999</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">80</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40108903</issue-id>
         <fpage>648</fpage>
         <lpage>665</lpage>
         <permissions>
            <copyright-statement>© 1999 University of Texas Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42864398"/>
         <abstract>
            <p>Objective. Studies have found that democracy is associated with increased government respect for human rights, but we are not sure exactly why. The purpose of this research is to examine the effect that the presence of national elections has on government respect for a category of human rights known as physical integrity rights. Methods. This study uses a random-effects GLS model to analyze a pooled cross-sectional time-series data set containing information about seventy-four countries from 1981 to 1987. Results. It is found that the presence of national elections, either executive or legislative, has no effect on government respect for human rights. Conclusions. This finding suggests that policy makers need to be cognizant of the differences between strict democracy and liberal democracy, as they are likely to be promoting one, and expecting the benefits of the other. The consequences of doing so may be disastrous in the context of what Zakaria (1997) calls "illiberal democracies."</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d232e132a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d232e139" publication-type="other">
Kim and Mueller,
1978:14-17</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e148" publication-type="other">
Kim and
Mueller (1978).</mixed-citation>
            </p>
         </fn>
         <fn id="d232e158a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d232e165" publication-type="other">
Lenski (1966:319)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e171" publication-type="other">
Crewe (1981:217)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e177" publication-type="other">
William Riker (1965:35)</mixed-citation>
            </p>
         </fn>
         <fn id="d232e184a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d232e191" publication-type="other">
Friedrich (1982)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e197" publication-type="other">
Jaccard, Turrisi, and Wan (1990),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e203" publication-type="other">
Friedrich, 1982:803</mixed-citation>
            </p>
         </fn>
         <fn id="d232e210a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d232e217" publication-type="other">
Poe and Tate's (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e223" publication-type="other">
http://www.polsci.binghamton.edu/hr.htm.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d232e229" publication-type="other">
Poe and
Tate (1994).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d232e248a1310">
            <mixed-citation id="d232e252" publication-type="other">
Bollen, Kenneth. 1992. "Political Rights and Political Liberties in Nations: An Evaluation of
Human Rights Measures, 1950-1984." Pp. 188-215 in Thomas B. Jabine and Richard P.
Claude, eds., Human Rights and Statistics: Getting the Record Straight. Philadelphia:
University of Pennsylvania Press.</mixed-citation>
         </ref>
         <ref id="d232e268a1310">
            <mixed-citation id="d232e272" publication-type="other">
---. 1993. "Liberal Democracy: Validity and Method Factors in Cross-National
Measures." American Journal of Political Science 37:1207-30.</mixed-citation>
         </ref>
         <ref id="d232e282a1310">
            <mixed-citation id="d232e286" publication-type="other">
Carleton, David, and Michael Stohl. 1985. "The Foreign Policy of Human Rights: Rhetoric
and Reality from Jimmy Carter to Ronald Reagan." Human Rights Quarterly 7:205-29.</mixed-citation>
         </ref>
         <ref id="d232e296a1310">
            <mixed-citation id="d232e300" publication-type="other">
---. 1987. "The Role of Human Rights in U.S. Foreign Assistance Policy." American
Journal of Political Science 31:1002-18.</mixed-citation>
         </ref>
         <ref id="d232e311a1310">
            <mixed-citation id="d232e315" publication-type="other">
Crewe, Ivor. 1981. "Electoral Participation." Pp. 216-63 in David Butler, Howard R.
Penniman, and Austin Ranney, eds., Democracy at the Polls: A Comparative Study of
Competitive National Elections. Washington, D.C.: American Enterprise Institute for Public
Policy Research.</mixed-citation>
         </ref>
         <ref id="d232e331a1310">
            <mixed-citation id="d232e335" publication-type="other">
Dahl, Robert. 1989. Democracy and Its Critics. New Haven, Conn.: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d232e342a1310">
            <mixed-citation id="d232e346" publication-type="other">
Dalton, Russell J. 1988. Citizen Politics in Western Democracies. Chatham, N.J.: Chatham
House.</mixed-citation>
         </ref>
         <ref id="d232e356a1310">
            <mixed-citation id="d232e360" publication-type="other">
Davenport, Christian. 1991. "The Dynamics of State Repression: A Cross-National Time-
Series Examination of Domestic Political Processes." Ph.D. diss., State University of New
York at Binghamton.</mixed-citation>
         </ref>
         <ref id="d232e373a1310">
            <mixed-citation id="d232e377" publication-type="other">
---. 1995. "The Ballot and the Bullet: An Empirical Assessment of the Relationship
between National Elections and State Repression." Presented at the American Political
Science Association Annual Meeting, Chicago.</mixed-citation>
         </ref>
         <ref id="d232e390a1310">
            <mixed-citation id="d232e394" publication-type="other">
Davidson, Russell, and James G. Mackinnon. 1993. Estimation and Inference in Econo-
metrics. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d232e405a1310">
            <mixed-citation id="d232e409" publication-type="other">
Diamond, Larry. 1996. "Is the Third Wave Over?" Journal of Democracy 7:20-37.</mixed-citation>
         </ref>
         <ref id="d232e416a1310">
            <mixed-citation id="d232e420" publication-type="other">
Dogan, Mattei. 1987. "Romania, 1919-1938." Pp. 369-89 in Myron Wiener and Ergun
Ozbudun, eds., Competitive Elections in Developing Countries. Durham, N.C.: Duke
University Press.</mixed-citation>
         </ref>
         <ref id="d232e433a1310">
            <mixed-citation id="d232e437" publication-type="other">
Dworkin, Ronald. 1977. Taking Rights Seriously. Cambridge, Mass.: Harvard University
Press.</mixed-citation>
         </ref>
         <ref id="d232e447a1310">
            <mixed-citation id="d232e451" publication-type="other">
Farer, Tom J. 1989. "Elections, Democracy, and Human Rights: Toward Union." Human
Rights Quarterly 11:504-21.</mixed-citation>
         </ref>
         <ref id="d232e461a1310">
            <mixed-citation id="d232e465" publication-type="other">
Fein, Helen. 1995. "More Murder in the Middle: Life Integrity Violations and Democracy in
the World, 1987." Human Rights Quarterly 17:170-91.</mixed-citation>
         </ref>
         <ref id="d232e475a1310">
            <mixed-citation id="d232e479" publication-type="other">
Friedrich, Robert J. 1982. "In Defense of Multiplicative Terms in Multiple Regression
Equations." American Journal of Political Science 26:797-833.</mixed-citation>
         </ref>
         <ref id="d232e490a1310">
            <mixed-citation id="d232e494" publication-type="other">
Garvin, Ian, ed. 1989. Elections since 1945: A Worldwide Reference Compendium. Chicago:
St. James Press.</mixed-citation>
         </ref>
         <ref id="d232e504a1310">
            <mixed-citation id="d232e508" publication-type="other">
Gibney, Mark, and Matthew Dalton. 1996. "The Political Terror Scale." Pp. 73-84 in David L.
Cingranelli, ed., Human Rights and Developing Countries. Greenwich, Conn.: JAI Press.</mixed-citation>
         </ref>
         <ref id="d232e518a1310">
            <mixed-citation id="d232e522" publication-type="other">
Ginsberg, Benjamin, and Alan Stone, eds. 1986. Do Elections Matter? Armonk, N.Y.: M. E.
Sharpe.</mixed-citation>
         </ref>
         <ref id="d232e532a1310">
            <mixed-citation id="d232e536" publication-type="other">
Henderson, Conway. 1991. "Conditions Affecting the Use of Political Repression." Journal
of Conflict Resolution 35:120-42.</mixed-citation>
         </ref>
         <ref id="d232e546a1310">
            <mixed-citation id="d232e550" publication-type="other">
---. 1993. "Population Pressures and Political Repression." Social Science Quarterly
74:322-33.</mixed-citation>
         </ref>
         <ref id="d232e560a1310">
            <mixed-citation id="d232e564" publication-type="other">
Herman, Edward, and Frank Brodhead. 1984. Demonstration Elections: U.S.-Staged Elec-
tions in the Dominican Republic, Vietnam, and El Salvador. Boston: South End Press.</mixed-citation>
         </ref>
         <ref id="d232e575a1310">
            <mixed-citation id="d232e579" publication-type="other">
Hofferbert, Richard I., and David L. Cingranelli. 1996. "Democratic Institutions and Human
Rights." Pp. 145-59 in David L. Cingranelli, ed., Human Rights and Developing Countries.
Greenwich, Conn.: JAI Press.</mixed-citation>
         </ref>
         <ref id="d232e592a1310">
            <mixed-citation id="d232e596" publication-type="other">
Huntington, Samuel P. 1997. "After Twenty Years: The Future of the Third Wave." Journal
of Democracy 8:3-12.</mixed-citation>
         </ref>
         <ref id="d232e606a1310">
            <mixed-citation id="d232e610" publication-type="other">
International Commission of Jurists. 1978. Human Rights in a One-Party State. London:
Search Press.</mixed-citation>
         </ref>
         <ref id="d232e620a1310">
            <mixed-citation id="d232e624" publication-type="other">
Jaccard, James, Robert Turrisi, and Choi K. Wan. 1990. Interaction Effects in Multiple
Regression. Newbury Park, Calif.: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d232e634a1310">
            <mixed-citation id="d232e638" publication-type="other">
Karl, Terry Lynn. 1990. "Dilemmas of Democratization in Latin America." Comparative
Politics 23:1-21.</mixed-citation>
         </ref>
         <ref id="d232e648a1310">
            <mixed-citation id="d232e652" publication-type="other">
Kim, Jae-On, and Charles W. Mueller. 1978. Factor Analysis: Statistical Methods and
Practical Issues. Beverly Hills, Calif.: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d232e663a1310">
            <mixed-citation id="d232e667" publication-type="other">
Kupchan, Charles A. 1998. "Democracy First." Foreign Affairs 77:122-25.</mixed-citation>
         </ref>
         <ref id="d232e674a1310">
            <mixed-citation id="d232e678" publication-type="other">
Lawyer's Committee for Human Rights. 1987. The Reagan Administration's Record on
Human Rights in 1987. New York: Human Rights Watch.</mixed-citation>
         </ref>
         <ref id="d232e688a1310">
            <mixed-citation id="d232e692" publication-type="other">
Lenski, Gerhard E., 1966. Power and Privilege. Chapel Hill: The University of North
Carolina Press.</mixed-citation>
         </ref>
         <ref id="d232e702a1310">
            <mixed-citation id="d232e706" publication-type="other">
Lijphart, Arend. 1993. "Constitutional Choices for New Democracies." Pp. 146-58 in Larry
Diamond and Marc F. Plattner, eds., The Global Resurgence of Democracy. Baltimore: The
Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d232e719a1310">
            <mixed-citation id="d232e723" publication-type="other">
Lipset, Seymour. 1963. Political Man: The Social Bases of Politics. New York: Anchor Books.</mixed-citation>
         </ref>
         <ref id="d232e730a1310">
            <mixed-citation id="d232e734" publication-type="other">
Mackie, Thomas T., and Richard Rose. 1991. The International Almanac of Electoral
History. Basingstoke, England: Macmillan.</mixed-citation>
         </ref>
         <ref id="d232e745a1310">
            <mixed-citation id="d232e749" publication-type="other">
McCann, James A., and Mark Gibney. 1996. "An Overview of Political Terror in the De-
veloping World." Pp. 15-27 in David L. Cingranelli, ed., Human Rights and Developing
Countries. Greenwich, Conn.: JAI Press.</mixed-citation>
         </ref>
         <ref id="d232e762a1310">
            <mixed-citation id="d232e766" publication-type="other">
McColm, R. Bruce., ed. 1991. Freedom in the World: Political Rights and Civil Liberties
1990-1991. New York: Freedom House.</mixed-citation>
         </ref>
         <ref id="d232e776a1310">
            <mixed-citation id="d232e780" publication-type="other">
McCormick, James M., and Neil J. Mitchell. 1997. "Human Rights Violations, Umbrella
Concepts, and Empirical Analysis." World Politics 49:510-25.</mixed-citation>
         </ref>
         <ref id="d232e790a1310">
            <mixed-citation id="d232e794" publication-type="other">
Mill, John Stuart. [1861] 1991. "Considerations on Representative Government." In On
Liberty and Other Essays. Oxford, England: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d232e804a1310">
            <mixed-citation id="d232e808" publication-type="other">
Mitchell, Neil J., and James M. McCormick. 1988. "Is U.S. Aid Really Linked to Human
Rights in Latin America?" American Journal of Political Science 32:476-98.</mixed-citation>
         </ref>
         <ref id="d232e818a1310">
            <mixed-citation id="d232e822" publication-type="other">
Monshipouri, Mahmood. 1995. Democratization, Liberalization &amp; Human Rights in the
Third World. Boulder, Colo.: Lynne Rienner.</mixed-citation>
         </ref>
         <ref id="d232e833a1310">
            <mixed-citation id="d232e837" publication-type="other">
Mueller, John. 1992. "Democracy and Ralph's Pretty Good Grocery: Elections, Equality, and
Minimal Human Being." American Journal of Political Science 36:983-1003.</mixed-citation>
         </ref>
         <ref id="d232e847a1310">
            <mixed-citation id="d232e851" publication-type="other">
Pilon, Juliana Geran. 1998. "Election Realities." Foreign Affairs 77:125-26.</mixed-citation>
         </ref>
         <ref id="d232e858a1310">
            <mixed-citation id="d232e862" publication-type="other">
Poe, Steven C. 1991. "Human Rights and the Allocation of U.S. Military Assistance." Journal
of Peace Research 28:205-16.</mixed-citation>
         </ref>
         <ref id="d232e872a1310">
            <mixed-citation id="d232e876" publication-type="other">
---. 1992. "Human Rights and Economic Aid under Ronald Reagan and Jimmy Carter."
American Journal of Political Science 36:147-67.</mixed-citation>
         </ref>
         <ref id="d232e886a1310">
            <mixed-citation id="d232e890" publication-type="other">
Poe, Steven C., and C. Neal Tate. 1994. "Repression of Human Rights to Personal Integrity
in the 1980s: A Global Analysis." American Political Science Review 88:853-72.</mixed-citation>
         </ref>
         <ref id="d232e900a1310">
            <mixed-citation id="d232e904" publication-type="other">
Poe, Steven C., C. Neal Tate, and Linda Camp Keith. 1997. "Repression of Human Rights to
Personal Integrity Revisited." Presented at the Annual Meeting of the International Studies
Association, Toronto, Canada, March 18-22.</mixed-citation>
         </ref>
         <ref id="d232e918a1310">
            <mixed-citation id="d232e922" publication-type="other">
Powell, G. Bingham, Jr. 1982. Contemporary Democracies. Cambridge, Mass.: Harvard
University Press.</mixed-citation>
         </ref>
         <ref id="d232e932a1310">
            <mixed-citation id="d232e936" publication-type="other">
Rawls, John. 1971. A Theory of Justice. Cambridge, Mass.: The Belknap Press of Harvard
University Press.</mixed-citation>
         </ref>
         <ref id="d232e946a1310">
            <mixed-citation id="d232e950" publication-type="other">
Riker, William. 1965. Democracy in the United States, Second Edition. New York:
MacMillan.</mixed-citation>
         </ref>
         <ref id="d232e960a1310">
            <mixed-citation id="d232e964" publication-type="other">
Russett, Bruce. 1993. Grasping the Democratic Peace. Princeton, N.J.: Princeton University
Press.</mixed-citation>
         </ref>
         <ref id="d232e974a1310">
            <mixed-citation id="d232e978" publication-type="other">
Schumpeter, Joseph A. 1950. Capitalism, Socialism, and Democracy. New York: Harper and
Row.</mixed-citation>
         </ref>
         <ref id="d232e988a1310">
            <mixed-citation id="d232e992" publication-type="other">
Small, Melvin, and J. David Singer. 1982. Resort to Arms: International and Civil Wars,
1816-1980. Beverly Hills, Calif.: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d232e1003a1310">
            <mixed-citation id="d232e1007" publication-type="other">
Spirer, Herbert. 1990. "Violations of Human Rights—How Many?" American Journal of
Economics and Sociology 49:199-204.</mixed-citation>
         </ref>
         <ref id="d232e1017a1310">
            <mixed-citation id="d232e1021" publication-type="other">
Steiner, Henry. 1988. "Political Participation as a Human Right." In United Nations, ed.,
Harvard Human Rights Yearbook. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d232e1031a1310">
            <mixed-citation id="d232e1035" publication-type="other">
Stiefel, Matthias, and Marshall Wolfe. 1994. A Voice for the Excluded: Popular Participation
in Development. London: Zed Books.</mixed-citation>
         </ref>
         <ref id="d232e1045a1310">
            <mixed-citation id="d232e1049" publication-type="other">
Stimson, James. 1985. "Regression in Time and Space: A Statistical Essay." American Journal
of Political Science 29:914-47.</mixed-citation>
         </ref>
         <ref id="d232e1059a1310">
            <mixed-citation id="d232e1063" publication-type="other">
Stohl, Michael, David Carleton, and Steven E. Johnson. 1984. "Human Rights and U.S.
Foreign Assistance: From Nixon to Carter." Journal of Peace Research 21:215-26.</mixed-citation>
         </ref>
         <ref id="d232e1073a1310">
            <mixed-citation id="d232e1077" publication-type="other">
Stohl, Michael, David Carleton, George Lopez, and Stephen Samuels. 1986. "State Violation
of Human Rights: Issues and Problems of Measurement." Human Rights Quarterly
8:592-606.</mixed-citation>
         </ref>
         <ref id="d232e1091a1310">
            <mixed-citation id="d232e1095" publication-type="other">
USOCA. 1985. Democracy in Nicaragua: An Eyewitness Report on the 1984 Election and
Popular Democracy in Nicaragua. San Francisco: U.S. Out of Central America.</mixed-citation>
         </ref>
         <ref id="d232e1105a1310">
            <mixed-citation id="d232e1109" publication-type="other">
Verba, Sidney. 1978. Participation and Political Equality. New York: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d232e1119a1310">
            <mixed-citation id="d232e1123" publication-type="other">
Verba, Sidney, Norman H. Nie, and Jae-On Kim. 1971. The Modes of Democratic
Participation: A Cross National Comparison. Beverly Hills, Calif.: Sage Publications.</mixed-citation>
         </ref>
         <ref id="d232e1133a1310">
            <mixed-citation id="d232e1137" publication-type="other">
Wright, Theodore P., 1964. American Support of Free Elections Abroad. Washington, D.C.:
Public Affairs Press.</mixed-citation>
         </ref>
         <ref id="d232e1147a1310">
            <mixed-citation id="d232e1151" publication-type="other">
Zakaria, Fareed. 1997. "The Rise of Illiberal Democracy." Foreign Affairs 76:22-43.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
