<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt12615c</book-id>
      <subj-group>
         <subject content-type="call-number">HJ7980.L58 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Appropriations and expenditures</subject>
            <subj-group>
               <subject content-type="lcsh">Evaluation</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Government spending policy</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Government accountability</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Lives in the Balance</book-title>
         <subtitle>Improving Accountability for Public Spending in Developing Countries</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>GRIFFIN</surname>
               <given-names>CHARLES C.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>DE FERRANTI</surname>
               <given-names>DAVID</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib3">
            <name name-style="western">
               <surname>TOLMIE</surname>
               <given-names>COURTNEY</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib4">
            <name name-style="western">
               <surname>JACINTO</surname>
               <given-names>JUSTIN</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib5">
            <name name-style="western">
               <surname>RAMSHAW</surname>
               <given-names>GRAEME</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib6">
            <name name-style="western">
               <surname>BUN</surname>
               <given-names>CHINYERE</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>08</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="epub">9780815701774</isbn>
      <isbn content-type="epub">0815701772</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>WASHINGTON, DC</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>Results for Development Institute</copyright-holder>
         <copyright-holder>The Brookings Institution</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt12615c"/>
      <abstract abstract-type="short">
         <p>Because of its potential impact, and, in some cases, the harm it has brought, foreign aid is under the microscope. Donor countries, who don't want simply to give money away; recipient nations, who need to make the most of what they have and get; and analysts, policymakers, and writers are all scrutinizing how much is spent and where it goes. Perhaps more important, aid is only a small part of what developing country governments spend. Their own resources finance 80 percent or more of health and education spending except in the most aid-dependent countries.<italic>Lives in the Balance</italic>investigates a vital aspect of this landscape -how best to ensure that public spending, including aid money, gets to the right destination.</p>
         <p>The development of democratic institutions and the spread of cheap communications technology in developing countries make it possible for the "demand-side" -citizens and civil society institutions -to advocate for improved transparency, stronger accountability, better priorities, reduced corruption, and more emphasis on helping the poor. Securing real reform will depend not only on knowledge of how the recipient government operates, but also how to work with partner entities -the media, the private sector, other organizations, and legislators -to raise awareness and compel change.</p>
      </abstract>
      <counts>
         <page-count count="174"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.3</book-part-id>
                  <title-group>
                     <title>Preface—a story about this book</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.5</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>The process of government accountability—an anecdote and an agenda</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In a study of the effectiveness of education spending programs, the National Center for Economic Research (CIEN) in Guatemala looked at why increased school spending was not being converted into improved education results. Only half of school-age Guatemalan children complete primary school, and reading and math skills are at dismally low levels. Several problems immediately became clear as researchers surveyed parents and primary school students and teachers. Some 62 percent of head teachers reported that textbooks had not arrived in time for the start of the 2008 school year, disrupting student learning. And 73 percent of school boards surveyed reported</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.6</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Major issues and tools in public expenditure management</title>
                  </title-group>
                  <fpage>18</fpage>
                  <abstract>
                     <p>This chapter summarizes the main issues in public expenditure in low- and middle-income countries based on roughly 60 public expenditure reviews conducted by the World Bank between 2000 and 2007. The World Bank has been conducting public expenditure reviews for low- and middle-income countries since the 1980s, analyzing the level and pattern of public expenditure, assessing the effectiveness and equity of public spending, and identifying bottlenecks to improved spending effectiveness. The reviews have provided valuable insights to guide policy dialogue and have frequently resulted in tangible fiscal reform in many countries.</p>
                     <p>A recurring theme in public expenditure reviews is the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.7</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>The political dimension of public accountability</title>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract>
                     <p>Chapter 2 provided a diagnosis of public expenditure and financial management in developing countries, identifying progress and challenges. Despite substantial investment of time and resources by the international community to support improvements in budget management, public expenditure and financial management in many developing countries remains generally disappointing, especially at the subnational level.</p>
                     <p>This is not surprising. Securing public accountability is not only a technical challenge, but also a political one. In a political system with no effective demand for transparency and accountability, efforts to promote improved techniques are unlikely to succeed. But in systems where there is demand for improved</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.8</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Transparency and accountability in budgets and expenditure management</title>
                  </title-group>
                  <fpage>66</fpage>
                  <abstract>
                     <p>Chapter 2 suggests that national budgeting and expenditure management have fallen short of best practices but that the push to decentralize decisions to increase accountability to citizens could be risky under current control environments. On the positive side, chapter 3 argues that the democracy movement that has swept the world over the past two decades and the spread of cheap, more widely available information technology could transform the capability of citizens and domestic civil society institutions to hold governments more accountable for expenditure decisions and budget execution. Clearly, their role could be important not only at the central level but</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.9</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>Independent monitoring organizations at work</title>
                  </title-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>Citizens face significant challenges in trying to monitor government actions, as the Open Budget Initiative data reported in chapter 4 demonstrate. But the emergence of more open societies argues for a complementary form of monitoring by civil society organizations that we have called independent monitoring organizations. Although bottom-up accountability in developing countries is still in its infancy, a framework for accountability, such as the one presented in this book, with government entities acting as agents of citizen-principals, is nonetheless possible.</p>
                     <p>Independent monitoring organizations have the potential to become agents in making bottom-up accountability work, by turning the free flow of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.10</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>Strengthening independent monitoring organizations</title>
                  </title-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>Chapter 5 gave examples of how independent monitoring organizations can play a role in developing policy and improving government programs and operations. When people organize to improve public services, they can have a positive impact—lowering costs for street lights, ensuring delivery of textbooks to schools, or improving service delivery through citizen report cards.¹ Apart from the problem of information availability from governments, independent monitoring organizations face stiff organizational, financing, and development challenges in conducting research and advocating for change.</p>
                     <p>Within the development community it is widely believed that outside financing and capacity-building assistance can help independent monitoring organizations overcome</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.11</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Conclusion—bringing everyone to the same page</title>
                  </title-group>
                  <fpage>136</fpage>
                  <abstract>
                     <p>This final chapter looks at what all the key players—donors, governments, independent monitoring organizations—can do to strengthen independent monitoring organizations and increase their impact. But first a little background and some additional insight into the story presented in the preface.</p>
                     <p>The preface tells a story about this book. It shows what the numbers in chapter 2 look like on the ground and describes a dream of how civil society and independent monitoring organizations might improve the situation. This section reflects on what we have learned about that story in the intervening pages of this book.¹</p>
                     <p>For the World</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.12</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>147</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>157</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12615c.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>175</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
