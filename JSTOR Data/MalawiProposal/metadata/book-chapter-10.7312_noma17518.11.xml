<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt125wrc</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">noma17518</book-id>
      <subj-group>
         <subject content-type="call-number">HD3616.A3513153 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Industrial policy</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Industrial promotion</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Economic policy</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Learning</subject>
         <subj-group>
            <subject content-type="lcsh">Economic aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Industrial Policy and Economic Transformation in Africa</book-title>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">EDITED BY</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Noman</surname>
               <given-names>Akbar</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Stiglitz</surname>
               <given-names>Joseph E.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>09</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9780231540773</isbn>
      <isbn content-type="epub">0231540779</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/noma17518"/>
      <abstract abstract-type="short">
         <p>The revival of economic growth in Sub-Saharan Africa is all the more welcome for having followed one of the worst economic disasters--a quarter century of economic malaise for most of the region--since the industrial revolution. Six of the world's fastest-growing economies in the first decade of this century were African. Yet only in Ethiopia and Rwanda was growth not based on resources and the rising price of oil. Deindustrialization has yet to be reversed, and progress toward creating a modern economy remains limited.</p>
         <p>This book explores the vital role that active government policies can play in transforming African economies. Such policies pertain not just to industry. They traverse all economic sectors, including finance, information technology, and agriculture. These packages of learning, industrial, and technology (LIT) policies aim to bring vigorous and lasting growth to the region. This collection features case studies of LIT policies in action in many parts of the world, examining their risks and rewards and what they mean for Sub-Saharan Africa.</p>
      </abstract>
      <counts>
         <page-count count="328"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>I</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>VII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.3</book-part-id>
                  <title-group>
                     <title>ACRONYMS</title>
                  </title-group>
                  <fpage>IX</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>XIII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction and Overview:</title>
                     <subtitle>ECONOMIC TRANSFORMATION AND LEARNING, INDUSTRIAL, AND TECHNOLOGY POLICIES IN AFRICA</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Noman</surname>
                           <given-names>Akbar</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stiglitz</surname>
                           <given-names>Joseph E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The revival of reasonably rapid growth in Sub-Saharan Africa of about 5 percent per year for over a decade is all the more welcome for having followed a “lost quarter century”: per capita income for the region, which had started falling toward the end of the 1970s, did not recover to its previous peak level until after the turn of the century. This is an impressive turnaround from what was arguably the longest and deepest economic decline anywhere. In the 2000s, six of the world’s fastest-growing economies were in Sub-Saharan Africa (which we refer to simply as Africa): over about</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Is Industrial Policy Necessary and Feasible in Africa?:</title>
                     <subtitle>THEORETICAL CONSIDERATIONS AND HISTORICAL LESSONS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <string-name>Ha-Joon Chang</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>30</fpage>
                  <abstract>
                     <p>Industrial policy has been one of the most controversial issues in economics, especially in development economics (for a review of the industrial policy debate since the 1980s, see Chang 2011). Particularly surrounding its role in the development success of East Asia, there was a fierce debate that came to a head in the late 1980s and the early 1990s (Amsden 1989; Wade 1990; World Bank 1987, 1991, and 1993; Stiglitz 1996).</p>
                     <p>Fortunately, during the last decade or so there have been a number of developments in academia and in the real world that have made industrial policy more acceptable and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Industrial Strategy and Economic Transformation:</title>
                     <subtitle>LESSONS FROM FIVE OUTSTANDING CASES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hosono</surname>
                           <given-names>Akio</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>Industrial policy and economic transformation have been attracting renewed attention of late. Thus several studies in the past decade or so have focused on industrial development, especially industrial structure upgrading and diversification, as a basis for sustained economic growth and development.</p>
                     <p>These studies have emphasized such aspects as the accumulation of knowledge and capabilities and the creation of a learning society (Cimoli, Dosi, and Stiglitz 2009; Greenwald and Stiglitz 2012); the exploiting and changing factor endowments and comparative advantage (Lin 2012); the need to compensate for the information externalities generated by pioneer firms (Rodrik 2007); and pragmatic policymaking for developing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>The Economic Implications of a Comprehensive Approach to Learning on Industrial Policy:</title>
                     <subtitle>THE CASE OF ETHIOPIA</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shimada</surname>
                           <given-names>Go</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>102</fpage>
                  <abstract>
                     <p>Notwithstanding the much-improved economic performance of Africa in the past decade or so, industrial development continues to languish. The percentage of the GDP emanating from the manufacturing sector has been declining since the 1980s. Recent economic growth is dominated by the mining sector. Foreign direct investment (FDI) goes toward natural resources and not the manufacturing sector. As the population grows, youth unemployment will become a serious issue for sustainable growth and political stability in the region. Therefore industrial development that contributes to increases in employment and income can be crucial.</p>
                     <p>There has been heated debate over industrial policy elsewhere and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Review of Industrial Policies in Ethiopia:</title>
                     <subtitle>A PERSPECTIVE FROM THE LEATHER AND CUT FLOWER INDUSTRIES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Abebe</surname>
                           <given-names>Girum</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Schaefer</surname>
                           <given-names>Florian</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>123</fpage>
                  <abstract>
                     <p>As early as the 1950s the Nobel Laureate economist Arthur Lewis (1955, quoted in Lin and Chang 2009) exclaimed, “No country has made economic progress without positive stimulus from intelligent government.” The appropriate role for government in the economy has been a source of heated debate among academics and policymakers ever since. While the private sector is widely viewed as a driver of growth, there appears to have been little consensus on the role and ability of government to push the private sector toward promoting growth and development, let alone on whether such interventions are necessary for industrial takeoff. Proponents</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>The Return of Industrial Policy:</title>
                     <subtitle>(WHAT) CAN AFRICA LEARN FROM LATIN AMERICA?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Primi</surname>
                           <given-names>Annalisa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>162</fpage>
                  <abstract>
                     <p>Africa is growing. The development debate in Africa has shifted from how to overcome poverty in a low-growth continent to how to profit from the high-growth momentum. The rise of China and its demand for raw materials have contributed, to a large extent, to the boost in growth and to the rise of dynamism in African markets. Poverty has been decreasing, access to information technologies has increased in most of the countries of the region, and new partners have emerged in trade and investment. Media, on their side, are playing their part portraying Africa as the next “booming” continent, with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Can the Financial Sector Deliver Both Growth and Financial Stability in Sub-Saharan Africa?</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Griffith-Jones</surname>
                           <given-names>Stephany</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <role>with</role>
                        <name name-style="western">
                           <surname>Karwowski</surname>
                           <given-names>Ewa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>197</fpage>
                  <abstract>
                     <p>Finance provides a particularly challenging area for policy design and research, especially if placed in the context of those countries’ needs for development. The policy challenges and research needs are very large, due in part to a major rethinking of the role, scale, and structure of a desirable financial sector, as well as its regulation, in light of the North Atlantic financial crisis that started in 2007/2008. This major crisis also challenged the view that developed countries’ financial and regulatory systems should be emulated by developing countries, given that developed countries’ financial systems have been so problematic and so poorly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Growth Strategies for Africa in a Changing Global Environment:</title>
                     <subtitle>POLICY OBSERVATIONS FOR SUSTAINABLE AND SHARED GROWTH</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Leipziger</surname>
                           <given-names>Danny</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Yusuf</surname>
                           <given-names>Shahid</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>230</fpage>
                  <abstract>
                     <p>Following decolonization in the 1950s and the 1960s, growth accelerated in several of the leading African economies with the flowering of long-delayed industrial activity, the modernization of physical infrastructure, and the quickening of urbanization. However, by the mid-1970s this initial phase of catching up had run out of steam as countries fell prey to political turbulence, internal and cross-border conflicts, and economic distortions introduced by policy mismanagement and import substituting industrialization. Problems internal to the continent were exacerbated by the weakening performance of Africa’s main trading partners in the West. African countries entered a long economic twilight that extended through</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Measuring Policy Performance:</title>
                     <subtitle>CAN WE DO BETTER THAN THE WORLD BANK?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Cagé</surname>
                           <given-names>Julia</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>268</fpage>
                  <abstract>
                     <p>This article questions the relevance of the different measures of policy performance that are currently used by international organizations to allocate Official Development Assistance (ODA). It evaluates more especially the pertinence of the World Bank’s Country Policy and Institutional Assessment (CPIA) and of the various alternatives that have been proposed in the literature. It suggests a new way of assessing aid effectiveness. Measuring policy performance is of particular importance: ODA represents a limited but needed resource for developing countries. Finding criteria to allocate it selectively is thus of great concern for donors.</p>
                     <p>Using a yearly panel dataset of over 146</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.14</book-part-id>
                  <title-group>
                     <title>ABOUT THE EDITORS</title>
                  </title-group>
                  <fpage>293</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.15</book-part-id>
                  <title-group>
                     <title>ABOUT THE AUTHORS</title>
                  </title-group>
                  <fpage>295</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">noma17518.16</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>299</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
