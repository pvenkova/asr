<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">inteorga</journal-id>
         <journal-id journal-id-type="jstor">j100191</journal-id>
         <journal-title-group>
            <journal-title>International Organization</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Wisconsin Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00208183</issn>
         <issn pub-type="epub">15315088</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2706478</article-id>
         <title-group>
            <article-title>The Impact of Food Aid on World Malnutrition</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>James N.</given-names>
                  <surname>Schubert</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>1981</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i346120</issue-id>
         <fpage>329</fpage>
         <lpage>354</lpage>
         <page-range>329-354</page-range>
         <permissions>
            <copyright-statement>Copyright 1981 Board of Regents of the University of Wisconsin System</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2706478"/>
         <abstract>
            <p>Does food aid enhance or diminish the nutritional status of recipient populations in less developed countries? In proposing that the long-term impact is negative, critics have argued that aid depresses local food production, is maldistributed and mismanaged such that it does not reach the needy in sufficient quantities, or, where effective, that aid merely reduces the death rate relative to the birth rate, permitting more people to survive at the margin of existence. This study explores the long-term impact of U.S. Public Law 480 food aid through a crossnational analysis of aggregate data on aid receipts and change in nutritional status over the period from 1962 through 1974. Alternative hypotheses are tested through least squares methods and mean difference tests in the framework of a nonequivalent control group, quasi-experimental design. This study supports the following generalizations: food aid is significantly related with improved nutritional status; the greater the aid, the greater the improvement in nutrition; higher aid recipients do not have significantly lower rates of growth in domestic food production; higher aid recipients do not have higher rates of population growth; and food aid may lead to greater meat consumption among higher aid recipients. Negative effects, experienced in some countries at some times, are not systematically incurred by all food aid recipients over time. In general, food aid does improve nutrition.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1051e120a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1051e127" publication-type="book">
Pearson Commission report, Partners in Development (New York:
Praeger, 1969)<person-group>
                     <string-name>
                        <surname>Pearson Commission</surname>
                     </string-name>
                  </person-group>
                  <source>Partners in Development</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e151" publication-type="book">
D. Goulet and
M. Hudson in The Myth of Aid (New York: IODC N. America, 1971).<person-group>
                     <string-name>
                        <surname>Goulet</surname>
                     </string-name>
                  </person-group>
                  <source>The Myth of Aid</source>
                  <year>1971</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e176a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1051e183" publication-type="book">
T. Mende, From Aid To Recolonization (New York: Pantheon, 1972).<person-group>
                     <string-name>
                        <surname>Mende</surname>
                     </string-name>
                  </person-group>
                  <source>From Aid To Recolonization</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e205a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1051e212" publication-type="book">
M. Hudson's discussion of the cost of food aid to the U.S. in Super Imperialism: the
Economic Strategy of American Empire (New York: Holt, Rinehart, and Winston, 1972).<person-group>
                     <string-name>
                        <surname>Hudson</surname>
                     </string-name>
                  </person-group>
                  <source>Super Imperialism: the Economic Strategy of American Empire</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e237a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1051e244" publication-type="book">
L. Witt, "Food aid, commercial exports, and the balance of payments," in Food Policy: The
Responsibility of the United States in the Life and Death Choices, ed. by P. G. Brown and H.
Shue (New York: Free Press, 1977).<person-group>
                     <string-name>
                        <surname>Witt</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Food aid, commercial exports, and the balance of payments</comment>
                  <source>Food Policy: The Responsibility of the United States in the Life and Death Choices</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e277a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1051e286" publication-type="journal">
R. F. Hopkins and D. J. Puchala, "Perspectives on the international relations of
food," International Organization32,3 (1978).<person-group>
                     <string-name>
                        <surname>Hopkins</surname>
                     </string-name>
                  </person-group>
                  <issue>3</issue>
                  <volume>32</volume>
                  <source>International Organization</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e318a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1051e325" publication-type="book">
Witt, "Food Aid."  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e333" publication-type="book">
E. R. Wittkoph's Western
Bilateral Aid Allocations: A Comparative Study of Recipient State Attributes and Aid Received
(Beverly Hills: Sage, 1972).<person-group>
                     <string-name>
                        <surname>Wittkoph</surname>
                     </string-name>
                  </person-group>
                  <source>Western Bilateral Aid Allocations: A Comparative Study of Recipient State Attributes and Aid Received</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e361" publication-type="journal">
E. T. Rowe's "National attributes associated
with multilateral and U.S. bilateral aid to Latin America: 1960-197 1," International Organization
32,2 (1978).<object-id pub-id-type="jstor">10.2307/2706272</object-id>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e377a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1051e384" publication-type="book">
E. P. Prentice's Hunger and History: The Influence of Hunger on Human
History (New York: Harper and Brothers, 1939).<person-group>
                     <string-name>
                        <surname>Prentice</surname>
                     </string-name>
                  </person-group>
                  <source>Hunger and History: The Influence of Hunger on Human History</source>
                  <year>1939</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e409a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1051e416" publication-type="journal">
T. W. Schultz, "Value of U.S. farm surpluses to underdeveloped countries," Journal of Farm
Economics42 (1960).<object-id pub-id-type="doi">10.2307/1235653</object-id>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e428" publication-type="book">
C. B. Shuman's "Food aid and the free market," in Food Policy,
pp. 153-56.  <fpage>153</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e443a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1051e450" publication-type="book">
Theodore J. Goering and Lawrence Witt, United States Agricultural Surpluses in Colombia, A
Review of P.L. 480, Technical Bulletin 289 (East Lansing: Michigan State University, 1963).<person-group>
                     <string-name>
                        <surname>Goering</surname>
                     </string-name>
                  </person-group>
                  <source>United States Agricultural Surpluses in Colombia, A Review of P.L. 480</source>
                  <year>1963</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e475a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1051e482" publication-type="book">
Resat Aktan, Analysis and Assessment of the Economic Effects of Public Law 480 Title I
Program, Turkey (Ankara: University of Ankara, 1964).<person-group>
                     <string-name>
                        <surname>Aktan</surname>
                     </string-name>
                  </person-group>
                  <source>Analysis and Assessment of the Economic Effects of Public Law 480 Title I Program, Turkey</source>
                  <year>1964</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e508a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1051e515" publication-type="book">
U. K. Srivastava et al. Food Aid and International Economic Growth (Ames: Iowa State
University Press, 1975).<person-group>
                     <string-name>
                        <surname>Srivastava</surname>
                     </string-name>
                  </person-group>
                  <source>Food Aid and International Economic Growth</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e540a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1051e547" publication-type="book">
Goering and Witt, U.S. Agricultural Surpluses in Colombia.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e556a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1051e563" publication-type="book">
Nilakantha Rath and V. S. Patvardhan, Impact of Assistance under P.L. 480 on Indian
Economy, Gokhale Institute of Economics and Politics (Poona: Asia Publishing House, 1967).<person-group>
                     <string-name>
                        <surname>Rath</surname>
                     </string-name>
                  </person-group>
                  <source>Impact of Assistance under P.L. 480 on Indian Economy</source>
                  <year>1967</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e588a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1051e595" publication-type="book">
Haven D. Umstott, Public Law 480 and Other Economic Assistance to United Arab Republic
(Egypt), USDA, ERS Foreign Report 83 (June 1964).<person-group>
                     <string-name>
                        <surname>Umstott</surname>
                     </string-name>
                  </person-group>
                  <source>Public Law 480 and Other Economic Assistance to United Arab Republic (Egypt)</source>
                  <year>1964</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e620a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1051e627" publication-type="partial">
Shuman, "Food aid and free market,"</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e633" publication-type="journal">
C. Christensen, "World hunger: a
structural approach," International Organization32,3 (1978).<object-id pub-id-type="jstor">10.2307/2706332</object-id>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e646a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1051e653" publication-type="book">
W. Ophuls, Ecology and the Politics of Scarcity
(San Francisco: Freeman, 1977).<person-group>
                     <string-name>
                        <surname>Ophuls</surname>
                     </string-name>
                  </person-group>
                  <source>Ecology and the Politics of Scarcity</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e679a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1051e686" publication-type="book">
M. V. Krause and L. K. Manan, Food, Nutrition, and Diet
Therapy (Philadelphia: Saunders, 1979)<person-group>
                     <string-name>
                        <surname>Krause</surname>
                     </string-name>
                  </person-group>
                  <source>Food, Nutrition, and Diet Therapy</source>
                  <year>1979</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e710" publication-type="book">
N. S. Scrimshaw and J. C. Gordon, eds., Malnutrition,
Learning, and Behavior (Cambridge, Mass.: MIT Press, 1968)<person-group>
                     <string-name>
                        <surname>Scrimshaw</surname>
                     </string-name>
                  </person-group>
                  <source>Malnutrition, Learning, and Behavior</source>
                  <year>1968</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1051e734" publication-type="book">
D. A. Levitsky, ed., Malnutrition,
Environment, and Behavior (Ithaca, N.Y.: Cornell University Press, 1979).<person-group>
                     <string-name>
                        <surname>Levitsky</surname>
                     </string-name>
                  </person-group>
                  <source>Malnutrition, Environment, and Behavior</source>
                  <year>1979</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e759a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1051e766" publication-type="journal">
E. Rothschild's discussion in "Food for Peace," New York Times
Magazine, 13 March1977, p. 15.<person-group>
                     <string-name>
                        <surname>Rothschild</surname>
                     </string-name>
                  </person-group>
                  <issue>13 March</issue>
                  <fpage>15</fpage>
                  <source>New York Times Magazine</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e798a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1051e805" publication-type="book">
Srivastava et al., Food Aid and Growth.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e814a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1051e821" publication-type="book">
H. Mitchell et al., Nutrition in Health and Disease, 16th ed. (Philadelphia: Lippincott, 1976).<person-group>
                     <string-name>
                        <surname>Mitchell</surname>
                     </string-name>
                  </person-group>
                  <edition>16</edition>
                  <source>Nutrition in Health and Disease</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e846a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1051e853" publication-type="book">
United Nations, The Protein Crisis, U.N. Publication No. E/5018/Rev. 1, 1971.<person-group>
                     <string-name>
                        <surname>United Nations</surname>
                     </string-name>
                  </person-group>
                  <source>The Protein Crisis</source>
                  <year>1971</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e875a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1051e882" publication-type="book">
S. Reutlinger and M. Selowsky, Malnutrition and Poverty: Magnitude and Policy Options
(Baltimore: Johns Hopkins University Press, 1976).<person-group>
                     <string-name>
                        <surname>Reutlinger</surname>
                     </string-name>
                  </person-group>
                  <source>Malnutrition and Poverty: Magnitude and Policy Options</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e908a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1051e915" publication-type="book">
The Fourth World Food Survey (Rome: FAO, 1977), pp. 127-28.<fpage>127</fpage>
                  <source>The Fourth World Food Survey</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e931a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1051e938" publication-type="book">
Mitchell et al., Nutrition in Health and Disease.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e947a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1051e954" publication-type="book">
National Health and Nutrition Examination Survey: Plan and Operation, National
Center for Health Statistics, U.S. Dept. of H.E.W., 1977.<source>National Health and Nutrition Examination Survey: Plan and Operation</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e970a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1051e977" publication-type="book">
Reutlinger and Selowsky, Malnutrition and Poverty.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e986a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1051e993" publication-type="book">
Production Yearbook31 (Rome: FAO, 1977).<fpage>31</fpage>
                  <source>Production Yearbook</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1009a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1051e1016" publication-type="book">
R. G. Stanely, Food for Peace: Hope and Reality of U.S. Food Aid (London: Gordon and
Breach, 1972).<person-group>
                     <string-name>
                        <surname>Stanely</surname>
                     </string-name>
                  </person-group>
                  <source>Food for Peace: Hope and Reality of U.S. Food Aid</source>
                  <year>1972</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1042a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1051e1049" publication-type="book">
United Nations, Statistical Yearbook, 1977.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1058a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1051e1065" publication-type="book">
N. C. Nie et al., Statistical Package for the Social Sciences (New York: McGraw-Hill, 1975).<person-group>
                     <string-name>
                        <surname>Nie</surname>
                     </string-name>
                  </person-group>
                  <source>Statistical Package for the Social Sciences</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1087a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1051e1094" publication-type="book">
Reutlinger and
Selowsky study, Malnutrition and Poverty  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1106a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1051e1113" publication-type="conference">
S. Aziz, "The world food situation: today, and in the year 2000," Proceedings of the World
Food Conference of 1976 (Ames: Iowa State University Press, 1977).<person-group>
                     <string-name>
                        <surname>Aziz</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The world food situation: today, and in the year 2000</comment>
                  <source>Proceedings of the World Food Conference of 1976</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1142a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1051e1149" publication-type="journal">
R. B. Stauffer, "The biopolitics of underdevelopment,'4 Comparative Political Studies2
(1969).<person-group>
                     <string-name>
                        <surname>Stauffer</surname>
                     </string-name>
                  </person-group>
                  <volume>2</volume>
                  <source>Comparative Political Studies</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1178a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1051e1185" publication-type="book">
U.N., The Protein Crisis.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1195a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1051e1202" publication-type="book">
A. Berg, The Nutrition Factor: Its Role in National Development (Washington, D.C.:
Brookings, 1973).<person-group>
                     <string-name>
                        <surname>Berg</surname>
                     </string-name>
                  </person-group>
                  <source>The Nutrition Factor: Its Role in National Development</source>
                  <year>1973</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1051e1227a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d1051e1234" publication-type="book">
Food and Nutrition Policy in a Changing
World, ed. by J. Mayer and J. Dwyer (New York: Oxford University Press, 1979).<source>Food and Nutrition Policy in a Changing World</source>
                  <year>1979</year>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
