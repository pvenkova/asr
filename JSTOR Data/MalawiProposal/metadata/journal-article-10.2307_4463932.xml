<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4463932</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Determinants of Mortality and Nondeath Losses from an Antiretroviral Treatment Service in South Africa: Implications for Program Evaluation</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Stephen D. Lawn</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Landon</given-names>
                  <surname>Myer</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Guy Harling</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Catherine Orrell</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Linda-Gail</given-names>
                  <surname>Bekker</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Robin</given-names>
                  <surname>Wood</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>9</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">43</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id>i402520</issue-id>
         <fpage>770</fpage>
         <lpage>776</lpage>
         <page-range>770-776</page-range>
         <permissions>
            <copyright-statement>Copyright 2006 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4463932"/>
         <abstract>
            <p>Background. The scale-up of antiretroviral treatment (ART) services in resource-limited settings requires a programmatic model to deliver care to large numbers of people. Understanding the determinants of key outcome measures-including death and nondeath losses-would assist in program evaluation and development. Methods. Between September 2002 and August 2005, all in-program (pretreatment and on-treatment) deaths and nondeath losses were prospectively ascertained among treatment-naive adults (n = 1235) who were enrolled in a community-based ART program in South Africa. Results. At study censorship, 927 patients had initiated ART after a median of 34 days after enrollment in the program. One hundred twenty-one (9.8%) patients died. Mortality rates were 33.3 (95% CI, 25.5-43.0), 19.1 (95% CI, 14.4-25.2), and 2.9 (95% CI, 1.8-4.8) deaths/100 person-years in the pretreatment interval, during the first 4 months of ART (early deaths), and after 4 months of ART (late deaths), respectively. Pretreatment and early treatment deaths together accounted for 87% of deaths, and were independently associated with advanced immunodeficiency at enrollment. Late deaths were comparatively few and were only associated with the response to ART at 4 months. Nondeath program losses (loss to follow-up, 2.3%; transfer-out, 1.9%; relocation, 0.7%) were not associated with immune status and were evenly distributed during the study period. Conclusions. Loss to follow-up and late mortality rates were low, reflecting good cohort retention and treatment response. However, the extremely high pretreatment and early mortality rates indicate that patients are enrolling in ART programs with far too advanced immunodeficiency. Causes of late access to the ART program, such as delays in health care access, health system delays, or inappropriate treatment criteria, need to be addressed.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1457e209a1310">
            <label>1</label>
            <mixed-citation id="d1457e216" publication-type="other">
UNAIDS/WHO. AIDS epidemic update: December 2004 Available at:
http://www.unaids.org/epi/2005/doc/report_pdf.asp. Accessed 5 April
2006.</mixed-citation>
         </ref>
         <ref id="d1457e229a1310">
            <label>2</label>
            <mixed-citation id="d1457e236" publication-type="other">
WHO/UNAIDS. Progress on global access to HIV antiretroviral ther-
apy: an update on "3 by 5," June 2005. Available at: http://www.who
.int/hiv/pub/progressreports/3by5%20Progress%20Report_E_light.pdf.
Accessed 19 October 2005.</mixed-citation>
         </ref>
         <ref id="d1457e252a1310">
            <label>3</label>
            <mixed-citation id="d1457e259" publication-type="book">
World Health Organization. Scaling up antiretroviral therapy in re-
source-limited settings: guidelines for a public health approach; ex-
ecutive summary. Geneva: World Health Organization, 2002.<person-group>
                  <string-name>
                     <surname>World Health Organization</surname>
                  </string-name>
               </person-group>
               <source>Scaling up antiretroviral therapy in resource-limited settings: guidelines for a public health approach; executive summary</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e288a1310">
            <label>4</label>
            <mixed-citation id="d1457e295" publication-type="book">
World Health Organization. Scaling up antiretroviral therapy in re-
source-limited settings: guidelines for a public health approach; 2003
revision. Geneva: World Health Organization, 2002. Available at: http:
//www.who.int/3by5/publications/documents/arv_guidelines/en/index
.html. Accessed 15 February 2006.<person-group>
                  <string-name>
                     <surname>World Health Organization</surname>
                  </string-name>
               </person-group>
               <source>Scaling up antiretroviral therapy in resource-limited settings: guidelines for a public health approach; 2003 revision</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e331a1310">
            <label>5</label>
            <mixed-citation id="d1457e338" publication-type="journal">
Braitstein P, Brinkhof MW, Dabis F, et al. Mortality of HIV-1-infected
patients in the first year of antiretroviral therapy: comparison between
low-income and high-income countries. Lancet2006; 367:817-24.<person-group>
                  <string-name>
                     <surname>Braitstein</surname>
                  </string-name>
               </person-group>
               <fpage>817</fpage>
               <volume>367</volume>
               <source>Lancet</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e373a1310">
            <label>6</label>
            <mixed-citation id="d1457e382" publication-type="journal">
Ivers LC, Kendrick D, Doucette K. Efficacy of antiretroviral therapy
programs in resource-poor settings: a meta-analysis of the published
literature. Clin Infect Dis2005; 41:217-24.<person-group>
                  <string-name>
                     <surname>Ivers</surname>
                  </string-name>
               </person-group>
               <fpage>217</fpage>
               <volume>41</volume>
               <source>Clin Infect Dis</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e417a1310">
            <label>7</label>
            <mixed-citation id="d1457e424" publication-type="journal">
Lawn SD, Myer L, Wood R. Efficacy of antiretroviral therapy in re-
source-poor settings: are outcomes comparable to those in the devel-
oped world? Clin Infect Dis2005; 41:1683-4.<person-group>
                  <string-name>
                     <surname>Lawn</surname>
                  </string-name>
               </person-group>
               <fpage>1683</fpage>
               <volume>41</volume>
               <source>Clin Infect Dis</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e459a1310">
            <label>8</label>
            <mixed-citation id="d1457e466" publication-type="book">
Enarson DA, Rieder HL, Arnadottir T, Trebucq A. A guide for low
income countries. 5th ed. Paris: International Union Against Tuber-
culosis and Lung Disease, 2000.<person-group>
                  <string-name>
                     <surname>Enarson</surname>
                  </string-name>
               </person-group>
               <edition>5</edition>
               <source>A guide for low income countries</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e498a1310">
            <label>9</label>
            <mixed-citation id="d1457e505" publication-type="journal">
Lawn SD, Myer L, Orrell C, Bekker LG, Wood R. Early mortality among
adults accessing a community-based antiretroviral service in South
Africa: implications for programme design. AIDS2005; 19:2141-8.<person-group>
                  <string-name>
                     <surname>Lawn</surname>
                  </string-name>
               </person-group>
               <fpage>2141</fpage>
               <volume>19</volume>
               <source>AIDS</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e540a1310">
            <label>10</label>
            <mixed-citation id="d1457e547" publication-type="journal">
Lawn SD, Myer L, Bekker LG, et al. CD4 cell count recovery among
HIV-infected patients with very advanced immunodeficiency com-
mencing antiretroviral treatment in sub-Saharan Africa. BMC Infect
Dis2006; 6:59.<person-group>
                  <string-name>
                     <surname>Lawn</surname>
                  </string-name>
               </person-group>
               <fpage>59</fpage>
               <volume>6</volume>
               <source>BMC Infect Dis</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e586a1310">
            <label>11</label>
            <mixed-citation id="d1457e593" publication-type="journal">
Bekker LG, Myer L, Orrell C, Lawn SD, Wood R. Rapid scale up of a
community-based HIV treatment service: programme performance
over three consecutive years in Guguletu, South Africa. S Afr Med J
2006; 96:315-20.<person-group>
                  <string-name>
                     <surname>Bekker</surname>
                  </string-name>
               </person-group>
               <fpage>315</fpage>
               <volume>96</volume>
               <source>S Afr Med J</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e631a1310">
            <label>12</label>
            <mixed-citation id="d1457e638" publication-type="book">
Orrell C, Badri M, Wood R. Measuring adherence in a community
setting: which measure most valuable [abstract WePEB5787]? In: Pro-
gram and abstracts of the XVI International AIDS Conference (Bang-
kok). 2004.<person-group>
                  <string-name>
                     <surname>Orrell</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Measuring adherence in a community setting: which measure most valuable [abstract WePEB5787]?</comment>
               <source>Program and abstracts of the XVI International AIDS Conference (Bangkok)</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e673a1310">
            <label>13</label>
            <mixed-citation id="d1457e680" publication-type="book">
Wand MP, Jones MC. Kernel smoothing. In: Monographs in statistics
and applied probability. New York: Chapman &amp; Hall, 1995.<person-group>
                  <string-name>
                     <surname>Wand</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Kernel smoothing</comment>
               <source>Monographs in statistics and applied probability</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e709a1310">
            <label>14</label>
            <mixed-citation id="d1457e716" publication-type="journal">
Ng'andu NH. An empirical comparison of statistical tests for assessing
the proportional hazards assumption of Cox's model. Stat Med
1997; 16:611-26.<person-group>
                  <string-name>
                     <surname>Ng'andu</surname>
                  </string-name>
               </person-group>
               <fpage>611</fpage>
               <volume>16</volume>
               <source>Stat Med</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e751a1310">
            <label>15</label>
            <mixed-citation id="d1457e758" publication-type="journal">
Coetzee D, Hildebrand K, Boulle A, et al. Outcomes after two years
of providing antiretroviral treatment in Khayelitsha, South Africa. AIDS
2004; 18:887-95.<person-group>
                  <string-name>
                     <surname>Coetzee</surname>
                  </string-name>
               </person-group>
               <fpage>887</fpage>
               <volume>18</volume>
               <source>AIDS</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e793a1310">
            <label>16</label>
            <mixed-citation id="d1457e800" publication-type="journal">
Laurent C, Ngom Gueye NF, Ndour CT, et al. Long-term benefits of
highly active antiretroviral therapy in Senegalese HIV- 1-infected adults.
J Acquir Immune Defic Syndr2005;38:14-7.<person-group>
                  <string-name>
                     <surname>Laurent</surname>
                  </string-name>
               </person-group>
               <fpage>14</fpage>
               <volume>38</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e836a1310">
            <label>17</label>
            <mixed-citation id="d1457e843" publication-type="journal">
Tassie JM, Szumilin E, Calmy A, Goemaere E. Highly active antiret-
roviral therapy in resource-poor settings: the experience of Medecins
Sans Frontieres. AIDS2003; 17:1995-7.<person-group>
                  <string-name>
                     <surname>Tassie</surname>
                  </string-name>
               </person-group>
               <fpage>1995</fpage>
               <volume>17</volume>
               <source>AIDS</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e878a1310">
            <label>18</label>
            <mixed-citation id="d1457e885" publication-type="journal">
Djomand G, Roels T, Ellerbrock T, et al. Virologic and immunologic
outcomes and programmatic challenges of an antiretroviral treatment
pilot project in Abidjan, Cote d'Ivoire. AIDS2003; 17(Suppl 3):S5-15.<person-group>
                  <string-name>
                     <surname>Djomand</surname>
                  </string-name>
               </person-group>
               <issue>Suppl 3</issue>
               <fpage>S5</fpage>
               <volume>17</volume>
               <source>AIDS</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e923a1310">
            <label>19</label>
            <mixed-citation id="d1457e930" publication-type="journal">
Weidle PJ, Malamba S, Mwebaze R, et al. Assessment of a pilot an-
tiretroviral drug therapy programme in Uganda: patients' response,
survival, and drug resistance. Lancet2002; 360:34-40.<person-group>
                  <string-name>
                     <surname>Weidle</surname>
                  </string-name>
               </person-group>
               <fpage>34</fpage>
               <volume>360</volume>
               <source>Lancet</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e965a1310">
            <label>20</label>
            <mixed-citation id="d1457e972" publication-type="journal">
Lawn SD, Wood R. How can earlier entry of patients into antiretroviral
programs in low-income countries be promoted? Clin Infect Dis
2006; 42:431-2.<person-group>
                  <string-name>
                     <surname>Lawn</surname>
                  </string-name>
               </person-group>
               <fpage>431</fpage>
               <volume>42</volume>
               <source>Clin Infect Dis</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e1007a1310">
            <label>21</label>
            <mixed-citation id="d1457e1014" publication-type="journal">
Myer L, El Sadr W. Expanding access to antiretroviral therapy through
the public sector-the challenge of retaining patients in long-term
primary care. S Afr Med J2004; 94:273-4.<person-group>
                  <string-name>
                     <surname>Myer</surname>
                  </string-name>
               </person-group>
               <fpage>273</fpage>
               <volume>94</volume>
               <source>S Afr Med J</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1457e1049a1310">
            <label>22</label>
            <mixed-citation id="d1457e1056" publication-type="journal">
van Oosterhout JJ, Bodasing N, Kumwenda JJ, et al. Evaluation of
antiretroviral therapy results in a resource-poor setting in Blantyre,
Malawi. Trop Med Int Health2005; 10:464-70.<person-group>
                  <string-name>
                     <surname>van Oosterhout</surname>
                  </string-name>
               </person-group>
               <fpage>464</fpage>
               <volume>10</volume>
               <source>Trop Med Int Health</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
