<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt16314fw</book-id>
      <subj-group>
         <subject content-type="call-number">RA395.A3H432 2006</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Health care reform</subject>
         <subj-group>
            <subject content-type="lcsh">Moral and ethical aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Health care reform</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Medical ethics</subject>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Health Care Reform</subject>
         <subj-group>
            <subject content-type="nlm">ethics</subject>
            <subj-group>
               <subject content-type="nlm">United States</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="nlm">Health Policy</subject>
         <subj-group>
            <subject content-type="nlm">United States</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History of Science &amp; Technology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Health Care Reform</book-title>
         <subtitle>Ethics and Politics</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">EDITED BY</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>ENGSTRÖM</surname>
               <given-names>TIMOTHY H.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>ROBISON</surname>
               <given-names>WADE L.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>02</day>
         <month>10</month>
         <year>2006</year>
      </pub-date>
      <isbn content-type="ppub">9781580462266</isbn>
      <isbn content-type="epub">9781580466653</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Rochester, NY; Woodbridge, Suffolk</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2006</copyright-year>
         <copyright-holder>Timothy H. Engström</copyright-holder>
         <copyright-holder>Wade L. Robison</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt16314fw"/>
      <abstract abstract-type="short">
         <p>Health care reform has been stalled since the Clinton health care initiative, but the political difficulties internal to that initiative and the ethical problems that provoked it -- of cost, coverage, and overall fairness, for example -- have only gotten worse. This collection examines the moral principles that must underlie any new reform initiative and the processes of democratic decision-making essential to successful reform. This volume provides careful analyses that will allow the reader to short-circuit the mythmaking, polemics, and distortions that have too often characterized public discussion of health care reform. Its aim is to provide the moral foundations and institutional arrangements needed to drive any new health care initiative and so to stimulate a reasoned discussion before the next inevitable round of reform efforts. Foreword by Thomas H. Murray. Contributors: Howard Brody, Norman Daniels, Theodore Marmor, Tobie H. Olsan, Uwe E. Reinhardt, Gerd Richter, Rory B. Weiner, Lawrence W. White Wade L. Robison is the Ezra A. Hale Professor in Applied Ethics at the Rochester Institute of Technology and recipient of the Nelson A. Rockefeller Prize for Social Science and Public Policy for his book Decisions in Doubt: The Environment and Public Policy. Timothy H. Engström is Professor of Philosophy at the Rochester Institute of Technology and recipient of the Eisenhart Award for Outstanding Teaching.</p>
      </abstract>
      <counts>
         <page-count count="301"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Murray</surname>
                           <given-names>Thomas H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>Recently, I heard the distinguished physician and political leader, Doctor John Kitzhaber, former governor of Oregon, describe the principles one would need to follow in order to design the health care system we Americans live under. It was a ruefully hilarious exercise. It would take a singularly perverse and insane genius to create the American health care system from scratch. A system that leaves, at this writing, more than 45 million of our fellow citizens uninsured while those with the best policies can call on astonishingly expensive technologies to eke out an extra few days or hours of encumbered existence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.5</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>The Problems of Health Care Reform</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Engström</surname>
                           <given-names>Timothy H.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Robison</surname>
                           <given-names>Wade L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The headlines marking a health care crisis continue to appear with regularity: “Study Ties Bankruptcy to Medical Bills,”¹ “Cost of Insuring Workers’ Health Increases 11.2 %,”² “Retirees Are Paying More for Health Benefits, Study Says.”³ As the headlines suggest, matters are getting worse, not better, and no relief is on the horizon. No economic relief is in sight, no systematic political or legislative process is underway, and yet sound moral analysis should provoke us to do something. It is these three systematically related dimensions of discussion—moral, political, and economic—that concern this present volume.</p>
                     <p>The problems that led to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.6</book-part-id>
                  <title-group>
                     <title>[Part One Introduction]</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>Robison and Brody open the discussion with an analysis of the ethical problems the present health care system unavoidably creates. They argue that we are in a “moral crisis” and that we are thus obligated to address this crisis. We must first analyze its causes, suggest ethical foundations of a good health care system, and maintain our concentration upon the underlying ethical issues in advance of the rush to develop public policy.</p>
                     <p>Robison asks us to look at our health care system the way an objective observer would look at it—as though an alien from a distant planet were</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.7</book-part-id>
                  <title-group>
                     <label>Chapter One</label>
                     <title>The Moral Crisis in Health Care</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Robison</surname>
                           <given-names>Wade L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>13</fpage>
                  <abstract>
                     <p>In the midst of the Clinton administration’s efforts to reform health care, Senator Dole claimed that there was no crisis in health care and that all that was needed is some “fine tuning.”¹ Senator Daniel Patrick Moynihan conceded that on “health care costs, you’ve got a crisis,” but claimed there was no other major problem.² Both those assessments were woefully inadequate to the problems we faced then and would be even more inadequate now.</p>
                     <p>In his<italic>Dialogues Concerning Natural Religion</italic>, David Hume says that</p>
                     <p>Were a stranger to drop, in a sudden, into this world, I would show him, as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.8</book-part-id>
                  <title-group>
                     <label>Chapter Two</label>
                     <title>Ethics, Justice, and Health Reform</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Brody</surname>
                           <given-names>Howard</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>40</fpage>
                  <abstract>
                     <p>The issue of health care reform in the United States is commonly framed as an economic, political, or technical policy issue. It is less often characterized as an ethical or moral issue that necessarily touches upon the basic values of the average American.</p>
                     <p>I argue that health care reform deserves to be treated as fundamentally a moral question and that the failure to conceive of it in this way contributed greatly to the deficiencies in the national debate over health reform in 1993 and 1994. This chapter was originally prepared in 1996, and I relied very heavily for my arguments</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.9</book-part-id>
                  <title-group>
                     <title>[Part Two Introduction]</title>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>The ends the health care system is presently embracing, according to the authors in this section, take their shape essentially from the market-driven forces that have dominated health care since before the failure of the Clinton initiative. Although the ethical dream of an egalitarian health care system may continue to appear in much of our political rhetoric, we have, according to Reinhardt, officially embraced an income-based health system. Talk of “individual responsibility” and “private choice,” he argues, are code words for “rationing health care by income class.”</p>
                     <p>The consequences of the market forces that are likely to determine the next</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.10</book-part-id>
                  <title-group>
                     <label>Chapter Three</label>
                     <title>A Social Contract for Twenty-First Century Health Care:</title>
                     <subtitle>Three-Tier Health Care with Bounty Hunting</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Reinhardt</surname>
                           <given-names>Uwe E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>After the spectacular demise of the Clinton health reform plan in 1994, it was commonly said that health reform in the United States is dead. That is not an accurate assessment. Health reform in the United States is not dead. It is not even half dead. Rather, half of it is totally dead and the other half is very much alive.</p>
                     <p>The part of health reform that died in 1994 is the decades-old American dream that all Americans would one day have comprehensive health insurance and share a single-tier health system that would “remain” simply the “best in the world.”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.11</book-part-id>
                  <title-group>
                     <label>Chapter Four</label>
                     <title>Corporatization of Health Care</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>White</surname>
                           <given-names>Lawrence W.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>99</fpage>
                  <abstract>
                     <p>In the decade prior to the Clinton health care initiative, mergers and acquisitions involving health insurance companies and other for-profit corporations, HMOs, and networks of hospitals and physicians resulted in a dramatic and rapid change in the health care environment in all major populations centers across the country. In this vertical integration of our health care system, doctors were herded into managed care networks, hospitals merged and created large for-profit corporations, and both became increasingly dominated by the private health insurance industry.</p>
                     <p>Blue Cross affiliates were switching to for-profit status, taking over hospitals, and setting up clinics and managed care</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.12</book-part-id>
                  <title-group>
                     <label>Chapter Five</label>
                     <title>“We Can’t Be Nurses Anymore”:</title>
                     <subtitle>The Loss of Community Health Nurses’ Personhood in Market-Driven Health Care</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Olsan</surname>
                           <given-names>Tobie H.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>116</fpage>
                  <abstract>
                     <p>Morality is frequently equated with conduct and character of individuals, but when a group of community health nurses interprets market-driven health care by saying, “We can’t be nurses anymore,” they are sending a warning sign that corporatization is forcefully introducing a contradictory morality into health care at the institutional level. The nurses, whose encounter with corporatization I will describe, worked at a home care agency affiliate of an integrated health system (IHS). An ethnographic analysis of the institutional content of their moral problems more generally offers insight into the moral dimensions of changing values and structures in health care.</p>
                     <p>Less</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.13</book-part-id>
                  <title-group>
                     <title>[Part Three Introduction]</title>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract>
                     <p>Evaluation of our health care system often takes place as though its problems were uniquely ours and as if no one else has developed policies to solve them. Marmor begins this part by evaluating the ways in which comparative information enters, and fails to enter, public policy debate about our health care system and by discussing the economic and ethical principles that animate, or ought to animate, this evaluation. He then considers the reform stalemate in American federal policymaking. One of the strengths of the American federal system is that the fifty states can act with some independence from the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.14</book-part-id>
                  <title-group>
                     <label>Chapter Six</label>
                     <title>Politics of Medical Care Reform in Mature Welfare States:</title>
                     <subtitle>What Are America’s Prospects Now?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Marmor</surname>
                           <given-names>Theodore</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>147</fpage>
                  <abstract>
                     <p>None of us, as Rudolf Klein has rightly noted,¹ can escape the “bombardment of information about what is happening in other countries.” Yet, in the field of comparative studies in health care policy, there is an extraordinary imbalance between the magnitude of the information flows and the capacity to learn useful lessons from them. Indeed, I suspect that the speed of communication<italic>about</italic>developments abroad actually reduces the likelihood of cross-national learning.² Why might that be so? What does that speculation suggest about more promising forms of international intellectual learning in the world of health policy? That is the concern</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.15</book-part-id>
                  <title-group>
                     <label>Chapter Seven</label>
                     <title>Citizens and Customers:</title>
                     <subtitle>Establishing the Ethical Foundations of the German and U.S. Health Care Systems</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Engström</surname>
                           <given-names>Timothy H.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Richter</surname>
                           <given-names>Gerd</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>166</fpage>
                  <abstract>
                     <p>This chapter has five sections: First, we describe the structure of the German health care system. This is done in terms of the system’s ethical foundations and in terms of the social, political, and economic traditions that have institutionalized these foundations in a health care system. Some fundamental points of comparison and contrast between the German and U.S. health care systems are also established. Second, we explore some of the ethical principles that ought to guide our review and reform of health care. This is done by reference to some of the organizational, discursive, and representational structures within the German</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.16</book-part-id>
                  <title-group>
                     <title>[Part Four Introduction]</title>
                  </title-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>If we had the opportunity to create a health care system from scratch, we would be concerned to answer the following three questions:</p>
                     <p>1. What is the system for? What should its purpose, its goals, be?</p>
                     <p>Because no matter what its goals, any health care system will benefit some, by providing medical care, and could harm others, by excluding them from the system, we need to ask ourselves,</p>
                     <p>2. What ethical principles ought to guide our designing a system that will accomplish the system’s goals?</p>
                     <p>Because we can achieve a particular set of goals in a variety of ways, we</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.17</book-part-id>
                  <title-group>
                     <label>Chapter Eight</label>
                     <title>Preparing for the Next Health Care Reform:</title>
                     <subtitle>Notes for an Interim Ethic</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Churchill</surname>
                           <given-names>Larry R.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>195</fpage>
                  <abstract>
                     <p>The fundamental health care issue in the United States is neither excessive costs nor lack of access, but lack of national purpose. We have no consensus about what we want or expect from a health care system. In the absence of such a consensus, entrepreneurial forces remain in ascendancy. Instead of solving the costs and access problems, these forces have shifted costs to others while making access to services even more difficult. Defining a purpose for the health care system means seeking self-consciously to discern its proper goals, rather than assuming that these goals are self-evident or can be safely</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.18</book-part-id>
                  <title-group>
                     <label>Chapter Nine</label>
                     <title>A Cooperative Beneficence Approach to Health Care Reform</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Weiner</surname>
                           <given-names>Rory B.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>209</fpage>
                  <abstract>
                     <p>Over ten years have passed since President Clinton’s failed attempt to reform the United States’ health care system. In its wake, federal and state governments have tried to increase access to health care incrementally by passing a new layer of patchwork quilt programs and regulations,¹ and pinning their hopes on the increased expansion of managed care plans and free-market forces. These strategies have failed miserably. Despite a short-term hiatus in health care inflation and in the uninsured rolls during the economic boom of the late 90s, increases in health care spending and the numbers of uninsured persons has returned with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.19</book-part-id>
                  <title-group>
                     <label>Chapter Ten</label>
                     <title>Fairness and National Health Care Reform</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Daniels</surname>
                           <given-names>Norman</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>240</fpage>
                  <abstract>
                     <p>All of us potentially face exclusion from health insurance because of risk, loss of insurance coverage with job loss or job change, maldistribution or appropriate providers, and inadequate coverage for home care, mental health care, and other services, including drugs. A system that corrected these and other problems we all risk having to confront would go a long way toward ensuring fair treatment. The most effective way to do so would be to enact a comprehensive national health care reform that met key criteria for justice or fairness.</p>
                     <p>But what does a just or fair system require? What criteria of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.20</book-part-id>
                  <title-group>
                     <title>Conclusion:</title>
                     <subtitle>Prospects for Reform</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Engström</surname>
                           <given-names>Timothy H.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Robison</surname>
                           <given-names>Wade L.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>265</fpage>
                  <abstract>
                     <p>It seems a truism in democratic societies that only a crisis of a social institution will lead to fundamental change even when the institution is the source of considerable harm. The vested interests are so strong, the chances through a democratic process of rectifying the institution without causing still further harm are small, and the existing harm is so often to those without much of a voice in the political process. In effect, it is only when a crisis has occurred of such proportions that it cannot be ignored that the body politic is forced to act. The health care</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.21</book-part-id>
                  <title-group>
                     <title>Notes on the Contributors</title>
                  </title-group>
                  <fpage>275</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16314fw.22</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
