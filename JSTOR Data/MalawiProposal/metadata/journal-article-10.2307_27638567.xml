<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jconfreso</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100213</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Journal of Conflict Resolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00220027</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15528766</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27638567</article-id>
         <article-id pub-id-type="pub-doi">10.1177/0022002707303046</article-id>
         <title-group>
            <article-title>Why Do Countries Commit to Human Rights Treaties?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Oona A.</given-names>
                  <surname>Hathaway</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27638563</issue-id>
         <fpage>588</fpage>
         <lpage>621</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 Sage Publications</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27638567"/>
         <abstract>
            <p>This article examines states' decisions to commit to human rights treaties. It argues that the effect of a treaty on a state—and hence the state's willingness to commit to it—is largely determined by the domestic enforcement of the treaty and the treaty's collateral consequences. These broad claims give rise to several specific predictions. For example, states with less democratic institutions will be no less likely to commit to human rights treaties if they have poor human rights records, because there is little prospect that the treaties will be enforced. Conversely, states with more democratic institutions will be less likely to commit to human rights treaties if they have poor human rights records—precisely because treaties are likely to lead to changes in behavior. These predictions are tested by examining the practices of more than 160 countries over several decades.</p>
         </abstract>
         <kwd-group>
            <kwd>international law</kwd>
            <kwd>human rights</kwd>
            <kwd>democracy</kwd>
            <kwd>torture</kwd>
            <kwd>treaties</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d456e172a1310">
            <label>1</label>
            <mixed-citation id="d456e179" publication-type="other">
United Nations Treaty
Series Overview (2003).</mixed-citation>
         </ref>
         <ref id="d456e189a1310">
            <label>2</label>
            <mixed-citation id="d456e196" publication-type="other">
Moravcsik (2000</mixed-citation>
            <mixed-citation id="d456e202" publication-type="other">
2002)</mixed-citation>
            <mixed-citation id="d456e208" publication-type="other">
Finnemore (1996, 69-88)</mixed-citation>
            <mixed-citation id="d456e215" publication-type="other">
Simmons (2000</mixed-citation>
            <mixed-citation id="d456e221" publication-type="other">
2002)</mixed-citation>
            <mixed-citation id="d456e227" publication-type="other">
Vreeland (2000</mixed-citation>
            <mixed-citation id="d456e233" publication-type="other">
2003)</mixed-citation>
            <mixed-citation id="d456e239" publication-type="other">
Hathaway (2002</mixed-citation>
            <mixed-citation id="d456e245" publication-type="other">
2003a</mixed-citation>
            <mixed-citation id="d456e252" publication-type="other">
2003b</mixed-citation>
            <mixed-citation id="d456e258" publication-type="other">
2004</mixed-citation>
            <mixed-citation id="d456e264" publication-type="other">
2005)</mixed-citation>
            <mixed-citation id="d456e270" publication-type="other">
Hathaway and Koh (2004)</mixed-citation>
            <mixed-citation id="d456e276" publication-type="other">
Hathaway and Lavinbuk, (2006)</mixed-citation>
            <mixed-citation id="d456e282" publication-type="other">
von Stein (2005)</mixed-citation>
            <mixed-citation id="d456e289" publication-type="other">
Donnelly (1986)</mixed-citation>
            <mixed-citation id="d456e295" publication-type="other">
Keohane (1983</mixed-citation>
            <mixed-citation id="d456e301" publication-type="other">
1984)</mixed-citation>
            <mixed-citation id="d456e307" publication-type="other">
Kratochwil
and Ruggie (1986)</mixed-citation>
            <mixed-citation id="d456e316" publication-type="other">
Krasner (1983)</mixed-citation>
            <mixed-citation id="d456e322" publication-type="other">
Goldstein and Keohane (1993)</mixed-citation>
            <mixed-citation id="d456e329" publication-type="other">
Koremenos, Lipson, and Snidal
(2001)</mixed-citation>
            <mixed-citation id="d456e338" publication-type="other">
Guzman (2002).</mixed-citation>
         </ref>
         <ref id="d456e345a1310">
            <label>3</label>
            <mixed-citation id="d456e352" publication-type="other">
Hathaway (2002)</mixed-citation>
            <mixed-citation id="d456e358" publication-type="other">
Landman (2005</mixed-citation>
            <mixed-citation id="d456e364" publication-type="other">
2006)</mixed-citation>
            <mixed-citation id="d456e371" publication-type="other">
Hafner-Burton and Tsutsui (2005)</mixed-citation>
            <mixed-citation id="d456e377" publication-type="other">
Hafner-Burton and Tsutsui (Forthcoming)</mixed-citation>
            <mixed-citation id="d456e383" publication-type="other">
Neumayer (2005)</mixed-citation>
            <mixed-citation id="d456e389" publication-type="other">
Camp-
Keith (1999).</mixed-citation>
         </ref>
         <ref id="d456e399a1310">
            <label>4</label>
            <mixed-citation id="d456e406" publication-type="other">
Landman (2005)</mixed-citation>
            <mixed-citation id="d456e412" publication-type="other">
Goodliffe and Hawkins (2006).</mixed-citation>
         </ref>
         <ref id="d456e420a1310">
            <label>5</label>
            <mixed-citation id="d456e427" publication-type="other">
Hathaway (2005).</mixed-citation>
         </ref>
         <ref id="d456e434a1310">
            <label>7</label>
            <mixed-citation id="d456e441" publication-type="other">
CAT, art. 1.</mixed-citation>
         </ref>
         <ref id="d456e448a1310">
            <label>8</label>
            <mixed-citation id="d456e455" publication-type="other">
CEDAW, art. 1.</mixed-citation>
         </ref>
         <ref id="d456e462a1310">
            <label>13</label>
            <mixed-citation id="d456e469" publication-type="other">
Hathaway 2005</mixed-citation>
         </ref>
         <ref id="d456e476a1310">
            <label>14</label>
            <mixed-citation id="d456e483" publication-type="other">
Hathaway (2003a</mixed-citation>
            <mixed-citation id="d456e489" publication-type="other">
2005).</mixed-citation>
            <mixed-citation id="d456e495" publication-type="other">
Dai (2005)</mixed-citation>
         </ref>
         <ref id="d456e502a1310">
            <label>15</label>
            <mixed-citation id="d456e509" publication-type="other">
Alter (2001)</mixed-citation>
         </ref>
         <ref id="d456e517a1310">
            <label>16</label>
            <mixed-citation id="d456e524" publication-type="other">
Neumayer (2005)</mixed-citation>
            <mixed-citation id="d456e530" publication-type="other">
Hathaway (2002).</mixed-citation>
         </ref>
         <ref id="d456e537a1310">
            <label>17</label>
            <mixed-citation id="d456e544" publication-type="other">
Hathaway (2002).</mixed-citation>
         </ref>
         <ref id="d456e551a1310">
            <label>19</label>
            <mixed-citation id="d456e558" publication-type="other">
Davis 2005</mixed-citation>
         </ref>
         <ref id="d456e565a1310">
            <label>20</label>
            <mixed-citation id="d456e572" publication-type="other">
Jessup (1956, 2).</mixed-citation>
         </ref>
         <ref id="d456e579a1310">
            <label>25</label>
            <mixed-citation id="d456e586" publication-type="other">
Skogly(2001).</mixed-citation>
         </ref>
         <ref id="d456e593a1310">
            <label>26</label>
            <mixed-citation id="d456e600" publication-type="other">
Downs and Jones (2002)</mixed-citation>
            <mixed-citation id="d456e606" publication-type="other">
Guzman (2002).</mixed-citation>
         </ref>
         <ref id="d456e614a1310">
            <label>27</label>
            <mixed-citation id="d456e621" publication-type="other">
Brambor, Clark, and Golder 2006</mixed-citation>
            <mixed-citation id="d456e627" publication-type="other">
Braumoeller 2004</mixed-citation>
            <mixed-citation id="d456e633" publication-type="other">
Goodliffe and
Hawkins (2006)</mixed-citation>
         </ref>
         <ref id="d456e643a1310">
            <label>28</label>
            <mixed-citation id="d456e650" publication-type="other">
Simmons (2002)</mixed-citation>
            <mixed-citation id="d456e656" publication-type="other">
Goodliffe and Hawkins (2006)</mixed-citation>
         </ref>
         <ref id="d456e663a1310">
            <label>29</label>
            <mixed-citation id="d456e670" publication-type="other">
Moravcsik (1997)</mixed-citation>
            <mixed-citation id="d456e676" publication-type="other">
Slaughter (1995</mixed-citation>
            <mixed-citation id="d456e682" publication-type="other">
2000).</mixed-citation>
         </ref>
         <ref id="d456e689a1310">
            <label>30</label>
            <mixed-citation id="d456e696" publication-type="other">
Finnemore (1996)</mixed-citation>
            <mixed-citation id="d456e702" publication-type="other">
Koh (1997)</mixed-citation>
            <mixed-citation id="d456e708" publication-type="other">
Keck and Sikkink (1998)</mixed-citation>
            <mixed-citation id="d456e715" publication-type="other">
Sikkink (1998)</mixed-citation>
            <mixed-citation id="d456e721" publication-type="other">
Lutz and Sikkink (2000).</mixed-citation>
         </ref>
         <ref id="d456e728a1310">
            <label>32</label>
            <mixed-citation id="d456e735" publication-type="other">
Hathaway (2002, 1963-76)</mixed-citation>
            <mixed-citation id="d456e741" publication-type="other">
Hathaway (2003b).</mixed-citation>
         </ref>
         <ref id="d456e748a1310">
            <label>34</label>
            <mixed-citation id="d456e755" publication-type="other">
Parzen (1961, 952).</mixed-citation>
         </ref>
         <ref id="d456e763a1310">
            <label>35</label>
            <mixed-citation id="d456e770" publication-type="other">
Box-Steffensmeier and Zorn 2001</mixed-citation>
            <mixed-citation id="d456e776" publication-type="other">
Beck,
Katz, and Tucker 1998</mixed-citation>
            <mixed-citation id="d456e785" publication-type="other">
Beck, Katz, and Tucker 1998, 1268-70</mixed-citation>
            <mixed-citation id="d456e792" publication-type="other">
Landman (2001)</mixed-citation>
            <mixed-citation id="d456e798" publication-type="other">
Simmons (2000)</mixed-citation>
         </ref>
         <ref id="d456e805a1310">
            <label>36</label>
            <mixed-citation id="d456e812" publication-type="other">
Hettinger and Zorn 1999</mixed-citation>
            <mixed-citation id="d456e818" publication-type="other">
Schmidt and Witte 1989</mixed-citation>
         </ref>
         <ref id="d456e825a1310">
            <label>37</label>
            <mixed-citation id="d456e832" publication-type="other">
Braumoeller (2004)</mixed-citation>
            <mixed-citation id="d456e838" publication-type="other">
Brambor,
Clark, and Golder (2006).</mixed-citation>
         </ref>
         <ref id="d456e848a1310">
            <label>38</label>
            <mixed-citation id="d456e855" publication-type="other">
Golder (2006)</mixed-citation>
         </ref>
         <ref id="d456e862a1310">
            <label>43</label>
            <mixed-citation id="d456e869" publication-type="other">
Hathaway (2002).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d456e885a1310">
            <mixed-citation id="d456e889" publication-type="other">
Alter, Karen. 2001. Establishing the supremacy of European law: The making of an international rule
of law in Europe. Oxford, UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d456e899a1310">
            <mixed-citation id="d456e903" publication-type="other">
Beck, Nathaniel, Jonathan N. Katz, and Richard Tucker. 1998. Taking time seriously: Time-series-
cross-section analysis with a binary dependent variable. American Journal of Political Science 42
(4): 1260-88.</mixed-citation>
         </ref>
         <ref id="d456e916a1310">
            <mixed-citation id="d456e920" publication-type="other">
Box-Steffensmeier, Janet M., and Christopher J. W. Zorn. 2001. Duration models and proportional
hazards in political science. American Journal of Political Science 45 (4): 972-88.</mixed-citation>
         </ref>
         <ref id="d456e930a1310">
            <mixed-citation id="d456e934" publication-type="other">
Brambor, Thomas, William Roberts Clark, and Matt Golder. 2006. Understanding interaction models:
Improving empirical analysis. Political Analysis 14:63-82.</mixed-citation>
         </ref>
         <ref id="d456e945a1310">
            <mixed-citation id="d456e949" publication-type="other">
Braumoeller, Bear F. 2004. Hypothesis testing and multiplicative interaction terms. International Orga-
nization 58:807-20.</mixed-citation>
         </ref>
         <ref id="d456e959a1310">
            <mixed-citation id="d456e963" publication-type="other">
Camp-Keith, Linda. 1999. The United Nations international covenant on civil and political rights: Does
it make a difference in human rights behavior? Journal of Peace Research 36 (1): 95-118.</mixed-citation>
         </ref>
         <ref id="d456e973a1310">
            <mixed-citation id="d456e977" publication-type="other">
Cingranelli, David L., and David L. Richards. 2004. The Cingranelli-Richards (CIRI) Human Rights
Database, http://ciri.binghamton.edu/ (accessed on May 1, 2007).</mixed-citation>
         </ref>
         <ref id="d456e987a1310">
            <mixed-citation id="d456e991" publication-type="other">
Convention Against Torture and Other Cruel, Inhuman or Degrading Treatment or Punishment, opened
for signature Dec. 10, 1984, Senate Treaty Doc. No. 100-20 (1988), 1465 U.N.T.S. 85 (entered into
force June 26, 1987).</mixed-citation>
         </ref>
         <ref id="d456e1004a1310">
            <mixed-citation id="d456e1008" publication-type="other">
Convention on the Elimination of All Forms of Discrimination Against Women, adopted Dec. 18, 1979,
1249 U.N.T.S. 13 (entered into force Dec. 3, 1981).</mixed-citation>
         </ref>
         <ref id="d456e1018a1310">
            <mixed-citation id="d456e1022" publication-type="other">
Dai, Xinyuan. 2005. Why comply? The domestic constituency mechanism. International Organization
59:363-98.</mixed-citation>
         </ref>
         <ref id="d456e1033a1310">
            <mixed-citation id="d456e1037" publication-type="other">
Davis, Kevin E. 2005. What can the rule of law variable tell us about rule of law reforms? New York
University Law and Economics Research Paper Series.</mixed-citation>
         </ref>
         <ref id="d456e1047a1310">
            <mixed-citation id="d456e1051" publication-type="other">
Donnelly, Jack. 1986. International human rights: A regime analysis. International Organization 40 (3):
599-642.</mixed-citation>
         </ref>
         <ref id="d456e1061a1310">
            <mixed-citation id="d456e1065" publication-type="other">
Downs, George W., and Michael A. Jones. 2002. Reputation, compliance, and international law. Journal
of Legal Studies 31:95-114.</mixed-citation>
         </ref>
         <ref id="d456e1075a1310">
            <mixed-citation id="d456e1079" publication-type="other">
Finnemore, Martha. 1996. National interests in international society. Ithaca, NY: Cornell University
Press.</mixed-citation>
         </ref>
         <ref id="d456e1089a1310">
            <mixed-citation id="d456e1093" publication-type="other">
Freedom House. 2003. Freedom in the world country ratings, 1972-1973 to 2001-2002. http://www
.freedomhouse.org/research/freeworld/FHSCORES.xls. (accessed on January 1, 2004).</mixed-citation>
         </ref>
         <ref id="d456e1103a1310">
            <mixed-citation id="d456e1107" publication-type="other">
Gibney, Michael. 2004. Political terror scale, 1980-2004. http://www.unca.edu/ politicalscience/faculty-
staff/Gibney%20Doc/Political%20Terror%20Scale%201980-2004.xls (accessed on December 1, 2006).</mixed-citation>
         </ref>
         <ref id="d456e1118a1310">
            <mixed-citation id="d456e1122" publication-type="other">
Golder, Matt. 2006. Presidential coattails and legislative fragmentation. American Journal of Political
Science 50:34-48.</mixed-citation>
         </ref>
         <ref id="d456e1132a1310">
            <mixed-citation id="d456e1136" publication-type="other">
Goldstein, Judith, and Robert O. Keohane, eds. 1993. Ideas and foreign policy: Beliefs, institutions, and
political change. Ithaca, NY: Cornell University Press</mixed-citation>
         </ref>
         <ref id="d456e1146a1310">
            <mixed-citation id="d456e1150" publication-type="other">
Goodliffe, Jay, and Darren G. Hawkins. 2006. Explaining commitment: States and the convention
against torture. The Journal of Politics 68 (2): 358-71.</mixed-citation>
         </ref>
         <ref id="d456e1160a1310">
            <mixed-citation id="d456e1164" publication-type="other">
Guzman, Andrew. 2002. A compliance-based theory of international law. California Law Review 90 (6):
1823-87.</mixed-citation>
         </ref>
         <ref id="d456e1174a1310">
            <mixed-citation id="d456e1178" publication-type="other">
Hafner-Burton, Emilie, and Kiyoteru Tsutsui. 2005. Human Rights Practices in a Globalizing World:
The Paradox of Empty Promises. American Journal of Sociology 110(5): 1373-1411.</mixed-citation>
         </ref>
         <ref id="d456e1188a1310">
            <mixed-citation id="d456e1192" publication-type="other">
—. Forthcoming. Justice lost! The failure of international human rights law to matter where needed
most. Journal of Peace Research.</mixed-citation>
         </ref>
         <ref id="d456e1203a1310">
            <mixed-citation id="d456e1207" publication-type="other">
Hathaway, Oona A. 2002. Do human rights treaties make a difference? Yale Law Journal 111:
1935-2042.</mixed-citation>
         </ref>
         <ref id="d456e1217a1310">
            <mixed-citation id="d456e1221" publication-type="other">
—. 2003a. The cost of commitment. Stanford Law Review 55 (5): 1821-62.</mixed-citation>
         </ref>
         <ref id="d456e1228a1310">
            <mixed-citation id="d456e1232" publication-type="other">
—. 2003b. Testing conventional wisdom. European Journal of International Law 13 (1): 185-200.</mixed-citation>
         </ref>
         <ref id="d456e1239a1310">
            <mixed-citation id="d456e1243" publication-type="other">
—. 2004. The international law of torture. In Torture: Philosophical, political, and legal perspec-
tives, edited by Sanford Levinson, 199-212. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d456e1253a1310">
            <mixed-citation id="d456e1257" publication-type="other">
—. 2005. Between power and principle: An integrated theory of international law, University of
Chicago Law Review 72 (2): 469-536.</mixed-citation>
         </ref>
         <ref id="d456e1267a1310">
            <mixed-citation id="d456e1271" publication-type="other">
Hathaway, Oona A., and Harold H. Koh. 2004. Foundations of international law and politics.
New York: Foundation Press.</mixed-citation>
         </ref>
         <ref id="d456e1282a1310">
            <mixed-citation id="d456e1286" publication-type="other">
Hathaway, Oona A., and Ariel Lavinbuk. 2006. Rationalism and revisionism in international law.
Harvard Law Review 119(5): 1404-43.</mixed-citation>
         </ref>
         <ref id="d456e1296a1310">
            <mixed-citation id="d456e1300" publication-type="other">
Hettinger, Virginia A., and Christopher J. W. Zorn. 1999. Signals, models, and congressional overrides
of the Supreme Court. Paper presented at the annual meeting of the Midwest Political Science
Association, Chicago, IL, April 15-17.</mixed-citation>
         </ref>
         <ref id="d456e1313a1310">
            <mixed-citation id="d456e1317" publication-type="other">
Human Rights Internet. 1991. Master list of human rights organizations and serial publications 3-58.</mixed-citation>
         </ref>
         <ref id="d456e1324a1310">
            <mixed-citation id="d456e1328" publication-type="other">
—. 1994. Master list of human rights organizations and serial publications 143-95.</mixed-citation>
         </ref>
         <ref id="d456e1335a1310">
            <mixed-citation id="d456e1339" publication-type="other">
—. 2000. Human Rights Databank, http://www.hri.ca/welcome.asp. (accessed on July 1, 2000).</mixed-citation>
         </ref>
         <ref id="d456e1346a1310">
            <mixed-citation id="d456e1350" publication-type="other">
—. 2003. Human Rights Databank, http://www.hri.ca/welcome.asp. (accessed on July 16, 2003).</mixed-citation>
         </ref>
         <ref id="d456e1358a1310">
            <mixed-citation id="d456e1362" publication-type="other">
International Covenant on Civil and Political Rights, adopted Dec. 19, 1966, Senate Exec. Doc. E, 95-2,
at 23 (1978), 999 U.N.T.S. 171 (entered into force Mar. 23, 1976).</mixed-citation>
         </ref>
         <ref id="d456e1372a1310">
            <mixed-citation id="d456e1376" publication-type="other">
Jessup, Philip C. 1956. Transnational law. New Haven, CT: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d456e1383a1310">
            <mixed-citation id="d456e1387" publication-type="other">
Keck, Margaret, and Kathryn Sikkink. 1998. Activists beyond borders: Advocacy networks in interna-
tional politics. Ithaca, NY: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d456e1397a1310">
            <mixed-citation id="d456e1401" publication-type="other">
Keohane, Robert. O. 1983. The demand for international regimes. In International regimes, edited by
Stephen Krasner, 141-71. Ithaca, NY: Cornell University Press.</mixed-citation>
         </ref>
         <ref id="d456e1411a1310">
            <mixed-citation id="d456e1415" publication-type="other">
—. 1984. After hegemony: Cooperation and discord in the world political economy. Princeton, NJ:
Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d456e1425a1310">
            <mixed-citation id="d456e1429" publication-type="other">
Koh, Harold H. 1997. Why do nations obey international law? Yale Law Journal 106 (8): 2599-659.</mixed-citation>
         </ref>
         <ref id="d456e1437a1310">
            <mixed-citation id="d456e1441" publication-type="other">
Koremenos, Barbara, Charles Lipson, and Duncan Snidal. 2001. The rational design of international
institutions. International Organization 55 (4): 761-800.</mixed-citation>
         </ref>
         <ref id="d456e1451a1310">
            <mixed-citation id="d456e1455" publication-type="other">
Krasner, Stephen D. 1983. Structural causes and regime consequences: Regimes as intervening vari-
ables. In International regimes, edited by Stephen D. Krasner, 1-21. Ithaca, NY: Cornell University
Press.</mixed-citation>
         </ref>
         <ref id="d456e1468a1310">
            <mixed-citation id="d456e1472" publication-type="other">
Kratochwil, Friedrich, and John G. Ruggie. 1986. International organization: A state of the art on an art
of the state. International Organization 40 (4): 753-75.</mixed-citation>
         </ref>
         <ref id="d456e1482a1310">
            <mixed-citation id="d456e1486" publication-type="other">
Landman, Todd. 2001. Measuring the international human rights regime. Paper presented at the 97th
annual meeting of the American Political Science Association, San Francisco, August 30-September 2.</mixed-citation>
         </ref>
         <ref id="d456e1496a1310">
            <mixed-citation id="d456e1500" publication-type="other">
—. 2005. Protecting human rights: A comparative study. Washington, DC: Georgetown University
Press.</mixed-citation>
         </ref>
         <ref id="d456e1510a1310">
            <mixed-citation id="d456e1514" publication-type="other">
—. 2006. Studying human rights. New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d456e1522a1310">
            <mixed-citation id="d456e1526" publication-type="other">
Lutz, Ellen L., and Kathryn Sikkink. 2000. International human rights law and practice in Latin
America. International Organization 54 (3): 633-59.</mixed-citation>
         </ref>
         <ref id="d456e1536a1310">
            <mixed-citation id="d456e1540" publication-type="other">
Marshall, Monty G., and Keith Jaggers. 2005. Polity IV project: Political regime characteristics and
transitions, 1800-2003. http://www.bsos.umd.edu/cidcm/inscr/polity/ index.htm (accessed on June
30, 2005).</mixed-citation>
         </ref>
         <ref id="d456e1553a1310">
            <mixed-citation id="d456e1557" publication-type="other">
Moravcsik, Andrew. 1997. Taking preferences seriously: A liberal theory of international politics. Inter-
national Organization 51 (4): 513-53.</mixed-citation>
         </ref>
         <ref id="d456e1567a1310">
            <mixed-citation id="d456e1571" publication-type="other">
—. 2000. The origins of human rights regimes: Democratic delegation in postwar Europe. Interna-
tional Organization 54 (2): 217-52.</mixed-citation>
         </ref>
         <ref id="d456e1581a1310">
            <mixed-citation id="d456e1585" publication-type="other">
Neumayer, Eric. 2005. Do international human rights treaties improve respect for human rights? Journal
of Conflict Resolution 49 (6): 925-53.</mixed-citation>
         </ref>
         <ref id="d456e1595a1310">
            <mixed-citation id="d456e1599" publication-type="other">
Parzen, Emanuel. 1961. An approach to time series analysis. Annals of mathematical statistics
32:951-89.</mixed-citation>
         </ref>
         <ref id="d456e1610a1310">
            <mixed-citation id="d456e1614" publication-type="other">
Schmidt, Peter, and Ann D. Witte. 1989. Predicting criminal recidivism using "split population" survi-
val time models. Journal of Econometrics 40:141-59.</mixed-citation>
         </ref>
         <ref id="d456e1624a1310">
            <mixed-citation id="d456e1628" publication-type="other">
Sikkink, Kathryn. 1998. Transnational politics, international relations theory, and human rights. Political
Science 31 (3): 516-23.</mixed-citation>
         </ref>
         <ref id="d456e1638a1310">
            <mixed-citation id="d456e1642" publication-type="other">
Simmons, Beth. 2000. International law and state behavior: Commitment and compliance in interna-
tional monetary affairs. American Political Science Review 94 (4): 819-35.</mixed-citation>
         </ref>
         <ref id="d456e1652a1310">
            <mixed-citation id="d456e1656" publication-type="other">
—. 2002. Why commit? Explaining state acceptance of international human rights obligations.
Paper prepared for meeting of the Conference on Delegation to International Organizations, May 3-4,
Brigham Young University, Provo, Utah.</mixed-citation>
         </ref>
         <ref id="d456e1669a1310">
            <mixed-citation id="d456e1673" publication-type="other">
Skogly, Sigrun I. 2001. The human rights obligations of the World Bank and the International Monetary
Fund. London: Cavendish.</mixed-citation>
         </ref>
         <ref id="d456e1683a1310">
            <mixed-citation id="d456e1687" publication-type="other">
Slaughter, Anne-Marie. 1995. International law in a world of liberal states. European Journal of Interna-
tional Law 6 (4): 503-38.</mixed-citation>
         </ref>
         <ref id="d456e1698a1310">
            <mixed-citation id="d456e1702" publication-type="other">
—. 2000. A liberal theory of international law. American Society of International Law Procedure
94:5-23.</mixed-citation>
         </ref>
         <ref id="d456e1712a1310">
            <mixed-citation id="d456e1716" publication-type="other">
United Nations Treaty Collection. 2005. http://untreaty.un.org/English/treaty.asp (accessed on May 1,
2007).</mixed-citation>
         </ref>
         <ref id="d456e1726a1310">
            <mixed-citation id="d456e1730" publication-type="other">
United Nations Treaty Series Overview. 2003. http://untreaty.un.org/English/overview.asp. (accessed on
July 1, 2003)</mixed-citation>
         </ref>
         <ref id="d456e1740a1310">
            <mixed-citation id="d456e1744" publication-type="other">
United States Department of State. 2001. Country Reports on Human Rights. http://www.state.gov/g/drl/
hr/c 1470.htm (accessed on July 1, 2004).</mixed-citation>
         </ref>
         <ref id="d456e1754a1310">
            <mixed-citation id="d456e1758" publication-type="other">
von Stein, Jana. 2005. Do treaties constrain or screen? Selection bias and treaty compliance. American
Political Science Review 99:611.</mixed-citation>
         </ref>
         <ref id="d456e1768a1310">
            <mixed-citation id="d456e1772" publication-type="other">
Vreeland, James Raymond. 2000. Institutional determinants of IMF agreements. http://pantheon
.yale.edu/~jrv9/IMFVeto.html(accessed on June 1, 2003).</mixed-citation>
         </ref>
         <ref id="d456e1783a1310">
            <mixed-citation id="d456e1787" publication-type="other">
—. 2003. Why do governments and the IMF enter into agreements? International Political Science
Review: Special Issue on the Political Economy of International Finance 24 (3): 321-43.</mixed-citation>
         </ref>
         <ref id="d456e1797a1310">
            <mixed-citation id="d456e1801" publication-type="other">
World Bank. 2005. World Development Indicators Online, http://www.worldbank.org/data/onlinedata-
bases/onlinedatabases.html (accessed on July 16, 2005).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
