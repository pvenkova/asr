<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">infeconthospepid</journal-id>
         <journal-title-group>
            <journal-title>Infection Control and Hospital Epidemiology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">0899823X</issn>
         <issn pub-type="epub">15596834</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor-stable">30143454</article-id>
         <article-id pub-id-type="doi">10.1086/501869</article-id>
         <article-id pub-id-type="msid">ICHE6769</article-id>
         <article-id pub-id-type="pmid">11232884</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>Original Articles</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Factors Associated With Tuberculin Reactivity in Two General Hospitals in Mexico<xref ref-type="fn" rid="fn1"/>
            </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>María de Lourdes</given-names>
                  <x xml:space="preserve"/>
                  <surname>García‐García</surname>
                  <suffix>, MD, DrSc</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Aida</given-names>
                  <x xml:space="preserve"/>
                  <surname>Jiménez‐Corona</surname>
                  <suffix>, MD, MSc</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>María Eugenia</given-names>
                  <x xml:space="preserve"/>
                  <surname>Jiménez‐Corona</surname>
                  <suffix>, MD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Leticia</given-names>
                  <x xml:space="preserve"/>
                  <surname>Ferreyra‐Reyes</surname>
                  <suffix>, MD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Kenneth</given-names>
                  <x xml:space="preserve"/>
                  <surname>Martínez</surname>
                  <suffix>, MSEE, CIH</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Blanca</given-names>
                  <x xml:space="preserve"/>
                  <surname>Rivera‐Chavira</surname>
                  <suffix>, PhD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>María Elena</given-names>
                  <x xml:space="preserve"/>
                  <surname>Martínez‐Tapia</surname>
                  <suffix>, MD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Emilio</given-names>
                  <x xml:space="preserve"/>
                  <surname>Valenzuela‐Miramontes</surname>
                  <suffix>, MD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Manuel</given-names>
                  <x xml:space="preserve"/>
                  <surname>Palacios‐Martínez</surname>
                  <suffix>, MD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Luis</given-names>
                  <x xml:space="preserve"/>
                  <surname>Juárez‐Sandino</surname>
                  <suffix>, MD</suffix>
               </string-name>
               <x xml:space="preserve">;</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>José Luis</given-names>
                  <x xml:space="preserve"/>
                  <surname>Valdespino‐Gómez</surname>
                  <suffix>, MD, MPH</suffix>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>02</month>
            <year>2001</year>
            <string-date>February 2001</string-date>
         </pub-date>
         <volume>22</volume>
         <issue>2</issue>
         <issue-id>iche.2001.22.issue-2</issue-id>
         <fpage>88</fpage>
         <lpage>93</lpage>
         <permissions>
            <copyright-statement>© 2001 by The Society for Healthcare Epidemiology of America. All rights reserved.</copyright-statement>
            <copyright-year>2001</copyright-year>
            <copyright-holder>The Society for Healthcare Epidemiology of America</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/501869"/>
         <abstract>
            <sec>
               <p>To identify risk factors associated with tuberculin reactivity in healthcare workers (HCWs).</p>
            </sec>
            <sec>
               <p>Cross‐sectional survey of tuberculin reactivity (2 TU of purified protein derivative (PPD) RT23, using the Mantoux two‐step test).</p>
            </sec>
            <sec>
               <p>Two general hospitals located in a region with a high prevalence of tuberculosis and high bacille Calmette‐Guérin (BCG) coverage.</p>
            </sec>
            <sec>
               <p>Volunteer sample of HCWs.</p>
            </sec>
            <sec>
               <p>605 HCWs were recruited: 71.2% female; mean age, 36.4 (standard deviation [SD], 8.2) years; 48.9% nurses, 10.4% physicians, 26.8% administrative personnel; mean time of employment, 10.9 (SD, 6.7) years. PPD reactivity (<underline>&gt;</underline>10 mm) was found in 390 (64.5%). Multivariate analysis revealed an association of tuberculin reactivity with occupational exposure in the hospital: participation in autopsies (odds ratio [OR], 9.3; 95% confidence interval [CI<sub>95</sub>], 2.1‐40.5; P=.003.), more than 1 year of employment (OR, 2.4; CI<sub>95</sub>, 1.1‐5.0;<italic>P</italic>=.02), work in the emergency or radiology departments (OR, 2.0; CI<sub>95</sub>, 1.03‐3.81;<italic>P</italic>=.04), being physicians or nurses (OR, 1.5; CI<sub>95</sub>, 1.04‐2.11;<italic>P</italic>=.03), age (OR, 1.04; CI<sub>95</sub>, 1.02‐1.07 per year of age;<italic>P</italic>&lt;.001), and BCG scar (OR, 2.1; CI<sub>95</sub>, 1.2‐3.4;<italic>P</italic>=.005).</p>
            </sec>
            <sec>
               <p>Although the studied population has a high baseline prevalence of tuberculosis infection and high coverage of BCG vaccination, nosocomial risk factors associated with PPD reactivity were identified as professional risks; strict early preventive measures must be implemented accordingly.</p>
            </sec>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="fn1">
               <p>From the Instituto Nacional de Salud Pública (Drs. García‐García, Jiménez‐Corona, Jiménez‐Corona, Ferreyra‐Reyes, Palacios‐Martínez, Juárez‐ Sandino, and Valdespino‐Gómez), Cuernavaca, México; the National Institute for Occupational Safety and Health (Mr. Martínez), Centers for Disease Control and Prevention, Cincinnati, Ohio; the Universidad Autónoma de Chihuahua (Dr. Rivera‐Chavira), México; Hospital General Regional No. 1, “Unidad Morelos” IMSS (Dr. Valenzuela‐Miramontes), Chihuahua, México; Hospital General “Dr. Salvador Zubirán Anchondo,” Secretaría de Salud (Dr. Martínez‐Tapia), Chihuahua, México.</p>
               <p>Address reprint requests to María de Lourdes García‐García, MD, DrSc, Secretaría Académica, Instituto Nacional de Salud Pública, Ave Universidad No. 655, Col Sta Ma Ahuacatitlán, Cuernavaca CP 62508, México.</p>
               <p>This study was funded by the Consejo Nacional de Ciencia y Tecnología: M0008‐M9602. Preliminary results were presented at the 30th IUATLD World Conference on Lung Health; Madrid, Spain; September 14‐18, 1999. The authors acknowledge the support and enthusiastic collaboration of the healthcare workers who participated in the study and of the authorities of the study hospitals. The authors also acknowledge the comments of Dr. Jose Sifuentes and Alfredo Ponce de Leon, which greatly improved the quality of the manuscript.</p>
               <p>00‐OA‐008. García‐García ML, Jiménez‐Corona A, Jiménez‐Corona ME, Ferreyra‐Reyes L, Martínez K, Rivera‐Chavira B, Martínez‐Tapia ME, Valenzuela‐Miramontes E, Palacios‐Martínez M, Juárez‐Sandino L, Valdespino‐Gómez JL. Factors associated with tuberculin reactivity in two general hospitals in Mexico.<italic>Infect Control Hosp Epidemiol</italic>2001;22:88‐93.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
   <back/>
</article>
