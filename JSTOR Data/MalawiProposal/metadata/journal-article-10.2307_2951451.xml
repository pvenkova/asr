<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">revieconstat</journal-id>
         <journal-id journal-id-type="jstor">j100341</journal-id>
         <journal-title-group>
            <journal-title>The Review of Economics and Statistics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>MIT Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00346535</issn>
         <issn pub-type="epub">15309142</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2951451</article-id>
         <title-group>
            <article-title>Estimating Deterministic Trends in the Presence of Serially Correlated Errors</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Eugene</given-names>
                  <surname>Canjels</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Mark W.</given-names>
                  <surname>Watson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>5</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">79</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i352645</issue-id>
         <fpage>184</fpage>
         <lpage>200</lpage>
         <page-range>184-200</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 The President and Fellows of Harvard College and the Massachusetts Institute of Technology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2951451"/>
         <abstract>
            <p>This paper studies the problems of estimation and inference in the linear trend model y&lt;sub&gt;t&lt;/sub&gt; = α + β t + u&lt;sub&gt;t&lt;/sub&gt;, where u&lt;sub&gt;t&lt;/sub&gt; follows an autoregressive process with largest root ρ and β is the parameter of interest. We contrast asymptotic results for the cases &lt;latex&gt;$|\rho| &lt; 1$&lt;/latex&gt; and ρ = 1 and argue that the most useful asymptotic approximations obtain from modeling ρ as local to unity. Asymptotic distributions are derived for the OLS, first-difference, infeasible GLS, and three feasible GLS estimators. These distributions depend on the local-to-unity parameter and a parameter that governs the variance of the initial error term κ. The feasible Cochrane-Orcutt estimator has poor properties, and the feasible Prais-Winsten estimator is the preferred estimator unless the researcher has sharp a priori knowledge about ρ and κ. The paper develops methods for constructing confidence intervals for β that account for uncertainty in ρ and κ. We use these results to estimate growth rates for real per-capita GDP in 128 countries.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1026e132a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1026e141" publication-type="journal">
Beach and MacKinnon
(1978)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e152" publication-type="journal">
Chipman (1979)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e161" publication-type="book">
Kadiyala (1968)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e169" publication-type="journal">
Maeshiro (1976  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e177" publication-type="journal">
1979)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e185" publication-type="journal">
Magee
(1987)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e196" publication-type="journal">
Park and Mitchell (1980)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e204" publication-type="journal">
Rao and Griliches (1969)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e213" publication-type="journal">
Spitzer
(1979)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e224" publication-type="journal">
Thornton (1987)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e233a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1026e240" publication-type="book">
Prais and Winsten
(1954)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e251" publication-type="journal">
Chipman (1979)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e259" publication-type="journal">
Chipman (1979)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e268" publication-type="journal">
Chipman (1979)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e277a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1026e284" publication-type="journal">
Durlauf and Phillips (1988)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e292" publication-type="book">
Lee and Phillips (1994)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e301a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1026e308" publication-type="other">
Bobkosky (1983)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e316" publication-type="book">
Cavanagh (1985)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e324" publication-type="journal">
Cavanagh et al. (1995)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e333" publication-type="journal">
Chan and Wei
(1987)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e344" publication-type="journal">
Chan (1988)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e352" publication-type="journal">
Phillips (1987)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e360" publication-type="journal">
Stock (1991)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e370a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1026e377" publication-type="book">
Elliott (1993)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e386a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1026e393" publication-type="journal">
Phillips and
Solo (1992)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e405a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1026e412" publication-type="journal">
Chipman (1979)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e421a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1026e428" publication-type="conference">
Phillips and Lee (1996)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e437a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1026e444" publication-type="book">
Prais and Winsten (1954)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e452" publication-type="journal">
Maeshiro (1976  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e460" publication-type="journal">
1979)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e469" publication-type="journal">
Beach and
MacKinnon (1978)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e480" publication-type="journal">
Park and Mitchell (1980)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e488" publication-type="journal">
Thornton (1987)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1026e496" publication-type="book">
Davidson and MacKinnon (1993, sec. 10.6)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e505a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1026e512" publication-type="journal">
Park and Mitchell (1980)  </mixed-citation>
            </p>
         </fn>
         <fn id="d1026e522a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1026e529" publication-type="journal">
Stock (1991)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1026e547a1310">
            <mixed-citation id="d1026e551" publication-type="journal">
Beach, C. M., and J. G. MacKinnon, "A Maximum Likelihood Procedure
for Regression with Autocorrelated Errors," Econometrica46:1
(1978), 51-58.<object-id pub-id-type="doi">10.2307/1913644</object-id>
               <fpage>51</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e570a1310">
            <mixed-citation id="d1026e574" publication-type="other">
Bobkosky, M. J., "Hypothesis Testing in Nonstationary Time Series,"
Department of Statistics, University of Wisconsin, Ph.D. Thesis
(1983).</mixed-citation>
         </ref>
         <ref id="d1026e587a1310">
            <mixed-citation id="d1026e591" publication-type="book">
Canjels, E., and M. W. Watson, "Estimating Deterministic Trends in the
Presence of Serially Correlated Errors," NBER Technical Working
Paper 165 (1994).<person-group>
                  <string-name>
                     <surname>Canjels</surname>
                  </string-name>
               </person-group>
               <source>Estimating Deterministic Trends in the Presence of Serially Correlated Errors</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e620a1310">
            <mixed-citation id="d1026e624" publication-type="book">
Cavanagh, C. L., "Roots Local to Unity," Department of Economics,
Harvard University, Manuscript (1985).<person-group>
                  <string-name>
                     <surname>Cavanagh</surname>
                  </string-name>
               </person-group>
               <source>Roots Local to Unity</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e650a1310">
            <mixed-citation id="d1026e654" publication-type="journal">
Cavanagh, C., G. Elliott, and J. Stock, "Inference in Models with Nearly
Nonstationary Regressors," Econometric Theory11:5 (1995), 1131-
1147.<person-group>
                  <string-name>
                     <surname>Cavanagh</surname>
                  </string-name>
               </person-group>
               <issue>5</issue>
               <fpage>1131</fpage>
               <volume>11</volume>
               <source>Econometric Theory</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e692a1310">
            <mixed-citation id="d1026e696" publication-type="journal">
Chan, N. H., "On Parameter Inference for Nearly Nonstationary Time
Series," Journal of the American Statistical Association83 (1988),
857-862.<object-id pub-id-type="doi">10.2307/2289317</object-id>
               <fpage>857</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e715a1310">
            <mixed-citation id="d1026e719" publication-type="journal">
Chan, N. H., and C. Z. Wei, "Asymptotic Inference for Nearly Nonstation-
ary AR(1) Processes," Annals of Statistics15 (1987), 1050-1063.<object-id pub-id-type="jstor">10.2307/2241815</object-id>
               <fpage>1050</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e735a1310">
            <mixed-citation id="d1026e739" publication-type="journal">
Chipman, John S., "Efficiency of Least-Squares Estimation of Linear
Trend when Residuals Are Autocorrelated," Econometrica47
(1979), 115-128.<object-id pub-id-type="doi">10.2307/1912350</object-id>
               <fpage>115</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e758a1310">
            <mixed-citation id="d1026e762" publication-type="journal">
Cochrane, D., and G. H. Orcutt, "Applications of Least Squares Regres-
sion to Relationships Containing Autocorrelated Error Terms,"
Journal of the American Statistical Association44 (1949), 32-61.<object-id pub-id-type="doi">10.2307/2280349</object-id>
               <fpage>32</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e781a1310">
            <mixed-citation id="d1026e785" publication-type="book">
Davidson, R., and J. G. MacKinnon, Estimation and Inference in
Econometrics (New York: Oxford University Press, 1993).<person-group>
                  <string-name>
                     <surname>Davidson</surname>
                  </string-name>
               </person-group>
               <source>Estimation and Inference in Econometrics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e811a1310">
            <mixed-citation id="d1026e815" publication-type="journal">
Dickey, D. A., and W. A. Fuller, "Distribution of the Estimators for
Autoregressive Time Series with a Unit Root," Journal of the
American Statistical Association74 (1979), 427-431.<object-id pub-id-type="doi">10.2307/2286348</object-id>
               <fpage>427</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e834a1310">
            <mixed-citation id="d1026e838" publication-type="journal">
Dufour, Jean-Marie, "Exact Test and Confidence Sets in Linear Regres-
sions with Autocorrelated Errors," Econometrica58:2 (1990),
475-494.<object-id pub-id-type="doi">10.2307/2938212</object-id>
               <fpage>475</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e857a1310">
            <mixed-citation id="d1026e861" publication-type="journal">
Durlauf, S. N., and P. C. B. Phillips, "Trends versus Random Walks in
Time Series Analysis," Econometrica56 (1988), 1333-1354.<object-id pub-id-type="doi">10.2307/1913101</object-id>
               <fpage>1333</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e877a1310">
            <mixed-citation id="d1026e881" publication-type="book">
Elliott, Graham, "Efficient Tests for a Unit Root when the Initial
Observation Is Drawn from Its Unconditional Distribution," Har-
vard University, Manuscript (1993).<person-group>
                  <string-name>
                     <surname>Elliott</surname>
                  </string-name>
               </person-group>
               <source>Efficient Tests for a Unit Root when the Initial Observation Is Drawn from Its Unconditional Distribution</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e910a1310">
            <mixed-citation id="d1026e914" publication-type="journal">
Elliott, Graham, Thomas J. Rothenberg, and James H. Stock, "Efficient
Tests of an Autoregressive Unit Root," Econometrica64:4 (1996),
813-836.<object-id pub-id-type="doi">10.2307/2171846</object-id>
               <fpage>813</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e933a1310">
            <mixed-citation id="d1026e937" publication-type="journal">
Grenander, U., "On the Estimation of Regression Coefficients in the Case
of an Autocorrelated Disturbance," Annals of Mathematical Statis-
tics25 (1954), 252-272.<object-id pub-id-type="jstor">10.2307/2236729</object-id>
               <fpage>252</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e957a1310">
            <mixed-citation id="d1026e961" publication-type="book">
Grenander, U., and M. Rosenblatt, Statistical Analysis of Stationary Time
Series (New York: Wiley, 1957).<person-group>
                  <string-name>
                     <surname>Grenander</surname>
                  </string-name>
               </person-group>
               <source>Statistical Analysis of Stationary Time Series</source>
               <year>1957</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e986a1310">
            <mixed-citation id="d1026e990" publication-type="book">
Lee, C. C., and P. C. B. Phillips, "Efficiency Gains Using GLS over OLS
under Nonstationarity," Yale University, Manuscript (1994).<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <source>Efficiency Gains Using GLS over OLS under Nonstationarity</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1015a1310">
            <mixed-citation id="d1026e1019" publication-type="book">
Kadiyala, K. R., "A Transformation Used to Circumvent the Problem of
Autocorrelated Errors," Econometrica36 (1968), 93-96.<person-group>
                  <string-name>
                     <surname>Kadiyala</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A Transformation Used to Circumvent the Problem of Autocorrelated Errors</comment>
               <fpage>93</fpage>
               <volume>36</volume>
               <source>Econometrica</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1054a1310">
            <mixed-citation id="d1026e1058" publication-type="journal">
Maeshiro, Asatoshi, "Autoregressive Transformation, Trended Indepen-
dent Variables and Autocorrelated Disturbances," this REVIEW58
(1976), 497-500.<person-group>
                  <string-name>
                     <surname>Maeshiro</surname>
                  </string-name>
               </person-group>
               <fpage>497</fpage>
               <volume>58</volume>
               <source>this REVIEW</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1093a1310">
            <mixed-citation id="d1026e1097" publication-type="journal">
— "On the Retention of the First Observations in Serial Correlation
Adjustment of Regression Models," International Economic Re-
view20:1 (1979), 259-265.<object-id pub-id-type="doi">10.2307/2526430</object-id>
               <fpage>259</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1116a1310">
            <mixed-citation id="d1026e1120" publication-type="journal">
Magee, L., "A Note on Cochrane-Orcutt Estimation," Journal of Econo-
metrics35 (1987), 211-218.<person-group>
                  <string-name>
                     <surname>Magee</surname>
                  </string-name>
               </person-group>
               <fpage>211</fpage>
               <volume>35</volume>
               <source>Journal of Econometrics</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1153a1310">
            <mixed-citation id="d1026e1157" publication-type="journal">
Nagaraj, N. K., and W. A. Fuller, "Estimation of the Parameters of Linear
Time Series Models Subject to Nonlinear Restrictions," Annals of
Statistics19:3 (1991), 1143-1154.<object-id pub-id-type="jstor">10.2307/2241943</object-id>
               <fpage>1143</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1176a1310">
            <mixed-citation id="d1026e1180" publication-type="journal">
Park, R. E., and B. M. Mitchell, "Estimating the Autocorrelated Error
Model with Trended Data," Journal of Econometrics13 (1980),
185-201.<person-group>
                  <string-name>
                     <surname>Park</surname>
                  </string-name>
               </person-group>
               <fpage>185</fpage>
               <volume>13</volume>
               <source>Journal of Econometrics</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1215a1310">
            <mixed-citation id="d1026e1219" publication-type="journal">
Phillips, P. C. B., "Toward a Unified Asymptotic Theory for Autoregres-
sion," Biometrika74 (1987), 535-547.<object-id pub-id-type="doi">10.2307/2336692</object-id>
               <fpage>535</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1235a1310">
            <mixed-citation id="d1026e1239" publication-type="conference">
Phillips, P. C. B., and C. C. Lee, "Efficiency Gains from Quasi-
Differencing under Nonstationarity," in P. M. Robinson and M.
Rosenblatt (eds.), Conference on Applied Probability and Time
Series, vol. II, Time Series Analysis in Memory of E. J. Hannan
(New York: Springer, 1996).<person-group>
                  <string-name>
                     <surname>Phillips</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Efficiency Gains from Quasi-Differencing under Nonstationarity</comment>
               <source>Conference on Applied Probability and Time Series, vol. II, Time Series Analysis in Memory of E. J. Hannan</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1277a1310">
            <mixed-citation id="d1026e1281" publication-type="journal">
Phillips, P. C. B., and V. Solo, "Asymptotics for Linear Processes," Annals
of Statistics20 (1992), 971-1001.<object-id pub-id-type="jstor">10.2307/2241993</object-id>
               <fpage>971</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1297a1310">
            <mixed-citation id="d1026e1301" publication-type="book">
Prais, S. J., and C. B. Winsten, "Trend Estimators and Serial Correlation,"
Cowles Foundation, Discussion Paper 383 (1954).<person-group>
                  <string-name>
                     <surname>Prais</surname>
                  </string-name>
               </person-group>
               <source>Trend Estimators and Serial Correlation</source>
               <year>1954</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1327a1310">
            <mixed-citation id="d1026e1331" publication-type="book">
Quah, D., and J. Wooldridge, "A Common Error in the Treatment of
Trending Time Series," Department of Economics, M.I.T., Manu-
script (1988).<person-group>
                  <string-name>
                     <surname>Quah</surname>
                  </string-name>
               </person-group>
               <source>A Common Error in the Treatment of Trending Time Series</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1360a1310">
            <mixed-citation id="d1026e1364" publication-type="journal">
Rao, P., and Z. Griliches, "Small-Sample Properties of Several Two-Stage
Regression Methods in the Context of Auto-Correlated Errors,"
Journal of the American Statistical Association64 (1969), 253-
272.<object-id pub-id-type="doi">10.2307/2283733</object-id>
               <fpage>253</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1387a1310">
            <mixed-citation id="d1026e1391" publication-type="journal">
Sampson, M., "The Effect of Parameter Uncertainty on Forecast Variances
and Confidence Intervals for Unit Root and Trend Stationary
Time-Series Models," Journal of Applied Econometrics6:1 (1991),
67-76.<object-id pub-id-type="jstor">10.2307/2096703</object-id>
               <fpage>67</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1414a1310">
            <mixed-citation id="d1026e1418" publication-type="journal">
Schmidt, Peter, "Some Results on Testing for Stationarity Using Data
Detrended in Differences," Economics Letters41 (1993), 1-6.<person-group>
                  <string-name>
                     <surname>Schmidt</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>41</volume>
               <source>Economics Letters</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1450a1310">
            <mixed-citation id="d1026e1454" publication-type="journal">
Schmidt, P., and P. C. B. Phillips, "LM Tests for a Unit Root in the
Presence of Deterministic Trends," Oxford Bulletin of Economics
and Statistics54 (1992), 257-287.<person-group>
                  <string-name>
                     <surname>Schmidt</surname>
                  </string-name>
               </person-group>
               <fpage>257</fpage>
               <volume>54</volume>
               <source>Oxford Bulletin of Economics and Statistics</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1489a1310">
            <mixed-citation id="d1026e1495" publication-type="journal">
Spitzer, J. J., "Small Sample Properties of Nonlinear Least Squares and
Maximum Likelihood Estimators in the Context of Autocorrelated
Errors," Journal of the American Statistical Association74 (1979),
41-47.<object-id pub-id-type="doi">10.2307/2286718</object-id>
               <fpage>41</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1519a1310">
            <mixed-citation id="d1026e1523" publication-type="journal">
Stock, James H., "Confidence Intervals of the Largest Autoregressive Root
in U.S. Macroeconomic Time Series," Journal of Monetary Econom-
ics28 (1991), 435-460.<person-group>
                  <string-name>
                     <surname>Stock</surname>
                  </string-name>
               </person-group>
               <fpage>435</fpage>
               <volume>28</volume>
               <source>Journal of Monetary Economics</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1026e1558a1310">
            <mixed-citation id="d1026e1562" publication-type="journal">
Summers, Robert, and Alan Heston, "The Penn World Table (Mark 5): An
Expanded Set of International Comparisons, 1950-1988," Quar-
terly Journal of Economics106 (1991), 327-368.<object-id pub-id-type="doi">10.2307/2937941</object-id>
               <fpage>327</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1026e1581a1310">
            <mixed-citation id="d1026e1585" publication-type="journal">
Thornton, D. L., "A Note on the Effect of the Cochrane-Orcutt Estimator
in the AR(1) Regression Model," Journal of Econometrics36
(1987), 369-376.<person-group>
                  <string-name>
                     <surname>Thornton</surname>
                  </string-name>
               </person-group>
               <fpage>369</fpage>
               <volume>36</volume>
               <source>Journal of Econometrics</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
