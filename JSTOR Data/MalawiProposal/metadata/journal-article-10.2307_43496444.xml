<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">ethnography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50011353</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Ethnography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>SAGE Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14661381</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17412714</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43496444</article-id>
         <title-group>
            <article-title>Ways of dying: AIDS care and agency in contemporary urban South Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mieke</given-names>
                  <surname>deGelder</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">13</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40138719</issue-id>
         <fpage>189</fpage>
         <lpage>212</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 SAGE Publications Ltd.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43496444"/>
         <abstract>
            <p>In the HIV/AIDS literature, the tendency has been to avoid the agentive concept in favour of an emphasis on structure and subjectivity. Based on research with an HIV/AIDS project in Pretoria, South Africa, this article posits that examining how agency is locally conceptualized forms one route via which the notion may be plausibly inserted into the study of the pandemic. I evoke the figure of the 'ideal agent': the imagined AIDS sufferer against whose model actions those of actual AIDS sufferers are measured. Three stages emerge, the first characterized by victimhood, the second by moral transformation, and the third by the choice to 'go home to one's relatives'. In view of patients' impending deaths, local knowledge regarding proper burial and ancestorship, and the limited resources of the project, the ideal agent suggests how AIDS sufferers' agency is circumscribed and helps to clarify the volatility of the moral rebirth experience.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d631e114a1310">
            <label>3</label>
            <mixed-citation id="d631e121" publication-type="other">
Fassin et al.
(2008)</mixed-citation>
            <mixed-citation id="d631e130" publication-type="other">
Niehaus (2006),</mixed-citation>
         </ref>
         <ref id="d631e137a1310">
            <label>4</label>
            <mixed-citation id="d631e144" publication-type="other">
Castells, 1997</mixed-citation>
            <mixed-citation id="d631e150" publication-type="other">
Robins, 2008</mixed-citation>
         </ref>
         <ref id="d631e157a1310">
            <label>9</label>
            <mixed-citation id="d631e164" publication-type="other">
Fassin, 2007;</mixed-citation>
            <mixed-citation id="d631e170" publication-type="other">
Posel, 2005</mixed-citation>
         </ref>
         <ref id="d631e177a1310">
            <label>10</label>
            <mixed-citation id="d631e184" publication-type="other">
Sharp (2008).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d631e200a1310">
            <mixed-citation id="d631e204" publication-type="other">
Abu-Lughod L (1990) The romance of resistance: Tracing transformations of power
through Bedouin women. American Ethnologist 17: 41-55.</mixed-citation>
         </ref>
         <ref id="d631e214a1310">
            <mixed-citation id="d631e218" publication-type="other">
Ahearn L (2001) Language and agency. Annual Review of Anthropology 30: 109-137.</mixed-citation>
         </ref>
         <ref id="d631e225a1310">
            <mixed-citation id="d631e229" publication-type="other">
Ashforth A (2005) Witchcraft, Violence, and Democracy in South Africa. Chicago, IL:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d631e239a1310">
            <mixed-citation id="d631e243" publication-type="other">
Bahre E (2007) Reluctant solidarity: Death, urban poverty and neighbourly assistance in
South Africa. Ethnography 8(1): 33-59.</mixed-citation>
         </ref>
         <ref id="d631e254a1310">
            <mixed-citation id="d631e258" publication-type="other">
Biehl J (2005) Vita: Life in a Zone of Social Abandonment. Berkeley: University of California
Press.</mixed-citation>
         </ref>
         <ref id="d631e268a1310">
            <mixed-citation id="d631e272" publication-type="other">
Biehl J (2007) Pharmaceuticalization: AIDS treatment and global health politics.
Anthropological Quarterly 80(4): 1083-1126.</mixed-citation>
         </ref>
         <ref id="d631e282a1310">
            <mixed-citation id="d631e286" publication-type="other">
Bloch M (1971) Placing the Dead: Tombs, Ancestral Villages and Kinship Organization in
Madagascar. London: Seminar Press.</mixed-citation>
         </ref>
         <ref id="d631e296a1310">
            <mixed-citation id="d631e300" publication-type="other">
Bloch M and Parry J (1982) Introduction: Death and the regeneration of life. In: Bloch M
and Parry J (eds) Death and the Regeneration of Life. Cambridge: Cambridge University
Press, 1-44.</mixed-citation>
         </ref>
         <ref id="d631e313a1310">
            <mixed-citation id="d631e317" publication-type="other">
Bolton R and Singer M (eds) (1992) Rethinking AIDS Prevention: Cultural Approaches.
Philadelphia, PA: Gordon Breach.</mixed-citation>
         </ref>
         <ref id="d631e327a1310">
            <mixed-citation id="d631e331" publication-type="other">
Bourdieu P (1977) Outline of a Theory of Practice. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d631e339a1310">
            <mixed-citation id="d631e343" publication-type="other">
Brain J (1973) Ancestors as elders in Africa: Further thoughts. Africa 43(2): 122-123.</mixed-citation>
         </ref>
         <ref id="d631e350a1310">
            <mixed-citation id="d631e354" publication-type="other">
Castells M (1997) The Power of Identity. Oxford: Blackwell.</mixed-citation>
         </ref>
         <ref id="d631e361a1310">
            <mixed-citation id="d631e365" publication-type="other">
Comaroff J and Comaroff J (eds) (1993) Modernity and its Malcontents: Ritual and Power in
Postcolonial Africa. Chicago: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d631e375a1310">
            <mixed-citation id="d631e379" publication-type="other">
Davidson D (1980 [1971]) Agency. In: Davidson D (ed.) Essays on Actions and Events.
Oxford: Clarendon Press, 43-61.</mixed-citation>
         </ref>
         <ref id="d631e389a1310">
            <mixed-citation id="d631e393" publication-type="other">
Dilger H (2008) 'We are all going to die': Kinship, belonging, and the morality of HIV/
AIDS-related illnesses and deaths in rural Tanzania. Anthropological Quarterly 81(1):
207-232.</mixed-citation>
         </ref>
         <ref id="d631e406a1310">
            <mixed-citation id="d631e410" publication-type="other">
Englund H (2006) Prisoners of Freedom: Human Rights and the African Poor. Berkeley:
University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e421a1310">
            <mixed-citation id="d631e425" publication-type="other">
Farmer P (1992) AIDS and Accusation: Haiti and the Geography of Blame. Berkeley:
University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e435a1310">
            <mixed-citation id="d631e439" publication-type="other">
Farmer P, Connors M and Simmons J (eds) (1996) Women, Poverty and AIDS: Sex, Drugs
and Structural Violence. Monroe, ME: Common Courage.</mixed-citation>
         </ref>
         <ref id="d631e449a1310">
            <mixed-citation id="d631e453" publication-type="other">
Fassin D (2007) When Bodies Remember: Experiences and Politics of AIDS in South Africa.
Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e463a1310">
            <mixed-citation id="d631e467" publication-type="other">
Fassin D, le Marcis F and Lethata T (2008) Life and times of Magda A.: Telling a story of
violence in South Africa. Current Anthropology 49(2): 225-246.</mixed-citation>
         </ref>
         <ref id="d631e477a1310">
            <mixed-citation id="d631e481" publication-type="other">
Fortes M (1976) An introductory commentary. In: Newell WH (ed.) Ancestors. The Hague:
Mouton, 1-16.</mixed-citation>
         </ref>
         <ref id="d631e491a1310">
            <mixed-citation id="d631e495" publication-type="other">
Geschiere P (1997) The Modernity of Witchcraft: Politics and the Occult in Postcolonial
Africa. Charlottesville: University Press of Virginia.</mixed-citation>
         </ref>
         <ref id="d631e506a1310">
            <mixed-citation id="d631e510" publication-type="other">
Goffman E (1997 [1961]) From 'the moral career of the mental patient'. In: Lemert C and
Branaman A (eds) The Goffman Reader. Maiden, MA: Blackwell, 63-71.</mixed-citation>
         </ref>
         <ref id="d631e520a1310">
            <mixed-citation id="d631e524" publication-type="other">
Goody J (1962) Death, Property and the Ancestors : A Study of the Mortuary Customs of the
LoDagaa of West Africa. Stanford, CA: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d631e534a1310">
            <mixed-citation id="d631e538" publication-type="other">
Hunter M (2005) Cultural politics and masculinities: Multiple-partners in historical perspec-
tive in KwaZulu-Natal. Culture, Health &amp; Sexuality 7(3): 209-223.</mixed-citation>
         </ref>
         <ref id="d631e548a1310">
            <mixed-citation id="d631e552" publication-type="other">
Jindra M (2005) Christianity and the proliferation of ancestors: Changes in hierarchy and
mortuary ritual in the Cameroon grassfields. Africa 75(3): 356-377.</mixed-citation>
         </ref>
         <ref id="d631e562a1310">
            <mixed-citation id="d631e566" publication-type="other">
Keane W (2007) Christian Moderns: Freedom and Fetish in the Mission Encounter. Berkeley:
University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e576a1310">
            <mixed-citation id="d631e580" publication-type="other">
Kopytoff I (1971) Ancestors as elders in Africa. Africa 41(2): 129-142.</mixed-citation>
         </ref>
         <ref id="d631e588a1310">
            <mixed-citation id="d631e592" publication-type="other">
Lockhart C (2008) The life and death of a street boy in East Africa: Everyday violence in the
time of AIDS. Medical Anthropology Quarterly 22(1): 94-115.</mixed-citation>
         </ref>
         <ref id="d631e602a1310">
            <mixed-citation id="d631e606" publication-type="other">
Mahmood S (2005) Politics of Piety: The Islamic Revival and the Feminist Subject. Princeton,
NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d631e616a1310">
            <mixed-citation id="d631e620" publication-type="other">
Mbembe A (2004) Aesthetics of superfluity. Public Culture 16(3): 373-405.</mixed-citation>
         </ref>
         <ref id="d631e627a1310">
            <mixed-citation id="d631e631" publication-type="other">
McNeill F (2009) 'Condoms cause AIDS': Poison, prevention and denial in Venda, South
Africa. African Affairs 108: 353-370.</mixed-citation>
         </ref>
         <ref id="d631e641a1310">
            <mixed-citation id="d631e645" publication-type="other">
Moore H and Sanders T (2001) Magical interpretations and material realities: An
introduction. In: Moore H and Sanders T (eds) Magical Interpretations, Material
Realities: Modernity, Witchcraft and the Occult in Postcolonial Africa. London:
Routledge, 1-27.</mixed-citation>
         </ref>
         <ref id="d631e661a1310">
            <mixed-citation id="d631e665" publication-type="other">
Niehaus I and Jonsson G (2005) Dr. Wouter Basson, Americans, and wild beasts: Men's
conspiracy theories of HIV/AIDS in the South African Lowveld. Medical Anthropology
24: 179-208.</mixed-citation>
         </ref>
         <ref id="d631e679a1310">
            <mixed-citation id="d631e683" publication-type="other">
Niehaus I (2006) Biographical lessons: Life stories, sex, and culture in Bushbuckridge, South
Africa. Cahiers d'Etudes Africaines 181: 51-73.</mixed-citation>
         </ref>
         <ref id="d631e693a1310">
            <mixed-citation id="d631e697" publication-type="other">
Niehaus I (2007) Death before dying: Understanding AIDS stigma in the South African
Lowveld. Journal of Southern African Studies 33(4): 845-860.</mixed-citation>
         </ref>
         <ref id="d631e707a1310">
            <mixed-citation id="d631e711" publication-type="other">
Ortner S (1984) Theory in anthropology since the sixties. Comparative Studies in Society and
History 26(1): 126-166.</mixed-citation>
         </ref>
         <ref id="d631e721a1310">
            <mixed-citation id="d631e725" publication-type="other">
Ortner S (1995) Resistance and the problem of ethnographic refusal. Comparative Studies in
Society and History 37(1): 173-193.</mixed-citation>
         </ref>
         <ref id="d631e735a1310">
            <mixed-citation id="d631e739" publication-type="other">
Ortner S (2005) Subjectivity and cultural critique. Anthropological Theory 5: 31-52.</mixed-citation>
         </ref>
         <ref id="d631e746a1310">
            <mixed-citation id="d631e750" publication-type="other">
Parker R (2001) Sexuality, culture, and power in HIV/AIDS research. Annual Review of
Anthropology 30: 163-179.</mixed-citation>
         </ref>
         <ref id="d631e761a1310">
            <mixed-citation id="d631e765" publication-type="other">
Posel D (2005) Sex, death and the fate of the nation: Reflections on the politicization of
sexuality in post-apartheid South Africa. Africa 75(2): 125-153.</mixed-citation>
         </ref>
         <ref id="d631e775a1310">
            <mixed-citation id="d631e779" publication-type="other">
Rasmussen S (2000) Alms, elders, and ancestors: The spirit of the gift among the Tuareg.
Ethnology 39(1): 15-38.</mixed-citation>
         </ref>
         <ref id="d631e789a1310">
            <mixed-citation id="d631e793" publication-type="other">
Rasmussen S (2004) Reflections on witchcraft, danger, and modernity among the Tuareg.
Africa 74(3): 315-340.</mixed-citation>
         </ref>
         <ref id="d631e803a1310">
            <mixed-citation id="d631e807" publication-type="other">
Roberts SJ (2005) Resisting stigma: Living positively with HIV/AIDS in South Africa.
Unpublished MA thesis, Department of Sociology, University of Johannesburg.</mixed-citation>
         </ref>
         <ref id="d631e817a1310">
            <mixed-citation id="d631e821" publication-type="other">
Robins S (2008) From Revolution to Rights in South Africa: Social Movements, NGOs &amp;
Popular Politics after Apartheid. Woodbridge: James Currey.</mixed-citation>
         </ref>
         <ref id="d631e831a1310">
            <mixed-citation id="d631e835" publication-type="other">
Robins S (2009) Foot soldiers of global health: Teaching and preaching AIDS science and
modern medicine on the frontline. Medical Anthropology 28(1): 81-107.</mixed-citation>
         </ref>
         <ref id="d631e846a1310">
            <mixed-citation id="d631e850" publication-type="other">
Rosaldo R (1989) Culture and Truth: The Remaking of Social Analysis. Boston, MA: Beacon
Press.</mixed-citation>
         </ref>
         <ref id="d631e860a1310">
            <mixed-citation id="d631e864" publication-type="other">
Scheper-Hughes N (1992) Death Without Weeping: The Violence of Everyday Life in Brazil.
Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e874a1310">
            <mixed-citation id="d631e878" publication-type="other">
Setel P (1999) A Plague of Paradoxes: AIDS, Culture and Demography in Northern Tanzania.
Chicago, IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d631e888a1310">
            <mixed-citation id="d631e892" publication-type="other">
Sharp J (2008) 'Fortress SA': Xenophobic violence in South Africa. Anthropology Today
24(4): 1-3.</mixed-citation>
         </ref>
         <ref id="d631e902a1310">
            <mixed-citation id="d631e906" publication-type="other">
Singer M (ed.) (1998) The Political Economy of AIDS. Amityville, NY: Bay wood.</mixed-citation>
         </ref>
         <ref id="d631e913a1310">
            <mixed-citation id="d631e917" publication-type="other">
Steadman L, Palmer C and Tilley C (1996) The universality of ancestor worship. Ethnology
35(1): 63-76.</mixed-citation>
         </ref>
         <ref id="d631e928a1310">
            <mixed-citation id="d631e932" publication-type="other">
Stein J, Soskolne T and Gibson K (2003) Working with ambivalence: Finding a positive
identity for HIV/AIDS in South Africa. Centre for Social Science Research Working
Paper 53, Centre for Social Science Research, Cape Town.</mixed-citation>
         </ref>
         <ref id="d631e945a1310">
            <mixed-citation id="d631e949" publication-type="other">
Straight B (2006) Becoming dead: The entangled agencies of the dearly departed.
Anthropology and Humanism 31(2): 101-110.</mixed-citation>
         </ref>
         <ref id="d631e959a1310">
            <mixed-citation id="d631e963" publication-type="other">
Taylor C (1985) What is human agency? Human Action and Agency: Philosophical Papers I.
Cambridge: Cambridge University Press, 15-44.</mixed-citation>
         </ref>
         <ref id="d631e973a1310">
            <mixed-citation id="d631e977" publication-type="other">
Thomas F (2007) 'Our families are killing us': HIV/AIDS, witchcraft and social tensions in
the Caprivi Region, Namibia. Anthropology and Medicine 14: 279-291.</mixed-citation>
         </ref>
         <ref id="d631e987a1310">
            <mixed-citation id="d631e991" publication-type="other">
Thomas F (2008) Indigenous narratives of HIV/AIDS: Morality and blame in a time of
change. Medical Anthropology 27(3): 227-256.</mixed-citation>
         </ref>
         <ref id="d631e1001a1310">
            <mixed-citation id="d631e1005" publication-type="other">
Thornton R (2008) Unimagined Community: Sex, Networks, and AIDS in Uganda and South
Africa. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e1016a1310">
            <mixed-citation id="d631e1020" publication-type="other">
Treichler P (1999) How to Have Theory in an Epidemic: Cultural Chronicles of AIDS.
Durham, NC: Duke University Press.</mixed-citation>
         </ref>
         <ref id="d631e1030a1310">
            <mixed-citation id="d631e1034" publication-type="other">
Wardlow H (2006) Wayward Women: Sexuality and Agency in a New Guinea Society.
Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d631e1044a1310">
            <mixed-citation id="d631e1048" publication-type="other">
Weir J (2005) Whose uNkulunkulu? Africa 75(2): 203-219.</mixed-citation>
         </ref>
         <ref id="d631e1055a1310">
            <mixed-citation id="d631e1059" publication-type="other">
Wojcicki JM (2002) 'She drank his money': Survival, sex and the problem of violence in
taverns in Gauteng Province, South Africa. Medical Anthropology Quarterly 16(3):
267-293.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
