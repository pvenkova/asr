<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">intejplanscie</journal-id>
         <journal-title-group>
            <journal-title>International Journal of Plant Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10585893</issn>
         <issn pub-type="epub">15375315</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor-stable">3080067</article-id>
         <article-id pub-id-type="doi">10.1086/320776</article-id>
         <article-id pub-id-type="msid">IJPS010067</article-id>
         <title-group>
            <article-title>Cretaceous<italic>Widdringtonia</italic>Endl. (Cupressaceae) from North America</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>E. E.</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>McIver</surname>
               </string-name>
               <xref ref-type="fn" rid="fn1">1</xref>
            </contrib>
            <aff id="aff_1">Department of Geological Sciences, University of Saskatchewan, Saskatoon, Saskatchewan S7N 5E2, Canada</aff>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>07</month>
            <year>2001</year>
            <string-date>July 2001</string-date>
         </pub-date>
         <volume>162</volume>
         <issue>4</issue>
         <issue-id>ijps.2001.162.issue-4</issue-id>
         <fpage>937</fpage>
         <lpage>961</lpage>
         <permissions>
            <copyright-statement>© 2001 by The University of Chicago. All rights reserved.</copyright-statement>
            <copyright-year>2001</copyright-year>
            <copyright-holder>The University of Chicago</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/320776"/>
         <abstract>
            <p>Reinvestigation of Cretaceous Cupressaceae<italic>sensu stricto</italic>fossils previously described as<italic>Widdringtonites subtilis</italic>Heer confirms that plants remarkably similar to those of extant<italic>Widdringtonia</italic>Endl. inhabited North America ca. 95 million years ago.<italic>Widdringtonia</italic>is restricted today to Africa, and the consensus has been that the taxon has an origin in southern Africa and an evolutionary history restricted to the Southern Hemisphere. The fossil remains of<italic>Widdringtonia</italic>from the Tuscaloosa Formation of Alabama, including seed cones, pollen cones, and attached foliage, indicate otherwise. The quadrivalvate seed cones of the fossil are ca. 5.0 mm long and 5.0 mm wide and are attached to foliage bearing decussate, scalelike to linear leaves. Comparison with similar fossils from other sites in North America, Europe, and Greenland indicate a broad distribution for the genus during the mid‐Cretaceous and persistence in Europe well into the Tertiary. Fossil data indicate evolution in foliage morphology but little change in reproductive organs, except for size of seed cones, since the Cenomanian. Fossil remains of<italic>Brachyphyllum</italic>, inappropriately included within<italic>Widdringtonia subtilis</italic>are removed from the taxon and described separately.</p>
         </abstract>
         <kwd-group>
            <title>Keywords:</title>
            <kwd>
               <italic>Brachyphyllum</italic>
            </kwd>
            <x xml:space="preserve">,</x>
            <kwd>Cupressaceae</kwd>
            <x xml:space="preserve">,</x>
            <kwd>Cretaceous</kwd>
            <x xml:space="preserve">,</x>
            <kwd>evolution</kwd>
            <x xml:space="preserve">,</x>
            <kwd>North America</kwd>
            <x xml:space="preserve">,</x>
            <kwd>fossil</kwd>
            <x xml:space="preserve">,</x>
            <kwd>paleobotany</kwd>
            <x xml:space="preserve">,</x>
            <kwd>phytogeography</kwd>
            <x xml:space="preserve">,</x>
            <kwd>
               <italic>Widdringtonia</italic>
            </kwd>
            <x xml:space="preserve">,</x>
            <kwd>
               <italic>Widdringtonites</italic>
            </kwd>
            <x xml:space="preserve">.</x>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="fn1">
               <label>1</label>
               <p>Elisabeth McIver passed away on April 1, 2001. Address correspondence to James Basinger.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
   <back>
      <ack>
         <sec>
            <title>Acknowledgments</title>
            <p>Thanks are due to D. Christophel, H. Taylor, M. Pole, G. Jordan, and curators of the Adelaide Botanical Garden for assistance in collecting Cupressaceae from Australia; to B. Robertson and E. Campbell for seeds from South Africa; to the MO and US for loan of herbarium specimens; to A. Peterhansel for assistance in translating manuscripts; to J. Kvaček for assisting with the identification of<italic>Brachyphyllum</italic>and for providing photos of European Cenomanian<italic>Widdringtonia</italic>, to the University of Saskatchewan and the Natural Sciences and Engineering Research Council of Canada for financial support (President’s award, PSNERC, to E. E. McIver); and to J. F. Basinger, University of Saskatchewan, for critically reading the manuscript. Special thanks are due to K. Aulenback for assistance in procuring fossil specimens, for mounting fossil SEM and light microscopy specimens, and for photographing specimens for<xref ref-type="fig" rid="fg4">figures 4<italic>a</italic>
               </xref>,<xref ref-type="fig" rid="fg5">5<italic>i</italic>
               </xref>,<xref ref-type="fig" rid="fg9">9<italic>a</italic>
               </xref>,<xref ref-type="fig" rid="fg9">9<italic>b</italic>
               </xref>,<xref ref-type="fig" rid="fg10">10<italic>d</italic>
               </xref>, and<xref ref-type="fig" rid="fg10">10<italic>f</italic>
               </xref>.</p>
         </sec>
      </ack>
      <ref-list content-type="unparsed">
         <title>Literature Cited</title>
         <ref id="rf1">
            <mixed-citation xlink:type="simple" publication-type="">Baudin F 1995 Depositional controls on Mesozoic source rocks in the Tethys. Pages 191–211<italic>in</italic>A Huc, ed. Paleogeography, paleoclimate, and source rocks. AAPG studies in geology, no. 40. American Association of Petroleum Geologists, Tulsa, Okla.</mixed-citation>
         </ref>
         <ref id="rf2">
            <mixed-citation xlink:type="simple" publication-type="">Bell WA 1956 Lower Cretaceous floras of Western Canada. Geol Surv Can Mem 293:1–84.</mixed-citation>
         </ref>
         <ref id="rf3">
            <mixed-citation xlink:type="simple" publication-type="">——— 1963 Upper Cretaceous floras of the Dunvegan, Bad Heart, and Milk River formations of Western Canada. Geol Surv Can Bull 94:1–76.</mixed-citation>
         </ref>
         <ref id="rf4">
            <mixed-citation xlink:type="simple" publication-type="">Berry EW 1911<italic>a</italic> The flora of the Raritan Formation. N J Geol Surv Bull 3:1–233.</mixed-citation>
         </ref>
         <ref id="rf5">
            <mixed-citation xlink:type="simple" publication-type="">——— 1911<italic>b</italic> The Lower Cretaceous floras of the world. Pages 99–151<italic>in</italic>Maryland Geological Survey: Lower Cretaceous. Johns Hopkins University Press, Baltimore.</mixed-citation>
         </ref>
         <ref id="rf6">
            <mixed-citation xlink:type="simple" publication-type="">——— 1912 Notes on the genus<italic>Widdringtonites</italic>. Bull Torrey Bot Club 39:341–348.</mixed-citation>
         </ref>
         <ref id="rf7">
            <mixed-citation xlink:type="simple" publication-type="">——— 1919 Upper Cretaceous floras of the Eastern Gulf region in Tennessee, Mississippi, Alabama, and Georgia. US Geol Surv Prof Pap 112: 1–177.</mixed-citation>
         </ref>
         <ref id="rf8">
            <mixed-citation xlink:type="simple" publication-type="">——— 1929 The Kootenay and lower Blairmore floras. Can Natl Mus Bull 58:28–54.</mixed-citation>
         </ref>
         <ref id="rf9">
            <mixed-citation xlink:type="simple" publication-type="">Boyd A 1992 Revision of the Late Cretaceous Pautût flora from West Greenland: (Cycadeoidales, Caytoniales, Ginkoales, Coniferales). Palaeontogr Abt B 225:105–172.</mixed-citation>
         </ref>
         <ref id="rf10">
            <mixed-citation xlink:type="simple" publication-type="">Brunsfeld SJ, PS Soltis, DE Soltis, PA Gadek, CJ Quinn, DD Strenge, TA Ranker 1994 Phylogenetic relationships among the genera of Taxodiaceae and Cupressaceae: evidence from rbcL sequences. Syst Bot 19:253–262.</mixed-citation>
         </ref>
         <ref id="rf11">
            <mixed-citation xlink:type="simple" publication-type="">Christopher RA 1979 <italic>Normapolles</italic>and tricolporate pollen assemblages from the Raritan and Magothy formations (Upper Cretaceous) of New Jersey. Palynology 3:73–121.</mixed-citation>
         </ref>
         <ref id="rf12">
            <mixed-citation xlink:type="simple" publication-type="">Cox CB, PD Moore 1985 Biogeography: an ecological and evolutionary approach. 4th ed. Blackwell Scientific, Oxford. 244 pp.</mixed-citation>
         </ref>
         <ref id="rf13">
            <mixed-citation xlink:type="simple" publication-type="">Dallimore W, AB Jackson 1966 A handbook of Coniferae and Ginkgoaceae, revised by SG Harrison. Edward Arnold, London. 729 pp.</mixed-citation>
         </ref>
         <ref id="rf14">
            <mixed-citation xlink:type="simple" publication-type="">Domin K 1938 Gymnospermae. Nové Encyclopedie Pŕírodních Věd, Nékladem České Akademie Véd a Uméní, Prague. 380 pp.</mixed-citation>
         </ref>
         <ref id="rf15">
            <mixed-citation xlink:type="simple" publication-type="">Dorf E 1952 Critical analysis of Cretaceous stratigraphy and paleobotany of the Atlantic Coastal Plain. Am Assoc Pet Geol 36:2161–2184.</mixed-citation>
         </ref>
         <ref id="rf16">
            <mixed-citation xlink:type="simple" publication-type="">Doyle JA, EI Robbins 1977 Angiosperm pollen zonation of the continental Cretaceous of the Atlantic Coastal Plain and its application to deep wells in the Salisbury Embayment. Palynology 1:43–78.</mixed-citation>
         </ref>
         <ref id="rf17">
            <mixed-citation xlink:type="simple" publication-type="">Eckenwalder JE 1976 Re‐evaluation of Cupressaceae and Taxodiaceae: a proposed merger. Madroño 23:237–300.</mixed-citation>
         </ref>
         <ref id="rf18">
            <mixed-citation xlink:type="simple" publication-type="">Endlicher S 1842 Mantissa botanica sistens generum plantarum, supplementum secundum. Beck, Vindobonae.</mixed-citation>
         </ref>
         <ref id="rf19">
            <mixed-citation xlink:type="simple" publication-type="">——— 1847 Synopsis Coniferarum. Scheitlin &amp; Zollikofer, Sangalli. 368 pp.</mixed-citation>
         </ref>
         <ref id="rf20">
            <mixed-citation xlink:type="simple" publication-type="">Florin R 1940 The Tertiary fossil conifers of south Chile and their phytogeographical significance. K Sven Vetenskapsakad Handl 3:1–107.</mixed-citation>
         </ref>
         <ref id="rf21">
            <mixed-citation xlink:type="simple" publication-type="">——— 1963 The distribution of conifer and taxad genera in time and space. Acta Horti Bergiani 20:121–312.</mixed-citation>
         </ref>
         <ref id="rf22">
            <mixed-citation xlink:type="simple" publication-type="">Fontaine WM 1889 The Potomac or younger Mesozoic flora. US Geological Survey Monograph no. 15. Government Printing Office, Washington, D.C. 377 pp.</mixed-citation>
         </ref>
         <ref id="rf23">
            <mixed-citation xlink:type="simple" publication-type="">Gadek PA, DL Alpers, MM Heslewood, CJ Quinn 2000 Relationships within Cupressaceae<italic>sensu lato</italic>: a combined morphological and molecular approach. Am J Bot 87:1044–1057.</mixed-citation>
         </ref>
         <ref id="rf24">
            <mixed-citation xlink:type="simple" publication-type="">Golovneva LB 1988 A new genus<italic>Microconium</italic>(Cupressaceae) from the Late Cretaceous deposits of the north‐east of the USSR. Bot Zh 73:1179–1183.</mixed-citation>
         </ref>
         <ref id="rf25">
            <mixed-citation xlink:type="simple" publication-type="">Groot JJ, JS Penny, CR Groot 1961 Plant microfossils and age of the Raritan, Tuscaloosa and Magothy formations of the eastern United States. Palaeontogr Abt B 108:121–140.</mixed-citation>
         </ref>
         <ref id="rf26">
            <mixed-citation xlink:type="simple" publication-type="">Harland WB, RL Armstrong, AV Cox, LE Craig, AG Smith, DG Smith 1990 A geologic time scale 1989. Cambridge University Press, Avon. 263 pp.</mixed-citation>
         </ref>
         <ref id="rf27">
            <mixed-citation xlink:type="simple" publication-type="">Harris TM 1979 The Yorkshire Jurassic flora. Vol 5. Coniferales. Trustees of the British Museum, London. 166 pp.</mixed-citation>
         </ref>
         <ref id="rf28">
            <mixed-citation xlink:type="simple" publication-type="">Heer O 1874 Die Kreide‐Flora der arctischen Zone: Flora fossilis arctica 3. K Sven Vetenskapsakad Handl 12:1–138.</mixed-citation>
         </ref>
         <ref id="rf29">
            <mixed-citation xlink:type="simple" publication-type="">——— 1882 Die fossile Flora Grönlands. 1. Flora fossilis arctica 7:1–147.</mixed-citation>
         </ref>
         <ref id="rf30">
            <mixed-citation xlink:type="simple" publication-type="">——— 1883 Die fossile Flora Grönlands. 2. Flora fossilis arctica 8:148–275.</mixed-citation>
         </ref>
         <ref id="rf31">
            <mixed-citation xlink:type="simple" publication-type="">Hill RS, TJ Brodribb 1999 Turner review no. 2: southern conifers in time and space. Aust J Bot 47:639–696.</mixed-citation>
         </ref>
         <ref id="rf32">
            <mixed-citation xlink:type="simple" publication-type="">Hill RS, RJ Carpenter 1989 Tertiary gymnosperms from Tasmania: Cupressaceae. Alcheringa 13:89–102.</mixed-citation>
         </ref>
         <ref id="rf33">
            <mixed-citation xlink:type="simple" publication-type="">Hollick A 1906 The Cretaceous flora of southern New York and New England. US Geological Survey Monograph no. 50. 219 pp.</mixed-citation>
         </ref>
         <ref id="rf34">
            <mixed-citation xlink:type="simple" publication-type="">Hoya B 1986 The Oxford encyclopedia of trees of the world. Crescent, New York. 288 pp.</mixed-citation>
         </ref>
         <ref id="rf35">
            <mixed-citation xlink:type="simple" publication-type="">Knobloch E 1971 Neue Pflanzenfunde aus dem böhmischen und mährischen Cenoman. Neues Jahrb Geol Palaeontol Abh 139:43–56.</mixed-citation>
         </ref>
         <ref id="rf36">
            <mixed-citation xlink:type="simple" publication-type="">Kovar‐Eder J, Z Kvaček 1995 The record of a fertile twig of<italic>Tetraclinis brachyodon</italic>(Brogniart) Mai et Walther from Radoboj, Croatia (Middle‐Miocene). Flora 190:261–264.</mixed-citation>
         </ref>
         <ref id="rf37">
            <mixed-citation xlink:type="simple" publication-type="">Krasser F 1896 Beiträge zur Kenntniss der fossilen Kreideflora von Kunstadt in Mähren. Beitr Palaeontol Oesterr Ungarns 10:113–152.</mixed-citation>
         </ref>
         <ref id="rf38">
            <mixed-citation xlink:type="simple" publication-type="">Kvaček Z 1989 Fosilní<italic>Tetraclinis</italic>Mast. (Cupressaceae). Cas Nar Muz Oddcil Prirodoved 155:45–54.</mixed-citation>
         </ref>
         <ref id="rf39">
            <mixed-citation xlink:type="simple" publication-type="">Kvaček Z, SR Manchester, HE Schorn 2000 Cones, seeds, and foliage of<italic>Tetraclinis salicornioides</italic>(Cupressaceae) from the Oligocene and Miocene of western North America: a geographic extension of the European Tertiary species. Int J Plant Sci 161:331–344.</mixed-citation>
         </ref>
         <ref id="rf40">
            <mixed-citation xlink:type="simple" publication-type="">Li HL 1953 A reclassification of<italic>Libocedrus</italic>and Cupressaceae. J Arnold Arbor Harv Univ 34:17–36.</mixed-citation>
         </ref>
         <ref id="rf41">
            <mixed-citation xlink:type="simple" publication-type="">Little DP 2000 Phylogenetic relationship and monophyly of<italic>Cupressus</italic>and<italic>Chamaecyparis</italic>(Cupressaceae): molecular and organismal evidence. Abstracts of the annual meeting of the Botanical Society of America. Am J Bot 87:139.</mixed-citation>
         </ref>
         <ref id="rf42">
            <mixed-citation xlink:type="simple" publication-type="">Mai DH 1995 Two conifers—<italic>Tetraclinis</italic>Mast. (Cupressaceae) and<italic>Metasequoia</italic>Miki (Taxodiaceae)—relicts or palaeoclimatic indicators of the past. Pages 199–221<italic>in</italic>MC Boulter, HC Fisher, eds. Cenozoic plants and climates of the Arctic. NATO ASI ser 1: global environmental change, no. 27. Springer, Berlin.</mixed-citation>
         </ref>
         <ref id="rf43">
            <mixed-citation xlink:type="simple" publication-type="">Marsh JA 1966 Notes on<italic>Widdringtonia.</italic>Bothalia 9:124–126.</mixed-citation>
         </ref>
         <ref id="rf44">
            <mixed-citation xlink:type="simple" publication-type="">McIver EE 1992 Fossil<italic>Fokienia</italic>(Cupressaceae) from the Paleocene of Alberta, Canada. Can J Bot 70:742–749.</mixed-citation>
         </ref>
         <ref id="rf45">
            <mixed-citation xlink:type="simple" publication-type="">——— 1994 An early<italic>Chamaecyparis</italic>(Cupressaceae) from the Late Cretaceous of Vancouver Island, British Columbia, Canada. Can J Bot 72:1787–1796.</mixed-citation>
         </ref>
         <ref id="rf46">
            <mixed-citation xlink:type="simple" publication-type="">McIver EE, KR Aulenback 1994 The morphology and relationships of<italic>Mesocyparis umbonata</italic>sp. nov.: fossil Cupressaceae from the Late Cretaceous of Alberta, Canada. Can J Bot 72:273–295.</mixed-citation>
         </ref>
         <ref id="rf47">
            <mixed-citation xlink:type="simple" publication-type="">McIver EE, JF Basinger 1987 <italic>Mesocyparis borealis</italic>gen. et sp. nov.: fossil Cupressaceae from the early Tertiary of Saskatchewan, Canada. Can J Bot 65:2338–2351.</mixed-citation>
         </ref>
         <ref id="rf48">
            <mixed-citation xlink:type="simple" publication-type="">——— 1990 Fossil seed cones of<italic>Fokienia</italic>(Cupressaceae) from the Paleocene Ravenscrag Formation of Saskatchewan, Canada. Can J Bot 68:1609–1618.</mixed-citation>
         </ref>
         <ref id="rf49">
            <mixed-citation xlink:type="simple" publication-type="">——— 1993 Flora of the Ravenscrag Formation (Paleocene), southwestern Saskatchewan, Canada. Palaeontogr Can 10:1–167.</mixed-citation>
         </ref>
         <ref id="rf50">
            <mixed-citation xlink:type="simple" publication-type="">Miller CN 1999 Implications of fossil conifers for the phylogenetic relationships of living families. Bot Rev 65:239–277.</mixed-citation>
         </ref>
         <ref id="rf51">
            <mixed-citation xlink:type="simple" publication-type="">Moseley MF Jr 1943 Contribution to the life history, morphology, and phylogeny of<italic>Widdringtonia cupressoides</italic>. Lloydia 6:109–132.</mixed-citation>
         </ref>
         <ref id="rf52">
            <mixed-citation xlink:type="simple" publication-type="">Newberry JS 1895 The flora of the Amboy Clays. US Geological Survey Monograph no. 26. 60 pp.</mixed-citation>
         </ref>
         <ref id="rf53">
            <mixed-citation xlink:type="simple" publication-type="">——— 1898 The later extinct floras of North America. US Geological Survey Monograph no. 35. 95 pp.</mixed-citation>
         </ref>
         <ref id="rf54">
            <mixed-citation xlink:type="simple" publication-type="">Niklas KJ 1997 The evolutionary biology of plants. University of Chicago Press, Chicago. 449 pp.</mixed-citation>
         </ref>
         <ref id="rf55">
            <mixed-citation xlink:type="simple" publication-type="">Owens JN, M Molder 1974 Cone initiation and development before dormancy in yellow cedar (<italic>Chamaecyparis nootkatensis</italic>). Can J Bot 55:2075–2084.</mixed-citation>
         </ref>
         <ref id="rf56">
            <mixed-citation xlink:type="simple" publication-type="">Pauw CA, HP Linder 1997 Tropical African cedars (<italic>Widdringtonia</italic>, Cupressaceae): systematics, ecology and conservation status. Bot J Linn Soc 123:297–319.</mixed-citation>
         </ref>
         <ref id="rf57">
            <mixed-citation xlink:type="simple" publication-type="">Pilger R 1926 Coniferae. Pages 121–407<italic>in</italic>A Engler, K Prantl, eds. Natürliche Pflanzenfamilien. 2d ed. Engelmann, Leipzig.</mixed-citation>
         </ref>
         <ref id="rf58">
            <mixed-citation xlink:type="simple" publication-type="">Pomerol C 1980 Geology of France with twelve itineraries. Guides géologiques régionaux. Masson, Paris. 256 pp.</mixed-citation>
         </ref>
         <ref id="rf59">
            <mixed-citation xlink:type="simple" publication-type="">Saporta G 1863 Études sur la végétation du sud‐est de la France a l’époque Tertiaire. Annales des Sciences Naturelles, Partie Botanique, pt 1, ser 4, 19:5–124.</mixed-citation>
         </ref>
         <ref id="rf60">
            <mixed-citation xlink:type="simple" publication-type="">Saxton WT 1913 The classification of conifers. New Phytol 12:242–262.</mixed-citation>
         </ref>
         <ref id="rf61">
            <mixed-citation xlink:type="simple" publication-type="">Scotese CR 1997 Paleogeographic atlas, PALEOMAP Project Report No. 90‐0497. Dept Geol Univ Texas, Arlington. 45 pp.</mixed-citation>
         </ref>
         <ref id="rf62">
            <mixed-citation xlink:type="simple" publication-type="">——— 2000 “Paleomap Project” Earth History<ext-link ext-link-type="uri"
                         xlink:href="http://www.scotese.com/earth.htm"
                         xlink:type="simple">http://www.scotese.com/earth.htm</ext-link>; Climate History<ext-link ext-link-type="uri"
                         xlink:href="http://www.scotese.com/climate.htm"
                         xlink:type="simple">http://www.scotese.com/climate.htm</ext-link>Accessed October 10, 2000.</mixed-citation>
         </ref>
         <ref id="rf63">
            <mixed-citation xlink:type="simple" publication-type="">Seward AC 1919 Fossil plants. Vol 4. Ginkgoales, Coniferales, Gnetales. Cambridge University Press, London. 543 pp.</mixed-citation>
         </ref>
         <ref id="rf64">
            <mixed-citation xlink:type="simple" publication-type="">Silba J 1986 Encyclopaedia Coniferae. Phytol Mem 8:1–217.</mixed-citation>
         </ref>
         <ref id="rf65">
            <mixed-citation xlink:type="simple" publication-type="">Smith AG, DG Smith, BM Funnell 1994 Atlas of Mesozoic and Cenozoic coastlines. Cambridge University Press, New York. 99 pp.</mixed-citation>
         </ref>
         <ref id="rf66">
            <mixed-citation xlink:type="simple" publication-type="">Tiffney BH 1985 The Eocene North Atlantic land bridge: its importance in Tertiary and modern phytogeography of the Northern Hemisphere. J Arnold Arbor Harv Univ 66a:243–273.</mixed-citation>
         </ref>
         <ref id="rf67">
            <mixed-citation xlink:type="simple" publication-type="">——— 1995 An estimate of the Early Tertiary paleoclimate of the southern Arctic. Pages 267–296<italic>in</italic>MC Boulter, HC Fisher, eds. Cenozoic plants and climates of the Arctic. NATO ASI ser 1: global environmental change, no. 27. Springer, Berlin.</mixed-citation>
         </ref>
         <ref id="rf68">
            <mixed-citation xlink:type="simple" publication-type="">Upchurch GR, JA Doyle 1981 Paleoecology of the conifers<italic>Frenelopsis</italic>and<italic>Pseudofenelopsis</italic>(Cheirolepidiaceae) from the Cretaceous Potomac Group of Maryland and Virginia. Pages 167–202<italic>in</italic>RC Romans, ed. Geobotany I. Plenum, New York.</mixed-citation>
         </ref>
         <ref id="rf69">
            <mixed-citation xlink:type="simple" publication-type="">Vakhrameev VA 1991 Jurassic and Cretaceous floras and climates of the Earth. Cambridge University Press, Cambridge. 318 pp.</mixed-citation>
         </ref>
         <ref id="rf70">
            <mixed-citation xlink:type="simple" publication-type="">Velenovský J 1885 Die Gymnospermen der bömischen Kreideformation. Greger, Prague. 34 pp.</mixed-citation>
         </ref>
         <ref id="rf71">
            <mixed-citation xlink:type="simple" publication-type="">Velenovský J, L Viniklář 1931 Flora cretacea Bohemiae. IV. Rozpr St Geol Ust 4:1–66.</mixed-citation>
         </ref>
         <ref id="rf72">
            <mixed-citation xlink:type="simple" publication-type="">Walter H 1973 Vegetation of the earth and ecological systems of the geo‐biosphere. 2d ed. Springer, New York. 274 pp.</mixed-citation>
         </ref>
         <ref id="rf73">
            <mixed-citation xlink:type="simple" publication-type="">Walther H 1995 Invasion of Arcto‐Tertiary elements in the Palaeogene of central Europe. Pages 239–250<italic>in</italic>MC Boulter, HC Fisher, eds. Cenozoic plants and climates of the Arctic. NATO ASI ser. 1: global environmental change, no. 27. Springer, Berlin.</mixed-citation>
         </ref>
         <ref id="rf74">
            <mixed-citation xlink:type="simple" publication-type="">White R 1983 The vegetation of Africa: a descriptive memoir to accompany the UNESCO/AETFAT/UNSO vegetation map of Africa. UNESCO, Paris. 355 pp.</mixed-citation>
         </ref>
         <ref id="rf75">
            <mixed-citation xlink:type="simple" publication-type="">Wolfe JA, HM Pakiser 1971 Stratigraphic interpretations of some Cretaceous microfossil floras of the Middle Atlantic States. US Geol Surv Prof Pap 750‐B:35–47.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
