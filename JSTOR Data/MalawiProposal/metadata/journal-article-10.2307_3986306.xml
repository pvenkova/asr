<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">worlbankreseobse</journal-id>
         <journal-id journal-id-type="jstor">j101232</journal-id>
         <journal-title-group>
            <journal-title>The World Bank Research Observer</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>World Bank Publications</publisher-name>
         </publisher>
         <issn pub-type="ppub">02573032</issn>
         <issn pub-type="epub">15646971</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3986306</article-id>
         <title-group>
            <article-title>The Three Faces of the International Comparison Project</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Irving B.</given-names>
                  <surname>Kravis</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1986</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i382388</issue-id>
         <fpage>3</fpage>
         <lpage>26</lpage>
         <page-range>3-26</page-range>
         <permissions>
            <copyright-statement/>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3986306"/>
         <abstract>
            <p>Purchasing power parities (PPPs), this article confirms, are the correct converters for translating GDP and its components from own-currencies to dollars (the usual numeraire); the alternative measure, exchange rates, obscures the relationship between the quantity aggregates of different countries. Drawing on the reports of the United Nations International Comparison Project (ICP), the article contends that exchange rates systematically understate the purchasing power of the currencies of low-income countries and thus exaggerate the dispersion of national per capita incomes. Where full-scale (benchmark) PPP estimates are not available, estimates based on shortcut methods better approximate what the benchmark estimates would be than do the exchange rate conversions. The ICP results also illuminate price and exchange rate relationships among countries by providing a measure of the difference in the levels of prices in different countries. ICP price comparisons for components of GDP make possible the analysis of comparative price and quantity structures of different countries and provide the raw materials for many types of analytical studies.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d820e119a1310">
            <label>1</label>
            <mixed-citation id="d820e126" publication-type="other">
Kravis, Kenessey, Heston, and
Summers 1975</mixed-citation>
            <mixed-citation id="d820e135" publication-type="other">
Kravis, Heston, and Summers 1978a</mixed-citation>
            <mixed-citation id="d820e141" publication-type="other">
Kravis, Heston, and Summers
1982</mixed-citation>
         </ref>
         <ref id="d820e151a1310">
            <label>3</label>
            <mixed-citation id="d820e158" publication-type="other">
United Nations 1984</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d820e174a1310">
            <mixed-citation id="d820e178" publication-type="book">
Beckerman, Wilfred. 1966. International Comparisons of Real Income. Paris: Organisation
for Economic Co-operation and Development (OECD) Development Center.<person-group>
                  <string-name>
                     <surname>Beckerman</surname>
                  </string-name>
               </person-group>
               <source>International Comparisons of Real Income</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d820e203a1310">
            <mixed-citation id="d820e207" publication-type="journal">
Bhagwati, Jagdish N. 1984. "Why Are Services Cheaper in the Poor Countries?" Economic
Journal94 (June): 279-86.<object-id pub-id-type="doi">10.2307/2232350</object-id>
               <fpage>279</fpage>
            </mixed-citation>
         </ref>
         <ref id="d820e223a1310">
            <mixed-citation id="d820e229" publication-type="book">
Board of Trade. 1908a. Report on an Enquiry by the Board of Trade into Working Class
Rents, Housing and Retail Prices Together with the Rates of Wages in Certain Occupations
in the Principal Industrial Towns of the United Kingdom. Cd. 3864. London: Printed for
H.M. Stationery Office by Darling &amp; Son.<person-group>
                  <string-name>
                     <surname>Board of Trade</surname>
                  </string-name>
               </person-group>
               <source>Report on an Enquiry by the Board of Trade into Working Class Rents, Housing and Retail Prices Together with the Rates of Wages in Certain Occupations in the Principal Industrial Towns of the United Kingdom</source>
               <year>1908</year>
            </mixed-citation>
         </ref>
         <ref id="d820e261a1310">
            <mixed-citation id="d820e265" publication-type="other">
—. 1908b. German Towns. Cd. 4032.</mixed-citation>
         </ref>
         <ref id="d820e273a1310">
            <mixed-citation id="d820e277" publication-type="other">
—. 1909. French Towns. Cd. 4512.</mixed-citation>
         </ref>
         <ref id="d820e284a1310">
            <mixed-citation id="d820e288" publication-type="other">
—. 1910. Towns of Belgium. Cd. 5065.</mixed-citation>
         </ref>
         <ref id="d820e295a1310">
            <mixed-citation id="d820e299" publication-type="other">
—. 1911. American Towns. Cd. 5609.</mixed-citation>
         </ref>
         <ref id="d820e306a1310">
            <mixed-citation id="d820e310" publication-type="journal">
Braithwaite, Stanley N. 1968. "Real Income Levels in Latin America." Review of Income and
Wealth14, no. 2 (June): 113-82.<person-group>
                  <string-name>
                     <surname>Braithwaite</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>113</fpage>
               <volume>14</volume>
               <source>Review of Income and Wealth</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d820e345a1310">
            <mixed-citation id="d820e349" publication-type="book">
Clark, Colin. 1940. The Conditions of Economic Progress. London: Macmillan.<person-group>
                  <string-name>
                     <surname>Clark</surname>
                  </string-name>
               </person-group>
               <source>The Conditions of Economic Progress</source>
               <year>1940</year>
            </mixed-citation>
         </ref>
         <ref id="d820e371a1310">
            <mixed-citation id="d820e375" publication-type="journal">
Economic Commission for Europe. 1980. Economic Bulletin for Europe31 (2).<person-group>
                  <string-name>
                     <surname>Economic Commission for Europe</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <volume>31</volume>
               <source>Economic Bulletin for Europe</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d820e405a1310">
            <mixed-citation id="d820e409" publication-type="book">
Gilbert, Milton, and Irving B. Kravis. 1954. An International Comparison of National
Products and the Purchasing Power of Currencies: A Study of the United States, the United
Kingdom, France, Germany, and Italy. Paris: Organisation for European Economic
Cooperation (OEEC).<person-group>
                  <string-name>
                     <surname>Gilbert</surname>
                  </string-name>
               </person-group>
               <source>An International Comparison of National Products and the Purchasing Power of Currencies: A Study of the United States, the United Kingdom, France, Germany, and Italy</source>
               <year>1954</year>
            </mixed-citation>
         </ref>
         <ref id="d820e441a1310">
            <mixed-citation id="d820e445" publication-type="book">
Gilbert, Milton, and associates. 1958. Comparative National Products and Price Levels. Paris:
OEEC.<person-group>
                  <string-name>
                     <surname>Gilbert</surname>
                  </string-name>
               </person-group>
               <source>Comparative National Products and Price Levels</source>
               <year>1958</year>
            </mixed-citation>
         </ref>
         <ref id="d820e470a1310">
            <mixed-citation id="d820e474" publication-type="journal">
Hill, T. P. 1976. "Review of A System of International Comparisons." Economic Journal
(March): 161-64.<object-id pub-id-type="doi">10.2307/2230984</object-id>
               <fpage>161</fpage>
            </mixed-citation>
         </ref>
         <ref id="d820e490a1310">
            <mixed-citation id="d820e494" publication-type="journal">
IMF (International Monetary Fund). 1984a. International Financial Statistics37, no. 7(July).<person-group>
                  <string-name>
                     <surname>IMF (International Monetary Fund)</surname>
                  </string-name>
               </person-group>
               <issue>7</issue>
               <volume>37</volume>
               <source>International Financial Statistics</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d820e523a1310">
            <mixed-citation id="d820e527" publication-type="book">
.1984b. Yearbook of International Financial Statistics. Washington, D.C.<person-group>
                  <string-name>
                     <surname>IMF (International Monetary Fund)</surname>
                  </string-name>
               </person-group>
               <source>Yearbook of International Financial Statistics</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d820e549a1310">
            <mixed-citation id="d820e553" publication-type="journal">
.1985. International Financial Statistics38, no. 7(July).<person-group>
                  <string-name>
                     <surname>IMF (International Monetary Fund)</surname>
                  </string-name>
               </person-group>
               <issue>7</issue>
               <volume>38</volume>
               <source>International Financial Statistics</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d820e583a1310">
            <mixed-citation id="d820e587" publication-type="journal">
Isard, Peter. 1983. "Review of A System of International Comparisons." Journal of Interna-
tional Economics15 (August): 177-81.<person-group>
                  <string-name>
                     <surname>Isard</surname>
                  </string-name>
               </person-group>
               <issue>August</issue>
               <fpage>177</fpage>
               <volume>15</volume>
               <source>Journal of International Economics</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d820e622a1310">
            <mixed-citation id="d820e626" publication-type="journal">
Isenman, Paul. 1980. "Inter-Country Comparison of 'Real' (Ppp) Incomes: Revised Estimates
and Unresolved Questions." World Development8, no. 1 (January): 61-72.<person-group>
                  <string-name>
                     <surname>Isenman</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>61</fpage>
               <volume>8</volume>
               <source>World Development</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d820e661a1310">
            <mixed-citation id="d820e665" publication-type="book">
King, Gregory. 1936. Two Tracts. George C. Barnett, ed. Baltimore, Md.: Johns Hopkins
University Press.<person-group>
                  <string-name>
                     <surname>King</surname>
                  </string-name>
               </person-group>
               <source>Two Tracts</source>
               <year>1936</year>
            </mixed-citation>
         </ref>
         <ref id="d820e690a1310">
            <mixed-citation id="d820e694" publication-type="journal">
Kravis, Irving B. 1984. "Comparative Studies of National Incomes and Prices." Journal of
Economic Literature22 (March): 1-39.<object-id pub-id-type="jstor">10.2307/2725225</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d820e710a1310">
            <mixed-citation id="d820e714" publication-type="book">
Kravis, Irving B., Alan W. Heston, and Robert Summers. 1978a. International Comparisons
of Real Product and Purchasing Power. Baltimore, Md.: Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>Kravis</surname>
                  </string-name>
               </person-group>
               <source>International Comparisons of Real Product and Purchasing Power</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d820e739a1310">
            <mixed-citation id="d820e743" publication-type="journal">
.1978b. "Real GDP Per Capita for More than One Hundred Countries." Economic
Journal88, no. 350 (June): 215-42.<object-id pub-id-type="doi">10.2307/2232127</object-id>
               <fpage>215</fpage>
            </mixed-citation>
         </ref>
         <ref id="d820e760a1310">
            <mixed-citation id="d820e764" publication-type="book">
_ .1982. World Product and Income: International Comparisons of Real Gross Product.
Baltimore, Md.: Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>Kravis</surname>
                  </string-name>
               </person-group>
               <source>World Product and Income: International Comparisons of Real Gross Product</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d820e789a1310">
            <mixed-citation id="d820e793" publication-type="book">
Kravis, Irving B., Zoltan Kenessey, Alan W. Heston, and Robert Summers. 1975. A System
of International Comparisons of Gross Product and Purchasing Power. Baltimore, Md.:
Johns Hopkins University Press.<person-group>
                  <string-name>
                     <surname>Kravis</surname>
                  </string-name>
               </person-group>
               <source>A System of International Comparisons of Gross Product and Purchasing Power</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d820e822a1310">
            <mixed-citation id="d820e826" publication-type="book">
Kravis, Irving B., and Robert E. Lipsey. 1983. Towards an Explanation of National Price
Levels. Special Studies in International Finance no. 52. Princeton, N.J.: Princeton Univers-
ity Press.<person-group>
                  <string-name>
                     <surname>Kravis</surname>
                  </string-name>
               </person-group>
               <source>Towards an Explanation of National Price Levels</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d820e855a1310">
            <mixed-citation id="d820e859" publication-type="book">
. Forthcoming. "The Assessment of National Price Levels." In S. Arndt and D.
Richardson, eds. Real-Financial Linkages in Open Economies. Washington, D.C.: Ameri-
can Enterprise Institute.<person-group>
                  <string-name>
                     <surname>Kravis</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Assessment of National Price Levels</comment>
               <source>Real-Financial Linkages in Open Economies</source>
            </mixed-citation>
         </ref>
         <ref id="d820e888a1310">
            <mixed-citation id="d820e892" publication-type="book">
Maddison, Angus. 1970. Economic Progress and Policy in Developing Countries. New York:
Norton.<person-group>
                  <string-name>
                     <surname>Maddison</surname>
                  </string-name>
               </person-group>
               <source>Economic Progress and Policy in Developing Countries</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d820e917a1310">
            <mixed-citation id="d820e921" publication-type="journal">
_ . 1983. "A Comparison of the Levels of GDP Per Capita in Developed and Developing
Countries, 1790-1980." Journal of Economic History (March): 27-41.<object-id pub-id-type="jstor">10.2307/2120259</object-id>
               <fpage>27</fpage>
            </mixed-citation>
         </ref>
         <ref id="d820e938a1310">
            <mixed-citation id="d820e942" publication-type="book">
Paige, Deborah, and Gottfried Bombach. 1959. A Comparison of National Output and
Productivity in the United Kingdom and the United States. Paris: OEEC.<person-group>
                  <string-name>
                     <surname>Paige</surname>
                  </string-name>
               </person-group>
               <source>A Comparison of National Output and Productivity in the United Kingdom and the United States</source>
               <year>1959</year>
            </mixed-citation>
         </ref>
         <ref id="d820e967a1310">
            <mixed-citation id="d820e971" publication-type="journal">
Salazar-Carrillo, Jorge. 1973. "Price, Purchasing Power and Real Product Comparisons in
Latin America." Review of Income and Wealth19, no. 1 (March): 117-32.<person-group>
                  <string-name>
                     <surname>Salazar-Carrillo</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>117</fpage>
               <volume>19</volume>
               <source>Review of Income and Wealth</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1006a1310">
            <mixed-citation id="d820e1010" publication-type="book">
SOEC (Statistical Office of the European Community). 1983. Comparisons in Real Values of
the Aggregates of ESA, 1980. Luxembourg: European Economic Communities (EEC).<person-group>
                  <string-name>
                     <surname>SOEC (Statistical Office of the European Community)</surname>
                  </string-name>
               </person-group>
               <source>Comparisons in Real Values of the Aggregates of ESA, 1980</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1035a1310">
            <mixed-citation id="d820e1039" publication-type="book">
.1985a. Comparison of Price Levels and Economic Aggregates: The Results for
African Countries. Luxembourg: EEC.<person-group>
                  <string-name>
                     <surname>SOEC (Statistical Office of the European Community)</surname>
                  </string-name>
               </person-group>
               <source>Comparison of Price Levels and Economic Aggregates: The Results for African Countries</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1064a1310">
            <mixed-citation id="d820e1068" publication-type="book">
_ . 1985b. Comparison of National Accounts Aggregates between Israel and the
European Community. Luxembourg: EEC.<person-group>
                  <string-name>
                     <surname>SOEC (Statistical Office of the European Community)</surname>
                  </string-name>
               </person-group>
               <source>Comparison of National Accounts Aggregates between Israel and the European Community</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1093a1310">
            <mixed-citation id="d820e1097" publication-type="journal">
Summers, Robert. 1973. "International Comparisons Based upon Incomplete Data." Review
of Income and Wealth19, no. 1 (March): 1-16.<person-group>
                  <string-name>
                     <surname>Summers</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>1</fpage>
               <volume>19</volume>
               <source>Review of Income and Wealth</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1133a1310">
            <mixed-citation id="d820e1137" publication-type="journal">
Summers, Robert, and Alan W. Heston. 1984. "Improved International Comparisons of Real
Product and Its Composition: 1950-80." Review of Income and Wealth30, no. 2 (June):
207-62.<person-group>
                  <string-name>
                     <surname>Summers</surname>
                  </string-name>
               </person-group>
               <issue>2</issue>
               <fpage>207</fpage>
               <volume>30</volume>
               <source>Review of Income and Wealth</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1175a1310">
            <mixed-citation id="d820e1179" publication-type="book">
United Nations. 1968. A System of National Accounts. Studies in Methods, ser. F, no. 2, rev.
3. New York: United Natons Statistical Office.<person-group>
                  <string-name>
                     <surname>United Nations</surname>
                  </string-name>
               </person-group>
               <source>A System of National Accounts</source>
               <year>1968</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1204a1310">
            <mixed-citation id="d820e1208" publication-type="book">
United Nations, Economic and Social Council, Statistical Commission and Economic
Commission for Europe, Conference of European Statisticians. Summary of the Results of
the European Comparison Programme CES/514 (May). New York.<person-group>
                  <string-name>
                     <surname>United Nations, Economic and Social Council, Statistical Commission and Economic Commission for Europe, Conference of European Statisticians</surname>
                  </string-name>
               </person-group>
               <source>Summary of the Results of the European Comparison Programme CES/514 (May)</source>
            </mixed-citation>
         </ref>
         <ref id="d820e1233a1310">
            <mixed-citation id="d820e1237" publication-type="book">
Ward, Michael. 1985. Purchasing Power Parities and Real Expenditures in the OECD. Paris:
OECD.<person-group>
                  <string-name>
                     <surname>Ward</surname>
                  </string-name>
               </person-group>
               <source>Purchasing Power Parities and Real Expenditures in the OECD</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d820e1262a1310">
            <mixed-citation id="d820e1266" publication-type="book">
World Bank. 1983. 1983 World Bank Atlas. Washington, D.C.<person-group>
                  <string-name>
                     <surname>World Bank</surname>
                  </string-name>
               </person-group>
               <source>1983 World Bank Atlas</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
