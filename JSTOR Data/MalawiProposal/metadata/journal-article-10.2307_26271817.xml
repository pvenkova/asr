<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">consecol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015597</journal-id>
         <journal-title-group>
            <journal-title>Conservation Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">11955449</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26271817</article-id>
         <article-categories>
            <subj-group>
               <subject>Report</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Spatial Modeling of Risk in Natural Resource Management</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Jones</surname>
                  <given-names>Peter G.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Thornton</surname>
                  <given-names>Philip K.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>CIAT (International Center for Tropical Agriculture);</aff>
            <aff id="af2">
               <label>²</label>International Livestock Research Institute</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2002</year>
            <string-date>Jan 2002</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">5</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26271806</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2002 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26271817"/>
         <abstract>
            <label>ABSTRACT</label>
            <p>Making decisions in natural resource management involves an understanding of the risk and uncertainty of the outcomes, such as crop failure or cattle starvation, and of the normal spread of the expected production. Hedging against poor outcomes often means lack of investment and slow adoption of new methods. At the household level, production instability can have serious effects on income and food security. At the national level, it can have social and economic impacts that may affect all sectors of society. Crop models such as CERES-Maize are excellent tools for assessing weather-related production variability. WATBAL is a water balance model that can provide robust estimates of the potential growing days for a pasture. These models require large quantities of daily weather data that are rarely available. MarkSim is an application for generating synthetic daily weather files by estimating the third-order Markov model parameters from interpolated climate surfaces. The models can then be run for each distinct point on the map. This paper examines the growth of maize and pasture in dryland agriculture in southern Africa. Weather simulators produce independent estimates for each point on the map; however, we know that a spatial coherence of weather exists. We investigated a method of incorporating spatial coherence into MarkSim and show that it increases the variance of production. This means that all of the farmers in a coherent area share poor yields, with important consequences for food security, markets, transport, and shared grazing lands. The long-term aspects of risk are associated with global climate change. We used the results of a Global Circulation Model to extrapolate to the year 2055. We found that low maize yields would become more likely in the marginal areas, whereas they may actually increase in some areas. The same trend was found with pasture growth. We outline areas where further work is required before these tools and methods can address natural resource management problems in a comprehensive manner at local community and policy levels.</p>
         </abstract>
         <kwd-group>
            <label>KEY WORDS:</label>
            <kwd>crop modeling</kwd>
            <kwd>dryland agriculture</kwd>
            <kwd>global change</kwd>
            <kwd>Global Circulation Model</kwd>
            <kwd>maize</kwd>
            <kwd>Markov models</kwd>
            <kwd>MarkSim</kwd>
            <kwd>natural resource management</kwd>
            <kwd>risk</kwd>
            <kwd>southern Africa</kwd>
            <kwd>spatial modeling</kwd>
            <kwd>weather simulation</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Batjes, N. H., editor. 1995. A homogenized soil data file for global environmental research: a subset of FAO, ISRIC, and NRCS profiles (Version 1.0). Working Paper 95/10, International Soil Reference and Information Centre (ISRIC), Wageningen, The Netherlands.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Batjes, N. H., and E. M. Bridges. 1994. Potential emissions of radiatively active trace gases from soil to atmosphere with special reference to methane: development of a global database (WISE). Journal of Geophysical Research 99(D8):16, 479-489.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Cullen, M. J. P. 1993. The unified forecast/climate model. The Meteorological Magazine 122:81-95.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Durand, W., and A. S. du Toit. 2000. Using crop growth models and GIS to address issues influencing sustainability in the short, medium and long term. Pages 101-105 in W. Durand and A. S. du Toit, editors. Proceedings of the Highveld Ecoregion Workshop on Methodology and Strategy Development Using Systems Analysis for Sustainability in the South African Highveld Ecoregion. ARC-Grain Crops Institute, Potchefstroom, South Africa.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Eastman, J. R. 1993. IDRISI Version 4.1 Technical Reference Manual. Clark University, Worcester, Massachusetts, USA.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">FAO (Food and Agriculture Organization). 1974 . FAO-UNESCO soil map of the world 1:5 000 000. Volume 1, Legend. UNESCO, Paris, France, and FAO, Rome, Italy.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">FAO (Food and Agriculture Organization). 1978. Report on the AgroEcological Zones project. Volume 1. Methodology and results for Africa. World Soil Resources Report 48. UNESCO, Paris, France, and FAO, Rome, Italy.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">FAO (Food and Agriculture Organization). 1995. Digital soil map of the world and derived soil properties. Version 3.5. Land and Water Digital Media Series 1, FAO, Rome, Italy.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Hartkamp, A. D., K. de Beurs, A. Stein, and J. W. White. 1999. Interpolation techniques for climate variables. NRGGIS Series 99-01. International Maize and Wheat Improvement Center (CIMMYT), Mexico City, Mexico.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Hassan, R. M., editor. 1998. Maize technology development and transfer. A GIS application for research planning in Kenya. CAB International, Wallingford, UK.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Hulme, M., editor. 1996. Climate change and southern Africa: An exploration of some potential impacts and implications in the SADC region. Report commissioned by WWF International and co-ordinated by the Climatic Research Unit, University of East Anglia, Norwich, UK.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">IPCC (Intergovernmental Panel on Climate Change). 2001a. The regional impacts of climate change: an assessment of vulnerability. [Online] URL: http://www.ipcc.ch</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">IPCC (Intergovernmental Panel on Climate Change). 2001b. Climate change 2001: the scientific basis. Working Group 1 contribution to the IPCC Third Assessment Report. [Online] URL: http://www.ipcc.ch</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Johns, T. C., R. E. Carnell, J. F. Crossley, J. M. Gregory, J. F. B. Mitchell, C. A. Senior, S. F. B. Tett, and R. A. Wood. 1997. The second Hadley Centre coupled ocean–atmosphere GCM: model description, spinup and validation. Climate Dynamics 13:103-134.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Jones, P. G., and P. K. Thornton. 1993. A rainfall generator for agricultural applications in the tropics. Agricultural and Forest Meteorology 63:1-19.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Jones, P. G., and P. K. Thornton. 1997. Spatial and temporal variability of rainfall related to a third-order Markov model. Agricultural and Forest Meteorology 86:127-138.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Jones, P. G., and P. K. Thornton. 1999. Fitting a third-order Markov rainfall model to interpolated climate surfaces. Agricultural and Forest Meteorology 97:213-231.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Jones, P. G., and P. K. Thornton. 2000. MarkSim: Software to generate daily weather data for Latin America and Africa. Agronomy Journal 92(3):445-453.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">McDermott, J. J., P. M. Kristjanson, R. L. Kruska, R. S. Reid, T. P. Robinson, P. G. Coleman, P. G. Jones, and P. K. Thornton. 2001. Effects of climate, human population and socio-economic changes on tsetse-transmitted trypanosomosis to 2050. In S. J. Black and J. R. Seed, editors. World class parasites. Volume 1. The African Trypanosomes. Kluwer Academic Press, Dordrecht, The Netherlands. In press.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Mitchell, J. F. B., T. C. Johns, J. M. Gregory, and S. Tett. 1995. Climate response to increasing levels of greenhouse gases and sulphate aerosols. Nature 376:501-504.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Muchena, P., and A. Iglesias. 1995. Vulnerability of maize yields to climate change in different farming sectors in Zimbabwe. Pages 229-239 in C. Rosenzweig, L. H. Allen Jr., L. A. Harper, S. E. Hollinger, and J. W. Jones, editors. Climate change and agriculture: analysis of potential international impacts. American Society of Agronomy. Special Publication Number 59. Madison, Wisconsin, USA.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">NOAA (National Oceanographic and Atmospheric Administration). 1984. TGP-OO6 D. Computer-compatible tape. NOAA, Boulder, Colorado, USA.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Richardson, C. W. 1985. Weather simulation for crop management models. Transactions of the American Society of Agricultural Engineers 28(5):1602-1606.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Ritchie, J. T., U. Singh, D. C. Godwin, and W. T. Bowen. 1998. Cereal growth, development and yield. Pages 79-98 in G. Y. Tsuji, G. Hoogenboom, and P. K. Thornton, editors. Understanding options for agricultural production. Kluwer Academic, Dordrecht, The Netherlands.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Rogers, D. J., and S. E. Randolph. 2000. The global spread of malaria in a future, warmer world. Science 289:1763- 1766.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Schulze, R. 2000. Transcending scales of space and time in impact studies of climate and climate change on agrohydrological responses. Agriculture, Ecosystems and Environment 82:185-212.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Staal, S. J., S. Ehui, and J. C. Tanner. 2001. Livestock–environment interactions under intensifying production. Pages 345-364 in D. R. Lee and C. B. Barrett, editors. Tradeoffs or synergies? Agricultural intensification, economic development and the environment. CAB International, Wallingford, UK.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Thornton, P. K., and M. Herrero. 2001. Integrated crop-livestock simulation models for scenario analysis and impact assessment. Agricultural Systems 70(2-3):581-602.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Thornton, P. K., and P. W. Wilkens. 1998. Risk assessment and food security. Pages 329-346 in G. Y. Tsuji, G. Hoogenboom, and P. K. Thornton, editors. Understanding options for agricultural production. Kluwer Academic, Dordrecht, The Netherlands.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Thornton, P. K., A. R. Saka, U. Singh, J. D. T. Kumwenda, J. E. Brink, and J. B. Dent. 1995. Application of a maize crop simulation model in the central region of Malawi. Experimental Agriculture 31:213-226.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Tsuji, G. Y., G. Hoogenboom, and P. K. Thornton, editors. 1998. Understanding options for agricultural production. Kluwer Academic, Dordrecht, The Netherlands.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Walker, T. S., and J. G. Ryan. 1990. Village and household economics in India’s semi-arid tropics. Johns Hopkins University Press, Baltimore, Maryland, USA.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
