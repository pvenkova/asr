<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt125wrc</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/ocam15014</book-id>
      <subj-group>
         <subject content-type="call-number">HC59.7.O263 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Economic policy</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Growth and Policy in Developing Countries</book-title>
         <subtitle>A Structuralist Approach</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Ocampo</surname>
               <given-names>José Antonio</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Rada</surname>
               <given-names>Codrina</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib3">
            <name name-style="western">
               <surname>Taylor</surname>
               <given-names>Lance</given-names>
            </name>
         </contrib>
         <contrib contrib-type="other" id="contrib4">
            <role>with contributions from</role>
            <name name-style="western">
               <surname>Parra</surname>
               <given-names>Mariángela</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>22</day>
         <month>10</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="epub">9780231520836</isbn>
      <isbn content-type="epub">0231520832</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/ocam15014"/>
      <abstract abstract-type="short">
         <p>Economic structuralists use a broad, systemwide approach to understanding development, and this textbook assumes a structuralist perspective in its investigation of why a host of developing countries have failed to grow at 2 percent or more since 1960. Sensitive to the wide range of factors that affect an economy's strength and stability, the authors identify the problems that have long frustrated growth in many parts of the developing world while suggesting new strategies and policies to help improve standards of living.</p>
         <p>After a survey of structuralist methods and post-World War II trends of global economic growth, the authors discuss the role that patterns in productivity, production structures, and capital accumulation play in the growth dynamics of developing countries. Next, it outlines the evolution of trade patterns and the effect of the terms of trade on economic performance, especially for countries that depend on commodity exports.</p>
         <p>The authors acknowledge the structural limits of macroeconomic policy, highlighting the negative effects of financial volatility and certain financial structures while recommending policies to better manage external shocks. These policies are then further developed through a discussion of growth and structural improvements, and are evaluated according to which policy options-macro, industrial, or commercial-best fit within different kinds of developing economies.</p>
      </abstract>
      <counts>
         <page-count count="200"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.3</book-part-id>
                  <title-group>
                     <title>Figures</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.4</book-part-id>
                  <title-group>
                     <title>Tables</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Economic Structure, Policy, and Growth</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Almost a decade into the twenty-first century, absolute poverty still pervades outside the industrialized world. Helping poor people in developing countries improve their standards of living is on the short list of international policy goals. There are a multitude of ideas about how poverty should be analyzed and attacked. Although there have been some success stories, particularly in East Asia, the unhappy truth is that anti-poverty programs in developing countries have quite often failed or have had limited success.¹</p>
                     <p>The reason is that they did not enable poor economies to generate long-term growth of real per capita income. A useful</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Growth and Policy Space in Historical Terms</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <role>WITH</role>
                        <name name-style="western">
                           <surname>PARRA</surname>
                           <given-names>MARIÁNGELA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>25</fpage>
                  <abstract abstract-type="extract">
                     <p>In this chapter, we take up the history of income growth in developing economies and the changes in the international environment in which they operate. Details about structural change in several dimensions are presented in the following two chapters.</p>
                     <p>Observation of regional growth experiences in now rich and poor countries since the early nineteenth century clearly shows that there has been no overall convergence of per capita incomes. Instead, the income gap widened considerably in both relative and absolute terms. The growth rates of GDP per capita for the countries that were most advanced in 1820 were the highest throughout</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Growth Rates, Economic Structures, and Energy Use</title>
                  </title-group>
                  <fpage>37</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter is about the growth and development performance of non-industrialized countries in the latter part of the twentieth century, in particular the “great divergence” of their growth rates of per capita GDP from those of the industrial world since around 1980 until the early part of the 2000s that was illustrated in chapter 2. The goal is to explore the factors underlying observed patterns of growth and trace out plausible lines of causation for their diversity. The analysis follows Kuznets (1966) in attempting to organize the data in such a way as to highlight salient relationships, or the lack</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Open Economies and Patterns of Trade</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <role>WITH</role>
                        <name name-style="western">
                           <surname>PARRA</surname>
                           <given-names>MARIÁNGELA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>58</fpage>
                  <abstract abstract-type="extract">
                     <p>This chapter takes up the relationship between foreign trade and growth in developing countries in the latter part of the twentieth century. Regional diversity was again the rule, with changing patterns of trade accompanying structural transformation. Fast-growing regions generally recorded increases in shares of manufactured exports with mid- and high-technological content, the most impressive being the Tigers and, in its speed of transformation, China. Recently in some countries, economic growth has been associated with specialization in dynamic services such as information and communications technologies, with India standing out in this area. In the slow-growing regions on the other hand, trade</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Patterns of Net Borrowing in Open Developing Economies</title>
                  </title-group>
                  <fpage>75</fpage>
                  <abstract abstract-type="extract">
                     <p>This short and final empirical chapter looks at net-lending flows—incomes minus expenditures—over time for the government, private, and rest of the world normalized in all cases by GDP. Long debates and many policy recommendations have followed from the interpretation of how net lending by different sectors relate to each other. We therefore review the conceptual debate first. This review also serves as an introduction to the short-term macroeconomic analysis of chapter 7.</p>
                     <p>As an accounting identity, of course, total net borrowings must sum to zero:</p>
                     <p>(Private Investment - Savings) + (Public Spending - Taxes) + (Exports - Imports)</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Financial Structures</title>
                  </title-group>
                  <fpage>84</fpage>
                  <abstract abstract-type="extract">
                     <p>The financial side of an economy undergoes structural change through the development process. There is no strict progression of financial development and initial conditions matter. But, broadly speaking, new financial structures gradually evolve, in a process that can be seen as a sequence of five “stages” of increasing financial complexity. Understanding financial structures is crucial for the analysis of macroeconomic policy in chapter 7, since they can increase policy flexibility but can also constrain policy maneuver and generate risks of destabilization for the entire macroeconomy.</p>
                     <p>The underlying concepts are a blend of flows of funds accounting and more traditional approaches.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Macroeconomic Policy Choices</title>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract abstract-type="extract">
                     <p>What options do the economic authorities in developing economies have for policy formation, at the macro and sectoral levels? Limits on policy maneuverability vary greatly across economies. In this chapter, we try to sort out the possibilities regarding macroeconomic regulation and then take up growth and sectoral policy in chapter 8.</p>
                     <p>We start by looking at how private and government net-borrowing flows and current account balances interact in the short-to-medium run. Some algebraic backup is provided in appendix 7.1, which deals with gap models, relationships between flow and stock variables, and theories of the exchange rate.</p>
                     <p>Macroeconomic policy packages appropriate</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Growth and Sectoral Policy</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract abstract-type="extract">
                     <p>Policies regarding growth and sectoral strategies to support long-term structural transformation are the focus of this chapter. A theory of growth for a developing economy is the first topic. It serves as the background for analyzing policy frameworks for industry and agriculture and their interactions with trade.</p>
                     <p>Kaldor’s (1978, chap. 4) model introduced in chapter 1 is the template for analysis of growth in the “modern” sector of the economy. We then turn to a “dual economy” extension simplified from Rada (2007).¹ The model is used to illustrate the implications of external liberalization packages à la the Washington consensus. A</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Stylized Facts and Policy Alternatives</title>
                  </title-group>
                  <fpage>142</fpage>
                  <abstract abstract-type="extract">
                     <p>The main objective of this book has been to explain why so many parts of the developing world have failed to generate stable rates of growth in per capita incomes, and to offer policy alternatives for them to correct this outcome. The failure of growth was particularly widespread during the quarter century or so after a series of adverse shocks beginning in the late 1970s. This period also coincided with a major shift in development policies toward an emphasis on market liberalization and a retreat from state intervention. History has not been kind to the mainstream interpretation of economic development</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.15</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>159</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">ocam15014.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>169</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
