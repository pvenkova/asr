<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt183gx9h</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt183p82d</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
      </subj-group>
      <book-title-group>
         <book-title>Money and Power</book-title>
         <subtitle>Great Predators in the Political Economy of Development</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Bracking</surname>
               <given-names>Sarah</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>11</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780745320113</isbn>
      <isbn content-type="epub">9781849641807</isbn>
      <publisher>
         <publisher-name>Pluto Press</publisher-name>
         <publisher-loc>London; New York</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Sarah Bracking</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt183p82d"/>
      <abstract abstract-type="short">
         <p>-- Examines why southern states are still experiencing mass poverty after over sixty years of 'development' -- Sarah Bracking explores the role of governments and development finance institutions in managing the markets in which the poorest countries operate. These institutions -- the 'Great Predators' -- are trapping the populations of the south in a permanent cycle of austerity. Bracking examines the political economy relations between states. She shows how pseudo-public 'development' institutions retain complete economic control over Southern markets, yet the international system is itself unregulated. Operating in the interests of North America and the European Union, they have a political purpose, and yet serve to cloud the brute power relations between states. This book will be of interest to anyone studying debt and development, global financial institutions, and the way the world economy is regulated and governed.</p>
      </abstract>
      <counts>
         <page-count count="256"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.3</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bracking</surname>
                           <given-names>Sarah</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The political economy of development</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Every day tens of thousands of workers and ‘beneficiaries’ toil to make development happen: to feed hungry children, to vaccinate against disease, to build schools, roads and airports, to promote good governance and civic education, and to do a host of other activities on an ever-increasing list. Development competes with the great religions of our time, motivating and disciplining, providing moral leadership and proving a clarion call against the neglect of the poor, diseased and incapable. As a social project it carries all the great meanings of the modern age, from the Enlightenment to now, of human progress and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Money in the political economy of development</title>
                  </title-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>Various factors have been included in analysis of the increased impoverishment of the poorer world in the last quarter century or so: declines in commodity prices; negative real interest rates in the mid 1970s changing to high interest rates in 1979 after the Volcker Shock, and even higher in 1981; global recession in the early 1980s and again in the early 1990s; monetary crashes in the late 1980s; the Asian financial crisis of 1998 onward; the excess liquidity of the early 2000s, followed by the sub-prime crash and credit crunch of 2007 to 2008. Over the long period of 30</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Making markets</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>We saw in the last chapter that access to private funds for development for the majority of the poorest countries has been sporadic and difficult since 1982. In Africa, from the onset of the debt crisis, what are termed ‘externalised’ forms of multinational corporation (MNC) involvement became increasingly common, such as subcontracting and production under license; forms of involvement which involve a thin equity base and which are less risky, often incorporating arrangements for assured payment in foreign exchange for services, brand use and royalties for patented processes (Bennell 1994: 14; United Nations Centre for Transnational Corporations (UNCTC) 1989; United</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>International development banks and creditor states</title>
                  </title-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>The Bretton Woods banks and regional development banks (RDBs) (collectively referred to here as international financial institutions (IFIs) or, when the bilateral development finance institutions (DFIs) are included as well, as the ‘Great Predators’) can only generate and regulate markets because they themselves are underwritten and their risk is managed by their joint owners: the rich economies of the global system, principally those who were the ‘winners’ of the Second World War. Since 1948, these have pooled their resources in a global system of public credit. Politically, this system collectivised the management of empire, as the economies of the bilateral</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The British market makers</title>
                  </title-group>
                  <fpage>66</fpage>
                  <abstract>
                     <p>In order to illustrate the historical development of the global multiplier which occupies the centre of the political economy of development (but which is actually a many-to-many system or set of similar bilateral multipliers), this chapter uses a case study of the British state, which has been a key author of power in the international system. The Commonwealth Development Corporation (CDC), Export Credit Guarantee Department (ECGD) and Crown Agents primarily express British economic power in the frontier zone and help regulate and police those economic spaces in favour of British concerns. They are all ultimately underwritten by the Treasury, although</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Poverty in Africa and the history of multilateral aid</title>
                  </title-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>This chapter presents an overview of poverty in African countries and then explores the role of the multilateral aid architecture that has grown up in the last 60 years in ostensibly ameliorating widespread poverty. That majority populations in African countries in particular, as compared to their European, Asian or Latin American counterparts, suffer from acute poverty, is not generally contested. In the United Nations Development Programme’s (UNDP), ‘human development index’ (HDI) for 2007– 08, the lowest ranking 24 countries were in Africa, and of the lowest 50, 38 were African. In 2005, incoming private investment was in single figures or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Derivative business and aid-funded accumulation</title>
                  </title-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>This chapter explores the role of the Great Predators, the bilateral, regional and multilateral development finance institutions (DFIs), in directly sponsoring and underwriting an economy and set of activities in supply and procurement which delivers goods and services to the development industry. In other words, if a country borrows money from the World Bank to fund the construction of a port facility, this in itself then generates contracts for technical assistance, supply of cement and steel, supplies of soft infrastructure such as customs systems, as well as a set of contracts for its actual construction. All of these go to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Private sector development and bilateral interventions</title>
                  </title-group>
                  <fpage>140</fpage>
                  <abstract>
                     <p>Bilateral finance institutions taken together make investments across the developing world and manage foreign direct investment (FDI) from within their institutional structures, using umbrella guarantees which cover their private sector partners. In relation to investment and finance capital per se, they have a very distinctive role, all under the auspices and organising fulcrum of the directly multilateral International Finance Corporation (IFC), of regulating liquidity. In this, the role of linking up businesses in the North and South, as well as consumers and trading partners, is a central effect of bilateral interventions. This chapter examines the theoretical benefits of private sector</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Taking the long view of promoting capitalism</title>
                  </title-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>We saw in chapter 2 how poorer countries must rely on three sources of money: private investment, debt relief and ‘new’ aid, and that in general the poorer a country is, the more it relies on public finance. This chapter looks at a case study of British flows of investment, debt relief and aid, which go abroad to poorer countries. The case study shows, for Britain at least, that while recent noise about increasing the benevolence of the political economy of development has attracted much attention, when you look at the actual numbers involved, it is clear that the system</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Aid effectiveness:</title>
                     <subtitle>what are we measuring?</subtitle>
                  </title-group>
                  <fpage>181</fpage>
                  <abstract>
                     <p>In the last three chapters we have examined profitability within the political economy of aid, both in and of itself in chapter 7 (through direct contracts), and then in terms of the market structures it facilitates in chapters 8 and 9. In this chapter the more mainstream debate on aid effectiveness will be reviewed, to see how the political economy of development is represented within it. We conclude that the debate on aid is generally inconclusive, since the things that are being measured are generally abstracted and rarefied, such as growth, or ‘good governance’. Therefore, it is not a surprise</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>196</fpage>
                  <abstract>
                     <p>In this book we have examined the market structures which confront developing countries wishing to enhance wellbeing in their societies. It has explained how the closure of development opportunities for many African countries occurred in the briefest period of historical time, after possibilities were opened on independence, providing only the shortest interregnum in which the development dream could be wrought and then reigned in again by the Great Predators of global capitalism. The argument has been that power exercised through the Northern states by the wealthy, since around 1982, has increasingly wrought those ‘developmental’ frontiers of the core creditor states</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.16</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>214</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt183p82d.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>233</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
