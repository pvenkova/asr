<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">economica</journal-id>
         <journal-id journal-id-type="jstor">j100144</journal-id>
         <journal-title-group>
            <journal-title>Economica</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Suntory-Toyota International Centre for Economics and Related Disciplines</publisher-name>
         </publisher>
         <issn pub-type="ppub">00130427</issn>
         <issn pub-type="epub">14680335</issn>
         <custom-meta-group>
            <custom-meta>
               <meta-name>journal-series-title</meta-name>
               <meta-value>New Series</meta-value>
            </custom-meta>
         </custom-meta-group>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2554918</article-id>
         <title-group>
            <article-title>Growth, Capital Accumulation and Foreign Debt</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Ross</given-names>
                  <surname>Milbourne</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">64</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">253</issue>
         <issue-id>i343843</issue-id>
         <fpage>1</fpage>
         <lpage>13</lpage>
         <page-range>1-13</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 The London School of Economics and Political Science</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2554918"/>
         <abstract>
            <p>This paper investigates the relationship between growth, population growth, capital accumulation and foreign debt. The paper uses an open economy neoclassical growth model to look at what macroeconomic forces explain why a number of countries accumulated (often unsustainable) debt and others did not. We show that a condition for debt stabilization relates the marginal propensity to consume out of wealth to the population growth rate and real rate of interest. The paper also shows that higher natural population growth must be associated with a higher level of net foreign debt per head, but that the same conclusion does not follow for higher rates of immigration, nor necessarily for higher rates of productivity growth. Finally, the paper characterizes a fiscal policy rule which if followed would prevent economies from entering debt traps following adverse shocks.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1607e120a1310">
            <label>1</label>
            <mixed-citation id="d1607e127" publication-type="book">
Rider (1994: table 11)  </mixed-citation>
         </ref>
         <ref id="d1607e136a1310">
            <label>2</label>
            <mixed-citation id="d1607e143" publication-type="journal">
Fischer and Frenkel (1972)  </mixed-citation>
            <mixed-citation id="d1607e151" publication-type="journal">
Borts (1964)  </mixed-citation>
            <mixed-citation id="d1607e159" publication-type="journal">
Onitsuka (1974)  </mixed-citation>
         </ref>
         <ref id="d1607e168a1310">
            <label>3</label>
            <mixed-citation id="d1607e175" publication-type="journal">
Obstfeld (1982)  </mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1607e193a1310">
            <mixed-citation id="d1607e197" publication-type="journal">
BARRO, R.J. (1990). Government spending in a simple model of endogenous growth. Journal of
Political Economy, 98, S103-25.<object-id pub-id-type="jstor">10.2307/2937633</object-id>
               <fpage>S103</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e213a1310">
            <mixed-citation id="d1607e217" publication-type="journal">
(1991). Economic growth in a cross-section of countries. Quarterly Journal of Economics,
101, 407-43.<object-id pub-id-type="doi">10.2307/2937943</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e233a1310">
            <mixed-citation id="d1607e237" publication-type="journal">
, MANKIW, N. G. and SALA-I-MARTIN, X. (1995). Capital mobility and neoclassical models
of growth. American Economic Review, 85, 103-15.<object-id pub-id-type="jstor">10.2307/2117998</object-id>
               <fpage>103</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e253a1310">
            <mixed-citation id="d1607e257" publication-type="journal">
BORTS, G.H. (1964). A theory of long-run international capital movements. Journal of Political
Economy, 71, 341-59.<object-id pub-id-type="jstor">10.2307/1828391</object-id>
               <fpage>341</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e274a1310">
            <mixed-citation id="d1607e278" publication-type="journal">
BUITER, W.H. (1981). Time preference and international lending and borrowing in an overlap-
ping-generations model. Journal of Political Economy, 89, 769-97.<object-id pub-id-type="jstor">10.2307/1833034</object-id>
               <fpage>769</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e294a1310">
            <mixed-citation id="d1607e298" publication-type="book">
DORNBUSCH, R. (1993). Stabilization, Debt and Reform. Brighton: Harvester Wheatsheaf.<person-group>
                  <string-name>
                     <surname>Dornbusch</surname>
                  </string-name>
               </person-group>
               <source>Stabilization, Debt and Reform</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e320a1310">
            <mixed-citation id="d1607e324" publication-type="book">
FISCHER, S. (1991). Growth, macroeconomics and development. NBER Working Paper no. 3702,
May.<person-group>
                  <string-name>
                     <surname>Fischer</surname>
                  </string-name>
               </person-group>
               <source>Growth, macroeconomics and development</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e349a1310">
            <mixed-citation id="d1607e353" publication-type="journal">
and FRENKEL, J. A. (1972). Investment, the two-sector model and trade in debt and capital
goods. Journal of International Economics, 2, 211-33.<person-group>
                  <string-name>
                     <surname>Fischer</surname>
                  </string-name>
               </person-group>
               <fpage>211</fpage>
               <volume>2</volume>
               <source>Journal of International Economics</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e385a1310">
            <mixed-citation id="d1607e389" publication-type="book">
FRENKEL, J.A. and RAZIN, A. (1992). Fiscal Policies and the World Economy, 2nd edn. Cam-
bridge, Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Frenkel</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Fiscal Policies and the World Economy</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e418a1310">
            <mixed-citation id="d1607e422" publication-type="book">
FRIEDMAN, M. (1957). A Theory of Consumption Function. Princeton, NJ: Princeton University
Press.<person-group>
                  <string-name>
                     <surname>Friedman</surname>
                  </string-name>
               </person-group>
               <source>A Theory of Consumption Function</source>
               <year>1957</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e448a1310">
            <mixed-citation id="d1607e452" publication-type="journal">
LUCAS, R.E. (1988). On the mechanics of economic development. Journal of Monetary Economics,
22, 3-42.<person-group>
                  <string-name>
                     <surname>Lucas</surname>
                  </string-name>
               </person-group>
               <fpage>3</fpage>
               <volume>22</volume>
               <source>Journal of Monetary Economics</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e484a1310">
            <mixed-citation id="d1607e488" publication-type="journal">
MADDISON, A. (1987). Growth and slowdown in advanced capitalist economies. Journal of Econ-
omic Literature, 25, 649-98.<object-id pub-id-type="jstor">10.2307/2726106</object-id>
               <fpage>649</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e504a1310">
            <mixed-citation id="d1607e508" publication-type="journal">
MANKIW, N.G., ROMER, D. and WEIL, D. (1992). A contribution to the empirics of economic
growth. Quarterly Journal of Economics, 102, 407-38.<object-id pub-id-type="doi">10.2307/2118477</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e524a1310">
            <mixed-citation id="d1607e528" publication-type="journal">
OBSTFELD, M. (1982). Aggregate spending and the terms of trade: is there a Laursen-Metzler
effect? Quarterly Journal of Economics, 97, 251-70.<object-id pub-id-type="doi">10.2307/1880757</object-id>
               <fpage>251</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e544a1310">
            <mixed-citation id="d1607e548" publication-type="journal">
ONITSUKA, Y. (1974). International capital movements and the patterns of economic growth.
American Economic Review, 64, 24-36.<object-id pub-id-type="jstor">10.2307/1814879</object-id>
               <fpage>24</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e564a1310">
            <mixed-citation id="d1607e568" publication-type="book">
PITCHFORD, J. (1990). Australia's Foreign Debt: Myths and Realities. Sydney: Allen &amp; Unwin.<person-group>
                  <string-name>
                     <surname>Pitchford</surname>
                  </string-name>
               </person-group>
               <source>Australia's Foreign Debt: Myths and Realities</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e591a1310">
            <mixed-citation id="d1607e595" publication-type="book">
RIDER, M. (1994). External debt and liabilities of industrial countries. Reserve Bank of Australia
Discussion Paper RDP 9405.<person-group>
                  <string-name>
                     <surname>Rider</surname>
                  </string-name>
               </person-group>
               <source>External debt and liabilities of industrial countries</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e620a1310">
            <mixed-citation id="d1607e624" publication-type="journal">
ROMER, P. (1986). Increasing returns and long run growth. Journal of Political Economy, 94,
1002-37.<object-id pub-id-type="jstor">10.2307/1833190</object-id>
               <fpage>1002</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e640a1310">
            <mixed-citation id="d1607e644" publication-type="book">
SACHS, J.D. (ed.) (1989). Developing Country Debt and the World Economy. Chicago: NBER/
University of Chicago Press.<person-group>
                  <string-name>
                     <surname>Sachs</surname>
                  </string-name>
               </person-group>
               <source>Developing Country Debt and the World Economy</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1607e669a1310">
            <mixed-citation id="d1607e673" publication-type="journal">
SOLOW, R. (1956). A contribution to the theory of economic growth. Quarterly Journal of Econ-
omics, 70, 65-94.<object-id pub-id-type="doi">10.2307/1884513</object-id>
               <fpage>65</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e689a1310">
            <mixed-citation id="d1607e693" publication-type="journal">
STIGLITZ, J. and WEISS, A. Credit rationing in markets with imperfect information. American
Economic Review, 71, 393-419.<object-id pub-id-type="jstor">10.2307/1802787</object-id>
               <fpage>393</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1607e709a1310">
            <mixed-citation id="d1607e713" publication-type="journal">
SWAN, T.W. (1956). Economic growth and capital accumulation. Economic Record, 32, 334-61.<person-group>
                  <string-name>
                     <surname>Swan</surname>
                  </string-name>
               </person-group>
               <fpage>334</fpage>
               <volume>32</volume>
               <source>Economic Record</source>
               <year>1956</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
