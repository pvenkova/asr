<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envideveecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50012637</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environment and Development Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridage University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">1355770X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694395</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44379060</article-id>
         <title-group>
            <article-title>Payments for environmental services and the poor: concepts and preliminary evidence</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>SVEN</given-names>
                  <surname>WUNDER</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">13</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40183234</issue-id>
         <fpage>279</fpage>
         <lpage>297</lpage>
         <permissions>
            <copyright-statement>© 2008 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44379060"/>
         <abstract>
            <p>Based on observations from all three tropical continents, there is good reason to believe that poor service providers can broadly gain access to payment for environmental services (PES) schemes, and generally become better off from that participation, in both income and non-income terms. However, poverty effects need to be analysed in a conceptual framework looking not only at poor service providers, but also at poor service users and non-participants. Effects on service users are positive if environmental goals are achieved, while those on non-participants can be positive or negative. The various participation filters of a PES scheme contain both pro-poor and anti-poor selection biases. Quantitative welfare effects are bound to remain small-scale, compared to national poverty-alleviation goals. Some pro-poor interventions are possible, but increasing regulations excessively could curb PES efficiency and implementation scale, which could eventually harm the poor. Prime focus of PES should thus remain on the environment, not on poverty.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1352e132a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1352e139" publication-type="other">
'Design of Payments for
Environmental Services in Developed and Developing Countries' (University of
Bonn &amp; CIFOR, 14-16 June 2005, Titisee, Germany),</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e152a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1352e159" publication-type="other">
Pagiola
et al. (2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e169a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1352e176" publication-type="other">
Turpie et al, 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e183a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1352e190" publication-type="other">
Muñoz-Piña et al, 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e198a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1352e205" publication-type="other">
The following refers to the Vietnamese Government Program 327, and its successor
Program 661.</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e215a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1352e222" publication-type="other">
Robertson and Wunder, 2005</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e229a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1352e236" publication-type="other">
Muñoz-Piña et al, 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d1352e243a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1352e250" publication-type="other">
Heinrich, 2007</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1352e256" publication-type="other">
Hanlon, 2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1352e262" publication-type="other">
F. Ellis, pers.
comm., 2007</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1352e281a1310">
            <mixed-citation id="d1352e285" publication-type="other">
Albán, M. and M. Argüello (2004), 'Un análisis de los impactos sociales y económicos
de los proyectos de fijación de carbono en el Ecuador', El caso de PROFAFOR-
FACE', Rep. No. 1 84369 506 5. IIED, London (in Spanish).</mixed-citation>
         </ref>
         <ref id="d1352e298a1310">
            <mixed-citation id="d1352e302" publication-type="other">
Angelsen, A. and S. Wunder (2003), 'Exploring the poverty-forestry link: key
concepts, issues and research implications', CIFOR Occasional Papers No. 42,
58.</mixed-citation>
         </ref>
         <ref id="d1352e315a1310">
            <mixed-citation id="d1352e319" publication-type="other">
Asquith, N., M. Vargas-Ríos, and J. Smith (2002), 'Can forest-protection carbon
projects improve rural livelihoods? Analysis of the Noel Kempff Mercado Climate
Action Project, Bolivia', Mitigation and Adaptation Strategies for Global Change 7:
323-337.</mixed-citation>
         </ref>
         <ref id="d1352e335a1310">
            <mixed-citation id="d1352e339" publication-type="other">
Bennett, M.T. (2007), 'China's Sloping Land Conversion Program: institutional
innovation or business as usual?', Ecological Economics (accepted).</mixed-citation>
         </ref>
         <ref id="d1352e350a1310">
            <mixed-citation id="d1352e354" publication-type="other">
Blanco, J., S. Wunder, and F. Navarrete (2005), 'La experiencia colombiana en
esquemas de pagos por servicios ambientales', Ecoversa &amp; CIFOR, Bogotá
(unpublished) (in Spanish).</mixed-citation>
         </ref>
         <ref id="d1352e367a1310">
            <mixed-citation id="d1352e371" publication-type="other">
Blanco, J., S. Wunder, and S. Sabogal (2006), 'Potencialidades de implementación de
esquemas de pagos por servicios ambientales en Venezuela', Ecoversa &amp; CIFOR,
Bogotá (unpublished) (in Spanish).</mixed-citation>
         </ref>
         <ref id="d1352e384a1310">
            <mixed-citation id="d1352e388" publication-type="other">
Costa, M.M. and M. Zeller (2003), 'Peasants' production systems and the integration
of incentives for watershed protection: a case study of Guatemala', Forests,
Livelihoods and Biodiversity, Bonn: CIFO, pp. 23.</mixed-citation>
         </ref>
         <ref id="d1352e401a1310">
            <mixed-citation id="d1352e405" publication-type="other">
Echavarria, M., J. Vogel, M. Alban, and F. Meneses (2004), 'The impacts of payments
for watershed services in Ecuador', Rep. No. 1 84369 484 0, IIED, London.</mixed-citation>
         </ref>
         <ref id="d1352e415a1310">
            <mixed-citation id="d1352e419" publication-type="other">
Ferraro, P. and A. Kiss (2002), 'Direct payments to conserve biodiversity', Science
298: 1718-1719.</mixed-citation>
         </ref>
         <ref id="d1352e429a1310">
            <mixed-citation id="d1352e433" publication-type="other">
Ferraro, P. and R. Simpson (2002), 'The cost-effectiveness of conservation payments',
Land Economics 78: 339-353.</mixed-citation>
         </ref>
         <ref id="d1352e444a1310">
            <mixed-citation id="d1352e448" publication-type="other">
Frost, P.G.H. and I. Bond (2007). 'The CAMPFIRE programme in Zimbabwe:
payments for wildlife services', Ecological Economics (accepted).</mixed-citation>
         </ref>
         <ref id="d1352e458a1310">
            <mixed-citation id="d1352e462" publication-type="other">
Grieg-Gran, M., I.T. Porras, and S. Wunder (2005), 'How can market mechanisms
for forest environmental services help the poor? Preliminary lessons from Latin
America', World Development 33: 1511-1527.</mixed-citation>
         </ref>
         <ref id="d1352e475a1310">
            <mixed-citation id="d1352e479" publication-type="other">
Hanlon, J. (2004), 'It is possible to just give money to the poor?', Development and
Change 35: 375-383.</mixed-citation>
         </ref>
         <ref id="d1352e489a1310">
            <mixed-citation id="d1352e493" publication-type="other">
Heinrich, C.J. (2007), 'Demand and supply-side determinants of conditional cash
transfer program effectiveness', World Development 35: 121-143.</mixed-citation>
         </ref>
         <ref id="d1352e503a1310">
            <mixed-citation id="d1352e507" publication-type="other">
IPCC (2001), 'Summary for policy makers. Climate change 2001: impacts, adaptation,
and vulnerability', Intergovernmental Panel on Climate Change.</mixed-citation>
         </ref>
         <ref id="d1352e517a1310">
            <mixed-citation id="d1352e521" publication-type="other">
Kerr, J. (2002), 'Watershed development, environmental services, and poverty
alleviation in India', World Development 30: 1387-1400.</mixed-citation>
         </ref>
         <ref id="d1352e532a1310">
            <mixed-citation id="d1352e536" publication-type="other">
Landell-Mills, N. and I.T. Porras (2002), 'Silver bullet or fool's gold? A global review
of markets for forest environmental services and their impact on the poor', IIED,
London.</mixed-citation>
         </ref>
         <ref id="d1352e549a1310">
            <mixed-citation id="d1352e553" publication-type="other">
Milne, M. (2000), 'Forest carbon, livelihoods and biodiversity: a report to the
European Commission', CIFOR, Bogor.</mixed-citation>
         </ref>
         <ref id="d1352e563a1310">
            <mixed-citation id="d1352e567" publication-type="other">
Milne, M., P. Arroyo, and H. Peacock (2001), 'Assessing the livelihood benefits to
local communities from forest carbon projects: case study analysis Noel Kempff
Mercado Climate Action Project' (unpublished) CIFOR, Bogor.</mixed-citation>
         </ref>
         <ref id="d1352e580a1310">
            <mixed-citation id="d1352e584" publication-type="other">
Miranda, M., I. Porras, and M. Moreno (2003), 'The social impacts of payments for
environmental services in Costa Rica', Rep. No. 1 84369 453 0, IIED, London.</mixed-citation>
         </ref>
         <ref id="d1352e594a1310">
            <mixed-citation id="d1352e598" publication-type="other">
Muñoz, R. (2004), 'Efectos del programa de servicios ambientales en las condiciones
de vida de los campesinos de la Península de Osa', Masters Thesis, Universidad
de Costa Rica, San José (in Spanish).</mixed-citation>
         </ref>
         <ref id="d1352e611a1310">
            <mixed-citation id="d1352e615" publication-type="other">
Muñoz-Piña, C, A. Guevara, J. M. Torres, and J. Braña (2007), 'Paying for the
hydrological services of Mexico's forests: analysis, negotiation and results',
Ecological Economics (accepted).</mixed-citation>
         </ref>
         <ref id="d1352e629a1310">
            <mixed-citation id="d1352e633" publication-type="other">
Pagiola, S. (2007), 'Payments for environmental services in Costa Rica', Ecological
Economics (accepted).</mixed-citation>
         </ref>
         <ref id="d1352e643a1310">
            <mixed-citation id="d1352e647" publication-type="other">
Pagiola, S., A. Arcenas, and G. Platais (2005), 'Can payments for environmental
services help reduce poverty? An exploration of the issues and the evidence to
date', World Development 33: 237-253.</mixed-citation>
         </ref>
         <ref id="d1352e660a1310">
            <mixed-citation id="d1352e664" publication-type="other">
PREM (2005), 'Compensating upland forest communities for the provision of
watershed protection services: using "Payments for Environmental Services"
instruments in the Philippines', PREM Policy Brief No. 8.</mixed-citation>
         </ref>
         <ref id="d1352e677a1310">
            <mixed-citation id="d1352e681" publication-type="other">
Robertson, N. and S. Wunder (2005), 'Fresh tracks in the forest: assessing incipient
payments for environmental services initiatives in Bolivia', CIFOR, Bogor.</mixed-citation>
         </ref>
         <ref id="d1352e691a1310">
            <mixed-citation id="d1352e695" publication-type="other">
Rosa, H., S. Kandel, and L. Dimas. (2003), 'Compensation for environmental services
and rural communities', PRISMA, San Salvador.</mixed-citation>
         </ref>
         <ref id="d1352e705a1310">
            <mixed-citation id="d1352e709" publication-type="other">
Simpson, R. and R.A. Sedjo (1996), 'Paying for the conservation of endangered
ecosystems: a comparison of direct and indirect approaches', Environment and
Development Economics 1: 241-257.</mixed-citation>
         </ref>
         <ref id="d1352e723a1310">
            <mixed-citation id="d1352e727" publication-type="other">
Smith, J. and S. Scherr (2002), 'Forest carbon and local livelihoods: assessment of
opportunities and policy recommendations', CIFOR, Bogor, Indonesia.</mixed-citation>
         </ref>
         <ref id="d1352e737a1310">
            <mixed-citation id="d1352e741" publication-type="other">
Southgate, D., T. Haab, J. Lundine, and F. Rodriguez (2007), 'Responses of poor, rural
households in Ecuador and Guatemala to payments for environmental services',
Ohio State University (unpublished).</mixed-citation>
         </ref>
         <ref id="d1352e754a1310">
            <mixed-citation id="d1352e758" publication-type="other">
Sunderlin, W.D., S.D. Dewi, and A. Puntodewo (2007), 'Poverty and forests: multi-
country analysis of spatial association and proposed policy solutions', CIFOR
Occasional Paper No. 47, 43.</mixed-citation>
         </ref>
         <ref id="d1352e771a1310">
            <mixed-citation id="d1352e775" publication-type="other">
Turpie, J.K., C. Marais, and J.N. Blignaut (2007), 'The Working for Water Programme:
evolution of a payments for environmental services mechanism that addresses
both poverty and ecosystem service delivery in South Africa', Ecological Economics
(accepted).</mixed-citation>
         </ref>
         <ref id="d1352e791a1310">
            <mixed-citation id="d1352e795" publication-type="other">
World Bank (2000), World Development Report 2000-2001: Attacking Poverty, Oxford
and New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1352e805a1310">
            <mixed-citation id="d1352e809" publication-type="other">
Wunder, S. (2005), 'Payments for environmental services: some nuts and bolts',
CIFOR Occasional Paper No. 42, 24.</mixed-citation>
         </ref>
         <ref id="d1352e820a1310">
            <mixed-citation id="d1352e824" publication-type="other">
Wunder, S. (2006), 'Are direct payments for environmental services spelling doom
for sustainable forest management in the tropics?', Ecology and Society 11: 23.</mixed-citation>
         </ref>
         <ref id="d1352e834a1310">
            <mixed-citation id="d1352e838" publication-type="other">
Wunder, S. and M. Albán (2007), 'Decentralized payments for environmental
services: comparing the cases of Pimampiro and PROFAFOR in Ecuador',
Ecological Economics (accepted).</mixed-citation>
         </ref>
         <ref id="d1352e851a1310">
            <mixed-citation id="d1352e855" publication-type="other">
Wunder, S., B. Campbell, P.H.G. Frost, R. Iwan, J.A. Sayer, and L. Wollenberg (2007),
'When donors get cold feet: the community conservation concession in Setulang
(Kalimantan, Indonesia) that never happened', submitted to Ecology and Society.</mixed-citation>
         </ref>
         <ref id="d1352e868a1310">
            <mixed-citation id="d1352e872" publication-type="other">
Wunder, S., B.D. The, and E. Ibarra (2005), Payment is good, control is better: why
payments for environmental services so far have remained incipient in Vietnam',
CIFOR, Bogor, pp. 86.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
