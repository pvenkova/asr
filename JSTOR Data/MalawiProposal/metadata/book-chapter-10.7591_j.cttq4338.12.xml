<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.cttq4338</book-id>
      <subj-group>
         <subject content-type="call-number">HT1163.H89 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Antislavery movements</subject>
         <subj-group>
            <subject content-type="lcsh">Great Britain</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
               <subj-group>
                  <subject content-type="lcsh">19th century</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Abolitionists</subject>
         <subj-group>
            <subject content-type="lcsh">Great Britain</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
               <subj-group>
                  <subject content-type="lcsh">19th century</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Slave trade</subject>
         <subj-group>
            <subject content-type="lcsh">Great Britain</subject>
            <subj-group>
               <subject content-type="lcsh">Public opinion</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">19th century</subject>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Imperialism</subject>
         <subj-group>
            <subject content-type="lcsh">Great Britain</subject>
            <subj-group>
               <subject content-type="lcsh">Public opinion</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
                  <subj-group>
                     <subject content-type="lcsh">19th century</subject>
                  </subj-group>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Public opinion</subject>
         <subj-group>
            <subject content-type="lcsh">Great Britain</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
               <subj-group>
                  <subject content-type="lcsh">19th century</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Politics and culture</subject>
         <subj-group>
            <subject content-type="lcsh">Great Britain</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
               <subj-group>
                  <subject content-type="lcsh">19th century</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Great Britain</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
            <subj-group>
               <subject content-type="lcsh">1837–1901</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Freedom Burning</book-title>
         <subtitle>Anti-Slavery and Empire in Victorian Britain</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>HUZZEY</surname>
               <given-names>RICHARD</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>28</day>
         <month>08</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780801451089</isbn>
      <isbn content-type="epub">9780801465819</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>ITHACA; LONDON</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.cttq4338"/>
      <abstract abstract-type="short">
         <p>After Britain abolished slavery throughout most of its empire in 1834, Victorians adopted a creed of "anti-slavery" as a vital part of their national identity and sense of moral superiority to other civilizations. The British government used diplomacy, pressure, and violence to suppress the slave trade, while the Royal Navy enforced abolition worldwide and an anxious public debated the true responsibilities of an anti-slavery nation. This crusade was far from altruistic or compassionate, but Richard Huzzey argues that it forged national debates and political culture long after the famous abolitionist campaigns of William Wilberforce and Thomas Clarkson had faded into memory. These anti-slavery passions shaped racist and imperialist prejudices, new forms of coerced labor, and the expansion of colonial possessions.</p>
         <p>In a sweeping narrative that spans the globe,<italic>Freedom Burning</italic>explores the intersection of philanthropic, imperial, and economic interests that underlay Britain's anti-slavery zeal- from London to Liberia, the Sudan to South Africa, Canada to the Caribbean, and the British East India Company to the Confederate States of America. Through careful attention to popular culture, official records, and private papers, Huzzey rewrites the history of the British Empire and a century-long effort to end the global trade in human lives.</p>
      </abstract>
      <counts>
         <page-count count="312"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.5</book-part-id>
                  <title-group>
                     <title>Prologue:</title>
                     <subtitle>Freedom Burning</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>The Painter captured the fire burning. It happened in West Africa, on the banks of the Gallinas River. On 4 February 1845 sailors advanced from their ships in small boats to reach the African settlement and burn it to the ground. The flags of the vessels and the flames of the fire reflected on the surface of the water, as the moment was recorded by an unknown artist. As the village burned, he captured the smoldering sky and the assembled vessels, the flaming houses and the raiding party.</p>
                     <p>The attackers were not slave traders or pirates but serving men of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>An Anti-Slavery Nation</title>
                  </title-group>
                  <fpage>5</fpage>
                  <abstract abstract-type="extract">
                     <p>Dawn on the morning of 1 August 1834 brought a kind of freedom to the enslaved women, men, and children of the British West Indies. As the sun rose, the Emancipation Act of the previous year made them free—legally free, at least. Patriotic prose, images, and poetry depicted the dawn of their freedom. Composed by the anti-slavery publisher Josiah Conder, the above lines are typical of the celebration and self- satisfaction expressed by British abolitionists, politicians, and newspapermen. The “meridian blaze” of liberty had finally drenched the sugar colonies in light and warmth equal to the midday sun, he</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Uncle Tom’s Britain</title>
                  </title-group>
                  <fpage>21</fpage>
                  <abstract abstract-type="extract">
                     <p>Fifteen years into Victoria’s reign, Britain was enthralled with “a remarkable and very exciting story by an American lady, whose purpose is to exhibit the evils of slavery.”¹ Harriet Beecher Stowe’s <italic>Uncle Tom’s Cabin</italic> created a phenomenon that amazed contemporaries and has intrigued cultural historians. From 1852 the book racked up countless editions thanks to lax transatlantic copyright laws. Ten different editions came out in one October fortnight and forty were on offer by the end of 1853; the book sold 1,500,000 copies in Britain and her colonies.² One contemporary complained that the book’s “title has been pirated to give</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Anti-Slavery State</title>
                  </title-group>
                  <fpage>40</fpage>
                  <abstract abstract-type="extract">
                     <p>Anti-slavery’s transition from reformist crusade to national policy was a curious and complicated process. After centuries of supporting the slave trade, the British state was transformed, in stages, from the patron of slavery to its determined enemy. How and why did international suppression of the slave trade become an objective for successive Victorian governments? Various strands of government policy responded to the challenge and the relationship between anti-slavery and the state was transformed as anti-slavery evolved from a question of imperial morality to a cause for moral imperialism.¹</p>
                     <p>Anti-slavery action by the British state dated back to Sir William Dolben’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Britons’ Unreal Freedom</title>
                  </title-group>
                  <fpage>75</fpage>
                  <abstract abstract-type="extract">
                     <p>Given the political struggles over anti-slavery in Victorian foreign policy, it would be surprising if anti-slavery did not cast long shadows over domestic policy. Having looked at the politics of antislavery abroad, we now turn to the ways anti-slavery sentiment affected domestic Britain. Although Britons had broadly agreed that slavery was incompatible with national freedom, they struggled over which rights and protections should subsequently be advanced or rejected. The splintering of abolitionist societies did not diminish a strong anti-slavery influence on public debates about social, moral, political, and economic reform within Britain. Of course, every authorial reference to slavery was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Power, Prosperity, and Liberty</title>
                  </title-group>
                  <fpage>98</fpage>
                  <abstract abstract-type="extract">
                     <p>It was a painful truth for Victorian Britons, as the Earl of Clarendon observed in 1846, that “for our necessaries and luxuries of life, for the employment of our people, for our revenue, for our very position in the world as a nation, we are indebted to the production of slave labour.”¹ As an anti-slavery pioneer, it was not clear how much Britain could or should isolate itself from other countries’ surviving (and often thriving) slave systems. Debates over economic sanctions for the importation of slave-grown products and the use of violence to suppress the international slave trade burned brightly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Africa Burning</title>
                  </title-group>
                  <fpage>132</fpage>
                  <abstract abstract-type="extract">
                     <p>The relationship between anti-slavery and British imperial power was complex.¹ Before 1838, anti-slavery campaigning promoted imperial morality, reforming governance in the empire. After West Indian emancipation, this impulse contributed to a moral imperialism, a forceful quest that pried into societies across the globe.² With British slave-trade suppression on the west and east coasts of Africa came racial contempt and massive territorial expansion across the continent in the last quarter of the nineteenth century. Victorian sentiment against slavery could be used to fuel both expansionist and anti-expansionist politics, but the former proved to be a dominant interpretation of anti-slavery and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The Anti-Slavery Empire</title>
                  </title-group>
                  <fpage>177</fpage>
                  <abstract abstract-type="extract">
                     <p>“Queen victoria eye, see too far” was the verdict of some Africans living near the River Gambia when they encountered the electric searchlight of HMS <italic>Racer</italic> in January 1887. According to Edwin Parker, who recorded his service aboard the ship in a private journal, the new light was useful for the sailors’ regular attempts to intimidate African societies along the west coast of Africa. On this occasion, the Royal Navy was intervening to enforce peace between local African communities at Swarra Cunda Creek because “commerce is prevented by civil wars, as native farmers and others will not cultivate the ground.”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Ideologies of Freedom</title>
                  </title-group>
                  <fpage>203</fpage>
                  <abstract abstract-type="extract">
                     <p>This book began with the burning of a village on the Gallinas River. The Royal Navy’s self-righteous arson epitomizes the dilemma of Victorian anti-slavery in the question of whether freedom burned bright. Having looked at various theaters of politics and culture, high and low, touched by this same problem, it is appropriate to return to the metaphor. Comparisons between fire and freedom are not just a fanciful conceit. Buxton hoped that Africa would, in the future, follow Britain’s development away from the afflictions of similar superstition, rude intellect, and slavery to become “a blaze of light, liberty, religion, and happiness.”¹</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.14</book-part-id>
                  <title-group>
                     <title>List of Abbreviations</title>
                  </title-group>
                  <fpage>215</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.15</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>217</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.16</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>267</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.cttq4338.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>289</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
