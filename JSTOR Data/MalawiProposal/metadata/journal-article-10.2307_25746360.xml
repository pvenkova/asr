<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socistudscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100645</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Social Studies of Science</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03063127</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25746360</article-id>
         <title-group>
            <article-title>Weather profits: Weather derivatives and the commercialization of meteorology</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Samuel</given-names>
                  <surname>Randalls</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25746356</issue-id>
         <fpage>705</fpage>
         <lpage>730</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 SAGE Publications Ltd.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25746360"/>
         <abstract>
            <p>While many scholars researching the commercialization of science focus on biomedicine, this paper explores the changing commercial frameworks for meteorology in the UK and the US. The organization of meteorology in both countries increasingly reflects a political—economic approach that treats science as an economic entity in which market-based criteria can be used to allocate scientific resources. The differences are equally significant in terms of the production and dissemination of meteorological forecasts and other data to public and private services. Alongside this commercialization has been the emergence of weather derivatives markets — financial products that enable trading on weather indices in a way similar to oil or gas futures — which have re-shaped how some businesses interact with meteorologists. This paper explores how weather derivatives traders engage with, shape, and are frustrated by a commercialized approach to funding meteorological data and forecasts. It highlights how commercial imperatives raise questions about the collection and quality of meteorological data, and how forecasting and weather modelling is being adopted within the private sector to enable trading strategies in the weather derivatives market. The consequences for commercial actors are highly variable, suggesting that any account of commercialization of science, while recognizing extant policy shifts, must be sufficiently nuanced in its interpretation of such effects.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d912e143a1310">
            <label>1</label>
            <mixed-citation id="d912e150" publication-type="other">
HC 823-ii</mixed-citation>
         </ref>
         <ref id="d912e157a1310">
            <label>3</label>
            <mixed-citation id="d912e164" publication-type="other">
Fine, 2007: 226</mixed-citation>
            <mixed-citation id="d912e170" publication-type="other">
US Department of Labor, 2009</mixed-citation>
         </ref>
         <ref id="d912e177a1310">
            <label>8</label>
            <mixed-citation id="d912e184" publication-type="other">
Pollard et al., 2008</mixed-citation>
         </ref>
         <ref id="d912e191a1310">
            <label>10</label>
            <mixed-citation id="d912e198" publication-type="other">
Syroka and Wilcox, 2006</mixed-citation>
         </ref>
         <ref id="d912e206a1310">
            <label>11</label>
            <mixed-citation id="d912e213" publication-type="other">
Letter from Lynda Clemmons to Samuel Bodman, 2002, previously available from: www.
wrma.com.</mixed-citation>
         </ref>
         <ref id="d912e223a1310">
            <label>12</label>
            <mixed-citation id="d912e230" publication-type="other">
Mirowski (2004: 138-141)</mixed-citation>
            <mixed-citation id="d912e236" publication-type="other">
Mirowski and Van Horn, 2005</mixed-citation>
         </ref>
         <ref id="d912e243a1310">
            <label>17</label>
            <mixed-citation id="d912e250" publication-type="other">
Hurrell, 1995</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d912e266a1310">
            <mixed-citation id="d912e270" publication-type="other">
Agrawala, Shardul, Kenneth Broad &amp; David H. Guston (2001) 'Integrating Climate Forecasts
and Societal Decision Making: Challenges to an Emergent Boundary Organization', Science,
Technology, &amp; Human Values 26(4): 454-77.</mixed-citation>
         </ref>
         <ref id="d912e283a1310">
            <mixed-citation id="d912e287" publication-type="other">
Anderson, Katharine (2005) Predicting the Weather: Victorians and the Science of Meteorology
(Chicago: University of Chicago Press).</mixed-citation>
         </ref>
         <ref id="d912e297a1310">
            <mixed-citation id="d912e301" publication-type="other">
Banks, Erik (ed.) (2002) Weather Risk Management: Markets, Products, and Applications
(New York: Palgrave).</mixed-citation>
         </ref>
         <ref id="d912e311a1310">
            <mixed-citation id="d912e315" publication-type="other">
Block, Walter (2006) 'Katrina: Private Enterprise, the Dead Hand of the Past, and Weather Socialism:
An Analysis in Economic Geography', Ethics, Place and Environment 9(2): 231-41.</mixed-citation>
         </ref>
         <ref id="d912e326a1310">
            <mixed-citation id="d912e330" publication-type="other">
Brettle, Mike (2006) 'Can Numerical Weather Prediction Replace Observations?' Weather 61(7):
194-95.</mixed-citation>
         </ref>
         <ref id="d912e340a1310">
            <mixed-citation id="d912e344" publication-type="other">
Burton, J.M.C. (1983) 'The Foundation and Early Years of the Meteorological Office: Part ,
Weather 38(12): 364-68.</mixed-citation>
         </ref>
         <ref id="d912e354a1310">
            <mixed-citation id="d912e358" publication-type="other">
Burton, J.M.C. (1984) 'The Foundation and Early Years of the Meteorological Office: Part 2',
Weather 39(1): 7-10.</mixed-citation>
         </ref>
         <ref id="d912e368a1310">
            <mixed-citation id="d912e372" publication-type="other">
Castrée, Noel (2006) 'From Neoliberalism to Neoliberalisation: Consolations, Confusions, and
Necessary Illusions', Environment and Planning A 38(1): 1-6.</mixed-citation>
         </ref>
         <ref id="d912e382a1310">
            <mixed-citation id="d912e386" publication-type="other">
Chang, Hasok (2004) Inventing Temperature: Measurement and Scientific Progress (New York:
Oxford University Press).</mixed-citation>
         </ref>
         <ref id="d912e396a1310">
            <mixed-citation id="d912e400" publication-type="other">
Changnon, Stanley A. (2007) 'New Risk Assessments for Dealing with Financial Exposure to
Weather Hazards', Natural Hazards 43(3): 295-301.</mixed-citation>
         </ref>
         <ref id="d912e411a1310">
            <mixed-citation id="d912e415" publication-type="other">
Changnon, S.A., E.R. Fosse &amp; EX. Lecomte (1999) 'Interactions between the Atmospheric
Sciences and Insurers in the United States', Climatic Change 42(1): 51-67.</mixed-citation>
         </ref>
         <ref id="d912e425a1310">
            <mixed-citation id="d912e429" publication-type="other">
Craft, Erik D. (1999) 'Private Weather Organizations and the Founding of the United States
Weather Bureau', Journal of Economic History 59(4): 1063-71.</mixed-citation>
         </ref>
         <ref id="d912e439a1310">
            <mixed-citation id="d912e443" publication-type="other">
Cronon, William ( 1991 ) Nature s Metropolis: Chicago and the Great West (New York: W. W. Norton).</mixed-citation>
         </ref>
         <ref id="d912e450a1310">
            <mixed-citation id="d912e454" publication-type="other">
Doel, Ronald E. (2003) 'Constituting the Postwar Earth Sciences: The Military's Influence on
the Environmental Sciences in the USA after 1945', Social Studies of Science 33(5): 635-66.</mixed-citation>
         </ref>
         <ref id="d912e464a1310">
            <mixed-citation id="d912e468" publication-type="other">
Dupree, A. Hunter (1986) Science in the Federal Government: A History of Policies and Activities
(Baltimore: The Johns Hopkins University Press).</mixed-citation>
         </ref>
         <ref id="d912e478a1310">
            <mixed-citation id="d912e482" publication-type="other">
Dutton, John A. (2002) 'Opportunities and Priorities in a New Era for Weather and Climate
Services', Bulletin of the American Meteorological Society 83(9): 1303-11.</mixed-citation>
         </ref>
         <ref id="d912e493a1310">
            <mixed-citation id="d912e497" publication-type="other">
Edwards, Paul N. (1999) 'Global Climate Science, Uncertainty and Polities', Science as Culture
8(4): 437-72.</mixed-citation>
         </ref>
         <ref id="d912e507a1310">
            <mixed-citation id="d912e511" publication-type="other">
Edwards, Paul N. (2001) 'Representing the Global Atmosphere: Computer Models, Data, and
Knowledge about Climate Change', in CA. Miller &amp; PN. Edwards (eds), Changing the
Atmosphere: Expert Knowledge and Environmental Governance (Cambridge: MIT Press): 31-65.</mixed-citation>
         </ref>
         <ref id="d912e524a1310">
            <mixed-citation id="d912e528" publication-type="other">
Eilig, Jerome (1989) Set Fair: Gradualist Proposal for Privatising Weather Forecasting (London:
Social Affairs Unit).</mixed-citation>
         </ref>
         <ref id="d912e538a1310">
            <mixed-citation id="d912e542" publication-type="other">
Evans, Robert &amp; Simon Shackley (1997) 'Report: ESRC Workshop - 10-11 April 1997: The
Use of Models in Policy Making: Towards a Comparison and Evaluation of Experiences',
Technoscience 10(3): 7-10.</mixed-citation>
         </ref>
         <ref id="d912e555a1310">
            <mixed-citation id="d912e559" publication-type="other">
Fine, Gary Alan (2007) Authors of the Storm: Meteorologists and the Culture of Prediction
(Chicago: University of Chicago Press).</mixed-citation>
         </ref>
         <ref id="d912e569a1310">
            <mixed-citation id="d912e573" publication-type="other">
Fisher, Jill A. (2009) Medical Research for Hire: The Political Economy of Pharmaceutical
Clinical Trials (Piscataway, NJ: Rutgers University Press).</mixed-citation>
         </ref>
         <ref id="d912e584a1310">
            <mixed-citation id="d912e588" publication-type="other">
Fleming, James R. (1990) Meteorology in America, 1800-1870 (Baltimore: The Johns Hopkins
University Press).</mixed-citation>
         </ref>
         <ref id="d912e598a1310">
            <mixed-citation id="d912e602" publication-type="other">
Fleming, James R. (2005) 'Telegraphing the Weather: Military Meteorology, Strategy and
"Homeland Security" on the American frontier in the 1870s', in S.A. Walton (ed.), Instrumental
in War: Science, Research and Instruments. Between Knowledge and the World (Leiden: Brill
Academic): 153-78.</mixed-citation>
         </ref>
         <ref id="d912e618a1310">
            <mixed-citation id="d912e622" publication-type="other">
Freebairn, John W &amp; John W. Zillman (2002a) 'Funding Meteorological Services', Meteorological
Applications 9(1): 45-54.</mixed-citation>
         </ref>
         <ref id="d912e632a1310">
            <mixed-citation id="d912e636" publication-type="other">
Freebairn, John W. &amp; John W. Zillman (2002b) 'Economic Benefits of Meteorological Services',
Meteorological Applications 9(1): 33^44.</mixed-citation>
         </ref>
         <ref id="d912e646a1310">
            <mixed-citation id="d912e650" publication-type="other">
Friedman, Robert Marc (1989) Appropriating the Weather: Vilhelm Bjerknes and the Construction
of a Modern Meteorology (Ithaca, NY: Cornell University Press).</mixed-citation>
         </ref>
         <ref id="d912e660a1310">
            <mixed-citation id="d912e664" publication-type="other">
Harper, Kristine C. (2003) 'Research from the Boundary Layer: Civilian Leadership, Military
Funding and the Development of Numerical Weather Prediction (1946-55)', Social Studies of
Science 33(5): 667-96.</mixed-citation>
         </ref>
         <ref id="d912e678a1310">
            <mixed-citation id="d912e682" publication-type="other">
Harper, Kristine C. (2008) Weather by the Numbers: The Genesis of Modern Meteorology
(Cambridge, MA: Harvard University Press).</mixed-citation>
         </ref>
         <ref id="d912e692a1310">
            <mixed-citation id="d912e696" publication-type="other">
Hurrell, J.W. (1995) 'Decadal Trends in the North Atlantic Oscillation - Regional Temperatures
and Precipitation', Science 269(5224): 676-79.</mixed-citation>
         </ref>
         <ref id="d912e706a1310">
            <mixed-citation id="d912e710" publication-type="other">
Jones, Nicola (2003) 'The Weather Profits', New Scientist 179(2407): 42.</mixed-citation>
         </ref>
         <ref id="d912e717a1310">
            <mixed-citation id="d912e721" publication-type="other">
Kelly, Jack J. (2001) 'Opportunities for 21st Century Meteorology: New Markets for Weather,
Water and Climate Information', presentation at First AMS Presidential Policy Forum,
American Meteorological Society (AMS) Annual Meeting, Albuquerque, New Mexico (17
January 2001). Available at: www.ametsoc.org/AMS/atmospolicy/presforums/albq2001/kelly.
pdf (accessed 25 February 2010).</mixed-citation>
         </ref>
         <ref id="d912e740a1310">
            <mixed-citation id="d912e744" publication-type="other">
Krimsky, Sheldon (2003) Science in the Private Interest: Has the Lure of Profits Corrupted
Biomedicai Research? (Lanham, MD: Rowman and Littlefield).</mixed-citation>
         </ref>
         <ref id="d912e754a1310">
            <mixed-citation id="d912e758" publication-type="other">
Lave, Rebecca, Philip Mirowski &amp; Samuel Randalls (2010) 'Introduction: STS and Neoliberal
Science' Social Studies of Science 40(5): 659-75.</mixed-citation>
         </ref>
         <ref id="d912e769a1310">
            <mixed-citation id="d912e773" publication-type="other">
MacLeod, Roy M. (1971) 'The Royal Society and the Government Grant: Notes on the
Administration of Scientific Research', Historical Journal 14(2): 323-58.</mixed-citation>
         </ref>
         <ref id="d912e783a1310">
            <mixed-citation id="d912e787" publication-type="other">
Maunder, William J. (1970) The Value of the Weather (London: Methuen and Co Ltd).</mixed-citation>
         </ref>
         <ref id="d912e794a1310">
            <mixed-citation id="d912e798" publication-type="other">
Mergen, Bernard (2008) Weather Matters: An American Cultural History since 1900 (Lawrence,
KS: University Press of Kansas).</mixed-citation>
         </ref>
         <ref id="d912e808a1310">
            <mixed-citation id="d912e812" publication-type="other">
Miller, Clark A. (2001) 'Scientific Internationalism in American Foreign Policy: The Case of
Meteorology, 1947-1958', in C.A. Miller &amp; P.N. Edwards (eds), Changing the Atmosphere:
Expert Knowledge and Environmental Governance (Cambridge, MA: MIT Press): 169-217.</mixed-citation>
         </ref>
         <ref id="d912e825a1310">
            <mixed-citation id="d912e829" publication-type="other">
Mirowski, Philip (2004) The Effortless Economy of Science (Durham, NC: Duke University Press).</mixed-citation>
         </ref>
         <ref id="d912e836a1310">
            <mixed-citation id="d912e840" publication-type="other">
Mirowski, Philip (2009) 'Postface: Defining Neoliberalism', in P. Mirowski &amp; D. Plehwe (eds),
The Road from Mont Pèlerin (Cambridge, MA: Harvard University Press): 417-55.</mixed-citation>
         </ref>
         <ref id="d912e851a1310">
            <mixed-citation id="d912e855" publication-type="other">
Mirowski, Philip (2010) ScienceMart™: Rethinking the New Economics of Science (Cambridge,
MA: Harvard University Press).</mixed-citation>
         </ref>
         <ref id="d912e865a1310">
            <mixed-citation id="d912e869" publication-type="other">
Mirowski, Philip &amp; Esther-Mirjam Sent (2008) 'The Commercialization of Science and the
Response of STS', in Edward J. Hackett, Olga Amsterdamska, Michael Lynch &amp; Judy
Wacjman (eds), The Handbook of Science and Technology Studies, 3rd edition (Cambridge,
MA: MIT Press) 635-89.</mixed-citation>
         </ref>
         <ref id="d912e885a1310">
            <mixed-citation id="d912e889" publication-type="other">
Mirowski, Philip &amp; Robert Van Horn (2005) 'The Contract Research Organization and the
Commercialization of Scientific Research', Social Studies of Science 35(4): 503-48.</mixed-citation>
         </ref>
         <ref id="d912e899a1310">
            <mixed-citation id="d912e903" publication-type="other">
Morss, Rebecca E. &amp; William H. Hooke (2005) 'The Outlook for U.S. Meteorological Research
in a Commercializing World: Fair Early, but Clouds Moving In?' Bulletin of the American
Meteorological Society 86(7): 921-36.</mixed-citation>
         </ref>
         <ref id="d912e916a1310">
            <mixed-citation id="d912e920" publication-type="other">
Mukerji, Chandra (1989) A Fragile Power: Scientists and the State (Princeton, NJ: Princeton
University Press).</mixed-citation>
         </ref>
         <ref id="d912e930a1310">
            <mixed-citation id="d912e934" publication-type="other">
Murnane, Richard J., Michael Crowe, Allan Eustis, Susan Howard, Judy Koepsell, Robert
Leffler &amp; Robert Livezey (2002) 'The Weather Risk Management Industry's Climate
Forecast and Data Needs: A Workshop Report', Bulletin of the American Meteorological
Society 83(8): 1193-98.</mixed-citation>
         </ref>
         <ref id="d912e951a1310">
            <mixed-citation id="d912e955" publication-type="other">
National Research Council (2003) Fair Weather: Effective Partnerships in Weather and Climate
Services (Washington, DC: National Academies Press).</mixed-citation>
         </ref>
         <ref id="d912e965a1310">
            <mixed-citation id="d912e969" publication-type="other">
Ong, Aihwa (2006) Neoliberalism as Exception: Mutations in Citizenship and Sovereignty
(Durham, NC: Duke University Press).</mixed-citation>
         </ref>
         <ref id="d912e979a1310">
            <mixed-citation id="d912e983" publication-type="other">
Pollard, Jane S., Jonathan Oldfìeld, Samuel Randalls &amp; John E. Thornes (2008) 'Firm Finances,
Weather Derivatives and Geography', Geoforum 39(2): 616-24.</mixed-citation>
         </ref>
         <ref id="d912e993a1310">
            <mixed-citation id="d912e997" publication-type="other">
Porter, Theodore (1995) Trust in Numbers: The Pursuit of Objectivity in Science and Public Life
(Princeton, NJ: Princeton University Press).</mixed-citation>
         </ref>
         <ref id="d912e1007a1310">
            <mixed-citation id="d912e1011" publication-type="other">
PricewaterhouseCoopers (2008) 'Weather Derivatives Annual Industry Survey', Weather Risk
Management Association. Available at http://www.wrma.org/documents/WRMA%20PwC%20
2008%20Survey%20results%20press%20release.pdf (accessed 31 May 2010).</mixed-citation>
         </ref>
         <ref id="d912e1024a1310">
            <mixed-citation id="d912e1028" publication-type="other">
Pryke, Michael (2007) 'Geomoney: An Option on Frost, Going Long on Clouds', Geoforum 38(3):
576-88.</mixed-citation>
         </ref>
         <ref id="d912e1039a1310">
            <mixed-citation id="d912e1043" publication-type="other">
Rasmussen, Nicolas (2004) 'The Moral Economy of the Drug Company-Medical Scientist
Collaboration in Interwar America', Social Studies of Science 34(2): 161-186.</mixed-citation>
         </ref>
         <ref id="d912e1053a1310">
            <mixed-citation id="d912e1057" publication-type="other">
Rindova, V.P., T.G. Pollock &amp; M.L.A. Hayward (2006) 'Celebrity Firms: The Social Construction
of Market Popularity', Academy of Management Review 31(1): 50-71.</mixed-citation>
         </ref>
         <ref id="d912e1067a1310">
            <mixed-citation id="d912e1071" publication-type="other">
Sismondo, Sergio (2009) 'Ghosts in the Machine: Publication Planning in the Medical Sciences',
Social Studies of Science 39(2): 171-98.</mixed-citation>
         </ref>
         <ref id="d912e1081a1310">
            <mixed-citation id="d912e1085" publication-type="other">
Slaughter, Sheila &amp; Gary Rhoades (2004) Academic Capitalism and the New Economy (Baltimore:
Johns Hopkins University Press).</mixed-citation>
         </ref>
         <ref id="d912e1095a1310">
            <mixed-citation id="d912e1099" publication-type="other">
Smith, Michael R. (2002) 'Five Myths of Commercial Meteorology', Bulletin of the American
Meteorological Society 83(5): 993-96.</mixed-citation>
         </ref>
         <ref id="d912e1109a1310">
            <mixed-citation id="d912e1113" publication-type="other">
Sunder Rajan, Kaushik (2006) Biocapital: The Constitution of Postgenomic Life (Durham, NJ:
Duke University Press).</mixed-citation>
         </ref>
         <ref id="d912e1124a1310">
            <mixed-citation id="d912e1128" publication-type="other">
Syroka, Joanna &amp; Richard Wilcox (2006) 'Rethinking International Disaster Aid Finance', Journal
of International Affairs 59(2): 197-214.</mixed-citation>
         </ref>
         <ref id="d912e1138a1310">
            <mixed-citation id="d912e1142" publication-type="other">
Thornes, John E. (2003) 'Weather and Climate Derivatives', Weather 58(5): 193-96.</mixed-citation>
         </ref>
         <ref id="d912e1149a1310">
            <mixed-citation id="d912e1153" publication-type="other">
US Department of Labor (2009) Occupational Outlook Handbook 2008-9', Bureau of Labor
Statistics, U.S. Department of Labor. Available at: www.bls.gov/oco/ocos051.htm (accessed
5 December 2009).</mixed-citation>
         </ref>
         <ref id="d912e1166a1310">
            <mixed-citation id="d912e1170" publication-type="other">
Varma, Roli (2000) 'Changing Research Cultures in U.S. Industry', Science, Technology, &amp;
Human Values 25(4): 395^16.</mixed-citation>
         </ref>
         <ref id="d912e1180a1310">
            <mixed-citation id="d912e1184" publication-type="other">
Weiss, Peter (2002) Borders in Cyberspace: Conflicting Public Sector Information Policies and
their Economic Impacts (Washington, DC: U.S. Department of Commerce). Available at:
http://www. epsiplus.net/psi_lto (accessed 20 June 2010).</mixed-citation>
         </ref>
         <ref id="d912e1197a1310">
            <mixed-citation id="d912e1201" publication-type="other">
Zeng, Lixin (2000) 'Weather Derivatives and Weather Insurance: Concept, Application, and
Analysis', Bulletin of the American Meteorological Society 81(9): 2075-82.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
