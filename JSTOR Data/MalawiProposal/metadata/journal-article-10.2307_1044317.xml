<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">annaameracadpoli</journal-id>
         <journal-id journal-id-type="jstor">j100052</journal-id>
         <journal-title-group>
            <journal-title>The Annals of the American Academy of Political and Social Science</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn pub-type="ppub">00027162</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">1044317</article-id>
         <article-categories>
            <subj-group>
               <subject>North-South Issues</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Arguments for the Generation of Technology by Less-Developed Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Frances</given-names>
                  <surname>Stewart</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>11</month>
            <year>1981</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">458</volume>
         <issue-id>i243073</issue-id>
         <fpage>97</fpage>
         <lpage>109</lpage>
         <page-range>97-109</page-range>
         <permissions>
            <copyright-statement>Copyright 1981 The American Academy of Political and Social Science</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1044317"/>
         <abstract>
            <p>This article considers whether LDCs should try to develop their own technological capacity rather than continue to rely on developed countries for their technology. The developed countries at present have a comparative advantage in the production of technology. They have the specialized resources of manpower and physical equipment as well as considerable experience. It is therefore likely to be cheaper for developing countries to buy their technology from developed countries. But there are three reasons why developing countries should nonetheless try to develop their own technological capacity. First, local technological capacity is necessary to adapt imported technology to local conditions so that it becomes more efficient in use. Second, technology imported from industrialized countries is often inappropriate for the different conditions in less-developed countries (LDCs). The imported technology tends to be capital-intensive and large scale and often produces oversophisticated high income products. LDCs need their own technological capacity to develop more appropriate technologies. Third, total technological dependence is a major factor behind the generally dependent relationship that many countries experience vis-a-vis industrial countries. Without independent technological capacity, the technological dependence and the more general dependent relationship is likely to be perpetuated.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1520e129a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1520e136" publication-type="book">
D. C. Landes, The Unbound
Prometheus, (Cambridge: Cambridge Uni-
versity Press, 1969).<person-group>
                     <string-name>
                        <surname>Landes</surname>
                     </string-name>
                  </person-group>
                  <source>The Unbound Prometheus</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e165a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1520e172" publication-type="book">
S. Hollander, The Sour-
ces of Increased Efficiency, A Study of the Du
Pont Rayon Plant (Boston: MIT, 1966)<person-group>
                     <string-name>
                        <surname>Hollander</surname>
                     </string-name>
                  </person-group>
                  <source>The Sources of Increased Efficiency, A Study of the Du Pont Rayon Plant</source>
                  <year>1966</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e200" publication-type="book">
J. Katz et al.,
"Productivity, Technology and Domestic
Effects in Research and DeveloDment," IBD/
ECLA Working Paper, No. 13<person-group>
                     <string-name>
                        <surname>Katz</surname>
                     </string-name>
                  </person-group>
                  <source>Productivity, Technology and Domestic Effects in Research and DeveloDment</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e228" publication-type="book">
J. Enos, "Invention and Innovation in
the Petroleum Refining Industry," in The
Rate and Direction of Inventive Activity
(Princeton: Princeton University Press,
1962)<person-group>
                     <string-name>
                        <surname>Enos</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Invention and Innovation in the Petroleum Refining Industry</comment>
                  <source>The Rate and Direction of Inventive Activity</source>
                  <year>1962</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e266a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1520e273" publication-type="other">
OECD, 1970.</mixed-citation>
            </p>
         </fn>
         <fn id="d1520e280a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1520e287" publication-type="book">
J. Annerstadt, "Technological
Dependence: A Permanent Phenomenon of
World Inequality," mimeo, Institute of Social
and Economic Planning, Roskilde University
Center, 1978).<person-group>
                     <string-name>
                        <surname>Annerstadt</surname>
                     </string-name>
                  </person-group>
                  <source>Technological Dependence: A Permanent Phenomenon of World Inequality</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e323a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1520e330" publication-type="book">
UNCTAD, "The Role of the Patent
System in the Transfer of Technology to
Developing Countries," TD/B/AC.11/19,
Rev. 1, 1975.<person-group>
                     <string-name>
                        <surname>UNCTAD</surname>
                     </string-name>
                  </person-group>
                  <source>The Role of the Patent System in the Transfer of Technology to Developing Countries</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e362a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1520e369" publication-type="book">
S. Lall,
Developing Countries as Exporters of Tech-
nology (London: Macmillan, forthcoming).<person-group>
                     <string-name>
                        <surname>Lall</surname>
                     </string-name>
                  </person-group>
                  <source>Developing Countries as Exporters of Technology</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e394a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1520e401" publication-type="book">
UNCTAD, "Technology Planning in
Developing Countries: A Preliminary
Review," TD/B/C.6 29, 1978.<person-group>
                     <string-name>
                        <surname>UNCTAD</surname>
                     </string-name>
                  </person-group>
                  <volume>29</volume>
                  <source>Technology Planning in Developing Countries: A Preliminary Review</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e433a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1520e440" publication-type="book">
W. Beranek and G. Ranis, Science
and Technology and Economic Development
(New York: Praeger, 1978)<person-group>
                     <string-name>
                        <surname>Beranek</surname>
                     </string-name>
                  </person-group>
                  <source>Science and Technology and Economic Development</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e468" publication-type="journal">
D.
Crane, "Technological Innovation in Develop-
ing Countries: A Review of the Literature,"
Research Policy, 6 (1977)<person-group>
                     <string-name>
                        <surname>Crane</surname>
                     </string-name>
                  </person-group>
                  <volume>6</volume>
                  <source>Research Policy</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e503a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1520e510" publication-type="book">
P. Maxwell, "Learn-
ing and Technical Change in the Steelplant of
Acinder S.A. in Rosario, Argentina," BID/
CEPAL/BA/18, 1976<person-group>
                     <string-name>
                        <surname>Maxwell</surname>
                     </string-name>
                  </person-group>
                  <source>Learning and Technical Change in the Steelplant of Acinder S.A. in Rosario, Argentina</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e542a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1520e549" publication-type="book">
Lall, Developing Countries.<person-group>
                     <string-name>
                        <surname>Lall</surname>
                     </string-name>
                  </person-group>
                  <source>Developing Countries</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e569a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1520e576" publication-type="journal">
Gus-
tafson, "Research and Development, New
Products and Productivity Change," Ameri-
can Economic Review, Papers and Proceed-
ings, LII (1962)<person-group>
                     <string-name>
                        <surname>Gustafson</surname>
                     </string-name>
                  </person-group>
                  <volume>LII</volume>
                  <source>American Economic Review, Papers and Proceedings</source>
                  <year>1962</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e614a1310">
            <p>
               <mixed-citation id="d1520e618" publication-type="book">
A. Gerschenkron, Economic Back-
wardness in Historical Perspective (Cam-
bridge, MA: Harvard University Press,
1962).<person-group>
                     <string-name>
                        <surname>Gerschenkron</surname>
                     </string-name>
                  </person-group>
                  <source>Economic Backwardness in Historical Perspective</source>
                  <year>1962</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e650a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1520e657" publication-type="journal">
Beranek and Ranis, passim.<person-group>
                     <string-name>
                        <surname>Beranek</surname>
                     </string-name>
                  </person-group>
                  <source>passim</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e676a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1520e683" publication-type="journal">
Financial Times, 12 June 1981 reporting
on a survey in the Japanese Economic Jour-
nal.<issue>12 June</issue>
                  <source>Financial Times</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e706a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1520e713" publication-type="conference">
L. Westphal
and C. Dahlman, "The Acquisition of Tech-
nological Mastery in Industry," paper pres-
ented to U.S.-China Conference on
Alternative Strategies for Economic Devel-
opment, Wingspread Conference Center,
Wisconsin, 1980.<person-group>
                     <string-name>
                        <surname>Westphal</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The Acquisition of Technological Mastery in Industry</comment>
                  <source>U.S.-China Conference on Alternative Strategies for Economic Development, Wingspread Conference Center, Wisconsin, 1980</source>
                  <year>1980</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e757a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1520e764" publication-type="journal">
S.
Teitel, "Productivity, Mechanisation and
Skills: A Test of the Hirschman hypothesis
for Latin American Industry," World Devel-
opment, 9 (1981)<person-group>
                     <string-name>
                        <surname>Teitel</surname>
                     </string-name>
                  </person-group>
                  <volume>9</volume>
                  <source>World Development</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e803a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1520e810" publication-type="book">
L. Doyle,
Inter-Economy Comparisons-A Case Study
(Berkeley: University of California Press,
1965).<person-group>
                     <string-name>
                        <surname>Doyle</surname>
                     </string-name>
                  </person-group>
                  <source>Inter-Economy Comparisons-A Case Study</source>
                  <year>1965</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e842a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1520e849" publication-type="book">
C. S. Dahlman and T.V.
Fonseca, "From Technological Dependence
to Technological Development: The Case of
the Usiminos Steel Plant in Brazil," IDP/
ECLA/UNDP/IDRC. Regional Program of
Studies on Scientific and Technical Develop-
ment in Latin America, Working Paper No.
21, 1978.<person-group>
                     <string-name>
                        <surname>Dahlman</surname>
                     </string-name>
                  </person-group>
                  <source>From Technological Dependence to Technological Development: The Case of the Usiminos Steel Plant in Brazil</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e893a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1520e900" publication-type="other">
T. Ozawa, Imitation, Innovation and
Trade: A Study of Foreign Licensing Opera-
tions in Japan (Ph.D. dissertation, Columbia
University, 1966)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e915" publication-type="book">
D. Granick, Soviet
Metal Fabricating and Economic Develop-
ment (Madison: University of Wisconsin
Press, 1967)<person-group>
                     <string-name>
                        <surname>Granick</surname>
                     </string-name>
                  </person-group>
                  <source>Soviet Metal Fabricating and Economic Development</source>
                  <year>1967</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e947a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1520e954" publication-type="journal">
G. Ranis, "Industrial Sector Labour
Absorption," Economic Development and
Cultural Change, 21 (1973).<person-group>
                     <string-name>
                        <surname>Ranis</surname>
                     </string-name>
                  </person-group>
                  <volume>21</volume>
                  <source>Economic Development and Cultural Change</source>
                  <year>1973</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e986a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1520e993" publication-type="journal">
Ibid.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1002a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1520e1009" publication-type="book">
J. Enos, "The Adoption and Diffu-
sion of Imported Techniques in S. Korea,"
paper for Science and Technology Workshop,
Queen Elizabeth House, Oxford, 1980<person-group>
                     <string-name>
                        <surname>Enos</surname>
                     </string-name>
                  </person-group>
                  <source>The Adoption and Diffusion of Imported Techniques in S. Korea</source>
                  <year>1980</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e1040" publication-type="book">
L.
Westphal, Y. Rhee, and G. Pursell, "Foreign
Influences on Korean Industrial Develop-
ment," Oxford Bulletin of Economics and Sta-
tistics, 41 (1979)<person-group>
                     <string-name>
                        <surname>Westphal</surname>
                     </string-name>
                  </person-group>
                  <source>Foreign Influences on Korean Industrial Development</source>
                  <year>1979</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1076a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1520e1083" publication-type="book">
M. Bell, D. Scott-Kemmis, and W. Sat-
yarakwit, "Learning and Technical Change
in the Development of Manufacturing Indus-
try: A Case Study of a Permanently Infant
Enterprise," mimeo, Science Policy Research
Unit, Sussex, 1980.<person-group>
                     <string-name>
                        <surname>Bell</surname>
                     </string-name>
                  </person-group>
                  <source>Learning and Technical Change in the Development of Manufacturing Industry: A Case Study of a Permanently Infant Enterprise</source>
                  <year>1980</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1121a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1520e1128" publication-type="book">
C. Pratten, Economies of Scale in
Manufacturing Industry (Cambridge: Cam-
bridge University Press, 1971)<person-group>
                     <string-name>
                        <surname>Pratten</surname>
                     </string-name>
                  </person-group>
                  <source>Economies of Scale in Manufacturing Industry</source>
                  <year>1971</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1157a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1520e1164" publication-type="book">
M. Merhav, Technological
Dependence, Monopoly and Growth (New
York: Pergamon, 1969)<person-group>
                     <string-name>
                        <surname>Merhav</surname>
                     </string-name>
                  </person-group>
                  <source>Technological Dependence, Monopoly and Growth</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1193a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1520e1200" publication-type="journal">
K. Lancaster, "A New Approach
to Consumer Theory," Journal of Political
Economy, LXXIV (1966).<object-id pub-id-type="jstor">10.2307/1828835</object-id>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1216a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1520e1223" publication-type="book">
F. Stewart, Technology and
Underdevelopment (New York: Macmillan,
1977), ch. 4.<person-group>
                     <string-name>
                        <surname>Stewart</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">ch. 4</comment>
                  <source>Technology and Underdevelopment</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1255a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1520e1262" publication-type="journal">
Ranis.  </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1272a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1520e1279" publication-type="journal">
Y. Rhee and L. Westpha, "A Micro
Econometric Investigation of Choice of Tech-
nology," Journal of Development Economics, 4
(1977).<person-group>
                     <string-name>
                        <surname>Rhee</surname>
                     </string-name>
                  </person-group>
                  <volume>4</volume>
                  <source>Journal of Development Economics</source>
                  <year>1977</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1314a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1520e1321" publication-type="book">
M. K. Garg, Mini Sugar Project
Proposal and Feasibility Report ATDA;
Lucknow, 1979.<person-group>
                     <string-name>
                        <surname>Garg</surname>
                     </string-name>
                  </person-group>
                  <source>Mini Sugar Project Proposal and Feasibility Report ATDA</source>
                  <year>1979</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1350a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1520e1357" publication-type="book">
G. McRobie, Small is
Possible (Jonathon Cape, 1981)<person-group>
                     <string-name>
                        <surname>McRobie</surname>
                     </string-name>
                  </person-group>
                  <source>Small is Possible</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e1381" publication-type="book">
M. Carr,
Economically Appropriate Technologies for
Developing Countries (I.T. Publications,
1976)<person-group>
                     <string-name>
                        <surname>Carr</surname>
                     </string-name>
                  </person-group>
                  <source>Economically Appropriate Technologies for Developing Countries</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1413a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1520e1420" publication-type="book">
Westphal, Rhee, and Pursell  </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1429a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1520e1436" publication-type="journal">
S. Lall, "Is 'Dependence' a Useful
Concept in Analysing Underdevelopment?",
World Development, 3 (1975) for an illuminat-
ing critical discussion of the concept.<person-group>
                     <string-name>
                        <surname>Lall</surname>
                     </string-name>
                  </person-group>
                  <volume>3</volume>
                  <source>World Development</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1471a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1520e1478" publication-type="book">
UNCTAD, "Major Issues Arising
from the Transfer of Technology to Develop-
ing Countries," TD/B/A.C.11/19 Rev.1, 1975.<person-group>
                     <string-name>
                        <surname>UNCTAD</surname>
                     </string-name>
                  </person-group>
                  <source>Major Issues Arising from the Transfer of Technology to Developing Countries</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1508a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1520e1515" publication-type="book">
C. V. Vaitsos, Intercountry Income
Distribution and Transnational Enterprises
(New York: Oxford University Press, 1974)<person-group>
                     <string-name>
                        <surname>Vaitsos</surname>
                     </string-name>
                  </person-group>
                  <source>Intercountry Income Distribution and Transnational Enterprises</source>
                  <year>1974</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1520e1543" publication-type="book">
P. V. Roumelotis and C. P. Golemis,
"Transfer Pricing and the Power of Transna-
tional Enterprises in Greece," in Multina-
tionals and the Market: Transfer Pricing and
its Control, ed. R. Murray (Harvester, 1981)<person-group>
                     <string-name>
                        <surname>Roumelotis</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Transfer Pricing and the Power of Transnational Enterprises in Greece</comment>
                  <source>Multinationals and the Market: Transfer Pricing and its Control</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1581a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1520e1588" publication-type="book">
UNCTAD, "Major Issues."<person-group>
                     <string-name>
                        <surname>UNCTAD</surname>
                     </string-name>
                  </person-group>
                  <source>Major Issues</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1607a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1520e1614" publication-type="book">
C. Harvey in Economic Inde-
pendence in Africa, ed. D. Ghai (East African
Literature Bureau, 1973).<person-group>
                     <string-name>
                        <surname>Harvey</surname>
                     </string-name>
                  </person-group>
                  <source>Economic Independence in Africa</source>
                  <year>1973</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d1520e1643a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1520e1650" publication-type="book">
N. Rosenberg, Perspectives on Tech-
nology (Cambridge: Cambridge University
Press, 1976).<person-group>
                     <string-name>
                        <surname>Rosenberg</surname>
                     </string-name>
                  </person-group>
                  <source>Perspectives on Technology</source>
                  <year>1976</year>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
