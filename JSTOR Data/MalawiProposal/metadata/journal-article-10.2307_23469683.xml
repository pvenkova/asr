<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amereconrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100009</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The American Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Economic Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00028282</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23469683</article-id>
         <title-group>
            <article-title>Optimal Expectations and Limited Medical Testing: Evidence from Huntington Disease</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Emily</given-names>
                  <surname>Oster</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ira</given-names>
                  <surname>Shoulson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>E. Ray</given-names>
                  <surname>Dorsey</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">103</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23469610</issue-id>
         <fpage>804</fpage>
         <lpage>830</lpage>
         <permissions>
            <copyright-statement>Copyright© 2013 The American Economic Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23469683"/>
         <abstract>
            <p>We use novel data to study genetic testing among individuals at risk for Huntington disease (HD), a hereditary disease with limited life expectancy. Although genetic testing is perfectly predictive and carries little economic cost, presymptomatic testing is rare. Testing rates increase with increases in ex ante risk of having HD. Untested individuals express optimistic beliefs about their health and make decisions (e.g., retirement) as if they do not have HD, even though individuals with confirmed HD behave differently. We suggest that these facts can be reconciled by an optimal expectations model (Brunnermeier and Parker 2005).</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1114e222a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1114e229" publication-type="other">
Kószegi 2006</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1114e235" publication-type="other">
Kószegi (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1114e241" publication-type="other">
Brunnermeier and Parker (2005).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1114e248" publication-type="other">
Yariv (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1114e254" publication-type="other">
Mayraz
(2011).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1114e263" publication-type="other">
Bénabou and Tiróle (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1114e270a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1114e277" publication-type="other">
Shoulson and Young (2011).</mixed-citation>
            </p>
         </fn>
         <fn id="d1114e284a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1114e293" publication-type="other">
Brunnermeier and Parker (2005)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1114e309a1310">
            <mixed-citation id="d1114e313" publication-type="other">
Baer, Heather, Phyllis Brawarsky, Michael Murray, and Jennifer Haas. 2010. "Familial Risk of Cancer
and Knowledge and Use of Genetic Testing." Journal of General Internal Medicine 25 (7): 717-24.</mixed-citation>
         </ref>
         <ref id="d1114e323a1310">
            <mixed-citation id="d1114e327" publication-type="other">
Bénabou, Roland, and Jean Tiróle. 2002. "Self-Confidence and Personal Motivation." Quarterly Jour-
nal of Economics 117 (3): 871-915.</mixed-citation>
         </ref>
         <ref id="d1114e337a1310">
            <mixed-citation id="d1114e341" publication-type="other">
Brunnermeier, Markus K., and Jonathan A. Parker. 2005. "Optimal Expectations." American Eco-
nomic Review 95 (4): 1092-118.</mixed-citation>
         </ref>
         <ref id="d1114e351a1310">
            <mixed-citation id="d1114e355" publication-type="other">
Caplin, Andrew, and John Leahy. 2001. "Psychological Expected Utility Theory and Anticipatory
Feelings." Quarterly Journal of Economics 116 (1): 55-79.</mixed-citation>
         </ref>
         <ref id="d1114e366a1310">
            <mixed-citation id="d1114e370" publication-type="other">
Caplin, Andrew, and John Leahy. 2004. "The Supply of Information by a Concerned Expert." Eco-
nomic Journal 114 (497): 487-505.</mixed-citation>
         </ref>
         <ref id="d1114e380a1310">
            <mixed-citation id="d1114e384" publication-type="other">
Choi, James J., David Laibson, and Brigitte C. Madrian. 2011. "$100 Bills on the Sidewalk: Subopti-
mal Investment in 401 (K) Plans." Review of Economics and Statistics 93 (3): 748-63.</mixed-citation>
         </ref>
         <ref id="d1114e394a1310">
            <mixed-citation id="d1114e398" publication-type="other">
Choi, James J., David Laibson, Brigitte Madrian, and Andrew Metrick. 2002. "Defined Contribu-
tion Pensions: Plan Rules, Participant Choices, and the Path of Least Resistance." In Tax Policy and
the Economy Volume 16, edited by James M. Poterba, 67-113. Cambridge, MA: MIT Press for the
National Bureau of Economic Research.</mixed-citation>
         </ref>
         <ref id="d1114e414a1310">
            <mixed-citation id="d1114e418" publication-type="other">
Cummings, Linda, and Gregory Cooper. 2011. "Colorectal Cancer Screening: Update for 2011." Semi-
nars in Oncology 38 (4): 483-89.</mixed-citation>
         </ref>
         <ref id="d1114e428a1310">
            <mixed-citation id="d1114e432" publication-type="other">
Dawson, Erica, Thomas Gilovich, and Dennis Regan. 2002. "Motivated Reasoning and Performance
on the Wason Selection Task." Personality and Social Psychology Bulletin 28 (10): 1379-87.</mixed-citation>
         </ref>
         <ref id="d1114e442a1310">
            <mixed-citation id="d1114e446" publication-type="other">
DeSantis, Carol, Rebecca Siegel, Priti Bandi, and Ahmedin Jemal. 2011. "Breast Cancer Statistics,
2011." CA: A Cancer Journal of Clinicians 61 (6): 408-18.</mixed-citation>
         </ref>
         <ref id="d1114e457a1310">
            <mixed-citation id="d1114e461" publication-type="other">
Foster, C., M. Watson, R. Eeles, D. Eccles, S. Ashley, R. Davidson, J. Mackay, P. J. Morrison, P. Hop-
wood, and D. G. Evans. 2007. "Predictive Genetic Testing for BRCAVi in a UK Clinical Cohort:
Three-year Follow-up." British Journal of Cancer 96: 718-24.</mixed-citation>
         </ref>
         <ref id="d1114e474a1310">
            <mixed-citation id="d1114e478" publication-type="other">
Huntington Study Group PHAROS Investigators. 2006. "At Risk for Huntington Disease: The
PHAROS (Prospective Huntington At Risk Observational Study) Cohort Enrolled." Archives of
Neurology 63 (7): 991-96.</mixed-citation>
         </ref>
         <ref id="d1114e491a1310">
            <mixed-citation id="d1114e495" publication-type="other">
Köszegi, Botond. 2003. "Health Anxiety and Patient Behavior." Journal of Health Economics 22 (6):
1073-84.</mixed-citation>
         </ref>
         <ref id="d1114e505a1310">
            <mixed-citation id="d1114e509" publication-type="other">
Köszegi, Botond. 2006. "Emotional Agency." Quarterly Journal of Economics 121 (1): 121-55.</mixed-citation>
         </ref>
         <ref id="d1114e516a1310">
            <mixed-citation id="d1114e520" publication-type="other">
Laibson, David. 1997. "Golden Eggs and Hyperbolic Discounting." Quarterly Journal of Economics
112 (2): 443-77.</mixed-citation>
         </ref>
         <ref id="d1114e530a1310">
            <mixed-citation id="d1114e534" publication-type="other">
Langbehn, D. R., R. R. Brinkman, D. Falush, J. S. Paulsen, and M. R. Hayden. 2004. "A New Model
for Prediction of the Age of Onset and Penetrance for Huntington's Disease Based on CAG Length."
Clinical Genetics 65 (4): 267-77.</mixed-citation>
         </ref>
         <ref id="d1114e548a1310">
            <mixed-citation id="d1114e552" publication-type="other">
Lerman, C., C. Hughes, B. J. Trock, R. E. Myers, D. Main, A. Bonney, M. R. Abbaszadegan, et al. 1999.
"Genetic Testing in Families with Hereditary Nonpolyposis Colon Cancer." Journal of the American
Medical Association 281 (17): 1618-22.</mixed-citation>
         </ref>
         <ref id="d1114e565a1310">
            <mixed-citation id="d1114e569" publication-type="other">
Lerman, C., S. Narod, K. Schulman, C. Hughes, A. Gomez-Caminero, G. Bonney, K. Gold, et al. 1996.
"BRCA1 Testing in Families with Hereditary Breast-Ovarian Cancer. A Prospective Study of Patient
Decision Making and Outcomes." Journal of the American Medical Association 275 (24): 1885-92.</mixed-citation>
         </ref>
         <ref id="d1114e582a1310">
            <mixed-citation id="d1114e586" publication-type="other">
Matovu, Joseph, and Fredrick Makumbi. 2007. "Expanding Access to Voluntary HIV Counselling and
Testing in Sub-Saharan Africa: Alternative Approaches for Improving Uptake, 2001-2007." Tropi-
cal Medicine and International Health 12(11): 1315-22.</mixed-citation>
         </ref>
         <ref id="d1114e599a1310">
            <mixed-citation id="d1114e603" publication-type="other">
Mayraz, Guy. 2011. "Priors and Desires: A Model of Payoff-Dependent Beliefs." Unpublished.</mixed-citation>
         </ref>
         <ref id="d1114e610a1310">
            <mixed-citation id="d1114e614" publication-type="other">
Meijers-Heijboer, E. J., L. C. Verhoog, C. T. Brekelmans, C. Seynaeve, M. M. Tilanus-Linthorst, A.
Wagner, L. Dukel, et al. 2000. "Presymptomatic DNA Testing and Prophylatic Surgery in Families
with a BRCA1 or BRCA2 Mutation." The Lancet 355 (9220): 2015-20.</mixed-citation>
         </ref>
         <ref id="d1114e627a1310">
            <mixed-citation id="d1114e631" publication-type="other">
Myers, Richard. 2004. "Huntington's Disease Genetics." NeuroRX 1 (2): 255-62.</mixed-citation>
         </ref>
         <ref id="d1114e639a1310">
            <mixed-citation id="d1114e643" publication-type="other">
Oster, Emily, E. Ray Dorsey, Jan Bausch, Aileen Shinaman, Elise Kayson, David Oakes, Ira Shoulson,
and Kimberly Quaid. 2008. "Fear of Health Insurance Loss Among Individuals at Risk for Hunting-
ton Disease." American Journal of Medical Genetics 146A (16): 2070-77.</mixed-citation>
         </ref>
         <ref id="d1114e656a1310">
            <mixed-citation id="d1114e660" publication-type="other">
Oster, Emily, Ira Shoulson, and E. Ray Dorsey. Forthcoming. "Limited Life Expectancy, Human Capi-
tal. and Health Investments." American Economic Review.</mixed-citation>
         </ref>
         <ref id="d1114e670a1310">
            <mixed-citation id="d1114e674" publication-type="other">
Oster, Emily, Ira Shoulson, and E. Ray Dorsey. 2013. "Optimal Expectations and Limited Medical
Testing: Evidence from Huntington Disease: Dataset." American Economic Review, http://dx.doi.
org/10.1257/aer,103.2.804.</mixed-citation>
         </ref>
         <ref id="d1114e687a1310">
            <mixed-citation id="d1114e691" publication-type="other">
Oster, Emily, Ira Shoulson, Kimberly Quaid, and E. Ray Dorsey. 2010. "Genetic Adverse Selection:
Evidence from Long-Term Care Insurance and Huntington Disease." Journal of Public Economics
94 (11-12): 1041-50.</mixed-citation>
         </ref>
         <ref id="d1114e704a1310">
            <mixed-citation id="d1114e708" publication-type="other">
Quaid, Kimberly, and Michael Morris. 1993. "Reluctance to Undergo Predictive Testing: The Case of
Huntington Disease." American Journal of Medical Genetics 45 (l): 41-45.</mixed-citation>
         </ref>
         <ref id="d1114e718a1310">
            <mixed-citation id="d1114e722" publication-type="other">
Roberts, J. Scott, Melissa Barber, Tamsen M. Brown, L. Adrienne Cupples, Lindsay A. Farrer, Susan
A. LaRusse, Stephen G. Post, et al. 2004. "Who Seeks Genetic Susceptibility Testing for Alzheim-
er's Disease? Findings from a Multisite Randomized Clinical Trial." Genetics in Medicine 6 (4):
197-203.</mixed-citation>
         </ref>
         <ref id="d1114e739a1310">
            <mixed-citation id="d1114e743" publication-type="other">
Ropka, Mary, Jennifer Wenzel, Elayne Phillips, Mir Siadaty, and John Philbrick. 2006. "Uptake Rates
for Breast Cancer Genetic Testing: A Systematic Review." Cancer Epidemiology, Biomarkers &amp;
Prevention 15 (5): 840-44.</mixed-citation>
         </ref>
         <ref id="d1114e756a1310">
            <mixed-citation id="d1114e760" publication-type="other">
Schwartz, Marc, Elizabeth Kaufman, Beth Peshkin, Claudine Isaacs, Chanita Hughes, Tiffani
DeMarco, Clinton Finch, and Caryn Lerman. 2003. "Bilateral Prophylactic Oophorectomy and
Ovarian Cancer Screening Following BRCA1/BRCA2 Mutation Testing." Journal of Clinical
Oncology 21 (21): 4034-41.</mixed-citation>
         </ref>
         <ref id="d1114e776a1310">
            <mixed-citation id="d1114e780" publication-type="other">
Shoulson, Ira, and Anne Young. 2011. "Milestones in Huntington Disease." Movement Disorders 26
(6): 1127-33.</mixed-citation>
         </ref>
         <ref id="d1114e790a1310">
            <mixed-citation id="d1114e794" publication-type="other">
Thornton, Rebecca L. 2008. "The Demand for, and Impact of, Learning HIV Status." American Eco-
nomic Review 98 (5): 1829-63.</mixed-citation>
         </ref>
         <ref id="d1114e804a1310">
            <mixed-citation id="d1114e808" publication-type="other">
Weinhardt, Lance, Michael Carey, Blair Johnson, and Nicole Bickham. 1999. "Effects of HIV Coun-
seling and Testing on Sexual Risk Behavior: A Meta-Analytic Review of Published Research, 1985-
1997." American Journal of Public Health 89 (9): 1397-1405.</mixed-citation>
         </ref>
         <ref id="d1114e821a1310">
            <mixed-citation id="d1114e825" publication-type="other">
Yariv, Leeat. 2005. "I'll See It When I Believe It: A Simple Model of Cognitive Consistency."
Unpublished.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
