<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1x71q0</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>A History of Malawi</book-title>
         <subtitle>1859-1966</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>McCRACKEN</surname>
               <given-names>JOHN</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>20</day>
         <month>09</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9781847010506</isbn>
      <isbn content-type="epub">9781782040286</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk; Rochester, NY</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>John McCracken</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt1x71q0"/>
      <abstract abstract-type="short">
         <p>This is the first comprehensive history of Malawi during the colonial period. Using a wide range of primary and secondary sources, it places this history within the context of the pre-colonial past. The book examines the way in which British people, starting with David Livingstone, followed by the pioneer Scottish Presbyterian missionaries and including soldiers, speculators, colonial officials and politicians, played an influential part in shaping Malawi. But even more important is the story of how Malawian people responded to the intrusion of colonialism and imperialism and the role they played in the dissolution of the colonial state. There is much here on resistance to colonial occupation, including religious-inspired revolt, on the shaping of the colonial economy, on the influence of Christian missions and on the growth of a powerful popular nationalism that contained within it the seeds of a new authoritarianism. But space is also given to less mainstream activities: the creation of dance societies, the eruption of witchcraft eradication movements and the emergence of football as a popular national sport. In particular, the book seeks to demonstrate the interrelationship between environmental and economic change and the impact these forces had on a poverty-stricken yet resilient Malawian peasantry. John McCracken is Honorary Senior Research Fellow, Stirling University. He has taught at University College of Rhodesia and Nyasaland, University College of Dar es Salaam and was Professor and Head of the Department of History at Chancellor College, University of Malawi from 1980-83 and returned as Visiting Professor in 2009. John McCracken was awarded ASAUK's Distinguished Africanist Award in 2008.</p>
      </abstract>
      <counts>
         <page-count count="504"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.3</book-part-id>
                  <title-group>
                     <title>List of Maps, Photographs &amp; Tables</title>
                  </title-group>
                  <fpage>x</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.5</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.6</book-part-id>
                  <title-group>
                     <title>Note on Terminology</title>
                  </title-group>
                  <fpage>xiv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.7</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>xiv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.8</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
                  <fpage>xvi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.9</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>There can be no better introduction to the character of colonial rule in Malawi than the view from the lip of Zomba plateau looking down on the old colonial capital. In the distance, nearly 50 miles away, rears Mulanje Mountain, a rocky massif divided from its neighbour Mchesa by the Fort Lister Gap, a pass used by slave traders in the late nineteenth century. Further to the south, on the road to Blantyre, looms the jutting eminence of Chiradzulu and, closer to Zomba, the small hill known as Magomero, once the site of the headquarters of A.L. Bruce’s Magomero estate,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.10</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Land &amp; the People</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract abstract-type="extract">
                     <p>For the peoples living in what is now the modern state of Malawi, the forty years prior to the establishment of colonial rule in 1891 was a period of exceptionally violent and rapid change. During those decades, groups of refugees from Southern Africa, collectively known as Ngoni, stormed northwards and again south, seizing people, cattle and agricultural resources and eventually creating three major and two minor conquest states in the region. Yao-speaking peoples from the east of Lake Malawi, wielding guns and trading in slaves, conquered much of the Upper Shire Valley and Shire Highlands; in the Lower Shire Valley,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.11</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Commerce, Christianity &amp; Colonial Conquest</title>
                  </title-group>
                  <fpage>38</fpage>
                  <abstract abstract-type="extract">
                     <p>During debates in Nyasaland’s legislative council in the run up to independence the nationalist politician, Henry Masauko Chipembere, used to delight his supporters and scandalise his opponents by describing David Livingstone as a ‘tourist’. In certain respects, Chipembere’s comment was sound. Livingstone, despite his vehement claims to the contrary, was not the first European to ‘discover’ Lakes Malawi and Chilwa; nor did he travel over previously unknown territory: almost wherever he went, he was guided by local Africans who escorted him along well used tracks, employed in both regional and international trade.¹ Yet if Livingstone’s claims as a pioneer explorer</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.12</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Making of the Colonial Economy 1891–1915</title>
                  </title-group>
                  <fpage>74</fpage>
                  <abstract abstract-type="extract">
                     <p>No period in the colonial history of Malawi is more important than the two decades following the declaration of a British Protectorate. During these years the British not only established their territorial hegemony through force, they also brought about a fundamental reshaping of the country’s economy along lines that are still familiar today. Long distance trade in gathered items such as ivory declined and was gradually replaced by trade in agricultural staples: principally coffee, cotton and tobacco. In the Shire Highlands much land was alienated to a small group of settlers. But the plantation economy they created had to compete</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.13</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Religion, Culture &amp; Society</title>
                  </title-group>
                  <fpage>100</fpage>
                  <abstract abstract-type="extract">
                     <p>The large-scale adoption of Christianity, accompanied in parts of the south by the large-scale adoption of Islam, is a major theme in the history of colonial Malawi. Not only did it involve an enormous increase in the number of Malawians who identified themselves as Christians, it also resulted in the emergence of new, vibrant religious communities, authentically African in ethos yet closely linked ideologically and institutionally with Christian churches in the West. As Schoffeleers, in particular, has shown, central to the process was the growing interaction between indigenous and external religious systems and beliefs.¹ But also important was the pervading</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.14</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Chilembwe Rising</title>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract abstract-type="extract">
                     <p>On the night of Saturday 23 January 1915 a party of armed men made their way the eight miles from Mbombwe near Chiradzulu, the headquarters of the Revd John Chilembwe’s Providence Industrial Mission, to Magomero, the headquarters of the A.L. Bruce Estates. There they killed two Europeans, including the estate manager, William Jervis Livingstone. They then cut off his head and carried it back to Chiradzulu where it was displayed on a pole at the Sunday service conducted next day by Chilembwe. Livingstone’s wife Katherine, two other European women and their children were escorted across the Phalombe plain towards Mbombwe</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.15</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Malawi &amp; the First World War</title>
                  </title-group>
                  <fpage>147</fpage>
                  <abstract abstract-type="extract">
                     <p>European politics were exported to Africa in a particularly oppressive form during the First World War. In military terms, the East African campaign was a sideshow, a distraction from the titanic struggle being conducted on the Western Front in France. Yet for millions of Africans (and many thousands of Malawians) the war marked the high point of colonial violence: a period in which the compromises of the colonial state fell away to reveal a naked demand for manpower, both as soldiers and even more as porters, and for supplies of grain and cattle. Nyasaland was largely spared the horror inflicted</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.16</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Planters, Peasants &amp; Migrants The Interwar Years</title>
                  </title-group>
                  <fpage>162</fpage>
                  <abstract abstract-type="extract">
                     <p>In his political history of Southern Rhodesia, Lord Blake describes the inter-war years as a ‘not very interesting’ period in the colony’s history, when little of significance took place.¹ In Nyasaland, by contrast, the interwar years were marked by profound although unspectacular changes, resulting in the creation of economic structures that continued virtually unaltered into the 1960s. Estate agriculture, once seen as the leading sector in the territory’s economy, went into steep decline, while peasant communities emerged into more active participation in the cash economy, only to run headlong into the Great Depression. On a political level, the fierce dramas</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.17</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The Great Depression &amp; its Aftermath</title>
                  </title-group>
                  <fpage>193</fpage>
                  <abstract abstract-type="extract">
                     <p>Historians are divided on the impact of the Great Depression on third world societies. One view, expressed most eloquently by John Iliffe for Tanganyika, is that the period originating with the onset of the world depression in 1929 and continuing up to 1945 was a turning point, marking both the emergence of colonial society in its most complete form and the beginning of its dissolution.¹ The alternative argument, advanced most vigorously in a collection of essays edited by Ian Brown, is that the impact of the depression on non-Western societies was much less damaging than is conventionally believed. Peasants suffered</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.18</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Contours of Colonialism</title>
                  </title-group>
                  <fpage>215</fpage>
                  <abstract abstract-type="extract">
                     <p>Three features distinguish the colonial power structure that emerged in Nyasaland in the inter-war years and which continued to influence patterns of rural government up to the coming of independence. The first was the creation of a ‘prefectural administration’, as Berman describes it, ‘staffed by an elite cadre of political officers acting as direct agents for the central government, and exercising diffuse and wide-ranging powers’.¹ The central element was the secretariat in Zomba which engaged both with the Colonial Office in London and with the provincial and district administration spread across the Protectorate. This basic structure came into force as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.19</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Age of Development</title>
                  </title-group>
                  <fpage>237</fpage>
                  <abstract abstract-type="extract">
                     <p>In May 1949 Col. Laurens van der Post visited Malawi on behalf of the Colonial Development Corporation to investigate the economic potential of the two main mountainous areas in the country, Mulanje and the Nyika Plateau. In <italic>Venture to the Interior</italic>, the immensely popular account of his expedition – still in print and with sales of over a million – van der Post recorded his impressions of a territory on the cusp of economic and political change. Blantyre was a disappointment, its buildings ‘drab and insignificant... dumped by the side of a road full of dust’. But of even greater</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.20</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Urban Experience</title>
                  </title-group>
                  <fpage>282</fpage>
                  <abstract abstract-type="extract">
                     <p>At the time of independence in 1964 over 95 per cent of Malawi’s population lived in the countryside and the economy was almost entirely agricultural in character. Nevertheless, the importance of Malawi’s colonial towns should not be underestimated. In their different ways, settlements like Blantyre, Limbe, Zomba and Lilongwe epitomised the colonial imagination at its most vivid in the way that urban space was ordered into precisely designated functions, normally involving the segregation of the European zone from Asian and African sections. Although towns were thus perceived as strong points in the colonial system of control the reality was often</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.21</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Peasants &amp; Politicians 1943–1953</title>
                  </title-group>
                  <fpage>304</fpage>
                  <abstract abstract-type="extract">
                     <p>During the 1940s and 1950s, the colonial government of Malawi was faced by mounting opposition larger in scale and more intense in type than anything it had previously experienced. The nature of the opposition varied considerably from place to place. In the Shire Highlands, long-term resentment among tenants over the exaction of <italic>thangata</italic> on European-owned estates deepened from the early 1940s, leading in 1953 to an outbreak of peasant-based violence, the nearest that Central Africa experienced at this period to the Mau Mau revolt in Kenya. Independent peasants in the Central Province were aggrieved by the policies pursued by the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.22</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>The Liberation Struggle 1953–1959</title>
                  </title-group>
                  <fpage>336</fpage>
                  <abstract abstract-type="extract">
                     <p>The decade ending in 1964 was the most dramatic in the twentieth century history of Malawi. At one level, the key feature was the transformation of Congress from the weak, divided movement of 1953 into the infinitely larger, more united, more powerful Malawi Congress Party that was to dominate Malawian politics in the early 1960s. As modern research has demonstrated, in many parts of Africa the rhetoric of nationalist ‘struggle’ disguised the reality that political independence was the outcome of a negotiated settlement achieved as much through the input of colonial planners in Europe as it was through the agitation</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.23</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>The Making of Malawi 1959–1963</title>
                  </title-group>
                  <fpage>366</fpage>
                  <abstract abstract-type="extract">
                     <p>The reconstitution of the Nyasaland African Congress from September 1959 as the Malawi Congress Party (MCP) marked the beginnings of a significant shift in the character of Malawian politics. Often regarded as the NAC by another name, the MCP quickly emerged as one of the most popular, dynamic and successful nationalist parties in Africa, infinitely larger than its predecessor, much more consciously ‘traditionalist’ in style, distinctly more illiberal in its attitudes and drawing support from regions into which Congress had barely penetrated. It was also important that while the Nyasaland African Congress was, above all, an anti-colonial protest movement seeking</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.24</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Prelude to Independence:</title>
                     <subtitle>Unity &amp; Diversity</subtitle>
                  </title-group>
                  <fpage>403</fpage>
                  <abstract abstract-type="extract">
                     <p>At the Marlborough House conference in November 1962, Dr Banda looked back with justifiable pride on his party’s ‘successful and creditable performance in office’.¹ A range of important legislative measures had been introduced; Chiume, Chisiza and particularly Banda himself had delivered a number of eloquent and forceful speeches in the Legislative Council; all the ministers had demonstrated a capacity to master their portfolios that the experienced Henry Phillips found very impressive.² Banda could legitimately deride the many critics who had forecast administrative and financial disaster. Yet, these early sessions of the Legislative Council had also brought into focus tensions that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.25</book-part-id>
                  <title-group>
                     <label>16</label>
                     <title>Revolt &amp; Realignment 1964–1966</title>
                  </title-group>
                  <fpage>429</fpage>
                  <abstract abstract-type="extract">
                     <p>According to older narratives, the lowering of the Union Jack at Rangeley (later Kamuzu) Stadium at midnight on 5 July 1964 marked awatershed, the moment when Malawi became free. Modern historians, more sceptical about what precisely independence entailed, might point to alternative events as marking the key transition: the two-day emergency debate in Parliament on 8–9 September, when Banda won a vote of confidence over his younger cabinet colleagues; Chipembere’s failed coup d’état in February 1965, the point at which it became clear that Banda’s regime would not be overthrown by force; perhaps even the economic crisis of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.26</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>461</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.27</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>472</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1x71q0.28</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>487</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
