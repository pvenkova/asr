<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">eurojhealecon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101038</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The European Journal of Health Economics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">16187598</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16187601</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41474347</article-id>
         <title-group>
            <article-title>Analysis of the validity of the vignette approach to correct for heterogeneity in reporting health system responsiveness</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nigel</given-names>
                  <surname>Rice</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Silvana</given-names>
                  <surname>Robone</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40071758</issue-id>
         <fpage>141</fpage>
         <lpage>162</lpage>
         <permissions>
            <copyright-statement>© Springer-Verlag Berlin Heidelberg 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41474347"/>
         <abstract>
            <p>Despite the growing popularity of the vignette methodology to deal with self-reported, categorical data, the formal evaluation of the validity of this methodology is still a topic of research. Some critical assumptions need to hold in order for this method to be valid. In this paper we analyse the assumption of "vignette equivalence" using data on health system responsiveness contained within the World Health Survey. We perform several tests to check the assumption of vignette equivalence. First, we use a test based on the global ordering of the vignettes. A minimal condition for the assumption of vignette equivalence to hold is that individual responses are consistent with the global ordering of vignettes. Secondly, using the hierarchical ordered probit model (HOPIT) model on the pool of countries, we undertake sensitivity analyses, stratifying countries according to the Inglehart-Welzel scale and the Human Development Index. The results of this analysis are robust, suggesting that the vignette equivalence assumption is not contradicted. Thirdly, we model the reporting behaviour of the respondents through a two-step regression procedure to evaluate whether the vignettes construct is perceived by respondents in different ways. Overall, across the analyses the results do not contradict the assumption of vignette equivalence and accordingly lend support to the use of the vignette methodology when analysing selfreported data and health system responsiveness.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1719e332a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1719e339" publication-type="other">
Kristensen and Johansson [11].</mixed-citation>
            </p>
         </fn>
         <fn id="d1719e346a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1719e353" publication-type="other">
Murray
et al. [19], Fig. 30.3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1719e363a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1719e370" publication-type="other">
Kapteyn et al. [5]</mixed-citation>
            </p>
         </fn>
         <fn id="d1719e377a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1719e384" publication-type="other">
Murray et al. [19],</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1719e400a1310">
            <label>1</label>
            <mixed-citation id="d1719e407" publication-type="other">
Murray, C, Frenk, J.: A framework for assessing the performance
of health systems. Bull. World Health Org. 78, 717-731 (2000)</mixed-citation>
         </ref>
         <ref id="d1719e417a1310">
            <label>2</label>
            <mixed-citation id="d1719e424" publication-type="other">
Valentine, N., De Silva, A., Kawabata, K., Darby, C., Murray,
C.J.L., Evans, D.: Health system responsiveness: concepts,
domains and operationalization. In: Murray, C.J.L., Evans, D.B.
(eds.) Health systems performance assessment: debates, methods
and empiricism, pp. 573-596. World Health Organization, Gen-
eva (2003)</mixed-citation>
         </ref>
         <ref id="d1719e447a1310">
            <label>3</label>
            <mixed-citation id="d1719e454" publication-type="other">
Salomon, J., Tandon, A., Murray, C.J.L., World Health Survey
Pilot Study Collaborating Group: Comparability of self-rated
health: cross sectional multi-country survey using anchoring
vignettes. Brit. Med. J. 328(258), 258-261 (2004)</mixed-citation>
         </ref>
         <ref id="d1719e470a1310">
            <label>4</label>
            <mixed-citation id="d1719e477" publication-type="other">
Bago d'Uva, T., van Doorlsaer, E., Lindeboom, M., O'Donnell,
O.: Does reporting heterogeneity bias the measurement of health
disparities? Health Econ. 17(3), 351-375 (2008)</mixed-citation>
         </ref>
         <ref id="d1719e491a1310">
            <label>5</label>
            <mixed-citation id="d1719e498" publication-type="other">
Kapteyn, A., Salomon, J., van Soest, A.: Vignettes and self-
reports of work disability in the US and the Netherlands. AER
97(1), 461-473 (2007)</mixed-citation>
         </ref>
         <ref id="d1719e511a1310">
            <label>6</label>
            <mixed-citation id="d1719e518" publication-type="other">
Van Soest, A., Delaney, A., Harmon, C., Kapteyn, A., Smith, J.P.:
Validating the use of vignettes for subjective threshold scales.
Discussion Paper, Tilburg University (2007)</mixed-citation>
         </ref>
         <ref id="d1719e531a1310">
            <label>7</label>
            <mixed-citation id="d1719e538" publication-type="other">
King, G., Murray, C.J.L., Salomon, J., Tandon, A.: Enhancing the
validity and cross-cultural comparability of measurement in
survey research. Am. Polit. Sei. Rev. 98(1), 184-191 (2004)</mixed-citation>
         </ref>
         <ref id="d1719e551a1310">
            <label>8</label>
            <mixed-citation id="d1719e558" publication-type="other">
Rice, N., Robone, S., Smith, P.C.: Analysis of the validity of the
vignette approach to correct for heterogeneity in reporting health
system responsiveness. HEDG Working Paper, University of
York (2009)</mixed-citation>
         </ref>
         <ref id="d1719e574a1310">
            <label>9</label>
            <mixed-citation id="d1719e581" publication-type="other">
Rice, N., Robone, S., Smith, P.C.: International comparison of
public sector performance: the use of anchoring vignettes to
adjust self-reported data. Evaluation 16(1), 81-101 (2010)</mixed-citation>
         </ref>
         <ref id="d1719e594a1310">
            <label>10</label>
            <mixed-citation id="d1719e601" publication-type="other">
Sirven, N., Santos-Eggimann, B., Spagnoli, J.: Comparability of
health care responsiveness in Europe using anchoring vignettes
from SHARE. IRDES working paper DTI 5 (2008)</mixed-citation>
         </ref>
         <ref id="d1719e615a1310">
            <label>11</label>
            <mixed-citation id="d1719e622" publication-type="other">
Kristensen, N., Johansson, E.: New evidence on cross-country
differences in job satisfaction using anchoring vignettes. Labour
Econ. 15, 96-117 (2008)</mixed-citation>
         </ref>
         <ref id="d1719e635a1310">
            <label>12</label>
            <mixed-citation id="d1719e642" publication-type="other">
Hsee, C.K., Tang, J.N.: Sun and water: on a modulus-based
measurement of happiness. Emotion 7, 213-218 (2007)</mixed-citation>
         </ref>
         <ref id="d1719e652a1310">
            <label>13</label>
            <mixed-citation id="d1719e659" publication-type="other">
Javaras, K.N., Ripley, B.D.: An "unfolding" latent variable
model for likert attitude data: drawing inferences adjusted for
response style. JASA 102(478), 454-463 (2007)</mixed-citation>
         </ref>
         <ref id="d1719e672a1310">
            <label>14</label>
            <mixed-citation id="d1719e679" publication-type="other">
Grzymala-Busse, A.: Rebuilding Levithan: party competition and
state exploitation in post-communist democracies. Cambridge
University Press, New York (2007)</mixed-citation>
         </ref>
         <ref id="d1719e692a1310">
            <label>15</label>
            <mixed-citation id="d1719e699" publication-type="other">
Bago d'Uva, T., Lindeboom, M., O'Donnell, O., van Doorslaer
E.: Slipping anchor? Testing the vignettes approach to identifi-
cation and correction of reporting heterogeneity. HEDG Working
Paper, University of York (2009)</mixed-citation>
         </ref>
         <ref id="d1719e715a1310">
            <label>16</label>
            <mixed-citation id="d1719e722" publication-type="other">
Hopkins, D., King, G.: Improving anchoring vignettes: designing
surveys to correct interpersonal incomparability. Public Opin. Q.
(2010) (in press)</mixed-citation>
         </ref>
         <ref id="d1719e736a1310">
            <label>17</label>
            <mixed-citation id="d1719e743" publication-type="other">
Gupta N., Kristensen, N., Pozzoli, D.: External validation of the
use of vignettes in cross-country health studies. IZA Discussion
Paper (2008)</mixed-citation>
         </ref>
         <ref id="d1719e756a1310">
            <label>18</label>
            <mixed-citation id="d1719e763" publication-type="other">
Wand, J.: Credible comparisons using interpersonally incompa-
rable data: ranking self-evaluations relative to anchoring vign-
ettes or other common survey questions. Mimeo (2007)</mixed-citation>
         </ref>
         <ref id="d1719e776a1310">
            <label>19</label>
            <mixed-citation id="d1719e783" publication-type="other">
Murray, C.J.L., Ozaltin, E., Tandon, A.J., Salomon, J.: Empirical
evaluation of the anchoring vignettes approach in health surveys.
In: Murray, C.J.L., Evans, D.B. (eds.) Health systems perfor-
mance assessment: debates, methods and empiricism, pp. 369-
399. World Health Organization, Geneva (2003)</mixed-citation>
         </ref>
         <ref id="d1719e802a1310">
            <label>20</label>
            <mixed-citation id="d1719e809" publication-type="other">
King, G., Wand, J.: Comparing incomparable survey responses:
new tools for anchoring vignettes. Polit. Anal. 15(1, Winter), 46-
66 (2007)</mixed-citation>
         </ref>
         <ref id="d1719e822a1310">
            <label>21</label>
            <mixed-citation id="d1719e829" publication-type="other">
Kapteyn, A., Salomon J., van Soest, A.: Are Americans really
less happy with their incomes? Rand Working Paper (2008)</mixed-citation>
         </ref>
         <ref id="d1719e839a1310">
            <label>22</label>
            <mixed-citation id="d1719e846" publication-type="other">
Üstün, T.B., Chatterji, S., Mechbal, A., Murray, C.: The world
health surveys. In: Murray, C.J.L., Evans, D.B. (eds.) Health
systems performance assessment: debates, methods and empiri-
cism, pp. 762-796. World Health Organization, Geneva (2003)</mixed-citation>
         </ref>
         <ref id="d1719e863a1310">
            <label>23</label>
            <mixed-citation id="d1719e870" publication-type="other">
Valentine, N., Prasat, A., Rice, N., Robone, S., Chatterji, S.:
Health systems responsiveness -a measure of the acceptability of
health care processes and systems. In: Mossialos, E., Smith, P.,
Leatherman, S. (eds.) Performance measurement for health sys-
tem improvement: experiences, challenges and prospects, pp.
256-305. WHO European Regional Office, London (2009)</mixed-citation>
         </ref>
         <ref id="d1719e893a1310">
            <label>24</label>
            <mixed-citation id="d1719e900" publication-type="other">
Ferguson, B.D., Tandon, A., Gakidou, E., Murray, C.J.L.: Esti-
mating permanent income using indicator variables. In: Murray,
C.J.L., Evans, D.B. (eds.) Health systems performance assess-
ment: debates, methods and empiricism, pp. 748-760. World
Health Organization, Geneva (2003)</mixed-citation>
         </ref>
         <ref id="d1719e919a1310">
            <label>25</label>
            <mixed-citation id="d1719e926" publication-type="other">
United Nation Development Programme: Human Development
Report. New York (2006)</mixed-citation>
         </ref>
         <ref id="d1719e936a1310">
            <label>26</label>
            <mixed-citation id="d1719e943" publication-type="other">
Inglehart R.: Inglehart-Welzel cultural map of the World.
http://www.worldvaluessurvey.org</mixed-citation>
         </ref>
         <ref id="d1719e953a1310">
            <label>27</label>
            <mixed-citation id="d1719e960" publication-type="other">
Tandon, A., Murray, C.J.L., Salomon, J.A., King, G.: Statistical
models for enhancing cross-population comparability. In: Mur-
ray, C.J.L., Evans, D.B. (eds.) Health systems performance
assessment: debates, methods and empiricism, pp. 727-746.
World Health Organization, Geneva (2003)</mixed-citation>
         </ref>
         <ref id="d1719e979a1310">
            <label>28</label>
            <mixed-citation id="d1719e986" publication-type="other">
Terza, J.V.: Ordinal probit: a generalization. Commun. Stat.
14(1), 1-11 (1985)</mixed-citation>
         </ref>
         <ref id="d1719e997a1310">
            <label>29</label>
            <mixed-citation id="d1719e1004" publication-type="other">
Lewis, J.B., Linzer, D.A.: Estimating regression models in which
the dependent variable is based on estimates. Polit. Anal. 13,
345-364 (2005)</mixed-citation>
         </ref>
         <ref id="d1719e1017a1310">
            <label>30</label>
            <mixed-citation id="d1719e1024" publication-type="other">
Jones, A.M., Rice, N., D'Uva, T.B., Balia, S.: Applied health
economics. Routledge, New York (2007)</mixed-citation>
         </ref>
         <ref id="d1719e1034a1310">
            <label>31</label>
            <mixed-citation id="d1719e1041" publication-type="other">
Efron, B.: The Jackknife, the Bootstrap and other resampling
plans. Society for Industrial and Applied Mathematics, Phila-
delphia (1982)</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
