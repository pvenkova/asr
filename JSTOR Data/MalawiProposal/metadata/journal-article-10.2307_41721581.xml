<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jpeaceresearch</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100245</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Peace Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00223433</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41721581</article-id>
         <title-group>
            <article-title>Does transnational terrorism reduce foreign direct investment? Business-related versus non-business-related terrorism</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Matthew</given-names>
                  <surname>Powers</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Seung-Whan</given-names>
                  <surname>Choi</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>5</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">49</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40080993</issue-id>
         <fpage>407</fpage>
         <lpage>422</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 Peace Research Institute Oslo</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41721581"/>
         <abstract>
            <p>Although several existing studies examine the economic impact of transnational terrorism by referring to its potential to reduce foreign direct investment (FDI), they overlook possible differences in the effects of business-related and non-business-related terrorism. We argue that the former type of terror negatively affects FDI since it damages multinationals' buildings, destroys their products, kills their employees, and causes a rise in insurance premiums. The latter type of terror, however, does not induce the same ramifications and should thus have little or less influence on a country's FDI. In order to examine the effects of these two different types of transnational terrorism, we employ three different statistical techniques using data gleaned from the International Terrorism: Attributes of Terrorist Events (ITERATE) dataset. A cross-sectional, time-series data analysis of 123 developing countries during the period from 1980 to 2008 reveals that transnational terrorism that harms multinational businesses contributes to a decrease of foreign investment but transnational terrorism that afflicts non-business-related targets is statistically irrelevant. This implies that when countries implement counterterrorism measures that are directly intended to mitigate the impact of business-related terrorist activities, they are likely to attract more foreign capital and should therefore realize a greater degree of economic development.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1211e176a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1211e183" publication-type="other">
Enders, Sachsida &amp; Sandler, 2006</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1211e189" publication-type="other">
Enders &amp; Sandler,
1996</mixed-citation>
            </p>
         </fn>
         <fn id="d1211e199a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1211e206" publication-type="other">
Banks,
2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1211e215" publication-type="other">
World Bank, 2010</mixed-citation>
            </p>
         </fn>
         <fn id="d1211e222a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1211e229" publication-type="other">
LaFree &amp; Dugan, 2007</mixed-citation>
            </p>
         </fn>
         <fn id="d1211e236a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1211e243" publication-type="other">
Zorn, 2001</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1211e249" publication-type="other">
Zorn (2001).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1211e265a1310">
            <mixed-citation id="d1211e269" publication-type="other">
Abadie, Alberto &amp; Javier Gardeazabal (2008) Terrorism and the
world economy. European Economic Review 52(1): 1-27.</mixed-citation>
         </ref>
         <ref id="d1211e279a1310">
            <mixed-citation id="d1211e283" publication-type="other">
Achen, Christopher (2000) Why lagged dependent variables
can suppress the explanatory power of other independent
variables. Working paper, Society for Political Methodol-
ogy, St Louis, WA.</mixed-citation>
         </ref>
         <ref id="d1211e299a1310">
            <mixed-citation id="d1211e303" publication-type="other">
Aisbett, Emma (2007) Bilateral investment treaties and for-
eign direct investment: Correlation versus causation.
CUDARE Working paper No. 1032. Berkeley, CA: Uni-
versity of California, Berkeley.</mixed-citation>
         </ref>
         <ref id="d1211e319a1310">
            <mixed-citation id="d1211e323" publication-type="other">
Banks, Arthur (2004) Cross-National Time-Series Data
Archive. Bingham ton, NY: Center for Social Analysis.</mixed-citation>
         </ref>
         <ref id="d1211e334a1310">
            <mixed-citation id="d1211e338" publication-type="other">
Beck, Nathaniel &amp; Jonathan Katz (1995) What to do (and not
to do) with time-series cross-sectional data. American Polit-
ical Science Review 89(3): 634-647.</mixed-citation>
         </ref>
         <ref id="d1211e351a1310">
            <mixed-citation id="d1211e355" publication-type="other">
Biglaiser, Glen &amp; Karl DeRouen (2007) Following the flag:
Troop deployment and US foreign direct investment. Inter-
national Studies Quarterly 51(4): 835-854.</mixed-citation>
         </ref>
         <ref id="d1211e368a1310">
            <mixed-citation id="d1211e372" publication-type="other">
Blomberg, Brock &amp; Ashoka Mody (2005) How severely
does violence deter international investment? Typescript.
Department of Economics, Claremont McKenna College,
Claremont, CA.</mixed-citation>
         </ref>
         <ref id="d1211e388a1310">
            <mixed-citation id="d1211e392" publication-type="other">
Blomberg, Brock; Gregory Hess &amp; Daniel Tan (2011) Terror-
ism and the economics of trust. Journal of Peace Research
48(3): 383-398.</mixed-citation>
         </ref>
         <ref id="d1211e405a1310">
            <mixed-citation id="d1211e409" publication-type="other">
Blonigen, Bruce &amp; Miao Wang (2005) Inappropriate pooling
of wealthy and poor countries in empirical FDI studies. In:
Theodore Moran, Edward Graham &amp; Magnus Blomstrom
(eds) Does Foreign Direct Investment Promote Development?
Washington, DC: Institute for International Economics,
221-244.</mixed-citation>
         </ref>
         <ref id="d1211e432a1310">
            <mixed-citation id="d1211e436" publication-type="other">
Burkhart, Ross &amp; Michael Lewis-Beck (1994) Comparative
democracy: The economic development thesis. American
Political Science Review 88(4): 903-910.</mixed-citation>
         </ref>
         <ref id="d1211e450a1310">
            <mixed-citation id="d1211e454" publication-type="other">
Büthe, Tim &amp; Helen Milner (2008) The politics of foreign
direct investment into developing countries: Increasing
FDI through international trade agreements? American
Journal of Political Science 52(4): 741-762.</mixed-citation>
         </ref>
         <ref id="d1211e470a1310">
            <mixed-citation id="d1211e474" publication-type="other">
Cheng, Leonard &amp; Yum Kwan (2000) What are the determi-
nants of the location of foreign direct investment? The
Chinese experience. Journal of International Economics
51(2): 379-400.</mixed-citation>
         </ref>
         <ref id="d1211e490a1310">
            <mixed-citation id="d1211e494" publication-type="other">
Choi, Seung-Whan (2009) The effect of outliers on regression
analysis: Regime type and foreign direct investment. Quar-
terly Journal of Political Science 4(2): 153-165.</mixed-citation>
         </ref>
         <ref id="d1211e507a1310">
            <mixed-citation id="d1211e511" publication-type="other">
Choi, Seung-Whan (2010) Fighting terrorism through
the rule of law? Journal of Conflict Resolution 54(6):
940-966.</mixed-citation>
         </ref>
         <ref id="d1211e524a1310">
            <mixed-citation id="d1211e528" publication-type="other">
Choi, Seung-Whan &amp; Yiagadeesen Samy (2008) Reexamining
the effect of democratic institutions on inflows of foreign
direct investment in developing countries. Foreign Policy
Analysis 4(1): 83-103.</mixed-citation>
         </ref>
         <ref id="d1211e544a1310">
            <mixed-citation id="d1211e548" publication-type="other">
Control Risks (2010) Terrorist Damage. London (http://www.
controlrisks.com/default.aspx?page=1129), accessed 14
December 2010.</mixed-citation>
         </ref>
         <ref id="d1211e562a1310">
            <mixed-citation id="d1211e566" publication-type="other">
Crain, Nicole &amp; William Mark Crain (2006) Terrorized
economies. Public Choice 128(1/2): 317-349.</mixed-citation>
         </ref>
         <ref id="d1211e576a1310">
            <mixed-citation id="d1211e580" publication-type="other">
Czinkota, Michael; Gary Knight &amp; Peter Liesch (2004)
Terrorism and international business: Conceptual founda-
tions. In: Gabriele Suder (ed.) Terrorism and the Interna-
tional Business Environment: The Security-Business Nexus.
Nottingham: Edward Elgar, 43-57.</mixed-citation>
         </ref>
         <ref id="d1211e599a1310">
            <mixed-citation id="d1211e603" publication-type="other">
Czinkota, Michael; Gary Knight, Peter Liesch &amp; John S teen
(2005) Positioning terrorism in management and market-
ing: Research propositions. Journal of International Man-
agement 11(4): 581-604.</mixed-citation>
         </ref>
         <ref id="d1211e619a1310">
            <mixed-citation id="d1211e623" publication-type="other">
Czinkota, Michael; Gary Knight, Peter Liesch &amp; John Steen
(2010) Terrorism and international business: A research
agenda. Journal of International Business Studies 41(5):
826-843.</mixed-citation>
         </ref>
         <ref id="d1211e639a1310">
            <mixed-citation id="d1211e643" publication-type="other">
Daly, Martin (1987) Read all about it: World hot spots for
profit and trouble. Advertiser, 3 January.</mixed-citation>
         </ref>
         <ref id="d1211e653a1310">
            <mixed-citation id="d1211e657" publication-type="other">
Dees, Stephane (1998) Foreign direct investment in China:
Determinants and effects. Economics of Planning 31(2/3):
175-194.</mixed-citation>
         </ref>
         <ref id="d1211e671a1310">
            <mixed-citation id="d1211e675" publication-type="other">
Dunning, John (1988) Explaining International Production.
London: Unwin Hyman.</mixed-citation>
         </ref>
         <ref id="d1211e685a1310">
            <mixed-citation id="d1211e689" publication-type="other">
Dunning, John (1993) Multinational Enterprises and the Global
Economy. Reading, MA: Addison-Wesley.</mixed-citation>
         </ref>
         <ref id="d1211e699a1310">
            <mixed-citation id="d1211e703" publication-type="other">
Enders, Walter &amp; Todd Sandler (1996) Terrorism and for-
eign direct investment in Spain and Greece. Kyklos
49(3): 331-352.</mixed-citation>
         </ref>
         <ref id="d1211e716a1310">
            <mixed-citation id="d1211e720" publication-type="other">
Enders, Walter &amp; Todd Sandler (1999) Transnational terror-
ism in the post-Cold War era. International Studies Quar-
terly 43(1): 145-167.</mixed-citation>
         </ref>
         <ref id="d1211e733a1310">
            <mixed-citation id="d1211e737" publication-type="other">
Enders, Walter &amp; Todd Sandler (2006) The Political Economy
of Terrorism. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1211e747a1310">
            <mixed-citation id="d1211e751" publication-type="other">
Enders, Walter; Adolfo Sachsida &amp; Todd Sandler (2006) The
impact of transnational terrorism on US foreign direct
investment. Political Research Quarterly 59(4): 517-531.</mixed-citation>
         </ref>
         <ref id="d1211e765a1310">
            <mixed-citation id="d1211e769" publication-type="other">
Frey, Bruno &amp; Simon Luechinger (2005) Measuring terror-
ism. In: Alain Marciano &amp; Jean-Michel Josselin (eds) Law
and the State: A Political Economy Approach. Cheltenham &amp;
Northampton, MA: Edward Elgar, 142-181.</mixed-citation>
         </ref>
         <ref id="d1211e785a1310">
            <mixed-citation id="d1211e789" publication-type="other">
Frey, Bruno; Simon Luechinger &amp; Alois Stutzer (2007) Calcu-
lating tragedy: Assessing the costs of terrorism. Journal of
Economic Surveys 21(1): 1-24.</mixed-citation>
         </ref>
         <ref id="d1211e802a1310">
            <mixed-citation id="d1211e806" publication-type="other">
Gaibulloev, Khusrav &amp; Todd Sandler (2011) The adverse
effect of transnational and domestic terrorism on growth
in Africa. Journal of Peace Research 48(3): 355-371.</mixed-citation>
         </ref>
         <ref id="d1211e819a1310">
            <mixed-citation id="d1211e823" publication-type="other">
Gastanaga, Victor; Jeffrey Nugent &amp; Bistra Pashamova (1998)
Host country reforms and FDI inflows: How much differ-
ence do they make? World Development 26(7): 1299-1314.</mixed-citation>
         </ref>
         <ref id="d1211e836a1310">
            <mixed-citation id="d1211e840" publication-type="other">
Gettler, Leon (2007) Violence seen as bar to investment.
Sydney Morning Herald, 10 September: 28.</mixed-citation>
         </ref>
         <ref id="d1211e850a1310">
            <mixed-citation id="d1211e854" publication-type="other">
Gleditsch, Nils Petter; Peter Wallensteen, Mikael Eriksson,
Margareta Sollenberg &amp; Hâvard Strand (2002) Armed con-
flict 1946-2001: A new dataset. Journal of Peace Research
39(5): 615-637.</mixed-citation>
         </ref>
         <ref id="d1211e871a1310">
            <mixed-citation id="d1211e875" publication-type="other">
Haftel, Yoram (2010) Ratification counts: US investment trea-
ties and FDI flows into developing countries. Review of
International Political Economy 17(2): 348-377.</mixed-citation>
         </ref>
         <ref id="d1211e888a1310">
            <mixed-citation id="d1211e892" publication-type="other">
Heston, Alan; Robert Summers &amp; Bettina Aten (2009) Penn
World Tables, Version 6.3. Center for International Com-
parisons of Production, Income and Prices at the University
of Pennsylvania (http://pwt.econ.upenn.edu/).</mixed-citation>
         </ref>
         <ref id="d1211e908a1310">
            <mixed-citation id="d1211e912" publication-type="other">
Jensen, Nathan (2003) Democratic governance and multina-
tional corporations: Political regimes and inflows of for-
eign direct investment. International Organization 57(3):
801-828.</mixed-citation>
         </ref>
         <ref id="d1211e928a1310">
            <mixed-citation id="d1211e932" publication-type="other">
Jun, Kwang &amp; Harinder Singh (1996) The determinants of
foreign direct investment in developing countries. Transna-
tional Corporations 5(2): 67-105.</mixed-citation>
         </ref>
         <ref id="d1211e945a1310">
            <mixed-citation id="d1211e949" publication-type="other">
LaFree, Gary &amp; Laura Dugan (2007) Introducing the Global
Terrorism Database. Terrorism and Political Violence 19(2):
181-204.</mixed-citation>
         </ref>
         <ref id="d1211e962a1310">
            <mixed-citation id="d1211e966" publication-type="other">
Lee, Jeong-Yeon &amp; Edwin Mansfield (1996) Intellectual prop-
erty protection and US foreign direct investment. Review of
Economics and Statistics 78 (2): 181-186.</mixed-citation>
         </ref>
         <ref id="d1211e980a1310">
            <mixed-citation id="d1211e984" publication-type="other">
Li, Quan (2005) Does democracy promote or reduce transna-
tional terrorist incidents? Journal of Conflict Resolution
49(2): 278-297.</mixed-citation>
         </ref>
         <ref id="d1211e997a1310">
            <mixed-citation id="d1211e1001" publication-type="other">
Li, Quan (2006) Political violence and foreign direct invest-
ment. In: Michele Fratianni (ed.) Research in Global
Security Management, Volume 12: Regional Economic Inte-
gration. Oxford: Elsevier, 231-250.</mixed-citation>
         </ref>
         <ref id="d1211e1017a1310">
            <mixed-citation id="d1211e1021" publication-type="other">
Li, Quan &amp; Adam Resnick (2003) Reversal of fortunes: Dem-
ocratic institutions and foreign direct investment inflows to
developing countries. International Organization 57(1):
175-211.</mixed-citation>
         </ref>
         <ref id="d1211e1037a1310">
            <mixed-citation id="d1211e1041" publication-type="other">
Li, Quan &amp; Drew Schaub (2004) Economic globalization and
transnational terrorism. Journal of Conflict Resolution 48(2):
278-297.</mixed-citation>
         </ref>
         <ref id="d1211e1054a1310">
            <mixed-citation id="d1211e1058" publication-type="other">
Lutz, James &amp; Brenda Lutz (2006) International terrorism in
Latin America: Effects on foreign investment and tourism.
Journal of Social, Political, and Economic Studies 31(3):
321-338.</mixed-citation>
         </ref>
         <ref id="d1211e1074a1310">
            <mixed-citation id="d1211e1078" publication-type="other">
Mancuso, Anthony; Cassandra E Dirienzo &amp; Jayoti Das
(2010) Assessing terrorist risk and FDI using relative infor-
mation measures. Applied Economics Letters 17(8): 787-790.</mixed-citation>
         </ref>
         <ref id="d1211e1092a1310">
            <mixed-citation id="d1211e1096" publication-type="other">
Marshall, Monty &amp; Keith Jaggers (2010) POLITY IV Project:
Political Regime Characteristics and Transitions , 1800-2010.
Fairfax, VA: Center for Systemic Peace, George Mason Uni-
versity (http://www.systemicpeace.org/polity/polity4.htm) .</mixed-citation>
         </ref>
         <ref id="d1211e1112a1310">
            <mixed-citation id="d1211e1116" publication-type="other">
Mickolus, Edward; Todd Sandler, Jean Murdock &amp; Peter
Flemming (2008) International Terrorism: Attributes of
Terrorist Events, 1968-2008. Dunn Loring, VA: Vinyard
Software.</mixed-citation>
         </ref>
         <ref id="d1211e1132a1310">
            <mixed-citation id="d1211e1136" publication-type="other">
Political Risk Services Group (2010) Political Risk Services Meth-
odology. Syracuse, NY (http://www.prsgroup.com/PRS_
Methodology.aspx#turmoil), accessed 4 August 2010.</mixed-citation>
         </ref>
         <ref id="d1211e1149a1310">
            <mixed-citation id="d1211e1153" publication-type="other">
Poole-Robb, Stuart &amp; Alan Bailey (2004) Risky Business:
Corruption, Fraud, Terrorism and Other Threats to Global
Business. London: Kogan Page.</mixed-citation>
         </ref>
         <ref id="d1211e1166a1310">
            <mixed-citation id="d1211e1170" publication-type="other">
Schneider, Friedrich &amp; Bruno Frey (1985) Economic and
political determinants of foreign direct investment. World
Development 13(2): 161-175.</mixed-citation>
         </ref>
         <ref id="d1211e1183a1310">
            <mixed-citation id="d1211e1187" publication-type="other">
Spich, Robert &amp; Robert Grosse (2005) How does homeland
security affect US firms' international competitiveness?
Journal of International Management 11(4): 457-478.</mixed-citation>
         </ref>
         <ref id="d1211e1201a1310">
            <mixed-citation id="d1211e1205" publication-type="other">
UNCTAD (2002) Sources ana Definitions (http://www.unctad.
org /templates/Page.asp?intItemID=3200&amp;lang=1), accessed
20 November 2010.</mixed-citation>
         </ref>
         <ref id="d1211e1218a1310">
            <mixed-citation id="d1211e1222" publication-type="other">
UNCTAD (2010) UNCTAD STAT (http://unctadstat.
unctad.org/ReportFolders/reportFolders.aspx).</mixed-citation>
         </ref>
         <ref id="d1211e1232a1310">
            <mixed-citation id="d1211e1236" publication-type="other">
United States Department of Agriculture (2011) International
Macroeconomic Data Set (http://www.ers.usda.gov/Data/
Macroeconomics/).</mixed-citation>
         </ref>
         <ref id="d1211e1249a1310">
            <mixed-citation id="d1211e1253" publication-type="other">
Wei, Shang-Jin (2000) How taxing is corruption on international
investors? Review of Economics and Statistics 82(1): 1-11.</mixed-citation>
         </ref>
         <ref id="d1211e1263a1310">
            <mixed-citation id="d1211e1267" publication-type="other">
World Bank (2010) The World Development Indicators,
CD-ROM. Washington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d1211e1277a1310">
            <mixed-citation id="d1211e1281" publication-type="other">
Zorn, Christopher (2001) Generalized estimating equation
models for correlated data: A review with applications.
American Journal of Political Science 45(2): 470-490.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
