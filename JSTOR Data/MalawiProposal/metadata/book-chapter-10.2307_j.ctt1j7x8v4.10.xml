<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt234z0z</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1j7x8v4</book-id>
      <subj-group>
         <subject content-type="call-number">BR1446.6.G67 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zambia</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
            <subj-group>
               <subject content-type="lcsh">Religious aspects</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zambia</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
            <subj-group>
               <subject content-type="lcsh">Autonomy and independence movements</subject>
               <subj-group>
                  <subject content-type="lcsh">Religious aspects</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Christianity and culture</subject>
         <subj-group>
            <subject content-type="lcsh">Zambia</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Religion and politics</subject>
         <subj-group>
            <subject content-type="lcsh">Zambia</subject>
            <subj-group>
               <subject content-type="lcsh">History</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zambia</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
            <subj-group>
               <subject content-type="lcsh">To 1964</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zambia</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
            <subj-group>
               <subject content-type="lcsh">1964–1991</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United National Independence Party (Zambia)</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Lumpa Church</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Bemba (African people)</subject>
         <subj-group>
            <subject content-type="lcsh">Religion</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Religion</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Invisible Agents</book-title>
         <subtitle>Spirits in a Central African History</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Gordon</surname>
               <given-names>David M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <contrib-group>
         <role content-type="series-editor">Series editors:</role>
         <contrib contrib-type="series-editor" id="contrib2">
            <name name-style="western">
               <surname>Allman</surname>
               <given-names>Jean</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib3">
            <name name-style="western">
               <surname>Isaacman</surname>
               <given-names>Allen</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>26</day>
         <month>11</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="epub">9780821444399</isbn>
      <isbn content-type="epub">0821444395</isbn>
      <publisher>
         <publisher-name>Ohio University Press</publisher-name>
         <publisher-loc>ATHENS</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>Ohio University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1j7x8v4"/>
      <abstract abstract-type="short">
         <p>
            <italic>Invisible Agents</italic>shows how personal and deeply felt spiritual beliefs can inspire social movements and influence historical change. Conventional historiography concentrates on the secular, materialist, or moral sources of political agency. Instead, David M. Gordon argues, when people perceive spirits as exerting power in the visible world, these beliefs form the basis for individual and collective actions. Focusing on the history of the south-central African country of Zambia during the nineteenth and twentieth centuries, his analysis invites reflection on political and religious realms of action in other parts of the world, and complicates the post-Enlightenment divide of sacred and profane.The book combines theoretical insights with attention to local detail and remarkable historical sweep, from oral narratives communicated across slave-trading routes during the nineteenth century, through the violent conflicts inspired by Christian and nationalist prophets during colonial times, and ending with the spirits of Pentecostal rebirth during the neoliberal order of the late twentieth century. To gain access to the details of historical change and personal spiritual beliefs across this long historical period, Gordon employs all the tools of the African historian. His own interviews and extensive fieldwork experience in Zambia provide texture and understanding to the narrative. He also critically interprets a diverse range of other sources, including oral traditions, fieldnotes of anthropologists, missionary writings and correspondence, unpublished state records, vernacular publications, and Zambian newspapers.<italic>Invisible Agents</italic>will challenge scholars and students alike to think in new ways about the political imagination and the invisible sources of human action and historical change.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.5</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.6</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                     <subtitle>Seeing Invisible Worlds</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Invisible forces mobilize us to action. Sometimes they are remote and absolute, such as “freedom” and “fate”; or they are proximate and changing human creations, such as the “state” and its “laws”; or they combine proximity with the personal, as in the emotions of “love” and “hate.” Invisible forces are sometimes imagined to be spirits that possess bodies, incarnate the dead, and guide the actions of the living. Yet not all agents of the invisible world are compatible. While we accept the influence of our own invisible worlds, those of others appear implausible forces for change in the visible world.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Passion of Chitimukulu</title>
                  </title-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>The history of the Bemba kingdom’s rise prior to the nineteenth century remains vague. In Bemba renditions, the military conquest of the region by the Luba-related Crocodile Clan was entwined with stories of autochthonous magical powers, especially those of women, and the passion of the Crocodile Clan’s leader, Chitimukulu, “the Great Tree.” Objects such as the staff of rule, depicted below in figure 1.1 and on the cover, evoked memories of similar conquests across south-central Africa. Similar figurines could have represented a number of different iconic heroines praised in many of the savanna’s most renowned stories of conquest: Luweji of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Christian Witches</title>
                  </title-group>
                  <fpage>50</fpage>
                  <abstract>
                     <p>In the early 1930s, the Roman Catholic missionary society of the White Fathers applied to open a mission in the Crocodile Clan chief Nkula’s area, around twelve miles from the already established Protestant mission of the Presbyterian Church of Scotland at Lubwa. Since the land was designated “Native Trust,” Nkula had to approve the White Fathers’ application before the colonial administration would agree to the mission. Nkula refused. He did not want the Catholics to open a mission in his chieftaincy, and especially not near his own village. At first glance, Nkula’s denial of permission for the White Fathers to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Satan in the City</title>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>In April 1935, a note signed by “G. Loveway” was posted outside the workers’ compound at the Nkana mine on the Copperbelt of colonial Zambia. It complained of persecution, increased taxation, and stagnant wages. “Listen to this all of you who live in the country,” the note began. “Know how they cause us to suffer, they cheat us for money, they arrest us for loafing, they persecute us and put us in gaol for tax. … He who cannot read should tell his companion that on 29th April not to go to work.” This was no ordinary call for the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>A New Jerusalem</title>
                  </title-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>Mulenga Lubusha Ngandu was born in the Chinsali District in the early 1920s. Her mother and her father, a village policeman who fought for the British during World War I, were members of the royal Crocodile Clan. She married Gipson Nkwale soon after puberty and had a child with him. After the death of Gipson, she was “cleansed” and “inherited” (<italic>ukupyana</italic>) by his cousin Petros Chintankwa, with whom she had five children.¹ As with most mothers, she was named BanaKangwa (the mother of Kangwa) in honor of her first child. In all these respects she lived a life typical of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Dawn</title>
                  </title-group>
                  <fpage>114</fpage>
                  <abstract>
                     <p>A gentle light and warmth that banishes darkness, the dawn is an age-old concept that indicates a liminal moment of a new but familiar beginning. In south-central Africa, the dawn was a long-lasting political idiom that associated everyday welfare with the overcoming of fear and uncertainty. Luchele Ng’anga, the solar hero, came from the east and inspired the Crocodile Clan to offer a Luba civilization that promised to join the sky and the earth. The dawn was a prophetic revolution, the conceptualization of a new day and a new order.</p>
                     <p>In the 1950s, two movements in colonial Zambia’s Northern Province</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Devils of War</title>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>In July 1964, while UNIP controlled the government but the colonial administration was still in charge of security, the interim administration sent in troops to resolve a violent conflict between UNIP cadres and the Lumpa Church. Under orders from the head colonial administrator, Governor Evelyn Hone, and Prime Minister Kenneth Kaunda, the troops insisted that the Lumpa abandon their independent villages. When they refused to submit or surrender their rudimentary weapons, the troops opened fire. Over the next two months, similar military operations took place at eleven other villages and against roaming Lumpa refugees. At the very least, one thousand</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>God in Heaven, Kaunda on Earth</title>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>As the last soldiers of the Northern Rhodesia Regiment withdrew from the Northern Province, Zambians prepared to celebrate their independence. A new stadium to accommodate thirty thousand people had been built on the outskirts of Lusaka. The details of the handing over of power followed a well-rehearsed formula, practiced by the British authorities in their recent withdrawals from other African colonial territories. A British protocol officer planned the events in consultation with a government committee. At midnight, on the dawn of the new nation, during a ceremony attended by the Princess Royal, the Union Jack was lowered and the new</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>A Nation Reborn</title>
                  </title-group>
                  <fpage>178</fpage>
                  <abstract>
                     <p>In 1981, the South African–based faith healer and evangelist Reinhard Bonnke, founder of Christ for All Nations (CfAN), planned a crusade in Zambia. He liaised with existing church networks and sent “prayer warriors” to prepare for mass conversions and healing sessions. For two weeks that July, Zambians flocked to the ten-thousand-person tent in Lusaka to hear Bonnke’s preaching and to experience his healing. “Indeed, miracles have been happening this week,” declared an editorial in the Christian weekly, the<italic>National Mirror</italic>. “Empty wheelchairs and excited faces, which have regained sight after long periods of blindness, were tangible evidence of miracles.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.15</book-part-id>
                  <title-group>
                     <title>CONCLUSION</title>
                     <subtitle>The Spirit Realm of Agency</subtitle>
                  </title-group>
                  <fpage>199</fpage>
                  <abstract>
                     <p>Some of the most significant political movements and moments in Zambian history—Bemba chieftaincy, Bamuchape, Watchtower, Lenshina’s church, popular nationalism, Kaunda’s humanism, and Chiluba’s reborn Christian nation—have been part of an ongoing Zambian debate about the relationships between the individual, the community, the state, and the spirits. By talking about spirits, people made sense of the worlds that enveloped their lives, and, in turn, transformed these worlds. In this regard, the ideas of Zambia’s first and second presidents, Kenneth Kaunda and Frederick Chiluba, shortly after they came to office, represent two conflicting notions of the spirit world dealt with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>203</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.17</book-part-id>
                  <title-group>
                     <title>Glossary of Select Spiritual Terms</title>
                  </title-group>
                  <fpage>259</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.18</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>261</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j7x8v4.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>283</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
