<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">geojournal</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50002206</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>GeoJournal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Kluwer Academic Publishers</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03432521</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15729893</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41147872</article-id>
         <title-group>
            <article-title>The fuelwood crisis in southern Africa - relating fuelwood use to livelihoods in a rural village</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Delali B.K.</given-names>
                  <surname>Dovie</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>E.T.F.</given-names>
                  <surname>Witkowski</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Charlie M.</given-names>
                  <surname>Shackleton</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">60</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40050880</issue-id>
         <fpage>123</fpage>
         <lpage>133</lpage>
         <permissions>
            <copyright-statement>© 2004 Kluwer Academic Publishers</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41147872"/>
         <abstract>
            <p>The aim of the paper is to examine wood as a source of fuel energy in rural South Africa and factors influencing its usage. The analysis is based on household profiles and characteristics (e.g., gender, caste, population and income) in a livelihood framework. Fuelwood consumption was estimated to be 692 kg/capita, and 4343 kg/user household per annum, valued at $311 per household. Consumption was modelled in relation to informal and formal cash incomes, and population of children, female and male adults. However, only the population of female adults could significantly influence consumption of fuelwood. This implied that where there were more women in a household, consumption was likely to be high. This might be due to the majority of women doing the cooking and heating in the household. Any change in the value of cash income of households had no significant impacts on fuelwood consumed. Cash incomes might therefore not be strong determinants of the types of energy used by rural households. The average quantity of wood consumed for fuel energy in summer was not significantly different from consumption in winter. Some households perpetually used more wood than others. The study further showed that harvesting of wood for fuel energy is not opportunistic, but requires reallocation of time for other livelihood activities in times of shortage. The fuelwood crisis is not simple and not only about shortage of fuelwood and/or population growth but linked to household profiles and other livelihood strategies and subsequently vulnerability of households. These would require thorough investigation and understanding in relation to precise demand and supply data for fuelwood before the fuelwood problem can be sufficiently managed.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1492e220a1310">
            <label>1</label>
            <mixed-citation id="d1492e227" publication-type="other">
(Shackleton, 1996)</mixed-citation>
         </ref>
         <ref id="d1492e234a1310">
            <label>2</label>
            <mixed-citation id="d1492e241" publication-type="other">
(Lipton, et al.,
1996; Ellis, 1998).</mixed-citation>
            <mixed-citation id="d1492e250" publication-type="other">
Robinson, 1993;</mixed-citation>
            <mixed-citation id="d1492e256" publication-type="other">
Forsyth et ai, 1998</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1492e272a1310">
            <mixed-citation id="d1492e276" publication-type="other">
An L., Lupi F., Liu J.G., Linderman M.A. and Huang J.Y., 2002: Modelling
the choice to switch from fuelwood to electricity Implications for giant
panda habitat conservation. Ecological Economics 42(3): 445-457.</mixed-citation>
         </ref>
         <ref id="d1492e289a1310">
            <mixed-citation id="d1492e293" publication-type="other">
Anderson D. and Fiswick R., 1984: Fuelwood consumption and deforest-
ation in African countries. World Bank Staff Working paper No. 704.
World Bank, Washington DC.</mixed-citation>
         </ref>
         <ref id="d1492e306a1310">
            <mixed-citation id="d1492e310" publication-type="other">
Bembridge T.J. and Tarlton J.E., 1990: Woodfuel in Ciskei: a headload
study. South African Forestry Journal 154: 88-93.</mixed-citation>
         </ref>
         <ref id="d1492e320a1310">
            <mixed-citation id="d1492e324" publication-type="other">
Bembridge T.J., 1990: Woodlots, woodfuel and energy strategies for Ciskei.
South African Forestry Journal 155: 42-50.</mixed-citation>
         </ref>
         <ref id="d1492e335a1310">
            <mixed-citation id="d1492e339" publication-type="other">
Bernard H.R., 1994: Research methods in Anthropology: Qualitative and
quantitative Approaches, 2nd Edition. SABA Publishers, California.</mixed-citation>
         </ref>
         <ref id="d1492e349a1310">
            <mixed-citation id="d1492e353" publication-type="other">
Black R. and Sessay M.F., 1997: Forced migration, environmental change
and woodfuel issues in the Senegal River Valley. Environmental Conser-
vation 24(3): 251-260.</mixed-citation>
         </ref>
         <ref id="d1492e366a1310">
            <mixed-citation id="d1492e370" publication-type="other">
Brouwer I.D., Hoorweg J.C. and VanLiere M.J., 1997: When households
run out of fuel: Responses of rural households to decreasing fuelwood
availability, Ntcheu District, Malawi. World Development 25(2): 255-
266.</mixed-citation>
         </ref>
         <ref id="d1492e386a1310">
            <mixed-citation id="d1492e390" publication-type="other">
Campbell B.M., Costanza R. and van den belt M., 2000: Land use options in
dry tropical woodland ecosystems in Zimbabwe: Introduction, overview
and synthesis. Ecological Economics 33: 341-351.</mixed-citation>
         </ref>
         <ref id="d1492e403a1310">
            <mixed-citation id="d1492e407" publication-type="other">
Campbell B.M., Luckert M. and Scoones, I., 1997: Local level valuation
of savanna resources: A case study from Zimbabwe. Economic Botany
51(1): 59-77.</mixed-citation>
         </ref>
         <ref id="d1492e420a1310">
            <mixed-citation id="d1492e424" publication-type="other">
Clarke J., Cavendish W. and Coyote C, 1996. Rural households and mi-
ombo woodlands: use, value and management. In Campbell, B.M.
(ed), The miombo in transition: woodlands and welfare in Africa,
pp. 101-136. CIFOR, Bogor.</mixed-citation>
         </ref>
         <ref id="d1492e441a1310">
            <mixed-citation id="d1492e445" publication-type="other">
Cline-Cole R.A., Falola J.A., Main H.A.C., Mortimore M.J., Nichol J.E.
and O'Reilly F.D., 1990: Woodfuel in Kano. United Nations University
Press, Japan.</mixed-citation>
         </ref>
         <ref id="d1492e458a1310">
            <mixed-citation id="d1492e462" publication-type="other">
Cuthbert A.L. and Dufournaud CM., 1998: An econometric analysis
of fuelwood consumption in Sub-Saharan Africa. Environment and
Planning A30(4): 721-729.</mixed-citation>
         </ref>
         <ref id="d1492e475a1310">
            <mixed-citation id="d1492e479" publication-type="other">
Department of Water Affairs and Forestry (DWAF), 1997: National Forestry
Action Plan. DWAF, Pretoria.</mixed-citation>
         </ref>
         <ref id="d1492e489a1310">
            <mixed-citation id="d1492e493" publication-type="other">
Dewees P.A., 1989: The woodfuel crisis reconsidered: observations on the
dynamics of abundance and scarcity. World development 17: 1 159-1 162.</mixed-citation>
         </ref>
         <ref id="d1492e503a1310">
            <mixed-citation id="d1492e507" publication-type="other">
Dovie B.D.K., Shackleton CM. and Witkowski E.T.F., 2002: Direct-use
value of woodland resources consumed and traded in a South African
village. International Journal of Sustainable Development and World
Ecology 9(3): 269-283.</mixed-citation>
         </ref>
         <ref id="d1492e523a1310">
            <mixed-citation id="d1492e527" publication-type="other">
Dovie, B.D.K., 2003: Whose involvement? -Can Hierarchical Valuation
Scheme intercede for participatory methods for evaluating secondary
forest resources use? Forest Policy and Economics 5(3): 265-283.</mixed-citation>
         </ref>
         <ref id="d1492e541a1310">
            <mixed-citation id="d1492e545" publication-type="other">
Dovie B.D.K., Witkowski E.T.F. and Shackleton CM., in press: Mon-
etary valuation of livelihoods for understanding the composition and
complexity of rural households. Agriculture and Human Values (in
press).</mixed-citation>
         </ref>
         <ref id="d1492e561a1310">
            <mixed-citation id="d1492e565" publication-type="other">
Durning A., 1991: Saving the forests: what will it take? Worldwatch Paper
117. Worldwatch Institute, Washington DC.</mixed-citation>
         </ref>
         <ref id="d1492e575a1310">
            <mixed-citation id="d1492e579" publication-type="other">
Eberhard A.A., 1990: Energy consumption patterns and supply problems in
underdeveloped areas of South Africa. Development Southern Africa 7:
335-346.</mixed-citation>
         </ref>
         <ref id="d1492e592a1310">
            <mixed-citation id="d1492e596" publication-type="other">
Ellis F., 1998: Household Strategies and Rural Livelihood Diversification.
Journal of Development Studies 35(1): 1-38.</mixed-citation>
         </ref>
         <ref id="d1492e606a1310">
            <mixed-citation id="d1492e610" publication-type="other">
Forsyth T., Leach M. and Scoones I., 1998: Poverty and Environment:
Priorities for Research and Policy. Brighton, UK, IDS: UNDP Report.</mixed-citation>
         </ref>
         <ref id="d1492e620a1310">
            <mixed-citation id="d1492e624" publication-type="other">
Gandar M.V., 1991: The imbalance of power. In Cock J. and Koch E.
(eds), Going green: people, politics and the environment in South Africa.
Oxford University Press, Cape Town.</mixed-citation>
         </ref>
         <ref id="d1492e638a1310">
            <mixed-citation id="d1492e642" publication-type="other">
Geist H.J. and Lambin E.F., 2001: What drives deforestation? A meta-
analysis and underlying causes of deforestation based on subnational
case study evidence. Ciaco Printshop, Louvaine-la Neuve.</mixed-citation>
         </ref>
         <ref id="d1492e655a1310">
            <mixed-citation id="d1492e659" publication-type="other">
Griffin N.J., Banks B., Mavrandonis J., Shackleton CM. and Shackleton
S.E., 1992: Household energy and wood use in a peripheral rural area
of the eastern Transvaal lowveld. Department of Mineral and Energy
Affairs, Pretoria.</mixed-citation>
         </ref>
         <ref id="d1492e675a1310">
            <mixed-citation id="d1492e679" publication-type="other">
Gwebu, T.D., 2002: Energy sector policies in Botswana and their implica-
tions for global climatic change. GeoJournal 56: 83-92.</mixed-citation>
         </ref>
         <ref id="d1492e689a1310">
            <mixed-citation id="d1492e693" publication-type="other">
Hall D.O., 1994: Introduction, summary and conclusions. In Hall D.O. and
Mao Y.S. (eds), Biomass energy and coal in Africa, pp. 1-16. Zed Books
and AFREPEN.</mixed-citation>
         </ref>
         <ref id="d1492e706a1310">
            <mixed-citation id="d1492e710" publication-type="other">
IIED, 2000: Rural livelihoods and carbon management. International
Institute for Environment and Development, London.</mixed-citation>
         </ref>
         <ref id="d1492e720a1310">
            <mixed-citation id="d1492e724" publication-type="other">
Leach G. and Mearns R., 1988: Beyond the fuelwood crisis: people land
and tress in Africa. Earthscan, London.</mixed-citation>
         </ref>
         <ref id="d1492e735a1310">
            <mixed-citation id="d1492e739" publication-type="other">
Letsela T., Witkowski E.T.F. and Balkwill K., 2002: Direct use values of
communal resources in Bokong and Tsehlanyane in Lesotho: whither
the commons? International Journal of Sustainable Development and
World Ecology 9: 351-368.</mixed-citation>
         </ref>
         <ref id="d1492e755a1310">
            <mixed-citation id="d1492e759" publication-type="other">
Lipton M., Ellis F. and Lipton M. (eds), 1996: Land Labor and Livelihoods
in Rural South Africa, Vol. 2: KwaZulu-Natal and Northern Province.
Durban, South Africa: Indicator Press.</mixed-citation>
         </ref>
         <ref id="d1492e772a1310">
            <mixed-citation id="d1492e776" publication-type="other">
Luoga J.E., Witkowski E.T.F. and Balkwill K., 2000: Subsistence use of
wood products and shifting cultivation within a miombo woodland of
eastern Tanzania, with notes on commercial uses. South African Journal
of Botany 66(1): 72-85.</mixed-citation>
         </ref>
         <ref id="d1492e792a1310">
            <mixed-citation id="d1492e796" publication-type="other">
Luoga J.E., Witkowski E.T.F. and Balkwill K., 2002: Economics of char-
coal production in miombo woodlands of eastern Tanzania: some hidden
costs associated with commercialization of the resources. Ecological
Economics 35: 243-257.</mixed-citation>
         </ref>
         <ref id="d1492e812a1310">
            <mixed-citation id="d1492e816" publication-type="other">
Mahiri I. and Howorth C, 2001: Twenty years of resolving the irresolvable:
Approaches to the fuelwood problem in Kenya. Land Degradation &amp;
Development 12(3): 205-215.</mixed-citation>
         </ref>
         <ref id="d1492e829a1310">
            <mixed-citation id="d1492e833" publication-type="other">
MEF 1996: Annual report. Ministry of Environment and Forests, Govern-
ment of India, New Delhi.</mixed-citation>
         </ref>
         <ref id="d1492e844a1310">
            <mixed-citation id="d1492e848" publication-type="other">
Mercer D.E. and Soussan J., 1992: Fuelwood problems and solutions.
In Sharma M.P. (ed), Looking for balance between conservation and
development, pp. 177-213. World Bank, Washington DC</mixed-citation>
         </ref>
         <ref id="d1492e861a1310">
            <mixed-citation id="d1492e865" publication-type="other">
Metz J.J., 1990: Conservation practices at upper elevation village of west
Nepal. Mountain Research and Development 10(4): 7-15.</mixed-citation>
         </ref>
         <ref id="d1492e875a1310">
            <mixed-citation id="d1492e879" publication-type="other">
Parikesit T.K., Tsunekawa A. and Abdoellah O.S., 2001: Non-forest fuel-
wood acquisition and transition in type of energy for domestic uses in
the changing agricultural landscape of the Upper Citarum Watershed,
Indonesia. Agriculture Ecosystems &amp; Environment 84(3): 245-258.</mixed-citation>
         </ref>
         <ref id="d1492e895a1310">
            <mixed-citation id="d1492e899" publication-type="other">
Robinson N.A. (ed), 1993: Agenda 21: Earth's Action Plan Annotated. New
York: Oceana.</mixed-citation>
         </ref>
         <ref id="d1492e909a1310">
            <mixed-citation id="d1492e913" publication-type="other">
Salick J., Bium A., Martin G., Apin L. and Beaman R., 1999: Whence
useful plants? A direct relationship between biodiversity and useful
plants among the Dusun of Mt. Kinabalu. Biodiversity &amp; Conservation
8: 797-818.</mixed-citation>
         </ref>
         <ref id="d1492e929a1310">
            <mixed-citation id="d1492e933" publication-type="other">
Samant S.S., Dhar U. and Rawal R.S., 2000: Assessment of fuel resource
diversity and utilization patterns in Askot Wildlife Sanctuary in Kumaun
Himalaya, India, for conservation and management. Environmental
Conservation 27(1): 5-13.</mixed-citation>
         </ref>
         <ref id="d1492e950a1310">
            <mixed-citation id="d1492e954" publication-type="other">
Shackleton CM. and Shackleton S.E., 2000: Direct use values of second-
ary resources harvested from communal savannas in the Bushbuckridge
Lowveld, South Africa. Journal of Tropical Forest Products 6: 28-47.</mixed-citation>
         </ref>
         <ref id="d1492e967a1310">
            <mixed-citation id="d1492e971" publication-type="other">
Shackleton CM., 1993: Fuelwood harvesting and sustainable utilization in
a communal grazing land and protected area of the eastern Transvaal
lowveld. Biological Conservation 63(3): 247-254.</mixed-citation>
         </ref>
         <ref id="d1492e984a1310">
            <mixed-citation id="d1492e988" publication-type="other">
Shackleton CM., 1998: Annual production of harvestable deadwood in
semi-arid savannas, South Africa. Forest Ecology and Management 112:
139-144.</mixed-citation>
         </ref>
         <ref id="d1492e1001a1310">
            <mixed-citation id="d1492e1005" publication-type="other">
Shackleton, CM., 1996: Potential Stimulation of Local Rural Economies
by Harvesting Secondary Products: A Case Study from the Transvaal
Lowveld, South Africa. Ambio 25(1): 33-38.</mixed-citation>
         </ref>
         <ref id="d1492e1018a1310">
            <mixed-citation id="d1492e1022" publication-type="other">
Soussan J., 1988: Primary resources and energy in the Third World.
Routledge, London.</mixed-citation>
         </ref>
         <ref id="d1492e1032a1310">
            <mixed-citation id="d1492e1036" publication-type="other">
Tewari J.C, Tripathi, D., Pratap, N. and Singh, S.P., 2003: A study of
the structure, energy fluxes and emerging trends in traditional Central
Himalayan agroforestry systems. Forests, Trees &amp; Livelihoods 13(10):
17-38.</mixed-citation>
         </ref>
         <ref id="d1492e1053a1310">
            <mixed-citation id="d1492e1057" publication-type="other">
Tietema T., 1993: Biomass determination of fuelwood trees and bushes of
Botswana, southern Africa. Forest Ecology and Management 60(3-4):
257-269.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
