<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.3998/mpub.4772634</book-id>
      <subj-group>
         <subject content-type="call-number">JC423.B2637 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Democracy</subject>
         <subj-group>
            <subject content-type="lcsh">Case studies</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Dictatorship</subject>
         <subj-group>
            <subject content-type="lcsh">Case studies</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Democracy, Dictatorship, and Term Limits</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Baturo</surname>
               <given-names>Alexander</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib2">
            <role>Series Editor</role>
            <name name-style="western">
               <surname>Laver</surname>
               <given-names>Michael</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>10</day>
         <month>02</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780472119318</isbn>
      <isbn content-type="epub">9780472120239</isbn>
      <publisher>
         <publisher-name>University of Michigan Press</publisher-name>
         <publisher-loc>Ann Arbor</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>University of Michigan</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3998/mpub.4772634"/>
      <abstract abstract-type="short">
         <p>A national constitution or other statute typically specifies restrictions on executive power, often including a limit to the number of terms the chief executive may hold office. In recent decades, however, some presidents of newly established democracies have extended their tenure by various semilegal means, thereby raising the specter-and in some cases creating the reality-of dictatorship.</p>
         <p>Alexander Baturo tracks adherence to and defiance of presidential term limits in all types of regimes (not only democratic regimes) around the world since 1960. Drawing on original data collection and fieldwork to investigate the factors that encourage playing by or manipulating the rules, he asks what is at stake for the chief executive if he relinquishes office. Baturo finds that the income-generating capacity of political office in states where rent-seeking is prevalent, as well as concerns over future immunity and status, determines whether or not an executive attempts to retain power beyond the mandated period.<italic>Democracy, Dictatorship, and Term Limits</italic>will appeal to scholars of democratization and executive power and also to political theorists.</p>
      </abstract>
      <counts>
         <page-count count="360"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.3</book-part-id>
                  <title-group>
                     <title>List of Tables</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.4</book-part-id>
                  <title-group>
                     <title>List of Figures</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction:</title>
                     <subtitle>Presidential Term Limits and the New Caesarism</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In the past fifty years, between 1960 and 2010, from Latin America to post-Soviet Eurasia, out of two hundred term-bounded presidents that have served their terms, more than a quarter have managed to extend their stay in office beyond constitutionally mandated periods in one way or another. Many ambitious presidents throughout the world strive to increase their power and control the executive, the legislative and judiciary branches of their nations, and to preside over enforcement agencies that make it possible to extend these leaders’ remit into all corners of public and private life. Yet, if these leaders have to surrender</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Term Limits in Historical Perspective</title>
                  </title-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>Presidential term limit is a restriction on the maximum length of tenure that a president can serve in office. It stipulates the length of term — four, five, six or seven years (historically there were both shorter and longer terms), and the number of consecutive or nonconsecutive terms — usually a single, two, or less often three terms — that the chief executive is permitted to serve. If there exists a restriction on the number of terms, it can be further qualified whether it applies to a consecutive re-election only or whether the latter is allowed only after some interim</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Continuismo in Comparison:</title>
                     <subtitle>From a Few More Years to a Presidency for Life</subtitle>
                  </title-group>
                  <fpage>47</fpage>
                  <abstract>
                     <p>The previous chapter demonstrated that the majority of democratic regimes, as well as many non- and partly democratic regimes, restrict the tenures of their presidents, especially after the end of the Cold War. Many leaders throughout history, however, were able to circumvent constitutional provisions on re-election. When the scheduled time for departures looms and the most powerful politicians confront the necessity to honor their constitutions, third term debates come to dominate national political agendas. In some countries such extensions are so salient and controversial that the attempts to remain in office or the supporters thereof even receive their own labels:</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Costs and Benefits of Leaving Presidential Office</title>
                  </title-group>
                  <fpage>91</fpage>
                  <abstract>
                     <p>What explains the success of modern Caesars — incumbent presidents? In the previous chapter I explored major obstacles for executives contemplating tenure extensions. The strength of a president vis-à-vis his or her own party, legislature and judiciary, the extent of mass collective action, as well as the role of international players, are the key factors that can prevent self-perpetuation in power. In this chapter however I turn not to what restraints presidents — the strength of democracy, institutional checks and balances and other factors — but to what drives them, motivates them to take over their regimes and extend their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>The Stakes of Losing Office in Comparison</title>
                  </title-group>
                  <fpage>121</fpage>
                  <abstract>
                     <p>When a president in a mature democracy with a developed economy complies with a constitution and steps down when required, we can put forward two related explanations for such behavior. If we assume that all politicians are pure office-seekers and only institutional constraints hinder their political ambitions, we will attribute constitutional compliance to the existing checks and balances alone. However, if we allow for the possibility that motivations of political actors are more complex, the behavior of presidents can also be explained if we turn to how these leaders perceive the value of their office and career prospects after departures.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Endogenous Term Limits:</title>
                     <subtitle>Statistical Analyses</subtitle>
                  </title-group>
                  <fpage>146</fpage>
                  <abstract>
                     <p>In this chapter I bring together the previous discussion of the major obstacles that the presidents have to overcome when they desire political immortality (chapter 3) and of the value of political office (chapter 4). The previous chapter explored how the costs and benefits of holding office influenced presidents’ behavior in three cases. Here I turn to empirical estimations and attempt to map the costs and benefits of holding office to the observable outcomes of presidents extending their tenure or stepping down. I take several approaches to statistical analyses in this chapter. Following detailed discussion of the operationalization of dependent</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Personal Background of Presidents and Constitutional Compliance</title>
                  </title-group>
                  <fpage>187</fpage>
                  <abstract>
                     <p>Presidents are more likely to attempt to remain in office beyond their constitutional mandate if they face insignificant political constraints, as discussed earlier in chapter 3. They are also more likely to extend their terms if personal costs of departing from office are too high, as discussed in chapter 4. These explanations are not mutually exclusive, and they were tested extensively in the previous chapter. Either way, the strength of institutional constraints and the value of holding office are the main factors behind constitutional compliance.</p>
                     <p>Ultimately, however, sitting presidents themselves decide whether to honor constitutions and step down, or not.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Effects of Presidential Term Limits</title>
                  </title-group>
                  <fpage>212</fpage>
                  <abstract>
                     <p>The main inquiry of this book is how the value of holding presidential office contributes to authoritarian reversals. If the presidents are not restricted from running for consecutive re-election, they, as demonstrated in chapter 3, predominantly win. Does the lack of term limits make political leaders into contemporary kings re-elected forever, however, until they die in office or until they step down in old age at a time of their own choosing? In the first section of this chapter I examine whether presidents who desired political immortality and overturned term limits achieved their dream, in other words, their survival in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Conclusion:</title>
                     <subtitle>Presidentialism, Democracy, and Term Limits</subtitle>
                  </title-group>
                  <fpage>248</fpage>
                  <abstract>
                     <p>In the preceding chapters, I have developed a leader-centric explanation for the process of democratic breakdown and personalization in presidential regimes that occurs when political leaders decide to overturn term limits. From the 1990s onward, the main focus in comparative democratization literature has been on democratic consolidation (Diamond 1999; Huntington 1991, 208–316; Linz and Stepan 1996; Svolik 2009). Due to a proliferation of clearly undemocratic regimes that conduct elections and display other attributes of democratic politics, a number of scholars, however, turned to the analyses of electoral authoritarian, partly democratic regimes, as a separate subtype (e.g., Brownlee 2007<italic>a</italic>; Levitsky</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.15</book-part-id>
                  <title-group>
                     <title>Appendix:</title>
                     <subtitle>Continuismo and Constitutions</subtitle>
                  </title-group>
                  <fpage>261</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>267</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.17</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>293</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.4772634.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>321</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
