<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt16gzh3p</book-id>
      <subj-group>
         <subject content-type="call-number">HC800.M625 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Social change</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
            <subj-group>
               <subject content-type="lcsh">1960-</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Social conditions</subject>
            <subj-group>
               <subject content-type="lcsh">1960-</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Economic policy</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Modernization as Spectacle in Africa</book-title>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">EDITED BY</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Bloom</surname>
               <given-names>Peter J.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Miescher</surname>
               <given-names>Stephan F.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Manuh</surname>
               <given-names>Takyiwaa</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>09</day>
         <month>05</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780253012296</isbn>
      <isbn content-type="epub">9780253012333</isbn>
      <isbn content-type="epub">0253012333</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington; Indianapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Indiana University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt16gzh3p"/>
      <abstract abstract-type="short">
         <p>For postcolonial Africa, modernization was seen as a necessary outcome of the struggle for independence and as crucial to the success of its newly established states. Since then, the rhetoric of modernization has pervaded policy, culture, and development, lending a kind of political theatricality to nationalist framings of modernization and Africans' perceptions of their place in the global economy. These 15 essays address governance, production, and social life; the role of media; and the discourse surrounding large-scale development projects, revealing modernization's deep effects on the expressive culture of Africa.</p>
      </abstract>
      <counts>
         <page-count count="378"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Miescher</surname>
                           <given-names>Stephan F.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bloom</surname>
                           <given-names>Peter J.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Manuh</surname>
                           <given-names>Takyiwaa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In the early years of independence, the discourse of modernization played a central role in imagining a postcolonial African future. Independence as event and spectacle, however, has often overshadowed its emerging context within the paradigm of modernization. It was couched within a preexisting rhetoric of African development proclaiming a new urgency of nation building already set in motion during the 1950s and 1960s. Foregrounding the age of modernization, in contrast to the moment of independence, allows us to propose a more subtle and nuanced understanding of the immediate postwar and early independence period. As less indebted to the quality of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>After Modernization:</title>
                     <subtitle>Globalization and the African Dilemma</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hintzen</surname>
                           <given-names>Percy C.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>I recall a meeting that I attended at a time when media reports were circulating raising concerns about South Asia as a cheap location for computer programming and software development. The value of the U.S. dollar was falling on international currency markets, which was having a negative effect on industry profits in South Asia. Increasingly the region was being rendered less competitive relative to the United States. In response, high-tech companies began shifting their operations back to the United States, generating increasing demand for programmers and computer engineers. With increasing U.S. demand came rising salaries and compensation packages driving costs</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Modernization Theory and the Figure of Blindness:</title>
                     <subtitle>Filial Reflections</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Apter</surname>
                           <given-names>Andrew</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>41</fpage>
                  <abstract>
                     <p>How does one read a text, or an oeuvre? How does one reread modernization theory? In my own case the answers to these questions are linked by Freud’s family romance and “the anxiety of influence” (Bloom 1973), which together guarantee a radical misreading of an intellectual father-figure who was also my father. David E. Apter (1924–2010), a modernization theorist of the 1960s, worked in the Gold Coast and Uganda in the 1950s before turning to issues of comparative development. His Africanist case studies of institutional transfer (1955) and of bureaucratic nationalism (1961) represent two of the four developmental trajectories</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Film as Instrument of Modernization and Social Change in Africa:</title>
                     <subtitle>The Long View</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Smyth</surname>
                           <given-names>Rosaleen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>In this chapter I will ground the theme of modernization in sub-Saharan Africa in its authentic historical context by demonstrating its colonial roots. The central focus will be the efforts made to use film as an instrument of modernization and development communication. In doing so I will turn the current academic orthodoxy on its head. Development communication did not have “its origins in post-war international aid programs,” which were in turn “derived from theories of development and social change that identified the main problems of the post-war world in terms of a lack of development or progress equivalent to Western</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Mass Education, Cooperation, and the “African Mind”</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Windel</surname>
                           <given-names>Aaron</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>The history of credit and farming cooperatives in Africa bridges its colonial and postcolonial past. In the 1960s, cooperatives were linchpins in government programs of rural modernization in post-independence Central and East Africa. Tanzania’s<italic>ujamaa</italic>plan for socialist development, spearheaded by President Julius Nyerere, pinned rural development to the creation of state farm cooperatives. In Zambia, there was a similar embrace of cooperative farming under Kenneth Kaunda’s direction (Hyden 1980; Quick 1978). In both cases, the state provided land and even subsidies to farmers for clearing it, and Zambians and Tanzanians were encouraged to view cooperatives as places for the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Is Propaganda Modernity?</title>
                     <subtitle>Press and Radio for “Africans” in Zambia, Zimbabwe, and Malawi during World War II and Its Aftermath</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chikowero</surname>
                           <given-names>Mhoze</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>Anthropologist Debra Spitulnik (1999, 63) observed that the introduction of electronic media in Zambia went hand in hand with the introduction of cultural practices, orientations, and evaluations related to the ideas of progress, sophistication, consumption, innovation, and Westernization. She bundled this nexus under the cover term “modernity.” Spitulnik argues that this cover term does both too much and too little. While she problematizes the concept, she proceeds to utilize African radio listener feedback to Lusaka Radio collected by anthropologist Hortense Powdermaker (1962) and broadcaster Harry Franklin (1950) to reinforce the notion that radio was an instrument and signifier of modernity.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Elocution, Englishness, and Empire:</title>
                     <subtitle>Film and Radio in Late Colonial Ghana</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bloom</surname>
                           <given-names>Peter J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>136</fpage>
                  <abstract>
                     <p>In the 1936 British Colonial Office’s report on broadcasting services in the colonies, a series of recommendations were made to accelerate radio broadcasting throughout the empire in association with the British Broadcasting Corporation (BBC). More specifically, the report explains that the BBC Empire Service should not only be a form of entertainment for Europeans and others with educational means “but also an instrument of advanced administration . . . for the enlightenment and education of the more backward sections of the population and for their instruction.”¹ The emphasis on advanced administration serves as a point of departure for exploring the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Negotiating Modernization:</title>
                     <subtitle>The Kariba Dam Project in the Central African Federation, ca. 1954–1960</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Tischler</surname>
                           <given-names>Julia</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>On the border between Zambia and Zimbabwe, about 240 miles downstream from the Victoria Falls, you can see a “gracefully curved mass of concrete,” “a colossus” which has tamed the “moods of violence” of the Zambezi River (South African News Agencies 1959, 5).¹ The Kariba Dam was built in the second half of the 1950s to meet the growing energy needs of the Federation of Rhodesia and Nyasaland, creating what was, at the time, the biggest artificial lake in the world. Newspapers around the world did not tire of telling “the romantic and adventurous story of how the dark jungle</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>“No One Should Be Worse Off”:</title>
                     <subtitle>The Akosombo Dam, Modernization, and the Experience of Resettlement in Ghana</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Miescher</surname>
                           <given-names>Stephan F.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>184</fpage>
                  <abstract>
                     <p>Ghanaians express pride about Akosombo, the large hydroelectric dam commissioned in 1966. They consider the Akosombo Dam not only the most impressive testimony to the country’s development but also a powerful reminder of how the country’s first leader, Kwame Nkrumah, envisioned to reach the elusive goal of modernity.¹ People and institutions have produced competing interpretations of the resettlement caused by building Akosombo. They are reflected in what I call “Akosombo stories.” Some of these accounts are emphasized in official records, others have entered the genre of social science literature on Africa’s failed or uneven development, and yet another grouping is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Radioactive Excess:</title>
                     <subtitle>Modernization as Spectacle and Betrayal in Postcolonial Gabon</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hecht</surname>
                           <given-names>Gabrielle</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>205</fpage>
                  <abstract>
                     <p>In 1982, El Hadj Omar Bongo, president of Gabon, officiated a two-day ceremony to celebrate the opening of his nation’s first yellowcake plant.¹ The new factory would transform raw uranium ore extracted by French-run mines into yellowcake, a commodity that could be bought and sold on the world market. The plant itself was not particularly spectacular to behold, and was certainly not a paradigmatic example of the technological sublime (Nye 1996; Larkin 2008). In order to become a symbol of modernization, the plant needed a mise en scène: in this case, a scripted spectacle that explicitly heralded Gabon’s ascension in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Modeling Modernity:</title>
                     <subtitle>The Brief Story of Kwame Nkrumah, a Nazi Pilot Named Hanna, and the Wonders of Motorless Flight</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Allman</surname>
                           <given-names>Jean</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>229</fpage>
                  <abstract>
                     <p>On 18 may 1963, a ceremony of much international pomp and spectacle took place near the small village of Afienya, about fifteen kilometers from accra, the capital of Ghana.¹ The ceremony marked the opening of the newly independent nation’s first gliding school. In attendance were members of the international press corps, German and Ghanaian dignitaries, ambassadors from most of the foreign missions in Ghana, chiefs and their advisors, and representatives from the Young pioneers, the ruling Convention People’s Party’s youth group. The German ambassador, on behalf of the West German government, presented a glider, christened Akroma [the hawk], to President</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The African Personality Dances Highlife:</title>
                     <subtitle>Popular Music, Urban Youth, and Cultural Modernization in Nkrumah’s Ghana, 1957–1965</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Plageman</surname>
                           <given-names>Nate</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>244</fpage>
                  <abstract>
                     <p>On any given weekend evening in the late 1950s and early 1960s, thousands of Ghanaian men and women left their homes, set out for a nearby nightclub, and enjoyed an evening of popular musical recreation. Shortly after dusk, they put on a fashionable set of clothes, met up with their dancing partner or a group of friends, purchased admission tickets and rounds of refreshments, and found a table where they could relax and converse. Around eight o’clock, a set of bandsmen took the stage and began to play long and varied musical sets designed to pull the gathered crowd out</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Building Institutions for the New Africa:</title>
                     <subtitle>The Institute of African Studies at the University of Ghana</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Manuh</surname>
                           <given-names>Takyiwaa</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>268</fpage>
                  <abstract>
                     <p>In the volume<italic>African Intellectuals,</italic>Thandika Mkandawire (2005) and his co-authors contend that Pan-Africanism has been an enduring framework that has shaped several generations of African intellectuals and nationalists in their imaginings of the nation. This embrace of Pan-Africanism formed an intrinsic component of the struggle against foreign domination and underdevelopment on the one hand, and the desire, on the other, to put Africa on par with other modern nations, expressed by the nationalists’ dual track of nation-building and economic development. Crucially, African nationalists and intellectuals linked Africa’s domination to its technoeconomic backwardness (see Rodney 1972). The demand for independence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.17</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Theater and the Politics of Display:</title>
                     <subtitle>The Tragedy of King Christophe at Senegal’s First World Festival of Negro Arts</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McMahon</surname>
                           <given-names>Christina S.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>287</fpage>
                  <abstract>
                     <p>In Aimé Césaire’s 1963 play, Henri Christophe dreams of constructing a grandiose Citadel that would broadcast to the world Haiti’s glory in becoming the first free Black republic after a hard-fought revolution against France in the early nineteenth century. Casting the Citadel as a symbol of the labor of nation-building, Christophe likens himself to an engineer, the “builder” of the Haitian people (Césaire 1969, 44). As John Conteh-Morgan notes, this is a strange metaphor for Césaire to employ, given that he is one of the architects of Négritude philosophy. Césaire’s earliest writing on the subject, his epic poem<italic>Notebook of</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.18</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>Reengaging Narratives of Modernization in Contemporary African Literature</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wilson-Tagoe</surname>
                           <given-names>Nana</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>307</fpage>
                  <abstract>
                     <p>The challenge in revisiting modernization in Africa is to pluralize its contexts and conditions in order to understand its formations in non-Western and ex-colonial regions of the world. We ought to be able to engage with modernization in ways that can unravel the agency of modern African societies in shaping a place beyond the paradigms of center and margin. How, for instance, can we characterize modernizing processes in societies that were incorporated in global systems of modernity? To what extent have Western institutions reshaped these dynamics in unique ways? Are these societies modern? This chapter focuses on narratives of modernization</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.19</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>Between Nationalism and Pan-Africanism:</title>
                     <subtitle>Ngũgĩ wa Thiong’o’s Theater and the Art and Politics of Modernizing African Culture</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mbowa</surname>
                           <given-names>Aida</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>328</fpage>
                  <abstract>
                     <p>Modernization was an important and guiding philosophy on how best to build the newly formed nation-state in post-independence Africa. Modernization encapsulated more than the matter of industrialization and infrastructure. It also incorporated cultural efforts to bolster the psychology of the formerly colonized. Significant objectives for cultural politicians to achieve under the rubric of modernization included how to empower citizens, how to facilitate pride and allegiance to the nation-state, and how to overcome divisions or in some instances maintain ethnic specificity while facilitating national unity. The arts in general, and performance arts in particular, were a key method through which to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.20</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>349</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gzh3p.21</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>353</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
