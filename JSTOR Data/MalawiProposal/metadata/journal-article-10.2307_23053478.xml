<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23053478</article-id>
         <article-categories>
            <subj-group>
               <subject>INVITED ARTICLES</subject>
               <subj-group>
                  <subject>VACCINES</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>"Herd Immunity": A Rough Guide</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Paul</given-names>
                  <surname>Fine</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ken</given-names>
                  <surname>Eames</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David L.</given-names>
                  <surname>Heymann</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">52</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23053459</issue-id>
         <fpage>911</fpage>
         <lpage>916</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/cid/cir007"
                   xlink:title="an external site"/>
         <abstract>
            <p>The term "herd immunity" is widely used but carries a variety of meanings [1-7]. Some authors use it to describe the proportion immune among individuals in a population. Others use it with reference to a particular threshold proportion of immune individuals that should lead to a decline in incidence of infection. Still others use it to refer to a pattern of immunity that should protect a population from invasion of a new infection. A common implication of the term is that the risk of infection among susceptible individuals in a population is reduced by the presence and proximity of immune individuals (this is sometimes referred to as "indirect protection" or a "herd effect"). We provide brief historical, epidemiologic, theoretical, and pragmatic public health perspectives on this concept.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d641e188a1310">
            <label>1</label>
            <mixed-citation id="d641e195" publication-type="other">
Fox JP, Elveback L, Scott W, et al. Herd immunity: basic concept and
relevance to public health immunization practices. Am ] Epidemiol
1971; 94:179-89.</mixed-citation>
         </ref>
         <ref id="d641e208a1310">
            <label>2</label>
            <mixed-citation id="d641e215" publication-type="other">
Anderson RM, May RM. Vaccination and herd immunity to infectious
diseases. Nature 1985; 318:323-9.</mixed-citation>
         </ref>
         <ref id="d641e225a1310">
            <label>3</label>
            <mixed-citation id="d641e232" publication-type="other">
Fine PEM. Herd immunity: history, theory, practice. Epidemiol Rev
1993; 15:265-302.</mixed-citation>
         </ref>
         <ref id="d641e242a1310">
            <label>4</label>
            <mixed-citation id="d641e249" publication-type="other">
Fine PEM, Mulholland K. Community immunity. In: Plotkin SA,
Orenstein WA, Offrt PA eds. Vaccines. 5th ed. Chapter 71. Phila-
delphia, PA: Elsevier Inc., 2008:1573-92.</mixed-citation>
         </ref>
         <ref id="d641e263a1310">
            <label>5</label>
            <mixed-citation id="d641e270" publication-type="other">
John TJ, Samuel R. Herd immunity and herd effect: new insights and
definitions. Eur J Epidemiol 2000; 16:601-6.</mixed-citation>
         </ref>
         <ref id="d641e280a1310">
            <label>6</label>
            <mixed-citation id="d641e287" publication-type="other">
Stephens DS. Vaccines for the unvaccinated: protecting the herd. J Inf
Dis 2008; 197:643-45.</mixed-citation>
         </ref>
         <ref id="d641e297a1310">
            <label>7</label>
            <mixed-citation id="d641e304" publication-type="other">
Heymann D, Aylward B. Mass vaccination in public health. In: Hey-
mann D, ed. Control of communicable diseases manual. 19th ed.
Washington, DC: American Public Health Association, 2008.</mixed-citation>
         </ref>
         <ref id="d641e317a1310">
            <label>8</label>
            <mixed-citation id="d641e324" publication-type="other">
Topley WWC, Wilson GS. The spread of bacterial infection: the
problem of herd immunity. J Hyg 1923; 21:243-9.</mixed-citation>
         </ref>
         <ref id="d641e334a1310">
            <label>9</label>
            <mixed-citation id="d641e341" publication-type="other">
Smith CEG. Prospects of the control of disease. Proc Roy Soc Med
1970; 63:1181-90.</mixed-citation>
         </ref>
         <ref id="d641e351a1310">
            <label>10</label>
            <mixed-citation id="d641e358" publication-type="other">
Dietz K. Transmission and control of arbovirus diseases. In: Ludwig D,
Cooke KL, eds. Epidemiology. Philadelphia PA: Society for Industrial
and Applied Mathematics, 1975: 104-21.</mixed-citation>
         </ref>
         <ref id="d641e372a1310">
            <label>11</label>
            <mixed-citation id="d641e379" publication-type="other">
Macdonald G. The epidemiology and control of malaria. London:
Oxford University Press, 1957.</mixed-citation>
         </ref>
         <ref id="d641e389a1310">
            <label>12</label>
            <mixed-citation id="d641e396" publication-type="other">
Anderson RM, May RM. Infectious diseases of humans: dynamics and
control. Oxford, UK: Oxford University Press, 1991.</mixed-citation>
         </ref>
         <ref id="d641e406a1310">
            <label>13</label>
            <mixed-citation id="d641e413" publication-type="other">
Keeling MJ, Rohani P. Modeling infectious diseases in humans and
animals. Princeton, NJ: Princeton University Press, 2007.</mixed-citation>
         </ref>
         <ref id="d641e423a1310">
            <label>14</label>
            <mixed-citation id="d641e430" publication-type="other">
Heesterbeek JA. A brief history of RO and a recipe for its calculation.
Acta Biotheor 2002; 50:189-204.</mixed-citation>
         </ref>
         <ref id="d641e440a1310">
            <label>15</label>
            <mixed-citation id="d641e447" publication-type="other">
Hamer WH. Epidemic disease in England: the evidence of variability
and persistency of type. Lancet 1906; 11:733-9.</mixed-citation>
         </ref>
         <ref id="d641e457a1310">
            <label>16</label>
            <mixed-citation id="d641e464" publication-type="other">
Hedrich AW. Monthly estimates of the child population suscepti-
ble' to measles: 1900 - 31, Baltimore, MD. Am J Hygiene 1933; 17:
613-36.</mixed-citation>
         </ref>
         <ref id="d641e478a1310">
            <label>17</label>
            <mixed-citation id="d641e485" publication-type="other">
Reichert TA, Sugaya N, Fedson DS, et al. The Japanese experience with
vaccinating schoolchildren against influenza. N Engl J Med 2001;
344:889-96.</mixed-citation>
         </ref>
         <ref id="d641e498a1310">
            <label>18</label>
            <mixed-citation id="d641e505" publication-type="other">
Kim J J, Andres-Beck B, Goldie SJ. The value of including boys in an
HPV vaccination programme: a cost-effectiveness analysis in a low-
resource setting. Br J Cancer 2007; 97:1322-8.</mixed-citation>
         </ref>
         <ref id="d641e518a1310">
            <label>19</label>
            <mixed-citation id="d641e525" publication-type="other">
Carter R, Mendis KN, Miller LH, Molyneux L, Saul A. Malaria trans-
mission-blocking vaccines: how can their development be supported?
Nat Med 2000; 6:241-4.</mixed-citation>
         </ref>
         <ref id="d641e538a1310">
            <label>20</label>
            <mixed-citation id="d641e545" publication-type="other">
Bottiger M. A study of the sero-immunity that has protected the
Swedish population against poliomyelitis for 25 years. Scand J Infect
Dis 1987; 19:595-601.</mixed-citation>
         </ref>
         <ref id="d641e558a1310">
            <label>21</label>
            <mixed-citation id="d641e565" publication-type="other">
Longini IM, Halloran ME, Nizam A. Model-based estimation of vac-
cine effects from community vaccine trials. Stat Med 2002; 21:481-95.</mixed-citation>
         </ref>
         <ref id="d641e575a1310">
            <label>22</label>
            <mixed-citation id="d641e582" publication-type="other">
Vynnycky E, White RG. An introduction to infectious disease model-
ling. Oxford, UK: Oxford University Press, 2010.</mixed-citation>
         </ref>
         <ref id="d641e593a1310">
            <label>23</label>
            <mixed-citation id="d641e600" publication-type="other">
Grassly NC, Wenger J, Durrani S, et al. Protective efficacy of a mono-
valent oral type 1 poliovirus vaccine: a case-cOntrol study. Lancet 2007;
369:1356-62.</mixed-citation>
         </ref>
         <ref id="d641e613a1310">
            <label>24</label>
            <mixed-citation id="d641e620" publication-type="other">
Mossong J, Hens N, Jit M, Beutels P, Auranen K, et al. Social contacts
and mixing patterns relevant to the spread of infectious diseases. PLoS
Med 2008; 5:e74 doi:10.1371/journal.pmed.0050074.</mixed-citation>
         </ref>
         <ref id="d641e633a1310">
            <label>25</label>
            <mixed-citation id="d641e640" publication-type="other">
Colizza V, Barrat A, Barthelemy M, Vespigniani A. The role of the
airline transportation network in the prediction and predictability of
global epidemics. Proc Natl Acad Sci U S A 2006; 103:2015-20.</mixed-citation>
         </ref>
         <ref id="d641e653a1310">
            <label>26</label>
            <mixed-citation id="d641e660" publication-type="other">
Keeling MJ, Eames KTD. Networks and epidemic models. J R
Soc_Interface 2005; 2:295-307. doi:10.1098/rsif.2005.0051.</mixed-citation>
         </ref>
         <ref id="d641e670a1310">
            <label>27</label>
            <mixed-citation id="d641e677" publication-type="other">
Eames KTD. Networks of influence and infection: parental choices and
childhood disease. J R Soc Interface 2009; 6:811-4.</mixed-citation>
         </ref>
         <ref id="d641e687a1310">
            <label>28</label>
            <mixed-citation id="d641e694" publication-type="other">
Feikin DR, Lezotte DC, Hamman RF, Salmon DA, Chen RT, Hoffman
RE. Individual and community risks of measles and pertussis asso-
ciated with personal exemptions to immunization. JAMA 2000;
284:3145-50.</mixed-citation>
         </ref>
         <ref id="d641e711a1310">
            <label>29</label>
            <mixed-citation id="d641e718" publication-type="other">
van den Hof S, Meffre CM, Conyn-van Spaendonck MA, Woonink F,
de Melker HE, van Binnendijk RS. Measles outbreak in a community
with very low vaccine coverage, the Netherlands. Emerg Infect Dis
2001; 7(Suppl 3):593-7.</mixed-citation>
         </ref>
         <ref id="d641e734a1310">
            <label>30</label>
            <mixed-citation id="d641e741" publication-type="other">
Bauch CT, Earn DJD. Vaccination and the theory of games. Proc Natl
Acad Sci U S A 2004; 101:13391^4. doi:10.1073/pnas.0403823101.</mixed-citation>
         </ref>
         <ref id="d641e751a1310">
            <label>31</label>
            <mixed-citation id="d641e758" publication-type="other">
Galvani AP, Reluga TC, Chapman GB. Long-standing influenza vac-
cination policy is in accord with individual self-interest but not with
the utilitarian optimum. Proc Natl Acad Sci U S A 2007;
104:5692-7doi:10.1073/pnas.0606774104.</mixed-citation>
         </ref>
         <ref id="d641e774a1310">
            <label>32</label>
            <mixed-citation id="d641e781" publication-type="other">
Fine PEM, Clarkson JA. Individual versus public priorities in the de-
termination of optimal vaccination policies. Am J Epidemiol 1986;
124:1012-20.</mixed-citation>
         </ref>
         <ref id="d641e794a1310">
            <label>33</label>
            <mixed-citation id="d641e801" publication-type="other">
Jansen VAA, Stollenwerk N, Jensen HJ, Ramsay ME, Edmunds WJ,
Rhodes CJ. Measles outbreaks in a population with declining vaccine
uptake. Science 2003; 301:804 doi:10.1126/science.l086726.</mixed-citation>
         </ref>
         <ref id="d641e814a1310">
            <label>34</label>
            <mixed-citation id="d641e821" publication-type="other">
Jahn A, Floyd S, Mwinuka V, et al. Ascertainment of childhood vac-
cination histories in northern Malawi. Trop Med Int Health 2008;
13:129-38.</mixed-citation>
         </ref>
         <ref id="d641e835a1310">
            <label>35</label>
            <mixed-citation id="d641e842" publication-type="other">
CDC. Notes from the held: pertussis: California, January - June 2010.
MMWR Morb Mortal Wkly Rep 2010; 59:817.</mixed-citation>
         </ref>
         <ref id="d641e852a1310">
            <label>36</label>
            <mixed-citation id="d641e859" publication-type="other">
Health Protection Agency. Mumps increase in university students.
Health Protection Report. March 2009. Available at: http://
www.hpa.org.uk/hpr/archives/2009/newsl009.htm. Accessed 01 Octo-
ber 2010.</mixed-citation>
         </ref>
         <ref id="d641e875a1310">
            <label>37</label>
            <mixed-citation id="d641e882" publication-type="other">
Panagiotopoulos T, Antoniadou I, Valassi-Adam E. Increase in con-
genital rubella occurrence after immunization in Greece: retrospective
survey and systematic review. Br Med J 1999; 319:1462-7.</mixed-citation>
         </ref>
         <ref id="d641e895a1310">
            <label>38</label>
            <mixed-citation id="d641e902" publication-type="other">
Greenwood BM. Manson lecture: meningococcal meningitis in Africa.
Trans R Soc Trop Med Hyg 1999; 93:341-53.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
