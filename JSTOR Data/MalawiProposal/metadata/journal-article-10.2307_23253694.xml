<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">ecosystems</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100928</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Ecosystems</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer Science+Business Media</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14329840</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14350629</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23253694</article-id>
         <title-group>
            <article-title>Carbon Stocks in an African Woodland Landscape: Spatial Distributions and Scales of Variation</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Emily</given-names>
                  <surname>Woollen</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Casey M.</given-names>
                  <surname>Ryan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Mathew</given-names>
                  <surname>Williams</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">15</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23253572</issue-id>
         <fpage>804</fpage>
         <lpage>818</lpage>
         <permissions>
            <copyright-statement>© 2012 Springer Science+Business Media, LLC</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23253694"/>
         <abstract>
            <p>Current knowledge of Africa's carbon (C) pools is limited despite its importance in the global C budget. To increase the understanding of C stocks in African woodlands, we asked how C stocks in soil and vegetation vary across a miombo woodland landscape and to what degree and at what scales are these stocks linked? We sampled along a 5-km transect using a cyclic sampling scheme to allow geostatistical analyses. Soil C stocks in the top 5 cm (12.1 ± 0.6 Mg C ha -1 (± SE)) and 30 cm depths (40.1 ± 2.5 Mg C ha -1 ) varied significantly at scales of a few meters (autocorrelation distance 14 m in 0—5-cm and 26 m in 0—30-cm interval), and aboveground (AG) woody C stocks (20.7 ± 1.8 Mg C ha -1 ) varied significantly at kilometer scales (1,426 m). Soil textural distributions were linked to topography (r 2 = 0.54) as were large-tree AG C stocks (r 2 = 0.70). AG C stocks were constrained to an upper boundary by soil texture with greater AG C being associated with coarser textured soils. Vegetation and soil C stocks were coupled in the landscape in the top 5 cm of soil (r 2 = 0.24) but not with deeper soil C stocks, which were coupled to soil clay content (r 2 = 0.38). This study is one of the most complete transect studies in an African miombo woodland, and suggests that C stock distributions are strongly linked to topography and soil texture. To optimize sampling strategies for C stock assessments in miombo, soil C should be sampled at more than 26 m apart, and AG C should be sampled at more than 1,426 m apart in plots larger than 0.5 ha.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d301e231a1310">
            <mixed-citation id="d301e235" publication-type="other">
Bird MI, Veenendaal EM, Moyo C, Lloyd J, Frost P. 2000. Effect
of fire and soil texture on soil carbon in a sub-humid savanna
(Matopos, Zimbabwe). Geoderma 94:71-90.</mixed-citation>
         </ref>
         <ref id="d301e248a1310">
            <mixed-citation id="d301e252" publication-type="other">
Bombelli A, Henry M, Castaldi S, Adu-Bredu S, Arneth A, de
Grandcourt A, Grieco E, Kutsch WL, Lehsten V, Rasile A,
Reichstein M, Tansey K, Weber U, Valentini R. 2009. The sub-
saharan Africa carbon balance, an overview. Biogeosci Discuss
6:2085-123.</mixed-citation>
         </ref>
         <ref id="d301e271a1310">
            <mixed-citation id="d301e275" publication-type="other">
Bond WJ. 2008. What limits trees in C-4 grasslands and savan-
nas? Annu Rev Ecol Evol Syst 39:641-59.</mixed-citation>
         </ref>
         <ref id="d301e285a1310">
            <mixed-citation id="d301e289" publication-type="other">
Bucini G, Hanan NP. 2007. A continental-scale analysis of tree
cover in African savannas. Glob Ecol Biogeogr 16:593-605.</mixed-citation>
         </ref>
         <ref id="d301e300a1310">
            <mixed-citation id="d301e304" publication-type="other">
Burrows SN, Gower ST, Clayton MK, Mackay DS, Ahl DE,
Norman JM, Diak G. 2002. Application of geostatistics to
characterize leaf area index (LAI) from flux tower to landscape
scales using a cyclic sampling design. Ecosystems 5:667-79.</mixed-citation>
         </ref>
         <ref id="d301e320a1310">
            <mixed-citation id="d301e324" publication-type="other">
Campbell BM. 1996. The miombo in transition: woodlands and
welfare in Africa. Bogor: Center for International Forestry
Research, pp 1-5.</mixed-citation>
         </ref>
         <ref id="d301e337a1310">
            <mixed-citation id="d301e341" publication-type="other">
Campbell BM, Angelsen A, Cunningham A, Katerere Y, Sitoe A,
Wunder S. 2007. Miombo woodlands—opportunities and
barriers to sustainable forest management. Bogor: Centre for
International Forestry Research.</mixed-citation>
         </ref>
         <ref id="d301e357a1310">
            <mixed-citation id="d301e361" publication-type="other">
Campbell BM, Cunliffe RN, Gambizia J. 1995. Vegetation
structure and small-scale pattern in miombo woodland,
Marondera, Zimbabwe. Bothalia 25:121-6.</mixed-citation>
         </ref>
         <ref id="d301e374a1310">
            <mixed-citation id="d301e378" publication-type="other">
Chidumayo EN. 1997. Miombo ecology and management: an
introduction. London: Intermediate Technology Publications,
pp 6-34.</mixed-citation>
         </ref>
         <ref id="d301e391a1310">
            <mixed-citation id="d301e395" publication-type="other">
Ciais P, Bombelli A, Williams M, Piao SL, Chave J, Ryan CM,
Henry M, Brender P, Valentini R. 2011. The carbon balance of
Africa: synthesis of recent research studies. Philos Trans A R
Soc A Math Phys Eng Sci 369:2038-57.</mixed-citation>
         </ref>
         <ref id="d301e412a1310">
            <mixed-citation id="d301e416" publication-type="other">
Cressie N. 1985. Fitting variogram models by weighted least-
squares. J Int Assoc Math Geol 17:563-86.</mixed-citation>
         </ref>
         <ref id="d301e426a1310">
            <mixed-citation id="d301e430" publication-type="other">
Dorgeloh WG. 2002. Calibrating a disc pasture meter to estimate
above-ground standing biomass in Mixed Bushveld, South
Africa. Afr J Ecol 40:100-2.</mixed-citation>
         </ref>
         <ref id="d301e443a1310">
            <mixed-citation id="d301e447" publication-type="other">
February EC, Higgins SI. 2010. The distribution of tree and grass
roots in savannas in relation to soil nitrogen and water. S Afr J
Bot 76:517-23.</mixed-citation>
         </ref>
         <ref id="d301e460a1310">
            <mixed-citation id="d301e464" publication-type="other">
Feller C, Beare MH. 1997. Physical control of soil organic matter
dynamics in the tropics. Geoderma 79:69-116.</mixed-citation>
         </ref>
         <ref id="d301e474a1310">
            <mixed-citation id="d301e478" publication-type="other">
Frazer GW, Canham CD. 1999. Gap Light Analyzer (Version
2.0). British Columbia &amp; New York: Simon Fraser University &amp;
Institute of Ecosystem Studies, http://www.ecostudies.org/
gla/.</mixed-citation>
         </ref>
         <ref id="d301e494a1310">
            <mixed-citation id="d301e498" publication-type="other">
Frost P. 1996. The ecology of miombo woodlands. In: Campbell
B, Ed. The miombo in transition: woodlands and welfare in
Africa. Bogor: Center for International Forestry Research,
p 11-57.</mixed-citation>
         </ref>
         <ref id="d301e515a1310">
            <mixed-citation id="d301e519" publication-type="other">
Furley P. 2010. Tropical savannas: biomass, plant ecology, and
the role of fire and soil on vegetation. Prog Phys Geogr
34:563-85.</mixed-citation>
         </ref>
         <ref id="d301e532a1310">
            <mixed-citation id="d301e536" publication-type="other">
Groen TA, van Langevelde F, de Vijver C, Govender N, Prins
HHT. 2008. Soil clay content and fire frequency affect clus-
tering in trees in South African savannas. J Trop Ecol 24:269-
79.</mixed-citation>
         </ref>
         <ref id="d301e552a1310">
            <mixed-citation id="d301e556" publication-type="other">
Hogberg P. 1986. Soil nutrient availability, root symbioses and
tree species composition in tropical Africa: a review. J Trop
Ecol 2:359-72.</mixed-citation>
         </ref>
         <ref id="d301e569a1310">
            <mixed-citation id="d301e573" publication-type="other">
Hook PB, Burke IC. 2000. Biogeochernistry in a shortgrass
landscape: control by topography, soil texture, and microcli-
mate. Ecology 81:2686-703.</mixed-citation>
         </ref>
         <ref id="d301e586a1310">
            <mixed-citation id="d301e590" publication-type="other">
IPCC. 2007. Fourth assessment report: climate change 2007
(AR4). Geneva.</mixed-citation>
         </ref>
         <ref id="d301e600a1310">
            <mixed-citation id="d301e604" publication-type="other">
Jarque CM, Bera AK. 1987. A test for normality of observations
and regression residuals Int. Stat Rev 55:163-72.</mixed-citation>
         </ref>
         <ref id="d301e615a1310">
            <mixed-citation id="d301e619" publication-type="other">
Jeltsch F, Weber GE, Grimm V. 2000. Ecological buffering
mechanisms in savannas: a unifying theory of long-term tree-
grass coexistence. Plant Ecol 150:161-71.</mixed-citation>
         </ref>
         <ref id="d301e632a1310">
            <mixed-citation id="d301e636" publication-type="other">
Jobbagy EG, Jackson RB. 2000. The vertical distribution of soil
organic carbon and its relation to climate and vegetation. Ecol
Appl 10:423-36.</mixed-citation>
         </ref>
         <ref id="d301e649a1310">
            <mixed-citation id="d301e653" publication-type="other">
Johnson KD, Scatena FN, Silver WL. 2011. Atypical soil carbon
distribution across a tropical steepland forest catena. Catena
87:391-7.</mixed-citation>
         </ref>
         <ref id="d301e666a1310">
            <mixed-citation id="d301e670" publication-type="other">
Koenker R. 2011. Quantile regression. CRAN. http://www.r-
project.org.</mixed-citation>
         </ref>
         <ref id="d301e680a1310">
            <mixed-citation id="d301e684" publication-type="other">
Li P, Wang Q, Endo T, Zhao X, Kakubari Y. 2010. Soil organic
carbon stock is closely related to aboveground vegetation
properties in cold-temperate mountainous forests. Geoderma
154:407-15.</mixed-citation>
         </ref>
         <ref id="d301e700a1310">
            <mixed-citation id="d301e704" publication-type="other">
Liu F, Wu XB, Bai E, Boutton TW, Archer SR. 2010. Spatial
scaling of ecosystem C and N in a subtropical savanna land-
scape. Glob Chang Biol 16:2213-23.</mixed-citation>
         </ref>
         <ref id="d301e718a1310">
            <mixed-citation id="d301e722" publication-type="other">
Matheron G. 1965. Les variables regionalisees et leur estimation,
Universite de Paris. Paris: Masson. pp 1-305.</mixed-citation>
         </ref>
         <ref id="d301e732a1310">
            <mixed-citation id="d301e736" publication-type="other">
MathWorks. 2007. Matlab Version 7.4. Natick, MA: The Math-
Works Inc.</mixed-citation>
         </ref>
         <ref id="d301e746a1310">
            <mixed-citation id="d301e750" publication-type="other">
McFarlane MJ. 1989. Dambos-their characteristics and geo-
morphological evolution in parts of Malawi and Zimbabwe,
with particular reference to their role in the hydrogeological
regime of surviving areas of African surface. In: Proceedings of
the groundwater exploration and development in crystalline
basement aquifers workshop, Commonwealth Science
Council Technical Paper 273. Harare: Commonwealth Science
Council, pp. 254-302.</mixed-citation>
         </ref>
         <ref id="d301e779a1310">
            <mixed-citation id="d301e783" publication-type="other">
Quinn GP, Keough MJ. 2002. Experimental design and data
analysis for biologists. Cambridge: Cambridge University Press,
pp 72-110.</mixed-citation>
         </ref>
         <ref id="d301e796a1310">
            <mixed-citation id="d301e800" publication-type="other">
Reed DN, Anderson TM, Dempewolf J, Metzger K, Serneels S.
2009. The spatial distribution of vegetation types in the Ser-
engeti ecosystem: the influence of rainfall and topographic
relief on vegetation patch characteristics. J Biogeogr 36:770-
82.</mixed-citation>
         </ref>
         <ref id="d301e819a1310">
            <mixed-citation id="d301e823" publication-type="other">
Rossatto DR, Silva LDR, Villalobos-Vega R, Sternberg LDL,
Franco AC. 2012. Depth of water uptake in woody plants
relates to groundwater level and vegetation structure along a
topographic gradient in a neotropical savanna. Environ Exp
Bot 77:259-66.</mixed-citation>
         </ref>
         <ref id="d301e843a1310">
            <mixed-citation id="d301e847" publication-type="other">
Rossi J, Govaerts A, De Vos B, Verbist B, Vervoort A, Poesen J,
Muys B, Deckers J. 2009. Spatial structures of soil organic
carbon in tropical forests—a case study of Southeastern Tan-
zania. Catena 77:19-27.</mixed-citation>
         </ref>
         <ref id="d301e863a1310">
            <mixed-citation id="d301e867" publication-type="other">
Ryan CM. 2009. Carbon Cycling, Fire and Phenology in a
Tropical Savanna Woodland in Nhambita, Mozambique.
School of GeoSciences. Edinburgh: The University of Edin-
burgh. pp 36-37.</mixed-citation>
         </ref>
         <ref id="d301e883a1310">
            <mixed-citation id="d301e887" publication-type="other">
Ryan CM, Williams M. 2011. How does fire intensity and fre-
quency affect miombo woodland tree populations and bio-
mass? Ecol Appl 21:48-60.</mixed-citation>
         </ref>
         <ref id="d301e900a1310">
            <mixed-citation id="d301e904" publication-type="other">
Ryan CM, Williams M, Grace J. 2011. Above- and belowground
carbon stocks in a miombo woodland landscape of Mozam-
bique. Biotropica 43:423-32.</mixed-citation>
         </ref>
         <ref id="d301e917a1310">
            <mixed-citation id="d301e921" publication-type="other">
Sankaran M, Ratnam J, Hanan NP. 2004. Tree-grass coexistence
in savannas revisited—insights from an examination of
assumptions and mechanisms invoked in existing models.
Ecol Lett 7:480-90.</mixed-citation>
         </ref>
         <ref id="d301e937a1310">
            <mixed-citation id="d301e941" publication-type="other">
Scholes RJ, Archer SR. 1997. Tree-grass interactions in savannas.
Annu Rev Ecol Syst 28:517-44.</mixed-citation>
         </ref>
         <ref id="d301e952a1310">
            <mixed-citation id="d301e956" publication-type="other">
Schwanghart W. 2010. Variogramfit. Nattick, MA: Mathworks
Inc. http://www.mathworks.com/matlabcentral/flleexchange/
2 5 948- variogramfit.</mixed-citation>
         </ref>
         <ref id="d301e969a1310">
            <mixed-citation id="d301e973" publication-type="other">
Spielvogel S, Prietzel J, Auerswald K, Koegel-Knabner I. 2009.
Site-specific spatial patterns of soil organic carbon stocks in
different landscape units of a high-elevation forest including a
site with forest dieback. Geoderma 152:218-30.</mixed-citation>
         </ref>
         <ref id="d301e989a1310">
            <mixed-citation id="d301e993" publication-type="other">
Timberlake JR, Calvert GM. 1993. Preliminary root atlas for
Zimbabwe and Zambia. Zimb Bull For Res 10:1-96.</mixed-citation>
         </ref>
         <ref id="d301e1003a1310">
            <mixed-citation id="d301e1007" publication-type="other">
Tinley KL. 1977. Framework of the Gorongosa ecosystem. Fac-
ulty of Science. Pretoria: University of Pretoria, pp 34-54.</mixed-citation>
         </ref>
         <ref id="d301e1017a1310">
            <mixed-citation id="d301e1021" publication-type="other">
Tinley KL. 1982. The Influence of Soil Moisture Balance on
Ecosystem Patterns in Southern Africa. In: Huntley BJ,
Walker BH, Eds. Ecology of tropical savannas. Berlin:
Springer, p 175-92.</mixed-citation>
         </ref>
         <ref id="d301e1037a1310">
            <mixed-citation id="d301e1041" publication-type="other">
von der Heyden CJ. 2004. The hydrology and hydrogeology of
dambos: a review. Prog Phys Geogr 28:544-64.</mixed-citation>
         </ref>
         <ref id="d301e1052a1310">
            <mixed-citation id="d301e1056" publication-type="other">
Walter H. 1971. Ecology of tropical and subtropical vegetation.
Edinburgh: Oliver and Boyd, pp 207-36.</mixed-citation>
         </ref>
         <ref id="d301e1066a1310">
            <mixed-citation id="d301e1070" publication-type="other">
Wang LX, Okin GS, Caylor KK, Macko SA. 2009. Spatial het-
erogeneity and sources of soil carbon in southern African
savannas. Geoderma 149:402-8.</mixed-citation>
         </ref>
         <ref id="d301e1083a1310">
            <mixed-citation id="d301e1087" publication-type="other">
Webster R. Oliver MA. 2001. Geostatistics for environmental
scientists. Chichester: Wiley, pp 65-134.</mixed-citation>
         </ref>
         <ref id="d301e1097a1310">
            <mixed-citation id="d301e1101" publication-type="other">
White F. 1983. The vegetation of Africa: a descriptive memoir to
accompany the Unesco/AETFAT/UNSO vegetation map of
Africa. Paris: UNESCO, pp 92-93.</mixed-citation>
         </ref>
         <ref id="d301e1114a1310">
            <mixed-citation id="d301e1118" publication-type="other">
Whitlow JR. 1985. Dambos in Zimbabwe: a review. Zeitschrift
fur Geomorphologie Supplementbande 52:115-46.</mixed-citation>
         </ref>
         <ref id="d301e1128a1310">
            <mixed-citation id="d301e1132" publication-type="other">
Whittaker RJ, Willis KJ, Field R. 2001. Scale and species rich-
ness: towards a general, hierarchical theory of species diver-
sity. J Biogeogr 28:453-70.</mixed-citation>
         </ref>
         <ref id="d301e1146a1310">
            <mixed-citation id="d301e1150" publication-type="other">
Williams M, Ryan CM, Rees RM, Sambane E, Fernando J, Grace
J. 2008. Carbon sequestration and biodiversity of re-growing
miombo woodlands in Mozambique. For Ecol Manag
254:145-55.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
