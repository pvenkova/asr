<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jepidcommheal</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000490</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Epidemiology and Community Health (1979-)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>BMJ Publishing Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">0143005X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14702738</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43282063</article-id>
         <article-categories>
            <subj-group>
               <subject>Other topics</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Microcredit participation and child health: results from a cross-sectional study in Peru</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>H</given-names>
                  <surname>Moseson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>R</given-names>
                  <surname>Hamad</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>L</given-names>
                  <surname>Fernald</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">68</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40129901</issue-id>
         <fpage>1175</fpage>
         <lpage>1181</lpage>
         <permissions>
            <copyright-statement>© 2014 BMJ Publishing Group</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43282063"/>
         <abstract>
            <p>Background: Childhood malnutrition is a major consequence of poverty worldwide. Microcredit programmes—which offer small loans, financial literacy and social support to low-income individuals—are increasingly promoted as a way to improve the health of clients and their families. This study evaluates the hypothesis that longer participation in a microcredit programme is associated with improvements in the health of children of microcredit clients. Methods: Cross-sectional data were collected in February 2007 from 511 clients of a microcredit organisation in Peru and 596 of their children under 5 years of age. The primary predictor variable was length of participation in the microcredit programme. Outcome variables included height, weight, anaemia, household food security and parent-reported indicators of child health. Multivariate linear and logistic regressions assessed the association between the number of loan cycles and child health outcomes. Pathways through which microcredit may have influenced health outcomes were also explored via mediation analyses. Results: Longer participation in microcredit was associated with greater household food security and reduced likelihood of childhood anaemia. No significant associations were observed between microcredit participation and incidence of childhood illnesses or anthropométrie indicators. Increased consumption of red meat may mediate the association between the number of loan cycles and food security, but not the association with anaemia. Conclusions: The effects of microcredit on the health of clients' children are understudied. Exploratory findings from this analysis suggest that microcredit may positively influence child health, and that diet may play a causal role.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1024e188a1310">
            <label>1</label>
            <mixed-citation id="d1024e195" publication-type="other">
Blössner M, De Onis M, Prüss-Üstün A, et al. Malnutrition: quantifying the health
impact at national and local levels. 2005. Geneva, World Health Organization.
WHO Environmental Burden of Disease Series, No. 12.</mixed-citation>
         </ref>
         <ref id="d1024e208a1310">
            <label>2</label>
            <mixed-citation id="d1024e215" publication-type="other">
Caulfield LE, de Onis M, Blössner M, et al. Undernutrition as an underlying cause
of child deaths associated with diarrhea, pneumonia, malaria, and measles. Am J
Clin Nutr 2004;80:193-8.</mixed-citation>
         </ref>
         <ref id="d1024e228a1310">
            <label>3</label>
            <mixed-citation id="d1024e235" publication-type="other">
Walker SP, Wachs TD, Grantham-McGregor S, et al. Inequality in early childhood:
risk and protective factors for early child development. Lancet 2011;378:1325-38.</mixed-citation>
         </ref>
         <ref id="d1024e245a1310">
            <label>4</label>
            <mixed-citation id="d1024e252" publication-type="other">
Bhutta ZA, Das JK, Rizvi A, et al. Evidence-based interventions for improvement of
maternal and child nutrition: what can be done and at what cost? Lancet
2013;382:452-77.</mixed-citation>
         </ref>
         <ref id="d1024e266a1310">
            <label>5</label>
            <mixed-citation id="d1024e273" publication-type="other">
Marmot M, Friel S, Bell R, et al. Closing the gap in a generation: health equity
through action on the social determinants of health. Lancet 2008;372:1661-9.</mixed-citation>
         </ref>
         <ref id="d1024e283a1310">
            <label>6</label>
            <mixed-citation id="d1024e290" publication-type="other">
Littlefield E, Morduch J, Hashemi S. Is microfinance an effective strategy to reach
the Millennium Development Goals? Focus Note 2003;24:1-11.</mixed-citation>
         </ref>
         <ref id="d1024e300a1310">
            <label>7</label>
            <mixed-citation id="d1024e307" publication-type="other">
Leatherman S, Metcalfe M, Geissler K, et al. Integrating microfinance and health
strategies: examining the evidence to inform policy and practice. Health Policy Plan
2012;27:85-101.</mixed-citation>
         </ref>
         <ref id="d1024e320a1310">
            <label>8</label>
            <mixed-citation id="d1024e327" publication-type="other">
Dunford C, MkNelly B. Using microfinance to improve health and nutrition security.
Global HealthLink 2002;118:9, 22.</mixed-citation>
         </ref>
         <ref id="d1024e337a1310">
            <label>9</label>
            <mixed-citation id="d1024e344" publication-type="other">
Hamad R, Fernald LC. Microcredit participation and nutrition outcomes among
women in Peru. J Epidemiol Community Health 2012;66:e1.</mixed-citation>
         </ref>
         <ref id="d1024e354a1310">
            <label>10</label>
            <mixed-citation id="d1024e361" publication-type="other">
Guha-Khasnobis B, Hazarika G. Household access to microcredit and children's food
security in rural Malawi: a gender perspective. Research Paper, UNU-WIDER, United
Nations University (UNU), 2007.</mixed-citation>
         </ref>
         <ref id="d1024e375a1310">
            <label>11</label>
            <mixed-citation id="d1024e382" publication-type="other">
Bangladesh Institute of Development Studies. Final Report on BIDS Study on PKSF's
Monitoring and Evaluation System. Dhaka, Bangladesh, 2001.</mixed-citation>
         </ref>
         <ref id="d1024e392a1310">
            <label>12</label>
            <mixed-citation id="d1024e399" publication-type="other">
Ahmed SM, Petzold M, Kabir ZN, et al. Targeted intervention for the ultra poor in
rural Bangladesh: does it make any difference in their health-seeking behaviour?
Soc Sci Med 2006;63:2899-911.</mixed-citation>
         </ref>
         <ref id="d1024e412a1310">
            <label>13</label>
            <mixed-citation id="d1024e419" publication-type="other">
Coleman BE. The impact of group lending in Northeast Thailand. J Dev Econ
1999;60:105-41.</mixed-citation>
         </ref>
         <ref id="d1024e429a1310">
            <label>14</label>
            <mixed-citation id="d1024e436" publication-type="other">
Mohindra KS, Haddad S, Narayana D. Can microcredit help improve the health of
poor women? Some findings from a cross-sectional study in Kerala, India. Int J
Equity Health 2008;7:2.</mixed-citation>
         </ref>
         <ref id="d1024e449a1310">
            <label>15</label>
            <mixed-citation id="d1024e456" publication-type="other">
Fernald LC, Hamad R, Karlan D, et al. Small individual loans and mental health:
a randomized controlled trial among South African adults. BMC Public Health
2008;8:409.</mixed-citation>
         </ref>
         <ref id="d1024e469a1310">
            <label>16</label>
            <mixed-citation id="d1024e476" publication-type="other">
Ahmed S, Chowdhury M, Bhulya A. Micro-credit and emotional well-being:
experience of poor rural women from Matlab, Bangladesh. World Dev
2001;29:1957-66.</mixed-citation>
         </ref>
         <ref id="d1024e490a1310">
            <label>17</label>
            <mixed-citation id="d1024e497" publication-type="other">
Krauss A, Lontzek L, Meyer J. Does market saturation increase the risk of
over-indebtedness? Washington DC: Consultative Group to Assist the Poor, 2013.</mixed-citation>
         </ref>
         <ref id="d1024e507a1310">
            <label>18</label>
            <mixed-citation id="d1024e514" publication-type="other">
Banerjee A, Duflo E, Glennerster R, et al. The miracle of microfinance? Evidence from
a randomized evaluation. 2013. Working Paper. MIT Department of Economics,
National Bureau of Economic Research, Abdul Latif Jameel Poverty Action Lab.</mixed-citation>
         </ref>
         <ref id="d1024e527a1310">
            <label>19</label>
            <mixed-citation id="d1024e534" publication-type="other">
Goldberg N. Measuring the impact of microfinance: taking stock of what we know.
Washington DC: Grameen Foundation USA, 2005.</mixed-citation>
         </ref>
         <ref id="d1024e544a1310">
            <label>20</label>
            <mixed-citation id="d1024e551" publication-type="other">
Duvendack M, Palmer-Jones R, Copestake JG, et al. What is the evidence of the
impact of microfinance on the well-being of poor people? London: EPPI-Centre,
Social Science Research Unit, Institute of Education, University of London, 2011.</mixed-citation>
         </ref>
         <ref id="d1024e564a1310">
            <label>21</label>
            <mixed-citation id="d1024e571" publication-type="other">
Odell K. Measuring the impact of microfinance. Washington: Grameen Foundation,
2010:1-38.</mixed-citation>
         </ref>
         <ref id="d1024e581a1310">
            <label>22</label>
            <mixed-citation id="d1024e588" publication-type="other">
Orso CE. Microcredit and poverty. An overview of the principal statistical methods
used to measure the programme net impacts; 2011 February, POLIS Working Paper
No 180.</mixed-citation>
         </ref>
         <ref id="d1024e602a1310">
            <label>23</label>
            <mixed-citation id="d1024e609" publication-type="other">
MkNelly B, Dunford C. Impact of credit with education on mothers and their young
children's nutrition: CRECER credit with education program in Bolivia. Davis, CA:
Freedom From Hunger, 1999.</mixed-citation>
         </ref>
         <ref id="d1024e622a1310">
            <label>24</label>
            <mixed-citation id="d1024e629" publication-type="other">
MkNelly B, Dunford C. Impact of credit with education on mothers and their young
children's nutrition: lower Pra rural bank credit with education program in Ghana.
Davis, CA: Freedom From Hunger, 1998.</mixed-citation>
         </ref>
         <ref id="d1024e642a1310">
            <label>25</label>
            <mixed-citation id="d1024e649" publication-type="other">
Prynee C, Wagemakers J, Stephen A, et al. Meat consumption after disaggregation
of meat dishes in a cohort of British adults in 1989 and 1999 in relation to diet
quality. Eur J Clin Nutr 2009;63:660-6.</mixed-citation>
         </ref>
         <ref id="d1024e662a1310">
            <label>26</label>
            <mixed-citation id="d1024e669" publication-type="other">
Ashburn K, Kerrigan D, Sweat M. Micro-credit, women's groups, control of own
money: HIV-related negotiation among partnered Dominican women. AIDS Behav
2008;12:396-403.</mixed-citation>
         </ref>
         <ref id="d1024e682a1310">
            <label>27</label>
            <mixed-citation id="d1024e689" publication-type="other">
Schuler SR, Hashemi SM. Credit programs, women's empowerment, and
contraceptive use in rural Bangladesh. Stud Fam Plann 1994;25:65-76.</mixed-citation>
         </ref>
         <ref id="d1024e699a1310">
            <label>28</label>
            <mixed-citation id="d1024e706" publication-type="other">
Pronyk PM, Hargreaves JR, Morduch J. Microfinance programs and better health:
prospects for sub-Saharan Africa. JAMA 2007;298:1925-7.</mixed-citation>
         </ref>
         <ref id="d1024e717a1310">
            <label>29</label>
            <mixed-citation id="d1024e724" publication-type="other">
Hamad R, Fernald LC, Karlan DS. Health education for microcredit clients in Peru:
a randomized controlled trial. BMC Public Health 2011:11:51.</mixed-citation>
         </ref>
         <ref id="d1024e734a1310">
            <label>30</label>
            <mixed-citation id="d1024e741" publication-type="other">
Smith SC. Village banking and maternal and child health: evidence from Ecuador
and Honduras. World Dev 2002;30:707-23.</mixed-citation>
         </ref>
         <ref id="d1024e751a1310">
            <label>31</label>
            <mixed-citation id="d1024e758" publication-type="other">
Black RE, Allen LH, Bhutta ZA, et al. Maternal and child undernutrition: global and
regional exposures and health consequences. Lancet 0000;371:243-60.</mixed-citation>
         </ref>
         <ref id="d1024e768a1310">
            <label>32</label>
            <mixed-citation id="d1024e775" publication-type="other">
Brett J. "We sacrifice and eat less": the structural complexities of microfinance
participation. Hum Organ 2006;65:8-19.</mixed-citation>
         </ref>
         <ref id="d1024e785a1310">
            <label>33</label>
            <mixed-citation id="d1024e792" publication-type="other">
Mayoux L. Women's empowerment and micro-finance programmes: strategies for
increasing impact. Dev Pract 1998;8:235-41.</mixed-citation>
         </ref>
         <ref id="d1024e802a1310">
            <label>34</label>
            <mixed-citation id="d1024e809" publication-type="other">
Reed L. State of the microcredit summit campaign report. Washington DC:
Microcredit Summit Campaign, 2011.</mixed-citation>
         </ref>
         <ref id="d1024e820a1310">
            <label>35</label>
            <mixed-citation id="d1024e827" publication-type="other">
World Bank. Data by Country: Peru.</mixed-citation>
         </ref>
         <ref id="d1024e834a1310">
            <label>36</label>
            <mixed-citation id="d1024e841" publication-type="other">
Easton T. The hidden wealth of the poor: a survey of microfinance. Economist
2005;377. Special Report: Microfinance, http://www.economist.com/node/5079324</mixed-citation>
         </ref>
         <ref id="d1024e851a1310">
            <label>37</label>
            <mixed-citation id="d1024e858" publication-type="other">
Yunus M. Toward eliminating poverty from the world: Grameen Bank experience.
In: Anderson CL, Looney JW, eds. Making progress: essays in progress and public
policy. Lanham, MD: Lexington Books, 2002:371-8.</mixed-citation>
         </ref>
         <ref id="d1024e871a1310">
            <label>38</label>
            <mixed-citation id="d1024e878" publication-type="other">
Mosley P. The use of control groups in impact assessment for microfinance.
Department of Economics and Department of Agricultural Economics, University of
Reading, 1997.</mixed-citation>
         </ref>
         <ref id="d1024e891a1310">
            <label>39</label>
            <mixed-citation id="d1024e898" publication-type="other">
Bornstein MH. Sensitive periods in developments: interdisciplinary perspectives.
Hillsdale, NJ: Lawrence Erlbaum Publishers, 1987.</mixed-citation>
         </ref>
         <ref id="d1024e908a1310">
            <label>40</label>
            <mixed-citation id="d1024e915" publication-type="other">
MEASURE DHS. Demographic and Health Surveys. Calverton, MD 2009.</mixed-citation>
         </ref>
         <ref id="d1024e923a1310">
            <label>41</label>
            <mixed-citation id="d1024e930" publication-type="other">
Filmer D, Pritchett L. Estimating wealth effects without expenditure data—or tears:
an application to educational enrollments in states of India. Policy Research
Working Papers No. 1994. Washington DC: World Bank, 1998.</mixed-citation>
         </ref>
         <ref id="d1024e943a1310">
            <label>42</label>
            <mixed-citation id="d1024e950" publication-type="other">
Zeller M, Houssou N, Alcaraz GV, et al. Developing poverty assessment tools based
on principal component analysis: results from Bangladesh, Kazakhstan, Uganda,
and Peru. International Association of Agricultural Economists 2006 Annual
Meeting. Queensland, Australia 2006.</mixed-citation>
         </ref>
         <ref id="d1024e966a1310">
            <label>43</label>
            <mixed-citation id="d1024e973" publication-type="other">
Moore JC, Stinson LL, Welniak EJ. Income measurement error in surveys: a review.
J Official Stat 2000;16:331-61.</mixed-citation>
         </ref>
         <ref id="d1024e983a1310">
            <label>44</label>
            <mixed-citation id="d1024e990" publication-type="other">
Brooks-Gunn J, Duncan GJ. The effects of poverty on children. Future Child
1997;7:55-71.</mixed-citation>
         </ref>
         <ref id="d1024e1000a1310">
            <label>45</label>
            <mixed-citation id="d1024e1007" publication-type="other">
WHO. Physical status: the use and interpretation of anthropometry. Report of a
WHO expert committee. Technical report series no. 854. Geneva 1995:854.</mixed-citation>
         </ref>
         <ref id="d1024e1017a1310">
            <label>46</label>
            <mixed-citation id="d1024e1024" publication-type="other">
WHO. Iron deficiency anaemia: assessment, prevention, and control. A Guide for
Programme Managers. Geneva 2001. A publication of the World Health
Organization.</mixed-citation>
         </ref>
         <ref id="d1024e1038a1310">
            <label>47</label>
            <mixed-citation id="d1024e1045" publication-type="other">
Service UER. Food security in the US: survey tools. USDA 2013.</mixed-citation>
         </ref>
         <ref id="d1024e1052a1310">
            <label>48</label>
            <mixed-citation id="d1024e1059" publication-type="other">
Melgar-Quinonez HR, Zubieta A, MkNelly B, et al. Household food insecurity and
food expenditure in Bolivia, Burkina Faso, and the Philippines. J Nutr
2006;136:1431S-7S.</mixed-citation>
         </ref>
         <ref id="d1024e1072a1310">
            <label>49</label>
            <mixed-citation id="d1024e1079" publication-type="other">
Olaya GA, Lawson M, Fewtrell MS. Efficacy and safety of new complementary
feeding guidelines with an emphasis on red meat consumption: a randomized trial
in Bogota, Colombia. Am J Clin Nutr 2013;98:983-93.</mixed-citation>
         </ref>
         <ref id="d1024e1092a1310">
            <label>50</label>
            <mixed-citation id="d1024e1099" publication-type="other">
Marshall SW. Power for tests of interaction: effect of raising the type I error rate.
Epidemiol Perspect Innov 2007;4:4.</mixed-citation>
         </ref>
         <ref id="d1024e1109a1310">
            <label>51</label>
            <mixed-citation id="d1024e1116" publication-type="other">
Zellner A. An efficient method of estimating seemingly unrelated regressions and
tests for aggregation bias. J Am Stat Assoc 1962;57:348-68.</mixed-citation>
         </ref>
         <ref id="d1024e1126a1310">
            <label>52</label>
            <mixed-citation id="d1024e1133" publication-type="other">
Baron RM, Kenny DA. The moderator-mediator variable distinction in social
psychological research: Conceptual, strategic, and statistical considerations. J Pers
Soc Psychol 1986;51:1173.</mixed-citation>
         </ref>
         <ref id="d1024e1147a1310">
            <label>53</label>
            <mixed-citation id="d1024e1154" publication-type="other">
Preacher KJ, Hayes AF. Asymptotic and resampling strategies for assessing and
comparing indirect effects in multiple mediator models. Behav Res Methods
2008;40:879-91.</mixed-citation>
         </ref>
         <ref id="d1024e1167a1310">
            <label>54</label>
            <mixed-citation id="d1024e1174" publication-type="other">
MacKinnon DP, Dwyer JH. Estimating mediated effects in prevention studies. Eval
Rev 1993;17:144-58.</mixed-citation>
         </ref>
         <ref id="d1024e1184a1310">
            <label>55</label>
            <mixed-citation id="d1024e1191" publication-type="other">
Cole S, Hernán M. Fallibility in estimating direct effects. Int J Epidemiol
2002;31:163-5.</mixed-citation>
         </ref>
         <ref id="d1024e1201a1310">
            <label>56</label>
            <mixed-citation id="d1024e1208" publication-type="other">
Petersen M, Sinisi S, van der Laan M. Estimation of direct causal effects.
Epidemiology 2006;17:276-84.</mixed-citation>
         </ref>
         <ref id="d1024e1218a1310">
            <label>57</label>
            <mixed-citation id="d1024e1225" publication-type="other">
VanderWeele T, Vansteelandt S. Odds ratios for mediation analysis for dichotomous
outcome. Am J Epidemiol 2010;172:1339-48.</mixed-citation>
         </ref>
         <ref id="d1024e1235a1310">
            <label>58</label>
            <mixed-citation id="d1024e1242" publication-type="other">
Benoist Bd, McLean E, Egll I, et al. Worldwide prevalence of anaemia 1993-2005:
WHO global database on anaemia. World Health Organization, 2008.</mixed-citation>
         </ref>
         <ref id="d1024e1253a1310">
            <label>59</label>
            <mixed-citation id="d1024e1260" publication-type="other">
Castro Bedriñana J, Chirinos Peinado D. Z-score anthropometric indicators derived
from NCHS-1 977, CDC-2000 and WHO-2006 in children under 5 years in Central
Area of Peru. Universal J Public Health 2014;2:73-81.</mixed-citation>
         </ref>
         <ref id="d1024e1273a1310">
            <label>60</label>
            <mixed-citation id="d1024e1280" publication-type="other">
Allen MW, Baines S. Manipulating the symbolic meaning of meat to encourage
greater acceptance of fruits and vegetables and less proclivity for red and white
meat. Appetite 2002;38:118-30.</mixed-citation>
         </ref>
         <ref id="d1024e1293a1310">
            <label>61</label>
            <mixed-citation id="d1024e1300" publication-type="other">
Messer E. Intra-household allocation of food and health care: current findings and
understandings—Introduction. Soc Sci Med 1997;44:1675-84.</mixed-citation>
         </ref>
         <ref id="d1024e1310a1310">
            <label>62</label>
            <mixed-citation id="d1024e1317" publication-type="other">
Pitt MM, Khandker SR. The impact of group-based credit programs on poor
households in Bangladesh: does the gender of participants matter? J Political
Economy 1998;106:958-96.</mixed-citation>
         </ref>
         <ref id="d1024e1330a1310">
            <label>63</label>
            <mixed-citation id="d1024e1337" publication-type="other">
Pelletier DL, Deneke K, Kidane Y, et al. The food-first bias and nutrition policy:
lessons from Ethiopia. Food Policy 1995;20:279-98.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
