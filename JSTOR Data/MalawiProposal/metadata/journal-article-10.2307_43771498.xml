<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">britjsociwork</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009790</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The British Journal of Social Work</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00453102</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1468263X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43771498</article-id>
         <title-group>
            <article-title>Change and Continuity: A Quantitative Investigation of Trends and Characteristics of International Social Workers in England</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Shereen</given-names>
                  <surname>Hussein</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Martin</given-names>
                  <surname>Stevens</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jill</given-names>
                  <surname>Manthorpe</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jo</given-names>
                  <surname>Moriarty</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">41</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">6</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40152835</issue-id>
         <fpage>1140</fpage>
         <lpage>1157</lpage>
         <permissions>
            <copyright-statement>© The British Association of Social Workers 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43771498"/>
         <abstract>
            <p>The UK has long experienced a shortage of social workers and has recruited internationally to meet demand. There have been few specific data quantifying the scale of social work mobility to the UK through which such experiences can be set in context. The analysis reported in this article uses data from October 2008, relating to registered social workers working in England. As part of a wider study of migrant social care workers in England, the article reports analysis of data records of over 7,000 non-UK social workers registered to work in England and compares their characteristics to UK-qualified social workers. These analyses are supplemented by analysis of more recent application and registration data from the General Social Care Council pertaining to social workers qualified within and outside the European Union during 2008 and 2009. The findings highlight several important observations in terms of non-UK-qualified social workers' profile as well as some possible trends in migration and variations in rates of qualification verification. Over half of all international social workers in England were trained in four countries: Australia, South Africa, India and the USA. Findings are contextualised with qualitative data obtained from the wider study and policy debates.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1835e205a1310">
            <mixed-citation id="d1835e209" publication-type="other">
Carey, M. (2008) 'Everything must go? The Privatisation of state social work', British
Journal of Social Work, 38, pp. 918-35.</mixed-citation>
         </ref>
         <ref id="d1835e219a1310">
            <mixed-citation id="d1835e223" publication-type="other">
Carey, M. (2009) 'The order of chaos: Exploring agency care managers' construction of
social order within fragmented worlds of state social work', British Journal of Social
Work, 39(3), pp. 556-73.</mixed-citation>
         </ref>
         <ref id="d1835e236a1310">
            <mixed-citation id="d1835e240" publication-type="other">
Comes, M., Moriarty, J., Blendi-Mahota, S., Chittleburgh, T., Hussein, S.
and Manthorpe, J. (2010) Working for the Agency: The Role and Significance of Tem-
porary Employment Agencies in the Social Care Workforce, London, Social Care
Workforce Research Unit, King's College London.</mixed-citation>
         </ref>
         <ref id="d1835e256a1310">
            <mixed-citation id="d1835e260" publication-type="other">
Curtis, L., Moriarty, J. and Netten, A. (2010) 'The expected working life of a social
worker', British Journal of Social Work, 40(5), pp. 1628-43.</mixed-citation>
         </ref>
         <ref id="d1835e271a1310">
            <mixed-citation id="d1835e275" publication-type="other">
Dominelli, L. (2010) Social Work in a Globalizing World, Cambridge, Polity Press.</mixed-citation>
         </ref>
         <ref id="d1835e282a1310">
            <mixed-citation id="d1835e286" publication-type="other">
European Ministers of Education (1999) The Bologna Declaration of 19 June 1999,
Brussels, The European Union.</mixed-citation>
         </ref>
         <ref id="d1835e296a1310">
            <mixed-citation id="d1835e300" publication-type="other">
Evaluation of Social Work Degree Qualification in England Team (2008) Evaluation of
the New Social Work Degree Qualification in England. Volume 1: Findings, London,
Social Care Workforce Research Unit, King's College London.</mixed-citation>
         </ref>
         <ref id="d1835e313a1310">
            <mixed-citation id="d1835e317" publication-type="other">
Harlow, E. (2004) 'Why don't women want to be social workers anymore? New manage-
rialism, postfeminism and the shortage of social workers in social services depart-
ments in England and Wales', European Journal of Social Work, 7(2), pp. 167-79.</mixed-citation>
         </ref>
         <ref id="d1835e330a1310">
            <mixed-citation id="d1835e334" publication-type="other">
Harris, R. and Lavan, A. (1992) 'Professional mobility in the new Europe: The case of
social work', Journal of European Social Policy, 2, pp. 1-15.</mixed-citation>
         </ref>
         <ref id="d1835e344a1310">
            <mixed-citation id="d1835e348" publication-type="other">
Hussein, S., Manthorpe, J. and Stevens, M. (2010a) 'People in places: a qualitative
exploration of recruitment agencies' perspectives on the employment of international
social workers in the UK', British Journal of Social Work, 40(3), pp. 1000-16.</mixed-citation>
         </ref>
         <ref id="d1835e362a1310">
            <mixed-citation id="d1835e366" publication-type="other">
Hussein, S., Moriarty, J., Manthorpe, J. and Huxley, P. (2008) 'Diversity and progression
among students starting social work qualifying programmes in England between 1995
to 1998: A quantitative study', British Journal of Social Work, 38(8), pp. 1588-609.</mixed-citation>
         </ref>
         <ref id="d1835e379a1310">
            <mixed-citation id="d1835e383" publication-type="other">
Hussein, S., Stevens, M. and Manthorpe, J. (2010Ò) International Social Care Workers in
England: Profile, Motivations, Experiences and Future Expectations: Final Report,
London, Social Care Workforce Research Unit, King's College London.</mixed-citation>
         </ref>
         <ref id="d1835e396a1310">
            <mixed-citation id="d1835e400" publication-type="other">
Hussein, S., Stevens, M. and Manthorpe, J. (2011) 'What drives the recruitment of
migrant workers to work in social care in England?', Social Policy and Society, 10(3).</mixed-citation>
         </ref>
         <ref id="d1835e410a1310">
            <mixed-citation id="d1835e414" publication-type="other">
Hussein, S., Manthorpe, J. and Stevens, M. (in press) 'The experiences of migrant social
work and social care practitioners in the UK: Findings from an online survey',
European Journal of Social Work. First published 25 January 2011 (iFirst) DOI:
10.1080/13691457.2010.513962.</mixed-citation>
         </ref>
         <ref id="d1835e430a1310">
            <mixed-citation id="d1835e434" publication-type="other">
Kornbeck, J. (2004) 'Linguistic affinity and achieved geographic mobility: Evidence from
the recognition of non-national social work qualifications in Ireland and the UK',
European Journal of Social Work, 7(2), pp. 143-65.</mixed-citation>
         </ref>
         <ref id="d1835e447a1310">
            <mixed-citation id="d1835e451" publication-type="other">
Lyons, K. (2006) 'Globalization and social work: International and local implications',
British Journal of Social Work, 36, pp. 365-80.</mixed-citation>
         </ref>
         <ref id="d1835e462a1310">
            <mixed-citation id="d1835e466" publication-type="other">
Lyons, K. and Lawrence, S. (2009) 'Social work as an international profession: Origins,
organisations and networks', in S. Lawrence, K. Lyons, G. Simpson and N. Huegler
(eds), Introducing International Social Work, Exeter, Learning Matters.</mixed-citation>
         </ref>
         <ref id="d1835e479a1310">
            <mixed-citation id="d1835e483" publication-type="other">
Manthorpe, J., Hussein, S., Charles, N., Rapaport, P., Stevens, M. and Nagendran, T.
(2010a) 'Social care stakeholders' perceptions of the recruitment of international
practitioners in the United Kingdom: A qualitative study', European Journal of
Social Work, 13(3), pp. 393-408.</mixed-citation>
         </ref>
         <ref id="d1835e499a1310">
            <mixed-citation id="d1835e503" publication-type="other">
Manthorpe, J., Moriarty, J., Hussein, S., Stevens, M., Sharpe, E., Orme, J., Maclntyre, G.,
Green Lister, P. and Crisp, B. (2010Ò) 'Applications to social work programmes in
England: Students as consumers?', Social Work Education, 20(6), pp. 584-98.</mixed-citation>
         </ref>
         <ref id="d1835e516a1310">
            <mixed-citation id="d1835e520" publication-type="other">
Moriarty, J., Manthorpe, J., Hussein, S. and Cornes, M. (2008) 'Staff shortages and immi-
gration in the social care sector', paper prepared for the Migration Advisory Commit-
tee (MAC), London, Social Care Workforce Research Unit, King's College London.</mixed-citation>
         </ref>
         <ref id="d1835e533a1310">
            <mixed-citation id="d1835e537" publication-type="other">
Simpson, G. (2009) 'Global and local issues in the training of "overseas" social workers',
Social Work Education, 28(6), pp. 655-67.</mixed-citation>
         </ref>
         <ref id="d1835e547a1310">
            <mixed-citation id="d1835e551" publication-type="other">
Social Work Task Force (2010) Building a Safe, Confident Future: The Final Report of the
Social Work Task Force: One Year On-Progress Report from the Social Work
Reform Board, London, Department of Education, available online at www.
education,gov.uk/swrb/downloads/Building%20a%20safe%20and%20confident%
20future%20-%200ne%20year%20on, %20progress%20report.pdf (accessed 21
December 2010).</mixed-citation>
         </ref>
         <ref id="d1835e575a1310">
            <mixed-citation id="d1835e579" publication-type="other">
Statham, J., Cameron, C. and Mooney, A. (2006) The Tasks and Roles of Social Workers:
A Focused Overview of Research Evidence, London, The Thomas Coram Research
Unit.</mixed-citation>
         </ref>
         <ref id="d1835e592a1310">
            <mixed-citation id="d1835e596" publication-type="other">
Walsh, T., Wilson, G. and O'Connor, E. (2010) 'Local, European and global: An explora-
tion of migration patterns of social workers into Ireland', British Journal of Social
Work, 40(6), pp. 1978-95.</mixed-citation>
         </ref>
         <ref id="d1835e609a1310">
            <mixed-citation id="d1835e613" publication-type="other">
Welbourne, P., Harrison, G. and Ford, D. (2007) 'Social work in the UK and the global
labour market', International Social Work, 50(1), pp. 27-40.</mixed-citation>
         </ref>
         <ref id="d1835e623a1310">
            <mixed-citation id="d1835e627" publication-type="other">
Yeates, N. (2009) Globalising Care Economies and Migrant Workers: Explorations in
Global Care Chains, London, Palgrave.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
