<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt24hpvc</book-id>
      <subj-group>
         <subject content-type="call-number">JC571.H25 2013</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Human rights</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Law</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Making Human Rights a Reality</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Hafner-Burton</surname>
               <given-names>Emilie M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>21</day>
         <month>03</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9780691155357</isbn>
      <isbn content-type="ppub">0691155356</isbn>
      <isbn content-type="epub">9781400846283</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>Princeton; Oxford</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt24hpvc"/>
      <abstract abstract-type="short">
         <p>In the last six decades, one of the most striking developments in international law is the emergence of a massive body of legal norms and procedures aimed at protecting human rights. In many countries, though, there is little relationship between international law and the actual protection of human rights on the ground.<italic>Making Human Rights a Reality</italic>takes a fresh look at why it's been so hard for international law to have much impact in parts of the world where human rights are most at risk.</p>
         <p>Emilie Hafner-Burton argues that more progress is possible if human rights promoters work strategically with the group of states that have dedicated resources to human rights protection. These human rights "stewards" can focus their resources on places where the tangible benefits to human rights are greatest. Success will require setting priorities as well as engaging local stakeholders such as nongovernmental organizations and national human rights institutions.</p>
         <p>To date, promoters of international human rights law have relied too heavily on setting universal goals and procedures and not enough on assessing what actually works and setting priorities. Hafner-Burton illustrates how, with a different strategy, human rights stewards can make international law more effective and also safeguard human rights for more of the world population.</p>
      </abstract>
      <counts>
         <page-count count="296"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.4</book-part-id>
                  <title-group>
                     <title>Research</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.5</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>xv</fpage>
                  <abstract>
                     <p>This book concerns the promotion and protection of human rights. Human rights are basic rights and freedoms to which all people are entitled regardless of nationality, sex, ethnic origin, race, religion, language, or other status.¹ They are moral assurances and prerequisites to human well-being. Many laws provide for them—from rights to life, liberty, and personal security, to education, peaceful assembly, opinion, and expression. Those assurances are for everyone: the principle of universality is the cornerstone of international human rights law. Human rights are indivisible: we aren’t supposed to pick and choose among them.</p>
                     <p>Of course, universal assurances don’t automatically</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Problem of Human Rights</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>On June 12, 2009, Iran held a presidential election whose outcome was preordained. The next day, authorities declared the incumbent president Mahmoud Ahmadinejad the winner. Protests erupted. Millions of people flooded the streets to dispute the results. The world watched while the government responded with sweeping human rights violations.</p>
                     <p>Plainclothes forces attacked a Tehran University dormitory and reportedly killed student protesters.¹ The government banned foreign journalists from the streets after arresting almost one hundred people, including former government ministers and senior political figures.² Among the casualties was Neda Agha, a young bystander killed by riot police who became an icon</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.7</book-part-id>
                  <title-group>
                     <title>[PART I Introduction]</title>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>This book is about how international law and state power can change the benefits and costs of protecting human rights. Before we look to law and power, however, we must understand why abuse arises in the first place and why abusive practices persist. That’s the task of chapters 2 and 3. In them, I adopt the perspective that the perpetrators of human rights abuse act in ways similar to other criminals. That vantage point not only helps us understand when and how law and power might have an impact on abusive behavior—tasks taken up in other portions of this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Contexts</title>
                  </title-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>People break laws for many reasons. The most basic is opportunity, not biology or mental illness.¹ People want something (a benefit from lawbreaking) they believe they can obtain with tolerable consequences. Indeed, even seemingly ordinary or upstanding citizens may choose to break laws given the chance.² To be sure, some criminal behavior arises from less reasoned circumstances—on a whim, out of delusion or anger, or in response to fear. But human rights abuses usually are not so impulsive. Normally they are planned and organized, and they frequently engage networks of people and institutions—not as a single impetuous action</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Rationales</title>
                  </title-group>
                  <fpage>29</fpage>
                  <abstract>
                     <p>A now-famous experiment studied the reactions of twenty-four male volunteers from junior colleges when they were given abusive tasks. The volunteers were tasked with punishing, with what they thought were painful electric shocks, another group of students if they made poor collective decisions. Some of the targets of this abuse were described as being “rotten”; others were labeled perceptive and understanding; and still others were given neutral terms. The group administering shocks had never met the other students, but its members gave stronger shocks to the students who had been portrayed in the most pejorative terms.¹ Of course, this study</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.10</book-part-id>
                  <title-group>
                     <title>[PART II Introduction]</title>
                  </title-group>
                  <fpage>41</fpage>
                  <abstract>
                     <p>In part I, I explored why people commit human rights abuses and other crimes. I now look at the international legal agreements and procedures that governments and NGOs have created with the goal of spreading norms that curtail abuse. In response to human rights atrocities such as the Nazi Holocaust, over the last six decades governments have elaborated an entire structure of doctrines, treaties, courts, and organizations—a network of obligations, norms, and procedures that make up the international human rights legal system.</p>
                     <p>The breadth of the international human rights legal system is extraordinary. Its existence is the result of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.11</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The International Human Rights Legal System</title>
                  </title-group>
                  <fpage>44</fpage>
                  <abstract>
                     <p>This chapter, written for nonexperts, is an introduction to the most important nuts and bolts of the international human rights legal system. Most of those human rights laws are intended to regulate governments themselves. Yet some also apply directly to individuals who perpetrate human rights abuses—also known as international criminal law. International legal norms come in many different types. I concentrate on two that have attracted the most scholarship and political attention.</p>
                     <p>One type of law—the most visible kind—is treaties. Today’s human rights legal system includes many treaties. These are a form of contract that governments willingly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.12</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Scholarly Perspectives</title>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>The last chapter outlined the large and growing system of international laws and procedures that is tasked with protecting human rights. The next three chapters, from different perspectives, will look at whether those legal institutions are actually associated with protecting human rights. No single vantage point can capture the richness of experience with human rights law. All three perspectives, however, suggest that the actual implementation of the system’s norms is highly uneven. Compliance with international norms is highest in countries already primed to protect human rights—notably in some established and emerging democracies. Yet human rights abuse continues apace because</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.13</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Practitioner Perspectives</title>
                  </title-group>
                  <fpage>86</fpage>
                  <abstract>
                     <p>The battery of statistical associations described in chapter 5 suggest that participation in the international human rights legal system doesn’t always relate well to actual respect for human rights. It relates best in particular contexts—notably in the presence of factors that are strongly associated with democratic rule, such as a well-functioning independent national legal system and organized civil society that can hold governments accountable through elections or other mechanisms. But the statistical research can only reveal so much. It is best for looking at broad patterns, and it’s only as good as the quality of the underlying data. Crucially,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>System Reform</title>
                  </title-group>
                  <fpage>116</fpage>
                  <abstract>
                     <p>While the aspirations for the international human rights legal system are high, the facts on the ground about how the system actually functions are worrying. Chapter 5 suggested, from the vantage point of systematic scholarly research, that the system most corresponds to human rights protections in a subset of countries—democracies, and some “in flux”—which are generally not the leading abusers of human rights. Chapter 6 echoed this message, along with a lot of detail from practitioners who work inside the international legal system and know it well. Mindful of those chapters, what can be done to make the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.15</book-part-id>
                  <title-group>
                     <title>[PART III Introduction]</title>
                  </title-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>While the international human rights legal system has achieved much, it also leaves substantial room for improvement. Some of the very attributes that make this system so attractive in theory—that it aims for universal coverage and aspires to protect a growing list of rights—also limit the system’s practical influence on the calculus of abuse. Parts I and II of this book explained why the system is likely to work best to improve respect for human rights mainly in special circumstances. People with incentives to break the law are inclined to obey legal norms most when they fear the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.16</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The Status Quo</title>
                  </title-group>
                  <fpage>138</fpage>
                  <abstract>
                     <p>Where the international legal system can’t have much impact on abuse, the policy of states that are willing protectors of human rights could play a special role. Governments in these states are willing to invest substantial resources in protecting human rights abroad. Often these states also face strong public pressure at home from human rights organizations, religious groups, and other groups to use their power in this way.</p>
                     <p>This chapter looks at how two especially active stewards already use their political authority, resources, and reach to promote human rights. I focus on the US and EU, but they’re not the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.17</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Nongovernmental Organizations</title>
                  </title-group>
                  <fpage>151</fpage>
                  <abstract>
                     <p>Governments already use their power as stewards for human rights. They discuss human rights at the highest levels of diplomacy, doling out punishments and rewards. They try to be persuasive. Chapter 8 surveyed some of their efforts to help protect human rights and illustrated what works, even in contexts where international laws usually don’t get direct traction. Chapter 8 also described some of the challenges and dangers.</p>
                     <p>A central challenge with all these attempts is that they are seen, within the target country, as foreign governments imposing “their” interests on the rest of the world. Such troubles arise not only</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.18</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>National Human Rights Institutions</title>
                  </title-group>
                  <fpage>164</fpage>
                  <abstract>
                     <p>Chapter 9 looked at how governments that are keen to promote human rights abroad can use their power more effectively by working with local NGOs. This chapter discusses localization of a different type: through national human rights institutions (NHRIs). Unlike NGOs, these organizations have formalized roles in public debate and the national policy process. States create and mostly fund them; in most instances they are accountable to legislatures.¹ Like NGOs, they are abundant and active—existing in more than a hundred countries.² They work through reporting, educational programs, media campaigns, networking, and involvement with civil society and other stakeholders.</p>
                     <p>This</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.19</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Triage</title>
                  </title-group>
                  <fpage>176</fpage>
                  <abstract>
                     <p>This chapter further develops a stewardship strategy. Its central message is that the universality of human rights norms, which are the bedrock of the international human rights legal system and the core idea of the Universal Declaration of Human Rights, is not a tenable guide for the most effective implementation of human rights norms. The resources needed to protect human rights are scarce. No steward can or will try to make a difference everywhere. No reformer can fix every aspect of the international legal system. Constraints require choices. This chapter offers a framework for making those choices in ways that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.20</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Making More of Law and Power</title>
                  </title-group>
                  <fpage>193</fpage>
                  <abstract>
                     <p>During the twentieth century, the toll from human rights violations was greater than that of other crimes such as street violence that get more attention. Violations of human rights cost the lives of almost two hundred million people, and hundreds of millions were raped, tortured, and displaced.¹ What can be done to address this crisis? The answers lie in a careful rethinking of strategy. It requires choices, based on evidence, about how to allocate available resources more effectively. And it demands the ability to realize that in some settings, human rights promotion can’t have much impact. The answers lead to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.21</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>199</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt24hpvc.22</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>267</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
