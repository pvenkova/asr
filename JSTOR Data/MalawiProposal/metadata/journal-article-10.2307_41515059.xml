<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jvertpale</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101365</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Vertebrate Paleontology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Taylor &amp; Francis Group</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02724634</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19372809</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41515059</article-id>
         <title-group>
            <article-title>POSTCRANIAL ANATOMY OF SEBECUS ICAEORHINUS (CROCODYLIFORMES, SEBECIDAE) FROM THE EOCENE OF PATAGONIA</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>DIEGO</given-names>
                  <surname>POL</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>JUAN M.</given-names>
                  <surname>LEARDI</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>AGUSTINA</given-names>
                  <surname>LECUONA</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>MARCELO</given-names>
                  <surname>KRAUSE</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">32</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40074233</issue-id>
         <fpage>328</fpage>
         <lpage>354</lpage>
         <permissions>
            <copyright-statement>Copyright © 2012 Society of Vertebrate Paleontology</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41515059"/>
         <abstract>
            <p>We describe postcranial remains of new specimens referred to Sebecus icaeorhinus found in the lower section of the Sarmiento Formation at Cañadón Hondo (central Patagonia, Argentina), commonly regarded as part of the Casamayoran South American Land Mammal Age (middle Eocene). The new specimens include a partially articulated postcranium associated with teeth and fragmentary remains of the mandible that allows their identification as S. icaeorhinus. This taxon was almost exclusively known from skull remains from the same stratigraphie unit and was characterized by unique cranial features such as a long, high, and narrow rostrum bearing serrated teeth. The new material reveals numerous details on the postcranial anatomy of this crocodyliform, including the presence of proportionately long limbs and 10 autapomorphies in the vertebrae, forelimb, and pelvic girdle (some of which are interpreted as adaptations to terrestriality and an erect limb posture). These features depict a highly modified postcranial anatomy for S. icaeorhinus in comparison with that of neosuchian crocodyliforms, paralleling the uniqueness of its skull anatomy. The new information is also phylogenetically informative and incorporated into a cladistic analysis that corroborates not only the close affinities of Sebecidae with Baurusuchidae (sebecosuchian monophyly), but also the deeply nested position of this clade within Notosuchia. The incorporation of postcranial characters to the phylogenetic analysis also results in a novel arrangement of the basal mesoeucrocodylians recorded in the Cretaceous-Cenozoic of Gondwana, clustering all of these species into a large monophyletic clade.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>LITERATURE CITED</title>
         <ref id="d903e381a1310">
            <mixed-citation id="d903e385" publication-type="other">
Andreis, R. R. 1977. Geología del área de Cañadón Hondo, Depto Es-
calante, provincia del Chubut, República Argentina. Obra Cente-
nario de La Plata 4:77-102.</mixed-citation>
         </ref>
         <ref id="d903e398a1310">
            <mixed-citation id="d903e402" publication-type="other">
Brinkman, D. 1980. The hind limb step cycle of Caiman sclerops and the
mechanics of the crocodile tarsus and metatarsus. Canadian Journal
of Zoology 464:1-23.</mixed-citation>
         </ref>
         <ref id="d903e415a1310">
            <mixed-citation id="d903e419" publication-type="other">
Buckley, G. A., and C. A. Brochu. 1999. An enigmatic new crocodile from
the Upper Cretaceous of Madagascar. Special Papers in Palaeontol-
ogy 60:149-175.</mixed-citation>
         </ref>
         <ref id="d903e432a1310">
            <mixed-citation id="d903e436" publication-type="other">
Buckley, G. A., C. A. Brochu, D. W. Krause, and D. Pol. 2000. A pug-
nosed crocodyliform from the Late Cretaceous of Madagascar. Na-
ture 405:941-944.</mixed-citation>
         </ref>
         <ref id="d903e450a1310">
            <mixed-citation id="d903e454" publication-type="other">
Buffetaut, E. 1980. Histoire biogéographique des Sebecosuchia
(Crocodylia, Mesosuchia): un essai d'interprétation. Annales
de Paléontologie (Vertébrés) 66:1-18.</mixed-citation>
         </ref>
         <ref id="d903e467a1310">
            <mixed-citation id="d903e471" publication-type="other">
Buffetaut, E. 1991. Itasuchus Price, 1955; pp. 348-350 in J. G. Maisey
(ed.), Santana Fossils: An Illustrated Atlas. TFH Publications Inc.,
Neptune, New Jersey.</mixed-citation>
         </ref>
         <ref id="d903e484a1310">
            <mixed-citation id="d903e488" publication-type="other">
Buffetaut, E., and L. Marshall. 1991. A new crocodilian, Sebecus quere-
jazus, nov. sp. (Mesosuchia, Sebecidae) from the Santa Lucía For-
mation (Early Paleocene) at Vila Vila, Southcentral Bolivia; pp.
545-557 in R. Suárez-Soruco (ed.), Fósiles y Facies de Bolivia. Vol-
ume I, Vertebrados. Revista Técnica de YPFB, Santa Cruz, Bolivia.</mixed-citation>
         </ref>
         <ref id="d903e507a1310">
            <mixed-citation id="d903e511" publication-type="other">
Busbey, A. B. I. 1986. New material of Sebecus cf. huilensis (Crocodylia:
Sebecosuchidae) from the Miocene of La Venta Formation of
Colombia. Journal of Vertebrate Paleontology 6:20-27.</mixed-citation>
         </ref>
         <ref id="d903e524a1310">
            <mixed-citation id="d903e528" publication-type="other">
Buscalioni, A. D., and J. L. Sanz. 1988. Phylogenetic Relationships of
the Atoposauridae (Archosauria, Crocodylomorpha). Historical Bi-
ology 1:233-250.</mixed-citation>
         </ref>
         <ref id="d903e541a1310">
            <mixed-citation id="d903e545" publication-type="other">
Carrano, M. T., and J. R. Hutchinson. 2002. Pelvis and hindlimb mus-
culature of Tyrannosaurus rex (Dinosauria: Theropoda). Journal of
Morphology 253:207-228.</mixed-citation>
         </ref>
         <ref id="d903e559a1310">
            <mixed-citation id="d903e563" publication-type="other">
Carvalho, I. d. S., L. C. B. Ribeiro, and L. d. S. Avilla. 2004. Uberaba-
suchus terrificus sp. nov., a New Crocodylomorpha from the Bauru
Basin (Upper Cretaceous), Brazil. Gondwana Research 7:975-1002.</mixed-citation>
         </ref>
         <ref id="d903e576a1310">
            <mixed-citation id="d903e580" publication-type="other">
Clark, J. M. 1986. Phylogenetic relationships of the Crocodylomorph Ar-
chosaurs. Unpublished Ph.D. dissertation, University of Chicago,
Chicago, Illinois, 556 pp.</mixed-citation>
         </ref>
         <ref id="d903e593a1310">
            <mixed-citation id="d903e597" publication-type="other">
Clark, J. M. 1994. Patterns of evolution in Mesozoic Crocodyliformes; pp.
84-97 in N. C. Fraser and H.-D. Sues (eds.), In the Shadow of the
Dinosaurs. Early Mesozoic Tetrapods, Cambridge University Press,
Cambridge, U.K.</mixed-citation>
         </ref>
         <ref id="d903e613a1310">
            <mixed-citation id="d903e617" publication-type="other">
Colbert, E. H. 1946. Sebecus , representative of a peculiar suborder of fos-
sil Crocodilia from Patagonia. Bulletin of the American Museum of
Natural History 87:217-270.</mixed-citation>
         </ref>
         <ref id="d903e630a1310">
            <mixed-citation id="d903e634" publication-type="other">
Company, J., X. Pereda Suberbiola, J. I. Ruiz-Omeñaca, and A. D. Bus-
calioni. 2005. A new species of Doratodon (Crocodyliformes: Zipho-
suchia) from the Late Cretaceous of Spain. Journal of Vertebrate
Paleontology 25:343-353.</mixed-citation>
         </ref>
         <ref id="d903e650a1310">
            <mixed-citation id="d903e654" publication-type="other">
Dilkes, D. W. 1999. Appendicular myology of the hadrosaurian dinosaur
Maiasaura peeblesorum from the Late Cretaceous (Campanian) of
Montana. Transactions of the Royal Society of Edimburgh: Earth
Sciences 90:87-125.</mixed-citation>
         </ref>
         <ref id="d903e671a1310">
            <mixed-citation id="d903e675" publication-type="other">
Farlow, J. O., G. R. Hurlburt, R. M. Elsey, A. R. C. Britton, and W.
J. Langston. 2005. Femoral dimensions and body size of Alligator
mississippiensis: estimating the size of extinct mesoeucrocodylians.
Journal of Vertebrate Paleontology 25:354-369.</mixed-citation>
         </ref>
         <ref id="d903e691a1310">
            <mixed-citation id="d903e695" publication-type="other">
Feruglio, E. (ed.) 1949. Descripción Geológica de la Patagonia 2. Im-
prenta Coni, Buenos Aires, 349 pp.</mixed-citation>
         </ref>
         <ref id="d903e705a1310">
            <mixed-citation id="d903e709" publication-type="other">
Fiorelli, L. E. 2005. Nuevos Restos de Notosuchus terrestris Woodward,
1896 (Crocodyliformes: Mesoeucrocodylia) del Cretácico Superior
(Santoniano) de la Provincia de Neuquén, Patagonia, Argentina. Li-
cenciatura Thesis, Universidad Nacional de Córdoba, Córdoba, Ar-
gentina, 80 pp.</mixed-citation>
         </ref>
         <ref id="d903e728a1310">
            <mixed-citation id="d903e732" publication-type="other">
Frey, E. 1988. Das Tragsystem der Krocodile—eine biomechanische und
phylogenetische Analyse. Stuttgarter Beitrage zur Naturkunde (Se-
rie A) 426:1-60.</mixed-citation>
         </ref>
         <ref id="d903e745a1310">
            <mixed-citation id="d903e749" publication-type="other">
Gasparini, Z. 1971. Los Notosuchia del Cretácico de América del Sur
como un nuevo Infraorden de los Mesosuchia (Crocodilia). Amegh-
iniana 8:83-103.</mixed-citation>
         </ref>
         <ref id="d903e762a1310">
            <mixed-citation id="d903e766" publication-type="other">
Gasparini, Z. 1972. Los Sebecosuchia (Crocodilia) del Territorio Ar-
gentino. Consideraciones sobre su "status" taxonómico. Ameghini-
ana 9:23-34.</mixed-citation>
         </ref>
         <ref id="d903e780a1310">
            <mixed-citation id="d903e784" publication-type="other">
Gasparini, Z. 1984. New Tertiary Sebecosuchia (Crocodylia: Mesosuchia)
from Argentina. Journal of Vertebrate Paleontology 4:85-95.</mixed-citation>
         </ref>
         <ref id="d903e794a1310">
            <mixed-citation id="d903e798" publication-type="other">
Gasparini, Z. В. 1996. Biogeographic evolution of the South Ameri-
can crocodilians. Münchner Geowissenschaftliche Abhandlungen
30:159-184.</mixed-citation>
         </ref>
         <ref id="d903e811a1310">
            <mixed-citation id="d903e815" publication-type="other">
Gasparini, Z., M. Fernandez, and J. Powell. 1993. New Tertiary sebeco-
suchians (Crocodylomorpha) from South America: phylogenetic im-
plications. Historical Biology 7:1-19.</mixed-citation>
         </ref>
         <ref id="d903e828a1310">
            <mixed-citation id="d903e832" publication-type="other">
Gasparini, Z., D. Pol, and L. A. Spalletti. 2006. An unusual marine
crocodyliform from the Jurassic-Cretaceous boundary of Patagonia.
Science 311:70-73.</mixed-citation>
         </ref>
         <ref id="d903e845a1310">
            <mixed-citation id="d903e849" publication-type="other">
Georgi, J. A., and D. W. Krause. 2010. Postcranial axial skeleton of Simo-
suchus clarki (Crocodyliformes: Notosuchia) from the Late Creta-
ceous of Madagascar. Society of Vertebrate Paleontology Memoir
10:99-121.</mixed-citation>
         </ref>
         <ref id="d903e865a1310">
            <mixed-citation id="d903e869" publication-type="other">
Goloboff, P. A., J. S. Farris, and K. C. Nixon. 2008a. TNT, a free program
for phylogenetic analysis. Cladistics 24:774-786.</mixed-citation>
         </ref>
         <ref id="d903e880a1310">
            <mixed-citation id="d903e884" publication-type="other">
Goloboff, P. A., J. S. Farris, and K. C. Nixon. 2008b. TNT: Tree Analy-
sis Using New Technologies. Program and documentation available
from the authors and at http://www.zmuc.dk/public/phylogeny. Ac-
cessed May 2011.</mixed-citation>
         </ref>
         <ref id="d903e900a1310">
            <mixed-citation id="d903e904" publication-type="other">
Gomani, E. M. 1997. A crocodyliform from the Early Cretaceous di-
nosaur beds, northern Malawi. Journal of Vertebrate Paleontology
17:280-294.</mixed-citation>
         </ref>
         <ref id="d903e917a1310">
            <mixed-citation id="d903e921" publication-type="other">
Hay, O. P. 1930. Second Bibliography and Catalogue of the Fossil Ver-
tebrata of North America 2. Carnegie Institute, Washington, D.C.,
1074 pp.</mixed-citation>
         </ref>
         <ref id="d903e934a1310">
            <mixed-citation id="d903e938" publication-type="other">
Hutchinson, J. R. 2001a. The evolution of femoral osteology and soft tis-
sues on the line to extant birds (Neornithes). Zoological Journal of
the Linnean Society 131:169-197.</mixed-citation>
         </ref>
         <ref id="d903e951a1310">
            <mixed-citation id="d903e955" publication-type="other">
Hutchinson, J. R. 2001b. The evolution of pelvic osteology and soft tissues
on the line to extant birds (Neornithes). Zoological Journal of the
Linnean Society 131:123-168.</mixed-citation>
         </ref>
         <ref id="d903e968a1310">
            <mixed-citation id="d903e972" publication-type="other">
Hutchinson, J. R. 2002. The evolution of hindlimb tendons and muscles
on the line to crown-group birds. Comparative Biochemistry and
Physiology Part A 133:1051-1086.</mixed-citation>
         </ref>
         <ref id="d903e986a1310">
            <mixed-citation id="d903e990" publication-type="other">
Langston, W. J. 1965. Fossil crocodilians from Colombia and the Ceno-
zoic history of the Crocodilia in South America. University of Cali-
fornia Publications in Geological Sciences 52:1-98.</mixed-citation>
         </ref>
         <ref id="d903e1003a1310">
            <mixed-citation id="d903e1007" publication-type="other">
Langston, W. J., and Z. B. Gasparini. 2007. Crocodilians, Gryposuchus ,
and the South American gavials; pp. 113-154 in R. Kay, R. Mad-
den, R. Cifelli, and J. Flynn (eds.), Vertebrate Paleontology in the
Neotropics. The Miocene Fauna of La Venta, Colombia. Smithso-
nian Institution Press, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d903e1026a1310">
            <mixed-citation id="d903e1030" publication-type="other">
Larsson, H. C. E., and H.-D. Sues. 2007. Cranial osteology and phylo-
genetic relationships of Hamadasuchus rebouli (Crocodyliformes:
Mesoeucrocodylia) from the Cretaceous of Morocco. Zoological
Journal of the Linnean Society 149:533-567.</mixed-citation>
         </ref>
         <ref id="d903e1046a1310">
            <mixed-citation id="d903e1050" publication-type="other">
Legasa, O., A. D. Buscalioni, and Z. Gasparini. 1994. The serrated
teeth of Sebecus and the iberoccitanian crocodile, a morphologi-
cal and ultrastructural comparison. Studia Geologica Salmanticen-
sia 29:127-144.</mixed-citation>
         </ref>
         <ref id="d903e1066a1310">
            <mixed-citation id="d903e1070" publication-type="other">
Meers, M. В. 2003. Crocodylian forelimb musculature and its relevance
to Archosauria. The Anatomical Record Part A 274A:891-916.</mixed-citation>
         </ref>
         <ref id="d903e1080a1310">
            <mixed-citation id="d903e1084" publication-type="other">
Moinar, R. E. 2010. A new reconstruction of the skull of Sebecus icae-
orhinus (Crocodyliformes: Sebecosuchia) from the Eocene of Ar-
gentina. Brazilian Geographical Journal: Geosciences and Humani-
ties Research Medium 1:314-330.</mixed-citation>
         </ref>
         <ref id="d903e1101a1310">
            <mixed-citation id="d903e1105" publication-type="other">
Müller, G. В., and P. Alberch. 1990. Ontogeny of the limb skeleton in Al-
ligator mississippiensis: developmental invariance and change in the
evolution of archosaur limbs. Journal of Morphology 203:151-164.</mixed-citation>
         </ref>
         <ref id="d903e1118a1310">
            <mixed-citation id="d903e1122" publication-type="other">
Nascimento, P. M., and H. Zaher. 2010. A new species of Baurusuchus
(Crocodyliformes, Mesoeucrocodylia) from the Upper Cretaceous
of Brazil, with the first complete postcranial skeleton described for
the family Baurusuchidae. Papéis Avulsos de Zoologia 50:323-361.</mixed-citation>
         </ref>
         <ref id="d903e1138a1310">
            <mixed-citation id="d903e1142" publication-type="other">
Novas, F. E. 1993. New information on the systematics and postcra-
nial skeleton of Herrerasaurus ischigualastensis (Theropoda: Her-
rerasauridae) from the Ischigualasto Formation (Upper Triassic) of
Argentina. Journal of Vertebrate Paleontology 13:400-423.</mixed-citation>
         </ref>
         <ref id="d903e1158a1310">
            <mixed-citation id="d903e1162" publication-type="other">
Ortega, F. 2004. Historia evolutiva de los cocodrilos Mesoeucrocodylia.
Ph.D. dissertation, Universidad Autónoma de Madrid, Madrid, 350
pp.</mixed-citation>
         </ref>
         <ref id="d903e1175a1310">
            <mixed-citation id="d903e1179" publication-type="other">
Ortega, F., A. D. Buscalioni, and Z. B. Gasparini. 1996. Reinterpreta-
tion and new denomination of Atacisaurus crassiproratus (middle
Eocene; Issel, France) as cf. Iberosuchus (Crocodylomorpha: Meta-
suchia). Geobios 29:353-364.</mixed-citation>
         </ref>
         <ref id="d903e1195a1310">
            <mixed-citation id="d903e1199" publication-type="other">
Ortega, F., Z. B. Gasparini, A. D. Buscalioni, and J. О. Calvo.
2000. A new species of Araripesuchus (Crocodylomorpha, Mesoeu-
crocodylia) from the Lower Cretaceous of Patagonia (Argentina).
Journal of Vertebrate Paleontology 20:57-76.</mixed-citation>
         </ref>
         <ref id="d903e1216a1310">
            <mixed-citation id="d903e1220" publication-type="other">
Paolillo, A., and O. J. Linares. 2007. Nuevos cocodrilos Sebecosuchia del
Cenozoico Suramericano (Mesosuchia: Crocodylia). Paleobiologia
Neotropical 3:1-25.</mixed-citation>
         </ref>
         <ref id="d903e1233a1310">
            <mixed-citation id="d903e1237" publication-type="other">
Parrish, J. M. 1986. Locomotor adaptations in the hindlimb and pelvis of
the Thecodontia. Hunteria 1:1-35.</mixed-citation>
         </ref>
         <ref id="d903e1247a1310">
            <mixed-citation id="d903e1251" publication-type="other">
Paula-Couto, C. 1970. Evolução de comunidades, modificações
faunísticas e integrações bióticas dos vertebrados cenozóicos do
Brasil. Congreso Latinoamericano de Zoología, Actas 4:907-930.</mixed-citation>
         </ref>
         <ref id="d903e1264a1310">
            <mixed-citation id="d903e1268" publication-type="other">
Piatnitzky, A. 1931. Observaciones estratigráficas sobre las tobas con
mamíferos del Terciario inferior en el valle del río Chico (Chubut).
Boletín de Informaciones Petroleras 85:617-634.</mixed-citation>
         </ref>
         <ref id="d903e1281a1310">
            <mixed-citation id="d903e1285" publication-type="other">
Pol, D. 2003. New remains of Sphagesaurus huenei (Crocodylomorpha:
Mesoeucrocodylia) from the Late Cretaceous of Brazil. Journal of
Vertebrate Paleontology 23:817-831.</mixed-citation>
         </ref>
         <ref id="d903e1298a1310">
            <mixed-citation id="d903e1302" publication-type="other">
Pol, D. 2005. Postcranial remains of Notosuchus terrestris Woodward (Ar-
chosauria: Crocodyliformes) from the Upper Cretaceous of Patago-
nia, Argentina. Ameghiniana 42:21-38.</mixed-citation>
         </ref>
         <ref id="d903e1316a1310">
            <mixed-citation id="d903e1320" publication-type="other">
Pol, D., and S. Apesteguia. 2005. New Araripesuchus remains from
the early Late Cretaceous (Cenomanian-Turonian) of Patagonia.
American Museum Novitates 3490:1-38.</mixed-citation>
         </ref>
         <ref id="d903e1333a1310">
            <mixed-citation id="d903e1337" publication-type="other">
Pol, D., and J. Powell. 2011. A new basal mesoeucrocodylian from the Rio
Loro Formation (Paleocene) of northwestern Argentina. Zoological
Journal of the Linnean Society 163:S7-S36.</mixed-citation>
         </ref>
         <ref id="d903e1350a1310">
            <mixed-citation id="d903e1354" publication-type="other">
Pol, D., A. H. Turner, and M. A. Norell. 2009. Morphology of the Late
Cretaceous crocodylomorph Shamosuchus djadochtaensis and a dis-
cussion of neosuchian phylogeny as related to the origin of Eu-
suchia. Bulletin of American Museum of Natural History 324:1-103.</mixed-citation>
         </ref>
         <ref id="d903e1370a1310">
            <mixed-citation id="d903e1374" publication-type="other">
Pol, D., S.-A. Ji, J. M. Clark, and L. M. Chiappe. 2004. Basal crocodyli-
forms from the Lower Cretaceous Tugulu Group (Xinjiang, China),
and the phylogenetic position of Edentosuchus. Cretaceous Re-
search 25:603-622.</mixed-citation>
         </ref>
         <ref id="d903e1390a1310">
            <mixed-citation id="d903e1394" publication-type="other">
Prasad, G. V. R., and F. d. Broin. 2002. Late Cretaceous crocodile remains
from Naskal (India): comparisons and biogeographic affinities. An-
nales de Paléontologie 88:19-71.</mixed-citation>
         </ref>
         <ref id="d903e1407a1310">
            <mixed-citation id="d903e1411" publication-type="other">
Raigemborn, M. S., J. M. Krause, E. S. Bellosi, and S. D. Matheos. 2010.
Redefinición estratigráfica dek Grupo Río Chico (Paleógeno infe-
rior), en el norte de la cuenca del Golfo San Jorge, Chubut, Ar-
gentina. Revista de la Asociación Geológica Argentina 67:239-256.</mixed-citation>
         </ref>
         <ref id="d903e1428a1310">
            <mixed-citation id="d903e1432" publication-type="other">
Rauhe, M. 1995. Die Lebensweise und Ökologie der Geiseltal-
Krokodilier-Abschied von traditionellen Lehrmeinungen.
Hallesches Jahrbuch für Geowissenschaften 17:65-80.</mixed-citation>
         </ref>
         <ref id="d903e1445a1310">
            <mixed-citation id="d903e1449" publication-type="other">
Riff, D. 2007. Anatomia apendicular de Stratiotosuchus maxhechti (Bau-
rusuchidae , Cretáceo Superior do Brasil) e análise filogenética dos
Mesoeucrocodylia. Ph.D. dissertation, Universidade Federal do Rio
de Janeiro, Rio de Janeiro, 395 pp.</mixed-citation>
         </ref>
         <ref id="d903e1465a1310">
            <mixed-citation id="d903e1469" publication-type="other">
Riff, D., and A. W. A. Kellner. 2001. On the dentition of Baurusuchus
pachecoi Price (Crocodyliformes, Metasuchia) from the Upper Cre-
taceous of Brazil. Boletim do Museu Nacional, Nova Série, Geologia
59:1-15.</mixed-citation>
         </ref>
         <ref id="d903e1485a1310">
            <mixed-citation id="d903e1489" publication-type="other">
Riff, D., and A. W. A. Kellner. 2011. Baurusuchids crocodyliforms as
theropod mimics: clues from the appendicular morphology of Stra-
tiotosuchus maxhechti (Upper Cretaceous of Brazil). Zoological
Journal of the Linnean Society 163:S37-S56.</mixed-citation>
         </ref>
         <ref id="d903e1505a1310">
            <mixed-citation id="d903e1509" publication-type="other">
Romer, A. S. 1923. Crocodilian pelvic muscles and their avian and reptil-
ian homologues. Bulletin of the American Museum of Natural His-
tory 48:533-551.</mixed-citation>
         </ref>
         <ref id="d903e1522a1310">
            <mixed-citation id="d903e1526" publication-type="other">
Rossmann, T. 2000. Studien an känozoischen Krokodilen: 5. Biomech-
anische Untersuchung am poskranialen Skelett des paläogenen
Krokodils Pristichampsus rollinatii (Eusuchia: Pristichampsidae).
Neues Jahrbuch für Geologie und Paläontologie, Abhandlungen
217:289-330.</mixed-citation>
         </ref>
         <ref id="d903e1546a1310">
            <mixed-citation id="d903e1550" publication-type="other">
Rusconi, С. 1933. Sobre reptiles Cretaceous del Uruguay (Uruguay suchus
aznarezi, n. g. n. sp) y sus relaciones con los notosúquidos de Patag-
onia. Boletín Instituto de Geología y Perforaciones Montevideo
Uruguay 19:1-64.</mixed-citation>
         </ref>
         <ref id="d903e1566a1310">
            <mixed-citation id="d903e1570" publication-type="other">
Schaeffer, B. 1947. An Eocene serranid from Patagonia. American Mu-
seum Novitates 1331:1-9.</mixed-citation>
         </ref>
         <ref id="d903e1580a1310">
            <mixed-citation id="d903e1584" publication-type="other">
Sereno, Р. С. 1991. Basal archosaurs: phylogenetic relationships and
functional implications. Society of Vertebrate Paleontology Mem-
oir 2:1-53.</mixed-citation>
         </ref>
         <ref id="d903e1597a1310">
            <mixed-citation id="d903e1601" publication-type="other">
Sereno, P. С., and H. С. E. Larsson. 2009. Cretaceous crocodyliforms
from the Sahara. ZooKeys 28:1-143.</mixed-citation>
         </ref>
         <ref id="d903e1611a1310">
            <mixed-citation id="d903e1615" publication-type="other">
Sereno, P. С., H. С. E. Larsson, C. A. Sidor, and B. Gado. 2001. The giant
crocodyliform Sarcosuchus from the Cretaceous of Africa. Science
294:1516-1519.</mixed-citation>
         </ref>
         <ref id="d903e1628a1310">
            <mixed-citation id="d903e1632" publication-type="other">
Sereno, P. С., C. A. Sidor, H. С. E. Larsson, and B. Gado. 2003. A new
notosuchian from the Early Cretaceous of Niger. Journal of Verte-
brate Paleontology 23:477-482.</mixed-citation>
         </ref>
         <ref id="d903e1646a1310">
            <mixed-citation id="d903e1650" publication-type="other">
Sertich, J. J. W., and J. R. Groenke. 2010. Appendicular Skeleton of Simo-
suchus clarki (Crocodyliformes: Notosuchia) from the Late Creta-
ceous of Madagascar. Society of Vertebrate Paleontology Memoir
10:122-153.</mixed-citation>
         </ref>
         <ref id="d903e1666a1310">
            <mixed-citation id="d903e1670" publication-type="other">
Simpson, G. G. 1935. Occurrence and relationships of the Río
Chico fauna of Patagonia. American Museum Novitates 818:
1-21.</mixed-citation>
         </ref>
         <ref id="d903e1683a1310">
            <mixed-citation id="d903e1687" publication-type="other">
Simpson, G. G. 1937. New reptiles from the Eocene of South America.
American Museum Novitates 927:1-3.</mixed-citation>
         </ref>
         <ref id="d903e1697a1310">
            <mixed-citation id="d903e1701" publication-type="other">
Soto, M., D. Pol, and D. Perea. 2011. A new specimen of Uruguaysuchus
aznarezi (Crocodyliformes: Notosuchia) from the Cretaceous of
Uruguay and its phylogenetic relationships. Zoological Journal of
the Linnean Society 163:S173-S198.</mixed-citation>
         </ref>
         <ref id="d903e1717a1310">
            <mixed-citation id="d903e1721" publication-type="other">
Turner, A. H. 2006. Osteology and phylogeny of a new species of
Araripesuchus (Crocodyliformes: Mesoeucrocodylia) from the Late
Cretaceous of Madagascar. Historical Biology 18:255-369.</mixed-citation>
         </ref>
         <ref id="d903e1734a1310">
            <mixed-citation id="d903e1738" publication-type="other">
Turner, A. H., and G. A. Buckley. 2008. Mahajangasuchus insignis
(Crocodyliformes: Mesoeucrocodylia) cranial anatomy and new
data on the origin of the eusuchian-style palate. Journal of Verte-
brate Paleontology 28:382-408.</mixed-citation>
         </ref>
         <ref id="d903e1755a1310">
            <mixed-citation id="d903e1759" publication-type="other">
Turner, A. H., and J. O. Calvo. 2005. A new sebecosuchian crocodyli-
form from the Late Cretaceous of Patagonia. Journal of Vertebrate
Paleontology 25:87-98.</mixed-citation>
         </ref>
         <ref id="d903e1772a1310">
            <mixed-citation id="d903e1776" publication-type="other">
Turner, A. H., and J. J. W. Sertich. 2010. Phylogenetic history of Simo-
suchus clarki (Crocodyliformes: Notosuchia) from the Late Creta-
ceous of Madagascar. Society of Vertebrate Paleontology Memoir
10:177-236.</mixed-citation>
         </ref>
         <ref id="d903e1792a1310">
            <mixed-citation id="d903e1796" publication-type="other">
Walker, A. D. 1970. A Revision of the Jurassic Reptile Hallopus vic-
tor (Marsh), with remarks on the classification of crocodiles. Philo-
sophical Transactions of the Royal Society of London, Series В
257:323-372.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
