<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jmedethics</journal-id>
         <journal-id journal-id-type="jstor">j50000494</journal-id>
         <journal-title-group>
            <journal-title>Journal of Medical Ethics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>BMJ Publishing Group</publisher-name>
         </publisher>
         <issn pub-type="ppub">03066800</issn>
         <issn pub-type="epub">14734257</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">27719145</article-id>
         <article-id pub-id-type="pub-doi">10.1136/jme.2002.001933</article-id>
         <article-categories>
            <subj-group>
               <subject>Global Research Ethics</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ethical Review of Health Research: A Perspective from Developing Country Researchers</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>A. A.</given-names>
                  <surname>Hyder</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>S. A.</given-names>
                  <surname>Wali</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>A. N.</given-names>
                  <surname>Khan</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>N. B.</given-names>
                  <surname>Teoh</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>N. E.</given-names>
                  <surname>Kass</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>L.</given-names>
                  <surname>Dawson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>2</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">30</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i27719129</issue-id>
         <fpage>68</fpage>
         <lpage>72</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 BMJ Publishing Group &amp; Institute of Medical Ethics</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/27719145"/>
         <abstract>
            <p>Background: Increasing collaboration between industrialised and developing countries in human research studies has led to concerns regarding the potential exploitation of resource deprived countries. This study, commissioned by the former National Bioethics Advisory Commission of the United States, surveyed developing country researchers about their concerns and opinions regarding ethical review processes and the performance of developing country and US international review boards (IRBs). Methods: Contact lists from four international organisations were used to identify and survey 670 health researchers in developing countries. A questionnaire with 169 questions explored issues of IRB review, informed consent, and recommendations. Results: The majority of the developing country researchers were middle aged males who were physicians and were employed by educational institutions, carrying out research on part time basis. Forty four percent of the respondents reported that their studies were not reviewed by a developing country IRB or Ministry of Health and one third of these studies were funded by the US. During the review process issues such as the need for local language consent forms and letters for approval, and confidentiality protection of participants were raised by US IRBs in significantly higher proportions than by host country IRBs. Conclusion: This survey indicates the need for the ethical review of collaborative research in both US and host countries. It also reflects a desire for focused capacity development in supporting ethical review of research.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d799e226a1310">
            <label>1</label>
            <mixed-citation id="d799e233" publication-type="other">
Angell M. Ethics of clinical research in third world. N Engl J Med
1997;337:847–9.</mixed-citation>
         </ref>
         <ref id="d799e243a1310">
            <label>2</label>
            <mixed-citation id="d799e250" publication-type="other">
Cleaton-Jones PE. An ethical dilemma: availability of antiretroviral therapy
after clinical trials with HIV infected patients are ended. BMJ
1997;314:887–91.</mixed-citation>
         </ref>
         <ref id="d799e263a1310">
            <label>3</label>
            <mixed-citation id="d799e270" publication-type="other">
Barry M. Ethical considerations of human investigation in developing
countries: the AIDS dilemma. N Engt J Med 1988,319:1083.</mixed-citation>
         </ref>
         <ref id="d799e280a1310">
            <label>4</label>
            <mixed-citation id="d799e287" publication-type="other">
Angell M. Ethical imperialism-ethics in international collaborative clinical
research. N Engl J Med 1988;319:1081–3.</mixed-citation>
         </ref>
         <ref id="d799e298a1310">
            <label>5</label>
            <mixed-citation id="d799e305" publication-type="other">
Farmer P. Can transnational research be ethical in developing world? Lancet
2000;360:1266.</mixed-citation>
         </ref>
         <ref id="d799e315a1310">
            <label>6</label>
            <mixed-citation id="d799e322" publication-type="other">
Porter L, Gostin L. The application of United States protection of human
subjects regulations and ethical principles to United States funded or
conducted HIV-related research in foreign countries, (Georgetown University,
1994, unpublished).</mixed-citation>
         </ref>
         <ref id="d799e338a1310">
            <label>7</label>
            <mixed-citation id="d799e345" publication-type="other">
Wichman A, Smith J, Mills D, et al. Collaborative research involving human
subjects: a survey of researchers using international single project assurances.
IRB 1997;19:1–6.</mixed-citation>
         </ref>
         <ref id="d799e358a1310">
            <label>8</label>
            <mixed-citation id="d799e365" publication-type="other">
Phanuphak P. Ethical issues in studies in Thailand of the vertical transmission
of HIV. N Engl J Med 1998,338:834–5.</mixed-citation>
         </ref>
         <ref id="d799e375a1310">
            <label>9</label>
            <mixed-citation id="d799e382" publication-type="other">
Bayer R. The debate over maternal-fetal HIV transmission prevention trials in
Africa, Asia, and the Caribbean: Racist exploitation or exploitation of racism.
Am J Public Health 1998;88:567–70.</mixed-citation>
         </ref>
         <ref id="d799e395a1310">
            <label>10</label>
            <mixed-citation id="d799e402" publication-type="other">
Annas GJ, Grodin MA. Human rights and maternal-fetal HIV transmission
prevention trials in Africa. Am J Public Health 1998,88:560–3.</mixed-citation>
         </ref>
         <ref id="d799e413a1310">
            <label>11</label>
            <mixed-citation id="d799e420" publication-type="other">
Palca J. African AIDS: Whose research rules? Science 1990,250:199–201.</mixed-citation>
         </ref>
         <ref id="d799e427a1310">
            <label>12</label>
            <mixed-citation id="d799e434" publication-type="other">
Levine RJ. Informed consent: some challenges to the universal validity of the
Western model. Law Med Health Care 1991,19:207–13.</mixed-citation>
         </ref>
         <ref id="d799e444a1310">
            <label>13</label>
            <mixed-citation id="d799e451" publication-type="other">
Wilmhurst P. Scientific imperialism. BMJ 1997,314:840.</mixed-citation>
         </ref>
         <ref id="d799e458a1310">
            <label>14</label>
            <mixed-citation id="d799e465" publication-type="other">
The Council for International Organizations of Medical Sciences (CIOMS).
International ethical guidelines for biomédical research involving human
subjects. Geneva, Switzerland: The Council for International Organizations of
Medical Sciences (CIOMS), 2002.</mixed-citation>
         </ref>
         <ref id="d799e481a1310">
            <label>15</label>
            <mixed-citation id="d799e488" publication-type="other">
Nuffeild Council on Bioethics. The ethics of research related to health care in
developing countries. London, UK: Nuffield Council on Bioethics, 2002.</mixed-citation>
         </ref>
         <ref id="d799e498a1310">
            <label>16</label>
            <mixed-citation id="d799e505" publication-type="other">
National Bioethics Advisory Commission, Rockville MD, Ethical and Policy
Issues in International Research, 2000 Available from http://
www.georgetown.edu/research/nrcbl/nbac/pubs.html.</mixed-citation>
         </ref>
         <ref id="d799e519a1310">
            <label>17</label>
            <mixed-citation id="d799e526" publication-type="other">
World Health Organization (WHO) Ad Hoc Committee on Health Research
Relating to Future Intervention Options. Investing in health research and
development. Geneva, Switzerland: World Health Organization (WHO),
1996.</mixed-citation>
         </ref>
         <ref id="d799e542a1310">
            <label>18</label>
            <mixed-citation id="d799e549" publication-type="other">
Gostin L. Ethical principles for the conduct of human subject research:
population-based research and ethics. Law Med Health Care
1991,19:191–201.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
