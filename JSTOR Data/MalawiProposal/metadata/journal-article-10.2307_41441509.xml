<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">philosophy</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100321</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Philosophy</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00318191</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1469817X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41441509</article-id>
         <title-group>
            <article-title>Epistemic restraint and the vice of curiosity</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>NEIL C.</given-names>
                  <surname>MANSON</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">87</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">340</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40070318</issue-id>
         <fpage>239</fpage>
         <lpage>259</lpage>
         <permissions>
            <copyright-statement>© The Royal Institute of Philosophy 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:title="an external site"
                   xlink:href="http://dx.doi.org/10.1017/S0031819112000046"/>
         <abstract>
            <p>In recent years there has been wide-ranging discussion of epistemic virtues. Given the value and importance of acquiring knowledge this discussion has tended to focus upon those traits that are relevant to the acquisition of knowledge. This acquisitionist focus ignores or downplays the importance of epistemic restraint: refraining from seeking knowledge. In contrast, in many periods of history, curiosity was viewed as a vice. By drawing upon critiques of curiositas in Middle Platonism and Early Christian philosophy, we gain useful insights into the value and importance of epistemic restraint. The historical discussion paves the way for a clarification of epistemic restraint, one that distinguishes the morally relevant features of epistemic process, content, purpose, and context. Epistemic restraint is identified as an important virtue where our epistemic pursuits pose risks and burdens, where such pursuits have opportunity costs, where they are pursued for vicious purposes. But it is in the social realm where epistemic restraint has most purchase: epistemic restraint is important both because privacy is important and because being trusted are important. Finally, some suggestions are offered as to why epistemic restraint has not received the contemporary attention that it deserves.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d46e116a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d46e123" publication-type="other">
'An epistemic virtue is an excellence of character instru-
mental to the acquisition of true belief and knowledge' Jonathan Dancy in A
Companion to Epistemology, ed. By J. Dancy, E. Sosa and M. Steup. (Oxford
Blackwell 2010), 343;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e138" publication-type="other">
'An epistemic virtue is a special kind of intellectual
virtue, one that is conducive to producing epistemically valuable states of
affairs' Richard Foley, Intellectual Trust in Ourselves and Others,
(Cambridge: Cambridge University Press, 2001), 224.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e154a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d46e161" publication-type="other">
James Montmarquet, Epistemic Virtue and Doxastic Responsibility,
(Lanham: Rowman and Littlefield, 1993), 223.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e171a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d46e178" publication-type="other">
Christopher Hookway, 'Epistemic Akrasia and Epistemic Virtue' in
Virtue Epistemology: Essays on Epistemic Virtue and Responsibility by
Abrol Fairweather and Linda Zagzebski (New York: Oxford Universty
Press, 2001), 178-199, 195.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e194a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d46e201" publication-type="other">
John Heil, 'Doxastic Incontinence'
Mind 93 (1984), 56-70;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e210" publication-type="other">
David Owens, 'Epistemic Akrasia', The Monist 85
(2002), 381-397.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e221a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d46e228" publication-type="other">
Lorraine Code, Epistemic Responsibility (Hanover: University Press of
New England and Brown University Press, 1987), 55.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e238a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d46e245" publication-type="other">
P.G. Walsh 'The Rights and Wrongs of
Curiosity (Plutarch to Augustine)', Greece &amp; Rome 35 (1988), 73-85.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e255a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d46e264" publication-type="other">
John Calvin's commentary on Genesis 3:5 'Eve erred in not regulating
the measure of her knowledge by the will of God [. . .] we all daily suffer
under the same disease, because we desire to know more than is right, and more
than God allows.'</mixed-citation>
            </p>
         </fn>
         <fn id="d46e280a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d46e287" publication-type="other">
Op. cit. note 7, 81.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e294a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d46e301" publication-type="other">
Augustine Confessions and Enchiridion trans. Albert Cook Outler
(Philadelphia: Westminster Press, 1955), Book X, Chanter xxxv.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e311a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d46e318" publication-type="other">
Op. cit. note 10, 234.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e326a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d46e333" publication-type="other">
http://www.new
advent.org/summa/3167.htm</mixed-citation>
            </p>
         </fn>
         <fn id="d46e343a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d46e352" publication-type="other">
Moralia, Essay 39, Book VI.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e358" publication-type="other">
W. Goodwin
'An over busy inquisitiveness into things impertinent' in Plutarch's
Miscellanies and Essay Vol. 2 (Boston: Little Brown, 1 878).</mixed-citation>
            </p>
         </fn>
         <fn id="d46e371a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d46e378" publication-type="other">
Op. cit. note 13, 424.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e385a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d46e392" publication-type="other">
Op. cit. note 13, 424.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e399a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d46e406" publication-type="other">
Op. cit. note 13, 431.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e413a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d46e420" publication-type="other">
Op. cit. note 13, 437.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e426" publication-type="other">
Carlin A. Barton The
Sorrows of the Ancient Romans :The Gladiator and the Monster (Princeton:
Princeton University Press, 1995), 86.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e440a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d46e447" publication-type="other">
Op. cit. note 13, 442.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e454a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d46e461" publication-type="other">
Op. cit. note 13, 435.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e468a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d46e475" publication-type="other">
Vincent Hunink, 'Plutarch and Apuleius', in: Lukas de Blois, Jeroen
Bons, Ton Kessels, Dirk M. Schenkeveld (eds.), The statesman in Plutarch's
works. Vol. I: Plutarch's statesman and his aftermath: political philosophical,
and literary aspects. (Leiden: Brill, 2004), 251-60.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e491a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d46e498" publication-type="other">
Joseph G. DeFilippo, 'Curiositas and the
Platonism of Apuleius' Golden Ass', The American Journal of Philology
111 (1990), 471-492, 476.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e510" publication-type="other">
Hunink (op. cit. note 20)</mixed-citation>
            </p>
         </fn>
         <fn id="d46e517a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d46e524" publication-type="other">
Jonathan P. Masinick and Hualiang Teng, An analysis on the impact
of rubbernecking on urban freeway traffic, http://www.gmupolicy.net/its/
pdfweb/Impact%20of%20Rubbernecking.pdf;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e536" publication-type="other">
Victor L. Knoop,
Henk J. van Zuylen and Serge P. Hoogendoorn, 'Microscopic Traffic
Behaviour near Incidents' Transportation and Traffic Theory (2009), 75-97.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e549a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d46e556" publication-type="other">
(op. cit. note 13, 429).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d46e562" publication-type="other">
Lawrence K. Altman, Who Goes
First? The Story of Self -Experimentation in Medicine (Berkeley &amp; Los
Angeles: University of California Press, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d46e576a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d46e583" publication-type="other">
http: //www. economist,
com/node/8023316).</mixed-citation>
            </p>
         </fn>
         <fn id="d46e593a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d46e600" publication-type="other">
Linda Zagzebski, Virtues of the Mind (Cambridge University Press,
1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d46e610a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d46e617" publication-type="other">
Sheldon H. Harris, Factories of Death: Japanese Biological Warfare
1932-45 and the American Cover-Up. (London: Routledge, 1994).</mixed-citation>
            </p>
         </fn>
         <fn id="d46e627a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d46e634" publication-type="other">
Thomas S. Hibbs, 'Aquinas Virtue and Recent Epistemology' The
Review of Metaphysics 52 (1999), 573-594, 589.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e644a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d46e651" publication-type="other">
Op. cit. note 30.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e658a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d46e665" publication-type="other">
Charles Fried, 'Privacy', Yale Law Journal 77 (1968), 475-493.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e673a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d46e680" publication-type="other">
James Rachels, 'Why Privacy is Important' Philosophy and Public
Affairs 4 (1975), 315-322.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e690a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d46e697" publication-type="other">
'Privacy: its constitution and vicissitudes', Law and Contemporary
Problem 31 (1966), 281-306.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e707a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d46e714" publication-type="other">
Op. cit. note 13, 435.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e721a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d46e728" publication-type="other">
Julia Driver, 'The Virtues of Ignorance', Journal of Philosophy 86
(1989), 373-384.</mixed-citation>
            </p>
         </fn>
         <fn id="d46e738a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d46e745" publication-type="other">
Jean -Jacques Rousseau, Discourse on the Arts and Sciences , in
The Social Contract and Discourses by Jean-Jacques Rousseau , (1750) trans.
G.D. H. Cole (London: Dent and Sons, 1923).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
