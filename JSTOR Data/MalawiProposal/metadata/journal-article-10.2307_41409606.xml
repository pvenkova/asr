<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">poprespolrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000506</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population Research and Policy Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01675923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737829</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41409606</article-id>
         <title-group>
            <article-title>"The Luggage that isn't Theirs is Too Heavy...": Understandings of Orphan Disadvantage in Lesotho</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rachel E.</given-names>
                  <surname>Goldberg</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Susan E.</given-names>
                  <surname>Short</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40068597</issue-id>
         <fpage>67</fpage>
         <lpage>83</lpage>
         <permissions>
            <copyright-statement>© Springer Science+Business Media B. V. 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41409606"/>
         <abstract>
            <p>In Southern Africa, high adult HIV prevalence has fueled concern about the welfare of children losing parents to the epidemic. A growing body of evidence indicates that parental, particularly maternal, death is negatively associated with child outcomes. However, a better understanding of the mechanisms is needed. In addition, the way orphan disadvantage and the mechanisms giving rise to it are understood on the ground is essential for the successful translation of research into policies and programs. This study employs data from 89 in-depth interviews with caregivers and key informants in Lesotho, a setting where approximately onequarter of adults is infected with HIV, to elaborate understandings of orphan disadvantage. Our analysis focuses on two questions: (i) Do local actors perceive orphans to be disadvantaged compared to non-orphans, and if so, in what ways; and (ii) How do they explain orphans' differential disadvantage? Analyses suggest that orphans were widely perceived to be disadvantaged; respondents described this disadvantage in material as well as affective domains. Thematic analyses reveal five broad categories of explanation: poverty, love and kin connection, caregiver character, perceptions of orphans, and community norms related to orphan care. These results underscore the need for research and policy to address (i) multiple types of disadvantage, including deficits in kindness and attention; and (ii) the social embeddedness of disadvantage, recognizing that poverty, kinship, and community interact with individual attributes to shape caregiving relationships and child experiences. The findings suggest limited success for programs and policies that do not address the emotional needs of children, or that focus on child or caregiver support to the exclusion of community outreach.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d17e128a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d17e135" publication-type="other">
(UNAIDS et al. 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d17e142a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d17e149" publication-type="other">
Beegle and Krutikova (2008)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d17e155" publication-type="other">
Timaeus and Boler (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d17e162a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d17e169" publication-type="other">
Kaggwa and Hindin (2010)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d17e175" publication-type="other">
Nyamukapa et al. (2010)</mixed-citation>
            </p>
         </fn>
         <fn id="d17e182a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d17e189" publication-type="other">
(Gage 2005).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d17e205a1310">
            <mixed-citation id="d17e209" publication-type="other">
Abebe, T., &amp; Aase, A. (2007). Children, AIDS and the politics of orphan care in Ethiopia: The extended
family revisited. Social Science and Medicine, 64 , 1058-2069.</mixed-citation>
         </ref>
         <ref id="d17e219a1310">
            <mixed-citation id="d17e223" publication-type="other">
Ainsworth, M., Beegle, K., &amp; Koda, G. (2005). The impact of adult mortality and parental deaths on
schooling in Northwestern Tanzania. Journal of Development Studies, 41(3), 412-439.</mixed-citation>
         </ref>
         <ref id="d17e233a1310">
            <mixed-citation id="d17e237" publication-type="other">
Ansell, N., &amp; van Blerk, L. (2004). Children's migration as a household/family strategy: coping with
AIDS in Lesotho and Malawi. Journal of Southern African Studies, 50(3), 673-690.</mixed-citation>
         </ref>
         <ref id="d17e247a1310">
            <mixed-citation id="d17e251" publication-type="other">
Ansell, N., &amp; Young, L. (2004). Enabling households to support successful migration of AIDS orphans in
Southern Africa. AIDS Care, 76(1), 3-10.</mixed-citation>
         </ref>
         <ref id="d17e262a1310">
            <mixed-citation id="d17e266" publication-type="other">
Atwine, В., Cantor-Graae, E., &amp; Bajunirwe, F. (2005). Psychological distress among AIDS orphans in
rural Uganda. Social Science and Medicine, 61, 555-564.</mixed-citation>
         </ref>
         <ref id="d17e276a1310">
            <mixed-citation id="d17e280" publication-type="other">
Backstrand, J. R., Allen, L. H., Pelto, G. H., &amp; Chavez, A. (1997). Examining the gender gap in nutrition:
An example from rural Mexico. Social Science and Medicine, 44, 1751-1759.</mixed-citation>
         </ref>
         <ref id="d17e290a1310">
            <mixed-citation id="d17e294" publication-type="other">
Beegle, K., De Weerdt, J., &amp; Dercon, S. (2010a). Orphanhood and human capital destruction: Is there
persistence into adulthood? Demography, 47(1), 163-180.</mixed-citation>
         </ref>
         <ref id="d17e304a1310">
            <mixed-citation id="d17e308" publication-type="other">
Beegle, K., Filmer, D., Stokes, A., &amp; Tiererova, L. (2010b). Orphanhood and the living arrangements of
children in sub-Saharan Africa. World Development , 35(12), 1727-1746.</mixed-citation>
         </ref>
         <ref id="d17e318a1310">
            <mixed-citation id="d17e322" publication-type="other">
Beegle, K., &amp; Krutikova, S. (2008). Adult mortality and children's transition into marriage. Demographic
Research, 19, 1551-1572.</mixed-citation>
         </ref>
         <ref id="d17e332a1310">
            <mixed-citation id="d17e336" publication-type="other">
Birdthistle, I. J., Floyd, S., Machingur, A., Mudziwapasi, N., Gregson, S., &amp; Glynn, J. R. (2008). From
affected to infected? Orphanhood and HIV risk among female adolescents in urban Zimbabwe.
AIDS, 22(6), 759-766.</mixed-citation>
         </ref>
         <ref id="d17e350a1310">
            <mixed-citation id="d17e354" publication-type="other">
Bishai, D., Suliman, E. D., Brahmbhatt, H., Wabwire-Mangen, F., Kigozi, G., Sewankambo, N., et al.
(2003). Does biological relatedness affect survival? Demographic Research, 8, 261-278.</mixed-citation>
         </ref>
         <ref id="d17e364a1310">
            <mixed-citation id="d17e368" publication-type="other">
Borooah, V. K. (2004). Gender bias among children in India in their diet and immunization against
disease. Social Science and Medicine, 58, 1719-1731.</mixed-citation>
         </ref>
         <ref id="d17e378a1310">
            <mixed-citation id="d17e382" publication-type="other">
Case, A., &amp; Ardington, C. (2006). The impact of parental death on school outcomes: Longitudinal
evidence from South Africa. Demography, 43(3), 401-420.</mixed-citation>
         </ref>
         <ref id="d17e392a1310">
            <mixed-citation id="d17e396" publication-type="other">
Case, A., Paxson, С., &amp; Abieidinger, J. (2004). Orphans in Africa: Parental death, poverty, and school
enrollment. Demography, 41(3), 483-508.</mixed-citation>
         </ref>
         <ref id="d17e406a1310">
            <mixed-citation id="d17e410" publication-type="other">
Castle, S. E. (1995). Child fostering and children's nutritional outcomes in rural Mali: The role of female
status in directing child transfers. Social Science and Medicine, 40, 679-693.</mixed-citation>
         </ref>
         <ref id="d17e420a1310">
            <mixed-citation id="d17e424" publication-type="other">
Cluver, L., &amp; Orkin, M. (2009). Cumulative risk and AIDS-orphanhood: Interactions of stigma, bullying
and poverty on child mental health in South Africa. Social Science and Medicine, 69, 1186-1193.</mixed-citation>
         </ref>
         <ref id="d17e435a1310">
            <mixed-citation id="d17e439" publication-type="other">
Dahl, В. (2009a). Left behind? Orphaned children, humanitarian aid, and the politics of kinship, culture,
and caregiving during Botswana's AIDS crisis. Dissertation, University of Chicago.</mixed-citation>
         </ref>
         <ref id="d17e449a1310">
            <mixed-citation id="d17e453" publication-type="other">
Dahl, B. (2009b). The "failures of culture": Christianity, kinship, and moral discourses about orphans
during Botswana's AIDS crisis. Africa Today, 56(1), 23-43.</mixed-citation>
         </ref>
         <ref id="d17e463a1310">
            <mixed-citation id="d17e467" publication-type="other">
Demographic and Health Surveys (DHS). (2011). Compilation by authors of statistics from most recent
DHS final reports, http://www.measuredhs.com.</mixed-citation>
         </ref>
         <ref id="d17e477a1310">
            <mixed-citation id="d17e481" publication-type="other">
Dreze, J., &amp; Sen, A. (1989). Hunger and public action. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d17e488a1310">
            <mixed-citation id="d17e492" publication-type="other">
Evans, D. K., &amp; Miguel, E. (2007). Orphans and schooling in Africa: A longitudinal analysis.
Demography, 44(1), 35-57.</mixed-citation>
         </ref>
         <ref id="d17e502a1310">
            <mixed-citation id="d17e506" publication-type="other">
Foster, G., Makufa, C., Drew, R., Mashumba, S., &amp; Kambeu, S. (1997). Perceptions of children and
community members concerning the circumstances of orphans in rural Zimbabwe. AIDS Care, 9(4),
391-405.</mixed-citation>
         </ref>
         <ref id="d17e520a1310">
            <mixed-citation id="d17e524" publication-type="other">
Gage, A. (2005). The interrelationship between fosterage, schooling, and children's labor force
participation in Ghana. Population Research and Policy Review, 24(5), 431-466.</mixed-citation>
         </ref>
         <ref id="d17e534a1310">
            <mixed-citation id="d17e538" publication-type="other">
Grant, M. J., &amp; Yeatman, S. E. (2008). Children's living arrangements and gender differences in parental
support in rural Malawi. Paper presented at the Annual Meeting of the Population Association of
America, New Orleans.</mixed-citation>
         </ref>
         <ref id="d17e551a1310">
            <mixed-citation id="d17e555" publication-type="other">
Gregson, S., Nyamukapa, C. A., Garnett, G. P., Wambe, M., Lewis, J. J. C., Mason, P. R., et al. (2005).
HIV infection and reproductive health in teenage women orphaned and made vulnerable by AIDS in
Zimbabwe. AIDS Care, 17, 785-794.</mixed-citation>
         </ref>
         <ref id="d17e568a1310">
            <mixed-citation id="d17e572" publication-type="other">
Hosegood, V., Floyd, S., Marston, M., Hill, С., McGrath, N., Isingo, R., et al. (2007a). The effects of high
HIV prevalence on orphanhood and living arrangements of children in Malawi, Tanzania, and South
Africa. Population Studies, 67(3), 327-336.</mixed-citation>
         </ref>
         <ref id="d17e585a1310">
            <mixed-citation id="d17e589" publication-type="other">
Hosegood, V., Preston-Whyte, E., Busza, J., Moitse, S., &amp; Timaeus, I. (2007b). Revealing the full extent
of household's experiences of HIV/AIDS in rural South Africa. Social Science and Medicine, 65,
1249-1259.</mixed-citation>
         </ref>
         <ref id="d17e602a1310">
            <mixed-citation id="d17e606" publication-type="other">
Kaggwa, E. В., &amp; Hindin, M. J. (2010). The psychological effect of orphanhood in a mature HIV
epidemic: An analysis of young people in Mukono, Uganda. Social Science and Medicine, 70,
1002-1010.</mixed-citation>
         </ref>
         <ref id="d17e620a1310">
            <mixed-citation id="d17e624" publication-type="other">
Madhavan, S. (2004). Fosterage patterns in the age of AIDS: Continuity and change. Social Science and
Medicine, 58, 1443-1454.</mixed-citation>
         </ref>
         <ref id="d17e634a1310">
            <mixed-citation id="d17e638" publication-type="other">
Madhavan, S., Schatz, E., &amp; Clark, В. (2009). Effect of HIV/AIDS-related mortality on household
dependency ratios in rural South Africa, 2000-2005. Population Studies, 63(1), 37-51.</mixed-citation>
         </ref>
         <ref id="d17e648a1310">
            <mixed-citation id="d17e652" publication-type="other">
Makame, V., Ani, С., &amp; Grantham-McGregor, S. (2002). Psychological well-being of orphans in Dar El
Salaam, Tanzania. Acta Paediatrica, 91, 459-465.</mixed-citation>
         </ref>
         <ref id="d17e662a1310">
            <mixed-citation id="d17e666" publication-type="other">
Miller, C. M., Gruskin, S., Subramanian, S. V., &amp; Heymann, J. (2007). Emerging health disparities in
Botswana: Examining the situation of orphans during the AIDS epidemic. Social Science and
Medicine, 64, 2476-2486.</mixed-citation>
         </ref>
         <ref id="d17e679a1310">
            <mixed-citation id="d17e683" publication-type="other">
Ministry of Health and Social Welfare [Lesotho] and ICF Macro. (2010). Lesotho Demographic and
Health Survey: 2009. Maseru, Lesotho: Authors.</mixed-citation>
         </ref>
         <ref id="d17e693a1310">
            <mixed-citation id="d17e697" publication-type="other">
Mojola, S. A. (2011). Multiple transitions and HIV risk among orphaned Kenyan schoolgirls. Studies in
Family Planning, 42(1), 29-40.</mixed-citation>
         </ref>
         <ref id="d17e708a1310">
            <mixed-citation id="d17e712" publication-type="other">
Monasch, R., &amp; Boerma, J. T. (2004). Orphanhood and childcare patterns in sub-Saharan Africa: An
analysis of national surveys from 40 countries. AIDS, 18(S2), S55-S65.</mixed-citation>
         </ref>
         <ref id="d17e722a1310">
            <mixed-citation id="d17e726" publication-type="other">
National AIDS Commission. (2010). Report on the National Response to HIV and AIDS: Reporting
period 2006-2010. Maseru, Lesotho: Author.</mixed-citation>
         </ref>
         <ref id="d17e736a1310">
            <mixed-citation id="d17e740" publication-type="other">
Nyamukapa, C., &amp; Gregson, S. (2005). Extended family's and women's roles in safeguarding orphans'
education in AIDS-afflicted rural Zimbabwe. Social Science and Medicine, 60, 2155-2167.</mixed-citation>
         </ref>
         <ref id="d17e750a1310">
            <mixed-citation id="d17e754" publication-type="other">
Nyamukapa, C. A., Gregson, S., Lopman, В., Salto, S., Watts, H. J., Monasch, R., et al. (2008). HIV-
associated orphanhood and children's psychosocial distress: Theoretical framework tested with data
from Zimbabwe. American Journal of Public Health, 98, 133-141.</mixed-citation>
         </ref>
         <ref id="d17e767a1310">
            <mixed-citation id="d17e771" publication-type="other">
Nyamukapa, C. A., Gregson, S., Wambe, M., Mushore, P., Lopman, В., Mupambireyi, Z., et al. (2010).
Causes and consequences of psychological distress among orphans in eastern Zimbabwe. AIDS
Care, 22, 988-996.</mixed-citation>
         </ref>
         <ref id="d17e784a1310">
            <mixed-citation id="d17e788" publication-type="other">
Operario, D., Pettifor, A., Cluver, L., MacPhail, C., &amp; Rees, H. (2007). Prevalence of parental death
among young people in South Africa and risk for HIV infection. Journal of Acquired Immune
Deficiency Syndromes, 44, 93-98.</mixed-citation>
         </ref>
         <ref id="d17e802a1310">
            <mixed-citation id="d17e806" publication-type="other">
Parker, E. M., &amp; Short, S. E. (2009). Grandmother co-residence and school enrollment in sub-Saharan
Africa. Journal of Family Issues, 30(6), 813-836.</mixed-citation>
         </ref>
         <ref id="d17e816a1310">
            <mixed-citation id="d17e820" publication-type="other">
Parker, E., Short, S. E., Goldberg, R. E., &amp; Hlabana, T. (2007). Growing up in the context of high HIV
prevalence: adult death and illness, family living arrangements, and children's lives in Southern
Africa. Paper presented at the 5th African Population Conference, Arusha, Tanzania.</mixed-citation>
         </ref>
         <ref id="d17e833a1310">
            <mixed-citation id="d17e837" publication-type="other">
Schatz, E. (2007). 'Taking care of my own blood': Older women's relationships to their households in
rural South Africa. Scandinavian Journal of Public Health, 55(Suppl 69), 147-154.</mixed-citation>
         </ref>
         <ref id="d17e847a1310">
            <mixed-citation id="d17e851" publication-type="other">
Seeley, J., Biraro, S., Shafer, L. A., Nasirumbi, P., Foster, S., Whitworth, J., et al. (2008). Using in-depth
qualitative data to enhance our understanding of quantitative results regarding the impact of HIV
and AIDS on households in rural Uganda. Social Science and Medicine, 67, 1434-1446.</mixed-citation>
         </ref>
         <ref id="d17e864a1310">
            <mixed-citation id="d17e868" publication-type="other">
Skovdal, M., Mwasiaji, W., Webale, A., &amp; Tomkins, A. (201 1). Building orphan competent communities:
Experiences from a community-based capital cash transfer initiative in Kenya. Health Policy and
Planning, 26, 233-241.</mixed-citation>
         </ref>
         <ref id="d17e881a1310">
            <mixed-citation id="d17e885" publication-type="other">
Skovdal, M., Oguto, V. O., Aaro, C., &amp; Campbell, C. (2009). Young carers as social actors: Coping
strategies of children caring for ailing or ageing guardians in Western Kenya. Social Science and
Medicine, 69, 587-595.</mixed-citation>
         </ref>
         <ref id="d17e899a1310">
            <mixed-citation id="d17e903" publication-type="other">
Thomas, K. J. A. (2010). Family contexts and schooling disruption among orphans in post-genocide
Rwanda. Population Research and Policy Review, 29 , 819-842.</mixed-citation>
         </ref>
         <ref id="d17e913a1310">
            <mixed-citation id="d17e917" publication-type="other">
Thurman, T. R., Snider, L. A., Boris, N. W., Kalisa, E., Nyirazinyoye, L., &amp; Brown, L. (2008). Barriers to
the community support of orphans and vulnerable youth in Rwanda. Social Science and Medicine,
66, 1557-1567.</mixed-citation>
         </ref>
         <ref id="d17e930a1310">
            <mixed-citation id="d17e934" publication-type="other">
Timaeus, I. ML, &amp; Boler, T. (2007). Father figures: The progress at school of orphans in South Africa.
AIDS, 27(S7), S83-S93.</mixed-citation>
         </ref>
         <ref id="d17e944a1310">
            <mixed-citation id="d17e948" publication-type="other">
UNAIDS. (2010). Report on the global AIDS epidemic. Geneva: Author. Available via http://www.
unaids.org.</mixed-citation>
         </ref>
         <ref id="d17e958a1310">
            <mixed-citation id="d17e962" publication-type="other">
UNAIDS, UNICEF, and USAID. (2004). Children on the brink 2004: A joint report of new orphan
estimates and a framework for action . Washington, DC: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d17e972a1310">
            <mixed-citation id="d17e976" publication-type="other">
UNAIDS/WHO. (2009). UNAIDS/WHO epidemiological fact sheets on HIV and AIDS, 2008 update.
Geneva: UNAIDS/WHO Working Group on Global HIV/AIDS and STI. Available by country via
http://www.unaids.org/en/KnowledgeCentre/HIVData/Epidemiology/epifactsheets.asp.</mixed-citation>
         </ref>
         <ref id="d17e990a1310">
            <mixed-citation id="d17e994" publication-type="other">
UNICEF. (2007). Revised country programme document (no changes) Lesotho (2008-2012). Maseru,
Lesotho: Author. Available viahttp://www.unicef.org/about/execboard/files/07-PL36-Lesotho(2).pdf.</mixed-citation>
         </ref>
         <ref id="d17e1004a1310">
            <mixed-citation id="d17e1008" publication-type="other">
UNICEF. (2010). UNICEF statistics expert honoured for research on the needs of children affected by
AIDS. Available via http://www.unicef.org/aids/index_55221.html.</mixed-citation>
         </ref>
         <ref id="d17e1018a1310">
            <mixed-citation id="d17e1022" publication-type="other">
UNICEF. (2011). Lesotho statistics. Available via http://www.unicef.org/infobycountry/lesotho_statistics.html.</mixed-citation>
         </ref>
         <ref id="d17e1029a1310">
            <mixed-citation id="d17e1033" publication-type="other">
Wood, K., Chase, E., &amp; Aggleton, P. (2006). "Telling the truth is the best thing": teenage orphans
experiences of parental AIDS-related illness and bereavement in Zimbabwe. Social Science and
Medicine, 63, 1923-1933.</mixed-citation>
         </ref>
         <ref id="d17e1046a1310">
            <mixed-citation id="d17e1050" publication-type="other">
Yamano, T., &amp; Jayne, T. S. (2005). Working age adult mortality and primary school attendance in rural
Kenya. Economic Development and Cultural Change, 53(3), 619-653.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
