<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amersocirevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100080</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Sociological Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00031224</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41723038</article-id>
         <title-group>
            <article-title>Resolving the Democracy Paradox: Democratization and Women's Legislative Representation in Developing Nations, 1975 to 2009</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kathleen M.</given-names>
                  <surname>Fallon</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Liam</given-names>
                  <surname>Swiss</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jocelyn</given-names>
                  <surname>Viterna</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">77</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40081041</issue-id>
         <fpage>380</fpage>
         <lpage>408</lpage>
         <permissions>
            <copyright-statement>Copyright ©2012 American Sociological Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41723038"/>
         <abstract>
            <p>Increasing levels of democratic freedoms should, in theory, improve women's access to political positions. Yet studies demonstrate that democracy does little to improve women's legislative representation. To resolve this paradox, we investigate how variations in the democratization process—including pre-transition legacies, historical experiences with elections, the global context of transition, and post-transition democratic freedoms and quotas—affect women's representation in developing nations. We find that democratization's effect is curvilinear. Women in non-democratic regimes often have high levels of legislative representation but little real political power. When democratization occurs, women's representation initially drops, but with increasing democratic freedoms and additional elections, it increases again. The historical context of transition further moderates these effects. Prior to 1995, women's representation increased most rapidly in countries transitioning from civil strife—but only when accompanied by gender quotas. After 1995 and the Beijing Conference on Women, the effectiveness of quotas becomes more universal, with the exception of postcommunist countries. In these nations, quotas continue to do little to improve women's representation. Our results, based on pooled time series analysis from 1975 to 2009, demonstrate that it is not democracy—as measured by a nation's level of democratic freedoms at a particular moment in time—but rather the democratization process that matters for women's legislative representation.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d181e215a1310">
            <label>1</label>
            <mixed-citation id="d181e222" publication-type="other">
Mansbridge 1999;</mixed-citation>
            <mixed-citation id="d181e228" publication-type="other">
Phil-
lips 1998</mixed-citation>
         </ref>
         <ref id="d181e238a1310">
            <label>2</label>
            <mixed-citation id="d181e245" publication-type="other">
Paxton, Hughes, and Painter
(2010).</mixed-citation>
         </ref>
         <ref id="d181e255a1310">
            <label>4</label>
            <mixed-citation id="d181e262" publication-type="other">
Hughes (2007),</mixed-citation>
         </ref>
         <ref id="d181e269a1310">
            <label>5</label>
            <mixed-citation id="d181e276" publication-type="other">
Dahlerup (2005)</mixed-citation>
         </ref>
         <ref id="d181e284a1310">
            <label>6</label>
            <mixed-citation id="d181e291" publication-type="other">
Htun and Jones 2002;</mixed-citation>
            <mixed-citation id="d181e297" publication-type="other">
Jones 2009;
Krook 2009;</mixed-citation>
            <mixed-citation id="d181e306" publication-type="other">
Matland 2006;</mixed-citation>
            <mixed-citation id="d181e313" publication-type="other">
Schmidt and Saunders
2004</mixed-citation>
         </ref>
         <ref id="d181e323a1310">
            <label>8</label>
            <mixed-citation id="d181e332" publication-type="other">
Beckfield (2006)</mixed-citation>
            <mixed-citation id="d181e338" publication-type="other">
Achen
2000</mixed-citation>
         </ref>
         <ref id="d181e348a1310">
            <label>10</label>
            <mixed-citation id="d181e355" publication-type="other">
Bollen, Entwisle, and Alderson 1993</mixed-citation>
         </ref>
         <ref id="d181e362a1310">
            <label>15</label>
            <mixed-citation id="d181e369" publication-type="other">
Uppsala Conflict
Data Program (UCDP)/Peace Research Institute
Oslo (PRIO) Intrastate Conflict Onset Dataset V.4
(2007)</mixed-citation>
            <mixed-citation id="d181e384" publication-type="other">
Gleditsch et al. 2002</mixed-citation>
         </ref>
         <ref id="d181e391a1310">
            <label>17</label>
            <mixed-citation id="d181e398" publication-type="other">
http://asr.sagepub.com/supplemental</mixed-citation>
         </ref>
         <ref id="d181e405a1310">
            <label>24</label>
            <mixed-citation id="d181e412" publication-type="other">
Paxton and col-
leagues (2006),</mixed-citation>
         </ref>
         <ref id="d181e423a1310">
            <label>25</label>
            <mixed-citation id="d181e430" publication-type="other">
United Nations 2006</mixed-citation>
         </ref>
         <ref id="d181e437a1310">
            <label>26</label>
            <mixed-citation id="d181e444" publication-type="other">
Paxton and colleagues' (2006)</mixed-citation>
         </ref>
         <ref id="d181e451a1310">
            <label>28</label>
            <mixed-citation id="d181e460" publication-type="other">
Wooldridge 2009:99</mixed-citation>
         </ref>
         <ref id="d181e467a1310">
            <label>31</label>
            <mixed-citation id="d181e474" publication-type="other">
Chen et al. 2003</mixed-citation>
         </ref>
         <ref id="d181e481a1310">
            <label>33</label>
            <mixed-citation id="d181e488" publication-type="other">
Freedom House 2010</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d181e504a1310">
            <mixed-citation id="d181e508" publication-type="other">
Achen, Christopher H. 2000. "Why Lagged Dependent
Variables Can Suppress the Explanatory Power of
Other Independent Variables." Presented at the
annual meeting of the Political Methodology Section
of the American Political Science Association,
UCLA, Los Angeles, CA.</mixed-citation>
         </ref>
         <ref id="d181e531a1310">
            <mixed-citation id="d181e535" publication-type="other">
Almeida, Paul. 2008. Waves of Protest: Popular Strug-
gle in El Salvador, 1925-2005. Minneapolis: Uni-
versity of Minnesota Press.</mixed-citation>
         </ref>
         <ref id="d181e548a1310">
            <mixed-citation id="d181e552" publication-type="other">
Alvarez, Sonia E. 1990. Engendering Democracy in
Brazil. Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d181e562a1310">
            <mixed-citation id="d181e566" publication-type="other">
Beck, Thorsten, George Clarke, Alberto Groff, Philip
Keefer, and Patrick Walsh. 2001. "New Tools in
Comparative Political Economy: The Database of
Political Institutions." The World Bank Economic
Review 15:165-76.</mixed-citation>
         </ref>
         <ref id="d181e586a1310">
            <mixed-citation id="d181e590" publication-type="other">
Beckfield, Jason. 2006. "European Integration and
Income Inequality." American Sociological Review
71:964-85.</mixed-citation>
         </ref>
         <ref id="d181e603a1310">
            <mixed-citation id="d181e607" publication-type="other">
Bollen, Kenneth A., Barbara Entwisle, and Arthur S. A1-
derson. 1993. "Macrocomparative Research Meth-
ods." Annual Review of Sociology 19:321-51.</mixed-citation>
         </ref>
         <ref id="d181e620a1310">
            <mixed-citation id="d181e626" publication-type="other">
Brady, David, Yunus Kaya, and Jason Beckfield. 2007.
"Reassessing the Effect of Economic Growth on
Weil-Being in Less Developed Countries, 1980-
2003." Studies in Comparative International Devel-
opment 42:1-35.</mixed-citation>
         </ref>
         <ref id="d181e645a1310">
            <mixed-citation id="d181e649" publication-type="other">
Britton, Hannah E. 2002. "Coalition Building, Election
Rules, and Party Politics: South African Women's
Path to Parliament." Africa Today 49:33-64.</mixed-citation>
         </ref>
         <ref id="d181e662a1310">
            <mixed-citation id="d181e666" publication-type="other">
Bystydzienski, Jill M. and Joti Sekhon. 1999. "Introduc-
tion." Pp. 1-21 in Democratization and Women's
Grassroots Movements, edited by J. M. Bystydzien-
ski and J. Sekhon. Bloomington: Indiana University
Press.</mixed-citation>
         </ref>
         <ref id="d181e685a1310">
            <mixed-citation id="d181e689" publication-type="other">
Chen, Li-Ju. 2008. "Do Gender Quotas Influence Wom-
en's Representation and Policies?" Department of
Economics, Stockholm University. Unpublished
manuscript. Retrieved June 13, 2011 (http://people
.su.se/~lchen/pub/womenquota7.pdf).</mixed-citation>
         </ref>
         <ref id="d181e709a1310">
            <mixed-citation id="d181e713" publication-type="other">
Chen, Xiao, Philip B. Ender, Michael Mitchell, and
Christine Wells. 2003. "Regression with Stata."
Retrieved September 16, 2008 (http://www.ats
.ucla.edu/stat/stata/webbooks/reg/default.htm).</mixed-citation>
         </ref>
         <ref id="d181e729a1310">
            <mixed-citation id="d181e733" publication-type="other">
Childs, Sarah and Mona Lena Krook. 2009. "Analysing
Women's Substantive Representation: From Critical
Mass to Critical Actors." Government &amp; Opposition
44:125-45.</mixed-citation>
         </ref>
         <ref id="d181e749a1310">
            <mixed-citation id="d181e753" publication-type="other">
Chinchilla, Norma Stoltz. 1994. "Women's Movements
and Democracy in Latin America: Some Unresolved
Tensions." Pp.. 1-19 in Women and the Transition to
Democracy: The Impact of Political and Economic
Reform in Latin America. Washington, DC: Wood-
row Wilson International Center for Scholars.</mixed-citation>
         </ref>
         <ref id="d181e776a1310">
            <mixed-citation id="d181e780" publication-type="other">
Clarke, Killian. 2011. "Saying 'Enough': Authoritarian-
ism and Egypt's Kefaya Movement." Mobilization
16:490-509.</mixed-citation>
         </ref>
         <ref id="d181e793a1310">
            <mixed-citation id="d181e797" publication-type="other">
Dahlerup, Drude. 2005. "Increasing Women's Political
Representation: New Trends in Gender Quotas."
Pp. 141-53 in Women in Parliament: Beyond Num-
bers, rev. ed., edited by J. Ballington and A. Karam.
Stockholm, Sweden: International IDEA.</mixed-citation>
         </ref>
         <ref id="d181e816a1310">
            <mixed-citation id="d181e820" publication-type="other">
Dahlerup, Drude. 2006. "Introduction." Pp. 3-31 in
Women, Quotas and Politics, edited by D. Dahlerup.
New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d181e834a1310">
            <mixed-citation id="d181e838" publication-type="other">
Einhorn, Barbara. 1993. Cinderella Goes to Market: Cit-
izenship, Gender and Women 's Movements in East
Central Europe. London: Verso.</mixed-citation>
         </ref>
         <ref id="d181e851a1310">
            <mixed-citation id="d181e855" publication-type="other">
Fallon, Kathleen M. 2008. Democracy and the Rise of
Women 's Movements in Sub-Saharan Africa. Balti-
more, MD: The Johns Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d181e868a1310">
            <mixed-citation id="d181e872" publication-type="other">
Fisher, Jo. 1989. Mothers of the Disappeared. Cam-
bridge, UK: South End Press.</mixed-citation>
         </ref>
         <ref id="d181e882a1310">
            <mixed-citation id="d181e886" publication-type="other">
Fisher, Jo. 1993. "Women and Democracy: For Home
and Country." NACLA Report on the Americas
27:30-36.</mixed-citation>
         </ref>
         <ref id="d181e899a1310">
            <mixed-citation id="d181e903" publication-type="other">
Freedom House. 2010. "Freedom in the World 2010."
Retrieved March 27, 2011 (http://www.freedom
house.org/sites/default/files/inline_images/FIWAll
ScoresCountries 1973-20 11.xls).</mixed-citation>
         </ref>
         <ref id="d181e919a1310">
            <mixed-citation id="d181e925" publication-type="other">
Friedman, Elisabeth J. 1998. "Paradoxes of Gendered
Political Opportunity in the Venezuelan Transition
to Democracy." Latin American Research Review
33:87-135.</mixed-citation>
         </ref>
         <ref id="d181e942a1310">
            <mixed-citation id="d181e946" publication-type="other">
Gal, Susan and Gail Kligman, eds. 2000. Reproducing
Gender. Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d181e956a1310">
            <mixed-citation id="d181e960" publication-type="other">
Geisler, Gisela. 1995. "Troubled Sisterhood: Women
and Politics in Southern Africa." African Affairs
94:545-78.</mixed-citation>
         </ref>
         <ref id="d181e973a1310">
            <mixed-citation id="d181e977" publication-type="other">
Gerring, John, Strom C. Thacker, and Carola Moreno.
2005. "Centripetal Democratic Governance: A The-
ory and Global Inquiry." American Political Science
Review 99:567-81.</mixed-citation>
         </ref>
         <ref id="d181e993a1310">
            <mixed-citation id="d181e997" publication-type="other">
Gleditsch, Nils Petter, Peter Wallensteen, Mikael Eriks-
son, Margareta Sollenberg, and Hávard Strand. 2002.
"Armed Conflict 1946-2001: A New Dataset."
Journal of Peace Research 39:615-37.</mixed-citation>
         </ref>
         <ref id="d181e1013a1310">
            <mixed-citation id="d181e1017" publication-type="other">
Haney, Lynne A. 1994. "From Proud Worker to Good
Mother: Women, the State, and Regime Change in
Hungary." Frontiers Editorial Collective 14:113-50.</mixed-citation>
         </ref>
         <ref id="d181e1030a1310">
            <mixed-citation id="d181e1034" publication-type="other">
Hassim, Shireen. 2006. Women's Organizations and
Democracy in South Africa. Madison: University of
Wisconsin Press.</mixed-citation>
         </ref>
         <ref id="d181e1048a1310">
            <mixed-citation id="d181e1052" publication-type="other">
Hausman, Jerry A. 1978. "Specification Tests in Econo-
metrics." Econometrica 46:1251-71.</mixed-citation>
         </ref>
         <ref id="d181e1062a1310">
            <mixed-citation id="d181e1066" publication-type="other">
Heston, Alan, Robert Summers, and Bettina Aten. 2006.
"Penn World Table Version 6.2." Center for Inter-
national Comparisons of Production, Income and
Prices at the University of Pennsylvania.</mixed-citation>
         </ref>
         <ref id="d181e1082a1310">
            <mixed-citation id="d181e1086" publication-type="other">
Htun, Mala N. and Mark P. Jones. 2002. "Engendering
the Right to Participate in Decision-Making: Elec-
toral Quotas and Women's Leadership in Latin
America." Pp. 32-56 in Gender and the Politics of
Rights and Democracy in Latin America , edited by
N. Craske and M. Molyneux. Houndmills, Basing-
stoke: Palgrave.</mixed-citation>
         </ref>
         <ref id="d181e1112a1310">
            <mixed-citation id="d181e1116" publication-type="other">
Hughes, Melanie. 2005. "The Continuing Importance of
History: The Residual Effects of Colonialism on
Women's Parliamentary Participation." Presented
at the annual meeting of the American Sociological
Association, Philadelphia, PA, August 13-16.</mixed-citation>
         </ref>
         <ref id="d181e1135a1310">
            <mixed-citation id="d181e1139" publication-type="other">
Hughes, Melanie. 2007. "Windows of Political Opportu-
nity." International Journal of Sociology 37:27-52.</mixed-citation>
         </ref>
         <ref id="d181e1149a1310">
            <mixed-citation id="d181e1153" publication-type="other">
Hughes, Melanie. 2009. "Armed Conflict, International
Linkages, and Women's Parliamentary Representa-
tion in Developing Nations." Social Problems
56:174-204.</mixed-citation>
         </ref>
         <ref id="d181e1170a1310">
            <mixed-citation id="d181e1174" publication-type="other">
Hughes, Melanie and Pamela Paxton. 2007. "Familiar
Theories from a New Perspective: The Implications
of a Longitudinal Approach to Women in Politics
Research." Politics and Gender 3:370-78.</mixed-citation>
         </ref>
         <ref id="d181e1190a1310">
            <mixed-citation id="d181e1194" publication-type="other">
Inglehart, Ronald and Pippa Norris. 2003. Rising Tide:
Gender Equality and Cultural Change around the
World. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d181e1207a1310">
            <mixed-citation id="d181e1211" publication-type="other">
International Institute for Democracy and Electoral
Assistance (IDEA). 2002. "Voter Turnout Since
1945: A Global Report." Stockholm, Sweden:
IDEA.</mixed-citation>
         </ref>
         <ref id="d181e1227a1310">
            <mixed-citation id="d181e1231" publication-type="other">
International Institute for Democracy and Electoral
Assistance (IDEA). 2005. "Electoral System Design
Database." Retrieved March 19, 2005 (http://www
.idea.int/esd/world.cfm).</mixed-citation>
         </ref>
         <ref id="d181e1247a1310">
            <mixed-citation id="d181e1251" publication-type="other">
International Institute for Democracy and Electoral
Assistance (IDEA) and Stockholm University.
2010. "Global Database of Quotas for Women."
Stockholm, Sweden: IDEA. Retrieved November
10, 2010 (http://www.quotaproject.org/).</mixed-citation>
         </ref>
         <ref id="d181e1270a1310">
            <mixed-citation id="d181e1274" publication-type="other">
Inter-Parliamentary Union (IPU). 1995. "Women in Par-
liaments: 1945-1995." Geneva: IPU.</mixed-citation>
         </ref>
         <ref id="d181e1285a1310">
            <mixed-citation id="d181e1289" publication-type="other">
Inter-Parliamentary Union (IPU). 2011. "Women in
National Parliaments." Retrieved January 21, 2011
(http://www.ipu.org/wmn-e/classif.htm).</mixed-citation>
         </ref>
         <ref id="d181e1302a1310">
            <mixed-citation id="d181e1306" publication-type="other">
Jaquette, Jane S. and Sharon L. Wolchik, eds. 1998.
Women and Democracy: Latin America and Central
and Eastern Europe. Baltimore, MD: The Johns
Hopkins University Press.</mixed-citation>
         </ref>
         <ref id="d181e1322a1310">
            <mixed-citation id="d181e1326" publication-type="other">
Jones, Mark P. 1996. "Increasing Women's Representa-
tion via Gender Quotas: The Argentine Ley de Cu-
pos." Women &amp; Politics 16:75-98.</mixed-citation>
         </ref>
         <ref id="d181e1339a1310">
            <mixed-citation id="d181e1343" publication-type="other">
Jones, Mark P. 1997. "Legislator Gender and Legislator
Policy Priorities in the Argentine Chamber of Depu-
ties and the United States House of Representa-
tives." Policy Studies Journal 25:613-29.</mixed-citation>
         </ref>
         <ref id="d181e1359a1310">
            <mixed-citation id="d181e1363" publication-type="other">
Jones, Mark P. 2009. "Gender Quotas, Electoral Laws,
and the Election of Women: Evidence from the Latin
American Vanguard." Comparative Political Studies
42:56-81.</mixed-citation>
         </ref>
         <ref id="d181e1379a1310">
            <mixed-citation id="d181e1383" publication-type="other">
Kasfir, Nelson, ed. 1998. Civil Society and Democracy in
Africa: Critical Perspectives. London: Frank Cass.</mixed-citation>
         </ref>
         <ref id="d181e1394a1310">
            <mixed-citation id="d181e1398" publication-type="other">
Kenworthy, Lane and Melissa Malami. 1999. "Gender
Inequality in Political Representation: A Worldwide
Comparative Analysis." Social Forces 78:235-68.</mixed-citation>
         </ref>
         <ref id="d181e1411a1310">
            <mixed-citation id="d181e1415" publication-type="other">
Kittilson, Miki Caul. 2008. "Representing Women: The
Adoption of Family Leave in Comparative Perspec-
tive." Journal of Public Economics 70:323-34.</mixed-citation>
         </ref>
         <ref id="d181e1428a1310">
            <mixed-citation id="d181e1432" publication-type="other">
Krook, Mona L. 2006. "Reforming Representation: The
Diffusion of Candidate Gender Quotas Worldwide."
Politics &amp; Gender 2:303-327.</mixed-citation>
         </ref>
         <ref id="d181e1445a1310">
            <mixed-citation id="d181e1449" publication-type="other">
Krook, Mona L. 2009. Quotas for Women in Politics:
Gender and Candidate Selection Reform Worldwide.
New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d181e1462a1310">
            <mixed-citation id="d181e1466" publication-type="other">
Kunovich, Sheri and Pamela Paxton. 2005. "Pathways
to Power: The Role of Political Parties in Women's
National Political Representation." American Jour-
nal of Sociology 111 :505-552.</mixed-citation>
         </ref>
         <ref id="d181e1482a1310">
            <mixed-citation id="d181e1486" publication-type="other">
Lindberg, Staffan I. 2004. "Women's Empowerment
and Democratization: The Effects of Electoral Sys-
tems, Participation and Experience in Africa." Stud-
ies in Comparative International Development
39:28-53.</mixed-citation>
         </ref>
         <ref id="d181e1506a1310">
            <mixed-citation id="d181e1510" publication-type="other">
Luciak, Ilja A. 2001. After the Revolution: Gender and
Democracy in El Salvador, Nicaragua, and Guate-
mala. Baltimore, MD: John Hopkins University
Press.</mixed-citation>
         </ref>
         <ref id="d181e1526a1310">
            <mixed-citation id="d181e1530" publication-type="other">
Mamdani, Mahmood. 1996. Citizen and Subject: Con-
temporary Africa and the Legacy of Late Colonial-
ism. Princeton, NJ: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d181e1543a1310">
            <mixed-citation id="d181e1547" publication-type="other">
Mansbridge, Jane. 1999. "Should Women Represent
Women and Blacks Represent Blacks? A Contingent
4 Yes."' Journal of Politics 61:628-57.</mixed-citation>
         </ref>
         <ref id="d181e1560a1310">
            <mixed-citation id="d181e1564" publication-type="other">
Marshall, Monty G., Keith Jaggers, and Ted R. Gurr.
2009. "Polity IV Project: Political Regime Charac-
teristics and Transitions, 1800-2009." Computer
file. Retrieved February 7, 2011 (http://www.syste
micpeace.org/inscr/inscr.htm).</mixed-citation>
         </ref>
         <ref id="d181e1583a1310">
            <mixed-citation id="d181e1587" publication-type="other">
Mailand, Richard E. 1998. "Women's Representation in
National Legislatures: Developed and Developing
Countries." Legislative Studies Quarterly 23:109-
125.</mixed-citation>
         </ref>
         <ref id="d181e1603a1310">
            <mixed-citation id="d181e1607" publication-type="other">
Mailand, Richard E. 2002. "Enhancing Women's Polit-
ical Participation: Legislative Recruitment and Elec-
toral Systems." In Women in Parliament: Beyond
Numbers, edited by A. Karam. Stockholm, Sweden:
International Idea. Retrieved September 17, 2008
(http://www.idea.int/publications/wip/upload/
2_Matland.pdf).</mixed-citation>
         </ref>
         <ref id="d181e1634a1310">
            <mixed-citation id="d181e1638" publication-type="other">
Mailand, Richard E. 2006. "Electoral Quotas: Fre-
quency and Effectiveness." Pp. 275-92 in Women,
Quotas and Politics, edited by D. Dahlerup. New
York: Routledge.</mixed-citation>
         </ref>
         <ref id="d181e1654a1310">
            <mixed-citation id="d181e1658" publication-type="other">
McAllister, Ian and Donley T. Studiar. 2002. "Electoral
Systems and Women's Representation: A Long-
Term Perspective." Representation 39:3-14.</mixed-citation>
         </ref>
         <ref id="d181e1671a1310">
            <mixed-citation id="d181e1675" publication-type="other">
Meer, Shamim. 2005. "Freedom for Women: Main-
streaming Gender in the South African Liberation
Struggle and Beyond." Gender and Development
13:36-45.</mixed-citation>
         </ref>
         <ref id="d181e1691a1310">
            <mixed-citation id="d181e1695" publication-type="other">
Nelson, Barbara J. and Najma Chowdhury, eds. 1994.
Women and Politics Worldwide. New Haven, CT:
Yale University Press.</mixed-citation>
         </ref>
         <ref id="d181e1708a1310">
            <mixed-citation id="d181e1712" publication-type="other">
Paxton, Pamela. 1997. "Women in National Legisla-
tures: A Cross-National Analysis." Social Science
Research 26:442-64.</mixed-citation>
         </ref>
         <ref id="d181e1725a1310">
            <mixed-citation id="d181e1729" publication-type="other">
Paxton, Pamela, Melanie M. Hughes, and Jennifer L.
Green. 2006. "The International Women's Move-
ment and Women's Political Representation, 1893 -
2003." American Sociological Review 71:898-920.</mixed-citation>
         </ref>
         <ref id="d181e1746a1310">
            <mixed-citation id="d181e1750" publication-type="other">
Paxton, Pamela, Melanie Hughes, and Matthew Painter.
2010. "Growth in Women's Political Representa-
tion: A Longitudinal Exploration of Democracy,
Electoral System, and Gender Quotas." European
Journal of Political Research 49:25-52.</mixed-citation>
         </ref>
         <ref id="d181e1769a1310">
            <mixed-citation id="d181e1773" publication-type="other">
Paxton, Pamela and Sheri Kunovich. 2003. "Women's
Political Representation: The Importance of Ideol-
ogy." Social Forces 82:87-114.</mixed-citation>
         </ref>
         <ref id="d181e1786a1310">
            <mixed-citation id="d181e1790" publication-type="other">
Paxton, Pamela, Sheri Kunovich, and Melanie Hughes.
2007. "Gender in Politics." Annual Review of Soci-
ology 33:263-84.</mixed-citation>
         </ref>
         <ref id="d181e1803a1310">
            <mixed-citation id="d181e1809" publication-type="other">
Phillips, Anne. 1998. "Democracy and Representation:
Or, Why Should it Matter Who Our Representatives
Are?" Pp. 224-40 in Feminism and Politics, edited
by A. Phillips. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d181e1825a1310">
            <mixed-citation id="d181e1829" publication-type="other">
Pietila, Hilkka and Jeanne Vickers. 1990. Making
Women Matter: The Role of the United Nations.
London: Zed Books.</mixed-citation>
         </ref>
         <ref id="d181e1842a1310">
            <mixed-citation id="d181e1846" publication-type="other">
Ray, Raka and Anna C. Korteweg. 1999. "Women's
Movements in the Third World: Identity, Mobiliza-
tion, and Autonomy." Annual Review of Sociology
25:47-71.</mixed-citation>
         </ref>
         <ref id="d181e1863a1310">
            <mixed-citation id="d181e1867" publication-type="other">
Reynolds, Andrew. 1999. "Women in the Legislatures
and Executives of the World Knocking at the High-
est Glass Ceiling." World Politics 51:547-72.</mixed-citation>
         </ref>
         <ref id="d181e1880a1310">
            <mixed-citation id="d181e1884" publication-type="other">
Schmidt, Gregory D. and Kyle L. Saunders. 2004.
"Effective Quotas, Relative Party Magnitude, and
the Success of Female Candidates: Peruvian Munic-
ipal Elections in Comparative Perspective." Com-
parative Political Studies 37:704-734.</mixed-citation>
         </ref>
         <ref id="d181e1903a1310">
            <mixed-citation id="d181e1907" publication-type="other">
Schock, Kurt. 2005. Unarmed Insurrections: People
Power in Movements and Nondemocracies. Minne-
apolis: University of Minnesota Press.</mixed-citation>
         </ref>
         <ref id="d181e1920a1310">
            <mixed-citation id="d181e1924" publication-type="other">
Schwindt-Bayer, Leslie A. 2006. "Still Supermadres?
Gender and the Policy Priorities of Latin American
Legislators." American Journal of Political Science
50:570-85.</mixed-citation>
         </ref>
         <ref id="d181e1940a1310">
            <mixed-citation id="d181e1944" publication-type="other">
Seidman, Gay W. 1993. "'No Freedom without the
Women': Mobilization and Gender in South Africa,
1970-1992." Signs: Journal of Women in Culture
and Society 18:291-320.</mixed-citation>
         </ref>
         <ref id="d181e1960a1310">
            <mixed-citation id="d181e1964" publication-type="other">
Studiar, Donley T. and Ian McAllister. 2002. "Does
a Critical Mass Exist? A Comparative Analysis of
Women's Legislative Representation since 1950."
European Journal of Political Research 41:233-53.</mixed-citation>
         </ref>
         <ref id="d181e1981a1310">
            <mixed-citation id="d181e1985" publication-type="other">
Swiss, Liam. 2009. "Decoupling Values from Action:
An Event-History Analysis of the Election of
Women to Parliament in the Developing World,
1945-90." International Journal of Comparative
Sociology 50:69-95.</mixed-citation>
         </ref>
         <ref id="d181e2004a1310">
            <mixed-citation id="d181e2008" publication-type="other">
Tinker, Irene. 2004. "Quotas for Women in Elected
Legislatures: Do They Really Empower Women?"
Women's Studies International Forum 27:531-47.</mixed-citation>
         </ref>
         <ref id="d181e2021a1310">
            <mixed-citation id="d181e2025" publication-type="other">
Tripp, Aili Mari. 1994. "Gender, Political Participation
and the Transformation of Association Life in
Uganda and Tanzania." African Studies Review
37:107-131.</mixed-citation>
         </ref>
         <ref id="d181e2041a1310">
            <mixed-citation id="d181e2045" publication-type="other">
Tripp, Aili Mari, Isabel Casimiro, Joy Kwesiga, and
Alice Mungwa. 2009. Women 's Movements: Trans-
forming Political Landscapes. New York: Cam-
bridge University Press.</mixed-citation>
         </ref>
         <ref id="d181e2061a1310">
            <mixed-citation id="d181e2065" publication-type="other">
Tripp, Aili Mari and Alice Kang. 2008. "The Global
Impact of Quotas: On the Fast Track to Increased
Female Legislative Representation." Comparative
Political Studies 41:338-61.</mixed-citation>
         </ref>
         <ref id="d181e2081a1310">
            <mixed-citation id="d181e2085" publication-type="other">
United Nations. 1996. Report of the Fourth World Con-
ference on Women. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d181e2096a1310">
            <mixed-citation id="d181e2102" publication-type="other">
United Nations. 2006. "Declarations, Reservations, Ob-
jections, and Notifications of Withdrawal of
Reservations Relating to the Convention on the
Elimination of all Forms of Discrimination against
Women." Item 6 of the Provisional Agenda of the
Fourteenth Meeting of States Parties to the Conven-
tion on the Elimination of all Forms of Discrimina-
tion against Women.</mixed-citation>
         </ref>
         <ref id="d181e2131a1310">
            <mixed-citation id="d181e2135" publication-type="other">
United Nations Education, Scientific, and Cultural Orga-
nization (UNESCO). 2011. "UNESCO Institute for
Statistics." Retrieved January 21, 2011 (http://
www.uis.unesco.org/Pages/default.aspx).</mixed-citation>
         </ref>
         <ref id="d181e2151a1310">
            <mixed-citation id="d181e2155" publication-type="other">
Vincent, Louise. 2004. "Quotas: Changing the Way
Things Look without Changing the Way Things
Are." Journal of Legislative Studies 10:71-97.</mixed-citation>
         </ref>
         <ref id="d181e2168a1310">
            <mixed-citation id="d181e2172" publication-type="other">
Viterna, Jocelyn. 2006. "Pulled, Pushed, and Persuaded:
Explaining Women's Mobilization into the Salva-
doran Guerrilla Army." American Journal of Sociol-
ogy 112:1-45.</mixed-citation>
         </ref>
         <ref id="d181e2188a1310">
            <mixed-citation id="d181e2192" publication-type="other">
Viterna, Jocelyn and Kathleen Fallon. 2008. "Democra-
tization, Women's Movements, and Gender-Equita-
ble States: A Framework for Comparison."
American Sociological Review 73:668-89.</mixed-citation>
         </ref>
         <ref id="d181e2208a1310">
            <mixed-citation id="d181e2212" publication-type="other">
Viterna, Jocelyn, Kathleen Fallon, and Jason Beckfield.
2008. "How Development Matters: A Research Note
on the Relationship between Development, Democ-
racy, and Women's Legislative Representation."
International Journal of Comparative Sociology
49:455-76.</mixed-citation>
         </ref>
         <ref id="d181e2236a1310">
            <mixed-citation id="d181e2240" publication-type="other">
Watson, Peggy. 1993. "The Rise of Masculinism in
Eastern Europe." New Left Review 198:71-82.</mixed-citation>
         </ref>
         <ref id="d181e2250a1310">
            <mixed-citation id="d181e2254" publication-type="other">
Waylen, Georgina. 1994. "Women and Democratiza-
tion: Conceptualizing Gender Relations in Transition
Politics." World Politics 46:327-55.</mixed-citation>
         </ref>
         <ref id="d181e2267a1310">
            <mixed-citation id="d181e2271" publication-type="other">
Waylen, Georgina. 2000. "Gender and Democratic Pol-
itics: A Comparative Analysis of Consolidation in
Argentina and Chile." Journal of Latin American
Studies 32:765-93.</mixed-citation>
         </ref>
         <ref id="d181e2287a1310">
            <mixed-citation id="d181e2291" publication-type="other">
Waylen, Georgina. 2007. Engendering Transitions:
Women's Mobilization, Institutions, and Gender
Outcomes . Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d181e2304a1310">
            <mixed-citation id="d181e2308" publication-type="other">
Wooldridge, Jeffrey M. 2002. Econometric Analysis of
Cross Section and Panel Data. Cambridge, MA:
MIT Press.</mixed-citation>
         </ref>
         <ref id="d181e2321a1310">
            <mixed-citation id="d181e2325" publication-type="other">
Wooldridge, Jeffrey M. 2009. Introductory Economet-
rics: A Modern Approach. Manson, OH: South-
western.</mixed-citation>
         </ref>
         <ref id="d181e2339a1310">
            <mixed-citation id="d181e2343" publication-type="other">
Yoon, Mi Yung. 2001. "Democratization and Women's
Legislative Representation in Sub-Saharan Africa."
Democracy 8:169-90.</mixed-citation>
         </ref>
         <ref id="d181e2356a1310">
            <mixed-citation id="d181e2360" publication-type="other">
Yoon, Mi Yung. 2004. "Explaining Women's Legisla-
tive Representation in Sub-Saharan Africa." Legisla-
tive Studies Quarterly 29:447-68.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
