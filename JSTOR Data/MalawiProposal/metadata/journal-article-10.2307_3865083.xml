<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">epidinfe</journal-id>
         <journal-id journal-id-type="jstor">j100820</journal-id>
         <journal-title-group>
            <journal-title>Epidemiology and Infection</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">09502688</issn>
         <issn pub-type="epub">14694409</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3865083</article-id>
         <title-group>
            <article-title>The Age-Specific Prevalence of Human Parvovirus Immunity in Victoria, Australia Compared with Other Parts of the World</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>H. A.</given-names>
                  <surname>Kelly</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>D.</given-names>
                  <surname>Siebert</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>R.</given-names>
                  <surname>Hammond</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J.</given-names>
                  <surname>Leydon</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>P.</given-names>
                  <surname>Kiely</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>W.</given-names>
                  <surname>Maskill</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2000</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">124</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i294835</issue-id>
         <fpage>449</fpage>
         <lpage>457</lpage>
         <page-range>449-457</page-range>
         <permissions>
            <copyright-statement>Copyright 2000 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3865083"/>
         <abstract>
            <p>The age-specific immunity to human parvovirus infection was estimated in Victoria, Australia using prospectively collected samples from the Royal Children's Hospital, the Royal Women's Hospital and the Australian Red Cross Blood Service and from sera stored at the Victorian Infectious Diseases Reference Laboratory (VIDRL). All testing was performed at VIDRL using a commercial enzyme-linked immunosorbent assay (Biotrin). Of the 824 sera tested, 28% of those drawn from people aged 0-9 years contained protective antibodies to human parvovirus. This rose to 51% in the next decade of life. There was then a slow rise to about 78% immunity over 50 years of age. An analysis of all requests for parvovirus serology at VIDRL from 1992 to 1998 suggested that parvovirus tended to occur in 4-year cycles, with 2 epidemic years followed by 2 endemic years. A review of published reports of parvovirus immunity suggested that parvovirus infection may be more common, with a correspondingly higher proportion of the community immune, in temperate as opposed to tropical countries.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d547e181a1310">
            <label>1</label>
            <mixed-citation id="d547e188" publication-type="journal">
Anderson MJ, Jones SE, Fisher-Hoch SP, et al. Human
parvovirus, the cause of erythema infectiosum (fifth
disease)? Lancet1983; 1: 1378.<person-group>
                  <string-name>
                     <surname>Anderson</surname>
                  </string-name>
               </person-group>
               <fpage>1378</fpage>
               <volume>1</volume>
               <source>Lancet</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d547e223a1310">
            <label>2</label>
            <mixed-citation id="d547e230" publication-type="journal">
Ozawa K, Kurtzman G, Young N. Productive infection
by B19 parvovirus of human erythroid bone marrow
cells in vitro. Blood1987; 70: 384-91.<person-group>
                  <string-name>
                     <surname>Ozawa</surname>
                  </string-name>
               </person-group>
               <fpage>384</fpage>
               <volume>70</volume>
               <source>Blood</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d547e265a1310">
            <label>3</label>
            <mixed-citation id="d547e272" publication-type="journal">
Mustafa MM, McClain KL. Diverse haematological
effects of parvovirus B19 infection. Pediatr Clin Nth
Am1996; 43: 809-21.<person-group>
                  <string-name>
                     <surname>Mustafa</surname>
                  </string-name>
               </person-group>
               <fpage>809</fpage>
               <volume>43</volume>
               <source>Pediatr Clin Nth Am</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d547e307a1310">
            <label>4</label>
            <mixed-citation id="d547e314" publication-type="journal">
Shirono K, Tsuda H. Parvovirus B19-associated haemo-
phagocytic syndrome in healthy adults. Br J Haematol
1995; 89: 923-6.<person-group>
                  <string-name>
                     <surname>Shirono</surname>
                  </string-name>
               </person-group>
               <fpage>923</fpage>
               <volume>89</volume>
               <source>Br J Haematol</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d547e350a1310">
            <label>5</label>
            <mixed-citation id="d547e357" publication-type="journal">
Chorba T, Coccia P, Holman RC, et al. The role of
parvovirus in aplastic crisis and erythema infectiosum
(fifth disease). J Infect Dis1986; 154: 383-93.<person-group>
                  <string-name>
                     <surname>Chorba</surname>
                  </string-name>
               </person-group>
               <fpage>383</fpage>
               <volume>154</volume>
               <source>J Infect Dis</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d547e392a1310">
            <label>6</label>
            <mixed-citation id="d547e399" publication-type="journal">
Cohen B. Parvovirus B19: an expanding spectrum of
disease. BMJ1995; 311: 1549-52.<person-group>
                  <string-name>
                     <surname>Cohen</surname>
                  </string-name>
               </person-group>
               <fpage>1549</fpage>
               <volume>311</volume>
               <source>BMJ</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d547e431a1310">
            <label>7</label>
            <mixed-citation id="d547e438" publication-type="journal">
Miller E, Fairley CK, Cohen BJ, Seng C. Immediate
and long term outcome of human parvovirus B19
infection in pregnancy. Brit J Obstet Gynaecol1998;
105: 174-8.<person-group>
                  <string-name>
                     <surname>Miller</surname>
                  </string-name>
               </person-group>
               <fpage>174</fpage>
               <volume>105</volume>
               <source>Brit J Obstet Gynaecol</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d547e476a1310">
            <label>8</label>
            <mixed-citation id="d547e483" publication-type="journal">
Torok TJ. Parvovirus B19 and human disease. Adv
Intern Med1992; 37: 431-55.<person-group>
                  <string-name>
                     <surname>Torok</surname>
                  </string-name>
               </person-group>
               <fpage>431</fpage>
               <volume>37</volume>
               <source>Adv Intern Med</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d547e515a1310">
            <label>9</label>
            <mixed-citation id="d547e522" publication-type="journal">
Valeur-Jensen AK, Pedersen CB, Westergaard T, et al.
Risk factors for parvovirus B19 infection in pregnancy.
JAMA1999; 281: 1099-105.<person-group>
                  <string-name>
                     <surname>Valeur-Jensen</surname>
                  </string-name>
               </person-group>
               <fpage>1099</fpage>
               <volume>281</volume>
               <source>JAMA</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d547e557a1310">
            <label>10</label>
            <mixed-citation id="d547e564" publication-type="journal">
Adler SP, Manganello AM, Koch WC, Hempfling SH,
Best AM. Risk of human parvovirus B19 infections
among school and hospital employees during endemic
periods. J Infect Dis1993; 168: 361-8.<person-group>
                  <string-name>
                     <surname>Adler</surname>
                  </string-name>
               </person-group>
               <fpage>361</fpage>
               <volume>168</volume>
               <source>J Infect Dis</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d547e603a1310">
            <label>11</label>
            <mixed-citation id="d547e610" publication-type="journal">
Gillespie SM, Cartter ML, Asch S, et al. Occupational
risk of human parvovirus B19 infection for school and
day-care personnel during an outbreak of erythema
infectiosum. JAMA1990; 263: 2061-5.<person-group>
                  <string-name>
                     <surname>Gillespie</surname>
                  </string-name>
               </person-group>
               <fpage>2061</fpage>
               <volume>263</volume>
               <source>JAMA</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d547e648a1310">
            <label>12</label>
            <mixed-citation id="d547e655" publication-type="journal">
Cartter ML, Farley TA, Rosengren S, et al. Occu¬
pational risk factors for infection with parvovirus B19
among pregnant women. J Infect Dis1991; 163: 282-5.<person-group>
                  <string-name>
                     <surname>Cartter</surname>
                  </string-name>
               </person-group>
               <fpage>282</fpage>
               <volume>163</volume>
               <source>J Infect Dis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d547e690a1310">
            <label>13</label>
            <mixed-citation id="d547e697" publication-type="journal">
Bremner JAG, Cohen B. Parvovirus as a cause for
anaemia in human immunodeficiency virus-infected
patients. J Infect Dis1994; 169: 938-9.<person-group>
                  <string-name>
                     <surname>Bremner</surname>
                  </string-name>
               </person-group>
               <fpage>938</fpage>
               <volume>169</volume>
               <source>J Infect Dis</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d547e732a1310">
            <label>14</label>
            <mixed-citation id="d547e739" publication-type="journal">
Cassinotti P, Schultze D, Wieczorek K, Schonenberger
R, Siegl G. Parvovirus B19 infection during pregnancy
and development of hydrops fetalis despite the evidence
for pre-existing anti-B19 antibody: how reliable are
serological results? Clin Diag Virol1994; 2: 87-94.<person-group>
                  <string-name>
                     <surname>Cassinotti</surname>
                  </string-name>
               </person-group>
               <fpage>87</fpage>
               <volume>2</volume>
               <source>Clin Diag Virol</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d547e780a1310">
            <label>15</label>
            <mixed-citation id="d547e787" publication-type="journal">
Gay NJ, Hesketh LM, Cohen BJ, et al. Age specific
antibody prevalence to parvovirus B19; how many
women are infected in pregnancy? CDR1994; 4:
104-7.<person-group>
                  <string-name>
                     <surname>Gay</surname>
                  </string-name>
               </person-group>
               <fpage>104</fpage>
               <volume>4</volume>
               <source>CDR</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d547e825a1310">
            <label>16</label>
            <mixed-citation id="d547e832" publication-type="journal">
Cohen BJ, Buckley MM. The prevalence of antibody to
human parvovirus B19 in England and Wales. J Med
Microbiol1988; 25: 151-3.<person-group>
                  <string-name>
                     <surname>Cohen</surname>
                  </string-name>
               </person-group>
               <fpage>151</fpage>
               <volume>25</volume>
               <source>J Med Microbiol</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d547e868a1310">
            <label>17</label>
            <mixed-citation id="d547e877" publication-type="journal">
Kelly HA, Rae PB, Donnelly JK, Leydon JA. Fifth
diseases in a small rural community - what are the
consequences? Aust Fam Phys1999; 28: 139-45.<person-group>
                  <string-name>
                     <surname>Kelly</surname>
                  </string-name>
               </person-group>
               <fpage>139</fpage>
               <volume>28</volume>
               <source>Aust Fam Phys</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d547e912a1310">
            <label>18</label>
            <mixed-citation id="d547e919" publication-type="journal">
Letaief M, Vanham G, Boukef K, Yacoub S, Muylle L,
Mertens G. Higher prevalence of parvovirus B19 in
Belgian as compared to Tunisian blood donors:
differential implications for rirevention of transfusional
transmission. Transfus Sci1997; 18: 523-30.<person-group>
                  <string-name>
                     <surname>Letaief</surname>
                  </string-name>
               </person-group>
               <fpage>523</fpage>
               <volume>18</volume>
               <source>Transfus Sci</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d547e960a1310">
            <label>19</label>
            <mixed-citation id="d547e967" publication-type="journal">
Lefrere JJ, Courouce AM. Human parvovirus in¬
fections in France 1984^86. Lancet1987; 1: 166-7.<person-group>
                  <string-name>
                     <surname>Lefrere</surname>
                  </string-name>
               </person-group>
               <fpage>166</fpage>
               <volume>1</volume>
               <source>Lancet</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d547e999a1310">
            <label>20</label>
            <mixed-citation id="d547e1006" publication-type="journal">
Lin KH, You SL, Chen CJ, Wang CF, Yang CS,
Yamazaki S. Seroepidemiology of human parvovirus
B19 in Taiwan. J Med Virol1999; 57: 169-73.<person-group>
                  <string-name>
                     <surname>Lin</surname>
                  </string-name>
               </person-group>
               <fpage>169</fpage>
               <volume>57</volume>
               <source>J Med Virol</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1041a1310">
            <label>21</label>
            <mixed-citation id="d547e1048" publication-type="journal">
Lim WL, Wong KF, Lau CS. Parvovirus B19 infection
in Hong Kong. J Infect1997; 35: 247-9.<person-group>
                  <string-name>
                     <surname>Lim</surname>
                  </string-name>
               </person-group>
               <fpage>247</fpage>
               <volume>35</volume>
               <source>J Infect</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1080a1310">
            <label>22</label>
            <mixed-citation id="d547e1087" publication-type="journal">
Matsunaga Y, Goh KT, Utagawa E, Muroi N. Low
prevalence of antibody to human parvovirus B19 in
Singapore. Epidemiol Infect1994; 113: 537-40.<person-group>
                  <string-name>
                     <surname>Matsunaga</surname>
                  </string-name>
               </person-group>
               <fpage>537</fpage>
               <volume>113</volume>
               <source>Epidemiol Infect</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1123a1310">
            <label>23</label>
            <mixed-citation id="d547e1130" publication-type="journal">
Schoub BD, Blackburn NK, Johnson S, McAnerney
JM. Primary and secondary infection with human
parvovirus B19 in pregnant women in South Africa. S
Afr Med J1993; 83: 505-6.<person-group>
                  <string-name>
                     <surname>Schoub</surname>
                  </string-name>
               </person-group>
               <fpage>505</fpage>
               <volume>83</volume>
               <source>S Afr Med J</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1168a1310">
            <label>24</label>
            <mixed-citation id="d547e1175" publication-type="journal">
Migasena S, Simasathien S, Desakom V, et al. Sero¬
prevalence of varicella-zoster virus antibody in
Thailand. Int J Infect Dis1997; 2: 26-30.<person-group>
                  <string-name>
                     <surname>Migasena</surname>
                  </string-name>
               </person-group>
               <fpage>26</fpage>
               <volume>2</volume>
               <source>Int J Infect Dis</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1210a1310">
            <label>25</label>
            <mixed-citation id="d547e1217" publication-type="journal">
Yamashita K, Matsunaga Y, Taylor-Wiedman J,
Yamazaki S. A significant age shift of the human
parvovirus B19 antibody prevalence among young
adults in Japan observed in a decade. Jpn J Med Sci
Biol1992; 45: 49-58.<person-group>
                  <string-name>
                     <surname>Yamashita</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>45</volume>
               <source>Jpn J Med Sci Biol</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1258a1310">
            <label>26</label>
            <mixed-citation id="d547e1265" publication-type="journal">
de Freitas RB, Wong D, Boswell F, et al. Prevalence of
human parvovirus (B19) and rubella virus infections in
urban and remote rural areas in northern Brazil. J Med
Virol1990;32:203-8.<person-group>
                  <string-name>
                     <surname>de Freitas</surname>
                  </string-name>
               </person-group>
               <fpage>203</fpage>
               <volume>32</volume>
               <source>J Med Virol</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1303a1310">
            <label>27</label>
            <mixed-citation id="d547e1310" publication-type="journal">
Nascimento JP, Buckley MM, Brown KE, Cohen BJ.
The prevalence of antibody to human parvovirus B19 in
Rio de Janeiro, Brazil. Rev Inst Med Trop Sao Paulo
1990; 32: 41-5.<person-group>
                  <string-name>
                     <surname>Nascimento</surname>
                  </string-name>
               </person-group>
               <fpage>41</fpage>
               <volume>32</volume>
               <source>Rev Inst Med Trop Sao Paulo</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1348a1310">
            <label>28</label>
            <mixed-citation id="d547e1355" publication-type="journal">
Mata Rebon M, Bartolome Husson C, Bernardez
Hermida I. Seroprevalence of anti-human parvovirus
B19 antibodies in a sample of blood donors in Galicia.
Enferm Infecc Microbiol Clin1998; 16: 25-7.<person-group>
                  <string-name>
                     <surname>Mata Rebon</surname>
                  </string-name>
               </person-group>
               <fpage>25</fpage>
               <volume>16</volume>
               <source>Enferm Infecc Microbiol Clin</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1394a1310">
            <label>29</label>
            <mixed-citation id="d547e1401" publication-type="journal">
Sodja I, Mrazova M, Smelhausova M, et al. Sero¬
prevalence of IgG antibodies against parvovirus B19 in
the population of the Czech Republic. Epidemiol
Mikrobiol Imunol1995; 44: 171-4.<person-group>
                  <string-name>
                     <surname>Sodja</surname>
                  </string-name>
               </person-group>
               <fpage>171</fpage>
               <volume>44</volume>
               <source>Epidemiol Mikrobiol Imunol</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1439a1310">
            <label>30</label>
            <mixed-citation id="d547e1446" publication-type="journal">
Schwarz TF, Hottentrager B, Roggendorf M. Pre¬
valence of antibodies to parvovirus B19 in selected
groups of patients and healthy individuals. Int J Med
Microbiol Virol Parasitol Infect Dis1992; 276: 437-42.<person-group>
                  <string-name>
                     <surname>Schwarz</surname>
                  </string-name>
               </person-group>
               <fpage>437</fpage>
               <volume>276</volume>
               <source>Int J Med Microbiol Virol Parasitol Infect Dis</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1484a1310">
            <label>31</label>
            <mixed-citation id="d547e1491" publication-type="journal">
Schwarz TF, Roggendorf M, Deinhardt F. Incidence of
parvovirus B19 infection. Seroepidemiologic studies.
Dtsch Med Wochenschr1987; 112: 1526-31.<person-group>
                  <string-name>
                     <surname>Schwarz</surname>
                  </string-name>
               </person-group>
               <fpage>1526</fpage>
               <volume>112</volume>
               <source>Dtsch Med Wochenschr</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1526a1310">
            <label>32</label>
            <mixed-citation id="d547e1533" publication-type="journal">
Schwarz TF, Jager G, Schlipkoter UA, et al. Parvovirus
B19 infections in Germany 1987-1988. Offentl Gesund-
heitswes 1990; 52: 53-7.<person-group>
                  <string-name>
                     <surname>Schwarz</surname>
                  </string-name>
               </person-group>
               <fpage>53</fpage>
               <volume>52</volume>
               <source>Parvovirus B</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1568a1310">
            <label>33</label>
            <mixed-citation id="d547e1575" publication-type="journal">
Eis-Hubinger AM, Dieck D, Schild R, Hansmann M,
Schneweis KE. Parvovirus B19 infection in pregnancy.
Intervirol1998; 41: 178-84.<person-group>
                  <string-name>
                     <surname>Eis-Hubinger</surname>
                  </string-name>
               </person-group>
               <fpage>178</fpage>
               <volume>41</volume>
               <source>Intervirol</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1610a1310">
            <label>34</label>
            <mixed-citation id="d547e1619" publication-type="journal">
Kyriazopoulou V, Simitsopoulou M, Bondis M, et al.
Human parvovirus B19: immunity of Greek females
and prenatal investigation of hydrops fetalis. Eur J
Obstet Gynecol Reprod Biol1997; 74: 157-60.<person-group>
                  <string-name>
                     <surname>Kyriazopoulou</surname>
                  </string-name>
               </person-group>
               <fpage>157</fpage>
               <volume>74</volume>
               <source>Eur J Obstet Gynecol Reprod Biol</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1658a1310">
            <label>35</label>
            <mixed-citation id="d547e1665" publication-type="journal">
Mauser-Bunschoten EP, Zaaijer HL, van Drimmelen
AA, et al. High prevalence of parvovirus B19 IgG
antibodies among Dutch hemophilia patients. Vox
Sang1998; 74: 225-7.<person-group>
                  <string-name>
                     <surname>Mauser-Bunschoten</surname>
                  </string-name>
               </person-group>
               <fpage>225</fpage>
               <volume>74</volume>
               <source>Vox Sang</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1703a1310">
            <label>36</label>
            <mixed-citation id="d547e1710" publication-type="journal">
Tsujimura M, Matsushita K, Shiraki H, Sato H, Okochi
K, Maeda Y. Human parvovirus B19 infection in blood
donors. Vox Saug1995; 69: 206-12.<person-group>
                  <string-name>
                     <surname>Tsujimura</surname>
                  </string-name>
               </person-group>
               <fpage>206</fpage>
               <volume>69</volume>
               <source>Vox Saug</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1745a1310">
            <label>37</label>
            <mixed-citation id="d547e1752" publication-type="journal">
Matsunaga Y, Takeda N, Yamazaki S, Kamata K,
Kurosawa D. Seroepidemiology of human parvovirus
B19 using recombinanat VP1+VP2 particle antigen.
Kansenshogaku Zasshi1995; 69: 1371-5.<person-group>
                  <string-name>
                     <surname>Matsunaga</surname>
                  </string-name>
               </person-group>
               <fpage>1371</fpage>
               <volume>69</volume>
               <source>Kansenshogaku Zasshi</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1790a1310">
            <label>38</label>
            <mixed-citation id="d547e1797" publication-type="journal">
Alsaeid K, Alsaeid M, Essa S, Dimitrov D, Pacsa A.
Seroprevalence of human parvovirus B19 in children of
a desert region. Ann Trop Paediatr1996; 16: 255-7.<person-group>
                  <string-name>
                     <surname>Alsaeid</surname>
                  </string-name>
               </person-group>
               <fpage>255</fpage>
               <volume>16</volume>
               <source>Ann Trop Paediatr</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1832a1310">
            <label>39</label>
            <mixed-citation id="d547e1839" publication-type="journal">
Schwarz TF, Gurtler LG, Zoulek G, Deinhardt F,
Roggendorf M. Seroprevalence of human parvovirus
B19 infection in Sao Tome and Principe, Malawi and
Mascarene Islands. Zentralbl Bakteriol1989; 271:
231-6.<person-group>
                  <string-name>
                     <surname>Schwarz</surname>
                  </string-name>
               </person-group>
               <fpage>231</fpage>
               <volume>271</volume>
               <source>Zentralbl Bakteriol</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1880a1310">
            <label>40</label>
            <mixed-citation id="d547e1887" publication-type="journal">
Rollag H, Patou G, Pattison JR, et al. Prevalence of
antibodies against parvovirus B19 in Norwegians with
congenital coagulation factor defects treated with
plasma products from small donor pools. Scand J Infect
Dis1991; 23: 675-9.<person-group>
                  <string-name>
                     <surname>Rollag</surname>
                  </string-name>
               </person-group>
               <fpage>675</fpage>
               <volume>23</volume>
               <source>Scand J Infect Dis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1929a1310">
            <label>41</label>
            <mixed-citation id="d547e1936" publication-type="journal">
Araujo FM, Koch MC, Araujo AR. Prevalence of
parvovirus B19 infection in Portugal. Blood Coag
Fibrinolysis1995; 6: 687.<person-group>
                  <string-name>
                     <surname>Araujo</surname>
                  </string-name>
               </person-group>
               <fpage>687</fpage>
               <volume>6</volume>
               <source>Blood Coag Fibrinolysis</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d547e1971a1310">
            <label>42</label>
            <mixed-citation id="d547e1978" publication-type="journal">
al-Frayh AR, Bahakim H, Kidess E, Ramia S. IgG and
IgM antibodies to human parvovirus B19 in the serum
of patients with a clinical diagnosis of infection with the
virus and in the general population of Saudi Arabia. J
Infect1993; 27: 51-5.<person-group>
                  <string-name>
                     <surname>al-Frayh</surname>
                  </string-name>
               </person-group>
               <fpage>51</fpage>
               <volume>27</volume>
               <source>J Infect</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d547e2019a1310">
            <label>43</label>
            <mixed-citation id="d547e2026" publication-type="journal">
Munoz S, Alonso MA, Fernandez MJ, Munoz JL,
Garcia-Rodriguez JA. Seroprevalence versus parvo-
virus B19 in blood donors. Enferm Infecc Microbiol
Clin1998; 16: 161-2.<person-group>
                  <string-name>
                     <surname>Munoz</surname>
                  </string-name>
               </person-group>
               <fpage>161</fpage>
               <volume>16</volume>
               <source>Enferm Infecc Microbiol Clin</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d547e2064a1310">
            <label>44</label>
            <mixed-citation id="d547e2071" publication-type="journal">
Skjolderbrand-Sparre L, Fridell E, Nyman M, Wahren
B. A prospective study of antibodies against parvovirus
B19 in pregnancy. Acta Obstset Gynecol Scand1996;
75: 336-9.<person-group>
                  <string-name>
                     <surname>Skjolderbrand-Sparre</surname>
                  </string-name>
               </person-group>
               <fpage>336</fpage>
               <volume>75</volume>
               <source>Acta Obstset Gynecol Scand</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
