<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">inteaffaroyainst</journal-id>
         <journal-id journal-id-type="jstor">j100185</journal-id>
         <journal-title-group>
            <journal-title>International Affairs (Royal Institute of International Affairs 1944-)</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00205850</issn>
         <issn pub-type="epub">14682346</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3569425</article-id>
         <title-group>
            <article-title>HIV / AIDS Financing: A Case for Improving the Quality and Quantity of Aid</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Nana K.</given-names>
                  <surname>Poku</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">82</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i282859</issue-id>
         <fpage>345</fpage>
         <lpage>358</lpage>
         <page-range>345-358</page-range>
         <permissions>
            <copyright-statement>Copyright 2006 International Affairs and Blackwell Publishing Ltd.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3569425"/>
         <abstract>
            <p>There is no doubt that increasing amounts of funding are needed to provide a full package of HIV / AIDS prevention, treatment and mitigation interventions to Africa. However, even the existing funding flows are posing considerable challenges at a national level. In the quest for rapid results, donors have too often chosen to alleviate the lack of local capacity by bringing in foreign technical assistance or building parallel systems for delivering commodities such as drugs that may not be sustainable over the long term once external assistance stops. Even when such interventions may be relevant, they do not address the biggest challenge, namely how to build up the capacity and the systems needed for large-scale implementation of the AIDS response. This article argues that to attain the needed efficacy in HIV / AIDS mitigation programmes, further sustainable increase in external financing is certainly required (particularly for treatment programmes), but even more important is the need to implement them.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d665e131a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d665e138" publication-type="other">
Rene Bonnel, 'HIV/AIDS</mixed-citation>
            </p>
         </fn>
         <fn id="d665e145a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d665e152" publication-type="journal">
David E. Bloom, David Canning and Jaypee Sevilla, 'The effect of health on economic growth: a
production function approach', World Development 32: I, 2004, pp. 1-13.<person-group>
                     <string-name>
                        <surname>Bloom</surname>
                     </string-name>
                  </person-group>
                  <issue>I</issue>
                  <fpage>1</fpage>
                  <volume>32</volume>
                  <source>World Development</source>
                  <year>2004</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d665e187a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d665e194" publication-type="journal">
Joshua A. Salomon, Daniel R. Hogan, John Stover, Karen A. Stanecki, Neff Walker, Peter D. Ghys and
Bernard Schwarlainder, 'Integrating HIV prevention and treatment: from slogans to impact', Public
Library of Science Medicine 2: I, Jan. 2005.<person-group>
                     <string-name>
                        <surname>Salomon</surname>
                     </string-name>
                  </person-group>
                  <issue>I</issue>
                  <volume>2</volume>
                  <source>Public Library of Science Medicine</source>
                  <year>2005</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d665e229a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d665e236" publication-type="journal">
D. Bradshaw, P. Groenewald, R. Laubscher, N. Nannan, B. Nojilana, R. Norman et al., 'Initial burden
of disease estimates for South Africa, 2000', South African Medical Journal 93, 2003, pp. 682-8.<person-group>
                     <string-name>
                        <surname>Bradshaw</surname>
                     </string-name>
                  </person-group>
                  <fpage>682</fpage>
                  <volume>93</volume>
                  <source>South African Medical Journal</source>
                  <year>2003</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d665e269a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d665e276" publication-type="journal">
D. Bourne, D. Bradshaw, P. Groenewald, R. Laubscher and N. Nannan, 'Identifying deaths from AIDS
in South Africa', AIDS 19, 2005, pp. 193-201.<person-group>
                     <string-name>
                        <surname>Bourne</surname>
                     </string-name>
                  </person-group>
                  <fpage>193</fpage>
                  <volume>19</volume>
                  <source>AIDS</source>
                  <year>2005</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d665e308a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d665e315" publication-type="other">
Under the assumption that the median time from infection to death is ten years, adult mortality would
increase by 7 per 1,000 or 100%. As the group aged 15-49 typically accounts for 20% of the number of
deaths in the absence of AIDS, the aggregate mortality would increase by 20%.</mixed-citation>
            </p>
         </fn>
         <fn id="d665e328a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d665e335" publication-type="other">
With an HIV prevalence of 2%, adult mortality rises from 7 to 27 deaths per I,ooo, an increase of 386%,
which translates into a 77% increase in the total demand for health services.</mixed-citation>
            </p>
         </fn>
         <fn id="d665e345a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d665e352" publication-type="book">
World Bank, Millennium Goals:from consensus to momentum, Global Monitoring Report 2005
(Washington DC: World Bank, 2005).<person-group>
                     <string-name>
                        <surname>Bank</surname>
                     </string-name>
                  </person-group>
                  <source>Millennium Goals:from consensus to momentum</source>
                  <year>2005</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d665e377a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d665e384" publication-type="book">
UNAIDS, Resource needs for an expanded response to AIDS in low and middle income countries (Geneva:
UNAIDS, 2005).<person-group>
                     <string-name>
                        <surname>UNAIDS</surname>
                     </string-name>
                  </person-group>
                  <source>Resource needs for an expanded response to AIDS in low and middle income countries</source>
                  <year>2005</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d665e409a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d665e416" publication-type="book">
Commission on HIV/AIDS and Governance in Africa, Antiretroviral treatment in resource limited settings:
economic considerations (Addis Ababa, 2005).<person-group>
                     <string-name>
                        <surname>Commission on HIV/AIDS</surname>
                     </string-name>
                  </person-group>
                  <source>Africa, Antiretroviral treatment in resource limited settings: economic considerations</source>
                  <year>2005</year>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
