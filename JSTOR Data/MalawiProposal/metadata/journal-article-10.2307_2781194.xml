<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">amerjsoci</journal-id>
         <journal-id journal-id-type="jstor">j100067</journal-id>
         <journal-title-group>
            <journal-title>American Journal of Sociology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00029602</issn>
         <issn pub-type="epub">15375390</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">2781194</article-id>
         <title-group>
            <article-title>Growth Effects of Foreign and Domestic Investment</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Glenn</given-names>
                  <surname>Firebaugh</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>1992</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">98</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i329110</issue-id>
         <fpage>105</fpage>
         <lpage>130</lpage>
         <page-range>105-130</page-range>
         <permissions>
            <copyright-statement>Copyright 1992 The University of Chicago</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2781194"/>
         <abstract>
            <p>Over tha past few years, numerous studies in sociology have concluded that foreign investment harms poor nations. These studies have focused on the coefficient for foreign capital stock, controlling for "flow" (new investment), and-defying logic-have inferred that a negative coefficient for stock reflects "dependency effects" that retard economic growth. Since capital stock is the denominator for investment rate, the greater the stock, the lower the investment rate, for a given level of new investment. Hence the negative coefficient for capital stock found in dependency studies indicates a beneficial investment effect, not a harmful effect. This point is demonstrated by reanalysis of the data used in dependency studies. In these data, foreign investment spurs growth. Earlier analysts have concluded otherwise only because their conclusions have belied their findings.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d78e120a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d78e127" publication-type="journal">
Boswell and Dixon (1990)  </mixed-citation>
            </p>
         </fn>
         <fn id="d78e136a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d78e143" publication-type="journal">
Bornschier (1980)  </mixed-citation>
            </p>
         </fn>
         <fn id="d78e152a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d78e161" publication-type="partial">
Bornschier and Chase-Dunn fault Jackman (1982)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d78e167" publication-type="book">
Bornschier and Chase-Dunn 1985, p. 86  <fpage>86</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d78e179a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d78e186" publication-type="journal">
Bornschier et al. 1978  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d78e194" publication-type="journal">
Szymanski (1983)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d78e202" publication-type="journal">
Firebaugh and Gibbs 1985  </mixed-citation>
            </p>
         </fn>
         <fn id="d78e212a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d78e219" publication-type="partial">
BCD's (1985, p. 105, n. 20)</mixed-citation>
            </p>
         </fn>
         <fn id="d78e226a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d78e233" publication-type="journal">
Ballmer-Cao and Scheidegger 1979  </mixed-citation>
            </p>
         </fn>
         <fn id="d78e242a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d78e249" publication-type="partial">
Bornschier and Chase-Dunn estimate domestic flow by taking the average for three
years (1965</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d78e258" publication-type="partial">
1970</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d78e264" publication-type="partial">
1973)</mixed-citation>
            </p>
         </fn>
         <fn id="d78e271a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d78e278" publication-type="journal">
Crenshaw (1991, p. 1173)  <fpage>1173</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d78e290a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d78e297" publication-type="book">
Turner 1991, tables 12 and 13  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d78e305" publication-type="book">
Sassen 1988, p. 188  <fpage>188</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d78e317a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d78e324" publication-type="book">
Bornschier and Chase-
Dunn (1985)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d78e345a1310">
            <mixed-citation id="d78e349" publication-type="book">
Amin, Samir. 1974. Accumulation on a World Scale: A Critique of the Theory of
Underdevelopment, 2 vols. New York: Monthly Review.<person-group>
                  <string-name>
                     <surname>Amin</surname>
                  </string-name>
               </person-group>
               <source>Accumulation on a World Scale: A Critique of the Theory of Underdevelopment</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d78e374a1310">
            <mixed-citation id="d78e378" publication-type="book">
—1976. Unequal Development: An Essay on the Social Formations of Peripheral
Capitalism. New York: Monthly Review.<person-group>
                  <string-name>
                     <surname>Amin</surname>
                  </string-name>
               </person-group>
               <source>Unequal Development: An Essay on the Social Formations of Peripheral Capitalism</source>
               <year>1976</year>
            </mixed-citation>
         </ref>
         <ref id="d78e403a1310">
            <mixed-citation id="d78e407" publication-type="journal">
Ballmer-Cao, Thanh-Huyen, and Juerg Scheidegger. 1979. Compendium of Data for
World-System Analysis: Bulletin of the Sociological Institute of the University of
Zurich, March.<person-group>
                  <string-name>
                     <surname>Ballmer-Cao</surname>
                  </string-name>
               </person-group>
               <source>Compendium of Data for World-System Analysis: Bulletin of the Sociological Institute of the University of Zurich</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d78e436a1310">
            <mixed-citation id="d78e440" publication-type="book">
Barnet, Richard, and Ronald Mueller. 1974. Global Reach: The Power of Multina-
tional Corporations. New York: Simon &amp; Schuster.<person-group>
                  <string-name>
                     <surname>Barnet</surname>
                  </string-name>
               </person-group>
               <source>Global Reach: The Power of Multinational Corporations</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d78e466a1310">
            <mixed-citation id="d78e470" publication-type="journal">
Barrett, Richard E., and Martin K. Whyte. 1982. "Dependency Theory and Taiwan:
Analysis of a Deviant Case." American Journal of Sociology82:1064-89.<object-id pub-id-type="jstor">10.2307/2778418</object-id>
               <fpage>1064</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e486a1310">
            <mixed-citation id="d78e490" publication-type="journal">
Bornschier, Volker. 1980. "Multinational Corporations and Economic Growth: A
Cross-national Test of the Decapitalization Thesis." Journal of Development Eco-
nomics 7:191-210.<person-group>
                  <string-name>
                     <surname>Bornschier</surname>
                  </string-name>
               </person-group>
               <fpage>191</fpage>
               <volume>7</volume>
               <source>Journal of Development Economics</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d78e525a1310">
            <mixed-citation id="d78e529" publication-type="journal">
Bornschier, Volker, and Christopher Chase-Dunn. 1983. "Reply to Szymanski."
American Journal of Sociology89:694-99.<object-id pub-id-type="jstor">10.2307/2779013</object-id>
               <fpage>694</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e545a1310">
            <mixed-citation id="d78e549" publication-type="book">
. 1985. Transnational Corporations and Underdevelopment. New York:
Praeger.<person-group>
                  <string-name>
                     <surname>Bornschier</surname>
                  </string-name>
               </person-group>
               <source>Transnational Corporations and Underdevelopment</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d78e574a1310">
            <mixed-citation id="d78e578" publication-type="journal">
Bornschier, Volker, Christopher Chase-Dunn, and Richard Rubinson. 1978. "Cross-
national Evidence of the Effects of Foreign Investment and Aid on Economic
Growth and Inequality: A Survey of Findings and a Reanalysis." American Journal
of Sociology84:651-83.<object-id pub-id-type="jstor">10.2307/2778259</object-id>
               <fpage>651</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e601a1310">
            <mixed-citation id="d78e605" publication-type="journal">
Boswell, Terry, and William J. Dixon. 1990. "Dependency and Rebellion: A Cross-
national Analysis," American Sociological Review55:540-59.<object-id pub-id-type="doi">10.2307/2095806</object-id>
               <fpage>540</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e622a1310">
            <mixed-citation id="d78e626" publication-type="journal">
Bradshaw, York W.1988. "Reassessing Economic Dependency and Uneven Develop-
ment: The Kenyan Experience." American Sociological Review53:693-708.<object-id pub-id-type="doi">10.2307/2095816</object-id>
               <fpage>693</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e642a1310">
            <mixed-citation id="d78e646" publication-type="journal">
Crenshaw, Edward. 1991. "Foreign Investment as a Dependent Variable: Determi-
nants of Foreign Investment and Capital Penetration in Developing Nations, 1967-
1978." Social Forces69:1169-82.<object-id pub-id-type="doi">10.2307/2579307</object-id>
               <fpage>1169</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e665a1310">
            <mixed-citation id="d78e669" publication-type="journal">
Domar, Evsey. 1946. "Capital Expansion, Rate of Growth and Employment."
Econometrica14:137-47.<object-id pub-id-type="doi">10.2307/1905364</object-id>
               <fpage>137</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e685a1310">
            <mixed-citation id="d78e689" publication-type="journal">
.1947. "Expansion and Employment." American Economic Review37:34-55.<object-id pub-id-type="jstor">10.2307/1802857</object-id>
               <fpage>34</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e702a1310">
            <mixed-citation id="d78e706" publication-type="journal">
Firebaugh, Glenn, and Jack P. Gibbs. 1985. "User's Guide to Ratio Variables."
American Sociological Review50:713-22.<object-id pub-id-type="doi">10.2307/2095384</object-id>
               <fpage>713</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e722a1310">
            <mixed-citation id="d78e726" publication-type="book">
Gereffi, Gary. 1983. The Pharmaceutical Industry and Dependency in the Third
World. Princeton, N.J.: Princeton University Press.<person-group>
                  <string-name>
                     <surname>Gereffi</surname>
                  </string-name>
               </person-group>
               <source>The Pharmaceutical Industry and Dependency in the Third World</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d78e752a1310">
            <mixed-citation id="d78e756" publication-type="book">
Gillis, Malcolm, Dwight H. Perkins, Michael Roemer, and Donald Snodgrass. 1983.
Economics of Development. New York: Norton.<person-group>
                  <string-name>
                     <surname>Gillis</surname>
                  </string-name>
               </person-group>
               <source>Economics of Development</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d78e781a1310">
            <mixed-citation id="d78e785" publication-type="journal">
Harrod, Roy F.1939. "An Essay in Dynamic Theory," Economic Journal49:14-33.<object-id pub-id-type="doi">10.2307/2225181</object-id>
               <fpage>14</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e798a1310">
            <mixed-citation id="d78e802" publication-type="book">
Hirschman, Albert 0. 1958. The Strategy of Economic Development. New Haven,
Conn.: Yale University Press.<person-group>
                  <string-name>
                     <surname>Hirschman</surname>
                  </string-name>
               </person-group>
               <source>The Strategy of Economic Development</source>
               <year>1958</year>
            </mixed-citation>
         </ref>
         <ref id="d78e827a1310">
            <mixed-citation id="d78e831" publication-type="book">
—1977. "A Generalized Linkage Approach to Development, with Special Refer-
ence to Staples." Pp. 67-98 in Essays on Economic Development and Cultural
Change in Honor of B. F. Hoselitz, edited by Manning Nash. Chicago: University
of Chicago Press.<person-group>
                  <string-name>
                     <surname>Hirschman</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A Generalized Linkage Approach to Development, with Special Reference to Staples</comment>
               <fpage>67</fpage>
               <source>Essays on Economic Development and Cultural Change in Honor of B. F. Hoselitz</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d78e869a1310">
            <mixed-citation id="d78e873" publication-type="book">
Hymer, Stephen H.1979. The Multinational Corporation: A Radical Approach. Cam-
bridge: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Hymer</surname>
                  </string-name>
               </person-group>
               <source>The Multinational Corporation: A Radical Approach</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d78e898a1310">
            <mixed-citation id="d78e902" publication-type="journal">
Jackman, Robert W.1980. "A Note on the Measurement of Growth Rates in Cross-
national Research." American Journal of Sociology86:604-17.<object-id pub-id-type="jstor">10.2307/2778631</object-id>
               <fpage>604</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e919a1310">
            <mixed-citation id="d78e923" publication-type="journal">
.1982. "Dependence on Foreign Investment and Economic Growth in the
Third World." World Politics34:175-96.<object-id pub-id-type="doi">10.2307/2010262</object-id>
               <fpage>175</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e939a1310">
            <mixed-citation id="d78e943" publication-type="journal">
London, Bruce. 1987. "Structural Determinants of Third World Urban Change: An
Ecological and Political Economic Analysis." American Sociological Review
52:28-43.<object-id pub-id-type="doi">10.2307/2095390</object-id>
               <fpage>28</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e962a1310">
            <mixed-citation id="d78e966" publication-type="journal">
.1988. "Dependence, Distorted Development, and Fertility Trends in Noncore
Nations: A Structural Analysis of Cross-national Data." American Sociological
Review53:606-18.<object-id pub-id-type="doi">10.2307/2095852</object-id>
               <fpage>606</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e985a1310">
            <mixed-citation id="d78e989" publication-type="journal">
London, Bruce, and Thomas D. Robinson. 1989. "The Effect of International Depen-
dence on Income Inequality and Political Violence." American Sociological Review
54:305-8.<object-id pub-id-type="doi">10.2307/2095798</object-id>
               <fpage>305</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1008a1310">
            <mixed-citation id="d78e1012" publication-type="journal">
London, Bruce, and David A. Smith. 1988. "Urban Bias, Dependence, and Economic
Stagnation in Noncore Nations." American Sociological Review53:454-63.<object-id pub-id-type="doi">10.2307/2095652</object-id>
               <fpage>454</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1028a1310">
            <mixed-citation id="d78e1032" publication-type="journal">
London, Bruce, and Bruce A. Williams. 1988. "Multinational Corporate Penetration,
Protest, and Basic Needs Provision in Non,core Nations: A Cross-national Analy-
sis." Social Forces66:747-73.<object-id pub-id-type="doi">10.2307/2579574</object-id>
               <fpage>747</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1052a1310">
            <mixed-citation id="d78e1056" publication-type="journal">
.1990. "National Politics, International Dependency, and Basic Needs Provi-
sion: A Cross-national Study." Social Forces69:565-84.<object-id pub-id-type="doi">10.2307/2579674</object-id>
               <fpage>565</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1072a1310">
            <mixed-citation id="d78e1076" publication-type="journal">
O'Hearn, Denis. 1989. "The Irish Case of Dependency: An Exception to the Excep-
tions?" American Sociological Review54:578-96.<object-id pub-id-type="doi">10.2307/2095880</object-id>
               <fpage>578</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1092a1310">
            <mixed-citation id="d78e1096" publication-type="journal">
Papanek, Gustav F.1973. "Aid, Foreign Private Investment, Savings, and Growth
in Less Developed Countries." Journal of Political Economy81:120-30.<object-id pub-id-type="jstor">10.2307/1837329</object-id>
               <fpage>120</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1112a1310">
            <mixed-citation id="d78e1116" publication-type="book">
Sassen, Saskia. 1988. The Mobility of Labor and Capital. Cambridge: Cambridge
University Press.<person-group>
                  <string-name>
                     <surname>Sassen</surname>
                  </string-name>
               </person-group>
               <source>The Mobility of Labor and Capital</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1141a1310">
            <mixed-citation id="d78e1145" publication-type="journal">
Salvatore, Dominick. 1983. "A Simultaneous Equations Model of Trade and Develop-
ment with Dynamic Policy Simulations." Kyklos36:66-90.<person-group>
                  <string-name>
                     <surname>Salvatore</surname>
                  </string-name>
               </person-group>
               <fpage>66</fpage>
               <volume>36</volume>
               <source>Kyklos</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1177a1310">
            <mixed-citation id="d78e1181" publication-type="book">
Sprout, Ronald V. A., and James H. Weaver. 1990. "Exports and Economic Growth:
Neoclassical and Dependency Theory Contrasted." American University Depart-
ment of Economics Working Paper no. 130. Washington, D.C.<person-group>
                  <string-name>
                     <surname>Sprout</surname>
                  </string-name>
               </person-group>
               <source>Exports and Economic Growth: Neoclassical and Dependency Theory Contrasted</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1211a1310">
            <mixed-citation id="d78e1215" publication-type="journal">
Stoneman, Colin. 1975. "Foreign Capital and Economic Growth." World Develop-
ment3:11-26.<person-group>
                  <string-name>
                     <surname>Stoneman</surname>
                  </string-name>
               </person-group>
               <fpage>11</fpage>
               <volume>3</volume>
               <source>World Development</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1247a1310">
            <mixed-citation id="d78e1251" publication-type="journal">
Streeten, Paul. 1973. "The Multinational Enterprise and the Theory of Development
Policy." World Development1:1-14.<person-group>
                  <string-name>
                     <surname>Streeten</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>1</volume>
               <source>World Development</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1283a1310">
            <mixed-citation id="d78e1287" publication-type="journal">
Szymanski, Albert. 1983. "Comment on Bornschier, Chase-Dunn, and Rubinson."
American Journal of Sociology89:690-94.<object-id pub-id-type="jstor">10.2307/2779012</object-id>
               <fpage>690</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1303a1310">
            <mixed-citation id="d78e1307" publication-type="book">
Todaro, Michael P.1981. Economic Development in the Third World, 2d ed. New
York: Longman.<person-group>
                  <string-name>
                     <surname>Todaro</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Economic Development in the Third World</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1336a1310">
            <mixed-citation id="d78e1340" publication-type="book">
Turner, Philip. 1991. Capital Flows in the 1980s: A Survey of Major Trends. Eco-
nomic Paper no. 30. Basle: Bank for International Settlements.<person-group>
                  <string-name>
                     <surname>Turner</surname>
                  </string-name>
               </person-group>
               <source>Capital Flows in the 1980s: A Survey of Major Trends</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1365a1310">
            <mixed-citation id="d78e1369" publication-type="book">
Vernon, Raymond W.1971. Sovereignty at Bay: The Multinational Spread of U.S.
Enterprises. New York: Basic.<person-group>
                  <string-name>
                     <surname>Vernon</surname>
                  </string-name>
               </person-group>
               <source>Sovereignty at Bay: The Multinational Spread of U.S. Enterprises</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1395a1310">
            <mixed-citation id="d78e1399" publication-type="journal">
White, Lawrence J.1978. "The Evidence on Appropriate Factor Proportions for
Manufacturing in Less Developed Countries: A Survey." Economic Development
and Cultural Change27:27-59.<object-id pub-id-type="jstor">10.2307/1153311</object-id>
               <fpage>27</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1418a1310">
            <mixed-citation id="d78e1422" publication-type="journal">
Wimberley, Dale W.1990. "Investment Dependence and Alternative Explanations
of Third World Mortality: A Cross-national Study." American Sociological Review
55:75-91.<object-id pub-id-type="doi">10.2307/2095704</object-id>
               <fpage>75</fpage>
            </mixed-citation>
         </ref>
         <ref id="d78e1441a1310">
            <mixed-citation id="d78e1445" publication-type="journal">
—1991. "Transnational Corporate Investment and Food Consumption in the
Third World: A Cross-national Analysis." Rural Sociology56:406-31.<person-group>
                  <string-name>
                     <surname>Wimberley</surname>
                  </string-name>
               </person-group>
               <fpage>406</fpage>
               <volume>56</volume>
               <source>Rural Sociology</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d78e1477a1310">
            <mixed-citation id="d78e1481" publication-type="journal">
Wimberley, Dale W., and Rosario Bello. 1992. "Effects of Foreign Investment, Ex-
ports, and Economic Growth on Third World Food Consumption." Social Forces.
In press.<object-id pub-id-type="doi">10.2307/2580194</object-id>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
