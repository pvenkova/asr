<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">oxfoeconpape</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100310</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Oxford Economic Papers</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00307653</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14643812</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3488699</article-id>
         <title-group>
            <article-title>Equipment Investment and the Solow Model</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jonathan</given-names>
                  <surname>Temple</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">50</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i277706</issue-id>
         <fpage>39</fpage>
         <lpage>62</lpage>
         <page-range>39-62</page-range>
         <permissions>
            <copyright-statement>Copyright 1998 Oxford University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3488699"/>
         <abstract>
            <p>This paper investigates the correlation between equipment investment and economic growth, and its compatibility with the Solow growth model. The paper improves on previous work by starting from an explicit theoretical model, using recent data on human capital, taking a rigorous approach to outliers, using instrumental variables, and taking unobserved heterogeneity into account. Rates of return to investment, and their precision, are estimated. The main finding is that the implied returns to equipment investment are very high in developing countries.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d938e122a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d938e129" publication-type="other">
Rosenberg (1963)</mixed-citation>
            </p>
         </fn>
         <fn id="d938e136a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d938e143" publication-type="other">
Greenwood et aL (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d938e150a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d938e157" publication-type="other">
Greenwood et al. (1996)</mixed-citation>
            </p>
         </fn>
         <fn id="d938e164a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d938e171" publication-type="other">
DS (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d938e179a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d938e186" publication-type="other">
Benhabib and Spiegel (1994)</mixed-citation>
            </p>
         </fn>
         <fn id="d938e193a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d938e200" publication-type="other">
De Long and Summers (1992)</mixed-citation>
            </p>
         </fn>
         <fn id="d938e207a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d938e214" publication-type="other">
Lee (1995)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d938e230a1310">
            <mixed-citation id="d938e234" publication-type="journal">
Abel, A.B. (1992). 'Comments', Brooking Papers on Economic Activity, 200-5.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Abel</surname>
                  </string-name>
               </person-group>
               <fpage>200</fpage>
               <source>Brooking Papers on Economic Activity</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d938e259a1310">
            <mixed-citation id="d938e263" publication-type="book">
Auerbach, A.J., Hassett, K.A., and Oliner, S.D. (1993). 'Reassessing the Social Returns to
Equipment Investment', Working Paper No. 4405, NBER, Cambridge, MA.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Auerbach</surname>
                  </string-name>
               </person-group>
               <source>Reassessing the Social Returns to Equipment Investment</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d938e288a1310">
            <mixed-citation id="d938e292" publication-type="journal">
Auerbach, A.J., Hassett, K.A., and Oliner, S.D. (1994). 'Reassessing the Social Returns to
Equipment Investment', Quarterly Journal of Economics, 109, 789-802.<object-id pub-id-type="doi">10.2307/2118422</object-id>
               <fpage>789</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e308a1310">
            <mixed-citation id="d938e312" publication-type="journal">
Benhabib, J. and Jovanovic, B. (1991). 'Externalities and Growth Accounting', American
Economic Review, 81, 82-113.<object-id pub-id-type="jstor">10.2307/2006789</object-id>
               <fpage>82</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e329a1310">
            <mixed-citation id="d938e333" publication-type="journal">
Benhabib, J. and Spiegel, M.M. (1994). 'The Role of Human Capital in Economic
Development: Evidence from Aggregate Cross-country Data', Journal of Monetary
Economics, 34, 143-73.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Benhabib</surname>
                  </string-name>
               </person-group>
               <fpage>143</fpage>
               <volume>34</volume>
               <source>Journal of Monetary Economics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d938e368a1310">
            <mixed-citation id="d938e372" publication-type="journal">
Bernard, A.B. and Jones, C.I. (1996). 'Productivity across Industries and Countries: Time
Series Theory and Evidence', Review of Economics and Statistics, 135-46.<object-id pub-id-type="doi">10.2307/2109853</object-id>
               <fpage>135</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e388a1310">
            <mixed-citation id="d938e392" publication-type="journal">
Berndt, E.R. and Christensen, L.R. (1973). 'The Translog Function and the Substitution of
Equipment, Structures, and Labor in US Manufacturing 1929-68', Journal of Econometrics, 1,
81-114.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Berndt</surname>
                  </string-name>
               </person-group>
               <fpage>81</fpage>
               <volume>1</volume>
               <source>Journal of Econometrics</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d938e427a1310">
            <mixed-citation id="d938e431" publication-type="journal">
Blomstriim, M., Lipsey, R.E., and Zejan, M. (1996). 'Is Fixed Investment the Key to
Economic Growth?', Quarterly Journal of Economics, 111, 269-76.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Blomstriim</surname>
                  </string-name>
               </person-group>
               <fpage>269</fpage>
               <volume>111</volume>
               <source>Quarterly Journal of Economics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d938e463a1310">
            <mixed-citation id="d938e467" publication-type="journal">
Bound, J., Jaeger, D.A., and Baker, R.M. (1995). 'Problems with Instrumental Variables
Estimation when the Correlation between the Instruments and the Endogenous
Explanatory Variable is Weak', Journal of the American Statistical Association, 90, 443-50.<object-id pub-id-type="doi">10.2307/2291055</object-id>
               <fpage>443</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e486a1310">
            <mixed-citation id="d938e490" publication-type="journal">
Carroll, C.D. and Weil, D.N. (1994). 'Saving and Growth: a Reinterpretation', Carnegie-
Rochester Conference Series on Public Policy, 40, 133-92.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Carroll</surname>
                  </string-name>
               </person-group>
               <fpage>133</fpage>
               <volume>40</volume>
               <source>CarnegieRochester Conference Series on Public Policy</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d938e523a1310">
            <mixed-citation id="d938e527" publication-type="journal">
Caselli, F., Esquivel, G., and Lefort, F. (1996). 'Reopening the Convergence Debate: a New
Look at Cross-country Growth Empirics', Journal of Economic Growth, 1, 363-90.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Caselli</surname>
                  </string-name>
               </person-group>
               <fpage>363</fpage>
               <volume>1</volume>
               <source>Journal of Economic Growth</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d938e559a1310">
            <mixed-citation id="d938e563" publication-type="book">
Davidson, R. and MacKinnon, J.G. (1993). Estimation and Inference in Econometrics, Oxford
University Press, Oxford.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Davidson</surname>
                  </string-name>
               </person-group>
               <source>Estimation and Inference in Econometrics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d938e588a1310">
            <mixed-citation id="d938e592" publication-type="journal">
De Long, J.B. and Summers, L.H. (1991). 'Equipment Investment and Economic Growth',
Quarterly Journal of Economics, 106, 445-502.<object-id pub-id-type="doi">10.2307/2937944</object-id>
               <fpage>445</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e608a1310">
            <mixed-citation id="d938e612" publication-type="journal">
De Long, J.B. and Summers, L.H. (1992). 'Equipment Investment and Economic Growth:
How Strong is the Nexus?', Brooking Papers on Economic Activity, 157-99.<object-id pub-id-type="doi">10.2307/2534583</object-id>
               <fpage>157</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e628a1310">
            <mixed-citation id="d938e632" publication-type="journal">
De Long, J.B. and Summers, L.H. (1993). 'How Strongly do Developing Countries Benefit
from Equipment Investment?', Journal of Monetary Economics, 32, 395-415.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>De Long</surname>
                  </string-name>
               </person-group>
               <fpage>395</fpage>
               <volume>32</volume>
               <source>Journal of Monetary Economics</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d938e664a1310">
            <mixed-citation id="d938e668" publication-type="book">
De Long, J.B. and Summers, L.H. (1994a). 'How Robust is the Growth-Machinery Nexus?',
in M. Baldassarri, L. Paganetto, and E.S. Phelps (eds), International Differences in Growth
Rates, St. Martin's Press, New York, NY.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>De Long</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">How Robust is the Growth-Machinery Nexus?</comment>
               <source>International Differences in Growth Rates</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d938e701a1310">
            <mixed-citation id="d938e705" publication-type="journal">
De Long, J.B. and Summers, L.H. (1994b). 'Equipment Investment and Economic Growth:
Reply', Quarterly Journal of Economics, 109, 803-7.<object-id pub-id-type="doi">10.2307/2118423</object-id>
               <fpage>803</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e721a1310">
            <mixed-citation id="d938e725" publication-type="journal">
Denny, M. and May, D. (1978). 'A Representation of Canadian Manufacturing Technology',
Applied Economics, 10, 305-17.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Denny</surname>
                  </string-name>
               </person-group>
               <fpage>305</fpage>
               <volume>10</volume>
               <source>Applied Economics</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d938e757a1310">
            <mixed-citation id="d938e761" publication-type="other">
Easterly, W. and Levine, R. (1996). 'Africa's Growth Tragedy: Policies and Ethnic Divisions',
manuscript, World Bank, April.</mixed-citation>
         </ref>
         <ref id="d938e771a1310">
            <mixed-citation id="d938e775" publication-type="journal">
Greenwood, J., Hercowitz, Z., and Krusell, P. (1996). 'Long-run Implications of Investment-
specific Technological Change', Working paper No. 420, Rochester Center for Economic
Research, American Economic Review, forthcoming.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Greenwood</surname>
                  </string-name>
               </person-group>
               <source>American Economic Review</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d938e804a1310">
            <mixed-citation id="d938e808" publication-type="other">
Griliches, Z. and Mairesse, J. (1995). 'Production Functions: the Search for Identification',
manuscript, NBER.</mixed-citation>
         </ref>
         <ref id="d938e818a1310">
            <mixed-citation id="d938e822" publication-type="journal">
Islam, N. (1995). 'Growth Empirics: a Panel Data Approach', Quarterly Journal of Economics,
110, 1,127-70.<object-id pub-id-type="doi">10.2307/2946651</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e839a1310">
            <mixed-citation id="d938e843" publication-type="journal">
Jones, C.I. (1994). 'Economic Growth and the Relative Price of Capital', Journal of Monetary
Economics, 34, 359-82.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Jones</surname>
                  </string-name>
               </person-group>
               <fpage>359</fpage>
               <volume>34</volume>
               <source>Journal of Monetary Economics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d938e875a1310">
            <mixed-citation id="d938e879" publication-type="journal">
Klepper, S. (1988). 'Regressor Diagnostics for the Classical Errors-in-Variables Model',
Journal of Econometrics, 37, 225-50.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Klepper</surname>
                  </string-name>
               </person-group>
               <fpage>225</fpage>
               <volume>37</volume>
               <source>Journal of Econometrics</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d938e911a1310">
            <mixed-citation id="d938e915" publication-type="journal">
Klepper, S., Kamlet, M.S., and Frank, R.G. (1993). 'Regressor Diagnostics for the Errors-in-
Variables Model-an Application to the Health Effects of Pollution', Journal of
Environmental Economics and Management, 34, 190-211.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Klepper</surname>
                  </string-name>
               </person-group>
               <fpage>190</fpage>
               <volume>34</volume>
               <source>Journal of Environmental Economics and Management</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d938e950a1310">
            <mixed-citation id="d938e954" publication-type="journal">
Klepper, S. and Leamer, E.E. (1984). 'Consistent Sets of Estimates for Regressions with
Errors in all Variables', Econometrica, 52, 163-83.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Klepper</surname>
                  </string-name>
               </person-group>
               <fpage>163</fpage>
               <volume>52</volume>
               <source>Econometrica</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d938e986a1310">
            <mixed-citation id="d938e990" publication-type="book">
Koop, G., Osiewalski, J., and Steel, M.F.J. (1995). 'Measuring the Sources of Output Growth
in a Panel of Countries', Discussion Paper No. 9,542, CORE, Université Catholique de
Louvain, Louvain.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Koop</surname>
                  </string-name>
               </person-group>
               <source>Measuring the Sources of Output Growth in a Panel of Countries</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1019a1310">
            <mixed-citation id="d938e1023" publication-type="journal">
Lee, J.-W. (1995). 'Capital Goods Imports and Long-run Growth', Journal of Development
Economics, 48, 91-110.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <fpage>91</fpage>
               <volume>48</volume>
               <source>Journal of Development Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1056a1310">
            <mixed-citation id="d938e1060" publication-type="journal">
MacKinnon, J.G. and White, H. (1985). 'Some Heteroscedasticity-consistent Covariance
Matrix Estimators with Improved Finite Sample Properties', Journal of Econometrics, 29,
305-25.<object-id pub-id-type="jstor">10.2307/2096645</object-id>
               <fpage>305</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1079a1310">
            <mixed-citation id="d938e1083" publication-type="journal">
Maddison, A. (1987). 'Growth and Slowdown in Advanced Capitalist Economies:
Techniques of Quantitative Assessment', Journal of Economic Literature, 25, 649-98.<object-id pub-id-type="jstor">10.2307/2726106</object-id>
               <fpage>649</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1099a1310">
            <mixed-citation id="d938e1103" publication-type="journal">
Mankiw, N.G., Romer, D., and Weil, D.N. (1992). 'A Contribution to the Empirics of
Economic Growth', Quarterly Journal of Economics, 107, 407-37.<object-id pub-id-type="doi">10.2307/2118477</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1119a1310">
            <mixed-citation id="d938e1123" publication-type="journal">
Nehru, V., Swanson, E., and Dubey, A. (1995). 'A New Database on Human Capital Stock in
Developing and Industrial Countries: Sources, Methodology and Results', Journal of
Development Economics, 46, 379-401.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Nehru</surname>
                  </string-name>
               </person-group>
               <fpage>379</fpage>
               <volume>46</volume>
               <source>Journal of Development Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1158a1310">
            <mixed-citation id="d938e1162" publication-type="journal">
Nelson, C.R. and Startz, R. (1990a). 'The Distribution of the Instrumental Variable Estimator
and its t-Ratio when the Instrument is a Poor One', Journal of Business, 63, S125-40.<object-id pub-id-type="jstor">10.2307/2353264</object-id>
               <fpage>S125</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1178a1310">
            <mixed-citation id="d938e1182" publication-type="journal">
Nelson, C.R. and Startz, R. (1990b), 'Some Further Results on the Exact Small Sample
Properties of the Instrumental Variables Estimator', Econometrica, 58, 967-76.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Nelson</surname>
                  </string-name>
               </person-group>
               <fpage>967</fpage>
               <volume>58</volume>
               <source>Econometrica</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1215a1310">
            <mixed-citation id="d938e1219" publication-type="book">
Romer, D. (1996). Advanced Macroeconomics, McGraw-Hill, New York, NY.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Romer</surname>
                  </string-name>
               </person-group>
               <source>Advanced Macroeconomics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1241a1310">
            <mixed-citation id="d938e1245" publication-type="journal">
Romer, P.M. (1986). 'Increasing Returns and Long Run Growth', Journal of Political
Economy, 94, 1,002-37.<object-id pub-id-type="jstor">10.2307/1833190</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1261a1310">
            <mixed-citation id="d938e1265" publication-type="journal">
Rosenberg, N. (1963). 'Capital Goods, Technology, and Economic Growth', Oxford
Economic Papers, 15, 217-27.<object-id pub-id-type="jstor">10.2307/2661716</object-id>
               <fpage>217</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1281a1310">
            <mixed-citation id="d938e1285" publication-type="book">
Rousseeuw, P.J. and Leroy, A.M. (1987). Robust Regression and Outlier Detection, Wiley,
New York, NY.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Rousseeuw</surname>
                  </string-name>
               </person-group>
               <source>Robust Regression and Outlier Detection</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1310a1310">
            <mixed-citation id="d938e1314" publication-type="book">
Sachs, J.D. and Warner, A.M. (1995). 'Natural Resource Abundance and Economic Growth',
Working Paper No. 5,398, NBER, Cambridge, MA.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Sachs</surname>
                  </string-name>
               </person-group>
               <source>Natural Resource Abundance and Economic Growth</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1339a1310">
            <mixed-citation id="d938e1343" publication-type="journal">
Sato, K. (1967). 'A Two-level Constant-elasticity-of-substitution Production Function',
Review of Economic Studies, 34, 201-18.<object-id pub-id-type="doi">10.2307/2296809</object-id>
               <fpage>201</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1360a1310">
            <mixed-citation id="d938e1364" publication-type="journal">
Solow, R.M. (1966). 'Review of Capital and Growth', American Economic Review, 56, 1,257-
60.<object-id pub-id-type="jstor">10.2307/1815314</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d938e1380a1310">
            <mixed-citation id="d938e1384" publication-type="journal">
Temple, J.R.W. and Voth, H.-J. (1996). 'Human Capital, Equipment Investment, and
Industrialization', Discussion Paper No. 116, Nuffield College, Oxford, European
Economic Review, forthcoming.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Temple</surname>
                  </string-name>
               </person-group>
               <source>European Economic Review</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1413a1310">
            <mixed-citation id="d938e1417" publication-type="journal">
Williams, M. and Kwon, J.K. (1982). 'Substitution of Equipment, Structures and Labour in
South Korea's Manufacturing Industry', Applied Economics, 14, 391-400.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Williams</surname>
                  </string-name>
               </person-group>
               <fpage>391</fpage>
               <volume>14</volume>
               <source>Applied Economics</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d938e1449a1310">
            <mixed-citation id="d938e1453" publication-type="journal">
Woodland, A.D. (1975). 'Substitution of Structures, Equipment and Labor in Canadian
Production', International Economic Review, 16, 171-87.<object-id pub-id-type="doi">10.2307/2525892</object-id>
               <fpage>171</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
