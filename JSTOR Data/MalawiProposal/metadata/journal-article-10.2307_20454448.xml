<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">studfamiplan</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100383</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Studies in Family Planning</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley-Blackwell</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00393665</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284465</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20454448</article-id>
         <title-group>
            <article-title>Fertility Transitions in Developing Countries: Progress or Stagnation?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John</given-names>
                  <surname>Bongaarts</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">39</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20454445</issue-id>
         <fpage>105</fpage>
         <lpage>110</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20454448"/>
         <abstract>
            <p>Over the past quarter-century, fertility has declined rapidly in many developing countries. Projections typically assume that this trend will continue until replacement level is reached. Recent evidence suggests, however, that ongoing fertility declines may have slowed or stalled in a number of countries in transition. This study examines the pace of fertility change in developing countries that have multiple Demographic and Health Surveys to determine whether ongoing transitions are decelerating or stalling. The main findings are that in sub-Saharan African countries, the average pace of decline in fertility was lower around 2000 than in the mid-1990s and that more than half the countries in transition in this region have stalled.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1142e141a1310">
            <label>3</label>
            <mixed-citation id="d1142e148" publication-type="other">
Ross and
his colleagues (2005)</mixed-citation>
            <mixed-citation id="d1142e157" publication-type="other">
United Nations (2003)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1142e173a1310">
            <mixed-citation id="d1142e177" publication-type="other">
Alexandratos, Nikos. 2005. "Countries with rapid population growth
and resource constraints: Issues of food, agriculture, and develop-
ment." Population and Development Review 31(2): 237-258.</mixed-citation>
         </ref>
         <ref id="d1142e190a1310">
            <mixed-citation id="d1142e194" publication-type="other">
Blanc, Ann K. 2004. "The role of conflict in the rapid fertility decline
in Eritrea and prospects for the future." Studies in Family Planning
35(4): 236-245.</mixed-citation>
         </ref>
         <ref id="d1142e207a1310">
            <mixed-citation id="d1142e211" publication-type="other">
Blanc, Ann K. and Amy O. Tsui. 2005. "The dilemma of past success:
Insiders' views on the future of the international family planning
movement." Studies in Family Planning 36(4): 263-276.</mixed-citation>
         </ref>
         <ref id="d1142e224a1310">
            <mixed-citation id="d1142e228" publication-type="other">
Bongaarts, John. 2006. "The causes of stalling fertility transitions." Stud-
ies in Family Planning 37(1): 1-16.</mixed-citation>
         </ref>
         <ref id="d1142e239a1310">
            <mixed-citation id="d1142e243" publication-type="other">
Bulatao, Rodolfo A. and Ronald D. Lee (eds.). 1983. Determinants of Fer-
tility in Developing Countries. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="d1142e253a1310">
            <mixed-citation id="d1142e257" publication-type="other">
Caldwell, John C. 1982. Theory of Fertility Decline. New York: Academic
Press.</mixed-citation>
         </ref>
         <ref id="d1142e267a1310">
            <mixed-citation id="d1142e271" publication-type="other">
Casterline, John B. 2001. "The pace of fertility transition: National pat-
terns in the second half of the twentieth century." In Global Fertility
Transition. Eds. Rodolfo A. Bulatao and John B. Casterline. Popula-
tion and Development Review, supplement to volume 27. New York:
Population Council. Pp. 17-52.</mixed-citation>
         </ref>
         <ref id="d1142e290a1310">
            <mixed-citation id="d1142e294" publication-type="other">
Cleland, John, Stan Bernstein, Alex Ezeh, Anibal Faundes, Anna Glasier,
and Jolene Innis. 2006. "Family planning: The unfinished agenda."
Lancet 368 (9,549): 1,810-1,827.</mixed-citation>
         </ref>
         <ref id="d1142e307a1310">
            <mixed-citation id="d1142e311" publication-type="other">
Demeny, Paul and Geoffrey McNicoll. 2006. "The political demography
of the world system, 2000-2050." In The Political Economy of Global
Population Change, 1950-2050. Eds. Paul Demeny and Geoffrey Mc-
Nicoll. Population and Development Review, supplement to volume
32. New York: Population Council. Pp. 254-287.</mixed-citation>
         </ref>
         <ref id="d1142e330a1310">
            <mixed-citation id="d1142e334" publication-type="other">
Easterlin, Richard A. 1975. "An economic framework for fertility analy-
sis." Studies in Family Planning 6(3): 54-63.</mixed-citation>
         </ref>
         <ref id="d1142e345a1310">
            <mixed-citation id="d1142e349" publication-type="other">
Eltigani, Eltigani. 2003. "Stalled fertility decline in Egypt, why?" Popula-
tion and Environment 25(1): 41-59.</mixed-citation>
         </ref>
         <ref id="d1142e359a1310">
            <mixed-citation id="d1142e363" publication-type="other">
Gendell, Murray. 1985. "Stalls in the Fertility Decline in Costa Rica, Ko-
rea, and Sri Lanka." World Bank Staff Working Paper No. 693. Wash-
ington, DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d1142e376a1310">
            <mixed-citation id="d1142e380" publication-type="other">
National Population Commission [Nigeria]. 2000. Nigeria Demographic
and Health Survey 1999. Calverton, MD: National Population Com-
mission and ORC/Macro.</mixed-citation>
         </ref>
         <ref id="d1142e393a1310">
            <mixed-citation id="d1142e397" publication-type="other">
National Research Council. 2000. Beyond Six Billion: Forecasting the
World's Population. Panel on Population Projections. Eds. John Bon-
gaarts and Rodolfo A. Bulatao, Committee on Population, Commis-
sion on Behavioral and Social Sciences and Education. Washington,
DC: National Academy Press.</mixed-citation>
         </ref>
         <ref id="d1142e416a1310">
            <mixed-citation id="d1142e420" publication-type="other">
Notestein, Frank W. 1953. "Economic problems of population change."
In Proceedings of the Eighth International Conference of Agricultural
Economics. London: Oxford University Press. Pp. 13-31.</mixed-citation>
         </ref>
         <ref id="d1142e433a1310">
            <mixed-citation id="d1142e437" publication-type="other">
Ross, John, Edward Abel, and Katherine Abel. 2004. "Plateaus during
the rise of contraceptive prevalence." International Family Planning
Perspectives 30(1): 39-44.</mixed-citation>
         </ref>
         <ref id="d1142e451a1310">
            <mixed-citation id="d1142e455" publication-type="other">
Ross, John, John Stover, and Demi Adelaja. 2005. Profiles for Family Plan-
ning and Reproductive Health Programs: 116 Countries. Second edition.
Glastonbury, CT: The Futures Group.</mixed-citation>
         </ref>
         <ref id="d1142e468a1310">
            <mixed-citation id="d1142e472" publication-type="other">
Shapiro, David and Tesfayi Gebreselassie. 2007. "Fertility transition in
sub-Saharan Africa: Falling and stalling." Paper presented at the
Annual Meeting of the Population Association of America, New
York, 28-31 March.</mixed-citation>
         </ref>
         <ref id="d1142e488a1310">
            <mixed-citation id="d1142e492" publication-type="other">
United Nations. 2002. "Fertility levels and trends in countries with in-
termediate levels of fertility." In Completing the Fertility Transition.
Report of the Expert Group Meeting on Completing the Fertility
Transition, Department of Economic and Social Affairs, Population
Division. ESA/P/WP.172/Rev.1. New York: United Nations.</mixed-citation>
         </ref>
         <ref id="d1142e511a1310">
            <mixed-citation id="d1142e515" publication-type="other">
—. 2003. Levels and Trends of Contraceptive Use as Assessed in 2002.
New York: United Nations Department of Economic and Social Af-
fairs. ST/ESA/SER.A/190.</mixed-citation>
         </ref>
         <ref id="d1142e528a1310">
            <mixed-citation id="d1142e532" publication-type="other">
—. 2007. World Population Prospects: The 2006 Revision. Department
of Economic and Social Affairs, Population Division. New York:
United Nations.</mixed-citation>
         </ref>
         <ref id="d1142e545a1310">
            <mixed-citation id="d1142e549" publication-type="other">
Westoff, Charles and Anne Cross. 2006. "The stall in the fertility transition
in Kenya." DHS Analytic Studies 9. Calverton, MD: ORC Macro.</mixed-citation>
         </ref>
         <ref id="d1142e560a1310">
            <mixed-citation id="d1142e564" publication-type="other">
World Bank. 2005. Economic Growth in the 1990s: Learning from a Decade
of Reform. Washington, DC: World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
