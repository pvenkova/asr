<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40307864</article-id>
         <article-id pub-id-type="pub-doi">10.1086/591539</article-id>
         <article-categories>
            <subj-group>
               <subject>Review Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Cryptosporidiosis in Children in Sub-Saharan Africa: A Lingering Challenge</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Siobhan M.</given-names>
                  <surname>Mor</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Saul</given-names>
                  <surname>Tzipori</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">47</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">7</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40012525</issue-id>
         <fpage>915</fpage>
         <lpage>921</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40307864"/>
         <abstract>
            <p>Hospital-and community-based studies in sub-Saharan Africa document a high prevalence of cryptosporidiosis in children aged 6-36 months, particularly among those who are malnourished or positive for human immunodeficiency virus (HIV) infection and during rainy seasons. This is despite advances in developed countries that have curbed the incidence of cryptosporidiosis in the general and HIV-positive populations. Transmission in sub-Saharan Africa appears to occur predominantly through an anthroponotic cycle. The preponderance of Cryptosporidium hominis, given its limited host range, and the dominance of the more ubiquitous Cryptosporidium parvum after coexposure to both species, however, suggest that the current knowledge of transmission is incomplete. Given the poor sanitation and hygiene, limited availability of antiretrovirals, and the high prevalence of cryptosporidiosis in children—independent of HIV infection—in this region, effective control measures for cryptosporidiosis are desperately needed. Molecular targets from the recently sequenced parasite genome should be exploited to develop an effective and safe treatment for children.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1395e147a1310">
            <label>1</label>
            <mixed-citation id="d1395e154" publication-type="other">
Lake IR, Nichols G, Bentham G, Harrison FC, Hunter PR, Kovats SR.
Cryptosporidiosis decline after regulation, England and Wales,
1989-2005. Emerg Infect Dis 2007; 13:623-5.</mixed-citation>
         </ref>
         <ref id="d1395e167a1310">
            <label>2</label>
            <mixed-citation id="d1395e174" publication-type="other">
Carr A, Marriott D, Field A, Vasak E, Cooper DA. Treatment of HIV-
1-associated microsporidiosis and cryptosporidiosis with combination
antiretroviral therapy. Lancet 1998; 351:256-61.</mixed-citation>
         </ref>
         <ref id="d1395e187a1310">
            <label>3</label>
            <mixed-citation id="d1395e194" publication-type="other">
Maggi P, Larocca AM, Quarto M, et al. Effect of antiretroviral therapy
on cryptosporidiosis and microsporidiosis in patients infected with
human immunodeficiency virus type 1. Eur J Clin Microbiol Infect
Dis 2000; 19:213-7.</mixed-citation>
         </ref>
         <ref id="d1395e210a1310">
            <label>4</label>
            <mixed-citation id="d1395e217" publication-type="other">
Valentiner-Branth P, Steinsland H, Fischer TK, et al. Cohort study of
Guiñean children: incidence, pathogenicity, conferred protection, and
attributable risk for enteropathogens during the first 2 years of life. J
Clin Microbiol 2003; 41:4238-45.</mixed-citation>
         </ref>
         <ref id="d1395e234a1310">
            <label>5</label>
            <mixed-citation id="d1395e241" publication-type="other">
Gatei W, Wamae CN, Mbae C, et al. Cryptosporidiosis: prevalence,
genotype analysis, and symptoms associated with infections in children
in Kenya. Am J Trop Med Hyg 2006; 75:78-82.</mixed-citation>
         </ref>
         <ref id="d1395e254a1310">
            <label>6</label>
            <mixed-citation id="d1395e261" publication-type="other">
Tumwine JK, Kekitiinwa A, Nabukeera N, et al. Cryptosporidium par-
vum in children with diarrhea in Mulago Hospital, Kampala, Uganda.
Am J Trop Med Hyg 2003; 68:710-5.</mixed-citation>
         </ref>
         <ref id="d1395e274a1310">
            <label>7</label>
            <mixed-citation id="d1395e281" publication-type="other">
Molbak K, Hojlyng N, Gottschau A, et al. Cryptosporidiosis in infancy
and childhood mortality in Guinea Bissau, West Africa. BMJ 1993;
307:417-20.</mixed-citation>
         </ref>
         <ref id="d1395e294a1310">
            <label>8</label>
            <mixed-citation id="d1395e301" publication-type="other">
Sodemann M, Jakobsen MS, Molbak K, Martins C, Aaby P. Episode-
specific risk factors for progression of acute diarrhoea to persistent
diarrhoea in West African children. Trans R Soc Trop Med Hyg 1999;93:
65-8.</mixed-citation>
         </ref>
         <ref id="d1395e317a1310">
            <label>9</label>
            <mixed-citation id="d1395e326" publication-type="other">
Molbak K, Andersen M, Aaby P, et al. Cryptosporidium infection in
infancy as a cause of malnutrition: a community study from Guinea-
Bissau, West Africa. Am J Clin Nutr 1997; 65:149-52.</mixed-citation>
         </ref>
         <ref id="d1395e339a1310">
            <label>10</label>
            <mixed-citation id="d1395e346" publication-type="other">
Checkley W, Epstein LD, Gilman RH, Black RE, Cabrera L, Sterling
CR. Effects of Cryptosporidium parvum infection in Peruvian children:
growth faltering and subsequent catch-up growth. Am J Epidemiol
1998; 148:497-506.</mixed-citation>
         </ref>
         <ref id="d1395e363a1310">
            <label>11</label>
            <mixed-citation id="d1395e370" publication-type="other">
Checkley W, Gilman RH, Epstein LD, et al. Asymptomatic and symp-
tomatic cryptosporidiosis: their acute effect on weight gain in Peruvian
children. Am J Epidemiol 1997; 145:156-63.</mixed-citation>
         </ref>
         <ref id="d1395e383a1310">
            <label>12</label>
            <mixed-citation id="d1395e390" publication-type="other">
Guerrant DI, Moore SR, Lima AA, Patrick PD, Schorling JB, Guerrant
RL. Association of early childhood diarrhea and cryptosporidiosis with
impaired physical fitness and cognitive function four-seven years later
in a poor urban community in northeast Brazil. Am J Trop Med Hyg
1999; 61:707-13.</mixed-citation>
         </ref>
         <ref id="d1395e409a1310">
            <label>13</label>
            <mixed-citation id="d1395e416" publication-type="other">
Amadi B, Kelly P, Mwiya M, et al. Intestinal and systemic infection,
HIV, and mortality in Zambian children with persistent diarrhea and
malnutrition. J Pediatr Gastroenterol Nutr 2001; 32:550-4.</mixed-citation>
         </ref>
         <ref id="d1395e429a1310">
            <label>14</label>
            <mixed-citation id="d1395e436" publication-type="other">
Henry MC, Alary M, Desmet P, et al. Community survey of diarrhoea
in children under 5 years in Kinshasa, Zaire. Ann Soc Belg Med Trop
1995; 75:105-14.</mixed-citation>
         </ref>
         <ref id="d1395e449a1310">
            <label>15</label>
            <mixed-citation id="d1395e456" publication-type="other">
Cegielski JP, Ortega YR, McKee S, et al. Cryptosporidium, Enterocyto-
zoon, and Cyclospora infections in pediatric and adult patients with
diarrhea in Tanzania. Clin Infect Dis 1999; 28:314-21.</mixed-citation>
         </ref>
         <ref id="d1395e469a1310">
            <label>16</label>
            <mixed-citation id="d1395e476" publication-type="other">
Heron H, Dufillot D, Duong TH, et al. Digestive cryptosporidiosis:
prospective study in Gabonese infants with diarrhoea. Ann Pediatr
(Paris) 1994; 41:225-9.</mixed-citation>
         </ref>
         <ref id="d1395e490a1310">
            <label>17</label>
            <mixed-citation id="d1395e497" publication-type="other">
Assefa T, Mohammed H, Abebe A, Abebe S, Tafesse B. Cryptospori-
diosis in children seen at the children's clinic of Yekatit 12 Hospital,
Addis Ababa. Ethiop Med I 1996; 34:43-5.</mixed-citation>
         </ref>
         <ref id="d1395e510a1310">
            <label>18</label>
            <mixed-citation id="d1395e517" publication-type="other">
Tumwine JK, Kekitiinwa A, Bakeera-Kitaka S, et al. Cryptosporidiosis
and microsporidiosis in Ugandan children with persistent diarrhea with
and without concurrent infection with the human immunodeficiency
virus. Am J Trop Med Hyg 2005; 73:921-5.</mixed-citation>
         </ref>
         <ref id="d1395e533a1310">
            <label>19</label>
            <mixed-citation id="d1395e540" publication-type="other">
Mersha D, Tiruneh M. Frequency of isolation of Cryptosporidium oo-
cysts in Ethiopian children with acute diarrhoeal disease. East Afr Med
J 1992; 69:314-5.</mixed-citation>
         </ref>
         <ref id="d1395e553a1310">
            <label>20</label>
            <mixed-citation id="d1395e560" publication-type="other">
Morse TD, Nichols RA, Grimason AM, Campbell BM, Tembo KC,
Smith HV. Incidence of cryptosporidiosis species in paediatric patients
in Malawi. Epidemiol Infect 2007; 135:1307-15.</mixed-citation>
         </ref>
         <ref id="d1395e573a1310">
            <label>21</label>
            <mixed-citation id="d1395e580" publication-type="other">
Chintu C, Luo C, Baboo S, et al. Intestinal parasites in HIV-seropositive
Zambian children with diarrhoea. J Trop Pediatr 1995; 41:149-52.</mixed-citation>
         </ref>
         <ref id="d1395e590a1310">
            <label>22</label>
            <mixed-citation id="d1395e597" publication-type="other">
Bogaerts J, Lepage P, Rouvroy D, et al. Cryptosporidiosis in Rwanda:
clinical and epidemiological features. Ann Soc Belg Med Trop 1987;
67:157-65.</mixed-citation>
         </ref>
         <ref id="d1395e611a1310">
            <label>23</label>
            <mixed-citation id="d1395e618" publication-type="other">
Simango C, Mutikani S. Cryptosporidiosis in Harare, Zimbabwe. Cent
Afr J Med 2004; 50:52-4.</mixed-citation>
         </ref>
         <ref id="d1395e628a1310">
            <label>24</label>
            <mixed-citation id="d1395e635" publication-type="other">
Berkowitz FE, Vallabh W, Buqwana A, Heney C. Cryptosporidiosis in
black South African children. S Afr Med J 1988; 74:272-3.</mixed-citation>
         </ref>
         <ref id="d1395e645a1310">
            <label>25</label>
            <mixed-citation id="d1395e652" publication-type="other">
Geyer A, Crewe-Brown HH, Greeff AS, et al. The microbial aetiology
of summer paediatric gastroenteritis at Ga-Rankuwa Hospital in South
Africa. East Afr Med J 1993; 70:78-81.</mixed-citation>
         </ref>
         <ref id="d1395e665a1310">
            <label>26</label>
            <mixed-citation id="d1395e672" publication-type="other">
Dlamini MS, Nkambule SJ, Grimason AM. First report of cryptos-
poridiosis in paediatric patients in Swaziland. Int J Environ Health Res
2005; 15:393-6.</mixed-citation>
         </ref>
         <ref id="d1395e685a1310">
            <label>27</label>
            <mixed-citation id="d1395e692" publication-type="other">
Nacro B, Bonkoungou P, Nagalo K, Tall FR, Curtis V. Clinical profile
of cryptosporidiosis in a pediatric hospital environment in Burkina
Faso [in French]. Med Trop (Mars) 1998; 58:47-50.</mixed-citation>
         </ref>
         <ref id="d1395e705a1310">
            <label>28</label>
            <mixed-citation id="d1395e712" publication-type="other">
Kassi RR, Kouassi RA, Yavo W, et al. Cryptosporidiosis and isosporiasis
in children suffering from diarrhoea in Abidjan. Bull Soc Pathol Exot
2004; 97:280-2.</mixed-citation>
         </ref>
         <ref id="d1395e726a1310">
            <label>29</label>
            <mixed-citation id="d1395e733" publication-type="other">
Adegbola RA, Demba E, De Veer G, Todd J. Cryptosporidium infection
in Gambian children less than 5 years of age. J Trop Med Hyg 1994;
97:103-7.</mixed-citation>
         </ref>
         <ref id="d1395e746a1310">
            <label>30</label>
            <mixed-citation id="d1395e753" publication-type="other">
Adjei AA, Armah H, Rodrigues O, et al. Cryptosporidium spp., a fre-
quent cause of diarrhea among children at the Korle-Bu Teaching
Hospital, Accra, Ghana. Ipn J Infect Dis 2004; 57:216-9.</mixed-citation>
         </ref>
         <ref id="d1395e766a1310">
            <label>31</label>
            <mixed-citation id="d1395e773" publication-type="other">
Addy PA, Aikins-Bekoe P. Cryptosporidiosis in diarrhoeal children in
Kumasi, Ghana. Lancet 1986; 1:735.</mixed-citation>
         </ref>
         <ref id="d1395e783a1310">
            <label>32</label>
            <mixed-citation id="d1395e790" publication-type="other">
Gay-Andrieu E, Adehossi E, Illa H, Garba Ben A, Kourna H, Boureima
H. Prevalence of cryptosporidiosis in pediatric hospital patients in
Niamey, Niger. Bull Soc Pathol Exot 2007; 100:193-6.</mixed-citation>
         </ref>
         <ref id="d1395e803a1310">
            <label>33</label>
            <mixed-citation id="d1395e810" publication-type="other">
Reinthaler FF, Hermentin K, Mascher F, Klem G, Sixl W. Cryptos-
poridiosis in Ogun State, south-west Nigeria. Trop Med Parasitol
1987; 38:51-2.</mixed-citation>
         </ref>
         <ref id="d1395e823a1310">
            <label>34</label>
            <mixed-citation id="d1395e830" publication-type="other">
Chappell CL, Okhuysen PC, Sterling CR, Wang C, Jakubowski W,
Dupont HL. Infectivity of Cryptosporidium parvum in healthy adults
with pre-existing anti–C parvum serum immunoglobulin G. Am J Trop
Med Hyg 1999; 60:157-64.</mixed-citation>
         </ref>
         <ref id="d1395e847a1310">
            <label>35</label>
            <mixed-citation id="d1395e854" publication-type="other">
Houpt ER, Bushen OY, Sam NE, et al. Short report: asymptomatic
Cryptosporidium hominis infection among human immunodeficiency
virus-infected patients in Tanzania. Am J Trop Med Hyg 2005; 73:
520-2.</mixed-citation>
         </ref>
         <ref id="d1395e870a1310">
            <label>36</label>
            <mixed-citation id="d1395e877" publication-type="other">
Duong TH, Dufillot D, Koko J, et al. Digestive cryptosporidiosis in
young children in an urban area in Gabon. Sante 1995; 5:185-8.</mixed-citation>
         </ref>
         <ref id="d1395e887a1310">
            <label>37</label>
            <mixed-citation id="d1395e894" publication-type="other">
Bushen OY, Kohli A, Pinkerton RC, et al. Heavy cryptosporidial in-
fections in children in northeast Brazil: comparison of Cryptosporidium
hominis dina Cryptosporidium parvum. Trans R Soc Trop Med Hyg
2007; 101:378-84.</mixed-citation>
         </ref>
         <ref id="d1395e910a1310">
            <label>38</label>
            <mixed-citation id="d1395e917" publication-type="other">
Hojlyng N, Molbak K, lepsen S. Cryptosporidium spp., a frequent cause
of diarrhea in Liberian children. J Clin Microbiol 1986; 23:1 109-13.</mixed-citation>
         </ref>
         <ref id="d1395e927a1310">
            <label>39</label>
            <mixed-citation id="d1395e934" publication-type="other">
Carstensen H, Hansen HL, Kristiansen HO, Gomme G. The epide-
miology of cryptosporidiosis and other intestinal parasitoses in children
in southern Guinea-Bissau. Trans R Soc Trop Med Hyg 1987; 81:860-4.</mixed-citation>
         </ref>
         <ref id="d1395e947a1310">
            <label>40</label>
            <mixed-citation id="d1395e954" publication-type="other">
Mata L, Bolanos H, Pizarro D, Vives M. Cryptosporidiosis in children
from some highland Costa Rican rural and urban areas. Am J Trop
Med Hyg 1984; 33:24-9.</mixed-citation>
         </ref>
         <ref id="d1395e968a1310">
            <label>41</label>
            <mixed-citation id="d1395e975" publication-type="other">
Nchito M, Kelly P, Sianongo S, et al. Cryptosporidiosis in urban Zam-
bian children: an analysis of risk factors. Am J Trop Med Hyg 1998;
59:435-7.</mixed-citation>
         </ref>
         <ref id="d1395e988a1310">
            <label>42</label>
            <mixed-citation id="d1395e995" publication-type="other">
Xiao L, Ryan UM. Cryptosporidiosis: an update in molecular epide-
miology. Curr Opin Infect Dis 2004; 17:483-90.</mixed-citation>
         </ref>
         <ref id="d1395e1005a1310">
            <label>43</label>
            <mixed-citation id="d1395e1012" publication-type="other">
Peng MM, Meshnick SR, Cunliffe NA, et al. Molecular epidemiology
of cryptosporidiosis in children in Malawi. J Eukaryot Microbiol
2003;50(Suppl):557-9.</mixed-citation>
         </ref>
         <ref id="d1395e1025a1310">
            <label>44</label>
            <mixed-citation id="d1395e1032" publication-type="other">
Sarnie A, Bessong PO, Obi CL, et al. Cryptosporidium species: prelim-
inary descriptions of the prevalence and genotype distribution among
school children and hospital patients in the Venda region, Limpopo
Province, South Africa. Exp Parasitol 2006; 114:314-22.</mixed-citation>
         </ref>
         <ref id="d1395e1048a1310">
            <label>45</label>
            <mixed-citation id="d1395e1055" publication-type="other">
Akiyoshi DE, Mor S, Tzipori S. Rapid displacement of Cryptosporidium
parvum type 1 by type 2 in mixed infections in piglets. Infect Immun
2003; 71:5765-71.</mixed-citation>
         </ref>
         <ref id="d1395e1068a1310">
            <label>46</label>
            <mixed-citation id="d1395e1075" publication-type="other">
Ajjampur SS, Gladstone BP, Selvapandian D, Muliyil IP, Ward H, Kang
G. Molecular and spatial epidemiology of cryptosporidiosis in children
in a semiurban community in South India. J Clin Microbiol 2007; 45:
915-20.</mixed-citation>
         </ref>
         <ref id="d1395e1092a1310">
            <label>47</label>
            <mixed-citation id="d1395e1099" publication-type="other">
Cama VA, Bern C, Sulaiman IM, et al. Cryptosporidium species and
genotypes in HIV-positive patients in Lima, Peru. J Eukaryot Microbiol
2003;50(Suppl):531-3.</mixed-citation>
         </ref>
         <ref id="d1395e1112a1310">
            <label>48</label>
            <mixed-citation id="d1395e1119" publication-type="other">
Sopwith W, Osborn K, Chalmers R, Regan M. The changing epide-
miology of cryptosporidiosis in North West England. Epidemiol Infect
2005; 133:785-93.</mixed-citation>
         </ref>
         <ref id="d1395e1132a1310">
            <label>49</label>
            <mixed-citation id="d1395e1139" publication-type="other">
World Health Organization. Progress in scaling up access to HIV treat-
ment in low-and middle-income countries, June 2006. Geneva, Swit-
zerland, World Health Organization, 2006.</mixed-citation>
         </ref>
         <ref id="d1395e1152a1310">
            <label>50</label>
            <mixed-citation id="d1395e1159" publication-type="other">
Rossignol JF, Ayoub A, Ayers MS. Treatment of diarrhea caused by
Cryptosporidium parvum: a prospective randomized, double-blind, pla-
cebo-controlled study of nitazoxanide. J Infect Dis 2001; 184:103-6.</mixed-citation>
         </ref>
         <ref id="d1395e1172a1310">
            <label>51</label>
            <mixed-citation id="d1395e1179" publication-type="other">
Rossignol JF, Kabil SM, el-Gohary Y, Younis AM. Effect of nitazoxanide
in diarrhea and enteritis caused by Cryptosporidium species. Clin Gas-
troenterol Hepatol 2006; 4:320-4.</mixed-citation>
         </ref>
         <ref id="d1395e1192a1310">
            <label>52</label>
            <mixed-citation id="d1395e1199" publication-type="other">
Amadi B, Mwiya M, Musuku J, et al. Effect of nitazoxanide on mor-
bidity and mortality in Zambian children with cryptosporidiosis: a
randomised controlled trial. Lancet 2002; 360:1375-80.</mixed-citation>
         </ref>
         <ref id="d1395e1213a1310">
            <label>53</label>
            <mixed-citation id="d1395e1220" publication-type="other">
Rossignol JF, Abu-Zekry M, Hussein A, Santoro MG. Effect of nita-
zoxanide for treatment of severe rotavirus diarrhoea: randomised dou-
ble-blind placebo-controlled trial. Lancet 2006; 368:124-9.</mixed-citation>
         </ref>
         <ref id="d1395e1233a1310">
            <label>54</label>
            <mixed-citation id="d1395e1240" publication-type="other">
Abubakar I, Aliyu SH, Arumugam C, Usman NK, Hunter PR. Treat-
ment of cryptosporidiosis in immunocompromised individuals: sys-
tematic review and meta-analysis. Br J Clin Pharmacol 2007; 63:387-93.</mixed-citation>
         </ref>
         <ref id="d1395e1253a1310">
            <label>55</label>
            <mixed-citation id="d1395e1262" publication-type="other">
Rossignol JF. Nitazoxanide in the treatment of acquired immune de-
ficiency syndrome-related cryptosporidiosis: results of the United
States compassionate use program in 365 patients. Aliment Pharmacol
Ther 2006; 24:887-94.</mixed-citation>
         </ref>
         <ref id="d1395e1278a1310">
            <label>56</label>
            <mixed-citation id="d1395e1285" publication-type="other">
Abrahamsen MS, Templeton TJ, Enomoto S, et al. Complete genome
sequence of the apicomplexan, Cryptosporidium parvum. Science
2004; 304:441-5.</mixed-citation>
         </ref>
         <ref id="d1395e1298a1310">
            <label>57</label>
            <mixed-citation id="d1395e1305" publication-type="other">
Xu P, Widmer G, Wang Y, et al. The genome of Cryptosporidium
hominis. Nature 2004; 431:1107-12.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
