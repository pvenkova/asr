<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">intejsoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000659</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>International Journal of Sociology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>M. E. Sharpe</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00207659</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15579336</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20628337</article-id>
         <article-id pub-id-type="pub-doi">10.2753/IJS0020-7659380303</article-id>
         <title-group>
            <article-title>Time and Country Variation in Contentious Politics: Multilevel Modeling of Dissent and Repression</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thomas V.</given-names>
                  <surname>Maher</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lindsey</given-names>
                  <surname>Peterson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">38</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20628332</issue-id>
         <fpage>52</fpage>
         <lpage>81</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 M.E.Sharpe, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20628337"/>
         <abstract>
            <p>Previous analyses of the relationship between dissent and repression have turned up mixed, and often conflicting, results. Although research on the effect of repression on dissent has been inconsistent, it becomes obvious that time and country variation does matter: the effects of dissent and repression do not occur in a social vacuum. Our analyses seek to determine what country-level contextual variables influence levels of nonviolent and violent dissent, as well as nonviolent and violent repression. We include a battery of variables describing domestic economic and political conditions, sociodemographics, and global linkages. We test specific hypotheses about these potential determinants of various forms of dissent and repression by using data on 530 event-weeks of the period 1994-2004 across 97 countries. We find that proximity to the center of the world polity network and capacity for state terror have an effect on both dissent and repression, and international, political, and economic factors have an impact when dissent and repression are broken down by violence.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d606e131a1310">
            <p>
               <mixed-citation id="d606e135" publication-type="other">
A version of this article was presented as a paper at the American Sociological Association
Meeting in Boston, August 1-4,2008.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d606e154a1310">
            <mixed-citation id="d606e158" publication-type="other">
Aya, Rod. 1979. "Theories of Revolution Reconsidered: Contrasting Models of
Collective Violence." Theory and Society 8, no. 1: 39-99.</mixed-citation>
         </ref>
         <ref id="d606e168a1310">
            <mixed-citation id="d606e172" publication-type="other">
Boli, John, and George M. Thomas. 1997. "World Culture in the World Polity: A Century
of International Non-Governmental Organization." American Sociological Review 62,
no. 2: 171-90.</mixed-citation>
         </ref>
         <ref id="d606e185a1310">
            <mixed-citation id="d606e189" publication-type="other">
—. 1999. Constructing World Culture: International Nongovernmental
Organizations Since 1875. Stanford: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d606e199a1310">
            <mixed-citation id="d606e203" publication-type="other">
Boswell, Terry, and William J. Dixon. 1990. "Dependency and Rebellion: A Cross-
National Analysis." American Sociological Review 55, no. 5: 540-59.</mixed-citation>
         </ref>
         <ref id="d606e214a1310">
            <mixed-citation id="d606e218" publication-type="other">
—. 1993. Marx's Theory of Rebellion: A Cross-National Analysis of Class
Exploitation, Economic Development, and Violent Revolt." American Sociological
Review 58: 681-702.</mixed-citation>
         </ref>
         <ref id="d606e231a1310">
            <mixed-citation id="d606e235" publication-type="other">
Carey, Sabine. 2006. "The Dynamic Relationship Between Protest, Repression, and
Political Regimes." Political Research Quarterly 59, no. 1: 1-11.</mixed-citation>
         </ref>
         <ref id="d606e245a1310">
            <mixed-citation id="d606e249" publication-type="other">
Collier, P., and A. Hoeffler. 1998. "On the Economic Causes of Civil War." Oxford
Economic Papers 50: 563-73.</mixed-citation>
         </ref>
         <ref id="d606e259a1310">
            <mixed-citation id="d606e263" publication-type="other">
Davenport, Christian. 1995. "Multi-Dimensional Threat Perception and State Repression:
An Inquiry into Why States Apply Negative Sanctions." American Journal of Political
Science 39, no. 3: 683-713.</mixed-citation>
         </ref>
         <ref id="d606e276a1310">
            <mixed-citation id="d606e280" publication-type="other">
—. 1996. "The Weight of the Past: Exploring Lagged Determinants of Political
Repression." Political Research Quarterly 49, no. 2: 377-403.</mixed-citation>
         </ref>
         <ref id="d606e290a1310">
            <mixed-citation id="d606e294" publication-type="other">
—. 2007. "State Repression and Political Order." Annual Review of Political Science
10: 1-23.</mixed-citation>
         </ref>
         <ref id="d606e305a1310">
            <mixed-citation id="d606e309" publication-type="other">
Elbadawi, Ibrahim A. 1999. "Civil Wars and Poverty: The Role of External Interventions,
Political Rights and Economic Growth." Paper presented at the World Bank
Conference on Civil Conflicts, Crime and Violence, Washington, DC, February
22-23.</mixed-citation>
         </ref>
         <ref id="d606e325a1310">
            <mixed-citation id="d606e329" publication-type="other">
Ellingsen, Tanja. 2000. "Colorful Community or Ethnic Witches Brew." Journal of
Conflict Resolution 44: 228-49.</mixed-citation>
         </ref>
         <ref id="d606e339a1310">
            <mixed-citation id="d606e343" publication-type="other">
Fargues, Philippe. 2000. "Protracted National Conflict and Fertility Change: Palestinians
and Israelis in the Twentieth Century." Population and Development Review 26, no. 3:
441-82.</mixed-citation>
         </ref>
         <ref id="d606e356a1310">
            <mixed-citation id="d606e360" publication-type="other">
Francisco, Ronald. 1995. "The Relationship Between Coercion and Protest: An
Empirical Evaluation in Three Coercive States." Journal of Conflict Resolution 39,
no. 2: 263-82.</mixed-citation>
         </ref>
         <ref id="d606e373a1310">
            <mixed-citation id="d606e377" publication-type="other">
Freedom House. 2006. Freedom in the World, www.freedomhouse.org.</mixed-citation>
         </ref>
         <ref id="d606e384a1310">
            <mixed-citation id="d606e388" publication-type="other">
Freeman, John. 1989. "Systematic Sampling, Temporal Aggregation, and the Study of
Political Relationships." Political Analysis 1: 61-98.</mixed-citation>
         </ref>
         <ref id="d606e399a1310">
            <mixed-citation id="d606e403" publication-type="other">
Gartner, Scott S., and Patrick M. Reagan. 1996. "Threat and Repression: The Non-Linear
Relationship Between Government and Opposition Violence." Journal of Peace
Research 33, no. 3: 273-88.</mixed-citation>
         </ref>
         <ref id="d606e416a1310">
            <mixed-citation id="d606e420" publication-type="other">
Gibney, Mark, and Matthew Dalton. 1996. "The Political Terror Scale." Policy Studies
and Developing Nations 4: 73-84. Available at www.unca.edu/politicalscience/DOCS/
Gibney/Political percent20Terror percent20Scale percent201980-2005.pdf (accessed
May 14, 2008).</mixed-citation>
         </ref>
         <ref id="d606e436a1310">
            <mixed-citation id="d606e440" publication-type="other">
Gupta, Dipak K.; Harinder Singh; and Tom Sprague. 1993. "Government Coercion of
Dissidents: Deterrence or Provocation?" Journal of Conflict Resolution 37, no. 2:
301-39.</mixed-citation>
         </ref>
         <ref id="d606e453a1310">
            <mixed-citation id="d606e457" publication-type="other">
Gurr, Ted. 1970. Why Men Rebel. New Haven: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d606e464a1310">
            <mixed-citation id="d606e468" publication-type="other">
Hartman, John, and Wey Hsiao. 1988. "Inequality and Violence: Issues of Theory and
Measurement in Muller." American Sociological Review 53: 794-99.</mixed-citation>
         </ref>
         <ref id="d606e478a1310">
            <mixed-citation id="d606e482" publication-type="other">
Henderson, Conway W. 1991. "Conditions Affecting the Use of Political Repression."
Journal of Conflict Resolution 35, no. 1: 120-42.</mixed-citation>
         </ref>
         <ref id="d606e493a1310">
            <mixed-citation id="d606e497" publication-type="other">
Hirschman, Charles. 2004. "The Origins and Demise of the Concept of Race." Population
and Development Review 30, no. 3: 385-415.</mixed-citation>
         </ref>
         <ref id="d606e507a1310">
            <mixed-citation id="d606e511" publication-type="other">
Hoover, Dean, and David Kowalewski. 1992. "Dynamic Models of Dissent and
Repression." Journal of Conflict Resolution 36, no. 1: 150-82.</mixed-citation>
         </ref>
         <ref id="d606e521a1310">
            <mixed-citation id="d606e525" publication-type="other">
Hughes, Melanie; Lindsey Peterson; Jill Ann Harrison; and Pamela Paxton. Forthcoming.
"Power and Relation in the World Polity: The INGO Network Country Score, 1978-
1998." Social Forces.</mixed-citation>
         </ref>
         <ref id="d606e538a1310">
            <mixed-citation id="d606e542" publication-type="other">
Huntington, Samuel. 1968. Political Order in Changing Societies. New Haven: Yale
University Press.</mixed-citation>
         </ref>
         <ref id="d606e552a1310">
            <mixed-citation id="d606e556" publication-type="other">
Jenkins, J. Craig, and Kurt Schock. 1992. "Global Structures and Political Processes in
the Study of Domestic Political Conflict." Annual Review of Sociology 18: 161-85.</mixed-citation>
         </ref>
         <ref id="d606e566a1310">
            <mixed-citation id="d606e570" publication-type="other">
Jenkins, J. Craig, and Charles Lewis Taylor. Forthcoming. The World Handbook of
Political Indicators IV. Preliminary release for 1991-2000 available at www.sociology.
ohio-state.edu/~jcj/.</mixed-citation>
         </ref>
         <ref id="d606e584a1310">
            <mixed-citation id="d606e588" publication-type="other">
Khawaja, Marwan. 1993. "Repression and Popular Collective Action: Evidence from the
West Bank." Sociological Forum 8, no. 1: 47-71.</mixed-citation>
         </ref>
         <ref id="d606e598a1310">
            <mixed-citation id="d606e602" publication-type="other">
Lichbach, Mark I. 1987. "Deterrence or Escalation? The Puzzle of Aggregate Studies of
Repression and Dissent." Journal of Conflict Resolution 31, no. 2: 266-97.</mixed-citation>
         </ref>
         <ref id="d606e612a1310">
            <mixed-citation id="d606e616" publication-type="other">
—. 1989. "An Evaluation of 'Does Economic Inequality Breed Political Conflict'?"
World Politics 41: 431-70.</mixed-citation>
         </ref>
         <ref id="d606e626a1310">
            <mixed-citation id="d606e630" publication-type="other">
Lichbach, Mark I., and Ted Robert Gurr. 1981. "The Conflict Process: A Formal Model."
Journal of Conflict Resolution 25: 3-29.</mixed-citation>
         </ref>
         <ref id="d606e640a1310">
            <mixed-citation id="d606e644" publication-type="other">
Marshall, Monty G.; Keith Jaggers; and Ted Gurr. 2005. Polity IV. Available at www.
cidcm.umd.edu/polity/data/.</mixed-citation>
         </ref>
         <ref id="d606e654a1310">
            <mixed-citation id="d606e658" publication-type="other">
McAdam, Doug. 1996 "Conceptual Origins. Current Problems. Future Directions." In
Comparative Perspectives on Social Movements: Political Opportunities. Mobilizing
Structures, and Cultural Framings, ed. Doug McAdam, John D. McCarthy, and Mayer
N. Zald, 3^0. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d606e675a1310">
            <mixed-citation id="d606e679" publication-type="other">
Meyer, David S. 2004. "Protest and Political Opportunity." Annual Review of Sociology
30: 125-45.</mixed-citation>
         </ref>
         <ref id="d606e689a1310">
            <mixed-citation id="d606e693" publication-type="other">
Meyer, David S., and Debra C. Minkoff. 2004. "Conceptualizing Political Opportunity."
Social Forces 82, no. 4: 1457-92.</mixed-citation>
         </ref>
         <ref id="d606e703a1310">
            <mixed-citation id="d606e707" publication-type="other">
Meyer, John W; John Boli; George M. Thomas; and Francisco O. Ramirez. 1997. "World
Society and the Nation State." American Journal of Sociology 103: 144-81.</mixed-citation>
         </ref>
         <ref id="d606e717a1310">
            <mixed-citation id="d606e721" publication-type="other">
Moore, Barrington. 1966. The Social Origins of Dictatorship and Democracy. Boston:
Beacon Press.</mixed-citation>
         </ref>
         <ref id="d606e731a1310">
            <mixed-citation id="d606e735" publication-type="other">
Moore, Will H. 1998. "Repression and Dissent: Substitution, Context, and Timing."
American Journal of Political Science 42, no. 3: 851-73.</mixed-citation>
         </ref>
         <ref id="d606e745a1310">
            <mixed-citation id="d606e749" publication-type="other">
Moore, Will H. 2000. "The Repression of Dissent: A Substitution Model of Government
Coercion." Journal of Conflict Resolution 44, no. 1: 107-27.</mixed-citation>
         </ref>
         <ref id="d606e760a1310">
            <mixed-citation id="d606e764" publication-type="other">
Muller, Edward, and Erich Weede. 1990. "Cross-National Variation in Political Violence:
A Rational Action Approach." Journal of Conflict Resolution 34: 624-51.</mixed-citation>
         </ref>
         <ref id="d606e774a1310">
            <mixed-citation id="d606e778" publication-type="other">
Muller, Edward N. 1985. "Income Inequality, Regime Repressiveness, and Political
Violence." American Sociological Review 50, no. 1: 47-61.</mixed-citation>
         </ref>
         <ref id="d606e788a1310">
            <mixed-citation id="d606e792" publication-type="other">
Opp, Karl-Dieter, and Wolfgang Roehl. 1990. "Repression, Micromobilization, and
Political Protest." Social Forces 69, no. 2: 521-47.</mixed-citation>
         </ref>
         <ref id="d606e802a1310">
            <mixed-citation id="d606e808" publication-type="other">
Ortiz, David G. 2007. "Confronting Oppression with Violence: Inequality, Military
Infrastructure and Dissident Repression." Mobilization 12, no. 3: 219-38.</mixed-citation>
         </ref>
         <ref id="d606e818a1310">
            <mixed-citation id="d606e822" publication-type="other">
Ortiz, David G.; Daniel J. Myers; N. Eugene Walls; and Maria-Elena Diaz, 2005. "Where
Do We Stand with Newspaper Data?" Mobilization 10, no. 3: 397-419.</mixed-citation>
         </ref>
         <ref id="d606e832a1310">
            <mixed-citation id="d606e836" publication-type="other">
Raudenbush, Stephen W., and Anthony S. Bryk. 2002. Hierarchical Linear Models;
Applications and Data Analysis Methods. 2d ed. Thousand Oaks, CA: Sage.</mixed-citation>
         </ref>
         <ref id="d606e847a1310">
            <mixed-citation id="d606e851" publication-type="other">
Rasier, Karen. 1996. "Concessions, Repression, and Political Protest in the Iranian
Revolution." American Sociological Review 61, no. 1: 132-52.</mixed-citation>
         </ref>
         <ref id="d606e861a1310">
            <mixed-citation id="d606e865" publication-type="other">
Reuters. 2007. "About Us." Available at http://about.reuters.com/aboutus/worldwide.asp
(accessed March 14, 2007).</mixed-citation>
         </ref>
         <ref id="d606e875a1310">
            <mixed-citation id="d606e879" publication-type="other">
Schock, Kurt. 1999. "People Power and Political Opportunities: Social Movement
Mobilization and Outcomes in the Philippines and Burma." Social Problems 46:
355-75.</mixed-citation>
         </ref>
         <ref id="d606e892a1310">
            <mixed-citation id="d606e896" publication-type="other">
Shellman, Stephen M. 2004. "Time Series Intervals and Statistical Inference: The Effects
of Temporal Aggregation on Event Data Analysis." Political Analysis 12: 97-104.</mixed-citation>
         </ref>
         <ref id="d606e906a1310">
            <mixed-citation id="d606e910" publication-type="other">
Skocpol, Theda. 1979. States and Social Revolutions. Cambridge: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d606e920a1310">
            <mixed-citation id="d606e924" publication-type="other">
Tilly, Charles. 1973. "Does Modernization Breed Revolution?" Comparative Politics 5:
425-47.</mixed-citation>
         </ref>
         <ref id="d606e935a1310">
            <mixed-citation id="d606e939" publication-type="other">
—. 1978. From Mobilization to Revolution. Reading MA: Addison-Wesley.</mixed-citation>
         </ref>
         <ref id="d606e946a1310">
            <mixed-citation id="d606e950" publication-type="other">
United Nations Development Programme (UNDP). 2006. Human Development Report.
Available at http://hdr.undp.org/hdr2006/statistics (accessed May 14, 2008).</mixed-citation>
         </ref>
         <ref id="d606e960a1310">
            <mixed-citation id="d606e964" publication-type="other">
Vanhanen, Tatu. 1999. "Domestic Ethnic Conflict and Ethnic Nepotism: A Comparative
Analysis." Journal of Peace Research 36, no. 1: 55-74.</mixed-citation>
         </ref>
         <ref id="d606e974a1310">
            <mixed-citation id="d606e978" publication-type="other">
Walton, John, and Charles Ragin. 1990. "Global and National Sources of Political
Protest: Third World Responses to the Debt Crisis." American Sociological Review 55,
no. 6: 876-90.</mixed-citation>
         </ref>
         <ref id="d606e991a1310">
            <mixed-citation id="d606e995" publication-type="other">
Weede, Eric. 1986. "Rent Seeking, Military Participation and Economic Performance in
LDCs." Journal of Conflict Resolution 30, no. 2: 77-91</mixed-citation>
         </ref>
         <ref id="d606e1005a1310">
            <mixed-citation id="d606e1009" publication-type="other">
—. 1987. "Urban Bias and Economic Growth in Cross-National Perspective."
International Journal of Comparative Sociology 28, no. 2: 30-42.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
