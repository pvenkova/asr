<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7hn4pb</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt1t89g2h</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Social work and lesbian, gay, bisexual and trans people</book-title>
         <subtitle>Making a difference</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Fish</surname>
               <given-names>Julie</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>14</day>
         <month>03</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="epub">9781447314400</isbn>
      <isbn content-type="epub">1447314409</isbn>
      <publisher>
         <publisher-name>Policy Press</publisher-name>
         <publisher-loc>Bristol</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>The Policy Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1t89g2h"/>
      <abstract abstract-type="short">
         <p>This important textbook makes a timely contribution to international agendas in social work with lesbian, gay, bisexual and trans (LGBT) people. It examines how practitioners and student social workers can provide appropriate care across the lifespan (including work with children and families and older people) and considers key challenges in social work practice, for example asylum, mental health, and substance misuse. Drawing on practice scenarios, the book takes an enquiry-based learning approach to facilitate critical reflection. Its distinctive approach includes: • use of the concepts of the Professional Capabilities Framework for social work • key theoretical perspectives including human rights • structuring of the text around the framework of the UK National Occupational Standards for Social Work • student-friendly features including key questions and exercises • a complete glossary of key terms and concepts • examination of the UK policy and legislative context It is informed by international research in social work with LGBT people The book is essential reading for students on qualifying social work programmes and practitioners in statutory, voluntary and independent sectors.This important textbook examines how practitioners and student social workers can provide appropriate care for lesbian, gay, bisexual and trans people across the lifespan (including work with children and families and older people) and considers key challenges in social work practice.</p>
      </abstract>
      <counts>
         <page-count count="200"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.3</book-part-id>
                  <title-group>
                     <title>List of tables and figures</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.4</book-part-id>
                  <title-group>
                     <title>List of abbreviations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.5</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Over the past decade, there have been profound social and legal changes for Lesbian, Gay, Bisexual and Trans (LGBT) people in the UK that impact on the practice of social work. These include the legal recognition of same-sex partnerships, eligibility to apply to adopt a child, protection from dismissal from employment, legal recognition of homophobic hate crime and rights of succession to a tenancy if a partner dies. Until the introduction of the Equality Act (Sexual Orientation) Regulations in 2007, there was no legislation to prohibit discrimination against LGBT people in public services. Changes in legislation have been accompanied by</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The requirement to consider sexual orientation in social work</title>
                  </title-group>
                  <fpage>15</fpage>
                  <abstract>
                     <p>Social work is practised with those who are among the disadvantaged in society; its core aim is to work collaboratively with people to bring about change in their lives. In order to work alongside people to enable them to bring about lasting change, social workers must have attitudes and values that demonstrate their recognition of people’s lives and circumstances.</p>
                     <p>The international requirement for social workers to promote social justice for lesbian, gay and bisexual people is included alongside those of other equality grounds in the code of ethics developed jointly by the International Federation of Social Workers (IFSW) and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Theoretical perspectives in social work with LGBT people</title>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>Linking theory and practice is something that many practitioners and social work students grapple with. Theory is sometimes perceived as separate from practice and an aspect of social work learning that takes place predominantly in the university. The complexities of people’s lives do not appear to fit into a neat box labelled theory. But theory provides a rationale for social work interventions (why one course of action is more appropriate than another), can aid understanding of the processes and the barriers that people may experience in their everyday lives, and can help to clarify potential outcomes. Theory helps social workers</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Trans people in social work</title>
                  </title-group>
                  <fpage>49</fpage>
                  <abstract>
                     <p>Trans people have only recently been recognised as users of health and social care services (DH, 2008c). In comparison to lesbian, gay and bisexual people, they have been almost totally overlooked in social work theory or research (Kenagy, 2005; Davis, 2008; Alleyn and Jones, 2010). Previous studies have sometimes included trans people in their samples, but have failed to give separate consideration of their specific concerns. Existing research has focused largely on four main areas: theoretical understandings about sex and Sex is considered to be a defining and enduring aspect of who we are. We think of it as unambiguous:</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Children and young people</title>
                  </title-group>
                  <fpage>67</fpage>
                  <abstract>
                     <p>Children and young people have been at the centre of two of the most contested pieces of legislation relating to LGBT people. In 1986, a children’s book entitled<italic>Jenny lives with Eric and Martin</italic>, which was stocked in the library of the Inner London Education Authority, caused huge media controversy because it was the first to discuss the everyday family life experiences of a child (five-year-old Jenny) and her gay male parents. The ensuing debate led to the introduction of arguably one of the most regressive pieces of legislation, the Local Government Act 1988, which outlawed the promotion of homosexuality</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Older people</title>
                  </title-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>The ‘baby boom’ generation, people born in the post-war period (1945–64), represent a significant demographic group currently approaching retirement. It is estimated that in the next 25 years, older people in the population will outnumber those under 16 for the first time. Baby boomers were born at the time the modern welfare state came into being: better living and working conditions and universal health care have contributed to increased longevity. During the 1930s, men could expect to live until the age of 53 and women until age 59. Current life expectancy is age 76 and 80 for men and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Mental health</title>
                  </title-group>
                  <fpage>101</fpage>
                  <abstract>
                     <p>In the practice scenario, Amiyah believes that sexual abuse in her childhood led to her being lesbian as a young woman. Some studies have found higher rates of sexual abuse in lesbians in comparison to heterosexual women (Hughes et al, 2001). If a woman believes that her identity was formed as a result of sexual abuse, her feelings of shame and internalised homophobia may be intensified. It may be that she accounts for her lesbianism in this way because this means that her identity is neither chosen nor something she was born with, but rather a consequence of her life</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Disability</title>
                  </title-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>The medical model has formed the traditional approach to disability (Brothers, 2003). The approach defines disabled people by their health condition – by what they are not able to do – rather than their abilities. It is sometimes known as the individual model because it promotes the view that the disability is the responsibility of the individual and that an individual disabled person should fit in to the way that society is organised; the problem is with the individual disabled person not with society. It assumes that a disabled person is dependent and needs to be cured or cared for. Consequently, disabled</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Substance misuse</title>
                  </title-group>
                  <fpage>133</fpage>
                  <abstract>
                     <p>Substance misuse became a key area for social policy in the early 21st century where it was linked to a number of social problems including criminal activity, unemployment and anti-social behaviours. Misusing drugs applies to legal substances, such as alcohol, or illegal substances, like cannabis or cocaine. In the popular imagination, people who misuse substances are often seen to be the agents of their own problems and are believed to be ambivalent about changing their behaviour. For many people who are dependent on substances, the greatest harm is not to others, but to themselves. The impact on the body of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.15</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Asylum seekers and refugees</title>
                  </title-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>The international setting provides an important context for LGBT people’s rights in the UK because many of the rights they enjoy have come about because of judgments made in the European Court of Human Rights or through human rights advances globally for LGBT people (de Jong, 2003). Since 2007, the International Lesbian and Gay Association (ILGA), a campaigning organisation for LGBT people’s rights worldwide, has published an annual report –<italic>State Sponsored Homophobia</italic>– which identifies the international legal situation for LGBT people. The struggle for social equality has gained momentum by the collaboration of advocates from diverse countries globally and not</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.16</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Concluding remarks</title>
                  </title-group>
                  <fpage>161</fpage>
                  <abstract>
                     <p>This book is intended as a stage in the journey towards developing good practice in social work with LGBT people and to underpin learning on qualifying programmes in higher education. There are some gaps and limitations in the book. Although there are a number of voluntary-sector organisations who work with BME communities, indicating distinctive concerns, studies with and for LGBT people from BME communities are notably absent in research in social work and related disciplines. For this reason, separate consideration has not been given to the differing needs of these communities, although where research does exist, this has been addressed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.17</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>165</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.18</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>169</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t89g2h.19</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>189</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
