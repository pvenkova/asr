<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">econbota</journal-id>
         <journal-id journal-id-type="jstor">j101315</journal-id>
         <journal-title-group>
            <journal-title>Economic Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The New York Botanical Garden Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00130001</issn>
         <issn pub-type="epub">18749364</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4256864</article-id>
         <title-group>
            <article-title>Discovery and Genetic Assessment of Wild Bottle Gourd [Lagenaria Siceraria (Mol.) Standley; Cucurbitaceae] from Zimbabwe</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Deena S.</given-names>
                  <surname>Decker-Walters</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Mary Wilkins-Ellert</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Sang-Min</given-names>
                  <surname>Chung</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Jack E.</given-names>
                  <surname>Staub</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>12</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">58</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i389518</issue-id>
         <fpage>501</fpage>
         <lpage>508</lpage>
         <page-range>501-508</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The New York Botanical Garden Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4256864"/>
         <abstract>
            <p>Bottle gourd [Lagenaria siceraria (Mol.) Standley] is an edible, medicinal, and otherwise utilitarian domesticated cucurbit with an ancient pantropical distribution. This African native reached Asia and the Americas 9000 years ago, probably as a wild species whose fruits had floated across the seas. Independent domestications from wild populations are believed to have occurred in both the Old and New Worlds. However, few wild populations of L. siceraria have been found during recorded history and none has been verified or studied in detail. In 1992, Mary Wilkins-Ellert discovered an unusual free-living plant of Lagenaria in a remote region of southeastern Zimbabwe. Her morphological observations during several plantings of the collected seed, as well as results from two genetic analyses (random amplified polymorphic DNA and chloroplast sequencing), indicate that the Zimbabwe collection is part of a genetically distinct and wild lineage of L. siceraria.</p>
         </abstract>
         <kwd-group>
            <kwd>Bottle gourd</kwd>
            <kwd>Lagenaria siceraria</kwd>
            <kwd>Lagenaria sphaerica</kwd>
            <kwd>RAPD</kwd>
            <kwd>Cpdna sequencing</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d345e197a1310">
            <mixed-citation id="d345e201" publication-type="book">
Chakravarty, H. L.1959. Monograph on Indian Cu-
curbitaceae. Records of the Botanical Survey of In-
dia, vol. 18, No. 1. Government of India Press, Cal-
cutta.<person-group>
                  <string-name>
                     <surname>Chakravarty</surname>
                  </string-name>
               </person-group>
               <volume>18</volume>
               <source>Monograph on Indian Cucurbitaceae. Records of the Botanical Survey of India</source>
               <year>1959</year>
            </mixed-citation>
         </ref>
         <ref id="d345e236a1310">
            <mixed-citation id="d345e240" publication-type="journal">
Chung, S.-M., and J. E. Staub. 2003. The develop-
ment and evaluation of consensus chloroplast prim-
er pairs that possess highly variable sequence re-
gions in a diverse array of plant taxa. Theoretical
and Applied Genetics107:757-767.<person-group>
                  <string-name>
                     <surname>Chung</surname>
                  </string-name>
               </person-group>
               <fpage>757</fpage>
               <volume>107</volume>
               <source>Theoretical and Applied Genetics</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d345e281a1310">
            <mixed-citation id="d345e285" publication-type="journal">
, D. S. Decker-Walters, and J. E. Staub.
2003. Genetic relationships within the Cucurbita-
ceae as assessed by ccSSR marker and sequence
analyses. Canadian Journal of Botany81:814-832.<person-group>
                  <string-name>
                     <surname>Chung</surname>
                  </string-name>
               </person-group>
               <fpage>814</fpage>
               <volume>81</volume>
               <source>Canadian Journal of Botany</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d345e323a1310">
            <mixed-citation id="d345e327" publication-type="other">
Cogniaux, A., and H. Harms. 1924. Cucurbitaceae—
Cucurbiteae—Cucumerinae. Das Pflanzenreich.
Heft 88 (IV. 275. II).</mixed-citation>
         </ref>
         <ref id="d345e341a1310">
            <mixed-citation id="d345e345" publication-type="journal">
Cutler, H. C., and T. W. Whitaker. 1961. History
and distribution of the cultivated cucurbits in the
Americas. American Antiquity26:469-485.<object-id pub-id-type="doi">10.2307/278735</object-id>
               <fpage>469</fpage>
            </mixed-citation>
         </ref>
         <ref id="d345e364a1310">
            <mixed-citation id="d345e368" publication-type="book">
DeCandolle, A.1967. Origin of cultivated plants. Haf-
ner Publishing Company, New York &amp; London.
[Reprint of the second edition in 1886].<person-group>
                  <string-name>
                     <surname>DeCandolle</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Origin of cultivated plants</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d345e400a1310">
            <mixed-citation id="d345e404" publication-type="journal">
Decker-Walters, D. S., S.-M. Chung, and J. E.
Staub. 2004. Plastid sequence evolution: A new
pattern of nucleotide substitutionns in the Cucur-
bitaceae. Journal of Molecular Evolution58:606-
614.<person-group>
                  <string-name>
                     <surname>Decker-Walters</surname>
                  </string-name>
               </person-group>
               <fpage>606</fpage>
               <volume>58</volume>
               <source>Journal of Molecular Evolution</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d345e445a1310">
            <mixed-citation id="d345e451" publication-type="journal">
, J. Staub, A. Lopez-Sese, and E. Nakata.
2001. Diversity in landraces and cultivars of bottle
gourd (Lagenaria siceraria; Cucurbitaceae) as as-
sessed by random amplified polymorphic DNA.
Genetic Resources and Crop Evolution48:369-
380.<person-group>
                  <string-name>
                     <surname>Decker-Walters</surname>
                  </string-name>
               </person-group>
               <fpage>369</fpage>
               <volume>48</volume>
               <source>Genetic Resources and Crop Evolution</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d345e495a1310">
            <mixed-citation id="d345e499" publication-type="book">
Doebley, J.1989. Isozymic evidence and the evolution
of crop plants. Pages 165-191 in D. E. Soltis and
P. S. Soltis, eds., Isozymes in plant biology. Dios-
corides Press, Portland, OR.<person-group>
                  <string-name>
                     <surname>Doebley</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Isozymic evidence and the evolution of crop plants</comment>
               <fpage>165</fpage>
               <source>Isozymes in plant biology</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d345e537a1310">
            <mixed-citation id="d345e541" publication-type="book">
Fagan, B.1970. Hunter-gatherers at Gwisho. Pages
168-174 in B. Fagan, ed., Introductory readings in
archaeology. Little, Brown, and Company, Boston.<person-group>
                  <string-name>
                     <surname>Fagan</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Hunter-gatherers at Gwisho</comment>
               <fpage>168</fpage>
               <source>Introductory readings in archaeology</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d345e577a1310">
            <mixed-citation id="d345e581" publication-type="journal">
Gorman, C. F.1969. Hoabinhian: A pebble-tool com-
plex with early plant associations in Southeast
Asia. Science163:671-673.<object-id pub-id-type="jstor">10.2307/1726336</object-id>
               <fpage>671</fpage>
            </mixed-citation>
         </ref>
         <ref id="d345e600a1310">
            <mixed-citation id="d345e604" publication-type="book">
Heiser, C. B., Jr.1973. Variation in the bottle gourd.
Pages 121-128 in B. J. Meggers, E. S. Ayensu, and
W. D. Duckworth, eds., Tropical forest ecosystems
in Africa and South America: A comparative review.
Smithsonian Institution Press, Washington, DC.<person-group>
                  <string-name>
                     <surname>Heiser</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Variation in the bottle gourd</comment>
               <fpage>121</fpage>
               <source>Tropical forest ecosystems in Africa and South America: A comparative review</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d345e645a1310">
            <mixed-citation id="d345e649" publication-type="book">
.1979. The gourd book. University of
Oklahoma Press, Norman.<person-group>
                  <string-name>
                     <surname>Heiser</surname>
                  </string-name>
               </person-group>
               <source>The gourd book</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d345e674a1310">
            <mixed-citation id="d345e678" publication-type="journal">
Huang, S. C., C. C. Tsai, and C. S. Sheu. 2000.
Genetic analysis of Chrysanthemum hybrids based
on RAPD molecular markers. Botanical Bulletin of
Academia Sinica41:257-262.<person-group>
                  <string-name>
                     <surname>Huang</surname>
                  </string-name>
               </person-group>
               <fpage>257</fpage>
               <volume>41</volume>
               <source>Botanical Bulletin of Academia Sinica</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d345e716a1310">
            <mixed-citation id="d345e720" publication-type="book">
Jeffrey, C.1967. Cucurbitaceae. Pages 1-157 in E.
Milne-Redhead and R. M. Polhill, eds., Flora of
tropical East Africa. Crown Agents for Oversea
Governments and Administrations, London.<person-group>
                  <string-name>
                     <surname>Jeffrey</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cucurbitaceae</comment>
               <fpage>1</fpage>
               <source>Flora of tropical East Africa</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d345e758a1310">
            <mixed-citation id="d345e762" publication-type="book">
.1978. Cucurbitaceae. Pages 414-499 in E.
Launert, ed., Flora Zambesiaca, vol. 4. Royal Bo-
tanic Gardens Kew, London.<person-group>
                  <string-name>
                     <surname>Jeffrey</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cucurbitaceae</comment>
               <fpage>414</fpage>
               <volume>4</volume>
               <source>Flora Zambesiaca</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d345e801a1310">
            <mixed-citation id="d345e805" publication-type="book">
.1995. Cucurbitaceae. Pages 17-59 in S. Ed-
wards, M. Tadesse, and I. Hedberg, eds., Flora of
Ethiopia and Eritrea, vol. 2, pt. 2. The National
Herbarium, Addis Ababa, Ethiopia.<person-group>
                  <string-name>
                     <surname>Jeffrey</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cucurbitaceae</comment>
               <fpage>17</fpage>
               <volume>2</volume>
               <source>Flora of Ethiopia and Eritrea</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d345e846a1310">
            <mixed-citation id="d345e850" publication-type="book">
MacNeish, R. S., A. N. Nelkin-Terner, and A. G.
Cook. 1970. The second annual report of the Aya-
cucho archaeological-botanical project. R. S. Pea-
body Foundation for Archaeology, Andover, MA.<person-group>
                  <string-name>
                     <surname>MacNeish</surname>
                  </string-name>
               </person-group>
               <source>The second annual report of the Ayacucho archaeological-botanical project</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d345e882a1310">
            <mixed-citation id="d345e886" publication-type="journal">
Meeuse, A. D. J.1962. The Cucurbitaceae of southern
Africa. Bothalia8(1):1-111.<person-group>
                  <string-name>
                     <surname>Meeuse</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>1</fpage>
               <volume>8</volume>
               <source>Bothalia</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d345e921a1310">
            <mixed-citation id="d345e925" publication-type="journal">
Richardson, J. B., III. 1972. The pre-Columbian dis-
tribution of the bottle gourd (Lagenaria siceraria):
A re-evaluation. Economic Botany26:265-273.<person-group>
                  <string-name>
                     <surname>Richardson</surname>
                  </string-name>
               </person-group>
               <fpage>265</fpage>
               <volume>26</volume>
               <source>Economic Botany</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d345e960a1310">
            <mixed-citation id="d345e964" publication-type="journal">
Rieseberg, L. H.1996. Homology among RAPD frag-
ments in interspecific comparisons. Molecular
Ecology5:99-105.<person-group>
                  <string-name>
                     <surname>Rieseberg</surname>
                  </string-name>
               </person-group>
               <fpage>99</fpage>
               <volume>5</volume>
               <source>Molecular Ecology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d345e999a1310">
            <mixed-citation id="d345e1003" publication-type="book">
Roxburgh, W.1832. Descriptions of Indian plants.
Vol. 3. W. Thacker and Company, Calcutta.<person-group>
                  <string-name>
                     <surname>Roxburgh</surname>
                  </string-name>
               </person-group>
               <volume>3</volume>
               <source>Descriptions of Indian plants</source>
               <year>1832</year>
            </mixed-citation>
         </ref>
         <ref id="d345e1033a1310">
            <mixed-citation id="d345e1037" publication-type="book">
Saldanha, C., and M. S. Eswar Rao. 1984. Cucur-
bitaceae. Pages 292-308 in C. J. Saldanha, ed., Flo-
ra of Karnataka, vol. 1. Oxford and IBH Publishing
Company, New Delhi, India.<person-group>
                  <string-name>
                     <surname>Saldanha</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Cucurbitaceae</comment>
               <fpage>292</fpage>
               <volume>1</volume>
               <source>Flora of Karnataka</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d345e1078a1310">
            <mixed-citation id="d345e1082" publication-type="journal">
Schweinfurth, G.1884. Further discoveries in the flo-
ra of ancient Egypt. Nature29:312-315.<person-group>
                  <string-name>
                     <surname>Schweinfurth</surname>
                  </string-name>
               </person-group>
               <fpage>312</fpage>
               <volume>29</volume>
               <source>Nature</source>
               <year>1884</year>
            </mixed-citation>
         </ref>
         <ref id="d345e1114a1310">
            <mixed-citation id="d345e1118" publication-type="book">
Swofford, D. L.2002. PAUP*: Phylogenetic analysis
using parsimony, version 40b10 for the Macintosh.
Sinauer Associates, Sunderland, MA.<person-group>
                  <string-name>
                     <surname>Swofford</surname>
                  </string-name>
               </person-group>
               <source>PAUP*: Phylogenetic analysis using parsimony, version 40b10 for the Macintosh</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
