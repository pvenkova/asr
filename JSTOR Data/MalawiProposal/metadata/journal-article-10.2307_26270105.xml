<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26270105</article-id>
         <article-categories>
            <subj-group>
               <subject>Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Indirect contributions of forests to dietary diversity in Southern Ethiopia</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Baudron</surname>
                  <given-names>Frédéric</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Chavarría</surname>
                  <given-names>Jean-Yves Duriaux</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Remans</surname>
                  <given-names>Roseline</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
               <xref ref-type="aff" rid="af3">³</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Yang</surname>
                  <given-names>Kevin</given-names>
               </string-name>
               <xref ref-type="aff" rid="af4">⁴</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Sunderland</surname>
                  <given-names>Terry</given-names>
               </string-name>
               <xref ref-type="aff" rid="af5">⁵</xref>
               <xref ref-type="aff" rid="af6">⁶</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>International Maize and Wheat Improvement Centre (CIMMYT),</aff>
            <aff id="af2">
               <label>²</label>Bioversity International,</aff>
            <aff id="af3">
               <label>³</label>Faculty of Bioscience Engineering, Ghent University,</aff>
            <aff id="af4">
               <label>⁴</label>Department of Forest and Conservation Sciences, University of British Columbia,</aff>
            <aff id="af5">
               <label>⁵</label>Center for International Forestry Research (CIFOR),</aff>
            <aff id="af6">
               <label>⁶</label>James Cook University</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2017</year>
            <string-date>Jun 2017</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">22</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26270059</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2017 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26270105"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>We assess whether forests contribute indirectly to the dietary diversity of rural households by supporting diverse agricultural production systems. We applied our study in a landscape mosaic in Southern Ethiopia that was divided into three zones of increasing distance to Munesa Forest—“near,” “intermediate,” and “distant.” A variety of research tools and methods, including remote sensing, participatory methods, farm survey, and yield assessment, were employed. Diets of households were more diverse in the near zone than in the other two zones (6.58 ± 1.21, 5.38 ± 1.02, and 4.41 ± 0.77 food groups consumed daily in the near, intermediate, and distant zones, respectively). This difference was not explained by food items collected from Munesa Forest but by biomass flows from the forest to farmlands. Munesa Forest contributed an average of 6.13 ± 2.90 tons of biomass per farm and per year to the farms in the near zone, in the form of feed and fuelwood. Feed from the forest allowed for larger livestock herds in the near zone compared with the other two zones, and fuelwood from the forest reduced the need to use cattle dung as fuel in the near zone compared with the two other zones. These two biomass flows contributed to the availability of more manure to farmers closer to the forest (908 ± 853 kg farm<sup>−1</sup>, 771 ± 717 kg farm<sup>−1</sup>, and 261 ± 487 kg farm<sup>−1</sup>in the near, intermediate, and distant zones, respectively). In turn, increased manure enabled a larger percentage of farms to cultivate a diversified homegarden (87, 64, and 39% of farms in the near, intermediate, and distant zones, respectively). Homegardens and livestock products provided the greater contribution to household dietary diversity closer to the forest.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>fuelwood</kwd>
            <kwd>homegarden</kwd>
            <kwd>landscape mosaic</kwd>
            <kwd>livestock</kwd>
            <kwd>nontimber forest products</kwd>
            <kwd>nutrition</kwd>
            <kwd>nutrition-sensitive agriculture</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Arnold, M., B. Powell, P. Shanley, and T. C. H. Sunderland. 2011. EDITORIAL: Forests, biodiversity and food security. International Forestry Review 13(3):259–264. http://dx.doi.org/10.1505/146554811798293962</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Baudron, F., A. Mamo, D. Tirfessa, and M. Argaw. 2015. Impact of farmland exclosure on the productivity and sustainability of a mixed crop-livestock system in the central Rift Valley of Ethiopia. Agriculture, Ecosystems &amp; Environment 207:109–118. http://dx.doi.org/10.1016/j.agee.2015.04.003</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Beck, T., and C. Nesmith. 2001. Building on poor people’s capacities: the case of common property resources in India and West Africa. World Development 29(1):119–133. http://dx.doi.org/10.1016/s0305-750x(00)00089-9</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Bennett, E. M. 2017. Changing the agriculture and environment conversation. Nature Ecology and Evolution 1(January):1–2. http://dx.doi.org/10.1038/s41559-016-0018</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Bianchi, F. J. J. A., C. J. H. Booij, and T. Tscharntke. 2006. Sustainable pest regulation in agricultural landscapes: a review on landscape composition, biodiversity and natural pest control. Proceedings of the Royal Society B: Biological Sciences 273 (1595):1715–1727. http://dx.doi.org/10.1098/rspb.2006.3530</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Bilinsky, P., and A. Swindale. 2007. Months of adequate household food provisioning (MAHFP) for measurement of household food access: indicator guide. Food and Nutrition Technical Assistance III Project, U.S. Agency for International Development (USAID) and FHI 360.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Chikowo, R., P. Mapfumo, P. Nyamugafata, G. Nyamadzawo, and K. E. Giller. 2003. Nitrate-N dynamics following improved fallows and maize root development in a Zimbabwean sandy clay loam. Agroforestry Systems 59(3):187–195. http://dx.doi.org/10.1023/b:agfo.0000005219.07409.a0</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Clough, Y., J. Barkmann, J. Juhrbandt, M. Kessler, T. C. Wanger, A. Anshary, D. Buchori, D. Cicuzza, K. Darras, D. D. Putra, S. Erasmi, R. Pitopang, C. Schmidt, C. H. Schulze, D. Seidel, I. Steffan-Dewenter, K. Stenchly, S. Vidal, M. Weist, A. C. Wielgoss, and T. Tscharntke. 2011. Combining high biodiversity with high yields in tropical agroforests. Proceedings of the National Academy of Sciences of the United States of America 108 (20):8311–8316. http://dx.doi.org/10.1073/pnas.1016799108</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Danso, S. K. A., G. D. Bowen, and N. Sanginga. 1992. Biological nitrogen fixation in trees in agro-ecosystems. Plant and Soil 141 (1–2):177–196. http://dx.doi.org/10.1007/BF00011316</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Dix, M. E., M. O. Harrell, R. J. Wright, L. Hodges, J. R. Brandle, M. M. Schoenberger, N. J. Sunderman, R. L. Fitzmaurice, L. J. Young, and K. G. Hubbard. 1995. Influences of trees on abundance of natural enemies of insect pests: a review. Agroforestry Systems 29:303–311. http://dx.doi.org/10.1007/BF00704876</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Dounias, E., and A. Froment. 2006. When forest-based huntergatherers become sedentary: consequences for diet and health. Unasylva 57(224):26–33.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Duguma, L. A., P. A. Minang, O. E. Freeman, and H. Hager. 2014. System wide impacts of fuel usage patterns in the Ethiopian highlands: potentials for breaking the negative reinforcing feedback cycles. Energy for Sustainable Development 20:77–85. http://dx.doi.org/10.1016/j.esd.2014.03.004</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Duriaux, J.-Y., and F. Baudron. 2016. Understanding people and forest interrelations along an intensification gradient in Arsi- Negele, Ethiopia. Pages 14–53 in E. L. Deakin, M. Kshatriya, and T. C. H. Sunderland, editors. Agrarian change in tropical landscapes. Center for International Forestry Research, Bogor, Indonesia.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Fa, J. E., D. Currie, and J. Meeuwig. 2003. Bushmeat and food security in the Congo Basin: linkages between wildlife and people’s future. Environmental Conservation 30(1):71–78. http://dx.doi.org/10.1017/s0376892903000067</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Foli, S., J. Reed, J. Clendenning, G. Petrokofsky, C. Padoch, and T. C. H. Sunderland. 2014. To what extent does the presence of forests and trees contribute to food production in humid and dry forest landscapes?: a systematic review protocol. Environmental Evidence 3(1):15. http://dx.doi.org/10.1186/2047-2382-3-15</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Foote, J. A., S. P. Murphy, L. R. Wilkens, P. P. Basiotis, and A. Carlson. 2004. Dietary variety increases the probability of nutrient adequacy among adults. Journal of Nutrition 134 (7):1779–1785.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Gabriel, D., S. M. Sait, W. E. Kunin, and T. G. Benton. 2013. Food production vs. biodiversity: comparing organic and conventional agriculture. Journal of Applied Ecology 50(2):355-364. http://dx.doi.org/10.1111/1365-2664.12035</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Garibaldi, L. A., I. Steffan-Dewenter, R. Winfree, M. A. Aizen, R. Bommarco, S. A. Cunningham, C. Kremen, L. G. Carvalheiro, L. D. Harder, O. Afik, I. Bartomeus, F. Benjamin, V. Boreux, D. Cariveau, N. P. Chacoff, J. H. Dudenhöffer, B. M. Freitas, J. Ghazoul, S. S. Greenleaf, J. Hipólito, A. Holzschuh, B. Howlett, R. Isaacs, S. K. Javorek, C. M. Kennedy, K. M. Krewenka, S. Krishnan, Y. Mandelik, M. M. Mayfield, I. Motzke, T. Munyuli, B. A. Nault, M. Otieno, J. Petersen, G. Pisanty, S. G. Potts, R. Rader, T. H. Ricketts, M. Rundlöf, C. L. Seymour, C. Schüepp, H. Szentgyörgyi, H. Taki, T. Tscharntke, C. H. Vergara, B. F. Viana, T. C. Wanger, C. Westphal, N. Williams, and A.-M. Klein. 2013. Wild pollinators enhance fruit set of crops regardless of honey bee abundance. Science 339(6127):1608–1611. http://dx.doi.org/10.1126/science.1230200</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Geifus, F. 2008. 80 tools for participatory development: appraisal, planning, follow-up and evaluation. Inter-American Institute for Cooperation on Agriculture (IICA), San Jose, Costa Rica. Gibbs, H. K., A. S. Ruesch, F. Achard, M. K. Clayton, P. Holmgren, N. Ramankutty, and J. A. Foley. 2010. Tropical forests were the primary sources of new agricultural land in the 1980s and 1990s. Proceedings of the National Academy of Sciences of the United States of America 107(38):16732–16737. http://dx.doi.org/10.1073/pnas.0910275107</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Giller, K. E., P. A. Tittonell, M. C. Rufino, M. T. van Wijk, S. Zingore, P. Mapfumo, S. Adjei-Nsiah, M. Herrero, R. Chikowo, M. Corbeels, E. C. Rowe, F. Baijukya, A. Mwijage, J. Smith, E. Yeboah, W. J. van der Burg, O. M. Sanogo, M. Misiko, N. De Ridder, S. Karanja, C. Kaizzi, J. K’ungu, M. Mwale, D. Nwaga, G. C. Pacini, and B. Vanlauwe. 2011. Communicating complexity: integrated assessment of trade-offs concerning soil fertility management within African farming systems to support innovation and development. Agricultural Systems 104(2):191-203. http://dx.doi.org/10.1016/j.agsy.2010.07.002</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Gómez, M. I., C. B. Barrett, T. Raney, P. Pinstrup-Andersen, J. Meerman, A. Croppenstedt, B. Carisma, and B. Thompson. 2013. Post-green revolution food systems and the triple burden of malnutrition. Food Policy 42:129–138. http://dx.doi.org/10.1016/j.foodpol.2013.06.009</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Green, R. E., S. J. Cornell, J. P. W. Scharlemann, and A. Balmford. 2005. Farming and the fate of wild nature. Science 307(5709):550-555. http://dx.doi.org/10.1126/science.1106049</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Gryseels, G. 1988. Role of livestock on mixed smallholder farms in the Ethiopian Highlands: a case study from the Baso and Worena Wereda near Debre Berhan. Wageningen University, Wageningen, The Netherlands.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Hardin, G. 1968. The tragedy of the commons. Science 162 (June):1243–1248.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Hladik, C. M., S. Bahuchet, and I. De Garine. 1990. Food and nutrition in the African rain forest. Unesca/MAB, Paris, France. Houérou, H. N. Le, and C. H. Hoste. 1977. Rangeland production and annual rainfall relations in the Mediterranean Basin and in the African Sahelo-Sudanian Zone. Journal of Range Management 30(3):181–189. http://dx.doi.org/10.2307/3897463</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Ickowitz, A., B. Powell, M. A. Salim, and T. C. H. Sunderland. 2014. Dietary quality and tree cover in Africa. Global Environmental Change 24(1):287–294. http://dx.doi.org/10.1016/j.gloenvcha.2013.12.001</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">International Food Policy Research Institute (IFPRI). 2014. Global nutrition report 2014: actions and accountability to accelerate the world’s progress on nutrition. Washington, D.C., USA</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Jagger, P., and J. Pender. 2003. The role of trees for sustainable management of less-favored lands: the case of eucalyptus in Ethiopia. Forest Policy and Economics 5:83–95. http://dx.doi.org/10.1016/S1389-9341(01)00078-8</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Johnson, K. B., A. Jacob, and M. E. Brown. 2013. Forest cover associated with improved child health and nutrition: evidence from the Malawi Demographic and Health Survey and satellite data. Global Health: Science and Practice 1(2):237–248. http://dx.doi.org/10.9745/ghsp-d-13-00055</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Jones, A. D., A. Shrinivas, and R. Bezner-Kerr. 2014. Farm production diversity is associated with greater household dietary diversity in Malawi: findings from nationally representative data. Food Policy 46:1–12. http://dx.doi.org/10.1016/j.foodpol.2014.02.001</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Kanshie, K. T. 2002. Five thousand years of sustainablity? A case study on Gedeo land use (Southern Ethiopia). Treemail Publishers, Heelsum, The Netherlands.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Kaschula, S. A., W. E. Twine, and M. C. Scholes. 2005. Coppice harvesting of fuelwood species on a South African common: utilizing scientific and indigenous knowledge in community based natural resource management. Human Ecology 33(3):387–418. http://dx.doi.org/10.1007/s10745-005-4144-7</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Kennedy, G., A. Berardo, C. Papavero, P. Horjus, T. Ballard, M. Dop, J. Delbaere, and I. D. Brouwer. 2010. Proxy measures of household food consumption for food security assessment and surveillance: comparison of the household dietary diversity and food consumption scores. Public Health Nutrition 13(12):2010-2018. http://dx.doi.org/10.1017/s136898001000145x</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Kleijn, D., F. Kohler, A. Báldi, P. Batáry, E. D. Concepción, Y. Clough, M. Díaz, D. Gabriel, A. Holzschuh, E. Knop, A. Kovács, E. J. P. Marshall, T. Tscharntke, and J. Verhulst. 2009. On the relationship between farmland biodiversity and land-use intensity in Europe. Proceedings of the Royal Society B: Biological Sciences 276(1658):903–909. http://dx.doi.org/10.1098/rspb.2008.1509</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Murphy, S. P., and L. H. Allen. 2003. Nutritional importance of animal source foods. Journal of Nutrition 133(1):3932S–3935S. Muthayya, S., J. H. Rah, J. D. Sugimoto, F. F. Roos, K. Kraemer, and R. E. Black. 2013. The global hidden hunger indices and maps: an advocacy tool for action. PLoS ONE 8(6):1–12. http://dx.doi.org/10.1371/journal.pone.0067860</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Nasi, R., A. Taber, and N. Van Vliet. 2011. Empty forests, empty stomachs? Bushmeat and livelihoods in the Congo and Amazon Basins. International Forestry Review 13(3):355–368. http://dx.doi.org/10.1505/146554811798293872</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Ong, C. K., C. R. Black, J. S. Wallace, A. A. H. Khan, J. E. Lott, N. A. Jackson, S. B. Howard, and D. M. Smith. 2000. Productivity, microclimate and water use in Grevillea robusta-based agroforestry systems on hillslopes in semi-arid Kenya. Agriculture, Ecosystems &amp; Environment 80(1–2):121–141. http://dx.doi.org/10.1016/s0167-8809(00)00144-4</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Ostrom, E., J. Walker, and R. Gardner. 1992. Covenants with and without a sword: self-governance is possible. American Political Science Review 86:404–417. http://dx.doi.org/10.2307/1964229</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Pfund, J. L., J. D. Watts, M. Boissière, A. Boucard, R. M. Bullock, A. Ekadinata, S. Dewi, L. Feintrenie, P. Levang, S. Rantala, D. Sheil, T. C. H. Sunderland, and Z. L. Urech. 2011. Understanding and integrating local perceptions of trees and forests into incentives for sustainable landscape management. Environmental Management 48(2):334–349. http://dx.doi.org/10.1007/s00267-011-9689-1</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Phalan, B., M. Onial, A. Balmford, and R. E. Green. 2011. Reconciling food production and biodiversity conservation: land sharing and land sparing compared. Science 333(6047):1289-1291. http://dx.doi.org/10.1126/science.1208742</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Pimentel, D., C. Wilson, C. McCullum, R. Huang, P. Dwen, J. Flack, Q. Tran, T. Saltman, and B. Cliff. 1997. Economic and environmental benefits of biodiversity. BioScience 47(11):747-757. http://dx.doi.org/10.2307/1313097</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Powell, B., J. Hall, and T. Johns. 2011. Forest cover, use and dietary intake in the East Usambara Mountains, Tanzania. International Forestry Review 13(3):305–317. http://dx.doi.org/10.1505/146554811798293944</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Powell, J. M., R. A. Pearson, and P. H. Hiernaux. 2004. Crop-livestock interactions in the West African drylands. Agronomy Journal 96(2):469–483. http://dx.doi.org/10.2134/agronj2004.0469</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Reed, J., J. van Vianen, E. L. Deakin, J. Barlow, and T. Sunderland. 2016. Integrated landscape approaches to managing social and environmental issues in the tropics: learning from the past to guide the future. Global Change Biology 22:2540–2554. http://dx.doi.org/10.1111/gcb.13284</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Reed, J., J. van Vianen, S. Foli, J. Clendenning, K. Yang, M. MacDonald, G. Petrokofsky, C. Padoch, and T. Sunderland. 2017. Trees for life: the ecosystem service contribution of trees to food production and livelihoods in the tropics. Forest Policy and Economics. http://dx.doi.org/10.1016/j.forpol.2017.01.012</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Remans, R., F. A. J. DeClerck, G. Kennedy, and J. Fanzo. 2015. Expanding the view on the production and dietary diversity link: scale, function, and change over time. Proceedings of the National Academy of Sciences of the United States of America 112(45): E6082. http://dx.doi.org/10.1073/pnas.1518531112</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Remans, R., J. Fanzo, C. A. Palm, and F. A. J. DeClerck. 2011. Ecological approaches to human nutrition. In F. A. De Clerck, C. Rumbaitis Del Rio, and J. C. Ingram, editors. Integrating ecology and poverty alleviation and international development efforts: a practical guide. Springer, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Sanchez, P. A., R. J. Buresh, and R. R. B. Leakey. 1997. Trees, soils, and food security. Philosophical Transactions of the Royal Society B: Biological Sciences 352:949–961. http://dx.doi.org/10.1098/rstb.1997.0074</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Sayer, J., T. C. H. Sunderland, J. Ghazoul, J.-L. Pfund, D. Sheil, E. Meijaard, M. Venter, A. K. Boedhihartono, M. Day, C. Garcia, C. van Oosten, and L. E. Buck. 2013. Ten principles for a landscape approach to reconciling agriculture, conservation, and other competing land uses. Proceedings of the National Academy of Sciences of the United States of America 110(21):8349–8356. http://dx.doi.org/10.1073/pnas.1210595110</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Shackleton, C., and S. Shackleton. 2004. The importance of nontimber forest products in rural livelihood security and as safety nets: a review of evidence from South Africa. South African Journal of Science 100(11–12):658–664. http://dx.doi.org/10.1016/j.forpol.2006.03.004</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Shackleton, C. M., S. E. Shackleton, E. Buiten, and N. Bird. 2007. The importance of dry woodlands and forests in rural livelihoods and poverty alleviation in South Africa. Forest Policy and Economics 9(5):558–577.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Sibhatu, K. T., V. V Krishna, and M. Qaim. 2015. Production diversity and dietary diversity in smallholder farm households. Proceedings of the National Academy of Sciences of the United States of America 112(34):10657–10662. http://dx.doi.org/10.1073/pnas.1510982112</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Smith, S. E., and D. J. Read. 2008. Mycorrhizal symbiosis. Academic Press.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Steinfeld, H., P. Gerber, T. Wassenaar, V. Castel, M. Rosales, and C. de Haan. 2006. Livestock’s long shadow: environmental issues and options. Food and Agriculture Organization of the United Nations, Rome, Italy.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Steyn, N. P., J. H. Nel, G. Nantel, G. Kennedy, and D. Labadarios. 2006. Food variety and dietary diversity scores in children: Are they good indicators of dietary adequacy? Public Health Nutrition 9(5):644–650. http://dx.doi.org/10.1079/PHN2005912</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Termote, C., P. Van Damme, and B. D. a Djailo. 2011. Eating from the wild: Turumbu, Mbole and Bali traditional knowledge on non-cultivated edible plants, District Tshopo, DRCongo. Genetic Resources and Crop Evolution 58(4):585–618. http://dx.doi.org/10.1007/s10722-010-9602-4</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Tesfaye, G. 2007. Structure, biomass and net primary production in dry tropical afromontane forest in Ethiopia. Addis Ababa University, Addis Ababa, Ethiopia.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Tittonell, P. A. 2014. Ecological intensification of agriculture – sustainable by nature. Current Opinion in Environmental Sustainability 8:53–61. http://dx.doi.org/10.1016/j.cosust.2014.08.006</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Tscharntke, T., A.-M. Klein, A. Kruess, I. Steffan-Dewenter, and C. Thies. 2005. Landscape perspectives on agricultural intensification and biodiversity – ecosystem service management. Ecology Letters 8(8):857–874. http://dx.doi.org/10.1111/j.1461-0248.2005.00782.x</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Vinceti, B., P. Eyzaguirre, and T. Johns. 2008. The nutritional role of forest plant foods for rural communities. Pages 65–96 in C. J. P. Colfer, editor. Human health and forests: a global overview of issues, practice and policy. Earthscan, London, U.K.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Watt, M., and J. R. Evans. 1999. Proteoid roots. Physiology and development. Plant Physiology 121:317–323. http://dx.doi.org/10.1104/pp.121.2.317</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Williams, T. O. 1998. Multiple uses of common pool resources in semi-arid West Africa: a survey of existing practices and options for sustainable resource management. ODI Natural Resource Perspectives 38(38).</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Young, A. 1989. Agroforestry for soil conservation. CAB International.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Young, V. R., and P. L. Pellett. 1994. Plant proteins in relation to human protein and amino acid nutrition. American Journal of Clinical Nutrition 59(5):1203S–1212S.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
