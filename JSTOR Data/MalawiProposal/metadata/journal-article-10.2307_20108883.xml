<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">economicj</journal-id>
         <journal-id journal-id-type="jstor">j100143</journal-id>
         <journal-title-group>
            <journal-title>The Economic Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn pub-type="ppub">00130133</issn>
         <issn pub-type="epub">14680297</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20108883</article-id>
         <title-group>
            <article-title>Used-Clothing Donations and Apparel Production in Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Garth</given-names>
                  <surname>Frazer</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>10</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">118</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">532</issue>
         <issue-id>i20108870</issue-id>
         <fpage>1764</fpage>
         <lpage>1784</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Royal Economic Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20108883"/>
         <abstract>
            <p>This article examines the importance of one possible explanation for the failure of African countries to step onto the bottom rung of the manufacturing sophistication ladder, that is to produce apparel. Used-clothing donations to thrift shops and other organisations in industrialised countries typically end up being sold to consumers in Africa. Since used clothing is initially provided as a donation, it shares characteristics with food aid, which always assists consumers, but at times harms African food producers. Used-clothing imports are found to have a negative impact on apparel production in Africa, explaining roughly 40% of the decline in production and 50% of the decline in employment over the period 1981-2000.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1235e125a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1235e132" publication-type="other">
Silverman (1986).</mixed-citation>
            </p>
         </fn>
         <fn id="d1235e139a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1235e146" publication-type="other">
Dhrymes (1970).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1235e162a1310">
            <mixed-citation id="d1235e166" publication-type="other">
Altonji, J. and Card, D.E. (1991). 'The effects of immigration on the labor market outcomes of less-skilled
natives', in (J.M. Abowd and R.B. Freeman, eds), Immigration, Trade and the Labor Market, Chicago:
University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d1235e179a1310">
            <mixed-citation id="d1235e183" publication-type="other">
Blundell, R. and Powell, J.L. (2003). 'Endogeneity in nonparametric and semiparametric regression models',
in (M. Dewatripont, et al., eds.), Advances in Economics and Econometrics: Theory and Publications, Eighth
World Congress, Vol. II, pp. 312-57, Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1235e196a1310">
            <mixed-citation id="d1235e200" publication-type="other">
Caselli, F. and Coleman, II, W.J. (2001). 'The US structural transformation and regional convergence:
a reinterpretation', Journal of Political Economy, vol. 109(3), pp. 584-616.</mixed-citation>
         </ref>
         <ref id="d1235e210a1310">
            <mixed-citation id="d1235e214" publication-type="other">
Chenery, H.B. (1960). 'Patterns of industrial growth', American Economic Review, vol. 50(4), pp. 624-54.</mixed-citation>
         </ref>
         <ref id="d1235e222a1310">
            <mixed-citation id="d1235e226" publication-type="other">
Chenery, H.B. and Syrquin, M. (1975). Patterns of Development, 1950-1970. London: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1235e233a1310">
            <mixed-citation id="d1235e237" publication-type="other">
Chenery, H.B. and Syrquin, M. (1988). 'Patterns of development, 1950 to 1983', World Bank Discussion
Papers no. 41</mixed-citation>
         </ref>
         <ref id="d1235e247a1310">
            <mixed-citation id="d1235e251" publication-type="other">
Collier, P. and Gunning, J.W. (1999). 'Explaining African economic performance', Journal of Economic Liter-
ature, vol. 37 (March), pp. 64-111.</mixed-citation>
         </ref>
         <ref id="d1235e261a1310">
            <mixed-citation id="d1235e265" publication-type="other">
Dhrymes, P.J. (1970). Econometrics: Statistical Foundations and Applications, New York: Springer-Verlag.</mixed-citation>
         </ref>
         <ref id="d1235e272a1310">
            <mixed-citation id="d1235e276" publication-type="other">
Easterly, W. and Levine, R. (1997). 'Africa's growth tragedy: policies and ethnic divisions', Quarterly Journal
of Economics, vol. 112(4), pp. 1203-50.</mixed-citation>
         </ref>
         <ref id="d1235e286a1310">
            <mixed-citation id="d1235e290" publication-type="other">
Fafchamps, M. (2004). Market Institutions in Sub-Saharan Africa, Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d1235e298a1310">
            <mixed-citation id="d1235e302" publication-type="other">
Fisher, F.M. (1963). 'A theoretical analysis of the impact of food surplus disposal on agricultural production
in recipient countries', Journal of Farm Economics, vol. 45, pp. 863-75.</mixed-citation>
         </ref>
         <ref id="d1235e312a1310">
            <mixed-citation id="d1235e316" publication-type="other">
Frankel, J.A. and Romer, D. (1999). 'Does trade cause growth?' American Economic Review, vol. 89(3), (June)
pp. 379-99.</mixed-citation>
         </ref>
         <ref id="d1235e326a1310">
            <mixed-citation id="d1235e330" publication-type="other">
Gollin, D., Parente, S. and Rogerson, R. (2002). 'The role of agriculture in development', American Economic
Review, vol. 92(2), pp. 160-4.</mixed-citation>
         </ref>
         <ref id="d1235e340a1310">
            <mixed-citation id="d1235e344" publication-type="other">
Haggblade, S. (1990). 'The flip side of fashion: used clothing exports to the third world', Journal of Development
Studies, vol. 26(3), pp. 505-21.</mixed-citation>
         </ref>
         <ref id="d1235e354a1310">
            <mixed-citation id="d1235e358" publication-type="other">
Hansen, K.T. (2000). Salaula: The World of Secondhand Clothing and Zambia, Chicago: University of Chicago
Press.</mixed-citation>
         </ref>
         <ref id="d1235e368a1310">
            <mixed-citation id="d1235e372" publication-type="other">
Johnson, G.E. (1980). 'The labor market effects of immigration', Industrial and Labor Relations Review,
vol. 33(3), pp. 331-41.</mixed-citation>
         </ref>
         <ref id="d1235e383a1310">
            <mixed-citation id="d1235e387" publication-type="other">
Kuznets, S. (1971). Economic Growth of Nations: Total Output and Production Structure, Cambridge, MA: Harvard
University Press.</mixed-citation>
         </ref>
         <ref id="d1235e397a1310">
            <mixed-citation id="d1235e401" publication-type="other">
Laitner, J. (2000). 'Structural change and economic growth', Review of Economic Studies, vol. 67(3), pp. 545-61.</mixed-citation>
         </ref>
         <ref id="d1235e408a1310">
            <mixed-citation id="d1235e412" publication-type="other">
Matsuyama, K. (1992). 'A simple model of sectoral adjustment', Review of Economic Studies, vol. 59(2), pp. 375-
88.</mixed-citation>
         </ref>
         <ref id="d1235e422a1310">
            <mixed-citation id="d1235e426" publication-type="other">
McCormick, D., Kinyanjui, M.J. and Ongile, G. (1997). 'Growth and barriers to growth among Nairobi's small
and medium-sized garment producers', World Development, vol. 25(7), pp. 1095-110.</mixed-citation>
         </ref>
         <ref id="d1235e436a1310">
            <mixed-citation id="d1235e440" publication-type="other">
Murphy, K.M., Shleifer, A. and Vishny, R. (1989). 'Income distribution, market size and industrialization',
Quarterly Journal of Economics, vol. 104(3), pp. 537-64.</mixed-citation>
         </ref>
         <ref id="d1235e450a1310">
            <mixed-citation id="d1235e454" publication-type="other">
Newey, W.K., Powell, J.L. and Vella, F. (1999). 'Nonparametric estimation of triangular simultaneous equa-
tions models', Econometrica, vol. 67, pp. 565-603.</mixed-citation>
         </ref>
         <ref id="d1235e465a1310">
            <mixed-citation id="d1235e469" publication-type="other">
Romalis, J. (2004). 'Factor proportions and the structure of commodity trade', American Economic Review,
vol. 94(1), (March) pp. 67-97.</mixed-citation>
         </ref>
         <ref id="d1235e479a1310">
            <mixed-citation id="d1235e483" publication-type="other">
Sambanis, N. (2000). 'Partition as a solution to ethnic war: an empirical critique of the theoretical literature',
World Politics, vol. 52 (July), pp. 437-83.</mixed-citation>
         </ref>
         <ref id="d1235e493a1310">
            <mixed-citation id="d1235e497" publication-type="other">
Schultz, T.W. (1960). 'Value of US farm surpluses to underdeveloped countries', Journal of Farm Economics,
vol. 42, pp. 1019-30.</mixed-citation>
         </ref>
         <ref id="d1235e507a1310">
            <mixed-citation id="d1235e511" publication-type="other">
U.S.Department of Commerce (USDOC), International Trade Administration. (2003). U.S. African Trade
Profile, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1235e521a1310">
            <mixed-citation id="d1235e525" publication-type="other">
U.S. International Trade Commission (USITC). (1999) U.S.-Africa Trade Flows and Effects of the Uruguay Round
Agreements and U.S. Trade and Development Policy, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1235e535a1310">
            <mixed-citation id="d1235e539" publication-type="other">
U.S. Trade Representative (USTR). (2001). U.S. Trade and Investment Policy Toward Sub-Saharan Africa and
Implementation of the African Growth and Opportunity Act, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1235e550a1310">
            <mixed-citation id="d1235e554" publication-type="other">
Ventura, J. (1997). 'Growth and interdependence', Quarterly Journal of Economics, vol. 112 (February),
pp. 57-84.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
