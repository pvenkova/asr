<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101235</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02586770</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1564698X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40282222</article-id>
         <article-id pub-id-type="pub-doi">10.1093/wber/lhl006</article-id>
         <title-group>
            <article-title>When Is External Debt Sustainable?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Aart</given-names>
                  <surname>Kraay</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Vikram</given-names>
                  <surname>Nehru</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">20</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40011163</issue-id>
         <fpage>341</fpage>
         <lpage>365</lpage>
         <permissions>
            <copyright-statement>Copyright 2006 The International Bank for Reconstruction and Development/The World Bank</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40282222"/>
         <abstract>
            <p>The article empirically examines the determinants of debt distress, defined as periods in which countries resort to any of three forms of exceptional finance: significant arrears on external debt, Paris Club rescheduling, and nonconcessional International Monetary Fund lending. Probit regressions show that three factors explain a substantial fraction of the crosscountry and time-series variation in the incidence of debt distress: the debt burden, the quality of policies and institutions, and shocks. The relative importance of these factors varies with the level of development. These results are robust to a variety of alternative specifications, and the core specifications have substantial out-of-sample predictive power. The quantitative implications of these results are examined for the lending strategies of official creditors.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1366e158a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1366e165" publication-type="other">
Birdsall, Claessens, and Diwan (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1366e172a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1366e179" publication-type="other">
Underwood (1991) and Cohen (1996).</mixed-citation>
            </p>
         </fn>
         <fn id="d1366e186a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1366e193" publication-type="other">
Reinhart, Rogoff, and Savastano (2003),</mixed-citation>
            </p>
         </fn>
         <fn id="d1366e200a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1366e207" publication-type="other">
IMF (2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d1366e215a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1366e222" publication-type="other">
Collins (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1366e228" publication-type="other">
Manasse, Roubini, and Schimmelpfenning (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1366e235a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1366e242" publication-type="other">
(Kraay and Nehru 2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d1366e249a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1366e256" publication-type="other">
(Kraay and Nehru 2004).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1366e272a1310">
            <mixed-citation id="d1366e276" publication-type="other">
Aylward, Lynn, and Rupert Thorne. 1998. "An Econometric Analysis of Countries' Repayment Perfor-
mance to the International Monetary Fund." IMF Working Paper 98/32. International Monetary
Fund, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e289a1310">
            <mixed-citation id="d1366e293" publication-type="other">
Berg, Andrew, and Jeffrey Sachs. 1988. "The Debt Crisis: Structural Explanations of Country Perfor-
mance." Journal of Development Economics 29(3):271-306.</mixed-citation>
         </ref>
         <ref id="d1366e303a1310">
            <mixed-citation id="d1366e307" publication-type="other">
Birdsall, Nancy, C. Claessens, and I. Diwan. 2003. "Policy Selectivity Forgone: Debt and Donor Behavior
in Africa." World Bank Economic Review 17(3):409-35.</mixed-citation>
         </ref>
         <ref id="d1366e317a1310">
            <mixed-citation id="d1366e321" publication-type="other">
Cline, William. 1984. International Debt: Systemic Risk and Policy Response. Washington, D.C:
Institute of International Economics.</mixed-citation>
         </ref>
         <ref id="d1366e332a1310">
            <mixed-citation id="d1366e336" publication-type="other">
Cohen, Daniel. 1996. "The Sustainability of African Debt." Policy Research Working Paper 1691. World
Bank, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e346a1310">
            <mixed-citation id="d1366e350" publication-type="other">
Collins, Susan. 2003. "Probabilities, Probits, and the Timing of Currency Crises." Georgetown Univer-
sity, Department of Economics, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e360a1310">
            <mixed-citation id="d1366e364" publication-type="other">
Detragiache, Enrica, and Antonio Spilimbergo. 2001. "Crises and Liquidity: Evidence and Interpretation."
IMF Working Paper 01/2. International Monetary Fund, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e374a1310">
            <mixed-citation id="d1366e378" publication-type="other">
Dikhanov, Yuri. 2003. "Reconstruction of Historical Present Value of Debt for Developing Countries,
1980-2001: Methodology and Calculations." World Bank, Development Economics Data Group,
Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e391a1310">
            <mixed-citation id="d1366e395" publication-type="other">
IMF (International Monetary Fund). Various years. International Financial Statistics. Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e402a1310">
            <mixed-citation id="d1366e406" publication-type="other">
---. 2002. "Evaluation of the Prolonged Use of Fund Resources." Independent Evaluation Office,
International Monetary Fund, Washington D.C.</mixed-citation>
         </ref>
         <ref id="d1366e417a1310">
            <mixed-citation id="d1366e421" publication-type="other">
Kaufmann, Daniel, Aart Kraay, and Massimo Mastruzzi. 2005. "Governance Matters IV: Updated
Governance Indicators for 1996-2004." World Bank Policy Research Working Paper 3630. World
Bank, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e434a1310">
            <mixed-citation id="d1366e438" publication-type="other">
Kraay, Aart, and Vikram Nehru. 2004. "When is External Debt Sustainable?" Policy Research Working
Paper 3200. World Bank, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e448a1310">
            <mixed-citation id="d1366e452" publication-type="other">
Lloyd-Ellis, H., G. W. McKenzie, and S. H. Thomas. 1990. "Predicting the Quantity of LDC Debt
Rescheduling." Economics Letters 32(1):67-73.</mixed-citation>
         </ref>
         <ref id="d1366e462a1310">
            <mixed-citation id="d1366e466" publication-type="other">
Manasse, Paolo, Nouriel Roubini, and Axel Schimmelpfenning. 2003. "Predicting Sovereign Debt Crises."
IMF Working Paper 03/221. International Monetary Fund, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e476a1310">
            <mixed-citation id="d1366e480" publication-type="other">
McFadden, Daniel, Richard Eckaus, Gershon Feder, Vassilis Hajivassiliou, and Stephen O'Connell. 1985.
"Is There Life After Debt? An Econometric Analysis of the Creditworthiness of Developing Coun-
tries." In Gordon Smith and John Cuddington, eds., International Debt and the Developing Countries.
Washington, D.C: World Bank.</mixed-citation>
         </ref>
         <ref id="d1366e496a1310">
            <mixed-citation id="d1366e500" publication-type="other">
McKenzie, David. 2004. "An Econometric Analysis of IBRD Creditworthiness." International Economic
Journal 18(4):427-48.</mixed-citation>
         </ref>
         <ref id="d1366e511a1310">
            <mixed-citation id="d1366e515" publication-type="other">
Raddatz, Claudio. 2005. "Are External Shocks Responsible for the Instability of Output in Low-Income
Countries?" Policy Research Working Paper 3680. World Bank, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e525a1310">
            <mixed-citation id="d1366e529" publication-type="other">
Reinhart, Carmen, Kenneth Rogoff, and Miguel Savastano. 2003. "Debt Intolerance." Brookings Papers
on Economic Activity 1:1-74.</mixed-citation>
         </ref>
         <ref id="d1366e539a1310">
            <mixed-citation id="d1366e543" publication-type="other">
Underwood, John. 1991. "The Sustainability of International Debt." World Bank, International Finance
Division, Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e553a1310">
            <mixed-citation id="d1366e557" publication-type="other">
Wooldridge, Jeffrey. 2005. "Simple Solutions to the Initial Conditions Problem in Dynamic, Nonlinear
Panel Data Models with Unobserved Heterogeneity." Journal of Applied Econometrics 20(1):39-54.</mixed-citation>
         </ref>
         <ref id="d1366e567a1310">
            <mixed-citation id="d1366e571" publication-type="other">
World Bank. 2003. "Special Purpose Financial Statements and Internal Control Reports of the Interna-
tional Development Association." In The World Bank Annual Report 2003. Vol. 2: Financial State-
ments and Appendixes. Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e584a1310">
            <mixed-citation id="d1366e588" publication-type="other">
---. Various years. Global Development Finance. Washington, D.C.</mixed-citation>
         </ref>
         <ref id="d1366e596a1310">
            <mixed-citation id="d1366e600" publication-type="other">
--- . Various years. World Development Indicators Database. Washington, D.C.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
