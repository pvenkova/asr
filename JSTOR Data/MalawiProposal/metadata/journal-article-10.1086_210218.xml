<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">amerjsoci</journal-id>
         <journal-title-group>
            <journal-title>American Journal of Sociology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00029602</issn>
         <issn pub-type="epub">15375390</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor-stable">2990982</article-id>
         <article-id pub-id-type="jstor">210218</article-id>
         <article-id pub-id-type="msid">AJSv104p1597</article-id>
         <title-group>
            <article-title>Empirics of World Income Inequality<xref ref-type="fn" rid="FN1">
                  <sup>1</sup>
               </xref>
            </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Glenn</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Firebaugh</surname>
               </string-name>
               <x xml:space="preserve"/>
            </contrib>
            <aff id="aff_1">
               <italic>Pennsylania State University</italic>
            </aff>
            <x xml:space="preserve"/>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>05</month>
            <year>1999</year>
            <string-date>May 1999</string-date>
         </pub-date>
         <volume>104</volume>
         <issue>6</issue>
         <issue-id>ajs.1999.104.issue-6</issue-id>
         <fpage>1597</fpage>
         <lpage>1630</lpage>
         <permissions>
            <copyright-statement>© 1999 by The University of Chicago. All rights reserved.</copyright-statement>
            <copyright-year>1999</copyright-year>
            <copyright-holder>The University of Chicago</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/210218"/>
         <abstract>
            <p>This article employs a common general formula for inequality indexes to answer several basic questions about intercountry income inequality in recent decades: Has inequality across nations increased or declined (and why have earlier studies yielded mixed results)? Have different rates of population growth played a significant role in the trend? Have large nations dominated the trend? Are the results robust over different inequality measures and different income series? Two findings stand out. First, different rates of population growth in rich and poor nations played the predominant role in determining change in the distribution of per capita income across nations. Second, the centuries‐old trend of rising inequality leveled off from 1960 to 1989. The dependency theory thesis of a polarizing world system receives no support.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="FN1">
               <label>
                  <sup>1</sup>
               </label>
               <p>1 This article is based on research supported by NSF grants SBR‐9515153 and SBR‐9870870. I thank Bill Frase, Dana Haynie, and Jason Yarrington for research assistance and Barry Lee, Dan Lichter, and Jeff Manza for comments. Direct correspondence to Glenn Firebaugh, Department of Sociology, Pennsylvania State University, 205 Oswald Tower, University Park, Pennsylvania 16802. E‐mail:<email xlink:href="mailto:firebaug@pop.psu.edu" xlink:type="simple">firebaug@pop.psu.edu</email>.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
</article>
