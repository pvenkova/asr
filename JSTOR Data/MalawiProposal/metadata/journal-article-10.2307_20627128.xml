<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jvertpale</journal-id>
         <journal-id journal-id-type="jstor">j101365</journal-id>
         <journal-title-group>
            <journal-title>Journal of Vertebrate Paleontology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society of Vertebrate Paleontology</publisher-name>
         </publisher>
         <issn pub-type="ppub">02724634</issn>
         <issn pub-type="epub">19372809</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20627128</article-id>
         <title-group>
            <article-title>Taxonomic Revision and New Observations on the Postcranial Skeleton, Biogeography, and Biostratigraphy of the Dicynodont Genus Dicynodontoides, the Senior Subjective Synonym of Kingoria (Therapsida, Anomodontia)</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Kenneth D.</given-names>
                  <surname>Angielczyk</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Christian A.</given-names>
                  <surname>Sidor</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Sterling J.</given-names>
                  <surname>Nesbitt</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Roger M. H.</given-names>
                  <surname>Smith</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Linda A.</given-names>
                  <surname>Tsuji</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>12</day>
            <month>12</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i20627112</issue-id>
         <fpage>1174</fpage>
         <lpage>1187</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Society of Vertebrate Paleontology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20627128"/>
         <abstract>
            <p>The postcranial skeleton of Kingoria is well-described, and previous authors noted the taxon's divergent pectoral, pelvic, and femoral morphologies. Yet, humeral morphology of Kingoria has remained enigmatic because of poor preservation and the absence of published descriptions. Here we describe new, nearly complete humeri of Kingoria collected from the Upper Permian Usili Formation, Ruhuhu Basin, Tanzania. These specimens demonstrate that the humeral morphology of K. nowacki is much more conservative than that of its pelvis or femur. There is evidence of increased importance for long-axis rotation of the humerus in K. nowacki, and the trochlea and the capitellum are partially separated, but these morphologies are not taken to the extremes observed in cistecephalid dicynodonts. Surprisingly, the new Tanzanian humeri differ from South African specimens: the best-preserved South African specimen is more gracile, with different humeral head and deltopectoral crest morphologies. We use these differences, along with new observations on skull morphology, tusk frequency, pelvic and fibular morphology, and body size to revise the taxonomy of Kingoria. Kingoria is shown to be a junior synonym of Dicynodontoides Broom, 1940, and two species are recognized: D. recurvidens from South Africa and D. nowacki from Tanzania. Finally, we review the stratigraphic and geographic ranges of Dicynodontoides, and document new occurrences in the Pristerognathus and Tropidostoma assemblage zones of the Karoo Basin, as well as its presence in the Chiweta Beds of Malawi.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d2511e219a1310">
            <mixed-citation id="d2511e223" publication-type="other">
Angielczyk, K. D. 2001. Preliminary phylogenetic analysis and strati-
graphic congruence of the dicynodont anomodonts (Synapsida:
Therapsida). Palaeontologia Africana 37:53-79.</mixed-citation>
         </ref>
         <ref id="d2511e236a1310">
            <mixed-citation id="d2511e240" publication-type="other">
Angielczyk, K. D. 2002a. Redescription, phylogenetic position, and strat-
igraphic significance of the dicynodont genus Odontocyclops
(Synapsida; Therapsida). Journal of Paleontology 76:1047-1059.</mixed-citation>
         </ref>
         <ref id="d2511e253a1310">
            <mixed-citation id="d2511e257" publication-type="other">
Angielczyk, K. D. 2002b. A character-based method for measuring
the fit of a cladogram to the fossil record. Systematic Biology
51:176-191.</mixed-citation>
         </ref>
         <ref id="d2511e270a1310">
            <mixed-citation id="d2511e274" publication-type="other">
Angielczyk, K. D. 2007. New specimens of the Tanzanian dicynodont
"Cryptocynodon" parringtoni von Huene, 1942 (Therapsida, Anom-
odontia), with an expanded analysis of Permian dicynodont phylog-
eny. Journal of Vertebrate Paleontology 27:116-131.</mixed-citation>
         </ref>
         <ref id="d2511e291a1310">
            <mixed-citation id="d2511e295" publication-type="other">
Angielczyk, K. D., and A. A. Kurkin. 2003. Phylogenetic analysis of
Russian Permian dicynodonts (Therapsida: Anomodontia) implica-
tions for Permian biostratigraphy and Pangaean biogeography. Zoo-
logical Journal of the Linnean Society 139:157-212.</mixed-citation>
         </ref>
         <ref id="d2511e311a1310">
            <mixed-citation id="d2511e315" publication-type="other">
Angielczyk, K. D., and C. Sullivan. 2008. Diictodon feliceps (Owen,
1876), a dicynodont (Therapsida, Anomodontia) species with a Pan-
gaean distribution. Journal of Vertebrate Paleontology 28:788-802.</mixed-citation>
         </ref>
         <ref id="d2511e328a1310">
            <mixed-citation id="d2511e332" publication-type="other">
Angielczyk, K. D, J. Fröbisch, and R. M. H. Smith. 2005. On the strati-
graphic range of the dicynodont taxon Emydops (Therapsida,
Anomodontia) in the Karoo Basin, South Africa. Palaeontologia
Africana 41:23-33.</mixed-citation>
         </ref>
         <ref id="d2511e348a1310">
            <mixed-citation id="d2511e352" publication-type="other">
Botha, J., and K. D. Angielczyk. 2007. An integrative approach to
distinguishing the Late Permian dicynodont species Oudenodon
bainii and Tropidostoma microtrema (Therapsida, Anomodontia).
Palaeontology 50:1175-1209.</mixed-citation>
         </ref>
         <ref id="d2511e368a1310">
            <mixed-citation id="d2511e372" publication-type="other">
Botha, J., and R. M. H. Smith. 2006. Rapid vertebrate recuperation in
the Karoo Basin of South Africa following the end-Permian extinc-
tion. Journal of African Earth Sciences 45:502-514.</mixed-citation>
         </ref>
         <ref id="d2511e385a1310">
            <mixed-citation id="d2511e389" publication-type="other">
Brink, A. S., and A. W. Keyser. 1986. Illustrated bibliographic catalogue
of the Synapsida. Geological Survey of South Africa Handbook 10:
J212A251A.</mixed-citation>
         </ref>
         <ref id="d2511e403a1310">
            <mixed-citation id="d2511e407" publication-type="other">
Broom, R. 1901. On the structure and affinities of Udenodon. Proceed-
ings of the Zoological Society of London 1901:162-190.</mixed-citation>
         </ref>
         <ref id="d2511e417a1310">
            <mixed-citation id="d2511e421" publication-type="other">
Broom, R. 1905a. On the structure and affinities of the endothiodont reptiles.
Transactions of the South African Philosophical Society 15:259-282.</mixed-citation>
         </ref>
         <ref id="d2511e431a1310">
            <mixed-citation id="d2511e435" publication-type="other">
Broom, R. 1905b. On the use of the term Anomodontia. Albany Muse-
um Records 1:266-269.</mixed-citation>
         </ref>
         <ref id="d2511e445a1310">
            <mixed-citation id="d2511e449" publication-type="other">
Broom, R. 1925. On the pelvis and sacrum of Dicynodon. Records of the
Albany Museum 3:327-330.</mixed-citation>
         </ref>
         <ref id="d2511e459a1310">
            <mixed-citation id="d2511e463" publication-type="other">
Broom, R. 1940. On some new genera and species of fossil reptiles from
the Karroo Beds of Graaff-Reinet. Annals of the Transvaal Museum
20:157-192.</mixed-citation>
         </ref>
         <ref id="d2511e476a1310">
            <mixed-citation id="d2511e480" publication-type="other">
Broom, R. 1948. A contribution to our knowledge of the vertebrates of
the Karoo Beds of South Africa. Transactions of the Royal Society
of Edinburgh 61:577-629.</mixed-citation>
         </ref>
         <ref id="d2511e494a1310">
            <mixed-citation id="d2511e498" publication-type="other">
Broom, R. 1950. Three new species of anomodonts from the Rubidge
Collection. Annals of the Transvaal Museum 21:246-250.</mixed-citation>
         </ref>
         <ref id="d2511e508a1310">
            <mixed-citation id="d2511e512" publication-type="other">
Broom, R. and J. T. Robinson. 1948. Some new fossil reptiles from the
Karroo Beds of South Africa. Proceedings of the Zoological Society
of London 118:392-407.</mixed-citation>
         </ref>
         <ref id="d2511e525a1310">
            <mixed-citation id="d2511e529" publication-type="other">
Camp, C. L., and S. P. Welles. 1956. Triassic dicynodont reptiles. Part I.
The North American genus Placerias. Memoirs of the University of
California 13:255-304.</mixed-citation>
         </ref>
         <ref id="d2511e542a1310">
            <mixed-citation id="d2511e546" publication-type="other">
Cluver, M. A. 1978. The skeleton of the mammal-like reptile Cistecepha-
lus with evidence for a fossorial mode of life. Annals of the South
African Museum 76:213-246.</mixed-citation>
         </ref>
         <ref id="d2511e559a1310">
            <mixed-citation id="d2511e563" publication-type="other">
Cluver, M. A., and N. Hotton. 1981. The genera Dicynodon and Diictodon
and their bearing on the classification or the Dicynodontia (Reptilia,
Therapsida). Annals of the South African Museum 83:99-146.</mixed-citation>
         </ref>
         <ref id="d2511e576a1310">
            <mixed-citation id="d2511e580" publication-type="other">
Cluver, M. A., and G. M. King. 1983. A reassessment of the relationships
of Permian Dicynodontia (Reptilia, Therapsida) and a new classifica-
tion of dicynodonts. Annals of the South African Museum 91:195-273.</mixed-citation>
         </ref>
         <ref id="d2511e594a1310">
            <mixed-citation id="d2511e598" publication-type="other">
Cox, C. B. 1959. On the anatomy of a new dicynodont genus with evi-
dence of the position of the tympanum. Proceedings of the Zoologi-
cal Society of London 132:321-367.</mixed-citation>
         </ref>
         <ref id="d2511e611a1310">
            <mixed-citation id="d2511e615" publication-type="other">
Cox, C. B. 1972. A new digging dicynodont from the Upper Permian of
Tanzania; pp. 173-190 in K. A. Joysey and T. S. Kemp (eds.), Stud-
ies in Vertebrate Evolution. Oliver and Boyd, Edinburgh.</mixed-citation>
         </ref>
         <ref id="d2511e628a1310">
            <mixed-citation id="d2511e632" publication-type="other">
Cox, C. B. 1998. The jaw function and adaptive radiation of the dicyno-
dont mammal-like reptiles of the Karoo Basin of South Africa.
Zoological Journal of the Linnean Society 122:349-384.</mixed-citation>
         </ref>
         <ref id="d2511e645a1310">
            <mixed-citation id="d2511e649" publication-type="other">
DeFauw, S. L. 1986. The Appendicular Skeleton of African Dicyno-
donts. Ph.D. Dissertation, Wayne State University, Detroit, Michi-
gan, 284 pp.</mixed-citation>
         </ref>
         <ref id="d2511e662a1310">
            <mixed-citation id="d2511e666" publication-type="other">
Drysdall, A. R. and J. W. Kitching. 1963. A re-examination of the Kar-
roo succession and fossil localities of part of the Upper Luangwa
Valley. Geological Survey of Northern Rhodesia Memoir 1:1-62.</mixed-citation>
         </ref>
         <ref id="d2511e679a1310">
            <mixed-citation id="d2511e683" publication-type="other">
Fröbisch, J. 2006. Locomotion in derived dicynodonts (Synapsida, Anomo-
dontia): a functional analysis of the pelvic girdle and hind limb of
Tetragonias njalilus. Canadian Journal of Earth Sciences 43:1297-1308.</mixed-citation>
         </ref>
         <ref id="d2511e697a1310">
            <mixed-citation id="d2511e701" publication-type="other">
Fröbisch, J. 2007. The cranial anatomy of Kombuisia frerensis Hotton
(Synapsida, Dicynodontia) and a new phylogeny of anomodont
therapsids. Zoological Journal of the Linnean Society 150:117-144.</mixed-citation>
         </ref>
         <ref id="d2511e714a1310">
            <mixed-citation id="d2511e718" publication-type="other">
Gay, S. A., and A. R. I. Cruickshank. 1999. Biostratigraphy of the
Permian tetrapod faunas from the Ruhuhu Valley, Tanzania. Jour-
nal of African Earth Sciences 29:195-210.</mixed-citation>
         </ref>
         <ref id="d2511e731a1310">
            <mixed-citation id="d2511e735" publication-type="other">
Govender, R. 2006. Morphological and Functional Analysis of the Post-
cranial Anatomy of Two Dicynodont Morphotypes from the Cynog-
nathus Assemblage Zone of South Africa and their Taxonomic
Implications. Ph.D. Dissertation University of the Witwatersrand,
Johannesburg, 179 pp.</mixed-citation>
         </ref>
         <ref id="d2511e754a1310">
            <mixed-citation id="d2511e758" publication-type="other">
Govender, R., P. J. Hancox, and A. M. Yates. 2008. Re-evaluation of the
postcranial skeleton of the Triassic dicynodont Kannemyeria simo-
cephalus from the Cynognathus Assemblage Zone (Subzone B) of
South Africa. Palaeontologia Africana 43:19-37.</mixed-citation>
         </ref>
         <ref id="d2511e774a1310">
            <mixed-citation id="d2511e778" publication-type="other">
Hoepen, E. C. N. van. 1934. Oor die indeling van die Dicynodontidae na
aanleiding van nuew vorme. Paleontologiese Navorsing van die
Nasionale Museum 2:67-101.</mixed-citation>
         </ref>
         <ref id="d2511e791a1310">
            <mixed-citation id="d2511e795" publication-type="other">
Huene, F. von. 1942. Die Anomodontier des Ruhuhu-Gebietes in der
Tübinger Sammlung. Palaeontographica Abteilung A 44:154-184.</mixed-citation>
         </ref>
         <ref id="d2511e806a1310">
            <mixed-citation id="d2511e810" publication-type="other">
Jacobs, L. L., D. A. Winkler, K. D. Newman, E. M. Gomani, and
A. Deino. 2005. Therapsids from the Permian Chiweta Beds and
the age of the Karoo Supergroup in Malawi. Palaeontologia Elec-
tronica 8:1-23.</mixed-citation>
         </ref>
         <ref id="d2511e826a1310">
            <mixed-citation id="d2511e830" publication-type="other">
Jenkins, F. A., Jr. 1971. The postcranial skeleton of African cynodonts.
Peabody Museum of Natural History Bulletin 36:1-216.</mixed-citation>
         </ref>
         <ref id="d2511e840a1310">
            <mixed-citation id="d2511e844" publication-type="other">
Kaaya, C. Z. 1992. Depositional environment of Late Permian Karoo beds
in the Ruhuhu Basin and Mikumi area of Tanzania. Geologisches
Institut der Universität zu Köln Sonderveröffentlichungen 83:1-126.</mixed-citation>
         </ref>
         <ref id="d2511e857a1310">
            <mixed-citation id="d2511e861" publication-type="other">
Keyser, A. W. 1975. A reevaluation of the cranial morphology and sys-
tematics of some tuskless Anomodontia. Geological Survey of South
Africa Memoir 67:1-110.</mixed-citation>
         </ref>
         <ref id="d2511e874a1310">
            <mixed-citation id="d2511e878" publication-type="other">
King, G. M. 1981a. The postcranial skeleton of Robertia broomiana, an
early dicynodont (Reptilia, Therapsida) from the South African
Karoo. Annals of the South African Museum 84:203-231.</mixed-citation>
         </ref>
         <ref id="d2511e891a1310">
            <mixed-citation id="d2511e895" publication-type="other">
King, G. M. 1981b. The functional anatomy of a Permian dicynodont.
Philosophical Transactions of the Royal Society of London Series B
291:243-322.</mixed-citation>
         </ref>
         <ref id="d2511e909a1310">
            <mixed-citation id="d2511e913" publication-type="other">
King, G. M. 1985. The postcranial skeleton of Kingoria nowacki (von
Huene) (Therapsida: Dicynodontia). Zoological Journal of the Lin-
nean Society 84:263-289.</mixed-citation>
         </ref>
         <ref id="d2511e926a1310">
            <mixed-citation id="d2511e930" publication-type="other">
King, G. M. 1988. Anomodontia; in P. Wellnhofer (ed.). Handbuch der Paläo-
herpetologie Volume 17C. Gustav Fischer Verlag, Stuttgart, 174 pp.</mixed-citation>
         </ref>
         <ref id="d2511e940a1310">
            <mixed-citation id="d2511e944" publication-type="other">
King, G. M. 1990. The Dicynodonts: A Study in Palaeobiology. Chap-
man and Hall, London, 233 pp.</mixed-citation>
         </ref>
         <ref id="d2511e954a1310">
            <mixed-citation id="d2511e958" publication-type="other">
King, G. M. 1992. The paleobiogeography of Permian anomodonts. Terra
Nova 4:633-640.</mixed-citation>
         </ref>
         <ref id="d2511e968a1310">
            <mixed-citation id="d2511e972" publication-type="other">
King, G. M. 1993. How many species of Diictodon were there? Annals of
the South African Museum 102:303-325.</mixed-citation>
         </ref>
         <ref id="d2511e982a1310">
            <mixed-citation id="d2511e986" publication-type="other">
Kitching, J. W. 1977. The distribution of the Karroo vertebrate fauna.
Memoirs of the Bernard Price Institute for Palaeontological Re-
search 1:1-111.</mixed-citation>
         </ref>
         <ref id="d2511e1000a1310">
            <mixed-citation id="d2511e1004" publication-type="other">
Lucas, S. G. 2006. Global Permian tetrapod biostratigraphy and biochro-
nology; pp. 65-93 in S. G. Lucas, G. Cassinis, and J. W. Schneider
(eds.), Non-marine Permian Biostratigraphy and Biochronology.
Geological Society, London, Special Publications 265.</mixed-citation>
         </ref>
         <ref id="d2511e1020a1310">
            <mixed-citation id="d2511e1024" publication-type="other">
Maisch, M. W. 1999. The Tetrapods form the Late Permian of Tanzania
in the Collections of the Institut und Museum für Geologie und
Paläontologie der Universität Tübingen, with Special Reference to
the Pristerodontian Dicynodonts Rhachiocephalus and Pelanomo-
don. Ph.D. Dissertation, Universität Tübingen, Germany, 362 pp.</mixed-citation>
         </ref>
         <ref id="d2511e1043a1310">
            <mixed-citation id="d2511e1047" publication-type="other">
Maisch, M. W. 2004. Postcranial morphology of Rhachiocephalus Seeley,
1898 (Therapsida: Dicynodontia) from the Upper Permian of Tan-
zania and the status of Platypodosaurus robustus Owen, 1880. Geo-
logica et Palaeontologica 38:161-175.</mixed-citation>
         </ref>
         <ref id="d2511e1063a1310">
            <mixed-citation id="d2511e1067" publication-type="other">
Nowack, E. 1937. Zur Kenntnis der Karruformation im Ruhuhu-Graben
(D.O.A). Neues Jahrbuch für Mineralogie, Geologie, und Paläonto-
logie Abteilung B 78:380-412.</mixed-citation>
         </ref>
         <ref id="d2511e1080a1310">
            <mixed-citation id="d2511e1084" publication-type="other">
Olson, E. C., and F. Byrne. 1938. The osteology of Aulacocephalodon
peavoti. Journal of Geology 46:177-190.</mixed-citation>
         </ref>
         <ref id="d2511e1094a1310">
            <mixed-citation id="d2511e1098" publication-type="other">
Owen, R. 1845. Description of certain fossil crania discovered by A. G.
Bain, Esq., in the sandstone rocks at the southeastern extremity of
Africa, referable to different species of an extinct genus of Reptilia
(Dicynodon), and indicative of a new tribe or suborder of Sauria.
Transactions of the Geological Society of London 7:59-84.</mixed-citation>
         </ref>
         <ref id="d2511e1118a1310">
            <mixed-citation id="d2511e1122" publication-type="other">
Owen, R. 1860. On the orders of fossil and recent Reptilia and their
distribution in time. Report of the British Association for the Ad-
vancement of Science for 1859:153-166.</mixed-citation>
         </ref>
         <ref id="d2511e1135a1310">
            <mixed-citation id="d2511e1139" publication-type="other">
Owen, R. 1876. Descriptive and Illustrated Catalogue of the Fossil Rep-
tilia in the Collection of the British Museum. Trustees of the British
Museum of Natural History, London, 88 pp.</mixed-citation>
         </ref>
         <ref id="d2511e1152a1310">
            <mixed-citation id="d2511e1156" publication-type="other">
Owen, R. 1880. Description of parts of the skeleton of an anomodont
reptile (Platypodosaurus robustus Ow.) from the Trias of Graaf
Reinet, S. Africa. Quarterly Journal of the Geological Society of
London 36:414-425.</mixed-citation>
         </ref>
         <ref id="d2511e1172a1310">
            <mixed-citation id="d2511e1176" publication-type="other">
Owen, R. 1881. Description of parts of the skeleton of an anomodont
reptile (Platypodosaurus robustus Owen). Part II. The pelvis. Quar-
terly Journal of the Geological Society of London 37:266-271.</mixed-citation>
         </ref>
         <ref id="d2511e1189a1310">
            <mixed-citation id="d2511e1193" publication-type="other">
Pearson, H. S. 1924. A dicynodont reptile reconstructed. Proceedings of
the Zoological Society of London 1924:827-855.</mixed-citation>
         </ref>
         <ref id="d2511e1203a1310">
            <mixed-citation id="d2511e1207" publication-type="other">
Ray, S. 2001. Small Permian dicynodonts from India. Paleontological
Research 5:177-191.</mixed-citation>
         </ref>
         <ref id="d2511e1218a1310">
            <mixed-citation id="d2511e1222" publication-type="other">
Ray, S. 2006. Functional and evolutionary aspects of the postcranial
anatomy of dicynodonts (Synapsida, Therapsida). Palaeontology
49:1263-1286.</mixed-citation>
         </ref>
         <ref id="d2511e1235a1310">
            <mixed-citation id="d2511e1239" publication-type="other">
Ray, S., and S. Bandyopadhyay. 2003. Late Permian vertebrate commu-
nity of the Pranhita-Godavari Valley, India. Journal of Asian Earth
Sciences 21:643-654.</mixed-citation>
         </ref>
         <ref id="d2511e1252a1310">
            <mixed-citation id="d2511e1256" publication-type="other">
Ray, S., and A. Chinsamy. 2003. Functional aspects of the postcranial
anatomy of the Permian dicynodont Diictodon and their ecological
implications. Palaeontology 46:151-183.</mixed-citation>
         </ref>
         <ref id="d2511e1269a1310">
            <mixed-citation id="d2511e1273" publication-type="other">
Romer, A. S., and L. I. Price. 1940. Review of the Pelycosauria. Geologi-
cal Society of America Special Paper 28:1-538.</mixed-citation>
         </ref>
         <ref id="d2511e1283a1310">
            <mixed-citation id="d2511e1287" publication-type="other">
Rubidge, B. S. (ed.). 1995. Biostratigraphy of the Beaufort Group
(Karoo Supergroup). South African Committee for Stratigraphy
Biostratigraphic Series 1:1-46.</mixed-citation>
         </ref>
         <ref id="d2511e1300a1310">
            <mixed-citation id="d2511e1304" publication-type="other">
Rubidge, B. S. 2005. Reuniting lost continents - fossil reptiles from the
ancient Karoo and their wanderlust. South African Journal of Geol-
ogy 108:135-172.</mixed-citation>
         </ref>
         <ref id="d2511e1318a1310">
            <mixed-citation id="d2511e1322" publication-type="other">
Rubidge, B. S., G. M. King, and P. J. Hancox. 1994. The postcranial
skeleton of the earliest dicynodont synapsid Eodicynodon from the
Upper Permian of South Africa. Palaeontology 37:397-408.</mixed-citation>
         </ref>
         <ref id="d2511e1335a1310">
            <mixed-citation id="d2511e1339" publication-type="other">
Sidor, C. A., K. D. Angielczyk, D. M. Weide, R. M. H. Smith, S. J. Nesbitt,
and L. A. Tsuji. In press. Tetrapod fauna of the lowermost Usili
Formation (Songea Group, Ruhuhu Basin) of southern Tanzania, with
a new burnetiid record. Journal of Vertebrate Paleontology.</mixed-citation>
         </ref>
         <ref id="d2511e1355a1310">
            <mixed-citation id="d2511e1359" publication-type="other">
Stockley, G. M. 1932. The geology of the Ruhuhu Coalfields, Tanganyika
Territory. Quarterly Journal of the Geological Society of London
88:610-622.</mixed-citation>
         </ref>
         <ref id="d2511e1372a1310">
            <mixed-citation id="d2511e1376" publication-type="other">
Sullivan, C., and R. R. Reisz. 2005. Cranial anatomy and taxonomy of the
Late Permian dicynodont Diictodon. Annals of Carnegie Museum
74:45-75.</mixed-citation>
         </ref>
         <ref id="d2511e1389a1310">
            <mixed-citation id="d2511e1393" publication-type="other">
Sullivan, C., R. R. Reisz, and R. M. H. Smith. 2003. The Permian mam-
mal-like herbivore Diictodon, the oldest known example of sexually
dimorphic armament. Proceedings of the Royal Society of London
Series B 270:173-178.</mixed-citation>
         </ref>
         <ref id="d2511e1409a1310">
            <mixed-citation id="d2511e1413" publication-type="other">
Sun, A.-L. 1963. The Chinese kannemeyeriids. Palaeontologia Sinica
New Series C 17:1-109.</mixed-citation>
         </ref>
         <ref id="d2511e1424a1310">
            <mixed-citation id="d2511e1428" publication-type="other">
Surkov, M. V. 1998a. The postcranial skeleton of Rhinodicynodon gracile
Kalandadze, 1970 (Dicynodontia). Paleontological Journal 32:402-409.</mixed-citation>
         </ref>
         <ref id="d2511e1438a1310">
            <mixed-citation id="d2511e1442" publication-type="other">
Surkov, M. V. 1998b. Morphological features of the postcranial skeleton
in anomodonts reflecting the evolutionary development of the
group. Paleontological Journal 32:620-623.</mixed-citation>
         </ref>
         <ref id="d2511e1455a1310">
            <mixed-citation id="d2511e1459" publication-type="other">
Surkov, M. V. 2004. Certain features of the postcranial skeleton of
Vivaxosaurus permirus Kalandadze et Kurkin (Anomodontia, Dicy-
nodontidae), with a note on their presumable trophic adaptation.
Paleontological Journal 38:67-72.</mixed-citation>
         </ref>
         <ref id="d2511e1475a1310">
            <mixed-citation id="d2511e1479" publication-type="other">
Surkov, M. V., N. N. Kalandadze, and M. J. Benton. 2005. Lystrosaurus
georgi, a dicynodont from the Lower Triassic of Russia. Journal of
Vertebrate Paleontology 25:402-413.</mixed-citation>
         </ref>
         <ref id="d2511e1492a1310">
            <mixed-citation id="d2511e1496" publication-type="other">
Vega-Dias, C., and C. L. Schultz. 2004. Postcranial material of Jachaleria
candelariensis Araújo and Gonzaga 1980 (Therapsida, Dicynodontia),
Upper Triassic of Rio Grande do Sul, Brazil. PaleoBios 24:7-31.</mixed-citation>
         </ref>
         <ref id="d2511e1509a1310">
            <mixed-citation id="d2511e1513" publication-type="other">
Vega-Dias, C., M. W. Maisch, and C. L. Schultz. 2004. A new phylogenetic
analysis of Triassic dicynodonts (Therapsida) and the systematic posi-
tion of Jachaleria candelariensis from the Upper Triassic of Brazil.
Neues Jahrbuch für Geologie und Paläontologie Abhandlungen
231:145-166.</mixed-citation>
         </ref>
         <ref id="d2511e1533a1310">
            <mixed-citation id="d2511e1537" publication-type="other">
Watson, D. M. S. 1912. The skeleton of Lystrosaurus. Records of the
Albany Museum 2:287-295.</mixed-citation>
         </ref>
         <ref id="d2511e1547a1310">
            <mixed-citation id="d2511e1551" publication-type="other">
Watson, D. M. S. 1960. The anomodont skeleton. Transactions of the
Zoological Society of London 29:131-208.</mixed-citation>
         </ref>
         <ref id="d2511e1561a1310">
            <mixed-citation id="d2511e1565" publication-type="other">
Weide, D. M., C. A. Sidor, K. D. Angielczyk, and R. M. H. Smith.
In press. A new record of Procynosuchus delaharpeae (Therapsida:
Cynodontia) from the Upper Permian Usili Formation, Tanzania.
Palaeontologia Africana.</mixed-citation>
         </ref>
         <ref id="d2511e1581a1310">
            <mixed-citation id="d2511e1585" publication-type="other">
Wild, R., C. S. Kaaya, T. Kreuser, S. Markwort, and P. Z. Semkiwa. 1993.
Discovery of a skull of Dicynodon lacerticeps in the uppermost
Permian (Tartarian) of Tanzania. Sonderveröffentlichungen, Geolo-
gisches Institut der Universität zu Köln 70:231-242.</mixed-citation>
         </ref>
         <ref id="d2511e1601a1310">
            <mixed-citation id="d2511e1605" publication-type="other">
Wopfner, H. 2002. Tectonic and climatic events controlling deposition in
Tanzanian Karoo basins. Journal of African Earth Sciences 34:167-177.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
