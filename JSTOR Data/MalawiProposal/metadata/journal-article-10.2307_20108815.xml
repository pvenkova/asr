<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">economicj</journal-id>
         <journal-id journal-id-type="jstor">j100143</journal-id>
         <journal-title-group>
            <journal-title>The Economic Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn pub-type="ppub">00130133</issn>
         <issn pub-type="epub">14680297</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20108815</article-id>
         <title-group>
            <article-title>Brain Drain and Human Capital Formation in Developing Countries: Winners and Losers</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Michel</given-names>
                  <surname>Beine</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Fréderic</given-names>
                  <surname>Docquier</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Hillel</given-names>
                  <surname>Rapoport</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">118</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">528</issue>
         <issue-id>i20108810</issue-id>
         <fpage>631</fpage>
         <lpage>652</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Royal Economic Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20108815"/>
         <abstract>
            <p>Using new data on emigration rates by education level, we examine the impact of brain drain migration on human capital formation in developing countries. We find evidence of a positive effect of skilled migration prospects on gross human capital formation in a cross-section of 127 countries. For each country of the sample we then estimate the net effect of the brain drain using counter-factual simulations. Countries combining relatively low levels of human capital and low emigration rates are shown to experience a 'beneficial brain drain', and conversely, there are more losers than winners, and the former tend to lose relatively more than what the latter gain.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d10e140a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d10e147" publication-type="other">
Beine et al., 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d10e154a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d10e161" publication-type="other">
Miyagiwa (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e167" publication-type="other">
Haque and Kim (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e173" publication-type="other">
Wong and Yip (1999).</mixed-citation>
            </p>
         </fn>
         <fn id="d10e180a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d10e187" publication-type="other">
McKenzie and Rapoport (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e193" publication-type="other">
Mexico and De Brauw and Giles (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e200a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d10e207" publication-type="other">
Stark et al. (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e213" publication-type="other">
Katz and Rapoport (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e219" publication-type="other">
McCormick and
Wahba (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e229" publication-type="other">
Commander et al. (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e235" publication-type="other">
Docquier and Rapoport (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e243a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d10e250" publication-type="other">
Beine et al. (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e257a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d10e264" publication-type="other">
Docquier and Marfouk (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e271a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d10e278" publication-type="other">
Rapoport and Docquier
(2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d10e288a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d10e295" publication-type="other">
Carrington and Detragiache (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e301" publication-type="other">
Beine et al. (2003).</mixed-citation>
            </p>
         </fn>
         <fn id="d10e308a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d10e315" publication-type="other">
http://esa.un.org/unpp.</mixed-citation>
            </p>
         </fn>
         <fn id="d10e322a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d10e329" publication-type="other">
http://www.cia.gov/cia/publications/factbook.</mixed-citation>
            </p>
         </fn>
         <fn id="d10e337a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d10e344" publication-type="other">
Klenow and Rodriguez-
Clare, 2005</mixed-citation>
            </p>
         </fn>
         <fn id="d10e354a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d10e361" publication-type="other">
Carrington et al. (1996)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e367" publication-type="other">
Munshi (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e373" publication-type="other">
Kanbur and Rapoport (2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d10e380a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d10e387" publication-type="other">
Tremblay (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d10e393" publication-type="other">
Docquier and Rapoport (2003).</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d10e409a1310">
            <mixed-citation id="d10e413" publication-type="other">
Barro, R.J. and Lee, J.W. (2001). 'International data on educational attainment: updates and implications',
Oxford Economic Papers, vol. 53(3), pp. 541-63.</mixed-citation>
         </ref>
         <ref id="d10e423a1310">
            <mixed-citation id="d10e427" publication-type="other">
Barro, R. and Sala-I-Martin, X. (1995). Economic Growth, New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d10e434a1310">
            <mixed-citation id="d10e438" publication-type="other">
Beine, M., Docquier, F. and Rapoport, H. (2001). 'Brain drain and economic growth: theory and evidence',
Journal of Development Economics, vol. 64 (1), pp. 275-89.</mixed-citation>
         </ref>
         <ref id="d10e448a1310">
            <mixed-citation id="d10e452" publication-type="other">
Beine, M., Docquier, F. and Rapoport, H. (2003). 'Brain drain and LDCs' growth: winners and losers', IZA
Discussion Paper No 819, July.</mixed-citation>
         </ref>
         <ref id="d10e463a1310">
            <mixed-citation id="d10e467" publication-type="other">
Bhagwati, J.N. and Hamada, K. (1974). 'The brain drain, international integration of markets for profes-
sionals and unemployment', Journal of Development Economics, vol. 1 (1), pp. 19-42.</mixed-citation>
         </ref>
         <ref id="d10e477a1310">
            <mixed-citation id="d10e481" publication-type="other">
Carrington, W.J. and Detragiache, E. (1998). 'How big is the brain drain?', International Monetary Fund
Working Paper, No 98.</mixed-citation>
         </ref>
         <ref id="d10e491a1310">
            <mixed-citation id="d10e495" publication-type="other">
Carrington, W.J., Detragiache, E. and Vishwanath, T. (1996). 'Migration with endogenous moving costs',
American Economic Review, vol. 86 (4), pp. 909-30.</mixed-citation>
         </ref>
         <ref id="d10e505a1310">
            <mixed-citation id="d10e509" publication-type="other">
Commander, S., Kangasniemi, M. and Winters, L.A. (2004). 'The brain drain: curse or boon? A survey of the
literature', in (R. Baldwin and L.A. Winters, eds.), Challenges to Globalization, pp. 235-72, Chicago:
University of Chicago Press,</mixed-citation>
         </ref>
         <ref id="d10e522a1310">
            <mixed-citation id="d10e526" publication-type="other">
de Brauw, A. and Giles, J. (2006). 'Migrant opportunity and the educational attainment of youth in rural
China', IZA Discussion Paper No 2326, September.</mixed-citation>
         </ref>
         <ref id="d10e536a1310">
            <mixed-citation id="d10e540" publication-type="other">
Docquier, F. and Marfouk, A. (2006). 'International migration by education attainment, 1990-2000', in
(C. Ozden and M. Schiff, eds.), International Migration, Brain Drain and Remittances, pp. 151-99, New York:
Palgrave Macmillan.</mixed-citation>
         </ref>
         <ref id="d10e554a1310">
            <mixed-citation id="d10e558" publication-type="other">
Docquier, F. and H. Rapoport (2003). 'Ethnic discrimination and the migration of skilled labor', Journal of
Development Economics, vol. 70 (1), pp. 159-72.</mixed-citation>
         </ref>
         <ref id="d10e568a1310">
            <mixed-citation id="d10e572" publication-type="other">
Docquier, F. and Rapoport, H. (2008). 'Skilled migration: the perspective of developing countries', in
(Baghwati, J. and Hanson, G., eds.) Skilled Migration: Problems and Prospects, New York: Russell Sage
Foundation.</mixed-citation>
         </ref>
         <ref id="d10e585a1310">
            <mixed-citation id="d10e589" publication-type="other">
Easterly, W. and Levine, R. (1997). 'Africa's growth tragedy: policies and ethnic divisions', Quarterly Journal of
Economics, vol. 112 (4), pp. 1203-50.</mixed-citation>
         </ref>
         <ref id="d10e599a1310">
            <mixed-citation id="d10e603" publication-type="other">
Grubel, H. G. and Scott, A. (1966). 'The international flow of human capital', American Economic Review,
vol. 56, pp. 268-74.</mixed-citation>
         </ref>
         <ref id="d10e613a1310">
            <mixed-citation id="d10e617" publication-type="other">
Hall, R.E. and Jones, C.I. (1999). 'Why do some countries produce so much more output per worker than
others?', Quarterly Journal of Economics, vol. 114 (1), pp. 83-116.</mixed-citation>
         </ref>
         <ref id="d10e627a1310">
            <mixed-citation id="d10e631" publication-type="other">
Haque, N.U. and Kim, S.-J. (1995). 'Human capital flight: impact of migration on income and growth', IMF
Staff Papers, vol. 42 (3), pp. 577-607.</mixed-citation>
         </ref>
         <ref id="d10e642a1310">
            <mixed-citation id="d10e646" publication-type="other">
ILO (2006). Competing for Global Talent, Geneva: ILO.</mixed-citation>
         </ref>
         <ref id="d10e653a1310">
            <mixed-citation id="d10e657" publication-type="other">
Kanbur, R. and Rapoport, H. (2005). 'Migration selectivity and the evolution of spatial inequality', Journal of
Economic Geography, vol. 5 (1), pp. 43-57.</mixed-citation>
         </ref>
         <ref id="d10e667a1310">
            <mixed-citation id="d10e671" publication-type="other">
Katz, E. and Rapoport, H. (2005). 'On human capital formation with exit options', Journal of Population
Economics, vol. 18 (2), pp. 267-74.</mixed-citation>
         </ref>
         <ref id="d10e681a1310">
            <mixed-citation id="d10e685" publication-type="other">
Klenow, P.J. and Rodriguez-Clare, A. (2005). 'Externalities and growth', in (P. Aghion and S. Durlauf, eds.),
Handbook of Economic Growth, Vol. 1, pp. 817-61, Amsterdam: Elsevier-North Holland.</mixed-citation>
         </ref>
         <ref id="d10e695a1310">
            <mixed-citation id="d10e699" publication-type="other">
McCormick, B. and Wahba, J. (2000). 'Overseas unemployment and remittances to a dual economy',
Economic Journal, vol. 110(463), pp. 509-34.</mixed-citation>
         </ref>
         <ref id="d10e709a1310">
            <mixed-citation id="d10e713" publication-type="other">
McCulloch, R. and Yellen, J.T. (1977). 'Factor mobility, regional development and the distribution of income',
Journal of Political Economy, vol. 85 (1), pp. 79-96.</mixed-citation>
         </ref>
         <ref id="d10e724a1310">
            <mixed-citation id="d10e728" publication-type="other">
McKenzie, D.J. and Rapoport, H. (2006). 'Can migration reduce educational attainment? Evidence from
Mexico', World Bank Policy Research Working Paper No 3952, June.</mixed-citation>
         </ref>
         <ref id="d10e738a1310">
            <mixed-citation id="d10e742" publication-type="other">
Miyagiwa, K. (1991). 'Scale economies in education and the brain drain problem', International Economic
Review, vol. 32 (3), pp. 743-59.</mixed-citation>
         </ref>
         <ref id="d10e752a1310">
            <mixed-citation id="d10e756" publication-type="other">
Mountford, A. (1997). 'Can a brain drain be good for growth in the source economy?', Journal of Development
Economics, vol. 53 (2), pp. 287-303.</mixed-citation>
         </ref>
         <ref id="d10e766a1310">
            <mixed-citation id="d10e770" publication-type="other">
Munshi, K. (2003). 'Networks in the modern economy: Mexican migrants in the US labour market', Quarterly
Journal of Economics, vol. 118 (2), pp. 549-99.</mixed-citation>
         </ref>
         <ref id="d10e780a1310">
            <mixed-citation id="d10e784" publication-type="other">
Rapoport, H. and Docquier, F. (2006). 'The economics of migrants' remittances', in (S.-C. Kolm and
J. Mercier Ythier, eds.), Handbook of the Economics of Giving, Altruism and Reciprocity, Vol 2, pp. 1135-98,
Amsterdam: Elsevier-North Holland.</mixed-citation>
         </ref>
         <ref id="d10e797a1310">
            <mixed-citation id="d10e801" publication-type="other">
Stark, O., Helmenstein, C. and Prskawetz, A. (1997). 'A brain gain with a brain drain', Economics Letters, vol. 55,
pp. 227-34.</mixed-citation>
         </ref>
         <ref id="d10e812a1310">
            <mixed-citation id="d10e816" publication-type="other">
Tremblay, K. (2001). 'Ethnic conflicts, migration and development', unpublished PhD dissertation, University
of Paris I-Sorbonne.</mixed-citation>
         </ref>
         <ref id="d10e826a1310">
            <mixed-citation id="d10e830" publication-type="other">
Vidal, J.-P. (1998). 'The effect of emigration on human capital formation', Journal of Population Economics,
vol. 11 (4), pp. 589-600.</mixed-citation>
         </ref>
         <ref id="d10e840a1310">
            <mixed-citation id="d10e844" publication-type="other">
Wong, K.-Y. and Yip, C.K. (1999). 'Education, economic growth, and brain drain', Journal of Economic Dynamics
and Control, vol. 23 (5-6), pp. 699-726.</mixed-citation>
         </ref>
         <ref id="d10e854a1310">
            <mixed-citation id="d10e858" publication-type="other">
World Bank (2005). World Development Indicators, Washington: The World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
