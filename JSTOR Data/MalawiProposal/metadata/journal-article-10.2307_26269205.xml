<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">ecologysociety</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50015787</journal-id>
         <journal-title-group>
            <journal-title>Ecology and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Resilience Alliance</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17083087</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26269205</article-id>
         <article-categories>
            <subj-group>
               <subject>Research</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Understanding Household Connectivity and Resilience in Marginal Rural Communities through Social Network Analysis in the Village of Habu, Botswana</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Cassidy</surname>
                  <given-names>Lin</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Barnes</surname>
                  <given-names>Grenville D.</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Okavango Research Institute, University of Botswana,</aff>
            <aff id="af2">
               <label>²</label>School of Forest Resources and Conservation, University of Florida</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>12</month>
            <year>2012</year>
            <string-date>Dec 2012</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">17</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26269191</issue-id>
         <permissions>
            <copyright-statement>Copyright © 2012 by the author(s)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26269205"/>
         <abstract>
            <label>ABSTRACT.</label>
            <p>Adaptability is emerging as a key issue not only in the climate change debate but in the general area of sustainable development. In this context, we examine the link between household resilience and connectivity in a rural community in Botswana. We see resilience and vulnerability as the positive and negative dimensions of adaptability. Poor, marginal rural communities confronted with the vagaries of climate change, will need to become more resilient if they are to survive and thrive. We define resilience as the capacity of a social–ecological system to cope with shocks such as droughts or economic crises without changing its fundamental identity. We make use of three different indices of household resilience: livelihood diversity, wealth, and a comprehensive resilience index based on a combination of human, financial, physical, social, and natural capital. Then, we measure the social connectivity of households through a whole network approach in social network analysis, using two measures of network centrality (degree centrality and betweenness). We hypothesize that households with greater social connectivity have greater resilience, and analyze a community in rural Botswana to uncover how different households make use of social networks to deal with shocks such as human illness and death, crop damage, and livestock disease. We surveyed the entire community of Habu using a structured questionnaire that focused on livelihood strategies and social networks. We found that gender, age of household head, and household size were positively correlated with social connectivity. Our analysis indicates that those households that are more socially networked are likely to have a wider range of livelihood strategies, greater levels of other forms of social capital, and greater overall capital. Therefore, they are more resilient.</p>
         </abstract>
         <kwd-group>
            <label>Key Words:</label>
            <kwd>adaptive capacity</kwd>
            <kwd>capital</kwd>
            <kwd>household connectivity</kwd>
            <kwd>household resilience</kwd>
            <kwd>social networks</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>LITERATURE CITED</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Adger, W. 2003. Social capital, collective action, and adaptation to climate change. Economic Geography 79 (4):387–404.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Adger, W. N. 2000. Social and ecological resilience: are they related? Progress in Human Geography 24(3):347–364.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Adger, W. N., N. Brooks, G. Bentham, M. Agnew, and S. Eriksen. 2004. New indicators of vulnerability and adaptive capacity. Tyndall Centre for Climate Change Research, University of East Anglia, Norwich, Norfolk, UK.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Alcorn, J., and V. Toledo. 1998. Resilient resource management in Mexico’s forest ecosystems: the contribution of property rights. Pages 216–249 in F. Berkes and C. Folke, editors. Linking social and ecological systems. Cambridge University Press, Cambridge, UK.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Anderies, J. M., B. Walker, and A. Kinzig. 2006. Fifteen weddings and a funeral: case studies and resilience-based management. Ecology and Society 11 (1):21. [online] URL: http://www.ecologyandsociety.org/vol11/iss1/art21/</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Bauer, S., and I. Scholz. 2010. Adaptation to climate change in southern Africa: new boundaries for sustainable development? Climate and Development 2(2):83–93. http://dx.doi.org/10.3763/cdev.2010.0040</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Bendsen, H., and H. Gelmroth. 1983. Land use planning: Ngamiland Communal First Development Area: final report. Ministry of Local Government, Lands and Housing, Gabarone, Botswana.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Berkes, F., and C. Folke, editors. 1998. Linking social and ecological systems: management practices and social mechanisms for building resilience. Cambridge University Press, Cambridge, UK.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Blaikie, P. 2006. Is small really beautiful? Community-based natural resource management in Malawi and Botswana. World Development 34(11):1942–1957. http://dx.doi.org/10.1016/j.worlddev.2005.11.023</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Blaikie, P., T. Cannon, I. Davis, and B. Wisner. 1994. At risk–natural hazards, peoples vulnerability and disasters. Routledge, London, UK.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Bodin, Ö., B. Crona, and H. Ernstson. 2006. Social networks in natural resource management: what is there to learn from a structural perspective? Ecology and Society 11 (2):r2. [online] URL: http://www.ecologyandsociety.org/vol11/iss2/resp2/</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Borgatti, S., M. Everett, and L. Freeman. 2002. UCINET for Windows: software for social network analysis. Analytic Technologies, Harvard, Massachusetts, USA.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Brass, D. J. 1984. Being in the right place: a structural analysis of individual influence in an organization. Administrative Science Quarterly 29:518–539. http://dx.doi.org/10.2307/2392937</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Brooks, N., and W. N. Adger. 2005. Assessing and enhancing adaptive capacity. Pages 165–181 in B. Lim and E. Spanger-Siegfried, editors. Adaptation policy frameworks for climate change: developing strategies, policies and measures. UNDPGEF, Cambridge University Press, Cambridge, UK.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Busby, J. W., T. D. Smith, K. L. White, and S. M. Strange. 2010. Locating climate insecurity: where are the most vulnerable places in Africa? Robert S. Strauss Center for International Security and Law, University of Texas, Austin, Texas, USA.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Carney, D., M. Drinkwater, T. Rusinow, K. Neefjes, S. Wanmali, and N. Singh. 1999. Livelihoods approaches compared. Department for International Development (DFID), London, UK.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Carpenter, S. R., B. Walker, J. M. Anderies, and N. Abel. 2001. From metaphor to measurement: resilience of what to what? Ecosystems 4:765–781.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Cassidy, L. 2000. CBNRM and legal rights to resources in Botswana. SNV/IUCN CBNRM Support Programme, Gabarone, Botswana.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Cassidy, L. 2003. Anthropogenic burning in the Okavango panhandle of Botswana: livelihoods and spatial dimensions. Thesis. University of Florida, Gainesville, Florida, USA.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Central Statistics Office. 2002. Population of towns, villages and associated localities in August 2001. Government of Botswana, Gaborone, Botswana.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Chambers, R., and G Conway. 1992. Sustainable rural livelihoods: practical concepts for the 21st century. Institute of Development Studies, University of Sussex, Brighton, UK.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Crona, B., and Ö. Bodin. 2006. What you know is who you know: communication patterns among resource users as a prerequisite for co-management Ecology and Society 11 (2):7. [online] URL: http://www.ecologyandsociety.org/vol11/iss2/art7/</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Dershem, L., and D. Gzirishvili. 1998. Informal social support networks and household vulnerability: empirical findings from Georgia. World Development 26(10):1827–1838. http://dx.doi.org/10.1016/S0305-750X(98)00085-0</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Ellis, F. 2000. Rural livelihoods and diversity in developing countries. Oxford University Press, Oxford, UK.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Food, Agriculture and Natural Resources Policy Analysis Network (FANRPAN). 2006. Developing a statistical index —the Household Vulnerability Index (HVI)—for quantifying vulnerability as a means of improving targeting of impact responses. [online] URL: http://www.fanrpan.org/documents/d00134/</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Folke, C., J. Colding, and F. Berkes. 2003. Synthesis: building resilience and adaptive capacity in social–ecological systems. Pages 352–387 in F. Berkes, J. Colding, and C. Folke, editors. Navigating social–ecological systems—building resilience for complexity and change. Cambridge University Press, Cambridge, UK. http://dx.doi.org/10.1017/CBO9780511541957.020</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Goh, K. I., E. Oh, B. Kahng, and D. Kim. 2003. Betweenness centrality correlation in social networks. Physical Review E 67(1):017101. http://dx.doi.org/10.1103/PhysRevE.67.017101</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Gunderson, L. H. 2000 Ecological resilience—in theory and application. Annual Review of Ecology, Evolution and Systematics 31:425–439.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Gunderson, L H., and C. S. Holling, editors. 2002. Panarchy: understanding transformations in human and natural systems. Island Press, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Hahn, M. B., A. M. Riederer, and S. O. Foster. 2009. The Livelihood Vulnerability Index: a pragmatic approach to assessing risks from climate variability and change. A case study in Mozambique. Global Environmental Change 19 (1):74–88. http://dx.doi.org/10.1016/j.gloenvcha.2008.11.002</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Hanneman, R., and M. Riddle. 2005. Introduction to social network methods. University of California, Riverside, California, USA.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Heijmans, A. 2004. From vulnerability to empowerment. Pages 115–127 in G. Bankoff, G. Frerks, and D. Hilhorst, editors. Mapping vulnerability: disasters development and people. Earthscan, London, UK.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Holland, J. H. 1995. Hidden order: how adaptation builds complexity. Helix, Reading, Massachusetts, USA.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Holling, C. S. 2001. Understanding the complexity of economic, ecological, and social systems Ecosystems 4:390–405.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Janssen, M., Ö. J. Bodin, T. Anderies, H. Elmqvist, H. Ernstson, R. McAllister, P. Olsson, and P. Ryan. 2006. Toward a network perspective of the study of resilience in social–ecological systems. Ecology and Society 11(1): 15. [online] URL: http://www.ecologyandsociety.org/vol11/iss1/art15/</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Kgathi, D. L., and M. R. Motsholapheko. 2011. Livelihood activities and income portfolios in rural areas of the Okavango Delta, Botswana. Pages 35–54 in D. L. Kgathi, B. N. Ngwenya, and M. B. K. Darkoh, editors. Rural livelihoods, risk and political economy of access to natural resources in the Okavango Delta, Botswana. Nova, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Kgathi, D. L., B. N. Ngwenya, and J. Wilk. 2007. Shocks and rural livelihoods in the Okavango Delta, Botswana. Development Southern Africa 24 (2):289–308. http://dx.doi.org/10.1080/03768350701327186</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Levin, S. 1999. Fragile dominion: complexity and the commons. Helix, Reading, Massachusetts, USA.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Lhotka, L., C. Bailey, and M. Dubois. 2008. Ideologically structured information exchange among environmental groups. Rural Sociology 73 (2):230–249.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Manyena, S. B. 2006. The concept of resilience revisited. Disasters 40(3):433–450. http://dx.doi.org/10.1111/j.0361-3666.2006.00331.x</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Mbaiwa, J. E., A. Stronza, and U. Kreuter. 2011. From collaboration to conservation: insights from the Okavango Delta, Botswana. Society and Natural Resources 24(4):400–411. http://dx.doi.org/10.1080/08941921003716745</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">McCarty, C., P. Killworth, and J. Rennell. 2007. Impact of methods for reducing respondent burden on personal network structural measures. Social Networks 29 (2):300–315. http://dx.doi.org/10.1016/j.socnet.2006.12.005</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Morton, J. F. 2007. The impact of climate change on smallholder and subsistence agriculture. Proceedings of the National Academy of Sciences 104(50):19680. http://dx.doi.org/10.1073/pnas.0701855104</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Nelson, D., W. Adger, and K. Brown. 2007. Adaptation to environmental change: contributions of a resilience framework. Annual Review of Environment and Resources 32:395–419. http://dx.doi.org/10.1146/annurev.energy.32.051807.090348</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Olsson, P., C. Folke, and F. Berkes. 2004. Adaptive comanagement for building resilience in social–ecological systems. Environmental Management 34(1):75–90. http://dx.doi.org/10.1007/s00267-003-0101-7</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Pacheco, P. 2009. Smallholder livelihoods, wealth and deforestation in the eastern Amazon. Human Ecology 37 (1):27–41. http://dx.doi.org/10.1007/s10745-009-9220-y</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Perz, S. 2005. The importance of household asset diversity for livelihood diversity and welfare among small farm colonists in the Amazon. The Journal of Development Studies 41 (7):1193–1220. http://dx.doi.org/10.1080/00220380500170899</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Putnam, R. D. 2001. Bowling alone: the collapse and revival of American community. Simon and Schuster, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Regional Climate Change Programme. 2009. Problem areas and hotspots in Botswana. UK Aid, Department for International Development (DFID), London, UK. [online] URL: http://www.rccp.org.za/index.php?option=com_content&amp;view= category&amp;layout=blog&amp;id=61&amp;goto=top&amp;Itemid=68&amp;lang=en#top</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Sagar, A. D., and A. Najam. 1998. The human development index: a critical review. Ecological Economics 25(3):249–264. http://dx.doi.org/10.1016/S0921-8009(97)00168-7</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Sallu, S., C. Twyman, and D. Thomas. 2009. The multidimensional nature of biodiversity and social dynamics and implications for contemporary rural livelihoods in remote Kalahari settlements, Botswana. African Journal of Ecology 47:110–118. http://dx.doi.org/10.1111/j.1365-2028.2008.01057.x</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Sallu, S. M., C. Twyman, and L. C. Stringer. 2010. Resilient or vulnerable livelihoods? Assessing livelihood dynamics and trajectories in rural Botswana. Ecology and Society 15(4): 3[online] URL: http://www.ecologyandsociety.org/vol15/iss4/art3/</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Scoones, I. 1998. Sustainable rural livelihoods: a framework for analysis. IDS Brighton, Brighton, UK.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Shackleton, C. M., and S. E. Shackleton. 2006. Household wealth status and natural resource use in the Kat River Valley, South Africa. Ecological Economics 57 (2):306–317. http://dx.doi.org/10.1016/j.ecolecon.2005.04.011</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Shackleton, C. M., S. E. Shackleton, and B. Cousins. 2001. The role of land-based strategies in rural livelihoods: the contribution of arable production, animal husbandry and natural resource harvesting in communal areas in South Africa. Development Southern Africa 18(5):581–604. http://dx.doi.org/10.1080/03768350120097441</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Slocum, R., L. Wichhart, D. Rocheleau, and B. Thomas-Slayter, editors. 1998. Power, process and participation—tools for change. Intermediate Technology, London, UK.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Thomalla, F., T. Downing, E. Spanger-Siegfried, G. Han, and J. Rockström. 2006. Reducing hazard vulnerability: towards a common approach between disaster risk reduction and climate adaptation. Disasters 30(1):39–48. http://dx.doi.org/10.1111/j.1467-9523.2006.00305.x</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Tlou, T. 1976. The peopling of the Okavango Delta c. 1750–1906. Pages 49–53 in Proceedings of the Symposium on the Okavango Delta and its Future Utilisation. Botswana Society, National Museum, Gaborone, Botswana.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Twigg, J. 2007. Characteristics of a disaster-resilient community. A guidance note. DFID Disaster Risk Reduction Interagency Coordination Group, London, UK.</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Twyman, C. 2000. Livelihood opportunity and diversity in Kalahari wildlife management areas, Botswana: rethinking community resource management. Journal of Southern African Studies 26(4):783–806. http://dx.doi.org/10.1080/713683606</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Twyman, C. 2001. Natural resource use and livelihoods in Botswana’s Wildlife Management Areas. Applied Geography 21:45–68. http://dx.doi.org/10.1016/S0143-6228(00)00016-3</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Valente, T. W., and R. K. Foreman. 1998. Integration and radiality: measuring the extent of an individual's connectedness and reachability in a network. Social Networks 20(1):89–105. http://dx.doi.org/10.1016/S0378-8733(97)00007-5</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Vemuri, A. W., and R. Costanza. 2006. The role of human, social, built, and natural capital in explaining life satisfaction at the country level: toward a National Well-being index (NWI). Ecological Economics 58(1):119–133. http://dx.doi.org/10.1016/j.ecolecon.2005.02.008</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Vincent, K. 2007. Uncertainty in adaptive capacity and the importance of scale. Global Environmental Change 17(1):12–24. http://dx.doi.org/10.1016/j.gloenvcha.2006.11.009</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Walker, B. H., and D. A. Salt. 2006. Resilience thinking: sustaining ecosystems and people in a changing world. Island Press, Washington, D.C., USA.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Webb, C., and O. Bodin. 2008. A network perspective on modularity and control of flow in robust systems. Pages 85–118 in J. Norberg and G. Cumming, editors. Complexity theory for a sustainable future. Columbia Press, Chichester, New York, USA.</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">Wilk, J., and D. L. Kgathi. 2007. Risk in the Okavango Delta in the face of social and environmental change. Geojournal 70:121–132. http://dx.doi.org/10.1007/s10708-008-9119-y</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
