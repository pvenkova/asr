<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jwildmana</journal-id>
         <journal-id journal-id-type="jstor">j100259</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Wildlife Management</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Wildlife Society</publisher-name>
         </publisher>
         <issn pub-type="ppub">0022541X</issn>
         <issn pub-type="epub">19372817</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3803610</article-id>
         <title-group>
            <article-title>Population Genetics of Resurgence: White-Tailed Deer in Kentucky</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Kinchel C.</given-names>
                  <surname>Doerner</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Wes</given-names>
                  <surname>Braden</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Jennifer</given-names>
                  <surname>Cork</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Tom</given-names>
                  <surname>Cunningham</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Amanda</given-names>
                  <surname>Rice</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Bonnie J.</given-names>
                  <surname>Furman</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Doug</given-names>
                  <surname>McElroy</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">69</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i293846</issue-id>
         <fpage>345</fpage>
         <lpage>355</lpage>
         <page-range>345-355</page-range>
         <permissions>
            <copyright-statement>Copyright 2005 The Wildlife Society</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3803610"/>
         <abstract>
            <p>White-tailed deer (Odocoileus virginianus) in Kentucky represent an example of successful wildlife restoration. Eliminated from all but a few remnant areas by the early part of the twentieth century, the species is once again widely distributed and abundant as a result of intensive restocking efforts since the 1940s. Seven DNA microsatellite markers were used to survey the extent and pattern of genetic variation in 322 deer from multiple localities throughout the commonwealth. Six genetically homogeneous regions and 1 heterogeneous region were identified across Kentucky. High levels of allelic diversity were detected with no apparent reduction in heterozygosity, disproportionate loss of rare alleles, or shift in modal allele frequency class as might be expected if the severe historical population size reduction generated a concomitant genetic bottleneck. These results are consistent with predictions of founder-flush models: that rapid population growth may minimize the loss of genetic variability associated with a population bottleneck. Nevertheless, comparisons of our data to that derived from other imperiled taxa suggest that species demographics can also play an important role in determining the genetic consequences of population size reduction and subsequent recovery. Our data also illuminate the critical role of deer from Land Between the Lakes (LBL) as the initial source population from which all extant Kentucky deer are descended. While generally supporting current regional management perspectives, our results argue for recognition of the LBL herd as a distinct management island of genetic and historical value.</p>
         </abstract>
         <kwd-group>
            <kwd>Genetic structure</kwd>
            <kwd>Kentucky</kwd>
            <kwd>Microsatellite DNA markers</kwd>
            <kwd>Population genetics</kwd>
            <kwd>White-tailed deer</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d407e235a1310">
            <mixed-citation id="d407e239" publication-type="journal">
ANDERSON, J. D., R. L. HONEYCUTT, R. A. GONZALES, K. L.
GEE, L. C. SNOW, R. L. GALLAGHER, D. A. HONEYCUTT, r
AND R. W. DEYONG. 2002. Development of microsatel-
lite DNA markers for the automated genetic charac-
terization of white-tailed deer populations.Journal of
Wildlife Management 66:67-74.<object-id pub-id-type="doi">10.2307/3802872</object-id>
               <fpage>67</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e268a1310">
            <mixed-citation id="d407e272" publication-type="book">
APPLIED BIOSTATISTICS, INC. 2000. NTSYS-pc, version 2.in.
Applied Biostatistics, Inc., Setauket, New York, USA.<person-group>
                  <string-name>
                     <surname>Applied Biostatistics</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">NTSYS-pc, version 2</comment>
               <source>Applied Biostatistics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d407e301a1310">
            <mixed-citation id="d407e305" publication-type="journal">
BARICK, F. B. 1951. Deer restoration in the southeastern
United States. Annual Conference of the Southeast-
ern Association of the Game and Fish Commissions
5:342-367.<person-group>
                  <string-name>
                     <surname>Barick</surname>
                  </string-name>
               </person-group>
               <fpage>342</fpage>
               <volume>5</volume>
               <source>Annual Conference of the Southeastern Association of the Game and Fish Commissions</source>
               <year>1951</year>
            </mixed-citation>
         </ref>
         <ref id="d407e343a1310">
            <mixed-citation id="d407e347" publication-type="journal">
BISHOP, M. D., S. M. KAPPES, J. W. KEELE, R. T. STONE,
S. L. F. SUNDEN, G. A. HAWKINS, S. S. TOLDO, R. FRIES,
M. D. GROSZ, J. Yoo, AND C. W. BEATRIE. 1994. A genetic
linkage map for cattle. Genetics 136:619-639.<person-group>
                  <string-name>
                     <surname>Bishop</surname>
                  </string-name>
               </person-group>
               <fpage>619</fpage>
               <volume>136</volume>
               <source>Genetics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d407e386a1310">
            <mixed-citation id="d407e390" publication-type="journal">
BOUZAT, J., H. H. CHENG, H. A. LEWIN, R. L. WESTEMEIER,
J. D. BRAWN, AND K. N. PAIGE. 1998. Genetic evaluation
of a demographic bottleneck in the greater prairie
chicken. Conservation Biology 12:836-843.<object-id pub-id-type="jstor">10.2307/2387543</object-id>
               <fpage>836</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e413a1310">
            <mixed-citation id="d407e417" publication-type="journal">
CARSON H. L., AND A. R. TEMPLETON. 1984. Genetic rev-
olutions in relation to speciation phenomena: the
founding of new populations. Annual Review of Ecol-
ogy and Systematics 15:97-113.<object-id pub-id-type="jstor">10.2307/2096944</object-id>
               <fpage>97</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e440a1310">
            <mixed-citation id="d407e444" publication-type="journal">
DEMPSTER, A., N. LAIRD, AND D. RUBIN. 1977. Maximum
likelihood estimation from incomplete data via the
EM algorithm. Journal of the Royal Statistical Society
39:1-38.<object-id pub-id-type="jstor">10.2307/2984875</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e467a1310">
            <mixed-citation id="d407e471" publication-type="journal">
DEWOODY, J. A., R. L. HONEYCUTT, AND L. C. SKOW. 1995.
Microsatellite markers in white-tailed deer. Journal of
Heredity 86:317-319.<person-group>
                  <string-name>
                     <surname>Dewoody</surname>
                  </string-name>
               </person-group>
               <fpage>317</fpage>
               <volume>86</volume>
               <source>Journal of Heredity</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d407e506a1310">
            <mixed-citation id="d407e510" publication-type="journal">
EXCOFFIER, L., AND M. SLATKIN. 1995. Maximum-likeli-
hood estimation of molecular haplotype frequencies
in a diploid population. Molecular Biology and Evo-
lution 12:921-927.<person-group>
                  <string-name>
                     <surname>Excoffier</surname>
                  </string-name>
               </person-group>
               <fpage>921</fpage>
               <volume>12</volume>
               <source>Molecular Biology and Evolution</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d407e548a1310">
            <mixed-citation id="d407e552" publication-type="journal">
---, P. SMOUSE, AND J. QUATRRO. 1992. Analysis of
molecular variance inferred from metric distances
among DNA haplotypes: application to human mito-
chondrial DNA restriction data. Genetics 131:479-491.<person-group>
                  <string-name>
                     <surname>Excoffier</surname>
                  </string-name>
               </person-group>
               <fpage>479</fpage>
               <volume>131</volume>
               <source>Genetics</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d407e591a1310">
            <mixed-citation id="d407e595" publication-type="journal">
GARZA, J. C., AND E. G. WILLIAMSON. 2001. Detection of
reduction in population size using data from micro-
satellite loci. Molecular Ecology 10:305-318.<person-group>
                  <string-name>
                     <surname>Garza</surname>
                  </string-name>
               </person-group>
               <fpage>305</fpage>
               <volume>10</volume>
               <source>Molecular Ecology</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d407e630a1310">
            <mixed-citation id="d407e634" publication-type="book">
GASSETr, J. W. 2001. Restoration of white-tailed deer in
Kentucky: from absence to overabundance. Pages
119-123 in D. S. Maehr, R. F. Noss, and J. L. Larkin,
editors. Large mammal restoration: ecological and
sociological challenges in the 21st century. Island
Press, Washington, D.C., USA.<person-group>
                  <string-name>
                     <surname>Gassetr</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Restoration of white-tailed deer in Kentucky: from absence to overabundance</comment>
               <fpage>119</fpage>
               <source>Large mammal restoration: ecological and sociological challenges in the 21st century</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d407e678a1310">
            <mixed-citation id="d407e682" publication-type="journal">
Guo, S., AND E. THOMPSON. 1992. Performing the exact
test of Hardy-Weinberg proportion for multiple alle-
les. Biometrics 48:361-372.<object-id pub-id-type="doi">10.2307/2532296</object-id>
               <fpage>361</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e701a1310">
            <mixed-citation id="d407e705" publication-type="journal">
HARDY, F. C. 1953. Kentucky's deer population growing.
Happy Hunting Ground 27:10.<person-group>
                  <string-name>
                     <surname>Hardy</surname>
                  </string-name>
               </person-group>
               <fpage>10</fpage>
               <volume>27</volume>
               <source>Happy Hunting Ground</source>
               <year>1953</year>
            </mixed-citation>
         </ref>
         <ref id="d407e737a1310">
            <mixed-citation id="d407e741" publication-type="book">
HARTL, D. L., AND A. G. CLARK. 1997. Principles of pop-
ulation genetics. Third edition. Sinauer Associates,
Sunderland, Massachusetts, USA..<person-group>
                  <string-name>
                     <surname>Hartl</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>Principles of population genetics</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d407e773a1310">
            <mixed-citation id="d407e777" publication-type="journal">
JONES, K. C., K. F. LEVINE, AND J. D. BANKS. 2000. DNA-
based genetic markers in black-tailed and mule deer
for forensic applications. California Fish and Game
86:115-126.<person-group>
                  <string-name>
                     <surname>Jones</surname>
                  </string-name>
               </person-group>
               <fpage>115</fpage>
               <volume>86</volume>
               <source>California Fish and Game</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d407e816a1310">
            <mixed-citation id="d407e820" publication-type="book">
KORNFIELD, I., AND A. PARKER. 1997. Molecular systemat-
ics of a rapidly evolving species flock: the mbuna of
Lake Malawi and the search for phylogenetic signal.
Pages 25-37 in T. D. Kocher and C. A. Stepien, edi-
tors. Molecular systematics of fishes. Academic Press,
New York, USA.<person-group>
                  <string-name>
                     <surname>Kornfield</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Molecular systematics of a rapidly evolving species flock: the mbuna of Lake Malawi and the search for phylogenetic signal</comment>
               <fpage>25</fpage>
               <source>Molecular systematics of fishes</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d407e864a1310">
            <mixed-citation id="d407e868" publication-type="journal">
LEBERG, P. L. 2002. Estimating alleic richness: effects of
sample size and bottlenecks. Molecular Ecology
11:2445-2449.<person-group>
                  <string-name>
                     <surname>Leberg</surname>
                  </string-name>
               </person-group>
               <fpage>2445</fpage>
               <volume>11</volume>
               <source>Molecular Ecology</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d407e903a1310">
            <mixed-citation id="d407e907" publication-type="journal">
LUIKART, G., F. W. ALLENDORF, J. -M. CORNUET, AND W. B.
SHERWIN. 1998. Distortion of allele frequency distrib-
utions provides a test for recent population bottle-
necks. Journal of Heredity 89:238-247.<person-group>
                  <string-name>
                     <surname>Luikart</surname>
                  </string-name>
               </person-group>
               <fpage>238</fpage>
               <volume>89</volume>
               <source>Journal of Heredity</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d407e945a1310">
            <mixed-citation id="d407e949" publication-type="journal">
MACINTOSH, D. 1962. Between the rivers area wealthy in
historic events. Happy Hunting Ground 18:30.<person-group>
                  <string-name>
                     <surname>Macintosh</surname>
                  </string-name>
               </person-group>
               <fpage>30</fpage>
               <volume>18</volume>
               <source>Happy Hunting Ground</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d407e981a1310">
            <mixed-citation id="d407e985" publication-type="journal">
MANTEL, N. 1967. The detection of disease clustering
and a generalized regression approach. Cancer Re-
search 27:209-220.<person-group>
                  <string-name>
                     <surname>Mantel</surname>
                  </string-name>
               </person-group>
               <fpage>209</fpage>
               <volume>27</volume>
               <source>Cancer Research</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1020a1310">
            <mixed-citation id="d407e1024" publication-type="journal">
NEI, M., T. MARUYAMA, AND R. CHAKRABORTY. 1975. The
bottleneck effect and genetic variability in popula-
tions. Evolution 29:1-10.<object-id pub-id-type="doi">10.2307/2407137</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e1044a1310">
            <mixed-citation id="d407e1048" publication-type="journal">
---, R. CHAKRABORTY, AND P. A. FUERST. 1976. Infinite
allele model with varying mutation rate. Proceedings of
the National Academy of Sciences USA 73:4164-4168.<object-id pub-id-type="jstor">10.2307/66475</object-id>
               <fpage>4164</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e1067a1310">
            <mixed-citation id="d407e1071" publication-type="journal">
NELSON, L. K. 1967. Speaking of wildlife-through the
eyes of a biologist. Happy Hunting Ground 23:25.<person-group>
                  <string-name>
                     <surname>Nelson</surname>
                  </string-name>
               </person-group>
               <fpage>25</fpage>
               <volume>23</volume>
               <source>Happy Hunting Ground</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1103a1310">
            <mixed-citation id="d407e1107" publication-type="journal">
PEMBERTON, J. M., J. SLATE, D. R. BANCROFT, AND J. A.
BARRET. 1995. Nonamplifying alleles at microsatellite
loci: a caution for parentage and population studies.
Molecular Ecology 4:249-252.<person-group>
                  <string-name>
                     <surname>Pemberton</surname>
                  </string-name>
               </person-group>
               <fpage>249</fpage>
               <volume>4</volume>
               <source>Molecular Ecology</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1145a1310">
            <mixed-citation id="d407e1149" publication-type="journal">
PRITCHARD, J. K., M. STEPHENS, AND P. DONNELLY. 2000.
Inference of population structure using multilocus
genotype data. Genetics 155:945-959.<person-group>
                  <string-name>
                     <surname>Pritchard</surname>
                  </string-name>
               </person-group>
               <fpage>945</fpage>
               <volume>155</volume>
               <source>Genetics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1184a1310">
            <mixed-citation id="d407e1188" publication-type="journal">
RATNASWAMY, M. J., M. H. SMITH, R. J. WARREN, C. L.
ROGERS, AND K. A. K. STROMAYER. 1999. Genetic effects
of a population bottleneck on a restored deer herd in a
national military park. Natural Areas Journal 19:41-46.<person-group>
                  <string-name>
                     <surname>Ratnaswamy</surname>
                  </string-name>
               </person-group>
               <fpage>41</fpage>
               <volume>19</volume>
               <source>Natural Areas Journal</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1226a1310">
            <mixed-citation id="d407e1230" publication-type="journal">
ROBICHAUX, R. H., E. A. FRIAR, AND D. W. MOUNT. 1997.
Molecular genetic consequences of a population bottle-
neck associated with reintroduction of the Mauna Kea
silversword (Argyroxiphium sandwicense ssp. sandwicense
[Asteraceae]). Conservation Biology 11:1140-1146.<object-id pub-id-type="jstor">10.2307/2387396</object-id>
               <fpage>1140</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e1257a1310">
            <mixed-citation id="d407e1261" publication-type="journal">
RUNDLE, H. D., A. O. MOOERS, AND M. C. WHITLOCK.
1998. Single founder-flush events and the evolution
of reproductive isolation. Evolution 52:1850-1855.<object-id pub-id-type="doi">10.2307/2411356</object-id>
               <fpage>1850</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e1280a1310">
            <mixed-citation id="d407e1284" publication-type="book">
SCHNEIDER, S., D. ROESSLI, AND L. EXCOFFIER. 2000.
ARLEQUIN, version 2.000. University of Geneva,
Geneva, Switzerland.<person-group>
                  <string-name>
                     <surname>Schneider</surname>
                  </string-name>
               </person-group>
               <source>ARLEQUIN, version 2.000</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1313a1310">
            <mixed-citation id="d407e1317" publication-type="book">
SHRAUDER, P. A. 1984. Whitetail populations and habitats:
Appalachian Mountains. Pages 331-334 in L. K. Halls,
editor. White-tailed deer: ecology and management.
Stackpole Books, Harrisburg, Pennsylvania, USA.<person-group>
                  <string-name>
                     <surname>Shrauder</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Whitetail populations and habitats: Appalachian Mountains</comment>
               <fpage>331</fpage>
               <source>White-tailed deer: ecology and management</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1355a1310">
            <mixed-citation id="d407e1361" publication-type="book">
SMITH, M. H., M. N. MANLOVE, ANDJ. JOULE. 1978. Spa-
tial and temporal dynamics of the genetic organiza-
tion of small mammal populations. Pages 99-113 in
D. P. Snyder, editor. Populations of small mammals
under natural conditions. University of Pittsburgh
Press, Pittsburgh, Pennsylvania, USA.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Spatial and temporal dynamics of the genetic organization of small mammal populations</comment>
               <fpage>99</fpage>
               <source>Populations of small mammals under natural conditions</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1405a1310">
            <mixed-citation id="d407e1409" publication-type="journal">
SMITH, P. F., D. D. DENDANTO, K. T SMITH, D. PALMAN,
AND I. KORNFIELD. 2002. Allele frequencies for three
str loci rt24, rt09, and bm1225, in northern New Eng-
land white-tailed deer. Journal of Forensic Sciences
47:673-675.<person-group>
                  <string-name>
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <fpage>673</fpage>
               <volume>47</volume>
               <source>Journal of Forensic Sciences</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1450a1310">
            <mixed-citation id="d407e1454" publication-type="book">
SPSS. 1999. SYSTAT 9.0. SPSS, Chicago, Illinois, USA.<person-group>
                  <string-name>
                     <surname>Spss</surname>
                  </string-name>
               </person-group>
               <source>SYSTAT 9.0</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1477a1310">
            <mixed-citation id="d407e1481" publication-type="journal">
STRODE, D. H. 1951. Restoration of big game is paying
off. Happy Hunting Ground 30:7.<person-group>
                  <string-name>
                     <surname>Strode</surname>
                  </string-name>
               </person-group>
               <fpage>7</fpage>
               <volume>30</volume>
               <source>Happy Hunting Ground</source>
               <year>1951</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1513a1310">
            <mixed-citation id="d407e1517" publication-type="journal">
TAJIMA, F. 1983. Evolutionary relationships of DNA
sequences in finite populations. Genetics 105:437-460.<person-group>
                  <string-name>
                     <surname>Tajima</surname>
                  </string-name>
               </person-group>
               <fpage>437</fpage>
               <volume>105</volume>
               <source>Genetics</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1549a1310">
            <mixed-citation id="d407e1553" publication-type="book">
---. 1993. Measurement of DNA polymorphism.
Pages 37-59 in N. Takahata and A. G. Clark, editors.
Mechanisms of molecular evolution: introduction to
molecular paleopopulation biology. Sinauer Associ-
ates, Sunderland, Massachusetts, USA.<person-group>
                  <string-name>
                     <surname>Tajima</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Measurement of DNA polymorphism</comment>
               <fpage>37</fpage>
               <source>Mechanisms of molecular evolution: introduction to molecular paleopopulation biology</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1594a1310">
            <mixed-citation id="d407e1600" publication-type="book">
TORGERSON, O., AND W. P. PORATH. 1984. Whitetail pop-
ulations and habitats: midwest oak/hickory forest.
Pages 411-426 in L. K. Halls, editor. White-tailed
deer: ecology and management. Stackpole Books,
Harrisburg, Pennsylvania, USA.<person-group>
                  <string-name>
                     <surname>Torgerson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Whitetail populations and habitats: midwest oak/hickory forest</comment>
               <fpage>411</fpage>
               <source>White-tailed deer: ecology and management</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1641a1310">
            <mixed-citation id="d407e1645" publication-type="book">
VERME, L. J., AND D. E. ULLREY. 1984. Whitetail popula-
tions and management: physiology and nutrition.
Pages 91-118 in L. K. Halls, editor. White-tailed deer:
ecology and management. Stackpole Books, Harris-
burg, Pennsylvania, USA.<person-group>
                  <string-name>
                     <surname>Verme</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Whitetail populations and management: physiology and nutrition</comment>
               <fpage>91</fpage>
               <source>White-tailed deer: ecology and management</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1686a1310">
            <mixed-citation id="d407e1690" publication-type="book">
WEIR, B. S. 1996. Genetic data analysis II: methods for
discrete population data. Sinauer Associates, Sunder-
land, Massachusetts, USA.<person-group>
                  <string-name>
                     <surname>Weir</surname>
                  </string-name>
               </person-group>
               <source>Genetic data analysis II: methods for discrete population data</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d407e1720a1310">
            <mixed-citation id="d407e1724" publication-type="journal">
---, AND C. C. COCKERHAM. 1984. Estimating Fstatis-
tics for the analysis of population structure. Evolution
38:1358-1370.<object-id pub-id-type="doi">10.2307/2408641</object-id>
               <fpage>1358</fpage>
            </mixed-citation>
         </ref>
         <ref id="d407e1743a1310">
            <mixed-citation id="d407e1747" publication-type="journal">
WILSON, G., C. STROBECK, L. WU, AND J. COFFIN. 1997.
Characterization of microsatellite loci in caribou
(Rangifer tarandus) and their use in other artiodactyls.
Molecular Ecology 6:697-699.<person-group>
                  <string-name>
                     <surname>Wilson</surname>
                  </string-name>
               </person-group>
               <fpage>697</fpage>
               <volume>6</volume>
               <source>Molecular Ecology</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
