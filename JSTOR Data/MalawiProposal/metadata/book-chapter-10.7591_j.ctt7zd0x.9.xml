<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7zd0x</book-id>
      <subj-group>
         <subject content-type="call-number">HD9017.A3572A35 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food supply</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Nutrition</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agricultural systems</subject>
         <subj-group>
            <subject content-type="lcsh">Health aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food industry and trade</subject>
         <subj-group>
            <subject content-type="lcsh">Health aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Africa, Sub-Saharan</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Food security</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Public Health</subject>
         <subj-group>
            <subject content-type="lcsh">Africa, Sub-Saharan</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The African Food System and Its Interactions with Human Health and Nutrition</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>edited by</role>
            <name name-style="western">
               <surname>PINSTRUP-ANDERSEN</surname>
               <given-names>PER</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>04</day>
         <month>11</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780801476921</isbn>
      <isbn content-type="epub">9780801462290</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>ITHACA; LONDON</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.ctt7zd0x"/>
      <abstract abstract-type="short">
         <p>Hunger, malnutrition, poor health, and deficient food systems are widespread in Sub-Saharan Africa. While much is known about African food systems and about African health and nutrition, our understanding of the interaction between food systems and health and nutrition is deficient. Moreover, the potential health gains from changes in the food system are frequently overlooked in policy design and implementation.</p>
         <p>The authors of<italic>The African Food System and its Interactions with Human Health and Nutrition</italic>examine how public policy and research aimed at the food system and its interaction with human health and nutrition can improve the well-being of Africans and help achieve the United Nations Millennium Development Goals (MDGs). Several of the MDGs focus on health-related challenges: hunger alleviation; maternal, infant, and child mortality; the control of HIV/AIDS, tuberculosis, and malaria; and the provision of safe water and improved sanitation. These challenges are intensified by problems of low agricultural and food system productivity, gender inequity, lack of basic infrastructure, and environmental degradation, all of which have direct and indirect detrimental effects on health, nutrition, and the food system.</p>
         <p>Reflecting the complexity and multidisciplinary nature of these problems and their solutions, this book features contributions by world-renowned experts in economics, agriculture, health, nutrition, food science, and demography.</p>
      </abstract>
      <counts>
         <page-count count="384"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.3</book-part-id>
                  <title-group>
                     <title>LIST OF FIGURES</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.4</book-part-id>
                  <title-group>
                     <title>LIST OF TABLES</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.5</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stover</surname>
                           <given-names>Patrick J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
                  <abstract abstract-type="extract">
                     <p>This volume is the first in a planned series focused on the continent of Africa. The intent of the Africa Series is to bring together leading scholars from Africa and around the globe to deepen our understanding and provide new insights into the gaps in knowledge and policy that hinder the progress of Sub-Saharan governments in achieving the Millennium Development Goals. The series also aims to inform the development and implementation of future policies and practices in the region and provide a forum for sharing best practices in the unique African context. This effort brings together two academic institutions, the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.6</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pinstrup-Andersen</surname>
                           <given-names>Per</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.7</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>The African Food System and Human Health and Nutrition:</title>
                     <subtitle>A Conceptual and Empirical Overview</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pinstrup-Andersen</surname>
                           <given-names>Per</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>A food system may be described simply as a process that turns natural and human-made resources and inputs into food. As illustrated in Figure 1.1, such a system may consist of the resources (such as land, water, and sunshine); inputs (such as plant nutrients, pest control measures, and knowledge); primary or agricultural production; secondary production or processing; and transport, storage, and exchange activities to make the food available at the time and place desired by consumers. Food systems need not be stagnant. In fact, if the goal is to improve them, it is useful to visualize food systems as dynamic</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>The Nutrition Situation in Sub-Saharan Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Watson</surname>
                           <given-names>Derrill D.</given-names>
                           <suffix>II</suffix>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pinstrup-Andersen</surname>
                           <given-names>Per</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>14</fpage>
                  <abstract abstract-type="extract">
                     <p>Globally, hunger and malnutrition affect the lives of more than 1 billion men, women, and children. As the Food and Agriculture Organization of the United Nations (FAO) compellingly states, hunger “saps strength and dulls intelligence. It destroys innocent lives, especially children. And by weakening a nation’s workforce, hunger cripples a nation’s growth” (FAO 2007c). In few areas is the need to overcome hunger and malnutrition more urgent than in Sub-Saharan Africa.</p>
                     <p>Malnutrition may take several forms: hunger, hidden hunger, and obesity. In this chapter, “hunger” refers primarily to energy and protein deficiencies. Yet even when people consume sufficient calories, they</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Disease Burdens of Sub-Saharan Africa and Their Interactions with Malnutrition</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ole-MoiYoi</surname>
                           <given-names>Onesmo K.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>34</fpage>
                  <abstract abstract-type="extract">
                     <p>Most countries in Sub-Saharan Africa are mired in a profound poverty problem and are not on track to achieve most of the Millennium Development Goals (MDGs). Civil strife and poor governance are major contributors to this lack of progress, and extreme poverty raises the likelihood of not only violent conflict and the collapse of states into lawlessness, but also falling health indicators.</p>
                     <p>The Africa of the 21st century is faced with a heavy burden of disease, combined with ill-equipped medical systems and underdeveloped technological capacity. In addition, the perceived risk of infections and other health problems discourage foreign investment and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Animals as a Source of Human Diseases:</title>
                     <subtitle>Historical Perspective and Future Health Risks for the African Population</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Torrey</surname>
                           <given-names>E. Fuller</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>58</fpage>
                  <abstract abstract-type="extract">
                     <p>Africa is a continent of firsts. Five million years ago, it gave birth to the first hominids, some of whom carried diseases they had inherited from their primate ancestors. Approximately 1.5 million years ago, Africa was the site of what may whimsically be regarded as the first experiment in nutrition—the cooking of meat using fire. And approximately 75,000 years ago, Africa was the site of the evolution of the first humans, <italic>Homo sapiens</italic>, armed with the ability to think about themselves, understand others, think about the past and the future, and write chapters for books about nutrition and disease.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>How AIDS Epidemics Interact with African Food Systems and How to Improve the Response</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gillespie</surname>
                           <given-names>Stuart</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>77</fpage>
                  <abstract abstract-type="extract">
                     <p>Among the multiple causes and consequences of AIDS epidemics, the vicious circle between AIDS and food insecurity is a growing concern, particularly in Sub-Saharan Africa, where HIV is hyperendemic in several countries. The coexistence of and interactions between AIDS and hunger are further accentuated at present by the ongoing food crisis.</p>
                     <p>It is useful to start by examining the evolution of the AIDS epidemic in Africa, using data from UNAIDS, released in July 2008 (UNAIDS 2008). At that time, globally, more than 25 million people had died since the first case was reported in 1981, from a total estimate of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Pest Management, Farmer Incomes, and Health Risks in Sub-Saharan Africa:</title>
                     <subtitle>Pesticides, Host Plant Resistance, and Other Measures</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nelson</surname>
                           <given-names>Rebecca</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>109</fpage>
                  <abstract abstract-type="extract">
                     <p>The majority of Africans are smallholder farmers who depend on rainfed agriculture for their livelihoods. Sixty-four percent of the population of Sub-Saharan Africa is rural, and 60 percent of the economically active population derives its livelihood from agriculture (FAO 2006b). Most farms are a hectare or less in size. For smallholder farmers, family food security may be affected by any of a wide array of biotic or abiotic stresses that reduce the quantity or quality of their crop yields. Biotic threats to crop productivity include chronic or periodic predation by insect or mammalian pests; infection by viruses, fungi, bacteria, or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Nutrition and the Environment:</title>
                     <subtitle>Fundamental to Food Security in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Herforth</surname>
                           <given-names>Anna</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>128</fpage>
                  <abstract abstract-type="extract">
                     <p>The environment is the foundation for food security and nutrition. Ecosystem services are the basis for food production and the caloric and nutrient yields necessary for human nutrition. Adequate nutrition is necessary for humans’ ability to work and to seek food. How people seek and produce food, from crop choices to agricultural use of fossil fuels and water, affects the environment, which in turn affects nutrition by dictating which crops can be grown where and when and what they will yield (Figure 7.1).</p>
                     <p>Components of “the environment” include biodiversity, soil, water, climate, and ecosystems, all of which provide ecosystem services.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Food Safety as a Bridge between the Food System and Human Health in Sub-Saharan Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Nakimbugwe</surname>
                           <given-names>Dorothy</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Boor</surname>
                           <given-names>Kathryn J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>161</fpage>
                  <abstract abstract-type="extract">
                     <p>Food systems, when operating optimally, should guarantee food security for the populations that depend on them—that is, individuals, families, communities, nations, and regions—by ensuring that even those who are most vulnerable have physical and economic access to sufficient, safe, and nutritious food to meet their dietary needs, at all times and from non-emergency sources (FAO 1996a). For the purposes of this chapter, a “food system” is defined as consisting of multiple subcomponents including food production systems, food-processing systems, food-marketing and distribution systems, and food control systems. Food control systems include food laws and regulations, food control management, food</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.16</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Population Dynamics and Future Food Requirements in Sub-Saharan Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Torrey</surname>
                           <given-names>Barbara Boyle</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>182</fpage>
                  <abstract abstract-type="extract">
                     <p>The inexorable growth in the Sub-Saharan African population in the next 40 years will complicate agricultural solutions developed for today’s problems. Even assuming that fertility rates drop steadily, the numbers of people to feed will more than double by 2050. They will also be older than today’s population, more urban, and more demanding of their agricultural systems.</p>
                     <p>This chapter begins with a brief discussion of the broad population changes in Africa since 1950. These changes vary by region but are impressive in aggregate. The population changes are occurring simultaneously with epidemiological change, changes in how people live, the illnesses they</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.17</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>Income and Food Transfers to Improve Human Health and Nutrition</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Alderman</surname>
                           <given-names>Harold</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>199</fpage>
                  <abstract abstract-type="extract">
                     <p>One of the first major initiatives launched by Nelson Mandela after becoming president of South Africa was to establish a nationwide school meal program in August 1994. By coincidence, the announcement came while the country’s nutritionists were holding a conference in Durban. The news was not greeted by congratulatory smiles. Rather, the workshop participants were generally disappointed. Why would the announcement of a full-scale food transfer program receive such a cool reaction? This chapter addresses that question, although, hopefully, the inquiry can be placed in the more constructive framework of how programs that aim to increase food consumption can complement,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.18</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>The Impact of Poor Health and Nutrition on Labor Productivity, Poverty, and Economic Growth in Sub-Saharan Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sahn</surname>
                           <given-names>David E.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>218</fpage>
                  <abstract abstract-type="extract">
                     <p>It is well recognized that health status is the end product of the process of economic growth. Receiving less attention, however, is that economic development and prosperity are, in large measure, determined by the quality of human resources. More specifically, the productivity of workers is directly influenced by the health and nutritional well-being of the population. That the physical and mental health of workers is essential to economic growth is a concept that dates back to perhaps the most notable economics treatise ever written: Adam Smith’s The Wealth of Nations, which first appeared in 1776. In that influential work, Smith</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.19</book-part-id>
                  <title-group>
                     <label>CHAPTER 12</label>
                     <title>Food Systems and the Escape from Poverty and Ill-Health Traps in Sub-Saharan Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Barrett</surname>
                           <given-names>Christopher B.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>242</fpage>
                  <abstract abstract-type="extract">
                     <p>The Millennium Declaration of September 2000, adopted by the 189 member states of the United Nations, renewed the vigor of the global community’s commitment to improving living conditions throughout the world. The very first Millennium Development Goal (MDG) is to halve, by 2015, the proportion of people living in extreme poverty and suffering from hunger. This chapter focuses on that objective and its relation to food systems—the human-managed biophysical systems that are involved in the production, distribution, and consumption of food—especially in Sub-Saharan Africa.</p>
                     <p>The bold but attainable goal enshrined in MDG 1 seems likely to be met</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.20</book-part-id>
                  <title-group>
                     <label>CHAPTER 13</label>
                     <title>Strengthening the Role of Women in the Food Systems of Sub-Saharan Africa to Achieve Nutrition and Health Goals</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Cramer</surname>
                           <given-names>Laura K.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wandira</surname>
                           <given-names>Speciosa K.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>261</fpage>
                  <abstract abstract-type="extract">
                     <p>Women make up 70 percent of the agricultural workers in many countries of Sub-Saharan Africa, contribute 60–80 percent of the labor that produces subsistence food crops (Dao 2004), and account for 80 percent of food processing (Markwei et al. 2008). Despite these significant contributions to agricultural output, women have traditionally received minimal attention from local government agricultural extension services and have benefited least from innovations that reduce labor and boost value addition. Low technology inputs have resulted in low levels of productivity, poor participation in markets, and poor returns on their investments.</p>
                     <p>Researchers and development professionals have begun to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.21</book-part-id>
                  <title-group>
                     <label>CHAPTER 14</label>
                     <title>Bridging the Gap:</title>
                     <subtitle>Linking Agriculture and Health to Achieve the Millennium Development Goals</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>von Braun</surname>
                           <given-names>Joachim</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ruel</surname>
                           <given-names>Marie T.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Gillespie</surname>
                           <given-names>Stuart</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>279</fpage>
                  <abstract abstract-type="extract">
                     <p>In September 2000, heads of state adopted the Millennium Declaration confirming their countries’ commitment to achieving the Millennium Development Goals (MDGs), a set of eight targets for addressing the many dimensions of extreme poverty—from income poverty, hunger, and health to education, environment, and gender.</p>
                     <p>The central MDG is the first one: MDG 1, which aims at “eradicating poverty and hunger.” Although the MDGs do not establish a hierarchy of goals (and subgoals), most of the other goals in fact act as subgoals to MDG 1. Poverty and hunger eradication are goals as well as instruments for well-being, and they</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.22</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>305</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zd0x.23</book-part-id>
                  <title-group>
                     <title>CONTRIBUTORS</title>
                  </title-group>
                  <fpage>353</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
