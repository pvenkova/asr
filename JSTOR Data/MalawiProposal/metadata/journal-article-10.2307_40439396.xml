<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">weltarch</journal-id>
         <journal-id journal-id-type="jstor">j50000548</journal-id>
         <journal-title-group>
            <journal-title>Weltwirtschaftliches Archiv</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>J. C. B. Mohr (Paul Siebeck)</publisher-name>
         </publisher>
         <issn pub-type="ppub">00432636</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">40439396</article-id>
         <title-group>
            <article-title>Real Exchange Rate Variability and Monetary Disturbances</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Paul</given-names>
                  <surname>de Grauwe</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Marc</given-names>
                  <surname>Rosiers</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1987</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">123</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i40019600</issue-id>
         <fpage>430</fpage>
         <lpage>448</lpage>
         <permissions>
            <copyright-statement>Copyright Institut für Weltwirtschaft an der Universität Kiel</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/40439396"/>
         <abstract>
            <p>Die kurzfristigen Schwankungen der realen Wechselkurse sind seit der Einführung floatender Wechselkurse beträchtlich. Dabei haben die Länder ganz unterschiedliche Erfahrungen gemacht. Einige erlebten reale Wechselkursbewegungen, die fünfmal so hoch waren wie in anderen Fällen. In diesem Aufsatz wird versucht, diese großen Unterschiede zwischen den Ländern zu erklären. Dabei wird ein Modell benutzt und getestet, in dem die kurzfristige Variabilität der realen Wechselkurse eine positive Funktion des Umfanges monetärer Störungen ist. Eine zweite Eigenschaft des Modells besteht darin, daß diese Beziehung nichtlinear ist, d.h., wenn die monetären Störungen einen bestimmten Umfang überschreiten, wird es sich für die Wirtschaftssubjekte lohnen, die Preise schneller anzupassen. Dadurch begrenzen sie die Abweichungen der nominalen Wechselkurse von den durch die Kaufkraftparität angezeigten Werten.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d582e226a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d582e233" publication-type="other">
Aizenman [1984, p. 178].</mixed-citation>
            </p>
         </fn>
         <fn id="d582e240a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d582e247" publication-type="other">
De
Grauwe et al [1985].</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d582e266a1310">
            <mixed-citation id="d582e270" publication-type="other">
Aizenman, Joshua, "Modeling Deviations from Purchasing Power Parity". International
Economic Review, Vol. 25, 1984, pp. 175-191.</mixed-citation>
         </ref>
         <ref id="d582e280a1310">
            <mixed-citation id="d582e284" publication-type="other">
Dornbusch, Rudiger, "Expectations and Exchange Rate Dynamics". Journal of Political
Economy, Vol. 84, 1976, pp. 1161-1176.</mixed-citation>
         </ref>
         <ref id="d582e294a1310">
            <mixed-citation id="d582e298" publication-type="other">
Frenkel, Jacob A., "A Monetary Approach to the Exchange Rate. Doctrinal Aspects and
Empirical Evidence". In: Jacob A. Frenkel, Harry G. Johnson (Eds.), The Economics of
Exchange Rates. Reading, Mass., 1978, pp. 1-25.</mixed-citation>
         </ref>
         <ref id="d582e311a1310">
            <mixed-citation id="d582e315" publication-type="other">
-, "The Collapse of Purchasing Power Parities during the 1970s". The European Economic
Review, Vol. 16, 1981, pp. 145-165.</mixed-citation>
         </ref>
         <ref id="d582e326a1310">
            <mixed-citation id="d582e330" publication-type="other">
De Grauwe, Paul, Marc Janssens, H. Leliaert, Real Exchange Rate Variability during
1920-1926 and 1973-1982. Princeton Studies in International Finance, No. 56. Princeton,
September 1985.</mixed-citation>
         </ref>
         <ref id="d582e343a1310">
            <mixed-citation id="d582e347" publication-type="other">
International Monetary Fund (IMF), International Financial Statistics. Washington, D.C.,
var. issues.</mixed-citation>
         </ref>
         <ref id="d582e357a1310">
            <mixed-citation id="d582e361" publication-type="other">
Kravis, Irving B., Robert E. Lipsey, Toward an Explanation of National Price Levels.
Princeton Studies in International Finance, No. 52. Princeton, November 1983.</mixed-citation>
         </ref>
         <ref id="d582e371a1310">
            <mixed-citation id="d582e375" publication-type="other">
Mussa, Michael, "The Exchange Rate, the Balance of Payments and Monetary and Fiscal
Policy under a Regime of Controlled Floating". The Scandinavian Journal of Economics,
Vol. 58, 1976, pp. 229-248.</mixed-citation>
         </ref>
         <ref id="d582e388a1310">
            <mixed-citation id="d582e392" publication-type="other">
-, "Empirical Regularities in the Behavior of Exchange Rates and Theories of the Foreign
Exchange Market". In: Karl Brunner, Allan Meltzer (Eds.), Policies for Employment and
Exchange Rates. Amsterdam 1980, pp. 9-57.</mixed-citation>
         </ref>
         <ref id="d582e405a1310">
            <mixed-citation id="d582e409" publication-type="other">
Theil, Henri, Principles of Econometrics. New York 1971.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
