<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         article-type="research-article"
         dtd-version="1.0"
         xml:lang="en">
   <front>
      <journal-meta>
         <journal-id journal-id-type="publisher-id">amernatu</journal-id>
         <journal-title-group>
            <journal-title>The American Naturalist</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00030147</issn>
         <issn pub-type="epub">15375323</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.1086/657038</article-id>
         <article-id pub-id-type="msid">AN52063</article-id>
         <title-group>
            <article-title>Predictive Adaptive Responses: Condition‐Dependent Impact of Adult Nutrition and Flight in the Tropical Butterfly<italic>Bicyclus anynana</italic>
            </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Marjo</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Saastamoinen</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="aff">
                  <sup>1,2,</sup>
               </xref>
               <xref ref-type="fn" rid="fn1">*</xref>
               <x xml:space="preserve"/>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Dominique</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>van der Sterren</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="aff">
                  <sup>1</sup>
               </xref>
               <x xml:space="preserve"/>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Nienke</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Vastenhout</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="aff">
                  <sup>1</sup>
               </xref>
               <x xml:space="preserve"/>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Bas J.</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Zwaan</surname>
               </string-name>
               <x xml:space="preserve">,</x>
               <xref ref-type="aff">
                  <sup>1</sup>
               </xref>
               <x xml:space="preserve">and</x>
            </contrib>
            <contrib contrib-type="author" xlink:type="simple">
               <string-name>
                  <given-names>Paul M.</given-names>
                  <x xml:space="preserve"> </x>
                  <surname>Brakefield</surname>
               </string-name>
               <xref ref-type="aff">
                  <sup>1</sup>
               </xref>
            </contrib>
            <aff id="aff_1">1. Institute of Biology, Leiden University, P.O. Box 9505, 2300 RA, Leiden, The Netherlands;</aff>
            <aff id="aff_2">2. Metapopulation Research Group, Department of Biological Sciences, University of Helsinki, P.O. Box 65, FI‐00014 Helsinki, Finland</aff>
         </contrib-group>
         <contrib-group>
            <contrib contrib-type="editor" xlink:type="simple">
               <string-name>Associate Editor: Allen J. Moore</string-name>
            </contrib>
         </contrib-group>
         <contrib-group>
            <contrib contrib-type="editor" xlink:type="simple">
               <string-name>Editor: Judith L. Bronstein</string-name>
            </contrib>
         </contrib-group>
         <pub-date pub-type="ppub">
            <month>12</month>
            <year>2010</year>
            <string-date>December 2010</string-date>
         </pub-date>
         <pub-date pub-type="ppub">
            <day>18</day>
            <month>10</month>
            <year>2010</year>
            <string-date>October 18, 2010</string-date>
         </pub-date>
         <volume>176</volume>
         <issue>6</issue>
         <issue-id>648330</issue-id>
         <fpage>686</fpage>
         <lpage>698</lpage>
         <permissions>
            <copyright-statement>© 2010 by The University of Chicago.</copyright-statement>
            <copyright-year>2010</copyright-year>
            <copyright-holder>The University of Chicago</copyright-holder>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/10.1086/657038"/>
         <abstract>
            <label>Abstract:</label>
            <p>The experience of environmental stress during development can substantially affect an organism’s life history. These effects are often mainly negative, but a growing number of studies suggest that under certain environmental conditions early experience of such stress may yield individuals that are less sensitive to environmental stress later on in life. We used the butterfly<italic>Bicyclus anynana</italic>to study the effects of limited larval and adult food and forced flight on individual performance measured as reproduction and adult life span. Larvae exposed to food stress showed longer development and produced smaller adults. Thus, they were not able to fully compensate for the food deprivation during development. Females that experienced food stress during development did not increase tolerance for adult food limitation. However, females exposed to food stress during development coped better with forced flight compared with the control group. The apparent absence of costs of flight in poor‐quality females may be a by‐product of an altered body allocation, as females experiencing both food stress treatments had increased thorax ratios, compared with controls, and increased flight performances. The results reveal an important plasticity component to variation in flight performance and suggest that the cost of flight depends on an individual’s internal condition.</p>
         </abstract>
         <kwd-group>
            <title>Keywords:</title>
            <kwd>environment</kwd>
            <x xml:space="preserve">,</x>
            <kwd>flight</kwd>
            <x xml:space="preserve">,</x>
            <kwd>life history</kwd>
            <x xml:space="preserve">,</x>
            <kwd>nutrition limitation</kwd>
            <x xml:space="preserve">,</x>
            <kwd>stress</kwd>
            <x xml:space="preserve">,</x>
            <kwd>trade‐off</kwd>
            <x xml:space="preserve">.</x>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
      <notes notes-type="footnote">
         <fn-group>
            <fn id="fn1">
               <label>*</label>
               <p>Corresponding author; e‐mail:<email xlink:href="mailto:marjo.saastamoinen@helsinki.fi" xlink:type="simple">marjo.saastamoinen@helsinki.fi</email>.</p>
            </fn>
         </fn-group>
      </notes>
   </front>
   <back>
      <ack>
         <sec>
            <title>Acknowledgments</title>
            <p>We are grateful to F. Kesbeke for technical help. Comments by C. Boggs and an anonymous reviewer greatly improved our manuscript. This work was supported by the European Union–funded Network of Excellence LifeSpan (FP6 036894) and the Academy of Finland (to M.S.; grant 125970).</p>
         </sec>
      </ack>
      <ref-list content-type="unparsed">
         <title>Literature Cited</title>
         <ref id="rf1">
            <mixed-citation xlink:type="simple" publication-type="">Barrett, E. L. B., J. Hunt, A. J. Moore, and P. J. Moore. 2009. Separate and combined effects of nutrition during juvenile and sexual development on female life‐history trajectories: the thrifty phenotype in a cockroach. Proceedings of the Royal Society B: Biological Sciences 276:3257–3264.</mixed-citation>
         </ref>
         <ref id="rf2">
            <mixed-citation xlink:type="simple" publication-type="">Bauerfeind, S. S., and K. Fischer. 2005<italic>a</italic>. Effects of adult‐deprived carbohydrates, amino acids and micronutrients on female reproduction in a fruit‐feeding butterfly. Journal of Insect Physiology 51:545–554.</mixed-citation>
         </ref>
         <ref id="rf3">
            <mixed-citation xlink:type="simple" publication-type="">———. 2005<italic>b</italic>. Effects of food stress and density in different life stages on reproduction in a butterfly. Oikos 111:514–524.</mixed-citation>
         </ref>
         <ref id="rf4">
            <mixed-citation xlink:type="simple" publication-type="">———. 2009. Effects of larval starvation and adult diet‐derived amino acids on reproduction in a fruit‐feeding butterfly. Entomologia Experimentalis et Applicata 130:229–237.</mixed-citation>
         </ref>
         <ref id="rf5">
            <mixed-citation xlink:type="simple" publication-type="">Berwaerts, K., and H. Van Dyck. 2004. Take‐off performance under optimal and suboptimal thermal conditions in the butterfly<italic>Pararge aegeria</italic>. Oecologia (Berlin) 141:536–545.</mixed-citation>
         </ref>
         <ref id="rf6">
            <mixed-citation xlink:type="simple" publication-type="">Berwaerts, K., H. Van Dyck, and P. Aerts. 2002. Does flight morphology relate to flight performance? an experimental test with the butterfly<italic>Pararge aegeria</italic>. Functional Ecology 16:484–491.</mixed-citation>
         </ref>
         <ref id="rf7">
            <mixed-citation xlink:type="simple" publication-type="">Bijlsma, R. R., and V. Loeschke. 1997. Environmental stress: adaptation and evolution. Birkhäuser, Basel.</mixed-citation>
         </ref>
         <ref id="rf8">
            <mixed-citation xlink:type="simple" publication-type="">Boggs, C. L. 2009. Understanding insect life histories and senescence through a resource allocation lens. Functional Ecology 23:27–37.</mixed-citation>
         </ref>
         <ref id="rf9">
            <mixed-citation xlink:type="simple" publication-type="">Boggs, C. L., and K. D. Freeman. 2005. Larval food limitation in butterflies: effects of adult resource allocation and fitness. Oecologia (Berlin) 144:353–361.</mixed-citation>
         </ref>
         <ref id="rf10">
            <mixed-citation xlink:type="simple" publication-type="">Bonte, D., J. M. J. Travis, N. De Clercq, I. Zwertvaegher, and L. Lens. 2008. Thermal conditions during juvenile development affect adult dispersal in a spider. Proceedings of the National Academy of Sciences of the USA 105:17000–17005.</mixed-citation>
         </ref>
         <ref id="rf11">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M. 1997. Phenotypic plasticity and fluctuating asymmetry as responses to environmental stress in the butterfly<italic>Bicyclus anynana</italic>. Pages 65–78<italic>in</italic>R. Bijlsma and V. Loeschke, eds. Environmental stress, adaptation and evolution. Birkhäuser, Basel.</mixed-citation>
         </ref>
         <ref id="rf12">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M., and W. A. Frankino. 2009. Polyphenisms in Lepidoptera: multidisciplinary approaches to studies of evolution and development. Pages 281–312<italic>in</italic>D. W. Whitman and T. N. Ananthakrishnan, eds. Phenotypic plasticity in insects: mechanisms and consequences. Science, Plymouth.</mixed-citation>
         </ref>
         <ref id="rf13">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M., and F. Kesbeke. 1997. Genotype‐environment interactions for insect growth in constant and fluctuating temperature regimes. Proceedings of the Royal Society B: Biological Sciences 264:717–723.</mixed-citation>
         </ref>
         <ref id="rf14">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M., and T. B. Larsen. 1984. The evolutionary significance of dry and wet season forms in some tropical butterflies. Biological Journal of the Linnean Society 22:1–12.</mixed-citation>
         </ref>
         <ref id="rf15">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M., and N. Reitsma. 1991. Phenotypic plasticity, seasonal climate and the population biology of<italic>Bicyclus</italic>butterflies (Satyridae) in Malawi. Ecological Entomology 16:291–303.</mixed-citation>
         </ref>
         <ref id="rf16">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M., E. El Filali, R. Van der Laan, C. J. Breuker, I. J. Saccheri, and B. J. Zwaan. 2001. Effective population size, reproductive success and sperm precedence in the butterfly,<italic>Bicyclus anynana</italic>, in captivity. Journal of Evolutionary Biology 14:148–156.</mixed-citation>
         </ref>
         <ref id="rf17">
            <mixed-citation xlink:type="simple" publication-type="">Brakefield, P. M., P. Beldade, and B. J. Zwaan. 2009. The African butterfly<italic>Bicyclus anynana</italic>: a model for evolutionary genetics and evolutionary developmental biology. Pages 291–329<italic>in</italic>R. R. Behringer, A. D. Johnson, and R. E. Krumlauf, eds. Emerging model organisms: a laboratory manual. Cold Spring Harbor Laboratory, Cold Spring Harbor, NY.</mixed-citation>
         </ref>
         <ref id="rf18">
            <mixed-citation xlink:type="simple" publication-type="">Chin, E. H., O. P. Love, J. J. Verspoor, T. D. Williams, K. Rowley, and G. Burness. 2009. Juveniles exposed to embryonic corticosterone have enhanced flight performance. Proceedings of the Royal Society B: Biological Sciences 276:499–505.</mixed-citation>
         </ref>
         <ref id="rf19">
            <mixed-citation xlink:type="simple" publication-type="">Clobert, J., J. F. Le Galliard, J. Cote, S. Meylan, and M. Massot. 2009. Informed dispersal, heterogeneity in animal dispersal syndromes and the dynamics of spatially structured populations. Ecology Letters 12:197–209.</mixed-citation>
         </ref>
         <ref id="rf20">
            <mixed-citation xlink:type="simple" publication-type="">Criscuolo, F., P. Monaghan, L. Nasir, and N. B. Metcalfe. 2008. Early nutrition and phenotypic development: “catch‐up” growth leads to elevated metabolic rate in adulthood. Proceedings of the Royal Society B: Biological Sciences 275:1565–1570.</mixed-citation>
         </ref>
         <ref id="rf21">
            <mixed-citation xlink:type="simple" publication-type="">De Block, M., and R. Stoks. 2008. Short‐term larval food stress and associated compensatory growth reduce adult immune function in a damselfly. Ecological Entomology 33:796–801.</mixed-citation>
         </ref>
         <ref id="rf22">
            <mixed-citation xlink:type="simple" publication-type="">Dudley, R. 2002. The biomechanics of insect flight: form, function, evolution. Princeton University Press, Princeton, NJ.</mixed-citation>
         </ref>
         <ref id="rf23">
            <mixed-citation xlink:type="simple" publication-type="">Dufty, A. M., J. Clobert, and A. P. Møller. 2002. Hormones, developmental plasticity and adaptation. Trends in Ecology &amp; Evolution 17:190–196.</mixed-citation>
         </ref>
         <ref id="rf24">
            <mixed-citation xlink:type="simple" publication-type="">Fischer, K., B. J. Zwaan, and P. M. Brakefield. 2002. How does egg size relate to body size in butterflies? Oecologia (Berlin) 131:375–379.</mixed-citation>
         </ref>
         <ref id="rf25">
            <mixed-citation xlink:type="simple" publication-type="">Fischer, K., D. M. O’Brien, and C. L. Boggs. 2004. Allocation of larval and adult resources to reproduction in a fruit‐feeding butterfly. Functional Ecology 18:656–663.</mixed-citation>
         </ref>
         <ref id="rf26">
            <mixed-citation xlink:type="simple" publication-type="">Gibbs, M., and H. Van Dyck. 2010. Butterfly fight activity affects reproductive performance and longevity relative to landscape structure. Oecologia (Berlin) 163:341–350.</mixed-citation>
         </ref>
         <ref id="rf27">
            <mixed-citation xlink:type="simple" publication-type="">Gluckman, P. D., M. A. Hanson, and H. G. Spencer. 2005. Predictive adaptive responses and human evolution. Trends in Ecology &amp; Evolution 20:527–533.</mixed-citation>
         </ref>
         <ref id="rf28">
            <mixed-citation xlink:type="simple" publication-type="">Hamel, S., S. D. Côté, J.‐M. Gaillard, and M. Festa‐Bianchet. 2009. Individual variation in reproductive costs of reproduction: high‐quality females always do better. Journal of Animal Ecology 78:143–151.</mixed-citation>
         </ref>
         <ref id="rf29">
            <mixed-citation xlink:type="simple" publication-type="">Hanski, I., M. Saastamoinen, and O. Ovaskainen. 2006. Dispersal‐related life history trade‐offs in a butterfly metapopulation. Journal of Animal Ecology 75:91–100.</mixed-citation>
         </ref>
         <ref id="rf30">
            <mixed-citation xlink:type="simple" publication-type="">Hughes, C. L., J. K. Hill, and C. Dytham. 2003. Evolutionary trade‐offs between reproduction and dispersal in populations at expanding range boundaries. Proceedings of the Royal Society B: Biological Sciences 270:S147–S150.</mixed-citation>
         </ref>
         <ref id="rf31">
            <mixed-citation xlink:type="simple" publication-type="">Jannot, J. E., E. Bruneau, and S. A. Wissinger. 2007. Effects of larval energetic resources on life history and adult allocation patterns in a caddisfly (Trichoptera: Phryganeidae). Ecological Entomology 32:376–383.</mixed-citation>
         </ref>
         <ref id="rf32">
            <mixed-citation xlink:type="simple" publication-type="">Kisdi, E., G. Meszena, and L. Pasztor. 1998. Individual optimization: mechanisms shaping the optimal reaction norm. Evolutionary Ecology 12:211–221.</mixed-citation>
         </ref>
         <ref id="rf33">
            <mixed-citation xlink:type="simple" publication-type="">Koehn, R. K., and P. L. Bayne. 1989. Towards a physiological and genetic understanding of the energetics of the stress response. Biological Journal of the Linnean Society 37:157–171.</mixed-citation>
         </ref>
         <ref id="rf34">
            <mixed-citation xlink:type="simple" publication-type="">Lessells, C. M. 2008. Neuroendocrine control of life histories: what do we need to know to understand the evolution of phenotypic plasticity? Philosophical Transactions of the Royal Society B: Biological Sciences 363:1589–1598.</mixed-citation>
         </ref>
         <ref id="rf35">
            <mixed-citation xlink:type="simple" publication-type="">Lopez‐Maury, L., S. Marguerat, and J. Bähler. 2008. Tuning gene expression to changing environments: from rapid responses to evolutionary adaptation. Nature Review Genetics 9:583–593.</mixed-citation>
         </ref>
         <ref id="rf36">
            <mixed-citation xlink:type="simple" publication-type="">Marshall, D. J., and T. Uller. 2007. When is a maternal effect adaptive? Oikos 116:1957–1963.</mixed-citation>
         </ref>
         <ref id="rf37">
            <mixed-citation xlink:type="simple" publication-type="">McGlothlin, J. W., and E. D. Ketterson. 2008. Hormone‐mediated suites as adaptations and evolutionary constraints. Philosophical Transactions of the Royal Society B: Biological Sciences 363:1611–1620.</mixed-citation>
         </ref>
         <ref id="rf38">
            <mixed-citation xlink:type="simple" publication-type="">McNamara, J. M., and A. I. Houston 1996. State‐dependent life histories. Nature 380:215–221.</mixed-citation>
         </ref>
         <ref id="rf39">
            <mixed-citation xlink:type="simple" publication-type="">Metcalfe, N. B., and P. Monaghan. 2001. Compensation for a bad start: grow now, pay later? Trends in Ecology &amp; Evolution 16:254–260.</mixed-citation>
         </ref>
         <ref id="rf40">
            <mixed-citation xlink:type="simple" publication-type="">Meylan, S., J. Belliure, J. Clobert, and M. de Fraipont. 2002. Stress and body condition as prenatal and postnatal determinants of dispersal in the common lizard (<italic>Lacerta vivipara</italic>). Hormones and Behavior 42:319–326.</mixed-citation>
         </ref>
         <ref id="rf41">
            <mixed-citation xlink:type="simple" publication-type="">Meylan, S., M. De Fraipont, P. Aragon, E. Vercken, and J. Clobert. 2009. Are dispersal‐dependent behavioral traits produced by phenotypic plasticity? Journal of Experimental Zoology A 311:377–388.</mixed-citation>
         </ref>
         <ref id="rf42">
            <mixed-citation xlink:type="simple" publication-type="">Monaghan, P. 2008. Early growth conditions, phenotypic development and environmental change. Philosophical Transactions of the Royal Society B: Biological Sciences 363:1635–1645.</mixed-citation>
         </ref>
         <ref id="rf43">
            <mixed-citation xlink:type="simple" publication-type="">Niitepõld, K., A. D. Smith, J. L. Osborne, D. R. Reynolds, N. L. Carreck, A. P. Martin, J. H. Marden, et al. 2009. Flight metabolic rate and Pgi genotype influence butterfly dispersal rate in the field. Ecology 90:2223–2232.</mixed-citation>
         </ref>
         <ref id="rf44">
            <mixed-citation xlink:type="simple" publication-type="">Parker, W. E., and A. G. Gatehouse. 1985<italic>a</italic>. The effect of larval rearing conditions on flight performance of the African armyworm,<italic>Spodoptera exempta</italic>(Walker) (Lepidoptera: Noctuidae). Bulletin of Entomological Research 75:35–47.</mixed-citation>
         </ref>
         <ref id="rf45">
            <mixed-citation xlink:type="simple" publication-type="">———. 1985<italic>b</italic>. Genetic factors controlling flight performance and migration in the African armyworm moth,<italic>Spodoptera exempta</italic>(Walker) (Lepidoptera: Noctuidae). Bulletin of Entomological Research 75:49–63.</mixed-citation>
         </ref>
         <ref id="rf46">
            <mixed-citation xlink:type="simple" publication-type="">Parmesan, C. 2006. Ecological and evolutionary responses to recent climate change. Annual Review of Ecology and Systematics 37:637–669.</mixed-citation>
         </ref>
         <ref id="rf47">
            <mixed-citation xlink:type="simple" publication-type="">Pellegroms, B., S. Van Dongen, H. Van Dyck, and L. Lens. 2009. Larval food stress differentially affects flight morphology in male and female speckled woods (<italic>Pararge aegeria</italic>). Ecological Entomology 34:387–393.</mixed-citation>
         </ref>
         <ref id="rf48">
            <mixed-citation xlink:type="simple" publication-type="">Pijpe, J., K. Fischer, P. A. Brakefield, and B. J. Zwaan. 2006. Consequences of artificial selection on pre‐adult development for adult lifespan under benign conditions in the butterfly<italic>Bicyclus anynana</italic>. Mechanisms of Ageing and Development 127:802–807.</mixed-citation>
         </ref>
         <ref id="rf49">
            <mixed-citation xlink:type="simple" publication-type="">Pijpe, J., P. M. Brakefield, and B. J. Zwaan. 2008. Increased life span in a polyphenic butterfly artificially selected for starvation resistance. American Naturalist 171:81–90.</mixed-citation>
         </ref>
         <ref id="rf50">
            <mixed-citation xlink:type="simple" publication-type="">Rankin, M. A. 1991. Endocrine effects on migration. American Zoologist 31:217–230.</mixed-citation>
         </ref>
         <ref id="rf51">
            <mixed-citation xlink:type="simple" publication-type="">Roff, D. A. 2002. Life history evolution. Sinauer, Sunderland, MA.</mixed-citation>
         </ref>
         <ref id="rf52">
            <mixed-citation xlink:type="simple" publication-type="">Roff, D. A., and D. J. Fairbairn. 1991. Wing dimorphism and the evolution of migratory polymorphism among the Insecta. American Zoologist 31:343–351.</mixed-citation>
         </ref>
         <ref id="rf53">
            <mixed-citation xlink:type="simple" publication-type="">———. 2007. The evolution of trade‐offs: where are we? Journal of Evolutionary Biology 20:433–447.</mixed-citation>
         </ref>
         <ref id="rf54">
            <mixed-citation xlink:type="simple" publication-type="">SAS Institute. 1999. SAS/STAT user’s guide, release 8.00. SAS Institute, Cary, NC.</mixed-citation>
         </ref>
         <ref id="rf55">
            <mixed-citation xlink:type="simple" publication-type="">Senger, S. E., B. D. Roitberg, and H. M. A. Thistlewood. 2008. Ovarian response to resource availability in female<italic>Rhagoletis indifferens</italic>. Entomologia Experimentalis et Applicata 129:26–31.</mixed-citation>
         </ref>
         <ref id="rf56">
            <mixed-citation xlink:type="simple" publication-type="">Stearns, S. C. 1992. The evolution of life histories. Oxford University Press, New York.</mixed-citation>
         </ref>
         <ref id="rf57">
            <mixed-citation xlink:type="simple" publication-type="">Stelgenga, M. J., and K. Fischer. 2007. Within‐ and between‐generation effects of temperature on life‐history traits in a butterfly. Journal of Thermal Biology 32:396–405.</mixed-citation>
         </ref>
         <ref id="rf58">
            <mixed-citation xlink:type="simple" publication-type="">Stevens, V. M., C. Turlure, and M. Baguette. 2010. A meta‐analysis of dispersal in butterflies. Biological Reviews 85:625–642.</mixed-citation>
         </ref>
         <ref id="rf59">
            <mixed-citation xlink:type="simple" publication-type="">Thomas, C. D., J. K. Hill, and O. T. Lewis. 1998. Evolutionary consequences of habitat fragmentation in a localized butterfly. Journal of Animal Ecology 67:485–497.</mixed-citation>
         </ref>
         <ref id="rf60">
            <mixed-citation xlink:type="simple" publication-type="">van Noordwijk, A. J., and G. de Jong. 1986. Acquisition and allocation of resources: their influence on variation in life history tactics. American Naturalist 128:137–142.</mixed-citation>
         </ref>
         <ref id="rf61">
            <mixed-citation xlink:type="simple" publication-type="">Van’t Hof, A. E., B. J. Zwaan, I. J. Saccheri, D. Daly, A. N. M. Bot, and P. M. Brakefield. 2005. Characterization of 28 microsatellite loci for the butterfly<italic>Bicyclus anynana</italic>. Molecular Ecology Notes 5:169–172.</mixed-citation>
         </ref>
         <ref id="rf62">
            <mixed-citation xlink:type="simple" publication-type="">Wells, J. C. 2007. Flaws in the theory of predictive adaptive responses. Trends in Endocrinology and Metabolism 18:331–337.</mixed-citation>
         </ref>
         <ref id="rf63">
            <mixed-citation xlink:type="simple" publication-type="">Wickman, P. O., and B. Karlsson. 1989. Abdomen size, body size and the reproductive effort of insects. Oikos 56:209–214.</mixed-citation>
         </ref>
         <ref id="rf64">
            <mixed-citation xlink:type="simple" publication-type="">Wiggins, G. B. 1998. The caddisfly family Phryganeidae (Trichoptera). University of Toronto Press, Toronto.</mixed-citation>
         </ref>
         <ref id="rf65">
            <mixed-citation xlink:type="simple" publication-type="">Wilson, P. N., and D. F. Osbourn. 1960. Compensatory growth after undernutrition in mammals and birds. Biological Reviews 35:324.</mixed-citation>
         </ref>
         <ref id="rf66">
            <mixed-citation xlink:type="simple" publication-type="">Woodrow, K. P., A. G. Gatehouse, and D. A. Davies. 1987. The effect of larval phase on flight performance of African armyworm moths,<italic>Spodoptera exempta</italic>(Walker) (Lepidoptera, Noctuidae). Bulletin of Entomological Research 77:113–122.</mixed-citation>
         </ref>
         <ref id="rf67">
            <mixed-citation xlink:type="simple" publication-type="">Zera, A. J., and R. F. Denno. 1997. Physiology and ecology of dispersal polymorphism in insect. Annual Review of Entomology 42:207–230.</mixed-citation>
         </ref>
         <ref id="rf68">
            <mixed-citation xlink:type="simple" publication-type="">Zera, A. J., and L. G. Harshman. 2001. The physiology of life history trade‐offs in animals. Annual Review of Ecology and Systematics 32:95–126.</mixed-citation>
         </ref>
      </ref-list>
      <sec>
         <title/>
         <fig-group content-type="figgrp" id="fg5" position="float">
            <caption>
               <p>Female<italic>Bicyclus anynana</italic>. Photograph by Oskar Brattström.</p>
            </caption>
            <fig position="float" fig-type="figure">
               <graphic xlink:href="fg5.tiff"
                        mimetype="image"
                        position="float"
                        xlink:type="simple"/>
            </fig>
         </fig-group>
      </sec>
   </back>
</article>
