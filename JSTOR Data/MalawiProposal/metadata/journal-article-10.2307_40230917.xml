<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">poprespolrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000506</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population Research and Policy Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01675923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737829</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40230917</article-id>
         <article-id pub-id-type="pub-doi">10.1007/s11113-005-4290-z</article-id>
         <title-group>
            <article-title>The Interrelationship between Fosterage, Schooling, and Children's Labor Force Participation in Ghana</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anastasia</given-names>
                  <surname>Gage</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">24</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40008484</issue-id>
         <fpage>431</fpage>
         <lpage>466</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40230917"/>
         <abstract>
            <p>This paper examines the interrelationship between fosterage, school attendance, and children's employment in Ghana. Using the 1991/1992 Living Standards Survey data on children aged 7-17 years and a trivariate probit model, the paper demonstrates that ignoring the linkages between these outcomes leads to downwardbiased estimates of the impact of fosterage on schooling and upward-biased estimates of the impact of fosterage on work. Gender and age are important considerations in family decisions regarding children's activities. Joint decision-making is more common for girls aged 12-17 than for boys of a similar age. A significant negative correlation is also observed between the likelihood of employment and the likelihood of school attendance among adolescent girls.</p>
         </abstract>
         <kwd-group>
            <kwd>Child labor</kwd>
            <kwd>Education</kwd>
            <kwd>Fosterage</kwd>
            <kwd>Ghana</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d50e126a1310">
            <label>3</label>
            <mixed-citation id="d50e133" publication-type="other">
(Norton et al. 1995).</mixed-citation>
            <mixed-citation id="d50e139" publication-type="other">
(Canagarajah and
Coulombe 1997).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d50e158a1310">
            <mixed-citation id="d50e162" publication-type="other">
Ainsworth, M. (1996), Economic aspects of child fostering in Cote d' Ivoire, Research in
Population Economics 8: 25-62.</mixed-citation>
         </ref>
         <ref id="d50e172a1310">
            <mixed-citation id="d50e176" publication-type="other">
Akin, A. A. (1994), The socio-economic impact of child labour in Cameroon, Labor,
Capital and Society 27: 234-248.</mixed-citation>
         </ref>
         <ref id="d50e186a1310">
            <mixed-citation id="d50e190" publication-type="other">
Anh, T.S., Knodel, J., Lam, D. &amp; Friedman, J. (1998), Family size and children's
education in Vietnam, Demography 35: 57-70.</mixed-citation>
         </ref>
         <ref id="d50e200a1310">
            <mixed-citation id="d50e204" publication-type="other">
Bledsoe, C. &amp; Isiugo-Abanihe, U. (1989), Strategies of child-fosterage among Mende
grannies in Sierra Leone, pp. 442-474, in: Lesthaeghe (ed.), Reproduction and social
organization in sub-Saharan Africa, Berkeley, CA: University of California Press.</mixed-citation>
         </ref>
         <ref id="d50e218a1310">
            <mixed-citation id="d50e222" publication-type="other">
Bledsoe, C. (1991), 'No success without struggle': Social mobility and hardship for
foster children in Sierra Leone, Man 25: 70-88.</mixed-citation>
         </ref>
         <ref id="d50e232a1310">
            <mixed-citation id="d50e236" publication-type="other">
Bledsoe, C. (1993), The politics of polygyny in Mende education and child fosterage
transactions, pp. 170-192, in: B.D. Miller (ed.), Sex and gender hierarchies, Chicago,
IL: University of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d50e249a1310">
            <mixed-citation id="d50e253" publication-type="other">
Boateng, E.O., Ewusi, K., Kanbur, R. &amp; McKay, A. (1990). A poverty profile of
Ghana, 1987-88. Social Dimensions of Adjustment in Sub-Saharan Africa, Working
Paper No. 5. Washington, DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d50e266a1310">
            <mixed-citation id="d50e270" publication-type="other">
Buchmann, C. (2000), Family structure, parental perceptions, and child labor in Kenya:
What factors determine who is enrolled in school?, Social Forces 78: 1349-1378.</mixed-citation>
         </ref>
         <ref id="d50e280a1310">
            <mixed-citation id="d50e284" publication-type="other">
Canagarajah, S. &amp; Coulombe H. (1997). Child labor and schooling in Ghana. World
Bank Policy Research Working Paper No. 1844. Washington DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d50e294a1310">
            <mixed-citation id="d50e298" publication-type="other">
Chernichovsky, D. (1985), Socioeconomic and demographic aspects of school enroll-
ment and attendance in rural Botswana, Economic Development and Cultural Change
33: 319-332.</mixed-citation>
         </ref>
         <ref id="d50e312a1310">
            <mixed-citation id="d50e316" publication-type="other">
Csapo, M. (1981), Religious, social and economic factors hindering the education of
girls in northern Nigeria, Comparative Education Review 36: 446-466.</mixed-citation>
         </ref>
         <ref id="d50e326a1310">
            <mixed-citation id="d50e330" publication-type="other">
Fuller, B., Singer, D. &amp; Keily, M. (1995), Why do daughters leave school in southern
Africa? Family economy and mothers' commitments, Social Forces 74: 657-680.</mixed-citation>
         </ref>
         <ref id="d50e340a1310">
            <mixed-citation id="d50e344" publication-type="other">
Gomes, M. (1984), Family size and educational attainment in Kenya, Population and
Development Review 10: 647-660.</mixed-citation>
         </ref>
         <ref id="d50e354a1310">
            <mixed-citation id="d50e358" publication-type="other">
Greene, W.H. (1998), LIMDEP: Version 7.0, New York: Econometric Software, Inc.</mixed-citation>
         </ref>
         <ref id="d50e365a1310">
            <mixed-citation id="d50e369" publication-type="other">
Grosh, M.E. &amp; Glewwe, P. (1995). A guide to living standards measurement study
surveys and their data sets. Living standards measurement study, Working Paper
No. 120. Washington, DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d50e382a1310">
            <mixed-citation id="d50e386" publication-type="other">
Guilkey, D.K., Bollen, K.A. &amp; Mroz, T.A. (1995), Binary outcomes and endogenous
explanatory variables: Tests and solutions with an application to the demand for
contraceptive use in Tunisia, Demography 32: 111-131.</mixed-citation>
         </ref>
         <ref id="d50e400a1310">
            <mixed-citation id="d50e404" publication-type="other">
Hyde, K.A.L. (1993), Improving women's education in sub-Saharan Africa: A review of
the literature, pp. 100-135, in: E.M. King &amp; M.A. Hill (ed.), Women's education in
developing countries: Barriers, benefits and policies, Baltimore, MD: Johns Hopkins
University Press.</mixed-citation>
         </ref>
         <ref id="d50e420a1310">
            <mixed-citation id="d50e424" publication-type="other">
Ingstad, B. (1994), The grandmother and household viability in Botswana, pp. 209-240,
in: A. Adepoju &amp; C. Oppong (ed.), Gender, work and population in sub-Saharan
Africa, London: James Curry.</mixed-citation>
         </ref>
         <ref id="d50e437a1310">
            <mixed-citation id="d50e441" publication-type="other">
Isiugo-Abanihe, U.C. (1994), Reproductive motivation and family-size preferences
among Nigerian men, Studies in Family Planning 25: 149-161.</mixed-citation>
         </ref>
         <ref id="d50e451a1310">
            <mixed-citation id="d50e455" publication-type="other">
Knodel, J. &amp; Wongsith, M. (1991), Family size and children's education in Thailand:
Evidence from a national sample, Demography 28: 119-131.</mixed-citation>
         </ref>
         <ref id="d50e465a1310">
            <mixed-citation id="d50e469" publication-type="other">
Lloyd, C.B. &amp; Blanc, A.K. (1996), Children's schooling in sub-Saharan Africa: The role
of fathers, mothers, and others, Population and Development Review 22: 265-298.</mixed-citation>
         </ref>
         <ref id="d50e479a1310">
            <mixed-citation id="d50e483" publication-type="other">
Lloyd, C. &amp; Gage, A.J. (1995), High fertility and the intergenerational transmission
of gender inequality: Children's transition to adulthood in Ghana, pp. 127-146, in:
P. Makinwa &amp; A. Jensen (ed.), Women's position and demographic change in sub-
Saharan Africa, Liège, Belgium: International Union for the Scientific Study of
Population.</mixed-citation>
         </ref>
         <ref id="d50e503a1310">
            <mixed-citation id="d50e507" publication-type="other">
Lloyd, C.B. &amp; Gage-Brandon, A.J. (1994), High fertility and children's schooling in
Ghana: Sex differences in parental contributions and educational outcomes, Popu-
lation Studies 48: 293-306.</mixed-citation>
         </ref>
         <ref id="d50e520a1310">
            <mixed-citation id="d50e524" publication-type="other">
Lockheed, M., Fuller, B. &amp; Nyirongo, R. (1989), Family effects on students' achieve-
ment in Thailand and Malawi, Sociology of Education 62: 239-255.</mixed-citation>
         </ref>
         <ref id="d50e534a1310">
            <mixed-citation id="d50e538" publication-type="other">
Montgomery, M.R. &amp; Kouamé, A. (1993). Fertility and schooling in Côte d'"Ivoire: Is
there a trade-off? Technical Working Paper No. 11. Washington, DC: The World
Bank.</mixed-citation>
         </ref>
         <ref id="d50e551a1310">
            <mixed-citation id="d50e555" publication-type="other">
Mukhopadhyay, S.K. (1994), Adapting household behavior to agricultural technology
in West Bengal, India: Wage labor, fertility, and child schooling determinants,
Economic Development and Cultural Change 43: 91-103.</mixed-citation>
         </ref>
         <ref id="d50e568a1310">
            <mixed-citation id="d50e572" publication-type="other">
Norton, A., Bortei-Doku Aryeetey, E., Korboe, D. &amp; Dogbe D.K.T. (1995). Poverty
assessment in Ghana using qualitative and participatory research methods. Poverty
and Social Policy Discussion Paper. Washington, DC: The World Bank.</mixed-citation>
         </ref>
         <ref id="d50e585a1310">
            <mixed-citation id="d50e589" publication-type="other">
Oloko, B.A. (1991), Children's work in urban Nigeria: A case study of young Lagos
street traders, pp. 11-23, in: W.E. Myer (ed.), Protecting working children, London,
England: Zed Press.</mixed-citation>
         </ref>
         <ref id="d50e603a1310">
            <mixed-citation id="d50e607" publication-type="other">
Pong, S.L. (1997), Sibship size and educational attainment in peninsular Malaysia: Do
policies matter?, Sociological Perspectives 40: 227-242.</mixed-citation>
         </ref>
         <ref id="d50e617a1310">
            <mixed-citation id="d50e621" publication-type="other">
Ravololomanga, B. &amp; Schlemmer, B. (1994), De l'enfant richesse à l'enfant fardeau:
L'enfant au travail à Madagascar et sa place dans l'imaginaire social, Labor, Capital
and Society 27: 216-232.</mixed-citation>
         </ref>
         <ref id="d50e634a1310">
            <mixed-citation id="d50e638" publication-type="other">
Republic of Ghana and United Nations Children's Fund (1990), Children and women of
Ghana: A situation analysis 1989-90, Accra, Ghana: UNICEF.</mixed-citation>
         </ref>
         <ref id="d50e648a1310">
            <mixed-citation id="d50e652" publication-type="other">
Rose, P. (1995), Female education and adjustment programs: A cross-country statistical
analysis, World Development 23(11): 1931-1949.</mixed-citation>
         </ref>
         <ref id="d50e662a1310">
            <mixed-citation id="d50e666" publication-type="other">
Tansel, A. (1997), Schooling attainment, parental education and gender in Côte d'Ivoire
and Ghana, Economic Development and Cultural Change 45: 825-856.</mixed-citation>
         </ref>
         <ref id="d50e676a1310">
            <mixed-citation id="d50e680" publication-type="other">
UNESCO (1995), Statistical yearbook 1995, Lanham, MD: UNESCO and Bernan
Press.</mixed-citation>
         </ref>
         <ref id="d50e691a1310">
            <mixed-citation id="d50e695" publication-type="other">
United Nations Children's Fund (1993), Girls and women: A UNICEF development
priority, New York: UNICEF.</mixed-citation>
         </ref>
         <ref id="d50e705a1310">
            <mixed-citation id="d50e709" publication-type="other">
Verlet, M. (1994), Grandir à Nima: Dérégulation domestique et mise au travail des
enfants, Labor, Capital and Society 27: 162-190.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
