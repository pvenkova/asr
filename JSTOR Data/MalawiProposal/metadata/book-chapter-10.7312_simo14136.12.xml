<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/simo14136</book-id>
      <subj-group>
         <subject content-type="call-number">HV875.64.85577 2007</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Interracial adoption</subject>
         <subj-group>
            <subject content-type="lcsh">United States</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Adoptive parents</subject>
         <subj-group>
            <subject content-type="lcsh">United States</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>In Their Parents' Voices</book-title>
         <subtitle>Reflections on Raising Transracial Adoptees</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Simon</surname>
               <given-names>Rita J.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Roorda</surname>
               <given-names>Rhonda M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>02</day>
         <month>10</month>
         <year>2007</year>
      </pub-date>
      <isbn content-type="epub">9780231512350</isbn>
      <isbn content-type="epub">023151235X</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>NEW YORK</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2007</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/simo14136"/>
      <abstract abstract-type="short">
         <p>Rita J. Simon and Rhonda M. Roorda's<italic>In Their Own Voices: Transracial Adoptees Tell Their Stories</italic>shared the experiences of twenty-four black and biracial children who had been adopted into white families in the late 1960s and 70s. The book has since become a standard resource for families and practitioners, and now, in this sequel, we hear from the parents of these remarkable families and learn what it was like for them to raise children across racial and cultural lines.</p>
         <p>These candid interviews shed light on the issues these parents encountered, what part race played during thirty plus years of parenting, what they learned about themselves, and whether they would recommend transracial adoption to others. Combining trenchant historical and political data with absorbing firsthand accounts, Simon and Roorda once more bring an academic and human dimension to the literature on transracial adoption.</p>
      </abstract>
      <counts>
         <page-count count="240"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.3</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>ix</fpage>
                  <abstract abstract-type="extract">
                     <p>
                        <italic>IN THEIR PARENTS’ VOICES</italic>: <italic>Reflections on Raising Transracial Adoptees</italic> is the second of two volumes on black and biracial men and women who were adopted primarily in the 1970s by white parents when most of the children were younger than 2 years old. <italic>In Their Own Voices: Transracial Adoptees Tell Their Stories</italic> (2000) reports the experiences of twelve women and twelve men then aged 22 to 31 (with one male outlier who was 57) and who were transracially adopted. The stories of these transracial adoptees were obtained mostly through telephone interviews, with a few done in person. We found participants</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.4</book-part-id>
                  <title-group>
                     <label>PART 1:</label>
                     <title>THE CHILDREN FROM<italic>IN THEIR OWN VOICES</italic>
                     </title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract abstract-type="extract">
                     <p>TWENTY-FOUR MEN and women participated in <italic>In Their Own Voices</italic>. The twelve women ranged in age from 22 to 28. Eight were adopted when they were 3 months old or younger. The other four were 1 year; 18 months; 2 years; and 6 years old. But the 6-year-old had been living with the family as a foster child since her birth. The respondent who was adopted when she was 2, Rhonda, had been in foster care with an African American couple who may have wanted to adopt her but were not allowed to because of their age. Rhonda had been</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.5</book-part-id>
                  <title-group>
                     <title>AUTHOR’S NOTE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Roorda</surname>
                           <given-names>Rhonda M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>11</fpage>
                  <abstract abstract-type="extract">
                     <p>AFTER A WORKSHOP session on adoption a middle-aged white man and his young black daughter came up to me. He asked if I would listen to his predicament and give him advice about how to handle it. He and his wife had been invited to a family reunion, quite a gala affair. There was just one problem: the man’s brother, who was organizing the reunion, was a racist and had made it quite clear that his niece was not welcome.</p>
                     <p>The troubled man who stood before me, and his wife, both wanted to attend the reunion. Would it be okay,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.6</book-part-id>
                  <title-group>
                     <title>JOHN AND MARIAN PELTON</title>
                  </title-group>
                  <fpage>17</fpage>
                  <abstract abstract-type="extract">
                     <p>John Pelton is 65, and Marian Pelton is 62. They have one biological child, Peter, and three adopted children, Jay, Jessica, and Maria. Jay was 2 when he was adopted, Jessica was 3 weeks old, and Maria was 7 weeks old. The children were raised in rural Vermont. The Peltons are Protestant. John Pelton has a bachelor’s degree and has done graduate work in economics; Marian Pelton has a bachelor’s degree and has done graduate work in education. They are both retired general-store owners. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.7</book-part-id>
                  <title-group>
                     <title>JIM AND ALICE BANDSTRA</title>
                  </title-group>
                  <fpage>26</fpage>
                  <abstract abstract-type="extract">
                     <p>Jim Bandstra is 60, and Alice is 58. They have no biological children and adopted three: Jamie, Andrea, and Daniel. All the children were adopted before they were 6 months old. The children were reared in Silver Spring, Maryland, and later in Big Rapids, Michigan. The Bandstras are members of a Presbyterian/United Church of Christ church. Jim Bandstra has an M.S. and Alice Bandstra an M.Ed.; he is a professor at Ferris State University, where she also teaches. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.8</book-part-id>
                  <title-group>
                     <title>JIM AND KATHY STAPERT</title>
                  </title-group>
                  <fpage>34</fpage>
                  <abstract abstract-type="extract">
                     <p>Jim and Kathy Stapert are both 60. They have no biological children and have seven adopted children: Kim, Michael, Kara, Joe, Melissa, Chris, and Annie. Melissa was 2 when she was adopted, and the others were all adopted before their first birthday. The children were reared in Grand Rapids, Michigan. The Staperts are members of the Christian Reformed Church. Jim has an M.A. in education and is the principal of Grand Rapids Christian school; Kathy has an M.A. in early childhood education and is a kindergarten teacher. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.9</book-part-id>
                  <title-group>
                     <title>RON AND DOROTHY</title>
                  </title-group>
                  <fpage>51</fpage>
                  <abstract abstract-type="extract">
                     <p>Ron is 71 and Dorothy is 66. They have ten biological children and one adopted child. Shecara, who was adopted when she was 4 days old, is the only biracial child in the family. The children were all reared in West Lafayette, Indiana. The family is Roman Catholic. Ron has a master’s degree from Purdue University, and Dorothy completed two years of college at Purdue. Ron is retired and Dorothy is a homemaker and an artisan.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.10</book-part-id>
                  <title-group>
                     <title>PAUL GOFF</title>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract abstract-type="extract">
                     <p>Paul Goff, 64, was widowed in 1995 and remarried two years later. He and his first wife had two biological children, Michael and Adam, and one adopted child, Laurie. She was adopted when she was 1 month old. The Goffs raised their children in Seattle, Sierra Leone, Ivory Coast, Panama, Bangladesh, Washington, D.C., and New Orleans. Paul Goff is Jewish and a physician. He describes his economic status as upper middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.11</book-part-id>
                  <title-group>
                     <title>BARBARA TREMITIERE</title>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract abstract-type="extract">
                     <p>Barbara Tremitiere, who was divorced in 1986, has three biological children, Michelle, Steven, and Scott, and twelve adopted children, Laura, Kristine, Robert, Kevin, Douglas, Marc and Monique (twins), Nicolle, Chantel, Andrew, Daniel, and Michael. The age of the children at adoption ranged from 2 months to 15 years. The children were reared in York, Pennsylvania. Barbara Tremitiere is Protestant. She has a Ph.D. in social work (adoption emphasis) and is a professor at York College of Pennsylvania. She is the founder of the One Another adoption Program in Pennsylvania and is a therapist. She describes her economic status as middle</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.12</book-part-id>
                  <title-group>
                     <title>NORA ANKER</title>
                  </title-group>
                  <fpage>86</fpage>
                  <abstract abstract-type="extract">
                     <p>Nora Anker is 74 and her husband, Harvey Anker, is 75. They have three biological children, Kathy, Mary Ann, and Lynn, and two adopted children, Kim and Rachel. Kim was 5 years old when her adoption was finalized, and Rachel joined the family at 2 days old. The children were reared in south Holland, Illinois. The Ankers are Protestant. Both are high school graduates. Harvey Anker is a carpenter, and Nora Anker is a homemaker. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.13</book-part-id>
                  <title-group>
                     <title>MARJORIE GRAY</title>
                  </title-group>
                  <fpage>96</fpage>
                  <abstract abstract-type="extract">
                     <p>Marjorie Gray is 57 and has two biological children, Christopher and Jean, and one adopted child, Rhonda M. Roorda. Rhonda was 2 years old when she was adopted. All were raised in the Washington, D.C., area. Marjorie Gray was divorced from Rhonda’s father in 1990 and remarried in 1998. She is a member of the Christian Reformed Church. She holds a master’s degree from the University of Maryland and at the time of the interview was volunteering as an English tutor and at her church, where she had been employed as the director of a senior center. She describes her</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.14</book-part-id>
                  <title-group>
                     <title>EDSON AND JUDITH BIGELOW</title>
                  </title-group>
                  <fpage>105</fpage>
                  <abstract abstract-type="extract">
                     <p>Ed and Judy Bigelow are 65 and 66, respectively. They have three biological children, Ed, Brian, and Hilary, and three adopted children, Susan, Keith, and Khai. The children were reared in Essex Junction, Vermont. The Bigelows are members of the American Baptist Church. He has a B.S. and did graduate work in both engineering and business; Ed Bigelow is retired from IBM and the Vermont Air National Guard. Judy Bigelow, a retired school counselor, has a master’s degree and did additional graduate work in elementary education and counseling. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.15</book-part-id>
                  <title-group>
                     <title>AALDERT AND ELISABETH MENNEGA</title>
                  </title-group>
                  <fpage>114</fpage>
                  <abstract abstract-type="extract">
                     <p>Aaldert Mennega is 75 and his wife, Elisabeth, is 73. They have three biological children, Yvonne, Annette, and Michelle, and two adopted children, Rene and Dan. Rene was 3 months old and Dan was 1 month old when they were adopted; Yvonne was nine years old, which meant that she was no longer living at home during the formative years of her siblings. The children were reared in Siou Center, Iowa. The Mennegas are members of the Christian Reformed denomination. Aaldert Mennega has a Ph.D. in biology and retired as biology professor at Dordt College; Elisabeth Mennega is a retired</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.16</book-part-id>
                  <title-group>
                     <title>RIKK LARSEN</title>
                  </title-group>
                  <fpage>122</fpage>
                  <abstract abstract-type="extract">
                     <p>Rikk Larsen is 58. He is the father of four biological children and six adopted children. Tage is African American, Jens is Native American, and the other four are from Vietnam, Cambodia, and El Salvador. At the time of their adoption Tage, Jens, Peik, and Siri were less than a year old, Kari was between 3 and 5 years old, and Christian was 7. The family lived in Cambridge, Massachusetts. Rikk Larsen was divorced from the mother of his children in 2002. He received his M.B.A. from the Harvard Business School and works as a family mediator. He reports no</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.17</book-part-id>
                  <title-group>
                     <title>CHARLES AND PAM ADAMS</title>
                  </title-group>
                  <fpage>136</fpage>
                  <abstract abstract-type="extract">
                     <p>Charles Adams is 57, and Pam is 55. They have two biological children, Charles and Michael, and one adopted child, David. David was 3 months old when he was adopted. The children were reared in Sioux Center, Iowa, briefly in Connecticut, and in Wanaque, New Jersey. The Adamses are members of the Christian Reformed Church. Both Charles and Pam have Ph.D.’s and are professors at Dordt College in Iowa. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.18</book-part-id>
                  <title-group>
                     <title>DAVID AND LOLA HIMROD</title>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract abstract-type="extract">
                     <p>David Himrod is 66, and Lola is 61. They have no biological children and one adopted child, Seth. Seth was 4½ months old at the time of his adoption. Their home is in Evanston, Illinois, and they are Episcopalian. David Himrod has a Ph.D. and is a retired reference librarian. Lola Himrod’s Ph.D. is in clinical social work, and she is a part-time school social worker. They describe their economic status as upper middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.19</book-part-id>
                  <title-group>
                     <title>KEN AND JEAN</title>
                  </title-group>
                  <fpage>158</fpage>
                  <abstract abstract-type="extract">
                     <p>Ken is 64 and Jean is 63. They have no biological children and have adopted twins, Ned and Tonya, who were 6 weeks old when they were adopted. The children were reared in Grand Rapids, Michigan. Jean and Ken are members of the Christian Reformed Church. Ken has a B.A. in education and a master’s in communication arts and sciences and was a high school teacher; he also spent half his career teaching in the prison system. Jean has a B.A. and did graduate work in education; she taught elementary and middle school. She now works with refugees. They describe</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.20</book-part-id>
                  <title-group>
                     <title>WINNIE</title>
                  </title-group>
                  <fpage>172</fpage>
                  <abstract abstract-type="extract">
                     <p>Winnie is 64 and divorced. She has three biological children, Stephen, Catherine, and Anne, and one adopted child, Peter. Peter was less than a year old when he was adopted. The children were raised near Ithaca, New york. Winnie, a Unitarian, has a Ph.D. in French literature and is a library assistant. She describes her economic status as working class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.21</book-part-id>
                  <title-group>
                     <title>RODNEY AND JOYCE PERRY</title>
                  </title-group>
                  <fpage>183</fpage>
                  <abstract abstract-type="extract">
                     <p>Rodney Perry is 60, and Joyce Perry is 61. They have three biological children, Seth, Amanda, and Nathan, and one adopted child, Britton. Britton was about 3 months old when he was adopted. The children were raised primarily in Rochester, New York. The Perrys are Quakers. Rodney Perry has an M.A. in library science, and Joyce Perry has a B.A. in English literature. They are both self-employed. They describe their economic status as middle class.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.22</book-part-id>
                  <title-group>
                     <label>PART 3:</label>
                     <title>CONCLUSION</title>
                  </title-group>
                  <fpage>203</fpage>
                  <abstract abstract-type="extract">
                     <p>OF THE SIXTEEN PARENTS we interviewed—three mothers, two fathers, and eleven couples— all spoke warmly and lovingly of their adopted children. The Peltons are very grateful that Jessica and their other biracial daughter have found their way back to the black community, the mixed communities, and the white community. They are close to all their children, two of whom are living with them: the other biracial daughter and a birth son. Jessica lives thirty to forty miles away.</p>
                     <p>Alice Bandstra, Andrea’s mother, talked especially warmly of her relationship with her daughter. While both parents are close to all their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.23</book-part-id>
                  <title-group>
                     <title>AFTERWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Roorda</surname>
                           <given-names>Rhonda M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>209</fpage>
                  <abstract abstract-type="extract">
                     <p>THE OTHER DAY I was waiting in the checkout line of a large supermarket in Brighton, Michigan. Behind me were a white couple and their gregarious 3-year-old African American son. As I stood there drawing in the images and the sounds of the little boy—his laughter, his words, and his tears, as his parents were instructing him not to pull things off the racks—thoughts about the many transracially adopted children I have met through the years flooded my mind, including thoughts about my own childhood as a black transracial adoptee. And ever-present in my consciousness were the voices</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.24</book-part-id>
                  <title-group>
                     <title>POSTSCRIPT</title>
                  </title-group>
                  <fpage>215</fpage>
                  <abstract abstract-type="extract">
                     <p>SO WHAT are the adult adoptees of <italic>In Their Own Voices</italic> whose parents did not appear in this book doing now?</p>
                     <p>Ronald Wilson, who used the pseudonym Taalib in the first volume, has decided to make his identity known. He is the presiding judge of a municipal court in southern Arizona—the youngest presiding judge in Arizona and the first African American to serve as chief justice in that state. He recently was named Man of the Year by the <italic>Tucson Business Edge,</italic> and he is the proud recipient of a 2006 Community Service Award from the Federal Bureau of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.7312/simo14136.25</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Roorda</surname>
                           <given-names>Rhonda M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>217</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
