<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">ecology</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100138</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Ecology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Ecological Society of America</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00129658</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19399170</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43494681</article-id>
         <title-group>
            <article-title>A late-Quaternary perspective on atmospheric pCO₂, climate, and fire as drivers of C₄-grass abundance</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Michael A.</given-names>
                  <surname>Urban</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>David M.</given-names>
                  <surname>Nelson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>F. Alayne</given-names>
                  <surname>Street-Perrott</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dirk</given-names>
                  <surname>Verschuren</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Feng Sheng</given-names>
                  <surname>Hu</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2015</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">96</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40138635</issue-id>
         <fpage>642</fpage>
         <lpage>653</lpage>
         <permissions>
            <copyright-statement>Copyright © 2015 Ecological Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43494681"/>
         <abstract>
            <p>Various environmental factors, including atmospheric CO₂ (pCO₂), regional climate, and fire, have been invoked as primary drivers of long-term variation in C₄ grass abundance. Evaluating these hypotheses has been difficult because available paleorecords often lack information on past C₄ grass abundance or potential environmental drivers. We analyzed carbon isotope ratios (δ¹³C) of individual grains of grass pollen in the sediments of two East African lakes to infer changes in the relative abundance of C₃ vs. C₄ grasses during the past 25 000 years. Results were compared with concurrent changes in pCO₂, temperature, moisture balance, and fire activity. Our grass-pollen δ¹₃C analysis reveals a dynamic history of grass-dominated vegetation in equatorial East Africa: C₄ grasses have not consistently dominated lowland areas, and high-elevation grasses have not always been predominantly C₃. On millennial timescales, C₄ grass abundance does not correlate with charcoal influx at either site, suggesting that fire was not a major proximate control of the competitive balance between C₃ and C₄ grasses. Above the present-day treeline on Mt. Kenya, C₄ grass abundance declined from an average of ~90% during the glacial period to less than ~60% throughout the Holocene, coincident with increases in pCO ₂ and temperature, and shifts in moisture balance. In the lowland savanna southeast of Mt. Kilimanjaro, C₄ grass abundance showed no such directional trend, but fluctuated markedly in association with variation in rainfall amount and seasonal-drought severity. These results underscore spatiotemporal variability in the relative influence of pCO₂ and climate on the interplay of C₃ and C₄ grasses and shed light on an emerging conceptual model regarding the expansion of C₄-dominated grasslands in Earth's history. They also suggest that future changes in the C₃/C₄ composition of grass-dominated ecosystems will likely exhibit striking spatiotemporal variability as a result of varying combinations of environmental controls.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d946e189a1310">
            <mixed-citation id="d946e193" publication-type="other">
Barker, P. A., E. R. Hurrell, M. J. Leng, C. Wolff, C. Cocquyt,
H. J. Sloane, and D. Verschuren. 2011. Seasonality in
equatorial climate over the last 25 k.y. revealed by oxygen
isotope records from Mount Kilimanjaro. Geology 39:1111-
1114.</mixed-citation>
         </ref>
         <ref id="d946e212a1310">
            <mixed-citation id="d946e216" publication-type="other">
Barker, P. A., F. A. Street-Perrott, M. J. Leng, P. B.
Greenwood, D. L. Swain, R. A. Perrott, R. J. Telford, and
K. J. Ficken. 2001. A 14,000-year oxygen isotope record from
diatom silica in two alpine lakes on Mt. Kenya. Science 292:
2307-2310.</mixed-citation>
         </ref>
         <ref id="d946e235a1310">
            <mixed-citation id="d946e239" publication-type="other">
Blaauw, M., B. van Geel, I. Kristen, B. Plessen, A. Lyaruu,
D. R. Engstrom, J. van der Plicht, and D. Verschuren. 2011.
High-resolution ¹⁴C dating of a 25,000-year lake-sediment
record from equatorial East Africa. Quaternary Science
Reviews 30:3043-3059.</mixed-citation>
         </ref>
         <ref id="d946e258a1310">
            <mixed-citation id="d946e262" publication-type="other">
Bond, W. J., and G. F. Midgley. 2012. Carbon dioxide and the
uneasy interactions of trees and savannah grasses. Philo-
sophical Transactions of the Royal Society B 367:601-612.</mixed-citation>
         </ref>
         <ref id="d946e276a1310">
            <mixed-citation id="d946e280" publication-type="other">
Bond, W. J., F. I. Woodward, and G. F. Midgley. 2005. The
global distribution of ecosystems in a world without fire. New
Phytologist 165:525-537.</mixed-citation>
         </ref>
         <ref id="d946e293a1310">
            <mixed-citation id="d946e297" publication-type="other">
Boom, A., R. Marchant, H. Hooghiemstra, and J. S. Sinninghe
Damsté. 2002. CO₂-and temperature-controlled altitudinal
shifts of C₄-and C₃-dominated grasslands allow reconstruc-
tion of palaeoatmospheric pCO₂. Palaeogeography, Palaeo-
climatology, Palaeoecology 177:151-168.</mixed-citation>
         </ref>
         <ref id="d946e316a1310">
            <mixed-citation id="d946e320" publication-type="other">
Castañeda, I. S., J. P. Werne, and T. C. Johnson. 2007. Wet and
arid phases in the southeast African tropics since the Last
Glacial Maximum. Geology 35:823-826.</mixed-citation>
         </ref>
         <ref id="d946e333a1310">
            <mixed-citation id="d946e337" publication-type="other">
Ceding, T. E. 1999. Paleorecords of C₄ plants and ecosystems.
Pages 445-469 in R. F. Sage and R. K. Monson, editors. C4
plant biology. Academic Press, San Diego, California,
USA.</mixed-citation>
         </ref>
         <ref id="d946e353a1310">
            <mixed-citation id="d946e357" publication-type="other">
Coe, M. J. 1967. The ecology of the alpine zone of Mount
Kenya. Dr. W. Junk Publishers, The Hague, Netherlands.</mixed-citation>
         </ref>
         <ref id="d946e367a1310">
            <mixed-citation id="d946e371" publication-type="other">
Coetzee, J. A. 1967. Pollen analytical studies in East and
Southern Africa. Palaeoecology of Africa 3:1-146.</mixed-citation>
         </ref>
         <ref id="d946e382a1310">
            <mixed-citation id="d946e386" publication-type="other">
Di Gregorio, A. 2002. Land-cover map of Kenya. FAO
Africover, Nairobi, Kenya.</mixed-citation>
         </ref>
         <ref id="d946e396a1310">
            <mixed-citation id="d946e400" publication-type="other">
Edwards, E. J., C. P. Osborne, C. A. E. Strömberg, S. A. Smith,
and C. G. Consortium. 2010. The origins of C4 grasslands:
integrating evolutionary and ecosystem science. Science 328:
587-591.</mixed-citation>
         </ref>
         <ref id="d946e416a1310">
            <mixed-citation id="d946e420" publication-type="other">
Ehleringer, J. R. 1978. Implications of quantum yield differ-
ences on the distribution of C₃ and C₄ grasses. Oecologia 31:
255-267.</mixed-citation>
         </ref>
         <ref id="d946e433a1310">
            <mixed-citation id="d946e437" publication-type="other">
Ehleringer, J. R., T. E. Cerling, and B. R. Helliker. 1997. C₄
photosynthesis, atmospheric CO₂ and climate. Oecologia
112:285-299.</mixed-citation>
         </ref>
         <ref id="d946e450a1310">
            <mixed-citation id="d946e454" publication-type="other">
Faegri, K., J. Iverson, P. E. Kaland, and K. Krzywinski. 1989.
Textbook of pollen analysis. Wiley, New York, New York,
USA.</mixed-citation>
         </ref>
         <ref id="d946e467a1310">
            <mixed-citation id="d946e471" publication-type="other">
Ficken, K. J., M. J. Wooller, D. L. Swain, F. A. Street-Perrott,
and G. Eglinton. 2002. Reconstruction of a subalpine grass-
dominated ecosystem, Lake Rutundu, Mount Kenya: a novel
multi-proxy approach. Palaeogeography, Palaeoclimatology,
Palaeoecology 177:137-149.</mixed-citation>
         </ref>
         <ref id="d946e491a1310">
            <mixed-citation id="d946e495" publication-type="other">
Fox, D. L., and P. L. Koch. 2004. Carbon and oxygen isotopie
variability in Neogene paleosol carbonates: constraints on
the evolution of the C₄-grasslands of the Great Plains, USA.
Palaeogeography, Palaeoclimatology, Palaeoecology 207:
305-329.</mixed-citation>
         </ref>
         <ref id="d946e514a1310">
            <mixed-citation id="d946e518" publication-type="other">
Funk, C., M. D. Dettinger, J. C. Michaelsen, J. P. Verdin,
M. E. Brown, M. Barlow, and A. Hoell. 2008. Warming of
the Indian Ocean threatens eastern and southern African
food security but could be mitigated by agricultural
development. Proceedings of the National Academy of
Sciences USA 105:11081-11086.</mixed-citation>
         </ref>
         <ref id="d946e541a1310">
            <mixed-citation id="d946e545" publication-type="other">
Gibson, D. J. 2009. Grasses and grassland ecology. Oxford
University Press, New York, New York, USA.</mixed-citation>
         </ref>
         <ref id="d946e555a1310">
            <mixed-citation id="d946e559" publication-type="other">
Hemp, A. 2006. Vegetation of Kilimanjaro: hidden endemics
and missing bamboo. African Journal of Ecology 44:305-
328.</mixed-citation>
         </ref>
         <ref id="d946e572a1310">
            <mixed-citation id="d946e576" publication-type="other">
Higgins, S. I., and S. Scheiter. 2012. Atmospheric CO₂ forces
abrupt vegetation shifts locally, but not globally. Nature 488:
209-212.</mixed-citation>
         </ref>
         <ref id="d946e589a1310">
            <mixed-citation id="d946e593" publication-type="other">
Hoetzel, S., L. Dupont, E. Schefuß, F. Rommerskirchen, and
G. Wefer. 2013. The role of fire in Miocene to Pliocene C₄
grassland and ecosystem evolution. Nature Geoscience 6:
1027-1030.</mixed-citation>
         </ref>
         <ref id="d946e610a1310">
            <mixed-citation id="d946e614" publication-type="other">
Huang, Y., S. C. Clemens, W. G. Liu, Y. Wang, and W. L.
Prell. 2007. Large-scale hydrological change drove the late
Miocene C₄ plant expansion in the Himalayan foreland and
Arabian Peninsula. Geology 35:531-534.</mixed-citation>
         </ref>
         <ref id="d946e630a1310">
            <mixed-citation id="d946e634" publication-type="other">
Huang, Y., B. Shuman, Y. Wang, T. Webb, E. C. Grimm, and
G. L. Jacobson. 2006. Climatic and environmental controls
on the variation of C₃ and C₄ plant abundances in central
Florida for the past 62,000 years. Palaeogeography, Palaeo-
climatology, Palaeoecology 237:428-435.</mixed-citation>
         </ref>
         <ref id="d946e653a1310">
            <mixed-citation id="d946e657" publication-type="other">
Huang, Y., F. A. Street-Perrott, S. E. Metcalfe, M. Brenner, M.
Moreland, and K. H. Freeman. 2001. Climate change as the
dominant control on glacial-interglacial variations in C₃ and
C₄ plant abundance. Science 293:1647-1651.</mixed-citation>
         </ref>
         <ref id="d946e673a1310">
            <mixed-citation id="d946e677" publication-type="other">
Hulme, M., R. Doherty, T. Ngara, M. New, and D. Lister.
2001. African climate change: 1900-2100. Climate Research
17:145-168.</mixed-citation>
         </ref>
         <ref id="d946e690a1310">
            <mixed-citation id="d946e694" publication-type="other">
IPCC. 2014. Climate change 2014: mitigation of climate change.
Contribution of working group III to the fifth assessment
report of the Intergovernmental Panel on Climate Change. O.
Edenhofer et al., editors. Cambridge University Press,
Cambridge, UK.</mixed-citation>
         </ref>
         <ref id="d946e713a1310">
            <mixed-citation id="d946e717" publication-type="other">
Jacobs, B. F., J. D. Kingston, and L. L. Jacobs. 1999. The
origin of grass-dominated ecosystems. Annals of the Mis-
souri Botanical Garden 86:590-643.</mixed-citation>
         </ref>
         <ref id="d946e731a1310">
            <mixed-citation id="d946e735" publication-type="other">
Keeley, J. E., and P. W. Rundel. 2005. Fire and the Miocene
expansion of C₄ grasslands. Ecology Letters 8:683-690.</mixed-citation>
         </ref>
         <ref id="d946e745a1310">
            <mixed-citation id="d946e749" publication-type="other">
Lehmann, C. E. R., S. A. Archibald, W. A. Hoffmann, and
W. J. Bond. 2011. Deciphering the distribution of the
savanna biome. New Phytologist 191:197-209.</mixed-citation>
         </ref>
         <ref id="d946e762a1310">
            <mixed-citation id="d946e766" publication-type="other">
Lemieux-Dudon, B., E. Blayo, J. R. Petit, C. Waelbroeck, A.
Svensson, C. Ritz, J. M. Barnola, B. M. Narcisi, and F.
Parrenin. 2010. Consistent dating for Antarctic and Green-
land ice cores. Quaternary Science Reviews 29:8-20.</mixed-citation>
         </ref>
         <ref id="d946e782a1310">
            <mixed-citation id="d946e786" publication-type="other">
Livingstone, D. A., and W. D. Clayton. 1980. An altitudinal
cline in tropical African grass floras and its paleoecological
significance. Quaternary Research 13:392-402.</mixed-citation>
         </ref>
         <ref id="d946e799a1310">
            <mixed-citation id="d946e803" publication-type="other">
Lloyd, J., and G. D. Farquhar. 1994. ¹³C discrimination during
CO₂ assimilation by the terrestrial biosphere. Oecologia 99:
201-215.</mixed-citation>
         </ref>
         <ref id="d946e816a1310">
            <mixed-citation id="d946e820" publication-type="other">
Long, S. P. 1999. Environmental responses. Pages 215-249 in
R. F. Sage and R. K. Monson, editors. C₄ plant biology.
Academic Press, San Diego, California, USA.</mixed-citation>
         </ref>
         <ref id="d946e834a1310">
            <mixed-citation id="d946e838" publication-type="other">
Loomis, S. E., J. M. Russell, B. Ladd, F. A. Street-Perrott, and
J. S. Sinninghe Damsté. 2012. Calibration and application of
the branched GDGT temperature proxy on East African lake
sediments. Earth and Planetary Science Letters 357:277-288.</mixed-citation>
         </ref>
         <ref id="d946e854a1310">
            <mixed-citation id="d946e858" publication-type="other">
Loulergue, L., F. Parrenin, T. Blunier, J. M. Barnola, R.
Spahni, A. Schilt, G. Raisbeck, and J. Chappellaz. 2007. New
constraints on the gas age-ice age difference along the EPICA
ice cores, 0-50 kyr. Climate of the Past 3:527-540.</mixed-citation>
         </ref>
         <ref id="d946e874a1310">
            <mixed-citation id="d946e878" publication-type="other">
Mander, L., M. Li, W. Mio, C. C. Fowlkes, and S. W.
Punyasena. 2013. Classification of grass pollen through the
quantitative analysis of surface ornamentation and texture.
Proceedings of the Royal Society B 280(1770):20131905.</mixed-citation>
         </ref>
         <ref id="d946e894a1310">
            <mixed-citation id="d946e898" publication-type="other">
McNaughton, S. J., and N. J. Georgiadis. 1986. Ecology of
African grazing and browsing mammals. Annual Review of
Ecology and Systematics 17:39-65.</mixed-citation>
         </ref>
         <ref id="d946e911a1310">
            <mixed-citation id="d946e915" publication-type="other">
Monnin, E., A. Indermuhle, A. Dallenbach, J. Fluckiger, B.
Stauffer, T. F. Stocker, D. Raynaud, and J. M. Barnola.
2001. Atmospheric C02 concentrations over the last glacial
termination. Science 291:112-114.</mixed-citation>
         </ref>
         <ref id="d946e931a1310">
            <mixed-citation id="d946e935" publication-type="other">
Morgan, J. A., D. R. LeCain, E. Pendall, D. M. Blumenthal,
B. A. Kimball, Y. Carrillo, D. G. Williams, J. Heisler-White,
F. A. Dijkstra, and M. West. 2011. C₄ grasses prosper as
carbon dioxide eliminates desiccation in warmed semi-arid
grassland. Nature 476:202-205.</mixed-citation>
         </ref>
         <ref id="d946e955a1310">
            <mixed-citation id="d946e959" publication-type="other">
Nelson, D. M., F. S. Hu, and R. H. Michener. 2006. Stable-
carbon isotope composition of Poaceae pollen: an assessment
for reconstructing C₃ and C₄ grass abundance. Holocene 16:
819-825.</mixed-citation>
         </ref>
         <ref id="d946e975a1310">
            <mixed-citation id="d946e979" publication-type="other">
Nelson, D. M., F. S. Hu, J. Mikucki, J. Tian, and A. Pearson.
2007. Carbon isotopie analysis of individual pollen grains
from C₃ and C₄ grasses using a spooling wire micro-
combustion interface. Geochimica et Cosmochimica Acta
71:4005-4014.</mixed-citation>
         </ref>
         <ref id="d946e998a1310">
            <mixed-citation id="d946e1002" publication-type="other">
Nelson, D. M., F. S. Hu, D. R. Scholes, N. Joshi, and A.
Pearson. 2008. Using SPIRAL (Single Pollen Isotope Ratio
AnaLysis) to estimate C₃- and C₄- grass abundance in the
paleorecord. Earth and Planetary Science Letters 269:11-16.</mixed-citation>
         </ref>
         <ref id="d946e1018a1310">
            <mixed-citation id="d946e1022" publication-type="other">
Nelson, D. M., D. Verschuren, M. A. Urban, and F. S. Hu.
2012. Long-term variability and rainfall control of savanna
fire regimes in equatorial East Africa. Global Change Biology
18:3160-3170.</mixed-citation>
         </ref>
         <ref id="d946e1038a1310">
            <mixed-citation id="d946e1042" publication-type="other">
Osborne, C. P., and D. J. Beerling. 2006. Nature's green
revolution: the remarkable evolutionary rise of C4 plants.
Philosophical Transactions of the Royal Society B 361:173-
194.</mixed-citation>
         </ref>
         <ref id="d946e1058a1310">
            <mixed-citation id="d946e1062" publication-type="other">
Pedersen, M. W., A. Ginolhac, L. Orlando, J. Olsen, K.
Andersen, J. Holm, S. Funder, E. Willerslev, and K. H.
Kjaeer. 2013. A comparative study of ancient environmental
DNA to pollen and macrofossils from lake sediments reveals
taxonomie overlap and additional plant taxa. Quaternary
Science Reviews 75:161-168.</mixed-citation>
         </ref>
         <ref id="d946e1086a1310">
            <mixed-citation id="d946e1090" publication-type="other">
R Development Core Team. 2012. R: a language and
environment for statistical computing. R Foundation for
Statistical Computing, Vienna, Austria. http://www.
R-proiect.org</mixed-citation>
         </ref>
         <ref id="d946e1106a1310">
            <mixed-citation id="d946e1110" publication-type="other">
Ripley, B., G. Donald, C. P. Osborne, T. Abraham, and T.
Martin. 2010. Experimental investigation of fire ecology in
the C₃ and C₄ subspecies of Alloteropsis semialata. Journal of
Ecology 98:1196-1203.</mixed-citation>
         </ref>
         <ref id="d946e1126a1310">
            <mixed-citation id="d946e1130" publication-type="other">
Sepulchre, P., G. Ramstein, F. Fluteau, M. Schuster, J. J.
Tiercelin, and M. Brunet. 2006. Tectonic uplift and Eastern
Africa aridification. Science 313:1419-1423.</mixed-citation>
         </ref>
         <ref id="d946e1143a1310">
            <mixed-citation id="d946e1147" publication-type="other">
Shongwe, M. E., G. J. van Oldenborgh, B. van den Hurk, and
M. van Aalst. 2011. Projected changes in mean and extreme
precipitation in Africa under global warming. Part II: East
Africa. Journal of Climate 24:3718-3733.</mixed-citation>
         </ref>
         <ref id="d946e1163a1310">
            <mixed-citation id="d946e1167" publication-type="other">
Sinninghe Damsté, J. S., J. Ossebaar, S. Schouten, and D.
Verschuren. 2012. Distribution of tetraether lipids in the 25-
ka sedimentary record of Lake Challa: extracting reliable
TEX86 and MBT/CBT palaeotemperatures from an equato-
rial African lake. Quaternary Science Reviews 50:43-54.</mixed-citation>
         </ref>
         <ref id="d946e1186a1310">
            <mixed-citation id="d946e1190" publication-type="other">
Sinninghe Damsté, J. S., D. Verschuren, J. Ossebaar, J.
Blokker, R. van Houten, M. T. J. van der Meer, B. Plessen,
and S. Schouten. 2011. A 25,000-year record of climate-
induced changes in lowland vegetation of eastern equatorial
Africa revealed by the stable carbon-isotopic composition of
fossil plant leaf waxes. Earth and Planetary Science Letters
302:236-246.</mixed-citation>
         </ref>
         <ref id="d946e1217a1310">
            <mixed-citation id="d946e1221" publication-type="other">
Staver, A. C., S. Archibald, and S. Levin. 2011. Tree cover in
sub-Saharan Africa: rainfall and fire constrain forest and
savanna as alternative stable states. Ecology 92:1063-1072.</mixed-citation>
         </ref>
         <ref id="d946e1234a1310">
            <mixed-citation id="d946e1238" publication-type="other">
Street-Perrott, F. A., P. A. Barker, M. J. Lengļ H. J. Sloane,
M. J. Wooller, K. J. Ficken, and D. L. Swain. 2008. Towards
an understanding of late Quaternary variations in the
continental biogeochemical cycle of silicon: multi-isotope
and sediment-flux data for Lake Rutundu, Mt Kenya, East
Africa, since 38 ka BP. Journal of Quaternary Science 23:
375-387.</mixed-citation>
         </ref>
         <ref id="d946e1264a1310">
            <mixed-citation id="d946e1268" publication-type="other">
Street-Perrott, F. A., P. A. Barker, D. L. Swain, K. J. Ficken,
M. J. Wooller, D. O. Olago, and Y. Huang. 2007. Late
Quaternary changes in ecosystems and carbon cycling on Mt.
Kenya, East Africa: a landscape-ecological perspective based
on multi-proxy lake-sediment influxes. Quaternary Science
Reviews 26:1838-1860.</mixed-citation>
         </ref>
         <ref id="d946e1291a1310">
            <mixed-citation id="d946e1295" publication-type="other">
Street-Perrott, F. A., Y. Huang, A. Perrott, G. Eglinton, P.
Barker, L. Khelifa, D. D. Harkness, and D. Olago. 1997. The
impact of lower atmospheric CO₂ on tropical mountain
ecosystems. Science 278:1422-1426.</mixed-citation>
         </ref>
         <ref id="d946e1311a1310">
            <mixed-citation id="d946e1315" publication-type="other">
Strömberg, C. A. E. 2011. Evolution of grasses and grassland
ecosystems. Annual Review of Earth and Planetary Sciences
39:517-544.</mixed-citation>
         </ref>
         <ref id="d946e1328a1310">
            <mixed-citation id="d946e1332" publication-type="other">
Strömberg, C. A. E., and F. A. Mclnerney. 2011. The Neogene
transition from C₃ and C₄ grasslands in North America:
assemblage analysis of fossil phytoliths. Paleobiology 37:50-
71.</mixed-citation>
         </ref>
         <ref id="d946e1349a1310">
            <mixed-citation id="d946e1353" publication-type="other">
Taylor, S. H., B. S. Ripley, T. Martin, L. A. De-Wet, F. I.
Woodward, and C. P. Osborne. 2014. Physiological advan-
tages of C₄ grasses in the field: a comparative experiment
demonstrating the importance of drought. Global Change
Biology 20:1992-2003.</mixed-citation>
         </ref>
         <ref id="d946e1372a1310">
            <mixed-citation id="d946e1376" publication-type="other">
Thompson, B. W. 1966. The mean annual rainfall of Mt.
Kenya. Weather 21:48-49.</mixed-citation>
         </ref>
         <ref id="d946e1386a1310">
            <mixed-citation id="d946e1390" publication-type="other">
Tian, J., D. M. Nelson, and F. S. Hu. 2011. How well do
sediment indicators record past climate? An evaluation using
annually laminated sediments. Journal of Paleolimnology 45:
73-84.</mixed-citation>
         </ref>
         <ref id="d946e1406a1310">
            <mixed-citation id="d946e1410" publication-type="other">
Tierney, J. E., J. M. Russell, and Y. Huang. 2010. A molecular
perspective on late Quaternary climate and vegetation change
in the Lake Tanganyika basin, East Africa. Quaternary
Science Reviews 29:787-800.</mixed-citation>
         </ref>
         <ref id="d946e1426a1310">
            <mixed-citation id="d946e1430" publication-type="other">
Tierney, J. E., J. M. Russell, Y. S. Huang, J. S. Sinninghe
Damsté E. C. Hopmans, and A. S. Cohen. 2008. Northern
hemisphere controls on tropical southeast African climate
during the past 60,000 years. Science 322:252-255.</mixed-citation>
         </ref>
         <ref id="d946e1446a1310">
            <mixed-citation id="d946e1450" publication-type="other">
Tieszen, L. L., M. M. Senyimba, S. K. Imbamba, and J. H.
Troughton. 1979. The distribution of C₃ and C₄ grasses and
carbon isotope discrimination along an altitudinal and
moisture gradient in Kenya. Oecologia 37:337-350.</mixed-citation>
         </ref>
         <ref id="d946e1467a1310">
            <mixed-citation id="d946e1471" publication-type="other">
Urban, M. A., D. M. Nelson, R. Kelley, T. Ibriham, M. Dietze,
A. Pearson, and F. S. Hu. 2013. A hierarchical Bayesian
approach to the classification of C₃ and C₄ grass pollen based
on SPIRAL data. Geochimica et Cosmochimica Acta 121:
168-176.</mixed-citation>
         </ref>
         <ref id="d946e1490a1310">
            <mixed-citation id="d946e1494" publication-type="other">
van Geel, B., V. Gelorini, A. Lyaruu, A. Aptroot, S. M.
Rucina, R. Marchant, J. S. Sinninghe Damsté, and D.
Verschuren. 2011. Diversity and ecology of tropical African
fungal spores from a 25,000-year palaeoenvironmental
record in southeastern Kenya. Review of Palaeobontany
and Palynology 164:174-190.</mixed-citation>
         </ref>
         <ref id="d946e1517a1310">
            <mixed-citation id="d946e1521" publication-type="other">
Verschuren, D., J. S. Sinninghe Damsté, J. Moernaut, I.
Kristen, M. Blaauw, M. Fagot, G. H. Haug, and Challacea
Project members. 2009. Half-precessional dynamics of
monsoon rainfall near the East African equator. Nature
462:637-641.</mixed-citation>
         </ref>
         <ref id="d946e1540a1310">
            <mixed-citation id="d946e1544" publication-type="other">
Vincens, A., G. Buchet, D. Williamson, and M. Taieb. 2005. A
23,000 yr pollen record from Lake Rukwa (8°S, SW
Tanzania): new data on vegetation dynamics and climate in
Central Eastern Africa. Review of Palaeobotany and
Palynology 137:147-162.</mixed-citation>
         </ref>
         <ref id="d946e1563a1310">
            <mixed-citation id="d946e1567" publication-type="other">
White, F. 1983. The vegetation of Africa. Natural Resources
Research, UNESCO, Paris, France.</mixed-citation>
         </ref>
         <ref id="d946e1577a1310">
            <mixed-citation id="d946e1581" publication-type="other">
Williams, A. P., and C. Funk. 2011. A westward extension of
the warm pool leads to a westward extension of the Walker
circulation, drying eastern Africa. Climate Dynamics 37:
2417-2435.</mixed-citation>
         </ref>
         <ref id="d946e1598a1310">
            <mixed-citation id="d946e1602" publication-type="other">
Wooller, M. J., D. L. Swain, K. J. Ficken, A. D. Q. Agnew,
F. A. Street-Perrott, and G. Eglinton. 2003. Late Quaternary
vegetation changes around Lake Rutundu, Mount Kenya,
East Africa: evidence from grass cuticles, pollen and stable
carbon isotopes. Journal of Quaternary Science 18:3-15.</mixed-citation>
         </ref>
         <ref id="d946e1621a1310">
            <mixed-citation id="d946e1625" publication-type="other">
Wooller, M. J., D. L. Swain, and F. A. Street-Perrott. 2001. An
altitudinal and stable carbon isotope survey of C₃ and C₄
graminoids on Mount Kenya. Journal of East African
Natural History 90:69-85.</mixed-citation>
         </ref>
         <ref id="d946e1641a1310">
            <mixed-citation id="d946e1645" publication-type="other">
Zeileis, A., C. Kleiber, W. Kramer, and K. Hornik. 2003.
Testing and dating of structural changes in practice.
Computational Statistics and Data Analysis 44:109-123.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
