<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">irisstudinteaffa</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000059</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Irish Studies in International Affairs</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Royal Irish Academy</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03321460</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">20090072</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25469817</article-id>
         <article-categories>
            <subj-group>
               <subject>The Rise of Asia in International Affairs</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Reorienting East Asia: Ten Years after the Financial Crisis</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Meredith Jung-En</given-names>
                  <surname>Woo</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">18</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25469808</issue-id>
         <fpage>81</fpage>
         <lpage>90</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 Royal Irish Academy</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25469817"/>
         <abstract>
            <p>This year marks the tenth anniversary of the Asian financial crisis of 1997. This paper argues that the financial crisis marked the passing of the post-war order in East Asia. It was an order in which the United States provided political support for regime stability, Japan provided financial wherewithal for investment and enormous entrepreneurial talent had been provided by the overseas Chinese in Southeast Asia-and all of this in the context of the sequestration of the People's Republic of China. By the end of the last century, however, all of that had changed. In the aftermath of the Asian crisis, the influence of the United States and Japan has declined markedly, only to be replaced by the rising influence of China. The paper discusses the post-1997 East Asia by looking at the changing political order in the area, new patterns of trade and investment as well as demography-the new and old overseas Chinese and their relationship to their 'homeland'. Finally, the paper discusses how the economies in the region-Korea, for instance-are adjusting to the new Chinese regional order.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1030e123a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1030e130" publication-type="other">
profile of Mahathir available at http://news.bbc.co.uk/2hi/asia-pacific/2059518.stm (17
September 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e140a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1030e147" publication-type="other">
Shahid Yusuf and Joseph Stiglitz, Rethinking the East Asian miracle, World Bank Publication (New
York, 2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e157a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1030e164" publication-type="other">
Charles Kindleberger, Manias, panics and crashes: a history of financial crisis (New York, 2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e171a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1030e178" publication-type="other">
Jack London, 'The unparalleled invasion', in Earle Labor, Robert C. Leitz III and I. Milo Shepard
(eds), The complete stories of Jack London, vol. 2 (Stanford, 1993), 1234-46.</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e189a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1030e196" publication-type="other">
London, The unparalleled invasion', 1238.</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e203a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1030e210" publication-type="other">
Benedict Anderson, 'From miracle to crash', London Review of Books 20 (8) (27 April 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e217a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1030e224" publication-type="other">
World Bank, The East Asian miracle: economic growth and public policy (New York, 1993).</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e231a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1030e238" publication-type="other">
Dan Chirot and Anthony Reid (eds), Essential outsiders: Chinese and Jews in the modern
transformation of Southeast Asia and Central Europe (Seattle, 1997).</mixed-citation>
            </p>
         </fn>
         <fn id="d1030e248a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1030e255" publication-type="other">
Office of the United States trade representative, 'United
States and the Republic of Korea sign landmark free trade agreement', Press release, 30 June 2007,
available at http://www.ustr.gov/Document_Library/Press_Releases/2007/June/United_States_
the_Republic_of_Korea_Sign_Lmark_Free_Trade_Agreement.html (29 August 2007).</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
