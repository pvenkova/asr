<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">envicons</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020266</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Environmental Conservation</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>CAMBRIDGE UNIVERSITY PRESS</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03768929</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14694387</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">44520730</article-id>
         <title-group>
            <article-title>Market profiles and trade in medicinal plants in the Lowveld, South Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>J.</given-names>
                  <surname>BOTHA</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>E. T. F.</given-names>
                  <surname>WITKOWSKI</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>C. M.</given-names>
                  <surname>SHACKLETON</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40189810</issue-id>
         <fpage>38</fpage>
         <lpage>46</lpage>
         <permissions>
            <copyright-statement>© Foundation for Environmental Conservation 2004</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/44520730"/>
         <abstract>
            <p>Rising demand for medicinal plants has led to increased pressure on wild plant populations. This, combined with shrinking habitats, means that many species in South Africa are now facing local extinction. In 1997, a study was initiated to determine the extent of trade in medicinal plants in the South African Lowveld (the low lying plains to the east of the Drakensberg escarpment), and to investigate socio-economic factors influencing trade and resource management. Trade was not as extensive in the Lowveld as in major urban markets such as Durban or the Witwatersrand (Johannesburg and surrounding towns), either in terms of the quantity, number or range of species sold, or the numbers of people relying on the trade for an income. In markets assessed in Mpumalanga Province, 176 species were identified (71% of the vernacular names encountered in the market place), representing 69 plant families. In Limpopo, 70 different species were identified (84% of the vernacular names encountered in the market place), representing 40 families. Imports were significant in Mpumalanga (33% of the plants on offer), mainly from Mozambique. A detrended correspondence analysis showed substantial differences between species traded in Mpumalanga and those sold in Limpopo. There was little variation in the species stocked by vendors in Mpumalanga, regardless of the season, the attributes of the seller, or whether business was carried out in urban or rural areas. In contrast, there was considerable variation in the stock inventories of the Limpopo traders. Despite the lower levels of local trade, increased harvesting pressure is being experienced regionally, to meet demand in metropolitan centres such as the Witwatersrand. This study showed considerable local variation and complexities in the harvesting and marketing of medicinal plants, with both a national and an international dimension. This dual spatial scale presents both opportunities and challenges in the management of these plants, which need to be addressed simultaneously, particularly with respect to research requirements and development of predictive models and capacity. Cooperation in conservation strategies and policies is required at regional, national and international levels, while ensuring that management initiatives take into account local market conditions and the socio-economic realities facing both consumers and those who depend on the trade for their livelihoods.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1248e211a1310">
            <mixed-citation id="d1248e215" publication-type="other">
Bass, S., Hughes, C. &amp; Hawthorne, W. (2001) Forest, biodiversity
and livelihoods: linking policy and practice. In: Living Off
Biodiversity: Exploring Livelihoods and Biodiversity Issues in
Natural Resource Management, ed. I. Koziell &amp; J. Saunders,
pp. 24-73. London, UK: IIED.</mixed-citation>
         </ref>
         <ref id="d1248e234a1310">
            <mixed-citation id="d1248e238" publication-type="other">
Botha, J. (1998) Developing an understanding of problems being
experienced by traditional healers living on the western border
of the Kruger National Park: foundations for an integrated
conservation and development programme. Development Southern
Africa 15: 621-634.</mixed-citation>
         </ref>
         <ref id="d1248e257a1310">
            <mixed-citation id="d1248e261" publication-type="other">
Botha, J. (2001) Perceptions of species availability and values
of medicinal plants traded in areas adjacent to the Kruger
National Park. M.Sc. dissertation, University of Witwatersrand,
Witwatersrand, South Africa.</mixed-citation>
         </ref>
         <ref id="d1248e277a1310">
            <mixed-citation id="d1248e281" publication-type="other">
Botha, J., Witkowski, E.T.F. &amp; Shackleton, C.M. (2001) An
inventory of the medicinal plants traded on the western boundary
of the Kruger National Park, South Africa. Koedoe 44: 7^16.</mixed-citation>
         </ref>
         <ref id="d1248e295a1310">
            <mixed-citation id="d1248e299" publication-type="other">
Botha, J., Witkowski, E.T.F. &amp; Shackleton, C.M. (2002) A
comparison of anthropogenic and elephant disturbance on Acacia
xanthophloea (fever tree) populations in the Lowveld, South
Africa. Koedoe 45: 9-18.</mixed-citation>
         </ref>
         <ref id="d1248e315a1310">
            <mixed-citation id="d1248e319" publication-type="other">
Botha, J., Witkowski, E.T.F. &amp; Shackleton, C.M. (2003) Important
medicinal plants of the Lowveld, South Africa -a resource
users' perspective. Plant Ecology and Conservation Series No. 13.
Report compiled for South African National Parks, Mpumalanga
Parks Board and Department of Environment Affairs, Limpopo
Province, South Africa.</mixed-citation>
         </ref>
         <ref id="d1248e342a1310">
            <mixed-citation id="d1248e346" publication-type="other">
Botha, J., Witkowski, E.T.F. &amp; Shackleton, C.M. (2004) The impact
of commercial harvesting on Warburgia salutaris (pepper-bark tree)
in Mpumalanga, South Africa. Biodiversity and Conservation (in
press).</mixed-citation>
         </ref>
         <ref id="d1248e362a1310">
            <mixed-citation id="d1248e366" publication-type="other">
Choge, S.K. (2001) Study of the economic aspects of the woodcarving
industry in Kenya: implications for policy development to make
the industry more sustainable. M.Sc. thesis, University of Natal,
Durban, South Africa.</mixed-citation>
         </ref>
         <ref id="d1248e382a1310">
            <mixed-citation id="d1248e386" publication-type="other">
Cocks, M., Dold, T. &amp; Grundy, I. (2004) The medicinal plant trade
in the Eastern Cape Province of South Africa. In: Use and Value
of Indigenous Forests and Woodlands in South Africa, ed. M. Lawes,
H. Eeley, C.M. Shackleton &amp; B.S. Geach (in press). Natal, South
Africa: Natal University Press.</mixed-citation>
         </ref>
         <ref id="d1248e405a1310">
            <mixed-citation id="d1248e409" publication-type="other">
Crook, C. &amp; Clapp, A. (1998) Is market-orientated forest
conservation a contradiction in terms? Environmental Conservation
25: 131-145.</mixed-citation>
         </ref>
         <ref id="d1248e423a1310">
            <mixed-citation id="d1248e427" publication-type="other">
Cunningham, A.B. (1991) The herbal medicinal trade: resource
depletion and environmental management for a 'hidden economy'.
In: South Africa's Informal Economy, ed. E. Preston-Whyte &amp;
C. Rogerson, pp. 196-206. Cape Town, South Africa: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d1248e446a1310">
            <mixed-citation id="d1248e452" publication-type="other">
Cunningham, A.B. (1992) Imithi isiZulu: the traditional medicine
trade in Natal/KwaZulu. Unpublished dissertation, Master of
Social Science, Department of Geographical and Environmental
Sciences, Faculty of Science, University of Natal, South Africa.</mixed-citation>
         </ref>
         <ref id="d1248e468a1310">
            <mixed-citation id="d1248e472" publication-type="other">
Dauskardt, R. (1991) 'Urban herbalism': the restructuring of
informal survival in Witwatersrand. In: South Africa 's Informal
Economy, ed. E. Preston-Whyte &amp; C. Rogerson, pp. 87-100. Cape
Town, South Africa: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d1248e488a1310">
            <mixed-citation id="d1248e492" publication-type="other">
Hutchings, A. (1989) Observations on plant usage in Xhosa and Zulu
medicine. Bothalia 19: 225-235.</mixed-citation>
         </ref>
         <ref id="d1248e502a1310">
            <mixed-citation id="d1248e506" publication-type="other">
Ibarra-Manriquez, G., Ricker, M., Angeles, G., Sinaca Colin, S.
&amp; Sinaca Colin, M. (1997) Useful plants of the Los Tuxtlas
rainforest (Veracruz, Mexico): considerations of their market
potential. Economic Botany 51: 362-376.</mixed-citation>
         </ref>
         <ref id="d1248e522a1310">
            <mixed-citation id="d1248e526" publication-type="other">
Kumar, V. &amp; Jain, S.K. (2002) Plant products in some tribal markets
of central India. Economic Botany 56: 242-245.</mixed-citation>
         </ref>
         <ref id="d1248e537a1310">
            <mixed-citation id="d1248e541" publication-type="other">
Lange, D. (1997) Trade figures for botanical drugs world-wide.
Medicinal Plant Conservation 3: 16-17.</mixed-citation>
         </ref>
         <ref id="d1248e551a1310">
            <mixed-citation id="d1248e555" publication-type="other">
Letsela, T., Witkowski, E.T.F. &amp; Balkwill, K. (2002) Direct
use values of communal resources in Bokong and Tsehlanyane
in Lesotho: Whither the commons: International Journal of
Sustainable Development and World Ecology 9: 351-368.</mixed-citation>
         </ref>
         <ref id="d1248e571a1310">
            <mixed-citation id="d1248e575" publication-type="other">
Luoga, E.J., Witkowski, E.T.F. &amp; Balkwill, K. (2004) Land cover and
use changes in relation to the institutional framework and tenure
of land and resources in eastern Tanzania miombo woodlands.
Environment, Development and Sustainability (in press).</mixed-citation>
         </ref>
         <ref id="d1248e591a1310">
            <mixed-citation id="d1248e595" publication-type="other">
Mander, M. (1997) Medicinal plant marketing in Bushbuckridge and
Mpumalanga: a market survey and recommended strategies for
sustaining the supply of plants in the region. Unpublished report.
Danced Community Forestry Project in the Bushbuckridge area,
Darudec and DWAF, South Africa.</mixed-citation>
         </ref>
         <ref id="d1248e614a1310">
            <mixed-citation id="d1248e618" publication-type="other">
Mander, M. (1998) Marketing of Indigenous Medicinal Plants in South
Africa. A case study in KwaZulu-Natal. Rome, Italy: Food and
Agricultural Organization of the United Nations.</mixed-citation>
         </ref>
         <ref id="d1248e631a1310">
            <mixed-citation id="d1248e635" publication-type="other">
Marshall, N.T. (1998) Searching for a Cure: Conservation of Medicinal
Wildlife Resources in East and Southern Africa. Cambridge, UK:
Traffic International.</mixed-citation>
         </ref>
         <ref id="d1248e649a1310">
            <mixed-citation id="d1248e653" publication-type="other">
Martin, G. (1995) Ethnobotany. London, UK: Chapman and Hall.</mixed-citation>
         </ref>
         <ref id="d1248e660a1310">
            <mixed-citation id="d1248e664" publication-type="other">
Netshiluvhi, T.R. (1999) Demand, propagation and seedling
establishment of selected medicinal trees. South African Journal
of Botany 65: 331-338.</mixed-citation>
         </ref>
         <ref id="d1248e677a1310">
            <mixed-citation id="d1248e681" publication-type="other">
Neumann, R.P. &amp; Hirsch, E. (2000) Commercialisation of Non-
timber Forest Products: Review and Analysis for Research. Bogor,
Indonesia: CIFOR.</mixed-citation>
         </ref>
         <ref id="d1248e694a1310">
            <mixed-citation id="d1248e698" publication-type="other">
Olsen, C.S. (1998) The trade in medicinal and aromatic plants from
Central Nepal to Northern India. Journal of Economic Botany 52:
279-292.</mixed-citation>
         </ref>
         <ref id="d1248e711a1310">
            <mixed-citation id="d1248e715" publication-type="other">
Olsen, C.S. &amp; Helles, F. (1997) Medicinal plants, markets and
margins in the Nepal Himalaya: trouble in paradise. Mountain
Research and Development 17: 363-374.</mixed-citation>
         </ref>
         <ref id="d1248e728a1310">
            <mixed-citation id="d1248e732" publication-type="other">
Shackleton, C.M. (2001) Re-examining local and market orientated
use of wild species for the conservation of biodiversity.
Environmental Conservation 28: 270-278.</mixed-citation>
         </ref>
         <ref id="d1248e746a1310">
            <mixed-citation id="d1248e750" publication-type="other">
Shackleton, C.M., Shackleton, S.E. &amp; Cousins, B. (2001) The role of
land-based strategies in rural livelihoods: the contribution of arable
production, animal husbandry and natural resource harvesting in
communal areas in South Africa. Development Southern Africa 18:
581-604.</mixed-citation>
         </ref>
         <ref id="d1248e769a1310">
            <mixed-citation id="d1248e773" publication-type="other">
Statistics South Africa (1991) Census 1991. Pretoria, South Africa
[www document], http://www.statssa.gov.za</mixed-citation>
         </ref>
         <ref id="d1248e783a1310">
            <mixed-citation id="d1248e787" publication-type="other">
Statistics South Africa (1998) Census 1996. Pretoria, South Africa
[www document], http://www.statssa.gov.za</mixed-citation>
         </ref>
         <ref id="d1248e797a1310">
            <mixed-citation id="d1248e801" publication-type="other">
ter Braack, C.J.F. (1988) CANOCO: a FORTRAN programme for
canonical community ordination by partial detrended canonical
correspondence analysis, principle components analysis and
redundancy analysis. Wageningen, the Netherlands.</mixed-citation>
         </ref>
         <ref id="d1248e817a1310">
            <mixed-citation id="d1248e821" publication-type="other">
Ugent, D. (2000) Medicine, myths and magic. The folk
healers of a Mexican market. Economic Botany 54: 427-
438.</mixed-citation>
         </ref>
         <ref id="d1248e834a1310">
            <mixed-citation id="d1248e838" publication-type="other">
Veeman, M. (2002) Understanding local and regional markets for
forest products. In: Uncovering the Hidden Harvest: Valuation
Methods for Woodland and Forest Products, ed. B.M. Campbell &amp;
M.K. Lückert, pp. 66-102. London, UK: Earthscan.</mixed-citation>
         </ref>
         <ref id="d1248e855a1310">
            <mixed-citation id="d1248e859" publication-type="other">
Williams, V.L., Balkwill, K. &amp; Witkowski, E.T.F. (2000)
Unravelling the commercial market for medicinal plants and
plant parts on the Witwatersrand. Economic Botany 54: 310-
327.</mixed-citation>
         </ref>
         <ref id="d1248e875a1310">
            <mixed-citation id="d1248e879" publication-type="other">
Williams, V.L., Balkwill, K. &amp; Witkowski, E.T.F. (2001) A lexicon of
plants traded on the Witwatersrand umuthi shops, South Africa.
Bothalia 31: 71-98.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
