<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4485192</article-id>
         <article-categories>
            <subj-group>
               <subject>Articles and Commentaries</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Age Is an Important Risk Factor for Onset and Sequelae of Reversal Reactions in Vietnamese Patients with Leprosy</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Brigitte Ranque</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Van Thuc Nguyen</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Hong Thai Vu</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Thu Huong Nguyen</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Ngoc Ba Nguyen</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Xuan Khoa Pham</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Erwin</given-names>
                  <surname>Schurr</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Laurent</given-names>
                  <surname>Abel</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name> Alexandre Alcaïs </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>01</day>
            <month>1</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">44</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i402595</issue-id>
         <fpage>33</fpage>
         <lpage>40</lpage>
         <page-range>33-40</page-range>
         <permissions>
            <copyright-statement>Copyright 2006 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4485192"/>
         <abstract>
            <p> Background. Reversal, or type 1, leprosy reactions (T1Rs) are acute immune episodes that occur in skin and/or nerves and are the leading cause of neurological impairment in patients with leprosy. T1Rs occur mainly in patients with borderline or multibacillary leprosy, but little is known about additional risk factors. Methods. We enrolled 337 Vietnamese patients with leprosy in our study, including 169 subjects who presented with T1Rs and 168 subjects with no history of T1Rs. A multivariate analysis was used to determine risk factors for T1R occurrence, time to T1R onset after leprosy diagnosis, and T1R sequelae after treatment. Results. Prevalence of T1Rs was estimated to be 29.1%. Multivariate analysis identified 3 clinical features of leprosy associated with T1R occurrence. Borderline leprosy subtype (odds ratio, 6.3 [95% confidence interval, 2.9-13.7] vs. polar subtypes) was the major risk factor; 2 other risk factors were positive bacillary index and presence of &gt;5 skin lesions. In addition, age at leprosy diagnosis was a strong independent risk factor for T1Rs (odds ratio, 2.4 [95% confidence interval, 1.3-4.4] for patients aged ≥15 years old vs. &lt;15 years old). We observed that T1Rs with neuritis occurred significantly earlier than pure skin-related T1Rs. Sequelae were present in 45.1% of patients who experienced T1Rs after treatment. The presence of a motor or sensory deficit at T1R onset was an independent risk factor for sequelae, as was the age at diagnosis of leprosy (odds ratio, 4.4 [95% confidence interval, 1.7-11.6] for patients ≥20 years old vs. &lt;20 years old). Conclusion. In addition to specific clinical features of leprosy, age is an important risk factor for both T1R occurrence and sequelae after treatment for T1Rs. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d730e281a1310">
            <label>1</label>
            <mixed-citation id="d730e288" publication-type="journal">
World Health Organization. Global leprosy situation. Wkly Epidemiol
Rec2005; 80:113-24.<person-group>
                  <string-name>
                     <surname>World Health Organization</surname>
                  </string-name>
               </person-group>
               <fpage>113</fpage>
               <volume>80</volume>
               <source>Wkly Epidemiol Rec</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d730e320a1310">
            <label>2</label>
            <mixed-citation id="d730e327" publication-type="journal">
Meima A, Richardus JH, Habbema JD. Trends in leprosy case detection
worldwide since 1985. Lepr Rev2004; 75:19-33.<person-group>
                  <string-name>
                     <surname>Meima</surname>
                  </string-name>
               </person-group>
               <fpage>19</fpage>
               <volume>75</volume>
               <source>Lepr Rev</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d730e359a1310">
            <label>3</label>
            <mixed-citation id="d730e366" publication-type="journal">
Britton WJ, Lockwood DN. Leprosy. Lancet2004; 363:1209-19.<person-group>
                  <string-name>
                     <surname>Britton</surname>
                  </string-name>
               </person-group>
               <fpage>1209</fpage>
               <volume>363</volume>
               <source>Lancet</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d730e395a1310">
            <label>4</label>
            <mixed-citation id="d730e402" publication-type="journal">
Scollard DM, Adams LB, Gillis TP, Krahenbuhl JL, Truman RW, Wil-
liams DL. The continuing challenges of leprosy. Clin Microbiol Rev
2006; 19:338-81.<person-group>
                  <string-name>
                     <surname>Scollard</surname>
                  </string-name>
               </person-group>
               <fpage>338</fpage>
               <volume>19</volume>
               <source>Clin Microbiol Rev</source>
               <year>2006</year>
            </mixed-citation>
         </ref>
         <ref id="d730e438a1310">
            <label>5</label>
            <mixed-citation id="d730e445" publication-type="journal">
Ridley DS, Jopling WH. Classification of leprosy according to im-
munity: a five-group system. Int J Lepr Other Mycobact Dis1966; 34:
255-73.<person-group>
                  <string-name>
                     <surname>Ridley</surname>
                  </string-name>
               </person-group>
               <fpage>255</fpage>
               <volume>34</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d730e480a1310">
            <label>6</label>
            <mixed-citation id="d730e487" publication-type="journal">
World Health Organization. Expert committee on leprosy: 6th report.
World Health Organ Tech Rep Ser1988;768:1-51.<person-group>
                  <string-name>
                     <surname>World Health Organization</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>768</volume>
               <source>World Health Organ Tech Rep Ser</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d730e519a1310">
            <label>7</label>
            <mixed-citation id="d730e526" publication-type="journal">
Lienhardt C, Fine PE. Type 1 reaction, neuritis and disability in leprosy:
what is the current epidemiological situation? Lepr Rev1994; 65:9-33.<person-group>
                  <string-name>
                     <surname>Lienhardt</surname>
                  </string-name>
               </person-group>
               <fpage>9</fpage>
               <volume>65</volume>
               <source>Lepr Rev</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d730e558a1310">
            <label>8</label>
            <mixed-citation id="d730e565" publication-type="journal">
Becx-Bleumink M, Berhe D. Occurrence of reactions, their diagnosis
and management in leprosy patients treated with multidrug therapy:
experience in the leprosy control program of the All Africa Leprosy
and Rehabilitation Training Center (ALERT) in Ethiopia. Int J Lepr
Other Mycobact Dis1992; 60:173-84.<person-group>
                  <string-name>
                     <surname>Becx-Bleumink</surname>
                  </string-name>
               </person-group>
               <fpage>173</fpage>
               <volume>60</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d730e606a1310">
            <label>9</label>
            <mixed-citation id="d730e613" publication-type="journal">
Van Brakel WH, Khawas IB, Lucas SB. Reactions in leprosy: an epi-
demiological study of 386 patients in West Nepal. Lepr Rev1994; 65:
190-203.<person-group>
                  <string-name>
                     <surname>Van Brakel</surname>
                  </string-name>
               </person-group>
               <fpage>190</fpage>
               <volume>65</volume>
               <source>Lepr Rev</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d730e648a1310">
            <label>10</label>
            <mixed-citation id="d730e655" publication-type="journal">
Schreuder PA. The occurrence of reactions and impairments in leprosy:
experience in the leprosy control program of three provinces in north-
eastern Thailand, 1987-1995 [correction of 1978-1995]-II. reactions.
Int J Lepr Other Mycobact Dis1998;66:159-69.<person-group>
                  <string-name>
                     <surname>Schreuder</surname>
                  </string-name>
               </person-group>
               <fpage>159</fpage>
               <volume>66</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d730e694a1310">
            <label>11</label>
            <mixed-citation id="d730e701" publication-type="journal">
Sharma N, Koranne RV, Mendiratta V, Sharma RC. A study of leprosy
reactions in a tertiary hospital in Delhi. J Dermatol2004; 31:898-903.<person-group>
                  <string-name>
                     <surname>Sharma</surname>
                  </string-name>
               </person-group>
               <fpage>898</fpage>
               <volume>31</volume>
               <source>J Dermatol</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d730e733a1310">
            <label>12</label>
            <mixed-citation id="d730e740" publication-type="journal">
Kumar B, Dogra S, Kaur I. Epidemiological characteristics of leprosy
reactions: 15 years experience from north India. Int J Lepr Other My-
cobact Dis2004; 72:125-33.<person-group>
                  <string-name>
                     <surname>Kumar</surname>
                  </string-name>
               </person-group>
               <fpage>125</fpage>
               <volume>72</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d730e775a1310">
            <label>13</label>
            <mixed-citation id="d730e782" publication-type="journal">
Rose P, Waters ME Reversal reactions in leprosy and their management.
Lepr Rev1991; 62:113-21.<person-group>
                  <string-name>
                     <surname>Rose</surname>
                  </string-name>
               </person-group>
               <fpage>113</fpage>
               <volume>62</volume>
               <source>Lepr Rev</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d730e814a1310">
            <label>14</label>
            <mixed-citation id="d730e821" publication-type="journal">
Khanolkar-Young S, Rayment N, Brickell PM, et al. Tumour necrosis
factor-alpha (TNF-a) synthesis is associated with the skin and pe-
ripheral nerve pathology of leprosy reversal reactions. Clin Exp Im-
munol1995; 99:196-202.<person-group>
                  <string-name>
                     <surname>Khanolkar-Young</surname>
                  </string-name>
               </person-group>
               <fpage>196</fpage>
               <volume>99</volume>
               <source>Clin Exp Immunol</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d730e859a1310">
            <label>15</label>
            <mixed-citation id="d730e866" publication-type="journal">
Verhagen CE, Wierenga EA, Buffing AA, Chand MA, Faber WR, Das
PK. Reversal reaction in borderline leprosy is associated with a polar-
ized shift to type 1-like Mycobacterium leprae T cell reactivity in lesional
skin: a follow-up study. J Immunol1997; 159:4474-83.<person-group>
                  <string-name>
                     <surname>Verhagen</surname>
                  </string-name>
               </person-group>
               <fpage>4474</fpage>
               <volume>159</volume>
               <source>J Immunol</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d730e904a1310">
            <label>16</label>
            <mixed-citation id="d730e911" publication-type="journal">
Stefani MM, Martelli CM, Gillis TP, Krahenbuhl JL. In situ type 1
cytokine gene expression and mechanisms associated with early leprosy
progression. J Infect Dis2003; 188:1024-31.<person-group>
                  <string-name>
                     <surname>Stefani</surname>
                  </string-name>
               </person-group>
               <fpage>1024</fpage>
               <volume>188</volume>
               <source>J Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d730e947a1310">
            <label>17</label>
            <mixed-citation id="d730e954" publication-type="journal">
Moraes MO, Sarno EN, Almeida AS, et al. Cytokine mRNA expression
in leprosy: a possible role for interferon-gamma and interleukin-12 in
reactions (RR and ENL). Scand J Immunol1999;50:541-9.<person-group>
                  <string-name>
                     <surname>Moraes</surname>
                  </string-name>
               </person-group>
               <fpage>541</fpage>
               <volume>50</volume>
               <source>Scand J Immunol</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d730e989a1310">
            <label>18</label>
            <mixed-citation id="d730e998" publication-type="journal">
Moraes MO, Sampaio EP, Nery JA, Saraiva BC, Alvarenga FB, Sarno
EN. Sequential erythema nodosum leprosum and reversal reaction with
similar lesional cytokine mRNA patterns in a borderline leprosy patient.
Br J Dermatol2001; 144:175-81.<person-group>
                  <string-name>
                     <surname>Moraes</surname>
                  </string-name>
               </person-group>
               <fpage>175</fpage>
               <volume>144</volume>
               <source>Br J Dermatol</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1036a1310">
            <label>19</label>
            <mixed-citation id="d730e1043" publication-type="journal">
Britton WJ. The management of leprosy reversal reactions. Lepr Rev
1998; 69:225-34.<person-group>
                  <string-name>
                     <surname>Britton</surname>
                  </string-name>
               </person-group>
               <fpage>225</fpage>
               <volume>69</volume>
               <source>Lepr Rev</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1075a1310">
            <label>20</label>
            <mixed-citation id="d730e1082" publication-type="journal">
Roche PW, Theuvenet WJ, Britton WJ. Risk factors for type-1 reactions
in borderline leprosy patients. Lancet1991; 338:654-7.<person-group>
                  <string-name>
                     <surname>Roche</surname>
                  </string-name>
               </person-group>
               <fpage>654</fpage>
               <volume>338</volume>
               <source>Lancet</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1114a1310">
            <label>21</label>
            <mixed-citation id="d730e1121" publication-type="journal">
Lockwood DN, Vinayakumar S, Stanley JN, McAdam KP, Colston MJ.
Clinical features and outcome of reversal (type 1) reactions in Hy-
derabad, India. Int J Lepr Other Mycobact Dis1993;61:8-15.<person-group>
                  <string-name>
                     <surname>Lockwood</surname>
                  </string-name>
               </person-group>
               <fpage>8</fpage>
               <volume>61</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1156a1310">
            <label>22</label>
            <mixed-citation id="d730e1163" publication-type="journal">
de Rijk AJ, Gabre S, Byass P, Berhanu T. Field evaluation of WHO-
MDT of fixed duration, at ALERT, Ethiopia: the AMFES project-II.
reaction and neuritis during and after MDT in PB and MB leprosy
patients. Lepr Rev1994; 65:320-32.<person-group>
                  <string-name>
                     <surname>de Rijk</surname>
                  </string-name>
               </person-group>
               <fpage>320</fpage>
               <volume>65</volume>
               <source>Lepr Rev</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1202a1310">
            <label>23</label>
            <mixed-citation id="d730e1209" publication-type="journal">
Saunderson P, Gebre S, Byass P. Reversal reactions in the skin lesions
of AMFES patients: incidence and risk factors. Lepr Rev2000;71:
309-17.<person-group>
                  <string-name>
                     <surname>Saunderson</surname>
                  </string-name>
               </person-group>
               <fpage>309</fpage>
               <volume>71</volume>
               <source>Lepr Rev</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1244a1310">
            <label>24</label>
            <mixed-citation id="d730e1251" publication-type="journal">
Mira MT, Alcais A, Nguyen VT, et al. Susceptibility to leprosy is as-
sociated with PARK2 and PACRG. Nature2004; 427:636-40.<person-group>
                  <string-name>
                     <surname>Mira</surname>
                  </string-name>
               </person-group>
               <fpage>636</fpage>
               <volume>427</volume>
               <source>Nature</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1283a1310">
            <label>25</label>
            <mixed-citation id="d730e1290" publication-type="journal">
Mira MT, Alcais A, Di Pietrantonio T, et al. Segregation of HLA/TNF
region is linked to leprosy clinical spectrum in families displaying mixed
leprosy subtypes. Genes Immun2003; 4:67-73.<person-group>
                  <string-name>
                     <surname>Mira</surname>
                  </string-name>
               </person-group>
               <fpage>67</fpage>
               <volume>4</volume>
               <source>Genes Immun</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1325a1310">
            <label>26</label>
            <mixed-citation id="d730e1332" publication-type="journal">
World Health Organization. Chemotherapy of leprosy for control pro-
grammes. World Health Organ Tech Rep Ser1982;675:1-33.<person-group>
                  <string-name>
                     <surname>World Health Organization</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>675</volume>
               <source>World Health Organ Tech Rep Ser</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1364a1310">
            <label>27</label>
            <mixed-citation id="d730e1371" publication-type="journal">
Boerrigter G, Ponnighaus JM, Fine PE, Wilson RJ. Four-year follow-
up results of a WHO-recommended multiple-drug regimen in pau-
cibacillary leprosy patients in Malawi. Int J Lepr Other Mycobact Dis
1991; 59:255-61.<person-group>
                  <string-name>
                     <surname>Boerrigter</surname>
                  </string-name>
               </person-group>
               <fpage>255</fpage>
               <volume>59</volume>
               <source>Int J Lepr Other Mycobact Dis</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1409a1310">
            <label>28</label>
            <mixed-citation id="d730e1416" publication-type="journal">
Adkins B, Leclerc C, Marshall-Clarke S. Neonatal adaptive immunity
comes of age. Nat Rev Immunol2004; 4:553-64.<person-group>
                  <string-name>
                     <surname>Adkins</surname>
                  </string-name>
               </person-group>
               <fpage>553</fpage>
               <volume>4</volume>
               <source>Nat Rev Immunol</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1449a1310">
            <label>29</label>
            <mixed-citation id="d730e1456" publication-type="journal">
Sun CM, Deriaud E, Leclerc C, Lo-Man R. Upon TLR9 signaling, CD5+
B cells control the IL- 12-dependent Th 1-priming capacity of neonatal
DCs. Immunity2005; 22:467-77.<person-group>
                  <string-name>
                     <surname>Sun</surname>
                  </string-name>
               </person-group>
               <fpage>467</fpage>
               <volume>22</volume>
               <source>Immunity</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1491a1310">
            <label>30</label>
            <mixed-citation id="d730e1498" publication-type="journal">
Wilkinson RJ, Lockwood DN. Antigenic trigger for type 1 reaction in
leprosy. J Infect2005; 50:242-3.<person-group>
                  <string-name>
                     <surname>Wilkinson</surname>
                  </string-name>
               </person-group>
               <fpage>242</fpage>
               <volume>50</volume>
               <source>J Infect</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d730e1530a1310">
            <label>31</label>
            <mixed-citation id="d730e1537" publication-type="journal">
Alcais A, Mira M, Casanova JL, Schurr E, Abel L. Genetic dissection
of immunity in leprosy. Curr Opin Immunol2005; 17:44-8.<person-group>
                  <string-name>
                     <surname>Alcais</surname>
                  </string-name>
               </person-group>
               <fpage>44</fpage>
               <volume>17</volume>
               <source>Curr Opin Immunol</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
