<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt9m0s5k</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt22h6sjn</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Religion</subject>
      </subj-group>
      <book-title-group>
         <book-title>Abraham Our Father</book-title>
         <subtitle>Paul and the Ancestors in Postcolonial Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>KAMUDZANDU</surname>
               <given-names>ISRAEL</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>06</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="ppub">9780800698171</isbn>
      <isbn content-type="ppub">0800698177</isbn>
      <isbn content-type="epub">9781451426298</isbn>
      <isbn content-type="epub">1451426291</isbn>
      <publisher>
         <publisher-name>Fortress Press</publisher-name>
         <publisher-loc>Minneapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Fortress Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt22h6sjn"/>
      <abstract abstract-type="short">
         <p>Father Abraham had many sons . . . So goes the chorus that the Shona people learned from European missionaries as part of the broader experience of colonization that they share with other African peoples. Urged to abandon their ancestors and embrace Christianity, the Shona instead engaged in a complex and ambiguous negotiation of ancestral myths, culture, and power.</p>
         <p>Israel Kamudzandu explores this legacy, showing how the Shona found in the figure of Abraham himself a potent resource for cultural resistance, and makes intriguing comparisons with the ways the apostle Paul used the same figure in his interaction with the ancestry of Aeneas in imperial myths of the destiny of the Roman people. The result is a groundbreaking study that combines the best tradition-historical insights with postcolonial-critical acumen. Kamudzandu offers at last a model of multi-cultural Christianity forged in the experience of postcolonial Zimbabwe.</p>
      </abstract>
      <counts>
         <page-count count="208"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.3</book-part-id>
                  <title-group>
                     <title>Other Books in the Series</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.4</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This book seeks to comprehend Paul’s theology in the contexts of other cultures, more specifically a particular culture: postcolonial Christianity in Zimbabwe. This Christianity is not a stand-alone religion but a synthesis of Western and African cultures.¹ Missionaries and colonial forces in Zimbabwe during the colonial period (1890–1980) greatly overlooked the symbolic world of the Shona people and imposed a new culture on an already religious people. The Shona selectively and inventively appropriated parts of the colonial version of Christianity that missionaries offered them and then transformed it. One important element in that critical appropriation was the figure of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Empire, Gospel, and Culture</title>
                  </title-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>To understand a nation, one must look at its history, culture, and religion, as well as the forces that shaped and transformed it. The following brief description of the history and identity of the Shona people of Zimbabwe and their traditional culture is intended to assist readers in understanding the foundations and developments of the Shona people and how they were transformed and shaped first by colonialism and second by missionary encounters. Like the Greeks and Romans, the Shona people have a mythos or a story that is foundational to their community.</p>
                     <p>From time immemorial, migration and immigration have shaped</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Zimbabwe’s Religious Cultural Configurations</title>
                  </title-group>
                  <fpage>23</fpage>
                  <abstract>
                     <p>The period of colonial expansion and missionary education saw the transformation of Shona religion, culture, identity, and social structures. From the beginning, the educational process of both Africans and whites was the responsibility of colonial administrators and missionaries. When Western Christian missionaries came to Zimbabwe, their two main objectives were to supplant African traditional religion with Christianity and to civilize the socalled pagan natives of the Dark Continent.¹ They sought to do this through moral and religious education, which in the Christian sense included all efforts and processes that help to bring children and adults into a vital and saving</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Postcolonial Shona Christianity</title>
                  </title-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>The subject of ancestry and descent from a powerful ancestor or founder holds a central place in politics, religion, and social life.¹ The Romans and Greeks of the Augustan era (fourth century BCE) claimed a powerful pedigree from Aeneas.² The apostle Paul, who represented the religious-social world of Palestine, claimed descent from Abraham (Gal. 1:13-16). While the Shona people have tribal ancestors, the period between 1890 and 1980 saw an amalgamation of tribes and ethnic groups into one distinctive nation who for the first time claimed Mbuya Nehanda as the ancestress of all Shona people.³ Nehanda had a great deal</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Aeneas—A Constructed Ancestor</title>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>In the overarching story of the last three chapters, I have discussed issues of power, ancestry, and religious identity during and after colonial domination. Focusing on the power of ancestors, this chapter will consider the construction and reinvention of Aeneas as an ancestor of both Greeks and Romans of the Augustan era. Chapter 5 will then focus on the juxtaposition of Aeneas and Abraham and how this comparison is useful in the exegesis and theology of Paul in colonial and postcolonial African contexts. While both figures represent marginalization from their beginning, their stories are a perfect fit to Paul’s theology</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Aeneas and Abraham Paradigms</title>
                  </title-group>
                  <fpage>83</fpage>
                  <abstract>
                     <p>Throughout this book I have argued that ancestral practices—<italic>mores maiorum</italic>—are important signatures upon which cultures of the world seek to preserve the traditions of the past and use them to inform and guide both the present and the future. I have argued that Paul, as well as his communities of Jews, Greeks, and Romans, were aware of ancestor traditions, especially the Augustan allegiance to founding figures. I have argued and will continue to emphasize that Pauline exegesis must begin to take into account the conditions of people’s context when reading biblical texts.</p>
                     <p>Paul’s Letter to the Romans cannot</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Conclusion and Implications</title>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract>
                     <p>The argument of this work has been to describe how Paul’s construction of Abraham in Romans assisted Africans to appreciate Christianity, not as a foreign religion, but as one that was in alignment with African traditional religion. The Aeneas-Abraham paradigm is new, and it fits the experience of African Christianity. This comparison is powerful because Paul did what Christians do. He did not just walk onto the scene seeking to impose his Jewish ancestry. Rather, he selectively appropriated aspects of the Aeneid story and made them central to his Jesus story. Therefore, the creative action out of a situation of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.13</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>105</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.14</book-part-id>
                  <title-group>
                     <title>Index of Names</title>
                  </title-group>
                  <fpage>115</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt22h6sjn.15</book-part-id>
                  <title-group>
                     <title>Index of Passages</title>
                  </title-group>
                  <fpage>117</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
