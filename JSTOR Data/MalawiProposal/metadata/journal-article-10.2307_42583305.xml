<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42583305</article-id>
         <title-group>
            <article-title>Ready-to-use food-allocation policy to reduce the effects of childhood undernutrition in developing countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Yan</given-names>
                  <surname>Yang</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jan</given-names>
                  <surname>Van den Broeck</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lawrence M.</given-names>
                  <surname>Wein</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>19</day>
            <month>3</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">110</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40095946</issue-id>
         <fpage>4545</fpage>
         <lpage>4550</lpage>
         <permissions>
            <copyright-statement>copyright © 1993-2008 National Academy of Sciences of the United States of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42583305"/>
         <abstract>
            <p>Several aid groups have proposed strategies for allocating ready-touse (therapeutic and supplementary) foods to children in developing countries. Analysis is needed to investigate whether there are better alternatives. We use a longitudinal dataset of 5,657 children from Bwamanda to construct a bivariate time-series model that tracks each child's height-for-age z score (HAZ) and weight-forheight z score (WHZ) throughout the first 5 y of life. Our optimization model chooses which individual children should receive readyto-use therapeutic or supplementary food based on a child's sex, age, HAZ, and WHZ, to minimize the mean number of disabilityadjusted life years (DALYs) per child during 6-60 mo of age [which includes childhood mortality calculated from a logistic regression and the lifelong effects of stunting (i.e., low HAZ)] subject to a budget constraint. Compared with the strategies proposed by the aid groups, which do not use HAZ information, the simple strategy arising from our analysis [which prioritizes children according to low values of a linear combination of HAZ, WHZ, and age and allocates the entire budget to therapeutic (i.e., 500 kcal/d) food for the prioritized children] reduces the number of DALYs by 9% (for the same budget) or alternatively incurs the same number of DALYs with a 61 % reduction in cost. Whereas our qualitative conclusions appear to be robust, the quantitative results derived from our analysis should be treated with caution because of the lack of reliable data on the impact of supplementary food on HAZ and WHZ, the application of our model to a single cohort of children and the inclusion and exclusion errors related to imperfect food targeting.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d1218e160a1310">
            <label>1</label>
            <mixed-citation id="d1218e167" publication-type="other">
de Onis M, Garza C, Onyango AW, Borghi E (2007) Comparison of the WHO child
growth standards and the CDC 2000 growth charts. J Nutr 137(1):144-148.</mixed-citation>
         </ref>
         <ref id="d1218e177a1310">
            <label>2</label>
            <mixed-citation id="d1218e184" publication-type="other">
Black RE, et al.; Maternal and Child Undernutrition Study Group (2008) Maternal and
child undernutrition: Global and regional exposures and health consequences. Lancet
371(9608):243-260.</mixed-citation>
         </ref>
         <ref id="d1218e197a1310">
            <label>3</label>
            <mixed-citation id="d1218e204" publication-type="other">
United Nations Children's Fund (2012) The State of the World's Children 2012 (United
Nations Children's Fund, New York).</mixed-citation>
         </ref>
         <ref id="d1218e214a1310">
            <label>4</label>
            <mixed-citation id="d1218e221" publication-type="other">
Briend A, et al. (1999) Ready-to-use therapeutic food for treatment of marasmus.
Lancet 353(9166):1767-1768.</mixed-citation>
         </ref>
         <ref id="d1218e232a1310">
            <label>5</label>
            <mixed-citation id="d1218e239" publication-type="other">
Victora CG, et al.; Maternal and Child Undernutrition Study Group (2008) Maternal
and child undernutrition: Consequences for adult health and human capital. Lancet
371(9609):340-357.</mixed-citation>
         </ref>
         <ref id="d1218e252a1310">
            <label>6</label>
            <mixed-citation id="d1218e259" publication-type="other">
Pelletier DL (1994) The relationship between child anthropometry and mortality in
developing countries: Implications for policy, programs and future research. J Nutr
124(10 Suppl):2047S-2081S.</mixed-citation>
         </ref>
         <ref id="d1218e272a1310">
            <label>7</label>
            <mixed-citation id="d1218e279" publication-type="other">
United States Agency for International Development (2011) Delivering Improved
Nutrition: Recommendations for Changes to U.S. Food Aid Products and Programs
(United States Agency for International Development, Washington).</mixed-citation>
         </ref>
         <ref id="d1218e292a1310">
            <label>8</label>
            <mixed-citation id="d1218e299" publication-type="other">
World Food Programme (2008) Ten minutes to learn about nutrition programming.
Sight Life Mag 3(Suppl):4-14.</mixed-citation>
         </ref>
         <ref id="d1218e309a1310">
            <label>9</label>
            <mixed-citation id="d1218e316" publication-type="other">
Van den Broeck JV, Eeckels R, Vuylsteke J (1993) Influence of nutritional status on
child mortality in rural Zaire. Lancet 341(8859):1491-1495.</mixed-citation>
         </ref>
         <ref id="d1218e326a1310">
            <label>10</label>
            <mixed-citation id="d1218e333" publication-type="other">
Isanaka S, et al. (2009) Effect of preventive supplementation with ready-to-use thera-
peutic food on the nutritional status, mortality and morbidity of children aged 6 to 60
months in Niger. JAMA 301(3):277-285.</mixed-citation>
         </ref>
         <ref id="d1218e347a1310">
            <label>11</label>
            <mixed-citation id="d1218e354" publication-type="other">
Brockwell PJ, Davis RA (1991) Time Series: Theory and Methods (Springer, Berlin).</mixed-citation>
         </ref>
         <ref id="d1218e361a1310">
            <label>12</label>
            <mixed-citation id="d1218e368" publication-type="other">
United Nations Development Programme (2006) Human Development Report (United
Nations, New York).</mixed-citation>
         </ref>
         <ref id="d1218e378a1310">
            <label>13</label>
            <mixed-citation id="d1218e385" publication-type="other">
Lopez AD, Mathers CD, Ezzati M, Jamison DT, Murray CJL, eds (2006) Global Burden of
Disease and Risk Factors (World Bank, Washington).</mixed-citation>
         </ref>
         <ref id="d1218e395a1310">
            <label>14</label>
            <mixed-citation id="d1218e402" publication-type="other">
World Food Programme (2004) The State of Food Insecurity in the World (World Food
Programme, Rome).</mixed-citation>
         </ref>
         <ref id="d1218e412a1310">
            <label>15</label>
            <mixed-citation id="d1218e419" publication-type="other">
Isanaka S, et al. (2010) Reducing wasting in young children with preventive supple-
mentation: A cohort study in Niger. Pediatrics 126(2):e442-e450.</mixed-citation>
         </ref>
         <ref id="d1218e429a1310">
            <label>16</label>
            <mixed-citation id="d1218e436" publication-type="other">
Garenne M, et al. (2009) Incidence and duration of severe wasting in two African
populations. Public Health Nutr 12(11):1974-1982.</mixed-citation>
         </ref>
         <ref id="d1218e447a1310">
            <label>17</label>
            <mixed-citation id="d1218e454" publication-type="other">
Richard SA, et al.; Childhood Infection and Malnutrition Network (2012) Wasting is
associated with stunting in early childhood. J Nutr 142(7):1291-1296.</mixed-citation>
         </ref>
         <ref id="d1218e464a1310">
            <label>18</label>
            <mixed-citation id="d1218e471" publication-type="other">
Richard SA, Black RE, Checkley W (2012) Revisiting the relationship of weight and
height in early childhood. Adv Nutr 3(2):250-254.</mixed-citation>
         </ref>
         <ref id="d1218e481a1310">
            <label>19</label>
            <mixed-citation id="d1218e488" publication-type="other">
Maleta K, Virtanen SM, Espo M, Kulmala T, Ashorn P (2003) Seasonality of growth and
the relationship between weight and height gain in children under three years of age
in rural Malawi. Acta Paediatr 92(4):491-497.</mixed-citation>
         </ref>
         <ref id="d1218e501a1310">
            <label>20</label>
            <mixed-citation id="d1218e508" publication-type="other">
Bhagowalia P, Chen SE, Masters WA (2011) Effects and determinants of mild underweight
among preschool children across countries and over time. Econ Hum Biol 9(1):66-77.</mixed-citation>
         </ref>
         <ref id="d1218e518a1310">
            <label>21</label>
            <mixed-citation id="d1218e525" publication-type="other">
de Pee S, Bloem MW (2009) Current and potential role of specially formulated foods
and food supplements for preventing malnutrition among 6-to 23-month-old chil-
dren and for treating moderate malnutrition among 6-to 59-month-old children.
Food Nutr Bull 30(3 Suppl):S434-S463.</mixed-citation>
         </ref>
         <ref id="d1218e541a1310">
            <label>22</label>
            <mixed-citation id="d1218e548" publication-type="other">
Chaparro CM, Dewey KG (2010) Use of lipid-based nutrient supplements (LNS) to
improve the nutrient adequacy of general food distribution rations for vulnerable
sub-groups in emergency settings. Matern Child Nutr 6(Suppl 1):1-69.</mixed-citation>
         </ref>
         <ref id="d1218e562a1310">
            <label>23</label>
            <mixed-citation id="d1218e569" publication-type="other">
Fergusson P, Tomkins A (2009) HIV prevalence and mortality among children un-
dergoing treatment for severe acute malnutrition in sub-Saharan Africa: A systematic
review and meta-analysis. Trans R Soc Trop Med Hyg 103(6):541-548.</mixed-citation>
         </ref>
         <ref id="d1218e582a1310">
            <label>24</label>
            <mixed-citation id="d1218e589" publication-type="other">
Prentice AM, Moore SE (2005) Early programming of adult diseases in resource poor
countries. Arch Dis Child 90(4):429-432.</mixed-citation>
         </ref>
         <ref id="d1218e599a1310">
            <label>25</label>
            <mixed-citation id="d1218e606" publication-type="other">
Lentz E, Barrett CB (2008) Improving food aid: What reforms would yield the highest
payoff? World Dev 36:1152-1172.</mixed-citation>
         </ref>
         <ref id="d1218e616a1310">
            <label>26</label>
            <mixed-citation id="d1218e623" publication-type="other">
United Nations Department of Economic and Social Affairs, Detailed Indicators (United
Nations, New York). Available at esa.un.org/unpd/wpp/unpp/panel_indicators.htm. Ac-
cessed on July 30, 2012.</mixed-citation>
         </ref>
         <ref id="d1218e636a1310">
            <label>27</label>
            <mixed-citation id="d1218e643" publication-type="other">
Robins JM, Hernan MA (2009) Estimation of the causal effects of time-varying ex-
posures. Longitudinal Data Analysis, eds Fitzmaurice G, Davidian M, Verbeke G,
Molenberghs G (Chapman &amp; Hall/CRC, Boca Raton, FL), Chap 23.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
