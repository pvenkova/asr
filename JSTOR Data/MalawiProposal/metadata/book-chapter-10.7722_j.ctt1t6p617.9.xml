<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7p396</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1t6p617</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Technology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Electricity in Africa</book-title>
         <subtitle>The Politics of Transformation in Uganda</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>GORE</surname>
               <given-names>CHRISTOPHER D.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>18</day>
         <month>08</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9781847011688</isbn>
      <isbn content-type="epub">9781787440579</isbn>
      <publisher>
         <publisher-name>Boydell &amp; Brewer</publisher-name>
         <publisher-loc>Woodbridge, Suffolk; Rochester, NY</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Christopher D. Gore</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7722/j.ctt1t6p617"/>
      <abstract abstract-type="short">
         <p>No country has managed to develop beyond a subsistence economy without ensuring at least minimum access to electricity for the majority of its population. Yet many sub-Saharan African countries struggle to meet demand. Why is this, and what can be done to reduce energy poverty and further Africa's development? Examining the politics and processes surrounding electricity infrastructure, provision and reform, the author provides an overview of historical and contemporary debates about access in the sub-continent, and explores the shifting role and influence of national governments and of multilateral agencies in energy reform decisions. He describes a challenging political environment for electricity supply, with African governments becoming increasingly frustrated with the rules and the processes of multilateral donors. Civil society also began to question reform choices, and governments in turn looked to new development partners, such as China, to chart a fresh path of energy transformation. Drawing on over fifteen years of research on Uganda, which has one of the lowest levels of access to electricity in Africa and has struggled to construct several, large hydroelectric dams on the Nile, Gore argues that there is a critical need to recognize how the changing political and social context in African countries, and globally, has affected the capacity to fulfil national energy goals, minimize energy poverty and transform economies. Christopher Gore is Associate Professor, Department of Politics and Public Administration, Ryerson University, Toronto, Canada.BR&gt;</p>
      </abstract>
      <counts>
         <page-count count="200"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.3</book-part-id>
                  <title-group>
                     <title>LIST OF ILLUSTRATIONS</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGEMENTS</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.5</book-part-id>
                  <title-group>
                     <title>List of Acronyms</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.6</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>On 24 January 2002, crowds gathered for a celebration on the banks of the Nile River. Just north of the town of Jinja, in the east African country of Uganda, diplomats, bilateral and multilateral agency representatives, citizens, Members of Parliament, and the President of Uganda all congregated for what was thought to be the beginning of the construction of a new 250-megawatt (MW) hydroelectric dam – the Bujagali dam.</p>
                     <p>The location of the ground-breaking ceremony held historical significance. In 1907, a young Winston Churchill, then Parliamentary Under-Secretary of State for Colonies, stood about ten kilometres downstream and reflected on the potential</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Electricity, Infrastructure &amp; Dams in Africa</title>
                  </title-group>
                  <fpage>12</fpage>
                  <abstract>
                     <p>In 2002, only slightly more than 20% of sub-Saharan Africa’s entire population had access to electricity, compared to 85% in North Africa, Latin America, East Asia and the Middle East, and 40% in South Asia (Saghir 2005, p. 9). By 2009, the International Energy Agency (IEA) estimated that slightly more than 30% of the population of sub-Saharan Africa had access to electricity (IEA 2011). In many countries, such as Malawi, Uganda, Tanzania, Mozambique, Democratic Republic of Congo and Burkina Faso, less than 15% of populations had access to electricity; only four countries in sub-Saharan Africa of twenty-eight countries documented by</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>The Politics of Provision</title>
                     <subtitle>A History of Debate &amp; Reform</subtitle>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>‘How should a developing country government, concerned with tackling poverty amongst its citizens, think about its role in the energy sector?’ (Bond, in World Bank &amp; ESMAP 2000, p. vii). This simple question has proven vexing for countries in sub-Saharan Africa. Energy is a particularly vexing policy area because of its connection to so many other sectors and because of the varied and mixed ways that the benefits of energy sector reform are communicated and can be communicated to citizens. What is, for example, the primary reason for introducing private sector providers? For increasing electricity tariffs? For building a hydroelectric</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Privatization &amp; Electricity Sector Reform</title>
                  </title-group>
                  <fpage>64</fpage>
                  <abstract>
                     <p>From the late 1960s until the late 1990s, power industries in Africa were most often national monopolies in charge of providing a public electricity service (Girod &amp; Percebois 1998, p. 22). The three segments of electricity service delivery – production/generation, transport/transmission, and distribution – were vertically integrated, with supervision and regulation supported by public ministries or quasi-independent regulatory agencies. The rationale for this arrangement stemmed from the belief that public utilities had to support national development and the cohesion of society through the distribution of an important public good – electricity (1998, pp. 22–3). A fall in sales of electricity during the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Dam Building &amp; Electricity in Contemporary Uganda</title>
                  </title-group>
                  <fpage>106</fpage>
                  <abstract>
                     <p>In 2001, the Uganda Electricity Board, the public monopoly, was unbundled, creating the Uganda Electricity Distribution Company Ltd (UEDCL), the Uganda Electricity Transmission Company Ltd (UETCL), and the Uganda Electricity Generation Company Ltd (UEGCL). As UEGCL’s main functions were the operation and maintenance of the two hydroelectric generating stations in Uganda, the company was based in Jinja – the location of Uganda’s first large hydroelectric dam, Owen Falls dam (now Nalubaale), and an extension to the Owen Falls dam, called the Kiira Power Station. In 2003, under the auspices of the country’s privatization strategy, Eskom Uganda Ltd, a subsidiary of South</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Electricity &amp; the Politics of Transformation</title>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>The Government of Uganda and the World Bank’s commitment to the Bujagali project did not wane. In April 2007, the World Bank Group approved US$360 million in loans and guarantees for the project (Bujagali II) – $130 million in loans to Bujagali Energy Ltd (BEL) from the International Finance Corporation; a Partial Risk Guarantee of up to $115 million from the International Development Association; and an investment guarantee of up to $115 million from the Multilateral Investment Guarantee Agency. Financial support for the project also came from the African Development Bank ($110 million) along with the European Investment Bank and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.12</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>159</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.13</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>178</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t6p617.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>187</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
