<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">histrefl</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50003328</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Historical Reflections / Réflexions Historiques</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Berghahn Journals</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03157997</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19392419</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42703740</article-id>
         <article-categories>
            <subj-group>
               <subject>Special Section: (Re)presenting Women, the Female, and the Feminine</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Picturing Politics: Female Political Leaders in France and Norway</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anne</given-names>
                  <surname>Krogstad</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Aagoth</given-names>
                  <surname>Storvik</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">38</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40101372</issue-id>
         <fpage>129</fpage>
         <lpage>152</lpage>
         <permissions>
            <copyright-statement>© 2012 Historical Reflections/Réflexions Historiques and Berghahn Books</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.3167/hrrh.2012.380308"
                   xlink:title="an external site"/>
         <abstract>
            <p>This article explores images of high-level female politicians in France and Norway from 1980 to 2010, examining the ways in which they present themselves to the media and their subsequent reception by journalists. Women in French politics experience difficulties living up to a masculine heroic leadership ideal historically marked by drama, conquest, and seductiveness. In contrast, Norwegian female politicians have challenged the traditional leadership ethos of conspicuous modesty and low-key presentation. We argue that images of French and Norwegian politicians in the media are not only national constructions; they are also gendered. Seven images of women in politics are discussed: (1) men in skirts and ladies of stone, (2) seductresses, (3) different types of mothers, (4) heroines of the past, (5) women in red, (6) glamorous women, and (7) women using ironic femininity. The last three images— color, glamour, and irony— are identified as new strategies female politicians use to accentuate their positions of power with signs of female sensuality. It is thus possible for female politicians to show signs of feminine sensuality and still avoid negative gender stereotyping.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d456e212a1310">
            <label>1</label>
            <mixed-citation id="d456e219" publication-type="other">
James Stanyer and Dominic Wring, "Public Images, Private Lives: An Introduc-
tion," Parliamentary Affairs 57, no. 1 (2004): 1-8.</mixed-citation>
         </ref>
         <ref id="d456e229a1310">
            <label>2</label>
            <mixed-citation id="d456e236" publication-type="other">
Anne Krogstad, Image i politikken: Visuelle og retoriske virkemidler (Oslo: Pax Forlag,
1999).</mixed-citation>
         </ref>
         <ref id="d456e246a1310">
            <label>3</label>
            <mixed-citation id="d456e253" publication-type="other">
Doris A. Graber, Processing Politics: Learning from Television in the Internet Age (Chi-
cago, IL: University of Chicago Press, 2001);</mixed-citation>
            <mixed-citation id="d456e262" publication-type="other">
Dan Schill, "The Visual Image and
the Political Image: Finding a Place for Visual Communication in the Study of
Political Communication" (paper presented at the 2008 International Commu-
nication Association Annual Convention, Montreal, Canada);</mixed-citation>
         </ref>
         <ref id="d456e278a1310">
            <label>4</label>
            <mixed-citation id="d456e285" publication-type="other">
Margaret Scammel, Designer Politics: How Elections are Won (New York: St. Martin's
Press, 1995), 20.</mixed-citation>
         </ref>
         <ref id="d456e296a1310">
            <label>5</label>
            <mixed-citation id="d456e303" publication-type="other">
Marion G. Müller, "What Is Visual Communication? Past and Future of an
Emerging Field of Communication Research/' Studies in Communication Sciences 7,
no. 2 (2007): 13.</mixed-citation>
         </ref>
         <ref id="d456e316a1310">
            <label>6</label>
            <mixed-citation id="d456e323" publication-type="other">
Pippa Norris and Ronald Inglehart, "Culture Barriers to Women's Leadership, " Inter-
national Political Science Association 8, no. 7 (2000): 1-30, http://www0.parlamento
.gub.uy/parlamenta/descargas/BIBLIOGRAFIA/Inglehart%20y%20Norris_
2000.pdf;</mixed-citation>
            <mixed-citation id="d456e338" publication-type="other">
Pamela Paxton and Sheri Kunovich, "Women's Political Repre-
sentation: The Importance of Ideology," Social Forces 82, no. 1 (2003): 87-113.</mixed-citation>
         </ref>
         <ref id="d456e348a1310">
            <label>7</label>
            <mixed-citation id="d456e355" publication-type="other">
Norris and Inglehart, "Culture Barriers," 14.</mixed-citation>
         </ref>
         <ref id="d456e362a1310">
            <label>8</label>
            <mixed-citation id="d456e369" publication-type="other">
Ann Swidler, "Culture in Action: Symbols and Strategies," American Sociological
Review 51, no. 2 (1986): 273-286.</mixed-citation>
         </ref>
         <ref id="d456e379a1310">
            <label>9</label>
            <mixed-citation id="d456e386" publication-type="other">
Michèle Lamont and Laurant Thévenot, Rethinking Comparative Cultural Sociology
(Cambridge: Cambridge University Press, 2000).</mixed-citation>
         </ref>
         <ref id="d456e396a1310">
            <label>10</label>
            <mixed-citation id="d456e403" publication-type="other">
Ibid., 5-6.</mixed-citation>
         </ref>
         <ref id="d456e411a1310">
            <label>11</label>
            <mixed-citation id="d456e418" publication-type="other">
Iddo Tavory and Ann Swidler, "Condom Semiotics: Meaning and Condom Use
in Rural Malawi," American Sociological Review 74, no. 171 (2009): 189.</mixed-citation>
         </ref>
         <ref id="d456e428a1310">
            <label>12</label>
            <mixed-citation id="d456e435" publication-type="other">
Pierre Bourdieu, La domination masculine (Paris: Éditions du Seuil, 1998).</mixed-citation>
         </ref>
         <ref id="d456e442a1310">
            <label>13</label>
            <mixed-citation id="d456e449" publication-type="other">
Beverly Skeggs, Formations of Class and Gender: Becoming Respectable (London:
Sage, 1997);</mixed-citation>
            <mixed-citation id="d456e458" publication-type="other">
Beverly Skeggs, Class, Self Culture (London: Routledge, 2004).</mixed-citation>
         </ref>
         <ref id="d456e465a1310">
            <label>14</label>
            <mixed-citation id="d456e472" publication-type="other">
Helen Drake and John Gaffney, The Language of Leadership in Contemporary France
(Aldershot, UK: Dartmouth, 1996).</mixed-citation>
         </ref>
         <ref id="d456e482a1310">
            <label>15</label>
            <mixed-citation id="d456e489" publication-type="other">
Jean-Pascal Daloz, "Political Elites and Conspicuous Modesty: Norway, Swe-
den, Finland in Comparative Perspective," Comparative Social Research 23 (2007):
171-210;</mixed-citation>
            <mixed-citation id="d456e501" publication-type="other">
Erik Henningsen and Halvard Vike, "Folkelig elitisme? Om offent-
lighetens kultur i Nor ge/Norsk antropologisk tidsskrift 10 (1999): 150-167.</mixed-citation>
         </ref>
         <ref id="d456e511a1310">
            <label>16</label>
            <mixed-citation id="d456e518" publication-type="other">
Anne Krogstad and Aagoth Storvik, "Seductive Heroes and Ordinary Human
Beings: Charismatic Political Leadership in France and Norway/Comparative So-
cial Research 23 (2007): 211-245.</mixed-citation>
         </ref>
         <ref id="d456e532a1310">
            <label>17</label>
            <mixed-citation id="d456e539" publication-type="other">
Jean-Pascal Daloz, "Ostentation in Comparative Perspective: Culture and Elite
Legitimation," Comparative Social Research 21 (2003): 29-62.</mixed-citation>
         </ref>
         <ref id="d456e549a1310">
            <label>18</label>
            <mixed-citation id="d456e556" publication-type="other">
Michèle Lamont, Money, Morals and Manners: The Culture of the French and Ameri-
can Upper Middle Classes (Chicago: University of Chicago Press, 1992), 138.</mixed-citation>
         </ref>
         <ref id="d456e566a1310">
            <label>19</label>
            <mixed-citation id="d456e573" publication-type="other">
David Tjeder, The Power of Character: Middle-Class Masculinities, 1800-1900 (Stock-
holm: Department of History, Stockholm University, 2003).</mixed-citation>
         </ref>
         <ref id="d456e583a1310">
            <label>20</label>
            <mixed-citation id="d456e590" publication-type="other">
Anne Krogstad and Aagoth Storvik, "Reconsidering Politics as a Man's World/
Historical Reflections/Réflexions Historiques 36, no. 3 (2010): 19-38.</mixed-citation>
         </ref>
         <ref id="d456e600a1310">
            <label>21</label>
            <mixed-citation id="d456e607" publication-type="other">
David Niven and Jeremy Zilber, "How Does She Have Time
for Kids and Congress? Views on Gender and Media Coverage from House Of-
fices/Women &amp; Politics 23, nos. 1-2 (2001): 147-165;</mixed-citation>
            <mixed-citation id="d456e619" publication-type="other">
Thomas H. Little, Dana
Dunn, and Rebecca Deen, "A View from the Top: Gender Differences in Priori-
ties Among State Legislative Leaders/Women &amp; Politics 22, no. 4 (2001): 29-50;</mixed-citation>
            <mixed-citation id="d456e631" publication-type="other">
Dianne G. Bystrom and Lynda Lee Kaid, "Are Women Candidates Transform-
ing Campaign Communication? A Comparison of Advertising Videostyles in the
1990s/in Women Transforming Congress, ed. C. S. Rosenthal (Norman: University
of Oklahoma Press, 2002), 156-159;</mixed-citation>
            <mixed-citation id="d456e647" publication-type="other">
Elza Ibroscheva and Maria Raicheva-Sto-
ver, "Engendering Transition: Portrayals of Female Politicians in the Bulgarian
Press/Howard Journal of Communications 20, no. 2 (2009): 111-128;</mixed-citation>
            <mixed-citation id="d456e659" publication-type="other">
Rain-
bow Murray, ed., Cracking the Highest Glass Ceiling: A Global Comparison of Women 's
Campaigns for Executive Office (Santa Barbara, CA: Greenwood Publishing Group,
2010).</mixed-citation>
         </ref>
         <ref id="d456e675a1310">
            <label>22</label>
            <mixed-citation id="d456e682" publication-type="other">
Shauna L. Shames, "The "Un-Candidates": Gender and Outsider Signals in
Women's Political Advertisements/Women &amp; Politics 25, nos. 1-2 (2003): 120.</mixed-citation>
         </ref>
         <ref id="d456e693a1310">
            <label>23</label>
            <mixed-citation id="d456e700" publication-type="other">
Ibroscheva and Raicheva-Stover, "Engendering Transition";</mixed-citation>
            <mixed-citation id="d456e706" publication-type="other">
Kim Kahn, "Does
Gender Make a Difference: An Experimental Examination of Sex Stereotypes
and Press Patterns in Statewide campaigns," American Journal of Political Science
38, no. 1 (1994): 154-174.</mixed-citation>
         </ref>
         <ref id="d456e722a1310">
            <label>24</label>
            <mixed-citation id="d456e729" publication-type="other">
Pippa Norris, Women, Media and Politics (New York: Oxford University Press,
1997), 159.</mixed-citation>
         </ref>
         <ref id="d456e739a1310">
            <label>25</label>
            <mixed-citation id="d456e746" publication-type="other">
Krogstad, Image i politikken, and Anne Krogstad, "Fjernsynsvalgkamp. Noen re-
toriske ovelser i skyld og aere," in I valgkampens hete, ed. B. Aardal, A. Krogstad,
and H. M. Narud (Oslo: Universitetsforlaget, 2004), 85-111.</mixed-citation>
         </ref>
         <ref id="d456e759a1310">
            <label>26</label>
            <mixed-citation id="d456e768" publication-type="other">
Maria Braden, Women Politicians and the Media (Lexington: University of Kentucky
Press, 1996);</mixed-citation>
            <mixed-citation id="d456e777" publication-type="other">
Sean Aday and James Devitt, "Style over Substance: Newspaper
Coverage of Elizabeth Dole's Presidential Bid," Harvard International Journal of
Press/Politics 6, no. 2 (2001): 52-73;</mixed-citation>
            <mixed-citation id="d456e790" publication-type="other">
Ibroscheva and Raicheva -Stover, "Engender-
ing Transition";</mixed-citation>
            <mixed-citation id="d456e799" publication-type="other">
Jackie Stacey, Stargazing: Hollywood Cinema and Female Spectator-
ship (London: Routledge, 1994);</mixed-citation>
            <mixed-citation id="d456e808" publication-type="other">
Kahn, "Does Gender Make a Difference";</mixed-citation>
            <mixed-citation id="d456e814" publication-type="other">
Karen Ross and Annabelle Sreberny, "Women in the House: Media Representa-
tion of British Politicians," in Gender, Politics and Communication, ed. A. Sreberny
and L. van Zoonen (Cresskill, NJ: Hampton Press, 2000), 79-99.</mixed-citation>
         </ref>
         <ref id="d456e827a1310">
            <label>27</label>
            <mixed-citation id="d456e834" publication-type="other">
Harald Baldersheim and Jean-Pascal Daloz, eds., Political Leadership in a Global
Age: The Experiences of France and Norway (Aldershot, UK: Ashgate, 2003).</mixed-citation>
         </ref>
         <ref id="d456e844a1310">
            <label>28</label>
            <mixed-citation id="d456e851" publication-type="other">
Erika Apfelbaum, "Norwegian and French Women in High Leadership Positions:
The Importance of Cultural Contexts upon Gendered Relations," Psychology of
Women Quarterly 17, no. 4 (1993): 409-429.</mixed-citation>
         </ref>
         <ref id="d456e865a1310">
            <label>29</label>
            <mixed-citation id="d456e872" publication-type="other">
Raylene L. Ramsay, "Parity—From Perversion to Political Progress: Changing
Discourses of 'French Exception,'" French Politics 6 (2008): 45-62.</mixed-citation>
         </ref>
         <ref id="d456e882a1310">
            <label>30</label>
            <mixed-citation id="d456e889" publication-type="other">
Ibroscheva and Raicheva-Stover, "Engendering Transition," 116.</mixed-citation>
         </ref>
         <ref id="d456e896a1310">
            <label>31</label>
            <mixed-citation id="d456e903" publication-type="other">
Jean-Pascal Daloz, "The Guises of Political Representation," in Culture Troubles:
Politics and the Interpretation of Meaning, ed. P. Chabal and J.-P. Daloz (London:
Hurst &amp; Company, 2006), 269-308.</mixed-citation>
         </ref>
         <ref id="d456e916a1310">
            <label>32</label>
            <mixed-citation id="d456e923" publication-type="other">
The main sources here are Daloz, "Ostentation";</mixed-citation>
            <mixed-citation id="d456e929" publication-type="other">
Daloz, "Political Elites";</mixed-citation>
            <mixed-citation id="d456e935" publication-type="other">
Jean-
Pascal Daloz, "Between Majesty and Proximity: The Enduring Ambiguities of
Political Representation in France," French Politics 6 (2008): 302-320;</mixed-citation>
            <mixed-citation id="d456e948" publication-type="other">
Krogstad
and Storvik, "Seductive Heroes";</mixed-citation>
            <mixed-citation id="d456e957" publication-type="other">
Krogstad and Storvik, "Reconsidering
Politics."</mixed-citation>
         </ref>
         <ref id="d456e967a1310">
            <label>33</label>
            <mixed-citation id="d456e974" publication-type="other">
Michael Quinn Patton, Qualitative Evaluation and Research Methods (Newbury
Park, CA: Sage, 1990), 174-175.</mixed-citation>
         </ref>
         <ref id="d456e984a1310">
            <label>34</label>
            <mixed-citation id="d456e991" publication-type="other">
Hege Skjeie, "Politisk lederskap. Idealer og vrengebilder," Nytt Norsk Tidsskrift 2
(1992): 118-135;</mixed-citation>
            <mixed-citation id="d456e1000" publication-type="other">
Torild Skard, Maktens kvinner. Verdens kvinnelige presidenter og
statsministre 1960-2010 (Oslo: Universitetsforlaget, 2012), 132.</mixed-citation>
         </ref>
         <ref id="d456e1011a1310">
            <label>35</label>
            <mixed-citation id="d456e1018" publication-type="other">
R. Hirsti, Gro - midt i livet (Oslo: Tiden Norsk Forlag, 1989), 8.</mixed-citation>
         </ref>
         <ref id="d456e1025a1310">
            <label>36</label>
            <mixed-citation id="d456e1032" publication-type="other">
Raylene L. Ramsay, French Women in Politics: Writing Power, Paternal Legitimization
and Maternal Legacies (New York: Berghahn Books, 2003), 196.</mixed-citation>
         </ref>
         <ref id="d456e1042a1310">
            <label>37</label>
            <mixed-citation id="d456e1049" publication-type="other">
Ibid., 159.</mixed-citation>
         </ref>
         <ref id="d456e1056a1310">
            <label>38</label>
            <mixed-citation id="d456e1063" publication-type="other">
Ibid.</mixed-citation>
         </ref>
         <ref id="d456e1070a1310">
            <label>39</label>
            <mixed-citation id="d456e1077" publication-type="other">
Ibid., 191. This statement could be considered as rhetorical.</mixed-citation>
         </ref>
         <ref id="d456e1084a1310">
            <label>40</label>
            <mixed-citation id="d456e1091" publication-type="other">
Dagbladet, 28 September 2002.</mixed-citation>
         </ref>
         <ref id="d456e1099a1310">
            <label>41</label>
            <mixed-citation id="d456e1106" publication-type="other">
Le Monde, 31 January 2006;</mixed-citation>
            <mixed-citation id="d456e1112" publication-type="other">
Paris Match, 15 March 2006.</mixed-citation>
         </ref>
         <ref id="d456e1119a1310">
            <label>42</label>
            <mixed-citation id="d456e1126" publication-type="other">
Femmeactuelle, 21 February 2006.</mixed-citation>
         </ref>
         <ref id="d456e1133a1310">
            <label>43</label>
            <mixed-citation id="d456e1140" publication-type="other">
Ramsay, French Women in Politics.</mixed-citation>
         </ref>
         <ref id="d456e1147a1310">
            <label>44</label>
            <mixed-citation id="d456e1154" publication-type="other">
Dagsavisen, 22 November 2005.</mixed-citation>
         </ref>
         <ref id="d456e1161a1310">
            <label>45</label>
            <mixed-citation id="d456e1168" publication-type="other">
Krogstad, Image i politikken, and Krogstad, "Fjernsynsvalgkamp."</mixed-citation>
         </ref>
         <ref id="d456e1175a1310">
            <label>46</label>
            <mixed-citation id="d456e1182" publication-type="other">
Standpoint Magazine, 23 January 2009.</mixed-citation>
         </ref>
         <ref id="d456e1190a1310">
            <label>47</label>
            <mixed-citation id="d456e1197" publication-type="other">
Mail Online, 12 March 2008, accessed 13 June 2012, http://www.dailymail.co
.uk/femail/article-531272/Sarkozys-sirens-Why-French-politicians-glamorous-
ours.html.</mixed-citation>
         </ref>
         <ref id="d456e1210a1310">
            <label>48</label>
            <mixed-citation id="d456e1217" publication-type="other">
VG, 29 October 2005.</mixed-citation>
         </ref>
         <ref id="d456e1224a1310">
            <label>49</label>
            <mixed-citation id="d456e1231" publication-type="other">
Dagsavisen, 22 November 2005.</mixed-citation>
         </ref>
         <ref id="d456e1238a1310">
            <label>50</label>
            <mixed-citation id="d456e1245" publication-type="other">
Dagbladet, 21 September 2002.</mixed-citation>
         </ref>
         <ref id="d456e1252a1310">
            <label>51</label>
            <mixed-citation id="d456e1259" publication-type="other">
Mary Poovey, The Proper Lady and the Woman Writer: Ideology as Style in the Works
of Mary Wollenstonecraft, Mary Shelly, and Jane Austen (Chicago: University of Chi-
cago Press, 1984).</mixed-citation>
         </ref>
         <ref id="d456e1272a1310">
            <label>52</label>
            <mixed-citation id="d456e1279" publication-type="other">
Ramsay, French Women in Politics, 183.</mixed-citation>
         </ref>
         <ref id="d456e1287a1310">
            <label>53</label>
            <mixed-citation id="d456e1294" publication-type="other">
Krogstad and Storvik, "Seductive Heroes."</mixed-citation>
         </ref>
         <ref id="d456e1301a1310">
            <label>54</label>
            <mixed-citation id="d456e1308" publication-type="other">
Mary Ann Doane, Femmes Fatales: Feminism, Film Theory, Psychoanalysis (New
York: Routledge, 1991), 2.</mixed-citation>
         </ref>
         <ref id="d456e1318a1310">
            <label>55</label>
            <mixed-citation id="d456e1325" publication-type="other">
Poovey, The Proper Lady.</mixed-citation>
         </ref>
         <ref id="d456e1332a1310">
            <label>56</label>
            <mixed-citation id="d456e1339" publication-type="other">
Lynn Hunt, ed., Eroticism and the Body Politic (Baltimore, MD: Johns Hopkins Uni-
versity Press, 1991).</mixed-citation>
         </ref>
         <ref id="d456e1349a1310">
            <label>57</label>
            <mixed-citation id="d456e1356" publication-type="other">
Skeggs, Formations of Class and Gender.</mixed-citation>
         </ref>
         <ref id="d456e1363a1310">
            <label>58</label>
            <mixed-citation id="d456e1370" publication-type="other">
Ibid., 100.</mixed-citation>
         </ref>
         <ref id="d456e1378a1310">
            <label>59</label>
            <mixed-citation id="d456e1385" publication-type="other">
Kathleen Dolan, "How the Public Views Women Candidates/in Women and Elec-
tive Office: Past, Present and Future, ed. S. Thomas and C. Wilcox (Oxford: Oxford
University Press, 2005), 41-59;</mixed-citation>
            <mixed-citation id="d456e1397" publication-type="other">
Rainbow Murray, "Ségolène Royal and Gendered
Leadership in France," in The Presidents of the French Fifth Republic, eds. David S.
Bell and John Gaffney (London: Palgrave Macmillan, 2013 [forthcoming]).</mixed-citation>
         </ref>
         <ref id="d456e1410a1310">
            <label>60</label>
            <mixed-citation id="d456e1417" publication-type="other">
Skeggs, Formations of Class and Gender, 66.</mixed-citation>
         </ref>
         <ref id="d456e1424a1310">
            <label>61</label>
            <mixed-citation id="d456e1431" publication-type="other">
Le Figaro, 13 April 2007;</mixed-citation>
            <mixed-citation id="d456e1437" publication-type="other">
Le Monde, 17 April 2007.</mixed-citation>
         </ref>
         <ref id="d456e1444a1310">
            <label>62</label>
            <mixed-citation id="d456e1451" publication-type="other">
Beverly H. Burris, "Technocracy, Patriarchy and Management," in Men as Manag-
ers, Managers as Men: Critical Perspectives on Men, Masculinities and Managements, ed.
D. L. Collinson and J. Hearn (London: Sage Publications, 1996), 61-77, esp. 67.</mixed-citation>
         </ref>
         <ref id="d456e1464a1310">
            <label>63</label>
            <mixed-citation id="d456e1471" publication-type="other">
Apfelbaum, "Norwegian and French Women."</mixed-citation>
         </ref>
         <ref id="d456e1478a1310">
            <label>64</label>
            <mixed-citation id="d456e1485" publication-type="other">
Lamont, Money, Morals, and Manners, 137.</mixed-citation>
         </ref>
         <ref id="d456e1493a1310">
            <label>65</label>
            <mixed-citation id="d456e1500" publication-type="other">
Rune Slagstad, De nasjonale strateger (Oslo: Pax, 1998).</mixed-citation>
         </ref>
         <ref id="d456e1507a1310">
            <label>66</label>
            <mixed-citation id="d456e1514" publication-type="other">
Shames, "The Un-Candidates," 127.</mixed-citation>
         </ref>
         <ref id="d456e1521a1310">
            <label>67</label>
            <mixed-citation id="d456e1528" publication-type="other">
Krogstad, "Fjernsynsvalgkamp."</mixed-citation>
         </ref>
         <ref id="d456e1535a1310">
            <label>68</label>
            <mixed-citation id="d456e1542" publication-type="other">
Stacey, Stargazing.</mixed-citation>
         </ref>
         <ref id="d456e1549a1310">
            <label>69</label>
            <mixed-citation id="d456e1556" publication-type="other">
Erving Goffman, Encounters: Two Studies in the Sociology of Interaction (Indianapolis,
IN: Bobbs-Merrill, 1961).</mixed-citation>
         </ref>
         <ref id="d456e1566a1310">
            <label>70</label>
            <mixed-citation id="d456e1573" publication-type="other">
Stacey, Stargazing, 7.</mixed-citation>
         </ref>
         <ref id="d456e1581a1310">
            <label>71</label>
            <mixed-citation id="d456e1588" publication-type="other">
Ibid., 237.</mixed-citation>
         </ref>
         <ref id="d456e1595a1310">
            <label>72</label>
            <mixed-citation id="d456e1602" publication-type="other">
Rosabeth Moss Kanter, Men and Women of the Corporation (New York: Basic Books,
1977).</mixed-citation>
         </ref>
         <ref id="d456e1612a1310">
            <label>73</label>
            <mixed-citation id="d456e1619" publication-type="other">
Anna Kontula, "The Sex Worker and Her Pleasure," Current Sociology 56, no. 4
(2008): 605-620;</mixed-citation>
            <mixed-citation id="d456e1628" publication-type="other">
Miriam Adelman and Lennita Ruggi, "The Beautiful and
the Abject," Current Sociology 56, no. 4 (2008): 555-586.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
