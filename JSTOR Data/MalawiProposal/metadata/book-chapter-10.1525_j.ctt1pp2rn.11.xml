<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1pp2rn</book-id>
      <subj-group>
         <subject content-type="call-number">QK101.S28 1988</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Plants</subject>
         <subj-group>
            <subject content-type="lcsh">Migration</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Vegetation dynamics</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Phytogeography</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Botany &amp; Plant Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Plant Migration</book-title>
         <subtitle>The Dynamics of Geographic Patterning in Seed Plant Species</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Sauer</surname>
               <given-names>Jonathan D.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>08</day>
         <month>02</month>
         <year>1988</year>
      </pub-date>
      <isbn content-type="ppub">9780520060036</isbn>
      <isbn content-type="epub">9780520909861</isbn>
      <publisher>
         <publisher-name>University of California Press</publisher-name>
         <publisher-loc>Berkeley; Los Angeles; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>1988</copyright-year>
         <copyright-holder>The Regents of the University of California</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.1525/j.ctt1pp2rn"/>
      <abstract abstract-type="short">
         <p>Using cases of plant migration documented by both historical and fossil evidence, Jonathan D. Sauer provides a landmark assessment of what is presently known, and not merely assumed, about the process.</p>
      </abstract>
      <counts>
         <page-count count="298"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.4</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In different branches of biogeography, the term<italic>migration</italic>is applied to such diverse processes as diurnal vertical movements of plankton, mass seasonal journeys of animal populations, and gradual advances and retreats of plant species borders with changing climate. All these fit under a broad definition of migration as any change in spatial distribution of a species or other taxon through time. In this sense, organisms in general are capable of migration, although mobile stages in the life cycles are quite different. In the case of seed plants, the new diploid embryo is normally mobile while juveniles and adults are sedentary,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>SHORELINE AND OTHER NATURALLY OPEN HABITATS</title>
                  </title-group>
                  <fpage>11</fpage>
                  <abstract>
                     <p>By definition, mangroves are trees rooted in substrates that are flooded by seawater, either constantly or periodically. Mangroves belong to many different angiosperm families, some of which also include upland genera, but all the mangrove genera are tightly confined to their saline swamp habitat. Most mangrove genera are tropical, with a minority ranging into the subtropics. Members of the genus<italic>Avicennia</italic>, a tree belonging to the Verbena family, range farthest into temperate regions. Within their latitudinal limits, mangroves have very broad edaphic and climatic tolerances. Because they are able to draw water molecules and nutrients from the sea, they are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>VEGETATION SUBJECT TO NATURAL PERTURBATIONS</title>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Along the Arctic timberline in Canada and in the absence of fire, the dominant black spruce,<italic>Picea mariana</italic>, can maintain itself indefinitely by vegetative reproduction, regardless of weather. The forests at timberline are probably destroyed by fire at intervals of about 100 to 200 years on the average. Abundant viable seed is released from cones of the burned trees; if temperatures are above normal, a new forest is rapidly established without migration. However, seed germination is suppressed by low temperatures, and after a few consecutive years that are cooler than average, no viable seed remains. The burned area can then</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>INVASION AND ELIMINATION IN ESTABLISHED VEGETATION</title>
                  </title-group>
                  <fpage>82</fpage>
                  <abstract>
                     <p>Upland vegetation in California varies strikingly in vulnerability to exotic invasion. Chaparral and forest communities are remarkably impervious to invasion, even after fire. Innumerable shrub and tree species have been introduced to California from other Mediterranean climatic regions without naturalizing. For example, European natives widely planted in California gardens include<italic>Pinus halepensis</italic>,<italic>Quercus suber</italic>,<italic>Ceratonia siliqua</italic>,<italic>Spartium junceum</italic>,<italic>Myrtus communis</italic>, and<italic>Rosmarinus officinalis</italic>. Some of these escape in artificially disturbed habitats, but they do not displace native woody species. By contrast, California grasslands are now dominated by exotic grasses and dicot herbs that began immigrating with the beginning of Spanish</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>ARTIFICIALLY MODIFIED HABITATS</title>
                  </title-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>In the original Latin meaning, ruderals are plants that grow on ruins and rubble. The word will be used here in a broader sense to include weeds of dump heaps, urban wasteland, docks, pathways, railroads, roadsides, and other places heavily impacted by human habitation, industry, and commerce. The term is sometimes defined even more broadly to include all plant followers of humans or colonizers of artificially disturbed sites (Frenkel 1977). Agricultural and pastoral weeds, however, will be considered under other headings below.</p>
                     <p>Archaeological sites can commonly be recognized, even in aerial photographs, by distinctive vegetation patterns. A clear example is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>DISCUSSION OF MODERN AND HISTORICAL MIGRATIONS</title>
                  </title-group>
                  <fpage>139</fpage>
                  <abstract>
                     <p>In the abstract model presented in the Introduction, geographic patterning of seed plant species is the product of interplay between opposing forces tending to expand or restrict the distribution. The main centrifugal force is usually seed dispersal; the main centripetal force is environmental selection. When dispersal and environment are sufficiently stable, fluctuating within normal limits, these forces eventually equilibrate and the species borders become static. Migration, whether advance or retreat, occurs when equilibrium is upset by changes in dispersal or environment or both. The case histories that have been discussed thus far show a variety of migration patterns resulting from</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.10</book-part-id>
                  <title-group>
                     <title>[PART II Introduction]</title>
                  </title-group>
                  <fpage>143</fpage>
                  <abstract>
                     <p>There is a large body of literature in biogeography that attempts to deduce prehistoric migrations of taxa solely from their present distribution patterns. Strangely, such deductive reconstructions, by long tradition, have come to be called historical plant geography, although as Stott (1981) pointed out, they are not generally based on any genuine historical evidence. Some of these reconstructions assume that through geologic time plant ranges have simply expanded and contracted around centers of origin or refuge. Where present distributions of taxa are disjunct, there has been endless contention between proponents of two polar a priori explanations: long-range dispersal versus fragmentation</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>LAST GLACIAL AND HOLOCENE</title>
                  </title-group>
                  <fpage>145</fpage>
                  <abstract>
                     <p>This period offers the most readable chapter in all of paleobotany. Any fossil evidence, however, involves formidable problems not shared with modern historical evidence. These problems have been carefully examined by Davis (1978) and Watts (1978). Only a few of the more intractable ones will be briefly noted here.</p>
                     <p>
                        <italic>Identification of Taxa</italic>. The basic units of the fossil record are not ordinarily whole plants or assemblages of organs, as on herbarium specimens, but disarticulated parts. The great bulk of the seed plant record consists of pollen grains, the domain of palynology. The most abundant macrofossils are usually individual leaves or</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>PLEISTOCENE BEFORE LAST GLACIAL</title>
                  </title-group>
                  <fpage>180</fpage>
                  <abstract>
                     <p>The fossil record becomes more and more fragmented with increasing time depth. Also, time before the Last Glacial is beyond the reach of<sup>14</sup>C dating. Eventually it may be possible to establish a solid global chronology based on other kinds of dating, including seafloor magnetic reversals, such as the Jaramillo and Olduvai events. The terrestrial fossil record, however, has not yet been tied to marine chronology, and stratigraphic correlations between local series remain questionable. Furthermore, identification of fragmentary fossils with living taxa, particularly at the species level, becomes increasingly tentative with time depth. Nevertheless, the fossil record does give some</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>NEOGENE (MIOCENE AND PLIOCENE)</title>
                  </title-group>
                  <fpage>186</fpage>
                  <abstract>
                     <p>The Neogene Period spans over 20 million years, more than ten times the span of the whole Quaternary (Table 3). The fossil record reveals only glimpses separated by great gaps in space and time. However, there are some rich macrofossil deposits with floras that are almost all identifiable with living genera; many members appear to be closely related or identical to living species. In paleobotany, Tertiary and older fossils are usually given discrete species names in separate time levels, partly because their fragmentary nature prevents complete comparisons. Since the fossil binomials are meaningless to nonpaleobotanists, however, a common practice (which</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>THE DEEP PAST</title>
                  </title-group>
                  <fpage>195</fpage>
                  <abstract>
                     <p>The Neogene and later periods discussed above cover less than one-tenth of the time that seed plants have existed. Their fossil record, which has great gaps in space and time, began in the Devonian Period (Table 7), more than 300 million years before the Neogene. Unlike the Neogene, for which nearly all fossil plants can be identified to living genera, if not species, the cast of characters becomes increasingly strange wtih greater age. More often than not, it is highly speculative whether these strange creatures were evolutionary dead ends or primitive ancestors of much changed modern descendants. Moreover, paleontologists not</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.15</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>DISCUSSION OF PREHISTORIC MIGRATIONS</title>
                  </title-group>
                  <fpage>212</fpage>
                  <abstract>
                     <p>The clarity with which plant migrations can be resolved from the fossil record decreases drastically with increasing time into the past.</p>
                     <p>In the Late Glacial and Holocene fossil record, plants are generally assignable to living genera, if not species. There are many gaps and uncertainties, but the record is good enough to establish the presence or absence of dominant plant taxa over substantial areas and time spans. In so doing, the record reveals that in postglacial time, plant species carried out amazingly rapid and highly individualistic migrations. This fact conflicts with conceptual models that visualize vegetational associations or even whole</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.16</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>DEDUCTIVE INTERRELATIONSHIPS</title>
                  </title-group>
                  <fpage>217</fpage>
                  <abstract>
                     <p>In any sexually reproducing species, each generation can be expected to differ at least slightly from the parents in both spatial distribution and hereditary characters. It is easy to imagine the processes of migration and evolution proceeding simultaneously and interactively.</p>
                     <p>In the Introduction, the abstract model of migration considered the process to be the result of interplay between opposing forces:</p>
                     <p>+ centrifugal force (mainly seed dispersal) tending to expand the area occupied by the species versus</p>
                     <p>— centripetal force (mainly environmental selection) tending to limit the area occupied by the species.</p>
                     <p>This obviously parallels the orthodox abstract model of evolution</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.17</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>CASE HISTORIES OF EVOLUTION ASSOCIATED WITH MIGRATION</title>
                  </title-group>
                  <fpage>219</fpage>
                  <abstract>
                     <p>As before, case histories will be restricted to spontaneous populations; evolution of domesticated plants will be excluded. Also, as before, only case histories with actual evidence of change through time will be used. Unfortunately, this includes mostly very recent changes because the fossil record is too incomplete to tell whether similar but different fossils from different time levels and regions were connected in a direct ancestor–descendant lineage or were on separate migrational and evolutionary pathways.</p>
                     <p>Studies of plant colonization on historical mine wastes in England and Wales have shown that toxic concentrations of the heavy metals lead, copper, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.18</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>MIGRATION WITHOUT EVOLUTION?</title>
                  </title-group>
                  <fpage>230</fpage>
                  <abstract>
                     <p>The preceding chapter offered only a few examples of the interplay between the processes of migration and evolution. The meagerness of the record can be attributed partly to the recency of efforts to observe genetic changes in natural plant populations, the difficulty of monitoring such changes, and the limited manpower devoted to the task. There can be no doubt that the processes can interact. Obviously, evolution of a successful novel adaptation, whether for dispersal or survival, allows geographic expansion of a species. In the few widespread species that have been carefully studied genetically, local races or ecotypes have commonly evolved</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.19</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>CONCLUSION</title>
                  </title-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>We have seen that similar plant distribution patterns often develop by convergent rather than parallel migrations. Let me recall three examples on different time scales:</p>
                     <p>1. The conifer genera<italic>Podocarpus</italic>and<italic>Araucaria</italic>have modern ranges that suggest fragments of similar Gondwanaland ranges. The Mesozoic fossil record shows that<italic>Podocarpus</italic>was confined to Gondwanaland but that<italic>Araucaria</italic>was cosmopolitan in both the northern and southern hemispheres (Florin 1963).</p>
                     <p>2. A rich association of forest conifers now ranges throughout the Great Basin of western North America on high mountains that provide habitable islands in a sea of desert. During the Last Glacial, some of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.20</book-part-id>
                  <title-group>
                     <title>Appendix</title>
                  </title-group>
                  <fpage>237</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.21</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>249</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1pp2rn.22</book-part-id>
                  <title-group>
                     <title>INDEX TO GENERA</title>
                  </title-group>
                  <fpage>277</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
