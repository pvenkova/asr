<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7v94b</book-id>
      <subj-group>
         <subject content-type="call-number">JF1083.S34.2008</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Elections</subject>
         <subj-group>
            <subject content-type="lcsh">Corrupt practices</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Elections</subject>
         <subj-group>
            <subject content-type="lcsh">Corrupt practices</subject>
            <subj-group>
               <subject content-type="lcsh">United States</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Voting</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Voting</subject>
         <subj-group>
            <subject content-type="lcsh">United States</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Hidden Costs of Clean Election Reform</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Schaffer</surname>
               <given-names>Frederic Charles</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>29</day>
         <month>05</month>
         <year>2008</year>
      </pub-date>
      <isbn content-type="ppub">9780801441158</isbn>
      <isbn content-type="epub">9780801461798</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>Ithaca; London</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2008</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.ctt7v94b"/>
      <abstract abstract-type="short">
         <p>American voters are increasingly aware that the mechanics of elections matter. The conduct of elections-how eligible voters make it onto the voter rolls, how voters cast their ballots, and how those votes are counted-determines the degree to which the people's preferences are expressed freely, weighed equally, and recorded accurately. It is not surprising, then, that attempts to "clean up" elections are widely applauded as being unambiguously good for democracy.</p>
         <p>In<italic>The Hidden Costs of Clean Election Reform</italic>, Frederic Charles Schaffer reveals how tinkering with the electoral process can easily damage democratic ideals. Drawing on both recent and historical evidence from the United States and countries around the world, including the Philippines (where Schaffer has served as an election observer), Venezuela, South Africa, and Taiwan,<italic>The Hidden Costs of Clean Election Reform</italic>investigates why citizens sometimes find themselves abruptly disenfranchised. Schaffer examines numerous incidents in which election reforms have, whether intentionally or accidentally, harmed the quality and experience of democracy.</p>
         <p>These cases include the introduction of secret balloting in 1890s Arkansas, which deliberately stripped black citizens of the power to vote; efforts to insulate voters from outside influences in nineteenth-century France; the purge of supposed felons from the voter rolls of Florida ahead of the 2000 presidential election; and current debates over the reliability and security of touch-screen voting machines. Lawmakers, election officials, partisan operatives, and civic educators, Schaffer finds, can all contribute to the harm caused by improperly or cynically constructed election reforms. By understanding how even good-faith efforts to improve corrupt or flawed electoral practices may impede the democratic process,<italic>The Hidden Costs of Clean Election Reform</italic>suggests new ways to help prevent future breaches of democracy.</p>
      </abstract>
      <counts>
         <page-count count="264"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.4</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>xvii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>When Good Reforms Go Bad</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract abstract-type="extract">
                     <p>Poll fraud and error take a variety of forms. Ineligible voters can register illegally. Eligible voters can register in more than one locale. Spurious voters can impersonate registered ones, often those who died recently or are out of town. Campaign operatives can intimidate voters, buy their votes, or commandeer polling stations. Poll workers can stuff ballot boxes or miscount votes. Election officials can doctor or incompetently maintain the voter registry. Canvassers can intentionally pad or shave vote totals or make inadvertent tallying mistakes.</p>
                     <p>Such forms of fraud and error afflict many post-World War II quasi and emerging democracies. In some</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Lawmakers:</title>
                     <subtitle>Legal Disenfranchisement</subtitle>
                  </title-group>
                  <fpage>21</fpage>
                  <abstract abstract-type="extract">
                     <p>Lawmakers sometimes enact clean election measures that cause vote depression. Typically, this depression occurs when legislators impose regulations upon people who differ in their ability or disposition to comply with those regulations out of fear, distrust, embarrassment, political conviction, financial constraint, or the like. These differences effectively put up obstacles to the electoral participation of some people but not others. While such obstacles do not make it impossible for those affected to vote, they do make it costly, stifling, shameful, frightening, or inconvenient to do so. The regulations themselves can range from mandatory secret balloting to restrictions on how ballots</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Election Officials and Poll Workers:</title>
                     <subtitle>Administrative Exclusion</subtitle>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract abstract-type="extract">
                     <p>Clean election laws, as we have seen, sometimes become vehicles of disenfranchisement. But the depressive effect of new clean election measures can crystallize not only at the moment of formulation but also at the stage of implementation. In an effort to clean up elections, lawmakers often legislate new rules with no obvious disenfranchising import. Nevertheless, election officials and poll workers might enforce them unevenly, apply them falteringly, or subvert them altogether in ways that result in vote depression. I have called these various modes of depressive implementation “administrative exclusion” for short.</p>
                     <p>Administrative exclusion can result from either manipulation ( the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Parties, Candidates, and Their Agents:</title>
                     <subtitle>Partisan Demobilization</subtitle>
                  </title-group>
                  <fpage>98</fpage>
                  <abstract abstract-type="extract">
                     <p>Sometimes clean election reforms damage the quality of democracy directly. When lawmakers enact restrictive voter registration procedures, participation rates decline as a direct result. When election administrators remove eligible voters from the registry, people are immediately excluded. Sometimes, however, the damage to democracy is indirect. Lawmakers and election officials put in place clean election reforms that cause parties, candidates, and their agents to alter their campaign strategies, and it is the adoption of these new strategies that keeps people away from the polls.</p>
                     <p>In this chapter, I focus on the reaction of political parties, candidates, and agents to clean election</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Civic Educators:</title>
                     <subtitle>Disciplinary Reaction</subtitle>
                  </title-group>
                  <fpage>125</fpage>
                  <abstract abstract-type="extract">
                     <p>Election watchdog groups, public-minded corporations, government election bodies, reformist political parties, and other civic educators sometimes try to clean up dirty electoral practices by teaching ordinary voters to change their behavior. The goal of this education, typically, is to convince people to obey existing election laws, whether it be to vote secretly, to refrain from selling their votes, or to resist the temptation of voting more than once.</p>
                     <p>To the extent that civic educators wish to train voters to act “correctly,” their efforts have a disciplinary component. This reality suggests a need to understand how people who are the target</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Remedies</title>
                  </title-group>
                  <fpage>150</fpage>
                  <abstract abstract-type="extract">
                     <p>In this book I have explored how clean election reforms—reforms designed to reduce fraud and error in the casting and counting of votes—have sometimes damaged the quality of democracy in a range of cases, both historical and contemporary. I found, in fact, three broad ways in which reform can harm democracy.</p>
                     <p>Clean election reform can result in <italic>vote depression</italic> when it causes a drop in the number of people who register, turn out to vote, or have their votes count. It is the form of iatrogenic harm that has received most attention in the book because it is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.11</book-part-id>
                  <title-group>
                     <title>Appendix:</title>
                     <subtitle>Variables and Cases</subtitle>
                  </title-group>
                  <fpage>197</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.12</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>217</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7v94b.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
