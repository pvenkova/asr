<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">souteconj</journal-id>
         <journal-id journal-id-type="jstor">j100373</journal-id>
         <journal-title-group>
            <journal-title>Southern Economic Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Southern Economic Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">00384038</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/20111844</article-id>
         <title-group>
            <article-title>Testing Monetary Policy Intentions in Open Economies</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Jim</given-names>
                  <surname>Granato</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Melody</given-names>
                  <surname>Lo</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>M. C. Sunny</given-names>
                  <surname>Wong</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">72</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i20111829</issue-id>
         <fpage>730</fpage>
         <lpage>746</lpage>
         <permissions>
            <copyright-statement>Copyright 2006 Southern Economic Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20111844"/>
         <abstract>
            <p>Temple (2002) argues that the inflation level used in Romer (1993) lacks power in revealing the policy intentions of monetary authorities. Temple also points out that Romer's use of the openness--inflation correlation cannot be explained by time consistency theory. In this article, we demonstrate that more open economies experience less inflation volatility and persistence. We attribute our findings to the hypothesis that monetary authorities in more open economies adopt more aggressive monetary policies. This pattern emerges strongly after 1990. Our results indicate that the near-universal regime shift in 1990 is not just a simple process of increased monetary policy aggressiveness, but an increased response to economic openness.</p>
         </abstract>
         <kwd-group>
            <kwd>E31</kwd>
            <kwd>E52</kwd>
            <kwd>F41</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d568e150a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d568e157" publication-type="other">
Romer (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e163" publication-type="other">
Lane (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e169" publication-type="other">
Romer's (1993)</mixed-citation>
            </p>
         </fn>
         <fn id="d568e176a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d568e183" publication-type="other">
Alogoskoufis and Smith (1991)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d568e189" publication-type="other">
Burdekin and Siklos (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d568e196a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d568e203" publication-type="other">
Phillips (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d568e210a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d568e217" publication-type="other">
Akaike
(1974)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d568e236a1310">
            <mixed-citation id="d568e240" publication-type="other">
Akaike, Hirotugu. 1974. A new look at the statistical model identification. IEEE Transactions on Automatic Control AC-19:
716-23.</mixed-citation>
         </ref>
         <ref id="d568e250a1310">
            <mixed-citation id="d568e254" publication-type="other">
Alogoskoufis, George S., and Ron Smith. 1991. The Phillips curves, the persistence of inflation, and the Lucas critique: Evidence
from exchange-rate regimes. American Economic Review 81:1254-75.</mixed-citation>
         </ref>
         <ref id="d568e264a1310">
            <mixed-citation id="d568e268" publication-type="other">
Andrews, Donald W. K. 1993. Tests for parameter instability and structural change with unknown change point. Econometrica
61:821-56.</mixed-citation>
         </ref>
         <ref id="d568e278a1310">
            <mixed-citation id="d568e282" publication-type="other">
Bernanke, Ben S., Thomas Laubach, Frederic S. Mishkin, and Adam Posen. 1999. Inflation targeting. Princeton, NJ: Princeton
University Press.</mixed-citation>
         </ref>
         <ref id="d568e293a1310">
            <mixed-citation id="d568e297" publication-type="other">
Burdekin, C. K. Richard, and Pierre L. Siklos. 1999. Exchange rate regimes and shifts in inflation persistence: Does nothing else
matter? Journal of Money, Credit and Banking 31:235-47.</mixed-citation>
         </ref>
         <ref id="d568e307a1310">
            <mixed-citation id="d568e311" publication-type="other">
Cecchetti, Stephen G., and Michael Ehrmann. 2002. Does inflation targeting increase output volatility? An international
comparison of policymakers' preferences and outcomes. In Monetary policy: Rules and transmission mechanisms. Series
on Central Banking, Analysis, and Economic Policies, volume 4. Santiago: Central Bank of Chile, pp. 247-74.</mixed-citation>
         </ref>
         <ref id="d568e324a1310">
            <mixed-citation id="d568e328" publication-type="other">
Clarida, Richard, Jordi Gali, and Mark Gertler. 1999. The science of monetary policy: A new Keynesian perspective. Journal of
Economic Literature 37:1661-707.</mixed-citation>
         </ref>
         <ref id="d568e338a1310">
            <mixed-citation id="d568e342" publication-type="other">
Clarida, Richard, Jordi Gali, and Mark Gertler. 2000. Monetary policy rules and macroeconomic stability: Evidence and some
theory. Quarterly Journal of Economics 115:147-80.</mixed-citation>
         </ref>
         <ref id="d568e352a1310">
            <mixed-citation id="d568e356" publication-type="other">
Clarida, Richard, Jordi Gali, and Mark Gertler. 2001. Optimal monetary policy in open versus closed economies: An integrated
approach. AEA Papers and Proceedings 91:248-52.</mixed-citation>
         </ref>
         <ref id="d568e366a1310">
            <mixed-citation id="d568e370" publication-type="other">
Clarida, Richard, Jordi Gali, and Mark Gertler. 2002. A simple framework for international monetary policy analysis. Journal of
Monetary Economics 49:879-904.</mixed-citation>
         </ref>
         <ref id="d568e381a1310">
            <mixed-citation id="d568e385" publication-type="other">
Elliott, Graham, Thomas J. Rothenberg, and James H. Stock. 1996. Efficient tests for an autoregressive unit root. Econometrica
64:813-36.</mixed-citation>
         </ref>
         <ref id="d568e395a1310">
            <mixed-citation id="d568e399" publication-type="other">
Frankel, Jeffrey A., and Andrew K. Rose. 1996. A panel project on purchasing power parity: Mean reversion within and between
countries. Journal of International Economics 40:209-24.</mixed-citation>
         </ref>
         <ref id="d568e409a1310">
            <mixed-citation id="d568e413" publication-type="other">
Fuhrer, Jeffrey C. 1995. The persistence of inflation and the cost of disinflation. New England Economic Review February:3-16.</mixed-citation>
         </ref>
         <ref id="d568e420a1310">
            <mixed-citation id="d568e424" publication-type="other">
Fuhrer, Jeffrey C., and George R. Moore. 1995. Inflation persistence. Quarterly Journal of Economics 110:127-59.</mixed-citation>
         </ref>
         <ref id="d568e431a1310">
            <mixed-citation id="d568e435" publication-type="other">
Geweke, John, and Susan Porter-Hudak. 1983. The estimation and application of long memory time series models. Journal of
Time Series Analysis 4:221-38.</mixed-citation>
         </ref>
         <ref id="d568e445a1310">
            <mixed-citation id="d568e449" publication-type="other">
Hassler, Uwe, and Jurgen Wolters. 1994. On the power of unit root tests against fractional alternatives. Economics Letters 45:
1-5.</mixed-citation>
         </ref>
         <ref id="d568e460a1310">
            <mixed-citation id="d568e464" publication-type="other">
Hendry, David F. 1995. Dynamic econometrics. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d568e471a1310">
            <mixed-citation id="d568e475" publication-type="other">
Kydland, Finn E., and Edward C. Prescott. 1977. Rules rather than discretion: The inconsistency of optimal plans. Journal of
Political Economy 85:473-91.</mixed-citation>
         </ref>
         <ref id="d568e485a1310">
            <mixed-citation id="d568e489" publication-type="other">
Lane, Phillip R. 1997. Inflation in open economies. Journal of International Economics 42:327-47.</mixed-citation>
         </ref>
         <ref id="d568e496a1310">
            <mixed-citation id="d568e500" publication-type="other">
Owyang, Michael. 2001. Persistence, excess volatility, and volatility clusters in inflation. Review: Federal Reserve Bank of
St. Louis November/December:41-51.</mixed-citation>
         </ref>
         <ref id="d568e510a1310">
            <mixed-citation id="d568e514" publication-type="other">
Phillips, Peter C. B. 1999. Unit root log periodogram regression. Cowles Foundation for Research in Economics, Yale
University, Working Paper No. 1244.</mixed-citation>
         </ref>
         <ref id="d568e524a1310">
            <mixed-citation id="d568e528" publication-type="other">
Romer, David H. 1993. Openness and inflation: Theory and evidence. Quarterly Journal of Economics 108:869-903.</mixed-citation>
         </ref>
         <ref id="d568e536a1310">
            <mixed-citation id="d568e540" publication-type="other">
Siklos, Pierre L. 1999. Inflation-target design: Changing inflation performance and persistence in industrial countries. Review:
Federal Reserve Bank of St. Louis March/April:47-58.</mixed-citation>
         </ref>
         <ref id="d568e550a1310">
            <mixed-citation id="d568e554" publication-type="other">
Sowell, Fallaw. 1990. The fractional unit root distribution. Econometrica 58:495-505.</mixed-citation>
         </ref>
         <ref id="d568e561a1310">
            <mixed-citation id="d568e565" publication-type="other">
Taylor, John B. 1993. Discretion versus policy rules in practice. Carnegie Rochester Conference Series on Public Policy 39:
195-214.</mixed-citation>
         </ref>
         <ref id="d568e575a1310">
            <mixed-citation id="d568e579" publication-type="other">
Taylor, John B. 1999. A historical analysis of monetary policy rules. In Monetary policy rules, edited by John Taylor. Chicago
and London: University of Chicago Press, pp. 319-41.</mixed-citation>
         </ref>
         <ref id="d568e589a1310">
            <mixed-citation id="d568e593" publication-type="other">
Temple, Jonathan R. W. 2002. Openness, inflation, and the Phillips curve: A puzzle. Journal of Money, Credit and Banking
34:450-68.</mixed-citation>
         </ref>
         <ref id="d568e603a1310">
            <mixed-citation id="d568e607" publication-type="other">
Welsch, Roy E. 1982. Influence functions and regression diagnostics. In Modern data analysis, edited by R. L. Launer and A. F.
Siegel. New York: Academic Press, pp. 149-69.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
