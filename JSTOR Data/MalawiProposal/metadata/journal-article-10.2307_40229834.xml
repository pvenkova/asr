<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">poprespolrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000506</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population Research and Policy Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Martinus Nijhoff Publishers</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01675923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737829</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40229834</article-id>
         <title-group>
            <article-title>Public Policy and the Total Fertility Rate: Cross-Sectional Evidence from the LDCs</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter N.</given-names>
                  <surname>Hess</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>1986</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40008375</issue-id>
         <fpage>253</fpage>
         <lpage>275</lpage>
         <permissions>
            <copyright-statement>Copyright 1986 Martinus Nijhoff Publishers</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40229834"/>
         <abstract>
            <p>A framework developed by Easterlin for the analysis of fertility in developing societies is modified and then tested using a sample of 65 less-developed countries. The focus is on assessing the impact of public policy on the national fertility rate. Public policy is reflected in the average levels of education and health in the population and in the condition of the national family planning program. To test for threshold effects with respect to socioeconomic development, the sample is divided on the basis of the infant mortality rate. Fertility rates in those nations characterized by high infant mortality are likely to be determined more by conditions of natural fertility. Those nations with lower infant mortality, and hence greater socioeconomic development, are more likely to exhibit deliberate fertility control. The results of the regression analysis do suggest that different factors influence the national fertility rate depending upon the stage of development. For the least-developed nations, the secondary school enrollment rate, an indicator of the extent of economic mobility, and the ratio of school age children to teachers, a proxy for the national commitment to human capital formation, are important. For the more advanced of the LDCs, adult literacy and the infant mortality rate seem to predominate. For all the developing nations, however, the results confirm the importance of strong family planning programs. The paper concludes with a discussion of the policy implications of the research.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d316e132a1310">
            <label>1</label>
            <mixed-citation id="d316e139" publication-type="other">
Easterlin (1975)</mixed-citation>
            <mixed-citation id="d316e145" publication-type="other">
Easterlin and
Crimmins (1985)</mixed-citation>
            <mixed-citation id="d316e154" publication-type="other">
Easterlin, Pollak and Wachter (1980).</mixed-citation>
         </ref>
         <ref id="d316e161a1310">
            <label>4</label>
            <mixed-citation id="d316e168" publication-type="other">
Srinivasan et al.,
(1984)</mixed-citation>
            <mixed-citation id="d316e177" publication-type="other">
Easterlin and Crimmins (1985).</mixed-citation>
         </ref>
         <ref id="d316e184a1310">
            <label>6</label>
            <mixed-citation id="d316e191" publication-type="other">
(Becker, 1960).</mixed-citation>
         </ref>
         <ref id="d316e198a1310">
            <label>7</label>
            <mixed-citation id="d316e205" publication-type="other">
(World Bank, 1984: 188-189).</mixed-citation>
            <mixed-citation id="d316e211" publication-type="other">
(World Bank, 1984: 200-201).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d316e227a1310">
            <mixed-citation id="d316e231" publication-type="other">
Becker, G. (1960). 'An economic analysis of fertility' in Demographic and Economic Change in
Developed Countries. National Bureau of Economic Research, Princeton, N.J.: Princeton
University Press.</mixed-citation>
         </ref>
         <ref id="d316e244a1310">
            <mixed-citation id="d316e248" publication-type="other">
Bloom, D. and P. Reddy (1985). 'Age patterns of women at marriage, cohabitation, and first birth
in India'. Working Paper, Bangalore: Population Centre.</mixed-citation>
         </ref>
         <ref id="d316e258a1310">
            <mixed-citation id="d316e262" publication-type="other">
Bongaarts, J., O. Frank, and R. Lesthaeghe (1984). 'The proximate determinants of fertility in
sub-Saharan Africa', Population and Development Review 10: 511-537.</mixed-citation>
         </ref>
         <ref id="d316e272a1310">
            <mixed-citation id="d316e276" publication-type="other">
Brown, L. et al. (1985). State of the World 1985. New York: W. W. Norton.</mixed-citation>
         </ref>
         <ref id="d316e284a1310">
            <mixed-citation id="d316e288" publication-type="other">
Bulatao, R. (1984). 'Reducing fertility in developing countries: a review of determinants and
policy levers', World Bank Staff Working Paper No. 680. Washington, D.C.: The World Bank.</mixed-citation>
         </ref>
         <ref id="d316e298a1310">
            <mixed-citation id="d316e302" publication-type="other">
Caldwell, J. (1980). 'Mass education as a determinant of the timing of fertility decline', Population
and Development Review 6: 225-255.</mixed-citation>
         </ref>
         <ref id="d316e312a1310">
            <mixed-citation id="d316e316" publication-type="other">
Chowdhury, A., A. Khan, and L. Chen (1978). 'Experience in Pakistan and Bangladesh', in S.
Preston (ed.), The Effects of Infant and Child Mortality on Fertility. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="d316e326a1310">
            <mixed-citation id="d316e330" publication-type="other">
Cleland, J. (1985). 'Marital fertility decline in developing countries: theories and the evidence', in
J. Cleland and J. Hobcraft (eds.), Reproductive Change in Developing Countries. New York:
Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d316e343a1310">
            <mixed-citation id="d316e347" publication-type="other">
Cleland, J. and J. Hobcraft (1985). Reproductive Change in Developing Countries: Insights from
the World Fertility Survey. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d316e357a1310">
            <mixed-citation id="d316e361" publication-type="other">
Cuca, R. (1980). 'Family planning programs and fertility decline', Finance and Development 17: 4:
37-39.</mixed-citation>
         </ref>
         <ref id="d316e372a1310">
            <mixed-citation id="d316e376" publication-type="other">
Cutright, P. (1983). 'The ingredients of fertility decline in developing countries', International
Family Planning Perspectives 9: 101-109.</mixed-citation>
         </ref>
         <ref id="d316e386a1310">
            <mixed-citation id="d316e390" publication-type="other">
Cutright, P. and L. Hargens (1984). 'The threshold hypothesis: evidence from less developed
Latin American countries: 1950 to 1980', Demography 21: 459-473.</mixed-citation>
         </ref>
         <ref id="d316e400a1310">
            <mixed-citation id="d316e404" publication-type="other">
Cutright, P. and W. Kelley (1981). 'The role of family planning programs in fertility declines in less
developed countries 1958-1977', International Family Planning Perspectives 7: 145-151.</mixed-citation>
         </ref>
         <ref id="d316e414a1310">
            <mixed-citation id="d316e418" publication-type="other">
Easterlin, R. (1975). 'An economic framework for fertility analysis', Studies in Family Planning 6:
3: 54-63.</mixed-citation>
         </ref>
         <ref id="d316e428a1310">
            <mixed-citation id="d316e432" publication-type="other">
Easterlin, R. and E. Crimmins (1985). The Fertility Revolution: A Supply-Demand Analysis.
Chicago: The Univ. of Chicago Press.</mixed-citation>
         </ref>
         <ref id="d316e442a1310">
            <mixed-citation id="d316e446" publication-type="other">
Easterlin, R., R. Pollak and M. Wachter (1980). 'Toward a more general economic model of
fertility determination: endogenous preferences and natural fertility', in R. Easterlin (ed.),
Population and Economic Change in Developing Countries. Chicago: The Univ. of Chicago
Press.</mixed-citation>
         </ref>
         <ref id="d316e463a1310">
            <mixed-citation id="d316e467" publication-type="other">
Gille, H. (1985). 'Policy implications', in J. Cleland and J. Hobcraft (eds.), Reproductive Change
in Developing Countries. New York: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d316e477a1310">
            <mixed-citation id="d316e481" publication-type="other">
Hirschman, C. (1985). 'Premarital socioeconomic roles and the timing of family formation: a
comparative study of five Asian societies', Demography 22: 35-59.</mixed-citation>
         </ref>
         <ref id="d316e491a1310">
            <mixed-citation id="d316e495" publication-type="other">
Hobcraft, J. (1985). 'Family-building patterns', in J. Cleland and J. Hobcraft (eds.), Reproductive
Change in Developing Countries. New York: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d316e505a1310">
            <mixed-citation id="d316e509" publication-type="other">
Leridon, H. and B. Ferry (1985). 'Biological and traditional restraints on fertility', in J. Cleland
and J. Hobcraft (eds.), Reproductive Change in Developing Countries. New York: Oxford
Univ. Press.</mixed-citation>
         </ref>
         <ref id="d316e522a1310">
            <mixed-citation id="d316e526" publication-type="other">
Lightbourne, R. (1985). 'Individual preferences and fertility behavior', in J. Cleland and J.
Hobcraft (eds.), Reproductive Change in Developing Countries. New York: Oxford Univ.
Press.</mixed-citation>
         </ref>
         <ref id="d316e539a1310">
            <mixed-citation id="d316e543" publication-type="other">
Mauldin, P. (1978). 'Patterns of fertility decline in developing countries, 1950-75', Studies in
Family Planning 9: 75-84.</mixed-citation>
         </ref>
         <ref id="d316e554a1310">
            <mixed-citation id="d316e558" publication-type="other">
Me Donald, P. (1985). 'Social organization and nuptiality in developing societies', in J. Cleland
and J. Hobcraft (eds.), Reproductive Change in Developing Countries. New York: Oxford
University Press.</mixed-citation>
         </ref>
         <ref id="d316e571a1310">
            <mixed-citation id="d316e575" publication-type="other">
NOVA (1985). 'Child survival: the silent emergency', Transcript of a program originally broadcast
on Public Broadcasting System on November 12, 1985, Boston, Mass.: WGBH Educational
Foundation.</mixed-citation>
         </ref>
         <ref id="d316e588a1310">
            <mixed-citation id="d316e592" publication-type="other">
Preston, S. (1978). 'Introduction', in S. Preston (ed.), The Effects of Infant and Child Mortality on
Fertility. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="d316e602a1310">
            <mixed-citation id="d316e606" publication-type="other">
Preston, S. (1985). 'Mortality in childhood: lessons from WFS', in J. Cleland and J. Hobcraft
(eds.), Reproductive Change in Developing Countries. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d316e616a1310">
            <mixed-citation id="d316e620" publication-type="other">
Rodriguez, G. and J. Cleland (1981). 'The effects of socioeconomic characteristics on fertility in 20
countries', International Family Planning Perspectives 7: 93-101.</mixed-citation>
         </ref>
         <ref id="d316e630a1310">
            <mixed-citation id="d316e634" publication-type="other">
Sathar, Z. (1984). 'Does female education affect fertility behavior in Pakistan?' The Pakistan
Development Review 23: 573-590.</mixed-citation>
         </ref>
         <ref id="d316e645a1310">
            <mixed-citation id="d316e649" publication-type="other">
Scott, C. and V. Chidambaram (1985). 'World fertility survey: origins and achievements', in J.
Cleland and J. Hobcraft (eds.), Reproductive Change in Developing Countries. New York:
Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d316e662a1310">
            <mixed-citation id="d316e666" publication-type="other">
Simmons, G. (1979). 'Family planning programs or development: how persuasive is the new
wisdom?', International Family Planning Perspectives 5: 101-110.</mixed-citation>
         </ref>
         <ref id="d316e676a1310">
            <mixed-citation id="d316e680" publication-type="other">
Singh, S. and J. Casterline (1985). 'The socio-economic determinants of fertility', in J. Cleland and
J. Hobcraft (eds.), Reproductive Change in Developing Countries. New York: Oxford Univer-
sity Press.</mixed-citation>
         </ref>
         <ref id="d316e693a1310">
            <mixed-citation id="d316e697" publication-type="other">
Sivard, R. (1985). World Military and Social Expenditures 1985. Washington, D.C.: World
Priorities.</mixed-citation>
         </ref>
         <ref id="d316e707a1310">
            <mixed-citation id="d316e711" publication-type="other">
Srinivasan, K., S. Jejeebhoy, R. Easterlin, and E. Crimmins (1984). 'Factors affecting fertility
control in India: a cross-sectional study', Population and Development Review 10: 273-296.</mixed-citation>
         </ref>
         <ref id="d316e721a1310">
            <mixed-citation id="d316e725" publication-type="other">
Tawiah, E. (1984). 'Determinants of cumulative fertility in Ghana', Demography 21: 1-8.</mixed-citation>
         </ref>
         <ref id="d316e733a1310">
            <mixed-citation id="d316e737" publication-type="other">
Tsui, A. (1985). 'The rise of modern contraception', in J. Cleland and J. Hobcraft (eds.),
Reproductive Change in Developing Countries. New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d316e747a1310">
            <mixed-citation id="d316e751" publication-type="other">
Williams, A. (1976). 'Review and evaluation of the literature', in M. Keeley (ed.), Population,
Public Policy and Economic Development. New York: Praeger Publishers.</mixed-citation>
         </ref>
         <ref id="d316e761a1310">
            <mixed-citation id="d316e765" publication-type="other">
World Bank (1982). World Development Report 1982. New York: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d316e772a1310">
            <mixed-citation id="d316e776" publication-type="other">
World Bank (1984). World Development Report 1984. New York: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d316e783a1310">
            <mixed-citation id="d316e787" publication-type="other">
World Bank (1985). World Development Report 1985. New York: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d316e794a1310">
            <mixed-citation id="d316e798" publication-type="other">
Zachariah, K. and S. Patel (1984). 'Determinants of fertility decline in India: an analysis', World
Bank Staff Working Paper No. 699. Washington, D.C.: The World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
