<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt227269q</book-id>
      <subj-group>
         <subject content-type="call-number">PR9379.9.A7Z75 1990</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Armah, Ayi Kwei, 1939–</subject>
         <subj-group>
            <subject content-type="lcsh">Criticism and interpretation</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African fiction</subject>
         <subj-group>
            <subject content-type="lcsh">20th century</subject>
            <subj-group>
               <subject content-type="lcsh">History and criticism</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa</subject>
         <subj-group>
            <subject content-type="lcsh">Intellectual life</subject>
            <subj-group>
               <subject content-type="lcsh">20th century</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Decolonization in literature</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Radicalism in literature</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Africa in literature</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Resistance in Postcolonial African Friction</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Lazarus</surname>
               <given-names>Neil</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>10</day>
         <month>09</month>
         <year>1990</year>
      </pub-date>
      <isbn content-type="ppub">9780300045536</isbn>
      <isbn content-type="epub">9780300242027</isbn>
      <publisher>
         <publisher-name>Yale University Press</publisher-name>
         <publisher-loc>New Haven; London</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>1990</copyright-year>
         <copyright-holder>Yale University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt227269q"/>
      <abstract abstract-type="short">
         <p>This book is about resistance in Postcolonial African fiction.</p>
      </abstract>
      <counts>
         <page-count count="288"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.4</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>Great Expectations and the Mourning After</title>
                     <subtitle>Decolonization and African Intellectuals</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>On March 6, 1957, Ghana gained its independence from Britain, becoming the first sub-Saharan African colony to do so. Over 100,000 people crowded into the Polo Ground in Accra, the capital city, to watch the proceedings. The ceremony took place at midnight. There was tremendous excitement in the air as the Union Jack was lowered and the new Ghanaian flag—red, green, and gold—hoisted in its place. In the hushed silence that followed the flag-raising, Kwame Nkrumah, the first president of independent Ghana, began to speak:</p>
                     <p>At long last the battle has ended! And thus Ghana, your beloved country,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.5</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>From Frantz Fanon to Ayi Kwei Armah</title>
                     <subtitle>Messianism and the Representation of Postcolonialism</subtitle>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>Ayi Kwei Armahʹs first three novels—<italic>The Beautyful Ones Are Not Yet Born</italic>(1968),<italic>Fragments</italic>(1970), and<italic>Why Are We So Blest?</italic>(1972)—are all set in postcolonial Africa. Any attempt to delineate the conceptual horizon of these three novels must take the work of Frantz Fanon as its point of departure. Armahʹs intellectual debt to Fanon is profound, and freely acknowledged. Unless Fanon is understood, Armah himself wrote in ʺFanon: the Awakener,ʺ a 1969 essay, ʺweʹll never get where we need to go. We may move without him, but only blindly, wasting energy.ʺ¹ Specifically, we must return to Fanonʹs</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.6</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>The Beautyful Ones Are Not Yet Born</title>
                     <subtitle>Pessimism of the Intellect, Optimism of the Will</subtitle>
                  </title-group>
                  <fpage>46</fpage>
                  <abstract>
                     <p>At the beginning of<italic>The Beautyful Ones Are Not yet Born</italic>, the central protagonist (who is referred to throughout the novel only as ʺthe manʺ) is encountered at dawn sleeping aboard a bus on his way to work.¹ At the end of the novel he is seen walking ʺvery slowly, going homeʺ (183). In the space or, more correctly, the distance between these two points, the reader witnesses what Georg Lukács, speaking of the novel form at large, has styled ʺthe adventure of interiorityʺ: ʺthe content of the novel is the story of the soul that goes to find itself,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.7</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Fragments</title>
                     <subtitle>Enduring the Conditional, Thinking the Unconditional</subtitle>
                  </title-group>
                  <fpage>80</fpage>
                  <abstract>
                     <p>When<italic>The Beautyful Ones</italic>was first published in 1968, it received mixed reviews. Few critics were unimpressed by the novel, and few failed to acclaim Ayi Kwei Armahʹs voice as an important addition to the African literary scene. Yet the enthusiasm of the most influential readers—among them Chinua Achebe, Emmanuel Obiechina, and Abiola Irele—was less than wholehearted. While conceding that the novelʹs technical merits were considerable, these readers tended to argue that its presentation of Ghanaian society and politics was flat and one-sided. Achebe spoke of the book as being evidently the product of an alienated mind. ʺThere</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.8</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>Why Are We So Blest?</title>
                     <subtitle>Intellectualism, Masculinism, and Racial Essentialism</subtitle>
                  </title-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>
                        <italic>Why Are We So Blest?</italic>is a brutal, harrowing, and extreme novel.¹ In it, Armah takes several of the pressing and unresolved questions that had animated<italic>The Beautyful Ones Are Not Yet Born</italic>and<italic>Fragments</italic>—questions concerning creative intellectualism in the context of neocolonialism, the alienation of radical intellectuals from their larger communities, the dispossession and consequent depoliticization of urban and rural masses in the postcolonial era, the exocentric and fawning posture of indigenous elites—and attempts to force them to definitive formulation. In this search for closure and resolution, he resorts to the concentration of metaphorical language. At the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.9</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>After the Break</title>
                     <subtitle>Trends in Radical African Literature since 1970</subtitle>
                  </title-group>
                  <fpage>185</fpage>
                  <abstract>
                     <p>I have tried to argue that<italic>Why Are We So Blest?</italic>must, by any standards, be accounted a novelistic failure. The novelʹs sweeping dogmatism, its manichean racial and sexual essentialism, and its conspiratorial view of African history, all combine to destroy its internal plausibility and to undermine its ideological integrity. Setting out to interrogate the limits of creative intellectualism in the colonial and postcolonial contexts, it tends increasingly to represent these contexts not only as implacable and seamless configurations but, even more disturbingly, as brilliant strategies hatched in the minds of ruthless and predatory ʺWesterners.ʺ This means that even where</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.10</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt227269q.11</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>259</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
