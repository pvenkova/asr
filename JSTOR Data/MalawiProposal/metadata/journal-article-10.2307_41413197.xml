<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">irisstudinteaffa</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000059</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Irish Studies in International Affairs</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Royal Irish Academy</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03321460</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">20090072</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41413197</article-id>
         <article-categories>
            <subj-group>
               <subject>A CHALLENGING BACKGROUND FOR ETHICAL GOVERNANCE EFFORTS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>From Conflict to Ownership: Participatory Approaches to the Re-integration of Ex-Combatants in Sierra Leone</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Walt</given-names>
                  <surname>Kilroy</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">22</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40068792</issue-id>
         <fpage>127</fpage>
         <lpage>144</lpage>
         <permissions>
            <copyright-statement>© Royal Irish Acadamy 2011</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41413197"/>
         <abstract>
            <p>The demobilisation and re-integration of ex-combatants has become an important element in peace-building. The need for a more holistic, integrated approach, in which there is greater local ownership of the process, has long been recognised. However, putting this into practice remains a challenge. Re-integration ultimately takes place in the community, merging with development and post-conflict reconstruction. This study of re-integration in Sierra Leone uses the concept of 'participation' from development discourse, meaning the extent to which potential stakeholders have a say in how interventions are designed and implemented. It finds that participation and ownership are only seen to a limited extent, and only in certain situations. Many of the ex-combatants who participated in this study felt they did not receive adequate or accurate information regarding the re-integration process. This undermines the contribution that re-integration can have to the peace-building project. Participation proves to be a useful framework for assessing re-integration programmes, and for planning the more integrated approach that has long been advocated.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1138e180a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1138e187" publication-type="other">
Ministry for Foreign Affairs Sweden, Stockholm initiative on disarmament demobilisation
reintegration : Final report (Stockholm, 2006),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e196" publication-type="other">
http://www.sweden.gov.Se/content/l/c6/
06/43/56/cf5d85 1 b.pdf (24 April 2006);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e205" publication-type="other">
United Nations, Integrated disarmament, demobilization
and reintegration standards (IDDRS) (2006),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e215" publication-type="other">
http://www.unddr.org/iddrs/
framework.php (2 February 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e225a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1138e232" publication-type="other">
rma Specht, Practice note 4: Socio-economic reintegration of ex-combatants (London, 2010);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e238" publication-type="other">
Nat J. Colletta and Robert Muggah, 'Context matters: interim stabilisation and second
generation approaches to security promotion', Conflict , Security and Development 9 (4) (2009),
425-53;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e250" publication-type="other">
UN Department of Peacekeeping Operations, Second generation disarmament, demobi-
lization and reintegration (DDR) practices in peace operations (New York, 2010);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e260" publication-type="other">
Robert
Muggah, Innovations in disarmament, demobilization and reintegration policy and research:
reflections on the last decade (Oslo, 20 1 0).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e273a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1138e280" publication-type="other">
James Pugel, What the fighters say: a survey of ex-combatants in Liberia, February-March
2006 (Monrovia, 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e290a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1138e297" publication-type="other">
Macartan Humphreys and Jeremy M. Weinstein, What the fighters say: a survey of ex-
combatants in Sierra Leone, June-August 2003 (New York and Stanford, 2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e308a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1138e315" publication-type="other">
Kathleen M. Jennings, 'The struggle to satisfy: DDR through the eyes of ex-combatants in
Liberia', International Peacekeeping 14 (2) (2007), 204-18.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e325a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1138e332" publication-type="other">
Laurie Nathan, No ownership, no commitment: a guide to local ownership of security sector
reform (2nd edn, Birmingham, 2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e342a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1138e349" publication-type="other">
Ana Cutter Patel, 'Transitional justice, DDR, and security sector reform', in Ana Cutter
Patel, Pablo de Greiff and Lars Waldorf (eds), Disarming the past: transitional justice and
excombatants (New York, 2009), 262-83.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e362a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1138e369" publication-type="other">
Mats R. Berdal, Disarmament and demobilisation after Civil Wars Adelphi Paper 303 (Oxford,
1996);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e378" publication-type="other">
Robert Muggah, 'No magic bullet: a critical perspective on disarmament, demobilization
and reintegration (DDR) and weapons reduction in post-conflict contexts', Round Table 94 (379)
(2005), 239-52</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e391a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1138e398" publication-type="other">
UN Secretary-General, Disarmament, demobilization and reintegration , Report of the
Secretary-General to UN General Assembly, A/60/705, 2 March 2006, 8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e408a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1138e415" publication-type="other">
Robert Chambers, Whose reality counts? Putting the first last (London, 1997);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e421" publication-type="other">
Robert
Chambers, Challenging the professions : frontiers for rural development (London, 1998).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e432a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1138e439" publication-type="other">
Gwinyayi Albert Dzinesa, A participatory approach to DDR: empowering local institutions
and abilities', Conflict Trends (3) (2006), 39-43.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e449a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1138e456" publication-type="other">
Bill Cooke and Uma Kothari, 'The case for participation as tyranny', in Bill Cooke and Uma
Kothari (eds), Participation: the new tyranny? (London and New York, 2001), 1-15;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e465" publication-type="other">
Andrea
Cornwall, 'Unpacking 'Participation': models, meanings and practices', Community Development
Journal 43 (3) (2008), 269-83;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e477" publication-type="other">
Niamh Gaynor, Transforming participation? The politics of
development in Malawi and Ireland (Basingstoke, 2010);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e487" publication-type="other">
Niamh Gaynor, 'Between citizenship and
clientship: the politics of participatory governance in Malawi', Journal Of Southern African
Studies 36 (4) (2010), 801-16.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e500a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1138e507" publication-type="other">
Jules Pretty, 'Participatory learning for sustainable agriculture', World Development 23 (8)
(1995), 1247-63.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e517a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1138e524" publication-type="other">
Pretty, Participatory learning for sustainable agriculture , 1252.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e531a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1138e538" publication-type="other">
Jules Pretty, Irene Guijt, John Thompson and Ian Scoones, A trainer's guide for participatory
learning and action (London, 1995), 61.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e548a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1138e555" publication-type="other">
Pretty, 'Participatory learning for sustainable agriculture', 1252.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e563a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1138e570" publication-type="other">
Michael Doyle and Nicholas Sambams, Making war and building peace: United Nations
peace operations (Princeton, 2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e580a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1138e587" publication-type="other">
Richard Bowd, 'From combatant to civilian: the social reintegration of
ex-combatants in Rwanda and the implications for social capital and reconciliation', unpublished
PhD thesis, University of York, 2008,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e599" publication-type="other">
http://ethos.bl.uk/OrderDetails.do7did = 1
&amp;uin =uk.bl.ethos.516644 (3 June 2011);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1138e608" publication-type="other">
Richard Bowd, 'Burning the bridges and breaking the
bonds: social capital and its transformative influence in relation to violent conflict', Journal of
Conflict Transformation and Security 1 (1) (2011), 37-62.</mixed-citation>
            </p>
         </fn>
         <fn id="d1138e621a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1138e628" publication-type="other">
Mark Knight and Alpaslan Özerdem, 'Guns, camps and cash:
disarmament, demobilization and reinsertion of former combatants in transitions from war to
peace', Journal Of Peace Research 41 (4) 2004, 499-516: 504.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
