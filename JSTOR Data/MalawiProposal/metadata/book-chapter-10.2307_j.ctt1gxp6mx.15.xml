<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt1gsmx3r</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gxp6mx</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Economics</subject>
      </subj-group>
      <book-title-group>
         <book-title>Reforming the Governance of the IMF and the World Bank</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>BUIRA</surname>
               <given-names>ARIEL</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>11</month>
         <year>2005</year>
      </pub-date>
      <isbn content-type="ppub">9781843312116</isbn>
      <isbn content-type="epub">9780857287144</isbn>
      <publisher>
         <publisher-name>Anthem Press</publisher-name>
         <publisher-loc>London, UK; New York, NY, USA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2006</copyright-year>
         <copyright-holder>Ariel Buira</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1gxp6mx"/>
      <abstract abstract-type="short">
         <p>A radical and comprehensive review of the practices of governance within one of the world's most important and influential organizations.</p>
      </abstract>
      <counts>
         <page-count count="328"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.3</book-part-id>
                  <title-group>
                     <title>LIST OF CONTRIBUTORS</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.4</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Ocampo</surname>
                           <given-names>José Antonio</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xv</fpage>
                  <abstract>
                     <p>In the Monterrey Consensus, agreed at the 2002 International Conference on Financing for Development, held in Monterrey, Mexico, world leaders committed to broaden and strengthen the voice and participation of developing countries in international economic decision-making and norm-setting. This commitment reflected the perceived need to increase the representativeness of the Bretton Woods institutions, which operate within a governance structure largely defined six decades ago. It also reflected the need to adjust other multilateral financial institutions, such as the Bank for International Settlements, to a global environment characterized by the growing importance in the world economy of a dynamic group of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="editor" id="contrib1" xlink:type="simple">
                        <string-name>The Editor</string-name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The United Nations Monetary and Financial Conference held at Bretton Woods sixty years ago led to the establishment of the IMF and World Bank. This was a foundational moment. As the most powerful financial institutions of our times were created, hopes ran high of a better world, in which international cooperation through the IMF would sustain economic activity and prevent the adoption of measures destructive of national and international prosperity. The Bank was to assist reconstruction of war ravaged countries and finance the development of the less developed world. For a number of decades, the institutions created in Bretton Woods</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>THE BRETTON WOODS INSTITUTIONS:</title>
                     <subtitle>GOVERNANCE WITHOUT LEGITIMACY?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Buira</surname>
                           <given-names>Ariel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>Sixty years after their creation, the Bretton Woods Institutions (BWIs) face a crisis of legitimacy that impairs their credibility and effectiveness. At the root of this crisis lies the unrepresentative nature of their structure of governance, which places the control of the institutions in the hands of a small group of industrial countries. These countries consider the developing countries and economies in transition as minor partners, despite their accounting for half of the world’s output in real terms,¹ most of the world’s population and encompassing the most dynamic economies and the largest holders of international reserves.² Over time, the effects</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>REFORMING THE INTERNATIONAL MONETARY FUND:</title>
                     <subtitle>TOWARDS ENHANCED ACCOUNTABILITY AND LEGITIMACY</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kelkar</surname>
                           <given-names>Vijay L.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Chaudhry</surname>
                           <given-names>Praveen K.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Vanduzer-Snow</surname>
                           <given-names>Marta</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bhaskar</surname>
                           <given-names>V.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>Viable international institutions reflect the historical necessities of a particular period.</p>
                     <p>The strength of any institution lies in its ability to adapt and serve the changing economic and political forces. The International Monetary Fund (IMF) was the brainchild of John Maynard Keynes and Harry Dexter White, whose world-view was shaped by the Great Wars and the Depression. The IMF served well the dynamic economic forces of the second half of the 20th century. However, a recent report by the United Nations Development Program informs that economically and politically, frustration in the developing countries about the skewed distribution of global power</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>IMPROVING IMF GOVERNANCE AND INCREASING THE INFLUENCE OF DEVELOPING COUNTRIES IN IMF DECISION-MAKING</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Portugal</surname>
                           <given-names>Murilo</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>75</fpage>
                  <abstract>
                     <p>The IMF has a generally well thought out governance structure and is a relatively effective organization that has been able to adopt and implement complex decisions in a cooperative and timely fashion. The constituency system allows reconciling the legitimacy of an almost universal membership with efficient decision-making and collegiality of a not-too-large Executive Board. Weighted voting based on relative economic strength gives confidence to creditor countries to commit financial resources to the IMF, while consensus decision-making confers some protection to the interests of minority groups, making weighted voting acceptable to debtor countries, and may lead to better decisions that are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>ISSUES ON IMF GOVERNANCE AND REPRESENTATION:</title>
                     <subtitle>AN EVALUATION OF ALTERNATIVE OPTIONS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Le Fort V</surname>
                           <given-names>Guillermo</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>107</fpage>
                  <abstract>
                     <p>The IMF can be represented as an international organization that provides global public goods in the form of macroeconomic surveillance, support for adjustment programs and technical assistance for the design and implementation of economic and financial policies. Those services are particularly relevant for the EM economies and for developing countries that are particularly exposed to the volatility of global markets, either financial or commodities markets. These countries use the services provided by the IMF to recover from crisis events, or to improve their resiliency against external shocks.³ The work of the Fund contributes to prevent crisis to the extent that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>MAKING THE IMF AND THE WORLD BANK MORE ACCOUNTABLE</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Woods</surname>
                           <given-names>Ngaire</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>During the 1990s, the International Monetary Fund (IMF) and the World Bank (the Bank)² found themselves accused of being secretive, unaccountable and ineffective. Not only radical non-governmental organizations (NGOs) but equally, their major shareholders are demanding that the institutions become more transparent, accountable and participatory. Accountability became the catch cry of officials, scholars and activists in discussing the reform of the institutions. However, few attempts have been made to dissect the existing structure of accountability within the International Financial Institutions (IFIs), to explain its flaws and to propose solutions and is the aim of this paper.</p>
                     <p>The first section examines</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>PURCHASING POWER PARITIES AND COMPARISONS OF GDP IN IMF QUOTA CALCULATIONS</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McLenaghan</surname>
                           <given-names>John B.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>171</fpage>
                  <abstract>
                     <p>Issues of governance have assumed great importance for the IMF in recent years in terms of its financial programs with member countries and with respect to its internal operations. With the vastly extended ambit of IMF support programs for a number of Asian countries in 1997 and 1998, and subsequently for emerging and developing countries in other regions, the need to address shortcomings of governance in member countries has assumed a prominent place in many of its programs. Concomitantly, a sharp light has been thrown on to the Fund itself in terms of aspects of its internal governance. Prominent among</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>MEASURING VULNERABILITY:</title>
                     <subtitle>CAPITAL FLOWS VOLATILITY IN THE QUOTA FORMULA</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>dos Reis</surname>
                           <given-names>Laura</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>195</fpage>
                  <abstract>
                     <p>This paper discusses a proposal by the G-24 Ministers and also by the IMF to include capital flow volatility as an additional variable in the quota formula. The objective is to capture countries’ vulnerabilities to both capital account shocks and balance of payment crisis.</p>
                     <p>In the communiqué of October 2004, Ministers of the G-24 stated that enhancing the representation of developing countries requires a new quota formula that:</p>
                     <p>…takes into account the vulnerabilities of developing countries to movements in commodity prices, the volatility of capital movements, and other exogenous shocks.</p>
                     <p>A proposal to this effect was introduced in recent quota</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>ENHANCING THE VOICE OF DEVELOPING COUNTRIES IN THE WORLD BANK:</title>
                     <subtitle>SELECTIVE DOUBLE MAJORITY VOTING AND A PILOT PHASE APPROACH</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jakobeit</surname>
                           <given-names>Cord</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>213</fpage>
                  <abstract>
                     <p>The enhanced participation and voice of developing and transition countries in international economic and financial institutions has been on the international agenda for some time. The Millennium General Assembly of the United Nations in 2000 as well as the UN Conference on Financing for Development (Monterrey, March 2002) and the World Summit on Sustainable Development (Johannesburg, August 2002) listed this issue among their objectives. Within the World Bank, the issue of an enhanced ‘voice and participation of the poor’ has been discussed since the 2003 Spring Meeting and the Annual Meeting in Dubai in the fall of 2003. In Dubai,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>VOTING POWER IMPLICATIONS OF A DOUBLE MAJORITY VOTING PROCEDURE IN THE IMF’S EXECUTIVE BOARD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Strand</surname>
                           <given-names>Jonathan R.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rapkin</surname>
                           <given-names>David P.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>235</fpage>
                  <abstract>
                     <p>One of the most fundamental governance issues facing international organizations concerns the methods by which members’ preferences are aggregated for purposes of deciding upon and implementing collective action. From a slightly different perspective, what is at issue is how to reconcile the principle of sovereign equality with the reality of a hierarchical international system marked by large power disparities across member states. The strict Westphalian alternative—a one country – one vote rule, as practiced in the UN General Assembly—based as it is solely on the juridical equality of sovereign states, takes account of neither size, power, nor wealth</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>POWER VERSUS WEIGHT IN IMF GOVERNANCE:</title>
                     <subtitle>THE POSSIBLE BENEFICIAL IMPLICATIONS OF A UNITED EUROPEAN BLOC VOTE</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Leech</surname>
                           <given-names>Dennis</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Leech</surname>
                           <given-names>Robert</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>251</fpage>
                  <abstract>
                     <p>The governance of the IMF is based on a system of weighted voting in which the number of votes assigned to each member country is a reflection of its quota. The principle behind this is the idea that influence ought to be inextricably linked with financial contribution, rather as in voting capital of a joint stock company. Thus, for example, the 371,743 votes at the disposal of the United States are supposed to give it 17.09 per cent of the voting power, which is its percentage of the votes. On the other hand, Italy is supposed to have 3.25 per</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gxp6mx.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>CHANGING IMF QUOTAS:</title>
                     <subtitle>THE ROLE OF THE UNITED STATES CONGRESS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Broz</surname>
                           <given-names>J. Lawrence</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>283</fpage>
                  <abstract>
                     <p>The United States is positioned at the International Monetary Fund (IMF or Fund) to unilaterally veto changes in the size or distribution of ‘quotas’.³ This is because altering quotas requires an 85 per cent majority in the IMF’s Board of Governors, and the United States has never held less than 17 per cent of the votes. No matter how intensely other members feel about the need for increasing or redistributing quotas, opposition by the United States alone can block any quota adjustment. On quotas, the United States is predominant.</p>
                     <p>In this paper, I investigate the sources of US policy toward</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
