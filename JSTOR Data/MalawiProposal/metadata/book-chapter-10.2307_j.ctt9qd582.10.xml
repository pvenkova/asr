<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7hbgd7</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt9qd582</book-id>
      <subj-group>
         <subject content-type="call-number">JV6342.S78 2008</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Assimilation (Sociology)</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Home</subject>
         <subj-group>
            <subject content-type="lcsh">Psychological aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Identity (Psychology)</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Emigration and immigration</subject>
         <subj-group>
            <subject content-type="lcsh">Psychological aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">War</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Case studies</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
         <subject>Political Science</subject>
         <subject>Anthropology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Struggles for Home</book-title>
         <subtitle>Violence, Hope and the Movement of People</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Jansen</surname>
               <given-names>Stef</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Löfving</surname>
               <given-names>Staffan</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>10</month>
         <year>2008</year>
      </pub-date>
      <isbn content-type="ppub">9780857451507</isbn>
      <isbn content-type="epub">9781845458607</isbn>
      <isbn content-type="epub">1845458605</isbn>
      <publisher>
         <publisher-name>Berghahn Books</publisher-name>
         <publisher-loc>NEW YORK; OXFORD</publisher-loc>
      </publisher>
      <edition>NED - New edition, 1</edition>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Stef Jansen</copyright-holder>
         <copyright-holder>Staffan Löfving</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt9qd582"/>
      <abstract abstract-type="short">
         <p>Based on anthropological studies across the globe, this book explores the social practice of home-making amongst people whose lives are characterized by movement and violence. Social scientific and policy understandings of home and migration tend to focus on territory, culture and nation, often carrying implicit 'sedentarist' assumptions of a naturalised link between people and particular places. This book challenges such views, drawing attention instead to unpredictable forms of dwelling in the often violent processes that connect yet differently affect the movement of people and capital. Taking seriously the political implications of this challenge, the authors do not resort to a free floating, placeless approach. Instead, through the detailed ethnography of lived experiences of displacement and emplacement, *Struggles for Home* investigates the power sedentarism may have to provide or prohibit hope. Research conducted in Sri Lanka, Bosnia-Herzegovina, Zambia, Cyprus, the Palestinian West Bank, Guatemala, and amongst Romanians and Moroccans in Spain articulates a novel theoretical framework for the development of a critical political anthropology of one of the most controversial and fascinating issues of our time - the remaking of home in migration.</p>
      </abstract>
      <counts>
         <page-count count="200"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.3</book-part-id>
                  <title-group>
                     <title>Introduction:</title>
                     <subtitle>Towards an Anthropology of Violence, Hope and the Movement of People</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jansen</surname>
                           <given-names>Stef</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Löfving</surname>
                           <given-names>Staffan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The idea for this book grew out of our attempts to grapple with some sharp contradictions encountered in our respective ethnographic engagements. As anthropologists working in the post-Yugoslav states and in postwar Central America, we have been struck by a disturbing discrepancy in discussions of violence and the movement of people. On the one hand, there are the naturalised assumptions in both political and scholarly discourse that the cure for the ailment of violent displacement lies per definition in renewed territorialised belonging (i.e., usually, refugee return). Yet, on the other hand, we found that the realities of people and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Returning to Palestine:</title>
                     <subtitle>Confinement and Displacement under Israeli Occupation</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kelly</surname>
                           <given-names>Tobias</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>Subhi was bored.¹ During the summer of 2001, he had not been able to leave the West Bank Palestinian village where he had been living for the last six months, and his world had shrunk to the few square kilometres between the river that ran past the village to the west and the hills that rose to the east. In a vain attempt to ease his boredom, he would often stroll along the dried-out riverbed but come to a stop after a few hundred metres as he neared the crossroads and a potential Israeli checkpoint. He would then turn around</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Troubled Locations:</title>
                     <subtitle>Return, the Life Course and Transformations of Home in Bosnia-Herzegovina</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jansen</surname>
                           <given-names>Stef</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>Five years after the end of the 1992–95 military violence in Bosnia-Herzegovina, hundreds of thousands of survivors of ‘ethnic cleansing’ signed up for return. But most could not fail to notice the dramatic transformations their prewar place of residence had undergone over the past decade. In 2001, people’s memories of the two villages at the heart of this chapter¹ – Bistrica, in a shaded valley, and Izgled, perched on a hill range overlooking the Drina – evoked almost archetypically bucolic landscapes. Meanwhile, however, they had actually become unrecognizable. Buildings had been destroyed, forested areas had been decimated for firewood, and fields</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>The Loss of Home:</title>
                     <subtitle>From Passion to Pragmatism in Cyprus</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Loizos</surname>
                           <given-names>Peter</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>In 1974, following twenty years of intermittent political conflict in Cyprus, there was a military confrontation between the Greek Cypriots and Turkey, which ended with the Greek Cypriots being driven out of 36 per cent of the island, Turkey remaining as an occupying power speaking for the Turkish Cypriot minority. Many Greek and Turkish Cypriots in the island suffered losses and became displaced. In effect, within a few months of the ceasefire of August 1974, the island, an ethnic patchwork until 1963, and then a territory violently disputed between the two main groups in many different sites, was violently divided</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Social Significance of Crossing State Borders:</title>
                     <subtitle>Home, Mobility and Life Paths in the Angolan—Zambian Borderland</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Barrett</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>When the young man Reagan arrived at the UNHCR reception centre in Kalabo Town in Zambia’s Western Province in the final days of the twentieth century, he did so not merely as an ‘Angolan refugee’.¹ In fact, without denying the turmoil and suffering that occasioned thousands of Angolan citizens to cross the border into Zambia, I would argue that this label obscures many facets of the predicament in which Reagan and his family found themselves in Zambia. It fails to convey that Reagan was a fiancé and a young provider for his extended family, an experienced cross-border trader, and once</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Strategies of Visibility and Invisibility:</title>
                     <subtitle>Rumanians and Moroccans in El Ejido, Spain</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Potot</surname>
                           <given-names>Swanie</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>109</fpage>
                  <abstract>
                     <p>The violence encountered by the migrants discussed in this chapter is of several kinds: they were confronted with the ‘structural violence’ of poverty and economic crisis in their countries of origin (Galtung 1969), and they are now facing racist violence in the place they have migrated to in order to work. Categorised not as refugees but as economic migrants, they do not benefit from any sort of international protection regime.</p>
                     <p>This analysis will focus on the Almeria province of Andalusia, in the south of Spain, where large numbers of foreign workers from various origins are employed in agriculture. In this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>A New Morning?</title>
                     <subtitle>Reoccupying Home in the Aftermath of Violence in Sri Lanka</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Thiranagama</surname>
                           <given-names>Sharika</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>Malathi’s tone was measured.¹ We were sitting in the kitchen of the house she had just moved into. It stands in one of Colombo’s busy, thriving old neighborhoods, one of the first on the sandy road that leads off a main street to this cluster of houses jostling for space. Malathi is a Jaffna Tamil in her mid-thirties living in Colombo, the capital of Sri Lanka, with her mother, her husband and her two daughters, Ovia and Rosa. She was born in Colombo but in 1983 her father and eldest brother were murdered in the worst anti-Tamil riots in Colombo</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Liberal Emplacement:</title>
                     <subtitle>Violence, Home and the Transforming Space of Popular Protest in Central America</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Löfving</surname>
                           <given-names>Staffan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>From the perspective of a participant observer in anti-regime demonstrations in Serbia in 1996, Stef Jansen notes how demonstrators ‘disentangled and re-entangled power relations through oppositional spatial practices … , inserting their bodies into public spaces, and thereby probing the limits of regime control’ (Jansen 2001: 40). This chapter draws inspiration from such a focus on spatiality and meaning in and of protest, making the relation of public space to regime control in Europe speak to the relation of rural territories to state power and national space in Central America (see Stepputat 1994, 2001). In contrast to spaces where different</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.11</book-part-id>
                  <title-group>
                     <title>Postscript:</title>
                     <subtitle>Home, Fragility and Irregulation: Reflections on Ethnographies of Im/mobility</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Stepputat</surname>
                           <given-names>Finn</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>173</fpage>
                  <abstract>
                     <p>My reflections on the contributions to this volume will not add many new points, but will rather, drawing upon my own experience in the field, restate and tie together some of the points which to me are the most important ones brought forth in the volume. While doing this I will also elaborate on the points that I find particularly promising in terms of directions for future research.</p>
                     <p>The volume deals with the dynamics and effects of violence and movement as two of the most obvious processes challenging the meaning and taken-for-granted-ness of ‘home’. Together, the chapters represent a laudable</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.12</book-part-id>
                  <title-group>
                     <title>Notes on Contributors</title>
                  </title-group>
                  <fpage>183</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qd582.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>185</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
