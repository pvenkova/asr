<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">kewbulletin</journal-id>
         <journal-id journal-id-type="jstor">j101220</journal-id>
         <journal-title-group>
            <journal-title>Kew Bulletin</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Trustees, Royal Botanic Gardens, Kew</publisher-name>
         </publisher>
         <issn pub-type="ppub">00755974</issn>
         <issn pub-type="epub">1874933X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/4111200</article-id>
         <title-group>
            <article-title>A Taxonomic Revision of the Gymnosporia mossambicensis Group (Celastraceae: Celastroideae)</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>M. Jordaan</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>A. E.</given-names>
                  <surname>van Wyk</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">58</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i381305</issue-id>
         <fpage>833</fpage>
         <lpage>866</lpage>
         <page-range>833-866</page-range>
         <permissions>
            <copyright-statement>Copyright 2003 The Board of Trustees of The Royal Botanic Gardens, Kew</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4111200"/>
         <abstract>
            <p>The monophyletic Gymnosporia mossambicensis group occurs in eastern tropical and southern Africa, nearby Indian Ocean Islands and on Madagascar. Members are characterised, among other characters, by 3-locular capsules and seeds completely enveloped by an orange or bright yellow aril. Eleven species and four subspecies are recognised. Diagnostic characters to differentiate among species include growth form, hairiness of leaves, branches and peduncles, orientation of spines, presence and shape of stipules, colour of the flowers (white or red) and the shape of the capsules. Two species and one subspecies from Tanzania are newly described (G. livingstonei, G. schliebenii and G. gradlis subsp. usambarensis). G. harveyana Loes. [previously considered conspecific with Maytenus mossambicensis (Klotzsch) Blakelock)] is reinstated as a distinct species. Maytenus mossambicensis var. stolzii N. Robson is treated as a subspecies of G. harveyana. G. bachmannii Loes., G. mossambicensis sensu stricto and G. nemorosa (Eckl. &amp; Zeyh.) Szyszyl. retain their specific status in Gymnosporia. Maytenus drummondii N. Robson &amp; Sebsebe, M. vanwykii R. H. Archer, M. mossambicensis var. gurueensis N. Robson and var. ruber (Harv.) Blakelock are transferred to Gymnosporia and the latter two varieties raised to species level. Maytenus mossambicensis var. ambonensis (Loes.) N. Robson is transferred to Gymnosporia and becomes G. gracilis Loes. subsp. gracilis. Two neotypes and two lectotypes are selected. A key to species and subspecies, distribution maps, and illustrations of all the taxa are provided.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d764e364a1310">
            <mixed-citation id="d764e368" publication-type="book">
Archer, R. H. &amp; Jordaan, M. (2000). Celastraceae. In: O. A. Leistner (ed.), Seed plants
of southern Africa: families and genera. Strelitzia 10: 214-220. National
Botanical Institute, Pretoria.<person-group>
                  <string-name>
                     <surname>Archer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Celastraceae</comment>
               <fpage>214</fpage>
               <volume>10</volume>
               <source>Seed plants of southern Africa: families and genera</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d764e406a1310">
            <mixed-citation id="d764e410" publication-type="book">
Beentje, H. J. (1994). Kenya Trees, Shrubs and Lianas. National Museums of Kenya,
Nairobi.<person-group>
                  <string-name>
                     <surname>Beentje</surname>
                  </string-name>
               </person-group>
               <source>Kenya Trees, Shrubs and Lianas</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e435a1310">
            <mixed-citation id="d764e439" publication-type="journal">
Blakelock, R. A. (1957). Notes on African Celastraceae III. Kew Bull. 12: 37 - 39.<person-group>
                  <string-name>
                     <surname>Blakelock</surname>
                  </string-name>
               </person-group>
               <fpage>37</fpage>
               <volume>12</volume>
               <source>Kew Bull.</source>
               <year>1957</year>
            </mixed-citation>
         </ref>
         <ref id="d764e468a1310">
            <mixed-citation id="d764e472" publication-type="journal">
Brown, N. E. (1906). Diagnoses Africanae: XIV. Bull. Misc. Inform., Kew 1906: 15 - 30.<person-group>
                  <string-name>
                     <surname>Brown</surname>
                  </string-name>
               </person-group>
               <fpage>15</fpage>
               <volume>1906</volume>
               <source>Bull. Misc. Inform., Kew</source>
               <year>1906</year>
            </mixed-citation>
         </ref>
         <ref id="d764e502a1310">
            <mixed-citation id="d764e506" publication-type="book">
Clarke, G. P. (1998). A new Regional Centre of Endemism in Africa. In: C. R.
Huxley, J. M. Lock, &amp; D. F. Cutler (eds.), Chorology, taxonomy and ecology of the
floras of Africa and Madagascar. Royal Botanic Gardens, Kew.<person-group>
                  <string-name>
                     <surname>Clarke</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A new Regional Centre of Endemism in Africa</comment>
               <source>Chorology, taxonomy and ecology of the floras of Africa and Madagascar</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d764e538a1310">
            <mixed-citation id="d764e542" publication-type="book">
Davis, S. D., Heywood, V. H. &amp; Hamilton, (1994). (eds). Centres of Plant
Diversity. A guide and strategy for their conservation. Vol. 1. World Wide Fund
for Nature &amp; World Conservation Union.<person-group>
                  <string-name>
                     <surname>Davis</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Centres of Plant Diversity</comment>
               <volume>1</volume>
               <source>A guide and strategy for their conservation</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e577a1310">
            <mixed-citation id="d764e581" publication-type="book">
Gunn, M. &amp; Codd, L. E. (1981). Botanical exploration of southern Africa. Balkema,
Cape Town.<person-group>
                  <string-name>
                     <surname>Gunn</surname>
                  </string-name>
               </person-group>
               <source>Botanical exploration of southern Africa</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d764e606a1310">
            <mixed-citation id="d764e610" publication-type="book">
Izidine, S. &amp; Bandeira, S. O. (2002). Mozambique. In: J. S. Golding (ed.), Southern
African Plant Red Data Lists. Botanical Diversity Network Report No. 14: 46 - 60.
SABONET, Pretoria.<person-group>
                  <string-name>
                     <surname>Izidine</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Mozambique</comment>
               <fpage>46</fpage>
               <volume>14</volume>
               <source>Southern African Plant Red Data Lists</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d764e648a1310">
            <mixed-citation id="d764e652" publication-type="other">
Jordaan, M. (1995). A taxonomic revision of the spiny members of subfamily
Celastroideae (Celastraceae) in southern Africa. M.Sc. thesis, University of
Pretoria, Pretoria.</mixed-citation>
         </ref>
         <ref id="d764e665a1310">
            <mixed-citation id="d764e669" publication-type="journal">
--- &amp; Van Wyk, A. E. (1999). Systematic studies in subfamily Celastroideae
(Celastraceae) in southern Africa: reinstatement of the genus Gymnosporia. S.
African J. Bot. 65: 177-181.<person-group>
                  <string-name>
                     <surname>Jordaan</surname>
                  </string-name>
               </person-group>
               <fpage>177</fpage>
               <volume>65</volume>
               <source>S. African J. Bot.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d764e705a1310">
            <mixed-citation id="d764e709" publication-type="book">
Klotzsch, J. F. (1862). Peters, Naturw. Reise Mossambique, Botanik 6, 1: 112. Berlin.<person-group>
                  <string-name>
                     <surname>Klotzsch</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Peters, Naturw</comment>
               <fpage>112</fpage>
               <volume>1</volume>
               <source>Reise Mossambique, Botanik</source>
               <year>1862</year>
            </mixed-citation>
         </ref>
         <ref id="d764e741a1310">
            <mixed-citation id="d764e745" publication-type="journal">
Loesener, L. E. T. (1893). Celastraceae africanae 1. Bot. Jahrb. Syst. 17: 541 - 546.<person-group>
                  <string-name>
                     <surname>Loesener</surname>
                  </string-name>
               </person-group>
               <fpage>541</fpage>
               <volume>17</volume>
               <source>Bot. Jahrb. Syst.</source>
               <year>1893</year>
            </mixed-citation>
         </ref>
         <ref id="d764e774a1310">
            <mixed-citation id="d764e778" publication-type="journal">
--- (1894). Beiträge zur Flora von Afrika 8. Celastraceae africanae 2. Bot. Jahrb.
Syst. 19: 231 - 233.<person-group>
                  <string-name>
                     <surname>Loesener</surname>
                  </string-name>
               </person-group>
               <fpage>231</fpage>
               <volume>19</volume>
               <source>Bot. Jahrb. Syst.</source>
               <year>1894</year>
            </mixed-citation>
         </ref>
         <ref id="d764e810a1310">
            <mixed-citation id="d764e814" publication-type="journal">
--- (1896). Celastraceae africanae 3. Bull. Herb. Boissier 4: 429 - 430.<person-group>
                  <string-name>
                     <surname>Loesener</surname>
                  </string-name>
               </person-group>
               <fpage>429</fpage>
               <volume>4</volume>
               <source>Bull. Herb. Boissier</source>
               <year>1896</year>
            </mixed-citation>
         </ref>
         <ref id="d764e843a1310">
            <mixed-citation id="d764e847" publication-type="journal">
--- (1908). Celastraceae africanae 4. Bot. Jahrb. Syst. 41: 298 - 312.<person-group>
                  <string-name>
                     <surname>Loesener</surname>
                  </string-name>
               </person-group>
               <fpage>298</fpage>
               <volume>41</volume>
               <source>Bot. Jahrb. Syst.</source>
               <year>1908</year>
            </mixed-citation>
         </ref>
         <ref id="d764e876a1310">
            <mixed-citation id="d764e880" publication-type="journal">
Marais, W. (1960). An enumeration of the Maytenus species of southern Africa.
Bothalia 7: 381 - 386.<person-group>
                  <string-name>
                     <surname>Marais</surname>
                  </string-name>
               </person-group>
               <fpage>381</fpage>
               <volume>7</volume>
               <source>Bothalia</source>
               <year>1960</year>
            </mixed-citation>
         </ref>
         <ref id="d764e913a1310">
            <mixed-citation id="d764e917" publication-type="journal">
Robson, N. K. B. (1965). New and little known species from the Flora zambesiaca
area XVI. Bol. Soc. Brot., sér. 2, 39: 6 - 25.<person-group>
                  <string-name>
                     <surname>Robson</surname>
                  </string-name>
               </person-group>
               <comment content-type="series title">sér. 2</comment>
               <fpage>6</fpage>
               <volume>39</volume>
               <source>Bol. Soc. Brot.</source>
               <year>1965</year>
            </mixed-citation>
         </ref>
         <ref id="d764e952a1310">
            <mixed-citation id="d764e956" publication-type="book">
--- (1966). Celastraceae. In: A. W. Exell &amp; H. Wild (eds.), Flora Zambesiaca, vol. 2:
355 - 418. Crown Agents for Overseas Governments and Administrations,
London.<person-group>
                  <string-name>
                     <surname>Robson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Celastraceae</comment>
               <fpage>355</fpage>
               <volume>2</volume>
               <source>Flora Zambesiaca</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d764e994a1310">
            <mixed-citation id="d764e998" publication-type="book">
--- (1994). Celastraceae. Maytenus. In: R. M. Polhill (ed.), Flora of Tropical East
Africa, Celastraceae: 1-21. Balkema, Rotterdam.<person-group>
                  <string-name>
                     <surname>Robson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Celastraceae. Maytenus</comment>
               <fpage>1</fpage>
               <source>Flora of Tropical East Africa, Celastraceae</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1030a1310">
            <mixed-citation id="d764e1034" publication-type="book">
--- &amp; Sousa, E. P. (1969). Celastraceae. In: A. Fernandes (ed.), Flora de
Moçambique 48. Junta de Investigações do Ultramar, Lisbon.<person-group>
                  <string-name>
                     <surname>Robson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Celastraceae</comment>
               <fpage>48</fpage>
               <source>Flora de Moçambique</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1066a1310">
            <mixed-citation id="d764e1070" publication-type="book">
Scott-Shaw, R. (1999). Rare and threatened plants of KwaZulu-Natal and
neighbouring regions. KwaZulu-Natal Nature Conservation Service,
Pietermaritzburg.<person-group>
                  <string-name>
                     <surname>Scott-Shaw</surname>
                  </string-name>
               </person-group>
               <source>Rare and threatened plants of KwaZulu-Natal and neighbouring regions</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1099a1310">
            <mixed-citation id="d764e1103" publication-type="book">
Thomas, R. J., Marshall, C. G. A., Du Plessis, A., Fitch, F. J., Miller, J. A., Von Brunn,
V. &amp; Watkeys, M. K. (1992). Geological studies in southern Natal and Transkei:
implications for the Cape Oregon. In: M. J. de Wit &amp; I. G. D. Ransome (eds),
Inversion tectonics of the Cape Fold Belt, Karoo and Cretaceous basins of
southern Africa. Balkema, Rotterdam.<person-group>
                  <string-name>
                     <surname>Thomas</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Geological studies in southern Natal and Transkei: implications for the Cape Oregon</comment>
               <source>Inversion tectonics of the Cape Fold Belt, Karoo and Cretaceous basins of southern Africa</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1142a1310">
            <mixed-citation id="d764e1146" publication-type="book">
Van Wyk, A. E. (1994). Maputaland-Pondoland region. In: S. D. Davis, V. H.
Heywood &amp; A. C. Hamilton (eds.), Centres of plant diversity: a guide and strategy
for their conservation 1. IUCN Publications Unit, Cambridge.<person-group>
                  <string-name>
                     <surname>Van Wyk</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Maputaland-Pondoland region</comment>
               <fpage>1</fpage>
               <source>Centres of plant diversity: a guide and strategy for their conservation</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1181a1310">
            <mixed-citation id="d764e1185" publication-type="book">
--- &amp; Smith, G. F. (2001). Regions of floristic endemism in southern Africa.
Umdaus, Pretoria.<person-group>
                  <string-name>
                     <surname>Van Wyk</surname>
                  </string-name>
               </person-group>
               <source>Regions of floristic endemism in southern Africa</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1210a1310">
            <mixed-citation id="d764e1214" publication-type="book">
Van Wyk, B. &amp; Van Wyk, P. (1997). Trees of southern Africa. Struik, Cape Town.<person-group>
                  <string-name>
                     <surname>Van Wyk</surname>
                  </string-name>
               </person-group>
               <source>Trees of southern Africa</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d764e1236a1310">
            <mixed-citation id="d764e1240" publication-type="book">
White, F. (1983). The vegetation of Africa: a descriptive memoir to accompany the
Unesco/AETFAT/UNSO vegetation map of Africa. Unesco, Paris.<person-group>
                  <string-name>
                     <surname>White</surname>
                  </string-name>
               </person-group>
               <source>The vegetation of Africa: a descriptive memoir to accompany the Unesco/AETFAT/UNSO vegetation map of Africa</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
