<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.3998/mpub.13434</book-id>
      <subj-group>
         <subject content-type="call-number">HM495.C43 2000</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Rational choice theory</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Group identity</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Identity (Psychology)</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Choosing an Identity</book-title>
         <subtitle>A General Model of Preference and Belief Formation</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Chai</surname>
               <given-names>Sun-Ki</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>06</day>
         <month>05</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780472107018</isbn>
      <isbn content-type="epub">9780472023950</isbn>
      <publisher>
         <publisher-name>University of Michigan Press</publisher-name>
         <publisher-loc>ANN ARBOR</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2001</copyright-year>
         <copyright-holder>University of Michigan</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3998/mpub.13434"/>
      <abstract abstract-type="short">
         <p>Social science research is fragmented by the widely differing and seemingly contradictory approaches used by the different disciplines of the social sciences to explain human action. Attempts at integrating different social science approaches to explain action have often been frustrated by the difficulty of incorporating cultural assumptions into rational choice theories without robbing them of their generality or making them too vague for predictions. Another problem has been the major disagreements among cultural theorists regarding the ways in which culture affects preferences and beliefs.</p>
         <p>This book provides a general model of preference and belief formation, addressing the largest unresolved issue in rational choice theories of action. It attempts to play a bridging role between these approaches by augmenting and modifying the main ideas of the "rational choice" model to make it more compatible with empirical findings in other fields. The resulting model is used to analyze three major unresolved issues in the developing world: the sources of a government's economic ideology, the origins of ethnic group boundaries, and the relationship between modernization and violence.</p>
         <p>Addressing theoretical problems that cut across numerous disciplines, this work will be of interest to a diversity of theoretically-minded scholars.</p>
         <p>Sun-Ki Chai is Assistant Professor of Sociology, University of Arizona.</p>
      </abstract>
      <counts>
         <page-count count="336"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.4</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>The Success and Failure of Rational Choice</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The rational choice approach, despite widespread criticism, has reached a point of unrivaled prominence among general theoretical approaches for explaining human action. This prominence extends across the entire range of social sciences. In economics, rational choice remains unchallenged as the dominant, if not defining, theoretical paradigm, and is sometimes referred to simply as the "economic approach."¹ In political science, largely under auspices of the public choice school, the rational choice approach has grown to the point where it has more adherents than any other, and its threatened dominance has set off an intense debate that has polarized the discipline.² In</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.5</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>Alternatives to Conventional Rational Choice:</title>
                     <subtitle>A Survey</subtitle>
                  </title-group>
                  <fpage>24</fpage>
                  <abstract>
                     <p>This chapter examines a wide range of existing alternatives to the conventional rational choice model. Because of the large number of approaches covered, the reviews are too brief to provide an overall analysis or critique of each. Instead, I attempt to evaluate each approach's potential for remedying the major predictive shortcomings of conventional rational choice while retaining its strengths as a deductive, general, and predictive model. This involves analyzing the models found in each approach in terms of the main criteria described in the previous chapter:<italic>generality</italic>, the range of environments within which the models's assumptions are applicable;<italic>parsimony</italic>, the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.6</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>A General Model of Preference and Belief Formation</title>
                  </title-group>
                  <fpage>81</fpage>
                  <abstract>
                     <p>In this chapter, I present a general model of preference and belief formation. It is complementary to the rational optimization model of decision making, and is integrated with it to form the basis for a revised general model of action. In the model, choice of action isseen as the result of a decision-making process aimed at maximizing preferences given beliefs, but also triggers changes in these preferences and beliefs.</p>
                     <p>The next section of the chapter defines the concepts of regret and coherence that are central to the model, then discuss the model's basic assumptions. The remaining sections analyze preference and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.7</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>Ideology Formation and Policy Choice in Ex-Colonies</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>The social science literature is full of works that purport to recommend the correct economic policies for developing countries. However, this wealth of analysis is not matched by theories that attempt to explain the policy choices that Third World states actually make and the reasons for those choices. Positive analysis of economic policy formation in developing countries is a relatively recent academic phenomenon, and theories that attempt to explain variations in development strategies across a wide range of countries are close to nonexistent. Given the obvious centrality of this issue, the absence of systematic explanations for such variations presents a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.8</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>The Origins of Ethnic Identity and Collective Action</title>
                  </title-group>
                  <fpage>174</fpage>
                  <abstract>
                     <p>Ethnicity is an issue that long has occupied social scientists in general, but until recently received little attention from rational choice theorists. The reason for this is clear: the causal significance of ethnicity fits very uncomfortably with the conventional assumptions of rational choice. Since these assumptions revolve around the pursuit of economic self-interest, this implies that collective action should occur among individuals with shared economic interests. These interests in turn are reflections of individual economic characteristics, such as income, occupation and job skills. However, although ethnicity may be correlated with economic characteristics, no commonly accepted definition of ethnicity defines it</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.9</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>Structural Change, Cultural Change, and Civic Violence</title>
                  </title-group>
                  <fpage>213</fpage>
                  <abstract>
                     <p>Perhaps the single area of social science in which rational choice theories have had the least impact is in explaining long-term social, political and economic change. These topics, which have generally been studied under such theoretical labels as modernization, political development, and social change,¹ have been for the most part been ignored by rational choice theorists.</p>
                     <p>While it is difficult to extract a single reason why this is so, it is useful to note that almost all non-Marxist theories of longterm change implicitly or explicitly make use of cultural change as an independent causal variable or an intervening variable between</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.10</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>242</fpage>
                  <abstract>
                     <p>In concluding, I will try to present ways in which the theory can be augmented and improved as well as justifications for its basic shape and form. As was stated in the beginning of this book, I do not mean to propose that there is a single model which can predict all of human behavior or that a single actor model should be hegemonic in social science analysis. I simply attempted to provide a deductive model of preference and belief formation and to examine its implications in conjunction with the rational optimization assumption for empirical problems in comparative politics. Given</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.11</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>257</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.13434.12</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>321</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
