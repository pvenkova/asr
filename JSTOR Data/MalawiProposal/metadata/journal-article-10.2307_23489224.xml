<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">irisstudinteaffa</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000059</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Irish Studies in International Affairs</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Royal Irish Academy</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03321460</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">20090072</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23489224</article-id>
         <article-categories>
            <subj-group>
               <subject>ANNUAL REVIEWS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Ireland's Foreign Aid in 2011</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Helen</given-names>
                  <surname>O'Neill</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">23</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23488623</issue-id>
         <fpage>223</fpage>
         <lpage>240</lpage>
         <permissions>
            <copyright-statement>© Royal Irish Academy 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23489224"/>
         <abstract>
            <p>This paper reviews Ireland's official development assistance (ODA) programme in 2011. It opens with an overview of developments relating to the global economy and policy last year. It then presents an overview of official aid flows from DAC donors in 2011, which amounted to US$133.5 billion or 0.31% of their combined gross national income (GNI). This figure represented a fall of 2.7% in real terms compared with 2010 when ODA was at its peak. Irish ODA amounted to €657 million, a fall of €19 million or 2.8% compared with 2010, which itself had represented a fall of nearly 6% compared with 2009. As a percentage of gross national income (GNI), Irish ODA remained static at 0.52% in 2011. The paper provides data on total Irish ODA for selected years since 1991, and a detailed breakdown of bilateral and multilateral aid expenditures for 2010 and 2011. It then analyses expenditure under both the bilateral and multilateral sides of the programme. Under the former, it first provides an overview of Irish Aid's assistance to all its partner countries (PCs) and under all its thematic headings and then focuses on two topics: assistance to its 'Other countries' and changes in the classification of Ireland's partner countries, as well as comparative data on other DCs. Under the multilateral side of the programme, the paper examines Ireland's work with the EU, the international financial institutions and the UN.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1271e152a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1271e159" publication-type="other">
United Nations Department of Economic and Social Affairs, World economic situation and
prospects 2012: update as of mid-2012 (New York, 2012), 1, available at: http://www.un.org/en/
development/desa/policy/wesp/wesp_archive/2012wespupdate.pdf (2 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e172a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1271e179" publication-type="other">
World Trade Organisation, 'World trade 2011, prospects for 2012', Press Release/658/Rev. 1,
10 May 2012; available at: http://www.wto.org/englishynews_e/presl2_e/pr658_e.pdf (2 August
2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e192a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1271e199" publication-type="other">
United Nations Conference on Trade and Development, 'Global investment trends monitor',
no. 9,12 April 2012, available at: http://unctad.org/en/PublicationsLibrary/webdiaela2012dl9_en.
pdf (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e212a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1271e219" publication-type="other">
World Bank, Global economic prospects: managing growth in a volatile world, June 2012;
available at: http://web.worldbank.org/WBSITE/EXTERNAL/EXTDEC/EXTDECPROS-
PECTS/EXTGBLPROSPECTS APRIL/0„menuPK:659178 ~ pagePK:64218926 ~ piPK:6421895
3~theSitePK:659149,00.html (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e236a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1271e243" publication-type="other">
http://www.aideffectiveness.
org/busanhlf4/ (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e253a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1271e260" publication-type="other">
http://www.
oecd.org/dac/aideflfectiveness/31451637.pdf (7 August 2012)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1271e269" publication-type="other">
http://www.oecd.org/dataoecd/ll/
41/34428351.pdf (7 August 2012)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1271e278" publication-type="other">
http://www.oecd.org/dataoecd/58/16/41202012.pdf (7 Aug-
ust 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e288a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1271e295" publication-type="other">
Organisation for Economic Cooperation and Development, Development Assistance
Committee (hereafter cited as OECD/DAC), Assessing progress on implementing the Paris
Declaration and the Accra Agenda for Action (2011), available at: http://www.oecd.org/dac/
aideffectiveness/assessingprogressonimplementingtheparisdeclarationandtheaccraagendaforaction.
htm (4 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e314a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1271e321" publication-type="other">
Helen O'Neill, 'Ireland's foreign aid in 2010', Irish Studies in International Affairs 22 (2011),
187-223, 192-3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e331a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1271e338" publication-type="other">
Fourth High Level Forum on Aid Effectiveness, Busan partnership for effective development
cooperation (Busan, 2011), available at: www.aideffectiveness.org/busanhlf4/en/component/con-
tent/article/698.html (4 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e351a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1271e358" publication-type="other">
http://www.aideflfectiveness.org/busanhlf4/
about/global-partnership.html (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e369a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1271e376" publication-type="other">
1ATI are available at: http://www.aidtransparency.net (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e383a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1271e390" publication-type="other">
OECD, Aid statistics', available at: http://www.oecd.0rg/documentprint/0,3455,en_2649_
34447_44089692_l_l_l_L00.html (6 April 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e400a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1271e407" publication-type="other">
OECD/DAC, 'Outlook on aid: survey on donors' forward spending plans 2012-2015',
available at: http://www.oecd.org/development/aideffectiveness/50056866.pdf (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e417a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1271e424" publication-type="other">
Center for Global Development, Commitment to development index 2011, available at http://
www.cgdev.org/section/initiatives/_active/cdi/ (1 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e434a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1271e441" publication-type="other">
CGD overview on Ireland's 2011 performance at: http://www.cgdev.org/doc/CDI%
20201 l/Country_l l_Ireland_EN.pdf (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e451a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1271e458" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2010', 197.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e466a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1271e473" publication-type="other">
Helen O'Neill, 'Ireland's Foreign Aid in 2008', Irish Studies in International Affairs 20 (2009),
193-222, 214-17.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e483a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1271e490" publication-type="other">
O'Neill, 'Ireland's foreign aid in 2010', 198.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e497a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1271e504" publication-type="other">
Helen O'Neill, 'HICs, MICs, LICs and NICs: Some elements in the political economy of
graduation and differentiation', World Development 12 (7) (1984), 693-712.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e514a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1271e521" publication-type="other">
World Bank, 'How we classify countries', (2012), available at: http://data.worldbank.org/
about/country-classifications (8 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e531a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1271e538" publication-type="other">
Birzeit University, in association with the Ministry of Planning and with support from
UNDP (2005), Palestine human development report 2004, available at: http://www.undp.ps/en/
newsroom/publications/pdf/other/phdr2004.pdf</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e551a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1271e558" publication-type="other">
European Commission, Communication from the Commission to the European Parliament,
the Council, the European Economic and Social Committee and the Committee of the Regions:
'Increasing the impact of EU development policy: an agenda for change', COM(2011) 637 final
(Brussels, 2011), available at: http://eur-lex.europa.eu/LexUriServ/LexUriServ.do7uri =COM:
2011:0637:FIN:EN:PDF (11 August 2012). Hereafter cited as European Commission, 'Agenda
for change'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e582a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1271e589" publication-type="other">
European Commission, Communication from the Commission to the European Parliament,
the Council, the European Economic and Social Committee and the Committee of the Regions:
'The future approach to EU budget support to third countries', COM(2011) 638 final) Brussels,
2011), available at: http://eur-lex.europa.eu/LexUriServ/LexUriServ.do7uri =COM:2011:0638:
FIN:EN:PDF (11 August 2012). Hereafter cited as European Commission, 'Budget support to
third countries'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e612a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1271e619" publication-type="other">
Ivy Mungcal, 'NGOs reaffirm concerns over new EU development agenda', Devex, 29 May
2012, available at: http://www.devex.com/en/news/78315/print (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e629a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1271e636" publication-type="other">
European Commission, Communication from the Commission to the European Parliament,
the Council, the European Economic and Social Committee and the Committee of the Regions:
'Improving EU support to developing countries in mobilising financing for development',
COM(2012) 366 (Brussels 2012), available at: http://ec.europa.eu/europeaid/what/development-
policies/financing-for-development/documents/com-366-swp-199-annex4.pdf (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e655a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1271e662" publication-type="other">
European Commission, 'Agenda for change'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e669a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1271e676" publication-type="other">
European Commission, 'Budget support to third countries'.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e683a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1271e690" publication-type="other">
Jorg Faust, Svea Koch, Nadia Molenaers, Heidi Tavakoli and Jan Vanheukelom, The future
of EU budget support: political conditions, differentiation and coordination (European Think-Tanks
Group and University of Antwerp, May 2012), available at: http://www.odi.org.uk/resources/
docs/7671.pdf (7 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e707a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1271e714" publication-type="other">
Faust et al., Future of EU budget support, 1.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e721a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1271e728" publication-type="other">
House of Commons Select Committee, EU Development Assistance: written evidence
submitted by Open Europe (London, 2012), 3, available at http://www.publications.parliament.
uk/pa/cm201012/cmselect/cmintdev/writev/1680/m8.htm (8 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e741a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1271e748" publication-type="other">
House of Commons Select Committee, EU Development Assistance.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e755a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1271e762" publication-type="other">
House of Commons Select Committee, EU Development Assistance, 4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e769a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1271e776" publication-type="other">
Geert Laporte, The ACP and Europe: what future for a privileged relationship? (European
Centre for Development Policy Management, 20 May 2011), available at http://ecdpm-
talkingpoints.org/the-acp-and-europe/ (8 August 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e789a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1271e796" publication-type="other">
Department of Finance, Ireland's participation in the International Monetary Fund and World
Bank: annual report 2011 (Dublin, March 2012), 28-29, available at http://www.finance.gov.ie/
documents/publications/reports/2012/worldbank2012.pdf (30 July 2012).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1271e808" publication-type="other">
Michael Noonan's statement to the September 2011 meeting of the IMF/World Bank is available
at: http://www.imf.org/external/am/2011/speeches/pr35e.pdf (30 July 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e819a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1271e826" publication-type="other">
World Bank, World development report: gender equality and development (Washington, DC,
2011).</mixed-citation>
            </p>
         </fn>
         <fn id="d1271e836a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1271e843" publication-type="other">
Department of Finance, 'Statement of Ms Niamh Campbell, Head of Delegation at the
44th annual meeting, Board of Governors of the Asian Development Bank, Hanoi, Vietnam, 3-6
May 2011', made available to this writer by the Department of Finance on 9 August 2012.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
