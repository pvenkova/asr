<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt1551n80</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt18dzqzf</book-id>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Health Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>Beyond the state</book-title>
         <subtitle>The colonial medical service in British Africa</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>Greenwood</surname>
               <given-names>Anna</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>12</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="ppub">9780719089671</isbn>
      <isbn content-type="epub">9781784996789</isbn>
      <publisher>
         <publisher-name>Manchester University Press</publisher-name>
         <publisher-loc>Manchester</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Manchester University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt18dzqzf"/>
      <abstract abstract-type="short">
         <p>The Colonial Medical Service was the personnel section of the Colonial Service, employing the doctors who tended to the health of both the colonial staff and the local populations of the British Empire. Although the Service represented the pinnacle of an elite government agency, its reach in practice stretched far beyond the state, with the members of the African service collaborating, formally and informally, with a range of other non-governmental groups. This collection of essays on the Colonial Medical Service of Africa illustrates the diversity and active collaborations to be found in the untidy reality of government medical provision. The authors present important case studies covering former British colonial dependencies in Africa, including Kenya, Malawi, Nigeria, Tanzania, Uganda and Zanzibar. They reveal many new insights into the enactments of colonial policy and the ways in which colonial doctors negotiated the day-to-day reality during the height of imperial rule in Africa. The book provides essential reading for scholars and students of colonial history, medical history and colonial administration.</p>
      </abstract>
      <counts>
         <page-count count="208"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.3</book-part-id>
                  <title-group>
                     <title>NOTES ON CONTRIBUTORS</title>
                  </title-group>
                  <fpage>vi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGEMENTS</title>
                  </title-group>
                  <fpage>viii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.5</book-part-id>
                  <title-group>
                     <label>CHAPTER ONE</label>
                     <title>Introduction:</title>
                     <subtitle>looking beyond the state</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Greenwood</surname>
                           <given-names>Anna</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>There is no fresh news in stating that the history of colonial medicine has changed considerably in the last seventy-five years. As academic interests have expanded, attention has moved away from triumphalist accounts of the conquest of disease in former European colonies to a more critical, less ethno-centric and more socially inclusive examination of the complex relationships between colonial states and colonised societies. Yet, despite much self-congratulation at achieving a comparatively nuanced understanding of these relationships, glaring gaps remain and there is work still to be done. Although certain colonial institutions and policies have been revisited and reassessed by historians</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.6</book-part-id>
                  <title-group>
                     <label>CHAPTER TWO</label>
                     <title>Crossing the divide:</title>
                     <subtitle>medical missionaries and government service in Uganda, 1897–1940</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pringle</surname>
                           <given-names>Yolana</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>One of the distinctive features of Western medical practice in early colonial Uganda was the high level of collaboration between mission doctors and the Colonial Medical Service.¹ In the period before 1940, a number of Church Missionary Society (CMS) doctors negotiated dual roles as missionaries and colonial medical officers. An even greater number participated in and managed government health campaigns, or were engaged unofficially by the administration in an advisory capacity. The reasons for collaboration were diverse: some wished to extend the reach of missionary work, some to advance professionally, while others were determined to boost what they felt to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.7</book-part-id>
                  <title-group>
                     <label>CHAPTER THREE</label>
                     <title>The government medical service and British missions in colonial Malawi, c. 1891–1940:</title>
                     <subtitle>crucial collaboration, hidden conflicts</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Hokkanen</surname>
                           <given-names>Markku</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>39</fpage>
                  <abstract>
                     <p>As Megan Vaughan has pointed out, for most of the colonial period in Africa, Christian missions ‘provided vastly more medical care than did colonial states’.¹ Indeed, from the outset of European colonial rule, most imperial states left the provision of the majority of education and healthcare to Christian missions.²</p>
                     <p>In colonial Malawi, missionary medicine preceded British rule by nearly two decades, making it a crucial site for investigating relations and interactions between missions and the state.³ As Vaughan and others have shown, there were notable differences as well as common ground between missionary and secular discourses of African illnesses and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.8</book-part-id>
                  <title-group>
                     <label>CHAPTER FOUR</label>
                     <title>The maintenance of hegemony:</title>
                     <subtitle>the short history of Indian doctors in the Colonial Medical Service, British East Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Greenwood</surname>
                           <given-names>Anna</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Topiwala</surname>
                           <given-names>Harshad</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>64</fpage>
                  <abstract>
                     <p>It is known that an increasing number of Indian doctors came to reside in the East African Protectorate (Kenya after 1920), following its formal colonisation by the British in 1895. What is less known, however, is that although some of these medical immigrants established themselves as private practitioners, the majority of them – at least in the period before 1923 – joined the Colonial Medical Service. Although these Indian practitioners were not appointed to the same rank as the European Medical Officers (MOs), they nevertheless were medically qualified individuals who had undergone training in Western medicine in India, usually for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.9</book-part-id>
                  <title-group>
                     <label>CHAPTER FIVE</label>
                     <title>The Colonial Medical Service and the struggle for control of the Zanzibar Maternity Association, 1918–47</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Greenwood</surname>
                           <given-names>Anna</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>British colonialists on Zanzibar frequently grumbled that its colourful demographic character was a particular headache to their administration. When the British formally established their administration in 1890 they encountered a ‘distinctly urban, mercantile and cosmopolitan’ island economy which had developed over centuries and was headed by Sultan Sayyid Ali bin Said Al-Busaid and his extravagant court.¹ Zanzibar was home to a thriving merchant class of Arabs and Indians, as well as poorer members of these communities, co-existing alongside the majority African poor.² Even British generalisations had to concede that this was far from the supposed blank canvas implied through rhetorical</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.10</book-part-id>
                  <title-group>
                     <label>CHAPTER SIX</label>
                     <title>Elder Dempster and the transport of lunatics in British West Africa</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Heaton</surname>
                           <given-names>Matthew M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>104</fpage>
                  <abstract>
                     <p>In 1954 a Nigerian man named L.S. arrived in the United Kingdom to study carpentry at the L.C.C. School of Building in Brixton.¹ Within eight months of his arrival in the UK, L.S. suffered a mental breakdown. In April 1955 he was admitted to the Warlingham Park Hospital, having developed a ‘strong persecution mania’.² Hospital attendants described him as ‘extremely depressed, agitated and unsure of his surrounding’. He claimed he was about to die and that he heard voices calling his name in his native language.³ With auditory hallucinations ongoing in a patient saddled with what his doctor termed ‘an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.11</book-part-id>
                  <title-group>
                     <label>CHAPTER SEVEN</label>
                     <title>Social disease and social science:</title>
                     <subtitle>the intellectual influence of non-medical research on policy and practice in the Colonial Medical Service in Tanganyika and Uganda</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Doyle</surname>
                           <given-names>Shane</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>126</fpage>
                  <abstract>
                     <p>In scholarship since the mid-1980s colonial medicine has often been described as a key element in the imperial state’s attempt to understand, monitor and control subject communities. Moreover, scholars have noted how colonial hagiographies emphasised doctors’ intimate knowledge of local attitudes and practices, shaped by humanitarian concern and long service, extolled the technological mastery of the imperial scientist, and praised the power of the colonial state to transform communities, legitimised and facilitated by medical expertise.¹ These assertions and ambitions, however, were not always realised. Medical interventions were frequently shaped by racial or political rather than objective, scientific motivations, and their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.12</book-part-id>
                  <title-group>
                     <label>CHAPTER EIGHT</label>
                     <title>Cooperation and competition:</title>
                     <subtitle>missions, the colonial state and constructing a health system in colonial Tanganyika</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Jennings</surname>
                           <given-names>Michael</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>Patchy, incomplete and of varying quality over time and place: nonetheless, since the late nineteenth century, missions and other faithbased organisations and religious institutions have been central to meeting the welfare needs of Africans across the continent, a critical component of what we might consider the public-private mix of public goods provision. As Anna Greenwood reminds us in the Introduction to this volume, ‘colonial medicine’ in Tanganyika (as with other colonies and imperial possessions) was never, and never could have been, solely provided by any one actor. The Colonial Service was neither unified nor unidirectional in its provision, and collaboration</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.13</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>174</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt18dzqzf.14</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>193</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
