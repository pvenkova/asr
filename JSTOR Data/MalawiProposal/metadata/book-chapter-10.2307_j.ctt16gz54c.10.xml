<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt16f99gj</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt16gz54c</book-id>
      <subj-group>
         <subject content-type="call-number">DT443.3.K33B435 2012</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Kaguru (African people)</subject>
         <subj-group>
            <subject content-type="lcsh">Ethnic identity</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Kaguru (African people)</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Great Britain</subject>
         <subj-group>
            <subject content-type="lcsh">Colonies</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
               <subj-group>
                  <subject content-type="lcsh">Administration</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Great Britain</subject>
         <subj-group>
            <subject content-type="lcsh">Colonies</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
               <subj-group>
                  <subject content-type="lcsh">Cultural policy</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Tanzania</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
            <subj-group>
               <subject content-type="lcsh">To 1964</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Anthropology</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Culture of Colonialism</book-title>
         <subtitle>The Cultural Subjection of Ukaguru</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>BEIDELMAN</surname>
               <given-names>T. O.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>27</day>
         <month>06</month>
         <year>2012</year>
      </pub-date>
      <isbn content-type="ppub">9780253002150</isbn>
      <isbn content-type="epub">9780253002204</isbn>
      <isbn content-type="epub">0253002206</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington; Indianapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2012</copyright-year>
         <copyright-holder>T. O. Beidelman</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt16gz54c"/>
      <abstract abstract-type="short">
         <p>What did it mean to be an African subject living in remote areas of Tanganyika at the end of the colonial era? For the Kaguru of Tanganyika, it meant daily confrontation with the black and white governmental officials tasked with bringing this rural people into the mainstream of colonial African life. T. O. Beidelman's detailed narrative links this administrative world to the Kaguru's wider social, cultural, and geographical milieu, and to the political history, ideas of indirect rule, and the white institutions that loomed just beyond their world. Beidelman unveils the colonial system's problems as it extended its authority into rural areas and shows how these problems persisted even after African independence.</p>
      </abstract>
      <counts>
         <page-count count="414"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.4</book-part-id>
                  <title-group>
                     <title>INTRODUCTION:</title>
                     <subtitle>Colonialism and Anthropology</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>This is an ethnographic study of the colonial life of the Kaguru, most of whom lived in Ukaguru, a chiefdom in Kilosa district in east-central Tanganyika, British East Africa. Yet before considering colonial life in Ukaguru, I need to provide some account of the issues that have been raised in the study of colonialism. This is not easy, since even the meanings of terms such as “colony” and “colonialism” are disputed. Given the complexity of the disagreements about these, I focus on only a few key points in this huge scholarly literature. First, I briefly consider some of the ways</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.5</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>Kaguru and Colonial History:</title>
                     <subtitle>The Rise and Fall of Indirect Rule</subtitle>
                  </title-group>
                  <fpage>39</fpage>
                  <abstract>
                     <p>Ukaguru has a special colonial history in that it embodies a number of firsts.¹ Ukaguru is the site of some of the earliest European settlements in mainland Tanzania and is where the first white child was born on the mainland. These settlements involved Church Missionary Society people (CMS), who founded one of the earliest missions in East Africa in Ukaguru. This mission station was founded because it lay on the caravan route that the missionaries took to reach their main goal for conversion, the great African kingdoms far to the west in what is now Uganda. For similar reasons Ukaguru</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.6</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>Ukaguru 1957–58</title>
                  </title-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>This volume is an account of political life in Ukaguru, Kilosa district, during the years prior to the end of British colonial rule. To make this political account convincing I preface it with information on the general social and economic life within. In this chapter I briefly describe local African settlement and social organization and the ways Africans made a living, the markets, roads, climate, and terrain. I also briefly describe Kaguru beliefs and values, and the Christian mission and government schools, which changed traditional thinking. My study mainly involves the Kaguru, but it also involves the Baraguyu, Kamba, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.7</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>The Kaguru Native Authority</title>
                  </title-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>The Kaguru Native Authority is the official governing body of Ukaguru. It was the outcome of British policy to rule their African possessions through Indirect Rule, allowing the British to govern with minimal staff and funds while claiming to encourage natives both to oversee themselves and to gain experience in modernization at a slow pace that would supposedly accommodate local African values and needs. I have already discussed the pros and cons of Indirect Rule as well as the ways that policy was initiated and pursued in Tanganyika. I sketched out the ties between the local British administration and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.8</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Court Cases:</title>
                     <subtitle>Order and Disorder</subtitle>
                  </title-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>A Kaguru chief’s court was ordinarily held every Saturday at the Native Authority courthouse, usually starting about nine or ten in the morning and continuing nonstop until about four or five in the afternoon. Occasionally court was held on additional days as well. Very rarely a chief might hold an open-air court in an outlying area if he thought the cases and people involved merited this.¹ Applicants with cases registered their suits a few days to several weeks ahead, paying three shillings registration fee (<italic>uda</italic>) in civil cases, a fee later paid by the defendant if he was found liable.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.9</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>Subversions and Diversions:</title>
                     <subtitle>1957–58</subtitle>
                  </title-group>
                  <fpage>196</fpage>
                  <abstract>
                     <p>This chapter is about discontent, about Kaguru striving to change their social world. In 1957–58 Indirect Rule and the Kaguru Native Authority were presented by those in power as manifestations of a social order redolent of Kaguru tradition, an order legitimized by its supposed ties to the past and to Kaguru ethnic identity. These constructions were, at best, half-truths. At worst, the system was oppressive and frustrating, resented by many Africans who lived under it. For many decades the Kaguru colonial world continued against little opposition. Yet the foundation supporting this seeming stability was the fact that Africans knew</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.10</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>The World Beyond:</title>
                     <subtitle>Kaguru Marginality in a Plural World, 1957–61</subtitle>
                  </title-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>Ukaguru is the main subject of this book, but it needs to be seen within the larger context of the colonial system in which it was set, however insignificantly. For this reason, I here consider the chiefdom within the broader context of Kilosa district and, to a lesser extent, Eastern Province. Broader concerns of the district and provincial administrations, the nature of the British colonial civil service, the everyday worlds of the Europeans, Asians, and non-Kaguru Africans who lived outside Ukaguru must be understood to grasp what went on in Ukaguru itself. I began this consideration earlier with a study</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.11</book-part-id>
                  <title-group>
                     <title>EPILOGUE:</title>
                     <subtitle>Independence and After</subtitle>
                  </title-group>
                  <fpage>273</fpage>
                  <abstract>
                     <p>This book is about the colonial experience in Ukaguru, and therefore by strict standards it should not consider events after December 1961, the beginning of Tanganyikan (Tanzanian) national independence. Yet the end of colonialism was not sharply defined. It lingered for at least two more years. The colonial world of Ukaguru truly ceased when the Kaguru chiefships and Native Authority were dissolved in early 1963 (Tordoff 1965:80).¹ Yet other aspects of colonialism lingered on even after the end of Indirect Rule. Many of the changes that eventually took place can best be understood as outcomes of the earlier colonial system.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.12</book-part-id>
                  <title-group>
                     <title>CONCLUSION</title>
                  </title-group>
                  <fpage>286</fpage>
                  <abstract>
                     <p>This study is concerned with one small and relatively remote chiefdom in East Africa. In many ways this system was far more economically and politically undeveloped and insignificant than others that have been described in studies of colonial rule in East Africa. Far more attention has been paid to other peoples and to rich, complex, and populous areas such as the interlacustrine kingdoms of Uganda, the Kikuyu of Kenya, with their violent and tortured relations with British settlers and missionaries, and the prosperous and educated coffee-growing Chagga and Haya of Tanzania (Tanganyika). Ukaguru and its people lacked the drama and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.13</book-part-id>
                  <title-group>
                     <title>APPENDICES</title>
                  </title-group>
                  <fpage>301</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.14</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>317</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.15</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>337</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.16</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>373</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt16gz54c.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>385</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
