<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">imfstaffpapers</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100845</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>IMF Staff Papers</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Palgrave Macmillan</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10207635</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15645150</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25681041</article-id>
         <article-categories>
            <subj-group>
               <subject>Special Section: New Perspectives on African Growth</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Role of Domestic Debt Markets in Economic Growth: An Empirical Investigation for Low-Income Countries and Emerging Markets</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>S.M. ALI</given-names>
                  <surname>ABBAS</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>JAKOB E.</given-names>
                  <surname>CHRISTENSEN</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">57</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25681032</issue-id>
         <fpage>209</fpage>
         <lpage>255</lpage>
         <permissions>
            <copyright-statement>© 2010 International Monetary Fund</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25681041"/>
         <abstract>
            <p>This paper develops a new public domestic debt database covering 93 low-income countries and emerging markets over 1975–2004 to estimate the growth impact of domestic debt. Moderate levels of noninflationary domestic debt, as a share of GDP and bank deposits, are found to exert a positive overall impact on economic growth. Granger-causality regressions suggest support for a variety of channels: improved monetary policy; broader financial market development; strengthened domestic institutions/accountability; and enhanced private savings and financial intermediation. There is some evidence that, above a ratio of 35 percent of bank deposits, domestic debt begins to undermine growth, lending credence to traditional crowding out and bank efficiency concerns. Importantly, the growth contribution of domestic debt is higher if it is marketable, bears positive real interest rates, and is held outside the banking system.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1152e343a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1152e350" publication-type="other">
Eichengreen and Hausmann, 2005</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e357a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1152e364" publication-type="other">
Aizenmann,
Pinto, and Radziwill (2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e374a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1152e381" publication-type="other">
Abbas (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1152e387" publication-type="other">
Beaugrand, Loko, and Mlachila (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e394a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1152e401" publication-type="other">
Agenor and Montiel (1999, Chapter 5)</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e409a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1152e416" publication-type="other">
IMF (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1152e422" publication-type="other">
Detragiache, Tressel, and Gupta (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1152e428" publication-type="other">
Ndikumana
(2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e438a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1152e445" publication-type="other">
Chirwa and Mlachila (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1152e451" publication-type="other">
Barajas, Steiner, and Salazar (1999</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1152e457" publication-type="other">
2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1152e464" publication-type="other">
Brock
and Rojas-Suarez (2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e474a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d1152e481" publication-type="other">
Hauner (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e488a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1152e495" publication-type="other">
Hauner (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e502a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1152e509" publication-type="other">
Abbas (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e516a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1152e523" publication-type="other">
Beck, Demirgiic-Kunt, and Levine (2006; updated from
2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e534a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1152e541" publication-type="other">
Pattillo, Poirson, and Ricci (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1152e548a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1152e555" publication-type="other">
Pattillo, Poirson, and Ricci (2002)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>&lt;bold&gt;References&lt;/bold&gt;</title>
         <ref id="d1152e571a1310">
            <mixed-citation id="d1152e575" publication-type="other">
Abbas, S.M. Ali, 2005, "Public Debt Sustainability and Growth in Post-HIPC
Sub-Saharan Africa: The Role of Domestic Debt," Paper for GDNet's 2004/05
project on Macroeconomic Policy Challenges of Low Income Countries. Available via
the Internet: www.gdnet.org/pdf2/gdn_library/global_research__projects/macro_low_
income/Abbas.pdf.</mixed-citation>
         </ref>
         <ref id="d1152e594a1310">
            <mixed-citation id="d1152e598" publication-type="other">
_, 2007, "Public Domestic Debt and Growth in LICs," Draft doctoral thesis paper
(Oxford, United Kingdom, University of Oxford). Available via the Internet:
users.ox.ac.uk/ ~ hert 1734/Publicpercent20domesticpercent20debtpercent20andpercent
20growth.pdf.</mixed-citation>
         </ref>
         <ref id="d1152e614a1310">
            <mixed-citation id="d1152e618" publication-type="other">
_, forthcoming, Banks' Holding of Government Securities: Causes and Consequences
(unpublished; University of Oxford).</mixed-citation>
         </ref>
         <ref id="d1152e628a1310">
            <mixed-citation id="d1152e632" publication-type="other">
Agenor, P., and P.J. Montiel, 1999, Development Macroeconomics (Princeton, New
Jersey, Princeton University Press).</mixed-citation>
         </ref>
         <ref id="d1152e643a1310">
            <mixed-citation id="d1152e647" publication-type="other">
Aizenmann, J., B. Pinto, and A. Radziwill, 2004, "Sources for Financing Domestic
Capital: Is Foreign Saving a Viable Option for Developing Countries?" NBER
Working Paper No. W10624 (Cambridge, Massachusetts, National Bureau of
Economic Research).</mixed-citation>
         </ref>
         <ref id="d1152e663a1310">
            <mixed-citation id="d1152e667" publication-type="other">
Arellano, M., and O. Bover, 1995, "Another Look at the Instrumental Variable Estimation
of Error Component Models," Journal of Econometrics, Vol. 68, No. 1, pp. 29-51.</mixed-citation>
         </ref>
         <ref id="d1152e677a1310">
            <mixed-citation id="d1152e681" publication-type="other">
Barajas, A., A. Steiner, and N. Salazar, 1999, "Interest Spreads in Banking in
Colombia," IMF Staff Papers, Vol. 46, No. 2, pp. 196-224.</mixed-citation>
         </ref>
         <ref id="d1152e691a1310">
            <mixed-citation id="d1152e695" publication-type="other">
-, 2000, "The Impact of Liberalization and Foreign Investment in Colombia's
Financial Sector," Journal of Development Economics, Vol. 63, No. 1, pp. 157-96.</mixed-citation>
         </ref>
         <ref id="d1152e705a1310">
            <mixed-citation id="d1152e709" publication-type="other">
Barro, R.J., 1974, "Are Government Bonds Net Wealth?" The Journal of Political
Economy, Vol. 82, No. 6, pp. 1095-17.</mixed-citation>
         </ref>
         <ref id="d1152e719a1310">
            <mixed-citation id="d1152e723" publication-type="other">
Beaugrand, P., B. Loko, and M. Mlachila, 2002, "The Choice Between External
and Domestic Debt in Financing Budget Deficits: The Case of Central and West
African Countries," IMF Working Paper 02/79 (Washington, International
Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e740a1310">
            <mixed-citation id="d1152e744" publication-type="other">
Beck, T., A. Demirgug-Kunt, and R.E. Levine, 2006, "A New Database on Financial
Development and Structure," World Bank Finance Research Working Paper
No. 2146 (Washington, World Bank).</mixed-citation>
         </ref>
         <ref id="d1152e757a1310">
            <mixed-citation id="d1152e761" publication-type="other">
Blundell, R., and S. Bond, 1998, "GMM Estimation with Persistent Panel Data: An
Application to Production Functions," IFS Working Paper W99/04 (London,
Institute of Fiscal Studies).</mixed-citation>
         </ref>
         <ref id="d1152e774a1310">
            <mixed-citation id="d1152e778" publication-type="other">
Bond, S., A. Hoeffler, and J. Temple, 2002, "GMM Estimation of Empirical Growth
Models," CEPR Discussion Paper 3048 (London, Centre for Economic Policy Research).</mixed-citation>
         </ref>
         <ref id="d1152e788a1310">
            <mixed-citation id="d1152e792" publication-type="other">
Brock, P.L., and L. Rojas-Suarez, 2000, "Understanding the Behavior of Bank Spreads
in Latin America," Journal of Development Economics, Vol. 63, No. 1, pp. 113-34.</mixed-citation>
         </ref>
         <ref id="d1152e802a1310">
            <mixed-citation id="d1152e806" publication-type="other">
Chirwa, E.W., and M. Mlachila, 2004, "Financial Reforms and Interest Rate Spreads
in the Commercial Banking System in Malawi," IMF Staff Papers, Vol. 51, No. 1,
pp. 96-122.</mixed-citation>
         </ref>
         <ref id="d1152e819a1310">
            <mixed-citation id="d1152e823" publication-type="other">
Christensen, J.E., 2004, "Domestic Debt Markets in Sub-Saharan Africa," IMF Working
Paper No. 04/46 (Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e834a1310">
            <mixed-citation id="d1152e838" publication-type="other">
Detragiache, E., T. Tressel, and P. Gupta, 2005, "Finance in Lower Income Countries:
An Empirical Exploration," IMF Working Paper No. 05/167 (Washington,
International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e851a1310">
            <mixed-citation id="d1152e855" publication-type="other">
Diamond, P., 1965, "National Debt in a Neoclassical Debt Model," Journal of Political
Economy, Vol. 55, No. 5, pp. 1126-50.</mixed-citation>
         </ref>
         <ref id="d1152e865a1310">
            <mixed-citation id="d1152e869" publication-type="other">
Eichengreen, B., and R. Hausmann, 2005, Original Sin: The Road to Redemption
(Chicago, University of Chicago Press).</mixed-citation>
         </ref>
         <ref id="d1152e879a1310">
            <mixed-citation id="d1152e883" publication-type="other">
Elbadawi, I., B. Ndulu, and N. Ndung'u, 1997, "Debt Overhang and Economic Growth
in Sub-Saharan Africa," paper prepared for IMF Institute, Washington,
International Monetary Fund.</mixed-citation>
         </ref>
         <ref id="d1152e896a1310">
            <mixed-citation id="d1152e900" publication-type="other">
Fabella, R., and S. Madhur, 2003, "Bond Market Development in East Asia: Issues and
Challenges," Economics Research Department Working Paper No. 35, (Manilla,
Asian Development Bank). Available via the Internet: www.adb.org/Documents/
ERD/Working_Papers/wp035.pdf.</mixed-citation>
         </ref>
         <ref id="d1152e916a1310">
            <mixed-citation id="d1152e920" publication-type="other">
Fry, M., 1997, Emancipating the Banking System and Developing Markets for Government
Debt (London, Routledge).</mixed-citation>
         </ref>
         <ref id="d1152e931a1310">
            <mixed-citation id="d1152e935" publication-type="other">
Guide, A., C. Pattillo, and J. Christensen, 2006, Sub-Saharan Africa: Financial Sector
Challenges (Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e945a1310">
            <mixed-citation id="d1152e949" publication-type="other">
Hauner, D., 2006, "Fiscal Policy and Financial Development," IMF Working Paper
No. 06/26 (Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e959a1310">
            <mixed-citation id="d1152e963" publication-type="other">
Huang, Y., and J. Temple, 2005, "Does External Trade Promote Financial
Development?" CEPR Discussion Paper No. 5150 (London, Centre for Economic
Policy Research).</mixed-citation>
         </ref>
         <ref id="d1152e976a1310">
            <mixed-citation id="d1152e980" publication-type="other">
Husain, A.M., 1997, "Domestic Taxes and the External Debt Laffer Curve," Economica,
Vol. 64, No. 255, pp. 519-25.</mixed-citation>
         </ref>
         <ref id="d1152e990a1310">
            <mixed-citation id="d1152e994" publication-type="other">
International Monetary Fund (IMF), 2001, Developing Government Bond Markets:
A Handbook (Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e1004a1310">
            <mixed-citation id="d1152e1008" publication-type="other">
_, 2005, Monetary and Fiscal Policy Design Issues in Low-Income Countries
(Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e1019a1310">
            <mixed-citation id="d1152e1023" publication-type="other">
Jeanne, O., and A. Guscina, 2006, "Government Debt in Emerging Market Countries:
A New Data Set," IMF Working Paper No. 06/98 (Washington, International
Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e1036a1310">
            <mixed-citation id="d1152e1040" publication-type="other">
Kahn, B., 2005, Original Sin and Bond Market Development in Sub-Saharan Africa (The
Hague, Forum on Debt and Development).</mixed-citation>
         </ref>
         <ref id="d1152e1050a1310">
            <mixed-citation id="d1152e1054" publication-type="other">
Kumhof, M., 2004, "Fiscal Crisis Resolution: Taxation versus Inflation," Meeting Paper
No. 874 (New York, Society for Economic Dynamics).</mixed-citation>
         </ref>
         <ref id="d1152e1064a1310">
            <mixed-citation id="d1152e1068" publication-type="other">
_, and E. Tanner, 2005, "Government Debt: An Essential Role in Financial
Intermediation," IMF Working Paper 05/57 (Washington, International Monetary
Fund).</mixed-citation>
         </ref>
         <ref id="d1152e1081a1310">
            <mixed-citation id="d1152e1085" publication-type="other">
Mankiw, N.G., D. Romer, and D. Weil, 1992, "A Contribution to the Empirics of
Economic Growth," Quarterly Journal of Economics, Vol. 107, No. 2, pp. 407-37.</mixed-citation>
         </ref>
         <ref id="d1152e1095a1310">
            <mixed-citation id="d1152e1099" publication-type="other">
Masson, P.R., T. Bayoumi, and H. Samiei, 1998, "International Evidence on the
Determinants of Private Saving," The World Bank Economic Review, Vol. 12, No. 3,
pp. 483-501.</mixed-citation>
         </ref>
         <ref id="d1152e1113a1310">
            <mixed-citation id="d1152e1117" publication-type="other">
Mellor, D., and A. Guscina, forthcoming, "Domestic Debt Distress in Lower Income
Countries," IMF Working Paper (Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e1127a1310">
            <mixed-citation id="d1152e1131" publication-type="other">
Moss, T., N. Pettersson, and N.V. de Walle, 2006, "An Aid-Institutions Paradox?" CGD
Working Paper 74 (Washington, Center for Global Development).</mixed-citation>
         </ref>
         <ref id="d1152e1141a1310">
            <mixed-citation id="d1152e1145" publication-type="other">
Ndikumana, L., 2001, "Financial Markets and Economic Development in Africa," UM
Political Economy Research Institute Working Paper No. 17 (University of
Massachusetts).</mixed-citation>
         </ref>
         <ref id="d1152e1158a1310">
            <mixed-citation id="d1152e1162" publication-type="other">
Pattillo, C, H. Poirson, and L. Ricci, 2002, "External Debt and Growth," IMF Working
Paper 02/69 (Washington, International Monetary Fund).</mixed-citation>
         </ref>
         <ref id="d1152e1172a1310">
            <mixed-citation id="d1152e1176" publication-type="other">
Sachs, J., 1989, "The Debt Overhang of Developing Countries," in Debt Stabilization
and Development, ed. by G. Calvo, R. Findlay, P. Kouri and J.B. De Macedo
(Oxford, Basil Blackwell).</mixed-citation>
         </ref>
         <ref id="d1152e1189a1310">
            <mixed-citation id="d1152e1193" publication-type="other">
Windmeijer, F., 2000, "A finite sample correction for the variance of linear 2-step GMM
estimatiors," IFS Working Paper W00/19, Institute of Fiscal Studies, London.</mixed-citation>
         </ref>
         <ref id="d1152e1204a1310">
            <mixed-citation id="d1152e1208" publication-type="other">
World Bank, 2006, Global Development Finance (Washington, World Bank).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
