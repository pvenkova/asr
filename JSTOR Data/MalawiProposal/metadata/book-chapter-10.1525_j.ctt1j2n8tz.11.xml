<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1j2n8tz</book-id>
      <subj-group>
         <subject content-type="call-number">HF5718.2.K4 M34 2017</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Business communication</subject>
         <subj-group>
            <subject content-type="lcsh">Kenya</subject>
            <subj-group>
               <subject content-type="lcsh">Mombasa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Artisans</subject>
         <subj-group>
            <subject content-type="lcsh">Kenya</subject>
            <subj-group>
               <subject content-type="lcsh">Mombasa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Handicraft</subject>
         <subj-group>
            <subject content-type="lcsh">Technological innovations</subject>
            <subj-group>
               <subject content-type="lcsh">Kenya</subject>
               <subj-group>
                  <subject content-type="lcsh">Mombasa</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Digital communications</subject>
         <subj-group>
            <subject content-type="lcsh">Kenya</subject>
            <subj-group>
               <subject content-type="lcsh">Mombasa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Anthropology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Art of Connection</book-title>
         <subtitle>Risk, Mobility, and the Crafting of Transparency in Coastal Kenya</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Mahoney</surname>
               <given-names>Dillon</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>17</day>
         <month>01</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="epub">9780520966239</isbn>
      <isbn content-type="epub">0520966236</isbn>
      <publisher>
         <publisher-name>University of California Press</publisher-name>
         <publisher-loc>Oakland, California</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Dillon Mahoney</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.1525/j.ctt1j2n8tz"/>
      <abstract abstract-type="short">
         <p>
            <italic>The Art of Connection</italic>narrates the individual stories of artisans and traders of Kenyan arts and crafts as they overcome the loss of physical access to roadside market space by turning to new digital technologies to make their businesses more mobile and integrated into the global economy. Bringing together the studies of globalization, development, art, and communication, the book illuminates the lived experiences of informal economies and shows how traders and small enterprises balance new risks with the mobility afforded by digital technologies. An array of ethnic and generational politics have led to market burnings and witchcraft accusations as Kenya's crafts industry struggles to adapt to its new connection to the global economy. To mediate the resulting crisis of trust, the Fair Trade sticker and other NGO aesthetics continue to successfully represent a transparent, ethical, and trusting relationship between buyer and producer. Dillon Mahoney shows that by balancing revelation and obfuscation-what is revealed and what is not-Kenyan art traders make their own roles as intermediaries and the exploitative realities of the global economy invisible.</p>
      </abstract>
      <counts>
         <page-count count="264"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.3</book-part-id>
                  <title-group>
                     <title>List of Illustrations</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.5</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>The Art of Connection:</title>
                     <subtitle>AN INTRODUCTION</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Since 2000, few visitors to urban East Africa could help but experience the popularity and usefulness of cell phones and Internet cafés. These new technologies were at the center of a cultural negotiation and performance of modernity, decorated with the advertisements and billboards of service providers selling access to new means of communication and social and economic connection. Corporate marketing of new digital technologies to Kenyans in the twenty-first century was dangling the chance of economic success and social mobility before those who had long been denied full and equal participation in world affairs and economic development. The possibilities were</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.6</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>Mombasa Marginalized:</title>
                     <subtitle>CLAIMS TO LAND AND LEGITIMACY IN A TOURIST CITY</subtitle>
                  </title-group>
                  <fpage>26</fpage>
                  <abstract>
                     <p>I was initially drawn to Mombasa and its Old Town for reasons similar to those that have enticed hundreds of thousands of American and European travelers to visit the Kenyan coast every year since the late 1960s. Our predictable imaginations, desires, and spending habits were the economic foundation of Mombasa’s tour guides, hawkers, and the occupants of the roughly twenty wood-and-sheet-metal kiosks that were built on the roadside outside of Old Town’s Fort Jesus. These structures and their occupants were the focus of my initial study in 2001, during which time I befriended many of their resident art vendors who</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.7</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>Crafts Traders versus the State</title>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>In December 2001 and January 2002, during the lead-up to the 2002 general election, politicians closely affiliated with ruling-party KANU led the demolitions of between ten thousand and fifteen thousand roadside kiosks in Mombasa. Some claimed it to be “city cleaning” and “beautification” so that Mombasa might be granted full “city status.” Others, such as Mvita MP Shariff Nassir, told the occupants of the roadside kiosks that he wanted them to “go home.” While the demolitions were officially framed as a matter of public policy, there were clear undertones of ethnic politics.</p>
                     <p>If the previous chapter captured the predigital need</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.8</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>Negotiating Informality in Mombasa</title>
                  </title-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>During my time spent conducting interviews and research outside of Fort Jesus, national politics were a regular topic of conversation. The hard-working Kenyans with whom I spent my time had hopeful expectations when President Mwai Kibaki’s National Rainbow Coalition (NARC) won the 2002 election on an anticorruption platform of constitutional and economic reform. Kenya’s digital age began with a new president, who for many represented a positive move toward transparency and development. But the hope for political reform was short-lived in Mombasa and many other parts of Kenya as Kibaki’s first term as president was mired in repeated scandals and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.9</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>New Mobilities, New Risks</title>
                  </title-group>
                  <fpage>126</fpage>
                  <abstract>
                     <p>Economic informality and political insecurity create risky and precarious economic environments that push traders competing in such environments to seek new opportunities and connections. While cell phones and other new digital technologies can help struggling businesspeople overcome their immobility and lack of access to urban economic space, new forms of connection come with a host of new risks. The previous chapters have discussed the reasons why traders who participated in my research were increasingly working independently and without clear coordination with government departments following the demolitions of Mombasa’s roadside kiosks. At this time, many successful traders who ran large workshops</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.10</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>Crafting Ethical Connection and Transparency in Coastal Kenya</title>
                  </title-group>
                  <fpage>154</fpage>
                  <abstract>
                     <p>During an interview in Mombasa in October 2005, Peter, a successful Kenyan exporter of Fair Trade wood carvings, referred to his cell phone as his “best friend” (author’s interview, October 26, 2005, Mombasa). He did not know how to use a computer or e-mail, but that mattered less as long as his best friend was on a lanyard around his neck. I liked to ask such traders what they would do if they ever lost their phone. “I would immediately buy another,” Peter told me. “I would close this entire shop to keep my mobile.”</p>
                     <p>Peter was actually on the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.11</book-part-id>
                  <title-group>
                     <label>SEVEN</label>
                     <title>From Ethnic Brands to Fair Trade Labels</title>
                  </title-group>
                  <fpage>174</fpage>
                  <abstract>
                     <p>During the Christmas season of 2010, I was delighted to find so many of the Kenyan arts and handicrafts I had studied since 2001 readily available for purchase in a small boutique a few blocks from my New Jersey apartment. The store sold a variety of small crafts and handmade toys, as well as carvings from places as distant as Kenya, Bali, and Mexico. While nearly all the assorted toys, crafts, and carvings from Kenya carried a “Fair Trade Product of Kenya” sticker, small cards additionally described the raw material, the ethnic group, or the NGO that specialized in making</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.12</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>203</fpage>
                  <abstract>
                     <p>The stories presented in this book capture the lived realities of struggling Kenyans’ fleeting connections to the global economy, with all the risks and insecurities associated with these new connections and the new mobilities and imaginations made possible by digital technologies. People strategically approach new connections in terms of their risks. The art of connection in modern Kenya involves the production and maintenance of transparent connections: economically between producer and consumer and politically between citizen and state. Despite being rooted in illusion, the art of connection illustrated by Fair Trade branding emphasizes intelligibility and simplicity over complexity to create an</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.13</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>209</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.14</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>225</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1j2n8tz.15</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
