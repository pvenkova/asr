<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">systematicbotany</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100680</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Systematic Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Society of Plant Taxonomists</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03636445</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15482324</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23028960</article-id>
         <title-group>
            <article-title>Taxonomic Revision of the Alectra sessiliflora Complex (Orobanchaceae)</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeffery J.</given-names>
                  <surname>Morawetz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Andrea D.</given-names>
                  <surname>Wolfe</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2011</year>
      
            <day>1</day>
            <month>3</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23028876</issue-id>
         <fpage>141</fpage>
         <lpage>152</lpage>
         <permissions>
            <copyright-statement>© Copyright 2011 The American Society of Plant Taxonomists</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:title="an external site"
                   xlink:href="http://dx.doi.org/10.1600/036364411X553234"/>
         <abstract>
            <p>Alectra sessiliflora (Orobanchaceae) is the most widespread species within the genus, occurring throughout sub-Saharan Africa, and into India, China and the Philippines. Three varieties are currently recognized (A. sessiliflora var. monticola, A. sessiliflora var. senegalensis, and A. sessiliflora var. sessiliflora) and are distinguished by geographic range, calyx pubescence and stamen filament pubescence. Due to the overlapping nature of the characters used to distinguish among these varieties, accurate assignment of a specimen to a single variety is nearly impossible. We undertook a phenetic study of morphological characters to assess the validity of these varieties. Principal coordinate analysis and the unweighted pair-group method using arithmetic averages were used to explore whether specimens would cluster into the currently recognized varieties. Our analyses revealed no clustering based exclusively on geographic distribution. A small cluster of seven specimens was seen in the principal coordinate analysis using Gower's coefficient of similarity as input values, but this cluster was not diagnosable by unique characters. Based on these results we recommend that Alectra sessiliflora be recognized without infraspecific taxa. Finally, additional names are included as synonyms under Alectra sessiliflora based on extensive study of field-collected and herbarium specimens.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d491e177a1310">
            <mixed-citation id="d491e181" publication-type="other">
Bentham, G. 1835. Scrophularineae Indicae. London: J. Ridgway.</mixed-citation>
         </ref>
         <ref id="d491e188a1310">
            <mixed-citation id="d491e192" publication-type="other">
Bergius, P. J. 1767. Descriptiones Plantarum ex Capite Bonae Spei. Stockholm:
L. Salvii.</mixed-citation>
         </ref>
         <ref id="d491e202a1310">
            <mixed-citation id="d491e206" publication-type="other">
Fischer, E. 1996. A revision of the genus Alectra Thunberg (Scrophulariaceae)
in Madagascar, with a description of Pseudomelasma, gen. nov. Bulletin
du Museum National d'Histoire Naturelle Section B Adansonia Botanique
Phytochimie 18: 45-65.</mixed-citation>
         </ref>
         <ref id="d491e222a1310">
            <mixed-citation id="d491e226" publication-type="other">
Fischer, E. 2006. Scrophulariaceae. Pp. 235-308 in Flora of Ethiopia and Eritrea
vol. 5, eds. I. Hedberg, E. Kelbessa, S. Edwards, S. Demissew, and
E. Persson. Addis Ababa: The National Herbarium Addis Ababa &amp;
Uppsala University.</mixed-citation>
         </ref>
         <ref id="d491e243a1310">
            <mixed-citation id="d491e247" publication-type="other">
Ghazanfar, S. A., F. N. Hepper, and D. Philcox. 2008. Scrophulariaceae. Pp.
1-211 in Flora of tropical East Africa. London: Published on behalf of
the East African governments by Royal Botanic Gardens, Kew.</mixed-citation>
         </ref>
         <ref id="d491e260a1310">
            <mixed-citation id="d491e264" publication-type="other">
Gower, J. C. 1966. Some distance properties of latent root and vector meth-
ods used in multivariate analysis. Biometrika 53: 325-338.</mixed-citation>
         </ref>
         <ref id="d491e274a1310">
            <mixed-citation id="d491e278" publication-type="other">
Gower, J. C. 1971. A general coefficient of similarity and some of its prop-
erties. Biometrics 27: 857-871.</mixed-citation>
         </ref>
         <ref id="d491e288a1310">
            <mixed-citation id="d491e292" publication-type="other">
Handel-Mazzetti, H. 1936. Scrophulariaceae. Symbolae Sinicae 7: 829-872.</mixed-citation>
         </ref>
         <ref id="d491e299a1310">
            <mixed-citation id="d491e303" publication-type="other">
Hemsley, W. d. and b. A. bkan. 19U6. bcrophulariaceae. rp. 261-462 in tlora
of tropical Africa, ed. W. T. Dyer. London: L. Reeve &amp; Co.</mixed-citation>
         </ref>
         <ref id="d491e313a1310">
            <mixed-citation id="d491e317" publication-type="other">
Hepper, F. N. 1960. New and noteworthy Scrophulariaceae in Africa. Kew
Bulletin 14: 402-416.</mixed-citation>
         </ref>
         <ref id="d491e328a1310">
            <mixed-citation id="d491e332" publication-type="other">
Hepper, F. N. 1963. Scrophulariaceae. Pp. 352-374 in Flora of west tropical
Africa vol. 2, ed. F. N. Hepper. London: Crown Agents for Oversea
Governments and Administrations.</mixed-citation>
         </ref>
         <ref id="d491e345a1310">
            <mixed-citation id="d491e349" publication-type="other">
Hilliard, O. M. and B. L. Burtt. 1986. Notes on some plants of southern
Africa chiefly from Natal: XIII. Notes from the Royal Botanic Garden
Edinburgh 43: 345-405.</mixed-citation>
         </ref>
         <ref id="d491e362a1310">
            <mixed-citation id="d491e366" publication-type="other">
Hong, D., H. Yang, C. Jin, M. A. Fischer, N. H. Holmgren, and R. R. Mill.
1998. Scrophulariaceae. Pp. 1-212 in Flora of China vol. 18, eds. Z. Y.
Wu, and P. H. Raven. Beijing: Science Press, and St. Louis: Missouri
Botanical Garden Press.</mixed-citation>
         </ref>
         <ref id="d491e382a1310">
            <mixed-citation id="d491e386" publication-type="other">
Lauener, L. A. 1980. Catalogue of the names published by Hector Leveille:
XIII. Notes from the Royal Botanic Garden Edinburgh 38: 453-485.</mixed-citation>
         </ref>
         <ref id="d491e396a1310">
            <mixed-citation id="d491e400" publication-type="other">
Li, H., H. Guo, and Z. Dao. 2000. Flora of Gaoligong Mountains. Kunming:
Science Press.</mixed-citation>
         </ref>
         <ref id="d491e410a1310">
            <mixed-citation id="d491e414" publication-type="other">
Melchior, H. 1940. Beitrag zur Kenntnis der Gattung Melasma. Notizblatt
des Botanischen Gartens und Museums zu Berlin-Dahlem 15:119-127.</mixed-citation>
         </ref>
         <ref id="d491e425a1310">
            <mixed-citation id="d491e429" publication-type="other">
Melchior, H. 1941. Die Gattung Alectra Thunb. Notizblatt des Botanischen
Gartens und Museums zu Berlin-Dahlem 15: 423-447.</mixed-citation>
         </ref>
         <ref id="d491e439a1310">
            <mixed-citation id="d491e443" publication-type="other">
Merrill, E. D. 1917. Notes on the flora of Kwangtung Province, China. The
Philippine Journal of Science, C. Botany 12: 99-111.</mixed-citation>
         </ref>
         <ref id="d491e453a1310">
            <mixed-citation id="d491e457" publication-type="other">
Morawetz, J. J. 2007. Systematics of Alectra (Orobanchaceae) and phyloge-
netic relationships among the tropical clade of Orobanchaceae. Ph. D.
Dissertation. Columbus, Ohio: The Ohio State University.</mixed-citation>
         </ref>
         <ref id="d491e470a1310">
            <mixed-citation id="d491e474" publication-type="other">
Morawetz, J. J. and A. D. Wolfe. 2009. Assessing the monophyly of Alectra
and its relationship to Melasma (Orobanchaceae). Systematic Botany
34: 561-569.</mixed-citation>
         </ref>
         <ref id="d491e487a1310">
            <mixed-citation id="d491e491" publication-type="other">
Pennell, F. W. 1943. The Scrophulariaceae of the western Himalayas,
Philadelphia: Academy of Natural Sciences.</mixed-citation>
         </ref>
         <ref id="d491e501a1310">
            <mixed-citation id="d491e505" publication-type="other">
Philcox, D. 1990. Scrophulariaceae. Pp. 1-179 in Flora Zambesmca:
Mozambique, Malawi, Zambia, Zimbabwe, Botswana vol. 8(2), eds. E.
Launert, and V. G. Pope. London: Managing Committee on behalf of
the Contributors to Flora Zambesiaca.</mixed-citation>
         </ref>
         <ref id="d491e522a1310">
            <mixed-citation id="d491e526" publication-type="other">
Rohlf, F. J. 1972. An empirical comparison of three ordination techniques
in numerical taxonomy. Systematic Zoology 21: 271-280.</mixed-citation>
         </ref>
         <ref id="d491e536a1310">
            <mixed-citation id="d491e540" publication-type="other">
Rohlf, F. J. 1998. NTSYSpc version 2.0. Setauket: Exeter Software.</mixed-citation>
         </ref>
         <ref id="d491e547a1310">
            <mixed-citation id="d491e551" publication-type="other">
Sneath, P. H. A. and R. R. Sokal. 1973. Numerical taxonomy. San Francisco:
W. H. Freeman and Company.</mixed-citation>
         </ref>
         <ref id="d491e561a1310">
            <mixed-citation id="d491e565" publication-type="other">
Stuessy, T. F. 2008. Plant taxonomy. Ed. 2. New York: Columbia University
Press.</mixed-citation>
         </ref>
         <ref id="d491e575a1310">
            <mixed-citation id="d491e579" publication-type="other">
Thunberg, C. P. 1784. Nova Genera Plantarum. Uppsala: J. Edman.</mixed-citation>
         </ref>
         <ref id="d491e586a1310">
            <mixed-citation id="d491e590" publication-type="other">
Wood, J. R. I. 1997. A handbook of the Yemen flora. London: Royal Botanic
Gardens, Kew.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
