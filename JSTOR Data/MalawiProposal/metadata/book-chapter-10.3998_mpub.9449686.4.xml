<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">mpub.9449686</book-id>
      <subj-group>
         <subject content-type="call-number">D887 .T43 2018</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">BRIC countries</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">BRIC countries</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign economic relations</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">World politics</subject>
         <subj-group>
            <subject content-type="lcsh">21st century</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Rising Powers and Foreign Policy Revisionism</book-title>
         <subtitle>Understanding BRICS Identity and Behavior Through Time</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>THIES</surname>
               <given-names>CAMERON G.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>NIEMAN</surname>
               <given-names>MARK DAVID</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>29</day>
         <month>11</month>
         <year>2017</year>
      </pub-date>
      <isbn content-type="ppub">9780472130566</isbn>
      <isbn content-type="epub">9780472123285</isbn>
      <publisher>
         <publisher-name>University of Michigan Press</publisher-name>
         <publisher-loc>Ann Arbor</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Cameron G. Thies</copyright-holder>
         <copyright-holder>Mark David Nieman</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.3998/mpub.9449686"/>
      <abstract abstract-type="short">
         <p>In<italic>Rising Powers and Foreign Policy Revisionism</italic>, Cameron Thies and Mark Nieman examine the identity and behavior of the BRICS (Brazil, Russia, India, China, and South Africa) over time in light of academic and policymaker concerns that rising powers may become more aggressive and conflict-prone. The authors develop a theoretical framework that encapsulates pressures for revisionism through the mechanism of competition and pressures for accommodation and assimilation through the mechanism of socialization. The identity and behavior of the BRICS should be a product of the push and pull of these two forces as mediated by their domestic foreign policy processes.State identity is investigated qualitatively through the use of role theory and the identification of national role conceptions. Both economic and militarized conflict behavior are examined using Bayesian change-point modeling, which identifies structural breaks in time series data, revealing potential wholesale revision of foreign policy. Using this innovative approach to show that the behavior of rising powers is governed not simply by the structural dynamics of power but also by the roles that these rising powers define for themselves, they assert that this process will likely lead to a much more evolutionary approach to foreign policy and will not necessarily generate international conflict.</p>
      </abstract>
      <counts>
         <page-count count="224"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.4</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>Introduction:</title>
                     <subtitle>The “Problem” of Emerging Powers</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Nearly two decades has passed since the language of emerging powers or rising powers entered into the business, academic, policymaking, and media discourses. The promise of emerging powers as safe sites for investment that offered large returns with little risk may not have played out exactly as proponents suggested, but forecasting is often difficult in economics and politics. The potential perils associated with emerging powers also quickly entered into the academic and policymaking discourses. In particular, with the increasing decentering and dispersion of power in the international system, were we headed toward some kind of systemic war? Most of our</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Explaining Change in the International System:</title>
                     <subtitle>A Role-Theoretic Approach to Emerging Powers</subtitle>
                  </title-group>
                  <fpage>19</fpage>
                  <abstract>
                     <p>Anyone interested in the phenomenon of rising powers must come to grips with essentially two interrelated issues. First, how should we conceptualize rising powers as a category of states that is somehow distinct from neighboring categories of states? There is a sense that these states are not great powers yet, but they may be someday. They might be regional powers with associated responsibilities for the maintenance of order within specific geographical or functional regions. Yet all regional powers are not necessarily rising powers on the way to great power status. Thus, we must define the thing we wish to study</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>A Mixed-Methods Approach to Observing Identity and Behavior</title>
                  </title-group>
                  <fpage>39</fpage>
                  <abstract>
                     <p>The use of mixed methods to strengthen the kinds of causal inferences we make in political science has made great strides in the last decade or so. Advocates of more systematic approaches to qualitative evidence initially began to push the idea of mixed methods as a way for quantitative researchers to take them more seriously. The result produced many dissertations consisting of a large-n statistical analysis designed to test hypotheses that was followed by several case studies to allow for the more detailed tracing of causal chains. Further developments in this approach then began to take the actual case selection</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Brazil:</title>
                     <subtitle>A Regional Leader in Need of Followers</subtitle>
                  </title-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>The rise of Brazil has caught the attention of decision makers, pundits, academics, and the global media. Brazil’s rapid economic growth and development have led many observers to note a more assertive foreign policy orientation in recent years as well as greater interest in the provision and management of regional security. Brazil has campaigned for a seat on the U.N. Security Council and defied traditional alignment with the United States to engage Iran. Are these types of behaviors just the beginning of an assertive and more militarized foreign policy, or will Brazil simply be satis-fied with recognition of its status</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Russia:</title>
                     <subtitle>A Once and Future Great Power?</subtitle>
                  </title-group>
                  <fpage>81</fpage>
                  <abstract>
                     <p>The reemergence of Russia as an actor with an active foreign policy, global military capabilities, and autocratic tendencies has caught the attention of decision makers, pundits, academics, and the global media. Russia’s natural-resource-fueled economic growth plus its aspirations for control of the near abroad have led many observers to note a more assertive foreign policy orientation in recent years. Russia’s 2008 intervention in Georgia, invasion of Crimea and support of separatists in Ukraine, and intervention in Syria are viewed as evidence of a renewed Russian threat. Some observers have even suggested the dawn of a new Cold War as the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>India:</title>
                     <subtitle>From Nonalignment to Nuclear Power</subtitle>
                  </title-group>
                  <fpage>99</fpage>
                  <abstract>
                     <p>India is the world’s largest democracy and the world’s second-mostpopulous state. Its economic growth and potential for future development have led those within and outside of India to consider it an emerging or rising power. But will gains on the economic front translate into political revisionism? Structural materialist theories of international relations tend to see a deterministic relationship between rising powers and revisionist foreign policy and conflict behavior designed to bring about system change, while our approaches to foreign policy analysis recognize the domestic and international constraints on such revisionism. We continue our analyses of these contending approaches to rising</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>China:</title>
                     <subtitle>Responsible Stakeholder or Revisionist Great Power?</subtitle>
                  </title-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>The rise of China weighs heavily on the minds of decision makers and pundits. Many worry that China’s rapid economic growth and development may lead to a more assertive foreign policy orientation and militarized conflict behavior. China’s handling of the 2001 U.S. spy plane incident and the ongoing militarization of the Spratly Islands seem to fit such an aggressive vision for a rising power. Academics tend to have mixed views on whether China’s growing economic clout will result in attempts to transform the international order through conflict (e.g., Ross and Feng 2008). We examine the case of China through the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>South Africa:</title>
                     <subtitle>Punching above Its Weight as a Middle Power</subtitle>
                  </title-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>South Africa is an interesting case of a state that has been accorded rising power status by other conventionally acclaimed rising powers. The BRIC designation that caught on with the media, pundits, and policymakers initially did not include South Africa, yet the other BRIC countries invited South Africa to join them in 2010. South Africa, like the other BRICS, is a G20 member and is recognized as one of the most capable states in Africa. Many view it as a regional hegemon or leader of southern Africa, though these stylized facts have run into the internal difficulties experienced as the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>Conclusion:</title>
                     <subtitle>Toward a Better Understanding of Emerging Powers</subtitle>
                  </title-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>Our project in this book was to explore the phenomenon of emerging or rising powers. Much has been made of this phenomenon in recent years. It is hard to escape the analysis of the activities of this group of states by the media, pundits, and policymakers, even as some of these states are more active than others in conducting foreign policy. The analysis of rising powers has also begun to generate a great deal of scholarship. Our goal was to develop an approach to analyze what these states say about themselves in terms of their identity—an answer to the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.13</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>175</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.14</book-part-id>
                  <title-group>
                     <title>REFERENCES</title>
                  </title-group>
                  <fpage>183</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">mpub.9449686.15</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>205</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
