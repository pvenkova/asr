<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4485460</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Lung Fluid Immunoglobulin from HIV-Infected Subjects Has Impaired Opsonic Function against Pneumococci</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>Roger Eagan</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Homer L.</given-names>
                  <surname>Twigg,</surname>
                  <suffix>III</suffix>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Neil French</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Janelisa Musaya</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Richard B.</given-names>
                  <surname>Day</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Eduard E. Zijlstra</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Helen Tolmie</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>David Wyler</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Malcolm E. Molyneux</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Stephen B.</given-names>
                  <surname>Gordon</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>15</day>
            <month>6</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">44</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</issue>
         <issue-id>i402701</issue-id>
         <fpage>1632</fpage>
         <lpage>1638</lpage>
         <page-range>1632-1638</page-range>
         <permissions>
            <copyright-statement>Copyright 2007 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4485460"/>
         <abstract>
            <p>Background. The incidence of pneumococcal pneumonia is greatly increased among human immunodeficiency virus (HIV)-infected subjects, compared with among non-HIV-infected subjects. Lung fluid levels of immunoglobulin G (IgG) specific for pneumococcal capsular polysaccharide are not reduced in HIV-infected subjects; therefore, we examined immunoglobulin subtypes and compared lung fluid IgG opsonic function in HIV-infected subjects with that in healthy subjects. Methods. Bronchoalveolar lavage (BAL) fluid and serum samples were collected from 23 HIV-infected and 26 uninfected subjects. None of the subjects were receiving highly active antiretroviral therapy, and none had received pneumococcal vaccination. Pneumococcal capsule-specific IgG levels in serum and BAL fluid were measured by enzyme-linked immunosorbent assay, and IgG was concentrated from 40 mL of BAL fluid. Opsonization and opsonophagocytosis of pneumococci with serum, BAL fluid, and BAL IgG were compared between HIV-infected subjects and healthy subjects. Results. The effect of type 1 pneumococcal capsular polysaccharide-specific IgG in opsonizing of pneumococci was significantly less using both serum and BAL IgG from HIV-infected subjects, compared with serum and BAL IgG from healthy subjects (mean level, 8.9 fluorescence units [95% confidence interval, 8.1-9.7 fluorescence units] vs. 12.1 fluorescence units [95% confidence interval, 9.7-15.2 fluorescence units]; P = .002 for lung BAL IgG). The opsonophagocytosis of pneumococci observed using BAL IgG from HIV-infected subjects was significantly less than that observed using BAL IgG from healthy subjects (37 fluorescence units per ng of IgG [95% confidence interval, 25-53 fluorescence units per ng of IgG] vs. 127 fluorescence units per ng of IgG [95% confidence interval, 109-145 fluorescence units per ng of IgG]; P &lt; .001). Conclusion. HIV infection is associated with decreased antipneumococcal opsonic function in BAL fluid and serum.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1062e208a1310">
            <label>1</label>
            <mixed-citation id="d1062e215" publication-type="journal">
Brown PD, Lerer SA. Community-acquired pneumonia. Lancet1998; 352:
1295-302.<person-group>
                  <string-name>
                     <surname>Brown</surname>
                  </string-name>
               </person-group>
               <fpage>1295</fpage>
               <volume>352</volume>
               <source>Lancet</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e247a1310">
            <label>2</label>
            <mixed-citation id="d1062e254" publication-type="book">
Butler JC. Epidemiology of pneumococcal disease. In: Tuomanen EI, Mitch-
ell TJ, Morrison DA, Spratt BG, eds. The pneumococcus. Washington, DC:
ASM Press, 2004:148-68.<person-group>
                  <string-name>
                     <surname>Butler</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Epidemiology of pneumococcal disease</comment>
               <fpage>148</fpage>
               <source>The pneumococcus</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e289a1310">
            <label>3</label>
            <mixed-citation id="d1062e296" publication-type="journal">
Scott JA, Hall AJ, Muyodi C, et al. Aetiology, outcome, and risk factors for
mortality among adults with acute pneumonia in Kenya. Lancet2000; 355:
1225-30.<person-group>
                  <string-name>
                     <surname>Scott</surname>
                  </string-name>
               </person-group>
               <fpage>1225</fpage>
               <volume>355</volume>
               <source>Lancet</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e331a1310">
            <label>4</label>
            <mixed-citation id="d1062e338" publication-type="journal">
Gordon SB, Walsh AL, Chaponda M, et al. Bacterial meningitis in Malawian
adults: pneumococcal disease is common, severe, and seasonal. Clin Infect
Dis2000; 31:53-7.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>53</fpage>
               <volume>31</volume>
               <source>Clin Infect Dis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e374a1310">
            <label>5</label>
            <mixed-citation id="d1062e381" publication-type="journal">
Gilks CF, Ojoo SA, Ojoo JC, et al. Invasive pneumococcal disease in a cohort
of predominantly HIV-1 infected female sex-workers in Nairobi, Kenya.
Lancet1996; 347:718-23.<person-group>
                  <string-name>
                     <surname>Gilks</surname>
                  </string-name>
               </person-group>
               <fpage>718</fpage>
               <volume>347</volume>
               <source>Lancet</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e416a1310">
            <label>6</label>
            <mixed-citation id="d1062e423" publication-type="journal">
Gordon SB, Chaponda M, Walsh AL, et al. Pneumococcal disease in HIV-
infected Malawian adults: acute mortality and long-term survival. AIDS
2002; 16:1409-17.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>1409</fpage>
               <volume>16</volume>
               <source>AIDS</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e458a1310">
            <label>7</label>
            <mixed-citation id="d1062e465" publication-type="journal">
Gordon SB, Irving GR, Lawson RA, Lee ME, Read RC. Intracellular traf-
ficking and killing of Streptococcus pneumoniae by human alveolar mac-
rophages are influenced by opsonins. Infect Immun2000; 68:2286-93.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>2286</fpage>
               <volume>68</volume>
               <source>Infect Immun</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e500a1310">
            <label>8</label>
            <mixed-citation id="d1062e507" publication-type="journal">
Brown JS, Hussell T, Gilliland SM, et al. The classical pathway is the dom-
inant complement pathway required for innate immunity to Streptococcus
pneumoniae infection in mice. Proc Natl Acad Sci U S A2002; 99:16969-74.<object-id pub-id-type="jstor">10.2307/3074047</object-id>
               <fpage>16969</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1062e526a1310">
            <label>9</label>
            <mixed-citation id="d1062e533" publication-type="journal">
Rowland-Jones SL. Timeline: AIDS pathogenesis: what have two decades
of HIV research taught us? Nat Rev Immunol2003; 3:343-8.<person-group>
                  <string-name>
                     <surname>Rowland-Jones</surname>
                  </string-name>
               </person-group>
               <fpage>343</fpage>
               <volume>3</volume>
               <source>Nat Rev Immunol</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e565a1310">
            <label>10</label>
            <mixed-citation id="d1062e572" publication-type="journal">
Carson PJ, Schut RL, Simpson ML, O'Brien J, Janoff EN. Antibody class
and subclass responses to pneumococcal polysaccharides following im-
munization of human immunodeficiency virus-infected patients. J Infect
Dis1995; 172:340-5.<person-group>
                  <string-name>
                     <surname>Carson</surname>
                  </string-name>
               </person-group>
               <fpage>340</fpage>
               <volume>172</volume>
               <source>J Infect Dis</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e611a1310">
            <label>11</label>
            <mixed-citation id="d1062e618" publication-type="journal">
Janoff EN, Fasching C, Ojoo JC, O'Brien J, Gilks CF Responsiveness of
human immunodeficiency virus type 1-infected Kenyan women with or
without prior pneumococcal disease to pneumococcal vaccine. J Infect Dis
1997; 175:975-8.<person-group>
                  <string-name>
                     <surname>Janoff</surname>
                  </string-name>
               </person-group>
               <fpage>975</fpage>
               <volume>175</volume>
               <source>J Infect Dis</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e656a1310">
            <label>12</label>
            <mixed-citation id="d1062e663" publication-type="journal">
Janoff EN, O'Brien J, Thompson P, et al. Streptococcus pneumoniae colo-
nization, bacteremia, and immune response among persons with human
immunodeficiency virus infection. J Infect Dis1993; 167:49-56.<person-group>
                  <string-name>
                     <surname>Janoff</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>167</volume>
               <source>J Infect Dis</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e698a1310">
            <label>13</label>
            <mixed-citation id="d1062e705" publication-type="journal">
Takahashi H, Oishi K, Yoshimine H, et al. Decreased serum opsonic activity
against Streptococcus pneumoniae in human immunodeficiency vi-
rus-infected Ugandan adults. Clin Infect Dis2003;37:1534-40.<person-group>
                  <string-name>
                     <surname>Takahashi</surname>
                  </string-name>
               </person-group>
               <fpage>1534</fpage>
               <volume>37</volume>
               <source>Clin Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e740a1310">
            <label>14</label>
            <mixed-citation id="d1062e747" publication-type="journal">
Semenzato G, Agostini C. HIV-related interstitial lung disease. Curr Opin
Pulm Med1995; 1:383-91.<person-group>
                  <string-name>
                     <surname>Semenzato</surname>
                  </string-name>
               </person-group>
               <fpage>383</fpage>
               <volume>1</volume>
               <source>Curr Opin Pulm Med</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e779a1310">
            <label>15</label>
            <mixed-citation id="d1062e786" publication-type="journal">
Twigg HL III, Spain BA, Soliman DM, et al. Production of interferon-y by
lung lymphocytes in HIV-infected individuals. Am J Physiol1999;276:
L256-62.<person-group>
                  <string-name>
                     <surname>Twigg</surname>
                  </string-name>
               </person-group>
               <fpage>L256</fpage>
               <volume>276</volume>
               <source>Am J Physiol</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e821a1310">
            <label>16</label>
            <mixed-citation id="d1062e828" publication-type="journal">
Gordon SB, Molyneux ME, Boeree MJ, et al. Opsonic phagocytosis of
Streptococcus pneumoniaeby alveolar macrophages is not impaired in human
immunodeficiency virus-infected Malawian adults. J Infect Dis2001; 184:
1345-9.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>1345</fpage>
               <volume>184</volume>
               <source>J Infect Dis</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e867a1310">
            <label>17</label>
            <mixed-citation id="d1062e876" publication-type="journal">
Bice DE, Muggenburg BA. Pulmonary immune memory: localized pro-
duction of antibody in the lung after antigen challenge. Immunology
1996; 88:191-7.<person-group>
                  <string-name>
                     <surname>Bice</surname>
                  </string-name>
               </person-group>
               <fpage>191</fpage>
               <volume>88</volume>
               <source>Immunology</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e911a1310">
            <label>18</label>
            <mixed-citation id="d1062e918" publication-type="journal">
Scamurra RW, Nelson DB, Lin XM, et al. Mucosal plasma cell repertoire
during HIV-1 infection. J Immunol2002; 169:4008-16.<person-group>
                  <string-name>
                     <surname>Scamurra</surname>
                  </string-name>
               </person-group>
               <fpage>4008</fpage>
               <volume>169</volume>
               <source>J Immunol</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e950a1310">
            <label>19</label>
            <mixed-citation id="d1062e957" publication-type="journal">
Gordon SB, Janoff EN, Sloper D, et al. HIV-1 infection is associated with
altered innate pulmonary immunity. J Infect Dis2005; 192:1412-6.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>1412</fpage>
               <volume>192</volume>
               <source>J Infect Dis</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e989a1310">
            <label>20</label>
            <mixed-citation id="d1062e996" publication-type="journal">
Gordon SB, Miller DE, Day RB, et al. Pulmonary immunoglobulin re-
sponses to Streptococcus pneumoniae are altered but not reduced in human
immunodeficiency virus-infected Malawian Adults. J Infect Dis2003; 188:
666-70.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>666</fpage>
               <volume>188</volume>
               <source>J Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1034a1310">
            <label>21</label>
            <mixed-citation id="d1062e1041" publication-type="journal">
Croft M, Swain SL. B cell response to T helper cell subsets. II. Both the
stage ofT cell differentiation and the cytokines secreted determine the extent
and nature of helper activity. J Immunol1991; 147:3679-89.<person-group>
                  <string-name>
                     <surname>Croft</surname>
                  </string-name>
               </person-group>
               <fpage>3679</fpage>
               <volume>147</volume>
               <source>J Immunol</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1076a1310">
            <label>22</label>
            <mixed-citation id="d1062e1083" publication-type="journal">
Mascart-Lemone F, Gerard M, Libin M, et al. Differential effect of human
immunodeficiency virus infection on the IgA and IgG antibody responses
to pneumococcal vaccine. J Infect Dis1995; 172:1253-60.<person-group>
                  <string-name>
                     <surname>Mascart-Lemone</surname>
                  </string-name>
               </person-group>
               <fpage>1253</fpage>
               <volume>172</volume>
               <source>J Infect Dis</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1119a1310">
            <label>23</label>
            <mixed-citation id="d1062e1126" publication-type="journal">
French N, Moore M, Haikala R, Kayhty H, Gilks CF. A case-control study
to investigate serological correlates of clinical failure of 23-valent pneu-
mococcal polysaccharide vaccine in HIV- 1-infected Ugandan adults. J Infect
Dis2004; 190:707-12.<person-group>
                  <string-name>
                     <surname>French</surname>
                  </string-name>
               </person-group>
               <fpage>707</fpage>
               <volume>190</volume>
               <source>J Infect Dis</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1164a1310">
            <label>24</label>
            <mixed-citation id="d1062e1171" publication-type="journal">
Concepcion NF, Frasch CE. Pneumococcal type 22F absorption improves
the specificity of pneumococcal-polysaccharide enzyme-linked immuno-
sorbent assay. Clin Diagn Lab Immunol2001; 8:266-72.<person-group>
                  <string-name>
                     <surname>Concepcion</surname>
                  </string-name>
               </person-group>
               <fpage>266</fpage>
               <volume>8</volume>
               <source>Clin Diagn Lab Immunol</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1206a1310">
            <label>25</label>
            <mixed-citation id="d1062e1213" publication-type="journal">
Wernette CM, Frasch CE, Madore D, et al. Enzyme-linked immunosorbent
assay for quantitation of human antibodies to pneumococcal polysaccha-
rides. Clin Diagn Lab Immunol2003; 10:514-9.<person-group>
                  <string-name>
                     <surname>Wernette</surname>
                  </string-name>
               </person-group>
               <fpage>514</fpage>
               <volume>10</volume>
               <source>Clin Diagn Lab Immunol</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1248a1310">
            <label>26</label>
            <mixed-citation id="d1062e1255" publication-type="journal">
Gordon SB, Kanyanda S, Walsh AL, et al. Poor potential coverage for 7-
valent pneumococcal conjugate vaccine, Malawi. Emerg Infect Dis2003; 9:
747-9.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>747</fpage>
               <volume>9</volume>
               <source>Emerg Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1290a1310">
            <label>27</label>
            <mixed-citation id="d1062e1297" publication-type="journal">
Day RB, Wang Y, Knox KS, Pasula R, Martin WJ, Twigg HL III. Alveolar
macrophages from HIV-infected subjects are resistant to Mycobacterium
tuberculosis in vitro. Am J Respir Cell Mol Biol2004; 30:403-10.<person-group>
                  <string-name>
                     <surname>Day</surname>
                  </string-name>
               </person-group>
               <fpage>403</fpage>
               <volume>30</volume>
               <source>Am J Respir Cell Mol Biol</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1332a1310">
            <label>28</label>
            <mixed-citation id="d1062e1339" publication-type="journal">
Miiro G, Kayhty H, Watera C, et al. Conjugate pneumococcal vaccine in
HIV-infected Ugandans and the effect of past receipt of polysaccharide
vaccine. J Infect Dis2005; 192:1801-5.<person-group>
                  <string-name>
                     <surname>Miiro</surname>
                  </string-name>
               </person-group>
               <fpage>1801</fpage>
               <volume>192</volume>
               <source>J Infect Dis</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1375a1310">
            <label>29</label>
            <mixed-citation id="d1062e1382" publication-type="journal">
Gordon SB, Read RC. Macrophage defences against respiratory tract in-
fections. Br Med Bull2002; 61:45-61.<person-group>
                  <string-name>
                     <surname>Gordon</surname>
                  </string-name>
               </person-group>
               <fpage>45</fpage>
               <volume>61</volume>
               <source>Br Med Bull</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1414a1310">
            <label>30</label>
            <mixed-citation id="d1062e1421" publication-type="journal">
Doerschuk CM. Mechanisms of leukocyte sequestration in inflamed lungs.
Microcirculation2001; 8:71-8.<person-group>
                  <string-name>
                     <surname>Doerschuk</surname>
                  </string-name>
               </person-group>
               <fpage>71</fpage>
               <volume>8</volume>
               <source>Microcirculation</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1453a1310">
            <label>31</label>
            <mixed-citation id="d1062e1460" publication-type="journal">
Moja P, Jalil A, Quesnel A, et al. Humoral immune response within the
lung in HIV-1 infection. Clin Exp Immunol1997; 110:341-8.<person-group>
                  <string-name>
                     <surname>Moja</surname>
                  </string-name>
               </person-group>
               <fpage>341</fpage>
               <volume>110</volume>
               <source>Clin Exp Immunol</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1492a1310">
            <label>32</label>
            <mixed-citation id="d1062e1499" publication-type="journal">
Badr G, Borhis G, Treton D, Moog C, Garraud O, Richard Y. HIV type 1
glycoprotein 120 inhibits human B cell chemotaxis to CXC chemokine
ligand (CXCL) 12, CC chemokine ligand (CCL)20, and CCL21. J Immunol
2005; 175:302-10.<person-group>
                  <string-name>
                     <surname>Badr</surname>
                  </string-name>
               </person-group>
               <fpage>302</fpage>
               <volume>175</volume>
               <source>J Immunol</source>
               <year>2005</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1537a1310">
            <label>33</label>
            <mixed-citation id="d1062e1544" publication-type="journal">
Chang Q, Abadi J, Alpert P, Pirofski L. A pneumococcal capsular polysac-
charide vaccine induces a repertoire shift with increased VH3 expression
in peripheral B cells from human immunodeficiency virus (HIV)-
uninfected but not HIV-infected persons. J Infect Dis2000; 181:1313-21.<person-group>
                  <string-name>
                     <surname>Chang</surname>
                  </string-name>
               </person-group>
               <fpage>1313</fpage>
               <volume>181</volume>
               <source>J Infect Dis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1582a1310">
            <label>34</label>
            <mixed-citation id="d1062e1589" publication-type="journal">
Briles DE, Hollingshead SK, Nabors GS, Paton JC, Brooks-Walter A. The
potential for using protein vaccines to protect against otitis media caused
by Streptococcus pneumoniae. Vaccine2000; 19(Suppl 1):S87-95.<person-group>
                  <string-name>
                     <surname>Briles</surname>
                  </string-name>
               </person-group>
               <issue>Suppl 1</issue>
               <fpage>S87</fpage>
               <volume>19</volume>
               <source>Vaccine</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1628a1310">
            <label>35</label>
            <mixed-citation id="d1062e1635" publication-type="journal">
Malley R, Trzcinski K, Srivastava AK, Thompson CM, Anderson PW, Lip-
sitch M. CD4+ T cells mediate antibody-independent acquired immunity
to pneumococcal colonization. Proc Natl Acad Sci U S A2005; 102(13):
4848-53.<object-id pub-id-type="jstor">10.2307/3375138</object-id>
               <fpage>4848</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1062e1658a1310">
            <label>36</label>
            <mixed-citation id="d1062e1665" publication-type="journal">
Ren B, McCrory MA, Pass C, et al. The virulence function of Streptococcus
pneumoniae surface protein A involves inhibition of complement activation
and impairment of complement receptor-mediated protection. J Immunol
2004; 173:7506-12.<person-group>
                  <string-name>
                     <surname>Ren</surname>
                  </string-name>
               </person-group>
               <fpage>7506</fpage>
               <volume>173</volume>
               <source>J Immunol</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1062e1703a1310">
            <label>37</label>
            <mixed-citation id="d1062e1710" publication-type="journal">
Balachandran P, Brooks-Walter A, Virolainen-Julkunen A, Hollingshead SK,
Briles DE. Role of pneumococcal surface protein C in nasopharyngeal
carriage and pneumonia and its ability to elicit protection against carriage
of Streptococcus pneumoniae. Infect Immun2002; 70:2526-34.<person-group>
                  <string-name>
                     <surname>Balachandran</surname>
                  </string-name>
               </person-group>
               <fpage>2526</fpage>
               <volume>70</volume>
               <source>Infect Immun</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
