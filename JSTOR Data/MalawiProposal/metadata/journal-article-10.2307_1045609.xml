<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">annaameracadpoli</journal-id>
         <journal-id journal-id-type="jstor">j100052</journal-id>
         <journal-title-group>
            <journal-title>The Annals of the American Academy of Political and Social Science</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn pub-type="ppub">00027162</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">1045609</article-id>
         <article-categories>
            <subj-group>
               <subject>African States and International Forums</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Between Scylla and Charybdis: The Foreign Economic Relations of Sub-Saharan African States</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Thomas M.</given-names>
                  <surname>Callaghy</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1987</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">489</volume>
         <issue-id>i243105</issue-id>
         <fpage>148</fpage>
         <lpage>163</lpage>
         <page-range>148-163</page-range>
         <permissions>
            <copyright-statement>Copyright 1987 The American Academy of Political and Social Science</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1045609"/>
         <abstract>
            <p>Over the last decade the foreign economic relations of sub-Saharan African states have focused increasingly on their severe debt and economic crises. These relations have involved wrestling with debt service burdens and the rigors of rescheduling with the Paris and London Clubs; conducting difficult negotiations with bilateral and private creditors; bargaining over conditionality packages with the International Monetary Fund and the World Bank or fending them off; distributing the painful costs of adjustment; coping with import strangulation; and devising new development policies and strategies. The sub-Saharan states were already highly dependent on the outside world; the intensity, stakes, and levels of conditionality of these states' foreign economic relations have increased substantially since the middle of the 1970s. They are certainly political, as they impinge on very central issues-sovereignty, political order, development, and mass welfare. In this sense, they are foreign economic relations with very powerful domestic roots and consequences. African states and external actors are going to have to work together to ameliorate Africa's crises.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d169e129a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d169e136" publication-type="journal">
Julius K. Nyerere, "Africa and the Debt
Crisis," African Affairs, 84(337):494 (Oct. 1985)<person-group>
                     <string-name>
                        <surname>Nyerere</surname>
                     </string-name>
                  </person-group>
                  <issue>337</issue>
                  <fpage>494</fpage>
                  <volume>84</volume>
                  <source>African Affairs</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e170" publication-type="journal">
Chinweizu, "Debt Trap Peonage," Monthly Re-
view, 3(6):22, 22, 34 (Nov. 1985)<person-group>
                     <string-name>
                        <surname>Chinweizu</surname>
                     </string-name>
                  </person-group>
                  <issue>6</issue>
                  <fpage>22</fpage>
                  <volume>3</volume>
                  <source>Monthly Review</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e204" publication-type="journal">
Adebayo
Adedeji, "Foreign Debt and Prospects for Growth
in Africa during the 1980s," Journal of Modern
African Studies, 23(1):60-61 (Mar. 1985)<object-id pub-id-type="jstor">10.2307/160463</object-id>
                  <fpage>60</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e227a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d169e236" publication-type="book">
World Bank, Financing
Adjustment with Growth in Sub-Saharan Africa,
1986-90 (Washington, DC: World Bank, 1986)<person-group>
                     <string-name>
                        <surname>World Bank</surname>
                     </string-name>
                  </person-group>
                  <source>Financing Adjustment with Growth in Sub-Saharan Africa, 1986-90</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e264" publication-type="book">
idem, Development and Debt Service (Wash-
ington, DC: World Bank, 1986)<person-group>
                     <string-name>
                        <surname>World Bank</surname>
                     </string-name>
                  </person-group>
                  <source>Development and Debt Service</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e289" publication-type="book">
idem, World
Development Report 1985 (New York: Oxford
University Press, 1985)<person-group>
                     <string-name>
                        <surname>World Bank</surname>
                     </string-name>
                  </person-group>
                  <source>World Development Report 1985</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e317" publication-type="book">
idem, Toward Sustained
Development in Sub-Saharan Africa (Wash-
ington, DC: World Bank, 1984)<person-group>
                     <string-name>
                        <surname>World Bank</surname>
                     </string-name>
                  </person-group>
                  <source>Toward Sustained Development in Sub-Saharan Africa</source>
                  <year>1984</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e345" publication-type="book">
Chandra Hardy,
"Africa's Debt: Structural Adjustment with Sta-
bility," in Strategies for African Development,
ed. Robert J. Berg and Jennifer Seymour Whitaker
(Berkeley: University of California Press, 1986),
pp. 453-75<person-group>
                     <string-name>
                        <surname>Hardy</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Africa's Debt: Structural Adjustment with Stability</comment>
                  <fpage>453</fpage>
                  <source>Strategies for African Development</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e388" publication-type="book">
Rupert Pennant-Rea, The African
Burden (New York: Twentieth Century Fund/
Priority Press, 1986)<person-group>
                     <string-name>
                        <surname>Pennant-Rea</surname>
                     </string-name>
                  </person-group>
                  <source>The African Burden</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e416" publication-type="journal">
IMF Survey, 15(7):106 (31
Mar. 1986)<issue>7</issue>
                  <fpage>106</fpage>
                  <volume>15</volume>
                  <source>IMF Survey</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e441" publication-type="book">
Carol
Lancaster and John Williamson, eds., African
Debt and Financing (Washington, DC: Institute
for International Economics, 1986)<person-group>
                     <string-name>
                        <surname>Lancaster</surname>
                     </string-name>
                  </person-group>
                  <source>African Debt and Financing</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e473" publication-type="book">
Eduard Brau,
"African Debt: Facts and Figures on the Current
Situation," pp. 11-15, 30-43<person-group>
                     <string-name>
                        <surname>Brau</surname>
                     </string-name>
                  </person-group>
                  <fpage>11</fpage>
                  <source>African Debt: Facts and Figures on the Current Situation</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e501" publication-type="book">
John Williamson,
"Prospects for the Flow of IMF Finance,"
pp. 134-41<person-group>
                     <string-name>
                        <surname>Williamson</surname>
                     </string-name>
                  </person-group>
                  <fpage>134</fpage>
                  <source>Prospects for the Flow of IMF Finance</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e529" publication-type="book">
Edward V. K. Jaycox et al., "The
Nature of the Debt Problem in Eastern and
Southern Africa," pp. 47-62<person-group>
                     <string-name>
                        <surname>Jaycox</surname>
                     </string-name>
                  </person-group>
                  <fpage>47</fpage>
                  <source>The Nature of the Debt Problem in Eastern and Southern Africa</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e558a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d169e565" publication-type="book">
World Bank, Development and Debt Ser-
vice, p. xxvi  <fpage>xxvi</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e579" publication-type="book">
Reginald Herbold Green, "Reflec-
tions on the State of Knowledge and Ways For-
ward," in Crisis and Recovery in Sub-Saharan
Africa, ed. Tore Rose (Paris: Organization for
Economic Cooperation and Development, 1985),
p. 299<person-group>
                     <string-name>
                        <surname>Green</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Reflections on the State of Knowledge and Ways Forward</comment>
                  <fpage>299</fpage>
                  <source>Crisis and Recovery in Sub-Saharan Africa</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e623a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d169e630" publication-type="journal">
G. K. Helleiner, "The IMF and Africa in
the 1980s," Canadian Journal of African Studies.
17(1):61 (1983)<person-group>
                     <string-name>
                        <surname>Helleiner</surname>
                     </string-name>
                  </person-group>
                  <issue>1</issue>
                  <fpage>61</fpage>
                  <volume>17</volume>
                  <source>Canadian Journal of African Studies</source>
                  <year>1983</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e667" publication-type="book">
Green, "Reflections," p. 308  <fpage>308</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e678" publication-type="book">
Thomas M. Callaghy, "The
Political Economy of African Debt: The Case of
Zaire," in Africa in Economic Crisis, ed. John
Ravenhill (New York: Columbia University Press,
1986), pp. 307-46<person-group>
                     <string-name>
                        <surname>Callaghy</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The Political Economy of African Debt: The Case of Zaire</comment>
                  <fpage>307</fpage>
                  <source>Africa in Economic Crisis</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e720a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d169e727" publication-type="journal">
Miles Kahler, "Politics and Interna-
tional Debt: Explaining the Crisis," International
Organization, 39(3):357-82 (Summer 1985).<object-id pub-id-type="jstor">10.2307/2706684</object-id>
                  <fpage>357</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e746a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d169e753" publication-type="journal">
Nyerere, "Africa and the Debt Crisis,"
p. 492.  <fpage>492</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e768a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d169e775" publication-type="journal">
Helleiner, "IMF and Africa," p. 61  <fpage>61</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e786" publication-type="book">
Green,
"Reflections," p. 308  <fpage>308</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e801a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d169e808" publication-type="conference">
Carol Lancaster, "Multilateral Develop-
ment Banks and Africa" (Paper delivered at the
Conference on African Debt and Financing, Insti-
tute for International Economics and Georgetown
University, Washington, DC, 20-22 Feb. 1986),
p. 21.<person-group>
                     <string-name>
                        <surname>Lancaster</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Multilateral Development Banks and Africa</comment>
                  <fpage>21</fpage>
                  <source>Conference on African Debt and Financing, Institute for International Economics and Georgetown University, Washington, DC, 20-22 Feb. 1986</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e852a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d169e859" publication-type="book">
Gerald K. Helleiner, "The Question of
Conditionality," in African Debt and Financing,
ed. Lancaster and Williamson, p. 70.<person-group>
                     <string-name>
                        <surname>Helleiner</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The Question of Conditionality</comment>
                  <fpage>70</fpage>
                  <source>African Debt and Financing</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e891a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d169e898" publication-type="book">
World Bank, Financing Adjustment, p. 1.  <fpage>1</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e911a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d169e918" publication-type="book">
Justin B. Zulu and Saleh M. Nsouli,
"Adjustment Programs in Africa: The Recent
Experience," IMF Occasional Paper no. 34 (Inter-
national Monetary Fund, 1985), pp. 26-27.<person-group>
                     <string-name>
                        <surname>Zulu</surname>
                     </string-name>
                  </person-group>
                  <fpage>26</fpage>
                  <source>Adjustment Programs in Africa: The Recent Experience</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e953a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d169e960" publication-type="journal">
Stephan Haggard, "The Politics of
Adjustment: Lessons from the IMF's Extended
Fund Facility," International Organization, 39(3):
505-34 (Summer 1985).<object-id pub-id-type="jstor">10.2307/2706688</object-id>
                  <fpage>505</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e983a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d169e990" publication-type="journal">
Rattan J. Bhatia, "Adjustment Efforts in
Sub-Saharan Africa, 1980-84," Finance and Devel-
opment, 22(3):19-22 (Sept. 1985).<person-group>
                     <string-name>
                        <surname>Bhatia</surname>
                     </string-name>
                  </person-group>
                  <issue>3</issue>
                  <fpage>19</fpage>
                  <volume>22</volume>
                  <source>Finance and Development</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1028a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d169e1035" publication-type="book">
Green, "Reflections," p. 293.  <fpage>293</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1047a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d169e1054" publication-type="journal">
Louis M. Goreux, "Economic Adjustment
Efforts of Zaire Require Support of External
Creditors," IMF Survey, 15(5):72-75 (Mar. 1986)<person-group>
                     <string-name>
                        <surname>Goreux</surname>
                     </string-name>
                  </person-group>
                  <issue>5</issue>
                  <fpage>72</fpage>
                  <volume>15</volume>
                  <source>IMF Survey</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1091" publication-type="book">
Jaycox et al., "Nature of the Debt Problem,"p. 36<person-group>
                     <string-name>
                        <surname>Jaycox</surname>
                     </string-name>
                  </person-group>
                  <fpage>36</fpage>
                  <source>Nature of the Debt Problem</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1112" publication-type="journal">
M. Crawford Young, "Optimism on Zaire: Illusion
or Reality?" CSIS Africa Notes, 50:8 (22 Nov.
1985)<person-group>
                     <string-name>
                        <surname>Young</surname>
                     </string-name>
                  </person-group>
                  <issue>22 Nov.</issue>
                  <fpage>8</fpage>
                  <volume>50</volume>
                  <source>CSIS Africa Notes</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1150a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d169e1157" publication-type="journal">
Financial Times, 11 Apr. 1986.<issue>11 Apr.</issue>
                  <source>Financial Times</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1174a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d169e1181" publication-type="journal">
Henry S. Bienen and Mark Gersovitz,
"Economic Stabilization, Conditionality, and
Political Instability," International Organization,
39(4):730, 753 (Autumn 1985).<person-group>
                     <string-name>
                        <surname>Bienen</surname>
                     </string-name>
                  </person-group>
                  <issue>4</issue>
                  <fpage>730</fpage>
                  <volume>39</volume>
                  <source>International Organization</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1222a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d169e1229" publication-type="journal">
Henry F. Jackson, "The African Crisis:
Drought and Debt," Foreign Affairs, 63(5):1087
(Summer 1985).<person-group>
                     <string-name>
                        <surname>Jackson</surname>
                     </string-name>
                  </person-group>
                  <issue>5</issue>
                  <fpage>1087</fpage>
                  <volume>63</volume>
                  <source>Foreign Affairs</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1267a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d169e1274" publication-type="book">
Haggard, "Politics of Adjustment,"p. 511<person-group>
                     <string-name>
                        <surname>Haggard</surname>
                     </string-name>
                  </person-group>
                  <fpage>511</fpage>
                  <source>Politics of Adjustment</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1295" publication-type="journal">
Edward V.K. Jaycox, "Africa: Development Chal-
lenges and the World Bank's Response," Finance
and Development, 23(1):22 (Mar. 1986)<person-group>
                     <string-name>
                        <surname>Jaycox</surname>
                     </string-name>
                  </person-group>
                  <issue>1</issue>
                  <fpage>22</fpage>
                  <volume>23</volume>
                  <source>Finance and Development</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1332" publication-type="book">
Thomas M. Callahgy, "The State as
Lame Leviathan: The Patrimonial Administrative
State in Africa," in The African State in Transi-
tion, ed. Zaki Ergas (New York: St. Martin's,
forthcoming)<person-group>
                     <string-name>
                        <surname>Callahgy</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The State as Lame Leviathan: The Patrimonial Administrative State in Africa</comment>
                  <source>The African State in Transition</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1367" publication-type="book">
idem, "The State and the Develop-
ment of Capitalism in Africa: Theoretical, Histor-
ical, and Comparative Reflections," in The Precar-
ious Balance: State-Society Relations in Africa,
ed. Donald Rothchild and Naomi Chazan (Boulder,
CO: Westview Press, forthcoming)<person-group>
                     <string-name>
                        <surname>Callahgy</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The State and the Development of Capitalism in Africa: Theoretical, Historical, and Comparative Reflections</comment>
                  <source>The Precarious Balance: State-Society Relations in Africa</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1405a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d169e1412" publication-type="book">
World Bank, Financing Adjustment,
pp. 1, 21-22.  <fpage>1</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1427a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d169e1434" publication-type="book">
Ibid., p. 22.  <fpage>22</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1446a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d169e1453" publication-type="book">
Reginald Herbold Green and Caroline
Allison, "The World Bank's Agenda for Develop-
ment: Dialectics, Doubts, and Dialogues," in
Africa in Economic Crisis, ed. Ravenhill, p. 72<person-group>
                     <string-name>
                        <surname>Green</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The World Bank's Agenda for Development: Dialectics, Doubts, and Dialogues</comment>
                  <fpage>72</fpage>
                  <source>Africa in Economic Crisis</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1487" publication-type="book">
Elliot Berg, "The World Bank's Strategy," in ibid.,
p. 54<person-group>
                     <string-name>
                        <surname>Berg</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The World Bank's Strategy</comment>
                  <fpage>54</fpage>
                  <source>Africa in Economic Crisis</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1515" publication-type="book">
World Bank, Financing Adjustment, p. 38  <fpage>38</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d169e1527" publication-type="journal">
Peter F.
Drucker, "The Changed World Economy," For-
eign Affairs, 64(4):768-91 (Spring 1986)<person-group>
                     <string-name>
                        <surname>Drucker</surname>
                     </string-name>
                  </person-group>
                  <issue>4</issue>
                  <fpage>768</fpage>
                  <volume>64</volume>
                  <source>Foreign Affairs</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1566a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d169e1573" publication-type="book">
World Bank, Financing Adjustment,
"Forward."  </mixed-citation>
            </p>
         </fn>
         <fn id="d169e1585a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d169e1592" publication-type="book">
Philip Ndegwa, governor of the Central
Bank of Kenya, quoted in African Debt and
Financing, ed. Lancaster and Williamson, p. 9.<person-group>
                     <string-name>
                        <surname>Ndegwa</surname>
                     </string-name>
                  </person-group>
                  <fpage>9</fpage>
                  <source>African Debt and Financing</source>
               </mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
