<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt1t895nz</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Development in Africa</book-title>
         <subtitle>Refocusing the lens after the Millennium Development Goals</subtitle>
      </book-title-group>
      <contrib-group>
         <role content-type="editor">Edited by</role>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Kararach</surname>
               <given-names>George</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Besada</surname>
               <given-names>Hany</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Shaw</surname>
               <given-names>Timothy M.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>28</day>
         <month>10</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9781447328568</isbn>
      <isbn content-type="epub">1447328566</isbn>
      <publisher>
         <publisher-name>Policy Press</publisher-name>
         <publisher-loc>Bristol</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Policy Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1t895nz"/>
      <abstract abstract-type="short">
         <p>Since 2000, countries across Africa have maintained over a decade of unprecedented economic expansion in a phenomena known as ‘Africa rising’. However, despite pockets of strong economic growth, Africa still faces major development challenges. In this important book the contributors argue that Africa as a continent must work on securing social and political stability and build effective economic governance to ensure the development of a society that is socially, economically and politically inclusive. Looking beyond the Millennium Development Goals (MDGs) the contributors highlight what they consider to be the 12 major public policy conversations of the continent post-2015, from the legacy of African leadership, to the ‘youth bulge’ (and resulting unemployment) and climate change. The volume presents policy makers, academics and students with a chance to take a fresh look at urgent emerging challenges in post-MDG African development.Since 2000, countries across Africa have maintained over a decade of unprecedented economic expansion in a phenomena known as ‘Africa rising’. However, despite pockets of strong economic growth, Africa still faces major development challenges. In this important book the contributors argue that Africa as a continent must work on securing social and political stability and build effective economic governance to ensure the development of a society that is socially, economically and politically inclusive. Looking beyond the Millennium Development Goals (MDGs) the contributors highlight what they consider to be the 12 major public policy conversations of the continent post-2015, from the legacy of African leadership, to the ‘youth bulge’ (and resulting unemployment) and climate change. The volume presents policy makers, academics and students with a chance to take a fresh look at urgent emerging challenges in post-MDG African development.</p>
      </abstract>
      <counts>
         <page-count count="432"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.3</book-part-id>
                  <title-group>
                     <title>List of tables and figures</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.4</book-part-id>
                  <title-group>
                     <title>Notes on contributors</title>
                  </title-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.5</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>xix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.6</book-part-id>
                  <title-group>
                     <title>INTRODUCTION:</title>
                     <subtitle>Development policy, agency and Africa in the post-2015 development agenda</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kararach</surname>
                           <given-names>George</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Besada</surname>
                           <given-names>Hany</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shaw</surname>
                           <given-names>Timothy M.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Winters</surname>
                           <given-names>Kristen</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Since 2000, Africa’s economic expansion has proceeded with vigorous momentum, maintaining an annual average economic growth rate of 5 per cent or more (IMF, 2013). This robust economic growth is expected to extend beyond 2015, as the continent benefits from opportunities created by a natural resource boom, strong internal demand from its rapidly growing middle class,¹ increased spending on basic infrastructure by both governments and the private sector, adoption and penetration of ICT (for example, mobile telephone penetration has surpassed 90 per cent in urban areas; see The World Bank, 2010), foreign direct and portfolio investments that are projected to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.7</book-part-id>
                  <title-group>
                     <label>ONE</label>
                     <title>The post-2015 development agenda:</title>
                     <subtitle>Building a global convergence on policy options</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Wakiaga</surname>
                           <given-names>James</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>25</fpage>
                  <abstract>
                     <p>The international community has been engaged in a post-Millennium Development Goals (MDGs) policy conversation to forge a successor framework that is universally acceptable. The post-2015 development agenda must meet the minimum threshold of the current MDG framework, which successfully helped galvanise development efforts and guide global and national development priorities (ECOSOC, 2011). Multiple stakeholders, ranging from multilateral organisations, civil society and academic institutions, have been debating the form and substance of the post-2015 development agenda (Vandemoortele, 2012). In addition, different forums have been held and publications churned out prescribing the process and substance of the emerging framework. The task of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.8</book-part-id>
                  <title-group>
                     <label>TWO</label>
                     <title>Debating post-2015 developmentoriented reforms in Africa:</title>
                     <subtitle>agendas for action</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kararach</surname>
                           <given-names>George</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>47</fpage>
                  <abstract>
                     <p>The development history of Africa has been characterised by many twists and turns. Independence in the late 1950s and most of the 1960s was brought about by resistance to colonial sociopolitical and economic relations. For almost 30 years after that, ambitions for a New International Economic Order (NIEO) (a proposal from developing countries to revise the international economic system) was driven by a desire for an autonomous postcolonial development, including in Africa. The debt crisis, by undermining the financial space and tightening the dependence of the developing to the developed world, shattered the unity necessary for the achievement of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.9</book-part-id>
                  <title-group>
                     <label>THREE</label>
                     <title>Public diplomacy for developmental states:</title>
                     <subtitle>implementing the African Mining Vision</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shaw</surname>
                           <given-names>Timothy M.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Polonenko</surname>
                           <given-names>Leah McMillan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>83</fpage>
                  <abstract>
                     <p>The second decade of the 21st century may be that of Africa’s renaissance. As Africa’s economic agencies, the United Nations Economic Commission for Africa (UNECA) along with the African Development Bank (AfDB) and the African Union (AU) have come to advocate the adoption of policies leading towards developmental states, so the continent has articulated an African Mining Vision (AMV; see www.africaminingvision.org) (AMV, 2009, 2011a, 2011b) by contrast to other possible strategies for its natural resource governance (Florini and Dubash, 2011) from assorted global developmental, environmental, financial and industrial agencies.</p>
                     <p>This chapter begins to identify the background to this quite remarkable</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.10</book-part-id>
                  <title-group>
                     <label>FOUR</label>
                     <title>The role of gender in development:</title>
                     <subtitle>where do boys count?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Léautier</surname>
                           <given-names>Frannie A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>111</fpage>
                  <abstract>
                     <p>Most analyses of gender in development focus on the role of women or the differentiated impact of the development on women. Recent attention of external aid programmes and the rising violence against women in a number of countries, coupled with evidence of disparity in development results when the role of men and women is differentiated, has further clarified the issue by focusing on women and girls. Furthermore, gender has come up as a central issue in the post-2015 identification of priorities following an assessment of the Millennium Development Goals (MDGs) and efforts to define a new framework for channelling policy</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.11</book-part-id>
                  <title-group>
                     <label>FIVE</label>
                     <title>Service-oriented government:</title>
                     <subtitle>the developmental state and service delivery in Africa after 2015 – are capacity indicators important?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kararach</surname>
                           <given-names>George</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>135</fpage>
                  <abstract>
                     <p>A service-oriented government has a social contract to deliver services to its population, thereby winning trust and legitimacy (Besley and Ghatak, 2007). Since the early 1980s, African countries have embarked on various forms of ‘modern’ public sector reforms, with mixed results, and various reasons have been given for these. One of the criticisms is that reforms were undertaken without sufficient data or understanding of the realities on the ground, and thus resultant economic growth is questionable (Jerven, 2011). By the 1990s, the debate had moved to whether or not the African civil service was too big (‘bloated’), cost too much</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.12</book-part-id>
                  <title-group>
                     <label>SIX</label>
                     <title>Employment creation for youth in Africa:</title>
                     <subtitle>the role of extractive industries</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kamgnia</surname>
                           <given-names>Bernadette Dia</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Murinde</surname>
                           <given-names>Victor</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>169</fpage>
                  <abstract>
                     <p>Singular among the economic challenges facing African countries today is the issue of youth unemployment. Almost 200 million of the population in Africa, equivalent to approximately 17 per cent of the population in 2015, is in the age range of between 15 and 24 years old (AfDB, 2013a). Essentially, in the majority of African countries, young people represent a significant proportion of the total national population. Unfortunately, they constitute the bulk of the unemployed in Africa, irrespective of their school qualifications. And young women are the most likely to be out of the labour market in many African countries, due</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.13</book-part-id>
                  <title-group>
                     <label>SEVEN</label>
                     <title>Financing the post-2015 development agenda:</title>
                     <subtitle>domestic revenue mobilisation in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bhushan</surname>
                           <given-names>Aniket</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Samy</surname>
                           <given-names>Yiagadeesen</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Medu</surname>
                           <given-names>Kemi</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>193</fpage>
                  <abstract>
                     <p>The debate about what should replace the Millennium Development Goals (MDGs), come 2015, provides an opportunity to reflect once again on the Financing for Development (FFD) agenda. It also comes on the back of dramatic reductions in global poverty in the last decade, and when many of the so-called fragile and conflict-affected states – many of which are located in Sub-Saharan Africa – are the least likely to meet any of the MDGs. Poverty remains stubbornly high in Sub-Saharan Africa – despite a fall in poverty rates, the number of people living on less than US$ 1.25 per day has increased from 205</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.14</book-part-id>
                  <title-group>
                     <label>EIGHT</label>
                     <title>Economic performance and social progress in Sub-Saharan Africa:</title>
                     <subtitle>the effect of least developed countries and fragile states</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Agarwal</surname>
                           <given-names>Manmohan</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Pirzada</surname>
                           <given-names>Natasha</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>225</fpage>
                  <abstract>
                     <p>Economic growth in Sub-Saharan Africa has lagged behind that in other regions over the past five decades (1965-2011). This poor performance contrasts with earlier high hopes, as Sub-Saharan Africa was believed to have the potential for rapid growth (Enke, 1963; Kamarck, 1967). The World Bank has undertaken a number of studies (for example, 1981, 1989; Lele, 1991; Husain and Faruquee, 1994) to analyse the gap between this perceived potential and actual performance. Sub-Saharan Africa has a large number of least developed countries (LDCs) as defined by the United Nations (UN), and also most of the failed fragile states, as defined</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.15</book-part-id>
                  <title-group>
                     <label>NINE</label>
                     <title>From regional integration to regionalism in Africa:</title>
                     <subtitle>building capacities for the post-Millennium Development Goals agenda</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>D’Alessandro</surname>
                           <given-names>Cristina</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>251</fpage>
                  <abstract>
                     <p>Regional integration, regional cooperation, regional coordination, regional harmonisation, regionalism, alternative/new regionalism, pan-Africanism, African unity…. All are processes, strategies and ideologies aimed at encompassing the current limits, dysfunctions and weaknesses of African states. The vast number and diversity of these approaches concede that these attempts have encountered numerous difficulties. The above list also underlines the fact that two major levels have emerged that remain crucial today: the continental scale and the macro-regional scale, different not only in their size, but also in their aims and instruments. Moreover, they demonstrate the problems associated with bypassing the national scale in Africa. Despite their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.16</book-part-id>
                  <title-group>
                     <label>TEN</label>
                     <title>Reforming the Development Banks’ Country Policy and Institutional Assessment as an aid allocation tool:</title>
                     <subtitle>the case for country self-assessment</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kararach</surname>
                           <given-names>George</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kedir</surname>
                           <given-names>Abbi Mamo</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Léautier</surname>
                           <given-names>Frannie</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>Murinde</surname>
                           <given-names>Victor</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>283</fpage>
                  <abstract>
                     <p>In Sub-Saharan Africa, The World Bank and the African Development Bank (AfDB) have been the two major players in the arena of multilateral aid. Both have been redefining aid conditionality to Sub-Saharan Africa since the early 1980s, and into the late 1990s. This has implied a move away from an emphasis on structural adjustment, where financing was provided in return for the promise of policy reforms, to disbursement of funds conditional on reforms already achieved. The new practice is known as aid ‘<italic>selectivity</italic>’ or<italic>performancebased allocation</italic>(PBA) .</p>
                     <p>Over the last few decades, the aid literature has been engaged with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.17</book-part-id>
                  <title-group>
                     <label>ELEVEN</label>
                     <title>Development and sustainability in a warming world:</title>
                     <subtitle>measuring the impacts of climate change in Africa</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Besada</surname>
                           <given-names>Hany</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Denton</surname>
                           <given-names>Fatima</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>O’Bright</surname>
                           <given-names>Benjamin</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>315</fpage>
                  <abstract>
                     <p>In their current manifestation, the Millennium Development Goals (MDGs) are presented as a blueprint to galvanise governments and private actors towards a substantial reduction in extreme poverty levels by 2015 (Sachs et al, 2009, p 1502). While the links between poverty and climate change are unsurprisingly complex, academic research and discourse finds itself approaching these two themes not as interconnected subjects, but rather as variably distinct entities. Cohen et al (1998) argue that even though climate is one symptom of unsustainable development, the two concepts continue to coexist under separate epistemologies, to the extent that climate change has not readily</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.18</book-part-id>
                  <title-group>
                     <label>TWELVE</label>
                     <title>African development through peace and security to sustainability</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Werner</surname>
                           <given-names>Karolina</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>345</fpage>
                  <abstract>
                     <p>We have reached the deadline for achieving the Millennium Development Goals (MDGs), and debates over the post-2015 development agenda are coming to a close. Following consultations with scholars, practitioners, civil society and other experts, as well as the general public, publications on the way forward have multiplied over the last five years. Critical assessments of the MDGs and achievements have been discussed and weaknesses identified. The meetings early in 2015 finalised the proposal for the new Sustainable Development Goals (SDGs) to present them to the United Nations (UN) General Assembly Summit in September 2015. The negotiations, in which member states</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.19</book-part-id>
                  <title-group>
                     <label>THIRTEEN</label>
                     <title>African development, political economy and the road to Agenda 2063</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kararach</surname>
                           <given-names>George</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Besada</surname>
                           <given-names>Hany</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>Shaw</surname>
                           <given-names>Timothy</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>365</fpage>
                  <abstract>
                     <p>Since the turn of the millennium, seven out of the ten fastest growing economies in the world have been African, tangible proof of the continent’s upward trajectory. Although 10 African countries do still have an African Union (AU) or United Nations (UN) peacekeeping presence, 85 per cent of Africans enjoy peaceful, relatively stable conditions, and these economies generate 95 per cent of the continent’s GDP.</p>
                     <p>Following a 20 per cent decline between 1980 and 2000, average incomes in Africa have risen by 30 per cent since 2005. Strong commodity prices have been central to this turnaround, but other factors such</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.20</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>393</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.21</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>409</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1t895nz.22</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>412</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
