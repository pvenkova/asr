<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1npr32</book-id>
      <subj-group>
         <subject content-type="call-number">QL737.P98B73 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Elephants</subject>
         <subj-group>
            <subject content-type="lcsh">Behavior</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Elephants</subject>
         <subj-group>
            <subject content-type="lcsh">Psychology</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Elephants</subject>
         <subj-group>
            <subject content-type="lcsh">Effects of human beings on</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Social behavior in animals</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Captive wild animals</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Psychology, Comparative</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>General Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Elephants on the Edge</book-title>
         <subtitle>What Animals Teach Us about Humanity</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Bradshaw</surname>
               <given-names>G. A.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>06</day>
         <month>10</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="ppub">9780300127317</isbn>
      <isbn content-type="ppub">0300127316</isbn>
      <isbn content-type="epub">9780300154917</isbn>
      <isbn content-type="epub">0300154917</isbn>
      <publisher>
         <publisher-name>Yale University Press</publisher-name>
         <publisher-loc>NEW HAVEN; LONDON</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>G. A. Bradshaw</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1npr32"/>
      <abstract abstract-type="short">
         <p>Drawing on accounts from India to Africa and California to Tennessee, and on research in neuroscience, psychology, and animal behavior, G. A. Bradshaw explores the minds, emotions, and lives of elephants. Wars, starvation, mass culls, poaching, and habitat loss have reduced elephant numbers from more than ten million to a few hundred thousand, leaving orphans bereft of the elders who would normally mentor them. As a consequence, traumatized elephants have become aggressive against people, other animals, and even one another; their behavior is comparable to that of humans who have experienced genocide, other types of violence, and social collapse. By exploring the elephant mind and experience in the wild and in captivity, Bradshaw bears witness to the breakdown of ancient elephant cultures.</p>
         <p>All is not lost. People are working to save elephants by rescuing orphaned infants and rehabilitating adult zoo and circus elephants, using the same principles psychologists apply in treating humans who have survived trauma. Bradshaw urges us to support these and other models of elephant recovery and to solve pressing social and environmental crises affecting all animals, human or not.</p>
      </abstract>
      <counts>
         <page-count count="352"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Martin</surname>
                           <given-names>Calvin Luther</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>Elephant breakdown, the subject herein, disturbs me. It says my own was inevitable. Recall Nietzsche’s crackup, triggered by the sight of a tradesman flogging a horse, and you begin to understand what I’m talking about.</p>
                     <p>We are all susceptible. Descartes in dressing gown before his hearth, demolishing, as if brick by brick, his rational mind—one of the more famous crackups of history. The cloak of composure we wear carries its own unraveling—the bit of thread lying exposed. Sometimes, as with Nietzsche, it happens in a thunderclap of shattering dissonance.</p>
                     <p>Whether swift or slow, this is how we grow.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.4</book-part-id>
                  <title-group>
                     <title>Prologue</title>
                  </title-group>
                  <fpage>xiii</fpage>
                  <abstract>
                     <p>People often ask how I got involved with animals. I can’t answer that, but I can recall my first encounter with the cold hard stones of what Calvin Luther Martin calls Adam’s Wall: the rigid barrier separating us from other species.</p>
                     <p>I was about eight or nine. My parents did not like zoos, circuses, or even pet stores. Their disapproval was less spoken than simply understood through the scarcity of visits to these places. And so it wasn’t my own family, but that of a friend, who brought me to an animal park.</p>
                     <p>The animal park was probably a zoo,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xxiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.6</book-part-id>
                  <title-group>
                     <title>A Note on Terminology and Sources</title>
                  </title-group>
                  <fpage>xxvi</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>The Existential Elephant</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>
                        <italic>She is middle-aged, amiable, but known to keep to herself. Unlike others her age, she has no children and there are no men in her life. She has two neighbors who are contemporaries, Patty and Maxine, but the two have formed a fast friendship, leaving Happy something of a third wheel. According to records, Happy was born in 1971. As an infant, she was forcibly taken from her family in the Asian wild and shipped to the United States. After five years at a facility in West Palm Beach, on March 21, 1977, she was transferred to the Bronx Zoo,</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>A Delicate Network</title>
                  </title-group>
                  <fpage>17</fpage>
                  <abstract>
                     <p>
                        <italic>Connie is a wild-caught female Asian elephant born around 1964. She has lived at the Dickerson Park Zoo in Springfield, Missouri, since 1981, when she was purchased from the Abilene Zoo in Texas. Since she was seventeen years old, Connie has undergone at least seventeen natural and artificial breeding attempts. She conceived three times as a result of mating with a bull, each experience with motherhood ending unhappily. One calf, born in June of 1985, was a stillborn female. Another female infant, Maiya, was born in 1991 and lived, but Connie rejected her. After six days, Connie took her back</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>A Strange Kind of Animal</title>
                  </title-group>
                  <fpage>34</fpage>
                  <abstract>
                     <p>
                        <italic>Dawn comes slowly on the veldt. With the touch of morning light, each still-life character wakens into slow motion. But there is nothing measured in the young bull elephant racing toward the feeding rhinoceros—a dull gray mound grazing among gilded yellow grass. Hornbill chatter and the gazelles’ tentative glide burst into loud squawks and frantic movement as the two giant mammals collide. The air explodes with the crash of body against body, gray against gray, and deafening bellows. In a short while, stillness descends. Gradually, birds and antelope filter back, and the landscape resumes its former repose. One by</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Deposited in the Bones</title>
                  </title-group>
                  <fpage>52</fpage>
                  <abstract>
                     <p>
                        <italic>I first felt the danger when the birds suddenly became silent and the air stilled. Then came the deep thunder of Man’s guns. The cold clarity of memory filled my mind, an image of the massacre of many seasons ago. We lost many, even a few children, too young to be hunted for ivory, but felled in the hail of bullets. Smoke and sound burst from all around, and the calves ran screaming as they saw their mothers fall. I called to the rest to follow, and we ran into the forest, where I knew we would be safe and</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Bad Boyz</title>
                  </title-group>
                  <fpage>70</fpage>
                  <abstract>
                     <p>
                        <italic>Suddenly one day, while he suckled from his mother, there was an explosion. Noise filled the air, dust blew up in clouds, and the earth trembled. He could hear his mother and aunts calling frantically, but he lost sight of them in the confusion. When he ran to find them, he was caught in long, painful thorns. The next thing he remembers is being grabbed and tethered with coarse rope to the warm, motionless bulk of his mother. When the shooting stopped, he was pushed inside a truck with others, and after many hours, they arrived somewhere he had never</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Elephant on the Couch:</title>
                     <subtitle>Case Study, E. M.</subtitle>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>
                        <italic>E. M. (not her actual initials, case file identification 864858) is a thirty-two-year-old female (date of birth estimated 1976) who has been institutionalized since she was approximately two to three years of age. She came to the present facility in Dallas on December 14, 1986, and is being reassigned for a move to Mexico. Her current institution seeks to move her because of self-injurious behavior and persistent aggression in the form of outbreaks of attacks on staff. There are concerns that her depression following the loss of a companion will exacerbate these behaviors. According to staff, E.M has a history</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.13</book-part-id>
                  <title-group>
                     <title>Illustrations</title>
                  </title-group>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.14</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>The Sorrow of the Cooking Pot</title>
                  </title-group>
                  <fpage>115</fpage>
                  <abstract>
                     <p>We now explore what happens after trauma, what happens in the wake of pain “deposited in the bones” and how an individual, community, and culture recover from cataclysmic change. Trauma treatment typically focuses on those who have lived through the experience; indeed, it is their plight that moves us to compassion and inspires us to help them to heal. But victims are not the only ones involved. In keeping with attachment theory and neurosciences, Archbishop Desmond Tutu observes that neither are we apart from those who have suffered nor are we immune from their pain. “We are bound up in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.15</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>The Biology of Forgiveness</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>
                        <italic>Girija Prasad, also known as Manikantan, is a twenty-year-old Asian elephant who worked for many years as a temple elephant in India. In 2004 he was rescued by Compassion Unlimited Plus Action (CUPA), an animal welfare organization based in Bangalore, after a police complaint against Girija’s treatment was filed. Girija was brought to the Bannerghatta Biological Park. Dr. Surendra Varma of the Asian Elephant Specialist Group (AESG) evaluated Girija’s condition as dangerously poor, and Girija was diagnosed with complex posttraumatic stress disorder, common among long-term prisoners. More than sixty wounds to his face and neck alone were identified. On March</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.16</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Am I an Elephant?</title>
                  </title-group>
                  <fpage>147</fpage>
                  <abstract>
                     <p>
                        <italic>Week 3: The Divas are still running around like crazy—they are nonstop play activity. They have pushed over huge trees and are having a blast dragging them into pond to play. At one point there were six elephants playing in the pond together, frenetically—the water churned up like there were a bunch of piranhas under the surface. Suddenly something happened: Debbie uttered a sound and Minnie reacted. Minnie hit Debbie across the head with her trunk. Minnie slams into Debbie, one time knocking her feet out from under her, and grabs Debbie’s tail, ears, and sometimes trunk. It</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.17</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Speaking in Tongues</title>
                  </title-group>
                  <fpage>174</fpage>
                  <abstract>
                     <p>
                        <italic>As she approached his enclosure, her body stiffened and a cold sweat broke out. Massa would agitate and vocalize with his trunk swaying. It took every ounce of willpower to stop from just throwing the food and running. One day, Anna willed herself to stand still and kept eye contact with the approaching mountain. Suddenly, something happened. Anna began to cry, tears streaming down her cheeks and neck. The bull came to a standstill motionless in front of her and slowly reached out with his trunk. Anna reached through to touch it. Later, she recounted: “At that moment, it was</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.18</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Where Does the Soul Go?</title>
                  </title-group>
                  <fpage>188</fpage>
                  <abstract>
                     <p>
                        <italic>According to the African Elephant Studbook, Medundamelli was born in Zimbabwe in 1969 and, given the year of her birth, almost certainly was captured during the mass killing of her family. At age two, probably while tethered to a dead body and after watching the ivory hacked off the faces of her relatives, “Dunda” was captured, and she arrived at the San Diego Wild Animal Park on September 18, 1971. On February 4, 1977, she was transferred across town to the sister institution, the San Diego Zoo, then brought back to the Wild Animal Park, near Escondido, California, a year</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.19</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Beyond Numbers</title>
                  </title-group>
                  <fpage>221</fpage>
                  <abstract>
                     <p>
                        <italic>Chalakka Mohanan was a fifty-three-year-old Asian elephant, old enough to be a grandfather or even a great-grandfather, at a time of life to be surrounded by his progeny and looked up to by the young males in his group. But as a young elephant, he had been taken into servitude. When townspeople found him, he was alone. He had been abandoned by mahouts and left tethered with no shelter or food. Chalakka Mohanan, descendant of a proud heritage, starved to death. He was owned by Anwar Saddath of Perumbavoor, India, and had been brought by “middlemen” to parade in temple</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.20</book-part-id>
                  <title-group>
                     <title>Epilogue:</title>
                     <subtitle>Quilt Making</subtitle>
                  </title-group>
                  <fpage>248</fpage>
                  <abstract>
                     <p>
                        <italic>Elephant and human societies are linked in prehistory, the present, and with greatest hope, the future. The stories of these elephants are not only what we have done to them but also what has happened to our collective soul. These moments in the present here and now are the ripest moments for a new paradigm with newfound understanding. Like elephants, we humans have watched our families imprisoned and murdered, our children shed blood in our backyards and on foreign turf. We now fight back with a formidable strength that comes from common bonds and love. Elephants and humans have missed</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.21</book-part-id>
                  <title-group>
                     <title>Appendix:</title>
                     <subtitle>Ten Things You Can Do to Help Elephants</subtitle>
                  </title-group>
                  <fpage>253</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.22</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>257</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1npr32.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>293</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
