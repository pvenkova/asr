<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jpeaceresearch</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100245</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of Peace Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Sage Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00223433</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14603578</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25654463</article-id>
         <title-group>
            <article-title>Military Spending and Inequality: Panel Granger Causality Test</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>ERIC S.</given-names>
                  <surname>LIN</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>HAMID E.</given-names>
                  <surname>ALI</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>9</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25654457</issue-id>
         <fpage>671</fpage>
         <lpage>685</lpage>
         <permissions>
            <copyright-statement>Copyright © 2009 International Peace Research Institute</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25654463"/>
         <abstract>
            <p>The relationship between military spending and economic inequality is not well documented within the empirical literature, while numerous studies have uncovered the linkages between military spending and other macroeconomic variables, such as economic growth, unemployment, purchasing power parity, black market premium, poverty and investment. The purpose of this article is to examine the causal relationship between military spending and inequality using BVC and SIPRI data across 58 countries from 1987 to 1999. Panel unit root tests indicate that two inequality measures (Theil and EHII) under consideration are likely to be non-stationary. The authors' work addresses the adverse implications of modeling with non-stationary variables, since this omission casts serious doubt on the reliability of the relationship between military spending and inequality. The recently developed panel Granger non-causality tests provide no evidence to support the causal relationship in either direction between the military spending and the change in economic inequality. The results are consistently robust to alternative data sources for military spending, to alternative definitions of the inequality measures, to the log transformation of the military spending, to the deletion of some data points, and to the division of OECD and non-OECD countries. Finally, the impulse responses and variance decompositions based on the panel vector autoregressive regression model are consistent with the findings from Granger non-causality tests.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d2095e174a1310">
            <label>*</label>
            <p>
               <mixed-citation id="d2095e181" publication-type="other">
http://vvww.prio.
no/jpr/datasets.</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e191a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d2095e198" publication-type="other">
http://
publications.worldbank.org/WDI</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2095e207" publication-type="other">
Angeles-Castro (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e214a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d2095e221" publication-type="other">
Abell (1994)</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e228a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d2095e235" publication-type="other">
Yildirim &amp;amp; Sezgin (2002).</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e243a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d2095e250" publication-type="other">
Henderson (1998)</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e257a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d2095e264" publication-type="other">
Abell (1994)</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e271a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d2095e278" publication-type="other">
Levin, Lin &amp;amp; Chu (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2095e284" publication-type="other">
Levin &amp;amp; Lin
(1992)</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e294a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d2095e301" publication-type="other">
The EHII and UTIP-UNIDO (Theil index) datasets
are available at http://utip.gov.utexas.edu/data.html.</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e311a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d2095e318" publication-type="other">
Assane &amp;amp; Grammy (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2095e324" publication-type="other">
US annual data (1960-96)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2095e330" publication-type="other">
Nath &amp;amp; Mamun (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d2095e337a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d2095e344" publication-type="other">
Hurlin (2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d2095e350" publication-type="other">
2005)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>&lt;bold&gt;References&lt;/bold&gt;</title>
         <ref id="d2095e366a1310">
            <mixed-citation id="d2095e370" publication-type="other">
Abell, John D., 1990. 'Defense Spending and
Unemployment Rates: An Empirical Analysis
Disaggregated by Race', Cambridge Journal of
Economics 14(4): 405-419.</mixed-citation>
         </ref>
         <ref id="d2095e386a1310">
            <mixed-citation id="d2095e390" publication-type="other">
Abell, John D., 1992. 'Defense Spending and
Unemployment Rates: An Empirical Analysis
Disaggregated by Race and Gender', American
Journal of Economics and Sociology 51(1):
27-42.</mixed-citation>
         </ref>
         <ref id="d2095e409a1310">
            <mixed-citation id="d2095e413" publication-type="other">
Abell, John D., 1994. 'Military Spending and
Inequality', Journal of Peace Research 31(1):
35-43.</mixed-citation>
         </ref>
         <ref id="d2095e426a1310">
            <mixed-citation id="d2095e430" publication-type="other">
Ali, Hamid Eltgani, 2004. 'Essays on Economic
Development and Conflict', PhD dissertation,
University of Texas at Austin.</mixed-citation>
         </ref>
         <ref id="d2095e444a1310">
            <mixed-citation id="d2095e448" publication-type="other">
Ali, Hamid Eltgani, 2007. 'Military Expenditures
and Inequality: Empirical Evidence from
Global Data', Defence and Peace Economics
18(6): 519-535.</mixed-citation>
         </ref>
         <ref id="d2095e464a1310">
            <mixed-citation id="d2095e468" publication-type="other">
Angeles-Castro, Gerardo, 2006. 'The Relationship
Between Economic Growth and Inequality:
Evidence from the Age of Market Liberalism',
working paper, University of Kent.</mixed-citation>
         </ref>
         <ref id="d2095e484a1310">
            <mixed-citation id="d2095e488" publication-type="other">
Assane, Djeto &amp;amp; Abbas Grammy, 2003. 'An
Assessment of the Growth and Inequality
Causality Relationship', Applied Economics
Letters 10(14): 871-873.</mixed-citation>
         </ref>
         <ref id="d2095e504a1310">
            <mixed-citation id="d2095e508" publication-type="other">
Bahmani-Oskooee, Mohsen &amp;amp; Gour Gobinda
Goswami, 2005. 'Military Spending as
Another Cause of the Failure of the PPP',
Applied Economics Letters 12(11): 663—667.</mixed-citation>
         </ref>
         <ref id="d2095e524a1310">
            <mixed-citation id="d2095e528" publication-type="other">
Bahmani-Oskooee, Mohsen &amp;amp; Gour Gobinda
Goswami, 2006. 'Military Spending and
the Black Market Premium in Developing
Countries', Review of Social Economy 64(1):
77-91.</mixed-citation>
         </ref>
         <ref id="d2095e547a1310">
            <mixed-citation id="d2095e551" publication-type="other">
Barker, Terry; Paul Dunne &amp;amp; Ron Smith, 1991.
'Measuring the Peace Dividend in the United
Kingdom', Journal of Peace Research 28(4):
345-358.</mixed-citation>
         </ref>
         <ref id="d2095e568a1310">
            <mixed-citation id="d2095e572" publication-type="other">
Beach, Charles M., 1977. 'Cyclical Sensitivity of
Aggregate Income Equality', Review of Eco-
nomics and Statistics 59(1): 56-66.</mixed-citation>
         </ref>
         <ref id="d2095e585a1310">
            <mixed-citation id="d2095e589" publication-type="other">
Blau, Francine D. &amp;amp; Lawrence M. Kahn, 1996.
'International Differences in Male Wage
Inequality: Institutions versus Market Forces',
Journal of Political Economy 104(4): 791-837.</mixed-citation>
         </ref>
         <ref id="d2095e605a1310">
            <mixed-citation id="d2095e609" publication-type="other">
Brauer, Jurgen, 2007. 'Data, Models, Coeffi-
cients: The Case of United States Military
Expenditure', Conflict Management and Peace
Science 24(1): 55-64.</mixed-citation>
         </ref>
         <ref id="d2095e625a1310">
            <mixed-citation id="d2095e629" publication-type="other">
Breitung, Jorg, 2000. The Local Power of Some
Unit Root Tests for Panel Data', in B. Baltagi,
ed., Nonstationary Panels, Panel Cointegration,
and Dynamic Panels, Advances in Econometrics.
Vol 15. Amsterdam: JAI (161-178).</mixed-citation>
         </ref>
         <ref id="d2095e648a1310">
            <mixed-citation id="d2095e652" publication-type="other">
Caverley, Jonathan D., 2007. 'Who Pays for
Defense in Democracies? Inequality, Redis-
tribution and the Foundations of Militarism',
paper presented at the 48th Annual Meet-
ing of the International Studies Association,
Chicago, IL, 3 March 2007.</mixed-citation>
         </ref>
         <ref id="d2095e675a1310">
            <mixed-citation id="d2095e679" publication-type="other">
Chan, Steve, 1985. The Impact of Defense
Spending on Economic Performance: A Sur-
vey of Evidence and Problems', Orbis 29(2):
403-434.</mixed-citation>
         </ref>
         <ref id="d2095e696a1310">
            <mixed-citation id="d2095e700" publication-type="other">
Choe, Jong II, 2003. 'Do Foreign Direct Invest-
ment and Gross Domestic Investment Promote
Economic Growth?', Review of Development
Economics 7(1): 44-57.</mixed-citation>
         </ref>
         <ref id="d2095e716a1310">
            <mixed-citation id="d2095e720" publication-type="other">
Choi, In, 2001. 'Unit Root Tests for Panel Data',
Journal of International Money and Finance
20(2): 249-272.</mixed-citation>
         </ref>
         <ref id="d2095e733a1310">
            <mixed-citation id="d2095e737" publication-type="other">
Chowdhury, Abdur R., 1991. 'A Causal Analysis
of Defense Spending and Economic Growth',
fournal of Conflict Resolution 35(1): 80-97.</mixed-citation>
         </ref>
         <ref id="d2095e750a1310">
            <mixed-citation id="d2095e754" publication-type="other">
Deger, Saadet, 1985. 'Human Resources, Govern-
ment Education Expenditure and the Military
Burden in Less Developed Countries', Journal
of 'DevelopingAreas 20(1): 37-48.</mixed-citation>
         </ref>
         <ref id="d2095e770a1310">
            <mixed-citation id="d2095e774" publication-type="other">
Deininger, Klaus &amp;amp; Lyn Squire, 1996. 'A New
Data Set Measuring Income Inequality', World
Bank Economic Review 10(3): 565-591.</mixed-citation>
         </ref>
         <ref id="d2095e787a1310">
            <mixed-citation id="d2095e791" publication-type="other">
Diamond, Larry, 1992. 'Economic Develop-
ment and Democracy Reconsidered', in Gary
Marks &amp;amp; Larry Diamond, eds, Reexamining
Democracy: Essays in Honor of Seymour Martin
Lipset. Newbury Park, CA: Sage (93-139).</mixed-citation>
         </ref>
         <ref id="d2095e811a1310">
            <mixed-citation id="d2095e817" publication-type="other">
DiNardo, John; Nicole M. Fortin &amp;amp; Thomas
Lemieux, 1996. 'Labor Market Institutions
and the Distribution of Wages, 1973-1992:
A Semi-Parametric Approach', Econometrica
64(5): 1001-1044.</mixed-citation>
         </ref>
         <ref id="d2095e836a1310">
            <mixed-citation id="d2095e840" publication-type="other">
Dreze, Jean, 2000. 'Militarism, Development,
and Democracy', Economic and Political
Weekly (April): 1171-1183.</mixed-citation>
         </ref>
         <ref id="d2095e853a1310">
            <mixed-citation id="d2095e857" publication-type="other">
Dunne, J. Paul &amp;amp; Ron Smith, 1990. 'Military
Expenditure and Employment in the OECD',
Defence Economics 1(1): 57-73.</mixed-citation>
         </ref>
         <ref id="d2095e870a1310">
            <mixed-citation id="d2095e874" publication-type="other">
Dunne, J. Paul; Ron Smith &amp;amp; Dirk Willenbockel,
2005. 'Models of Military Expenditure and
Growth: A Critical Review', Defence and Peace
Economics 16(6): 449-461.</mixed-citation>
         </ref>
         <ref id="d2095e890a1310">
            <mixed-citation id="d2095e894" publication-type="other">
Galbraith, James K., 1998. Created Unequal: The
Crisis in American Pay. New York: Free Press.</mixed-citation>
         </ref>
         <ref id="d2095e904a1310">
            <mixed-citation id="d2095e908" publication-type="other">
Galbraith, James K. &amp;amp; Pedro Conceicao, 2001.
'Towards a New Kuznets Hypothesis: Theory
and Evidence on Growth and Inequality', in
James K. Galbraith &amp;amp; Maureen Berner, eds,
Inequality and Industrial Change: A Global
View. Cambridge: Cambridge University
Press (139-160).</mixed-citation>
         </ref>
         <ref id="d2095e935a1310">
            <mixed-citation id="d2095e939" publication-type="other">
Galbraith, James K. &amp;amp; Hyunsub Kum, 2004.
'Estimating the Inequality of Household
Incomes: A Statistical Approach to the Cre-
ation of a Dense and Consistent Global Data
Set', working paper, University of Texas
Inequality Project.</mixed-citation>
         </ref>
         <ref id="d2095e962a1310">
            <mixed-citation id="d2095e966" publication-type="other">
Gradstein, Mark; Branko Milanovic &amp;amp; Yvonne
Ying, 2001. Democracy and Income Inequal-
ity: An Empirical Analysis. Washington, DC:
World Bank.</mixed-citation>
         </ref>
         <ref id="d2095e982a1310">
            <mixed-citation id="d2095e986" publication-type="other">
Granger, Clive W. J., 1969. 'Investigating Causal
Relations by Econometric Models and
Cross-Spectral Methods', Econometrica 37(3):
424-438.</mixed-citation>
         </ref>
         <ref id="d2095e1002a1310">
            <mixed-citation id="d2095e1006" publication-type="other">
Granger, Clive W. J., 2003. 'Some Aspects of
Causal Relationships', Journal of Econometrics
112(1): 69-71.</mixed-citation>
         </ref>
         <ref id="d2095e1019a1310">
            <mixed-citation id="d2095e1023" publication-type="other">
Griffin, Larry J., Joel A. Devine &amp;amp; Michael
Wallace, 1982. 'Monopoly Capital, Orga-
nized Labor, and Military Expenditures in the
United States, 1949-1976', American Journal
of'Sociology 88: SI 13-53.</mixed-citation>
         </ref>
         <ref id="d2095e1042a1310">
            <mixed-citation id="d2095e1046" publication-type="other">
Hadri, Kaddour, 2000. 'Testing for Stationarity
in Heterogeneous Panel Data', Econometrics
Journal3(2): 148-161.</mixed-citation>
         </ref>
         <ref id="d2095e1060a1310">
            <mixed-citation id="d2095e1064" publication-type="other">
Henderson, Errol Anthony, 1998. 'Military
Spending and Poverty', Journal of Politics
60(2): 503-520.</mixed-citation>
         </ref>
         <ref id="d2095e1077a1310">
            <mixed-citation id="d2095e1081" publication-type="other">
Hoffmann, Robert; Chew-Ging Lee; Bala
Ramasamy &amp;amp; Matthew Yeung, 2005. 'FDI
and Pollution: A Granger Causality Test
Using Panel Data', Journal of International
Development 17(3): 311-317.</mixed-citation>
         </ref>
         <ref id="d2095e1100a1310">
            <mixed-citation id="d2095e1104" publication-type="other">
Holtz-Eakin, Douglas; Whitney Newey &amp;amp;
Harvey S. Rosen, 1988. 'Estimating Vector
Autoregressions with Panel Data', Econo-
metrica 56(6): 1371-1395.</mixed-citation>
         </ref>
         <ref id="d2095e1120a1310">
            <mixed-citation id="d2095e1124" publication-type="other">
Hooker, Mark A. &amp;amp; Micheal M. Knetter, 1997.
'The Effects of Military Spending on Eco-
nomic Activities: Evidence from State Pro-
curement Spending', Journal of Money, Credit
and Banking 29: 400-421.</mixed-citation>
         </ref>
         <ref id="d2095e1143a1310">
            <mixed-citation id="d2095e1147" publication-type="other">
Hurlin, Christophe, 2004. 'Testing Granger
Causality in Heterogeneous Panel Data
Models with Fixed Coefficients', mimeo,
University Paris IX.</mixed-citation>
         </ref>
         <ref id="d2095e1163a1310">
            <mixed-citation id="d2095e1167" publication-type="other">
Hurlin, Christophe, 2005. 'Un Test Simple de
l'Hypothese de Non-Causalite dans un Modele
de Panel Heterogene', Revue Economique
56(3): 799-809.</mixed-citation>
         </ref>
         <ref id="d2095e1184a1310">
            <mixed-citation id="d2095e1188" publication-type="other">
Hurlin, Christophe &amp;amp; Baptiste Venet, 2001.
'Granger Causality Tests in Panel Data Models
with Fixed Coefficients', mimeo, University
Paris IX.</mixed-citation>
         </ref>
         <ref id="d2095e1204a1310">
            <mixed-citation id="d2095e1208" publication-type="other">
Im, Kyung So; M. Hashem Pesaran &amp;amp; Yongcheol
Shin, 2003. 'Testing for Unit Roots in Hetero-
geneous Panels', Journal of Econometrics
115(1): 53-74.</mixed-citation>
         </ref>
         <ref id="d2095e1224a1310">
            <mixed-citation id="d2095e1228" publication-type="other">
Kollias, Christos &amp;amp; Stelios Makrydakis, 2000.
'A Note on the Causal Relationship between
Defense Spending and Growth in Greece:
1955—93', Defense and Peace Economics 11(1):
173-184.</mixed-citation>
         </ref>
         <ref id="d2095e1247a1310">
            <mixed-citation id="d2095e1251" publication-type="other">
Levin, Andrew &amp;amp; Chien-Fu Lin, 1992. 'Unit
Root Tests in Panel Data: Asymptotic and
Finite-Sample Properties', University of
California at San Diego, Economics Working
Paper Series 92-23.</mixed-citation>
         </ref>
         <ref id="d2095e1270a1310">
            <mixed-citation id="d2095e1274" publication-type="other">
Levin, Andrew; Chien-Fu Lin &amp;amp; Chia-Shang
James Chu, 2002. 'Unit Root Tests in Panel
Data: Asymptotic and Finite-Sample Proper-
ties', Journal of Econometrics 108(1): 1-24.</mixed-citation>
         </ref>
         <ref id="d2095e1290a1310">
            <mixed-citation id="d2095e1294" publication-type="other">
Lipset, Seymour Martin; Kyoung-Ryung Seong &amp;amp;
John Charles Torres, 1993. 'A Comparative
Analysis of the Social Requisites of Democracy',
International Social Journal 45: 155-175.</mixed-citation>
         </ref>
         <ref id="d2095e1311a1310">
            <mixed-citation id="d2095e1317" publication-type="other">
Maddala, G. S. &amp;amp; Shaowen Wu, 1999. 'A Com-
parative Study of Unit Root Tests with Panel
Data and a New Simple Test', Oxford Bulletin
of Economics and Statistics 61: 631-652.</mixed-citation>
         </ref>
         <ref id="d2095e1333a1310">
            <mixed-citation id="d2095e1337" publication-type="other">
Nath, Hiranya K. &amp;amp; Khawaja Abdullah Al
Mamun, 2004. 'Trade Liberalization, Growth
and Inequality in Bangladesh: An Empirical
Analysis', working paper, Sam Houston State
University.</mixed-citation>
         </ref>
         <ref id="d2095e1356a1310">
            <mixed-citation id="d2095e1360" publication-type="other">
Nickell, Stephen, 1981. 'Biases in Dynamic
Models with Fixed Effects', Econometrica
49(6): 1471-1426.</mixed-citation>
         </ref>
         <ref id="d2095e1373a1310">
            <mixed-citation id="d2095e1377" publication-type="other">
Rodrik, Dani, 1999. 'Democracies Pay Higher
Wages', Quarterly Journal of Economics 114(3):
707-738.</mixed-citation>
         </ref>
         <ref id="d2095e1390a1310">
            <mixed-citation id="d2095e1394" publication-type="other">
Smith, Ron, 1977. 'Military Expenditure and
Capitalism', Cambridge Journal of Economics
1(1): 61-76.</mixed-citation>
         </ref>
         <ref id="d2095e1407a1310">
            <mixed-citation id="d2095e1411" publication-type="other">
Theil, Henri, 1979. 'World Income Inequality
and Its Components', Economics Letters 2(1):
99-102.</mixed-citation>
         </ref>
         <ref id="d2095e1425a1310">
            <mixed-citation id="d2095e1429" publication-type="other">
Yildirim, Julide &amp;amp; Selami Sezgin, 2002. 'Defence,
Education and Health Expenditures in
Turkey, 1924-96', Journal of Peace Research
39(5): 569-580.</mixed-citation>
         </ref>
         <ref id="d2095e1445a1310">
            <mixed-citation id="d2095e1449" publication-type="other">
Yildirim, Julide &amp;amp; Selami Sezgin, 2003. 'Military
Expenditure and Employment in Turkey',
Defence and Peace Economics 14(2): 129-139.</mixed-citation>
         </ref>
         <ref id="d2095e1462a1310">
            <mixed-citation id="d2095e1466" publication-type="other">
Yildirim, Julide; Selami Sezgin &amp;amp; Nadir Ocal,
2005. 'Military Expenditures and Economic
Growth in Middle Eastern Countries:
A Dynamic Panel Data Analysis', Defence and
Peace Economics 16(4): 283-295.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
