<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt4z65n</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7zvgvn</book-id>
      <subj-group>
         <subject content-type="call-number">E185.97.G3E95 2014</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Garvey, Marcus, 1887–1940</subject>
         <subj-group>
            <subject content-type="lcsh">Influence</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Universal Negro Improvement Association</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">African diaspora</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
         <subject>Sociology</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Age of Garvey</book-title>
         <subtitle>How a Jamaican Activist Created a Mass Movement and Changed Global Black Politics</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Ewing</surname>
               <given-names>Adam</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>24</day>
         <month>08</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="ppub">9780691157795</isbn>
      <isbn content-type="ppub">0691157790</isbn>
      <isbn content-type="epub">9781400852444</isbn>
      <isbn content-type="epub">1400852447</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Princeton University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt7zvgvn"/>
      <abstract abstract-type="short">
         <p>Jamaican activist Marcus Garvey (1887-1940) organized the Universal Negro Improvement Association in Harlem in 1917. By the early 1920s, his program of African liberation and racial uplift had attracted millions of supporters, both in the United States and abroad.<italic>The Age of Garvey</italic>presents an expansive global history of the movement that came to be known as Garveyism. Offering a groundbreaking new interpretation of global black politics between the First and Second World Wars, Adam Ewing charts Garveyism's emergence, its remarkable global transmission, and its influence in the responses among African descendants to white supremacy and colonial rule in Africa, the Caribbean, and the United States.</p>
         <p>Delving into the organizing work and political approach of Garvey and his followers, Ewing shows that Garveyism emerged from a rich tradition of pan-African politics that had established, by the First World War, lines of communication among black intellectuals on both sides of the Atlantic. Garvey's legacy was to reengineer this tradition as a vibrant and multifaceted mass politics. Ewing looks at the people who enabled Garveyism's global spread, including labor activists in the Caribbean and Central America, community organizers in the urban and rural United States, millennial religious revivalists in central and southern Africa, welfare associations and independent church activists in Malawi and Zambia, and an emerging generation of Kikuyu leadership in central Kenya. Moving away from the images of quixotic business schemes and repatriation efforts,<italic>The Age of Garvey</italic>demonstrates the consequences of Garveyism's international presence and provides a dynamic and unified framework for understanding the movement, during the interwar years and beyond.</p>
      </abstract>
      <counts>
         <page-count count="320"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.3</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.4</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In 1916, Marcus Mosiah Garvey, a young Jamaican printer, entrepreneur, and aspiring race leader, sailed into New York harbor. Before the end of the First World War, from his base in Harlem he launched his great mass organization, the Universal Negro Improvement Association and African Communities League (UNIA-ACL, hereafter UNIA), ostentatiously pledging to redeem both the continent of Africa and its descendants from the thrall of white supremacy. To facilitate industrial progress and generate new mechanisms of commercial wealth, he organized the Negro Factories Corporation and founded a transatlantic shipping company, the Black Star Line, inviting the scattered members of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.5</book-part-id>
                  <title-group>
                     <label>Chapter One</label>
                     <title>THE EDUCATION OF MARCUS MOSIAH GARVEY</title>
                  </title-group>
                  <fpage>15</fpage>
                  <abstract>
                     <p>On the morning of October 11, 1865, men and women streamed out of the small black settlement of Stony Gut, Jamaica and trooped in military formation toward the town of Morant Bay, in the parish of St. Thomas in the East. They were armed with sticks and cutlasses; some carried guns. At their head was a Native Baptist preacher and peasant farmer named Paul Bogle. The columns marched first to the police station, which was ransacked for weapons, then headed to the courthouse, where they confronted the volunteer militia. As the Queen’s representative in the parish, Custos Baron von Ketelhodt,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.6</book-part-id>
                  <title-group>
                     <label>Chapter Two</label>
                     <title>THE CENTER CANNOT HOLD</title>
                  </title-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>Joseph Booth arrived in the Shire Highlands in 1892, and established his Zambesi Industrial Mission at Michiru, north of Blantyre. Invasions by Yao-speaking people, the expanding slave trade, and a devastating famine in 1862 had left the once-populous region nearly uninhabited. After a protectorate was declared over the area that became Nyasaland (today Malawi) by the British Foreign Office in 1891, colonists faced the problem not of engineering land alienation—large tracts had been purchased in 1880s by speculators hoping to draw British interest in the highlands—but of establishing a reliable labor force. The year Booth arrived, the first</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.7</book-part-id>
                  <title-group>
                     <label>Chapter Three</label>
                     <title>AFRICA FOR THE AFRICANS!</title>
                  </title-group>
                  <fpage>76</fpage>
                  <abstract>
                     <p>Several months after the war, serving as a colonial agent in a remote outpost in northern Nigeria, the aspiring Anglo-Irish novelist Joyce Cary was summoned to adjudicate a peculiar case. A young man had been arrested by the local emir after he had been overheard “talking sedition” about a black king, armed with a great iron ship and an army of black soldiers, preparing to set out for Africa to drive the whites from the continent. To his great surprise, Cary was informed by his trusted political agent, an elderly Hausa man named Musa, that the story of a “white</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.8</book-part-id>
                  <title-group>
                     <label>Chapter Four</label>
                     <title>“THE SILENT WORK THAT MUST BE DONE”</title>
                  </title-group>
                  <fpage>107</fpage>
                  <abstract>
                     <p>The opening parade of the Second International Convention of Negroes of the World stretched two miles, up and down the wide avenues of Seventh and Lenox. Large crowds assembled along the parade route, and residents hung streamers bearing the UNIA tricolor, the red, black, and green. Harlem, the<italic>Negro World</italic>boasted, had declared an unofficial public holiday. Leading the estimated ten thousand parade participants was Captain E. L. Gaines, the head of the Universal African Legions, astride a horse. The Black Star Line band and Liberty Hall choir played behind him. Then came the officials of the High Executive Council,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.9</book-part-id>
                  <title-group>
                     <label>Chapter Five</label>
                     <title>THE TIDE OF PREPARATION</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>On the evening of March 12, 1927 Arthur S. Gray found himself sharing the stage with a compelling cast of characters. Edgar Owens of the Communist Party sat to his side, accompanied by a representative from the Socialist party; a young Chinese student and an older Chinese man were joined by a representative of the People’s Party in India. The occasion was the second anniversary of the passing of Sun Yat-Sen. A celebration was being held at the headquarters of the Chinese Nationalist Party (Kuomingtang) in San Francisco. A nationalist icon in China, Sun was also revered as a symbol</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.10</book-part-id>
                  <title-group>
                     <label>Chapter Six</label>
                     <title>BROADCAST ON THE WINDS</title>
                  </title-group>
                  <fpage>160</fpage>
                  <abstract>
                     <p>On November 25, 1923 in the heart of the Shire Highlands, the white settler region of colonial Nyasaland, four Angoni youths visited the store of Osman Gani and asked about the price of cloth. When the shopkeeper quoted a figure, the young men demurred, and provocatively explained that they would wait until the next month, when Europeans and Indians were gone from the country and the cloth could be acquired for free. On the same day, on the nearby Glenbreck Estate, another Indian shopkeeper, Hassam Osman, told a surprised white patron that he would prefer not to install proper windows</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.11</book-part-id>
                  <title-group>
                     <label>Chapter Seven</label>
                     <title>THE VISIBLE HORIZON</title>
                  </title-group>
                  <fpage>186</fpage>
                  <abstract>
                     <p>In the middle of May 1923, Isaac Clements Katongo Muwamba, a government clerk working in Lusaka, Northern Rhodesia (Zambia), approached the colony’s Chief Secretary, Donald Mackenzie-Kennedy, with a proposal to establish a “Native Improvement Association” in the town compound. “[S]ince February … I have been observing … the constant week-end quarrels, differences and disrespectfulness among the African workers that consist of many tribes now living in the Lusaka area due no doubt to hatred, lies and jealousy,” explained Muwamba. Such circumstances not only encouraged breaches of justice; they were “barriers against civilisation and improvement.” They threatened to return Africans living</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.12</book-part-id>
                  <title-group>
                     <label>Chapter Eight</label>
                     <title>MUIGWITHANIA (THE RECONCILER)</title>
                  </title-group>
                  <fpage>212</fpage>
                  <abstract>
                     <p>In the beginning of things, Mogai (<italic>Ngai</italic>, God) took the man Gikuyu (Kikuyu) to the top of Mount Kenya and bequeathed to him the bountiful land below. Mogai provided Gikuyu with a wife, Moombi (Mumbi), and they had nine daughters. When the daughters had grown, Mogai provided them with nine husbands, and together they prospered and had many children of their own. After the passing of Gikuyu and Moombi, the nine daughters divided their families into separate<italic>mbaris</italic>, or clans, and spread across the land of their father. To sustain kinship ties and preserve bonds of unity, they organized themselves</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.13</book-part-id>
                  <title-group>
                     <title>AFTERWORD</title>
                  </title-group>
                  <fpage>238</fpage>
                  <abstract>
                     <p>The bustling streets of modern Nairobi are inscribed with the history of Kenya’s anticolonial struggle. Kenyatta Avenue runs along Uhuru Park and through the heart of the city’s downtown, past Koinange Street, past Kimathi Street and a memorial to the executed rebel, Dedan Kimathi, and ending at Tom Mboya Street. Harry Thuku Road starts out toward the affluent Westlands before abruptly halting; Joseph Kangethe Road runs to the north of the Kibera slums. Alongside these monuments to national liberation are gestures to a broader narrative of pan-Africanism: Haile Selassie Avenue and Menelik Road, an ode to Ethiopia; Nkrumah Lane, Banda</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.14</book-part-id>
                  <title-group>
                     <title>ABBREVIATIONS</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.15</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.16</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>299</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zvgvn.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>305</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
