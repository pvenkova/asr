<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wpgj4</book-id>
      <subj-group>
         <subject content-type="call-number">JF1081.C6733 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Political corruption</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Transnational crime</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Security, International</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Nuclear nonproliferation</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Corruption, Global Security, and World Order</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Rotberg</surname>
               <given-names>Robert I.</given-names>
            </name>
            <role>Editor</role>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>12</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="epub">9780815703969</isbn>
      <isbn content-type="epub">0815703961</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>World Peace Foundation</copyright-holder>
         <copyright-holder>American Academy of Arts &amp; Sciences</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt6wpgj4"/>
      <abstract abstract-type="short">
         <p>Never before have world order and global security been threatened by so many destabilizing factors -from the collapse of macroeconomic stability to nuclear proliferation, terrorism, and tyranny.<italic>Corruption, Global Security, and World Order</italic>reveals corruption to be at the very center of these threats and proposes remedies such as positive leadership, enhanced transparency, tougher punishments, and enforceable sanctions. Although eliminating corruption is difficult, this book's careful prescriptions can reduce and contain threats to global security.</p>
         <p>Contributors: Matthew Bunn (Harvard University), Erica Chenoweth (Wesleyan University), Sarah Dix (Government of Papua New Guinea), Peter Eigen (Freie Universität, Berlin, and Africa Progress Panel), Kelly M. Greenhill (Tufts University), Charles Griffin (World Bank and Brookings), Ben W. Heineman Jr. (Harvard University), Nathaniel Heller (Global Integrity), Jomo Kwame Sundaram (United Nations), Lucy Koechlin (University of Basel, Switzerland), Johann Graf Lambsdorff (University of Passau, Germany, and Transparency International), Robert Legvold (Columbia University), Emmanuel Pok (National Research Institute, Papua New Guinea), Susan Rose-Ackerma n (Yale University), Magdalena Sepúlveda Carmona (United Nations), Daniel Jordan Smith (Brown University), Rotimi T. Suberu (Bennington College), Jessica C. Teets (Middlebury College), and Laura Underkuffler (Cornell University).</p>
      </abstract>
      <counts>
         <page-count count="497"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Rotberg</surname>
                           <given-names>Robert I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>How Corruption Compromises World Peace and Stability</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROTBERG</surname>
                           <given-names>ROBERT I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Corruption is a human condition and an ancient phenomenon. From Mesopotamian times, if not before, public notables have abused their offices for personal gain; both well-born and common citizens have sought advantage by corrupting those holding power or controlling access to perquisites. The exercise of discretion, especially forms of discretion that facilitate or bar entry to opportunity, is a magnetic impulse that invariably attracts potential abusers. Moreover, since nearly all tangible opportunities are potentially zero-sum in their impact on individuals or classes of individuals, it is almost inevitable that claimants will seek favors from authorities and that authorities, in turn,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Defining Corruption:</title>
                     <subtitle>Implications for Action</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>UNDERKUFFLER</surname>
                           <given-names>LAURA S.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>The problems that result from public officials’ participation in corruption, and allegations of such problems, plague emerging and established governments. It is easy to deplore the existence of corruption; it is far more difficult to determine what corruption truly is and why we deplore it as a social and political evil.</p>
                     <p>For those who are actively involved in fighting corruption, an in-depth examination of what appears to be a definitional question might seem to be a misplaced effort. Obviously, the definition of corruption is important in the shaping of legal prohibitions and controls. In addition, there are issues of cultural</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Defining and Measuring Corruption:</title>
                     <subtitle>Where Have We Come From, Where Are We Now, and What Matters For the Future?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HELLER</surname>
                           <given-names>NATHANIEL</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>47</fpage>
                  <abstract>
                     <p>The importance in measuring corruption, and by extension, good governance (one of its antidotes), is not simply an esoteric academic debate left to development economists, political theoreticians, and statisticians. It has become, rather, a central issue to the broader field of good governance and anti-corruption reform, as a country’s performance in such reforms has become increasingly linked to foreign aid flows.</p>
                     <p>While former World Bank President James Wolfensohn’s famous 1996 “cancer of corruption” speech marked a watershed in acknowledging corruption as a central development issue, the challenge of measuring corruption and anti-corruption performance leapt to the forefront during the “Monterrey</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Corruption in the Wake of Domestic National Conflict</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROSE-ACKERMAN</surname>
                           <given-names>SUSAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>66</fpage>
                  <abstract>
                     <p>Nation-states emerging from conflict are particularly susceptible to corruption. Many of the factors that create corrupt incentives in<italic>any</italic>society are likely to be present in post-conflict environments. The cumulative effect of these factors may be greater than the independent effect of each one. Although corruption is a potential problem in all post-conflict states, the nature of the conflict and the conditions under which the conflict ended help to determine the types of corruption that emerge. Although there are broad similarities across all corrupt environments, including post-conflict situations, there are also differences, which can be traced to the conflict itself</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Kleptocratic Interdependence:</title>
                     <subtitle>Trafficking, Corruption, and the Marriage of Politics and Illicit Profits</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>GREENHILL</surname>
                           <given-names>KELLY M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>96</fpage>
                  <abstract>
                     <p>On 23 April 2008, United States Attorney General Michael Mukasey offered a stark and foreboding assessment of the rising threat from international organized crime, asserting that the new global criminals are “more sophisticated, they are richer, they have greater influence over government and political institutions worldwide . . . and [they] are far more involved in our everyday lives than many people appreciate. . . . [Consequently], we can’t ignore criminal syndicates in other countries on the naïve assumption that they are a danger only in their homeland, whether it is located in Eurasia, Africa, or anywhere else.”¹ Mukasey’s troubling</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Corruption and Nuclear Proliferation</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>BUNN</surname>
                           <given-names>MATTHEW</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>124</fpage>
                  <abstract>
                     <p>Corruption is a critical, under-recognized contributor to nuclear proliferation. With the possible exception of North Korea, corruption was a central enabling factor in all of the nuclear weapons programs of both states and terrorist groups in the past two decades. Indeed, corruption is likely to be essential to most cases of nuclear proliferation. Unless a state or group can get all the materials and technology needed for its nuclear weapons program from some combination of its own indigenous resources; outside sources motivated only by a desire to help that nuclear program; or outside sources genuinely fooled into providing technology that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>To Bribe or to Bomb:</title>
                     <subtitle>Do Corruption and Terrorism Go Together?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>TEETS</surname>
                           <given-names>JESSICA C.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>CHENOWETH</surname>
                           <given-names>ERICA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>167</fpage>
                  <abstract>
                     <p>Colombia consistently ranks as one of the more corrupt countries in the world; it ranked 3.8 out of 10 on the 2008 Corruption Perceptions Index. Incidentally, Colombia has suffered decades of terrorist attacks conducted by one of the most enduring terrorist groups in the world—the Fuerzas Armadas Revolucionarias de Colombia (FARC). Conventional wisdom suggests, and U.S. policy subscribes to the belief, that corruption and terrorism coexist in a mutually reinforcing relationship.¹ Such a conclusion seems intuitive given conditions in Colombia and elsewhere, where corruption and terrorism seem to coincide. This relationship, however, has always been a matter of speculation;</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Corruption, the Criminalized State, and Post-Soviet Transitions</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LEGVOLD</surname>
                           <given-names>ROBERT</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>194</fpage>
                  <abstract>
                     <p>Scarcely anyone doubts that Russia and the majority of post-Soviet states are among the most corrupt in the world. Only a scattering of African and Latin American states can claim as much, and even then not with the same across-the-board regional sweep.¹ In Transparency International’s (TI) 2008 rankings, 8 out of the 12 post-Soviet states were clustered in the bottom 35 of the 180 states rated.² A host of additional studies, surveys, and anecdotal evidence come to roughly the same conclusion. Since Russia, with its eleven time zones, nearly a third of the world’s gas, and close to half of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Combating Corruption in Traditional Societies:</title>
                     <subtitle>Papua New Guinea</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>DIX</surname>
                           <given-names>SARAH</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>POK</surname>
                           <given-names>EMMANUEL</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>239</fpage>
                  <abstract>
                     <p>Corruption is rampant and largely unchecked in the Pacific island nation of Papua New Guinea (PNG).¹ Grand corruption has many faces in PNG: nepotism, administrative corruption, and state capture. Systemic nepotism, including ghost workers on a payroll and political favor-based hiring, has cost the state millions of dollars in just one province, as seen in the case of the Southern Highlands provincial administration. Administrative corruption reportedly takes place in a formalized system of commissions on real or rigged procurement contracts, out of court settlements, and other payments made by the state to private actors. State capture is evident to most</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>The Travails of Nigeria’s Anti-Corruption Crusade</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SUBERU</surname>
                           <given-names>ROTIMI T.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>260</fpage>
                  <abstract>
                     <p>Since Nigeria’s transition from military to civilian rule in 1999, the country has witnessed an unprecedented official campaign against political corruption. Whereas previous attempts to combat this endemic and systematic malaise were largely ad hoc, arbitrary, rhetorical, ineffectual, or counterproductive in nature or impact, the post-1999 anti-corruption campaign has involved the establishment or revitalization of three national anti-corruption agencies, namely, the Code of Conduct Bureau (CCB) and Code of Conduct Tribunal (CCT); the Independent Corrupt Practices and Other Related Offences Commission (ICPC); and the Economic and Financial Crimes Commission (EFCC). These agencies have spearheaded the unprecedented investigations, indictments, prosecutions, convictions,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.14</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>The Paradoxes of Popular Participation in Corruption in Nigeria</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SMITH</surname>
                           <given-names>DANIEL JORDAN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>283</fpage>
                  <abstract>
                     <p>Nigeria has a widespread reputation as a corrupt country. Some years ago it appeared at the top of Transparency International’s list of the most corrupt countries, and it continues to be regarded as a bastion of fraud, graft, and deceit. This image of Nigeria coincides with an era in which corruption is a common explanation for political and economic disappointments in Africa and in much of the developing world. In the language of cause and effect, corruption is often portrayed as an independent variable inhibiting the desired and supposedly dependent outcomes of democracy and development. The view that corruption is</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.15</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>Corruption and Human Rights:</title>
                     <subtitle>Exploring the Connection</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KOECHLIN</surname>
                           <given-names>LUCY</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>CARMONA</surname>
                           <given-names>MAGDALENA SEPÚLVEDA</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>310</fpage>
                  <abstract>
                     <p>The first paragraph of the Preamble to the United Nations Convention against Corruption (UNCAC) declares the state parties’ concern “about the seriousness of problems and threats posed by corruption to the stability and security of societies, undermining the institutions and values of democracy, ethical values and justice and jeopardizing sustainable development and the rule of law.”¹ Similarly, the Preamble to the Council of Europe Criminal Law Convention on Corruption emphasizes that “corruption threatens the rule of law, democracy and human rights, undermines good governance, fairness and social justice, distorts competition, hinders economic development and endangers the stability of democratic institutions</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.16</book-part-id>
                  <title-group>
                     <label>13</label>
                     <title>Leadership Alters Corrupt Behavior</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ROTBERG</surname>
                           <given-names>ROBERT I.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>341</fpage>
                  <abstract>
                     <p>The policeman who “charges” drivers for non-existent infractions, the customs official who “under-invoices” a shipment of tractor parts and splits the difference with an importer, the office-bound bureaucrat who gives permits for foreign currency to favored businessmen for “a consideration,” and the high-ranking cabinet minister who prefers one supplier of fighter aircraft or naval frigates over another in exchange for serious “rents” are each behaving rationally by cheating their governments and the citizens that they are pledged to serve. They are all corrupt and corrupted, certainly, but by adopting a conscious strategy of self-enrichment through corrupt behavior they are merely</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.17</book-part-id>
                  <title-group>
                     <label>14</label>
                     <title>The Role of the Multi-National Corporation in the Long War against Corruption</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HEINEMAN,</surname>
                           <given-names>BEN W.</given-names>
                           <suffix>JR.</suffix>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>359</fpage>
                  <abstract>
                     <p>Multi-national corporations (MNCs) play a significant role in economic globalization. The annual revenues of the largest companies—such as Exxon-Mobil’s $405 billion in 2007—put them among the largest twenty-five nations if these total revenues were equated to GDP. The top 200 MNCs account for over 50 percent of the world’s industrial output.¹ MNCs undertake a wide variety of commercial activities: they export from their home country and manufacture and assemble in foreign countries. They have moved “off-shore” activities that were previously conducted in their home countries.² They have then “outsourced” to third parties some of those “off-shored” activities, creating</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.18</book-part-id>
                  <title-group>
                     <label>15</label>
                     <title>The Organization of Anti-Corruption:</title>
                     <subtitle>Getting Incentives Right</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LAMBSDORFF</surname>
                           <given-names>JOHANN GRAF</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>389</fpage>
                  <abstract>
                     <p>Corruption poses similar problems to both governments and private firms. Contracts are awarded to those paying bribes rather than delivering quality. Public policies as well as corporate strategies are distorted by side payments and resources are embezzled for private use. Due to these similarities, most of the ideas developed herein are equally applicable to public and private organizations.</p>
                     <p>Anti-corruption approaches can either relate to rules or to principles; they can be either top-down or bottom-up. This chapter reviews a variety of widely discussed approaches to anti-corruption and highlights some of the shortcomings of rules-based, top-down anti-corruption. The chapter makes the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.19</book-part-id>
                  <title-group>
                     <label>16</label>
                     <title>A Coalition to Combat Corruption:</title>
                     <subtitle>TI, EITI, and Civil Society</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>EIGEN</surname>
                           <given-names>PETER</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>416</fpage>
                  <abstract>
                     <p>Globalization is bringing new actors to the fore: civil society organizations (CSOs) and the private sector take on an increasing role in shaping global governance together with the public sector. Governance traditionally has been associated with the institutions of the state. However, in a globalized economy, this concept is out of sync with reality. Political and economic problems such as corruption are increasingly affecting more than one state. Solutions can no longer be sought solely on the national level and only by state actors.</p>
                     <p>As traditional governing institutions are losing influence, a governance void arises. However, this chapter argues that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.20</book-part-id>
                  <title-group>
                     <label>17</label>
                     <title>Reducing Corruption in the Health and Education Sectors</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>GRIFFIN</surname>
                           <given-names>CHARLES C.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>430</fpage>
                  <abstract>
                     <p>Health and education consume a significant fraction, usually half or more, of central government expenditures in low-and middle-income countries. Along with infrastructure, these two sectors dominate the activities of most national governments and, if public subsidies are effective and well targeted, are key investments to improve the capabilities and quality of life of the poor. Foreign assistance is concentrated in health and education, although it is typically not well coordinated either across donors or with domestic government spending, and many different channels and systems are used for disbursing the funds. A principal argument that donors use to defend why they</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.21</book-part-id>
                  <title-group>
                     <label>18</label>
                     <title>Good Governance, Anti-Corruption, and Economic Development</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>SUNDARAM</surname>
                           <given-names>JOMO KWAME</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>457</fpage>
                  <abstract>
                     <p>Corruption adversely affects development in many different ways, especially by diverting resources that may be invested productively and by causing uncertainty for investors. Efficient markets are defined as those with low transaction costs, particularly the costs of negotiating and enforcing contracts. If these costs are high, markets are inefficient, and transactions, including investments, become less likely.</p>
                     <p>The economic argument for the good governance agenda can be summarized as follows. Poor economic development performance is blamed on inefficient, high-transaction-cost markets. Inefficient markets are blamed on welfare-reducing government interventions, especially those that cause insecure property rights with uncertain, potentially high-cost implications. Unstable</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.22</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>469</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.23</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>477</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wpgj4.24</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>499</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
