<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt140d775</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt14btctd</book-id>
      <subj-group>
         <subject content-type="call-number">JQ2941.A91G55 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Malawi</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Dance</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Malawi</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Women</subject>
         <subj-group>
            <subject content-type="lcsh">Malawi</subject>
            <subj-group>
               <subject content-type="lcsh">Social conditions</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Music</subject>
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Dance of Politics</book-title>
         <subtitle>Gender, Performance, and Democratization in Malawi</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Gilman</surname>
               <given-names>Lisa</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>28</day>
         <month>05</month>
         <year>2009</year>
      </pub-date>
      <isbn content-type="ppub">9781592139859</isbn>
      <isbn content-type="epub">9781592139873</isbn>
      <publisher>
         <publisher-name>Temple University Press</publisher-name>
         <publisher-loc>Philadelphia</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>Temple University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt14btctd"/>
      <abstract abstract-type="short">
         <p>Election campaigns, political events, and national celebration days in Malawi usually feature groups of women who dance and perform songs of praise for politicians and political parties. These lively performances help to attract and energize throngs of prospective voters. However, as Lisa Gilman explains, "praise performing" is one of the only ways that women are allowed to participate in a male-dominated political system.</p>
         <p>Although political performances by women are not unique to Malawi, the case in Malawi is complicated by the fact that until 1994<italic>all</italic>Malawianwomen were<italic>required</italic>to perform on behalf of the long-reigning political party and its self-declared "President for Life," Dr. Hastings Kamuzu Banda+. This is the first book to examine the present-day situation, where issues of gender, economics and politics collide in surprising ways. Along with its solid grounding in the relevant literature,<italic>The Dance of Politics</italic>draws strength from Gilman's first-hand observations and her interviews with a range of participants in the political process, from dancers to politicians.</p>
      </abstract>
      <counts>
         <page-count count="268"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.4</book-part-id>
                  <title-group>
                     <title>[Illustrations]</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Introduction:</title>
                     <subtitle>Gender, Power, and Performance</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>In April 1999, I was in Malawi researching women’s political dancing during the campaigns leading to the country’s second multiparty elections. I had already spent several months attending political rallies in the country’s northern region when Kaliyoma Phumisa—at the time a high-ranking member of the ruling United Democratic Front (UDF) party, a member of Parliament, and a cabinet minister—and his wife, Jane Phumisa, generously invited me to join them as they rigorously campaigned in Phumisa’s constituency in the Ntcheu District in central Malawi. On April 23, the Phumisas, several UDF party officials from the Ntcheu District, and I</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Dance and Nationalism in the Independence Movement</title>
                  </title-group>
                  <fpage>26</fpage>
                  <abstract>
                     <p>Mayi Mphase remembered first dancing politically during Malawi’s movement for independence in her home district of Nkhata Bay. She later became one of the most prominent dance leaders in the district during Banda’s rule and was still an avid MCP supporter when I met her in 1999. I first encountered Mayi Mphase on a video documenting political dancing during Banda’s rule that I had purchased in the nearby city of Mzuzu. As my research assistant Simeon Nyirenda and I watched footage from a rally held during the referendum in the town of Nkhata Bay, he pointed out two of the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Dance and Social Control During Banda’s Presidency</title>
                  </title-group>
                  <fpage>43</fpage>
                  <abstract>
                     <p>In February 1999, during the height of the chilimika dance season, Mayi Ma. Longwe, an avid chilimika dancer, invited me to come to her home village, Nsanje, to meet and watch her team rehearse. Nsanje is relatively isolated, accessible from the nearest town of Nkhata Bay only by boat or a several-mile trek up and down hillsides.¹ We traveled with about thirty other passengers on a small wooden motorized taxi boat that bumped its way north, hugging the lakeshore as it dropped people off at fishing villages along the way. After about two hours, we arrived in Nsanje. Later that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>Dance, the Transition to Multipartyism, and Patronage</title>
                  </title-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>Waves of change spread across sub-Saharan Africa in the early 1990s, causing more than half of all African states to replace authoritarian regimes with multiparty systems of government (Lindberg 2006: 52). The end of the cold war precipitated changes in the global political climate, and in the early 1990s, the United States and European powers that had supported the pro-Western Banda during the cold war changed their tone. Donor countries and the World Bank refused Malawi economic assistance until “human rights were respected and a political liberalization agenda was announced” (Ihonvbere 1997: 227). Malawi’s economy, already weak was further weakened</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Power and Performance in Political Rallies</title>
                  </title-group>
                  <fpage>119</fpage>
                  <abstract>
                     <p>My focus now narrows to a single high-profile rally to illustrate how these performance events both serve the interests of political power wielders and yield opportunities for negotiations of power. This event, the launch of Muluzi’s campaign for reelection in the northern region, which took place on April 17, 1999 in the city of Mzuzu, was the most elaborate of any campaign rally I attended. It was especially interesting because the focal person was the country’s president, and the event took place in the northern region, many of whose residents felt that Muluzi had largely ignored their needs. UDF officials</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Why Do Women Dance?</title>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>Justin Malewezi served as Malawi’s vice president from 1994 to 2004 under Bakili Muluzi and then ran for president as an independent candidate in 2004. I was introduced to him by a friend who had worked on his 2004 campaign, and he agreed to a hurried interview on June 1, 2004, shortly after he lost the election. I asked him what contribution he thought women dancers made politically. He answered, “In my own experience,…in UDF, in PPM [People’s Progressive Movement], and as an independent candidate, we found that women were extremely important for a successful rally. At meetings where there</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Gendering Democracy</title>
                  </title-group>
                  <fpage>184</fpage>
                  <abstract>
                     <p>I returned to Blantyre on May 13, 2004, and was immediately immersed in the buzz surrounding the third multiparty elections, scheduled for May 20. As my husband, toddler daughter, and I traveled from the airport to our hosts’ house, the taxi driver talked of nothing else. Women in yellow were everywhere, and headlines about women politicians and election irregularities shouted from newspapers sold by vendors across the city. On this trip, I was interested in meeting with women political performers to trace how their roles were continuing to emerge and to discuss their perspectives on the 2004 campaigns. I also</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Gender at the Intersection of Politics, Democratization, and Tradition</title>
                  </title-group>
                  <fpage>205</fpage>
                  <abstract>
                     <p>The positioning of women’s singing and dancing within the political arena is complicated and demands a nuanced analysis that recognizes its important value at the same time that it considers the practice in relationship to the gender, political, and economic inequities operating in Malawi. At the heart of the phenomenon of political dancing in Malawi—whether one’s gaze is directed toward situated performances, the emergence of the practice and evolution of the form, the stages of production, or the discourse that surrounds it—is the idea of tradition. The practice exists because people in the region have long used their</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.13</book-part-id>
                  <title-group>
                     <title>Appendix A:</title>
                     <subtitle>Brief Timeline of Malawi’s Recent Political History</subtitle>
                  </title-group>
                  <fpage>223</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.14</book-part-id>
                  <title-group>
                     <title>Appendix B:</title>
                     <subtitle>People Interviewed</subtitle>
                  </title-group>
                  <fpage>225</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.15</book-part-id>
                  <title-group>
                     <title>Appendix C:</title>
                     <subtitle>Political Functions Attended and Referenced</subtitle>
                  </title-group>
                  <fpage>229</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.16</book-part-id>
                  <title-group>
                     <title>Appendix D:</title>
                     <subtitle>Associated Multimedia Websites</subtitle>
                  </title-group>
                  <fpage>231</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.17</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>233</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>249</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt14btctd.19</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>253</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
