<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3592210</article-id>
         <title-group>
            <article-title>Spread of Cattle Led to the Loss of Matrilineal Descent in Africa: A Coevolutionary Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Clare Janaki</given-names>
                  <surname>Holden</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ruth</given-names>
                  <surname>Mace</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>07</day>
            <month>12</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">270</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1532</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i369661</issue-id>
         <fpage>2425</fpage>
         <lpage>2433</lpage>
         <page-range>2425-2433</page-range>
         <permissions>
            <copyright-statement>Copyright 2003 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3592210"/>
         <abstract>
            <p>Matrilineal descent is rare in human societies that keep large livestock. However, this negative correlation does not provide reliable evidence that livestock and descent rules are functionally related, because human cultures are not statistically independent owing to their historical relationships (Galton's problem). We tested the hypothesis that when matrilineal cultures acquire cattle they become patrilineal using a sample of 68 Bantu- and Bantoid-speaking populations from sub-Saharan Africa. We used a phylogenetic comparative method to control for Galton's problem, and a maximum-parsimony Bantu language tree as a model of population history. We tested for coevolution between cattle and descent. We also tested the direction of cultural evolution-were cattle acquired before matriliny was lost? The results support the hypothesis that acquiring cattle led formerly matrilineal Bantu-speaking cultures to change to patrilineal or mixed descent. We discuss possible reasons for matriliny's association with horticulture and its rarity in pastoralist societies. We outline the daughter-biased parental investment hypothesis for matriliny, which is supported by data on sex, wealth and reproductive success from two African societies, the matrilineal Chewa in Malawi and the patrilineal Gabbra in Kenya.</p>
         </abstract>
         <kwd-group>
            <kwd>Matrilineal</kwd>
            <kwd>Pastoralism</kwd>
            <kwd>Cultural Evolution</kwd>
            <kwd>Coevolution</kwd>
            <kwd>Phylogeny</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d897e196a1310">
            <mixed-citation id="d897e200" publication-type="book">
Aberle, D. F. 1961 Matrilineal descent in cross-cultural com-
parison. In Matrilineal kinship (ed. D. Schneider &amp;
K. Gough), pp. 655-730. Berkeley, CA: University of Cali-
fornia Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Aberle</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Matrilineal descent in cross-cultural comparison</comment>
               <fpage>655</fpage>
               <source>Matrilineal kinship</source>
               <year>1961</year>
            </mixed-citation>
         </ref>
         <ref id="d897e238a1310">
            <mixed-citation id="d897e242" publication-type="journal">
Bastin, Y., Coupez, A. &amp; Mann, M. 1999 Continuity and
divergence in the Bantu languages: perspectives from a lex-
icostatic study. Annls Sci. Hum. 162, 1-226.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Bastin</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>162</volume>
               <source>Annls Sci. Hum.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d897e277a1310">
            <mixed-citation id="d897e281" publication-type="journal">
Bateman, R., Goddard, I., O'Grady, R., Funk, V., Mooi, R.,
Kress, W. &amp; Cannell, P. 1990 Speaking of forked tongues;
the feasibility of reconciling human phylogeny and the his-
tory of language. Curr. Anthropol. 31, 1-13.<object-id pub-id-type="jstor">10.2307/2743338</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e304a1310">
            <mixed-citation id="d897e308" publication-type="journal">
Bellwood, P. 2001 Early agriculturalist population diasporas?
Farming, languages, and genes. A. Rev. Anthropol. 30,
181-207.<object-id pub-id-type="jstor">10.2307/3069214</object-id>
               <fpage>181</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e328a1310">
            <mixed-citation id="d897e332" publication-type="book">
Blust, R. 2000 Why lexicostatistics doesn't work: the 'universal
constant' hypothesis and the Austronesian languages. In
Time depth in historical linguistics, vol. 2 (ed. C. Renfrew, A.
McMahon &amp; L. Trask), pp. 311-331. Cambridge, UK: The
McDonald Institute for Archaeological Research.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Blust</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Why lexicostatistics doesn't work: the 'universal constant' hypothesis and the Austronesian languages</comment>
               <fpage>311</fpage>
               <volume>2</volume>
               <source>Time depth in historical linguistics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e376a1310">
            <mixed-citation id="d897e380" publication-type="book">
Cavalli-Sforza, L. L. &amp; Feldman, M. W. 1981 Cultural trans-
mission and evolution: a quantitative approach. Monographs in
population biology. Princeton, NJ: Princeton University
Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cavalli-Sforza</surname>
                  </string-name>
               </person-group>
               <source>Cultural transmission and evolution: a quantitative approach. Monographs in population biology</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d897e412a1310">
            <mixed-citation id="d897e416" publication-type="journal">
Cavalli-Sforza, L. L., Minch, E. &amp; Mountain, J. 1988 Recon-
struction of human evolution: bringing together the genetic,
archaeological and linguistic data. Proc. Natl Acad. Sci. USA
85, 6002-6006.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Cavalli-Sforza</surname>
                  </string-name>
               </person-group>
               <fpage>6002</fpage>
               <volume>85</volume>
               <source>Proc. Natl Acad. Sci. USA</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d897e454a1310">
            <mixed-citation id="d897e458" publication-type="book">
Ehret, C. 1998 An African classical age: eastern and southern
Africa in world history, 1000 B. C. to A.D. 400. Charlottesville,
VA: University Press of Virginia.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ehret</surname>
                  </string-name>
               </person-group>
               <source>An African classical age: eastern and southern Africa in world history, 1000 B. C. to A.D. 400</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d897e487a1310">
            <mixed-citation id="d897e491" publication-type="journal">
Felsenstein, J. 1985 Phylogenies and the comparative method.
Am. Nat. 125, 1-15.<object-id pub-id-type="jstor">10.2307/2461605</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e507a1310">
            <mixed-citation id="d897e511" publication-type="book">
Flinn, M. 1981 Uterine vs. agnatic kinship variability and asso-
ciated cross-cousin marriage preferences: an evolutionary
biological analysis. In Natural selection and social behavior (ed.
R. D. Alexander &amp; D. W. Tinkle), pp. 439-475. New York:
Chiron Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Flinn</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Uterine vs. agnatic kinship variability and associated cross-cousin marriage preferences: an evolutionary biological analysis</comment>
               <fpage>439</fpage>
               <source>Natural selection and social behavior</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d897e553a1310">
            <mixed-citation id="d897e557" publication-type="journal">
Gray, R. &amp; Jordan, F. 2000 Language trees support the
express-train sequence of Austronesian expansion. Nature
405, 1052-1055.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Gray</surname>
                  </string-name>
               </person-group>
               <fpage>1052</fpage>
               <volume>405</volume>
               <source>Nature</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e592a1310">
            <mixed-citation id="d897e596" publication-type="journal">
Guglielmino, C., Viganotti, C., Hewlett, B. &amp; Cavalli-Sforza,
L. 1995 Cultural adaptation in Africa: role of mechanisms
of transmission and adaptation. Proc. Natl Acad. Sci. USA
92, 7585-7589.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Guglielmino</surname>
                  </string-name>
               </person-group>
               <fpage>7585</fpage>
               <volume>92</volume>
               <source>Proc. Natl Acad. Sci. USA</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d897e634a1310">
            <mixed-citation id="d897e638" publication-type="journal">
Hartung, J. 1982 Polygyny and the inheritance of wealth. Curr.
Anthropol. 23, 1-12.<object-id pub-id-type="jstor">10.2307/2742548</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e654a1310">
            <mixed-citation id="d897e658" publication-type="book">
Harvey, P. H. &amp; Pagel, M. D. 1991 The comparative method in
evolutionary biology. New York: Oxford University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Harvey</surname>
                  </string-name>
               </person-group>
               <source>The comparative method in evolutionary biology</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d897e683a1310">
            <mixed-citation id="d897e687" publication-type="journal">
Hewlett, B. S. &amp; Cavalli-Sforza, L. L. 1986 Cultural trans-
mission among the Aka pygmies. Am. Anthropol. 88, 922-
934.<object-id pub-id-type="jstor">10.2307/679081</object-id>
               <fpage>922</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e706a1310">
            <mixed-citation id="d897e710" publication-type="journal">
Hewlett, B. S., De Silvestri, A. &amp; Guglielmino, C. R. 2002
Semes and genes in Africa. Curr. Anthropol. 43, 313-321.<object-id pub-id-type="jstor">10.2307/3596591</object-id>
               <fpage>313</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e727a1310">
            <mixed-citation id="d897e731" publication-type="journal">
Holden, C. J. 2002 Bantu language trees reflect the spread of
farming across sub-Saharan Africa: a maximum-parsimony
analysis. Proc. R. Soc. Lond. B 269, 793-799. (DOI 10.1098/
rspb.2002.1955.)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Holden</surname>
                  </string-name>
               </person-group>
               <fpage>793</fpage>
               <volume>269</volume>
               <source>Proc. R. Soc. Lond.</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d897e769a1310">
            <mixed-citation id="d897e773" publication-type="book">
Holden, C. J. 2003 Evolution of Bantu languages. In McGraw-
Hill yearbook of science and technology 2003 (ed. McGraw-
Hill), pp. 121-124. New York: McGraw-Hill.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Holden</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Evolution of Bantu languages</comment>
               <fpage>121</fpage>
               <source>McGraw-Hill yearbook of science and technology 2003</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d897e808a1310">
            <mixed-citation id="d897e812" publication-type="journal">
Holden, C. &amp; Mace, R. 1997 Phylogenetic analysis of the evol-
ution of lactose digestion in adults. Hum. Biol. 69, 605-628.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Holden</surname>
                  </string-name>
               </person-group>
               <fpage>605</fpage>
               <volume>69</volume>
               <source>Hum. Biol.</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d897e844a1310">
            <mixed-citation id="d897e848" publication-type="journal">
Holden, C. &amp; Mace, R. 1999 Sexual dimorphism in stature
and women's work: a phylogenetic cross-cultural analysis.
Am. J. Phys. Anthropol. 110, 27-45.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Holden</surname>
                  </string-name>
               </person-group>
               <fpage>27</fpage>
               <volume>110</volume>
               <source>Am. J. Phys. Anthropol.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d897e883a1310">
            <mixed-citation id="d897e887" publication-type="book">
Holden, C. &amp; Mace, R. 2002 Pastoralism and the evolution of
lactase persistence. In Human biology of pastoral populations
(ed. W. R. Leonard &amp; M. H. Crawford), pp. 280-307. Cam-
bridge University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Holden</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Pastoralism and the evolution of lactase persistence</comment>
               <fpage>280</fpage>
               <source>Human biology of pastoral populations</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d897e925a1310">
            <mixed-citation id="d897e929" publication-type="journal">
Holden, C. J., Mace, R. &amp; Sear, R. 2003 Matriliny as
daughter-biased investment. Evol. Hum. Behav. 24, 99-112.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Holden</surname>
                  </string-name>
               </person-group>
               <fpage>99</fpage>
               <volume>24</volume>
               <source>Evol. Hum. Behav.</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d897e962a1310">
            <mixed-citation id="d897e966" publication-type="journal">
Lancaster, C. S. 1976 Women, horticulture, and society in
sub-Saharan Africa. Am. Anthropol. 78, 539-564.<object-id pub-id-type="jstor">10.2307/674416</object-id>
               <fpage>539</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e982a1310">
            <mixed-citation id="d897e986" publication-type="journal">
Mace, R. 1996 Biased parental investment and reproductive
success in Gabbra pastoralists. Behav. Ecol. Sociobiol. 38,
75-81.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mace</surname>
                  </string-name>
               </person-group>
               <fpage>75</fpage>
               <volume>38</volume>
               <source>Behav. Ecol. Sociobiol.</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1021a1310">
            <mixed-citation id="d897e1025" publication-type="journal">
Mace, R. &amp; Pagel, M. 1994 The comparative method in
anthropology. Curr. Anthropol. 35, 549-564.<object-id pub-id-type="jstor">10.2307/2744082</object-id>
               <fpage>549</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e1041a1310">
            <mixed-citation id="d897e1045" publication-type="book">
Mace, R. &amp; Pagel, M. 1997 Tips, branches and nodes. In
Human nature: a critical reader (ed. L. Betzig), pp. 297-310.
New York: Oxford University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mace</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Tips, branches and nodes</comment>
               <fpage>297</fpage>
               <source>Human nature: a critical reader</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1080a1310">
            <mixed-citation id="d897e1084" publication-type="book">
Marshall, F. 2000 The origins and spread of domestic animals
in East Africa. In The origins and development of African live-
stock: archaeology, genetics, linguistics and ethnography (ed. R.
M. Blench &amp; K. C. MacDonald), pp. 191-221. London:
UCL Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Marshall</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The origins and spread of domestic animals in East Africa</comment>
               <fpage>191</fpage>
               <source>The origins and development of African livestock: archaeology, genetics, linguistics and ethnography</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1125a1310">
            <mixed-citation id="d897e1129" publication-type="book">
Mitchell, P. 2002 The archaeology of southern Africa. Cambridge
World Archaeology series. Cambridge University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Mitchell</surname>
                  </string-name>
               </person-group>
               <source>The archaeology of southern Africa</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1155a1310">
            <mixed-citation id="d897e1159" publication-type="journal">
Moore, J. H. 1994 Putting anthropology back together again:
an ethnographic critique of cladistic theory. Am. Anthropol.
96, 925-948.<object-id pub-id-type="jstor">10.2307/682452</object-id>
               <fpage>925</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e1178a1310">
            <mixed-citation id="d897e1182" publication-type="book">
Murdock, G. P. 1967 Ethnographic atlas. University of Pitts-
burgh Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Murdock</surname>
                  </string-name>
               </person-group>
               <source>Ethnographic atlas</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1207a1310">
            <mixed-citation id="d897e1211" publication-type="journal">
Murdock, G. P. &amp; White, D. R. 1969 Standard cross-cultural
sample. Ethnology 8, 329-369.<object-id pub-id-type="doi">10.2307/3772907</object-id>
               <fpage>329</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e1227a1310">
            <mixed-citation id="d897e1231" publication-type="journal">
Oliver, R. 1982 The Nilotic contribution to Bantu Africa.
J. Afr. Hist. 23, 433-442.<object-id pub-id-type="jstor">10.2307/182034</object-id>
               <fpage>433</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e1247a1310">
            <mixed-citation id="d897e1251" publication-type="journal">
Orians, G. 1969 On the evolution of mating systems in birds
and mammals. Am. Nat. 103, 589-603.<object-id pub-id-type="jstor">10.2307/2459035</object-id>
               <fpage>589</fpage>
            </mixed-citation>
         </ref>
         <ref id="d897e1267a1310">
            <mixed-citation id="d897e1271" publication-type="journal">
Pagel, M. 1994 Detecting correlated evolution on phylogenies:
a general method for the comparative analysis of discrete
characters. Proc. R. Soc. Lond. B 255, 37-45.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pagel</surname>
                  </string-name>
               </person-group>
               <fpage>37</fpage>
               <volume>255</volume>
               <source>Proc. R. Soc. Lond.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1307a1310">
            <mixed-citation id="d897e1313" publication-type="book">
Pagel, M. 2000a The history, rate and pattern of world linguis-
tic evolution. In The evolutionary emergence of language: social
function and the origins of linguistic form (ed. C. Knight,
M. Studdert-Kennedy &amp; J. R. Hurford), pp. 391-416. Cam-
bridge University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pagel</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The history, rate and pattern of world linguistic evolution</comment>
               <fpage>391</fpage>
               <source>The evolutionary emergence of language: social function and the origins of linguistic form</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1354a1310">
            <mixed-citation id="d897e1358" publication-type="book">
Pagel, M. 2000b Maximum-likelihood models for glottochron-
ology and for constructing linguistic phylogenies. In Time
depth in historical linguistics, vol. 1 (ed. C. Renfrew, A.
McMahon &amp; L. Trask), pp. 189-207. Cambridge, UK: The
McDonald Institute for Archaeological Research.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pagel</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Maximum-likelihood models for glottochronology and for constructing linguistic phylogenies</comment>
               <fpage>189</fpage>
               <volume>1</volume>
               <source>Time depth in historical linguistics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1402a1310">
            <mixed-citation id="d897e1406" publication-type="book">
Phillipson, D. W. 1993 African archaeology. Cambridge World
Archaeology. Cambridge University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Phillipson</surname>
                  </string-name>
               </person-group>
               <source>African archaeology. Cambridge World Archaeology</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1431a1310">
            <mixed-citation id="d897e1435" publication-type="book">
Ruhlen, M. 1991 A guide to the world's languages: volume 1:
classification. London: Edward Arnold.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ruhlen</surname>
                  </string-name>
               </person-group>
               <volume>1</volume>
               <source>A guide to the world's languages</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1464a1310">
            <mixed-citation id="d897e1468" publication-type="book">
Schneider, D. M. &amp; Gough, K. 1961 Matrilineal kinship.
Berkeley, CA: University of California Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schneider</surname>
                  </string-name>
               </person-group>
               <source>Matrilineal kinship</source>
               <year>1961</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1493a1310">
            <mixed-citation id="d897e1497" publication-type="book">
Smith, A. B. 2000 The origins of the domesticated animals of
southern Africa. In The origins and development of African live-
stock: archaeology, genetics, linguistics and ethnography (ed.
R. M. Blench &amp; K. C. MacDonald), pp. 222-238. London:
UCL Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Smith</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The origins of the domesticated animals of southern Africa</comment>
               <fpage>222</fpage>
               <source>The origins and development of African livestock: archaeology, genetics, linguistics and ethnography</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1539a1310">
            <mixed-citation id="d897e1543" publication-type="book">
Swadesh, M. 1971 What is glottochronology? In The origin and
diversification of language (ed. J. Sherzer), pp. 271-284.
Chicago, IL: Aldine.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swadesh</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">What is glottochronology?</comment>
               <fpage>271</fpage>
               <source>The origin and diversification of language</source>
               <year>1971</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1578a1310">
            <mixed-citation id="d897e1582" publication-type="book">
Swofford, D. L. 1998 PAUP*. Phylogenetic analysis using parsi-
mony (* and other methods). Sunderland, MA: Sinauer
Associates.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Swofford</surname>
                  </string-name>
               </person-group>
               <source>Phylogenetic analysis using parsimony</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1611a1310">
            <mixed-citation id="d897e1615" publication-type="book">
Van Neer, W. 2000 Domestic animals from archaeological sites
in Central and west-central Africa. In The origins and develop-
ment of African livestock: archaeology, genetics, linguistics and
ethnography (ed. R. M. Blench &amp; K. C. MacDonald), pp.
163-190. London: UCL Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Van Neer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Domestic animals from archaeological sites in Central and west-central Africa</comment>
               <fpage>163</fpage>
               <source>The origins and development of African livestock: archaeology, genetics, linguistics and ethnography</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d897e1656a1310">
            <mixed-citation id="d897e1660" publication-type="book">
Vansina, J. 1990 Paths in the rainforests: toward a history of the
political tradition in equatorial Africa. London: James Currey.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Vansina</surname>
                  </string-name>
               </person-group>
               <source>Paths in the rainforests: toward a history of the political tradition in equatorial Africa</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
