<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">worlbankeconrevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101235</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The World Bank Economic Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">02586770</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1564698X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41679566</article-id>
         <title-group>
            <article-title>Is There a Metropolitan Bias? The relationship between poverty and city size in a selection of developing countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Céline</given-names>
                  <surname>Ferré</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Francisco H.G.</given-names>
                  <surname>Ferreira</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter</given-names>
                  <surname>Lanjouw</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">26</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40078859</issue-id>
         <fpage>351</fpage>
         <lpage>382</lpage>
         <permissions>
            <copyright-statement>COPYRIGHT © 2012 The International Bank for Reconstruction and Development/THE WORLD BANK</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/wber/lhs007"
                   xlink:title="an external site"/>
         <abstract>
            <p>This paper provides evidence from eight developing countries of an inverse relationship between poverty and city size. Poverty is both more widespread and deeper in very small and small towns than in large or very large cities. This basic pattern is generally robust to the choice of poverty line. The paper shows, further, that for all eight countries, a majority of the urban poor live in medium, small or very small towns. Moreover, it is shown that the greater incidence and severity of consumption poverty in smaller towns is generally compounded by similarly greater deprivation in terms of access to basic infrastructure services, such as electricity, heating gas, sewerage and solid waste disposal. We illustrate for one country – Morocco – that inequality within large cities is not driven by a severe dichotomy between slum dwellers and others. Robustness checks are performed to assess whether the findings in the paper hinge on a specific definition of "urban area"; are driven by differences in the cost of living across city-size categories; by reliance on an income-based concept of wellbeing; or by the application of small-area estimation techniques for estimating poverty rates at the town and city level.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1341e286a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1341e293" publication-type="other">
Bates (1981)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e300a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1341e307" publication-type="other">
Ravallion et al. (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e314a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1341e321" publication-type="other">
Ravallion et al. (2007).</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e328a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1341e335" publication-type="other">
Ferreira, Lanjouw and Neri (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e341" publication-type="other">
World
Bank (2011)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e352a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1341e359" publication-type="other">
Lanjouw and Lanjouw (2001),</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e366a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1341e373" publication-type="other">
INSTAT,
2004</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e382" publication-type="other">
IBGE (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e388" publication-type="other">
Brazil, Kenya Central Bureau of Statistics (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e395" publication-type="other">
Kenya,
López-Calva et al. (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e404" publication-type="other">
Plan (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e410" publication-type="other">
Healy and
Jitsuchon (2007)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e419" publication-type="other">
Thailand, Department of Census and Statistics (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e426a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1341e433" publication-type="other">
Foster, Greer and Thorbecke (1984)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e440a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1341e447" publication-type="other">
L'Oriental (49,000).</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e454a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1341e461" publication-type="other">
Thomas, 2008</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e468a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1341e475" publication-type="other">
Christiaensen, Demery
and Kuhl, 2011</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e484" publication-type="other">
Lanjouw and Murgai (2009)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e490" publication-type="other">
World Bank, 2011</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e498a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1341e505" publication-type="other">
Tarozzi and Deaton (2007)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e511" publication-type="other">
ELL (2002, 2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e517" publication-type="other">
Molina and Rao, 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1341e524" publication-type="other">
Elbers,
Lanjouw and Leite (2008)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e534a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1341e541" publication-type="other">
World Bank (2011)</mixed-citation>
            </p>
         </fn>
         <fn id="d1341e548a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1341e555" publication-type="other">
World Bank 2011</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1341e571a1310">
            <mixed-citation id="d1341e575" publication-type="other">
Bates, Robert H. 1981. Markets and States in Tropical Africa. Berkeley, C.A.: University of California
Press.</mixed-citation>
         </ref>
         <ref id="d1341e585a1310">
            <mixed-citation id="d1341e589" publication-type="other">
Chattopadhyay, A., and T.K. Roy. 2005. "Are urban poor doing better than their rural counterpart in
India? A study of fertility, family planning and health." Demography India 34(2):299-312.</mixed-citation>
         </ref>
         <ref id="d1341e599a1310">
            <mixed-citation id="d1341e603" publication-type="other">
Christiaensen, Luc, Lionel Demery, and Jesper Kuhl. 2011. "The (evolving) role of agriculture in
poverty reduction – An empirical perspective", Journal of Development Economics 96: 239-254.</mixed-citation>
         </ref>
         <ref id="d1341e613a1310">
            <mixed-citation id="d1341e617" publication-type="other">
Demombynes, Gabriel, and Berk Özler. 2005. "Crime and Local Inequality in South Africa." Journal of
Development Economics 76 (2):265-92.</mixed-citation>
         </ref>
         <ref id="d1341e628a1310">
            <mixed-citation id="d1341e632" publication-type="other">
Demombynes, Gabriel, Chris Elbers, Jean O. Lanjouw, and Peter Lanjouw (2007) "How Good a Map?
Putting Small Area Estimation to the Test" Policy Research Working Paper No. 4155, The World
Bank.</mixed-citation>
         </ref>
         <ref id="d1341e645a1310">
            <mixed-citation id="d1341e649" publication-type="other">
Department of Census and Statistics, Sri Lanka. 2005. A Poverty Map for Sri Lanka: Findings and
Lessons (Colombo: Department of Census and Statistics).</mixed-citation>
         </ref>
         <ref id="d1341e659a1310">
            <mixed-citation id="d1341e663" publication-type="other">
Elbers, Chris, Jean O. Lanjouw, and Peter Lanjouw. 2002. "Micro-Level Estimation of Welfare", Policy
Research Working Paper No. 2911, The World Bank.</mixed-citation>
         </ref>
         <ref id="d1341e673a1310">
            <mixed-citation id="d1341e677" publication-type="other">
---. 2003. "Micro-level Estimation of Poverty and Inequality", Econometrica, 71 (1), pp. 355-364.</mixed-citation>
         </ref>
         <ref id="d1341e684a1310">
            <mixed-citation id="d1341e688" publication-type="other">
Elbers, Chris, Peter Lanjouw, Johan Mistiaen, Berk Özler, and Kenneth Simler. 2004. "On the Unequal
Inequality of Poor Communities." World Bank Economic Review 18(3):401-21.</mixed-citation>
         </ref>
         <ref id="d1341e698a1310">
            <mixed-citation id="d1341e702" publication-type="other">
Elbers, Chris, Peter Lanjouw, and Phillippe Leite. 2008. 'Brazil within Brazil: Testing the Poverty
Mapping Methodology in Minas Gerais' World Bank Policy Research Working Paper No. 4513,
The World Bank.</mixed-citation>
         </ref>
         <ref id="d1341e716a1310">
            <mixed-citation id="d1341e720" publication-type="other">
Ferreira, Francisco, H.G., Peter Lanjouw, and Marcelo Neri. 2003. "A Robust Poverty Profile for Brazil
Using Multiple Data Sources", Revista Brasileira de Economia, 57 (1), pp. 59-92.</mixed-citation>
         </ref>
         <ref id="d1341e730a1310">
            <mixed-citation id="d1341e734" publication-type="other">
Foster, James, Joel Greer, and Erik Thorbecke. 1984. "A Class of Decomposable Poverty Measures",
Econometrica 52 (3): 761-766.</mixed-citation>
         </ref>
         <ref id="d1341e744a1310">
            <mixed-citation id="d1341e748" publication-type="other">
Fujii, Tomoko. Forthcoming. "Micro-level Estimation of Child Undernutrition Indicators in
Cambodia", World Bank Economic Review.</mixed-citation>
         </ref>
         <ref id="d1341e758a1310">
            <mixed-citation id="d1341e762" publication-type="other">
Gangopadhyay, S., P. Lanjouw, T. Vishwanath, and N. Yoshida 2010. Identifying Pockets of Poverty:
Insights from Poverty Mapping Experiments in Andhra Pradesh, Orissa and West Bengal, forthcom-
ing Indian Journal of Human Development.</mixed-citation>
         </ref>
         <ref id="d1341e775a1310">
            <mixed-citation id="d1341e779" publication-type="other">
Henderseon, J.V., Z Shalii, and A.J. Venables. 2001. "Geography and Development" Journal of
Economic Geography 1:81-105</mixed-citation>
         </ref>
         <ref id="d1341e789a1310">
            <mixed-citation id="d1341e793" publication-type="other">
Haut Commissariat au Plan, Maroc. 2005. Pauvreté, Développement Humain et Développement Social
au Maroc: Données Cartographiques et Statistiques d'après le Recensement Général de la Population
et de l'Habitat (Rabat: Haut Commissariat au Plan).</mixed-citation>
         </ref>
         <ref id="d1341e807a1310">
            <mixed-citation id="d1341e811" publication-type="other">
Healy, A., and S. Jitsuchon 2007. 'Finding the Poor in Thailand' Journal of Asian Economies 18(5),
739-759.</mixed-citation>
         </ref>
         <ref id="d1341e821a1310">
            <mixed-citation id="d1341e825" publication-type="other">
Instituto Brasileiro de Geografia e Estatística, IBGE. 2003. 'Mapa de Pobreza e Desigualdade
Municípios Brasileiros' DVD ROM available from IBGE at: http://www.ibge.gov.br/english/
presidencia/noticias/noticia_visualiza.php?id_noticia=1293&amp;id_pagina=1</mixed-citation>
         </ref>
         <ref id="d1341e838a1310">
            <mixed-citation id="d1341e842" publication-type="other">
INSTAT (2004). Poverty and Inequality Mapping in Albania. (Tirana, INSTAT).</mixed-citation>
         </ref>
         <ref id="d1341e849a1310">
            <mixed-citation id="d1341e853" publication-type="other">
Kapadia-Kundu, N., and T. Kanitkar. 2002. "Primary Healthcare in Urban Slums". Economic and
Politicai Weekly, December 21.</mixed-citation>
         </ref>
         <ref id="d1341e863a1310">
            <mixed-citation id="d1341e867" publication-type="other">
Kenya Central Bureau of Statistics. 2003. Geographic Dimensions of Well-Being in Kenya (Regal Press
Kenya, Nairobi).</mixed-citation>
         </ref>
         <ref id="d1341e877a1310">
            <mixed-citation id="d1341e881" publication-type="other">
Krugman, Paul. 1999. 'The Role of Geography in Development' International Regional Science Review,
22(2):142-61.</mixed-citation>
         </ref>
         <ref id="d1341e892a1310">
            <mixed-citation id="d1341e896" publication-type="other">
Lanjouw, Peter, and Jean O. Lanjouw. 2001. 'The Rural Non-Farm Sector: Issues and Evidence from
Developing Countries', Agricultural Economics 26 (2001) 1-23.</mixed-citation>
         </ref>
         <ref id="d1341e906a1310">
            <mixed-citation id="d1341e910" publication-type="other">
Lanjouw, Peter, and Rinku Murgai. 2009. 'Agricultural Wage Labour, Non-Farm Employment and
Poverty in Rural India', Agricultural Economics 40 (2009) 243-263.</mixed-citation>
         </ref>
         <ref id="d1341e920a1310">
            <mixed-citation id="d1341e924" publication-type="other">
Lanjouw, Peter, and Erika Rascón. 2010. 'Town Size and Malnutrition in Mexico', mimeo, DECRG,
the World Bank.</mixed-citation>
         </ref>
         <ref id="d1341e934a1310">
            <mixed-citation id="d1341e938" publication-type="other">
Lipton, Michael. 1977. Why Poor People Stay Poor: Urban Bias and World Development. London:
Temple Smith.</mixed-citation>
         </ref>
         <ref id="d1341e948a1310">
            <mixed-citation id="d1341e952" publication-type="other">
López-Calva, L., A. Melendez, E. Rascón, L. Rodríguez-Chammusy, and M. Székely (2005) 'Poniendo
al Ingreso de los Hogares en el Mapa de México' Working Paper EGAP-2005-04, Tecnológico de
Monterrey, Campus Ciudad de Mexico.</mixed-citation>
         </ref>
         <ref id="d1341e965a1310">
            <mixed-citation id="d1341e969" publication-type="other">
Luttmer, Erzo F.P. 2005. "Neighbors As Negatives: Relative Earnings And Well-Being," Quarterly
Journal of Economics, V120 (3,Aug), 963-1002.</mixed-citation>
         </ref>
         <ref id="d1341e980a1310">
            <mixed-citation id="d1341e984" publication-type="other">
Molina, I., and J.N.K. Rao (2010) 'Small Areas Estimation of Poverty Indicators' The Canadian Journal
of Statistics 38 (3): 369-385</mixed-citation>
         </ref>
         <ref id="d1341e994a1310">
            <mixed-citation id="d1341e998" publication-type="other">
Prebisch, Raul. 1950. "The Economic Development of Latin America and its Principal Problems",
CEPAL.</mixed-citation>
         </ref>
         <ref id="d1341e1008a1310">
            <mixed-citation id="d1341e1012" publication-type="other">
Ravallion, Martin, Shaohua Chen, and Prem Sangraula. 2007. "New Evidence on the Urbanization of
Global Poverty". Population and Development Review 33(4), 667-701.</mixed-citation>
         </ref>
         <ref id="d1341e1022a1310">
            <mixed-citation id="d1341e1026" publication-type="other">
Rascón, Erika. 2010. "Cracking Malnutrition: an Resolved Challenge for Mexico?: Micro-Level
Estimation of Child Malnutrition Indicators", mimeo, Development Economics Research Group, the
World Bank.</mixed-citation>
         </ref>
         <ref id="d1341e1039a1310">
            <mixed-citation id="d1341e1043" publication-type="other">
Rosenstein-Rodan, Paul. 1943. "Problems of Industrialization in Eastern and Southeastern Europe",
Economic Journal, 53, pp. 202-12.</mixed-citation>
         </ref>
         <ref id="d1341e1053a1310">
            <mixed-citation id="d1341e1057" publication-type="other">
Tarozzi, A., and A. Deaton 2009. 'Using Census and Survey Data to Estimate Poverty and Inequality for
Small Areas' Review of Economics and Statistics 91(4) 773-792.</mixed-citation>
         </ref>
         <ref id="d1341e1068a1310">
            <mixed-citation id="d1341e1072" publication-type="other">
Thomas, T. (2008) 'Is There Evidence of an Urban-Rural Spillover? An Empirical Analysis Using
Spatially Explicit Data from Three Countries', mimeo, Development Economics Research Group, the
World Bank.</mixed-citation>
         </ref>
         <ref id="d1341e1085a1310">
            <mixed-citation id="d1341e1089" publication-type="other">
World Bank (2007) Measuring Poverty Using Household Consumption Brazil Poverty Assessment,
Poverty Reduction and Economic Management Network, The World Bank.</mixed-citation>
         </ref>
         <ref id="d1341e1099a1310">
            <mixed-citation id="d1341e1103" publication-type="other">
--- (2009) World Development Report: Reshaping Economic Geography (Washington D.C.: World
Bank).</mixed-citation>
         </ref>
         <ref id="d1341e1113a1310">
            <mixed-citation id="d1341e1117" publication-type="other">
--- (2011) Perspectives on Poverty in India: Stylized Facts from Survey Data, India Poverty
Assessment, Poverty Reduction and Economic Management Network, the World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
