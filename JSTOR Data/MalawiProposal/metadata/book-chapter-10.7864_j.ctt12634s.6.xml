<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt12634s</book-id>
      <subj-group>
         <subject content-type="call-number">HC60.C333 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic assistance</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Technical assistance</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
         <subj-group>
            <subject content-type="lcsh">Planning</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Catalyzing Development</book-title>
         <subtitle>A New Vision for Aid</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>Kharas</surname>
               <given-names>Homi</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Makino</surname>
               <given-names>Koji</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Jung</surname>
               <given-names>Woojin</given-names>
            </name>
         </contrib>
         <role content-type="editor">EDITORS</role>
      </contrib-group>
      <pub-date>
         <day>21</day>
         <month>06</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="epub">9780815721345</isbn>
      <isbn content-type="epub">081572134X</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2011</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt12634s"/>
      <abstract abstract-type="short">
         <p>Some may dispute the effectiveness of aid. But few would disagree that aid delivered to the right source and in the right way can help poor and fragile countries develop. It can be a catalyst, but not a driver of development. Aid now operates in an arena with new players, such as middle-income countries, private philanthropists, and the business community; new challenges presented by fragile states, capacity development, and climate change; and new approaches, including transparency, scaling up, and South-South cooperation. The next High Level Forum on Aid Effectiveness must determine how to organize and deliver aid better in this environment.</p>
         <p>
            <italic>Catalyzing Development</italic>proposes ten actionable game-changers to meet these challenges based on in-depth, scholarly research. It advocates for these to be included in a Busan Global Development Compact in order to guide the work of development partners in a flexible and differentiated manner in the years ahead.</p>
         <p>Contributors: Kemal Dervis (Brookings Institution), Shunichiro Honda (JICA Research Institute), Akio Hosono (JICA Research Institute), Johannes F. Linn (Emerging Markets Forum and Brookings Institution), Ryutaro Murotani (JICA Research Institute), Jane Nelson (Harvard Kennedy School and Brookings Institution), Mai Ono (JICA Research Institute), Kang-ho Park (Ministry of Foreign Affairs and Trade, Korea), Tony Pipa (U.S. Agency for International Development), Sarah Puritz Milsom (Brookings Institution), Hyunjoo Rhee (Korea International Cooperation Agency), Mine Sato (JICA Research Institute), Shinichi Takeuchi (JICA Research Institute), Keiichi Tsunekawa (JICA Research Institute), Ngaire Woods (University College, Oxford), Sam Worthington (InterAction)</p>
      </abstract>
      <counts>
         <page-count count="305"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.4</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Overview:</title>
                     <subtitle>An Agenda for the Busan High-Level Forum on Aid Effectiveness</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KHARAS</surname>
                           <given-names>HOMI</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>MAKINO</surname>
                           <given-names>KOJI</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>JUNG</surname>
                           <given-names>WOOJIN</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Today’s world is shaped by growing economic integration alongside growing economic divergence. Over two dozen developing economies are expanding at rates that previously appeared miraculous, reducing poverty at unprecedented rates. Conversely, thirty-five developing countries with a combined population of 940 million can be classified as “fragile,” or at risk of suffering debilitating internal conflict.¹ The potential for globalization to act as a positive force for development contrasts with the prospects for globalization to threaten, or be unable to protect, development through a failure to deal with the challenges of hunger, poverty, disease, and climate change. Many developing countries have neither</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.5</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>New Development Partners and a Global Development Partnership</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>PARK</surname>
                           <given-names>KANG-HO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>38</fpage>
                  <abstract>
                     <p>Over the last few years, a number of new economic powers have become important players in the global aid system. Because of their growing economies and their increasing influence as regional and global players, they have gained much attention worldwide. Significantly, the volume of official development assistance (ODA) from these powers is rising. Some are more important players in aid today than smaller traditional donors. China, India, Saudi Arabia, Republic of Korea, Turkey, Brazil, and Venezuela, among others, have been increasing their outward flows of aid at a remarkable rate. While the OECD Development Assistance Committee (DAC) remains the core</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.6</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Private Development Assistance:</title>
                     <subtitle>The Importance of International NGOs and Foundations in a New Aid Architecture</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WORTHINGTON</surname>
                           <given-names>SAMUEL A.</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>PIPA</surname>
                           <given-names>TONY</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>61</fpage>
                  <abstract>
                     <p>Nonstate entities are increasingly providing significant resources for international development and in some countries and localities are dwarfing the presence of even the largest donor governments of the Development Assistance Committee of the Organization for Economic Cooperation and Development (OECD/DAC), the traditional source of official development assistance (ODA).¹ With their growing scale, scope, and nature, nongovernmental organizations (NGOs), local civil society organizations (CSOs), and other private groups active in development—together referred to as private development assistance (PDA)—are rapidly transforming the basic architecture of the international aid system.</p>
                     <p>The effectiveness framework developed through the Paris Declaration and the Accra</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.7</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>The Private Sector and Aid Effectiveness:</title>
                     <subtitle>Toward New Models of Engagement</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>NELSON</surname>
                           <given-names>JANE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>83</fpage>
                  <abstract>
                     <p>Private corporations, financial institutions, social enterprises, and business associations and coalitions are becoming significant players in international development, both individually and through a variety of collaborative mechanisms.¹ This trend has important implications for both the quantity and quality of resources being mobilized for development and for the aid effectiveness agenda. Ongoing efforts to improve aid effectiveness should place support for shared economic growth at their core and should call for a more coordinated approach to market development among donors, one that is focused on enabling vibrant and diversified domestic private sectors and on catalyzing more and better foreign private resources.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.8</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Rethinking Aid Coordination</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>WOODS</surname>
                           <given-names>NGAIRE</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>112</fpage>
                  <abstract>
                     <p>Should aid be better coordinated? And if so, how? The case for aid coordination is a powerful one. As aid poured into Haiti in the wake of a massive earthquake in January 2010, television coverage around the world broadcast two different realities. One story was about well-organized aid givers collecting record donations and dispatching food and medical equipment by the ton to Haiti. The other was a story about aid stymied by a lack of coordination. Television crews depicted Haitian families and children complaining that none of the food, medical assistance, or shelter was reaching them. The president of Haiti</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.9</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Capacity Traps and Legitimacy Traps:</title>
                     <subtitle>Development Assistance and State Building in Fragile Situations</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>TAKEUCHI</surname>
                           <given-names>SHINICHI</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>MUROTANI</surname>
                           <given-names>RYUTARO</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>TSUNEKAWA</surname>
                           <given-names>KEIICHI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>Dealing with fragility and fragile situations is one of today’s most critical international challenges, a challenge closely related to the problems of conflict prevention, poverty reduction, and global security. There have been many attempts at defining “fragility,” or “fragile states.”¹ In the present chapter we conceptualize fragility as a situation in which human security is under continuous threat, with armed conflict and chronic poverty as its most prominent features. We adopt this definition in part because we want to avoid punctilious debates over definitional matters and in part because conceptual simplicity highlights the true source of the problem: governments unwilling</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.10</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Development Aid and Global Public Goods:</title>
                     <subtitle>The Example of Climate Protection</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>DERVIŞ</surname>
                           <given-names>KEMAL</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>MILSOM</surname>
                           <given-names>SARAH PURITZ</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>155</fpage>
                  <abstract>
                     <p>Development aid has always had multiple purposes. The most openly proclaimed purpose has been to provide assistance to poor countries and vulnerable populations, with the objective of fighting extreme poverty, providing humanitarian relief, and supporting long-term development. Stated as such, this objective reflects a moral imperative and a redistributive intent: channeling resources from the more fortunate to the less fortunate, to help them survive and to help them grow. The target can be poor countries or specific poor and vulnerable populations within countries. Under this overall moral imperative, it is customary to distinguish purely humanitarian aid, such as that provided</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.11</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Inside the Black Box of Capacity Development</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>HOSONO</surname>
                           <given-names>AKIO</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>HONDA</surname>
                           <given-names>SHUNICHIRO</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib3" xlink:type="simple">
                        <name name-style="western">
                           <surname>SATO</surname>
                           <given-names>MINE</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib4" xlink:type="simple">
                        <name name-style="western">
                           <surname>ONO</surname>
                           <given-names>MAI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>179</fpage>
                  <abstract>
                     <p>In recent development discussions, capacity development (CD) has emerged as a central issue. The Accra Agenda for Action (AAA), adopted in 2008 at the third High-Level Forum on Aid Effectiveness, emphasizes CD even more strongly than does the Paris Declaration, which incorporates CD as a key crosscutting theme in aid effectiveness. The outcome document of the UN summit on its Millennium Development Goals (MDGs) in September 2010 repeatedly asserts the importance of capacity and CD; and the global initiative on management for development results identifies the development of statistical capacity as a top agenda item.</p>
                     <p>Underlying this trend is a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.12</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>Scaling Up with Aid:</title>
                     <subtitle>The Institutional Dimension</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>LINN</surname>
                           <given-names>JOHANNES F.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>202</fpage>
                  <abstract>
                     <p>Official aid flows have significantly increased over the last decade, from about $80 billion in 1997 to about $130 billion in 2008, but this level remains well short of the goal set by the G8 at the Gleneagles Summit.¹ At the same time, aid has become increasingly fragmented, as the number of aid agencies and actors has rapidly grown.² New bilateral donors have appeared on the scene, new official aid organizations have been set up in traditional donor countries, and the number of multilateral donors has skyrocketed.³ In addition, thousands of private aid donors—a few rivaling the largest official</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.13</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Transparency:</title>
                     <subtitle>Changing the Accountability, Engagement, and Effectiveness of Aid</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>KHARAS</surname>
                           <given-names>HOMI</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>233</fpage>
                  <abstract>
                     <p>Development cooperation, in an information age, is not only about whose projects and programs are most successful but also about whose stories are most compelling.¹ Transparency in aid is a critical part of promoting a story of successful development. Through transparency, donors and recipients can be held accountable for what they spend, more players can become actively engaged in development efforts by identifying underserved areas and niches, and aid can be made more effective through learning.</p>
                     <p>Transparency has long been recognized as a vital component of aid effectiveness, dating back all the way to the Marshall Plan and the founding</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.14</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>Promoting South-South Cooperation through Knowledge Exchange</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>RHEE</surname>
                           <given-names>HYUNJOO</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>260</fpage>
                  <abstract>
                     <p>Over the last decade, South-South cooperation in development assistance (SSC) has increased markedly. Although it has a long history, having emerged as a result of the decolonization movement in the 1950s and 1960s, South-South cooperation has expanded recently thanks to the rapid industrialization of key countries in the 1990s, the subsequent increase in South-South trade, and the progressive emergence of the South’s private sector. Today SSC is viewed as an emerging, yet powerful, game changer in global politics, trade, commerce, and development assistance.</p>
                     <p>The role of South-South cooperation in development has two aspects. The first aspect focuses on the volume</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.15</book-part-id>
                  <title-group>
                     <title>Contributors</title>
                  </title-group>
                  <fpage>281</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.16</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>283</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt12634s.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>307</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
