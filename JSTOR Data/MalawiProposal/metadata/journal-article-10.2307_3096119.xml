<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">intestudquar</journal-id>
         <journal-id journal-id-type="jstor">j100193</journal-id>
         <journal-title-group>
            <journal-title>International Studies Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishers</publisher-name>
         </publisher>
         <issn pub-type="ppub">00208833</issn>
         <issn pub-type="epub">14682478</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3096119</article-id>
         <article-categories>
            <subj-group>
               <subject>Presidential Address</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Political Economy of Speculative Attacks in the Developing World</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>David A.</given-names>
                  <surname>Leblang</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i355722</issue-id>
         <fpage>69</fpage>
         <lpage>91</lpage>
         <page-range>69-91</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 International Studies Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3096119"/>
         <abstract>
            <p>This paper examines the relationship between politics and speculative attacks in developing countries. While a burgeoning literature focuses on the economic determinants of speculative behavior, little attention has been paid to the importance of political factors. I examine the response of international capital markets to electoral and partisan changes in a sample of 78 developing countries using monthly data from January 1975 to December 1998. All other things being equal, the empirical evidence indicates that speculative attacks are more likely (1) under left rather than under right governments and (2) during the period after an election as compared with all other periods. The results suggest that models developed for OECD economies can be used to understand political-economic phenomena in developing countries.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d498e202a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d498e209" publication-type="book">
Krugman (2000)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e217" publication-type="book">
Obstfeld and Rogoff (1996).  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e226a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d498e233" publication-type="journal">
Salant and Henderson (1978)  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e242a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d498e249" publication-type="journal">
Eichengreen, Rose, and Wyplosz (1995)  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e258a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d498e265" publication-type="book">
Drazen (1999)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e273" publication-type="book">
Leblang (2001)  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e283a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d498e290" publication-type="journal">
Eichengreen, Rose, and Wyplosz, 1995  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e298" publication-type="journal">
Leblang and
Bernhard, 2000  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e310a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d498e317" publication-type="journal">
Eichengreen, Rose, and Wyplosz (1995)  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e326a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d498e333" publication-type="book">
Derksen's Web site is www.agora.stm.it/elections/parties.htm. Other publications that were consulted included
East and Joseph (1993)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e344" publication-type="book">
Szajkowski (1994).  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e353a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d498e360" publication-type="book">
King and Zeng.
King and Zeng's (2000)  </mixed-citation>
            </p>
         </fn>
         <fn id="d498e372a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d498e379" publication-type="book">
Tomz, King, and Zeng (1999)  </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e387" publication-type="other">
Tucker (1999)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d498e405a1310">
            <mixed-citation id="d498e409" publication-type="journal">
ALESINA, A. (1989) Politics and Business Cycles in Industrial Democracies. Economic Policy8:55-89.<object-id pub-id-type="doi">10.2307/1344464</object-id>
               <fpage>55</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e422a1310">
            <mixed-citation id="d498e426" publication-type="book">
ALESINA, A., AND N. ROUBINI WITH G. D. COHEN (1997) Political Cycles and the Macroeconomy. Cam-
bridge, MA: MIT Press.<person-group>
                  <string-name>
                     <surname>Alesina</surname>
                  </string-name>
               </person-group>
               <source>Political Cycles and the Macroeconomy</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e451a1310">
            <mixed-citation id="d498e455" publication-type="book">
BECK, N., J. KATZ, AND R. TUCKER (1997) Beyond Ordinary Logit: Taking Time Seriously in Binary
Time-Series-Cross-Section Models. Manuscript (available at ftp://weber.ucsd.edu:/pub/nbeck).<person-group>
                  <string-name>
                     <surname>Beck</surname>
                  </string-name>
               </person-group>
               <source>Beyond Ordinary Logit: Taking Time Seriously in Binary Time-Series-Cross-Section Models</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e480a1310">
            <mixed-citation id="d498e484" publication-type="book">
BECK, T., G. CLARKE, A. GROFF, P. KEEFER, AND P. WALSH (1999) New Tools and New Tests in Compar-
ative Political Economy: The Database of Political Institutions. Manuscript, World Bank Devel-
opment Research Group.<person-group>
                  <string-name>
                     <surname>Beck</surname>
                  </string-name>
               </person-group>
               <source>New Tools and New Tests in Comparative Political Economy: The Database of Political Institutions</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d498e514a1310">
            <mixed-citation id="d498e518" publication-type="journal">
BLANCO, H., AND P. GARBER (1986) Recurrent Devaluations and Speculative Attacks on the Mexican
Peso. Journal of Political Economy94:148-166.<object-id pub-id-type="jstor">10.2307/1831963</object-id>
               <fpage>148</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e534a1310">
            <mixed-citation id="d498e538" publication-type="book">
CALVO, G. A. (1995) Varieties of Capital-Market Crises. Center for International Economics. Working
Paper no. 15. University of Maryland.<person-group>
                  <string-name>
                     <surname>Calvo</surname>
                  </string-name>
               </person-group>
               <source>Varieties of Capital-Market Crises</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d498e563a1310">
            <mixed-citation id="d498e567" publication-type="journal">
CALVO, G., AND E. MENDOZA (2000a) Rational Contagion and the Globalization of Securities Markets.
Journal of International Economics51:79-113.<person-group>
                  <string-name>
                     <surname>Calvo</surname>
                  </string-name>
               </person-group>
               <fpage>79</fpage>
               <volume>51</volume>
               <source>Journal of International Economics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d498e599a1310">
            <mixed-citation id="d498e603" publication-type="journal">
CALVO, G., AND E. MENDOZA (2000b) Capital-Market Crises and Economic Collapse in Emerging
Markets: An Informational-Frictions Approach. American Economic Review90 (May).<object-id pub-id-type="jstor">10.2307/117192</object-id>
            </mixed-citation>
         </ref>
         <ref id="d498e616a1310">
            <mixed-citation id="d498e620" publication-type="book">
CORSETTI, G., P. PESENTI, AND N. RoUBINI (1998) Paper Tigers? A Model of the Asian Crisis. Manu-
script, Department of Economics, New York University.<person-group>
                  <string-name>
                     <surname>Corsetti</surname>
                  </string-name>
               </person-group>
               <source>Paper Tigers? A Model of the Asian Crisis</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e645a1310">
            <mixed-citation id="d498e649" publication-type="journal">
CUMBY, R., AND S. VAN WIJNBERGEN (1989) Financial Policy and Speculative Runs with a Crawling Peg.
Journal of International Economics27:111-127.<person-group>
                  <string-name>
                     <surname>Cumby</surname>
                  </string-name>
               </person-group>
               <fpage>111</fpage>
               <volume>27</volume>
               <source>Journal of International Economics</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d498e682a1310">
            <mixed-citation id="d498e686" publication-type="book">
DEMIRGUC-KUNT, A., AND E. DETRAGIACHE (1997) The Determinants of Banking Crises: Evidence
from the Industrial and Developing Countries. International Monetary Fund Working Paper no.
97-106.<person-group>
                  <string-name>
                     <surname>Demirguc-kunt</surname>
                  </string-name>
               </person-group>
               <source>The Determinants of Banking Crises: Evidence from the Industrial and Developing Countries</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e715a1310">
            <mixed-citation id="d498e719" publication-type="journal">
DIAMOND, D., AND P. DYBVIG (1983) Bank Runs, Deposit Insurance, and Liquidity. Journal of Political
Economy91:401-419.<object-id pub-id-type="jstor">10.2307/1837095</object-id>
               <fpage>401</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e735a1310">
            <mixed-citation id="d498e739" publication-type="book">
DOOLEY, M. (1998) A Model of Crises in Emerging Markets. International Finance Discussion Papers
no. 1998-630, Board of Governors, Federal Reserve Bank.<person-group>
                  <string-name>
                     <surname>Dooley</surname>
                  </string-name>
               </person-group>
               <source>A Model of Crises in Emerging Markets</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e764a1310">
            <mixed-citation id="d498e768" publication-type="journal">
DORNBUSCH, R., I. GOLDFAJN, AND R. VALDES (1995) Currency Crises and Collapses. Brookings Papers on
Economic Activity2:219-295.<object-id pub-id-type="doi">10.2307/2534613</object-id>
               <fpage>219</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e784a1310">
            <mixed-citation id="d498e788" publication-type="book">
DRAZEN, A. (1999) Interest Rate Defense Against Speculative Attacks Under Asymmetric Information.
Working Paper, Department of Economics, University of Maryland.<person-group>
                  <string-name>
                     <surname>Drazen</surname>
                  </string-name>
               </person-group>
               <source>Interest Rate Defense Against Speculative Attacks Under Asymmetric Information</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d498e813a1310">
            <mixed-citation id="d498e817" publication-type="book">
EAST, R., AND T. JOSEPH, EDS. (1993) Political Parties of Africa and the Middle East. Harlow, England:
Longman.<person-group>
                  <string-name>
                     <surname>East</surname>
                  </string-name>
               </person-group>
               <source>Political Parties of Africa and the Middle East</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d498e843a1310">
            <mixed-citation id="d498e847" publication-type="book">
EDWARDS, S., AND M. NAIM, EDS. (1997) Mexico 1994: The Anatomy of an Emerging-Market Crash. Wash-
ington, DC: Carnegie Endowment for World Peace.<person-group>
                  <string-name>
                     <surname>Edwards</surname>
                  </string-name>
               </person-group>
               <source>Mexico 1994: The Anatomy of an Emerging-Market Crash</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e872a1310">
            <mixed-citation id="d498e876" publication-type="book">
EICHENGREEN, B. (2001) What Problems Can Dollarization Solve? Manuscript, Department of Eco-
nomics, University of California, Berkeley (available at http://elsa.berkeley.edu/users/eichengr/
neworleans.pdf).<person-group>
                  <string-name>
                     <surname>Eichengreen</surname>
                  </string-name>
               </person-group>
               <source>What Problems Can Dollarization Solve?</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d498e905a1310">
            <mixed-citation id="d498e909" publication-type="journal">
EICHENGREEN, B., A. ROSE, AND C. WYPLOSZ (1995) Exchange Market Mayhem: The Antecedents and
Aftermath of Speculative Attacks. Economic Policy21:249-312.<object-id pub-id-type="doi">10.2307/1344591</object-id>
               <fpage>249</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e925a1310">
            <mixed-citation id="d498e929" publication-type="conference">
FERREE, K., AND S. SINGH (1999) Institutional Change and Economic Performance in Africa, 1970-
1995. Paper presented at the Annual Meeting of the American Political Science Association,
Atlanta.<person-group>
                  <string-name>
                     <surname>Ferree</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Institutional Change and Economic Performance in Africa, 19701995</comment>
               <source>Annual Meeting of the American Political Science Association, Atlanta</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d498e961a1310">
            <mixed-citation id="d498e965" publication-type="conference">
FISCHER, S. (2001) Exchange Rate Regimes: Is the Bipolar View Correct? Distinguished Lecture on
Economics in Government. Presented at the Annual Meeting of the American Economic Asso-
ciation, January 6, New Orleans.<person-group>
                  <string-name>
                     <surname>Fischer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Exchange Rate Regimes: Is the Bipolar View Correct? Distinguished Lecture on Economics in Government</comment>
               <source>Annual Meeting of the American Economic Association, January 6, New Orleans</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d498e997a1310">
            <mixed-citation id="d498e1001" publication-type="journal">
FRANKEL, J., AND A. ROSE (1996) Currency Crashes in Emerging Markets: An Empirical Treatment.
Journal of International Economics41:351-366.<person-group>
                  <string-name>
                     <surname>Frankel</surname>
                  </string-name>
               </person-group>
               <fpage>351</fpage>
               <volume>41</volume>
               <source>Journal of International Economics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1034a1310">
            <mixed-citation id="d498e1038" publication-type="journal">
FREEMAN,J. R.,J. C. HAYES, AND H. STIX (2000) Democracy and Markets: The Case of Exchange Rates.
American Journal of Political Science44:449-468.<object-id pub-id-type="doi">10.2307/2669258</object-id>
               <fpage>449</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1054a1310">
            <mixed-citation id="d498e1058" publication-type="book">
FRIEDEN, J. (1999) The Political Economy of European Exchange Rates: An Empirical Assessment.
Manuscript, Department of Government, Harvard University.<person-group>
                  <string-name>
                     <surname>Frieden</surname>
                  </string-name>
               </person-group>
               <source>The Political Economy of European Exchange Rates: An Empirical Assessment</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1083a1310">
            <mixed-citation id="d498e1087" publication-type="book">
FRIEDEN, J. (2000) The Political Economy of Dollarization: Domestic and International Factors.
Manuscript, Department of Government, Harvard University.<person-group>
                  <string-name>
                     <surname>Frieden</surname>
                  </string-name>
               </person-group>
               <source>The Political Economy of Dollarization: Domestic and International Factors</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1112a1310">
            <mixed-citation id="d498e1116" publication-type="book">
FRIEDEN,J., P. GHEZZI, AND E. STEIN (1999) The Political Economy of Exchange Rate Policy in Latin
America. Manuscript, Department of Government, Harvard University.<person-group>
                  <string-name>
                     <surname>Frieden</surname>
                  </string-name>
               </person-group>
               <source>The Political Economy of Exchange Rate Policy in Latin America</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1141a1310">
            <mixed-citation id="d498e1145" publication-type="book">
GARRETT, G. (1998) Partisan Politics in the Global Economy. New York: Cambridge University Press.<person-group>
                  <string-name>
                     <surname>Garrett</surname>
                  </string-name>
               </person-group>
               <source>Partisan Politics in the Global Economy</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1167a1310">
            <mixed-citation id="d498e1171" publication-type="book">
GOLDFAJN, I., AND R. VALDES (1997) Are Currency Crises Predictable? International Monetary Fund
Working Paper no. 97-159.<person-group>
                  <string-name>
                     <surname>Goldfajn</surname>
                  </string-name>
               </person-group>
               <source>Are Currency Crises Predictable?</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1197a1310">
            <mixed-citation id="d498e1201" publication-type="journal">
GURR, T. R., K. JAGGERS, AND W. H. MOORE (1990) The Transformation of the Western State: The
Growth of Democracy, Autocracy, and State Power Since 1800. Studies in Comparative Internal
Development25:73-108.<person-group>
                  <string-name>
                     <surname>Gurr</surname>
                  </string-name>
               </person-group>
               <fpage>73</fpage>
               <volume>25</volume>
               <source>Studies in Comparative Internal Development</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1236a1310">
            <mixed-citation id="d498e1240" publication-type="book">
HAGGARD, S. (2000) The Political Economy of the Asian Financial Crisis. Washington, DC: Institute for
International Economics.<person-group>
                  <string-name>
                     <surname>Haggard</surname>
                  </string-name>
               </person-group>
               <source>The Political Economy of the Asian Financial Crisis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1265a1310">
            <mixed-citation id="d498e1269" publication-type="journal">
HAGGARD, S., AND S. MAXFIELD (1996) The Political Economy of Financial Internationalization in the
Developing World. International Organization50:35-68.<object-id pub-id-type="jstor">10.2307/2706998</object-id>
               <fpage>35</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1285a1310">
            <mixed-citation id="d498e1289" publication-type="book">
HIBBS, D. (1987) The American Political Economy: Macroeconomics and Electoral Politics. Cambridge, MA:
Harvard University Press.<person-group>
                  <string-name>
                     <surname>Hibbs</surname>
                  </string-name>
               </person-group>
               <source>The American Political Economy: Macroeconomics and Electoral Politics</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1314a1310">
            <mixed-citation id="d498e1318" publication-type="book">
KAMINSKY, G., AND C. REINHART (1996) The Twin Crises: The Causes of Banking and Balance-of-
Payments Problems. International Finance Discussion Paper no. 544. Washington, DC: Board of
Governors of the Federal Reserve.<person-group>
                  <string-name>
                     <surname>Kaminsky</surname>
                  </string-name>
               </person-group>
               <source>The Twin Crises: The Causes of Banking and Balance-ofPayments Problems</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1347a1310">
            <mixed-citation id="d498e1351" publication-type="book">
KAMINSKY, G., S. LIZONDO, AND C. REINHART (1997) Leading Indicators of Currency Crises. Inter-
national Monetary Fund Working Paper no. 97-79.<person-group>
                  <string-name>
                     <surname>Kaminsky</surname>
                  </string-name>
               </person-group>
               <source>Leading Indicators of Currency Crises</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1377a1310">
            <mixed-citation id="d498e1381" publication-type="book">
KEOHANE, R., AND H. MILNER, EDS. (1996) Internationalization and Domestic Politics. New York: Cam-
bridge University Press.<person-group>
                  <string-name>
                     <surname>Keohane</surname>
                  </string-name>
               </person-group>
               <source>Internationalization and Domestic Politics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1406a1310">
            <mixed-citation id="d498e1410" publication-type="book">
KING, G., AND L. ZENG (2000) Logistic Regression in Rare Events Data. Manuscript, Harvard Univer-
sity (available at http://gking.harvard.edu/files/Os.pdf).<person-group>
                  <string-name>
                     <surname>King</surname>
                  </string-name>
               </person-group>
               <source>Logistic Regression in Rare Events Data</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1435a1310">
            <mixed-citation id="d498e1439" publication-type="journal">
KLEIN, M., AND N. MARION (1997) Explaining the Duration of Exchange-Rate Pegs. Journal of Devel-
opment Economics54:387-404.<person-group>
                  <string-name>
                     <surname>Klein</surname>
                  </string-name>
               </person-group>
               <fpage>387</fpage>
               <volume>54</volume>
               <source>Journal of Development Economics</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1471a1310">
            <mixed-citation id="d498e1475" publication-type="book">
KRAAY, A. (1998) Do High Interest Rates Defend Currencies During Speculative Attacks? Inter-
national Monetary Fund Working Paper.<person-group>
                  <string-name>
                     <surname>Kraay</surname>
                  </string-name>
               </person-group>
               <source>Do High Interest Rates Defend Currencies During Speculative Attacks?</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1500a1310">
            <mixed-citation id="d498e1504" publication-type="journal">
KRUGMAN, P. (1979) A Model of Balance of Payments Crises. Journal of Money, Credit and Banking
11:311-325.<object-id pub-id-type="doi">10.2307/1991793</object-id>
               <fpage>311</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1520a1310">
            <mixed-citation id="d498e1524" publication-type="other">
KRUGMAN, P. (1998) "What Happened to Aisa?" (available at http://web.mit.edu/krugman/www/
DISINTER.html).</mixed-citation>
         </ref>
         <ref id="d498e1535a1310">
            <mixed-citation id="d498e1539" publication-type="book">
KRUGMAN, P., ED. (2000) Currency Crises (NBER Conference Report). Chicago: University of Chicago
Press.<person-group>
                  <string-name>
                     <surname>Krugman</surname>
                  </string-name>
               </person-group>
               <source>Currency Crises (NBER Conference Report)</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1564a1310">
            <mixed-citation id="d498e1568" publication-type="book">
LAHIRI, A., AND C. VfIGH (2000) Delaying the Inevitable: Optimal Interest Policy and BOP Crises.
Working Paper. Department of Economics, UCLA.<person-group>
                  <string-name>
                     <surname>Lahiri</surname>
                  </string-name>
               </person-group>
               <source>Delaying the Inevitable: Optimal Interest Policy and BOP Crises</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1593a1310">
            <mixed-citation id="d498e1597" publication-type="journal">
LEBLANG, D. (1997) Are Capital Controls Obsolete? Evidence from the Developed and Developing
World, 1967-1986. International Studies Quarterly41:435-454.<object-id pub-id-type="jstor">10.2307/2600791</object-id>
               <fpage>435</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1613a1310">
            <mixed-citation id="d498e1617" publication-type="book">
LEBLANG, D. (2001) To Devalue or To Defend: The Political Economy of Exchange Rate Policy in the
Developing World. Manuscript, University of Colorado.<person-group>
                  <string-name>
                     <surname>Leblang</surname>
                  </string-name>
               </person-group>
               <source>To Devalue or To Defend: The Political Economy of Exchange Rate Policy in the Developing World</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1642a1310">
            <mixed-citation id="d498e1646" publication-type="journal">
LEBLANG, D., AND W. BERNHARD (2000) The Politics of Speculative Attacks in Industrial Democracies.
International Organization54:291-324.<object-id pub-id-type="jstor">10.2307/2601299</object-id>
               <fpage>291</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1662a1310">
            <mixed-citation id="d498e1666" publication-type="book">
LEE, J. W., AND C. RHEE (1998) Social Impacts of the Asian Crisis: Policy Challenges and Lessons.
United Nations Development Programme. Working Paper no. 333, Human Development Report
Office.<person-group>
                  <string-name>
                     <surname>Lee</surname>
                  </string-name>
               </person-group>
               <source>Social Impacts of the Asian Crisis: Policy Challenges and Lessons</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1696a1310">
            <mixed-citation id="d498e1700" publication-type="journal">
LOBO, B., AND D. TUFTE (1998) Exchange Rate Volatility: Does Politics Matter? Journal of Macroeconom-
ics20:351-365.<person-group>
                  <string-name>
                     <surname>Lobo</surname>
                  </string-name>
               </person-group>
               <fpage>351</fpage>
               <volume>20</volume>
               <source>Journal of Macroeconomics</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1732a1310">
            <mixed-citation id="d498e1736" publication-type="journal">
MACINTYRE, A. (2001) Institutions and Investors: The Politics of the Financial Crisis in Southeast
Asia. International Organization (spring).<object-id pub-id-type="jstor">10.2307/3078598</object-id>
            </mixed-citation>
         </ref>
         <ref id="d498e1749a1310">
            <mixed-citation id="d498e1753" publication-type="journal">
NORDHAUS, W. D. (1975) The Political Business Cycle. Review of Economics Studies42:169-190.<person-group>
                  <string-name>
                     <surname>Nordhaus</surname>
                  </string-name>
               </person-group>
               <fpage>169</fpage>
               <volume>42</volume>
               <source>Review of Economics Studies</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1782a1310">
            <mixed-citation id="d498e1786" publication-type="book">
OBSTFELD, M. (1994) The Logic of Currency Crises. National Bureau of Economics. Working Paper
no. 4640.<person-group>
                  <string-name>
                     <surname>Obstfeld</surname>
                  </string-name>
               </person-group>
               <source>The Logic of Currency Crises. National Bureau of Economics</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1811a1310">
            <mixed-citation id="d498e1815" publication-type="journal">
OBSTFELD, M. (1996) Models of Currency Crises with Self-Fulfilling Features. European Economic
Review40:1037-1047.<person-group>
                  <string-name>
                     <surname>Obstfeld</surname>
                  </string-name>
               </person-group>
               <fpage>1037</fpage>
               <volume>40</volume>
               <source>European Economic Review</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1847a1310">
            <mixed-citation id="d498e1851" publication-type="book">
OBSTFELD, M., AND K. ROGOFF (1996) Foundations of International Macroeconomics. Cambridge, MA: MIT
Press.<person-group>
                  <string-name>
                     <surname>Obstfeld</surname>
                  </string-name>
               </person-group>
               <source>Foundations of International Macroeconomics</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1877a1310">
            <mixed-citation id="d498e1881" publication-type="journal">
PLOEG, F. VAN DER (1989) The Political Economy of Overvaluation. Economic Journal99:850-855.<object-id pub-id-type="doi">10.2307/2233776</object-id>
               <fpage>850</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1894a1310">
            <mixed-citation id="d498e1898" publication-type="book">
RADELET, S., ANDJ. SACHS (1998) The Onset of the East Asian Financial Crisis. Cambridge, MA: Institute
for International Development, Harvard University.<person-group>
                  <string-name>
                     <surname>Radelet</surname>
                  </string-name>
               </person-group>
               <source>The Onset of the East Asian Financial Crisis</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d498e1923a1310">
            <mixed-citation id="d498e1927" publication-type="other">
ROSE, A. (1999) Is There a Case for an Asian Monetary Fund? Federal Reserve Bank of San Francisco
Economic Letter, 99-137.</mixed-citation>
         </ref>
         <ref id="d498e1937a1310">
            <mixed-citation id="d498e1941" publication-type="journal">
SACHS, J., A. TORNELL, AND A. VELASCO (1996) Financial Crises in Emerging Markets: The Lessons
from 1995. Brookings Papers on Economic Activity1:147-215.<object-id pub-id-type="doi">10.2307/2534648</object-id>
               <fpage>147</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1957a1310">
            <mixed-citation id="d498e1961" publication-type="journal">
SALANT, S., AND D. HENDERSON (1978) Market Anticipation of Government Policy and the Price of
Gold. Journal of Political Economy86:627-648.<object-id pub-id-type="jstor">10.2307/1840382</object-id>
               <fpage>627</fpage>
            </mixed-citation>
         </ref>
         <ref id="d498e1977a1310">
            <mixed-citation id="d498e1981" publication-type="book">
SZAJKOWSKI, B. (1994) Political Parties of Eastern Europe, Russia and the Successor States. Harlow, England:
Longman.<person-group>
                  <string-name>
                     <surname>Szajkowski</surname>
                  </string-name>
               </person-group>
               <source>Political Parties of Eastern Europe, Russia and the Successor States</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d498e2007a1310">
            <mixed-citation id="d498e2011" publication-type="other">
ToMz, M., G. KING, AND L. ZENG (1999) "RELOGIT: Rare Events Logistic Regression" (available at
http://gking.harvard.edu/files/relogit.zip).</mixed-citation>
         </ref>
         <ref id="d498e2021a1310">
            <mixed-citation id="d498e2025" publication-type="other">
TUCKER, R. (1999) "BTSCS: A Binary Time-Series Cross-Section Data Analysis Utility Program" (avail-
able at http://www.vanderbilt.edu/--rtucker/programs/btscs/).</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
