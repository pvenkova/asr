<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">sociforu</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100768</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Sociological Forum</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley Subscription Services</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08848971</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737861</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41330899</article-id>
         <article-categories>
            <subj-group>
               <subject>SPECIAL ARTICLE SECTION: SOCIOLOGICAL RESEARCH ON CONTEMPORARY ISSUES</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Migration, International Telecommunications, and Human Rights</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rob</given-names>
                  <surname>Clark</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jason</given-names>
                  <surname>Hall</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">26</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40061476</issue-id>
         <fpage>870</fpage>
         <lpage>896</lpage>
         <permissions>
            <copyright-statement>© 2011 Eastern Sociological Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41330899"/>
         <abstract>
            <p>World polity embeddedness has traditionally been measured by state and civil participation in formal venues, including international organizations, multilateral agreements, and world conferences. In this study, we highlight an alternative form of embeddedness found in cross-national social relations and apply this framework to the human rights sector of the world polity. Specifically, we propose that the international migrant community diffuses human rights values and practices via (1) local performance and (2) cross-national communication. Using data from the World Values Survey, we first show that immigrants are more likely to embrace, and actively participate in, the human rights movement. Next, using network data that report country-to-country bilateral flows, we observe a high degree of correspondence between international migration and telecommunications, confirming previous studies that trace telephone traffic to the flow of people. Finally, analyzing a balanced data set of 333 observations across 111 countries spanning the 1975-2000 period, we use ordered probit regression to assess the local and cross-national effects of migrants on a state's human rights record. We find that a country's immigration level and its in-degree centrality in international telecommunications both positively affect its Amnesty International rating, and that these effects are robust to a number of alternative specifications.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1268e192a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1268e199" publication-type="other">
Cohen-Marks and Stout (2011),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1268e205" publication-type="other">
Hass (2011),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1268e211" publication-type="other">
Hawdon and Ryan (2011),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1268e218" publication-type="other">
Longest and Smith (2011),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1268e224" publication-type="other">
Widner and Chicoine (2011).</mixed-citation>
            </p>
         </fn>
         <fn id="d1268e231a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1268e238" publication-type="other">
Department of Sociology, University of Oklahoma, Kaufman Hall 331, 780 Van Vleet Oval,
Norman, Oklahoma 73019;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1268e247" publication-type="other">
e-mail: robclark@ou.edu.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d1268e263a1310">
            <mixed-citation id="d1268e267" publication-type="other">
Beckfield, Jason. 2003. "Inequality in the World Polity: The Structure of International Organiza-
tion," American Sociological Review 68: 401-424.</mixed-citation>
         </ref>
         <ref id="d1268e277a1310">
            <mixed-citation id="d1268e281" publication-type="other">
Boli, John, Thomas Loya, and Teresa Loftin. 1999. "National Participation in World-Polity Orga-
nization," in John Boli and George Thomas (eds.), Constructing World Culture: International
Nongovernmental Organizations Since 1875 : pp. 50-77. Stanford, CA: Stanford University
Press.</mixed-citation>
         </ref>
         <ref id="d1268e297a1310">
            <mixed-citation id="d1268e301" publication-type="other">
Boli, John, and George Thomas. 1997. "World Culture in the World Polity: A Century of Inter-
national Nongovernmental Organization," American Sociological Review 62: 171-190.</mixed-citation>
         </ref>
         <ref id="d1268e311a1310">
            <mixed-citation id="d1268e315" publication-type="other">
Chatterjee, Sampnt, Ali Hadi, and Bertram Price. 2000. Regression Analysis by Example, 3rd ed.
New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d1268e326a1310">
            <mixed-citation id="d1268e330" publication-type="other">
Cohen-Marks, Mara A., and Christopher Stout. 2011. "Can the American Dream Survive the
New Multiethnic America? Evidence from Los Angeles," Sociological Forum 26(4): 824-845.</mixed-citation>
         </ref>
         <ref id="d1268e340a1310">
            <mixed-citation id="d1268e344" publication-type="other">
Cole, Wade. 2005. "Sovereignty Relinquished? Explaining Commitment to the International
Human Rights Covenants, 1966-1999," American Sociolo2ical Review 70: 472-195.</mixed-citation>
         </ref>
         <ref id="d1268e354a1310">
            <mixed-citation id="d1268e358" publication-type="other">
Cole, Wade. 2006. "When All Else Fails: International Adjudication of Human Rights Abuse
Claims, 1976-1999," Social Forces 84: 1909-1935.</mixed-citation>
         </ref>
         <ref id="d1268e368a1310">
            <mixed-citation id="d1268e372" publication-type="other">
Cole, Wade. 2009. "Hard and Soft Commitments to Human Rights Treaties, 1966-2000," Socio-
logical Forum 24(3): 563-588.</mixed-citation>
         </ref>
         <ref id="d1268e382a1310">
            <mixed-citation id="d1268e386" publication-type="other">
Drori, Gili, John Meyer, Francisco Ramirez, and Evan Schofer. 2003. Science in the Modern
World Polity: Institutionalization and Globalization. Stanford, CA: Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d1268e396a1310">
            <mixed-citation id="d1268e400" publication-type="other">
Frank, David. 1997. "Science, Nature, and the Globalization of the Environment, 1870-1990,"
Social Forces 76: 409-435.</mixed-citation>
         </ref>
         <ref id="d1268e411a1310">
            <mixed-citation id="d1268e415" publication-type="other">
Frank, David, Ann Hironaka, and Evan Schofer. 2000. "The Nation-State and the Natural
Environment Over the Twentieth Century," American Sociological Review 65: 96-116.</mixed-citation>
         </ref>
         <ref id="d1268e425a1310">
            <mixed-citation id="d1268e429" publication-type="other">
Frank, David, Wesley Longhofer, and Evan Schofer. 2007. "World Society, NGOs, and Environ-
mental Policy Reform in Asia," International Journal of Comparative Sociology 48: 275-295.</mixed-citation>
         </ref>
         <ref id="d1268e439a1310">
            <mixed-citation id="d1268e443" publication-type="other">
Frank, David, and Elizabeth Mceneaney. 1999. "The Individualization of Society and the Libera-
tion of State Policies on Same-Sex Sexual Relations, 1984-1995," Social Forces 77: 911-943.</mixed-citation>
         </ref>
         <ref id="d1268e453a1310">
            <mixed-citation id="d1268e457" publication-type="other">
Greig, J. Michael. 2002. "The End of Geography? Globalization, Communications, and Culture
in the International System," Journal of Conflict Resolution 46: 225-243.</mixed-citation>
         </ref>
         <ref id="d1268e467a1310">
            <mixed-citation id="d1268e471" publication-type="other">
Hafner-Burton, Emilie. 2005. "Trading Human Rights: How Preferential Trade Agreements
Influence Government Repression," International Organization 59: 593-629.</mixed-citation>
         </ref>
         <ref id="d1268e481a1310">
            <mixed-citation id="d1268e485" publication-type="other">
Hafner-Burton, Emilie, and Kiyoteru Tsutsui. 2005. "Human Rights in a Globalizing World:
The Paradox of Empty Promises," American Journal of Sociology 110: 1373-1411.</mixed-citation>
         </ref>
         <ref id="d1268e496a1310">
            <mixed-citation id="d1268e500" publication-type="other">
Hafner-Burton, Emilie, and Kiyoteru Tsutsui. 2007. "Justice Lost! The Failure of International
Human Rights Law to Matter Where Needed Most," Journal of Peace Research 44: 407-425.</mixed-citation>
         </ref>
         <ref id="d1268e510a1310">
            <mixed-citation id="d1268e514" publication-type="other">
Hass, Jeffrey K. 2011. "Norms and Survival in the Heat of War: Normative Versus Instrumental
Rationalities and Survival Tactics in the Blockade of Leningrad," Sociological Forum 26(4):
921-949.</mixed-citation>
         </ref>
         <ref id="d1268e527a1310">
            <mixed-citation id="d1268e531" publication-type="other">
Hathaway, Oona. 2002. "Do Human Rights Treaties Make a Difference?" Yale Law Journal 111-
1935-2042.</mixed-citation>
         </ref>
         <ref id="d1268e541a1310">
            <mixed-citation id="d1268e545" publication-type="other">
Hawdon, James, and John Ryan. 2011. "Neighborhood Organizations and Resident Assistance to
Police," Sociological Forum 26(4): 897-920.</mixed-citation>
         </ref>
         <ref id="d1268e555a1310">
            <mixed-citation id="d1268e559" publication-type="other">
Henderson, Conway. 1991. "Conditions Affecting the Use of Political Repression," Journal of
Conflict Resolution 35: 120-142.</mixed-citation>
         </ref>
         <ref id="d1268e569a1310">
            <mixed-citation id="d1268e573" publication-type="other">
Henderson, Conway. 1993. "Population Pressures and Political Repression," Social Science
Quarterly 74: 322-333.</mixed-citation>
         </ref>
         <ref id="d1268e584a1310">
            <mixed-citation id="d1268e588" publication-type="other">
International Bank for Reconstruction, Development. 1992. STARS 2.5 (Socioeconomic Time-
series Access and Retrieval System). Washington, DC: International Bank for Reconstruction
and Development.</mixed-citation>
         </ref>
         <ref id="d1268e601a1310">
            <mixed-citation id="d1268e605" publication-type="other">
International Bank for Reconstruction and Development. 2004, 2008. World Development Indica-
tors CD-ROM. Washington, DC: International Bank for Reconstruction and Development.</mixed-citation>
         </ref>
         <ref id="d1268e615a1310">
            <mixed-citation id="d1268e619" publication-type="other">
International Telecommunication Union/Telegeography Incorporated. 1998. Direction of Traffic
Database. International Telecommunication Geneva: Union/Telegeography Incorporated.</mixed-citation>
         </ref>
         <ref id="d1268e629a1310">
            <mixed-citation id="d1268e633" publication-type="other">
Keith, Linda. 1999. "The United Nations International Covenant on Civil and Political Rights:
Does it Make a Difference in Human Rights Behavior?" Journal of Peace Research 36: 95-
118.</mixed-citation>
         </ref>
         <ref id="d1268e646a1310">
            <mixed-citation id="d1268e650" publication-type="other">
Kellerman, Aharon. 1990. "International Telecommunications Around the World: A Flow Analy-
sis," Telecommunications Policy 14: 461-475.</mixed-citation>
         </ref>
         <ref id="d1268e660a1310">
            <mixed-citation id="d1268e664" publication-type="other">
Kellerman, Aharon. 1992. "U.S. International Telecommunications, 1961-1988: An International
Movement Model," Telecommunications Policy 16: 401-414.</mixed-citation>
         </ref>
         <ref id="d1268e675a1310">
            <mixed-citation id="d1268e679" publication-type="other">
Koster, Ferry. 2007. "Globalization, Social Structure, and the Willingness to Help Others: A
Multilevel Analysis Across 26 Countries," European Sociological Review 23: 537-551.</mixed-citation>
         </ref>
         <ref id="d1268e689a1310">
            <mixed-citation id="d1268e693" publication-type="other">
Lechner, Frank, and John Boli. 2005. World Culture: Origins and Consequences. Maiden, MA:
Blackwell Publishing.</mixed-citation>
         </ref>
         <ref id="d1268e703a1310">
            <mixed-citation id="d1268e707" publication-type="other">
Longest, Kyle C., and Christian Smith. 2011. "Conflicting or Compatible: Beliefs about Religion
and Science Among Emerging Adults in the United States," Sociological Forum 26(4): 846-869.</mixed-citation>
         </ref>
         <ref id="d1268e717a1310">
            <mixed-citation id="d1268e721" publication-type="other">
Marshall, Monty, and Keith Jaggers. 2005. "Polity IV Project: Political Regime Characteristics
and Transitions, 1800-2003." Retrieved May 2005 (http://www.cidcm.umd.edu/inscr/polity/
index.htm).</mixed-citation>
         </ref>
         <ref id="d1268e734a1310">
            <mixed-citation id="d1268e738" publication-type="other">
Meyer, John, John Boli, George Thomas, and Francisco Ramirez. 1997a. "World Society and the
Nation-State," American Journal of Sociology 103: 144-181.</mixed-citation>
         </ref>
         <ref id="d1268e748a1310">
            <mixed-citation id="d1268e752" publication-type="other">
Meyer, John, David Frank, Ann Hironaka, Evan Schofer, and Nancy Tuma. 1997b. "The Struc-
turing of a World Environmental Regime, 1870-1990," International Organization 51: 623-651.</mixed-citation>
         </ref>
         <ref id="d1268e763a1310">
            <mixed-citation id="d1268e767" publication-type="other">
Meyer, John, Francisco Ramirez, and Yasemin Soysal. 1992. "World Expansion of Mass Educa-
tion, 1870-1980," Sociology of Education 65: 128-149.</mixed-citation>
         </ref>
         <ref id="d1268e777a1310">
            <mixed-citation id="d1268e781" publication-type="other">
Neumayer, Eric. 2005. "Do International Human Rights Treaties Improve Respect for Human
Rights?" Journal of Conflict Resolution 49: 925-953.</mixed-citation>
         </ref>
         <ref id="d1268e791a1310">
            <mixed-citation id="d1268e795" publication-type="other">
Office of the U.N. High Commissioner for Human Rights. 2006. "Status of Ratifications of the
Principal International Human Rights Treaties." Retrieved September 2006 (http://www.
ohchr.org).</mixed-citation>
         </ref>
         <ref id="d1268e808a1310">
            <mixed-citation id="d1268e812" publication-type="other">
Palm, Risa. 2002. "International Telephone Calls: Global and Regional Patterns," Urban Geogra-
phy 23: 750-770.</mixed-citation>
         </ref>
         <ref id="d1268e822a1310">
            <mixed-citation id="d1268e826" publication-type="other">
Parsons, Christopher, Ronald Skeldon, Terrie Walmsley, and L. Alan Winters. 2007. Quantifying
International Migration: A Database of Bilateral Migrant Stocks," World Bank Policy
Research Working Paper 4165: 1-41.</mixed-citation>
         </ref>
         <ref id="d1268e839a1310">
            <mixed-citation id="d1268e843" publication-type="other">
Paxton, Pamela. 2002. "Social Capital and Democracy: An Interdependent Relationship," Ameri-
can Sociological Review 67: 254-277.</mixed-citation>
         </ref>
         <ref id="d1268e854a1310">
            <mixed-citation id="d1268e858" publication-type="other">
Paxton, Pamela, Melanie Hughes, and Jennifer Green. 2006. "The International Women's Move-
ment and Women's Political Representation, 1893-2003," American Sociological Review 71:
898-920.</mixed-citation>
         </ref>
         <ref id="d1268e871a1310">
            <mixed-citation id="d1268e875" publication-type="other">
Poe, Steven, Sabine Carey, and Tanya Vazquez. 2001. "How are These Pictures Different? A
Quantitative Comparison of the US State Department and Amnesty International Human
Rights Reports, 1976-1995," Human Rights Quarterly 23: 650-677.</mixed-citation>
         </ref>
         <ref id="d1268e888a1310">
            <mixed-citation id="d1268e892" publication-type="other">
Poe, Steven, and C. Neal Tate. 1994. "Repression of Human Rights to Personal Integrity in the
1980s: A Global Analysis," American Political Science Review 88: 853-872.</mixed-citation>
         </ref>
         <ref id="d1268e902a1310">
            <mixed-citation id="d1268e906" publication-type="other">
Poe, Steven, C. Neal Tate, and Linda Keith. 1999. "Repression of the Human Right to Personal
Integrity Revisited: A Global Cross-National Study Covering the Years 1976-1993," Interna-
tional Studies Quarterly 43: 291-313.</mixed-citation>
         </ref>
         <ref id="d1268e919a1310">
            <mixed-citation id="d1268e923" publication-type="other">
Powell, Emilia, and Jeffrey Staton. 2009. "Domestic Judicial Institutions and Human Rights
Treaty Violation," International Studies Quarterly 53: 149-174.</mixed-citation>
         </ref>
         <ref id="d1268e933a1310">
            <mixed-citation id="d1268e937" publication-type="other">
Ramirez, Francisco, Yasemin Soysal, and Suzanne Shanahan. 1997. "The Changing Logic of
Political Citizenship: Cross-National Acquisition of Women's Suffrage Rights, 1890 to 1990,"
American Sociological Review 62: 735-745.</mixed-citation>
         </ref>
         <ref id="d1268e951a1310">
            <mixed-citation id="d1268e955" publication-type="other">
Sandholtz, Wayne, and Mark Gray. 2003. "International Integration and National Corruption,
International Organization 57: 761-800.</mixed-citation>
         </ref>
         <ref id="d1268e965a1310">
            <mixed-citation id="d1268e969" publication-type="other">
Sarkees, Meredith. 2000. "The Correlates of War Data on War: An Update to 1997," Conflict
Management and Peace Science 18: 123-144.</mixed-citation>
         </ref>
         <ref id="d1268e979a1310">
            <mixed-citation id="d1268e983" publication-type="other">
Schafer, Mark. 1999. "International Nongovernmental Organizations and Third World Education
in 1990: A Cross-National Study," Sociolosv of Education 72: 69-88.</mixed-citation>
         </ref>
         <ref id="d1268e993a1310">
            <mixed-citation id="d1268e997" publication-type="other">
Schofer, Evan. 2003. "The Global Institutionalization of Geological Science, 1800 to 1990," Amer-
ican Sociological Review 68: 730-759.</mixed-citation>
         </ref>
         <ref id="d1268e1007a1310">
            <mixed-citation id="d1268e1011" publication-type="other">
Schofer, Evan. 2004. "Cross-National Differences in the Expansion of Science, 1970-1990," Social
Forces 83: 215-248.</mixed-citation>
         </ref>
         <ref id="d1268e1021a1310">
            <mixed-citation id="d1268e1025" publication-type="other">
Schofer, Evan, and Ann Hironaka. 2005. "The Effects of World Society on Environmental Protec-
tion Outcomes," Social Forces 84: 25-41.</mixed-citation>
         </ref>
         <ref id="d1268e1036a1310">
            <mixed-citation id="d1268e1040" publication-type="other">
Schofer, Evan, and John Meyer. 2005. "The Worldwide Expansion of Higher Education in the
Twentieth Century," American Sociological Review 70: 898-920.</mixed-citation>
         </ref>
         <ref id="d1268e1050a1310">
            <mixed-citation id="d1268e1054" publication-type="other">
Stata Corporation. 2007. Stata 10.0. College Station, TX: Stata Press.</mixed-citation>
         </ref>
         <ref id="d1268e1061a1310">
            <mixed-citation id="d1268e1065" publication-type="other">
Sun, Su-Lien, and George Barnett. 1994. "The International Telephone Network and Democrati-
zation," Journal of the American Society for Information Science 45: 411-421.</mixed-citation>
         </ref>
         <ref id="d1268e1075a1310">
            <mixed-citation id="d1268e1079" publication-type="other">
Swiss, Liam. 2009. "Decoupling Values from Action: An Event-History Analysis of the Election
of Women to Parliament in the Developing World, 1945-1990," International Journal of
Comparative Sociology 50: 69-95.</mixed-citation>
         </ref>
         <ref id="d1268e1092a1310">
            <mixed-citation id="d1268e1096" publication-type="other">
Tang, Linghui. 2003. "The Determinants of International Telephone Traffic Imbalances,"
Information Economics and Policy 15: 127-145.</mixed-citation>
         </ref>
         <ref id="d1268e1106a1310">
            <mixed-citation id="d1268e1110" publication-type="other">
Tsutsui, Kiyoteru, and Christine Wotipka. 2004. "Global Civil Society and the International
Human Rights Movement: Citizen Participation in Human Rights International Nongovern-
mental Organizations," Social Forces 83: 587-620.</mixed-citation>
         </ref>
         <ref id="d1268e1124a1310">
            <mixed-citation id="d1268e1128" publication-type="other">
Union of International Associations. 1985, 1990, 1995. Yearbook of International Organizations.
Munich, Germany: K.G. Saur.</mixed-citation>
         </ref>
         <ref id="d1268e1138a1310">
            <mixed-citation id="d1268e1142" publication-type="other">
Vertovec, Steven. 2004. "Cheap Calls: The Social Glue of Migrant Transnationalism," Global
Networks 4: 219-224.</mixed-citation>
         </ref>
         <ref id="d1268e1152a1310">
            <mixed-citation id="d1268e1156" publication-type="other">
Walker, Scott, and Steven Poe. 2002. "Does Cultural Diversity Affect Countries' Respect for
Human Rights?" Human Rights Quarterly 24: 237-263.</mixed-citation>
         </ref>
         <ref id="d1268e1166a1310">
            <mixed-citation id="d1268e1170" publication-type="other">
Wejnert, Barbara. 2005. "Diffusion, Development, and Democracy, 1800-1999," American Socio-
logical Review 70: 53-81.</mixed-citation>
         </ref>
         <ref id="d1268e1180a1310">
            <mixed-citation id="d1268e1184" publication-type="other">
Widner, Daniel, and Stephen Chicoine. 2011. "It's All in the Name: Employment Discrimination
Against Arab Americans," Sociological Forum 26(4): 806-823.</mixed-citation>
         </ref>
         <ref id="d1268e1194a1310">
            <mixed-citation id="d1268e1198" publication-type="other">
World Values Survey. 2006. "Values Surveys 1981-2004: Integrated Questionnaire, v.20060423."
Retrieved January 2009 (http://www.worldvaluessurvey.org).</mixed-citation>
         </ref>
         <ref id="d1268e1209a1310">
            <mixed-citation id="d1268e1213" publication-type="other">
Wotipka, Christine Min, and Kiyoteru Tsutsui. 2008. "Global Human Rights and State Sover-
eignty: State Ratification of International Human Rights Treaties, 1965-2001," Sociological
Forum 23(4): 724-754; 6(1): 194-195.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
