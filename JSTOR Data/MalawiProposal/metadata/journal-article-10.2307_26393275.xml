<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">conssoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50019729</journal-id>
         <journal-title-group>
            <journal-title>Conservation and Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>WOLTERS KLUWER INDIA PRIVATE LIMITED</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09724923</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">09753133</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26393275</article-id>
         <article-categories>
            <subj-group>
               <subject>Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Overfishing or Over Reacting? Management of Fisheries in the Pantanal Wetland, Brazil</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Chiaravalloti</surname>
                  <given-names>Rafael Morais</given-names>
               </string-name>
               <aff>Department of Anthropology, University College London, London, UK</aff>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2017</year>
            <string-date>2017</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">15</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26393264</issue-id>
         <fpage>111</fpage>
         <lpage>122</lpage>
         <permissions>
            <copyright-statement>Copyright: © Chiaravalloti 2016</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26393275"/>
         <abstract xml:lang="eng">
            <p>Historically, small-scale inland fisheries have been overlooked. Management practices based on industrial fishing, rarely take into account vital factors such as complex socio-environmental relations. This paper aims to help address this gap, contributing to a better understanding of small-scale inland fisheries. It uses the Pantanal wetland of Brazil as a case study, in which policymakers established restrictive fishing rules based on claims that local overfishing had caused numbers of recreational fishing tourists to decline. Through multiple regressions, participatory observation and mapping, this paper deconstructs the environmental narrative and uncovers the area’s complex traditional system of use. The case study, firstly illustrates the adverse consequences of misconceived top-down fishing management practices and, how such environmental narratives may be deconstructed. Then it presents important aspects of customary management in inland floodplains fisheries, including high levels of mobility within a common property regime and unexploitable reserves. It concludes by analysing recently proposed categories of property regimes, identifying fundamental elements that must be taken into account in designing appropriate management policies in inland floodplain fisheries.</p>
         </abstract>
         <kwd-group>
            <label>Keywords:</label>
            <kwd>The Pantanal</kwd>
            <kwd>wetland</kwd>
            <kwd>traditional fishermen</kwd>
            <kwd>overfishing</kwd>
            <kwd>tourism</kwd>
            <kwd>natural reserves</kwd>
            <kwd>rotational use</kwd>
            <kwd>common property regime</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>REFERENCES</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Abbott, J.G., and L.M. Campbell. 2009. Environmental histories and emerging fisheries management of the Upper Zambezi River Floodplains. Conservation and Society 7(2): 83–99.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Adger, W.N., and C. Luttrell. 2000. Property rights and the utilisation of wetlands. Ecological Economics 35: 75–89.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Agrawal, A. 2001. Common Property Institutions and Sustainable Governance of Resources. World Development 29(10): 1649–72.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Allan, J.D., R. Abell, Z. Hogan, C. Revenga, B.W Taylor, R. L Welcomme, and Kirk Winemiller. 2005. Overfishing of Inland Waters. Bioscience 55(12): 1041–51.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">Almeida, O.T., K. Lorenzen, and D.G. McGrath. 2002. Impact of Co-Management Agreements on the exploitation and productivity of Floodplain Lake Fisheries in the Lower Amazon. Proceedings of the 9th Biennial Conference of the International Association for the Study of Common Property, no. 2: 12.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Amâncio, C.O.G., R. Amâncio, R. Toniazzo, D. Botelho, and L.A. Pellegrin. 2010. Caracterização Socioeconômica da Comunidade do Amolar, Sub-Região do Paraguai, Corumbá, MS. Boletim de Pesquisa e Desenvolvimento 92:1-6.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Assies, W. 1997. The extraction of non-timber forest products as a conservation strategy in Amazonia. European Review of Latin American and Caribean Studies 62: 33–53.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Assine, M.L., H.A. Macedo, J. C. Stevaux, I. Bergier, C.R. Padovani, and A. Silva. 2015. Avulsive Rivers in the Hydrology of the Pantanal Wetland. In: Dynamics of the Pantanal Wetland in South America. (eds. Bergier, I. and M.L. Assine). 1st edition. Pp. 83-110. New York: Springer.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Behnke, R., S. Robinson, and E.J. Milner-Gulland. 2016. Governing open access: livestock distributions and institutional control in the Karakum Desert of Turkmenistan. Land Use Policy 52 (March): 103–19.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Beitl, C.M. 2015. Mobility in the mangroves: catch rates, daily decisions, and dynamics of artisanal fishing in a coastal commons. Applied Geography 59: 98–106.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Béné, C. 2009. Are fishers poor or vulnerable? assessing economic vulnerability in small-scale fishing communities. Journal of Development Studies 45(6): 911–33.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Béné, C., R. Arthur, H. Norbury, E.H. Allison, M. Beveridge, S. Bush, L. Campling, et al. 2016. Contribution of fisheries and aquaculture to food security and poverty reduction: assessing the current evidence. World Development 79: 177–196.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Berkes, F. 2006. From community-based resource management to complex systems: the scale issue and marine commons. Ecology and Society 11(1): 45.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Brasil, Marinha do. 2016. Capitania fluvial do Pantanal. https://www.mar.mil. br/cfpn/. Accessed on March 2, 2016</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Calcagno, V. and C. Mazancourt. 2010. Glmulti: an R package for easy automated model selection with (generalized) linear models. Journal of Statistical Software 34(12): 1–29.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Castello, L., D.G. Mcgrath, L.L. Hess, M.T. Coe, P.A. Lefebvre, P. Petry, M.N. Macedo, et al. 2013. The vulnerability of Amazon Freshwater Ecosystems. Conservation Letters 6(4): 217–29.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Catella, A.C., S.P. Albuquerque, F.L.R. Campos, and D.C. Santos. 2014. Sistema de Controle da Pesca de Mato Grosso Do Sul SCPESCA/MS - 20 - 2013. Boletim de Pesquisa e Desenvolvimento 127: 57.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Chiaravalloti, R.M. 2016. Is the Pantanal a pristine place? conflicts related to the conservation of the Pantanal. Ambiente &amp; Sociedade 19(2): 305–8.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Cochrane, K.L., N.L. Andrew, and A.M. Parma. 2011. Primary fisheries management: a minimum requirement for provision of sustainable human benefits in small-scale fisheries. Fish and Fisheries 12(3): 275–88.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Cooke, S.J., E.H. Allison, T. D. Beard, R. Arlinghaus, A.H. Arthington, D.M. Bartley, I.G. Cowx, et al. 2016. On the sustainability of inland fisheries: finding a future for the forgotten. AMBIO 45(7): 753–764. doi:10.1007/s13280-016-0787-4.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Costello, C., D. Ovando, T. Clavelle, C.K. Strauss, R. Hilborn, M.C. Melnychuk, T.A. Branch, et al. 2016. Global fishery futures under contrasting management regimes. PNAS 113(18): 5125-5129. doi: 10.1073/pnas.1520420113.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Cowx, I. G., O. Almeida, C. Bene, R. Brummett, S. Bush, W. Darwall, J. Pittock, et al. 2004. Value of River Fisheries. In: Proceedings of the second international symposium on the management of large rivers for fisheries. Organised by FAO Regional Office for Asia and the Pacific. eds. Robin, L. Welcomme and T. Petr. Bangkok. Pp.1:1–20.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">David, D. 1989. The first meeting for caiman conservation in the Pantanal. Crocodile Specialist Group - IUCN 8(3): 10–12.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Dietz, T., E. Ostrom, P. Stern. 2003. Struggle to govern the commons. Science 302(5652): 1907–1912. doi:10.1126/science.1091015.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Dixon, A. and R. Carrie. 2016. Creating local institutional arrangements for sustainable wetland socio-ecological systems: lessons from the ‘Striking a Balance’ project in Malawi. International Journal of Sustainable Development &amp; World Ecology 23(1): 40–52.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Dugan, P.J., C. Barlow, A.A. Agostinho, E. Baran, G.F. Cada, Da. Chen, I.G. Cowx, et al. 2010. Fish migration, dams, and loss of ecosystem services in the Mekong Basin. AMBIO 39(4): 344–348. doi: 10.1007/s13280-010-0036-1.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Fernandez-Gimenez, M.E. 2002. Spatial and social boundaries and the paradox of Pastoral Land Tenure: a case study from postsocialist Mongolia. Human Ecology 30(1): 49–78. doi: 10.1023/A:1014562913014.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Franco, J.L.A., J.A. Drummond, C. Gentile, and A.I. Azevedo. 2013. Biodiversidade e ocupação humana do Pantanal Mato-Grossense: conflitos e oportunidades. 1st edition. Rio de Janeiro: Garamond.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Girard, P. and I. Vargas. 2008. Turismo, desenvolvimento e saberes no Pantanal: diálogos e parcerias possíveis. Desenvolvimento e Meio Ambiente 18: 61–76.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Gray, N.J., L.M. Campbell. 2009. Science, policy advocacy, and marine protected areas. Conservation Biology 23(2): 460–468.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Hamilton, S.K., S.J. Sippel, J.M. Melack. 1996. Inundation patterns in the Pantanal wetland of South America determined from passive microwave remote sensing. Archiv fur Hydrobiol 137(1): 1–23.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Hanski, I. 1998. Metapopulation dynamics. Nature 396 (November): 41–49.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Holt, F. L. 2005. The Catch-22 of conservation: indigenous peoples, biologists, and cultural change. Human Ecology 33(2): 199–215.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Homewood, K. 1994. Pastoralists, environment and development in East African rangelands. In: Environment and population change. (eds. Zaba, B. and J. Clarke.). Pp. 311–22. Oxford, UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Homewood, K. and W. Rodgers. 1988. Pastoralism, conservation and the overgrazing controversy. In: Conservation in Africa: people, policies and practices. (eds. Anderson, D. and R. Grove). Pp: 111–28. Cambridge, UK: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Junk, W.J., P.B. Bayley, and R.E. Sparks. 1989. The flood pulse concept in river-floodplain systems. In: Proceedings of the International Large River Symposium (ed. Dodge, D.P). Pp 110-127. Ontário: Canadian Special Publication of Fisheries and Aquatic Sciences 106.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Junk, W.J., C.J.D.A. Silva, C.N. Cunha, and K. M. Wantzen. 2011. The Pantanal: ecology, biodiversity and sustainable management of a large neotropical wetland. 1st edition. Sofia-Moscow: Pensoft.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Keddy, P., L.H. Fraser, A.I. Solomeshch, W.J. Junk, D.R. Campbell, M.T.K. Arroyo, and C.J.R. Alho. 2009. Wet and wonderful: The world’s largest wetlands are conservation priorities. BioScience 59(1): 39–51.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Kennedy, M. and R.D. Gray, 1993. Can ecological theory predict the distribution of foraging animals? a critical analysis of experiments on the Ideal Free Distribution. Oikos 68(1): 158–166.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Kolding, J., M. Modesta, O. Mkumbo, and P. Zwieten. 2014. Status, trends and management of the Lake Victoria Fisheries. In: Inland fisheries evolution and management – case studies from four continents (eds. Welcomme, R., J. Valbo-Jorgenson, and A. Halls). Pp. 49–62. Rome: FAO - Fisheries and Aquaculture Technical Paper.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Kolding, J. and P.A.M.V. Zwieten. 2014. Sustainable fishing of inland waters. Journal of Limnology 73(1 SUPPL): 132–48.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Kolding, J., P. Zwieten, F. van, Martin, F. Poulain. 2016. Fisheries in the Drylands of Sub-Saharan Africa. Rome: FAO –Fisheries and Aquaculture Circular 1118.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Kothari, A., P. Camill, and J. Brown. 2013. Conservation as if people also mattered: policy and practice of community-based conservation. Conservation and Society 11(1): 1–15.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Kümpel, N., E. J. Milner-Gulland, G.U.Y. Cowlishaw, and J. M. Rowcliffe. 2009. Assessing sustainability at multiple scales in a rotational bushmeat hunting system. Conservation Biology 24(3): 861–71.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Lewis, J. 2007. Enabling forest people to map their resources and monitor illegal logging in Cameroon. Before Farming 2: 1–7. doi:10.3828/bfarm.2007.2.3</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Lorenzen, K., I. G. Cowx, R. E. M. Entsua-Mensah, N. P. Lester, J. D. Koehn, R. G. Randall, N. So, et al. 2016. Stock assessment in inland fisheries: a foundation for sustainable use and conservation. Reviews in Fish Biology and Fisheries 26(3): 405-440. doi:10.1007/s11160-016-9435-0.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Mateus, L. A. F., M. M. Vaz, and A. C. Catella. 2011. Fishery and fishing resources in the Pantanal. In: The Pantanal: ecology, biodiversity and sustainable management of a large neotropical seasonal wetland (eds. Junk, W.J., C.J. Silva, C. N. Cunha, and K.M. Wantzen.), 1st edition. Pp. 621–47. Sofia-Moscow: Pensoft.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Maymone, G. 2015. STF considera inconstitucional lei que permitia ‘abusos’ nos Rios de Mato Grosso do sul. http://www.correiodoestado.com.br/cidades/stf-considera-inconstitucional-lei-que-permitia-abusos-nosrios-do/245405/. Accessed on June 2, 2016.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Metcalfe, A.V. and P.S.P. Cowpertwait. 2009. Introductory time Series with R. Book. New York, NY: Springer.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Moraes, A. and L. Espinoza. 2001. Captura e comercialização de iscas vivas em corumbá, MS. Embrapa Pantanal: Boletim de Pesquisa 21: 38.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Moraes, A.S. and A.F. Seidl. 2000. Perfil dos pescadores esportivos do sul do Pantanal. embrapa Pantanal: Boletim de Pesquisa 24: 43.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Moritz, M., P. Scholte, I.M. Hamilton, S. Kari. 2013. Open access, open systems: pastoral management of common-pool resources in the Chad Basin. Human Ecology 41(3): 351–365.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Moritz, M., I.M. Hamilton, Y.J. Chen, and P. Scholte. 2014. Mobile pastoralists in the Logone Floodplain distribute themselves in an ideal free distribution. Current Anthropology 55(1): 115–122.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">Moritz, M., I.M. Hamilton, A.J. Yoak, P. Scholte, J. Cronley, P. Maddock, H. Pi. 2015. Simple movement rules result in ideal free distribution of mobile pastoralists. Ecological Modelling 305: 54–63.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">Mourão, G., Z. Campos, M. Coutinho, and C. Abercrombie. 1996. Size Structure of illegally harvested and surviving Caiman Caiman crocodilus yacare in Pantanal, Brazil. Biological Conservation 75(95): 261–65.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">Mourão, G., M. Coutinho, R. Mauro, Z. Campos, W. Tomás, and W. Magnusson. 2000. Aerial surveys of caiman, marsh deer and pampas deer in the Pantanal wetland of Brazil. Biological Conservation 92(5197): 175–83.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Mourão, G., W. Tomas, and Z. Campos. 2010. How much can the number of Jabiru Stork (Ciconiidae) nests vary due to change of flood extension in a large neotropical floodplain? Zoologia (Curitiba, Impresso) 27(5): 751–56.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Neuburger, M. and C.J. Silva. 2011. Riberinhos between ecological adaptation and modernisation. In: The Pantanal: ecology, biodiversity and sustainable management of a large neotropical seasonal wetland (eds. Junk, W.J., C.J. Silva, C. N. Cunha, and K. M. Wantzen.) Pp. 674–94. Sofia-Moscow: Pensoft.</mixed-citation>
         </ref>
         <ref id="ref59">
            <mixed-citation publication-type="other">Neumann, R.P. 2011. Political ecology III: theorizing landscape. Progress in Human Geography. 35(6): 843–850. doi:10.1177/0309132510390870</mixed-citation>
         </ref>
         <ref id="ref60">
            <mixed-citation publication-type="other">Norse, E.A., S. Brooke, W.W.L Cheung, M.R. Clark, I. Ekeland, R. Froese, K. M. Gjerde, et al. 2012. Sustainability of deep-sea fisheries. Marine Policy 36(2): 307–20.</mixed-citation>
         </ref>
         <ref id="ref61">
            <mixed-citation publication-type="other">Noy-Meir, I. 1975. Stability of grazing systems: an application of predatorprey graphs. The Journal of Ecology 63(2): 459.</mixed-citation>
         </ref>
         <ref id="ref62">
            <mixed-citation publication-type="other">Ostrom, E. 2009. A general framework for analysing sustainability of socialecological systems. Science 325 (5939): 419–22.</mixed-citation>
         </ref>
         <ref id="ref63">
            <mixed-citation publication-type="other">Padovani, C.R. 2010. Dinâmica espaço-temporal das inundações do Pantanal. Ph.D. thesis. University of São Paulo, São Paulo, Brazil.</mixed-citation>
         </ref>
         <ref id="ref64">
            <mixed-citation publication-type="other">Parsons, L.S. and J.J. Maguire. 1996. Comments on chaos, complexity and community management of fisheries. Marine Policy 20(2): 175–176.</mixed-citation>
         </ref>
         <ref id="ref65">
            <mixed-citation publication-type="other">Pauly, D., J. Alder, E. Bennett, V. Christensen, P. Tyedmers, and R. Watson. 2003. The Future for Fisheries. Science 302(5649): 1359–61.</mixed-citation>
         </ref>
         <ref id="ref66">
            <mixed-citation publication-type="other">Peres, C., C. Baider, P. Zuidema, L.H.O. Wadt, K. Kainer, D.P Gomes-Silva, R. Salomão, et al. 2003. Demographic threats to the sustainability of Brazil nut exploitation. Science 302 (5653): 2112–14.</mixed-citation>
         </ref>
         <ref id="ref67">
            <mixed-citation publication-type="other">R Core Team. 2015. R: a language and environment for statistical computing. Vienna, Austria: The R Foundation for Statistical Computing Platform.</mixed-citation>
         </ref>
         <ref id="ref68">
            <mixed-citation publication-type="other">Resende, E. K. 2011. Ecology of fish. In: The Pantanal: ecology, biodiversity and sustainable management of a large neotropical seasonal wetland (eds. Junk, W.J., C.J. Silva, C. N. Cunha, and K. M. Wantzen). Pp. 469–523. Sofia-Moscow: Pensoft.</mixed-citation>
         </ref>
         <ref id="ref69">
            <mixed-citation publication-type="other">Robbins, P. 2012. Political Ecology: A Critical Introduction. 2nd edition. Oxford: Wiley-Braclwell.</mixed-citation>
         </ref>
         <ref id="ref70">
            <mixed-citation publication-type="other">Rosa, M.R., F.S. Rosa, N. Crusco, E.R. Rosa, J. Freitas, F. Paternost, V. Mazin. 2009. Monitoramento das Alterações da Cobertura Vegetal e Uso do Solo na Bacia do Alto Paraguai. Brasília: WWF-Brasil, Ecoa, Conservation International, Avina, SOS Pantanal.</mixed-citation>
         </ref>
         <ref id="ref71">
            <mixed-citation publication-type="other">Shepard-Jr, G.H. and H. Ramirez. 2011. ‘Made in Brazil’: human dispersal of the Brazil Nut (Bertholletia excelsa, Lecythidaceae) in ancient Amazonia. Economic Botany 65(January): 44–65. doi: 10.1007/s12231-011-9151-6.</mixed-citation>
         </ref>
         <ref id="ref72">
            <mixed-citation publication-type="other">Silva, M.V. 1986. Mitos e verdades sobre a Pesca No Pantanal Sul-Mato-Grossense. 1st edition. Campo Grande: FIPLAN.</mixed-citation>
         </ref>
         <ref id="ref73">
            <mixed-citation publication-type="other">Silva, C.J. and J.A.F. Silva. 1995. No ritmo das Águas do Pantanal. 1st edition. São Paulo: NUPAUB/USP.</mixed-citation>
         </ref>
         <ref id="ref74">
            <mixed-citation publication-type="other">Sullivan, S. and R. Rohde. 2002. On non-equilibrium in arid and semi-arid grazing systems. Journal of Biogeography 29(12): 1595–1618.</mixed-citation>
         </ref>
         <ref id="ref75">
            <mixed-citation publication-type="other">Sunderlin, W.D., A. Angelsen, B. Belcher, P. Burgers, R. Nasi, L. Santoso, and S. Wunder. 2005. Livelihoods, forests, and conservation in developing countries: an overview. World Development 33(9): 1383–1402.</mixed-citation>
         </ref>
         <ref id="ref76">
            <mixed-citation publication-type="other">Valley, K. and R. Freeney. 2013. Reconciling spatial scales and sock structures for fisheries science and management. Fisheries Research 141: 1–2.</mixed-citation>
         </ref>
         <ref id="ref77">
            <mixed-citation publication-type="other">Vollan, B. and E. Ostrom. 2010. Cooperation and the commons. Science 330 (6006): 923–24.</mixed-citation>
         </ref>
         <ref id="ref78">
            <mixed-citation publication-type="other">Watts, M.E., I.R. Ball, R.S. Stewart, C.J. Klein, K. Wilson, C. Steinback, R. Lourival, et al. 2009. Marxan with zones: software for optimal conservation based land-and sea-use zoning. Environmental Modelling &amp; Software 24(12): 1513–21.</mixed-citation>
         </ref>
         <ref id="ref79">
            <mixed-citation publication-type="other">Welcomme, R. L. 1999. A review of a model for qualitative evaluation of exploitation levels in multi-species fisheries. Fisheries Management and Ecology 6(1): 1–19.</mixed-citation>
         </ref>
         <ref id="ref80">
            <mixed-citation publication-type="other">Welcomme, R. L., K. O. Winemiller, and I. G. Cowx. 2006. Fish environmental guilds as a tool for assessment of ecological condition of rivers. River Research and Applications 22(3): 377–96.</mixed-citation>
         </ref>
         <ref id="ref81">
            <mixed-citation publication-type="other">Welcomme, R.L, I.G. Cowx, D. Coates, C. Béné, S. Funge-Smith, A. Halls, and K. Lorenzen. 2010. Inland capture fisheries. Philosophical transactions of the Royal Society of B. 365 (1554): 2881–96.</mixed-citation>
         </ref>
         <ref id="ref82">
            <mixed-citation publication-type="other">Wilson, J., A. Hayden, and M. Kersula. 2013. The governance of diverse, multi-scale fisheries in which there is a lot to learn. Fisheries Research 141 (April): 24–30.</mixed-citation>
         </ref>
         <ref id="ref83">
            <mixed-citation publication-type="other">Young, C., S. Sheridan, S. Davies, and A. Hjort. 2012. Climate change implications for fishing communities in the Lake Chad Basin. what have we learned and what can we do better? In: FAO/Lake Chad Basin Commission Workshop. eds. Young, C. S. Sheridan, S. Davies, and A. Hjort. N’Djamena, Chad. 18-10 November, 2011. Rome: FAO - Fisheries and Aquaculture Proceedings 25.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
