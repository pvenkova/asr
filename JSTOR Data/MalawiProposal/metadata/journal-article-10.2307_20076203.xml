<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">annaeconstat</journal-id>
         <journal-id journal-id-type="jstor">j50000093</journal-id>
         <journal-title-group>
            <journal-title>Annales d'Économie et de Statistique</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>ADRES (Association pour le Développement de la Recherche en Économie et en Statistique)</publisher-name>
         </publisher>
         <issn pub-type="ppub">0769489X</issn>
         <issn pub-type="epub">22726497</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/20076203</article-id>
         <title-group>
            <article-title>Median-Unbiased Estimation in Fixed-Effects Dynamic Panels</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Rodolfo</given-names>
                  <surname>Cermeño</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>1999</year>
         
            <day>1</day>
            <month>12</month>
            <year>1999</year>
         </pub-date>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">55/56</issue>
         <issue-id>i20076188</issue-id>
         <fpage>351</fpage>
         <lpage>368</lpage>
         <permissions>
            <copyright-statement>Copyright 1999 INSEE</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20076203"/>
         <abstract>
            <p>This paper extends Andrews' [1993] median-unbiased estimation for autoregressive/unit root time series to panel data dynamic fixed effects models. It is shown that median-unbiased estimation applies straightforwardly to models that include linear time trends as well as to those including more general time specific effects. Using Monte Carlo simulations, median-unbiased LSDV estimators are computed and found to be robust to groupwise heteroskedastic and cross-sectionally correlated disturbances. The behavior of these estimators in the presence of exogenous regressors as well as AR parameter heterogeneity is also evaluated in this paper. As an application, these estimators are used to evaluate conditional convergence in the cases of 48 USA states, 13 OECD countries, and two wider samples from Summers and Heston's Penn World Tables, with 57 and 100 countries. It is found that median-unbiased estimates support conditional convergence only among USA states and OECD countries. /// Cet article étend la méthode d'estimation sans biais de médiane des séries temporelles autorégressives et/ou avec racine unitaire proposée par Andrews [1993], aux modèles dynamiques de panel avec effets fixes. On montre que l'estimation sans biais de médiane s'applique aisément autant aux modéles contenant des tendances temporelles linéaires, qu'à ceux incluant des effets temporels spécifiques plus généraux. Par des simulations de Monte Carlo, les estimateurs à effets fixes sans biais de médiane sont calculés et se révèlent robustes à l'hétéroscédasticité spécifique de groupe et à la corrélation des résidus entre coupes transversales. Le comportement de ces estimateurs en présence de régresseurs exogènes ainsi que d'hétérogénéité du paramètre autorégressif est aussi évalué dans cet article. Les estimateurs sont utilisés dans une application dont l'objectif est d'étudier la convergence conditionnelle du produit par tête de 48 Etats des U.S.A., 13 pays membres de l'OCDE et deux échantillons plus importants provenant des Penn World Tables de Summers et Heston, avec 57 et 100 pays. Les résultats d'estimation sans biais de médiane sont en faveur de la convergence conditionnelle uniquement pour les Etats des U.S.A. et les pays membres de l'OCDE.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d708e126a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d708e133" publication-type="other">
Andrews [1993], p. 140</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d708e139" publication-type="other">
Maddala and Kim [1998], pp. 141-144</mixed-citation>
            </p>
         </fn>
         <fn id="d708e146a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d708e153" publication-type="other">
Imhof's [1961]</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d708e169a1310">
            <mixed-citation id="d708e173" publication-type="other">
Anderson T.W., Hsiao C. (1981). — "Estimation of Dynamic Models with Error
Components", Journal of the American Statistical Association, 76, pp. 598-606.</mixed-citation>
         </ref>
         <ref id="d708e183a1310">
            <mixed-citation id="d708e187" publication-type="other">
Anderson T.W., Hsiao C. (1982). — "Formulation and Estimation of Dynamic Models
Using Panel Data", Journal of Econometrics, 18, pp. 47-82.</mixed-citation>
         </ref>
         <ref id="d708e197a1310">
            <mixed-citation id="d708e201" publication-type="other">
Andrews D.W.K. (1993). — "Exactly Median-Unbiased Estimation of First Order
Autoregressive/Unit Root Models", Econometrica, 61, pp. 139-165.</mixed-citation>
         </ref>
         <ref id="d708e211a1310">
            <mixed-citation id="d708e215" publication-type="other">
Ahn S.C., Schmidt P. (1995). — "Efficient Estimation of Models for Dynamic Panel Data",
Journal of Econometrics, 68, pp. 5-27.</mixed-citation>
         </ref>
         <ref id="d708e226a1310">
            <mixed-citation id="d708e230" publication-type="other">
Arellano M., Bover O. (1995). — "Another Look at the Instrumental Variable Estimation
of Error-Components Models", Journal of Econometrics, 68, pp. 29-51.</mixed-citation>
         </ref>
         <ref id="d708e240a1310">
            <mixed-citation id="d708e244" publication-type="other">
Arellano M., Bond S. (1991). — "Some Tests of Specification for Panel Data: Monte
Carlo Evidence and Application to Employment Equations", Review of Economic
Studies, 58, pp. 277-297.</mixed-citation>
         </ref>
         <ref id="d708e257a1310">
            <mixed-citation id="d708e261" publication-type="other">
Arellano M. (1989). — "A Note on the Anderson-Hsiao Estimator for Panel Data",
Economic Letters, 31, pp. 337-341.</mixed-citation>
         </ref>
         <ref id="d708e271a1310">
            <mixed-citation id="d708e275" publication-type="other">
Balestra P., Nerlove M. (1966). — "Pooling Cross Section and Time Series Data in the
Estimation of a Dynamic Model: The Demand for Natural Gas", Econometrica, 34,
pp. 585-612.</mixed-citation>
         </ref>
         <ref id="d708e288a1310">
            <mixed-citation id="d708e292" publication-type="other">
Baltagi B.H. (1995). — Econometric Analysis of Panel Data, John Wiley.</mixed-citation>
         </ref>
         <ref id="d708e299a1310">
            <mixed-citation id="d708e303" publication-type="other">
Baumol W.J. (1986). — "Productivity Growth, Convergence and Welfare: What the Long-
Run Data Show ?", American Economic Review, 76, pp. 1072-1085.</mixed-citation>
         </ref>
         <ref id="d708e314a1310">
            <mixed-citation id="d708e318" publication-type="other">
Barro R. (1991). — "Economic Growth in a Cross Section of Countries", Quarterly
Journal of Economics, 106, pp. 407-443.</mixed-citation>
         </ref>
         <ref id="d708e328a1310">
            <mixed-citation id="d708e332" publication-type="other">
Barro R., Sala-i-Martin X. (1992). — "Convergence", Journal of Political Economy,
100, pp. 223-251.</mixed-citation>
         </ref>
         <ref id="d708e342a1310">
            <mixed-citation id="d708e346" publication-type="other">
Barro R., Sala-i-Martin X. (1995). — Economic Growth, McGraw Hill, USA.</mixed-citation>
         </ref>
         <ref id="d708e353a1310">
            <mixed-citation id="d708e357" publication-type="other">
Beggs J.J., Nerlove M. (1988). — "Biases in Dynamic Models with Fixed Effects",
Economic Letters, 26, pp. 29-31</mixed-citation>
         </ref>
         <ref id="d708e367a1310">
            <mixed-citation id="d708e371" publication-type="other">
Cermeño R. (1997). — "Performance of Various Estimators in Dynamic Panel Data
Models", Documento de Trabajo No. 103, División de Economía, CIDE, México.</mixed-citation>
         </ref>
         <ref id="d708e381a1310">
            <mixed-citation id="d708e385" publication-type="other">
De Long B. (1988). — "Productivity Growth, Convergence and Welfare: What the Long-
Run Data Show? Comment", American Economic Review, 68, pp. 1138-1154.</mixed-citation>
         </ref>
         <ref id="d708e396a1310">
            <mixed-citation id="d708e400" publication-type="other">
Evans P. (1998). — Using Panel Data to Evaluate Growth Theories", International
Economic Review, 39, pp. 295-306.</mixed-citation>
         </ref>
         <ref id="d708e410a1310">
            <mixed-citation id="d708e414" publication-type="other">
Evans P. (1997). — "How Fast Do Economies Converge ?", Review of Economics and
Statistics, 79, pp. 219-225.</mixed-citation>
         </ref>
         <ref id="d708e424a1310">
            <mixed-citation id="d708e428" publication-type="other">
Evans P. (1996). — "Using Cross-Country Variances to Evaluate Growth Theories",
Journal of Economic Dynamics and Control, 20, pp. 1027-1049.</mixed-citation>
         </ref>
         <ref id="d708e438a1310">
            <mixed-citation id="d708e442" publication-type="other">
Evans P. (1994). — "How to Estimate Growth Regressions Consistently", Mimeo,
Department of Economics, The Ohio State University.</mixed-citation>
         </ref>
         <ref id="d708e452a1310">
            <mixed-citation id="d708e456" publication-type="other">
Evans P., Karras G. (1996a). — "Convergence Revisited", Journal of Monetary
Economics, 37, pp. 249-266.</mixed-citation>
         </ref>
         <ref id="d708e466a1310">
            <mixed-citation id="d708e470" publication-type="other">
Evans P., Karras G. (1996b). — "Do Economies Converge ? Evidence from a Panel of
U.S. States", Review of Economics and Statistics, 78, pp. 384-388.</mixed-citation>
         </ref>
         <ref id="d708e481a1310">
            <mixed-citation id="d708e485" publication-type="other">
Hsiao C. (1986). — Analysis of Panel Data, Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d708e492a1310">
            <mixed-citation id="d708e496" publication-type="other">
Howitt P., Aghion P. (1998). — "Capital Accumulation and Innovation as Complementary
Factors in Long-Run Growth", Journal of Economic Growth, 3, pp. 111-130.</mixed-citation>
         </ref>
         <ref id="d708e506a1310">
            <mixed-citation id="d708e510" publication-type="other">
Imhof J.P. (1961). — "Computing the Distribution of Quadratic Forms in Normal
Variables", Biometrica, 48, pp. 419-32.</mixed-citation>
         </ref>
         <ref id="d708e520a1310">
            <mixed-citation id="d708e524" publication-type="other">
Islam N. (1995). — "Growth Empirics: A Panel Data Approach", Quarterly Journal of
Economics, 110, pp. 1127-1170.</mixed-citation>
         </ref>
         <ref id="d708e534a1310">
            <mixed-citation id="d708e538" publication-type="other">
Kiviet J.F. (1995). — "On Bias, Inconsistency, and Efficiency of Various Estimators in
Dynamic Panel Data Models", Journal of Econometrics, 68, pp. 3-78.</mixed-citation>
         </ref>
         <ref id="d708e548a1310">
            <mixed-citation id="d708e552" publication-type="other">
Lee K., Pesaran M.H., Smith R. (1995). — "Growth and Convergence: A Multi-Country
Empirical Analysis of the Solow Growth Model", Mimeo, University of Cambridge and
Birkbeck College.</mixed-citation>
         </ref>
         <ref id="d708e566a1310">
            <mixed-citation id="d708e570" publication-type="other">
Lee K., Pesaran M.H., Smith R. (1998). — "Growth Empirics: A Panel Data Approach A
Comment", Quarterly Journal of Economics, 113, pp. 319-324.</mixed-citation>
         </ref>
         <ref id="d708e580a1310">
            <mixed-citation id="d708e584" publication-type="other">
Maddala G.S. (1997a). — "On the Use of Panel Data Models with Cross Country Data",
Paper presented at the 7th International Conference on Panel Data, Paris, June. in
Annales d'Économie et de Statistique, 1999, this issue.</mixed-citation>
         </ref>
         <ref id="d708e597a1310">
            <mixed-citation id="d708e601" publication-type="other">
Maddala G.S. (1997b). — "Recent Developments in Dynamic Econometric Modelling: A
Personal Viewpoint", Paper presented at the Annual Meeting of the Political
Methodology Group, Columbus, July, Political Analysis, 7, 1998.</mixed-citation>
         </ref>
         <ref id="d708e614a1310">
            <mixed-citation id="d708e618" publication-type="other">
Maddala G.S. (1971). — "The Use of Variance Components Models in Pooling Cross
Section and Time Series Data", Econometrica, 39, pp. 341-358.</mixed-citation>
         </ref>
         <ref id="d708e628a1310">
            <mixed-citation id="d708e632" publication-type="other">
Maddala G.S., Wu S. (1997). — "Cross Country Growth Regressions: Problems of
Heterogeneity, Stability and Interpretation", Manuscript, Department of Economics, The
Ohio State University.</mixed-citation>
         </ref>
         <ref id="d708e645a1310">
            <mixed-citation id="d708e649" publication-type="other">
Maddala G.S., Kim I.M. (1998). — Unit Roots, Cointegration, and Structural Change,
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d708e660a1310">
            <mixed-citation id="d708e664" publication-type="other">
Maddison A. (1991). — Dynamic Forces in Capitalist Development, Oxford University
Press.</mixed-citation>
         </ref>
         <ref id="d708e674a1310">
            <mixed-citation id="d708e678" publication-type="other">
Mankiw N.G., Romer D., Weil D.N. (1992). — "A Contribution to the Empirics of
Economic Growth", Quarterly Journal of Economics, 107, pp. 407-438.</mixed-citation>
         </ref>
         <ref id="d708e688a1310">
            <mixed-citation id="d708e692" publication-type="other">
Matyas L., Sevestre P. (Eds.) (1996). — The Econometrics of Panel Data, A Handbook of
the Theory with Applications, Second Revised Ed., Kluwer Academic Publishers.</mixed-citation>
         </ref>
         <ref id="d708e702a1310">
            <mixed-citation id="d708e706" publication-type="other">
Nickell S. (1981). — "Biases in Dynamic Models with Fixed Effects", Econometrica, 49,
pp. 1417-1426.</mixed-citation>
         </ref>
         <ref id="d708e716a1310">
            <mixed-citation id="d708e720" publication-type="other">
Quah D. (1994). — "Empirics for Economic Growth and Convergence", Mimeo, London
School of Economics.</mixed-citation>
         </ref>
         <ref id="d708e730a1310">
            <mixed-citation id="d708e734" publication-type="other">
Quah D. (1993a). — "Galton's Fallacy and Tests of the Convergence Hypothesis",
Scandinavian Journal of Economics 95, pp. 427-443.</mixed-citation>
         </ref>
         <ref id="d708e745a1310">
            <mixed-citation id="d708e749" publication-type="other">
Quah D. (1993b). — "Empirical Cross-Section Dynamics in Economic Growth", European
Economic Review 37, pp. 426-434.</mixed-citation>
         </ref>
         <ref id="d708e759a1310">
            <mixed-citation id="d708e763" publication-type="other">
Sevestre P., Trognon A. (1985). — "A Note on Autoregressive Error Components
Models", Journal of Econometrics 28, pp. 231-245.</mixed-citation>
         </ref>
         <ref id="d708e773a1310">
            <mixed-citation id="d708e777" publication-type="other">
Summers L., Heston C. (1991). — "The Penn World Tables (Mark 5): An Expanded Set of
International Comparisons, 1950-1988", Quarterly Journal of Economics 106, pp. 327-68.</mixed-citation>
         </ref>
         <ref id="d708e787a1310">
            <mixed-citation id="d708e791" publication-type="other">
Summers L., Heston C. (1995). — "The Penn World Tables, Version 5.6", Online Data,
National Bureau of Economic Research, Cambridge, MA.</mixed-citation>
         </ref>
         <ref id="d708e801a1310">
            <mixed-citation id="d708e805" publication-type="other">
Wallace T.D., Hussain A. (1969). — "The Use of Error Components Models in
Combining Cross-Section and Time-Series Data", Econometrica 37, pp. 55-72.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
