<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jfolkrese</journal-id>
         <journal-id journal-id-type="jstor">j100740</journal-id>
         <journal-title-group>
            <journal-title>Journal of Folklore Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Indiana University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">07377037</issn>
         <issn pub-type="epub">15430413</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4640209</article-id>
         <title-group>
            <article-title>Folk or Lore? The Stake in Dichotomies</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Elliott</given-names>
                  <surname>Oring</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>9</month>
            <year>2006</year>
         
            <day>1</day>
            <month>12</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">43</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i413783</issue-id>
         <fpage>205</fpage>
         <lpage>218</lpage>
         <page-range>205-218</page-range>
         <permissions>
            <copyright-statement>Copyright 2006 Department of Folklore and Ethnomusicology, Indiana University</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4640209"/>
         <abstract>
            <p>The divide between folklorists is not between the academy and the public sector. Academic and public sector folklorists are educators, and the differences between them are largely matters of contact hours, venue, and the preparation of the audience. The great divide between folklorists is between those who regard folklore as an intellectual inquiry and those who regard it as an ethical enterprise. The first approach puts the premium on the "lore" and pursues questions concerning tradition, transmission, art, and identity. The second puts the premium on the "folk" and the desire to ameliorate the circumstances of marginalized social, ethnic, religious, or occupational groups in the effort to create a more "humane and just world." Rather than being complementary, the approaches are at odds: the intellectual approach starts with questions while the ethical approach begins with answers. The distinction between these approaches becomes most visible when laboriously won knowledge of folklore processes is discounted or ignored in the pursuit of socially and culturally reparative agendas.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1695e132a1310">
            <label>1</label>
            <mixed-citation id="d1695e139" publication-type="other">
1997:7</mixed-citation>
         </ref>
         <ref id="d1695e146a1310">
            <label>2</label>
            <mixed-citation id="d1695e153" publication-type="other">
Davis (1996)</mixed-citation>
         </ref>
         <ref id="d1695e160a1310">
            <label>3</label>
            <mixed-citation id="d1695e167" publication-type="other">
1998:304</mixed-citation>
         </ref>
         <ref id="d1695e174a1310">
            <label>4</label>
            <mixed-citation id="d1695e181" publication-type="other">
1997</mixed-citation>
            <mixed-citation id="d1695e187" publication-type="other">
2006:29</mixed-citation>
         </ref>
         <ref id="d1695e195a1310">
            <label>5</label>
            <mixed-citation id="d1695e204" publication-type="other">
Proschan (1992)</mixed-citation>
         </ref>
         <ref id="d1695e211a1310">
            <label>6</label>
            <mixed-citation id="d1695e218" publication-type="other">
Jay (1974)</mixed-citation>
         </ref>
         <ref id="d1695e225a1310">
            <label>8</label>
            <mixed-citation id="d1695e232" publication-type="other">
Zinn 1970:40</mixed-citation>
         </ref>
         <ref id="d1695e239a1310">
            <label>10</label>
            <mixed-citation id="d1695e246" publication-type="other">
p. 385</mixed-citation>
            <mixed-citation id="d1695e252" publication-type="other">
p. 443</mixed-citation>
         </ref>
         <ref id="d1695e259a1310">
            <label>11</label>
            <mixed-citation id="d1695e266" publication-type="other">
Toelken 2004:443</mixed-citation>
         </ref>
         <ref id="d1695e273a1310">
            <label>12</label>
            <mixed-citation id="d1695e280" publication-type="other">
Toelken 1998:383</mixed-citation>
         </ref>
         <ref id="d1695e288a1310">
            <label>13</label>
            <mixed-citation id="d1695e295" publication-type="other">
Galtung 1967</mixed-citation>
            <mixed-citation id="d1695e301" publication-type="other">
Hymes 1974:49</mixed-citation>
         </ref>
         <ref id="d1695e308a1310">
            <label>14</label>
            <mixed-citation id="d1695e315" publication-type="other">
http://josephbruchac.
com/bruchac_biography.html</mixed-citation>
         </ref>
         <ref id="d1695e325a1310">
            <label>15</label>
            <mixed-citation id="d1695e332" publication-type="other">
New York Times 1992</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1695e348a1310">
            <mixed-citation id="d1695e352" publication-type="book">
Abrahams, Roger
1992 "The Public, the Folklorist, and the Public Folklorist." In Public Folklore,
ed. Robert Baron and Nicholas R. Spitzer, 17-27. Washington, D.C.:
Smithsonian Institution Press.<person-group>
                  <string-name>
                     <surname>Abrahams</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Public, the Folklorist, and the Public Folklorist</comment>
               <fpage>17</fpage>
               <source>Public Folklore</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e390a1310">
            <mixed-citation id="d1695e394" publication-type="journal">
Botkin, Benjamin A.
1953 "Applied Folklore: Creating Understanding through Folklore." Southern
Folklore Quarterly 17:199-206.<person-group>
                  <string-name>
                     <surname>Botkin</surname>
                  </string-name>
               </person-group>
               <fpage>199</fpage>
               <volume>17</volume>
               <source>Southern Folklore Quarterly</source>
               <year>1953</year>
            </mixed-citation>
            <mixed-citation id="d1695e428" publication-type="journal">
1958 "We Call It 'Living Lore.'" New York Folklore Quarterly 14:189-201.<person-group>
                  <string-name>
                     <surname>Botkin</surname>
                  </string-name>
               </person-group>
               <fpage>189</fpage>
               <volume>14</volume>
               <source>New York Folklore Quarterly</source>
               <year>1958</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e457a1310">
            <mixed-citation id="d1695e461" publication-type="journal">
Bruchac, Joe
1990 "Storytelling: Oral History or Game of 'Telephone'?" American Folklore
Society Newsletter 19/2:3-4.<person-group>
                  <string-name>
                     <surname>Bruchac</surname>
                  </string-name>
               </person-group>
               <fpage>3</fpage>
               <volume>19</volume>
               <source>American Folklore Society Newsletter</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e496a1310">
            <mixed-citation id="d1695e500" publication-type="journal">
Burns, Thomas A.
1970 "A Model for Textual Variation in Folksong." Folklore Forum 2:49-56.<person-group>
                  <string-name>
                     <surname>Burns</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>2</volume>
               <source>Folklore Forum</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e533a1310">
            <mixed-citation id="d1695e537" publication-type="book">
Davis, Gerald L.
1992 "So Correct lor the Photograph': 'Fixing' the Ineffable, Ineluctable
African American." In Public Folklore, ed. Robert Baron and Nicholas
R. Spitzer, 105-18. Washington, D.C.: Smithsonian Institution Press.<person-group>
                  <string-name>
                     <surname>Davis</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">So Correct lor the Photograph': 'Fixing' the Ineffable, Ineluctable African American</comment>
               <fpage>105</fpage>
               <source>Public Folklore</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e575a1310">
            <mixed-citation id="d1695e579" publication-type="book">
Davis, Kenneth C.
1996. Dont Know Much about the Civil War: Everything You Need to Know about
America's Createst Conflict but Neuer Learned. New York: William Morrow.<person-group>
                  <string-name>
                     <surname>Davis</surname>
                  </string-name>
               </person-group>
               <source>Dont Know Much about the Civil War: Everything You Need to Know about America's Createst Conflict but Neuer Learned</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e608a1310">
            <mixed-citation id="d1695e612" publication-type="book">
Galtung, John
1967 "After Camelot." In The Rise and Fall of Project Camelot: Studies in the Rela-
tionship between Social Science and Practical Politics, ed. Irving L. Horowitz,
281-312. Cambridge, Mass.: MIT Press.<person-group>
                  <string-name>
                     <surname>Galtung</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">After Camelot</comment>
               <fpage>281</fpage>
               <source>The Rise and Fall of Project Camelot: Studies in the Relationship between Social Science and Practical Politics</source>
               <year>1967</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e650a1310">
            <mixed-citation id="d1695e654" publication-type="journal">
Goodale, Mark
2006 "Ethical Theory as Social Practice." American Anthropologist 108:25-37.<object-id pub-id-type="jstor">10.2307/3804729</object-id>
               <fpage>25</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1695e670a1310">
            <mixed-citation id="d1695e674" publication-type="book">
Hawes, Bess Lomax
1992. "Happy Birthday. Dear American Folklore Society: Reflections on the
Work and Mission of the Folklorist." In Public Folklore, ed. Robert Baron
and Nicholas R. Spitzer, 65-73. Washington, D.C.: Smithsonian Institu-
tion Press.<person-group>
                  <string-name>
                     <surname>Hawes</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Happy Birthday. Dear American Folklore Society: Reflections on the Work and Mission of the Folklorist</comment>
               <fpage>65</fpage>
               <source>Public Folklore</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e715a1310">
            <mixed-citation id="d1695e719" publication-type="book">
Hymes, Dell
1974 "The Uses of Anthropology: Critical, Political, Personal." In Reinventing
Anthropology, ed. Dell Hymes, 3-79. New York: Vintage.<person-group>
                  <string-name>
                     <surname>Hymes</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Uses of Anthropology: Critical, Political, Personal</comment>
               <fpage>3</fpage>
               <source>Reinventing Anthropology</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e755a1310">
            <mixed-citation id="d1695e759" publication-type="book">
Jav, Robert
1974 "Personal and Fxtrapersonal Vision in Anthropology." In Reinventing
Anthropology, ed. Dell Hymes, 367-81. New York: Vintage.<person-group>
                  <string-name>
                     <surname>Jav</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Personal and Fxtrapersonal Vision in Anthropology</comment>
               <fpage>367</fpage>
               <source>Reinventing Anthropology</source>
               <year>1974</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e794a1310">
            <mixed-citation id="d1695e798" publication-type="journal">
Kerr, David
1991 "On Not Being a Folklorist: Field Methodology and the Reproduction
of Underdevelopment." Folklore 102:48-61.<object-id pub-id-type="jstor">10.2307/1260356</object-id>
               <fpage>48</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1695e817a1310">
            <mixed-citation id="d1695e821" publication-type="journal">
Kishenblatt-Gimblett, Barbara
1988 "Mistaken Dichotomies." Journal of American Folklore 101:140-55.<object-id pub-id-type="doi">10.2307/540105</object-id>
               <fpage>140</fpage>
            </mixed-citation>
            <mixed-citation id="d1695e836" publication-type="journal">
1998 "Folklore's Crisis." Journal of American Folklore 111:281-327.<object-id pub-id-type="doi">10.2307/541312</object-id>
               <fpage>281</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1695e849a1310">
            <mixed-citation id="d1695e853" publication-type="journal">
Kodish, Debora
1993 "On Coming of Age in the Sixties." Western Folklore 52:193-207.<object-id pub-id-type="doi">10.2307/1500086</object-id>
               <fpage>193</fpage>
            </mixed-citation>
            <mixed-citation id="d1695e868" publication-type="book">
1997 "Outside Memory." Paper presented at annual meeting of the American
Folklore Society, October 29-November 2, Austin, Texas.<person-group>
                  <string-name>
                     <surname>Kodish</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Outside Memory</comment>
               <source>Annual meeting of the American Folklore Society, October 29-November 2, Austin, Texas</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e897a1310">
            <mixed-citation id="d1695e901" publication-type="book">
Lawless, Elaine J.
2001 Women Escaping Violence: Empowerment through Narrative. Columbia: Uni-
versity of Missouri Press.<person-group>
                  <string-name>
                     <surname>Lawless</surname>
                  </string-name>
               </person-group>
               <source>Women Escaping Violence: Empowerment through Narrative</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e930a1310">
            <mixed-citation id="d1695e934" publication-type="journal">
Lindahl, Carl
2004 Afterword. Journal of Folklore Research 41:173-80.<object-id pub-id-type="jstor">10.2307/3814589</object-id>
               <fpage>173</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1695e951a1310">
            <mixed-citation id="d1695e955" publication-type="journal">
Lomax, Alan
1977 "Appeal for Cultural Equity." Journal of Communication 27:125-38.<person-group>
                  <string-name>
                     <surname>Lomax</surname>
                  </string-name>
               </person-group>
               <fpage>125</fpage>
               <volume>27</volume>
               <source>Journal of Communication</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e987a1310">
            <mixed-citation id="d1695e991" publication-type="book">
Narayan, Kirin
1989 Storytellers, Saints, and Scoundrels: Folk Narrative in Hindu Religious Teaching.
Philadelphia: University of Pennsylvania Press.<person-group>
                  <string-name>
                     <surname>Narayan</surname>
                  </string-name>
               </person-group>
               <source>Storytellers, Saints, and Scoundrels: Folk Narrative in Hindu Religious Teaching</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1020a1310">
            <mixed-citation id="d1695e1024" publication-type="journal">
New York Times
1992 "Supreme Court Voids Abenaki Land Claims." June 18, D23.<issue>June</issue>
               <fpage>D23</fpage>
               <source>New York Times</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1047a1310">
            <mixed-citation id="d1695e1051" publication-type="journal">
Norkunas, Martha
2004 "Narratives of Resistance and the Consequences of Resistance." Journal
of Folklore Research 41:105-23.<object-id pub-id-type="jstor">10.2307/3814585</object-id>
               <fpage>105</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1695e1070a1310">
            <mixed-citation id="d1695e1074" publication-type="journal">
Payne, Jessica M.
2004 "Critical Historiography of the Present: A Response to 'Looking Back,
Moving Forward' by Peggy Bulger." Journal of American Folklore 117:
337-43.<person-group>
                  <string-name>
                     <surname>Payne</surname>
                  </string-name>
               </person-group>
               <fpage>337</fpage>
               <volume>117</volume>
               <source>Journal of American Folklore</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1112a1310">
            <mixed-citation id="d1695e1116" publication-type="book">
Proschan, Frank
1992 "Fieldwork and Social Work: Folklore as a Helping Profession." In Public
Folklore, ed. Robert Baron and Nicholas R. Spitzer, 145-58. Washington,
D.C.: Smithsonian Institution Press.<person-group>
                  <string-name>
                     <surname>Proschan</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Fieldwork and Social Work: Folklore as a Helping Profession</comment>
               <fpage>145</fpage>
               <source>Public Folklore</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1155a1310">
            <mixed-citation id="d1695e1159" publication-type="book">
Seeger, Charles
1966 "The Folkness of the Non-Folk and the Non-Folkness of the Folk." In
Folklore and Society: Essays in Honor of Benj. A. Botkin, ed. Bruce Jackson,
1-9. Hatboro, Penn.: Folklore Associates.<person-group>
                  <string-name>
                     <surname>Seeger</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Folkness of the Non-Folk and the Non-Folkness of the Folk</comment>
               <fpage>1</fpage>
               <source>Folklore and Society: Essays in Honor of Benj</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1197a1310">
            <mixed-citation id="d1695e1201" publication-type="book">
Stoll, David
1999 Rigoberta Menchú and the Story of All Poor Guatemalans. Boulder, Colo.:
Westview.<person-group>
                  <string-name>
                     <surname>Stoll</surname>
                  </string-name>
               </person-group>
               <source>Rigoberta Menchú and the Story of All Poor Guatemalans</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1230a1310">
            <mixed-citation id="d1695e1234" publication-type="journal">
Toelken, Barre
1998 "The Yellowman Tapes. " Journal of American Folklore 111:381-91.<object-id pub-id-type="doi">10.2307/541046</object-id>
               <fpage>381</fpage>
            </mixed-citation>
            <mixed-citation id="d1695e1249" publication-type="journal">
2004 "Beauty Behind Me; Before Me." Journal of American Folklore 117:
441-45.<person-group>
                  <string-name>
                     <surname>Toelken</surname>
                  </string-name>
               </person-group>
               <fpage>441</fpage>
               <volume>117</volume>
               <source>Journal of American Folklore</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1281a1310">
            <mixed-citation id="d1695e1285" publication-type="book">
Vansina, Jan
1965 Oral Tradition: A Study in Historical Methodology, trans, by H. M. Wright.
London: Routledge and Kegan Paul.<person-group>
                  <string-name>
                     <surname>Vansina</surname>
                  </string-name>
               </person-group>
               <source>Oral Tradition: A Study in Historical Methodology</source>
               <year>1965</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1314a1310">
            <mixed-citation id="d1695e1318" publication-type="book">
Wilson, William A.
1979 "Folklore and History: Fact amid the Legends." In Readings in American
Folklore, ed. Jan Harold Brunvand, 449-66. New York: W. W. Norton.<person-group>
                  <string-name>
                     <surname>Wilson</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Folklore and History: Fact amid the Legends</comment>
               <fpage>449</fpage>
               <source>Readings in American Folklore</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d1695e1353a1310">
            <mixed-citation id="d1695e1357" publication-type="book">
Zinn, Howard
1970 The Politics of History. Boston: Beacon Press.<person-group>
                  <string-name>
                     <surname>Zinn</surname>
                  </string-name>
               </person-group>
               <source>The Politics of History</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
