<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socipsycquar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100565</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Social Psychology Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Sociological Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01902725</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3090108</article-id>
         <title-group>
            <article-title>Structural Location and Personality during the Transformation of Poland and Ukraine</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Melvin L.</given-names>
                  <surname>Kohn</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Wojciech</given-names>
                  <surname>Zaborowski</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Krystyna</given-names>
                  <surname>Janicka</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Valeriy</given-names>
                  <surname>Khmelko</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bogdan W.</given-names>
                  <surname>Mach</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Vladimir</given-names>
                  <surname>Paniotto</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kazimierz M.</given-names>
                  <surname>Slomczynski</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Cory</given-names>
                  <surname>Heyman</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bruce</given-names>
                  <surname>Podobnik</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">65</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i355531</issue-id>
         <fpage>364</fpage>
         <lpage>385</lpage>
         <page-range>364-385</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 The American Sociological Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3090108"/>
         <abstract>
            <p>In this paper we extend the analysis of social structure and personality, heretofore generally limited to the class positions and social-stratification levels of the employed, to encompass the nonemployed as well. We do this in a comparative study of Poland and Ukraine during an early period of their transformations from socialism to nascent capitalism. The analysis involves the systematic comparison of the nonemployed segments of the adult populations of these countries-those who were unemployed and looking for work, housewives, pensioners, and students-with the employed and with each other. The relationships between "structural location" and personality are statistically significant and nontrivial in magnitude, both for men and for women in both countries. Some of these relationships appear to reflect the extent of job loss and the resulting social compositions of the nonemployed segments of the population. The relationships result also, in substantial part, from the conditions of life experienced by the nonemployed segments of the population. Some of the nonemployed (most of all, the involuntary housewives) were subjected to economic duress, resulting in a sense of distress. Moreover, the conditions of life of unemployed and pensioned Poles (we do not have comparable data for Ukraine) were not as conducive to, or requiring of, complex activity as are those of gainfully employed men and women. This finding helps explain their relatively low levels of intellectual flexibility and self-directedness of orientation.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d176e281a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d176e288" publication-type="journal">
Kohn et al.
(1997)  </mixed-citation>
            </p>
         </fn>
         <fn id="d176e300a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d176e307" publication-type="journal">
Kohn et al. (2000:Table 4)  </mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d176e325a1310">
            <mixed-citation id="d176e329" publication-type="journal">
Baxter, Janeen. 1994. "Is Husband's Class
Enough? Class Location and Class Identity
in the United States, Sweden, Norway, and
Australia." American Sociological Review
59:220-35.<object-id pub-id-type="doi">10.2307/2096228</object-id>
               <fpage>220</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e355a1310">
            <mixed-citation id="d176e359" publication-type="journal">
Eisenberg, Philip and Paul F. Lazarsfeld. 1938.
"The Psychological Effects of
Unemployment." Psychological Bulletin
35:358-90.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Eisenberg</surname>
                  </string-name>
               </person-group>
               <fpage>358</fpage>
               <volume>35</volume>
               <source>Psychological Bulletin</source>
               <year>1938</year>
            </mixed-citation>
         </ref>
         <ref id="d176e397a1310">
            <mixed-citation id="d176e401" publication-type="journal">
Feather, N.T. 1997. "Economic Deprivation and
the Psychological Impact of
Unemployment." Australian Psychologist
32:37-45.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Feather</surname>
                  </string-name>
               </person-group>
               <fpage>37</fpage>
               <volume>32</volume>
               <source>Australian Psychologist</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d176e439a1310">
            <mixed-citation id="d176e443" publication-type="journal">
Goldthorpe, John H.1983. "Women and Class
Analysis: In Defense of the Conventional
View." Sociology17:465-88.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Goldthorpe</surname>
                  </string-name>
               </person-group>
               <fpage>465</fpage>
               <volume>17</volume>
               <source>Sociology</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d176e479a1310">
            <mixed-citation id="d176e483" publication-type="book">
House, James S.1981. "Social Structure and
Personality." Pp. 525-61 in Social
Psychology: Sociological Perspectives, edited
by Morris Rosenberg and Ralph H. Turner.
New York: Basic Books.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>House</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Social Structure and Personality</comment>
               <fpage>525</fpage>
               <source>Social Psychology: Sociological Perspectives</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d176e524a1310">
            <mixed-citation id="d176e528" publication-type="journal">
Jahoda, Marie. 1981. "Work, Employment, and
Unemployment: Values, Theories, and
Approaches in Social Research." American
Psychologist36:184-91.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Jahoda</surname>
                  </string-name>
               </person-group>
               <fpage>184</fpage>
               <volume>36</volume>
               <source>American Psychologist</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d176e566a1310">
            <mixed-citation id="d176e570" publication-type="book">
1982—Employment and Unemployment: A
Social-Psychological Analysis. Cambridge,
UK: Cambridge University Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Jahoda</surname>
                  </string-name>
               </person-group>
               <source>Employment and Unemployment: A Social-Psychological Analysis</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d176e599a1310">
            <mixed-citation id="d176e603" publication-type="book">
Kohn, Melvin L. [1969] 1977. Class and
Conformity: A Study in Values.2nd ed.
Chicago: University of Chicago Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kohn</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Class and Conformity: A Study in Values</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d176e635a1310">
            <mixed-citation id="d176e639" publication-type="journal">
—1987. "Cross-National Research As an
Analytic Strategy: American Sociological
Association, 1987 Presidential Address."
American Sociological Review52:713-31.<object-id pub-id-type="doi">10.2307/2095831</object-id>
               <fpage>713</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e662a1310">
            <mixed-citation id="d176e666" publication-type="journal">
1989— "Social Structure and Personality: A
Quintessentially Sociological Approach to
Social Psychology." Social Forces68:26-33.<object-id pub-id-type="doi">10.2307/2579217</object-id>
               <fpage>26</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e686a1310">
            <mixed-citation id="d176e690" publication-type="journal">
—1993. "Doing Social Research Under
Conditions of Radical Social Change: The
Biography of an Ongoing Research Project."
Social Psychology Quarterly56:4-20.<object-id pub-id-type="doi">10.2307/2786642</object-id>
               <fpage>4</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e713a1310">
            <mixed-citation id="d176e717" publication-type="journal">
Kohn, Melvin L., Atsushi Naoi, Carrie
Schoenbach, Carmi Schooler, and Kazimierz
M. Slomczynski. 1990. "Position in the Class
Structure and Psychological Functioning in
the United States, Japan, and Poland."
American Journal of Sociology95:964-1008.<object-id pub-id-type="jstor">10.2307/2780647</object-id>
               <fpage>964</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e746a1310">
            <mixed-citation id="d176e750" publication-type="book">
Kohn, Melvin L. and Carmi Schooler, with Joanne
Miller, Karen A. Miller, Carrie Schoenbach,
and Ronald Schoenberg. 1983. Work and
Personality: An Inquiry Into the Impact of
Social Stratification. Norwood, NJ: Ablex.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kohn</surname>
                  </string-name>
               </person-group>
               <source>Work and Personality: An Inquiry Into the Impact of Social Stratification</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d176e785a1310">
            <mixed-citation id="d176e789" publication-type="book">
Kohn, Melvin L. and Kazimierz M. Slomczynski,
with Carrie Schoenbach. 1990. Social
Structure and Self-Direction: A Comparative
Analysis of the United States and Poland.
Oxford: Basil Blackwell.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kohn</surname>
                  </string-name>
               </person-group>
               <source>Social Structure and Self-Direction: A Comparative Analysis of the United States and Poland</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d176e824a1310">
            <mixed-citation id="d176e830" publication-type="journal">
Kohn, Melvin L., Kazimierz M. Slomczynski,
Krystyna Janicka, Valeri Khmelko, Bogdan
W. Mach, Vladimir Paniotto, Wojciech
Zaborowski, Roberto Gutierrez, and Cory
Heyman. 1997. "Social Structure and
Personality Under Conditions of Radical
Social Change: A Comparative Analysis of
Poland and Ukraine." American Sociological
Review62:614-38.<object-id pub-id-type="doi">10.2307/2657430</object-id>
               <fpage>614</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e868a1310">
            <mixed-citation id="d176e872" publication-type="journal">
Kohn, Melvin L., Wojciech Zaborowski, Krystyna
Janicka, Bogdan W Mach, Valeriy Khmelko,
Kazimierz Slomczynski, Cory Heyman, and
Bruce Podobnik. 2000. "Complexity of
Activities and Personality Under Conditions
of Radical Social Change: A Comparative
Analysis of Poland and Ukraine." Social
Psychology Quarterly63:187-208.<object-id pub-id-type="doi">10.2307/2695868</object-id>
               <fpage>187</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e908a1310">
            <mixed-citation id="d176e912" publication-type="journal">
Naoi, Michiko. 1992. "Women and Stratification:
Frameworks and Indices." International
Journal of Japanese Sociology1:47-60.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Naoi</surname>
                  </string-name>
               </person-group>
               <fpage>47</fpage>
               <volume>1</volume>
               <source>International Journal of Japanese Sociology</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d176e947a1310">
            <mixed-citation id="d176e951" publication-type="journal">
Rosenberg, Morris and Leonard I. Pearlin. 1978.
"Social Class and Self-Esteem Among
Children and Adults." American Journal of
Sociology84:53-77.<object-id pub-id-type="jstor">10.2307/2777978</object-id>
               <fpage>53</fpage>
            </mixed-citation>
         </ref>
         <ref id="d176e974a1310">
            <mixed-citation id="d176e978" publication-type="book">
Schooler, Carmi, Melvin L. Kohn, Karen A. Miller,
and Joanne Miller. 1983. "Housework As
Work." Pp. 242-60 in Work and Personality:
An Inquiry Into the Impact of Social
Stratification, by Melvin L. Kohn and Carmi
Schooler, Norwood, NJ: Ablex.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Schooler</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Housework As Work</comment>
               <fpage>242</fpage>
               <source>Work and Personality: An Inquiry Into the Impact of Social Stratification</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d176e1022a1310">
            <mixed-citation id="d176e1026" publication-type="book">
Warr, Peter. 1987. Work, Unemployment, and
Mental Health. Oxford: Clarendon.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Warr</surname>
                  </string-name>
               </person-group>
               <source>Work, Unemployment, and Mental Health</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d176e1051a1310">
            <mixed-citation id="d176e1055" publication-type="book">
Wright, Erik Olin. 1978. Class, Crisis and the State.
London: New Left Books.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Wright</surname>
                  </string-name>
               </person-group>
               <source>Class, Crisis and the State</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d176e1080a1310">
            <mixed-citation id="d176e1084" publication-type="journal">
—1989. "Women in the Class Structure."
Politics and Society17:35-66.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Wright</surname>
                  </string-name>
               </person-group>
               <fpage>35</fpage>
               <volume>17</volume>
               <source>Politics and Society</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d176e1117a1310">
            <mixed-citation id="d176e1121" publication-type="journal">
Zawadski, Bohan and Paul F. Lazarsfeld. 1935.
"The Psychological Consequences of
Unemployment." Journal of Social
Psychology6:224-51.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Zawadski</surname>
                  </string-name>
               </person-group>
               <fpage>224</fpage>
               <volume>6</volume>
               <source>Journal of Social Psychology</source>
               <year>1935</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
