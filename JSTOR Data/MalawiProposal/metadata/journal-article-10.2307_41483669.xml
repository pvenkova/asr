<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">publchoi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000024</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Public Choice</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00485829</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15737101</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41483669</article-id>
         <title-group>
            <article-title>Special-interest groups and growth</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Dennis</given-names>
                  <surname>Coates</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jac C.</given-names>
                  <surname>Heckelman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bonnie</given-names>
                  <surname>Wilson</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">147</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3/4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40072335</issue-id>
         <fpage>439</fpage>
         <lpage>457</lpage>
         <permissions>
            <copyright-statement>© 2011 Springer</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41483669"/>
         <abstract>
            <p>This paper explores empirically the relation between special-interest groups and economic growth. Our analysis exploits new data on the number of groups observed across countries and time, in order to mitigate the identification problems associated with earlier studies. Also in contrast to earlier work, we examine the impact of groups on two sources of growth—capital accumulation and technological change—in addition to the impact of groups on output growth. The findings are consistent with Olson's (The rise and decline of nations: the political economy of economic growth, stagflation, and social rigidities. New Haven, Yale, 1982) claim that societies with greater numbers of interest groups grow slower, accumulate less capital, and experience reduced productivity growth relative to others.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d697e176a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d697e183" publication-type="other">
Heckelman (2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d697e190a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d697e197" publication-type="other">
Horgos and Zimmerman (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d697e204a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d697e211" publication-type="other">
Olson (1982: 46-47)</mixed-citation>
            </p>
         </fn>
         <fn id="d697e218a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d697e227" publication-type="other">
(Putnam 1993),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d697e233" publication-type="other">
Knack and Keefer (1997)</mixed-citation>
            </p>
         </fn>
         <fn id="d697e241a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d697e248" publication-type="other">
"OECD" nations are those nations that joined the OECD prior to 1985.</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d697e264a1310">
            <mixed-citation id="d697e268" publication-type="other">
Acemoglu, D., Johnson, S., &amp; Robinson, J. (2001). The colonial origins of comparative development: an
empirical investigation. American Economic Review, 91, 1369-1401.</mixed-citation>
         </ref>
         <ref id="d697e278a1310">
            <mixed-citation id="d697e282" publication-type="other">
Beck, T., Demirgûç-Kunt, A., &amp; Levine, R. (2005). SMEs, growth, and poverty: cross-country evidence.
Journal of Economic Growth, 10, 199-229.</mixed-citation>
         </ref>
         <ref id="d697e292a1310">
            <mixed-citation id="d697e296" publication-type="other">
Beck, T., Levine, R., &amp; Loayza, N. (2000). Finance and the sources of growth. Journal of Financial Eco-
nomics, 58, 261-300.</mixed-citation>
         </ref>
         <ref id="d697e306a1310">
            <mixed-citation id="d697e310" publication-type="other">
Becker, G. (1983). A theory of competition among pressure groups for political influence. Quarterly Journal
of Economics, 98, 371-397.</mixed-citation>
         </ref>
         <ref id="d697e321a1310">
            <mixed-citation id="d697e325" publication-type="other">
Belsley, D., Kuh, E., &amp; Welch, R. (1980). Regression diagnostics: identifying influential data and sources of
collinearity. New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d697e335a1310">
            <mixed-citation id="d697e339" publication-type="other">
Bernholz, R (1986). Growth of government, economic growth and individual freedom. Journal of Institutional
and Theoretical Economics, 142, 661-683.</mixed-citation>
         </ref>
         <ref id="d697e349a1310">
            <mixed-citation id="d697e353" publication-type="other">
Bischoff, I. (2003). Determinants of the increase in the number of interest groups in western democracies:
theoretical considerations and evidence from 21 OECD countries. Public Choice, 114, 197-218.</mixed-citation>
         </ref>
         <ref id="d697e363a1310">
            <mixed-citation id="d697e367" publication-type="other">
Choi, K. (1983). A statistical test of Olson's model. In D. Mueller (Ed.), The political economy of growth.
New Haven: Yale University Press.</mixed-citation>
         </ref>
         <ref id="d697e377a1310">
            <mixed-citation id="d697e381" publication-type="other">
Coates, D., &amp; Heckelman, J. C. (2003a). Interest groups and investment: a further test of the Olson hypothesis.
Public Choice, 117, 333-340.</mixed-citation>
         </ref>
         <ref id="d697e391a1310">
            <mixed-citation id="d697e395" publication-type="other">
Coates, D., &amp; Heckelman, J. C. (2003b). Absolute and relative effects of interest groups on the economy.
In J. C. Heckelman, &amp; D. Coates (Eds.), Collective choice: essays in honor of Mancur Olson. Berlin:
Springer.</mixed-citation>
         </ref>
         <ref id="d697e409a1310">
            <mixed-citation id="d697e413" publication-type="other">
Coates, D., &amp; Wilson, В. (2007). Interest group activity and long-run stock market performance. Public
Choice, 133, 343-358.</mixed-citation>
         </ref>
         <ref id="d697e423a1310">
            <mixed-citation id="d697e427" publication-type="other">
Coates, D., Heckelman, J. C., &amp; Wilson, B. (2007a). Determinants of interest group formation. Public Choice,
133, 377-391.</mixed-citation>
         </ref>
         <ref id="d697e437a1310">
            <mixed-citation id="d697e441" publication-type="other">
Coates, D., Heckelman, J. C., &amp; Wilson, B. (2007b). Special-interest groups and volatility. Economics Bul-
letin, 1-13.</mixed-citation>
         </ref>
         <ref id="d697e451a1310">
            <mixed-citation id="d697e455" publication-type="other">
Coates, D., Heckelman, J. C., &amp; Wilson, B. (2009). Democracy and volatility: do special-interest groups
matter? Manuscript.</mixed-citation>
         </ref>
         <ref id="d697e465a1310">
            <mixed-citation id="d697e469" publication-type="other">
Coates, D., Heckelman, J. C., &amp; Wilson, B. (2010). The political economy of investment: sclerotic effects
from interest groups. European Journal of Political Economy. doi:10.1016/j.ejpoleco.2009.12.003.</mixed-citation>
         </ref>
         <ref id="d697e479a1310">
            <mixed-citation id="d697e483" publication-type="other">
Crain, W., &amp; Lee, K. (1999). Economic growth regressions for the American states: a sensitivity analysis.
Economic Inquiry, 37, 242-257.</mixed-citation>
         </ref>
         <ref id="d697e494a1310">
            <mixed-citation id="d697e498" publication-type="other">
Cribari-Neto, F., &amp; Zarkos, S. (1999). Bootstrap methods for heteroskedastic regression models: evidence on
estimation and testing. Econometric Reviews, 18, 211-228.</mixed-citation>
         </ref>
         <ref id="d697e508a1310">
            <mixed-citation id="d697e512" publication-type="other">
Cribari-Neto, F., &amp; Zarkos, S. (2001). Heteroskedasticity-consistent covariance matrix estimation: White's
estimator and the bootstrap. Journal of Statistical Computation and Simulation, 68, 391-411.</mixed-citation>
         </ref>
         <ref id="d697e522a1310">
            <mixed-citation id="d697e526" publication-type="other">
Dombrovsky, V. (2008). Campaign contributions and firm performance: the 'Latvian way.' Manuscript.</mixed-citation>
         </ref>
         <ref id="d697e533a1310">
            <mixed-citation id="d697e537" publication-type="other">
Doner, R., &amp; Schneider, B. R. (2000). Business associations and economic development: why some associa-
tions contribute more than others. Business and Politics, 2, 261-288.</mixed-citation>
         </ref>
         <ref id="d697e547a1310">
            <mixed-citation id="d697e551" publication-type="other">
Fisman, R. (2001). Estimating the value of political connections. American Economic Review, 91, 1095-
1102.</mixed-citation>
         </ref>
         <ref id="d697e561a1310">
            <mixed-citation id="d697e565" publication-type="other">
Gawande, K., &amp; Bandyopadhyay, U. (2000). Is protection for sale? Evidence on the Grossman-Helpman
theory of endogenous protection. Review of Economics and Statistics, 82, 139-152.</mixed-citation>
         </ref>
         <ref id="d697e576a1310">
            <mixed-citation id="d697e580" publication-type="other">
Goldberg, P., &amp; Maggi, G. (1999). Protection for sale: an empirical investigation. American Economic Review,
89, 1135-1155.</mixed-citation>
         </ref>
         <ref id="d697e590a1310">
            <mixed-citation id="d697e594" publication-type="other">
Gray, V., &amp; Lowery, D. (1988). Interest group politics and growth in the U.S. states. American Political
Science Review , 80, 109-131.</mixed-citation>
         </ref>
         <ref id="d697e604a1310">
            <mixed-citation id="d697e608" publication-type="other">
Grier, К. В., Munger, M. С., &amp; Roberts, В. E. (1994). The determinants of industry political activity, 1978-
1986. American Political Science Review, 88, 911-926.</mixed-citation>
         </ref>
         <ref id="d697e618a1310">
            <mixed-citation id="d697e622" publication-type="other">
Hall, R., &amp; Jones, C. (1999). Why do some countries produce so much more output per worker than others?
The Quarterly Journal of Economics, 114, 83-116.</mixed-citation>
         </ref>
         <ref id="d697e632a1310">
            <mixed-citation id="d697e636" publication-type="other">
Heckelman, J. C. (2000). Consistent estimates of the impact of special interest groups on economic growth.
Public Choice, 104, 319-327.</mixed-citation>
         </ref>
         <ref id="d697e646a1310">
            <mixed-citation id="d697e650" publication-type="other">
Heckelman, J. C. (2007). Explaining the rain: the rise and decline of nations after 25 years. Southern Eco-
nomic Journal, 74, 18-33.</mixed-citation>
         </ref>
         <ref id="d697e661a1310">
            <mixed-citation id="d697e665" publication-type="other">
Horgos, D., &amp; Zimmerman, K. (2009). Interest groups and economic performance: some new evidence.
Public Choice, 138, 301-315.</mixed-citation>
         </ref>
         <ref id="d697e675a1310">
            <mixed-citation id="d697e679" publication-type="other">
Jayachandran, S. (2006). The Jeffords effect. Journal of Law and Economics, 49, 397-425.</mixed-citation>
         </ref>
         <ref id="d697e686a1310">
            <mixed-citation id="d697e690" publication-type="other">
К. G. Saur Verlag KG (1973, 1985, 1995). World guide to trade associations.</mixed-citation>
         </ref>
         <ref id="d697e697a1310">
            <mixed-citation id="d697e701" publication-type="other">
Kennelly, В., &amp; Murrell, P. (1991). Industry characteristics and interest group formation: an empirical study.
Public Choice, 70, 21-40.</mixed-citation>
         </ref>
         <ref id="d697e711a1310">
            <mixed-citation id="d697e715" publication-type="other">
Knack, S. (2003). Groups, growth and trust: cross-country evidence on the Olson and Putnam hypotheses.
Public Choice, 117, 341-355.</mixed-citation>
         </ref>
         <ref id="d697e725a1310">
            <mixed-citation id="d697e729" publication-type="other">
Knack, S., &amp; Keefer, P. (1997). Does social capital have an economic payoff? A cross country investigation.
The Quarterly Journal of Economics, 112, 1251-1288.</mixed-citation>
         </ref>
         <ref id="d697e740a1310">
            <mixed-citation id="d697e744" publication-type="other">
Kroszner, R., &amp; Strahan, P. (1999). What drives deregulation? Economics and politics of the relaxation of
bank branching restrictions in the United States. Quarterly Journal of Economics, 1437-1467.</mixed-citation>
         </ref>
         <ref id="d697e754a1310">
            <mixed-citation id="d697e758" publication-type="other">
La Porta, R., Lopez-de-Silanes, F., Shleifer, A., &amp; Vishny, R. (1997). Legal determinants of external finance.
Journal of Finance, 52, 1131-1150.</mixed-citation>
         </ref>
         <ref id="d697e768a1310">
            <mixed-citation id="d697e772" publication-type="other">
La Porta, R., Lopez-de-Silanes, F., Shleifer, A., &amp; Vishny, R. (1998). Law and finance. Journal of Political
Economy, 106, 1113-1155.</mixed-citation>
         </ref>
         <ref id="d697e782a1310">
            <mixed-citation id="d697e786" publication-type="other">
La Porta, R., Lopez-de-Silanes, F., Shleiier, A., &amp; Vishny, R. (1999). The quality or government. Journal of
Law ; Economics, and Organization, 15, 222-279.</mixed-citation>
         </ref>
         <ref id="d697e796a1310">
            <mixed-citation id="d697e800" publication-type="other">
Landes, D. (1998). The wealth and poverty of nations : why some are so rich and some so poor. New York:
Norton.</mixed-citation>
         </ref>
         <ref id="d697e810a1310">
            <mixed-citation id="d697e814" publication-type="other">
Li, W., &amp; Xu, L. (2002). The political economy of privatization and competition: cross-country evidence from
the telecommunications sector. Journal of Comparative Economics, 30, 439-462.</mixed-citation>
         </ref>
         <ref id="d697e825a1310">
            <mixed-citation id="d697e829" publication-type="other">
MacKinnon, J., &amp; White, H. (1985). Some heteroskedasticity-consistent covariance matrix estimators with
improved finite-sample properties. Journal of Econometrics, 29, 305-325.</mixed-citation>
         </ref>
         <ref id="d697e839a1310">
            <mixed-citation id="d697e843" publication-type="other">
Mobarak, A. (2005). Democracy, volatility and development. The Review of Economics and Statistics, 87,
348-361.</mixed-citation>
         </ref>
         <ref id="d697e853a1310">
            <mixed-citation id="d697e857" publication-type="other">
Mueller, D., &amp; Murrell, P. (1986). Interest groups and the size of government. Public Choice, 48, 125-145.</mixed-citation>
         </ref>
         <ref id="d697e864a1310">
            <mixed-citation id="d697e868" publication-type="other">
Murrell, P. (1984). An examination of the factors affecting the formation of interest groups in OECD. Public
Choice, 43, 151-171.</mixed-citation>
         </ref>
         <ref id="d697e878a1310">
            <mixed-citation id="d697e882" publication-type="other">
Olson, M. (1965). The logic of collective action. Cambridge: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d697e889a1310">
            <mixed-citation id="d697e893" publication-type="other">
Olson, M. (1982). The rise and decline of nations: the political economy of economic growth, stagflation, and
social rigidities. New Haven: Yale.</mixed-citation>
         </ref>
         <ref id="d697e904a1310">
            <mixed-citation id="d697e908" publication-type="other">
Pritchett, L. (2000). Understanding patterns of economic growth: searching for hills among plateaus, moun-
tains and plains. World Bank Economic Review, 14, 221-250.</mixed-citation>
         </ref>
         <ref id="d697e918a1310">
            <mixed-citation id="d697e922" publication-type="other">
Putnam, R. D. (1993). Making democracy work: civic traditions in modern Italy. Princeton: Princeton Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d697e932a1310">
            <mixed-citation id="d697e936" publication-type="other">
Rajan, R., &amp; Zingales, L. (2003). The great reversals: the politics of financial development in the 20th century.
Journal of Financial Economics, 69, 5-50.</mixed-citation>
         </ref>
         <ref id="d697e946a1310">
            <mixed-citation id="d697e950" publication-type="other">
Ramey, G., &amp; Ramey, V. (1995). Cross-country evidence on the link between volatility and growth. American
Economic Review, 85, 1138-1151.</mixed-citation>
         </ref>
         <ref id="d697e960a1310">
            <mixed-citation id="d697e964" publication-type="other">
Roberts, B. (1990). A dead senator tells no lies: seniority and the distribution of federal benefits. American
Journal of Political Science, 34, 31-58.</mixed-citation>
         </ref>
         <ref id="d697e974a1310">
            <mixed-citation id="d697e978" publication-type="other">
Weede, E. (1986). Sectoral reallocation, distributional coalitions and the welfare-state as determinants of
economic growth rates in industrialized democracies. European Journal of Political Research, 14, 501-
519.</mixed-citation>
         </ref>
         <ref id="d697e992a1310">
            <mixed-citation id="d697e996" publication-type="other">
Whitely, P. (1983). The political economy of economic growth. European Journal of Political Research, 11,
197-213.</mixed-citation>
         </ref>
         <ref id="d697e1006a1310">
            <mixed-citation id="d697e1010" publication-type="other">
Wittman, D. (1995). The myth of democratic failure: why political institutions are efficient. Chicago: The
University of Chicago Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
