<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">kewbulletin</journal-id>
         <journal-id journal-id-type="jstor">j101220</journal-id>
         <journal-title-group>
            <journal-title>Kew Bulletin</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Stationery Office</publisher-name>
         </publisher>
         <issn pub-type="ppub">00755974</issn>
         <issn pub-type="epub">1874933X</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/4110385</article-id>
         <title-group>
            <article-title>Classification and Species of Platostoma and Its Relationship with Haumaniastrum (Labiatae)</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Alan</given-names>
                  <surname>Paton</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">52</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i381274</issue-id>
         <fpage>257</fpage>
         <lpage>292</lpage>
         <page-range>257-292</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 The Board of Trustees of The Royal Botanic Gardens, Kew</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4110385"/>
         <abstract>
            <p>The generic delimitation of Platostoma P. Beauv., Haumaniastrum P. A. Duvign. &amp; Plancke and previously recognized allied genera is examined on a global basis for the first time this century. A parsimony analysis was carried out and the generic concept applied is discussed. Platostoma and Haumaniastrum are recognized here with Acrocephalus, Geniosporum, Ceratanthus, Octomeron, Mesona, Nosema and Limniboza being placed in the synonomy of Platostoma. The previously recognized Acrocephalus group of genera is renamed the Platostoma group. Benguellia is considered to be more closely related to Orthosiphon than to the Platostoma group. Platostoma is a paraphyletic genus due to the exclusion of Haumaniastrum. Platostoma is divided into three subgenera: subgen. Acrocephalus is broadly equivalent to Asiatic species of Geniosporum and Acrocephalus, but with the inclusion of some African and Madagascan species: subgen. Octomeron is monotypic; subgen. Platostoma is divided into four sections, sect. Platostoma, sect. Ceratanthus, sect. Mesona and sect. Limniboza. Platostoma is found in tropical Africa, Madagascar, tropical Asia and N Queensland. The genus has not been fully revised, but 45 species are recognized with 4 described for the first time. A key to the infrageneric taxa is provided and the recognized species are listed with their types, synonomy and distribution. The mainly African genus Haumaniastrum is found to be more closely related to the African Platostoma sect. Limniboza rather than the Asiatic sect. Acrocephalus as had been previously proposed.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d267e214a1310">
            <mixed-citation id="d267e218" publication-type="journal">
Bentham, G. (1829). Acrocephalus in Edward's Bot. Reg. sub t. 1282.<person-group>
                  <string-name>
                     <surname>Bentham</surname>
                  </string-name>
               </person-group>
               <fpage>1282</fpage>
               <source>Bot. Reg.</source>
               <year>1829</year>
            </mixed-citation>
         </ref>
         <ref id="d267e243a1310">
            <mixed-citation id="d267e247" publication-type="journal">
--- (1830). Ocimoideae in Edward's Bot. Reg. sub t. 1300.<person-group>
                  <string-name>
                     <surname>Bentham</surname>
                  </string-name>
               </person-group>
               <fpage>1300</fpage>
               <source>Bot. Reg.</source>
               <year>1830</year>
            </mixed-citation>
         </ref>
         <ref id="d267e272a1310">
            <mixed-citation id="d267e276" publication-type="book">
--- (1832). Ocimoideae, Labiatarum Genera &amp; Species: 1 - 151. J. Ridgeway &amp; sons,
London.<person-group>
                  <string-name>
                     <surname>Bentham</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ocimoideae</comment>
               <fpage>1</fpage>
               <source>Labiatarum Genera &amp; Species</source>
               <year>1832</year>
            </mixed-citation>
         </ref>
         <ref id="d267e308a1310">
            <mixed-citation id="d267e312" publication-type="book">
--- (1848). Ocimoideae. In: A. P. de Candolle (ed.), Prodromus Systematis Naturalis
12: 30 - 148. Fortei &amp; Masson, Paris.<person-group>
                  <string-name>
                     <surname>Bentham</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ocimoideae</comment>
               <fpage>30</fpage>
               <volume>12</volume>
               <source>Prodromus Systematis Naturalis</source>
               <year>1848</year>
            </mixed-citation>
         </ref>
         <ref id="d267e348a1310">
            <mixed-citation id="d267e352" publication-type="book">
--- G. (1876). Ocimoideae. In: G. Bentham &amp; J. D. Hooker, Genera Plantarum 2:
1171-1179. Reeve, London.<person-group>
                  <string-name>
                     <surname>Bentham</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ocimoideae</comment>
               <fpage>1171</fpage>
               <volume>2</volume>
               <source>Genera Plantarum</source>
               <year>1876</year>
            </mixed-citation>
         </ref>
         <ref id="d267e387a1310">
            <mixed-citation id="d267e391" publication-type="book">
Blume, L.(1826). Mesona in Bijdragen tot de flora von Nederlandsch Indie: 838.
Lands Drukkerij, Batavia.<person-group>
                  <string-name>
                     <surname>Blume</surname>
                  </string-name>
               </person-group>
               <fpage>838</fpage>
               <source>Mesona in Bijdragen tot de flora von Nederlandsch Indie</source>
               <year>1826</year>
            </mixed-citation>
         </ref>
         <ref id="d267e420a1310">
            <mixed-citation id="d267e424" publication-type="book">
Briquet, J. (1897). Ocimoideae-Moschosminae. In: A. Engler, &amp; K. A. E., Prantl (eds),
Die Natürlichen Pflanzenfamilien 4, 364 - 374. W. Engelmann, Leipzig.<person-group>
                  <string-name>
                     <surname>Briquet</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Ocimoideae-Moschosminae</comment>
               <fpage>364</fpage>
               <volume>4</volume>
               <source>Die Natürlichen Pflanzenfamilien</source>
               <year>1897</year>
            </mixed-citation>
         </ref>
         <ref id="d267e459a1310">
            <mixed-citation id="d267e463" publication-type="book">
Brummitt, R. K (1996). In defence of paraphyletic taxa. In: L.J. G. van der Maesen,
X. M. van der Burgt &amp; J. M. van der Medenbach de Rooy (eds), The biodiversity
of African plants: 371 - 384. Kluwer Academic Press, Netherlands.<person-group>
                  <string-name>
                     <surname>Brummitt</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">In defence of paraphyletic taxa</comment>
               <fpage>371</fpage>
               <source>The biodiversity of African plants</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d267e498a1310">
            <mixed-citation id="d267e502" publication-type="book">
Cantino, P. D., Harley, R. M. &amp; Wagstaff, S.J. (1992). Genera of Labiatae: Status and
Classification. In: R. M. Harley &amp; T. Reynolds (eds.), Advances in Labiate Science:
507 - 502. Royal Botanic Gardens, Kew.<person-group>
                  <string-name>
                     <surname>Cantino</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Genera of Labiatae: Status and Classification</comment>
               <fpage>507</fpage>
               <source>Advances in Labiate Science</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d267e537a1310">
            <mixed-citation id="d267e541" publication-type="other">
Chermsirivathana, C. 1964. Labiatae of Thailand. M.Sc. thesis. Aberdeen University.</mixed-citation>
         </ref>
         <ref id="d267e549a1310">
            <mixed-citation id="d267e553" publication-type="book">
Codd, L. E. (1985). Geniosporum. In: O. A. Leistner (ed.), Flora of Southern Africa
28, 4: 215 - 217. Botanical Research Institute, Dept. of Agriculture and Water
Supply, Pretoria.<person-group>
                  <string-name>
                     <surname>Codd</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Geniosporum</comment>
               <fpage>215</fpage>
               <volume>4</volume>
               <source>Flora of Southern Africa 28</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d267e591a1310">
            <mixed-citation id="d267e595" publication-type="journal">
de Queiroz, K. &amp; Gauthier, J. (1992). Phylogenetic Taxonomy. Annual Rev. Ecol.
Syst. 23: 449 - 480.<object-id pub-id-type="jstor">10.2307/2097296</object-id>
               <fpage>449</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e611a1310">
            <mixed-citation id="d267e615" publication-type="journal">
--- &amp; --- (1994). Toward a phylogenetic system of biological nomenclature.
Trends in Ecol. Evol. 9, 1: 27 - 31.<person-group>
                  <string-name>
                     <surname>de Queiroz</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>27</fpage>
               <volume>9</volume>
               <source>Trends in Ecol. Evol.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d267e650a1310">
            <mixed-citation id="d267e654" publication-type="book">
Doan, T. (1936). Labiatae. In: H. Lecomte (ed.), Flore Generale de l'Tndo-Chine 4:
915 - 1040. Masson, Paris.<person-group>
                  <string-name>
                     <surname>Doan</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Labiatae</comment>
               <fpage>915</fpage>
               <volume>4</volume>
               <source>Flore Generale de l'Tndo-Chine</source>
               <year>1936</year>
            </mixed-citation>
         </ref>
         <ref id="d267e689a1310">
            <mixed-citation id="d267e693" publication-type="journal">
Donoghue, M. J. &amp; Cantino, P. D. (1988). Paraphyly, ancestors and the goals of
taxonomy: a botanical defense of cladism. Bot. Rev. (Lancaster) 53: 1 - 52.<person-group>
                  <string-name>
                     <surname>Donoghue</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>53</volume>
               <source>Bot. Rev. (Lancaster)</source>
               <year>1988</year>
            </mixed-citation>
         </ref>
         <ref id="d267e725a1310">
            <mixed-citation id="d267e729" publication-type="journal">
---, Olmstead, R. J., Smith, J. F. &amp; Palmer, J. D. (1992). Phylogenetic relationships
of Dipsicales based on rbcL sequences. Ann. Missouri Bot. Gard. 79: 333 - 345.<object-id pub-id-type="doi">10.2307/2399772</object-id>
               <fpage>333</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e746a1310">
            <mixed-citation id="d267e750" publication-type="journal">
Duvigneaud, P. &amp; Plancke, J. (1959). Les "Acrocephalus" arborescents des plateaux
Katangais. Biol. Jaarb. 27: 214 - 257.<person-group>
                  <string-name>
                     <surname>Duvigneaud</surname>
                  </string-name>
               </person-group>
               <fpage>214</fpage>
               <volume>27</volume>
               <source>Biol. Jaarb.</source>
               <year>1959</year>
            </mixed-citation>
         </ref>
         <ref id="d267e782a1310">
            <mixed-citation id="d267e786" publication-type="book">
Fries, R. E. (1916). Limniboza in Wissenschuftliche Ergebnisse Der Schwedische
Rhodesia Kongo Expedition. Vol. 1 Botanische Untersuchungen: 277 - 279.
Gedruckt in Aftonbladets Druckerei, Stockholm.<person-group>
                  <string-name>
                     <surname>Fries</surname>
                  </string-name>
               </person-group>
               <fpage>277</fpage>
               <volume>1</volume>
               <source>Limniboza in Wissenschuftliche Ergebnisse Der Schwedische Rhodesia Kongo Expedition</source>
               <year>1916</year>
            </mixed-citation>
         </ref>
         <ref id="d267e821a1310">
            <mixed-citation id="d267e825" publication-type="journal">
Jeffrey, C. (1987). The concept of a genus. Newslett. Austral. Syst. Bot. Soc. 53: 27-
31.<person-group>
                  <string-name>
                     <surname>Jeffrey</surname>
                  </string-name>
               </person-group>
               <fpage>27</fpage>
               <volume>53</volume>
               <source>Newslett. Austral. Syst. Bot. Soc.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d267e857a1310">
            <mixed-citation id="d267e861" publication-type="journal">
Keng, H. (1978). Labiatae. In: G. G. J. van Steenis (ed.), Flora Malesiana 8, 3: 301
-394.<person-group>
                  <string-name>
                     <surname>Keng</surname>
                  </string-name>
               </person-group>
               <issue>3</issue>
               <fpage>301</fpage>
               <volume>8</volume>
               <source>Flora Malesiana</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d267e896a1310">
            <mixed-citation id="d267e900" publication-type="book">
Mueller, F. (1865). Plectranthus longicornis in Fragmenta Phytogeographiae Australiae
5: 51 - 52. Auctoritate Gubern. Colonie Victoria, Ex officina Joannis Ferres,
Melbourne.<person-group>
                  <string-name>
                     <surname>Mueller</surname>
                  </string-name>
               </person-group>
               <fpage>51</fpage>
               <volume>5</volume>
               <source>Plectranthus longicornis in Fragmenta Phytogeographiae Australiae</source>
               <year>1865</year>
            </mixed-citation>
         </ref>
         <ref id="d267e935a1310">
            <mixed-citation id="d267e939" publication-type="journal">
Nicolson, D. H. &amp; Sivadasan, M. (1980). Identification of Gomphrena hispida
Linnaeus with Acrocephalus (Lamiaceae). Taxon 29: 324 - 325.<object-id pub-id-type="doi">10.2307/1220300</object-id>
               <fpage>324</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e956a1310">
            <mixed-citation id="d267e960" publication-type="book">
Palisot de Beauvois, A. M. F.J. (1818). Platostoma in Flore D'Oware et de Benin en
Afrique 2: 61, t. 95. Fain, Paris.<person-group>
                  <string-name>
                     <surname>Palisot de Beauvois</surname>
                  </string-name>
               </person-group>
               <fpage>61</fpage>
               <volume>2</volume>
               <source>Platostoma in Flore D'Oware et de Benin en Afrique</source>
               <year>1818</year>
            </mixed-citation>
         </ref>
         <ref id="d267e992a1310">
            <mixed-citation id="d267e996" publication-type="journal">
Paton, A. (1992). A synopsis of Ocimum L. (Labiatae) in Africa. Kew Bull. 47: 405 - 437.<person-group>
                  <string-name>
                     <surname>Paton</surname>
                  </string-name>
               </person-group>
               <fpage>405</fpage>
               <volume>47</volume>
               <source>Kew Bull.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1025a1310">
            <mixed-citation id="d267e1029" publication-type="journal">
--- (1994). A revision of Endostemon (Labiatae). Kew Bull. 49: 673 - 716.<person-group>
                  <string-name>
                     <surname>Paton</surname>
                  </string-name>
               </person-group>
               <fpage>673</fpage>
               <volume>49</volume>
               <source>Kew Bull.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1058a1310">
            <mixed-citation id="d267e1062" publication-type="journal">
--- (1997). A revision of Haumaniastrum (Labiatae). Kew Bull. 52: 293 - 378.<person-group>
                  <string-name>
                     <surname>Paton</surname>
                  </string-name>
               </person-group>
               <fpage>293</fpage>
               <volume>52</volume>
               <source>Kew Bull.</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1091a1310">
            <mixed-citation id="d267e1095" publication-type="journal">
Prain, D. (1904). Nosema. In: Some new plants from Eastern Asia. J. Asiat. Soc.
Bengal, Pt. 2, Nat. Hist. 73: 20 - 21.<person-group>
                  <string-name>
                     <surname>Prain</surname>
                  </string-name>
               </person-group>
               <fpage>20</fpage>
               <volume>73</volume>
               <source>J. Asiat. Soc. Bengal, Pt. 2, Nat. Hist.</source>
               <year>1904</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1127a1310">
            <mixed-citation id="d267e1131" publication-type="journal">
Press, J. R. &amp; Sivarajan, V. V. (1989). The application of names of some Indian
species of Ocimum and Geniosporum (Labiatae). Bull. Brit. Mus. (Nat. Hist.), Bot.
19: 113-116.<person-group>
                  <string-name>
                     <surname>Press</surname>
                  </string-name>
               </person-group>
               <fpage>113</fpage>
               <volume>19</volume>
               <source>Bull. Brit. Mus. (Nat. Hist.), Bot.</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1167a1310">
            <mixed-citation id="d267e1171" publication-type="journal">
Robyns, W. (1943a). Note sur le genre Platostoma P. Beauv. (Labiatae- Ocimoideae).
Bull. Jard. Bot. État. 17: 15 - 26.<person-group>
                  <string-name>
                     <surname>Robyns</surname>
                  </string-name>
               </person-group>
               <fpage>15</fpage>
               <volume>17</volume>
               <source>Bull. Jard. Bot. État.</source>
               <year>1943</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1203a1310">
            <mixed-citation id="d267e1207" publication-type="journal">
--- (1943b). Un nouveau genre de Labiatae - Ocimoideae de l'Afrique centrale. Bull.
Jard. Bot. État. 17:27-31.<person-group>
                  <string-name>
                     <surname>Robyns</surname>
                  </string-name>
               </person-group>
               <fpage>27</fpage>
               <volume>17</volume>
               <source>Bull. Jard. Bot. État.</source>
               <year>1943</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1239a1310">
            <mixed-citation id="d267e1243" publication-type="journal">
--- (1966). On the status of Acrocephalus Benth. Bot. Not. 119: 185 - 195.<person-group>
                  <string-name>
                     <surname>Robyns</surname>
                  </string-name>
               </person-group>
               <fpage>185</fpage>
               <volume>119</volume>
               <source>Bot. Not.</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1272a1310">
            <mixed-citation id="d267e1276" publication-type="journal">
--- &amp; Lebrun, J. (1928). Révision des espèces congolaises du genre Acrocephalus
Benth. Ann. Soc. Sci. Bruxelles, Sèr. 48: 169 - 203.<person-group>
                  <string-name>
                     <surname>Robyns</surname>
                  </string-name>
               </person-group>
               <fpage>169</fpage>
               <volume>48</volume>
               <source>Ann. Soc. Sci. Bruxelles, Sèr.</source>
               <year>1928</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1308a1310">
            <mixed-citation id="d267e1312" publication-type="journal">
Ryding, O. (1992). Pericarp structure and phylogeny within Lamiaceae subfamily
Nepetoideae tribe Ocimeae. Nordic J. Bot. 12: 273 - 298.<person-group>
                  <string-name>
                     <surname>Ryding</surname>
                  </string-name>
               </person-group>
               <fpage>273</fpage>
               <volume>12</volume>
               <source>Nordic J. Bot.</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1344a1310">
            <mixed-citation id="d267e1348" publication-type="journal">
--- (1993). Pericarp structure and systematic positions of five genera of Lamiaceae
subfamily Nepetoideae tribe Ocimeae. Nordic J. Bot. 13: 631 - 635.<person-group>
                  <string-name>
                     <surname>Ryding</surname>
                  </string-name>
               </person-group>
               <fpage>631</fpage>
               <volume>13</volume>
               <source>Nordic J. Bot.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1381a1310">
            <mixed-citation id="d267e1385" publication-type="book">
Schrire, B. D. &amp; Lewis, G. P. (1996). Monophyly: a criterion for generic delimitation,
with special reference to Leguminosae. In: L. J. G. van der Maesen, X. M. van der
Burgt &amp; J. M. van der Medenbach de Rooy (eds), The biodiversity of African
plants: 353 - 370. Kluwer Academic Press, Netherlands.<person-group>
                  <string-name>
                     <surname>Schrire</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Monophyly: a criterion for generic delimitation, with special reference to Leguminosae</comment>
               <fpage>353</fpage>
               <source>The biodiversity of African plants</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1423a1310">
            <mixed-citation id="d267e1427" publication-type="book">
Swofford, D. L. (1993). Phylogenetic analysis using parsimony (PAUP), version 3.1.
Illinois Natural History Survey. Champaign.<person-group>
                  <string-name>
                     <surname>Swofford</surname>
                  </string-name>
               </person-group>
               <source>Phylogenetic analysis using parsimony (PAUP)</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1452a1310">
            <mixed-citation id="d267e1456" publication-type="journal">
Taylor, G. (1931). Benguellia in Gossweiler's Portuguese West African Plants. J. Bot.
69, Supp. 2: 156-157.<object-id pub-id-type="doi">10.2307/2441394</object-id>
               <fpage>156</fpage>
            </mixed-citation>
         </ref>
         <ref id="d267e1472a1310">
            <mixed-citation id="d267e1476" publication-type="journal">
--- (1936). Notes on Labiatae III. Ceratanthus. A collation of certain species allied
to Plectranthus. J. Bot. 74: 33-41.<person-group>
                  <string-name>
                     <surname>Taylor</surname>
                  </string-name>
               </person-group>
               <fpage>33</fpage>
               <volume>74</volume>
               <source>J. Bot.</source>
               <year>1936</year>
            </mixed-citation>
         </ref>
         <ref id="d267e1508a1310">
            <mixed-citation id="d267e1512" publication-type="journal">
Wu, C. Y. (1959). Revisio Labiatarum Sinensica, Elsholtzia. Acta Phytotax. Sin. 8: 49 - 50.<person-group>
                  <string-name>
                     <surname>Wu</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>8</volume>
               <source>Acta Phytotax. Sin.</source>
               <year>1959</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
