<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">annaeconstat</journal-id>
         <journal-id journal-id-type="jstor">j50000093</journal-id>
         <journal-title-group>
            <journal-title>Annales d'Économie et de Statistique</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>ADRES (Association pour le Développement de la Recherche en Économie et en Statistique)</publisher-name>
         </publisher>
         <issn pub-type="ppub">0769489X</issn>
         <issn pub-type="epub">22726497</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/27715167</article-id>
         <title-group>
            <article-title>Bayesian Estimation of Cox Models with Non-Nested Random Effects: An Application to the Ratification of ILO Conventions by Developing Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Guillaume</given-names>
                  <surname>Horny</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Bernhard</given-names>
                  <surname>Boockmann</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Dragana</given-names>
                  <surname>Djurdjevic</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>François</given-names>
                  <surname>Laisney</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>2008</year>
         
            <day>1</day>
            <month>3</month>
            <year>2008</year>
         </pub-date>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">89</issue>
         <issue-id>i27715159</issue-id>
         <fpage>193</fpage>
         <lpage>214</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 INSEE</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/27715167"/>
         <abstract>
            <p>We use a multivariate hazard model to analyse the ratification of ILO conventions by developing countries. The model accounts for two random effects: one at the country level, the other at the convention level. After investigating identification, we use a semi-parametric Bayesian approach based on the partial likelihood. We find diverging results between Bayesian and frequentist estimates concerning the importance of the two unobserved heterogeneities. /// Nous utilisons un modèle de durées multivarié pour analyser le comportement de ratification des pays en voie de développement. Le modèle comprend deux effets aléatoires: l'un au niveau du pays, l'autre au niveau de la convention. Après avoir établi l'identification, nous utilisons une approche bayésienne semi-paramétrique reposant sur la vraisemblance partielle. Les estimations bayésiennes et frésiennes fournissent des résultats différents quant à l'importance des deux hétérogénéités non observées.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d428e162a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d428e169" publication-type="other">
Lancaster [1990, pp. 195-201 and 267]</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d428e175" publication-type="other">
Bolstad and Manda [2001]</mixed-citation>
            </p>
         </fn>
         <fn id="d428e182a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d428e189" publication-type="other">
Ridder and Tunali, [1999]</mixed-citation>
            </p>
         </fn>
         <fn id="d428e196a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d428e203" publication-type="other">
Lancaster [1990]</mixed-citation>
            </p>
         </fn>
         <fn id="d428e210a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d428e217" publication-type="other">
Brooks and Morgan [2004]</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d428e223" publication-type="other">
Train [2003, Chapter 12, Table 12.2]</mixed-citation>
            </p>
         </fn>
         <fn id="d428e231a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d428e238" publication-type="other">
http://www.r-project.org/.</mixed-citation>
            </p>
         </fn>
         <fn id="d428e245a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d428e252" publication-type="other">
Boockmann [2001]</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d428e268a1310">
            <mixed-citation id="d428e272" publication-type="other">
Alvarez M., Cheibub J., Limongi F. and Przeworski A. (1996). – «Classifying political
regimes », Studies in Comparative International Development, 31(2), 2-36.</mixed-citation>
         </ref>
         <ref id="d428e282a1310">
            <mixed-citation id="d428e286" publication-type="other">
Bolstad W. and Manda S. (2001). – « Investigating child mortality in Malawi using family
and community random effects: a Bayesian analysis », Journal of the American Statistical
Association, 96, 12-19.</mixed-citation>
         </ref>
         <ref id="d428e299a1310">
            <mixed-citation id="d428e303" publication-type="other">
Boockmann B. (2001). – « The ratification of ILO conventions: a hazard rate analysis »,
Economics and Politics, 13, 281-309.</mixed-citation>
         </ref>
         <ref id="d428e313a1310">
            <mixed-citation id="d428e317" publication-type="other">
Breslow N. (1974). – « Covariance analysis of censored survival data», Biometrics, 30,
89-99.</mixed-citation>
         </ref>
         <ref id="d428e328a1310">
            <mixed-citation id="d428e332" publication-type="other">
Brooks S., K. R. and Morgan B. (2004). – «A Bayesian approach to combining animal
abundance and demographic data », To appear m Animal Biodiversity and Conservation.</mixed-citation>
         </ref>
         <ref id="d428e342a1310">
            <mixed-citation id="d428e346" publication-type="other">
Clayton D. (1978). – « A model for association in bivariate life tables and its application in
epidemiological studies of familial tendency in chronic disease incidence », Biometrika,
65, 141–151.</mixed-citation>
         </ref>
         <ref id="d428e359a1310">
            <mixed-citation id="d428e363" publication-type="other">
—(1991). – «A Monte Carlo method for Bayesian inference in frailty models »,
Biometrics, 47, 467-485.</mixed-citation>
         </ref>
         <ref id="d428e373a1310">
            <mixed-citation id="d428e377" publication-type="other">
Cox D. (1972). – « Regression models and life tables », Journal of the Royal Statistical
Society. Series B, 34, 187-220.</mixed-citation>
         </ref>
         <ref id="d428e387a1310">
            <mixed-citation id="d428e391" publication-type="other">
Dempster A., Laird N. and Rubin D. (1977). – «Maximum likelihood estimation from
incomplete data via the EM algorithm (with discussion) », Journal of the Royal Statistical
Society. Series B, 39, 1-38.</mixed-citation>
         </ref>
         <ref id="d428e404a1310">
            <mixed-citation id="d428e408" publication-type="other">
Djurdjevic D. (2000). – « Estimation de modèles de durée multivariés: application à la rati-
fication des conventions par les pays membres du BIT », Master's thesis, Université Louis
Pasteur, Strasbourg.</mixed-citation>
         </ref>
         <ref id="d428e422a1310">
            <mixed-citation id="d428e426" publication-type="other">
Efron B. (1977). – « The Efficiency of Cox's Likelihood Function for Censored Data »,
Journal of the American Statistical Association, 72, 557-565.</mixed-citation>
         </ref>
         <ref id="d428e436a1310">
            <mixed-citation id="d428e440" publication-type="other">
Elbers C. and Ridder G. (1982). – « True and spurious duration dependence: The identifi-
ability of the proportional hazard model », Review of Economic Studies, 49, 403-410.</mixed-citation>
         </ref>
         <ref id="d428e450a1310">
            <mixed-citation id="d428e454" publication-type="other">
Gelman A. and Rubin D. (1992). – « Inference from iterative simulation using multiple
sequences », Statistical Science, 7, 457-472.</mixed-citation>
         </ref>
         <ref id="d428e464a1310">
            <mixed-citation id="d428e468" publication-type="other">
Gilks W. and Wild P. (1992). – «Adaptative rejection sampling for Gibbs sampling»,
Journal of the Royal Statistical Society. Series C, 41, 337-348.</mixed-citation>
         </ref>
         <ref id="d428e478a1310">
            <mixed-citation id="d428e482" publication-type="other">
Gouriéroux C. and Monfort A. (1990). – Statistiques et modèles économétriques, vol. 1-2,
Paris: Economica, 2 ed.</mixed-citation>
         </ref>
         <ref id="d428e492a1310">
            <mixed-citation id="d428e496" publication-type="other">
Guo G. and Rodriguez G. (1992). – «Estimating a multivariate proportional hazards
model for clustered data using the EM algorithm, with an application to child survival in
Guatemala », Journal of the American Statistical Association, 87, 969-976.</mixed-citation>
         </ref>
         <ref id="d428e510a1310">
            <mixed-citation id="d428e514" publication-type="other">
Gustafson P. (1997). – «Large hierarchical Bayesian analysis of multivariate survival
data », Biometrics, 53, 230-242.</mixed-citation>
         </ref>
         <ref id="d428e524a1310">
            <mixed-citation id="d428e528" publication-type="other">
Hertz-Picciotto I. and Rockhill B. (1997). – « Validity and Efficiency of Approximation
Methods for Tied Survival Times in Cox Regression », Biometrics, 53, 1151-1156.</mixed-citation>
         </ref>
         <ref id="d428e538a1310">
            <mixed-citation id="d428e542" publication-type="other">
Honoré B. (1993). – « Identification results for duration models with multiple spells », The
Review of Economic Studies, 60, 241-246.</mixed-citation>
         </ref>
         <ref id="d428e552a1310">
            <mixed-citation id="d428e556" publication-type="other">
Horny G. (2001). – « Estimation de modèles de durée multivariés au moyen de l'algorihmc
EM », Master's thesis, Université Louis Pasteur, Strasbourg.</mixed-citation>
         </ref>
         <ref id="d428e566a1310">
            <mixed-citation id="d428e570" publication-type="other">
— (2009). – « Inference in mixed proportional hazard models with K random
effects », Statistical Papers, forthcoming.</mixed-citation>
         </ref>
         <ref id="d428e580a1310">
            <mixed-citation id="d428e584" publication-type="other">
Hougaard P. (2000). –Analysis of multivariate survival data, New York: Springer-Verlag.</mixed-citation>
         </ref>
         <ref id="d428e592a1310">
            <mixed-citation id="d428e596" publication-type="other">
Kalbfleisch J. (1978). – « Non-parametric Bayesian analysis of survival time data », Journal
of the Royal Statistical Society. Series B, 40, 214-221.</mixed-citation>
         </ref>
         <ref id="d428e606a1310">
            <mixed-citation id="d428e610" publication-type="other">
Kalbfleisch J. and Prentice R. (1980). – The Statistical analysis of failure time data, New
York: John Wiley.</mixed-citation>
         </ref>
         <ref id="d428e620a1310">
            <mixed-citation id="d428e624" publication-type="other">
Lancaster T. (1979). – «Econometric methods for the duration of unemployment»,
Econometrica, 47, 939-956.</mixed-citation>
         </ref>
         <ref id="d428e634a1310">
            <mixed-citation id="d428e638" publication-type="other">
—(1990). – The econometric analysis of transition data, Cambridge: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d428e648a1310">
            <mixed-citation id="d428e652" publication-type="other">
Liang K.-Y., Self S., Bandeen-Roche K. and Zeger S. (1995). – « Some recent develop-
ments for regression analysis of multivariate failure time data », Lifetime data analysis,
1,403-415.</mixed-citation>
         </ref>
         <ref id="d428e665a1310">
            <mixed-citation id="d428e669" publication-type="other">
Louis T. (1982). – « Finding the observed Information matrix when using the EM algo-
rithm », Journal of the Royal Statistical Society. Series B, 44, 226-233.</mixed-citation>
         </ref>
         <ref id="d428e680a1310">
            <mixed-citation id="d428e684" publication-type="other">
McGilchrist C. (1993). – « REML estimation for survival model with frailty », Biometrics,
49,221-225.</mixed-citation>
         </ref>
         <ref id="d428e694a1310">
            <mixed-citation id="d428e698" publication-type="other">
Milcent C. (2003). – « Ownership, system of reimbursement and mortality rate relation-
ships », mimeo.</mixed-citation>
         </ref>
         <ref id="d428e708a1310">
            <mixed-citation id="d428e712" publication-type="other">
Neal R. (1997). – « Markov Chain Monte Carlo methods based on "slicing" the density
function », Tech. Rep. 9722, Department of Statistics, University of Toronto.</mixed-citation>
         </ref>
         <ref id="d428e722a1310">
            <mixed-citation id="d428e726" publication-type="other">
Nelson W. (1969). – « Hazard plotting for incomplete failure time data », Journal of Quality
Technology, 1, 27-52.</mixed-citation>
         </ref>
         <ref id="d428e736a1310">
            <mixed-citation id="d428e740" publication-type="other">
Ridder G. and Tunali I. (1999). – « Stratified partial likelihood estimation », Journal of
Econometrics, 92, 193-232.</mixed-citation>
         </ref>
         <ref id="d428e750a1310">
            <mixed-citation id="d428e754" publication-type="other">
Ripatti S. and Palmgren J. (2000). – « Estimation of multivariate frailty models using penal-
ized partial likelihood », Biometrics, 56, 1016-1022.</mixed-citation>
         </ref>
         <ref id="d428e765a1310">
            <mixed-citation id="d428e769" publication-type="other">
Robert C. (1996). – Méthodes de Monte Carlo par chaînes de Markov, Paris: Económica.</mixed-citation>
         </ref>
         <ref id="d428e776a1310">
            <mixed-citation id="d428e780" publication-type="other">
Robert C. and Casella G. (1999). – Monte Carlo statistical methods, Springer.</mixed-citation>
         </ref>
         <ref id="d428e787a1310">
            <mixed-citation id="d428e791" publication-type="other">
Rodriguez G. and Goldman N. (2001). – « Improved estimation procedures for multilevel
models with binary responses: a case study », Journal of the Royal Statistical Society,
164,339-355.</mixed-citation>
         </ref>
         <ref id="d428e804a1310">
            <mixed-citation id="d428e808" publication-type="other">
Sargent D. (1998). – « A general framework for random effects survival analysis in the Cox
proportional hazards setting », Biometrics, 54, 1486-1497.</mixed-citation>
         </ref>
         <ref id="d428e818a1310">
            <mixed-citation id="d428e822" publication-type="other">
Sastry N. (1997). – « A nested frailty model for survival data, with an application to the study
of child survival in northeast Brazil », Journal of the American Statistical Association, 92,
426-435.</mixed-citation>
         </ref>
         <ref id="d428e835a1310">
            <mixed-citation id="d428e839" publication-type="other">
Schwarz G. (1978). – « Estimating the dimension of a model », The Annals of Statistics, 6,
461-464.</mixed-citation>
         </ref>
         <ref id="d428e850a1310">
            <mixed-citation id="d428e854" publication-type="other">
Therneau M. and Grambsch P. (2000). – Modeling survival data: extending the Cox model,
Statistics for Biology and Health, New-York: Springer.</mixed-citation>
         </ref>
         <ref id="d428e864a1310">
            <mixed-citation id="d428e868" publication-type="other">
Therneau T., Grambsch P. and Pankratz V. (2003). – « Penalized survival models and
frailty », Journal of Computational and Graphical Statistics, 12, 156-175.</mixed-citation>
         </ref>
         <ref id="d428e878a1310">
            <mixed-citation id="d428e882" publication-type="other">
Train K. (2003). – Discrete Choice Methods with Simulation, Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d428e889a1310">
            <mixed-citation id="d428e893" publication-type="other">
Vaida F. and Xu R. (2000). – « Proportional hazards model with random effects », Statistics
in Medicine, 19, 3309-3324.</mixed-citation>
         </ref>
         <ref id="d428e903a1310">
            <mixed-citation id="d428e907" publication-type="other">
Van den Berg G. (2001). – « Duration models: specification, identification and multiple
durations », in Handbook of Econometrics, Elsevier, Amsterdam: J. J. Heckman and E.
Learner (eds.), vol. 5, chap. 55, 3381-3463.</mixed-citation>
         </ref>
         <ref id="d428e920a1310">
            <mixed-citation id="d428e924" publication-type="other">
Wasserman L. (2000). – « Bayesian model selection and model averaging », Journal of
Mathematical Psychology, 44, 92-107.</mixed-citation>
         </ref>
         <ref id="d428e935a1310">
            <mixed-citation id="d428e939" publication-type="other">
Yamaguchi K. (1986). – « Alternative approaches to unobserved heterogeneity in the analy-
sis of repeatable events », in Sociological Methodology 1986, ed. by N. Tuma, Washington
D.C.: Jossey-Bass.</mixed-citation>
         </ref>
         <ref id="d428e952a1310">
            <mixed-citation id="d428e956" publication-type="other">
Yau K. (2001). – « Multilevel models for survival analysis with random effects », Biometrics,
57, 96-102.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
