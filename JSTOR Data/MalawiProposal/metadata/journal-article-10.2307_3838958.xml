<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jmonecredbank</journal-id>
         <journal-id journal-id-type="jstor">j100238</journal-id>
         <journal-title-group>
            <journal-title>Journal of Money, Credit and Banking</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Ohio State University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00222879</issn>
         <issn pub-type="epub">15384616</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">3838958</article-id>
         <title-group>
            <article-title>Bank Competition and Access to Finance: International Evidence</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Thorsten</given-names>
                  <surname>Beck</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Asli</given-names>
                  <surname>Demirgüç-Kunt</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Vojislav</given-names>
                  <surname>Maksimovic</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>6</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">36</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i371342</issue-id>
         <fpage>627</fpage>
         <lpage>648</lpage>
         <page-range>627-648</page-range>
         <permissions>
            <copyright-statement>Copyright 2004 The Ohio State University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/3838958"/>
         <abstract>
            <p>Using a unique database for 74 countries and for firms of small, medium, and large size we assess the effect of banking market structure on the access of firms to bank finance. We find that bank concentration increases obstacles to obtaining finance, but only in countries with low levels of economic and institutional development. A larger share of foreign-owned banks and an efficient credit registry dampen the effect of concentration on financing obstacles, while the effect is exacerbated by more restrictions on banks' activities, more government interference in the banking sector, and a larger share of government-owned banks.</p>
         </abstract>
         <kwd-group>
            <kwd>G30</kwd>
            <kwd>G10</kwd>
            <kwd>O16</kwd>
            <kwd>K40</kwd>
            <kwd>Financial Development</kwd>
            <kwd>Financing Obstacles</kwd>
            <kwd>Small and Medium Enterprises</kwd>
            <kwd>Bank Concentration</kwd>
            <kwd>Bank Competition</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d1394e282a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1394e289" publication-type="other">
Beck, Levine, and Loayza (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e295" publication-type="other">
Rousseau
and Wachtel (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e304" publication-type="other">
Wurgler (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e311" publication-type="other">
Demirgüç-Kunt and Maksimovic (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e317" publication-type="other">
Rajan and Zingales
(1998)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e327a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1394e334" publication-type="other">
Clarke, Cull, and Martinez-Peria (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e341a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1394e348" publication-type="other">
Cetorelli (2001a)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e355a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1394e362" publication-type="other">
Beck and Levine (2002)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e368" publication-type="other">
Demirgüç-Kunt and Maksimovic (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e374" publication-type="other">
Rajan and Zingales (1998)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e381" publication-type="other">
Claessens and Laeven (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e387" publication-type="other">
Jappelli and Pagano (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e395a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1394e402" publication-type="other">
Clarke et al. (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e409a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1394e416" publication-type="other">
La Porta, Lopez-de-Silanes, and Shleifer (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e423a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1394e430" publication-type="other">
Bonaccorsi di Patti and Dell'Ariccia (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e436" publication-type="other">
Bonaccorsi di
Patti and Gobbi (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e446a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1394e453" publication-type="other">
Demirgüç-Kunt, Laeven, and Levine (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e460a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1394e467" publication-type="other">
Galindo and Miller (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d1394e474a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1394e481" publication-type="other">
Barth, Caprio, and Levine (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1394e487" publication-type="other">
Berger,
Hasan, and Klapper 2003</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d1394e506a1310">
            <mixed-citation id="d1394e510" publication-type="book">
Barth, James R., Gerard Caprio, Jr., and Ross Levine (2001). "The Regulation and Supervision
of Banks around the World. A New Database." In Integrating Emerging Market Countries
into the Global Financial System, edited by Robert E. Litan and R. Herring, pp. 183-250.
Washington, DC: Brookings Institution Press<person-group>
                  <string-name>
                     <surname>Barth</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The Regulation and Supervision of Banks around the World. A New Database</comment>
               <fpage>183</fpage>
               <source>Integrating Emerging Market Countries into the Global Financial System</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e548a1310">
            <mixed-citation id="d1394e552" publication-type="journal">
Beck, Thorsten, and Ross Levine (2002). "Industry Growth and Capital Allocation: Does
Having a Market- or Bank-based System Matter?" Journal of Financial Economics64,
147-180<person-group>
                  <string-name>
                     <surname>Beck</surname>
                  </string-name>
               </person-group>
               <fpage>147</fpage>
               <volume>64</volume>
               <source>Journal of Financial Economics</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e587a1310">
            <mixed-citation id="d1394e591" publication-type="other">
Beck, Thorsten, Asli Demirgüç-Kunt, and Vojislav Maksimovic (2001a). "Financial and Legal
Constraints to Firm Growth: Does Size Matter?" World Bank Policy Research Working
Paper No. 2784</mixed-citation>
         </ref>
         <ref id="d1394e604a1310">
            <mixed-citation id="d1394e608" publication-type="book">
Beck, Thorsten, Asli Demirguc-Kunt, and Vojislav Maksimovic (2001b). "Financial and Legal
Institutions and Firm Size." Mimeo, World Bank<person-group>
                  <string-name>
                     <surname>Beck</surname>
                  </string-name>
               </person-group>
               <source>Financial and Legal Institutions and Firm Size</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e634a1310">
            <mixed-citation id="d1394e638" publication-type="journal">
Beck, Thorsten, Ross Levine, and Norman Loayza (2000). "Finance and the Sources of
Growth." Journal of Financial Economics58, 261-300<person-group>
                  <string-name>
                     <surname>Beck</surname>
                  </string-name>
               </person-group>
               <fpage>261</fpage>
               <volume>58</volume>
               <source>Journal of Financial Economics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e670a1310">
            <mixed-citation id="d1394e674" publication-type="book">
Berger, Allen N., Iftekhar Hasan, and Leora F. Klapper (2003). "Community Banking and
Economic Performance: Some International Evidence." Mimeo, World Bank<person-group>
                  <string-name>
                     <surname>Berger</surname>
                  </string-name>
               </person-group>
               <source>Community Banking and Economic Performance: Some International Evidence</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e699a1310">
            <mixed-citation id="d1394e703" publication-type="journal">
Black, Sandra E., and Philip E. Strahan (2002). "Entrepreneurship and Bank Credit Availabil¬
ity." Journal of Finance57, 2807-2833<object-id pub-id-type="jstor">10.2307/3094544</object-id>
               <fpage>2807</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e719a1310">
            <mixed-citation id="d1394e723" publication-type="journal">
Bonaccorsi di Patti, and Giovanni DeH'Ariccia (2003). "Bank Competition and Firm Cre¬
ation." Journal of Money, Credit, and Banking36, 225-252<person-group>
                  <string-name>
                     <surname>Bonaccorsi di Patti</surname>
                  </string-name>
               </person-group>
               <fpage>225</fpage>
               <volume>36</volume>
               <source>Journal of Money, Credit, and Banking</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e755a1310">
            <mixed-citation id="d1394e759" publication-type="other">
Bonaccorsi di Patti, and Giorgi Gobbi (2001). "The Effects of Bank Consolidation and Market
Entry on Small Business Lending." Banca d'Italia Temi di Discussione 404</mixed-citation>
         </ref>
         <ref id="d1394e769a1310">
            <mixed-citation id="d1394e773" publication-type="other">
Cetorelli, Nicola (2001a). "Competition Among Banks: Good or Bad?" Federal Reserve
Bank of Chicago Economic Perspectives, 38-48</mixed-citation>
         </ref>
         <ref id="d1394e784a1310">
            <mixed-citation id="d1394e788" publication-type="other">
Cetorelli, Nicola (2001b). "Does Bank Concentration Lead to Industry Concentration?" Fed-
eral Reserve Bank of Chicago Working Paper No. 2001-01</mixed-citation>
         </ref>
         <ref id="d1394e798a1310">
            <mixed-citation id="d1394e802" publication-type="journal">
Cetorelli, Nicola, and Michele Gambera (2001). "Banking Market Structure, Financial Depen¬
dence and Growth: International Evidence from Industry Data." Journal of Finance56,
617-648<object-id pub-id-type="jstor">10.2307/222576</object-id>
               <fpage>617</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e821a1310">
            <mixed-citation id="d1394e825" publication-type="other">
Cetorelli, Nicola, and Pietro F. Peretto (2000). "Oligopoly Banking and Capital Accumula-
tion." Federal Reserve Bank of Chicago Working Paper No. 2000-12</mixed-citation>
         </ref>
         <ref id="d1394e835a1310">
            <mixed-citation id="d1394e839" publication-type="journal">
Claessens, Stijn, and Luc Laeven (2003). "Financial Development, Property Rights and
Growth." Journal of Finance58, 2401-2436<object-id pub-id-type="jstor">10.2307/3648198</object-id>
               <fpage>2401</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e855a1310">
            <mixed-citation id="d1394e859" publication-type="other">
Clarke, George R.G., Robert Cull, and Maria Soledad Martinez Peria (2001). "Does Foreign
Bank Penetration Reduce Access to Credit in Developing Countries? Evidence from Asking
Borrowers." World Bank Policy Research Working Paper No. 2716</mixed-citation>
         </ref>
         <ref id="d1394e872a1310">
            <mixed-citation id="d1394e876" publication-type="journal">
Clarke, George R.G., Robert Cull, Maria Soledad Martinez Peria, and Susana M. Sanchez
(2003). "Foreign Bank Entry: Experience, Implications for Developing Countries, and
Agenda for Further Research." World Bank Research Observer18, 25-60<person-group>
                  <string-name>
                     <surname>Clarke</surname>
                  </string-name>
               </person-group>
               <fpage>25</fpage>
               <volume>18</volume>
               <source>World Bank Research Observer</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e912a1310">
            <mixed-citation id="d1394e916" publication-type="journal">
Demirguc-Kunt, Asli, and Vojislav Maksimovic (1998). "Law, Finance, and Firm Growth."
Journal of Finance53, 2107-2137<object-id pub-id-type="jstor">10.2307/117462</object-id>
               <fpage>2107</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e932a1310">
            <mixed-citation id="d1394e936" publication-type="journal">
Demirgüç-Kunt, Asli, Luc Laeven, and Ross Levine (2003). "Regulations, Market Structure,
Institutions, and the Cost of Financial Intermediation." Journal of Money, Credit, and
Banking36, 593-622. (this issue of JMCB)<person-group>
                  <string-name>
                     <surname>Demirgüç-Kunt</surname>
                  </string-name>
               </person-group>
               <fpage>593</fpage>
               <volume>36</volume>
               <source>Journal of Money, Credit, and Banking</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e971a1310">
            <mixed-citation id="d1394e975" publication-type="journal">
De Young, Robert, Lawrence G. Goldberg, and Lawrence J. White (1999). "Youth, Adoles¬
cence, and Maturity of Banks: Credit Availability to Small Business in an Era of Banking
Consolidation." Journal of Banking and Finance23, 463-492<person-group>
                  <string-name>
                     <surname>De Young</surname>
                  </string-name>
               </person-group>
               <fpage>463</fpage>
               <volume>23</volume>
               <source>Journal of Banking and Finance</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1010a1310">
            <mixed-citation id="d1394e1014" publication-type="journal">
Dinç, Serdar I. (2000). "Bank Reputation, Bank Commitment, and the Effects of Competition
in Credit Markets." Review of Financial Studies13, 781-812<object-id pub-id-type="jstor">10.2307/2646003</object-id>
               <fpage>781</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e1030a1310">
            <mixed-citation id="d1394e1034" publication-type="book">
Galindo, Arturo, and Margaret Miller (2001). "Can Credit Registries Reduce Credit Con-
straints? Empirical Evidence on the Role of Credit Registries in Firm Investment Decisions."
Inter-American Development Bank<person-group>
                  <string-name>
                     <surname>Galindo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Can Credit Registries Reduce Credit Constraints? Empirical Evidence on the Role of Credit Registries in Firm Investment Decisions</comment>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1063a1310">
            <mixed-citation id="d1394e1067" publication-type="journal">
Guzman, Mark G. (2000). "Bank Structure, Capital Accumulation and Growth: A Simple
Macroeconomic Model." Economic Theory16, 421-455<person-group>
                  <string-name>
                     <surname>Guzman</surname>
                  </string-name>
               </person-group>
               <fpage>421</fpage>
               <volume>16</volume>
               <source>Economic Theory</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1100a1310">
            <mixed-citation id="d1394e1104" publication-type="journal">
Hannan, Timothy H. (1991). "Bank Commercial Loan Markets and the Role of Market
Structure: Evidence from Surveys of Commercial Lending." Journal of Banking and
Finance15, 133-149<person-group>
                  <string-name>
                     <surname>Hannan</surname>
                  </string-name>
               </person-group>
               <fpage>133</fpage>
               <volume>15</volume>
               <source>Journal of Banking and Finance</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1139a1310">
            <mixed-citation id="d1394e1143" publication-type="other">
Hellman, Joel S., Geraint Jones, Daniel Kaufmann, and Mark Schankerman (2000). "Measur-
ing Governance and State Capture: The Role of Bureaucrats and Firms in Shaping the
Business Environment." European Bank for Reconstruction and Development Working
Paper No. 51</mixed-citation>
         </ref>
         <ref id="d1394e1159a1310">
            <mixed-citation id="d1394e1163" publication-type="journal">
Jackson, John E., and Ann R. Thomas (1995). "Bank Structure and New Business Creation.
Lessons from an Earlier Time." Regional Science and Urban Economics25, 323-353<person-group>
                  <string-name>
                     <surname>Jackson</surname>
                  </string-name>
               </person-group>
               <fpage>323</fpage>
               <volume>25</volume>
               <source>Regional Science and Urban Economics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1195a1310">
            <mixed-citation id="d1394e1199" publication-type="journal">
Jappelli, Tullio, and Marco Pagano (2002). "Information Sharing in Credit Markets: Interna¬
tional Evidence." Journal of Banking and Finance26, 2023-2054<person-group>
                  <string-name>
                     <surname>Jappelli</surname>
                  </string-name>
               </person-group>
               <fpage>2023</fpage>
               <volume>26</volume>
               <source>Journal of Banking and Finance</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1231a1310">
            <mixed-citation id="d1394e1235" publication-type="other">
Kaufmann, Daniel, Aart Kraay, and Pablo Zoido-Lobaton (1999). "Governance Matters."
World Bank Policy Research Working Paper No. 2196</mixed-citation>
         </ref>
         <ref id="d1394e1245a1310">
            <mixed-citation id="d1394e1249" publication-type="journal">
La Porta, Rafael, Florencio Lopez-de-Silanes, and Andrei Shleifer (2002). Government Own¬
ership of Commercial Banks. Journal of Finance57, 265-301<object-id pub-id-type="jstor">10.2307/2697840</object-id>
               <fpage>265</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e1266a1310">
            <mixed-citation id="d1394e1270" publication-type="journal">
Marquez, Robert (2002). "Competition, Adverse Selection, and Information Dispersion in
the Banking Industry." The Review of Financial Studies15, 901-926<object-id pub-id-type="jstor">10.2307/2696725</object-id>
               <fpage>901</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e1286a1310">
            <mixed-citation id="d1394e1290" publication-type="journal">
Pagano, Marco (1993). "Financial Markets and Growth. An Overview." European Economic
Review37, 613-622<person-group>
                  <string-name>
                     <surname>Pagano</surname>
                  </string-name>
               </person-group>
               <fpage>613</fpage>
               <volume>37</volume>
               <source>European Economic Review</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1322a1310">
            <mixed-citation id="d1394e1326" publication-type="journal">
Petersen, Mitchell A., and Raghuram Rajan (1995). "The Effect of Credit Market Competition
on Lending Relationships." Quarterly Journal of Economics110, 407-443<object-id pub-id-type="doi">10.2307/2118445</object-id>
               <fpage>407</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e1342a1310">
            <mixed-citation id="d1394e1346" publication-type="journal">
Rajan, Rhaguram, and Luigi Zingales (1998). "Financial Dependence and Growth." American
Economic Review88, 559-587<object-id pub-id-type="jstor">10.2307/116849</object-id>
               <fpage>559</fpage>
            </mixed-citation>
         </ref>
         <ref id="d1394e1362a1310">
            <mixed-citation id="d1394e1366" publication-type="journal">
Rousseau, Peter L., and Paul Wachtel (2000). "Equity Markets and Growth: Cross-country
Evidence on Timing and Outcomes, 1980-1995." Journal of Banking and Finance24,
1933-1957<person-group>
                  <string-name>
                     <surname>Rousseau</surname>
                  </string-name>
               </person-group>
               <fpage>1933</fpage>
               <volume>24</volume>
               <source>Journal of Banking and Finance</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1401a1310">
            <mixed-citation id="d1394e1405" publication-type="book">
Scott, Jonathan A., and William C. Dunkelberg (2001). "Competition and Credit Market
Outcomes: A Small Firm Perspective." Mimeo, Temple University<person-group>
                  <string-name>
                     <surname>Scott</surname>
                  </string-name>
               </person-group>
               <source>Competition and Credit Market Outcomes: A Small Firm Perspective</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d1394e1431a1310">
            <mixed-citation id="d1394e1435" publication-type="journal">
Wurgler, Jeffrey (2000). "Financial Markets and the Allocation of Capital." Journal of Finan-
cial Economics58, 187-214<person-group>
                  <string-name>
                     <surname>Wurgler</surname>
                  </string-name>
               </person-group>
               <fpage>187</fpage>
               <volume>58</volume>
               <source>Journal of Financial Economics</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
