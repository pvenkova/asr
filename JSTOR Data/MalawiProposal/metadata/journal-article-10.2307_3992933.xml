<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">thirworlquar</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101236</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Third World Quarterly</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Carfax Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">01436597</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">13602241</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3992933</article-id>
         <title-group>
            <article-title>Deterritorialised Territories, Borderless Borders: The New Geography of International Medical Assistance</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>François</given-names>
                  <surname>Debrix</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">19</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i383053</issue-id>
         <fpage>827</fpage>
         <lpage>846</lpage>
         <page-range>827-846</page-range>
         <permissions>
            <copyright-statement>Copyright 1998 Third World Quarterly</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3992933"/>
         <abstract>
            <p>This article examines the phenomenon of international medical assistance to populations in distress from the perspective of the new spatial strategies deployed by medical humanitarian organisations. Taking seriously the 'borderlessness' of movements such as Médecins sans Frontières (Doctors without Borders, or MSF), the article argues that transnational medical organisations participate in the practice of deterritorialisation. Deterritorialisation means that certain transnational actors now have the ability to intervene below, across and beyond state boundaries. In the case of MSF, going beyond state boundaries is creative of new territorial structures. One such structure is what may be called the 'space of victimhood'. Under the guise of reaching 'victims' the world over, MSF constructs new spaces--humanitarian zones--inside which individuals in distress are identified as 'victims', are sorted out, and become recognisable as generalised examples of human drama. This construction of a space of victimhood opens up the possibility for re-appropriations and manipulations by other non-humanitarian actors. Among such actors, one finds global media networks which avidly search for images of victims. By pointing out the potentially non-humanitarian effects of the new spatial arrangements deployed by transnational medical organisations (a phenomenon referred to as 'transversality'), this article urges international scholars and practitioners to keep a close eye on questions of space and, specifically, on the sociopolitical processes of inclusion and exclusion that such territorial delineations often produce.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d470e158a1310">
            <label>1</label>
            <mixed-citation id="d470e165" publication-type="book">
James Der Derian, Antidiplo-
macy: Spies, Terror, Speed, and War, Cambridge, MA: Blackwell, 1992<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Derian</surname>
                  </string-name>
               </person-group>
               <source>Antidiplomacy: Spies, Terror, Speed, and War</source>
               <year>1992</year>
            </mixed-citation>
            <mixed-citation id="d470e189" publication-type="book">
Paul Virilio, Speed and
Politics: An essay on Dromology, New York: Semiotext(e), 1986<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Virilio</surname>
                  </string-name>
               </person-group>
               <source>Speed and Politics: An essay on Dromology</source>
               <year>1986</year>
            </mixed-citation>
         </ref>
         <ref id="d470e214a1310">
            <label>2</label>
            <mixed-citation id="d470e221" publication-type="book">
Paul Virilio, Open Sky, trans Julie Rose, New York: Verso, 1997, p 70<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Virilio</surname>
                  </string-name>
               </person-group>
               <fpage>70</fpage>
               <source>Open Sky</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d470e246a1310">
            <label>3</label>
            <mixed-citation id="d470e253" publication-type="book">
Cynthia Weber, Simulating Sovereignty: Intervention, the State and Symbolic Exchange,
Cambridge: Cambridge University Press, 1995<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Weber</surname>
                  </string-name>
               </person-group>
               <source>Simulating Sovereignty: Intervention, the State and Symbolic Exchange</source>
               <year>1995</year>
            </mixed-citation>
            <mixed-citation id="d470e277" publication-type="book">
Roxanne Lynn Doty, Imperial Encounters: The Politics
of Representation in North-South Relations, Minneapolis, MN: University of Minnesota Press, 1996<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Doty</surname>
                  </string-name>
               </person-group>
               <source>Imperial Encounters: The Politics of Representation in North-South Relations</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d470e302a1310">
            <label>4</label>
            <mixed-citation id="d470e309" publication-type="book">
R B J Walker, Inside/Outside: International Relations as Political Theory, Cambridge: Cambridge
University Press, 1993<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Walker</surname>
                  </string-name>
               </person-group>
               <source>Inside/Outside: International Relations as Political Theory</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d470e335a1310">
            <label>5</label>
            <mixed-citation id="d470e342" publication-type="book">
John Agnew &amp; Stuart Corbridge, Mastering Space: Hegemony, Territory and International Political
Economy, New York: Routledge, 1995, p 14<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Agnew</surname>
                  </string-name>
               </person-group>
               <fpage>14</fpage>
               <source>Mastering Space: Hegemony, Territory and International Political Economy</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d470e371a1310">
            <label>6</label>
            <mixed-citation id="d470e378" publication-type="book">
Thom Kuehls, Beyond Sovereign Territory: The
Space of Ecopolitics, Minneapolis, MN: University of Minnesota Press, 1995, p 33<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kuehls</surname>
                  </string-name>
               </person-group>
               <fpage>33</fpage>
               <source>Beyond Sovereign Territory: The Space of Ecopolitics</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d470e407a1310">
            <mixed-citation id="d470e411" publication-type="other">
Agnew &amp; Corbridge, Mastering Space, p 80</mixed-citation>
         </ref>
         <ref id="d470e418a1310">
            <label>8</label>
            <mixed-citation id="d470e425" publication-type="book">
Gilles Deleuze &amp; F6lix Guattari, A Thousand Plateaus: Capitalism and Schizophrenia, Minneapolis, MN:
University of Minnesota Press, 1987, pp 474-475<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Deleuze</surname>
                  </string-name>
               </person-group>
               <fpage>474</fpage>
               <source>A Thousand Plateaus: Capitalism and Schizophrenia</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d470e454a1310">
            <label>9</label>
            <mixed-citation id="d470e461" publication-type="book">
Gilles Deleuze &amp; Felix Guattari, Anti-Oedipus: Capitalism and Schizophrenia, Minneapolis, MN: University
of Minnesota Press, 1983<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Deleuze</surname>
                  </string-name>
               </person-group>
               <source>Anti-Oedipus: Capitalism and Schizophrenia</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d470e486a1310">
            <mixed-citation id="d470e490" publication-type="other">
Deleuze &amp; Guattari, A Thousand Plateaus, p 10</mixed-citation>
         </ref>
         <ref id="d470e498a1310">
            <label>12</label>
            <mixed-citation id="d470e505" publication-type="other">
Kuehls, Beyond Sovereign Territory, p 49</mixed-citation>
         </ref>
         <ref id="d470e512a1310">
            <label>13</label>
            <mixed-citation id="d470e519" publication-type="other">
www.msf.org.</mixed-citation>
            <mixed-citation id="d470e525" publication-type="book">
Medecins sans Frontières, Populations en Danger, 1995:
Rapport Annuel sur les Crises Majeures et l'Action Humanitaire (Populations in Danger 1995: Annual
Report on Major Crises and Humanitarian Action), Paris: La Decouverte, 1995, p 174<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Frontières</surname>
                  </string-name>
               </person-group>
               <fpage>174</fpage>
               <source>Populations en Danger, 1995: Rapport Annuel sur les Crises Majeures et l'Action Humanitaire (Populations in Danger 1995: Annual Report on Major Crises and Humanitarian Action)</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d470e557a1310">
            <label>14</label>
            <mixed-citation id="d470e564" publication-type="other">
www.msf.org.</mixed-citation>
         </ref>
         <ref id="d470e571a1310">
            <label>16</label>
            <mixed-citation id="d470e580" publication-type="book">
Cathal Nolan, The Longman Guide
to World Affairs, White Plains, NY: Longman, 1995, p 261<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Nolan</surname>
                  </string-name>
               </person-group>
               <fpage>261</fpage>
               <source>The Longman Guide to World Affairs</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d470e609a1310">
            <label>18</label>
            <mixed-citation id="d470e616" publication-type="book">
Henri Dunant, A Memory of Solferino, Washington, DC: American Red Cross Publication, 1939<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Dunant</surname>
                  </string-name>
               </person-group>
               <source>A Memory of Solferino</source>
               <year>1939</year>
            </mixed-citation>
         </ref>
         <ref id="d470e638a1310">
            <label>20</label>
            <mixed-citation id="d470e645" publication-type="book">
Mike Toole, 'Frontline
medicine: the role of international medical groups in emergency relief', in M6decins sans Frontieres (eds),
World in Crisis: The Politics of Survival at the End of the 20th Century, New York: Routledge, 1997, p 32<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Toole</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Frontline medicine: the role of international medical groups in emergency relief</comment>
               <fpage>32</fpage>
               <source>World in Crisis: The Politics of Survival at the End of the 20th Century</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d470e681a1310">
            <label>22</label>
            <mixed-citation id="d470e688" publication-type="book">
Christopher Couples, 'Digital duplicities: techniques of the self(s)
in digital spaces', paper presented at the Annual Meeting of the Southern Political Science Association,
November 1997<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Couples</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Digital duplicities: techniques of the self(s) in digital spaces</comment>
               <source>Annual Meeting of the Southern Political Science Association, November 1997</source>
            </mixed-citation>
         </ref>
         <ref id="d470e717a1310">
            <label>24</label>
            <mixed-citation id="d470e724" publication-type="other">
www.msf.org.</mixed-citation>
         </ref>
         <ref id="d470e731a1310">
            <label>25</label>
            <mixed-citation id="d470e738" publication-type="journal">
Bernard Kouchner, 'Le mouvement humanitaire' (The humanitarian movement), Le Débat, November-
December 1991, p 35 (my translation)<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Kouchner</surname>
                  </string-name>
               </person-group>
               <issue>November</issue>
               <fpage>35</fpage>
               <source>Le Débat</source>
               <year>1991</year>
            </mixed-citation>
            <mixed-citation id="d470e769" publication-type="other">
Mike Toole,
'Frontline medicine', p 31</mixed-citation>
         </ref>
         <ref id="d470e779a1310">
            <label>27</label>
            <mixed-citation id="d470e786" publication-type="book">
Medecins sans Frontières, Life, Death and Aid: The Médecins sans Frontières Report on World Crisis
Intervention, New York: Routledge, 1993<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Frontières</surname>
                  </string-name>
               </person-group>
               <source>Life, Death and Aid: The Médecins sans Frontières Report on World Crisis Intervention</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d470e811a1310">
            <label>28</label>
            <mixed-citation id="d470e818" publication-type="journal">
Gear6id 6 Tuathail, 'At the end of geopolitics? Reflections on a plural problematic at the century's end',
Alternatives, 22(1), 1997, p 38<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Tuathail</surname>
                  </string-name>
               </person-group>
               <issue>1</issue>
               <fpage>38</fpage>
               <volume>22</volume>
               <source>Alternatives</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d470e853a1310">
            <label>29</label>
            <mixed-citation id="d470e860" publication-type="book">
Rony
Brauman, Devant le Mal, Rwanda: Un Génocide en Direct (Faced with Evil, Rwanda: A Genocide Live on
TV), Paris: Arl6a, 1994<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Brauman</surname>
                  </string-name>
               </person-group>
               <source>Devant le Mal, Rwanda: Un Génocide en Direct (Faced with Evil, Rwanda: A Genocide Live on TV)</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d470e890a1310">
            <label>30</label>
            <mixed-citation id="d470e897" publication-type="other">
Virilio, Open Sky</mixed-citation>
         </ref>
         <ref id="d470e904a1310">
            <label>31</label>
            <mixed-citation id="d470e911" publication-type="other">
Ibid, p 69</mixed-citation>
         </ref>
         <ref id="d470e918a1310">
            <label>32</label>
            <mixed-citation id="d470e925" publication-type="other">
Ibid, p 70</mixed-citation>
         </ref>
         <ref id="d470e932a1310">
            <label>33</label>
            <mixed-citation id="d470e939" publication-type="other">
Brauman, Devant le Mal,
Rwanda: Un Génocide en Direct</mixed-citation>
            <mixed-citation id="d470e948" publication-type="book">
Vincent Faber, 'Rwanda two years after: an ongoing humanitarian
crisis', in Medecins sans Frontieres (eds), World in Crisis: The Politics of Survival at the End of the 20th
Century, pp 161-180.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Faber</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Rwanda two years after: an ongoing humanitarian crisis</comment>
               <fpage>161</fpage>
               <source>World in Crisis: The Politics of Survival at the End of the 20th Century</source>
            </mixed-citation>
         </ref>
         <ref id="d470e980a1310">
            <label>34</label>
            <mixed-citation id="d470e987" publication-type="book">
Medecins sans Frontières, Populations in Danger, 1995: Annual Report, New York: Routledge, 1995, p 38<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Frontières</surname>
                  </string-name>
               </person-group>
               <fpage>38</fpage>
               <source>Populations in Danger, 1995: Annual Report</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d470e1012a1310">
            <label>35</label>
            <mixed-citation id="d470e1019" publication-type="book">
Jean Baudrillard's controversial piece, La Guerre du Golfe n'a pas eu Lieu (The Gulf
War did not Take Place), Paris: Galilee, 1991<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Baudrillard</surname>
                  </string-name>
               </person-group>
               <source>La Guerre du Golfe n'a pas eu Lieu (The Gulf War did not Take Place)</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d470e1045a1310">
            <label>36</label>
            <mixed-citation id="d470e1052" publication-type="other">
Brauman, Devant le Mal,
Rwanda: Un Génocide en Direct, p 83 (my translation)</mixed-citation>
         </ref>
         <ref id="d470e1062a1310">
            <label>37</label>
            <mixed-citation id="d470e1069" publication-type="other">
pamphlet on MSF 'Help Us save a Life Today'</mixed-citation>
         </ref>
         <ref id="d470e1076a1310">
            <label>39</label>
            <mixed-citation id="d470e1083" publication-type="other">
Agnew &amp;
Corbridge, Mastering Space: Hegemony, Territory and International Political Economy</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
