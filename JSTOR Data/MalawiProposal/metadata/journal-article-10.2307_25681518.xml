<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procnatiacadscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100014</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings of the National Academy of Sciences of the United States of America</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Academy of Sciences</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00278424</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25681518</article-id>
         <article-categories>
            <subj-group>
               <subject>COLLOQUIUM PAPERS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Reconstructing human evolution: Achievements, challenges, and opportunities</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bernard</given-names>
                  <surname>Wood</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>11</day>
            <month>5</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">107</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25681515</issue-id>
         <fpage>8902</fpage>
         <lpage>8909</lpage>
         <permissions>
            <copyright-statement>© Copyright National Academy of Sciences</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25681518"/>
         <abstract>
            <p>This contribution reviews the evidence that has resolved the branching structure of the higher primate part of the tree of life and the substantial body of fossil evidence for human evolution. It considers some of the problems faced by those who try to interpret the taxonomy and systematics of the human fossil record. How do you to tell an early human taxon from one in a closely related clade? How do you determine the number of taxa represented in the human clade? How can homoplasy be recognized and factored into attempts to recover phylogeny?</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>[Bibliography]</title>
         <ref id="d857e143a1310">
            <label>1</label>
            <mixed-citation id="d857e150" publication-type="other">
Huxley TH (1863) Evidence as to Man's place in Nature (Williams and Norgate, London).</mixed-citation>
         </ref>
         <ref id="d857e157a1310">
            <label>2</label>
            <mixed-citation id="d857e164" publication-type="other">
Goodman M (1963) Man's place in the phylogeny of the primates as reflected in serum
Proteins. Classification and Human Evolution, ed Washburn SL (Aldine, Chicago), pp
204-234.</mixed-citation>
         </ref>
         <ref id="d857e177a1310">
            <label>3</label>
            <mixed-citation id="d857e184" publication-type="other">
Zuckerkandl E (1963) Perspectives in molecular anthropology. Classification and
Human Evolution, ed Washburn SL (Aldine, Chicago), pp 243-272.</mixed-citation>
         </ref>
         <ref id="d857e194a1310">
            <label>4</label>
            <mixed-citation id="d857e201" publication-type="other">
Sarich V, Wilson AC (1967) Immunological time scale for hominid evolution. Science
158:1200-1203.</mixed-citation>
         </ref>
         <ref id="d857e212a1310">
            <label>5</label>
            <mixed-citation id="d857e219" publication-type="other">
King MC, Wilson AC (1975) Evolution in two levels in humans and chimpanzees.
Science 188:107-116.</mixed-citation>
         </ref>
         <ref id="d857e229a1310">
            <label>6</label>
            <mixed-citation id="d857e236" publication-type="other">
Ruvolo M (1997) Molecular phylogeny of the hominoids: Inferences from multiple
independent DNA sequence data sets. Mol Biol Evol 14:248-265.</mixed-citation>
         </ref>
         <ref id="d857e246a1310">
            <label>7</label>
            <mixed-citation id="d857e253" publication-type="other">
Bradley B (2008) Reconstructing phylogenies and phenotypes: A molecular view of
human evolution. J Anat 212:337-353.</mixed-citation>
         </ref>
         <ref id="d857e263a1310">
            <label>8</label>
            <mixed-citation id="d857e270" publication-type="other">
Fabre PH, Rodrigues A, Douzery EJP (2009) Patterns of macroevolution among
Primates inferred from a supermatrix of mitochondrial and nuclear DNA. Mol
Phylogenet Evol 53:808-825.</mixed-citation>
         </ref>
         <ref id="d857e283a1310">
            <label>9</label>
            <mixed-citation id="d857e290" publication-type="other">
Grehan JR, Schwartz JH (2009) Evolution of the second orangutan: Phylogeny and
biogeography of hominid origins. J Biogeogr 31:1263-1266.</mixed-citation>
         </ref>
         <ref id="d857e300a1310">
            <label>10</label>
            <mixed-citation id="d857e307" publication-type="other">
Harrison T (2010) Apes among the tangled branches of human origins. Science 327:
532-534.</mixed-citation>
         </ref>
         <ref id="d857e318a1310">
            <label>11</label>
            <mixed-citation id="d857e325" publication-type="other">
Huxley JS (1958) Evolutionary processes and taxonomy with special reference to
grades. Upps Univ Arssks 21-38.</mixed-citation>
         </ref>
         <ref id="d857e335a1310">
            <label>12</label>
            <mixed-citation id="d857e342" publication-type="other">
Asfaw B, Ebinger C, Harding D, White T, WoldeGabriel W (1990) Space-based imagery
in paleoanthropological research: An Ethiopian example. Natl Geogr Res 6:418-434.</mixed-citation>
         </ref>
         <ref id="d857e352a1310">
            <label>13</label>
            <mixed-citation id="d857e359" publication-type="other">
McDougall I, Brown FH, Fleagle JG (2005) Stratigraphic placement and age of modern
humans from Kibish, Ethiopia. Nature 433:733-736.</mixed-citation>
         </ref>
         <ref id="d857e369a1310">
            <label>14</label>
            <mixed-citation id="d857e376" publication-type="other">
Krings M, et al. (1997) Neandertal DNA sequences and the origin of modern humans.
Cell 90:19-30.</mixed-citation>
         </ref>
         <ref id="d857e386a1310">
            <label>15</label>
            <mixed-citation id="d857e393" publication-type="other">
Green R, et al. (2008) A complete neandertal mitochondrial genome sequence
determined by high-throughput sequencing. Cell 134:416-426.</mixed-citation>
         </ref>
         <ref id="d857e403a1310">
            <label>16</label>
            <mixed-citation id="d857e410" publication-type="other">
Briggs AW, et al. (2009) Targeted retrieval and analysis of five Neandertal mtDNA
genomes. Science 325:318-321.</mixed-citation>
         </ref>
         <ref id="d857e421a1310">
            <label>17</label>
            <mixed-citation id="d857e428" publication-type="other">
Wood B (1994) Hominid Cranial Remains, Koobi Fora Research Project (Clarendon
Press, Oxford), Vol 4, pp 1-492.</mixed-citation>
         </ref>
         <ref id="d857e438a1310">
            <label>18</label>
            <mixed-citation id="d857e445" publication-type="other">
Spoor F, et al. (2007) Implications of new early Homo fossils from lleret, east of Lake
Turkana, Kenya. Nature 448:688-691.</mixed-citation>
         </ref>
         <ref id="d857e455a1310">
            <label>19</label>
            <mixed-citation id="d857e462" publication-type="other">
Brown P, Moeda T (2009) Liang Bua Homo floresiensis mandibles and mandibular
teeth: A contribution to the comparative morphology of a new hominin species. J
Hum Evol 57:571-596.</mixed-citation>
         </ref>
         <ref id="d857e475a1310">
            <label>20</label>
            <mixed-citation id="d857e482" publication-type="other">
Morwood MJ, Jungers WL (2009) Conclusions: Implications of the Liang Bua
excavations for hominin evolution and biogeography. J Hum Evol 57:640-648.</mixed-citation>
         </ref>
         <ref id="d857e492a1310">
            <label>21</label>
            <mixed-citation id="d857e499" publication-type="other">
Wood BA, Collard M (1999) The human genus. Science 284:65-71.</mixed-citation>
         </ref>
         <ref id="d857e506a1310">
            <label>22</label>
            <mixed-citation id="d857e513" publication-type="other">
Ruff C (2009) Relative limb strength and locomotion in Homo habilis. Am J Phys
Anthropol 138:90-100.</mixed-citation>
         </ref>
         <ref id="d857e524a1310">
            <label>23</label>
            <mixed-citation id="d857e531" publication-type="other">
Tocheri MW, Orr CM, Larson SG (2007) The primitive wrist of Homo floresiensis and its
implications for hominin evolution. Science 317:1743-1745.</mixed-citation>
         </ref>
         <ref id="d857e541a1310">
            <label>24</label>
            <mixed-citation id="d857e548" publication-type="other">
Suwa G, White TD, Howell FC (1996) Mandibular postcanine dentition from the
Shungura Formation, Ethiopia: Crown morphology, taxonomic allocations, and Plio-
Pleistocene hominid evolution. Am J Phys Anthropol 101:247-282.</mixed-citation>
         </ref>
         <ref id="d857e561a1310">
            <label>25</label>
            <mixed-citation id="d857e568" publication-type="other">
Tobias PV (1991) The skulls, endocasts and teeth of Homo habilis. Olduvai Gorge
(Cambridge Univ Press, Cambridge, UK), Vol 4, pp 1-921.</mixed-citation>
         </ref>
         <ref id="d857e578a1310">
            <label>26</label>
            <mixed-citation id="d857e585" publication-type="other">
Clarke RJ (2008) Latest information on Sterkfontein's Australopithecus skeleton and
a new look at Australopithecus. S AfrJ Sci 104:443-449.</mixed-citation>
         </ref>
         <ref id="d857e595a1310">
            <label>27</label>
            <mixed-citation id="d857e602" publication-type="other">
Partridge TC, Granger DE, Caffee MW, Clarke RJ (2003) Lower Pliocene hominid
remains from Sterkfontein. Science 300:607-612.</mixed-citation>
         </ref>
         <ref id="d857e612a1310">
            <label>28</label>
            <mixed-citation id="d857e619" publication-type="other">
Kimbel WH, Delezene LK (2009) "Lucy" redux: A review of research on Australopithecus
afarensis. Am J Phys Anthropol 52:2-48.</mixed-citation>
         </ref>
         <ref id="d857e630a1310">
            <label>29</label>
            <mixed-citation id="d857e637" publication-type="other">
Bennett MR (2009) Early hominin foot morphology based on 1.5-million-year-old
footprints from lleret, Kenya. Science 323:1197-1201.</mixed-citation>
         </ref>
         <ref id="d857e647a1310">
            <label>30</label>
            <mixed-citation id="d857e654" publication-type="other">
Kimbel WH, et al. (2006) Was Australopithecus anamenis ancestral to Australopithecus
afarensis? A case of anagenesis in the hominin fossil record. J Hum Evol 51:134-152.</mixed-citation>
         </ref>
         <ref id="d857e664a1310">
            <label>31</label>
            <mixed-citation id="d857e671" publication-type="other">
Leakey MG, et al. (2001) New hominin genus from eastern Africa shows diverse
middle Pliocene lineages. Nature 410:433-440.</mixed-citation>
         </ref>
         <ref id="d857e681a1310">
            <label>32</label>
            <mixed-citation id="d857e688" publication-type="other">
White TD (2003) Early hominids—diversity or distortion? Science 299:1994-1997.</mixed-citation>
         </ref>
         <ref id="d857e695a1310">
            <label>33</label>
            <mixed-citation id="d857e702" publication-type="other">
Wood B, Constantino P (2Q09) Paranthropus boisei: Fifty years of evidence and
analysis. Am J Phys Anthropol 50:106-132.</mixed-citation>
         </ref>
         <ref id="d857e712a1310">
            <label>34</label>
            <mixed-citation id="d857e719" publication-type="other">
Wood BA, Wood CW, Konigsberg LW (1994) Paranthropus boisei—an example of
evolutionary stasis? Am J Phys Anthropol 95:117-136.</mixed-citation>
         </ref>
         <ref id="d857e730a1310">
            <label>35</label>
            <mixed-citation id="d857e737" publication-type="other">
Asfaw B, et al. (1999) Australopithecus garh'r. A new species of early hominid from
Ethiopia. Science 284:629-635.</mixed-citation>
         </ref>
         <ref id="d857e747a1310">
            <label>36</label>
            <mixed-citation id="d857e754" publication-type="other">
White TD, Suwa G, Asfaw B (1994) Australopithecus ramidus, a new species of early
hominid from Aramis, Ethiopia. Nature 371:306-312.</mixed-citation>
         </ref>
         <ref id="d857e764a1310">
            <label>37</label>
            <mixed-citation id="d857e771" publication-type="other">
White TD, Suwa G, Asfaw B (1995) Australopithecus ramidus, a new species of early
hominid from Aramis, Ethiopia— a corrigendum. Nature 375:88.</mixed-citation>
         </ref>
         <ref id="d857e781a1310">
            <label>38</label>
            <mixed-citation id="d857e788" publication-type="other">
White TD, et al. (2009) Ardipithecus ramidus and the paleobiology of early hominins.
Science 326:75-86.</mixed-citation>
         </ref>
         <ref id="d857e798a1310">
            <label>39</label>
            <mixed-citation id="d857e805" publication-type="other">
Senut B, Pickford M, Gommery D, Mein P, Cheboi K, Coppens Y (2001) First hominid
from the Miocene (Lukeino Formation, Kenya). Comptes Rendus de I'Academie des
Sciences 332:137-144.</mixed-citation>
         </ref>
         <ref id="d857e818a1310">
            <label>40</label>
            <mixed-citation id="d857e825" publication-type="other">
Richmond BG, Jungers WL (2008) Orrorin tugenensis femoral morphology and the
evolution of hominin bipedalism. Science 319:1662-1665.</mixed-citation>
         </ref>
         <ref id="d857e836a1310">
            <label>41</label>
            <mixed-citation id="d857e843" publication-type="other">
Ohman JC, Lovejoy CO, White T (2005) Questions about Orrorin tugenensis. Science
307:845.</mixed-citation>
         </ref>
         <ref id="d857e853a1310">
            <label>42</label>
            <mixed-citation id="d857e860" publication-type="other">
Brunet M, et al. (2002) A new hominid from the Upper Miocene of Chad, Central
Africa. Nature 418:145-151.</mixed-citation>
         </ref>
         <ref id="d857e870a1310">
            <label>43</label>
            <mixed-citation id="d857e877" publication-type="other">
Haile-Selassie Y (2001) Late Miocene hominids from the Middle Awash, Ethiopia.
Nature 412:178-181.</mixed-citation>
         </ref>
         <ref id="d857e887a1310">
            <label>44</label>
            <mixed-citation id="d857e894" publication-type="other">
Haile-Selassie Y, Asfaw B, White TD (2004) Hominid cranial remains from Upper
Pleistocene deposits at Aduma, Middle Awash, Ethiopia. Am J Phys Anthropol 123:
1-10.</mixed-citation>
         </ref>
         <ref id="d857e907a1310">
            <label>45</label>
            <mixed-citation id="d857e914" publication-type="other">
Smith AB (1994) Systematics and the Fossil Record: Documenting Evolutionary
Patterns (Blackwell, Oxford).</mixed-citation>
         </ref>
         <ref id="d857e924a1310">
            <label>46</label>
            <mixed-citation id="d857e931" publication-type="other">
Sokal RR, Crovello TJ (1970) The biological species concept: A critical evaluation. Am
Nat 104:127-153.</mixed-citation>
         </ref>
         <ref id="d857e942a1310">
            <label>47</label>
            <mixed-citation id="d857e949" publication-type="other">
Cracraft J (1983) Species concepts and speciation analysis. Current Ornithology, ed
Johnson RF (Plenum Press, New York).</mixed-citation>
         </ref>
         <ref id="d857e959a1310">
            <label>48</label>
            <mixed-citation id="d857e966" publication-type="other">
Nixon KC, Wheeler QD (1990) An amplification of the phylogenetic species concept.
Cladistics 6:211-233.</mixed-citation>
         </ref>
         <ref id="d857e976a1310">
            <label>49</label>
            <mixed-citation id="d857e983" publication-type="other">
Wood B (2000) Investigating human evolutionary history. J Anat 197:1-17.</mixed-citation>
         </ref>
         <ref id="d857e990a1310">
            <label>50</label>
            <mixed-citation id="d857e997" publication-type="other">
Green DJ, Gordon AD, Richmond BG (2007) Limb-size proportions in Australopithecus
afarensis and Australopithecus africanus. J Hum Evol 52:187-200.</mixed-citation>
         </ref>
         <ref id="d857e1007a1310">
            <label>51</label>
            <mixed-citation id="d857e1014" publication-type="other">
Spoor F, Jeffery N, Zonneveld F (2000) Using diagnostic radiology in human evo-
lutionary studies. J Anat 197:61-76.</mixed-citation>
         </ref>
         <ref id="d857e1024a1310">
            <label>52</label>
            <mixed-citation id="d857e1031" publication-type="other">
Kono R (2004) Molar enamel thickness and distribution patterns in extant primates
and humans: New insights based on a 3-dimensional whole crown perspective.
Anthropol Sci 112:121-146.</mixed-citation>
         </ref>
         <ref id="d857e1045a1310">
            <label>53</label>
            <mixed-citation id="d857e1052" publication-type="other">
Bromage TG, Perez-Ochoa A, Boyde A (2005) Portable confocal microscope reveals
fossil hominid microstructure. Microscop Anal 19:5-7.</mixed-citation>
         </ref>
         <ref id="d857e1062a1310">
            <label>54</label>
            <mixed-citation id="d857e1069" publication-type="other">
Tafforeau P, Smith TM (2007) Nondestructive imaging of hominoid dental micro-
structure using phase contrast X-ray synchrotron microtomography. J Hum Evol 54:
272-278.</mixed-citation>
         </ref>
         <ref id="d857e1082a1310">
            <label>55</label>
            <mixed-citation id="d857e1089" publication-type="other">
Smith T, Tafforeau P (2008) New visions of dental tissue research: tooth development,
chemistry, and structure. Evol Anthropol 17:213-226.</mixed-citation>
         </ref>
         <ref id="d857e1099a1310">
            <label>56</label>
            <mixed-citation id="d857e1106" publication-type="other">
Skinner M, et al. (2008) Dental trait expression at the enamel-dentine junction of
lower molars in extant and fossil hominoids. J Hum Evol 54:173-186.</mixed-citation>
         </ref>
         <ref id="d857e1116a1310">
            <label>57</label>
            <mixed-citation id="d857e1123" publication-type="other">
Braga J, et al. (2010) The enamel-dentine junction in the postcanine dentition of
Australopithecus africanus: Intra-individual metameric and antimreric variation. J
Anat 216:62-79.</mixed-citation>
         </ref>
         <ref id="d857e1136a1310">
            <label>58</label>
            <mixed-citation id="d857e1143" publication-type="other">
Skinner M, Wood BA, Hublin J-JH (2008) Enamel-dentine junction (EDJ) morphology
distinguishes the lower molars of Australopithecus africanus and Paranthropus
robustus. J Hum Evol 55:979-988.</mixed-citation>
         </ref>
         <ref id="d857e1157a1310">
            <label>59</label>
            <mixed-citation id="d857e1164" publication-type="other">
Skinner M, Wood BA, Hublin J-JH (2009) Protostylid expression at the enamel-dentine
junction and enamel surface of mandibular molars of Paranthropus robustus and
Australopithecus africanus. J Hum Evol 56:76-85.</mixed-citation>
         </ref>
         <ref id="d857e1177a1310">
            <label>60</label>
            <mixed-citation id="d857e1184" publication-type="other">
Dean C (2000) Progress in understanding hominoid dental development. J Anat 197:
77-101.</mixed-citation>
         </ref>
         <ref id="d857e1194a1310">
            <label>61</label>
            <mixed-citation id="d857e1201" publication-type="other">
Lacruz RS, Dean MC, Ramirez-Rossi F, Bromage TG (2008) Megadontia, striae
periodicity and patterns of enamel secretion in Plio-Pleistocene fossil hominins. J Anat
213:148-158.</mixed-citation>
         </ref>
         <ref id="d857e1214a1310">
            <label>62</label>
            <mixed-citation id="d857e1221" publication-type="other">
Berger LR, et al. (2010) Australopithecus sediba: A new species of Homo-like
australopith from South Africa. Science 328:195-204.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
