<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">intejsoci</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000659</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>International Journal of Sociology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>M. E. Sharpe</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00207659</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15579336</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20788545</article-id>
         <title-group>
            <article-title>Global Warmers and Global Coolers: A Cross-National Examination of Global Warming Dynamics</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gregory M.</given-names>
                  <surname>Fulkerson</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Laura A.</given-names>
                  <surname>McKinney</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Kelly</given-names>
                  <surname>Austin</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">40</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20788540</issue-id>
         <fpage>44</fpage>
         <lpage>64</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 M.E. Sharpe, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.2753/IJS0020-7659400203"
                   xlink:title="an external site"/>
         <abstract>
            <p>Carbon dioxide emissions are the largest anthropogenic contributor to global warming, a chief concern our society faces today. Carbon sequestration is a naturally occurring process of carbon absorption by the atmosphere, oceans, and terrestrial ecosystems, thereby decelerating warming trends. Indeed, the conservation of land to optimize carbon storage is an especially important strategy for nations to lessen changes in the greenhouse effect. Due to the significantly greater capacity of trees for carbon uptake per unit area compared to other vegetation, abundant national forests remain an especially critical defense against these noxious pollutants. Applying available cross-national data on carbon dioxide emissions and forest biocapacity (i.e., carbon storage capacity), the authors derive estimates for each country's net contribution to, or alleviation of, global warming trends—what we refer to as the "climate footprint." This is a new addition to the large literature on national greenhouse gas emissions and forestation dynamics as they relate to global warming that has not been examined empirically in the environmental sociology literature. We seek to fill this gap by constructing structural equation models with the climate footprint of nations as the dependent variable. Findings support world-system theory with mixed support for world polity predictions. The authors conclude with a discussion of the findings, public policy implications, and directions for future research.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d659e186a1310">
            <label>1</label>
            <mixed-citation id="d659e193" publication-type="other">
Meyer et al. (1997a)</mixed-citation>
         </ref>
         <ref id="d659e200a1310">
            <label>3</label>
            <mixed-citation id="d659e207" publication-type="other">
Smith (2004)</mixed-citation>
            <mixed-citation id="d659e213" publication-type="other">
Wiest (2005).</mixed-citation>
         </ref>
         <ref id="d659e220a1310">
            <label>5</label>
            <mixed-citation id="d659e227" publication-type="other">
Kentor 1981</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d659e243a1310">
            <mixed-citation id="d659e247" publication-type="other">
Amin, S. 1976. Unequal Development: An Essay on the Social Formations of
Peripheral Capitalism. New York: Monthly Review Press.</mixed-citation>
         </ref>
         <ref id="d659e257a1310">
            <mixed-citation id="d659e261" publication-type="other">
Bollen, K. 1983. "World-System Position, Dependency and Democracy: The
Cross-National Evidence." American Sociological Review 48, no. 1: 468-79.</mixed-citation>
         </ref>
         <ref id="d659e271a1310">
            <mixed-citation id="d659e275" publication-type="other">
—. 1989. Structural Equations with Latent Variables. New York: Wiley.</mixed-citation>
         </ref>
         <ref id="d659e282a1310">
            <mixed-citation id="d659e286" publication-type="other">
Bradshaw, Y., and M. Schafer. 2000. "Urbanization and Development: The
Emergence of International Non-Governmental Organizations Among Declining
States." Sociological Perspectives 43, no. 1: 97-116.</mixed-citation>
         </ref>
         <ref id="d659e300a1310">
            <mixed-citation id="d659e304" publication-type="other">
Brown, S.; A.E. Lugo; and L.R. Iverson. 1992. "Processes and Land for
Sequestering Carbon in the Tropical Forest Landscape." Water, Air, and Soil
Pollution 64, no. 1: 139-55.</mixed-citation>
         </ref>
         <ref id="d659e317a1310">
            <mixed-citation id="d659e321" publication-type="other">
Bunker, S.G. 1985. Underdeveloping the Amazon: Extraction, Unequal Exchange
and the Failure of the Modern State. Urbana: University of Illinois Press.</mixed-citation>
         </ref>
         <ref id="d659e331a1310">
            <mixed-citation id="d659e335" publication-type="other">
Buttel, F.H. 2000. "World Society, the Nation-State, and Environmental
Protection." American Sociological Review 65, no. 1: 117-21.</mixed-citation>
         </ref>
         <ref id="d659e345a1310">
            <mixed-citation id="d659e349" publication-type="other">
Burns, T.J.; B. Davis; and E.L. Kick. 1997. "Position in the World-System and
National Emissions of Greenhouse Gases." Journal of World-System Research 3,
no. 1:432-66.</mixed-citation>
         </ref>
         <ref id="d659e362a1310">
            <mixed-citation id="d659e366" publication-type="other">
Burns, T.J.; E.L. Kick; and B. Davis. 2003. "Theorizing and Rethinking Linkages
Between the Natural Environment and the Modern World-System: Deforestation
in the Late 20th Century." Journal of World-System Research 9, no. 1: 357-90.</mixed-citation>
         </ref>
         <ref id="d659e379a1310">
            <mixed-citation id="d659e383" publication-type="other">
Burns, T.J.; E.L. Kick; D. Murray; and D. Murray. 1994. "Demography,
Development and Deforestation in a World-System Perspective." International
Journal of Comparative Societies 3, no. 4: 221-39.</mixed-citation>
         </ref>
         <ref id="d659e397a1310">
            <mixed-citation id="d659e401" publication-type="other">
Byrne, B.M. 2001. Structural Equation Modeling with AMOS. Mahwah, NJ:
Erlbaum.</mixed-citation>
         </ref>
         <ref id="d659e411a1310">
            <mixed-citation id="d659e415" publication-type="other">
Clausen, R., and R. York. 2008. "Economic Growth and Marine Biodiversity:
Influence of Human Social Structure on Decline of Marine Trophic Levels."
Conservation Biology 22, no. 2: 458-66.</mixed-citation>
         </ref>
         <ref id="d659e428a1310">
            <mixed-citation id="d659e432" publication-type="other">
Commoner, B. 1972. "The Environmental Cost of Economic Growth." In
Population, Resources and the Environment, ed. R.G. Ridker, 339-63.
Washington, DC: Government Printing Office.</mixed-citation>
         </ref>
         <ref id="d659e445a1310">
            <mixed-citation id="d659e449" publication-type="other">
Durkheim, E. 1984 [1893]. The Division of Labor in Society. Trans. W.D. Halls.
New York: Free Press.</mixed-citation>
         </ref>
         <ref id="d659e459a1310">
            <mixed-citation id="d659e463" publication-type="other">
Espenshade, E.B., Jr., ed. 1993. Goode's World Atlas. 18th ed. New York: Rand
McNally.</mixed-citation>
         </ref>
         <ref id="d659e473a1310">
            <mixed-citation id="d659e477" publication-type="other">
Foster, J.B. 2000. Marx's Ecology. New York: Monthly Review Press.</mixed-citation>
         </ref>
         <ref id="d659e485a1310">
            <mixed-citation id="d659e489" publication-type="other">
—. 2009. The Ecological Revolution. New York: Monthly Review Press.</mixed-citation>
         </ref>
         <ref id="d659e496a1310">
            <mixed-citation id="d659e500" publication-type="other">
Frank, D.J. 1997. "Science, Nature, and the Globalization of the Environment,
1870-1990." Social Forces 76, no. 2: 409-35.</mixed-citation>
         </ref>
         <ref id="d659e510a1310">
            <mixed-citation id="d659e514" publication-type="other">
Frank, D.J.; A. Hironaka; and E. Schofer. 2000a. "The Nation-State and the Natural
Environment over the Twentieth Century." American Sociological Review 65,
no. 1:96-116.</mixed-citation>
         </ref>
         <ref id="d659e527a1310">
            <mixed-citation id="d659e531" publication-type="other">
—. 2000b. "Environmentalism as a Global Institution: Reply to Buttel."
American Sociological Review 65, no. 1: 122-27.</mixed-citation>
         </ref>
         <ref id="d659e541a1310">
            <mixed-citation id="d659e545" publication-type="other">
Frey, R.S. 1995. "The International Traffic in Pesticides." Technological
Forecasting and Social Change 50, no. 1: 151-69.</mixed-citation>
         </ref>
         <ref id="d659e555a1310">
            <mixed-citation id="d659e559" publication-type="other">
Global Footprint Network. 2009. "Footprint for Nations." 2008 Data Tables.
Available at www.footprintnetwork.org/en/index.php/GFN/page/footprint_for_
nations/ (accessed February 4, 2009).</mixed-citation>
         </ref>
         <ref id="d659e573a1310">
            <mixed-citation id="d659e577" publication-type="other">
Hafner-Burton, E.M., and K. Tsutsui. 2005. "Human Rights in a Globalizing
World: The Paradox of Empty Promises." American Journal of Sociology 110,
no. 3: 1373-411.</mixed-citation>
         </ref>
         <ref id="d659e590a1310">
            <mixed-citation id="d659e594" publication-type="other">
Houghton, R.A. 2007. "Balancing the Global Carbon Budget." Annual Review of
Earth and Planetary Sciences 35, no. 1: 313-47.</mixed-citation>
         </ref>
         <ref id="d659e604a1310">
            <mixed-citation id="d659e608" publication-type="other">
Joreskog, K.G. 1993. "Testing Structural Equation Models." In Testing Structural
Equation Models, ed. K. Bollen and R. Long, 36-48. Newbury Park, CA: Sage.</mixed-citation>
         </ref>
         <ref id="d659e618a1310">
            <mixed-citation id="d659e622" publication-type="other">
Kentor, Jeffrey. 1981. "Structural Determinants of Peripheral Urbanization: The
Effects of International Dependence." American Sociological Review 46:
201-11.</mixed-citation>
         </ref>
         <ref id="d659e635a1310">
            <mixed-citation id="d659e639" publication-type="other">
Kick, E.L. 1987. "World-System Structure, National Development, and the
Prospects for a Socialist World Order." In America's Changing Role in the
World-system, ed. T. Boswell and A. Bergesen, 127-55. New York: Praeger.</mixed-citation>
         </ref>
         <ref id="d659e652a1310">
            <mixed-citation id="d659e656" publication-type="other">
Kick, Edward L.; Laura A. McKinney; Steve McDonald; and Andrew Jorgenson.
Forthcoming. "World-System Position: A Network Analysis of Nations."
In Sage Handbook of Social Network Analysis, ed. John Scott and Peter
Carrington. Thousand Oaks, CA: Sage.</mixed-citation>
         </ref>
         <ref id="d659e673a1310">
            <mixed-citation id="d659e677" publication-type="other">
Kick, E.L.; T.J. Burns; B. Davis; D.A. Murray; and D.A. Murray. 1996. "Impacts
of Domestic Population Dynamics and Foreign Wood Trade on Deforestation: A
World-system Perspective." Journal of Developing Societies 12, no. 1: 68-87.</mixed-citation>
         </ref>
         <ref id="d659e690a1310">
            <mixed-citation id="d659e694" publication-type="other">
Kline, R.B. 2005. Principles and Practice of Structural Equation Modeling. New
York: Guilford Press.</mixed-citation>
         </ref>
         <ref id="d659e704a1310">
            <mixed-citation id="d659e708" publication-type="other">
Lemer, D. 1958. The Passing of Traditional Society. New York: Free Press.</mixed-citation>
         </ref>
         <ref id="d659e715a1310">
            <mixed-citation id="d659e719" publication-type="other">
Lugo, A.E., and S. Brown. 1992. "Tropical Forests as Sinks of Atmospheric
Carbon." Forest Ecology and Management 54, no. 1: 239-55.</mixed-citation>
         </ref>
         <ref id="d659e729a1310">
            <mixed-citation id="d659e733" publication-type="other">
McKinney, L.A.; G.M. Fulkerson; and E.L. Kick. 2009."Investigating the
Correlates of Biodiversity Loss: A Cross-National Quantitative Analysis of
Threatened Bird Species." Human Ecology Review 16, no. 1: 103-13.</mixed-citation>
         </ref>
         <ref id="d659e746a1310">
            <mixed-citation id="d659e750" publication-type="other">
Meyer, J.W.; J. Boli; G.M. Thomas; and F.O. Ramirez. 1997a. "World Society and
the Nation-State." American Journal of Sociology 103 no. 1: 144-81.</mixed-citation>
         </ref>
         <ref id="d659e761a1310">
            <mixed-citation id="d659e765" publication-type="other">
Meyer, J.W.; D.J. Frank; A. Hironaka; E. Schofer; and N.B. Turna. 1997b. "The
Structuring of a World Environmental Regime, 1870-1990." International
Organization 51, no. 2: 623-51.</mixed-citation>
         </ref>
         <ref id="d659e778a1310">
            <mixed-citation id="d659e782" publication-type="other">
Moore, J.W. 2003. "The Modem World-System as Environmental History?
Ecology and the Rise of Capitalism." Theory and Society 32, no. 3: 307-77.</mixed-citation>
         </ref>
         <ref id="d659e792a1310">
            <mixed-citation id="d659e796" publication-type="other">
O'Connor, J. 1988. "Capitalism, Nature, and Socialism: A Theoretical
Introduction." Capitalism, Nature, Socialism 1, no. 1: 11-38.</mixed-citation>
         </ref>
         <ref id="d659e806a1310">
            <mixed-citation id="d659e810" publication-type="other">
—. 1991. "On the Two Contradictions of Capitalism." Capitalism, Nature,
Socialism 2, no. 3: 107-9.</mixed-citation>
         </ref>
         <ref id="d659e820a1310">
            <mixed-citation id="d659e824" publication-type="other">
Parsons, T. 1951. The Social System. Glencoe, IL: Free Press of Glencoe.</mixed-citation>
         </ref>
         <ref id="d659e831a1310">
            <mixed-citation id="d659e835" publication-type="other">
Roberts, J.T., and B.C. Parks. 2006. A Climate of Injustice: Global Inequality,
North-South Politics and Climate Policy. Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d659e846a1310">
            <mixed-citation id="d659e850" publication-type="other">
Roberts, J.T., and N.D. Thanos. 2004. Trouble in Paradise. New York, Routledge.</mixed-citation>
         </ref>
         <ref id="d659e857a1310">
            <mixed-citation id="d659e861" publication-type="other">
Roberts, J.T.; P. Grimes; and J. Manale. 2003. "Social Roots of Global
Environmental Change: A World-Systems Analysis of Carbon Dioxide
Emissions." Journal of World-System Research 9, no. 2: 277-315.</mixed-citation>
         </ref>
         <ref id="d659e874a1310">
            <mixed-citation id="d659e878" publication-type="other">
Roberts, J.T.; B.C. Parks; and A.A. Vasquez. 2004. "Who Ratifies Environmental
Treaties and Why? Institutionalism, Structuralism and Participation by 192
Nations in 22 Treaties." Global Environmental Politics 4, no. 3: 22-64.</mixed-citation>
         </ref>
         <ref id="d659e891a1310">
            <mixed-citation id="d659e895" publication-type="other">
Rosa, E. 2001. "Global Climate Change: Background and Sociological
Contributions." Society and Natural Resources 14, no. 2: 491-99.</mixed-citation>
         </ref>
         <ref id="d659e905a1310">
            <mixed-citation id="d659e909" publication-type="other">
Rostow, W.W. 1960. The State of Economic Growth. Cambridge: Cambridge
University Press.</mixed-citation>
         </ref>
         <ref id="d659e919a1310">
            <mixed-citation id="d659e923" publication-type="other">
Rudel, T.K. 1989. "Population, Development, and Tropical Deforestation: A Cross-
National Study." Rural Sociology 54, no. 1: 327-38.</mixed-citation>
         </ref>
         <ref id="d659e934a1310">
            <mixed-citation id="d659e940" publication-type="other">
Schofer, E.; F.O. Ramirez; and J.W. Meyer. 2000. "The Effects of Science on
National Economic Development, 1970 to 1990." American Sociological
Review 65, no. 6: 866-87.</mixed-citation>
         </ref>
         <ref id="d659e953a1310">
            <mixed-citation id="d659e957" publication-type="other">
Shandra, J. 2007. "Economic Dependency, Repression, and Deforestation: A Cross-
National Analysis." Sociological Inquiry 77, no. 4 (November): 543-71.</mixed-citation>
         </ref>
         <ref id="d659e967a1310">
            <mixed-citation id="d659e971" publication-type="other">
Shandra, J.; M. Restivo; and B. London. 2008. "Non-Governmental Organizations
and Deforestation: Reconsidering the Cross-National Evidence." International
Review of Modern Sociology 34, no. 1: 109-32.</mixed-citation>
         </ref>
         <ref id="d659e984a1310">
            <mixed-citation id="d659e988" publication-type="other">
Shandra, J.; E. Shor; and B. London. 2008. "Debt, Structural Adjustment, and
Organic Water Pollution: A Cross-National Analysis." Organization and
Environment 21, no. 1: 38-55.</mixed-citation>
         </ref>
         <ref id="d659e1001a1310">
            <mixed-citation id="d659e1005" publication-type="other">
Smelser, N. 1964. Toward a Theory of Modernization. New York: Basic Books.</mixed-citation>
         </ref>
         <ref id="d659e1012a1310">
            <mixed-citation id="d659e1016" publication-type="other">
Smith, J. 2004. "Exploring Connections between Globalization and Political
Mobilization." Journal of World-System Research 10, no. 1: 255-85.</mixed-citation>
         </ref>
         <ref id="d659e1027a1310">
            <mixed-citation id="d659e1031" publication-type="other">
Smith, J., and D.R. Wiest. 2005. "The Uneven Geography of Global Civil Society:
National and Global Influences on Transnational Association." Social Forces 84,
no. 2: 621-52.</mixed-citation>
         </ref>
         <ref id="d659e1044a1310">
            <mixed-citation id="d659e1048" publication-type="other">
Snyder, D., and E.L. Kick. 1979. "Structural Position in the World-system and
Economic Growth, 1955-1970: A Multiple-Network Analysis of Transnational
Interactions." American Journal of Sociology, 84, no. 3: 1096-126.</mixed-citation>
         </ref>
         <ref id="d659e1061a1310">
            <mixed-citation id="d659e1065" publication-type="other">
Sohngen, B.; C. Tennity; M. Hnytka; and K. Meeusen. 2009. "Global Forestry Data
for the Economic Modeling of Land Use." In Analysis of Land Use in Global
Climate Change Policy, ed. T. Hertel, S. Rose, and R. Toi, 182-203. New York:
Routledge.</mixed-citation>
         </ref>
         <ref id="d659e1081a1310">
            <mixed-citation id="d659e1085" publication-type="other">
Wackemagel, M., and W. Rees. 1996. Our Ecological Footprint: Reducing Human
Impact on the Earth. Gabriola Island, BC: New Society.</mixed-citation>
         </ref>
         <ref id="d659e1095a1310">
            <mixed-citation id="d659e1099" publication-type="other">
Wallerstein, I. 1974. The Modern World System. New York: Academic Press.</mixed-citation>
         </ref>
         <ref id="d659e1106a1310">
            <mixed-citation id="d659e1110" publication-type="other">
—. 2004. World Systems Analysis: An Introduction. Durham: Duke University
Press.</mixed-citation>
         </ref>
         <ref id="d659e1121a1310">
            <mixed-citation id="d659e1125" publication-type="other">
World Resource Institute (WRI). 2009. EarthTrends. Available at http://earthtrends.
wri.org (accessed June 2, 2009).</mixed-citation>
         </ref>
         <ref id="d659e1135a1310">
            <mixed-citation id="d659e1139" publication-type="other">
York, R., and E.A. Rosa. 2003. "Key Challenges to Ecological Modernization
Theory." Organization and Environment, 16, no. 2: 273-88.</mixed-citation>
         </ref>
         <ref id="d659e1149a1310">
            <mixed-citation id="d659e1153" publication-type="other">
York, R.; B. Clark; and J.B. Foster. 2009. "Capitalism in Wonderland." Monthly
Review 61, no. 1 (May): 1-18.</mixed-citation>
         </ref>
         <ref id="d659e1163a1310">
            <mixed-citation id="d659e1167" publication-type="other">
York, R.; E.A. Rosa; and T. Dietz. 2003. "A Rift in Modernity? Assessing the
Anthropogenic Sources of Global Climate Change with the STIRPAT Model."
International Journal of Sociology and Social Policy 23, no. 10: 31-51.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
