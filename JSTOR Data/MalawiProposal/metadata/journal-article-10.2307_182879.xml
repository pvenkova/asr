<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jafricanhistory</journal-id>
         <journal-id journal-id-type="jstor">j100201</journal-id>
         <journal-title-group>
            <journal-title>The Journal of African History</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00218537</issn>
         <issn pub-type="epub">14695138</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">182879</article-id>
         <article-categories>
            <subj-group>
               <subject>Courtroom Dramas</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title> Text and Testimony in the Tribunal de Première Instance, Dakar, during the Early Twentieth Century </article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Richard</given-names>
                  <surname>Roberts</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>1</month>
            <year>1990</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">31</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i209485</issue-id>
         <fpage>447</fpage>
         <lpage>463</lpage>
         <page-range>447-463</page-range>
         <permissions>
            <copyright-statement>Copyright 1990 Cambridge University Press</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/182879"/>
         <abstract>
            <p>Historians have recently come to appreciate the importance of studying the colonial legal system and the potential in mining court cases for historical data. This article is a preliminary effort to present a methodology for the study of the records of the entry level civil court, the tribunal de premiere instance, in Dakar, Senegal. The records are somewhat peculiar, because they are the consequence of the extension of the legal rights to the African originaires of the four communes of Senegal, which empowered them to bring civil cases before this court. However, these records share with records from other courts in colonial Africa problems of determining how the litigants' 'testimony' was shaped by the legal procedures of the court. This article, therefore, focusses on the context in which litigants' testimony was transformed into the texts we read as court records. In particular, it examines how the phases of litigation and how the court's bias towards written evidence shaped the court records. This research was stimulated in part by the need to locate new sources providing African 'voices' about the changes associated with the transition to colonialism. This article concludes with an appraisal of the historical potential of using court records for African social history.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d498e144a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d498e151" publication-type="other">
Archives
Nationales, République du Sénégal (ANS) 5 M 201, 1914-15</mixed-citation>
            </p>
         </fn>
         <fn id="d498e161a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d498e170" publication-type="book">
M. Chanock, Law, Custom, and Social Order : The Colonial Experience
in Malawi and Zambia (Cambridge, 1985)<person-group>
                     <string-name>
                        <surname>Chanock</surname>
                     </string-name>
                  </person-group>
                  <source>Law, Custom, and Social Order : The Colonial Experience in Malawi and Zambia</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e194" publication-type="journal">
M. Chanock, 'Writing South African legal
history: a prospectus ', J. Afr. Hist., XXX (1989), 265-88<object-id pub-id-type="jstor">10.2307/183068</object-id>
                  <fpage>265</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e210" publication-type="book">
M. J. Hay and M. Wright (eds.),
African Women and the Law : Historical Perspectives (Boston, 1982)<person-group>
                     <string-name>
                        <surname>Hay</surname>
                     </string-name>
                  </person-group>
                  <source>African Women and the Law : Historical Perspectives</source>
                  <year>1982</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e234" publication-type="book">
S. Falk Moore,
Social Facts and Fabrications : 'Customary' Law on Kilimanjaro, 1880-1980 (Cambridge,
1986)<person-group>
                     <string-name>
                        <surname>Moore</surname>
                     </string-name>
                  </person-group>
                  <source>Social Facts and Fabrications : 'Customary' Law on Kilimanjaro, 1880-1980</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e262" publication-type="book">
M. Crowder, The Flogging of Phinehas McIntosh : a Tale of
Colonial Folly and Injustice, Bechuanaland 1933 (New Haven, 1988)<person-group>
                     <string-name>
                        <surname>Crowder</surname>
                     </string-name>
                  </person-group>
                  <source>The Flogging of Phinehas McIntosh : a Tale of Colonial Folly and Injustice, Bechuanaland 1933</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e286" publication-type="journal">
Richard
Rathbone, 'A murder in the colonial Gold Coast: law and politics in the I940S', J. Afr.
Hist., XXX (1989), 445-6 I.<object-id pub-id-type="jstor">10.2307/182918</object-id>
                  <fpage>445</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e305a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d498e312" publication-type="book">
F. Snyder, Capitalism and Legal Change : an African Transformation (New York, 1981)<person-group>
                     <string-name>
                        <surname>Snyder</surname>
                     </string-name>
                  </person-group>
                  <source>Capitalism and Legal Change : an African Transformation</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e333" publication-type="journal">
Dominique Sarr, 'La Chambre Speciale d'Homologation de la Cour d'Appel de I'AOF
et les coutumes penales de 1903-1920', Annales Africaines, I (1974), 101-i6<person-group>
                     <string-name>
                        <surname>Sarr</surname>
                     </string-name>
                  </person-group>
                  <fpage>101</fpage>
                  <volume>I</volume>
                  <source>Annales Africaines</source>
                  <year>1974</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e364" publication-type="journal">
D. Sarr,
'Jurisprudence des Tribunaux Indigenes du Senegal: Les causes de rupture du lien
matrimonial de I872 a 1946', Annales Africaines, II (1975), 143-78<person-group>
                     <string-name>
                        <surname>Sarr</surname>
                     </string-name>
                  </person-group>
                  <fpage>143</fpage>
                  <volume>II</volume>
                  <source>Annales Africaines</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e399" publication-type="other">
R. A. Dunbar,
'The evolution of bridewealth and dowry in West Africa : case studies from Senegal and
Niger', unpublished paper presented at the Stanford-Emory Conference on Law in
Colonial Africa, Stanford, 1988</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e414" publication-type="journal">
C. Dickerman, 'The use of court records as sources for African history: some
examples from Bujumbura, Burundi', History in Africa, xi (1984), 69-8I<object-id pub-id-type="doi">10.2307/3171628</object-id>
                  <fpage>69</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e429" publication-type="book">
C.
Dickerman, 'African courts under the colonial regime: Usumbura, Ruanda-Urundi,
1938-62', paper presented as part of the series on 'Sources and methods for the study of
law in colonial Africa', annual meeting of the African Studies Association, Atlanta, 1989<person-group>
                     <string-name>
                        <surname>Dickerman</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">African courts under the colonial regime: Usumbura, Ruanda-Urundi, 1938-62</comment>
                  <source>series on 'Sources and methods for the study of law in colonial Africa', annual meeting of the African Studies Association, Atlanta, 1989</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e461a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d498e468" publication-type="book">
' Sources and methods for
the study of law in colonial Africa', annual meeting of the African Studies Association,
Atlanta, 1989, organized by K. Mann and R. Roberts<person-group>
                     <string-name>
                        <surname>Mann</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Sources and methods for the study of law in colonial Africa</comment>
                  <source>annual meeting of the African Studies Association, Atlanta, 1989</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e498a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d498e505" publication-type="book">
The Stanford-Emory conference on 'Law in colonial Africa' was supported by the
SSRC, the Ford Foundation, Stanford Humanities Center, and by both Emory and
Stanford universities<source>The Stanford-Emory conference on 'Law in colonial Africa' was supported by the SSRC, the Ford Foundation, Stanford Humanities Center, and by both Emory and Stanford universities</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e520" publication-type="book">
R. Roberts, 'The case of
Faama Mademba Sy and the ambiguities of legal jurisdiction in early colonial French
Soudan', in K. Mann and R. Roberts (eds), Law in Colonial Africa (Portsmouth, NH,
forthcoming)<person-group>
                     <string-name>
                        <surname>Roberts</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The case of Faama Mademba Sy and the ambiguities of legal jurisdiction in early colonial French Soudan</comment>
                  <source>Law in Colonial Africa</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e552a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d498e559" publication-type="other">
Donald Moore and Richard
Roberts, 'Listening for silences'</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e568" publication-type="journal">
Richard Roberts, 'Reversible social processes,
historical memory, and the production of history', History in Africa (forthcoming)<person-group>
                     <string-name>
                        <surname>Roberts</surname>
                     </string-name>
                  </person-group>
                  <source>History in Africa</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e590a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d498e597" publication-type="book">
K. Mann and I have outlined such an argument for colonial Africa in our
'Introduction : law in colonial Africa', in Mann and Roberts (eds.), Law in Colonial Africa<person-group>
                     <string-name>
                        <surname>Mann</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Introduction : law in colonial Africa</comment>
                  <source>Law in Colonial Africa</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e622a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d498e629" publication-type="book">
E. Hilde, L'Organisation judiciaire en Afrique Occidentale
Française (Paris, 1912), 66-7<person-group>
                     <string-name>
                        <surname>Hilde</surname>
                     </string-name>
                  </person-group>
                  <fpage>66</fpage>
                  <source>L'Organisation judiciaire en Afrique Occidentale Française</source>
                  <year>1912</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e658a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d498e665" publication-type="other">
Com-
mandant Thiès to Lieutenant-Governor Senegal, 28 Feb. 1906, Thiès</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e674" publication-type="other">
Lieutenant-
Governor Senegal to Commandant Thiès, n.d., Saint Louis</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e683" publication-type="other">
Governor-General
Merlin to Lieutenant-Governor Senegal, 30 Nov. 1906, Dakar, ANS 2 M</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e693" publication-type="book">
R. L. Buell, The Native Problem in Africa (New York, 1928),
Vol. 1, 946-53<person-group>
                     <string-name>
                        <surname>Buell</surname>
                     </string-name>
                  </person-group>
                  <fpage>946</fpage>
                  <volume>1</volume>
                  <source>The Native Problem in Africa</source>
                  <year>1928</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e724" publication-type="book">
G. W. Johnson, The Emergence of Black Politics in Senegal : the Struggle
for Power in the Four Communes, 1900-1920 (Stanford, 1971)<person-group>
                     <string-name>
                        <surname>Johnson</surname>
                     </string-name>
                  </person-group>
                  <source>The Emergence of Black Politics in Senegal : the Struggle for Power in the Four Communes, 1900-1920</source>
                  <year>1971</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e749a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d498e756" publication-type="book">
R. Rémond, L'Anticlericalisme en France, de 1815 à nos jours
(Paris, 1985)<person-group>
                     <string-name>
                        <surname>Rémond</surname>
                     </string-name>
                  </person-group>
                  <source>L'Anticlericalisme en France, de 1815 à nos jours</source>
                  <year>1985</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e782a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d498e789" publication-type="book">
D. Sarr with R. Roberts, 'The jurisdiction of Muslim tribunals in colonial Senegal',
in Mann and Roberts (eds.), Law in Colonial Africa<person-group>
                     <string-name>
                        <surname>Sarr</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The jurisdiction of Muslim tribunals in colonial Senegal</comment>
                  <source>Law in Colonial Africa</source>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e814a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d498e821" publication-type="journal">
D. Cruise O'Brien, 'Towards an " Islamic policy " in French West Africa', J. Afr.
Hist., VIII (1967), 303-I6<object-id pub-id-type="jstor">10.2307/179485</object-id>
                  <fpage>303</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e836" publication-type="journal">
D. Robinson, 'French "Islamic" policy and practice in late
nineteenth-century Senegal', J. Afr. Hist., XXIX (1988), 415-35<object-id pub-id-type="jstor">10.2307/182350</object-id>
                  <fpage>415</fpage>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e851" publication-type="other">
Sarr, 'The jurisdiction
of Muslim tribunals'</mixed-citation>
            </p>
         </fn>
         <fn id="d498e861a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d498e868" publication-type="other">
K. Opoku, 'Traditional law under French colonial rule', Verfassung und Recht in
Übersee, 1974, 139-53</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e877" publication-type="other">
Buell, Native Problem, 946-7</mixed-citation>
            </p>
         </fn>
         <fn id="d498e884a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d498e891" publication-type="book">
A. Quellien, La politique musulmane dans l'Afrique Occidentale Française (Paris, 1910)<person-group>
                     <string-name>
                        <surname>Quellien</surname>
                     </string-name>
                  </person-group>
                  <source>La politique musulmane dans l'Afrique Occidentale Française</source>
                  <year>1910</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e913a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d498e920" publication-type="book">
P. Moreau, Les indigènes dans l'A.O.F. : leur
condition politique et économique (Paris, 1938), 156<person-group>
                     <string-name>
                        <surname>Moreau</surname>
                     </string-name>
                  </person-group>
                  <fpage>156</fpage>
                  <source>Les indigènes dans l'A.O.F. : leur condition politique et économique</source>
                  <year>1938</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e948" publication-type="other">
Hilde, Organisation, 103</mixed-citation>
            </p>
         </fn>
         <fn id="d498e955a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d498e962" publication-type="book">
R. David and H. de Vries, The French Legal System : an Introduction to Civil Law
Systems (Dobbs Ferry, 1958), 15, 34<person-group>
                     <string-name>
                        <surname>David</surname>
                     </string-name>
                  </person-group>
                  <fpage>15</fpage>
                  <source>The French Legal System : an Introduction to Civil Law Systems</source>
                  <year>1958</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e990" publication-type="book">
J. H. Merryman, The Civil Law Tradition :
an Introduction to the Legal Systems of Western Europe and Latin America (Stanford,
1969)<person-group>
                     <string-name>
                        <surname>Merryman</surname>
                     </string-name>
                  </person-group>
                  <source>The Civil Law Tradition : an Introduction to the Legal Systems of Western Europe and Latin America</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1020a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d498e1027" publication-type="book">
G. Stumberg, Guide to Law and Legal Literature of France (Washington, D.C.,
1931), 149<person-group>
                     <string-name>
                        <surname>Stumberg</surname>
                     </string-name>
                  </person-group>
                  <fpage>149</fpage>
                  <source>Guide to Law and Legal Literature of France</source>
                  <year>1931</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1056a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d498e1063" publication-type="other">
Stumberg, Guide, 148-9</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1070a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d498e1077" publication-type="book">
L. Crémieu, Précis
théorique et pratique de procédure civile (Paris, 1924)<person-group>
                     <string-name>
                        <surname>Crémieu</surname>
                     </string-name>
                  </person-group>
                  <source>Précis théorique et pratique de procédure civile</source>
                  <year>1924</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1101" publication-type="book">
R. Japiot, Traité élémentaire de
procédure civile et commerciale (Paris, 1929)<person-group>
                     <string-name>
                        <surname>Japiot</surname>
                     </string-name>
                  </person-group>
                  <source>Traité élémentaire de procédure civile et commerciale</source>
                  <year>1929</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1126a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d498e1133" publication-type="other">
Crémieu, Précis, 219</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1140a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d498e1147" publication-type="book">
N. Davis, Fiction in the Archives : Pardon Tales and their Tellers in Sixteenth-Century
France (Stanford, 1987), 22.<person-group>
                     <string-name>
                        <surname>Davis</surname>
                     </string-name>
                  </person-group>
                  <fpage>22</fpage>
                  <source>Fiction in the Archives : Pardon Tales and their Tellers in Sixteenth-Century France</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1176a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d498e1183" publication-type="other">
Johnson, Emergence, 113.</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1191a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d498e1198" publication-type="other">
Johnson, Emergence, 103-4</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1205a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d498e1212" publication-type="book">
M. Lowy, 'A good name is worth more than money : strategies of
court use in urban Ghana', in L. Nader and H. F. Todd, Jr. (eds.), The Disputing Process :
Law in Ten Societies (New York, 1978)<person-group>
                     <string-name>
                        <surname>Lowy</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">A good name is worth more than money : strategies of court use in urban Ghana</comment>
                  <source>The Disputing Process : Law in Ten Societies</source>
                  <year>1978</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1244a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d498e1251" publication-type="other">
Seck v Wade,
letter Procurer to Commissaire de Gendarmerie, 15 Dec. 1913</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1261a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d498e1268" publication-type="other">
Davis, Fiction</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1275a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d498e1282" publication-type="book">
W. L. Bennett and M. S. Feldman, Reconstructing Reality in the Courtroom : Justice
and Judgement in American Culture (New Brunswick, 1981), 3, 94<person-group>
                     <string-name>
                        <surname>Bennett</surname>
                     </string-name>
                  </person-group>
                  <fpage>3</fpage>
                  <source>Reconstructing Reality in the Courtroom : Justice and Judgement in American Culture</source>
                  <year>1981</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1311a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d498e1318" publication-type="other">
Bennett and Feldman, Reconstructing Reality, 6-7</mixed-citation>
            </p>
         </fn>
         <fn id="d498e1326a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d498e1333" publication-type="other">
Bennett and Feldman, Reconstructing Reality, 6-7</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1339" publication-type="book">
Shula Marks, Ambiguities of Dependence in South Africa : Class, Nationalism, and the State
in Natal (Baltimore, 1986)<person-group>
                     <string-name>
                        <surname>Marks</surname>
                     </string-name>
                  </person-group>
                  <source>Ambiguities of Dependence in South Africa : Class, Nationalism, and the State in Natal</source>
                  <year>1986</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1364a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d498e1371" publication-type="journal">
R. Roberts, 'Women's work and women's property: household social relations
in the Maraka textile industry of the nineteenth century', Comp. St. Society and Hist.,
XXVI (1984), 229-50<person-group>
                     <string-name>
                        <surname>Roberts</surname>
                     </string-name>
                  </person-group>
                  <fpage>229</fpage>
                  <volume>XXVI</volume>
                  <source>Comp. St. Society and Hist.</source>
                  <year>1984</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1405" publication-type="other">
C. Pitt, 'An economic and social history of the
Senegambia textile industry' (unpublished Ph.D. thesis, University of Chicago, 1978),
12, 125-7</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1417" publication-type="journal">
M. Joseph, 'West African indigo cloth', African Arts, II (1978), 34-7<object-id pub-id-type="doi">10.2307/3335446</object-id>
                  <fpage>34</fpage>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1430a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d498e1437" publication-type="book">
P. Curtin, Economic Change in Precolonial Africa : Senegambia in the Era of the
Slave Trade (Madison, 1975)<person-group>
                     <string-name>
                        <surname>Curtin</surname>
                     </string-name>
                  </person-group>
                  <source>Economic Change in Precolonial Africa : Senegambia in the Era of the Slave Trade</source>
                  <year>1975</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1461" publication-type="book">
A. G. Hopkins, An Economic History of West Africa
(London, 1973)<person-group>
                     <string-name>
                        <surname>Hopkins</surname>
                     </string-name>
                  </person-group>
                  <source>An Economic History of West Africa</source>
                  <year>1973</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1485" publication-type="book">
C. W. Newbury, 'Trade and authority in West Africa', in L. H. Gann
and P. Duignan (eds.), Colonialism in Africa, 1870-1960, vol. 1 (Cambridge, 1969)<person-group>
                     <string-name>
                        <surname>Newbury</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Trade and authority in West Africa</comment>
                  <volume>1</volume>
                  <source>Colonialism in Africa, 1870-1960</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1517a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d498e1524" publication-type="book">
A. Christelow, 'Theft, homicide, and oath in early twentieth century Kano,
Nigeria', in Mann and Roberts (eds.), Law in Colonial Africa<person-group>
                     <string-name>
                        <surname>Christelow</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Theft, homicide, and oath in early twentieth century Kano, Nigeria</comment>
                  <source>Law in Colonial Africa</source>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1548" publication-type="journal">
J. N. D. Anderson,
'Muslim procedure and evidence', parts I and II, J. Afr. Administration, I (1949),
123-9<person-group>
                     <string-name>
                        <surname>Anderson</surname>
                     </string-name>
                  </person-group>
                  <issue>I</issue>
                  <fpage>123</fpage>
                  <volume>I</volume>
                  <source>J. Afr. Administration</source>
                  <year>1949</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1585" publication-type="other">
1 (1949), 176-83</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1592" publication-type="book">
Lawrence Rosen, The Anthropology of Justice : Law as Culture
in Islamic Society (Cambridge, 1989)<person-group>
                     <string-name>
                        <surname>Rosen</surname>
                     </string-name>
                  </person-group>
                  <source>The Anthropology of Justice : Law as Culture in Islamic Society</source>
                  <year>1989</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1617a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d498e1624" publication-type="book">
T. Q. Reefe, 'The biggest game of all : gambling in traditional Africa', in W. J.
Baker and J. A. Mangan (eds.), Sport in Africa : Essays in Social History (New York,
1987)<person-group>
                     <string-name>
                        <surname>Reefe</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">The biggest game of all : gambling in traditional Africa</comment>
                  <source>Sport in Africa : Essays in Social History</source>
                  <year>1987</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1656a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d498e1663" publication-type="book">
M. Gluckman, 'Introduction', in A. L. Epstein (ed.), The Craft of Social Anthro-
pology (London, 1967), xvi<person-group>
                     <string-name>
                        <surname>Gluckman</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">Introduction</comment>
                  <fpage>xvi</fpage>
                  <source>The Craft of Social Anthropology</source>
                  <year>1967</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1696a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d498e1703" publication-type="book">
L. Fallers, Law without Precedent : Legal Ideas in Action in the Courts of Colonial
Busoga (Chicago, 1969), 34<person-group>
                     <string-name>
                        <surname>Fallers</surname>
                     </string-name>
                  </person-group>
                  <fpage>34</fpage>
                  <source>Law without Precedent : Legal Ideas in Action in the Courts of Colonial Busoga</source>
                  <year>1969</year>
               </mixed-citation>
            </p>
            <p>
               <mixed-citation id="d498e1731" publication-type="book">
J. Clifford makes the same point when he argues that the
courtroom encounter is an 'odd refracting and enlarging lens', in The Predicament of
Culture : Twentieth Century Ethnography, Literature, and Art (Cambridge, MA, 1988),
328-9<person-group>
                     <string-name>
                        <surname>Clifford</surname>
                     </string-name>
                  </person-group>
                  <comment content-type="section">odd refracting and enlarging lens</comment>
                  <fpage>328</fpage>
                  <source>The Predicament of Culture : Twentieth Century Ethnography, Literature, and Art</source>
                  <year>1988</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1769a1310">
            <label>42</label>
            <p>
               <mixed-citation id="d498e1776" publication-type="journal">
R. J. Coombe, '"Same as it ever was":
rethinking the politics of legal interpretation', McGill Law Journal/Revue de droit de
McGill, XXXIV (1989), 603-52<person-group>
                     <string-name>
                        <surname>Coombe</surname>
                     </string-name>
                  </person-group>
                  <fpage>603</fpage>
                  <volume>XXXIV</volume>
                  <source>McGill Law Journal/Revue de droit de McGill</source>
                  <year>1989</year>
               </mixed-citation>
            </p>
         </fn>
         <fn id="d498e1811a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d498e1818" publication-type="other">
D. W. Cohen, 'Comments : sources and methods for the study of law in colonial
Africa', unpublished paper presented at the annual meeting of the African Studies
Association, Atlanta, 1989</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
