<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">econbota</journal-id>
         <journal-id journal-id-type="jstor">j101315</journal-id>
         <journal-title-group>
            <journal-title>Economic Botany</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The New York Botanical Garden</publisher-name>
         </publisher>
         <issn pub-type="ppub">00130001</issn>
         <issn pub-type="epub">18749364</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4254502</article-id>
         <title-group>
            <article-title>Domestication of Sawa Millet (Echinochloa colona)</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>J. M. J.</given-names>
                  <surname>de Wet</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>K. E. Prasada Rao</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>M. H.</given-names>
                  <surname>Mengesha</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>D. E.</given-names>
                  <surname>Brink</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>1983</year>
         
            <day>1</day>
            <month>9</month>
            <year>1983</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">37</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i389430</issue-id>
         <fpage>283</fpage>
         <lpage>291</lpage>
         <page-range>283-291</page-range>
         <permissions>
            <copyright-statement>Copyright 1983 The New York Botanical Garden</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4254502"/>
         <abstract>
            <p>Two species of Echinochloa are grown as cereals. Echinochloa crusgalli is native to temperate Eurasia and was domesticated in Japan some 4,000 yr ago. Echinochloa colona is widely distributed in the tropics and subtropics of the Old World. It was domesticated in India. Echinochloa colona is morphologically allied to E. crusgalli, but hybrids between them are sterile. Echinochloa colona differs consistently from E. crusgalli in having smaller spikelets with membranaceous rather than chartaceous glumes. Hybrids between wild and cultivated taxa of E. colona and between those of E. crusgalli are fertile. Cultivated E. colona is variable. It is grown as a cereal across India, Kashmir and Sikkim. Four morphological races are recognized, although these do not have geographical, ecological or ethnological unity. Race laxa is confined to Sikkim where races robusta, intermedia and stolonifera are also grown. In India, races robusta, intermedia and stolonifera are often grown as mixtures, and Echinochloa is sometimes grown as a mixture with other cereals, particularly Setaria italica (foxtail millet) or Eleusine coracana (finger millet). The species is planted on poor soil, and some cultivars mature in less than 2 mo. They hold considerable promise as cereals for the semiarid tropics.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d540e235a1310">
            <mixed-citation id="d540e239" publication-type="book">
Clayton, W. D. 1972. Gramineae. In Flora of West Tropical Africa, F. N. Hepper, ed. 3: 349-574.<person-group>
                  <string-name>
                     <surname>Clayton</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Gramineae</comment>
               <fpage>349</fpage>
               <volume>3</volume>
               <source>Flora of West Tropical Africa</source>
               <year>1972</year>
            </mixed-citation>
         </ref>
         <ref id="d540e271a1310">
            <mixed-citation id="d540e275" publication-type="journal">
de Wet, J. M. J.1977. Domestication of African cereals. African Econ. Hist. (spring) no. 3: 15-32.<object-id pub-id-type="doi">10.2307/3601137</object-id>
               <fpage>15</fpage>
            </mixed-citation>
         </ref>
         <ref id="d540e288a1310">
            <mixed-citation id="d540e292" publication-type="book">
Dixon, D. M.1969. A note on cereals in ancient Egypt. In The Domestication and Exploitation of
Plants and Animals, J. P. Ucko and C. W. Dimbley, ed. Aldine, Chicago.<person-group>
                  <string-name>
                     <surname>Dixon</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">A note on cereals in ancient Egypt</comment>
               <source>The Domestication and Exploitation of Plants and Animals</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d540e321a1310">
            <mixed-citation id="d540e325" publication-type="book">
Fischer, C. E. C. 1934. Gramineae. In Flora of the Presidency of Madras, J. S. Gamble, ed. 10:
1690-1864.<person-group>
                  <string-name>
                     <surname>Fischer</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Gramineae</comment>
               <fpage>1690</fpage>
               <volume>10</volume>
               <source>Flora of the Presidency of Madras</source>
               <year>1934</year>
            </mixed-citation>
         </ref>
         <ref id="d540e361a1310">
            <mixed-citation id="d540e365" publication-type="other">
Hitchcock, A. S. 1950. Manual of the Grasses of the United States. 2nd ed, revised by Agnes
Chase. USDA Misc. Publ. 200.</mixed-citation>
         </ref>
         <ref id="d540e375a1310">
            <mixed-citation id="d540e379" publication-type="journal">
Hjelmqvist, H.1969. Dinkel und Hirse aus der Bronzezeit Sudschwedens nebst einigen Bemerkun-
gen uber ihre spatere Geschichte in Schweden. Bot. Not.122: 260-270.<person-group>
                  <string-name>
                     <surname>Hjelmqvist</surname>
                  </string-name>
               </person-group>
               <fpage>260</fpage>
               <volume>122</volume>
               <source>Bot. Not.</source>
               <year>1969</year>
            </mixed-citation>
         </ref>
         <ref id="d540e411a1310">
            <mixed-citation id="d540e415" publication-type="journal">
Kajale, M. D.1977. Ancient grains from excavations at Nevasa, Maharashtra. Geophytology7:
98-106.<person-group>
                  <string-name>
                     <surname>Kajale</surname>
                  </string-name>
               </person-group>
               <fpage>98</fpage>
               <volume>7</volume>
               <source>Geophytology</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d540e447a1310">
            <mixed-citation id="d540e451" publication-type="book">
Nie, N. H., C. H. Hull, J. G. Jenkins, K. Steinbrenner, and D. H. Bent. 1975. Statistical Package
for the Social Sciences. 2nd ed. McGraw-Hill, New York.<person-group>
                  <string-name>
                     <surname>Nie</surname>
                  </string-name>
               </person-group>
               <edition>2</edition>
               <source>Statistical Package for the Social Sciences</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d540e480a1310">
            <mixed-citation id="d540e484" publication-type="book">
Rozhevits, R. Yu. 1934. Grasses-Gramineae Juss. In Flora of the U.S.S.R. Vol. 2, V. L. Komorov,
ed, p. 1-622. Translated from Russian (1963). Israel Program for Scientific Translations, Je-
rusalem.<person-group>
                  <string-name>
                     <surname>Rozhevits</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Grasses-Gramineae Juss</comment>
               <fpage>1</fpage>
               <volume>2</volume>
               <source>Flora of the U.S.S.R.</source>
               <year>1963</year>
            </mixed-citation>
         </ref>
         <ref id="d540e522a1310">
            <mixed-citation id="d540e526" publication-type="book">
1937.Grasses. An Introduction to the Study of Fodder and Cereal Grasses. Translated
from Russian (1980). Indian National Scientific Documentation Centre, New Delhi.<person-group>
                  <string-name>
                     <surname>Rozhevits</surname>
                  </string-name>
               </person-group>
               <source>Grasses. An Introduction to the Study of Fodder and Cereal Grasses</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d540e552a1310">
            <mixed-citation id="d540e556" publication-type="book">
Stuhlmann, F.1909. Beitrage zur Kulturgeschichte von Ostafrica. D. Reimer (E. Voshen), Berlin.<person-group>
                  <string-name>
                     <surname>Stuhlmann</surname>
                  </string-name>
               </person-group>
               <source>Beitrage zur Kulturgeschichte von Ostafrica</source>
               <year>1909</year>
            </mixed-citation>
         </ref>
         <ref id="d540e578a1310">
            <mixed-citation id="d540e582" publication-type="journal">
Tisserant, R. P. Ch. 1953. L'agriculture dans les savanes de l'Oubangui. Bull. Inst. Etudes Centrafr.
5: 209-274.<person-group>
                  <string-name>
                     <surname>Tisserant</surname>
                  </string-name>
               </person-group>
               <fpage>209</fpage>
               <volume>5</volume>
               <source>Bull. Inst. Etudes Centrafr.</source>
               <year>1953</year>
            </mixed-citation>
         </ref>
         <ref id="d540e614a1310">
            <mixed-citation id="d540e618" publication-type="book">
Vickery, J. W. 1975. Gramineae. In Flora of New South Wales, M. D. Tindale, ed, New South
Wales Dept. Agric. Publ. 19: 125-306.<person-group>
                  <string-name>
                     <surname>Vickery</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Gramineae</comment>
               <fpage>125</fpage>
               <volume>19</volume>
               <source>Flora of New South Wales</source>
               <year>1975</year>
            </mixed-citation>
         </ref>
         <ref id="d540e653a1310">
            <mixed-citation id="d540e657" publication-type="book">
Vishnu-Mittre. 1977. Changing economy in ancient India. In Origins of Agriculture, C. A. Reed,
ed. Mouton, The Hague.<person-group>
                  <string-name>
                     <surname>Vishnu-Mittre</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Changing economy in ancient India</comment>
               <source>Origins of Agriculture</source>
               <year>1977</year>
            </mixed-citation>
         </ref>
         <ref id="d540e686a1310">
            <mixed-citation id="d540e690" publication-type="journal">
Watanabe, N.1970. A spodographic analysis of millet from prehistoric Japan. Fac. Sci. Univ.
Tokyo, Sec. 5, 3: 357-379.<person-group>
                  <string-name>
                     <surname>Watanabe</surname>
                  </string-name>
               </person-group>
               <fpage>357</fpage>
               <volume>3</volume>
               <source>Fac. Sci. Univ. Tokyo, Sec. 5</source>
               <year>1970</year>
            </mixed-citation>
         </ref>
         <ref id="d540e722a1310">
            <mixed-citation id="d540e726" publication-type="journal">
Wiegand, K. M.1921. The genus Echinochloa in North America. Rhodora23: 49-65.<person-group>
                  <string-name>
                     <surname>Wiegand</surname>
                  </string-name>
               </person-group>
               <fpage>49</fpage>
               <volume>23</volume>
               <source>Rhodora</source>
               <year>1921</year>
            </mixed-citation>
         </ref>
         <ref id="d540e756a1310">
            <mixed-citation id="d540e760" publication-type="journal">
Yabuno, T.1962. Cytotaxonomic studies on the two cultivated species and the wild relatives in the
genus Echinochloa. Cytologia27: 296-305.<person-group>
                  <string-name>
                     <surname>Yabuno</surname>
                  </string-name>
               </person-group>
               <fpage>296</fpage>
               <volume>27</volume>
               <source>Cytologia</source>
               <year>1962</year>
            </mixed-citation>
         </ref>
         <ref id="d540e792a1310">
            <mixed-citation id="d540e796" publication-type="journal">
1966. Biosystematic study of the genus Echinochloa (Gramineae). Jap. J. Bot.19: 277-323.<person-group>
                  <string-name>
                     <surname>Yabuno</surname>
                  </string-name>
               </person-group>
               <fpage>277</fpage>
               <volume>19</volume>
               <source>Jap. J. Bot.</source>
               <year>1966</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
