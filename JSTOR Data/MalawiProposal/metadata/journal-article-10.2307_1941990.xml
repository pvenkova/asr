<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">ecolappl</journal-id>
         <journal-id journal-id-type="jstor">j100864</journal-id>
         <journal-title-group>
            <journal-title>Ecological Applications</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Ecological Society of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">10510761</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/1941990</article-id>
         <title-group>
            <article-title>Associational Susceptibility: Effects of Cropping Pattern and Fertilizer on Malawian Bean Fly Levels</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Deborah K.</given-names>
                  <surname>Letourneau</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>1995</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id>i306431</issue-id>
         <fpage>823</fpage>
         <lpage>829</lpage>
         <page-range>823-829</page-range>
         <permissions>
            <copyright-statement>Copyright 1995 The Ecological Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/1941990"/>
         <abstract>
            <p>Subsistence farmers with little access to costly pesticides must rely on cultural practices to enhance biological control, reduce crop apparency or suitability to pests, or increase crop tolerance to pests. Farmers in Central Malawi traditionally plant beans (Phaseolus vulgaris L.) with the first rains and apply fertilizer to bean only when it is grown in dicultures with maize (Zea mays L.). I used a factorial experiment with a key pest of beans to assess the relative effects of fertilizer (predicted to increase pest levels) and intercropping (predicted to decrease pest densities). Stem-boring bean fly (Diptera: Agromyzidae) densities and their parasitism rates were compared with respect to cropping pattern, fertilizer application, sowing date, and Vaccinium diversity of beans. Together these factors address the effects of current practices (intercropping, mixed varieties) on bean fly infestation rates and those now encouraged for increased production (relay cropping, fertilized monoculture). In addition, associational resistance in this system is tested in the context of actual farming practices. In contrast to many other studies of specialist herbivores, neither the densities of bean flies [Ophiomyia phaseoli (Tryon) and O. spencerella (Greathead)] nor their rates of parasitism were changed significantly by diversifying the field with non-host plants (bean-maize dicultures). However, the two fly species were differentially susceptible to plant quality and parasitism, with fertilized bean plants harboring more bean fly puparia than did unfertilized plants regardless of cropping pattern. Thus, the indirect effect of fertilizing intercrops resulted in elevated pest densities. Differential plant quality has not been included in most studies of intercropping and pest incidence, yet reflects actual farming practices. Bean fly densities tended to increase in plants sown later in the wet season, suggesting that relay cropping of beans will exacerbate bean fly damage. In this study, greater yields were achieved in mixed variety bean treatments, but standard fertilizer applications did not contribute to increased bean production.</p>
         </abstract>
         <kwd-group>
            <kwd>African farming</kwd>
            <kwd>associational resistance</kwd>
            <kwd>host plant quality</kwd>
            <kwd>intercropping</kwd>
            <kwd>natural enemies</kwd>
            <kwd>resource concentration</kwd>
            <kwd>subsistence agriculture</kwd>
            <kwd>varietal mixtures</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
</article>
