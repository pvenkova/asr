<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7ztpv</book-id>
      <subj-group>
         <subject content-type="call-number">NDI460.A39P42 2008</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agriculture in art</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agriculture and the arts</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Art &amp; Art History</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Cultivated Landscape</book-title>
         <subtitle>An Exploration of Art and Agriculture</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>PEARSON</surname>
               <given-names>CARIG</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>NASBY</surname>
               <given-names>JUDITH</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>11</day>
         <month>08</month>
         <year>2008</year>
      </pub-date>
      <isbn content-type="ppub">9780773532465</isbn>
      <isbn content-type="epub">9780773574908</isbn>
      <publisher>
         <publisher-name>MQUP</publisher-name>
         <publisher-loc>Montreal; Kingston; London; Ithaca</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2008</copyright-year>
         <copyright-holder>McGill-Queen’s University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt7ztpv"/>
      <abstract abstract-type="short">
         <p>Craig Pearson and Judith Nasby discuss the evolution of how we think about agriculture, its use of the land and impact on landscape, and how landscape has been portrayed historically in art. They also offer a wider discussion on the role that science and economics have played in agricultural development and the parallels to changes in art form.</p>
      </abstract>
      <counts>
         <page-count count="304"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.3</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.4</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>CUSTODIANS OF THE LANDSCAPE IN EUROPE</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>On Lake Como, in northern Italy, summer days start clear: the water is blue, the houses crisp orange and white against the grass and pines on the steep hillsides. By mid-morning the haze rolls in, obscuring the crispness. At Cernobbio, Villa d’Este stands beside the lake, its rendered walls ornamented in ochre, orange, grey, and cream hues. This sixteenth-century villa (I568 ) is a fitting scene for the beginning of our story: landscapes and land-use in Europe before the Agricultural Revolution. Formal lines of cypresses cut across handmade terraces that would once have supported vines, herbs, and vegetables. The main</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>NEW WORLD EXPANSION</title>
                  </title-group>
                  <fpage>30</fpage>
                  <abstract>
                     <p>While grand manors and enclosed fields remained the centrepieces of agricultural and other planned landscapes in Europe, new landscapes were being fashioned in tandem with European migration to the northern and southern hemispheres. Western agriculture’s age of expansion began in about 1600, gaining momentum over the next few hundred years with the relentless clearing of land and increasing mechanization (Fig. 13 ), and peaked in the second half of the twentieth century – from about 1950 to 1970 in the United States and Canada, between 1975 and 1985 in Australia, Argentina and New Zealand, and in the 1990s in Brazil. In</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>PRODUCTION AND PRODUCTIVITY</title>
                  </title-group>
                  <fpage>69</fpage>
                  <abstract>
                     <p>From the mid-1800s onward, the agricultural paradigms of production and productivity began to overlap. In Europe, the widespread use of enclosures together with changed inputs to agriculture (particularly the payment of labour) created conditions in which innovation could flourish. Similarly, in the western New Worlds, land settlement and the rapid expansion of agriculture encouraged entrepreneurship and innovation. Severe winters (in the north) or summers (in the south) together with naive and inappropriate farming techniques were problematic. For instance, farming practices suitable to Europe, which has a fairly high rainfall, were applied in the more seasonal and drought-prone climates of Australia,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>PRODUCTIVITY TO EXCESS</title>
                  </title-group>
                  <fpage>102</fpage>
                  <abstract>
                     <p>Otis Dozier’s lithograph<italic>Grasshopper and Farmer</italic>(Fig. 45) is a powerful comment on the devastating effect of the Depression and the Dust Bowl years on agriculture before World War II. After the war, the paradigm of productivity – the dominant way of thinking among farmers, agricultural scientists and governments making land policies – gained momentum, encouraged by a belief that farm profitability could be improved by producing more food, more efficiently; by concerns about food security (the ability to produce enough food within national borders to avoid dependence on imports); and by urban consumers’ delight in shopping for evercheaper food in newfangled</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>SUSTAINABILITY</title>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>Nowadays, more people are fed, more safely, than at any other time in history. To achieve this, some agricultural lands, such as the rice paddies of Java, have been cropped continuously for more than a thousand years, without loss. But some agriculturally based civilizations have blown away with the wind. The Biblical Garden of Eden, representative of the partly irrigated “fertile crescent” of the Euphrates, blossomed and then returned to semi-desert a millennium ago. We repeat history. Moving land-clearing and agriculture into unsuited areas of Australia, where the geology reflects ancient sediments laid down by salty seas, has caused dry-land</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>MILLENNIUM ACCOUNTING</title>
                  </title-group>
                  <fpage>165</fpage>
                  <abstract>
                     <p>After World War II, when the weary world was fearful about national food security, the image of the “good farmer” persisted, as it had throughout history. By the start of the twenty-first century, however, this image had been replaced with that of the “poor farmer,” and even the “bad farmer.” To examine the meaning of these judgments is to demonstrate that an accounting of agricultural practice, even at a single point in time, is a daunting task that necessarily depends on personal perspective. But it may still be useful to attempt such an accounting. It is all too easy nowadays</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>CONNECTING THE FUTURE</title>
                  </title-group>
                  <fpage>195</fpage>
                  <abstract>
                     <p>Where is agriculture heading? We suggest that its future lies in connectivity, and that connectivity may become the next paradigm, the next dominant way of thinking in many sectors of society, so that it lies alongside and eventually takes over from the paradigms of productivity and sustainability. Let us begin this discussion with three images, not of the future, but from today. They are from a college town in rural Ontario, but might as easily be in Toronto, New York, or London.</p>
                     <p>Image one: a glossy supermarket, isolated in a carpet of shiny cars and suvs. Depending on the area</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.12</book-part-id>
                  <title-group>
                     <title>APPENDICES</title>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.13</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>241</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.14</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.15</book-part-id>
                  <title-group>
                     <title>INFORMATION ON ILLUSTRATIONS</title>
                  </title-group>
                  <fpage>273</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.16</book-part-id>
                  <title-group>
                     <title>ABOUT THE AUTHORS</title>
                  </title-group>
                  <fpage>289</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7ztpv.17</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>291</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
