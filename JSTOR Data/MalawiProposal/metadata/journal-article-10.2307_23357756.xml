<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amerpoliscierevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100077</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The American Political Science Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00030554</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15375943</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23357756</article-id>
         <title-group>
            <article-title>Ethnic Quotas and Political Mobilization: Caste, Parties, and Distribution in Indian Village Councils</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>THAD</given-names>
                  <surname>DUNNING</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>JANHAVI</given-names>
                  <surname>NILEKANI</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">107</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23357717</issue-id>
         <fpage>35</fpage>
         <lpage>56</lpage>
         <permissions>
            <copyright-statement>© American Political Science Association 2013</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1017/S0003055412000573"
                   xlink:title="an external site"/>
         <abstract>
            <p>Ethnic quotas are often expected to induce distribution of material benefits to members of disadvantaged groups. Yet, the presence of an ethnic quota does not imply that political mobilization takes place along ethnic lines: Cross-cutting affiliations within multi-ethnic party organizations may lessen the tendency of politicians to target benefits to particular ethnic groups. In this article, we evaluate the impact of quotas for the presidencies of village councils in India, a subject of considerable recent research. Drawing on fine-grained information from surveys of voters, council members, presidents, and bureaucrats and using a natural experiment to isolate the effects of quotas in the states of Karnataka, Rajasthan, and Bihar, we find weak distributive effects of quotas for marginalized castes and tribes, but suggestive evidence of the importance of partisanship. We then use survey experiments to compare the influence of party and caste on voting preferences and expectations of benefit receipt. Our results suggest that especially when politicians have dynamic political incentives to allocate benefits along party lines, cross-cutting partisan ties can blunt the distributive impact of ethnic quotas.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d407e210a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d407e217" publication-type="other">
Chandra and Wilkinson (2008, 517)</mixed-citation>
            </p>
         </fn>
         <fn id="d407e224a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d407e231" publication-type="other">
Chauchard (2010)</mixed-citation>
            </p>
         </fn>
         <fn id="d407e238a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d407e245" publication-type="other">
Kumar 2002,
26</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d407e254" publication-type="other">
Nugent 2011,58</mixed-citation>
            </p>
         </fn>
         <fn id="d407e261a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d407e268" publication-type="other">
http://nrega.nic.in/netnrega/home.aspx</mixed-citation>
            </p>
         </fn>
         <fn id="d407e276a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d407e283" publication-type="other">
Nilekani 2010</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d407e289" publication-type="other">
note 27</mixed-citation>
            </p>
         </fn>
         <fn id="d407e296a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d407e303" publication-type="other">
Shastri 2009</mixed-citation>
            </p>
         </fn>
         <fn id="d407e310a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d407e317" publication-type="other">
http://www.thaddunning.
com/data/india/replication</mixed-citation>
            </p>
         </fn>
         <fn id="d407e327a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d407e334" publication-type="other">
Dunning (2009)</mixed-citation>
            </p>
         </fn>
         <fn id="d407e341a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d407e348" publication-type="other">
Hartman and Hidalgo 2011</mixed-citation>
            </p>
         </fn>
         <fn id="d407e355a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d407e362" publication-type="other">
Benjamini
and Hochberg 1995</mixed-citation>
            </p>
         </fn>
         <fn id="d407e373a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d407e380" publication-type="other">
Gerber and
Malhotra 2008</mixed-citation>
            </p>
         </fn>
         <fn id="d407e390a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d407e397" publication-type="other">
Chattopadhyay and Duflo (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d407e404a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d407e413" publication-type="other">
Mitra (1998,115)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d407e419" publication-type="other">
Yadav 2011</mixed-citation>
            </p>
         </fn>
         <fn id="d407e426a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d407e433" publication-type="other">
Cox 2007</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d407e439" publication-type="other">
Dixit
and Londregan 1996</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d407e448" publication-type="other">
Nichter 2008</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d407e455" publication-type="other">
Stokes et al., 2013</mixed-citation>
            </p>
         </fn>
         <fn id="d407e462a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d407e469" publication-type="other">
Shastri 2009</mixed-citation>
            </p>
         </fn>
         <fn id="d407e476a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d407e483" publication-type="other">
Thachil (2011)</mixed-citation>
            </p>
         </fn>
         <fn id="d407e491a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d407e498" publication-type="other">
Brass (1965)</mixed-citation>
            </p>
         </fn>
         <fn id="d407e505a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d407e512" publication-type="other">
Wilkinson 2003</mixed-citation>
            </p>
         </fn>
         <fn id="d407e519a1310">
            <label>62</label>
            <p>
               <mixed-citation id="d407e526" publication-type="other">
Green and Gerber
2012, chapter 3</mixed-citation>
            </p>
         </fn>
         <fn id="d407e536a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d407e543" publication-type="other">
Jensenius
(2012)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d407e562a1310">
            <mixed-citation id="d407e566" publication-type="other">
Alesina, Alberto. 1988. "Credibility and Policy Convergence in a
Two-party System with Rational Voters." American Economic
Review 78: 796–805.</mixed-citation>
         </ref>
         <ref id="d407e579a1310">
            <mixed-citation id="d407e583" publication-type="other">
Ban, Radu, and Vijayendra Rao. 2008. "Tokenism versus Agency:
The Impact of Women's Reservations on Village Democracies in
South India." Economic Development and Cultural Change 56 (3):
501–30.</mixed-citation>
         </ref>
         <ref id="d407e599a1310">
            <mixed-citation id="d407e603" publication-type="other">
Bardhan, Pranab, Dilip Mookherjee, and Monica Parra Torrado.
2005. "Impact of Reservations of Panchayat Pradhans on Targeting
in West Bengal." Duke University. BREAD Working Paper.</mixed-citation>
         </ref>
         <ref id="d407e616a1310">
            <mixed-citation id="d407e620" publication-type="other">
Bardhan, Pranab, Dilip Mookherjee, and Monica Parra Torrado.
2010. "Impact of Political Reservations in West Bengal Local Gov-
ernments on Anti-poverty Targeting." Journal of Globalization
and Development 1 (1).</mixed-citation>
         </ref>
         <ref id="d407e637a1310">
            <mixed-citation id="d407e641" publication-type="other">
Bates, Robert. 1983. "Modernization, Ethnic Competition and
the Rationality of Politics in Contemporary Africa." In Donald
Rothchild and Victor Olorunsola, State Versus Ethnic Claims:
African Policy Dilemmas, Boulder, CO: Westview Press.</mixed-citation>
         </ref>
         <ref id="d407e657a1310">
            <mixed-citation id="d407e661" publication-type="other">
Beaman, Lori, Raghabendra Chattopadhyay, Esther Duflo, Rohini
Pande, and Petia Topalova. 2008. "Powerful Women: Does Expo-
sure Reduce Prejudice?" Weatherhead Center for International
Affairs, Harvard University. WCFIA Working Paper.</mixed-citation>
         </ref>
         <ref id="d407e677a1310">
            <mixed-citation id="d407e681" publication-type="other">
Benjamini, Yoav, and Yosef Hochberg. 1995. "The Control of the
False Discovery Rate: A Practical and Powerful Approach to
Multiple Testing." Journal of the Royal Statistical Society, Series
B 57: 289–300.</mixed-citation>
         </ref>
         <ref id="d407e697a1310">
            <mixed-citation id="d407e701" publication-type="other">
Besley, Timothy, Rohini Pande, Lupin Rahman, and Vijayendra
Rao. 2004. "Tlie Politics of Public Good Provision: Evidence from
Indian Local Governments." Journal of the European Economic
Association 2 (2/3): 416–26.</mixed-citation>
         </ref>
         <ref id="d407e717a1310">
            <mixed-citation id="d407e721" publication-type="other">
Besley, Timothy, Rohini Pande, and Vijayendra Rao. 2008. "The
Political Economy of Gram Panchayats in South India." In
Development in Karnataka: Challenges of Governance, Equity,
and Empowerment, eds. Gopal K. Kadekodi, Ravi Kanbur, and
Vijayendra Rao. New Delhi: Academic Foundation, pp. 243–
64.</mixed-citation>
         </ref>
         <ref id="d407e744a1310">
            <mixed-citation id="d407e748" publication-type="other">
Bhavnani, Rikhil. 2009. "Do Electoral Quotas Work after They
Are Withdrawn? Evidence from a Natural Experiment in India."
American Political Science Review 103 (1): 23–35.</mixed-citation>
         </ref>
         <ref id="d407e762a1310">
            <mixed-citation id="d407e766" publication-type="other">
Brass, Paul. 1965. Factional Politics in an Indian State: The Congress
Party in Uttar Pradesh. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d407e776a1310">
            <mixed-citation id="d407e780" publication-type="other">
Brass, Paul. 1984. Faction and Party. Vol. 1 of Caste, Faction, and
Party in Indian Politics. New Delhi: Chanakya Press.</mixed-citation>
         </ref>
         <ref id="d407e790a1310">
            <mixed-citation id="d407e794" publication-type="other">
Breeding, Mary. 2008. "Rewarding Loyalists, Buying Turnout, and
Mobilizing the Poor: The Success of Mixed-Clientelist Strategies in
Bangalore Elections." Discussion paper prepared for the Political
Economy Methods Group, Georgetown University.</mixed-citation>
         </ref>
         <ref id="d407e810a1310">
            <mixed-citation id="d407e814" publication-type="other">
Chandra, Kanchan. 2004. Why Ethnic Parties Succeed: Patronage and
Ethnic Head Counts in India. New York: Cambridge University
Press.</mixed-citation>
         </ref>
         <ref id="d407e827a1310">
            <mixed-citation id="d407e831" publication-type="other">
Chandra, Kanchan. 2005. "Ethnic Parties and Democratic Stability."
Perspectives on Politics 3 (2): 235–252.</mixed-citation>
         </ref>
         <ref id="d407e841a1310">
            <mixed-citation id="d407e845" publication-type="other">
Chandra, Kanchan, and Steven Wilkinson. 2008. "Measuring the
Effect of 'Ethnicity.'" Comparative Political Studies 41 (4/5): 515–
63.</mixed-citation>
         </ref>
         <ref id="d407e859a1310">
            <mixed-citation id="d407e863" publication-type="other">
Chattopadhyay, Raghabendra, and Esther Duflo. 2004. "Women as
Policy Makers: Evidence from a Randomized Policy Experiment
in India." Econometrica 72 (5): 1409–43.</mixed-citation>
         </ref>
         <ref id="d407e876a1310">
            <mixed-citation id="d407e880" publication-type="other">
Chauchard, Simon. 2010. "Can the Experience of Political Power by
a Member of a Stigmatized Group Change the Nature of Day-to-
Day Interpersonal Relations? Evidence from Rural India." Dart-
mouth University. Unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="d407e896a1310">
            <mixed-citation id="d407e900" publication-type="other">
Chhibber, Pradeeb K. 1999. Democracy without Associations: Trans-
formation of the Party System and Social Cleavages in India. Ann
Arbor: University of Michigan.</mixed-citation>
         </ref>
         <ref id="d407e913a1310">
            <mixed-citation id="d407e917" publication-type="other">
Corbridge, Stuart, Glyn Williams, Manoj Srivastava, and René
Véron. 2005. Seeing the State: Governance and Governmentality
in India. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d407e930a1310">
            <mixed-citation id="d407e934" publication-type="other">
Cox, Gary. 2007. "Swing Voters, Core Voters, and Distributive Pol-
itics." In Political Representation, eds. Ian Shapiro, Susan Stokes,
and Elisabeth Jean Wood. New York: Cambridge University Press,
pp. 342–57.</mixed-citation>
         </ref>
         <ref id="d407e950a1310">
            <mixed-citation id="d407e954" publication-type="other">
Deaton, Angus. 2009. "Instruments of Development: Randomization
in the Tropics, and the Search for the Elusive Keys to Economic
Development." Keynes Lecture, British Academy, October 9th,
2008.</mixed-citation>
         </ref>
         <ref id="d407e971a1310">
            <mixed-citation id="d407e975" publication-type="other">
Dixit, Avinash, Gene M. Grossman, and Faruk Gul. 2000. "The Dy-
namics of Political Compromise." Journal of Political Economy
108 (3): 531–68.</mixed-citation>
         </ref>
         <ref id="d407e988a1310">
            <mixed-citation id="d407e992" publication-type="other">
Dixit, Avinash, and John Londregan. 1996. "The Determinants of
Success of Special Interests in Redistributive Politics." Journal of
Politics 58:1132–55.</mixed-citation>
         </ref>
         <ref id="d407e1005a1310">
            <mixed-citation id="d407e1009" publication-type="other">
Duflo, Esther. 2005. "Why Political Reservations?" Journal of the
European Economic Association 3.2–3.3: 668–78.</mixed-citation>
         </ref>
         <ref id="d407e1019a1310">
            <mixed-citation id="d407e1023" publication-type="other">
Dunning, Thad. 2008. "Improving Causal Inference: Strengths and
Limitations of Natural Experiments." Political Research Quarterly
61 (2): 282–93.</mixed-citation>
         </ref>
         <ref id="d407e1036a1310">
            <mixed-citation id="d407e1040" publication-type="other">
Dunning, Thad. 2009. "The Salience of Ethnic Categories: Field and
Natural Experimental Evidence from Indian Village Councils."
Yale University. Unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="d407e1053a1310">
            <mixed-citation id="d407e1057" publication-type="other">
Dunning, Thad. 2012. Natural Experiments in the Social Sciences: A
Design-based Approach. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d407e1068a1310">
            <mixed-citation id="d407e1072" publication-type="other">
Dunning, Thad, and Lauren Harrison. 2010. "Cross-cutting Cleav-
ages and Ethnic Voting: An Experimental Study of Cousinage in
Mali." American Political Science Review 104 (1): 21–39.</mixed-citation>
         </ref>
         <ref id="d407e1085a1310">
            <mixed-citation id="d407e1089" publication-type="other">
Gerber, Alan S., and Donald P. Green. 2012. Field Experiments:
Design, Analysis, and Interpretation. New York: W.W. Norton &amp;
Co.</mixed-citation>
         </ref>
         <ref id="d407e1102a1310">
            <mixed-citation id="d407e1106" publication-type="other">
Gerber, Alan S., and Neil Malhotra. 2008. "Do Statistical Report-
ing Standards Affect What Is Published? Publication Bias in Two
Leading Political Science Journals." Quarterly Journal of Political
Science 3 (3): 313–26.</mixed-citation>
         </ref>
         <ref id="d407e1122a1310">
            <mixed-citation id="d407e1126" publication-type="other">
Hartman, Erin, and F. Daniel Hidalgo. 2011. "What's the Al-
ternative?: An Equivalence Approach to Balance and Placebo
Tests." Manuscript, Departments of Political Science, MIT and UC
Berkeley.</mixed-citation>
         </ref>
         <ref id="d407e1142a1310">
            <mixed-citation id="d407e1146" publication-type="other">
Imai, Kosuke, Gary King, and Clayton Nail. 2009. "The Essential
Role of Pair-matching in Cluster-randomized Experiments, with
Application to the Mexican Universal Health Insurance Evalua-
tion." Statistical Science 24: 29–53.</mixed-citation>
         </ref>
         <ref id="d407e1162a1310">
            <mixed-citation id="d407e1166" publication-type="other">
Jaffrelot, Christophe. 2003. India's Silent Revolution: The Rise of
the Lower Castes in India. New York: Columbia University
Press.</mixed-citation>
         </ref>
         <ref id="d407e1180a1310">
            <mixed-citation id="d407e1184" publication-type="other">
Jensenius, Francesca Refsum. 2012. "Development from Represen-
tation? A Study of Quotas for Scheduled Castes in India." Uni-
versity of California, Berkeley. Unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="d407e1197a1310">
            <mixed-citation id="d407e1201" publication-type="other">
Laitin, David. 1986. Hegemony and Culture: Politics and Religious
Change among the Yoruba. Chicago: The University of Chicago
Press.</mixed-citation>
         </ref>
         <ref id="d407e1214a1310">
            <mixed-citation id="d407e1218" publication-type="other">
Mitra, Chandand. 1998. The Corrupt Society: The Criminalization of
India from Independence to the 1990s. New Delhi: Viking.</mixed-citation>
         </ref>
         <ref id="d407e1228a1310">
            <mixed-citation id="d407e1232" publication-type="other">
Nichter, Simeon. 2008. "Vote Buying or Turnout Buying? Machine
Politics and the Secret Ballot." American Political Science Review
102: 19–31.</mixed-citation>
         </ref>
         <ref id="d407e1245a1310">
            <mixed-citation id="d407e1249" publication-type="other">
Nilekani, Janhavi. 2010. "Reservation for Women in Karnataka
Gram Panchayats: The Implications of Non-random Reservation
and the Effect of Women Leaders." Senior Honors Thesis. Yale
College.</mixed-citation>
         </ref>
         <ref id="d407e1265a1310">
            <mixed-citation id="d407e1269" publication-type="other">
Nugent, Amelia. 2011. "Panchayats, Seat Reservations, and the
Women's Question in India: A Historical Trajectory." Senior Hon-
ors Thesis. Mount Holyoke College.</mixed-citation>
         </ref>
         <ref id="d407e1283a1310">
            <mixed-citation id="d407e1287" publication-type="other">
Palaniswamy, Nethra, and Nandini Krishnan. 2008. "Local Poli-
tics, Political Institutions, and Public Resource Allocation." In-
ternational Food Policy Research Institute, Discussion Paper
00834.</mixed-citation>
         </ref>
         <ref id="d407e1303a1310">
            <mixed-citation id="d407e1307" publication-type="other">
Pande, Rohini. 2003. "Can Mandated Political Representation In-
crease Policy Influence for Disadvantaged Minorities? Theory and
Evidence from India." American Economic Review 93 (4): 1132–
51.</mixed-citation>
         </ref>
         <ref id="d407e1323a1310">
            <mixed-citation id="d407e1327" publication-type="other">
Parikh, Sunita. 1997. The Politics of Preference: Democratic Institu-
tions and Affirmative Action in the United States and India. Ann
Arbor: University of Michigan.</mixed-citation>
         </ref>
         <ref id="d407e1340a1310">
            <mixed-citation id="d407e1344" publication-type="other">
Pitkin, Hannah Fenichel. 1967. The Concept of Representation.
Berkeley: University of California.</mixed-citation>
         </ref>
         <ref id="d407e1354a1310">
            <mixed-citation id="d407e1358" publication-type="other">
Posner, Daniel N. 2004. "The Political Salience of Cultural Differ-
ence: Why Chewas and Tumbukas Are Allies in Zambia and Ad-
versaries in Malawi." American Political Science Review 98 (4):
529–545.</mixed-citation>
         </ref>
         <ref id="d407e1374a1310">
            <mixed-citation id="d407e1378" publication-type="other">
Posner, Daniel N. 2005. Institutions and Ethnic Politics in Africa.
Cambridge, UK: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d407e1389a1310">
            <mixed-citation id="d407e1395" publication-type="other">
Sekhon, Jasjeet. 2009. "Opiates for the Matches: Matching Methods
for Causal Inference." Annual Review of Political Science 12: 487–
508.</mixed-citation>
         </ref>
         <ref id="d407e1408a1310">
            <mixed-citation id="d407e1412" publication-type="other">
Selway, Joel. 2011. "The Measurement of Cross-cutting Cleavages
and Other Multidimensional Cleavage Structures." Political Anal-
ysis 19 (1): 48–65.</mixed-citation>
         </ref>
         <ref id="d407e1425a1310">
            <mixed-citation id="d407e1429" publication-type="other">
Shastri, Sandeep. 2009. "Legislators in Karnataka: Well-entrenched
Dominant Castes." In Rise of the Plebeians? The Changing Face of
Indian Legislative Assemblies, eds. Christophe Jaffrelot and Sanjay
Kumar. New Delhi: Routledge, pp. 245–76.</mixed-citation>
         </ref>
         <ref id="d407e1445a1310">
            <mixed-citation id="d407e1449" publication-type="other">
Singh, Darshan. 2009. "Development of Scheduled Castes in In-
dia: A Review." Journal of Rural Development 28 (4): 529–
42.</mixed-citation>
         </ref>
         <ref id="d407e1462a1310">
            <mixed-citation id="d407e1466" publication-type="other">
Stokes, Susan C., Thad Dunning, Marcelo Nazareno, and Valeria
Brusco. 2013. Brokers, Voters, and Clientelism. New York: Cam-
bridge University Press, forthcoming.</mixed-citation>
         </ref>
         <ref id="d407e1479a1310">
            <mixed-citation id="d407e1483" publication-type="other">
Tajfel, Henri, and John C. Turner. 1979. "The Social Identity Theory
of Inter-Group Behavior." In S. Worchel and L. W. Austin (eds.),
Psychology of Intergroup Relations. Chicago: Nelson-Hall.</mixed-citation>
         </ref>
         <ref id="d407e1497a1310">
            <mixed-citation id="d407e1501" publication-type="other">
Thachil, Tariq. 2011. "Embedded Mobilization: Social Services
as Electoral Strategy in India." World Politics 63 (3): 434–
69.</mixed-citation>
         </ref>
         <ref id="d407e1514a1310">
            <mixed-citation id="d407e1518" publication-type="other">
Thistlethwaite, Donald L., and Donald T. Campbell. 1960.
"Regression-discontinuity Analysis: An Alternative to the Ex-
post Facto Experiment." Journal of Educational Psychology
51 (6): 309–17.</mixed-citation>
         </ref>
         <ref id="d407e1534a1310">
            <mixed-citation id="d407e1538" publication-type="other">
Wilkinson, Steven I. 2003. "Social Cleavages and Electoral Compe-
tition in India." India Review 2 (4): 31–12.</mixed-citation>
         </ref>
         <ref id="d407e1548a1310">
            <mixed-citation id="d407e1552" publication-type="other">
Wilkinson, Steven I. 2006. "Explaining Changing Patterns of Party-
Voter Linkages in India." In Patrons, Clients or Politics: Patterns
of Political Accountability and Competition, eds. Herbert Kitschelt
and Steven I. Wilkinson. New York: Cambridge University Press,
pp. 110–40.</mixed-citation>
         </ref>
         <ref id="d407e1571a1310">
            <mixed-citation id="d407e1575" publication-type="other">
Yadav, Vineeta. 2011. Political Parties, Business Groups, and Cor-
ruption in Developing Countries. New York: Oxford University
Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
