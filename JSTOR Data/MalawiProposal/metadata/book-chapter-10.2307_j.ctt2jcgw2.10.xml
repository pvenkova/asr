<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2jcgw2</book-id>
      <subj-group>
         <subject content-type="call-number">PN1995.9.Q35F67 2009</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Queens in motion pictures</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Biographical films</subject>
         <subj-group>
            <subject content-type="lcsh">United States</subject>
            <subj-group>
               <subject content-type="lcsh">History and criticism</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Film Studies</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Royal Portraits in Hollywood</book-title>
         <subtitle>Filming the Lives of Queens</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Ford</surname>
               <given-names>Elizabeth A.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Mitchell</surname>
               <given-names>Deborah C.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>01</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780813125435</isbn>
      <isbn content-type="epub">9780813173399</isbn>
      <isbn content-type="epub">0813173396</isbn>
      <publisher>
         <publisher-name>The University Press of Kentucky</publisher-name>
         <publisher-loc>Lexington, Kentucky</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2009</copyright-year>
         <copyright-holder>The University Press of Kentucky</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt2jcgw2"/>
      <abstract abstract-type="short">
         <p>In the history of cinema, many film genres have gained and lost popularity with the changing times, but one has maintained its supreme reign -- the royal biopic. In Royal Portraits in Hollywood: Filming the Lives of Queens, authors Elizabeth A. Ford and Deborah C. Mitchell follow the lives of historical queens as depicted on film from the 1930s to the present. Women as diverse as Catherine the Great, Cleopatra, Mary Stuart, and Marie Antoinette have been represented on the silver screen, dominating the masculine world of politics while maintaining their femininity. During the golden age of American film, these roles gave Hollywood a means of portraying powerful women without threatening the patriarchal social order. Depictions of the lives of queens have progressed from idealized and romanticized portraits to the more personal, complex portrayals of modern Hollywood. By walking the line between fact and fiction, these royal portraits of queens reveal just as much our society as they do about the historical periods they represent. Audiences are drawn to the theaters year after year because the lives of queens promise good drama and attract some of the most talented actresses. The success of Hollywood's leading ladies in playing queens further solidifies the link between Hollywood royalty and authentic royalty. Actresses such as Bette Davis, Judy Dench, Helen Mirren, Elizabeth Taylor, and Greta Garbo have done more than influence the way we imagine historical queens -- they also have changed how we perceive women in powerful positions today. Royal Portraits in Hollywood analyzes seventy-five years of films about queens as well as the lives of the actresses who starred in them. Combining biographical sketches and excerpts from letters and journals, Ford and Mitchell show how filmmaking and our society's perceptions of gender have changed. The authors compare Hollywood's on-screen portrayals to the historical records, often drawing connections to the actresses' careers and personal lives. This comprehensive analysis provides a more complete picture of the lives that take place behind the thrones -- both real and fictional. The spectacle of a woman dressed in the full regalia of power remains a compelling image in our society. Hollywood actresses and the queens they portray are women who wield power, and by examining the lives of these women, the authors reveal not only society's perceptions about female power but also how those perceptions continue to evolve.</p>
      </abstract>
      <counts>
         <page-count count="338"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.4</book-part-id>
                  <title-group>
                     <title>Introduction</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>When Helen Mirren, authentically coiffed and costumed as Elizabeth II, turns to the camera in<italic>The Queen</italic>(Stephen Frears, 2006), she presents a perfect amalgam of royal personae, a queen playing a queen. Mirren is unarguably one of the most revered stars in the English-language cinema, and Elizabeth II is the<italic>queen,</italic>still so awe inducing that Jack Straw, a “former socialist” and the current lord chancellor, backed out of her royal presence at the 2007 opening of Parliament as meekly as a sixteenth-century courtier might have done (Lyall, “Ever Backward”). Elizabeth II wasn’t Mirren’s only royal role in 2006.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.5</book-part-id>
                  <title-group>
                     <label>Chapter One</label>
                     <title>Queen Christina of Sweden</title>
                  </title-group>
                  <fpage>8</fpage>
                  <abstract>
                     <p>Only two biopics of Queen Christina exist: Rouben Mamoulian’s 1933<italic>Queen Christina</italic>and Anthony Harvey’s 1974<italic>The Abdication,</italic>and, though vastly different interpretations, as the forty-one years between them might suggest, both capture the spirit of an unconventional life. Christina was born in 1626 under the shadow of her mentally unstable mother, Maria Eleonora of Brandenburg, and her legendary father, the Vasa king Gustav Adolph, also known as Gustav the Great and the Lion of the North for his political and military acumen. Disappointed in a female heir, King Gustav nevertheless insisted on educating his daughter in the manner befitting</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.6</book-part-id>
                  <title-group>
                     <label>Chapter Two</label>
                     <title>Catherine the Great</title>
                  </title-group>
                  <fpage>34</fpage>
                  <abstract>
                     <p>The 1930s introduced the first in a quartet of Disney fairy-tale adaptations:<italic>Snow White</italic>(David Hand, 1937),<italic>Cinderella</italic>(Clyde Geronimi et al., 1950),<italic>Sleeping Beauty</italic>(Clyde Geronimi, 1959), and<italic>Beauty and the Beast</italic>(Gary Trousdale and Kirk Wise, 1991). In these enduringly popular films, becoming a princess is the panacea. The complex lives of real queens provide antidotes to stories that equate a happy ending with a crown, some gorgeous gowns, and an adoring prince. Christina, born to rule and placed on the throne as a child, couldn’t put aside her royal burden quickly enough when she became an adult,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.7</book-part-id>
                  <title-group>
                     <label>Chapter Three</label>
                     <title>Cleopatra</title>
                  </title-group>
                  <fpage>71</fpage>
                  <abstract>
                     <p>Cleopatra VII had become a familiar cinematic subject by 1934, a boom year for royal biopics.<italic>Catherine the Great</italic>opened in February,<italic>The Scarlet Empress</italic>in September, and, in October, Paramount presented its lavish<italic>Cleopatra,</italic>starring Claudette Colbert, Warren William, and Henry Wilcoxon, and directed by that king of spectacle, Cecil B. DeMille. From our vantage point at the start of the film-saturated twenty-first century, the 1930s seem a distant, early decade in the development of the cinema, yet<italic>Cleopatra</italic>(1934) was the third American biopic of the Egyptian queen’s life, preceded by two silent films: in 1912, Helen Gardner</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.8</book-part-id>
                  <title-group>
                     <label>Chapter Four</label>
                     <title>Mary Stuart, Queen of Scotland</title>
                  </title-group>
                  <fpage>126</fpage>
                  <abstract>
                     <p>Carolyn Heilbrun has written: “Women who acquire power are more likely to be criticized for it than are the men who have always had it” (16). Witness the flurry of attention aimed at Katie Couric when she made the move from cohost of NBC’s<italic>Today Show</italic>to solo anchorwoman of the<italic>CBS Evening News.</italic>Hair too severe, delivery too serious, delivery too light, stories too frivolous, and on and on the critics wagged. Was Katie happy? Was Katie sorry she made the move? Was she making too much money? Could she handle the CBS power seat occupied by giants like</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.9</book-part-id>
                  <title-group>
                     <label>Chapter Five</label>
                     <title>Queen Victoria</title>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>Closest to us in time of all our queens, Queen Victoria has become the emblem of her age. Mary Stuart’s history can read like a hagiography, but even a casual mention of Victoria’s life conjures up a more earthly record of day-to-day dedication to country and family: the child trained toward the throne; the eighteen-year-old girl becoming queen of England in 1837; the young queen falling in love with the German Prince Albert; the two marrying and producing nine children; their family life encouraging domesticity in England; darkness entering with Albert’s death in 1861; Victoria’s deep mourning, lasting for nearly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.10</book-part-id>
                  <title-group>
                     <label>Chapter Six</label>
                     <title>Marie Antoinette</title>
                  </title-group>
                  <fpage>192</fpage>
                  <abstract>
                     <p>There’s a roundness to Queen Victoria’s life, a resounding satisfaction in the knowledge that all—or nearly all—had been spoken and done. No one could say the same about the life of Marie Antoinette. When she was fourteen and a half, the Austrian princess was sent, like a living treaty, to France. She married the dauphin in 1770, watched him ascend the French throne in 1774, and reigned as queen of pleasure in the splendor of Versailles. After the long-delayed consummation of their marriage, she bore Louis XVI four children, two of whom died. Marie Antoinette spent her happiest</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.11</book-part-id>
                  <title-group>
                     <label>Chapter Seven</label>
                     <title>Queen Elizabeth I</title>
                  </title-group>
                  <fpage>226</fpage>
                  <abstract>
                     <p>Images of the Virgin Queen fast-forward across film history, unforgettable, iconic images: the stately bearing; the red wigs; the high forehead; the long, aristocratic nose; the alabaster makeup; the pearl-drop earrings; the stiff, ornate ruffs; the fingers dripping with jewels; and the gowns, with yards and yards of white satin, purple velvet, gold, and silver ornamented and sparkling with rubies, diamonds, and more pearls. Even a schoolchild would be hard-pressed to mistake her for any other monarch.</p>
                     <p>As a case in point, this past fall, on one of our many visits to the National Portrait Gallery in London, we were</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.12</book-part-id>
                  <title-group>
                     <title>Conclusion</title>
                  </title-group>
                  <fpage>295</fpage>
                  <abstract>
                     <p>Feeling sheer gratitude at not being born royal is one effect of studying the lives of queens. Heads that<italic>don’t</italic>wear the crown can rest, if not always easily, at least without that particular weight. Recognizing the benefits of being common, however, doesn’t quench our thirst to peer inside if a palace gate opens a crack.<italic>The Queen</italic>(2006), directed by Stephen Frears and written by Peter Morgan, satisfies what must be a universal urge to experience the British royal enclosure and mind-set, to walk through Buckingham and Balmoral straight into the private quarters. This intriguing addition to royal biopics,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.13</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>301</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jcgw2.14</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>313</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
