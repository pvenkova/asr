<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">revieconstat</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100341</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Review of Economics and Statistics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>MIT Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00346535</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15309142</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40043107</article-id>
         <title-group>
            <article-title>Import Demand Elasticities and Trade Distortions</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Hiau Looi</given-names>
                  <surname>Kee</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alessandro</given-names>
                  <surname>Nicita</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Marcelo</given-names>
                  <surname>Olarreaga</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>11</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">90</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40002308</issue-id>
         <fpage>666</fpage>
         <lpage>682</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 the President and Fellows of Harvard College and the Massachusetts Institute of Technology</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40043107"/>
         <abstract>
            <p>This paper provides a systematic estimation of import demand elasticities for a broad group of countries at a very disaggregated level of product detail. We use a semiflexible translog GDP function approach to formally derive import demands and their elasticities, which are estimated with data on prices and endowments. Within a theoretically consistent framework, we use the estimated elasticities to construct Feenstra's (1995) simplification of Anderson and Neary's trade restrictiveness index (TRI). The difference between TRIs and import-weighted tariffs is shown to depend on the tariff variance and the covariance between tariffs and import demand elasticities.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d856e158a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d856e165" publication-type="other">
Panagariya, Shah, and Mishra (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e171" publication-type="other">
Broda and Weinstein (2006)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e178a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d856e185" publication-type="other">
Shiells, Stern, and Deardorff (1986)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e191" publication-type="other">
Shiells, Roland-
Holst, and Reinert (1993)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e200" publication-type="other">
Marquez (1999)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e207" publication-type="other">
Broda and Weinstein (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e213" publication-type="other">
Gallaway, McDaniel, and Rivera (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e219" publication-type="other">
Blackorby and Russell (1989)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e226a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d856e233" publication-type="other">
Anderson and Neary (1994, 1996, 2007)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e240a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d856e247" publication-type="other">
Riedel (1988) and Panagariya et al.
(2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d856e258a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d856e265" publication-type="other">
Harrigan, 1997</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e271" publication-type="other">
Kohli (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e278a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d856e285" publication-type="other">
section 5.3 of Kohli (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e292a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d856e299" publication-type="other">
Kohli (1991)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e306a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d856e313" publication-type="other">
Neary (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e320a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d856e327" publication-type="other">
Diewert and Wales (1988)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e334a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d856e341" publication-type="other">
Feenstra (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d856e347" publication-type="other">
Feenstra (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e355a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d856e362" publication-type="other">
Caves, Christensen, and Diewert (1982)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e369a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d856e376" publication-type="other">
Roodman (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d856e383a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d856e390" publication-type="other">
Wooldridge (2002, p. 275).</mixed-citation>
            </p>
         </fn>
         <fn id="d856e397a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d856e404" publication-type="other">
Kee, Nicita, and Olarreaga (2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d856e411a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d856e418" publication-type="other">
equation (3.5) in Feenstra (1995).</mixed-citation>
            </p>
         </fn>
         <fn id="d856e425a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d856e432" publication-type="other">
Kohli (1991), equations 18.27 to 18.31.</mixed-citation>
            </p>
         </fn>
         <fn id="d856e440a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d856e449" publication-type="other">
Anderson and Neary (2003),</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d856e465a1310">
            <mixed-citation id="d856e469" publication-type="other">
Anderson, James, and J. Peter Neary, "Measuring the Restrictiveness of
Trade Policies," World Bank Economic Review 8 (May 1994),
151-169.</mixed-citation>
         </ref>
         <ref id="d856e482a1310">
            <mixed-citation id="d856e486" publication-type="other">
--- "A New Approach to Evaluating Trade Policy," Review of Eco-
nomic Studies 63 (January 1996), 107-125.</mixed-citation>
         </ref>
         <ref id="d856e496a1310">
            <mixed-citation id="d856e500" publication-type="other">
--- "The Mercantilist Index of Trade Policy," International Economic
Review 44 (May 2003), 627-649.</mixed-citation>
         </ref>
         <ref id="d856e510a1310">
            <mixed-citation id="d856e514" publication-type="other">
---"Welfare versus Market Access: The Implications of Tariff Struc-
ture for Tariff Reform," Journal of International Economics 71
(March 2007), 187-205.</mixed-citation>
         </ref>
         <ref id="d856e528a1310">
            <mixed-citation id="d856e532" publication-type="other">
Arellano, Manuel, and Stephen Bond, "Some Tests of Specification for
Panel Data: Monte Carlo Evidence and an Application to Employ-
ment Equations," Review of Economic Studies 58 (April 1991),
277-297.</mixed-citation>
         </ref>
         <ref id="d856e548a1310">
            <mixed-citation id="d856e552" publication-type="other">
Arellano, Manuel, and Olympia Bover, "Female Labour Force Participa-
tion in the 1980s: The Case of Spain," Investigaciones Economicas
19 (May 1995), 171-194.</mixed-citation>
         </ref>
         <ref id="d856e565a1310">
            <mixed-citation id="d856e569" publication-type="other">
Blackorby, Charles, and Robert Russell, "The Morishima Elasticity of
Substitution; Symmetry, Constancy, Separability, and Its Relation-
ship to the Hicks and Allen Elasticities," Review of Economic
Studies 48 (January 1981), 147-158.</mixed-citation>
         </ref>
         <ref id="d856e585a1310">
            <mixed-citation id="d856e589" publication-type="other">
---"Will the Real Elasticity of Substitution Please Stand Up? (A
Comparison of the Allen/Uzawa and Morishima Elasticities),"
American Economic Review 79 (September 1989), 882-888.</mixed-citation>
         </ref>
         <ref id="d856e602a1310">
            <mixed-citation id="d856e606" publication-type="other">
Broda, Christian, and David Weinstein, "Globalization and the Gains from
Variety," Quarterly Journal of Economics 112 (May 2006), 541-
585.</mixed-citation>
         </ref>
         <ref id="d856e619a1310">
            <mixed-citation id="d856e623" publication-type="other">
Caves, Douglas W., Laurits R. Christensen, and W. Erwin Diewert,
"Multilateral Comparisons of Output, Input, and Productivity Us-
ing Superlative Index Numbers," Economic Journal 92 (March
1982), 73-86.</mixed-citation>
         </ref>
         <ref id="d856e640a1310">
            <mixed-citation id="d856e644" publication-type="other">
Diewert, W. Erwin, "Applications of Duality Theory," in M. D. Intriligator
and D. A. Kendrick (Eds.), Frontiers of Quantitative Economics II
(Amsterdam: North-Holland, 1974).</mixed-citation>
         </ref>
         <ref id="d856e657a1310">
            <mixed-citation id="d856e661" publication-type="other">
Diewert, W. Erwin, and Terence J. Wales, "A Normalized Quadratic
Semiflexible Function Form," Journal of Econometrics 37 (March
1988), 327-342.</mixed-citation>
         </ref>
         <ref id="d856e674a1310">
            <mixed-citation id="d856e678" publication-type="other">
Feenstra, Robert, "New Product Varieties and the Measurement of Inter-
national Prices," American Economic Review 84 (March 1994),
157-177.</mixed-citation>
         </ref>
         <ref id="d856e691a1310">
            <mixed-citation id="d856e695" publication-type="other">
--- "Estimating the Effects of Trade Policy," in Gene Grossman and
Kenneth Rogoff (Eds.), Handbook of International Economics, vol.
3 (Amsterdam: Elsevier, 1995).</mixed-citation>
         </ref>
         <ref id="d856e708a1310">
            <mixed-citation id="d856e712" publication-type="other">
---"A Homothetic Utility Function for Monopolistic Competition
Models, without Constant Price Elasticity," Economics Letters 78
(January 2003), 79-86.</mixed-citation>
         </ref>
         <ref id="d856e725a1310">
            <mixed-citation id="d856e729" publication-type="other">
Gallaway, Michael, Christine McDaniel, and Sandra Rivera, "Short-Run
and Long-Run Industry-Level Estimates of U.S. Armington Elas-
ticities," North American Journal of Economics and Finance 14
(March 2003), 49-68.</mixed-citation>
         </ref>
         <ref id="d856e746a1310">
            <mixed-citation id="d856e750" publication-type="other">
Harrigan, James, "Technology, Factor Supplies, and International Special-
ization: Estimating the Neoclassical Model," American Economic
Review 87 (September 1997), 475-94.</mixed-citation>
         </ref>
         <ref id="d856e763a1310">
            <mixed-citation id="d856e767" publication-type="other">
Helpman, Elhanan, Marc Melitz, and Yona Rubinstein, "Estimating Trade
Flows: Trading Partners and Trading Volumes," NBER working
paper no. W 12927 (2007).</mixed-citation>
         </ref>
         <ref id="d856e780a1310">
            <mixed-citation id="d856e784" publication-type="other">
Kee, Hiau Looi, Alessandro Nicita, and Marcelo Olarreaga, "Estimating
Ad- Valorem Equivalents of Non-Tariff Barriers" World Bank
Policy Research Working Paper no. 3840 (2006).</mixed-citation>
         </ref>
         <ref id="d856e797a1310">
            <mixed-citation id="d856e801" publication-type="other">
Kohli, Ulrich, Technology, Duality, and Foreign Trade: The GNP Func-
tion Approach to Modeling Imports and Exports (Ann Arbor:
University of Michigan Press, 1991).</mixed-citation>
         </ref>
         <ref id="d856e814a1310">
            <mixed-citation id="d856e818" publication-type="other">
Marquez, Jaime, "Long-Period Stable Trade Elasticities for Canada,
Japan, and the United States," Review of International Economics
7 (February 1999), 102-116.</mixed-citation>
         </ref>
         <ref id="d856e831a1310">
            <mixed-citation id="d856e835" publication-type="other">
Neary, J. Peter, "Rationalising the Penn World Table: True Multilateral
Indices for International Comparisons of Real Incomes," American
Economic Review 94 (December 2004), 1411-1428.</mixed-citation>
         </ref>
         <ref id="d856e849a1310">
            <mixed-citation id="d856e853" publication-type="other">
Panagariya, Arvind, Shekhar Shah, and Deepak Mishra, "Demand Elas-
ticities in International Trade: Are They Really Low?" Journal of
Development Economics 64 (April 2001), 313-342.</mixed-citation>
         </ref>
         <ref id="d856e866a1310">
            <mixed-citation id="d856e870" publication-type="other">
Rauch, James, "Network versus Markets in International Trade," Journal
of International Economics 48 (June 1999), 7-35.</mixed-citation>
         </ref>
         <ref id="d856e880a1310">
            <mixed-citation id="d856e884" publication-type="other">
Riedel, James, 'The Demand for LDC Exports of Manufactures: Esti-
mates for Hong Kong," Economic Journal 98 (March 1988),
138-148.</mixed-citation>
         </ref>
         <ref id="d856e897a1310">
            <mixed-citation id="d856e901" publication-type="other">
Roodman, David, "Xtabond2: Stata Module to Extend Xtabond Dynamic
Panel Data Estimator," Center for Global Development (2005).</mixed-citation>
         </ref>
         <ref id="d856e911a1310">
            <mixed-citation id="d856e915" publication-type="other">
Sanyal, Kalyan, and Ronald W. Jones, "The Theory of Trade in Middle
Products," American Economic Review 72 (March 1982), 16-
31.</mixed-citation>
         </ref>
         <ref id="d856e928a1310">
            <mixed-citation id="d856e932" publication-type="other">
Schott, Peter, "Across-Product versus Within-Product Specialization in
International Trade," Quarterly Journal of Economics 119 (May
2004), 647-678.</mixed-citation>
         </ref>
         <ref id="d856e946a1310">
            <mixed-citation id="d856e950" publication-type="other">
Semykina, Anastasia, and Jeffrey Wooldridge, "Estimating Panel Data
Models in the Presence of Endogeneity and Selection: Theory and
Application," Department of Economics, Michigan State Univer-
sity (2005).</mixed-citation>
         </ref>
         <ref id="d856e966a1310">
            <mixed-citation id="d856e970" publication-type="other">
Shiells, Clinton, David Roland-Holst, and Kenneth Reinert, "Modeling a
North American Free Trade Area: Estimation of Flexible Func-
tional Forms," Weltwirtschaftliches Archiv 129 (February 1993),
55-77.</mixed-citation>
         </ref>
         <ref id="d856e986a1310">
            <mixed-citation id="d856e990" publication-type="other">
Shiells, Clinton, Robert Stern, and Alan Deardorff, "Estimates of the
Elasticities of Substitution between Imports and Home Goods for
the United States," Weltwirtschaftliches Archiv 122 (June 1986),
497-519.</mixed-citation>
         </ref>
         <ref id="d856e1006a1310">
            <mixed-citation id="d856e1010" publication-type="other">
Stern, Robert, Jonathan Francis, and Bruce Schumacher, Price Elasticities
in International Trade: An Annotated Bibliography (London: Mac-
millan, 1976).</mixed-citation>
         </ref>
         <ref id="d856e1023a1310">
            <mixed-citation id="d856e1027" publication-type="other">
Wooldridge, Jeffrey, Econometric Analysis of Cross Section and Panel
Data (Cambridge, MA: MIT Press, 2002).</mixed-citation>
         </ref>
         <ref id="d856e1037a1310">
            <mixed-citation id="d856e1041" publication-type="other">
World Bank, World Development Indicators (2003).</mixed-citation>
         </ref>
         <ref id="d856e1049a1310">
            <mixed-citation id="d856e1053" publication-type="other">
Yi, Kei-Mu, "Can Vertical Specialization Explain the Growth of World
Trade?" Journal of Political Economy 111 (February 2003), 52-
102.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
