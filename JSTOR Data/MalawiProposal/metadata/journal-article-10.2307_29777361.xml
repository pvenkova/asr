<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">clininfedise</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j101405</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10584838</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">29777361</article-id>
         <article-categories>
            <subj-group>
               <subject>VIEWPOINTS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>The Potential Role for Protein-Conjugate Pneumococcal Vaccine in Adults: What Is the Supporting Evidence?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Daniel M.</given-names>
                  <surname>Musher</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Rahul</given-names>
                  <surname>Sampath</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Maria C.</given-names>
                  <surname>Rodriguez-Barradas</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2011</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">52</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i29777345</issue-id>
         <fpage>633</fpage>
         <lpage>640</lpage>
         <permissions>
            <copyright-statement>Copyright © 2011 Oxford University Press on behalf of the Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:role="external-fulltext-any"
                   xlink:href="http://dx.doi.org/10.1093/cid/ciq207"
                   xlink:title="an external site"/>
         <abstract>
            <p>Vaccination with protein-conjugate pneumococcal vaccine (PCV) provides children with extraordinary protection against pneumococcal disease, although the protective effect may be blunted by the emergence of replacement strains. Studies in adults have compared PCV with pneumococcal polysaccharide vaccine (PPV) using surrogate markers of protection, namely, serum anticapsular IgG antibody and opsonic activity. Results suggest that PCV is at least as effective as PPV for the strains covered, but a definitive and consistent advantage has not been demonstrated. Unfortunately, persons who are most in need of vaccine do not respond as well as otherwise healthy adults to either vaccine. Newer formulations of PCV will protect against the most prevalent of the current replacement strains, but replacement strains will create a moving target for PCVs. Unless an ongoing trial comparing 13-valent PCV with placebo (not to PPV) demonstrates a clearly better effect than that seen in the past with PPV, cost-effectiveness considerations are likely to prevent widespread use of PCV in adults.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d100e194a1310">
            <label>1</label>
            <mixed-citation id="d100e201" publication-type="other">
Avery OT, Goebel WR Chemo-immunological
studies on conjugated carboydrate-proteins.
II. Immunological specificity of synthetic
sugar-protein antigen. J Exp Med 1929; 50:
533-50.</mixed-citation>
         </ref>
         <ref id="d100e220a1310">
            <label>2</label>
            <mixed-citation id="d100e227" publication-type="other">
Schneerson R, Barrera O, Sutton A, Robbins
JB. Preparation, characterization, and im-
munogenicity of Haemophilus influenzae
type b polysaccharide-protein conjugates.
J Exp Med 1980; 152:361-76.</mixed-citation>
         </ref>
         <ref id="d100e246a1310">
            <label>3</label>
            <mixed-citation id="d100e253" publication-type="other">
Eskola J, Black S, Shinefield H. Pneumo¬
coccal conjugate vaccines. In: Plotkin SA,
Orenstein WA, eds. Vaccines. 4th ed. Phila¬
delphia: Saunders, 2004:589-624.</mixed-citation>
         </ref>
         <ref id="d100e269a1310">
            <label>4</label>
            <mixed-citation id="d100e276" publication-type="other">
Douglas RM, Paton JC, Duncan SJ,
Hansman DJ. Antibody response to pneu¬
mococcal vaccination in children younger
than five years of age. J Infect Dis 1983;
148:131-7.</mixed-citation>
         </ref>
         <ref id="d100e296a1310">
            <label>5</label>
            <mixed-citation id="d100e303" publication-type="other">
Smith DH, Peter G, Ingram DL, Harding AL,
Anderson P. Responses of children immu¬
nized with the capsular Polysaccharide of
Hemophilus influenzae, type b. Pediatrics
1973; 52:637-44.</mixed-citation>
         </ref>
         <ref id="d100e322a1310">
            <label>6</label>
            <mixed-citation id="d100e329" publication-type="other">
Robbins JB, Schneerson R. Polysaccharide-
protein conjugates: a new generation of
vaccines. J Infect Dis 1990; 161:821-32.</mixed-citation>
         </ref>
         <ref id="d100e342a1310">
            <label>7</label>
            <mixed-citation id="d100e349" publication-type="other">
Peltola H, Kilpi T, Anttila M. Rapid disap¬
pearance of Haemophilus influenzae type
b meningitis after routine childhood im¬
munisation with conjugate vaccines. Lancet
1992; 340:592-4.</mixed-citation>
         </ref>
         <ref id="d100e368a1310">
            <label>8</label>
            <mixed-citation id="d100e375" publication-type="other">
Black S, Shinefield H, Fireman B, et al.
Efficacy, safety and immunogenicity of
heptavalent pneumococcal conjugate vac¬
cine in children. Northern California Kaiser
Permanente Vaccine Study Center Group.
Pediatr Infect Dis J 2000; 19:187-95.</mixed-citation>
         </ref>
         <ref id="d100e398a1310">
            <label>9</label>
            <mixed-citation id="d100e407" publication-type="other">
Lexau CA, Lynfield R, Danila R, et al.
Changing epidemiology of invasive pneu¬
mococcal disease among older adults in the
era of pediatric pneumococcal conjugate
vaccine. JAMA 2005; 294:2043-51.</mixed-citation>
         </ref>
         <ref id="d100e426a1310">
            <label>10</label>
            <mixed-citation id="d100e433" publication-type="other">
Musher DM. Pneumococcal vaccine-direct
and indirect ("herd") effects. N Engl J Med
2006; 354:1522-4.</mixed-citation>
         </ref>
         <ref id="d100e447a1310">
            <label>11</label>
            <mixed-citation id="d100e454" publication-type="other">
Kayhty H, Auranen K, Nohynek H, Dagan R,
Makela H. Nasopharyngeal colonization:
a target for pneumococcal vaccination. Ex¬
pert Rev Vaccines 2006; 5:651-67.</mixed-citation>
         </ref>
         <ref id="d100e470a1310">
            <label>12</label>
            <mixed-citation id="d100e477" publication-type="other">
Musher DM, Groover JE, Watson DA, et al.
Genetic regulation of the capacity to make
immunoglobulin G to pneumococcal cap¬
sular Polysaccharides. J Investig Med 1997;
45:57-68.</mixed-citation>
         </ref>
         <ref id="d100e496a1310">
            <label>13</label>
            <mixed-citation id="d100e503" publication-type="other">
Conaty S, Watson L, Dinnes J, Waugh N.
The effectiveness of pneumococcal Poly¬
saccharide vaccines in adults: a systematic
review of observational studies and
comparison with results from ran¬
domised controlled trials. Vaccine 2004;
22:3214-4.</mixed-citation>
         </ref>
         <ref id="d100e529a1310">
            <label>14</label>
            <mixed-citation id="d100e536" publication-type="other">
Lipsky BA, Hirschmann JV. Pneumococcal
Polysaccharide vaccines do not protect the
elderly from pneumococcal infections. Neth
J Med 2004; 62:33-5.</mixed-citation>
         </ref>
         <ref id="d100e552a1310">
            <label>15</label>
            <mixed-citation id="d100e559" publication-type="other">
Moberley SA, Holden J, Tatham DP, An¬
drews RM. Vaccines for preventing pneu¬
mococcal infection in adults. Cochrane
Database Syst Rev 2008; CD000422.</mixed-citation>
         </ref>
         <ref id="d100e575a1310">
            <label>16</label>
            <mixed-citation id="d100e582" publication-type="other">
Shapiro ED, Berg AT, Austrian R, et al. The
protective efficacy of polyvalent pneumo¬
coccal Polysaccharide vaccine. N Engl J Med
1991; 325:1453-60.</mixed-citation>
         </ref>
         <ref id="d100e599a1310">
            <label>17</label>
            <mixed-citation id="d100e606" publication-type="other">
Centers for Disease Control and Prevention.
Prevention of pneumococcal disease of the
Advisory Committee on Immunization
Practices (ACIP). MMWR Morb Mortal
Wkly Rep 1997; 46:1-18.</mixed-citation>
         </ref>
         <ref id="d100e625a1310">
            <label>18</label>
            <mixed-citation id="d100e632" publication-type="other">
Dear K, Holden J, Andrews R, Tatham D.
Vaccines for preventing pneumococcal in¬
fection in adults. Cochrane Database Syst
Rev 2003; CD000422.</mixed-citation>
         </ref>
         <ref id="d100e648a1310">
            <label>19</label>
            <mixed-citation id="d100e655" publication-type="other">
Fedson DS, Musher DM. Pneumococcal
vaccine. In: Plotkin SA, Orenstein WB, eds.
Vaccines. 4th ed. Philadelphia: W.B. Sa-
unders, 2004:529-88.</mixed-citation>
         </ref>
         <ref id="d100e671a1310">
            <label>20</label>
            <mixed-citation id="d100e678" publication-type="other">
Melegaro A, Edmunds WJ. The 23-valent
pneumococcal Polysaccharide vaccine. Part
I. Efficacy of PPV in the elderly: a compari¬
son of meta-analyses, Eur J Epidemiol 2004;
19:353-63.</mixed-citation>
         </ref>
         <ref id="d100e697a1310">
            <label>21</label>
            <mixed-citation id="d100e704" publication-type="other">
Musher DM, Rueda-Jaimes AM, Graviss
EA, Rodriguez-Barradas MC. Effect of
pneumococcal vaccination: a comparison
of vaccination rates in patients with bac-
teremic and nonbacteremic pneumococcal
pneumonia. Clin Infect Dis 2006; 43:
1004-8.</mixed-citation>
         </ref>
         <ref id="d100e730a1310">
            <label>22</label>
            <mixed-citation id="d100e737" publication-type="other">
Johnstone J, Marrie TJ, Eurich DT,
Majumdar SR. Effect of pneumococcal vac¬
cination in hospitalized adults with com¬
munity-acquired pneumonia. Arch Intern
Med 2007; 167:1938-43.</mixed-citation>
         </ref>
         <ref id="d100e757a1310">
            <label>23</label>
            <mixed-citation id="d100e766" publication-type="other">
Vila-Corcoles A, Salsench E, Rodriguez-
Bianco T, et al. Clinical effectiveness of 23-
valent pneumococcal Polysaccharide vaccine
against pneumonia in middle-aged and
older adults: a matched case-control study.
Vaccine 2009; 27:1504-10.</mixed-citation>
         </ref>
         <ref id="d100e789a1310">
            <label>24</label>
            <mixed-citation id="d100e796" publication-type="other">
Musher DM, Manoff SB, Liss C, et al. Safety
and antibody response, including antibody
persistence for 5 years, after primary
vaccination or revaccination with pneumo¬
coccal Polysaccharide vaccine in middle-
aged and older adults. J Infect Dis 2010;
201:515-24.</mixed-citation>
         </ref>
         <ref id="d100e822a1310">
            <label>25</label>
            <mixed-citation id="d100e829" publication-type="other">
Musher DM, Manoff SB, McFetridge RD,
et al. Pneumococcal Polysaccharide vaccine,
and immunogenicity and safety of 2nd and
3d doses in older adults. Vaccine 2011;
Submitted for publication.</mixed-citation>
         </ref>
         <ref id="d100e848a1310">
            <label>26</label>
            <mixed-citation id="d100e855" publication-type="other">
Romero-Steiner S, Musher DM, Cetron MS,
et al. Reduction in functional antibody ac¬
tivity against Streptococcus pneumoniae in
vaccinated elderly individuals highly corre¬
lates with decreased IgG antibody avidity.
Clin Infect Dis 1999; 29:281-8.</mixed-citation>
         </ref>
         <ref id="d100e878a1310">
            <label>27</label>
            <mixed-citation id="d100e885" publication-type="other">
Musher DM, Phan HM, Watson DA,
Baughn RE. Antibody to capsular Poly¬
saccharide of Streptococcus pneumoniae at
the time of hospital admission for pneu¬
mococcal pneumonia. J Infect Dis 2000;
182:158-67.</mixed-citation>
         </ref>
         <ref id="d100e908a1310">
            <label>28</label>
            <mixed-citation id="d100e915" publication-type="other">
Simberkoff MS, Cross AP, Al-Ibrahim M,
et al. Efficacy of pneumococcal vaccine in
high-risk patients. Results of a Veterans
Administration Cooperative Study. N Engl J
Med 1986; 315:1318-27.</mixed-citation>
         </ref>
         <ref id="d100e935a1310">
            <label>29</label>
            <mixed-citation id="d100e942" publication-type="other">
Fedson DS, Liss C. Precise answers to the
wrong question: prospective clinical trials
and the meta-analyses of pneumococcal
vaccine in elderly and high-risk adults.
Vaccine 2004; 22:927-46.</mixed-citation>
         </ref>
         <ref id="d100e961a1310">
            <label>30</label>
            <mixed-citation id="d100e968" publication-type="other">
Carson PJ, Nichol KL, O'Brien J, Hilo P,
Janoff EN. Immune function and vaccine
responses in healthy advanced elderly pa¬
tients. Arch Intern Med 2000; 160:2017-24.</mixed-citation>
         </ref>
         <ref id="d100e984a1310">
            <label>31</label>
            <mixed-citation id="d100e991" publication-type="other">
Abraham-Van Parijs B. Review of pneumo¬
coccal conjugate vaccine in adults: im¬
plications on clinical development. Vaccine
2004; 22:1362-71.</mixed-citation>
         </ref>
         <ref id="d100e1007a1310">
            <label>32</label>
            <mixed-citation id="d100e1014" publication-type="other">
Jackson LA, Neuzil KM, Nahm MH, et al.
Immunogenicity of varying dosages of 7-val-
ent pneumococcal polysaccharide-protein
conjugate vaccine in seniors previously
vaccinated with 23-valent pneumococcal
Polysaccharide vaccine. Vaccine 2007; 25:
4029-37.</mixed-citation>
         </ref>
         <ref id="d100e1040a1310">
            <label>33</label>
            <mixed-citation id="d100e1047" publication-type="other">
de Roux A, Schmole-Thoma B, Siber GR,
et al. Comparison of pneumococcal conju¬
gate Polysaccharide and free Polysaccharide
vaccines in elderly adults: conjugate vaccine
elicits improved antibacterial immune re¬
sponses and immunological memory. Clin
Infect Dis 2008; 46:1015-23.</mixed-citation>
         </ref>
         <ref id="d100e1073a1310">
            <label>34</label>
            <mixed-citation id="d100e1080" publication-type="other">
Scott DA, Komjathy SF, Hu BT, et al. Phase 1
trial of a 13-valent pneumococcal conjugate
vaccine in healthy adults. Vaccine 2007;
25:6164-6.</mixed-citation>
         </ref>
         <ref id="d100e1097a1310">
            <label>35</label>
            <mixed-citation id="d100e1106" publication-type="other">
Goldblatt D, Southern J, Andrews N, et al.
The immunogenicity of 7-valent pneumo¬
coccal conjugate vaccine versus 23-valent
Polysaccharide vaccine in adults aged 50-
80 years. Clin Infect Dis 2009; 49:1318-25.</mixed-citation>
         </ref>
         <ref id="d100e1125a1310">
            <label>36</label>
            <mixed-citation id="d100e1132" publication-type="other">
Dransfeld MT, Nahm MH, Han MK, et al.
Superior immune response to protein-
conjugate vs. free pneumococcal Poly¬
saccharide vaccine in COPD. Am J Respir Crit
Care Med 2009; 15:495-505.</mixed-citation>
         </ref>
         <ref id="d100e1151a1310">
            <label>37</label>
            <mixed-citation id="d100e1158" publication-type="other">
Heidelberger M, DiLapi MM, Siegel M,
Walter AW. Persistence of antibodies in hu¬
man subjects injected with pneumococcal
Polysaccharides. J Immunol 1950; 65:535-41.</mixed-citation>
         </ref>
         <ref id="d100e1174a1310">
            <label>38</label>
            <mixed-citation id="d100e1181" publication-type="other">
Miernyk KM, Butler JC, Bulkow LR, et al.
Immunogenicity and reactogenicity of
pneumococcal Polysaccharide and conjugate
vaccines in Alaska native adults 55-70 years
of age. Clin Infect Dis 2009; 49:241-8.</mixed-citation>
         </ref>
         <ref id="d100e1200a1310">
            <label>39</label>
            <mixed-citation id="d100e1207" publication-type="other">
Davidson M, Parkinson AJ, Bulkow LR,
Fitzgerald MA, Peters HV, Parks DJ. The
epidemiology of invasive pneumococcal
disease in Alaska, 1986-1990—ethnic dif¬
ferences and opportunities for prevention. J
Infect Dis 1994; 170:368-76.</mixed-citation>
         </ref>
         <ref id="d100e1230a1310">
            <label>40</label>
            <mixed-citation id="d100e1237" publication-type="other">
Ridda I, Macintyre CR, Lindley R, et al.
Immunological responses to pneumococcal
vaccine in frail older people. Vaccine 2009;
27:1628-36.</mixed-citation>
         </ref>
         <ref id="d100e1254a1310">
            <label>41</label>
            <mixed-citation id="d100e1261" publication-type="other">
Feikin DR, Elie CM, Goetz MB, et al. Ran¬
domized trial of the quantitative and func¬
tional antibody responses to a 7-valent
pneumococcal conjugate vaccine and/or 23-
valent Polysaccharide vaccine among HIV-
infected adults. Vaccine 2001; 20:545-53.</mixed-citation>
         </ref>
         <ref id="d100e1284a1310">
            <label>42</label>
            <mixed-citation id="d100e1291" publication-type="other">
Lesprit P, Pedrono G, Molina JM, et al.
Immunological efficacy of a prime-boost
pneumococcal vaccination in HIV-infected
adults. AIDS 2007; 21:2425-34.</mixed-citation>
         </ref>
         <ref id="d100e1307a1310">
            <label>43</label>
            <mixed-citation id="d100e1314" publication-type="other">
Musher DM, Rueda AM, Nahm MH,
Graviss EA, Rodriguez-Barradas MC.
Initial and subsequent response to
pneumococcal Polysaccharide and protein-
conjugate vaccines administered sequentially
to adults who have recovered from pneu¬
mococcal pneumonia. J Infect Dis 2008;
198:1019-27.</mixed-citation>
         </ref>
         <ref id="d100e1343a1310">
            <label>44</label>
            <mixed-citation id="d100e1350" publication-type="other">
O'Brien KL, Hochman M, Goldblatt D.
Combined schedules of pneumococcal con¬
jugate and Polysaccharide vaccines: is hy¬
poresponsiveness an issue? Lancet Infect Dis
2007; 7:597-606.</mixed-citation>
         </ref>
         <ref id="d100e1369a1310">
            <label>45</label>
            <mixed-citation id="d100e1376" publication-type="other">
Dagan R, Givon-Lavi N, Greenberg D, Frit-
zell B, Siegrist CA. Nasopharyngeal carriage
of Streptococcus pneumoniae shortly before
vaccination with a pneumococcal conjugate
vaccine causes serotype-specific hypores¬
ponsiveness in early infancy. J Infect Dis
2010; 201:1570-9.</mixed-citation>
         </ref>
         <ref id="d100e1402a1310">
            <label>46</label>
            <mixed-citation id="d100e1409" publication-type="other">
Miiro G, Kayhty H, Watera C, et al. Conju¬
gate pneumococcal vaccine in HIV-infected
Ugandans and the effect of past receipt of
Polysaccharide vaccine. J Infect Dis 2005;
192:1801-5.</mixed-citation>
         </ref>
         <ref id="d100e1429a1310">
            <label>47</label>
            <mixed-citation id="d100e1436" publication-type="other">
Kumar D, Rotstein C, Miyata G, Arien D,
Humar A. Randomized, double-blind, con¬
trolled trial of pneumococcal vaccination in
renal transplant recipients. J Infect Dis 2003;
187:1639-45.</mixed-citation>
         </ref>
         <ref id="d100e1455a1310">
            <label>48</label>
            <mixed-citation id="d100e1462" publication-type="other">
Kumar D, Welsh B, Siegal D, Chen MH,
Humar A. Immunogenicity of pneumococ¬
cal vaccine in renal transplant recipients-
three year follow-up of a randomized trial.
Am J Transplant 2007; 7:633-8.</mixed-citation>
         </ref>
         <ref id="d100e1481a1310">
            <label>49</label>
            <mixed-citation id="d100e1488" publication-type="other">
Kumar D, Chen MH, Wong G, et al. A
randomized, double-blind, placebo-
controlled trial to evaluate the prime-boost
strategy for pneumococcal vaccination in
adult liver transplant recipients. Clin Infect
Dis 2008; 47:885-92.</mixed-citation>
         </ref>
         <ref id="d100e1511a1310">
            <label>50</label>
            <mixed-citation id="d100e1518" publication-type="other">
Kumar D, Chen MH, Welsh B, et al. A
randomized, double-blind trial of pneumo¬
coccal vaccination in adult allogeneic stem
cell transplant donors and recipients. Clin
Infect Dis 2007; 45:1576-82.</mixed-citation>
         </ref>
         <ref id="d100e1537a1310">
            <label>51</label>
            <mixed-citation id="d100e1544" publication-type="other">
French N, Nakiyingi J, Carpenter LM, et al.
23-valent pneumococcal Polysaccharide
vaccine in HIV-1-infected Ugandan adults:
double-blind, randomised and placebo
controlled trial. Lancet 2000; 355:2106-11.</mixed-citation>
         </ref>
         <ref id="d100e1563a1310">
            <label>52</label>
            <mixed-citation id="d100e1570" publication-type="other">
French N, Gordon SB, Mwalukomo T, et al.
A trial of a 7-valent pneumococcal conjugate
vaccine in HIV-infected adults. N Engl J
Med 2010; 362:812-22.</mixed-citation>
         </ref>
         <ref id="d100e1587a1310">
            <label>53</label>
            <mixed-citation id="d100e1594" publication-type="other">
Center KJ, Strauss A. Safety experience with
heptavalent pneumococcal CRM 197-
conjugate vaccine (Prevenar) since vaccine
introduction. Vaccine 2009; 27:3281-4.</mixed-citation>
         </ref>
         <ref id="d100e1610a1310">
            <label>54</label>
            <mixed-citation id="d100e1617" publication-type="other">
Dinleyici EC, Yargic ZA. Current knowledge
regarding the investigational 13-valent
pneumococcal conjugate vaccine. Expert
Rev Vaccines 2009; 8:977-86.</mixed-citation>
         </ref>
         <ref id="d100e1633a1310">
            <label>55</label>
            <mixed-citation id="d100e1640" publication-type="other">
Metersky ML, Dransfield MT, Jackson LA.
Determining the optimal pneumococcal
vaccination strategy for adults: is there a role
for the pneumococcal conjugate vaccine?
Chest 2010; 138:486-90.</mixed-citation>
         </ref>
         <ref id="d100e1659a1310">
            <label>56</label>
            <mixed-citation id="d100e1666" publication-type="other">
Musher DM, Groover JE, Watson DA, Ro-
driguez-Barradas MC, Baughn RE. IgG
responses to protein-conjugated pneumo¬
coccal capsular Polysaccharides in persons
who are genetically incapable of responding
to unconjugated Polysaccharides. Clin Infect
Dis 1998; 27:1487-90.</mixed-citation>
         </ref>
         <ref id="d100e1692a1310">
            <label>57</label>
            <mixed-citation id="d100e1701" publication-type="other">
Hak E, Grobbee DE, Sanders EA, et al. Ra¬
tionale and design of CAPITA: a RCT of 13-
valent conjugated pneumococcal vaccine
efficacy among older adults. Neth J Med
2008; 66:378-83.</mixed-citation>
         </ref>
         <ref id="d100e1720a1310">
            <label>58</label>
            <mixed-citation id="d100e1727" publication-type="other">
Weycker D, Sato R, Strutton D, Edelsberg
J, Jackson LA. Public health and economic
impact of 13-valent pneumococcal conju¬
gate vaccine (PCV13) in US adults aged
&gt;50 years [abstract 462]. Forty-
eighthAnnual Meeting, Infectious Diseases
Society of America. Vancouver, 2010.</mixed-citation>
         </ref>
         <ref id="d100e1754a1310">
            <label>59</label>
            <mixed-citation id="d100e1761" publication-type="other">
Pelton SI, Dagan R, Gaines BM, et al.
Pneumococcal conjugate vaccines: proceed¬
ings from an interactive symposium at the
41st Interscience Conference on Antimicro¬
bial Agents and Chemotherapy. Vaccine
2003; 21:1562-71.</mixed-citation>
         </ref>
         <ref id="d100e1784a1310">
            <label>60</label>
            <mixed-citation id="d100e1791" publication-type="other">
Cohen R, Levy C, Bonnet E, et al. Dynamic
of pneumococcal nasopharyngeal carriage in
children with acute Otitis media following
PCV7 introduction in France. Vaccine 2009;
28:6114-21.</mixed-citation>
         </ref>
         <ref id="d100e1810a1310">
            <label>61</label>
            <mixed-citation id="d100e1817" publication-type="other">
Jacobs MR, Good CE, Bajaksouzian S,
Windau AR. Emergence of Streptococcus
pneumoniae serotypes 19A, 6C, and 22F and
serogroup 15 in Cleveland, Ohio, in relation
to introduction of the protein-conjugated
pneumococcal vaccine. Clin Infect Dis 2008;
47:1388-95.</mixed-citation>
         </ref>
         <ref id="d100e1843a1310">
            <label>62</label>
            <mixed-citation id="d100e1850" publication-type="other">
Siira L, Rantala M, Jalava J, et al. Temporal
trends of antimicrobial resistance and clon-
ality of invasive Streptococcus pneumoniae
isolates in Finland, 2002 to 2006. Antimicrob
Agents Chemother 2009; 53:2066-73.</mixed-citation>
         </ref>
         <ref id="d100e1869a1310">
            <label>63</label>
            <mixed-citation id="d100e1876" publication-type="other">
Smith KJ, Zimmerman RK, Lin CJ, et al.
Alternative strategies for adult pneumo¬
coccal Polysaccharide vaccination: a cost-
effectiveness analysis. Vaccine 2008; 26:
1420-31.</mixed-citation>
         </ref>
         <ref id="d100e1895a1310">
            <label>64</label>
            <mixed-citation id="d100e1902" publication-type="other">
Evers SM, Ament AJ, Colombo GL, et al.
Cost-effectiveness of pneumococcal vac¬
cination for prevention of invasive pneu¬
mococcal disease in the elderly: an update
for 10 Western European countries. Eur J
Clin Microbiol Infect Dis 2007; 26:531-40.</mixed-citation>
         </ref>
         <ref id="d100e1926a1310">
            <label>65</label>
            <mixed-citation id="d100e1933" publication-type="other">
Smith KJ, Zimmerman RK, Nowalk MP,
Roberts MS. Age, revaccination, and
tolerance effects on pneumococcal vaccina¬
tion strategies in the elderly: a cost-
effectiveness analysis. Vaccine 2009; 27:
3159-64.</mixed-citation>
         </ref>
         <ref id="d100e1956a1310">
            <label>66</label>
            <mixed-citation id="d100e1963" publication-type="other">
http://www.cdc.gov/vaccines/programs/
VFC/cdc-vac-price-list.htm 2010.</mixed-citation>
         </ref>
         <ref id="d100e1973a1310">
            <label>67</label>
            <mixed-citation id="d100e1980" publication-type="other">
French N, Gordon SB, Mwalukomo T, et al.
A trial of a 7-valent pneumococcal conjugate
vaccine in HIV-infected adults. N Engl J
Med 2010; 362:812-22.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
