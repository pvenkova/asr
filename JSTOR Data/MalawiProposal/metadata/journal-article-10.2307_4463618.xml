<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">clininfedise</journal-id>
         <journal-id journal-id-type="jstor">j101405</journal-id>
         <journal-title-group>
            <journal-title>Clinical Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">10584838</issn>
         <issn pub-type="epub">15376591</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4463618</article-id>
         <article-categories>
            <subj-group>
               <subject>HIV/AIDS</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Does Severity of HIV Disease in HIV-Infected Mothers Affect Mortality and Morbidity among Their Uninfected Infants?</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Louise</given-names>
                  <surname>Kuhn</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Prisca Kasonde</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Moses Sinkala</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Chipepo</given-names>
                  <surname>Kankasa</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Katherine Semrau</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Nancy</given-names>
                  <surname>Scott</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Wei-Yann</given-names>
                  <surname>Tsai</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>Sten H. Vermund</string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Grace M.</given-names>
                  <surname>Aldrovandi</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Donald M.</given-names>
                  <surname>Thea</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>01</day>
            <month>12</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">41</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</issue>
         <issue-id>i402338</issue-id>
         <fpage>1654</fpage>
         <lpage>1661</lpage>
         <page-range>1654-1661</page-range>
         <permissions>
            <copyright-statement>Copyright 2005 The Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4463618"/>
         <abstract>
            <p> Background. Rates of perinatal human immunodeficiency virus (HIV) transmission are higher among HIV-infected mothers with more advanced disease, but effects of maternal disease on HIV-uninfected offspring are unclear. We investigated the hypothesis that the severity of HIV disease and immune dysfunction among mothers is associated with increased morbidity and mortality among their uninfected infants. Methods. In a birth cohort of 620 HIV-uninfected infants born to HIV-infected mothers in Lusaka, Zambia, we investigated associations between markers of more advanced maternal HIV disease and child mortality, hospital admissions, and infant weight through 4 months of age. Results. Mortality in the cohort of uninfected infants was 4.6% (95% confidence interval [CI], 2.8-6.3) through 4 months of age. Infants of mothers with CD4⁺ T cell counts of &lt;350 cells/μL were more likely to die (hazard ratio [HR], 2.87; 95% CI, 1.03-8.03) and were more likely to be hospitalized (HR, 2.28; 95% CI, 1.17-4.45), after adjusting for other factors, including maternal death and low birth weight. The most common cause of infant death and hospitalization was pneumonia and/or sepsis. A maternal viral load of &gt;100,000 copies/mL was associated with significantly lower child weight through 4 months of age. Conclusion. Children born to HIV-infected mothers with advanced disease who escaped perinatal or early breastfeeding-related HIV infection are nonetheless at high risk of mortality and morbidity during the first few months of life. HIV-related immunosuppression appears to have adverse consequences for the health of infants, in addition to risks of vertical transmission. </p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d92e238a1310">
            <label>1</label>
            <mixed-citation id="d92e245" publication-type="journal">
Connor EM, Sperling RS, Gelber R, et al. Reduction of maternal-infant
transmission of human immunodeficiency virus type 1 with zidovudine
treatment. N Engl J Med1994;331:1173-80.<person-group>
                  <string-name>
                     <surname>Connor</surname>
                  </string-name>
               </person-group>
               <fpage>1173</fpage>
               <volume>331</volume>
               <source>N Engl J Med</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d92e280a1310">
            <label>2</label>
            <mixed-citation id="d92e287" publication-type="journal">
Guay LA, Musoke P, Fleming T, et al. Intrapartum and neonatal single-
dose nevirapine compared with zidovudine for prevention of mother-
to-child transmission of HIV-1 in Kampala, Uganda: HIVNET 012
randomised trial. Lancet1999; 354:795-802.<person-group>
                  <string-name>
                     <surname>Guay</surname>
                  </string-name>
               </person-group>
               <fpage>795</fpage>
               <volume>354</volume>
               <source>Lancet</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d92e325a1310">
            <label>3</label>
            <mixed-citation id="d92e332" publication-type="journal">
Dabis F, Msellati A, Meda N, et al. 6-Month efficacy, tolerance, and
acceptability of a short regimen of oral zidovudine to reduce vertical
transmission of HIV in breast fed children in C6te d'Ivoire and Burkina
Faso: a double-blind placebo-controlled multi-centre trial. Lancet1999;
353:786-92.<person-group>
                  <string-name>
                     <surname>Dabis</surname>
                  </string-name>
               </person-group>
               <fpage>786</fpage>
               <volume>353</volume>
               <source>Lancet</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d92e373a1310">
            <label>4</label>
            <mixed-citation id="d92e382" publication-type="journal">
Newell ML, Coovadia H, Cortina-Borja M, Rollins N, Gaillard P, Dabis
F. Mortality of infected and uninfected infants born to HIV-infected
mothers in Africa: a pooled analysis. Lancet2004; 364:1236-43.<person-group>
                  <string-name>
                     <surname>Newell</surname>
                  </string-name>
               </person-group>
               <fpage>1236</fpage>
               <volume>364</volume>
               <source>Lancet</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e418a1310">
            <label>5</label>
            <mixed-citation id="d92e425" publication-type="journal">
Crampin AC, Floyd S, Glynn JR, et al. The long-term impact of HIV
and orphanhood on the mortality and physical well-being of children
in rural Malawi. AIDS2003; 17:389-97.<person-group>
                  <string-name>
                     <surname>Crampin</surname>
                  </string-name>
               </person-group>
               <fpage>389</fpage>
               <volume>17</volume>
               <source>AIDS</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e460a1310">
            <label>6</label>
            <mixed-citation id="d92e467" publication-type="journal">
Nakiyingi JS, Bracher M, Whitworth JA, et al. Child survival in relation
to mother's HIV infection and survival: evidence from a Ugandan
cohort study. AIDS2003; 17:1827-34.<person-group>
                  <string-name>
                     <surname>Nakiyingi</surname>
                  </string-name>
               </person-group>
               <fpage>1827</fpage>
               <volume>17</volume>
               <source>AIDS</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e502a1310">
            <label>7</label>
            <mixed-citation id="d92e509" publication-type="journal">
Ng'weshemi J, Urassa M, Isingo R, et al. HIV impact on mother and
child mortality in rural Tanzania. J Acquir Immune Defic Syndr2003;
33:393-404.<person-group>
                  <string-name>
                     <surname>Ng'weshemi</surname>
                  </string-name>
               </person-group>
               <fpage>393</fpage>
               <volume>33</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e544a1310">
            <label>8</label>
            <mixed-citation id="d92e551" publication-type="journal">
Ota MO, O'Donovan D, Alabi AS, et al. Maternal HIV-1 and HIV-2
infection and child survival in The Gambia. AIDS2000; 14:435-9.<person-group>
                  <string-name>
                     <surname>Ota</surname>
                  </string-name>
               </person-group>
               <fpage>435</fpage>
               <volume>14</volume>
               <source>AIDS</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d92e583a1310">
            <label>9</label>
            <mixed-citation id="d92e590" publication-type="journal">
Schim van der Loeff MF, Hansmann A, Awasana AA, et al. Survival
of HIV-1 and HIV-2 perinatally infected children in The Gambia. AIDS
2003; 17:2389-94.<person-group>
                  <string-name>
                     <surname>Schim van der Loeff</surname>
                  </string-name>
               </person-group>
               <fpage>2389</fpage>
               <volume>17</volume>
               <source>AIDS</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e625a1310">
            <label>10</label>
            <mixed-citation id="d92e632" publication-type="journal">
Garcia PM, Kalish LA, Pitt J, et al. Maternal levels of plasma human
immunodeficiency virus type 1 RNA and the risk of perinatal trans-
mission. Women and Infants Transmission Study Group. N Engl J Med
1999; 341:394-402.<person-group>
                  <string-name>
                     <surname>Garcia</surname>
                  </string-name>
               </person-group>
               <fpage>394</fpage>
               <volume>341</volume>
               <source>N Engl J Med</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d92e671a1310">
            <label>11</label>
            <mixed-citation id="d92e678" publication-type="journal">
Blanche S, Mayaux MJ, Rouzioux C, et al. Relation of the course of
HIV infection in children to the severity of the disease in their mothers
at delivery. N Engl J Med1994; 330:308-12.<person-group>
                  <string-name>
                     <surname>Blanche</surname>
                  </string-name>
               </person-group>
               <fpage>308</fpage>
               <volume>330</volume>
               <source>N Engl J Med</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d92e713a1310">
            <label>12</label>
            <mixed-citation id="d92e720" publication-type="journal">
Abrams EJ, Wiener J, Carter R, et al. Maternal health factors and early
pediatric antiretroviral therapy influence the rate of perinatal HIV-1
disease progression in children. AIDS2003; 17:867-77.<person-group>
                  <string-name>
                     <surname>Abrams</surname>
                  </string-name>
               </person-group>
               <fpage>867</fpage>
               <volume>17</volume>
               <source>AIDS</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e755a1310">
            <label>13</label>
            <mixed-citation id="d92e762" publication-type="journal">
Thea DM, Vwalika C, Kasonde P, et al. Issues in the design of a clinical
trial with a behavioral intervention-the Zambia exclusive breast-feed-
ing study. Control Clin Trials2004;25:353-65.<person-group>
                  <string-name>
                     <surname>Thea</surname>
                  </string-name>
               </person-group>
               <fpage>353</fpage>
               <volume>25</volume>
               <source>Control Clin Trials</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e797a1310">
            <label>14</label>
            <mixed-citation id="d92e804" publication-type="journal">
Ghosh MK, Kuhn L, West J, et al. Quantitation of human immunode-
ficiency virus type 1 in breast milk. J Clin Microbiol2003; 41:2465-70.<person-group>
                  <string-name>
                     <surname>Ghosh</surname>
                  </string-name>
               </person-group>
               <fpage>2465</fpage>
               <volume>41</volume>
               <source>J Clin Microbiol</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e836a1310">
            <label>15</label>
            <mixed-citation id="d92e843" publication-type="journal">
Adetunji J. Trends in under-5 mortality rates and the HIV/AIDS ep-
idemic. Bull WHO2000; 78:1200-6.<person-group>
                  <string-name>
                     <surname>Adetunji</surname>
                  </string-name>
               </person-group>
               <fpage>1200</fpage>
               <volume>78</volume>
               <source>Bull WHO</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d92e875a1310">
            <label>16</label>
            <mixed-citation id="d92e882" publication-type="journal">
Lederman MM, Valdez H. Immune restoration with antiretroviral ther-
apies: implications for clinical management. JAMA2000; 284:223-8.<person-group>
                  <string-name>
                     <surname>Lederman</surname>
                  </string-name>
               </person-group>
               <fpage>223</fpage>
               <volume>284</volume>
               <source>JAMA</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d92e915a1310">
            <label>17</label>
            <mixed-citation id="d92e922" publication-type="journal">
Pillay T, Sturm AW, Khan M, et al. Vertical transmission of Mycobac-
terium tuberculosis in KwaZulu Natal: impact of HIV-1 co-infection.
Int J Tuberc Lung Dis2004;8:59-69.<person-group>
                  <string-name>
                     <surname>Pillay</surname>
                  </string-name>
               </person-group>
               <fpage>59</fpage>
               <volume>8</volume>
               <source>Int J Tuberc Lung Dis</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e957a1310">
            <label>18</label>
            <mixed-citation id="d92e964" publication-type="journal">
Tedeschi R, Pivetta E, Zanussi S, et al. Quantification of hepatitis C
virus (HCV) in liver specimens and sera from patients with human
immunodeficiency virus coinfection by using the Versant HCV RNA
3.0 (branched DNA-based) DNA assay. J Clin Microbiol2003;41:
3046-50.<person-group>
                  <string-name>
                     <surname>Tedeschi</surname>
                  </string-name>
               </person-group>
               <fpage>3046</fpage>
               <volume>41</volume>
               <source>J Clin Microbiol</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1005a1310">
            <label>19</label>
            <mixed-citation id="d92e1012" publication-type="journal">
Tedeschi R, Enbom M, Bidoli E, Linde A, De Paoli P, Dillner J. Viral
load of human herpesvirus 8 in peripheral blood of human immu-
nodeficiency virus-infected patients with Kaposi's sarcoma. J Clin Mi-
crobiol2001; 39:4269-73.<person-group>
                  <string-name>
                     <surname>Tedeschi</surname>
                  </string-name>
               </person-group>
               <fpage>4269</fpage>
               <volume>39</volume>
               <source>J Clin Microbiol</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1050a1310">
            <label>20</label>
            <mixed-citation id="d92e1057" publication-type="journal">
Fidouh-Houhou N, Duval X, Bissuel F, et al. Salivary cytomegalovirus
(CMV) shedding, glycoprotein B genotype distribution, and CMV dis-
ease in human immunodeficiency virus-seropositive patients. Clin In-
fect Dis2001; 33:1406-11.<person-group>
                  <string-name>
                     <surname>Fidouh-Houhou</surname>
                  </string-name>
               </person-group>
               <fpage>1406</fpage>
               <volume>33</volume>
               <source>Clin Infect Dis</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1095a1310">
            <label>21</label>
            <mixed-citation id="d92e1102" publication-type="journal">
Gerard L, Leport C, Flandre P, et al. Cytomegalovirus (CMV) viremia
and the CD4+ lymphocyte count as predictors of CMV disease in
patients infected with human immunodeficiency virus. Clin Infect Dis
1997; 24:836-40.<person-group>
                  <string-name>
                     <surname>Gerard</surname>
                  </string-name>
               </person-group>
               <fpage>836</fpage>
               <volume>24</volume>
               <source>Clin Infect Dis</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1140a1310">
            <label>22</label>
            <mixed-citation id="d92e1147" publication-type="journal">
Brayfield BP, Phiri S, Kankasa C, et al. Postnatal human herpesvirus
8 and human immunodeficiency virus type 1 infection in mothers and
infants from Zambia. J Infect Dis2003; 187:559-68.<person-group>
                  <string-name>
                     <surname>Brayfield</surname>
                  </string-name>
               </person-group>
               <fpage>559</fpage>
               <volume>187</volume>
               <source>J Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1183a1310">
            <label>23</label>
            <mixed-citation id="d92e1190" publication-type="journal">
Mussi-Pinhata MM, Yamamoto AY, Figueiredo LT, Cervi MC, Duarte
G. Congenital and perinatal cytomegalovirus infection in infants born
to mothers infected with human immunodeficiency virus. J Pediatr
1998; 132:285-90.<person-group>
                  <string-name>
                     <surname>Mussi-Pinhata</surname>
                  </string-name>
               </person-group>
               <fpage>285</fpage>
               <volume>132</volume>
               <source>J Pediatr</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1228a1310">
            <label>24</label>
            <mixed-citation id="d92e1235" publication-type="journal">
Moraes-Pinto MI, Almeida AC, Kenj G, et al. Placental transfer and
maternally acquired neonatal IgG immunity in human immunodefi-
ciency virus infection. J Infect Dis1996; 173:1077-84.<person-group>
                  <string-name>
                     <surname>Moraes-Pinto</surname>
                  </string-name>
               </person-group>
               <fpage>1077</fpage>
               <volume>173</volume>
               <source>J Infect Dis</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1270a1310">
            <label>25</label>
            <mixed-citation id="d92e1279" publication-type="journal">
Embree JE, Datta P, Stackiw W, et al. Increased risk of early measles
in infants of human immunodeficiency virus type 1-seropositive moth-
ers. J Infect Dis1992; 165:262-7.<person-group>
                  <string-name>
                     <surname>Embree</surname>
                  </string-name>
               </person-group>
               <fpage>262</fpage>
               <volume>165</volume>
               <source>J Infect Dis</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1314a1310">
            <label>26</label>
            <mixed-citation id="d92e1321" publication-type="journal">
Thomas JE, Bunn JEG, Kleanthous H, et al. Specific immunoglobulin
A antibodies in maternal milk and delayed Helicobacter pylori colo-
nization in Gambian infants. Clin Infect Dis2004;39:1155-60.<person-group>
                  <string-name>
                     <surname>Thomas</surname>
                  </string-name>
               </person-group>
               <fpage>1155</fpage>
               <volume>39</volume>
               <source>Clin Infect Dis</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1356a1310">
            <label>27</label>
            <mixed-citation id="d92e1363" publication-type="journal">
Chougnet CA, Kovacs A, Baker R, et al. Influence of human immu-
nodeficiency virus-infected maternal environment on development of
infant interleukin-12 production. J Infect Dis2000; 181:1590-7.<person-group>
                  <string-name>
                     <surname>Chougnet</surname>
                  </string-name>
               </person-group>
               <fpage>1590</fpage>
               <volume>181</volume>
               <source>J Infect Dis</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1398a1310">
            <label>28</label>
            <mixed-citation id="d92e1405" publication-type="journal">
Clerici M, Saresella M, Colombo F, et al. T-lymphocyte maturation
abnormalities in uninfected newborns and children with vertical ex-
posure to HIV. Blood2000; 96:3866-71.<person-group>
                  <string-name>
                     <surname>Clerici</surname>
                  </string-name>
               </person-group>
               <fpage>3866</fpage>
               <volume>96</volume>
               <source>Blood</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1441a1310">
            <label>29</label>
            <mixed-citation id="d92e1448" publication-type="journal">
Ota MO, O'Donovan D, Marchant A, et al. HIV-negative infants born
to HIV-1 but not HIV-2-positive mothers fail to develop a Bacillus
Calmette-Guerin scar. AIDS1999; 13:996-8.<person-group>
                  <string-name>
                     <surname>Ota</surname>
                  </string-name>
               </person-group>
               <fpage>996</fpage>
               <volume>13</volume>
               <source>AIDS</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1483a1310">
            <label>30</label>
            <mixed-citation id="d92e1490" publication-type="journal">
Fawzi W. Micronutrients and human immunodeficiency virus type 1
disease progression among adults and children. Clin Infect Dis2003;
37(Suppl 2):S112-6.<person-group>
                  <string-name>
                     <surname>Fawzi</surname>
                  </string-name>
               </person-group>
               <issue>Suppl 2</issue>
               <fpage>S112</fpage>
               <volume>37</volume>
               <source>Clin Infect Dis</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1528a1310">
            <label>31</label>
            <mixed-citation id="d92e1535" publication-type="journal">
Cunningham CK, Charbonneau TT, Song K, et al. Comparison of
human immunodeficiency virus 1 DNA polymerase chain reaction and
qualitative and quantitative RNA polymerase chain reaction in human
immunodeficiency virus 1-exposed infants. Pediatr Infect Dis J1999;
18:30-5.<person-group>
                  <string-name>
                     <surname>Cunningham</surname>
                  </string-name>
               </person-group>
               <fpage>30</fpage>
               <volume>18</volume>
               <source>Pediatr Infect Dis J</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1576a1310">
            <label>32</label>
            <mixed-citation id="d92e1585" publication-type="journal">
Delamare C, Burgard M, Mayaux MJ, et al. HIV-1 RNA detection in
plasma for the diagnosis of infection in neonates. J Acquir Immune
Defic Syndr Hum Retrovirol1997; 15:121-5.<person-group>
                  <string-name>
                     <surname>Delamare</surname>
                  </string-name>
               </person-group>
               <fpage>121</fpage>
               <volume>15</volume>
               <source>J Acquir Immune Defic Syndr Hum Retrovirol</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1620a1310">
            <label>33</label>
            <mixed-citation id="d92e1627" publication-type="journal">
Steketee RW, Abrams EJ, Thea DM, et al. Early detection of perinatal
human immunodeficiency virus (HIV) type 1 infection using HIV RNA
amplification and detection. J Infect Dis1997; 175:707-11.<person-group>
                  <string-name>
                     <surname>Steketee</surname>
                  </string-name>
               </person-group>
               <fpage>707</fpage>
               <volume>175</volume>
               <source>J Infect Dis</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1662a1310">
            <label>34</label>
            <mixed-citation id="d92e1669" publication-type="journal">
Lambert JS, Harris DR, Stiehm ER, et al. Performance characteristics
of HIV-1 culture and HIV-1 DNA and RNA amplification assays for
early diagnosis of perinatal HIV-1 infection. J Acquir Immune Defic
Syndr2003;34:512-9.<person-group>
                  <string-name>
                     <surname>Lambert</surname>
                  </string-name>
               </person-group>
               <fpage>512</fpage>
               <volume>34</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1708a1310">
            <label>35</label>
            <mixed-citation id="d92e1715" publication-type="journal">
Nesheim S, Palumbo P, Sullivan K, et al. Quantitative RNA testing for
diagnosis of HIV-infected infants. J Acquir Immune Defic Syndr2003;
32:192-5.<person-group>
                  <string-name>
                     <surname>Nesheim</surname>
                  </string-name>
               </person-group>
               <fpage>192</fpage>
               <volume>32</volume>
               <source>J Acquir Immune Defic Syndr</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1750a1310">
            <label>36</label>
            <mixed-citation id="d92e1757" publication-type="journal">
WHO Collaborative Study Team on the Role of Breastfeeding on the
Prevention of Infant Mortality. Effect of breastfeeding on infant and
child mortality due to infectious diseases in less developed countries:
a pooled analysis. Lancet2000; 355:451-5.<person-group>
                  <string-name>
                     <surname>WHO Collaborative Study Team on the Role of Breastfeeding on the Prevention of Infant Mortality</surname>
                  </string-name>
               </person-group>
               <fpage>451</fpage>
               <volume>355</volume>
               <source>Lancet</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1795a1310">
            <label>37</label>
            <mixed-citation id="d92e1802" publication-type="journal">
Wei R, Msamanga GI, Spiegelman D, et al. Association between low
birth weight and infant mortality in children born to human immu-
nodeficiency virus 1-infected mothers in Tanzania. Pediatr Infect Dis
J2004; 23:530-5.<person-group>
                  <string-name>
                     <surname>Wei</surname>
                  </string-name>
               </person-group>
               <fpage>530</fpage>
               <volume>23</volume>
               <source>Pediatr Infect Dis J</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d92e1840a1310">
            <label>38</label>
            <mixed-citation id="d92e1847" publication-type="journal">
Thea DM, St Louis ME, Atido U, et al. A prospective study of diarrhea
and HIV-1 infection among 429 Zairian infants. N Engl J Med1993;
329:1696-702.<person-group>
                  <string-name>
                     <surname>Thea</surname>
                  </string-name>
               </person-group>
               <fpage>1696</fpage>
               <volume>329</volume>
               <source>N Engl J Med</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
