<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">afristudrevi</journal-id>
         <journal-id journal-id-type="jstor">j100049</journal-id>
         <journal-title-group>
            <journal-title>African Studies Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>African Studies Association</publisher-name>
         </publisher>
         <issn pub-type="ppub">00020206</issn>
         <issn pub-type="epub">15552462</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20065338</article-id>
         <title-group>
            <article-title>Is It Ethical to Study Africa? Preliminary Thoughts on Scholarship and Freedom</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Amina</given-names>
                  <surname>Mama</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>4</month>
            <year>2007</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">50</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i20065336</issue-id>
         <fpage>1</fpage>
         <lpage>26</lpage>
         <permissions>
            <copyright-statement>Copyright 2007 African Studies Association</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20065338"/>
         <abstract>
            <p>This article explores the manner in which ethical concerns have been addressed within Africa's progressive intellectual tradition through the eras of anticolonial, pan-African, and nationalist struggles for freedom, and into the era of globalization. Africa is characterized as the region bearing the most negative consequences of globalization, a reality that offers a critical vantage point well-attuned to the challenge of demystifying the global policy dictates currently dominating the global landscape. Ethical considerations are conceptualized as being framed by considerations of identity, epistemology, and methodology. It is suggested that Africa's radical intellectuals have effectively pursued anti-imperialist ethics, and developed regional and national intellectual communities of scholars who have worked for freedom, often challenging and subverting the constraints of dominant and received disciplinary approaches and paradigms. However, it is suggested that the liberatory promise of the anticolonial nationalist eras has not been fulfilled. While the fortunes of higher education and research in Africa have declined, scholars have established independent research networks in and beyond the campuses to keep African intellectual life alive. However, it is argued that Africa's intellectuals need to engage more proactively with the methodological implications of their own liberatory intellectual ethics. To do so requires that we address the intellectual challenges of Africa's complicated and contradictory location in the world and ensure that our unique vantage points inform methodological and pedagogical strategies that pursue freedom.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d736e116a1310">
            <mixed-citation id="d736e120" publication-type="other">
Ake, C. 1994. "Academic Freedom and Material Base." In Academic Freedom in Africa,
edited by M. Diouf and M. Mamdani, 17-25. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e130a1310">
            <mixed-citation id="d736e134" publication-type="other">
Anderson, B. 1983. Imagined Communities. London: Verso.</mixed-citation>
         </ref>
         <ref id="d736e141a1310">
            <mixed-citation id="d736e145" publication-type="other">
Appiah, K. A. 1992. In My Fathers House: Africa in the Philosophy of Culture. London:
Methuen.</mixed-citation>
         </ref>
         <ref id="d736e155a1310">
            <mixed-citation id="d736e159" publication-type="other">
—. 2005. The Ethics of Identity. Princeton: Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d736e167a1310">
            <mixed-citation id="d736e171" publication-type="other">
Bennett, Jane, ed. 2006. Killing a Virus with Stones? Research on the Implementation of
Policies against Sexual Harassment in Southern African Higher Education. Cape
Town: African Gender Institute.</mixed-citation>
         </ref>
         <ref id="d736e184a1310">
            <mixed-citation id="d736e188" publication-type="other">
Bourgois, P. 2006. "Foreword." In Engaged Observer: Anthropology, Advocacy and
Activism, edited by V Sanford and A. Angel-Ajani, ix-xii. New Brunswick, N.J.:
Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="d736e201a1310">
            <mixed-citation id="d736e205" publication-type="other">
Diagne, S. B. 2001. "Africanity as an Open Question." Discussion Paper 12, 19-24.
Uppsala: Nordikafrika Institutet.</mixed-citation>
         </ref>
         <ref id="d736e215a1310">
            <mixed-citation id="d736e219" publication-type="other">
Diouf, M., and M. Mamdani, eds. 1994. Academic Freedom in Africa. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e226a1310">
            <mixed-citation id="d736e230" publication-type="other">
Fanon, F. 1967. Black Skin, White Masks. London: Grove Press.</mixed-citation>
         </ref>
         <ref id="d736e237a1310">
            <mixed-citation id="d736e241" publication-type="other">
Federici, Silvia, George Caffentzis, and Ousseina Alidou. 2000. A Thousand Flowers:
Social Struggles against Structural Adjustment in African Universities. Trenton, N.J.:
Africa World Press.</mixed-citation>
         </ref>
         <ref id="d736e255a1310">
            <mixed-citation id="d736e259" publication-type="other">
Ferguson, J. 2006. Global Shadows: Africa in the Neoliberal World Order. Durham, N.C.:
Duke University Press.</mixed-citation>
         </ref>
         <ref id="d736e269a1310">
            <mixed-citation id="d736e273" publication-type="other">
Held, D., et al. 1999. Global Transofrmations; Politics, Economics and Culture. Stanford:
Stanford University Press.</mixed-citation>
         </ref>
         <ref id="d736e283a1310">
            <mixed-citation id="d736e287" publication-type="other">
Hountondji, P. 1983. African Philosophy: Myth and Reality. London: Hutchinson.</mixed-citation>
         </ref>
         <ref id="d736e294a1310">
            <mixed-citation id="d736e298" publication-type="other">
Hountondji, P, ed. 1997. Endogenous Knowledge: Research Trails. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e305a1310">
            <mixed-citation id="d736e309" publication-type="other">
—. 2002. The Struggle for Meaning: Reflections on Philosophy, Culture and Democ-
racy in Africa. Athens: Ohio University Press.</mixed-citation>
         </ref>
         <ref id="d736e319a1310">
            <mixed-citation id="d736e323" publication-type="other">
Imam, Ayesha, and Amina Mama. 1994. "The Role of Academics in the Restriction
of Academics in Africa." In Academic Freedom in Africa, edited by M. Diouf and
M. Mamdani, 73-107. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e337a1310">
            <mixed-citation id="d736e341" publication-type="other">
Ki-Zerbo, J. 2005 "African Intellectuals, Bationalism and Pan-Africanism: A Testi-
mony." In African Intellectuals: Rethinking language, Gender and Development,
edited by T. Mkandawire, 78-93. London: Zed Books.</mixed-citation>
         </ref>
         <ref id="d736e354a1310">
            <mixed-citation id="d736e358" publication-type="other">
Lewis, D. 2003. "African Women's Studies 1980-2001: A Review Essay." Prepared for
the African Gender Institute's "Strengthening Gender Studies for Social Trans-
formation: An Intellectual Capacity-Building and Information Technology
Development Project." Cape Town: African Gender Institute, University of
Cape Town, www.gwsafrica.org.</mixed-citation>
         </ref>
         <ref id="d736e377a1310">
            <mixed-citation id="d736e381" publication-type="other">
Mama, Amina. 1995. "Feminism or Femocracy: State Feminism and Democratisa-
tion in Nigeria." Africa Development 20 (1): 37-58.</mixed-citation>
         </ref>
         <ref id="d736e391a1310">
            <mixed-citation id="d736e395" publication-type="other">
—. 1996. Women's Studies and Studies of Women During the 1990s. Working Paper
Series 5/96. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e405a1310">
            <mixed-citation id="d736e409" publication-type="other">
—. 2001. "Challenging Subjects: Gender and Power in African Contexts." In
B. Diagne et al., Identity and Beyond: Rethinking Africanity. Discussion Paper 12.
Uppsala: Nordiska Africainstitutet.</mixed-citation>
         </ref>
         <ref id="d736e422a1310">
            <mixed-citation id="d736e426" publication-type="other">
—. 2003. "Restore, Reform but Do Not Transform: The Gender Politics of
Higher Education in Africa." Journal of Higher Education in Africa 1 (1): 101-25.</mixed-citation>
         </ref>
         <ref id="d736e437a1310">
            <mixed-citation id="d736e441" publication-type="other">
—. 2006. "Feminist Studies in African Contexts: The Challenge of Transfor-
mative Teaching in African Universities." In The Study of Africa: Disciplinary and
Interdisciplinary Encounters, edited by Paul Zeleza, 297-312. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e454a1310">
            <mixed-citation id="d736e458" publication-type="other">
Mkandawire, T. 1999. "Shifting Commitments and National Cohesion in African
Countries." In Common Security and Civil Society in Africa, edited by L. S.
Wohlegemuth et al. 14-41. Uppsala: Nordiska Africakainstitutet.</mixed-citation>
         </ref>
         <ref id="d736e471a1310">
            <mixed-citation id="d736e475" publication-type="other">
Mkandawire, T., ed. 2005. African Intellectuals: Rethinking Language, Gender and Devel-
opment. London: Zed Books.</mixed-citation>
         </ref>
         <ref id="d736e485a1310">
            <mixed-citation id="d736e489" publication-type="other">
Mkandawire, T., and C. Soludo. 1996. Our Continent Our Future: African Perspectives
on Structural Adjustment. Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e499a1310">
            <mixed-citation id="d736e503" publication-type="other">
Mudimbe, V. 1988. The Invention of Africa: Gnosis, Philosophy, and the Order of Knowl-
edge. London: James Currey.</mixed-citation>
         </ref>
         <ref id="d736e513a1310">
            <mixed-citation id="d736e517" publication-type="other">
Olagunju, Tunji, Adele Jinadu, and Sam Oyovbaire. 1993. The Transition to Democ-
racy in Nigeria (1985-1993). Ibadan: Spectrum Books.</mixed-citation>
         </ref>
         <ref id="d736e528a1310">
            <mixed-citation id="d736e532" publication-type="other">
Pereira, Charmaine. 2002. "Between Knowing and Imagining: What Space for Fem-
inism in Scholarship on Africa." Feminist Africa 1 (1).</mixed-citation>
         </ref>
         <ref id="d736e542a1310">
            <mixed-citation id="d736e546" publication-type="other">
Phiri, I. 2000. "Gender and Academic Freedom in Malawi." In Women in Academia:
Gender and Academic Freedom in Africa, edited by Ebrima Sail, 47-63. Dakar:
CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e559a1310">
            <mixed-citation id="d736e563" publication-type="other">
Sail, Ebrima. 2003. "The Social Sciences in Africa: Trends, Issues, Capacities and
Constraints." Paper prepared for the Human Capital Committee of the Social
Science Research Council, "Mapping Human Capital Globally." New York:
Social Science Research Council.</mixed-citation>
         </ref>
         <ref id="d736e579a1310">
            <mixed-citation id="d736e583" publication-type="other">
Sail, Ebrima, ed. 2000. Women in Academia: Gender and Academic Freedom in Africa.
Dakar: CODESRIA.</mixed-citation>
         </ref>
         <ref id="d736e593a1310">
            <mixed-citation id="d736e597" publication-type="other">
Samoff, Joel, and Bidemi Carrol. 2004. "The Promise of Partnership and Continu-
ities of Dependence: External Support to Higher Education in Africa." African
Studies Review 41 (1): 67-199.</mixed-citation>
         </ref>
         <ref id="d736e610a1310">
            <mixed-citation id="d736e614" publication-type="other">
Sawyerr, A. 2004. "Challenges Facing African Universities: Selected Issues." African
Studies Review 47 (1): 1-59.</mixed-citation>
         </ref>
         <ref id="d736e625a1310">
            <mixed-citation id="d736e629" publication-type="other">
Stiglitz, J. E. 2003. Globalization and Its Discontents. New York: W.W. Norton.</mixed-citation>
         </ref>
         <ref id="d736e636a1310">
            <mixed-citation id="d736e640" publication-type="other">
Third World Network (TWN)/CODESRIA. 2002. Declaration on African Development.
Accra: Third World Network.</mixed-citation>
         </ref>
         <ref id="d736e650a1310">
            <mixed-citation id="d736e654" publication-type="other">
wa Thiong'o, N. 1986. Decolonising the Mind: The Politics of Language in African Liter-
ature. Nairobi: Heinemann.</mixed-citation>
         </ref>
         <ref id="d736e664a1310">
            <mixed-citation id="d736e668" publication-type="other">
Zeleza, Tiyambe Paul. 2002. "African Universities and Globalization." Feminist Africa
1 (1): 64-85.</mixed-citation>
         </ref>
         <ref id="d736e678a1310">
            <mixed-citation id="d736e682" publication-type="other">
—. 2003. Rethinking Africa's Globalization. Volume 1: The Intellectual Challenges.
Trenton, N.J.: Africa World Press.</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>Notes</title>
         <ref id="d736e701a1310">
            <label>1</label>
            <mixed-citation id="d736e708" publication-type="other">
Zeleza 2003</mixed-citation>
            <mixed-citation id="d736e714" publication-type="other">
Held (1999)</mixed-citation>
            <mixed-citation id="d736e720" publication-type="other">
Stiglitz (2001)</mixed-citation>
            <mixed-citation id="d736e727" publication-type="other">
Ferguson (2006).</mixed-citation>
         </ref>
         <ref id="d736e734a1310">
            <label>2</label>
            <mixed-citation id="d736e741" publication-type="other">
Mkan-
dawire and Soludo (1996)</mixed-citation>
            <mixed-citation id="d736e750" publication-type="other">
Mkandawire (1999).</mixed-citation>
         </ref>
         <ref id="d736e757a1310">
            <label>3</label>
            <mixed-citation id="d736e764" publication-type="other">
Ngũgĩ wa Thiong'o (1986)</mixed-citation>
            <mixed-citation id="d736e770" publication-type="other">
Hountondji (1983)</mixed-citation>
            <mixed-citation id="d736e776" publication-type="other">
Mudimbe (1988)</mixed-citation>
            <mixed-citation id="d736e783" publication-type="other">
Appiah (1992)</mixed-citation>
         </ref>
         <ref id="d736e790a1310">
            <label>5</label>
            <mixed-citation id="d736e797" publication-type="other">
The Transition to Democracy in Nigeria by
Olagunju et al. (1993)</mixed-citation>
         </ref>
         <ref id="d736e808a1310">
            <label>6</label>
            <mixed-citation id="d736e815" publication-type="other">
Sawyerr (2004)</mixed-citation>
         </ref>
         <ref id="d736e822a1310">
            <label>9</label>
            <mixed-citation id="d736e829" publication-type="other">
Sail (2002)</mixed-citation>
         </ref>
         <ref id="d736e836a1310">
            <label>11</label>
            <mixed-citation id="d736e845" publication-type="other">
Urban Sexual Initiation Institutions in Uganda: A Case Study of the
Ssenga in Kampala, Uganda (Sylvia Tamale)</mixed-citation>
            <mixed-citation id="d736e854" publication-type="other">
The Witches of Gambaga: What It Means to Be a Witch in the Northern
Region of Ghana (Yaba Badoe)</mixed-citation>
            <mixed-citation id="d736e864" publication-type="other">
The Views of Young People in the Limpopo Province of South Africa con-
cerning Sexuality (Thelma Maluleke)
(A)sexualising the Youth in Modern Ghana? A Case Study of Virgin Clubs
(Akosua Darkwah and Alexina Arthur)</mixed-citation>
            <mixed-citation id="d736e879" publication-type="other">
The Sexual Politics of Zina under Shari'a in Northern Nigeria: The Strug-
gles surrounding Amina Lawal (Charmaine Pereira et al.)</mixed-citation>
            <mixed-citation id="d736e888" publication-type="other">
Sexualities of First Year Students at the University of the Cape Peninsula
(Elaine Salo et al.)</mixed-citation>
            <mixed-citation id="d736e897" publication-type="other">
The Gender and Institutional Culture Studies at the Universities of Addis
Ababa, Ibadan, Ghana, Chiekh Anta Diop, and Zimbabwe.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
