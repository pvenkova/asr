<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">humanbiology</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50003611</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Human Biology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wayne State University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00187143</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15346617</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41466615</article-id>
         <title-group>
            <article-title>Seasonality in Birth Weight: Review of Global Patterns and Potential Causes</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>GABRIEL</given-names>
                  <surname>CHODICK</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>SHIRA</given-names>
                  <surname>FLASH</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>YONIT</given-names>
                  <surname>DEOITCH</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>VARDA</given-names>
                  <surname>SHALEV</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">81</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40071382</issue-id>
         <fpage>463</fpage>
         <lpage>477</lpage>
         <permissions>
            <copyright-statement>Copyright © 2009 Wayne State University Press</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41466615"/>
         <abstract>
            <p>Birth weight is the single most significant determinant of infant mortality and the chances of a newborn to experience healthy development. Low birth weight also appears to be related to higher risks of several important chronic conditions, such as ischemie heart disease, non-insulindependent diabetes, and cancer in adults. Thus factors that influence in utero growth and birth weight may have a serious effect on health outcomes many years later in life. Analysis of seasonal variations in birth weights may enable us to suggest specific factors that influence this measure. In this review we summarize the literature on seasonal variations in birth weight. Although causes of seasonal variation in developing regions are more clearly understood, it is not yet clear which factors affect apparent seasonal variation in birth weight in developed countries. In our analysis we observed a pattern of seasonal variations in developed countries that differed between low-, middle-, and high-latitude countries, and we suggest several mechanisms that may be responsible for this diversity. Namely, we suggest that in middlelatitude climates, the large annual temperature range may cause low birth weights during summer, whereas in high-and low-latitude regions variations in sunlight exposure between seasons may contribute to low birth weights apparent during winter. Identification of the suggested causal environmental factors may have public health implications in the development of primary prevention programs for low birth weight and macrosomia in developed countries.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d858e168a1310">
            <mixed-citation id="d858e172" publication-type="other">
Adair, L., and E. Pollitt. 1983. Seasonal variation in pre-and postpartum maternal body measurements
and infant birth weights. Am. J. Phys. Anthropol. 62:325-331.</mixed-citation>
         </ref>
         <ref id="d858e182a1310">
            <mixed-citation id="d858e186" publication-type="other">
Akre, O., A. Ekbom, C. Hsieh et al. 1996. Testicular nonseminoma and seminoma in relation to peri-
natal characteristics. J. Natl. Cancer Inst. 88:883-889.</mixed-citation>
         </ref>
         <ref id="d858e196a1310">
            <mixed-citation id="d858e200" publication-type="other">
Bakwin, H., and R. Bakwin. 1929. Seasonal variation in weight loss of newborn. Am. J. Obst. Gynecol.
18:863-867.</mixed-citation>
         </ref>
         <ref id="d858e210a1310">
            <mixed-citation id="d858e214" publication-type="other">
Bakwin, H., and R. Bakwin. 1934. Body build in infants. Hum. Biol. 6:612-626.</mixed-citation>
         </ref>
         <ref id="d858e222a1310">
            <mixed-citation id="d858e226" publication-type="other">
Bantje, H. 1987. Seasonality of births and birth weights in Tanzania. Soc. Sei. Med. 24:733-739.</mixed-citation>
         </ref>
         <ref id="d858e233a1310">
            <mixed-citation id="d858e237" publication-type="other">
Bivings, L. 1934. Racial, geographic, annual, and seasonal variations in birth weight. Am. J. Phys.
Anthropol. 27:725-728.</mixed-citation>
         </ref>
         <ref id="d858e247a1310">
            <mixed-citation id="d858e251" publication-type="other">
Bondar, L., H. Simhan, R. Powers et al. 2007. High prevalence of vitamin D insufficiency in black and
white pregnant women residing in the northern United States and the neonates. J. Nutr. 137:447-
452.</mixed-citation>
         </ref>
         <ref id="d858e264a1310">
            <mixed-citation id="d858e268" publication-type="other">
Brauer, M., C. Lencar, L. Tamburic et al. 2008. A cohort study of traffic-related air pollution impacts
on birth outcomes. Environ. Health Perspect. 116:680-686.</mixed-citation>
         </ref>
         <ref id="d858e278a1310">
            <mixed-citation id="d858e282" publication-type="other">
Ceesay, S., A. Prentice, T. Cole et al. 1997. Effects on birth weight and perinatal mortality of ma-
ternal dietary supplements in rural Gambia: 5 year randomized controlled trial. Br. Med. J.
315:786-790.</mixed-citation>
         </ref>
         <ref id="d858e295a1310">
            <mixed-citation id="d858e299" publication-type="other">
Chodick, G., V. Shalev, I. Goren et al. 2007. Seasonality in birth weight in Israel: New evidence sug-
gests several global patterns and different etiologies. Ann. Epidemiol. 17:440-446.</mixed-citation>
         </ref>
         <ref id="d858e310a1310">
            <mixed-citation id="d858e314" publication-type="other">
de Castro, J. 1991. Seasonal rhythms of human nutrient intake and meal pattern. Physiol. Behav.
50:243-248.</mixed-citation>
         </ref>
         <ref id="d858e324a1310">
            <mixed-citation id="d858e328" publication-type="other">
DeLuca, H., and C. Zierold. 1998. Mechanisms and functions of vitamin D. Nutr. Rev. 56:S54-S75.</mixed-citation>
         </ref>
         <ref id="d858e335a1310">
            <mixed-citation id="d858e339" publication-type="other">
Doblhammer, G. 2004. The Late Life Legacy of Very Early Life. Berlin: Springer.</mixed-citation>
         </ref>
         <ref id="d858e346a1310">
            <mixed-citation id="d858e350" publication-type="other">
Elter, K., E. Ay, E. Uyar et al. 2004. Exposure to low outdoor temperature in the midtrimester is associ-
ated with low birth weight. Austral. NZJ. Obstet. Gynecol. 44:553-557.</mixed-citation>
         </ref>
         <ref id="d858e360a1310">
            <mixed-citation id="d858e364" publication-type="other">
Energy Information Administration. 2005. Trends in Air-Conditioning Usage from 1978 to 1997.
Washington, DC: Energy Information Administration.</mixed-citation>
         </ref>
         <ref id="d858e374a1310">
            <mixed-citation id="d858e378" publication-type="other">
Faber, H. 1920. A study of growth of infants in San Francisco with a new form weight chart. Arch.
Pediatr. 37:244-254.</mixed-citation>
         </ref>
         <ref id="d858e389a1310">
            <mixed-citation id="d858e393" publication-type="other">
Fallis, G., and J. Hilditch. 1989. A comparison of seasonal variation in birth weights between rural
Zaire and Ontario. Can. J. Public Health 80:205-208.</mixed-citation>
         </ref>
         <ref id="d858e403a1310">
            <mixed-citation id="d858e407" publication-type="other">
Forsen, T., J. Eriksson, J. Tuomilehto et al. 2000. The fetal and childhood growth of persons who
develop type 2 diabetes. Ann. Intern. Med. 133:176-182.</mixed-citation>
         </ref>
         <ref id="d858e417a1310">
            <mixed-citation id="d858e421" publication-type="other">
Frankel, S., P. Elwood, P. Sweetnam et al. 1996. Birth weight, body mass index in middle age, and
incident coronary heart disease. Lancet 348:1278-1280.</mixed-citation>
         </ref>
         <ref id="d858e431a1310">
            <mixed-citation id="d858e435" publication-type="other">
Gloria-Bottini, F., G. Meloni, A. Finocchi et al. 2000. Rh system and intrauterine growth: Interaction
with season of birth. Dis. Markers 16:139-142.</mixed-citation>
         </ref>
         <ref id="d858e445a1310">
            <mixed-citation id="d858e449" publication-type="other">
Godfrey, K., S. Robinson, D. Barker et al. 1996. Maternal nutrition in early and late pregnancy in rela-
tion to placental and fetal growth. Br. Med. J. 312:410-414.</mixed-citation>
         </ref>
         <ref id="d858e459a1310">
            <mixed-citation id="d858e463" publication-type="other">
Ha, E., Y. Hong, B. Lee et al. 2001. Is air pollution a risk factor for low birth weight in Seoul? Epide-
miology 12:643-648.</mixed-citation>
         </ref>
         <ref id="d858e474a1310">
            <mixed-citation id="d858e478" publication-type="other">
Jensen, G., and L. Moore. 1997. The effect of high altitude and other risk factors on birth weight:
Independent or interactive effects? Am. J. Public Health 87: 1003-1007.</mixed-citation>
         </ref>
         <ref id="d858e488a1310">
            <mixed-citation id="d858e492" publication-type="other">
Kasai, K. 1980. Meteorological Medicine. Tokyo: Kanehara.</mixed-citation>
         </ref>
         <ref id="d858e499a1310">
            <mixed-citation id="d858e503" publication-type="other">
Keatinge, W., S. Coleshaw, F. Cotter et al. 1984. Increases in platelet and red cell counts, blood viscos-
ity, and arterial pressure during mild surface cooling: Factors in mortality from coronary and
cerebral thrombosis in winter. Br. Med. J. 289:1405-1408.</mixed-citation>
         </ref>
         <ref id="d858e516a1310">
            <mixed-citation id="d858e520" publication-type="other">
Kinabo, J. 1993. Seasonal variation of birth weight distribution in Morogoro, Tanzania. East Afr. Med.
J. 70: 152-155.</mixed-citation>
         </ref>
         <ref id="d858e530a1310">
            <mixed-citation id="d858e534" publication-type="other">
Koscinski, K., M. Krenz-Niedbala, and A. Kozlowska-Rajewicz. 2004. Month-of-birth effect on
height and weight in Polish rural children. Am. J. Hum. Biol. 16:31-42.</mixed-citation>
         </ref>
         <ref id="d858e544a1310">
            <mixed-citation id="d858e548" publication-type="other">
Kramer, M. 1987. Determinants of low birth weight: Methodological assessment and meta-analysis.
Bull. WHO 65:663-737.</mixed-citation>
         </ref>
         <ref id="d858e559a1310">
            <mixed-citation id="d858e563" publication-type="other">
Lawlor, D., D. Leon, and G. Davey Smith. 2005. The association of ambient outdoor temperature
throughout pregnancy and offspring birth weight: Findings from the Aberdeen children of the
1950s cohort. Br. J. Obstet. Gynecol. 112:647-657.</mixed-citation>
         </ref>
         <ref id="d858e576a1310">
            <mixed-citation id="d858e580" publication-type="other">
Leon, D., H. Lithell, D. Vagero et al. 1998. Reduced fetal growth rate and increased risk of death from
ischemic heart disease: Cohort study of 15,000 Swedish men and women born 1915-19. Br.
Med. J. 317:241-245.</mixed-citation>
         </ref>
         <ref id="d858e593a1310">
            <mixed-citation id="d858e597" publication-type="other">
MacLennan, W., J. Hamilton, and J. Darmady. 1980. The effects of season and stage of pregnancy on
plasma 25-hydroxyvitamin D concentrations in pregnant women. Postgrad. Med. J. 56:75-79.</mixed-citation>
         </ref>
         <ref id="d858e607a1310">
            <mixed-citation id="d858e611" publication-type="other">
Mathews, F., P. Yudkin, and A. Neil. 1999. Influence of maternal nutrition on outcome of pregnancy:
Prospective cohort study. Br. Med. J. 319:339-343.</mixed-citation>
         </ref>
         <ref id="d858e621a1310">
            <mixed-citation id="d858e625" publication-type="other">
Matsuda, S., Y. Hiroshige, M. Furuta et al. 1995. Geographic differences in seasonal variation of mean
birth weight in Japan. Hum. Biol. 67:641-656.</mixed-citation>
         </ref>
         <ref id="d858e635a1310">
            <mixed-citation id="d858e639" publication-type="other">
Matsuda, S., T. Sone, T. Doi et al. 1993. Seasonality of mean birth weight and mean gestational period
in Japan. Hum. Biol. 65:481-501.</mixed-citation>
         </ref>
         <ref id="d858e650a1310">
            <mixed-citation id="d858e654" publication-type="other">
McCormack, V., I. dos Santos Silva, I. Koupil et al. 2005. Birth characteristics and adult cancer inci-
dence: Swedish cohort of over 11,000 men and women. Int. J. Cancer 115:611-617.</mixed-citation>
         </ref>
         <ref id="d858e664a1310">
            <mixed-citation id="d858e668" publication-type="other">
McGrath, J., A. Barnett, D. Eyles et al. 2007. The impact of nonlinear exposure-risk relationships on
seasonal time-series data: Modeling Danish neonatal birth anthropometric data. BMC Med.
Res. Methodol. 7:45-54.</mixed-citation>
         </ref>
         <ref id="d858e681a1310">
            <mixed-citation id="d858e685" publication-type="other">
McGrath, J., D. Keeping, S. Saha et al. 2005. Seasonal fluctuations in birth weight and neonatal limb
length: Does prenatal vitamin D influence neonatal size and shape? Early Hum. Dev. 81 :609-618.</mixed-citation>
         </ref>
         <ref id="d858e695a1310">
            <mixed-citation id="d858e699" publication-type="other">
Mercer, J. 2003. Cold, an underrated risk factor for health. Environ. Res. 92:8-13.</mixed-citation>
         </ref>
         <ref id="d858e706a1310">
            <mixed-citation id="d858e710" publication-type="other">
Michels, K., D. Trichopoulos, J. Robins et al. 1996. Birth weight as a risk factor for breast cancer.
Lancet 348:1542-1546.</mixed-citation>
         </ref>
         <ref id="d858e720a1310">
            <mixed-citation id="d858e724" publication-type="other">
Moller, H., and N. Skakkebaek. 1997. Testicular cancer and cryptorchidism in relation to prenatal fac-
tors: Case-control studies in Denmark. Cancer Causes Control 8:904-912.</mixed-citation>
         </ref>
         <ref id="d858e735a1310">
            <mixed-citation id="d858e739" publication-type="other">
Moore, L., S. Rounds, D. Jahnigen et al. 1982. Infant birth weight is related to maternal arterial oxy-
genation at high altitude. J. Appl. Physiol. 52:695-699.</mixed-citation>
         </ref>
         <ref id="d858e749a1310">
            <mixed-citation id="d858e753" publication-type="other">
Murray, L., D. O'Reilly, N. Betts et al. 2000. Season and outdoor ambient temperature: Effects on birth
weight. Obstet. Gynecol. 96:689-695.</mixed-citation>
         </ref>
         <ref id="d858e763a1310">
            <mixed-citation id="d858e767" publication-type="other">
National Center for Environmental Assessment. 1999. Air Quality Criteria for Carbon Monoxide.
Research Triangle Park, NC: United States Environmental Protection Agency.</mixed-citation>
         </ref>
         <ref id="d858e777a1310">
            <mixed-citation id="d858e781" publication-type="other">
Naughton, M., A. Henderson, M. Mirabelli et al. 2002. Heat-related mortality during a 1999 heat wave
in Chicago. Am. J. Prev. Med. 22:221-227.</mixed-citation>
         </ref>
         <ref id="d858e791a1310">
            <mixed-citation id="d858e795" publication-type="other">
Neild, P., D. Syndercombe-Court, W. Keatinge et al. 1994. Cold-induced increases in erythrocyte
count, plasma cholesterol, and plasma fibrinogen of elderly people without a comparable rise
in protein C or factor X. Clin. Sei. 86:43-48.</mixed-citation>
         </ref>
         <ref id="d858e808a1310">
            <mixed-citation id="d858e812" publication-type="other">
Olliaro, P. L., and P. B. Bioland. 2001. Clinical and public health implications of antimalarial drug
resistance. In Antimalarial Chemotherapy: Mechanisms of Action, Resistance, and New Direc-
tions in Drug Discovery, P. J. Rosenthal, ed. Totowa, NJ: Humana Press, 65-83.</mixed-citation>
         </ref>
         <ref id="d858e826a1310">
            <mixed-citation id="d858e830" publication-type="other">
Phillips, D., and J. Young. 2000. Birth weight, climate at birth, and the risk of obesity in adult life. Int.
J. Obes. Relat. Metab. Disord. 24:281-287.</mixed-citation>
         </ref>
         <ref id="d858e840a1310">
            <mixed-citation id="d858e844" publication-type="other">
Prentice, A., T. Cole, F. Foord et al. 1987. Increased birth weight after prenatal dietary supplementa-
tion of rural African women. Am. J. Clin. Nutr. 46:912-925.</mixed-citation>
         </ref>
         <ref id="d858e854a1310">
            <mixed-citation id="d858e858" publication-type="other">
Ramaiah, T., and V. Harasimham. 1967. Birth weight as a measure of prematurity and its relationship
with certain maternal factors. Indian J. Med. Res. 55:513-524.</mixed-citation>
         </ref>
         <ref id="d858e868a1310">
            <mixed-citation id="d858e872" publication-type="other">
Rantakallio, P. 1971. The effect of a northern climate on seasonality of births and the outcome of
pregnancies. Acta Paediatr. Scand. Suppl. 218:1-67.</mixed-citation>
         </ref>
         <ref id="d858e882a1310">
            <mixed-citation id="d858e886" publication-type="other">
Rayco-Solon, P., A. Fulford, and A. Prentice. 2005. Differential effects of seasonality on preterm birth
and intrauterine growth restriction in rural Africans. Am. J. Clin. Nutr. 81:134-139.</mixed-citation>
         </ref>
         <ref id="d858e896a1310">
            <mixed-citation id="d858e900" publication-type="other">
Ritz, B., F. Yu, S. Fruin et al. 2002. Ambient air pollution and risk of birth defects in Southern Califor-
nia. Am. J. Epidemiol. 155:17-24.</mixed-citation>
         </ref>
         <ref id="d858e911a1310">
            <mixed-citation id="d858e915" publication-type="other">
Rousham, E., and M. Gracey. 1998. Differences in growth among remote and town-dwelling ab-
original children in the Kimberley region of western Australia. Austral. NZ J. Public Health
22:690-694.</mixed-citation>
         </ref>
         <ref id="d858e928a1310">
            <mixed-citation id="d858e932" publication-type="other">
Sarkar, D. 1968. Birth weight in a hospital sample from South India. Indian J. Pediatr. 35:266-275.</mixed-citation>
         </ref>
         <ref id="d858e939a1310">
            <mixed-citation id="d858e943" publication-type="other">
Selvin, S., and D. Janerich. 1971. Four factors influencing birth weight. Br. J. Prev. Soc. Med. 25:
12-16.</mixed-citation>
         </ref>
         <ref id="d858e953a1310">
            <mixed-citation id="d858e957" publication-type="other">
Shaheen, R., A. de Francisco, S. El Arifeen et al. 2006. Effect of prenatal food supplementation on
birth weight: An observational study from Bangladesh. Am. J. Clin. Nutr. 83:1355-1361.</mixed-citation>
         </ref>
         <ref id="d858e967a1310">
            <mixed-citation id="d858e971" publication-type="other">
Shephard, R., H. Lavallee, J. Jequier et al. 1979. Season of birth and variations in stature, body mass,
and performance. Hum. Biol. 51:299-316.</mixed-citation>
         </ref>
         <ref id="d858e981a1310">
            <mixed-citation id="d858e985" publication-type="other">
Spencer, N. 2003. Weighing the Evidence: How Is Birth Weight Determined? Oxon, U.K.: Radcliffe.</mixed-citation>
         </ref>
         <ref id="d858e993a1310">
            <mixed-citation id="d858e997" publication-type="other">
Stekette, R., C. Stephen, J. Wirima et al. 1996. The effect of malaria and malaria prevention in preg-
nancy on offspring birth weight, prematurity, and intrauterine growth retardation in rural
Malawi. Am. J. Trop. Med. Hye. 55:33-41.</mixed-citation>
         </ref>
         <ref id="d858e1010a1310">
            <mixed-citation id="d858e1014" publication-type="other">
Tanaka, H., M. Sei, T. Quan Binh et al. 2007. Correlation of month and season of birth with height,
weight, and degree of obesity of rural Japanese children. J. Med. Invest. 54:133-139.</mixed-citation>
         </ref>
         <ref id="d858e1024a1310">
            <mixed-citation id="d858e1028" publication-type="other">
Tibblin, G., M. Eriksson, S. Cnattingius et al. 1995. High birth weight as a predictor of prostate cancer
risk. Epidemiology 6:423-424.</mixed-citation>
         </ref>
         <ref id="d858e1038a1310">
            <mixed-citation id="d858e1042" publication-type="other">
Tustin, K., J. Gross, and H. Hayne. 2004. Maternal exposure to first-trimester sun is associated with
increased birth weight in human infants. Dev. Psychobiol. 45:221-230.</mixed-citation>
         </ref>
         <ref id="d858e1052a1310">
            <mixed-citation id="d858e1056" publication-type="other">
Valcavi, R., M. Zini, G. Maestroni et al. 1993. Melatonin stimulates growth hormone secretion
through pathways other than the growth hormone-releasing hormone. Clin. Endocrinol.
39:193-199.</mixed-citation>
         </ref>
         <ref id="d858e1069a1310">
            <mixed-citation id="d858e1073" publication-type="other">
van Hanswijck de Jonge, L., G. Waller, and N. Stettler. 2003. Ethnicity modifies seasonal variations in
birth weight and weight gain of infants. J. Nutr. 133:1415-1418.</mixed-citation>
         </ref>
         <ref id="d858e1084a1310">
            <mixed-citation id="d858e1088" publication-type="other">
Vorherr, H. 1982. Factors influencing fetal growth. Am. J. Obstet. Gynecol. 142:577-588.</mixed-citation>
         </ref>
         <ref id="d858e1095a1310">
            <mixed-citation id="d858e1099" publication-type="other">
Waldie, K., R. Poulton, I. Kirk et al. 2000. The effects of pre-and post-natal sunlight exposure on
human growth: Evidence from the Southern Hemisphere. Early Hum. Dev. 60:35-42.</mixed-citation>
         </ref>
         <ref id="d858e1109a1310">
            <mixed-citation id="d858e1113" publication-type="other">
Weber, G., H. Prossinger, and H. Seidler. 1998. Height depends on month of birth. Nature 391:754-
755.</mixed-citation>
         </ref>
         <ref id="d858e1123a1310">
            <mixed-citation id="d858e1127" publication-type="other">
Wilcox, A. 2001. On the importance—and the unimportance—of birth weight. Int. J. Epidemiol.
30:1233-1241.</mixed-citation>
         </ref>
         <ref id="d858e1137a1310">
            <mixed-citation id="d858e1141" publication-type="other">
World Meteorological Organization. 2009. World Weather Information Services, www.worldweather
.org/. Accessed April 19, 2009.</mixed-citation>
         </ref>
         <ref id="d858e1151a1310">
            <mixed-citation id="d858e1155" publication-type="other">
Yajnik, C. 2002. The life cycle effects of nutrition and body size on adult adiposity, diabetes, and
cardiovascular disease. Obes. Rev. 3:217-224.</mixed-citation>
         </ref>
         <ref id="d858e1166a1310">
            <mixed-citation id="d858e1170" publication-type="other">
Yetley, E. 2008. Assessing the vitamin D status of the U.S. population. Am. J. Clin. Nutr. 88:558S-
564S.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
