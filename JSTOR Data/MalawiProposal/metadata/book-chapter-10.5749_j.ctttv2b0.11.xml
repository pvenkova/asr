<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctttv2b0</book-id>
      <subj-group>
         <subject content-type="call-number">GV776.05 .B74 2000</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Kayak touring</subject>
         <subj-group>
            <subject content-type="lcsh">Superior, Lake</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Superior, Lake</subject>
         <subj-group>
            <subject content-type="lcsh">Description and travel</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Language &amp; Literature</subject>
      </subj-group>
      <book-title-group>
         <book-title>Wild Shore</book-title>
         <subtitle>Exploring Lake Superior by Kayak</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Breining</surname>
               <given-names>Greg</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>27</day>
         <month>02</month>
         <year>2002</year>
      </pub-date>
      <isbn content-type="ppub">9780816631414</isbn>
      <isbn content-type="ppub">0816631417</isbn>
      <isbn content-type="epub">9780816689101</isbn>
      <isbn content-type="epub">0816689105</isbn>
      <publisher>
         <publisher-name>University of Minnesota Press</publisher-name>
         <publisher-loc>Minneapolis; London</publisher-loc>
      </publisher>
      <edition>NED - New edition</edition>
      <permissions>
         <copyright-year>2000</copyright-year>
         <copyright-holder>Greg Breining</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.5749/j.ctttv2b0"/>
      <abstract abstract-type="short">
         <p>Over two years, sportsman, writer, and world traveler Greg Breining set out to circle Lake Superior by kayak, a means of travel that allowed him to visit the lake’s places of rare beauty and solitude, experience its wildly varied moods, and see its remote historic sites and isolated communities. Wild Shore is a tale of outdoor adventure, odd characters, humorous stories, and quiet reflection.</p>
      </abstract>
      <counts>
         <page-count count="280"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.4</book-part-id>
                  <title-group>
                     <title>The Thin Blue Line</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>A stone sailed out in a puny arc and plunked silently into the water, the sound failing to rise above the whoosh and hiss of wind and surf. The horizon mocked the meager distance of the boy’s throw. It was not like throwing a stone across a pond, or even into a lake, where the flight might suggest some fraction of the distance to the far shore. No, the thin blue line spelled infinity, and the arc of the rock represented no meaningful fraction at all—only a vague wish to reach infinitely far.</p>
                     <p>It was fall on the Minnesota</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.5</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>East Shore:</title>
                     <subtitle>Sault Sainte Marie to Old Woman Bay</subtitle>
                  </title-group>
                  <fpage>5</fpage>
                  <abstract>
                     <p>Goulais Bay formed a beautiful funnel. To the south, Gros Cap loomed massive and purple above Superior. To the north, hills rose lush and fertile, as though we paddled in the humid tropics. Between these headlands, from the west-southwest, poured a howling wind. To stand in it would be hard enough. To paddle into it sucked the life right out of you.</p>
                     <p>Susan had never kayaked on big water before. I worried not so much for her safety as her spirit. We had just set out on the lake, only one hour into a ten-day trip, and here we were,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>Pukaskwa Peninsula:</title>
                     <subtitle>Michipicoten River to Hattie Cove</subtitle>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>As i hitchhiked back to our car at Goulais River, the first vehicle to give me a lift was a yellow pickup from the Ontario Ministry of Transportation. That seemed appropriate. The second was a Volvo that had passed me making vague grinding noises, but then doubled back two minutes later, turned around in front of me, and stopped.</p>
                     <p>“It’s tough to get a good look at people at highway speeds,” said the old man hunched over the wheel. “My wife said, ‘It’s an elderly gentleman. Maybe he’s in trouble.’ You have her to thank for getting a ride.”</p>
                     <p>The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>Coldwell Peninsula:</title>
                     <subtitle>Little Pic River to Port Coldwell</subtitle>
                  </title-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>From the steep bank along Jellicoe Cove, look out over Peninsula Harbour to the end of a club-shaped point, where Peninsula Hill rises five hundred feet above the lake. Far off in the other direction, the gnarly arm of Ypres Point cradles Hawkins, Skin, and Blondin Islands. The view is wooded, rocky, primeval. On a clear day it may extend twenty miles or more. Surely it occupies the pantheon of Superior’s grand views. Or it would. Drop your eyes from the far distance of the horizon, through the middle distance of Hawkins Island, and turn your attention to the beach</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>Slate Islands</title>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Of the many animals that symbolize the Superior country and wilderness—the loon, the gray wolf, the moose—one stands out for its rarity and utter failure to survive where civilization has encroached. The woodland caribou, with its short legs, long back, magnificent rack, and Frisbee feet, is an emblem of the time when the Ojibwa and Cree ruled the forest primeval.</p>
                     <p>Two hundred years ago, as journals of early explorers make clear, woodland caribou roamed the shores of Lake Superior and its major islands. It is a pair of woodland caribou, after all, that run across the face of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 5</label>
                     <title>Nipigon Islands:</title>
                     <subtitle>Rossport to Pigeon River</subtitle>
                  </title-group>
                  <fpage>87</fpage>
                  <abstract>
                     <p>The annals of Lake Superior shipping are filled with tales that illuminate the valiant side of the human character: Captain William E. Morse evacuated his crew but refused a lifeline himself with the words “Not by a damn sight” before the foundering<italic>William F. Sauber</italic>exploded and sank near Whitefish Bay. Seaman Fred Benson leaped from the deck of the grounded<italic>Madeira</italic>, scaled Gold Rock Point, and dropped a line by which eight of the nine remaining crew climbed to safety.</p>
                     <p>Other disasters expose our vices and foibles. Such is the tale of the<italic>Gunilda</italic>, a testament—at least as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.10</book-part-id>
                  <title-group>
                     <label>CHAPTER 6</label>
                     <title>Minnesota:</title>
                     <subtitle>Pigeon River to Duluth</subtitle>
                  </title-group>
                  <fpage>123</fpage>
                  <abstract>
                     <p>That night i crawled into my sleeping bag and saw water rising around me.<italic>Oh God, don’t let me dream about drowning</italic>. But the next time I opened my eyes, I heard the voice of a white-throated sparrow.</p>
                     <p>I pushed off early and soon crossed over into U.S. waters and cleared Pigeon Point. I glided among the Susie Islands, the last islands of significant size or number that I would encounter along the North Shore. The Susies were the end of one shore and the beginning of another: the archipelago of the Canadian shore gave way to the straight, singular</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.11</book-part-id>
                  <title-group>
                     <label>CHAPTER 7</label>
                     <title>Isle Royale</title>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>In the mid-1700s, two large islands occupied the western end of Lake Superior. At least they did on the maps of Jacques Nicolas Bellin, a prominent French cartographer of the time. Bellin labeled the one island Isle Royale. It was long and narrow and canted southwest to northeast.</p>
                     <p>Bellin called the second island Isle Philippeaux. He also used the Ojibwa name, Isle Minong—”place of blueberries” or “a nice place to be.” Isle Philippeaux was similar in size and alignment to the other island, but where Royale had a lumpish, approximated shape, Philippeaux showed the very details seen on present-day</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.12</book-part-id>
                  <title-group>
                     <label>CHAPTER 8</label>
                     <title>Apostle Islands:</title>
                     <subtitle>Brule Point to La Pointe</subtitle>
                  </title-group>
                  <fpage>179</fpage>
                  <abstract>
                     <p>I decided to skip Duluth and Superior. Sea kayaks aren’t much good for touring cities. Except for the rusted hulls of ships in the harbor, what could I see that I couldn’t see more easily by car? Instead, I decided to pick up my trip somewhere to the east of Superior, safely outside the shipping lanes, along the red clay banks and sand beaches that stretched past the Brule River to the beginning of the Apostle Islands. It was a landscape I wanted to see, but quickly. Except for three small towns, I didn’t anticipate much of interest. So one</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.13</book-part-id>
                  <title-group>
                     <label>CHAPTER 9</label>
                     <title>South Shore:</title>
                     <subtitle>Long Island to Houghton</subtitle>
                  </title-group>
                  <fpage>195</fpage>
                  <abstract>
                     <p>I emerged from my tent to a kind of stunning solitude, surprised to awaken on the same desolate beach where I had fallen asleep. The sun rose red and hazy, and the air was thick.</p>
                     <p>That morning, I wanted to fish in Chequamegon Bay for smallmouth bass. But how was I to get into the bay? I had already paddled several miles down Long Island—too far to want to double back to the shipping channel. So after breakfast, I continued east down the long point, separated from the bay by only the thin strip of sand and a sorry</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.14</book-part-id>
                  <title-group>
                     <label>CHAPTER 10</label>
                     <title>Keweenaw Peninsula:</title>
                     <subtitle>Houghton to Marquette</subtitle>
                  </title-group>
                  <fpage>217</fpage>
                  <abstract>
                     <p>Houghton was abundantly industrial. But that is the first impression of any city you first see from a working waterway. Minneapolis, Manhattan, Houghton—from the water, they all look like the backside of a factory.</p>
                     <p>Once we loaded the boats and drove around, I saw a town that was historic and picturesque, its downtown streets filled with tourists. Many of the old buildings along the waterfront had been reclaimed. On the hill above downtown stood the Houghton County Courthouse, built in 1887. It had a copper roof and sandstone trim, reflecting, according to a sign, “opulent high Victorian design.” The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.15</book-part-id>
                  <title-group>
                     <label>CHAPTER 11</label>
                     <title>Pictured Rocks:</title>
                     <subtitle>Grand Island to Brimley State Park</subtitle>
                  </title-group>
                  <fpage>241</fpage>
                  <abstract>
                     <p>A north wind howled off Superior. Whitecaps filled the lake all the way to Canada. Rank upon rank of six-foot waves rose, broke, and crashed upon the sand.</p>
                     <p>Wind bound again.</p>
                     <p>Still, the Little Beaver Lake campground was not a bad place to be stuck. There were plenty of vacant sites, and I had two weeks of food. I could hike to the beach on a trail that wound beneath old hemlock and mammoth beeches, and even follow the crest of the Pictured Rocks, the brightly colored sandstone cliffs, some nearly two hundred feet high, that flanked the lake. I’d</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.16</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>259</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctttv2b0.17</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>265</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
