<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">evolution</journal-id>
         <journal-id journal-id-type="jstor">j100004</journal-id>
         <journal-title-group>
            <journal-title>Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society for the Study of Evolution</publisher-name>
         </publisher>
         <issn pub-type="ppub">00143820</issn>
         <issn pub-type="epub">15585646</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="doi">10.2307/2411050</article-id>
         <title-group>
            <article-title>Butterfly Eyespots: The Genetics and Development of the Color Rings</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Antónia</given-names>
                  <surname>Monteiro</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Paul M.</given-names>
                  <surname>Brakefield</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>Vernon</given-names>
                  <surname>French</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>1997</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i200248</issue-id>
         <fpage>1207</fpage>
         <lpage>1216</lpage>
         <page-range>1207-1216</page-range>
         <permissions>
            <copyright-statement>Copyright 1997 The Society for the Study of Evolution</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/2411050"/>
         <abstract>
            <p>The butterfly Bicyclus anynana has a series of distal eyespots on its wings. Each eyespot is composed of a white pupil, a black disc, and a gold outer ring. We applied artificial selection to the large dorsal eyespot on the forewing to produce a line with the gold ring reduced or absent (BLACK) and another line with a reduced black disc and a broad gold ring (GOLD). High heritabilities, coupled with a rapid response to selection, produced two lines of butterflies with very different phenotypes. Other eyespots showed a correlated change in the proportion of their color rings. Surgical experiments were performed on pupal wings from the different lines at the time of eyespot pattern specification. They showed that the additive genetic variance for this trait was in the response of the wing epidermis to signaling from the organizing cells at the eyespot center (the focus). This response was found to vary across different regions of the wing and also between the sexes. The particular eyespot color composition found for each sex, as well as the maintenance of the high genetic variation, are discussed with reference to the ecology of the butterfly, sexual selection, and visual selection by predators.</p>
         </abstract>
         <kwd-group>
            <kwd>Bicyclus anynana</kwd>
            <kwd>Butterfly</kwd>
            <kwd>Color Pattern</kwd>
            <kwd>Correlated Responses</kwd>
            <kwd>Development</kwd>
            <kwd>Eyespot</kwd>
            <kwd>Gradient</kwd>
            <kwd>Pattern Formation</kwd>
            <kwd>Selection</kwd>
            <kwd>Thresholds</kwd>
            <kwd>Wing Pattern</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d204e233a1310">
            <mixed-citation id="d204e237" publication-type="journal">
BRAKEFIELD, P. M., AND V. FRENCH. 1993. Butterfly wing patterns:
developmental mechanisms and evolutionary change. Acta
Biother.41:447-468.<person-group>
                  <string-name>
                     <surname>Brakefield</surname>
                  </string-name>
               </person-group>
               <fpage>447</fpage>
               <volume>41</volume>
               <source>Acta Biother.</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d204e272a1310">
            <mixed-citation id="d204e276" publication-type="journal">
—.1995. Eyespot development on butterfly wings: The epi-
dermal response to damage. Dev. Biol.168:98-111.<person-group>
                  <string-name>
                     <surname>Brakefield</surname>
                  </string-name>
               </person-group>
               <fpage>98</fpage>
               <volume>168</volume>
               <source>Dev. Biol.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e308a1310">
            <mixed-citation id="d204e312" publication-type="journal">
BRAKEFIELD, P. M., AND T. B. LARSEN. 1984. The evolutionary
significance of dry and wet season forms in some tropical but-
terflies. Biol. J. Linn. Soc.22:1-12.<person-group>
                  <string-name>
                     <surname>Brakefield</surname>
                  </string-name>
               </person-group>
               <fpage>1</fpage>
               <volume>22</volume>
               <source>Biol. J. Linn. Soc.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d204e347a1310">
            <mixed-citation id="d204e351" publication-type="journal">
BRAKEFIELD, P. M., AND N. REITSMA. 1991. Phenotypic plasticity,
seasonal climate and the population biology of Bicyclus butter-
flies in Malawi. Ecol. Entomol.10:291-303.<person-group>
                  <string-name>
                     <surname>Brakefield</surname>
                  </string-name>
               </person-group>
               <fpage>291</fpage>
               <volume>10</volume>
               <source>Ecol. Entomol.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d204e387a1310">
            <mixed-citation id="d204e391" publication-type="journal">
CHEVERUD, J. M. 1984. Quantitative genetics and developmental
constraints on evolution by selection. J. Theor. Biol.110:155-
171.<person-group>
                  <string-name>
                     <surname>Cheverud</surname>
                  </string-name>
               </person-group>
               <fpage>155</fpage>
               <volume>110</volume>
               <source>J. Theor. Biol.</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d204e426a1310">
            <mixed-citation id="d204e430" publication-type="book">
CONDAMIN, M. 1973. Monographie du genre Bicyclus (Lepidoptera
Satyridae). Memoires de l'Institut Fondamental d'Afrique Noire.
IFAN, Dakar.<person-group>
                  <string-name>
                     <surname>Condamin</surname>
                  </string-name>
               </person-group>
               <source>Monographie du genre Bicyclus (Lepidoptera Satyridae)</source>
               <year>1973</year>
            </mixed-citation>
         </ref>
         <ref id="d204e459a1310">
            <mixed-citation id="d204e463" publication-type="book">
FALCONER, D. S. 1989. Introduction to quantitative genetics. 3d
ed. Longman, London.<person-group>
                  <string-name>
                     <surname>Falconer</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>Introduction to quantitative genetics</source>
               <year>1989</year>
            </mixed-citation>
         </ref>
         <ref id="d204e492a1310">
            <mixed-citation id="d204e496" publication-type="journal">
FRENCH, V., AND P. M. BRAKEFIELD. 1992. The development of
eyespot patterns on butterfly wings: morphogen sources or sinks?
Development116:103-109.<person-group>
                  <string-name>
                     <surname>French</surname>
                  </string-name>
               </person-group>
               <fpage>103</fpage>
               <volume>116</volume>
               <source>Development</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d204e531a1310">
            <mixed-citation id="d204e535" publication-type="journal">
—.1995. Eyespot development on butterfly wings: The focal
signal. Dev. Biol.168:112-123.<person-group>
                  <string-name>
                     <surname>French</surname>
                  </string-name>
               </person-group>
               <fpage>112</fpage>
               <volume>168</volume>
               <source>Dev. Biol.</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e567a1310">
            <mixed-citation id="d204e571" publication-type="journal">
HOLLOWAY, G. J., P. M. BRAKEFIELD, AND S. KOFMAN. 1993. The
genetics of wing pattern elements in the polyphenic butterfly,
Bicyclus anynana. Heredity70:1179-186.<person-group>
                  <string-name>
                     <surname>Holloway</surname>
                  </string-name>
               </person-group>
               <fpage>1179</fpage>
               <volume>70</volume>
               <source>Heredity</source>
               <year>1993</year>
            </mixed-citation>
         </ref>
         <ref id="d204e607a1310">
            <mixed-citation id="d204e611" publication-type="journal">
MAYNARD SMITH, J., R. BURIAN, S. KAUFFMAN, P. ALBERCH, J.
CAMPBELL, B. GOODWIN, R LANDE, D. RAUP AND L. WOLPERT.
1985. Developmental constraints and evolution. Q. Rev. Biol.
60:265-287.<object-id pub-id-type="jstor">10.2307/2828504</object-id>
               <fpage>265</fpage>
            </mixed-citation>
         </ref>
         <ref id="d204e634a1310">
            <mixed-citation id="d204e638" publication-type="journal">
MONTEIRO, A. F, P. M. BRAKEFIELD, AND V. FRENCH. 1994. The
evolutionary genetics and developmental basis of wing pattern
variation in the butterfly Bicyclus anynana. Evolution48:1147-
1157.<object-id pub-id-type="doi">10.2307/2410374</object-id>
               <fpage>1147</fpage>
            </mixed-citation>
         </ref>
         <ref id="d204e661a1310">
            <mixed-citation id="d204e665" publication-type="journal">
—.1997. The genetics and development of an eyespot pattern
in the butterfly Bicyclus anynana: response to selection for eye-
spot shape. Genetics146:287-294.<person-group>
                  <string-name>
                     <surname>Monteiro</surname>
                  </string-name>
               </person-group>
               <fpage>287</fpage>
               <volume>146</volume>
               <source>Genetics</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d204e700a1310">
            <mixed-citation id="d204e704" publication-type="journal">
NIJHOUT, H. F 1978. Wing pattern formation in Lepidoptera: a
model. J. Exp. Zool.206:119-136.<person-group>
                  <string-name>
                     <surname>Nijhout</surname>
                  </string-name>
               </person-group>
               <fpage>119</fpage>
               <volume>206</volume>
               <source>J. Exp. Zool.</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d204e736a1310">
            <mixed-citation id="d204e740" publication-type="journal">
—.1980. Pattern formation on Lepidopteran wings: deter-
mination of an eyespot. Dev. Biol.80:267-274.<person-group>
                  <string-name>
                     <surname>Nijhout</surname>
                  </string-name>
               </person-group>
               <fpage>267</fpage>
               <volume>80</volume>
               <source>Dev. Biol.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d204e772a1310">
            <mixed-citation id="d204e776" publication-type="journal">
—.1985. Cautery-induced colour patterns in Precis coenia
(Lepidoptera: Nymphalidae). J. Embryol. Exp. Morph.86:191-
203.<person-group>
                  <string-name>
                     <surname>Nijhout</surname>
                  </string-name>
               </person-group>
               <fpage>191</fpage>
               <volume>86</volume>
               <source>J. Embryol. Exp. Morph.</source>
               <year>1985</year>
            </mixed-citation>
         </ref>
         <ref id="d204e812a1310">
            <mixed-citation id="d204e816" publication-type="journal">
—.1990. A comprehensive model for color pattern formation
in butterflies. Proc. R. Soc. B Biol. Sci239:81-113.<person-group>
                  <string-name>
                     <surname>Nijhout</surname>
                  </string-name>
               </person-group>
               <fpage>81</fpage>
               <volume>239</volume>
               <source>Proc. R. Soc. B Biol. Sci</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d204e848a1310">
            <mixed-citation id="d204e852" publication-type="book">
—.1991. The development and evolution of butterfly wing
patterns. Smithsonian Institution Press, Washington, DC.<person-group>
                  <string-name>
                     <surname>Nijhout</surname>
                  </string-name>
               </person-group>
               <source>The development and evolution of butterfly wing patterns</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d204e877a1310">
            <mixed-citation id="d204e881" publication-type="journal">
PAULSEN, S. M. 1994. Quantitative genetics of butterfly wing color
patterns. Devel. Genet.15:79-91.<person-group>
                  <string-name>
                     <surname>Paulsen</surname>
                  </string-name>
               </person-group>
               <fpage>79</fpage>
               <volume>15</volume>
               <source>Devel. Genet.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d204e913a1310">
            <mixed-citation id="d204e917" publication-type="journal">
PAULSEN, S. M., AND H. F NIJHOUT. 1993. Phenotypic correlation
structure among elements of the color pattern in Precis coenia
(Lepidoptera: Nymphalidae). Evolution47:593-618.<object-id pub-id-type="doi">10.2307/2410073</object-id>
               <fpage>593</fpage>
            </mixed-citation>
         </ref>
         <ref id="d204e936a1310">
            <mixed-citation id="d204e940" publication-type="book">
SCHARLOO, W. 1990. The effect of developmental constraints on
selection response. Pp. 197-210 in G. Vida and J. Maynard
Smith, eds. Organisational constraints on the dynamics of evo-
lution. Manchester Univ. Press, Manchester.<person-group>
                  <string-name>
                     <surname>Scharloo</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">The effect of developmental constraints on selection response</comment>
               <fpage>197</fpage>
               <source>Organisational constraints on the dynamics of evolution</source>
               <year>1990</year>
            </mixed-citation>
         </ref>
         <ref id="d204e978a1310">
            <mixed-citation id="d204e982" publication-type="book">
SOKAL, R. R., AND F. J. ROHLF. 1995. Biometry. 3d ed. Freeman,
San Francisco, CA.<person-group>
                  <string-name>
                     <surname>Sokal</surname>
                  </string-name>
               </person-group>
               <edition>3</edition>
               <source>Biometry</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1012a1310">
            <mixed-citation id="d204e1016" publication-type="journal">
VAN DYCK, H., E. MATTHYSEN, AND A. A. DHONDT. 1997. The
effect of wing colour on male behavioural strategies in the speck-
led wood butterfly. Anim. Behav.53:39-51.<person-group>
                  <string-name>
                     <surname>van Dyck</surname>
                  </string-name>
               </person-group>
               <fpage>39</fpage>
               <volume>53</volume>
               <source>Anim. Behav.</source>
               <year>1997</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1051a1310">
            <mixed-citation id="d204e1055" publication-type="journal">
WINDIG, J. J. 1991. Quantification of Lepidoptera wing patterns
using an image analyzer. J. Res. Lepid.30:82-94.<person-group>
                  <string-name>
                     <surname>Windig</surname>
                  </string-name>
               </person-group>
               <fpage>82</fpage>
               <volume>30</volume>
               <source>J. Res. Lepid.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1087a1310">
            <mixed-citation id="d204e1091" publication-type="journal">
WINDIG, J. J., P. M. BRAKEFIELD, N. REITSMA, AND J. G. M. WILSON.
1994. Seasonal polyphenism in the wild: survey of wing patterns
in five species of Bicyclus butterflies in Malawi. Ecol. Entomol.
19:285-298.<person-group>
                  <string-name>
                     <surname>Windig</surname>
                  </string-name>
               </person-group>
               <fpage>285</fpage>
               <volume>19</volume>
               <source>Ecol. Entomol.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d204e1129a1310">
            <mixed-citation id="d204e1133" publication-type="journal">
WOURMS, M. K., AND F E. WASSERMAN. 1985. Butterfly wing
markings are more advantageous during handling than during
the initial strike of an avian predator. Evolution39:845-851.<object-id pub-id-type="doi">10.2307/2408684</object-id>
               <fpage>845</fpage>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
