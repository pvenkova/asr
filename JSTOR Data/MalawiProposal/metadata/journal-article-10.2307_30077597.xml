<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jinfedise</journal-id>
         <journal-id journal-id-type="jstor">j50000007</journal-id>
         <journal-title-group>
            <journal-title>The Journal of Infectious Diseases</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>University of Chicago Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">00221899</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">30077597</article-id>
         <article-categories>
            <subj-group>
               <subject>Major Article and Brief Reports</subject>
               <subj-group>
                  <subject>HIV/AIDS</subject>
               </subj-group>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Higher Concentration of HIV RNA in Rectal Mucosa Secretions than in Blood and Seminal Plasma, among Men Who Have Sex with Men, Independent of Antiretroviral Therapy</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Richard A.</given-names>
                  <surname>Zuckerman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>William L. H.</given-names>
                  <surname>Whittington</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Connie L.</given-names>
                  <surname>Celum</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Tarquin K.</given-names>
                  <surname>Collis</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Aldo J.</given-names>
                  <surname>Lucchetti</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Jorge L.</given-names>
                  <surname>Sanchez</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>James P.</given-names>
                  <surname>Hughes</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Jose L.</given-names>
                  <surname>Sanchez</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Robert W.</given-names>
                  <surname>Coombs</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>7</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">190</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i30077577</issue-id>
         <fpage>156</fpage>
         <lpage>161</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 Infectious Diseases Society of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/30077597"/>
         <abstract>
            <p>High levels of human immunodeficiency virus (HIV) in rectal secretions and semen likely increase the risk of HIV transmission. HIV-infected men who have sex with men made 2-3 study visits, over 4 weeks, to assess rectal, seminal, and plasma levels of HIV RNA. Mixed-effects models estimated the effect of factors on HIV shedding. Twenty-seven (42%) of 64 men were receiving antiretroviral therapy (ART); regardless of ART use, median HIV RNA levels were higher in rectal secretions (4.96<tex-math>$log_{10}$</tex-math>copies/mL) than in blood plasma (4.24<tex-math>$log_{10}$</tex-math>copies/mL) or seminal plasma (3.55<tex-math>$log_{10}$</tex-math>copies/mL; P &lt; .05, each comparison). ART was associated with a 1.3-<tex-math>$log_{10}$</tex-math>reduction in rectal HIV RNA in a model without plasma HIV RNA; with and without plasma RNA in models, ART accounted for a &gt; 1-<tex-math>$log_{10}$</tex-math>decrease in seminal HIV RNA levels. Thus, controlling for plasma HIV RNA, ART had an independent effect on seminal, but not rectal, HIV levels.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1156e277a1310">
            <label>1</label>
            <mixed-citation id="d1156e284" publication-type="other">
Vittinghoff E, Douglas J, Judson F, McKirnan D, MacQueen K, Buch-
binder SP. Per-contact risk of human immunodeficiency virus trans-
mission between male sexual partners. Am J Epidemiol 1999; 150:306-11.</mixed-citation>
         </ref>
         <ref id="d1156e297a1310">
            <label>2</label>
            <mixed-citation id="d1156e304" publication-type="other">
Chakraborty H, Sen PK, Helms RW, et al. Viral burden in genital
secretions determines male-to-female sexual transmission of HIV-1: a
probabilistic empiric model. AIDS 2001; 15:621-7.</mixed-citation>
         </ref>
         <ref id="d1156e317a1310">
            <label>3</label>
            <mixed-citation id="d1156e324" publication-type="other">
Joint United Nations Programme on AIDS (UNAIDS). Report on the
global HIV/AIDS epidemic. Geneva: UNAIDS, 2002:1-226.</mixed-citation>
         </ref>
         <ref id="d1156e334a1310">
            <label>4</label>
            <mixed-citation id="d1156e341" publication-type="other">
Quinn TC, Wawer MJ, Sewankambo N, et al. Viral load and hetero-
sexual transmission of human immunodeficiency virus type 1. Rakai
Project Study Group. N Engl J Med 2000;342:921-9.</mixed-citation>
         </ref>
         <ref id="d1156e355a1310">
            <label>5</label>
            <mixed-citation id="d1156e362" publication-type="other">
Chmiel JS, Detels R, Kaslow RA, Van Raden M, Kingsley LA, Brook-
meyer R. Factors associated with prevalent human immunodeficiency
virus (HIV) infection in the Multicenter AIDS Cohort Study. Am J
Epidemiol 1987; 126:568-77.</mixed-citation>
         </ref>
         <ref id="d1156e378a1310">
            <label>6</label>
            <mixed-citation id="d1156e385" publication-type="other">
Wasserheit JN. Epidemiological synergy. Interrelationships between hu-
man immunodeficiency virus infection and other sexually transmitted
diseases. Sex Transm Dis 1992; 19:61-77.</mixed-citation>
         </ref>
         <ref id="d1156e398a1310">
            <label>7</label>
            <mixed-citation id="d1156e405" publication-type="other">
de Vincenzi I. A longitudinal study of human immunodeficiency virus
transmission by heterosexual partners. European Study Group on Het-
erosexual Transmission of HIV. N Engl J Med 1994;331:341-6.</mixed-citation>
         </ref>
         <ref id="d1156e418a1310">
            <label>8</label>
            <mixed-citation id="d1156e425" publication-type="other">
Dean M, Carrington M, Winkler C, et al. Genetic restriction of HIV-
1 infection and progression to AIDS by a deletion allele of the CKR5
structural gene. Hemophilia Growth and Development Study, Multi-
center AIDS Cohort Study, Multicenter Hemophilia Cohort Study, San
Francisco City Cohort, ALIVE Study. Science 1996;273:1856-62.</mixed-citation>
         </ref>
         <ref id="d1156e444a1310">
            <label>9</label>
            <mixed-citation id="d1156e451" publication-type="other">
Zhu T, Wang N, Carr A, et al. Genetic characterization of human
immunodeficiency virus type 1 in blood and genital secretions: evi-
dence for viral compartmentalization and selection during sexual trans-
mission. J Virol 1996;70:3098-107.</mixed-citation>
         </ref>
         <ref id="d1156e467a1310">
            <label>10</label>
            <mixed-citation id="d1156e474" publication-type="other">
Eron JJ, Gilliam B, Fiscus S, Dyer J, Cohen MS. HIV-1 shedding and
chlamydial urethritis. JAMA 1996; 275:36.</mixed-citation>
         </ref>
         <ref id="d1156e485a1310">
            <label>11</label>
            <mixed-citation id="d1156e492" publication-type="other">
Cohen MS, Hoffman IF, Royce RA, et al. Reduction of concentration
of HIV-1 in semen after treatment of urethritis: implications for pre-
vention of sexual transmission of HIV-1. AIDSCAP Malawi Research
Group. Lancet 1997;349:1868-73.</mixed-citation>
         </ref>
         <ref id="d1156e508a1310">
            <label>12</label>
            <mixed-citation id="d1156e515" publication-type="other">
Fiore JR, Zhang YJ, Bjorndal A, et al. Biological correlates of HIV-1
heterosexual transmission. AIDS 1997;11:1089-94.</mixed-citation>
         </ref>
         <ref id="d1156e525a1310">
            <label>13</label>
            <mixed-citation id="d1156e532" publication-type="other">
Renzi C, Douglas JM Jr, Foster M, et al. Herpes simplex virus type 2
infection as a risk factor for human immunodeficiency virus acquisition
in men who have sex with men. J Infect Dis 2003; 187:19-25.</mixed-citation>
         </ref>
         <ref id="d1156e545a1310">
            <label>14</label>
            <mixed-citation id="d1156e552" publication-type="other">
Clayton F, Reka S, Cronin WJ, Torlakovic E, Sigal SH, Kotler DP. Rectal
mucosal pathology varies with human immunodeficiency virus antigen
content and disease stage. Gastroenterology 1992; 103:919-33.</mixed-citation>
         </ref>
         <ref id="d1156e565a1310">
            <label>15</label>
            <mixed-citation id="d1156e572" publication-type="other">
Veazey RS, DeMaria M, Chalifoux LV, et al. Gastrointestinal tract as a
major site of  T cell depletion and viral replication in SIV in-
fection. Science 1998;280:427-31.</mixed-citation>
         </ref>
         <ref id="d1156e588a1310">
            <label>16</label>
            <mixed-citation id="d1156e595" publication-type="other">
Di Stefano M, Favia A, Monno L, et al. Intracellular and cell-free (infec-
tious) HIV-1 in rectal mucosa. J Med Virol 2001;65:637-43.</mixed-citation>
         </ref>
         <ref id="d1156e606a1310">
            <label>17</label>
            <mixed-citation id="d1156e613" publication-type="other">
Poles MA, Elliott J, Vingerhoets J, et al. Despite high concordance,
distinct mutational and phenotypic drug resistance profiles in human
immunodeficiency virus type 1 RNA are observed in gastrointestinal
mucosal biopsy specimens and peripheral blood mononuclear cells
compared with plasma. J Infect Dis 2001; 183:143-8.</mixed-citation>
         </ref>
         <ref id="d1156e632a1310">
            <label>18</label>
            <mixed-citation id="d1156e639" publication-type="other">
Anton PA, Mitsuyasu RT, Decks SG, et al. Multiple measures of HIV
burden in blood and tissue are correlated with each other but not with
clinical parameters in aviremic subjects. AIDS 2003; 17:53-63.</mixed-citation>
         </ref>
         <ref id="d1156e652a1310">
            <label>19</label>
            <mixed-citation id="d1156e659" publication-type="other">
Zuckerman RA, Whittington WLH, Celum CL, et al. Factors associated
with oropharyngeal human immunodeficiency virus shedding. J Infect
Dis 2003; 188:142-5.</mixed-citation>
         </ref>
         <ref id="d1156e672a1310">
            <label>20</label>
            <mixed-citation id="d1156e679" publication-type="other">
Coombs RW, Speck CE, Hughes JP, et al. Association between culturable
human immunodeficiency virus type 1 (HIV-1) in semen and HIV-1
RNA levels in semen and blood: evidence for compartmentalization of
HIV-1 between semen and blood. J Infect Dis 1998; 177:320-30.</mixed-citation>
         </ref>
         <ref id="d1156e695a1310">
            <label>21</label>
            <mixed-citation id="d1156e702" publication-type="other">
Hughes JP. Mixed effects models with censored data with application
to HIV RNA levels. Biometrics 1999;55:625-9.</mixed-citation>
         </ref>
         <ref id="d1156e712a1310">
            <label>22</label>
            <mixed-citation id="d1156e719" publication-type="other">
Kashuba AD, Dyer JR, Kramer LM, Raasch RH, Eron JJ, Cohen MS.
Antiretroviral-drug concentrations in semen: implications for sexual
transmission of human immunodeficiency virus type 1. Antimicrob
Agents Chemother 1999;43:1817-26.</mixed-citation>
         </ref>
         <ref id="d1156e736a1310">
            <label>23</label>
            <mixed-citation id="d1156e743" publication-type="other">
Anton PA, Poles MA, Elliott J, et al. Sensitive and reproducible quan-
titation of mucosal HIV-1 RNA and DNA viral burden in patients with
detectable and undetectable plasma viral HIV-1 RNA using endoscopic
biopsies. J Virol Methods 2001;95:65-79.</mixed-citation>
         </ref>
         <ref id="d1156e759a1310">
            <label>24</label>
            <mixed-citation id="d1156e766" publication-type="other">
Reichelderfer PS, Coombs RW, Wright DJ, et al. Effect of menstrual
cycle on HIV-1 levels in the peripheral blood and genital tract. WHS
001 Study Team. AIDS 2000; 14:2101-7.</mixed-citation>
         </ref>
         <ref id="d1156e779a1310">
            <label>25</label>
            <mixed-citation id="d1156e786" publication-type="other">
John GC, Sheppard H, Mbori-Ngacha D, et al. Comparison of tech-
niques for HIV-1 RNA detection and quantitation in cervicovaginal
secretions. J Acquir Immune Defic Syndr 2001;26:170-5.</mixed-citation>
         </ref>
         <ref id="d1156e799a1310">
            <label>26</label>
            <mixed-citation id="d1156e806" publication-type="other">
Lampinen TM, Critchlow CW, Kuypers JM, et al. Association of an-
tiretroviral therapy with detection of HIV-1 RNA and DNA in the anorec-
tal mucosa of homosexual men. AIDS 2000; 14:F69-75.</mixed-citation>
         </ref>
         <ref id="d1156e819a1310">
            <label>27</label>
            <mixed-citation id="d1156e826" publication-type="other">
Gupta P, Mellors J, Kingsley L, et al. High viral load in semen of human
immunodeficiency virus type 1-infected men at all stages of disease
and its reduction by therapy with protease and nonnucleoside reverse
transcriptase inhibitors. J Virol 1997;71:6271-5.</mixed-citation>
         </ref>
         <ref id="d1156e842a1310">
            <label>28</label>
            <mixed-citation id="d1156e849" publication-type="other">
Vernazza PL, Gilliam BL, Flepp M, et al. Effect of antiviral treatment
on the shedding of HIV-1 in semen. AIDS 1997; 11:1249-54.</mixed-citation>
         </ref>
         <ref id="d1156e860a1310">
            <label>29</label>
            <mixed-citation id="d1156e867" publication-type="other">
Eron JJ Jr, Smeaton LM, Fiscus SA, et al. The effects of protease inhibitor
therapy on human immunodeficiency virus type 1 levels in semen (AIDS
clinical trials group protocol 850). J Infect Dis 2000; 181:1622-8.</mixed-citation>
         </ref>
         <ref id="d1156e880a1310">
            <label>30</label>
            <mixed-citation id="d1156e887" publication-type="other">
Liuzzi G, Chirianni A, Clementi M, et al. Analysis of HIV-1 load in
blood, semen and saliva: evidence for different viral compartments in
a cross-sectional and longitudinal study. AIDS 1996; 10:F51-6.</mixed-citation>
         </ref>
         <ref id="d1156e900a1310">
            <label>31</label>
            <mixed-citation id="d1156e907" publication-type="other">
Barroso PF, Schechter M, Gupta P, et al. Effect of antiretroviral therapy
on HIV shedding in semen. Ann Intern Med 2000; 133:280-4.</mixed-citation>
         </ref>
         <ref id="d1156e917a1310">
            <label>32</label>
            <mixed-citation id="d1156e924" publication-type="other">
Leruez-Ville M, Dulioust E, Costabliola D, et al. Decrease in HIV-1
seminal shedding in men receiving highly active antiretroviral therapy:
an 18 month longitudinal study (ANRS EP012). AIDS 2002; 16:486-8.</mixed-citation>
         </ref>
         <ref id="d1156e937a1310">
            <label>33</label>
            <mixed-citation id="d1156e944" publication-type="other">
Kiviat NB, Critchlow CW, Hawes SE, et al. Determinants of human
immunodeficiency virus DNA and RNA shedding in the anal-rectal
canal of homosexual men. J Infect Dis 1998; 177:571-8.</mixed-citation>
         </ref>
         <ref id="d1156e957a1310">
            <label>34</label>
            <mixed-citation id="d1156e964" publication-type="other">
Brambilla D, Reichelderfer PS, Bremer JW, et al. The contribution of
assay variation and biological variation to the total variability of plasma
HIV-1 RNA measurements. The Women Infant Transmission Study
Clinics. Virology Quality Assurance Program. AIDS 1999; 13:2269-79.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
