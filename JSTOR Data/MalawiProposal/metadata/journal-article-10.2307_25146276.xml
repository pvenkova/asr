<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">japplecon</journal-id>
         <journal-id journal-id-type="jstor">j100764</journal-id>
         <journal-title-group>
            <journal-title>Journal of Applied Econometrics</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>John Wiley &amp; Sons</publisher-name>
         </publisher>
         <issn pub-type="ppub">08837252</issn>
         <issn pub-type="epub">10991255</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">25146276</article-id>
         <article-id pub-id-type="pub-doi">10.1002/jae.722</article-id>
         <title-group>
            <article-title>The Solow Model with CES Technology: Nonlinearities and Parameter Heterogeneity</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Winford H.</given-names>
                  <surname>Masanjala</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Chris</given-names>
                  <surname>Papageorgiou</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>3</month>
            <year>2004</year>
         
            <day>1</day>
            <month>4</month>
            <year>2004</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">19</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id>i25146273</issue-id>
         <fpage>171</fpage>
         <lpage>201</lpage>
         <permissions>
            <copyright-statement>Copyright 2004 John Wiley &amp; Sons, Ltd.</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/25146276"/>
         <abstract>
            <p>This paper examines whether nonlinearities in the aggregate production function can explain parameter heterogeneity in the Solow growth regressions. Nonlinearities in the production technology are introduced by replacing the commonly used Cobb-Douglas (CD) aggregated production specification with the more general Constant-Elasticity-of-Substitution (CES) specification. We first justify our choice of production function by showing that cross-country regressions favour the CES over the CD technology. Then, by using an endogenous threshold methodology we show that the Solow model with CES technology is consistent with the existence of multiple regimes.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group>
         <title>[Footnotes]</title>
         <fn id="d252e141a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d252e148" publication-type="other">
Solow (1957)</mixed-citation>
            </p>
         </fn>
         <fn id="d252e155a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d252e162" publication-type="other">
Duffy and Papageorgiou (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d252e169a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d252e176" publication-type="other">
Allen, 1938, pp. 503-509</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e182" publication-type="other">
Uzawa (1962).</mixed-citation>
            </p>
         </fn>
         <fn id="d252e189a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d252e196" publication-type="other">
Bernanke and Gu̇rkaynak (2001, pp. 8-9).</mixed-citation>
            </p>
         </fn>
         <fn id="d252e204a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d252e211" publication-type="other">
Kaldor's (1961)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e217" publication-type="other">
Solow (1958)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e223" publication-type="other">
Gollin, 2002</mixed-citation>
            </p>
         </fn>
         <fn id="d252e230a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d252e237" publication-type="other">
Brock and Durlauf (2000, pp. 9-11)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e243" publication-type="other">
Durlauf (2001, p. 66).</mixed-citation>
            </p>
         </fn>
         <fn id="d252e250a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d252e257" publication-type="other">
Brock and Durlauf (2000, pp. 6-8)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e263" publication-type="other">
Durlauf (2001, p. 67).</mixed-citation>
            </p>
         </fn>
         <fn id="d252e270a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d252e277" publication-type="other">
Hansen (2000).</mixed-citation>
            </p>
         </fn>
         <fn id="d252e284a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d252e291" publication-type="other">
Durlauf and Johnson (1995)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e297" publication-type="other">
Hansen (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e303" publication-type="other">
Johnson and Takeyama
(2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e313" publication-type="other">
Papageorgiou (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d252e320a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d252e327" publication-type="other">
Durlauf and Johnson, 1995</mixed-citation>
            </p>
         </fn>
         <fn id="d252e335a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d252e342" publication-type="other">
Kalaitzidakis et al. (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e348" publication-type="other">
Kourtellos (2001)</mixed-citation>
            </p>
         </fn>
         <fn id="d252e355a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d252e362" publication-type="other">
Duffy and Papageorgiou (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e368" publication-type="other">
Miyagiwa and Papageorgiou (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d252e374" publication-type="other">
Duffy et al (2003)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d252e390a1310">
            <mixed-citation id="d252e394" publication-type="other">
Allen RGD. 1938. Mathematical Analysis of Economists. Macmillan: London.</mixed-citation>
         </ref>
         <ref id="d252e401a1310">
            <mixed-citation id="d252e405" publication-type="other">
Arrow KJ, Chenery HB, Minhas BS, Solow RM. 1961. Capital-labor substitution and economic efficiency.
Review of Economics and Statistics 43: 225-250.</mixed-citation>
         </ref>
         <ref id="d252e415a1310">
            <mixed-citation id="d252e419" publication-type="other">
Azariadis C. 2001. The theory of poverty traps: what have we learned? Working Paper, Department of
Economics, UCLA.</mixed-citation>
         </ref>
         <ref id="d252e429a1310">
            <mixed-citation id="d252e433" publication-type="other">
Azariadis C, de la Croix D. 2001. Growth or equality? Losers and gainers from financial reform. Working
Paper, Department of Economics, UCLA.</mixed-citation>
         </ref>
         <ref id="d252e444a1310">
            <mixed-citation id="d252e448" publication-type="other">
Azariadis C, Drazen A. 1990. Threshold externalities in economic development. Quarterly Journal of
Economics 105: 501-526.</mixed-citation>
         </ref>
         <ref id="d252e458a1310">
            <mixed-citation id="d252e462" publication-type="other">
Bernanke BE, Giirkaynak RS. 2001. Is growth exogenous? Taking Mankiw, Romer, and Weil seriously. In:
NBER Macroeconomics Annual 16, Bernanke B, Rogoff K (eds). MIT Press: Massachusetts.</mixed-citation>
         </ref>
         <ref id="d252e472a1310">
            <mixed-citation id="d252e476" publication-type="other">
Brock W, Durlauf S. 2000. Growth economics and reality. Working Paper, Department of Economics,
University of Wisconsin.</mixed-citation>
         </ref>
         <ref id="d252e486a1310">
            <mixed-citation id="d252e490" publication-type="other">
Duffy J, Papageorgiou C. 2000. A cross-country empirical investigation of the aggregate production function
specification. Journal of Economic Growth 5: 87-120.</mixed-citation>
         </ref>
         <ref id="d252e500a1310">
            <mixed-citation id="d252e504" publication-type="other">
Duffy J, Papageorgiou C, Perez-Sebastian F. 2003. Capital-skill complementarity? Evidence from a panel
of countries. Review of Economics and Statistics, in press.</mixed-citation>
         </ref>
         <ref id="d252e514a1310">
            <mixed-citation id="d252e518" publication-type="other">
Durlauf S. 1993. Nonergodic economic growth. Review of Economic Studies 60: 349-366.</mixed-citation>
         </ref>
         <ref id="d252e526a1310">
            <mixed-citation id="d252e530" publication-type="other">
Durlauf S. 2001. Manifesto for a growth econometrics. Journal of Econometrics 100: 65-69.</mixed-citation>
         </ref>
         <ref id="d252e537a1310">
            <mixed-citation id="d252e541" publication-type="other">
Durlauf S, Johnson P. 1995. Multiple regimes and cross-country growth behavior. Journal of Applied
Econometrics 10: 365-384.</mixed-citation>
         </ref>
         <ref id="d252e551a1310">
            <mixed-citation id="d252e555" publication-type="other">
Durlauf S, Quah D. 1999. The new empirics of economic growth. In: Handbook of Macroeconomics,
Taylor JB, Woodford M (eds). North Holland Press: Amsterdam; Vol. 1, Ch. 4, 235-308.</mixed-citation>
         </ref>
         <ref id="d252e565a1310">
            <mixed-citation id="d252e569" publication-type="other">
Durlauf S, Kourtellos A, Minkin A. 2001. The local Solow growth model. European Economic Review 45:
928-940.</mixed-citation>
         </ref>
         <ref id="d252e579a1310">
            <mixed-citation id="d252e583" publication-type="other">
Fernàndez C, Ley E, Steel MFJ. 2002. Model uncertainty in cross-country growth regressions. Journal of
Applied Econometrics 16: 563-576.</mixed-citation>
         </ref>
         <ref id="d252e593a1310">
            <mixed-citation id="d252e597" publication-type="other">
Galor O, Zeira J. 1993. Income distribution and macroeconomics. Review of Economic Studies 60: 35-52.</mixed-citation>
         </ref>
         <ref id="d252e605a1310">
            <mixed-citation id="d252e609" publication-type="other">
Gollin D. 2002. Getting income shares right. Journal of Political Economy 110: 458-474.</mixed-citation>
         </ref>
         <ref id="d252e616a1310">
            <mixed-citation id="d252e620" publication-type="other">
Hansen BE. 1996. Inference when a nuisance parameter is not identified under the null hypothesis.
Econometrica 64: 413-430.</mixed-citation>
         </ref>
         <ref id="d252e630a1310">
            <mixed-citation id="d252e634" publication-type="other">
Hansen BE. 2000. Sample splitting and threshold estimation. Econometrica 68: 575-603.</mixed-citation>
         </ref>
         <ref id="d252e641a1310">
            <mixed-citation id="d252e645" publication-type="other">
Hastie T, Tibshirani R. 1993. Varying coefficient models. Journal of the Royal Statistical Society 55(Series
B): 757-796.</mixed-citation>
         </ref>
         <ref id="d252e655a1310">
            <mixed-citation id="d252e659" publication-type="other">
Johnson PA, Takeyama LN. 2001. Initial conditions and economic growth in the US states. European
Economic Review 45: 919-927.</mixed-citation>
         </ref>
         <ref id="d252e669a1310">
            <mixed-citation id="d252e673" publication-type="other">
Kalaitzidakis P, Mamuneas TP, Savvides A, Stengos T. 2001. Measures of human capital and nonlinearities
in economic growth. Journal of Economic Growth 6: 229—254.</mixed-citation>
         </ref>
         <ref id="d252e684a1310">
            <mixed-citation id="d252e688" publication-type="other">
Kaldor N. 1961. Capital accumulation and economic growth. In: The Theory of Capital, Lutz FA, Hague DC
(eds). St. Martin's Press: New York; 177-222.</mixed-citation>
         </ref>
         <ref id="d252e698a1310">
            <mixed-citation id="d252e702" publication-type="other">
Klump R, de La Grandville O. 2000. Economic growth and the elasticity of substitution: two theorems and
some suggestions. American Economic Review 90: 282-291.</mixed-citation>
         </ref>
         <ref id="d252e712a1310">
            <mixed-citation id="d252e716" publication-type="other">
Kmenta J. 1967. On estimation of the CES production function. International Economic Review 8: 180-189.</mixed-citation>
         </ref>
         <ref id="d252e723a1310">
            <mixed-citation id="d252e727" publication-type="other">
Kourtellos A. 2001. Modelling coefficient heterogeneity in cross-country growth regression models. Working
Paper, Department of Economics, University of Wisconsin.</mixed-citation>
         </ref>
         <ref id="d252e737a1310">
            <mixed-citation id="d252e741" publication-type="other">
Liu C, Stengos T. 1999. Non-linearities in cross-country growth regressions: a semiparametric approach.
Journal of Applied Econometrics 14: 527-538.</mixed-citation>
         </ref>
         <ref id="d252e751a1310">
            <mixed-citation id="d252e755" publication-type="other">
Mankiw NG, Romer D, Weil DN. 1992. A contribution to the empirics of economic growth. Quarterly
Journal of Economics 107: 407-437.</mixed-citation>
         </ref>
         <ref id="d252e766a1310">
            <mixed-citation id="d252e770" publication-type="other">
Miyagiwa K, Papageorgiou C. 2003. Elasticity of substitution and growth: normalized CES in the diamond
model. Economic Theory 21: 155-165.</mixed-citation>
         </ref>
         <ref id="d252e780a1310">
            <mixed-citation id="d252e784" publication-type="other">
Papageorgiou C. 2002. Trade as a threshold variable for multiple regimes. Economics Letters 11: 85-91.</mixed-citation>
         </ref>
         <ref id="d252e791a1310">
            <mixed-citation id="d252e795" publication-type="other">
Romer D. 2001. Is growth exogenous? Taking Mankiw, Romer and Weil seriously: a comment. In: NBER
Macroeconomics Annual 16, Bernanke B, Rogoff K (eds). MIT Press: Massachusetts.</mixed-citation>
         </ref>
         <ref id="d252e805a1310">
            <mixed-citation id="d252e809" publication-type="other">
Sala-i-Martin X. 1997. I just ran two million regressions. AEA Papers and Proceedings 87: 178-183.</mixed-citation>
         </ref>
         <ref id="d252e816a1310">
            <mixed-citation id="d252e820" publication-type="other">
Solow RM. 1956. A contribution to the theory of economic growth. Quarterly Journal of Economics 70:
65-94.</mixed-citation>
         </ref>
         <ref id="d252e830a1310">
            <mixed-citation id="d252e834" publication-type="other">
Solow RM. 1957. Technical change and the aggregate production function. Review of Economics and Statistics
39: 312-320.</mixed-citation>
         </ref>
         <ref id="d252e845a1310">
            <mixed-citation id="d252e849" publication-type="other">
Solow RM. 1958. A skeptical note on the constancy of the relative shares. American Economic Review 48:
618-631.</mixed-citation>
         </ref>
         <ref id="d252e859a1310">
            <mixed-citation id="d252e863" publication-type="other">
Uzawa H. 1962. Production functions with constant elasticity of substitution. Review of Economic Studies
30: 291-299.</mixed-citation>
         </ref>
         <ref id="d252e873a1310">
            <mixed-citation id="d252e877" publication-type="other">
Ventura J. 1997. Growth and interdependence. Quarterly Journal of Economics 112: 57-84.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
