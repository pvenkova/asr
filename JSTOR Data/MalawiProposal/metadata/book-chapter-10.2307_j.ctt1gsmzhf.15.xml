<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1gsmzhf</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Economics</subject>
      </subj-group>
      <book-title-group>
         <book-title>The IMF and the World Bank at Sixty</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <role>Edited by</role>
            <name name-style="western">
               <surname>BUIRA</surname>
               <given-names>ARIEL</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>05</day>
         <month>05</month>
         <year>2005</year>
      </pub-date>
      <isbn content-type="ppub">9781843311966</isbn>
      <isbn content-type="epub">9780857287281</isbn>
      <publisher>
         <publisher-name>Anthem Press</publisher-name>
         <publisher-loc>London, UK; New York, NY, USA</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2005</copyright-year>
         <copyright-holder>Ariel Buira</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1gsmzhf"/>
      <abstract abstract-type="short">
         <p>An authoritative review of the position of the IMF and World Bank in their sixtieth year.</p>
      </abstract>
      <counts>
         <page-count count="362"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.3</book-part-id>
                  <title-group>
                     <title>LIST OF CONTRIBUTORS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.4</book-part-id>
                  <title-group>
                     <title>FOREWORD</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Helleiner</surname>
                           <given-names>Gerry</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
                  <abstract>
                     <p>Sixty years after their creation, the IMF and World Bank play roles quite different from those their original founders had envisaged. The founders would undoubtedly be astounded to learn that these international financial institutions (IFIs) have emerged as, above all else, significant financiers, analysts and advisors for the developing (and, more recently, transition) countries. As purveyors of current ‘knowledge’ about stabilization and development issues, and gatekeepers to external sources of finance, the IFIs are dominant influences in macroeconomic and development policy formation in much of the developing (and transition) world, particularly in the economically most vulnerable parts of it. The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Buira</surname>
                           <given-names>Ariel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>As the Bretton Woods institutions turn 60, they face a number of challenges, some new resulting from the changes that have occurred in the world economy, others the outcome of their approaches to the problems of stabilization and development, and of their own governance structure. This book presents a selection of research papers prepared for the G-24 that address these challenges and suggest the need for reform. The introductory essay presents a critical overview of the functioning of the IMF and the international monetary system and underscores a number of shortcomings that could be remedied through changes in its governance.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>THE IMF AT SIXTY:</title>
                     <subtitle>AN UNFULFILLED POTENTIAL?</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Buira</surname>
                           <given-names>Ariel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>5</fpage>
                  <abstract>
                     <p>Over the past half-century the world economy has become increasingly interdependent. Developments in the economy of one country or region are transmitted to other countries through increased international trade and financial flows. This integration differs from the patterns of a century ago in that a growing number of multinational firms have spread their production processes over a number of countries. As a result intra-firm trade and intra-industry trade have risen sharply as a proportion of international trade. Developments in information technology have erased distances and the integration of capital markets has proceeded in a way that essentially creates a single</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>CONDITIONALITY AND ITS ALTERNATIVES</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kapur</surname>
                           <given-names>Devesh</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>In the post-Bretton Woods era conditionality emerged at the heart of international development assistance. Its expansion in scale and scope poses a puzzle: why did an international institutional mechanism designed to change the behaviour of governments expand, despite continued uncertainty about the effectiveness of this mechanism? Recently, there has been a shift in the regime. While loan conditionalities of International Financial Institutions (IFIs) have been scaled back modestly, conditionalities as a meta-regime are becoming more, not less, important in development assistance.</p>
                     <p>Several explanations have been advanced to understand these trends. Some would challenge the assertion that conditionality ‘does not work’.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>MISSION CREEP, MISSION PUSH AND DISCRETION:</title>
                     <subtitle>THE CASE OF IMF CONDITIONALITY</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Babb</surname>
                           <given-names>Sarah</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Buira</surname>
                           <given-names>Ariel</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>59</fpage>
                  <abstract>
                     <p>In recent years, the Bretton Woods organizations have been subjected to intense scrutiny and pressure to change. Founded at the end of World War II to help lay the foundations of a new era of stability and prosperity, the World Bank and the International Monetary Fund (IMF) are widely viewed as having evolved in ways that would have surprised their founders. A term that has gained popularity among World Bank and IMF critics is ‘mission creep’, meaning the systematic shifting of organizational activities away from original mandates (IFI Advisory Commission 1998; Stiglitz 2002; Einhorn 2001; Bretton Woods Project 2003).</p>
                     <p>The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>UP FROM SIN:</title>
                     <subtitle>A PORTFOLIO APPROACH TO SALVATION</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Dodd</surname>
                           <given-names>Randall</given-names>
                        </name>
                     </contrib>
                     <contrib contrib-type="author" id="contrib2" xlink:type="simple">
                        <name name-style="western">
                           <surname>Spiegel</surname>
                           <given-names>Shari</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>85</fpage>
                  <abstract>
                     <p>The massive amount of foreign indebtedness is one of the most significant problems facing developing countries today. The majority of this debt is denominated in the major currencies,¹ and it has left developing countries with enormous exposures to foreign exchange and foreign interest-rate risk. External shocks transmitted through these exposures have proved costly to absorb. Most of the emerging-market crises over the past two decades were caused, or at least exacerbated, by foreign borrowings (although derivatives have added substantially to foreign-currency-denominated liabilities in some cases²). Concerns over foreign debt were raised soon after the 1973 oil price shock, and the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>TRIP WIRES AND SPEED BUMPS:</title>
                     <subtitle>MANAGING FINANCIAL RISKS AND REDUCING THE POTENTIAL FOR FINANCIAL CRISES IN DEVELOPING ECONOMIES</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Grabel</surname>
                           <given-names>Ilene</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>117</fpage>
                  <abstract>
                     <p>Developing countries have an interest in acting to curtail the financial risks to which they are exposed because these risks often culminate in costly and painful financial crises. Toward this end, ‘trip wires and speed bumps’ can be useful. We argue that the trip wire—speed bump approach presented here has a far greater ability to reduce financial risks (including the potential of these risks to induce crises) than do the ‘early warning systems’ (hereafter, EWS) that are currently being promoted with such vigour in the international and academic financial community (Goldstein, Kaminsky, and Reinhart 2000; and see below for</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.11</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>A FISCAL INSURANCE SCHEME FOR THE EASTERN CARIBBEAN CURRENCY UNION</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>dos Reis</surname>
                           <given-names>Laura</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>151</fpage>
                  <abstract>
                     <p>This paper provides empirical evidence to support the creation of a fiscal insurance mechanism for member countries of the Organization of the Eastern Caribbean States (OECS) monetary union.¹ Fiscal insurance refers to a system of intra-country compensating payments undertaken to smooth cyclical fluctuations in fiscal expenditures. Member countries would agree to contribute to a buffer fund administered by a supra-national institution or a centralized fiscal authority. The risk-sharing scheme would consist of a set of rules that determine the amounts of net transfers according to permanent and cyclical components of government revenues.</p>
                     <p>In a monetary union of countries subject to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.12</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>WHO PAYS FOR THE WORLD BANK?</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mohammed</surname>
                           <given-names>Aziz Ali</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>181</fpage>
                  <abstract>
                     <p>This paper looks at one aspect of the financial governance of the World Bank Group through the lens of the net income earned by the International Bank for Reconstruction and Development (IBRD), the principal income earning unit of the World Bank Group¹ (WBG), as defined in this paper. We first look at the growing divergence between voting rights and the contributions made to IBRD equity by shareholders and borrowers as the share of retained earnings has risen while the share of paid-in capital has declined over the years. We then explain the framework established to guide the allocation of net</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.13</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>REINVENTING INDUSTRIAL STRATEGY:</title>
                     <subtitle>THE ROLE OF GOVERNMENT POLICY IN BUILDING INDUSTRIAL COMPETITIVENESS</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lall</surname>
                           <given-names>Sanjaya</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>197</fpage>
                  <abstract>
                     <p>As liberalization and globalization gather momentum, concern with industrial competitiveness is growing, in industrial as well as in developing countries. But developing countries face the most intense competitive pressures: many find that their enterprises are unable to cope with the rigours of open markets both in exporting and in competing with imports, as they open their economies. Some countries are doing very well, but many are not. Diverging industrial competitiveness among developing countries is one of the basic causes of the growing disparities in income that are now a pervasive feature of the global economy. The immense potential that globalization</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.14</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>ASSESSING THE RISKS IN THE PRIVATE PROVISION OF ESSENTIAL SERVICES</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kessler</surname>
                           <given-names>Tim</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>235</fpage>
                  <abstract>
                     <p>Considerable scholarship has been devoted lately to researching how best to deliver essential public services, including water, sanitation, electricity and health care. The issue of the private provision of essential services¹ is explored in considerable depth in both the United Nations Development Programme’s (UNDP) 2003 Human Development Report (HDR), entitled ‘Millennium Development Goals: A Compact among Nations to End Poverty’, and in the World Bank’s 2004 World Development Report (WDR), entitled ‘Making Services Work for Poor People’. Based on a reading of the preliminary WDR draft, it appears that the World Bank is considerably more sanguine about market-based approaches to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.15</book-part-id>
                  <title-group>
                     <label>11</label>
                     <title>HOW WELL DO MEASUREMENTS OF AN ENABLING DOMESTIC ENVIRONMENT FOR DEVELOPMENT STAND UP?</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Herman</surname>
                           <given-names>Barry</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>281</fpage>
                  <abstract>
                     <p>
                        <italic>It’s the institutions, stupid.</italic>
                     </p>
                     <p>Guillermo Calvo and Frederic Mishkin¹</p>
                     <p>There is a great sense among economists working on development today that the quality and robustness of domestic political and economic institutions matter greatly, both for the effectiveness of all types of policies (including exchange-rate management, the focus of Calvo and Mishkin in the quote above) and for the prospects for development itself. In this view, if societies get their institutions ‘right’ and also adopt the ‘right’ policies (which is supposed to be more likely when a country has the ‘right’ institutions), they will create an ‘enabling environment’ for development that</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.16</book-part-id>
                  <title-group>
                     <label>12</label>
                     <title>THE COCOA MARKET UNDER NEOLIBERALISM</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>ul Haque</surname>
                           <given-names>Irfan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>311</fpage>
                  <abstract>
                     <p>There was a time when commodities figured prominently in discussions of international trade, financial, and development issues. Already in the early 1940s, John Maynard Keynes, in conceptualizing what has emerged as the twin Bretton Woods institutions, devoted a great deal of thought to the commodity issue and its close links with international financial stability (Keynes 1943). He laboured in the shadow of the Great Depression, when commodity prices plummeted to depths not seen before, and was well aware of the commodity shortages during and after World War II.</p>
                     <p>At that time, the volatility and unreliability of commodity markets was of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1gsmzhf.17</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>337</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
