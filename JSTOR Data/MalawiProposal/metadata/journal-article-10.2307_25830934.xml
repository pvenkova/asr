<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">savideve</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50002264</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Savings and Development</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Giodano Dell'Amore Foundation</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03934551</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25830934</article-id>
         <title-group>
            <article-title>VALUING MICROFINANCE INSTITUTIONS</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Barclay</given-names>
                  <surname>O'Brien</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2006</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">30</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25830930</issue-id>
         <fpage>275</fpage>
         <lpage>296</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25830934"/>
         <abstract>
            <p>This paper examines the main approaches taken in valuing a Microfinance Institution (MFI). A survey of the Council of Microfinance Equity Funds revealed that, whilst many of its members use a Discounted CashFlow (DCF) method, most favour a multiple of Book Value approach to valuing MFIs, with some reluctant to pay a premium to current Book Value. This paper recommends the use of a DCF or, where it is not feasible, an Earnings Multiple method. A number of qualitative factors are also identified as being important in any valuation exercise. Given the potentially crucial role that valuation may play in the future development of Microfinance, it is suggested that the collection of more data on transactions conducted by MFIs would aid the debate on which methodology to use. The paper also argues for more transparency in how MFIs are run and improved marketing of their shares to investors. The paper concludes that, on balance, the prices paid for financially sustainable and regulated MFIs may be at too great a discount to those applied to a commercial bank, based on an analysis of the characteristics that distinguish such MFIs from banks. At the very least, MFIs should take a more commercial approach to their own valuation, if they are to access the additional sources of funding, that are essential for the industry to grow, at a fair price.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d911e162a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d911e169" publication-type="other">
Goodman (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e176a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d911e183" publication-type="other">
McCarter (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e190a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d911e197" publication-type="other">
de Sousa-Shields and Frankiewicz (2005), p 48</mixed-citation>
            </p>
         </fn>
         <fn id="d911e204a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d911e211" publication-type="other">
Kaddaras and Rhyne (2004)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e219a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d911e226" publication-type="other">
Silva (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e233a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d911e240" publication-type="other">
King (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e247a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d911e254" publication-type="other">
The MicroBanking Bulletin (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d911e261a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d911e268" publication-type="other">
Silva (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d911e274" publication-type="other">
www.africapfund.com</mixed-citation>
            </p>
         </fn>
         <fn id="d911e281a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d911e288" publication-type="other">
Kaddaras and Rhyne (2004)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d911e294" publication-type="other">
de Sousa-Shields and Frankiewicz (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d911e300" publication-type="other">
Meehan (2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d911e307a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d911e314" publication-type="other">
McCarter (2002)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d911e330a1310">
            <mixed-citation id="d911e334" publication-type="other">
Daley-Harris, Sam (2005), "State of the Microcredit Summit Campaign Report 2005", Microcredit Summit</mixed-citation>
         </ref>
         <ref id="d911e341a1310">
            <mixed-citation id="d911e345" publication-type="other">
Damodaran, Aswarth (2005), "The Value of Control: Implications for Control Premia, Minority Discounts
and Voting Share Differentials", Stern School of Business</mixed-citation>
         </ref>
         <ref id="d911e355a1310">
            <mixed-citation id="d911e359" publication-type="other">
De Sousa-Shields, Marc and Cheryl Frankiewicz (2005), "Financing Microfinance Institutions: The Con-
text for Transitions to Private Equity", USAID MicroReport #8</mixed-citation>
         </ref>
         <ref id="d911e369a1310">
            <mixed-citation id="d911e373" publication-type="other">
European Private Equity &amp; Venture Capital Association (2005), "International Private Equity and Venture
Capital Valuation Guidelines", www.privateequityvaluation.com</mixed-citation>
         </ref>
         <ref id="d911e384a1310">
            <mixed-citation id="d911e388" publication-type="other">
Fernando, Nimal (2004), "Micro Success Story? Transformation of Nongovernmental Organizations into
Regulated Financial Institutions", Asian Development Bank</mixed-citation>
         </ref>
         <ref id="d911e398a1310">
            <mixed-citation id="d911e402" publication-type="other">
Goodman, Patrick (2005), "Microfinance Investment Funds: Key Features", Appui au Developpement
Autonome</mixed-citation>
         </ref>
         <ref id="d911e412a1310">
            <mixed-citation id="d911e416" publication-type="other">
Kaddaras, James and Elisabeth Rhyne (2004), "Characteristics of Equity Capital in Microfinance", Coun-
cil of Microfinance Equity Funds</mixed-citation>
         </ref>
         <ref id="d911e426a1310">
            <mixed-citation id="d911e430" publication-type="other">
King, Bradley (2005), "Investment Benchmarks for MicroFinance Institutions", MicroBanking Bulletin, p 22</mixed-citation>
         </ref>
         <ref id="d911e437a1310">
            <mixed-citation id="d911e441" publication-type="other">
Krauss, Nicolas (2006), "The Systemic Risk of Microfinance", New York University</mixed-citation>
         </ref>
         <ref id="d911e448a1310">
            <mixed-citation id="d911e452" publication-type="other">
McCarter, Elissa (2002), "Tying the Knot: A Guide to Mergers in Microfinance", Catholic Relief Services
Microfinance Unit</mixed-citation>
         </ref>
         <ref id="d911e463a1310">
            <mixed-citation id="d911e467" publication-type="other">
Meehan, Jennifer (2004), "Tapping the Financial Markets for Microfinance", Grameen Foundation Work-
ing Papers</mixed-citation>
         </ref>
         <ref id="d911e477a1310">
            <mixed-citation id="d911e481" publication-type="other">
MicroBanking Bulletin (2005), "Benchmark Tables", Number 11</mixed-citation>
         </ref>
         <ref id="d911e488a1310">
            <mixed-citation id="d911e492" publication-type="other">
Rhyne, Elisabeth (2005), "Perspectives from the Council of Microfinance Equity Funds", Small Enterprise
Development Journal, Volume 16, Number 1, pp 8-16</mixed-citation>
         </ref>
         <ref id="d911e502a1310">
            <mixed-citation id="d911e506" publication-type="other">
Silva, Alex (2005), "Investing in Microfinance - Profund's Story", Small Enterprise Development Journal,
Volume 16, Number 1, pp 17-29</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
