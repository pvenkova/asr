<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">easteconj</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000574</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Eastern Economic Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Palgrave Macmillan</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00945056</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19394632</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">20642497</article-id>
         <article-id pub-id-type="pub-doi">10.1057/eej.2008.28</article-id>
         <title-group>
            <article-title>Institutions and Human Progress: An Analysis of International Pooled Data</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nathan J.</given-names>
                  <surname>Ashby</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>7</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">35</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i20642490</issue-id>
         <fpage>396</fpage>
         <lpage>414</lpage>
         <permissions>
            <copyright-statement>Copyright 2009 Eastern Economic Association (EEJ)</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/20642497"/>
         <abstract>
            <p>This study investigates the impact of the components of economic freedom (Economic Freedom of the World Index, EFW) on improvements in well-being using pooled data of 105 countries spaced at 5-year intervals between 1990 and 2000. The EFW as well as other control variables are regressed on the log-odds ratios of the Index of Human Progress (IHP), a measurement of progress constructed by the Fraser Institute, and its components. Extreme bound analysis demonstrates that countries with a higher quality of government, well-defined property rights, sound money, limited trade restrictions, and limited regulation experience higher levels of human progress as measured by the IHP.</p>
         </abstract>
         <kwd-group>
            <kwd>institutions</kwd>
            <kwd>economic freedom</kwd>
            <kwd>progress</kwd>
            <kwd>H100</kwd>
            <kwd>F100</kwd>
            <kwd>P00</kwd>
            <kwd>O340</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d733e162a1310">
            <label>1</label>
            <mixed-citation id="d733e169" publication-type="other">
Gwartney and Lawson [2006].</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d733e185a1310">
            <mixed-citation id="d733e189" publication-type="other">
Acemoglu, Daron, Simon Johnson, and James Robinson. 2001. The Colonial Origins of Comparative
Development: An Empirical Investigation. American Economic Review, 91(5): 1369-1401.</mixed-citation>
         </ref>
         <ref id="d733e199a1310">
            <mixed-citation id="d733e203" publication-type="other">
Ayal, Eliezer B., and Georgios Karras. 1998. Components of Economic Freedom and Growth. The
Journal of Developing Areas, 32(3): 327-338.</mixed-citation>
         </ref>
         <ref id="d733e213a1310">
            <mixed-citation id="d733e217" publication-type="other">
Bauer, Peter. 2000. From Subsistence to Exchange: And Other Essays. Princeton: Princeton University
Press.</mixed-citation>
         </ref>
         <ref id="d733e227a1310">
            <mixed-citation id="d733e231" publication-type="other">
Carlsson, Fredrik, and Susanna Lundstrom. 2002. Economic Freedom and Growth: Decomposing the
Effects. Public Choice, 112(3-4): 335-344.</mixed-citation>
         </ref>
         <ref id="d733e242a1310">
            <mixed-citation id="d733e246" publication-type="other">
Demsetz, Harold. 1967. Toward a Theory of Property Rights. American Economic Review, 57(2):
347-359.</mixed-citation>
         </ref>
         <ref id="d733e256a1310">
            <mixed-citation id="d733e260" publication-type="other">
—. 2002. Toward a Theory of Property Rights II: The Competition Between Private and Collective
Ownership. Journal of Legal Studies, 31(2): S653-S672.</mixed-citation>
         </ref>
         <ref id="d733e270a1310">
            <mixed-citation id="d733e274" publication-type="other">
Easterly, William, and Ross Levine. 1997. Africa's Growth Tragedy. Quarterly Journal of Economics,
112(4): 1203-1250.</mixed-citation>
         </ref>
         <ref id="d733e284a1310">
            <mixed-citation id="d733e288" publication-type="other">
Ernes, Joel, and Tony Hahn. 2001. Measuring Development: An Index of Human Progress. Public Policy
Sources, [A Fraser Institute Occasional Paper], 36: 1-63.</mixed-citation>
         </ref>
         <ref id="d733e298a1310">
            <mixed-citation id="d733e302" publication-type="other">
Esposto, Alfred G., and Peter A. Zaleski. 1999. Economic Freedom and the Quality of Life: An Empirical
Analysis. Constitutional Political Economy, 10(2): 185-197.</mixed-citation>
         </ref>
         <ref id="d733e312a1310">
            <mixed-citation id="d733e316" publication-type="other">
Far, W. Ken, Richard A. Lord, and J.Larry Wolfenbarger. 1998. Economic Freedom, Political Freedom,
and Economic Well-being: A Causality Analysis. Cato Journal, 18(2): 247-262.</mixed-citation>
         </ref>
         <ref id="d733e327a1310">
            <mixed-citation id="d733e331" publication-type="other">
Gwartney, James, and Robert Lawson. 2006. Economic Freedom of the World 1970-2004. Vancouver, BC:
Fraser Institute.</mixed-citation>
         </ref>
         <ref id="d733e341a1310">
            <mixed-citation id="d733e345" publication-type="other">
Heckelman, Jac C., and M.D. Stroup. 2000. Which Economic Freedoms Contribute to Growth? Kyklos,
53(4): 527-544.</mixed-citation>
         </ref>
         <ref id="d733e355a1310">
            <mixed-citation id="d733e359" publication-type="other">
Hewson, Paul. 2004. Crumbs from Your Table, In How to Dismantle an Atomic Bomb. London, UK:
Universal Music Publishing.</mixed-citation>
         </ref>
         <ref id="d733e369a1310">
            <mixed-citation id="d733e373" publication-type="other">
Knack, Stephen. 1996. Institution and the Convergence Hypothesis: The Cross National Evidence. Public
Choice, 87(3-4): 207-228.</mixed-citation>
         </ref>
         <ref id="d733e383a1310">
            <mixed-citation id="d733e387" publication-type="other">
Knack, Stephen, and Philip Keefer. 1995. Institutions and Economic Performance: Cross-Country Tests
Using Alternative Institutional Measures. Economics and Politics, 7(3): 207-227.</mixed-citation>
         </ref>
         <ref id="d733e397a1310">
            <mixed-citation id="d733e401" publication-type="other">
—. 1997. Does Social Capital Have an Economic Payoff? A Cross-Country Investigation. Quarterly
Journal of Economics, 112(4): 1251-1288.</mixed-citation>
         </ref>
         <ref id="d733e412a1310">
            <mixed-citation id="d733e416" publication-type="other">
Levine, Ross, and David Renelt. 1992. A Sensitivity Analysis of Cross-Country Growth Regressions.
American Economic Review, 82(4): 942-963.</mixed-citation>
         </ref>
         <ref id="d733e426a1310">
            <mixed-citation id="d733e430" publication-type="other">
McMahon, Fred. 2002. Index of Human Progress. Vancouver, BC: Fraser Institute.</mixed-citation>
         </ref>
         <ref id="d733e437a1310">
            <mixed-citation id="d733e441" publication-type="other">
North, Douglass. 1990. Institutions, Institutional Change and Economic Performance. Cambridge:
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d733e451a1310">
            <mixed-citation id="d733e455" publication-type="other">
North, Douglass. 1991. Institutions. Journal of Economic Perspectives, 5(1): 97-112.</mixed-citation>
         </ref>
         <ref id="d733e462a1310">
            <mixed-citation id="d733e466" publication-type="other">
Norton, Seth. 1998. Poverty, Property Rights and Human Well-Being: A Cross-National Study. Cato
Journal, 22(2): 233-245.</mixed-citation>
         </ref>
         <ref id="d733e476a1310">
            <mixed-citation id="d733e480" publication-type="other">
—. 2003. Economic Institutions and Human Well-Being: A Cross-National Analysis. Eastern
Economic Journal, 29(1): 23-40.</mixed-citation>
         </ref>
         <ref id="d733e491a1310">
            <mixed-citation id="d733e495" publication-type="other">
Political Risk Services. 2004. International Country Risk Guide. MD: IRIS at University of Maryland.</mixed-citation>
         </ref>
         <ref id="d733e502a1310">
            <mixed-citation id="d733e506" publication-type="other">
Sachs, Jeffrey D., and Andrew M. Warner. 1997. Fundamental Sources of Long-Run Growth. American
Economic Review, 87(2): 184-188.</mixed-citation>
         </ref>
         <ref id="d733e516a1310">
            <mixed-citation id="d733e520" publication-type="other">
Scully, Gerald W. 1997. Rule and Policy Spaces and Economic Progress: Lessons for Third Worlds
Countries. Public Choice, 90(1-4): 311-324.</mixed-citation>
         </ref>
         <ref id="d733e530a1310">
            <mixed-citation id="d733e534" publication-type="other">
Sen, Amartya. 1999. Development As Freedom. New York, NY: Random House.</mixed-citation>
         </ref>
         <ref id="d733e541a1310">
            <mixed-citation id="d733e545" publication-type="other">
Smith, Adam. 1776, 1937. An Inquiry into the Nature and Causes of the Wealth of Nations. New York:
Modern Library.</mixed-citation>
         </ref>
         <ref id="d733e555a1310">
            <mixed-citation id="d733e559" publication-type="other">
Stigler, George J. 1971. The Economic Theory of Regulation. Bell Journal of Economics and Management
Science, 2(1): 3-21.</mixed-citation>
         </ref>
         <ref id="d733e570a1310">
            <mixed-citation id="d733e574" publication-type="other">
United Nations Development Programme [UNDP]. 1999. Human Development Report 1999. Oxford:
Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d733e584a1310">
            <mixed-citation id="d733e588" publication-type="other">
—. 2001. Human Development Report 2001. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d733e595a1310">
            <mixed-citation id="d733e599" publication-type="other">
World Bank. 2003. World Development Indicators. Washington, DC: ESDS International, University of
Manchester.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
