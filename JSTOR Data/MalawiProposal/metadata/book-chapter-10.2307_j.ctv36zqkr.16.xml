<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctv36zqkr</book-id>
      <subj-group>
         <subject content-type="call-number">JC311 .W464 2002</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">NATIONALISM</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">NATIONALISM</subject>
         <subj-group>
            <subject content-type="lcsh">HISTORY</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Who We Are</book-title>
         <subtitle>A History of Popular Nationalism</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>WIEBE</surname>
               <given-names>ROBERT H.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>05</day>
         <month>06</month>
         <year>2018</year>
      </pub-date>
      <isbn content-type="epub">9780691188676</isbn>
      <isbn content-type="epub">069118867X</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>PRINCETON; OXFORD</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2002</copyright-year>
         <copyright-holder>PRINCETON UNIVERSITY PRESS</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctv36zqkr"/>
      <abstract abstract-type="short">
         <p>How did educated Westerners make an enemy of an inspiration that has changed the lives of billions? Why is nationalism synonymous with atavism, fanaticism, xenophobia, and bloodshed? In this book, Robert Wiebe argues that we too often conflate nationalism with what states do in its name. By indiscriminately blaming it for terrorism, ethnic cleansing, and military thuggery, we avoid reckoning with nationalism for what it is: the desire among people who believe they share a common ancestry and destiny to live under their own government on land sacred to their history.</p>
         <p>For at least a century and a half, nationalism has been an effective answer to basic questions of identity and connection in a fluid world. It quiets fears of cultural disintegration and allows people to pursue closer bonds and seek freedom. By looking at nationalism in this clearer light and by juxtaposing it with its two great companion and competitor movements--democracy and socialism--Wiebe is able to understand nationalism's deep appeal and assess its historical record.</p>
         <p>Because Europeans and their kin abroad monopolized nationalism before World War I, Wiebe begins with their story, identifying migration as a motive force and examining related developments in state building, race theory, church ambition, and linguistic innovation. After case studies of Irish, German, and Jewish nationalism, Wiebe moves to the United States. He discusses America's distinctive place in transatlantic history, emphasizing its liberal government, cultural diversity, and racism. He then traces nationalism's spread worldwide, evaluating its adaptability and limits on that adaptability. The state-dominated nationalism of Japan, Turkey, and Mexico are considered, followed by Pan-Africanism and Nigeria's anticolonial-postcolonial nationalism. Finally, Wiebe shows how nationalism became integrated into a genuinely global process by the 1970s, only to find itself competing at a disadvantage with god- and gun-driven alternatives.</p>
         <p>This book's original answers to imperative questions will meet with deep admiration and controversy. They will also change the terms on which nationalism is debated for years to come.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Bass Warner</surname>
                           <given-names>Sam</given-names>
                           <suffix>Jr.</suffix>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>vii</fpage>
                  <abstract>
                     <p>I first met Bob Wiebe in 1972 when he came to teach at Harvard University for a year. At that time, five years after the publication of<italic>The Search for Order</italic>, he was the star among American historians. Throughout his career he was continually sought for panels and presentations. His gentle manner, attentive listening, and willingness to attempt large synthetic hypotheses when we all cowered within our specialties never failed to draw our admiration.</p>
                     <p>In 1972 some magic drew us together as fast friends. From this vantage point I learned of Bob’s growing frustration that no one was listening. Although</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.4</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Sheehan</surname>
                           <given-names>James J.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
                  <abstract>
                     <p>Writers on nationalism can be divided into two diverse but distinguishable groups. The first is composed of the nationalists themselves, who view nationalism as a natural, irresistible force—the expression of a deeply rooted collective identity formed by language, ethnicity, religion, history. National history, therefore, is the history o f the nation’s growing consciousness of its o w n existence and the fulfillment, often against terrible odds, of its common destiny, which usually means the formation of a territorial state. The second group is made up of nationalism’s critics and victims, who emphasize its historicity, artificiality, sometimes even its pathology.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xv</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.6</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>xix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.7</book-part-id>
                  <title-group>
                     <label>Chapter 1</label>
                     <title>Thinking about Nationalism</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>How did educated Westerners come to make enemies of an inspiration that has changed the lives of billions? It was not always so. At the turn of the twentieth century the philosopher William James judged “the attempt of a people long enslaved to attain the possession of itself, to organize its laws and government, to be free to follow its internal destinies, according to its own ideals . . . the sacredest thing in this great human world.”¹ Championing just such causes made Woodrow Wilson a global hero. But disillusionment after the First World War turned to revulsion after the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.8</book-part-id>
                  <title-group>
                     <label>Chapter 2</label>
                     <title>European Origins</title>
                  </title-group>
                  <fpage>12</fpage>
                  <abstract>
                     <p>During the long nineteenth century from the French Revolution to the First World War, nationalism was a monopoly of European societies. The very few exceptions, notably Japan, only highlighted the rule. Otherwise, nationalism outside Europe remained the province of tiny elites, borrowing heavily from Western ideas, who could not or did not want to spark popular interest. On the other side of the Great War, when nationalist movements did spread globally, Europe generated no new ones. As old ones revived or remade themselves, they were playing out stories already underway. The nineteenth century in Europe, in other words, marked out</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.9</book-part-id>
                  <title-group>
                     <label>Chapter 3</label>
                     <title>Changing Contexts</title>
                  </title-group>
                  <fpage>37</fpage>
                  <abstract>
                     <p>In two fundamental ways, the company nationalism kept shaped its history. One of these crucial contexts linked nationalism with the other great popular movements of the nineteenth century, socialism and democracy. A second context interwove changes in nationalism with changes in the other major dividers in nineteenth-century Europe: language, race, religion, and above all the state. In both cases, these interrelationships became tighter and their consequences deeper in the four decades that led up to the outbreak of the Great War in 1914. During those years, the course of socialism, nationalism, and democracy, the trio of movements that was now</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.10</book-part-id>
                  <title-group>
                     <label>Chapter 4</label>
                     <title>The Case of the United States</title>
                  </title-group>
                  <fpage>63</fpage>
                  <abstract>
                     <p>As an extension of Europe, the settlements that became the United States shared in the transformation that accompanied migration. On both sides of the Atlantic, democracy, socialism, and nationalism developed on roughly the same schedule; none had meaning without reference to the other two. In America as in Europe, the degree to which each in the trio was absorbed into a central state—and the degree to which nationalism especially was layered by influences from religion, race, and language—best accounted for any of the three movements’ softness or hardness, its tendency toward accommodation on one hand or exclusion on</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.11</book-part-id>
                  <title-group>
                     <label>Chapter 5</label>
                     <title>Climax in Europe</title>
                  </title-group>
                  <fpage>97</fpage>
                  <abstract>
                     <p>Between the 1870s and the 1940s, European states old and new, large and small, used the appeals of nationalism to mobilize power, only to unleash destructive forces on one another that crippled the continent and simultaneously released drives for freedom on a global scale. Before 1914, however, these consequences would have been hard to predict. In the three decades prior to the Great War, the major European states expanded as they centralized, adding 10 million square miles to their colonial empires and sucking an ever larger proportion of the world’s resources into their urban-industrial systems. Where nationalist, democratic, or socialist</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.12</book-part-id>
                  <title-group>
                     <label>Chapter 6</label>
                     <title>Nationalism Worldwide</title>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>As the pulse of nationalism quickened across Europe between 1880 and 1920, interest in it spread globally. By the beginning of the twentieth century, nationalist aspirations had currency among people everywhere who found Europe’s might by turns intimidating, horrifying, and fascinating. While Europeans watched and copied one another, handfuls of Chinese, Filipinos, Indians, Iraqis, Palestinians, and many others watched and copied, too. Rather than alienated intellectuals fleeing their failed cultures, as some Western scholars would have it, they were by and large questing individuals who imagined using the techniques of European power against Europeans and in the process preserving the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.13</book-part-id>
                  <title-group>
                     <label>Chapter 7</label>
                     <title>Global Nationalism</title>
                  </title-group>
                  <fpage>182</fpage>
                  <abstract>
                     <p>In a transformation that pivoted o n the years 1967 to 1972, nationalism changed from a worldwide spread of many movements into a global phenomenon with many variations. Each nationalist movement still had its own story, of course. Nevertheless, during these years, the simultaneous eruptions that brought nationalism back to Europe, spread it to North America, and kept it alive in scores of postcolonial settings rendered it implausible to think of nationalism as so many events that just happened to crop up everywhere at the same time. To see what any explanation of this extraordinary flourishing has to reckon with,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.14</book-part-id>
                  <title-group>
                     <label>Chapter 8</label>
                     <title>Thinking about the Future</title>
                  </title-group>
                  <fpage>211</fpage>
                  <abstract>
                     <p>By the end of the twentieth century the great trio—democracy, socialism, and nationalism—no longer structured public life in any major portion of the world. They had risen together; they fell together. What had once been primary solutions for Europeans and a menu of alternatives for people around the world slipped into a crowd of options. Each continued to have its partisans; each powered movements here and there. But an era of dominance, when the three of them shaped popular aspirations on a global scale, had passed.</p>
                     <p>A decade earlier, as the Cold War ended with the disintegration of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.15</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>221</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.16</book-part-id>
                  <title-group>
                     <title>Bibliographical Essay</title>
                  </title-group>
                  <fpage>229</fpage>
                  <abstract>
                     <p>My preparation has been one prolonged lesson at the hands of scholars whose evidence underlies this book and whose interpretations of it have stimulated me to formulate my own. If in the end I did not learn enough, it was certainly not the fault of my teachers. By no means do all of their names appear here, where my primary purpose is to invite further reading. To those missing mentors, I can only offer a general heartfelt thanks, with apologies for the need to be selective.</p>
                     <p>The contemporary study of nationalism dates from Karl W. Deutsch, Nationalism and Social Communication</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctv36zqkr.17</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>269</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
