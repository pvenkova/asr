<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt234w6z</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1rfsnzz</book-id>
      <subj-group>
         <subject content-type="call-number">RM171.S34 2013</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Blood</subject>
         <subj-group>
            <subject content-type="lcsh">Transfusion</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
               <subj-group>
                  <subject content-type="lcsh">History</subject>
               </subj-group>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Blood banks</subject>
         <subj-group>
            <subject content-type="lcsh">Risk management</subject>
            <subj-group>
               <subject content-type="lcsh">Africa</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">AIDS (Disease)</subject>
         <subj-group>
            <subject content-type="lcsh">Epidemiology</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Health Sciences</subject>
         <subject>History</subject>
         <subject>Technology</subject>
         <subject>Public Health</subject>
         <subject>History of Science &amp; Technology</subject>
      </subj-group>
      <book-title-group>
         <book-title>The History of Blood Transfusion in Sub-Saharan Africa</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Schneider</surname>
               <given-names>William H.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="series-editor" id="contrib2">
            <role>Series editor:</role>
            <name name-style="western">
               <surname>Webb</surname>
               <given-names>James L. A.</given-names>
               <suffix>Jr.</suffix>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>15</day>
         <month>07</month>
         <year>2013</year>
      </pub-date>
      <isbn content-type="epub">9780821444535</isbn>
      <isbn content-type="epub">0821444530</isbn>
      <publisher>
         <publisher-name>Ohio University Press</publisher-name>
         <publisher-loc>Athens</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2013</copyright-year>
         <copyright-holder>Ohio University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1rfsnzz"/>
      <abstract abstract-type="short">
         <p>This first extensive study of the practice of blood transfusion in Africa traces the history of one of the most important therapies in modern medicine from the period of colonial rule to independence and the AIDS epidemic. The introduction of transfusion held great promise for improving health, but like most new medical practices, transfusion needed to be adapted to the needs of sub-Saharan Africa, for which there was no analogous treatment in traditional African medicine.This otherwise beneficent medical procedure also created a "royal road" for microorganisms, and thus played a central part in the emergence of human immune viruses in epidemic form. As with more developed health care systems, blood transfusion practices in sub-Saharan Africa were incapable of detecting the emergence of HIV. As a result, given the wide use of transfusion, it became an important pathway for the initial spread of AIDS. Yet African health officials were not without means to understand and respond to the new danger, thanks to forty years of experience and a framework of appreciating long-standing health risks. The response to this risk, detailed in this book, yields important insight into the history of epidemics and HIV/AIDS.Drawing on research from colonial-era governments, European Red Cross societies, independent African governments, and directly from health officers themselves, this book is the only historical study of the practice of blood transfusion in Africa.</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.3</book-part-id>
                  <title-group>
                     <title>ACKNOWLEDGMENTS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.4</book-part-id>
                  <title-group>
                     <title>ABBREVIATIONS</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The history of blood transfusion is a subject of interest to historians of medicine and scholars of Africa, as well as those who work in and follow developments in global health. Like today, the introduction of new medical practices in Africa at the beginning of colonial rule held great promise for generally improving health. In the case of transfusion, as this study shows, there were advances to be sure, but also some setbacks. Most important, blood transfusion could not be put in place quickly or exactly as it was practiced in the countries of Europe or in other developed countries.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>BLOOD TRANSFUSION BEFORE THE SECOND WORLD WAR</title>
                  </title-group>
                  <fpage>9</fpage>
                  <abstract>
                     <p>Blood transfusion is one of the most important lifesaving discoveries of modern scientific medicine. The first effective procedures were demonstrated only at the beginning of the twentieth century, although attempts in its present form began shortly after William Harvey’s discovery of blood circulation, in the seventeenth century. Long before that, most cultures had recognized the significance of blood, which played a prominent role in many rituals and customs of healing.¹</p>
                     <p>The history of how blood transfusion was introduced to Africa is important beyond its dramatic role in saving lives. As a well-defined medical procedure that was unprecedented in traditional society,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>BLOOD TRANSFUSION FROM 1945 TO INDEPENDENCE</title>
                  </title-group>
                  <fpage>28</fpage>
                  <abstract>
                     <p>There was sufficient Western medical infrastructure to make blood transfusions possible in Africa between the world wars, but this did not immediately lead to large numbers of transfusions. The rapid increase came after the Second World War, for a number of reasons. The explanation of how this rapid growth happened in most colonies is best understood by the changes in general conditions that increased the number of hospitals and brought more doctors to Africa who were able to use transfusions.</p>
                     <p>The period after 1945 in the history of modern health care in Africa is usually subsumed together with the rest</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>BLOOD TRANSFUSION IN INDEPENDENT AFRICAN COUNTRIES</title>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>On December 2, 1964, President Léopold Sédar Senghor of Senegal inaugurated an expansion of the Centre national de transfusion sanguine in Dakar that doubled the size of the largest blood collection facility in sub-Saharan Africa. Funding came from the Fond éuropéen de développement d’outre-mer, which had been established by the six countries of the new European Common Market to provide development assistance to countries of Africa, the Caribbean, and the Pacific. Although the transfusion center had been created in 1951 to supply blood for all French West African colonies, by 1960 its facilities were at capacity just to meet the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>WHO GOT BLOOD?</title>
                     <subtitle>Indications for the Use of Blood Transfusion, 1945–2000</subtitle>
                  </title-group>
                  <fpage>106</fpage>
                  <abstract>
                     <p>Three general conditions are necessary for transfusions to take place: patients, a supply of blood, and the knowledge and ability to do transfusions. In sub-Saharan Africa the limiting determinant was usually a lack of persons with the knowledge and facilities to do transfusions. Although early in the twentieth century there were doctors and hospitals in Africa who practiced transfusion, their numbers were far fewer than both those in need of and those willing to donate blood. Especially in the case of the former, there is little evidence of unwilling patients being an impediment to the adoption of the practice. In</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>WHO GAVE BLOOD?</title>
                  </title-group>
                  <fpage>131</fpage>
                  <abstract>
                     <p>The previous chapter showed how the uses of blood transfusion were publicized in order to encourage blood donation. In Africa, as elsewhere, the question of who gives blood is of great importance for at least two reasons. First, in order to secure an adequate supply of blood it is crucial to know who gave and, even more important, why blood was given. Second, who donates blood is very relevant to the safety of blood transfusion, because people in certain circumstances have higher risk of carrying infections. So the question of who gave blood in the history of transfusion in Africa</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>BLOOD TRANSFUSION AND HEALTH RISK BEFORE AND AFTER THE AIDS EPIDEMIC</title>
                  </title-group>
                  <fpage>153</fpage>
                  <abstract>
                     <p>In the age of the global AIDS pandemic and the current concerns about blood safety, there is little need to justify a more detailed examination of the history of transfusion risk in Africa. Indeed, a crucial part of the initial research that confirmed the existence of HIV/AIDS in Africa was done in the blood banks of hospitals in Kigali, Rwanda, and Kinshasa.¹ Other evidence of the keen awareness of the link between AIDS and transfusion includes the fact that blood donors were one of the earliest groups widely tested for HIV, the first successful measure to limit the spread of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>AFRICAN BLOOD TRANSFUSION IN THE CONTEXT OF GLOBAL HEALTH</title>
                  </title-group>
                  <fpage>173</fpage>
                  <abstract>
                     <p>John Keegan has observed that only anesthesia and antibiotics have been the equals of transfusion in saving lives in wartime.¹ The history of transfusion in Africa is therefore best understood by keeping in mind several features of the procedure that have made it one of a handful of the most important therapies discovered in modern times. First among these features is its clearly demonstrable and comprehensible effectiveness. Dramatic resuscitation is almost immediately produced, and the logic of replacing a lost substance of such universally recognizable importance is understandable to patient and donor alike.</p>
                     <p>In Africa, transfusion was introduced as part</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.13</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>181</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.14</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>219</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1rfsnzz.15</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>235</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
