<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">amerlaweconrev</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009111</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>American Law and Economics Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Oxford University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14657252</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">14657260</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42705572</article-id>
         <title-group>
            <article-title>Do Independent Prosecutors Deter Political Corruption? An Empirical Evaluation across Seventy-eight Countries</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Anne</given-names>
                  <surname>van Aaken</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Lars P.</given-names>
                  <surname>Feld</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Stefan</given-names>
                  <surname>Voigt</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">12</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40101531</issue-id>
         <fpage>204</fpage>
         <lpage>244</lpage>
         <permissions>
            <copyright-statement>Copyright © 2010 American Law and Economics Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42705572"/>
         <abstract>
            <p>It is hypothesized that prosecution agencies that are dependent on the executive have less incentives to prosecute crimes committed by government members that in turn increases their incentives to commit such crimes. Here, this hypothesis is put to an empirical test focusing on a particular kind of crime, namely corruption. In order to test it, it was necessary to create an indicator measuring de jure as well as de facto independence of the prosecution agencies. The regressions show that de facto independence of prosecution agencies robustly reduces corruption of officials.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d274e231a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d274e238" publication-type="other">
Posner (1994)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d274e244" publication-type="other">
Glaeser et
al. (2000)</mixed-citation>
            </p>
         </fn>
         <fn id="d274e254a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d274e261" publication-type="other">
Dressier (2002)</mixed-citation>
            </p>
         </fn>
         <fn id="d274e268a1310">
            <label>10</label>
            <p>
               <mixed-citation id="d274e275" publication-type="other">
Feld and Voigt (2003)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d274e291a1310">
            <mixed-citation id="d274e295" publication-type="other">
Aidt, Toke S. 2003. "Economic Analysis of Corruption: A Survey," 113 Economic
Journal F632-F652.</mixed-citation>
         </ref>
         <ref id="d274e305a1310">
            <mixed-citation id="d274e309" publication-type="other">
Banks, Arthur. 2004. Banks' Cross-national Time-series Data Archive. Binghamton,
NY: Databanks International.</mixed-citation>
         </ref>
         <ref id="d274e319a1310">
            <mixed-citation id="d274e323" publication-type="other">
Beck, Thomas, George Clarke, Alberto Groff, Phil Keefer, and Patrick Walsh.
2000. New Tools and New Tests in Comparative Political Economy: The Data-
base of Political Institutions. Washington: The World Bank.</mixed-citation>
         </ref>
         <ref id="d274e336a1310">
            <mixed-citation id="d274e340" publication-type="other">
Central Intelligence Agency. 2005. The 2004 World Factbook. Available at: https://
www.cia.gov/library/publications/the-world-factbook/index.html.</mixed-citation>
         </ref>
         <ref id="d274e351a1310">
            <mixed-citation id="d274e355" publication-type="other">
Chang, Eric, and Miriam Golden. 2007. "Electoral Systems, District Magnitude and
Corruption," 37(1) British Journal of Political Science 1115-137.</mixed-citation>
         </ref>
         <ref id="d274e365a1310">
            <mixed-citation id="d274e369" publication-type="other">
Dressier, J., ed. 2002. Encyclopedia of Crime and Justice. 2nd ed. New York:
Macmillan.</mixed-citation>
         </ref>
         <ref id="d274e379a1310">
            <mixed-citation id="d274e383" publication-type="other">
Feld, Lars P., and Stefan Voigt. 2003. "Economic Growth and Judicial Independ-
ence: Cross Country Evidence Using a New Set of Indicators," 19(3) European
Journal of Political Economy 3497-527.</mixed-citation>
         </ref>
         <ref id="d274e396a1310">
            <mixed-citation id="d274e400" publication-type="other">
Forum of Federations. 2002. List of Federal Countries. Available at: http://www.
forumfed.org/federalism/entrylist.asp?lang=en.</mixed-citation>
         </ref>
         <ref id="d274e410a1310">
            <mixed-citation id="d274e414" publication-type="other">
Freedom House. 2002. The Annual Survey of Press Freedom. Available at: http://
www.freedomhouse.org/pfs2002/pfs2002.pdf.</mixed-citation>
         </ref>
         <ref id="d274e424a1310">
            <mixed-citation id="d274e428" publication-type="other">
Glaeser, Edward, Daniel Kessler, and Anne Piehl. 2000. "What do Prosecutors
Maximize? An Analysis of the Federalization of Drug Crimes," 2(2) American
Law and Economics Review 2259-290.</mixed-citation>
         </ref>
         <ref id="d274e442a1310">
            <mixed-citation id="d274e446" publication-type="other">
Golden, Miriam, and Lucio Picci. 2005. "Proposal for a New Measure of Corrup-
tion", Illustrated with Italian Data," Economics &amp; Politics 37-75.</mixed-citation>
         </ref>
         <ref id="d274e456a1310">
            <mixed-citation id="d274e460" publication-type="other">
Heston, Alan, Summers Robert, and Aten Bettina. 2001. "Penn World Table Ver-
sion 6.0. " Center for International Comparisons at the University of Pennsylva-
nia (CICUP).</mixed-citation>
         </ref>
         <ref id="d274e473a1310">
            <mixed-citation id="d274e477" publication-type="other">
Kaufmann, Daniel, Kraay Aart, and Mastruzzi Massimo. 2007. Governance Matters
VII: Aggregate and Individual Governance Indicators 1996-2007. Available at:
www.govindicators.org.</mixed-citation>
         </ref>
         <ref id="d274e490a1310">
            <mixed-citation id="d274e494" publication-type="other">
Kurtz, Marcus, and Andrew Schrank. 2007. "Growth and Governance: Models,
Measures, and Mechanisms," 69(2) Journal of Politics 2538-554.</mixed-citation>
         </ref>
         <ref id="d274e504a1310">
            <mixed-citation id="d274e508" publication-type="other">
Lambsdorff, Johann Graf. 2005. "Consequences and Causes of Corruption-
What do we Know from a Cross-section of Countries?" in S. Rose-Ackerman,
ed., International Handbook on the Economics of Corruption. Cheltenham,
UK: Edward Elgar. p. 3-51.</mixed-citation>
         </ref>
         <ref id="d274e524a1310">
            <mixed-citation id="d274e528" publication-type="other">
La Porta, Florencio Lopez-de-Silanes Rafael, Andrei Shleifer, and Robert Vishny.
1999. "The Quality of Government," 15(1) The Journal of Law, Economics &amp;
Organization 1222-279.</mixed-citation>
         </ref>
         <ref id="d274e542a1310">
            <mixed-citation id="d274e546" publication-type="other">
Marshall, Monty G., and Keith Jaggers. 2002. "Polity IV Project: Political Regime
Characteristics and Transitions," Dataset Users' Manual. College Park, MD:
University of Maryland.</mixed-citation>
         </ref>
         <ref id="d274e559a1310">
            <mixed-citation id="d274e563" publication-type="other">
Mauro, Paolo. 1995. "Corruption and Growth," 110 Quarterly Journal of Econom-
ics 681-712.</mixed-citation>
         </ref>
         <ref id="d274e573a1310">
            <mixed-citation id="d274e577" publication-type="other">
Olken, Benjamin A. 2007. "Monitoring Corruption: Evidence from a Field Experi-
ment in Indonesia," 115(2) Journal of Political Economy 2200-249.</mixed-citation>
         </ref>
         <ref id="d274e587a1310">
            <mixed-citation id="d274e591" publication-type="other">
Panizza, Ugo. 2001. "Electoral Rules, Political Systems, and Institutional Quality,"
13(3) Economics &amp; Politics 3311-342.</mixed-citation>
         </ref>
         <ref id="d274e601a1310">
            <mixed-citation id="d274e605" publication-type="other">
Persson, Torsten, and Guido Tabellini. 2003. Dataset Used for Economic Effects
of Constitutions. Available at: www.igier.uni-bocconi.it/folder.php?vedi=823&amp;
tbn=albero&amp;id_folder=l 80.</mixed-citation>
         </ref>
         <ref id="d274e618a1310">
            <mixed-citation id="d274e622" publication-type="other">
Persson, Torsten, Guido Tabellini, and Francesco Trebbi. 2003. "Electoral Rules and
Corruption," 1(4) Journal of the European Economic Association 4958-989.</mixed-citation>
         </ref>
         <ref id="d274e633a1310">
            <mixed-citation id="d274e637" publication-type="other">
Political Risk Services International. 2007. International Country Risk Guide.
Available at: www.prsgroup.com.</mixed-citation>
         </ref>
         <ref id="d274e647a1310">
            <mixed-citation id="d274e651" publication-type="other">
Posner, Richard. 1994. ""What do Judges and Justices Maximize?" (The Same
Thing Everybody Else Does)," 3 Supreme Court Economic Review 1-41.</mixed-citation>
         </ref>
         <ref id="d274e661a1310">
            <mixed-citation id="d274e665" publication-type="other">
Seldadyo, Harry, and Jakob De Haan. 2005. The Determinants of Corruption: A
Reinvestigation. Paper Presented at the EPCS-2005 Conference. Durham,
NC, USA.</mixed-citation>
         </ref>
         <ref id="d274e678a1310">
            <mixed-citation id="d274e682" publication-type="other">
Serra, Danila. 2006. "Empirical Determinants of Corruption: A Sensitivity Analysis,"
126 Public Choice 225-256.</mixed-citation>
         </ref>
         <ref id="d274e692a1310">
            <mixed-citation id="d274e696" publication-type="other">
Transparency International. 2007. Corruption Perceptions Indicator 2007.
Available at: http://www.transparency.org/policy_research/surveys_indices/
cpi/2007.</mixed-citation>
         </ref>
         <ref id="d274e709a1310">
            <mixed-citation id="d274e713" publication-type="other">
Treisman, Daniel. 2000. "The Causes of Corruption: A Cross-national Study," 76
Journal of Public Economics 399-457.</mixed-citation>
         </ref>
         <ref id="d274e724a1310">
            <mixed-citation id="d274e728" publication-type="other">
---. 2007. "What have we Learned About Causes of Corruption from Ten
Years of Cross-national Empirical Research?" 10 Annual Review of Political Sci-
ence 211-244.</mixed-citation>
         </ref>
         <ref id="d274e741a1310">
            <mixed-citation id="d274e745" publication-type="other">
van Aaken, Anne, Salzberger Eli, and Voigt Stefan. 2004. "The Prosecution of
Public Figures and the Separation of Powers. Confusion Within the Executive
Branch—A Conceptual Framework," 15(3) Constitutional Political Economy
3261-280.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
