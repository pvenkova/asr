<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">ethnography</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50011353</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Ethnography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>SAGE Publications</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14661381</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17412714</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">24047899</article-id>
         <title-group>
            <article-title>The Hollywood shtetl: From ethnic enclave to religious destination</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Iddo</given-names>
                  <surname>Tavory</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">11</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i24047107</issue-id>
         <fpage>89</fpage>
         <lpage>108</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/24047899"/>
         <abstract>
            <p>This article analyzes the relation between the development of religious institutions and neighborhood formation in the main Jewish Orthodox neighborhood of Los Angeles. The Melrose-La Brea neighborhood, an ethnically Jewish area since the 1930s, was transformed by religious place entrepreneurs from the East Coast in the 1970s. The emerging Orthodox community, however, was not principally populated by converting secular Jews. Instead, by developing prestigious institutions, religious place entrepreneurs made it into a viable destination for Orthodox Jews across the United States. Contrary to 'supply side' models developed in the sociology of religion, I then argue that institutional 'supply' may be successful without producing local 'religious demand'. Moving from a market metaphor to examine the role of far reaching networks, I contend that the success of religious place entrepreneurs in this case emerged from the relation between institutional development, the production of neighborhood identity, and population movements within an ethnic geography.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d400e117a1310">
            <label>6</label>
            <mixed-citation id="d400e124" publication-type="other">
Bourdieu's notion of fields (1985)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d400e140a1310">
            <mixed-citation id="d400e144" publication-type="other">
Adler-Huttler, M. (2008) 'A Mini-Biography of Grand Rabbi Eliezer Adler,
Zviller Rebbe, and a Mini-History of Chassidism in Los Angeles', un-
published manuscript.</mixed-citation>
         </ref>
         <ref id="d400e157a1310">
            <mixed-citation id="d400e161" publication-type="other">
Bourdieu, P. (1985) 'The Social Space and the Genesis of Groups', Theory and
Society 14: 723-44.</mixed-citation>
         </ref>
         <ref id="d400e171a1310">
            <mixed-citation id="d400e175" publication-type="other">
Brodkin, K. (1998) How Jews Became White Folk and What That Says About
Race in America. New Brunswick, NJ: Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="d400e185a1310">
            <mixed-citation id="d400e189" publication-type="other">
Dash Moore, D. (1994) To the Golden Cities: Pursuing the Jewish American
Dream in Miami and L.A. Cambridge, MA: Harvard University Press.</mixed-citation>
         </ref>
         <ref id="d400e200a1310">
            <mixed-citation id="d400e204" publication-type="other">
Gelfand, M.B. (1981) 'Chutzpah in El Dorado: Social Mobility of Jews in Los
Angeles 1900-1920', unpublished PhD dissertation, University of California,
Los Angeles.</mixed-citation>
         </ref>
         <ref id="d400e217a1310">
            <mixed-citation id="d400e221" publication-type="other">
Godfrey, B. (1988) Neighborhoods in Transition: The Making of San
Francisco's Ethnic and Nonconformist Communities. Berkeley: University of
California Press.</mixed-citation>
         </ref>
         <ref id="d400e234a1310">
            <mixed-citation id="d400e238" publication-type="other">
Helmreich, W.B. (1982) The World of The Yeshiva: An Intimate Portrait of
Orthodox Jewry. Hoboken, NJ: Ktav.</mixed-citation>
         </ref>
         <ref id="d400e248a1310">
            <mixed-citation id="d400e252" publication-type="other">
Home Owners Loan Corporation (HOLC) (1939) Area Description:
Beverly-Fuller District. Los Angeles, CA: HOLC.</mixed-citation>
         </ref>
         <ref id="d400e262a1310">
            <mixed-citation id="d400e266" publication-type="other">
Katz, J., P. Ibarra and M. Kusenbach (2008) 'Six Hollywoods Ethnographic
Project', unpublished manuscript.</mixed-citation>
         </ref>
         <ref id="d400e276a1310">
            <mixed-citation id="d400e280" publication-type="other">
Light, I.H. and E. Bonacich (1988) Immigrant Entrepreneurs: Koreans in Los
Angeles, 1965-1982. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d400e291a1310">
            <mixed-citation id="d400e295" publication-type="other">
Logan, J.R. and H.L. Molotch (1987) Urban Fortunes: The Political Economy
of Place. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d400e305a1310">
            <mixed-citation id="d400e309" publication-type="other">
McWilliams, C. (1946) Southern California: An Island on the Land. Layton,
UT: Gibbs Smith.</mixed-citation>
         </ref>
         <ref id="d400e319a1310">
            <mixed-citation id="d400e323" publication-type="other">
Phillips, B. (1986) 'Los Angeles Jewry: A Demographic Portrait', American
Jewish Yearbook.</mixed-citation>
         </ref>
         <ref id="d400e333a1310">
            <mixed-citation id="d400e337" publication-type="other">
Sherkat, D.E. and C.G. Ellison (1999) 'Recent Developments and Current
Controversies in the Sociology of Religion', Annual Review of Sociology 25:
363-94.</mixed-citation>
         </ref>
         <ref id="d400e350a1310">
            <mixed-citation id="d400e354" publication-type="other">
Stark, R. and R. Finke (2000) Acts of Faith: Explaining the Human Side of
Religion. Berkeley: University of California Press.</mixed-citation>
         </ref>
         <ref id="d400e364a1310">
            <mixed-citation id="d400e368" publication-type="other">
Vorspan, M. and L.P. Gartner (1970) History of the Jews in Los Angeles. San
Marino, CA: Huntington Library.</mixed-citation>
         </ref>
         <ref id="d400e379a1310">
            <mixed-citation id="d400e383" publication-type="other">
Young, P.V. (1928) 'The Reorganization of Jewish Family Life in America: A
Natural History of the Social Forces Governing the Assimilation of the
Jewish Immigrant', Social Forces 7(2): 238-44.</mixed-citation>
         </ref>
         <ref id="d400e396a1310">
            <mixed-citation id="d400e400" publication-type="other">
Young Israel Community Development Corporation (1982) 'Beverly-Fairfax
Neighborhood Revitalization Strategy: Final Report', Los Angeles, CA.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
