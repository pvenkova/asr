<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">jafricanlaw</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100202</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Journal of African Law</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Cambridge University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00218553</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41709961</article-id>
         <title-group>
            <article-title>Creating an Independent Traditional Court: A Study of Jopadhola Clan Courts in Uganda</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Maureen</given-names>
                  <surname>Owor</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2012</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">56</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40080242</issue-id>
         <fpage>215</fpage>
         <lpage>242</lpage>
         <permissions>
            <copyright-statement>© School of Oriental and African Studies, University of London 2012</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41709961"/>
         <abstract>
            <p>This article examines the contribution of clans (kinship institutions) to the administration of justice within the context of standards set out in the African regional human rights instruments. Field work on the Jopadhola of Eastern Uganda is drawn upon, to explore how clans reproduce their notion of an independent court using an abridged legal doctrine of separation of powers, and partially mimicking lower level government and judicial features. The field work also shows how clans accommodate interests of women and youth. Even so, clans retain a largely customary approach to the appointment, qualifications and tenure of court officials. The main findings lead to the conclusion that, by applying an "African" notion of human rights, clans have created traditional constructs of an independent court: one that is culturally appropriate for their indigenous communities.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1023e141a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d1023e148" publication-type="other">
GA res 2200A (XXI), 21 UN GAOR supp (no 16) at 52: UN doc A/6316 (1966).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e155a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1023e162" publication-type="other">
OAU doc CAB/LEG/67/3/Rev. 5 (1981), came into force on 21 October 1986.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e169a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1023e178" publication-type="other">
M Nowak United Nations Covenant on Civil and Political Rights - CCPR Commentary (2nd edi-
tion, 2005, NP Engel Publisher) at 319, para 25 and 321, para 28.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e187" publication-type="other">
7th United Nations Congress on the Prevention of Crime and Treatment of
Offenders, Milan 1985 and endorsed by GA res 40/32; 40/146.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e197" publication-type="other">
G Tumwine-Mukubwa
"Ruled from the grave: Challenging antiquated constitutional doctrines and values in
Africa" in J Oloka-Onyango (ed) Constitutionalism In Africa: Creating Opportunities, Fadng
Challenges (2004, Fountain Publishers) 287 at 295-99</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e212" publication-type="other">
G Kanyeihamba Constitutional
and Political History of Uganda: From 1894 to Present (2nd edition, 2010, LawAfrica
Publishing (U) Ltd) at 269-73</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e225a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1023e232" publication-type="other">
DOC/OS/(XXX)247 of 2003, reproduced in R Murray and M Evans (eds) Documents of the
African Commission on Human and Peoples' Rights (vol II 1999-2007, 2009, Hart
Publishing) 381.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e246a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1023e253" publication-type="other">
K Gyekye Tradition and Modernity: Philosophical Reflections on the African Experience (1997,
Oxford University Press) at 65.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e263a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1023e270" publication-type="other">
GA res 217A (III): UN doc A/810 at 71 (1948).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e277a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d1023e284" publication-type="other">
Uganda ratified the African Charter on 10 May 1986, deposited the instrument on 27 May
1986 and signed it on 18 August 1986.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e293" publication-type="other">
&lt;http://www.achpr.
org/instruments/achpr/ratification/&gt; (last accessed 3 July 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e303a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d1023e310" publication-type="other">
Art 123(1) and (2) of the Constitution chap 1 (2000 edition, Laws of Uganda),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e316" publication-type="other">
Constitution (Amendment) (no 2) Act 2005.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e323a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d1023e330" publication-type="other">
Under sec 2(a) of the Ratification of Treaties Act, chap 204 (2000),</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e337a1310">
            <label>11</label>
            <p>
               <mixed-citation id="d1023e344" publication-type="other">
P Wanda "The role of traditional courts in
Malawi" in P Takirambudde (ed) The Individual Under African Law (proceedings of the
first All Africa Law Conference, 11-16 October 1981) (1982, University of Swaziland) 78
at 79.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e361a1310">
            <label>12</label>
            <p>
               <mixed-citation id="d1023e368" publication-type="other">
M Baderin "Recent developments in the African regional human rights system" (2005) 5
Human Rights Law Review 117 at 126.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e378a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1023e385" publication-type="other">
P Kirchloff "Kinship organization: A study of terminology" (1932) 5 Africa: Journal of the
International African Institute 184 at 184-91.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e395a1310">
            <label>14</label>
            <p>
               <mixed-citation id="d1023e402" publication-type="other">
Local councils fall under the Local Governments Act chap 243 (2000) (LGA).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e408" publication-type="other">
Local Council Courts Act (LCCA) 13/2006.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e414" publication-type="other">
Superior courts
are delineated in art 129(2) of the Constitution and the Judicature Act chap 13 (2000).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e424" publication-type="other">
Magistrates courts are established by the Magistrates Courts Act chap 16 (MCA) (2000)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e430" publication-type="other">
Magistrates Courts Act (Amendment) Act 7 of 2007.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e437a1310">
            <label>15</label>
            <p>
               <mixed-citation id="d1023e444" publication-type="other">
PhD
thesis, University of Bristol, UK, 2009</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e454a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1023e461" publication-type="other">
Jo-Gem clan register 2006.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e468a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1023e475" publication-type="other">
Uganda Bureau of Statistics and International Livestock Research Institute Where are the
Poor? Mapping Patterns of Well-Being in Uganda (2003, Regal Press) at table 4.11 A.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e486a1310">
            <label>18</label>
            <p>
               <mixed-citation id="d1023e493" publication-type="other">
B Ogot History of the Southern Luo: Volume 1: Migration and Settlement 1500-1900 (1967, East
African Publishing House) at 85.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e503a1310">
            <label>19</label>
            <p>
               <mixed-citation id="d1023e510" publication-type="other">
H Mogensen "The resilience of Juok Confronting suffering in Eastern Uganda" (2002) 73
Africa: Journal of the International African Institute 420</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e520a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1023e527" publication-type="other">
Constitution of Tieng Adhola (2006 edition), chap 22, paras 25(14)(a), (b) and (c).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e533" publication-type="other">
Kwar Adhola. Interview with Mr MO: the Kwar Adhola (15 August 08).
Owor "Making international sentencing", above at note 15 at 195.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e543a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1023e550" publication-type="other">
N Sudarkasa "African and Afro-American family structure" in J Cole (ed) Anthropology for
the Nineties: Introductory Readings (1988, Free Press) 182 at 183-87 and 198-99.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e560a1310">
            <label>22</label>
            <p>
               <mixed-citation id="d1023e567" publication-type="other">
Owor "Making international sentencing", above at note 15 at 170-77;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e573" publication-type="other">
Ogot History of the
Southern Luo, above at note 18;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e582" publication-type="other">
F Burke Local Government and Politics in Uganda (1964,
Syracuse University Press) at 193, 196-98 and 240.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e592a1310">
            <label>23</label>
            <p>
               <mixed-citation id="d1023e599" publication-type="other">
T Elias The Nature of African Customary Law (1956, Manchester University Press) at 11-12,
30 and 216.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e610a1310">
            <label>24</label>
            <p>
               <mixed-citation id="d1023e617" publication-type="other">
"Umuntu ngumuntu ngabantu" [a person can only be a person through others]: Y Mokgoro
"Ubuntu and the law in South Africa" (1998) 1/1 Potchefitroom Electronic Law Journal 1 at
2, citing L Mbigi and J Maree Ubuntu: The Spirit of African Transformation Management
(1995, Sigma Press) at 1-7.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e632" publication-type="other">
Judge Mokgoro
in State v T Màkwanyane and M Mchunu CCT/3/94, delivered on 6 June 1995, 1 at 171,
para 308.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e645a1310">
            <label>25</label>
            <p>
               <mixed-citation id="d1023e652" publication-type="other">
R Mqeke "Customary law and human rights" (1996) 113 South African Law Journal 364 at
365, citing M Gluckman "Natural justice in Africa" (1964) 9 Natural Law Forum 22 at 25</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e661" publication-type="other">
K M'baye "The African conception of law" in R David (ed) International
Encyclopaedia of Comparative Law vol II chap 1 (1975, Martinus Nijhoff) at 138, para 225
and 148, para 257.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e674a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1023e681" publication-type="other">
Burke Local Government, above at note 22 at 240.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e688a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1023e695" publication-type="other">
A Allott "The people as law-makers: Custom, practice, and public opinion as sources of
Law in Africa and England" (1977) 21/1 Journal of African Law 1 at 8-10;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e704" publication-type="other">
Elias The
Nature of African Customary Law, above at note 23 at 198-99.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e714a1310">
            <label>28</label>
            <p>
               <mixed-citation id="d1023e721" publication-type="other">
Elias id, at 11-12,</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e728a1310">
            <label>29</label>
            <p>
               <mixed-citation id="d1023e735" publication-type="other">
L Mair Primitive Government (1970, The Scolar Press) at 49,</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e743a1310">
            <label>30</label>
            <p>
               <mixed-citation id="d1023e750" publication-type="other">
Burke Local Government, above at note 22 at 192.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e757a1310">
            <label>31</label>
            <p>
               <mixed-citation id="d1023e764" publication-type="other">
JMN Kakooza "Uganda's legal history in a nutshell" (1993) 1/1 M akerere Law Journal at 2,
cited in E Beyaraza Social Foundations of Law: A Philosophical Analysis (2003, Law
Development Centre Publishers) at 112.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e777a1310">
            <label>32</label>
            <p>
               <mixed-citation id="d1023e784" publication-type="other">
A Oboth-Ofumbi Lwo (Ludama ) Uganda: History and Customs of the Jo Padhola (1960, Eagle
Press) at 67-69.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e794a1310">
            <label>33</label>
            <p>
               <mixed-citation id="d1023e801" publication-type="other">
H Driberg "The status of women among the Nilotics and Nilo-Hamitics" (1932) 5 Africa:
Journal of the International African Institute 404 at 419.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e811a1310">
            <label>34</label>
            <p>
               <mixed-citation id="d1023e818" publication-type="other">
Kanyeihamba Constitutional and Political History, above at note 3 (1st edition, 2002,
Fountain Publishers) at 7-10;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e827" publication-type="other">
J Oloka-Onyango "Law, custom and access to justice in con-
temporary Uganda: A conceptual and analytical review" (paper prepared for New
Frontiers of Social Policy World Bank Conference, Dar Es Salaam, December 2005) part
II, paras 2.1-2.2.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e843a1310">
            <label>35</label>
            <p>
               <mixed-citation id="d1023e850" publication-type="other">
Burke Local Government, above at note 22 at 197-98.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e858a1310">
            <label>36</label>
            <p>
               <mixed-citation id="d1023e865" publication-type="other">
J Roscoe The Baganda: An Account of Their Native Customs and Beliefs (1965, Frank Cass and
Co Ltd) at 241.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e875a1310">
            <label>37</label>
            <p>
               <mixed-citation id="d1023e882" publication-type="other">
Burke Local Government, above at note 22 at 218</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e889a1310">
            <label>38</label>
            <p>
               <mixed-citation id="d1023e896" publication-type="other">
The Courts Ordinance 1909, sees 51 and 52.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e902" publication-type="other">
The Native Courts Act (chap 40), sec 4(1).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e909a1310">
            <label>39</label>
            <p>
               <mixed-citation id="d1023e916" publication-type="other">
African Courts Act (chap 38), sees 4(1), 4A(1) and (2)(f).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e923a1310">
            <label>40</label>
            <p>
               <mixed-citation id="d1023e930" publication-type="other">
Burke Local Government, above at note 22 at 188-89.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e937a1310">
            <label>41</label>
            <p>
               <mixed-citation id="d1023e944" publication-type="other">
Magistrates Court Act chap 36 (1964), sees 3, 9(1) and 15(1)(a).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e950" publication-type="other">
MCA
(2000),</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e961a1310">
            <label>43</label>
            <p>
               <mixed-citation id="d1023e968" publication-type="other">
F Von Benda-Beckmann "Law out of context: A comment on the creation of traditional
law discussion" (1984) Journal of African Law 28 at 29.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e978a1310">
            <label>44</label>
            <p>
               <mixed-citation id="d1023e985" publication-type="other">
Id at 29-33.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e992a1310">
            <label>45</label>
            <p>
               <mixed-citation id="d1023e999" publication-type="other">
M Bussani "Tort law and development: Insights into the case of Ethiopia and Eritrea"
(1996) 40/1 Journal of African Law 43 at 47, cited in G Woodman "Legal pluralism and
the search for justice" (1996) 40/2 Journal of African Law 152 at 164, note 38.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1012a1310">
            <label>46</label>
            <p>
               <mixed-citation id="d1023e1019" publication-type="other">
Woodman, id at 157-59 and 163-64.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1026a1310">
            <label>47</label>
            <p>
               <mixed-citation id="d1023e1033" publication-type="other">
J Baiya and J Oloka-Onyango Popular Justice and Resistance Committee Courts in Uganda
(1994, New Vision Printing and Publishing Corporation) at 46.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1042" publication-type="other">
B Baker
"Popular justice and policing from bush war to democracy: Uganda 1981-2004" (2004)
32 International Journal of Sociology of Law 333;</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1054" publication-type="other">
Nordic Consulting Group "Survey of LC
courts and legal aid providers findings: Tororo district" (2006)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1064" publication-type="other">
L Ekirikubinza
"Juvenile justice and the law in Uganda: Towards restorative justice" in L Lindholt and
S Schaumburg-Muller (eds) Human Rights in Development Yearbook 2003: Human Rights
and Local / Living Law (2005, Martinus Nijhoff Publishers) 295.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1080a1310">
            <label>48</label>
            <p>
               <mixed-citation id="d1023e1087" publication-type="other">
SALC "The harmonisation of the common law and indigenous law: Traditional courts
and the judicial function of traditional leaders" (discussion paper 82, Project 90, May
1999) at 13-15, available at: &lt;http://www.justice.gov.za/salrc/dpapers/dp82_pij90_
tradl_1999.pdf&gt; (last accessed 3 July 2012).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1102" publication-type="other">
South Africa
Traditional Courts Bill [B 15-2008],</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1111" publication-type="other">
National Assembly on 7 July
2009,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1121" publication-type="other">
&lt;http://www.justice.gov.
za/legislation/tradcourts/B15-2008.pdf&gt; (last accessed 3 July 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1132a1310">
            <label>49</label>
            <p>
               <mixed-citation id="d1023e1139" publication-type="other">
D Koyana Customary Law in a Changing Society (1980, Juta Publishers) at 130.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1146a1310">
            <label>50</label>
            <p>
               <mixed-citation id="d1023e1153" publication-type="other">
O Elechi Doing Justice Without the State: The Afikpo (Ehugho) Nigeria Model (2006, Routledge)
at 75-80, 118-21, 136-42 and 166-71.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1163a1310">
            <label>51</label>
            <p>
               <mixed-citation id="d1023e1170" publication-type="other">
A Griffiths In the Shadow of Marriage: Gender and Justice in an African Community (1997,
University of Chicago Press) at 113 and 117.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1180a1310">
            <label>52</label>
            <p>
               <mixed-citation id="d1023e1187" publication-type="other">
O Chase Law, Culture and Ritual (2005, New York University Press) at 24-25.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1194a1310">
            <label>53</label>
            <p>
               <mixed-citation id="d1023e1201" publication-type="other">
C Logan "Traditional leaders in modern Africa: Can democracy and the chief co-exist?"
(Afrobarometer working paper no 93, February 2008) 1 at 5 and 23, available at:
&lt;http://pdf.usaid.gov/pdf_docs/PNADL326.pd0&gt; (last accessed 3 July 2012).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1214a1310">
            <label>54</label>
            <p>
               <mixed-citation id="d1023e1221" publication-type="other">
J Cobbah "African values and the human rights debate: An African perspective" (1987) 9
Human Rights Quarterly 309 at 320-21.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1232a1310">
            <label>55</label>
            <p>
               <mixed-citation id="d1023e1239" publication-type="other">
N Sudarkasa "African and Afro-American family structure: A comparison" (1980) 11 Black
Scholar 37 at 44.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1249a1310">
            <label>56</label>
            <p>
               <mixed-citation id="d1023e1256" publication-type="other">
P Ikuenobe "Moral education and moral reasoning in traditional African cultures" (1998)
32/1 Journal of Value Inquiry 25 at 40-41.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1266a1310">
            <label>57</label>
            <p>
               <mixed-citation id="d1023e1273" publication-type="other">
Olo Bahamonde v Equatorial Guinea comm no 489/1991: UN doc CCPR/C/49/D/486/1991
(1993), para 9.4.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1283a1310">
            <label>58</label>
            <p>
               <mixed-citation id="d1023e1290" publication-type="other">
Jo-Gem gombolola court participants.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1297a1310">
            <label>59</label>
            <p>
               <mixed-citation id="d1023e1304" publication-type="other">
LGA, sec 50. LCCA, sec 4 and Local Council Courts Regulations (SI 51/2007), rule 13(1).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1311a1310">
            <label>60</label>
            <p>
               <mixed-citation id="d1023e1318" publication-type="other">
AJGM Sanders "Comparative law and law reform in Africa, with special reference to the
law of criminal justice" in Takirambudde (ed) The Individual, above at note 11, 148 at
150-51.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1332a1310">
            <label>61</label>
            <p>
               <mixed-citation id="d1023e1339" publication-type="other">
Chik Ma Pa Nono Morwa Guma Malasang [Constitution of the Morwa Guma Malasang clan]
(1985), chap 13(34)(vii).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1349a1310">
            <label>63</label>
            <p>
               <mixed-citation id="d1023e1356" publication-type="other">
Human Rights Committee "General comment 13": UN doc HRI/GEN/1/Rev.1 (1994) at 14,
para 3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1366a1310">
            <label>64</label>
            <p>
               <mixed-citation id="d1023e1373" publication-type="other">
Logan "Traditional leaders", above at note 53 at 3-4,</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1379" publication-type="other">
R Mattes "Building a demo-
cratic culture in traditional society" (paper presented to the International Conference
on Traditional Leadership in Southern Africa, at the University of Transkei, Umtata,
South Africa, 16-18 April 1997) at 6</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1394" publication-type="other">
M Owusu "Tradition and transformation:
Democracy and the politics of popular power in Ghana" (1996) 34(2) Journal of Modern
African Studies 307 at 330.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1407a1310">
            <label>66</label>
            <p>
               <mixed-citation id="d1023e1414" publication-type="other">
Morwa Guma Constitution, above at note 61, chap 11, para 32.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1421a1310">
            <label>67</label>
            <p>
               <mixed-citation id="d1023e1428" publication-type="other">
Judicial Service Act chap 14 (2000), sec 5. Art 147(3) and 148</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1435a1310">
            <label>68</label>
            <p>
               <mixed-citation id="d1023e1442" publication-type="other">
Kanyeihamba Constitutional and Political History, above at note 3 at 291-93.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1450a1310">
            <label>69</label>
            <p>
               <mixed-citation id="d1023e1457" publication-type="other">
L Madhuku "Constitutional protection of the independence of the judiciaiy. A survey of
the position in Southern Africa" (2002) 46/2 Journal of African Law 232 at 237.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1467a1310">
            <label>70</label>
            <p>
               <mixed-citation id="d1023e1474" publication-type="other">
MCA, rule 26, 3rd schedule. Schedule to the Trial on Indictments Act chap 23 (2000),
assessors rules 1 and 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1484a1310">
            <label>71</label>
            <p>
               <mixed-citation id="d1023e1491" publication-type="other">
B Oomen Tradition on the Move: Chiefi, Democracy and Change in Rural South Africa (2000,
Netherlands Institute for Southern Africa) at 64, cited in Logan "Traditional leaders",
above at note 53 at 23.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1504a1310">
            <label>72</label>
            <p>
               <mixed-citation id="d1023e1511" publication-type="other">
Madhuku "Constitutional protection", above at note 69 at 241-42.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1518a1310">
            <label>73</label>
            <p>
               <mixed-citation id="d1023e1525" publication-type="other">
The Advocates Act, chap 267 (2000), secs 1 and 8.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1532a1310">
            <label>74</label>
            <p>
               <mixed-citation id="d1023e1539" publication-type="other">
Morwa Guma miluka court participants.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1547a1310">
            <label>75</label>
            <p>
               <mixed-citation id="d1023e1554" publication-type="other">
LCCA, sec 24.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1561a1310">
            <label>76</label>
            <p>
               <mixed-citation id="d1023e1568" publication-type="other">
Trial on Indictments Act, rule 2(1).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1575a1310">
            <label>77</label>
            <p>
               <mixed-citation id="d1023e1582" publication-type="other">
Madhuku "Constitutional protection", above at note 69 at 240.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1588" publication-type="other">
Kanyeihamba
Constitutional and Political History, above at note 3 at 291.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1598a1310">
            <label>78</label>
            <p>
               <mixed-citation id="d1023e1605" publication-type="other">
Nowak United Nations Covenant, above at note 3 at 319-20, para 25.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1612a1310">
            <label>79</label>
            <p>
               <mixed-citation id="d1023e1619" publication-type="other">
LCCA, sees 4(1) and 8(4)(a);</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1625" publication-type="other">
Local Council Courts Regulations (2007), rule 19(1)(a).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1632a1310">
            <label>81</label>
            <p>
               <mixed-citation id="d1023e1639" publication-type="other">
Logan "Traditional leaders", above at note 53 at 6, citing JM Williams "Leading from
behind: Democratic consolidation and the chieftaincy in South Africa" (2004) 42/1
Journal of Modern African Studies 113 at 115-16.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1653a1310">
            <label>82</label>
            <p>
               <mixed-citation id="d1023e1660" publication-type="other">
Chase Law, above at note 52, chaps 2 and 7, at 114-16.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1667a1310">
            <label>83</label>
            <p>
               <mixed-citation id="d1023e1674" publication-type="other">
L Shaidi "Traditional, colonial and present day administration of criminal justice" in
T Mwene-Mushanga (ed) Criminology in Africa (2nd edition, 2002, Fountain Publishers)
1 at 2.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1687a1310">
            <label>84</label>
            <p>
               <mixed-citation id="d1023e1694" publication-type="other">
E Beyaraza Contemporary Relativism With Spedai Reference to Culture and Africa (2004,
Makerere University Printers) at 165-66.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1704a1310">
            <label>85</label>
            <p>
               <mixed-citation id="d1023e1711" publication-type="other">
Owor "Making international sentencing", above at note 15 at 199-200 and 222-26.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1718a1310">
            <label>86</label>
            <p>
               <mixed-citation id="d1023e1725" publication-type="other">
Morwa Guma p'oriwa court (15 August 2006).</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1732a1310">
            <label>87</label>
            <p>
               <mixed-citation id="d1023e1739" publication-type="other">
A Horwitz The Logic of Social Control (1990, Plenum Press) at 80-81 and 86-88.</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1023e1745" publication-type="other">
id at 22, 47-49
and 65-74.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1756a1310">
            <label>88</label>
            <p>
               <mixed-citation id="d1023e1763" publication-type="other">
S Roberts "Three models of family mediation" in R Dingwall and J Eekelaar Divorce,
Mediation and the Legal Process (1988, Clarendon Press) 144 at 145.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1773a1310">
            <label>91</label>
            <p>
               <mixed-citation id="d1023e1780" publication-type="other">
SALC "The harmonisation", above at note 48 at 15, para 4.3.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1787a1310">
            <label>92</label>
            <p>
               <mixed-citation id="d1023e1794" publication-type="other">
MCA, sec 10.</mixed-citation>
            </p>
         </fn>
         <fn id="d1023e1801a1310">
            <label>93</label>
            <p>
               <mixed-citation id="d1023e1808" publication-type="other">
Wanda "The role of traditional courts", above at note 11 at 81 and 90-91.</mixed-citation>
            </p>
         </fn>
      </fn-group>
   </back>
</article>
