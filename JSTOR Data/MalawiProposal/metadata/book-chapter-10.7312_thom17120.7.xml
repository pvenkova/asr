<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.7312/thom17120</book-id>
      <subj-group>
         <subject content-type="call-number">JF60.T48 2015</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Public administration</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Poverty</subject>
         <subj-group>
            <subject content-type="lcsh">Political aspects</subject>
            <subj-group>
               <subject content-type="lcsh">Developing countries</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Political culture</subject>
         <subj-group>
            <subject content-type="lcsh">Developing countries</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Developing countries</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">Western countries</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Western countries</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
            <subj-group>
               <subject content-type="lcsh">Developing countries</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Business</subject>
      </subj-group>
      <book-title-group>
         <book-title>Govern Like Us</book-title>
         <subtitle>U.S. Expectations of Poor Countries</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>THOMAS</surname>
               <given-names>M. A.</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>05</day>
         <month>05</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9780231539111</isbn>
      <isbn content-type="epub">0231539118</isbn>
      <publisher>
         <publisher-name>Columbia University Press</publisher-name>
         <publisher-loc>New York</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2015</copyright-year>
         <copyright-holder>Columbia University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7312/thom17120"/>
      <abstract abstract-type="short">
         <p>In the poorest countries, such as Afghanistan, Haiti, and Mali, the United States has struggled to work with governments whose corruption and lack of capacity are increasingly seen to be the cause of instability and poverty. The development and security communities call for "good governance" to improve the rule of law, democratic accountability, and the delivery of public goods and services. The United States and other rich liberal democracies insist that this is the only legitimate model of governance. Yet poor governments cannot afford to govern according to these ideals and instead are compelled to rely more heavily on older, cheaper strategies of holding power, such as patronage and repression.</p>
         <p>The unwillingness to admit that poor governments do and must govern differently has cost the United States and others inestimable blood and coin. Informed by years of fieldwork and drawing on practitioner work and academic scholarship in politics, economics, law, and history, this book explains the origins of poor governments in the formation of the modern state system and describes the way they govern. It argues that, surprisingly, the effort to stigmatize and criminalize the governance of the poor is both fruitless and destabilizing. The United States must pursue a more effective foreign policy to engage poor governments and acknowledge how they govern.</p>
      </abstract>
      <counts>
         <page-count count="264"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>I</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>V</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.3</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>VII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.4</book-part-id>
                  <title-group>
                     <label>1.</label>
                     <title>Blind Spot</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The United States has been embroiled in Afghanistan in the longest running war in its history, so long that I now teach graduate students who barely remember the attack on New York and Washington, D.C. on September 11, 2001. The objective of the United States and its Coalition partners in Afghanistan was not simply to rout the Taliban government and its al Qaeda allies—this was accomplished quickly and effectively in 2001. Instead, it has been to change the way Afghanistan is governed on the theory that a stable, prosperous, and well-governed country would provide no future foothold for terrorism.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.5</book-part-id>
                  <title-group>
                     <label>2.</label>
                     <title>The Governance Ideal</title>
                  </title-group>
                  <fpage>21</fpage>
                  <abstract>
                     <p>The attempt to restructure the government of Afghanistan is arguably one of the more ambitious efforts of the governance agenda, a Western effort to change the way other countries are governed. Over the last twenty years, both the development and the security communities have focused with increasing intensity on changing the way poor governments govern. The development community sees government as playing an essential role in fostering economic growth, contributing to individual well-being, and lifting the poor out of poverty. The security community sees government as necessary to address problems that threaten to expand beyond national borders, such as conflict,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.6</book-part-id>
                  <title-group>
                     <label>3.</label>
                     <title>Paper Empires, Paper Countries</title>
                  </title-group>
                  <fpage>44</fpage>
                  <abstract>
                     <p>To attempt a history of colonialism and decolonization in a single chapter is a thankless but necessary task. The governance problems of many of today’s poor countries are rooted in the way they came into being during the formation of the modern state system. The modern state system was formed through the process of “second wave” colonialism that occurred in the late nineteenth and the early twentieth centuries and the subsequent process of decolonization that followed in the mid-twentieth century. During second wave colonialism, the colonial powers claimed large territories and held most of them with a minimum of investment</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.7</book-part-id>
                  <title-group>
                     <label>4.</label>
                     <title>Poor Countries, Poor Governments</title>
                  </title-group>
                  <fpage>76</fpage>
                  <abstract>
                     <p>One reason that some poor governments have not established effective governance of their territories, much less ideal governance, is that they are too poor to do so.¹ They have much less revenue per capita to spend, and unlike rich liberal democracies that are financed largely through taxation of citizens and businesses, poor countries must get their money from much less desirable sources.</p>
                     <p>At independence, poor dependencies became poor countries. Their new governments were charged with building a state to the limit of their international borders. However, they were equipped only with a colonial administration of limited ambition that may have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.8</book-part-id>
                  <title-group>
                     <label>5.</label>
                     <title>Governing Cheaply</title>
                  </title-group>
                  <fpage>100</fpage>
                  <abstract>
                     <p>The poverty of poor governments means that they provide fewer and lower quality services to their populations, whether in terms of security, justice, health, or education. Many people pay no income taxes, receive no services, and have only occasional contact with the government. How then can poor governments maintain the acquiescence, if not gain the support, of such people? How can poor governments govern territories where they have no ability to maintain much presence?</p>
                     <p>We often imagine that simply organizing a democratic election will be sufficient to create popular support for a government; in fact, this was one mistake the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.9</book-part-id>
                  <title-group>
                     <label>6.</label>
                     <title>The Rule of Law</title>
                  </title-group>
                  <fpage>132</fpage>
                  <abstract>
                     <p>Poor governments govern differently. They do not get their revenue from the same sources, they do not use the same strategies of governance (and in particular do not rely as heavily on the legitimacy that comes from democratic elections and the supply of public goods), and they do not use state-made law as a coordinating mechanism for government and society to the same extent that rich liberal democracies do.</p>
                     <p>There is always a “gap between law on the books and law in action,” but when governments are poor that gap is especially wide. Enforcing laws costs money that poor governments</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.10</book-part-id>
                  <title-group>
                     <label>7.</label>
                     <title>Governance as It Is</title>
                  </title-group>
                  <fpage>158</fpage>
                  <abstract>
                     <p>One of the key problems of American (and more generally Western) engagement with poor governments is unrealistic expectations about governance. Americans approach poor governments with the expectations that governments control their territories; that they do, can, or should provide the same types of services that their own, much richer, governments provide; that law is a central coordinating mechanism for government and social behavior; that the legitimacy of government comes from providing services, the rule of law, and democratic elections, as it does in large part at home; and that providing services and assuring the rule of law are therefore top</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.11</book-part-id>
                  <title-group>
                     <label>8.</label>
                     <title>A Different Conversation</title>
                  </title-group>
                  <fpage>178</fpage>
                  <abstract>
                     <p>We see the governance ideal as a moral imperative. The governance of poor governments offends our democratic ideals, our belief in egalitarianism, and our belief in human rights. Our politicians routinely describe a global mission to change the way other countries govern, and this missionary impulse is a constant element in our foreign engagement. The development community has identified good governance as a development objective in itself, as well as the means to the achievement of other valued ends, such as economic growth. The security community has identified good governance as necessary for stability and the suppression of insurgencies and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.12</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>209</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">thom17120.13</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>245</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
