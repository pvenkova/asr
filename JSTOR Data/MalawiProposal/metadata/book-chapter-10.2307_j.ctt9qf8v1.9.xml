<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt9qf8v1</book-id>
      <subj-group>
         <subject content-type="call-number">E169.1.S5948 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Civilization</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Foreign relations</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">United States</subject>
         <subj-group>
            <subject content-type="lcsh">Social conditions</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Power (Social sciences)</subject>
         <subj-group>
            <subject content-type="lcsh">United States</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Rome</subject>
         <subj-group>
            <subject content-type="lcsh">History</subject>
            <subj-group>
               <subject content-type="lcsh">Empire, 30 B.C.–476 A.D.</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Power (Social sciences)</subject>
         <subj-group>
            <subject content-type="lcsh">Rome</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">World politics</subject>
         <subj-group>
            <subject content-type="lcsh">21st century</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Comparative civilization</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>History</subject>
      </subj-group>
      <book-title-group>
         <book-title>Why America Is Not a New Rome</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Smil</surname>
               <given-names>Vaclav</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>29</day>
         <month>01</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780262526852</isbn>
      <isbn content-type="epub">9780262283885</isbn>
      <isbn content-type="epub">0262283883</isbn>
      <publisher>
         <publisher-name>The MIT Press</publisher-name>
         <publisher-loc>Cambridge, Massachusetts; London, England</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>Massachusetts Institute of Technology</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt9qf8v1"/>
      <abstract abstract-type="short">
         <p>America's post--Cold War strategic dominance and its pre-recession affluence inspired pundits to make celebratory comparisons to ancient Rome at its most powerful. Now, with America no longer perceived as invulnerable, engaged in protracted fighting in Iraq and Afghanistan, and suffering the worst economic downturn since the Great Depression, comparisons are to the bloated, decadent, ineffectual later Empire. In<italic>Why America Is Not a New Rome</italic>, Vaclav Smil looks at these comparisons in detail, going deeper than the facile analogy-making of talk shows and glossy magazine articles. He finds profound differences.Smil, a scientist and a lifelong student of Roman history, focuses on several fundamental concerns: the very meaning of empire; the actual extent and nature of Roman and American power; the role of knowledge and innovation; and demographic and economic basics--population dynamics, illness, death, wealth, and misery. America is not a latter-day Rome, Smil finds, and we need to understand this in order to look ahead without the burden of counterproductive analogies. Superficial similarities do not imply long-term political, demographic, or economic outcomes identical to Rome's.</p>
      </abstract>
      <counts>
         <page-count count="240"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.3</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.4</book-part-id>
                  <title-group>
                     <title>[Part 1 Introduction]</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>The undeniably impressive extent and longevity of the Roman Empire and, arguably, its prestige, might, and glory (some of it real enough, much of it greatly misunderstood and often uncritically exaggerated, not a little of it entirely undeserved) created an irresistible standard of comparison for all subsequent powerful and expansive states of Western civilization. Some of these states pursued policies that were deliberately fashioned to invite positive comparisons with the great classical model (a quest that ranged from high-minded actions to tragicomic gestures); others unintentionally evolved in ways that made analogies inescapable to many classically educated minds as well as</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.5</book-part-id>
                  <title-group>
                     <label>I</label>
                     <title>Nihil Novi Sub Sole</title>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>America has been the latest great power whose intents, accomplishments, and failings have been compared to the actions of the Roman Empire and whose future has been foreseen as a variant of Rome’s decline and fall, the description of the demise of a powerful state that Edward Gibbon’s lifework inseparably attached to the most extensive and longest lasting of all Mediterranean empires.¹ Inevitable comparisons with its achievements began just a few centuries after the transformation of the Western Empire into a group of smaller Christian kingdoms with Charlemagne’s (742–814) effort to consolidate some of these states into a new</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.6</book-part-id>
                  <title-group>
                     <title>[Part 2 Introduction]</title>
                  </title-group>
                  <fpage>31</fpage>
                  <abstract>
                     <p>Chapters II–IV, the centerpiece of the book, answer the epigraph’s question by restating, defining, redefining, questioning, criticizing, deconstructing, and rejecting some of the most important analogies and parallels that are made between Rome and America, and they detail a number of fundamental differences that make any meaningful comparisons questionable. I divide these matters among three broad categories and, for a closer topical examination, subdivide each into three subcategories. This division has nothing to do with any triadic preferences (not uncommon among Romans and even more favored in the Chinese numerology)¹ and everything to do with imposing necessary brevity. Matters</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.7</book-part-id>
                  <title-group>
                     <label>II</label>
                     <title>Empires, Powers, Limits</title>
                  </title-group>
                  <fpage>35</fpage>
                  <abstract>
                     <p>There is nothing new about the uncritical elevation of Roman achievements or the hyperbolic admiration with which so many Western historians, novelists, and producers of films and TV series have echoed the judgment of classical writers (be they Roman or Greek) regarding the Roman Empire’s extent, durability, and power.¹ Too many commentators have recently succumbed to a similarly uncritical appraisal of America’s reach and might, a habit symbolized by unending references to the “only remaining superpower” and casual use of the term “American empire.” Perhaps the most notable difference is that the latter label has been used as much in</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.8</book-part-id>
                  <title-group>
                     <label>III</label>
                     <title>Knowledge, Machines, Energy</title>
                  </title-group>
                  <fpage>79</fpage>
                  <abstract>
                     <p>Traditional historians have sought to explain the fortunes of nations by delving into social, cultural, religious, and military aspects of their evolution, and economic historians, in common with mainstream economists, have concentrated on capital and labor as the key organizing principles of productive activities. But to students of energy systems who trace energy flows through ecosystems, another relation is as trivially obvious as it is irreplaceably fundamental: all systems, be they grasslands or coral reefs, cities or civilizations, are nothing but highly organized (simple or complex) converters of energy (Smil 2008a). Levels of energy use (more precisely, the availability of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.9</book-part-id>
                  <title-group>
                     <label>IV</label>
                     <title>Life, Death, Wealth</title>
                  </title-group>
                  <fpage>115</fpage>
                  <abstract>
                     <p>Recent comparisons of imperial Rome and modern America have taken place mostly on the elevated planes of grand strategy, armed conflicts, public policies, foreign relations, and cultural appeal, as if to descend to the level of private lives, to inquire into the enormous differences in the population dynamics of the two societies, and to assess the extent of economic growth and the distribution of wealth were irrelevant and unworthy choices. The very opposite is true. Much like the proper understanding of inventiveness, reliance on machines, and levels of energy use, these matters are essential for any revealing historical comparisons.</p>
                     <p>The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.10</book-part-id>
                  <title-group>
                     <title>[Part 3 Introduction]</title>
                  </title-group>
                  <fpage>147</fpage>
                  <abstract>
                     <p>Caeasar’s command should be taken as a universal admonition, as a warning to all those eager to rush even into what appear to be simple comparisons of two contemporaneous societies in order to draw hard conclusions based on seemingly revealing contrasts or parallels. Obviously, the task becomes even more questionable if the societies are separated by two millennia of history. But the temptation to compare cannot be stilled, and the impulse is particularly irresistible where ancient Rome is concerned. As I showed in chapter I, admiration and exaltation of that long-lived empire has itself become a part of Western history.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.11</book-part-id>
                  <title-group>
                     <label>V</label>
                     <title>Historical Analogies and Their (Lack of) Meaning</title>
                  </title-group>
                  <fpage>149</fpage>
                  <abstract>
                     <p>Giardina’s (1993) reminder should be kept in mind even before starting any comparative exercises: “Whatever noun we place with the adjective “Roman” (the Roman world, Roman man, etc.), the result is the same: what we are constructing is an abstract and totalizing, thus a partial, category. . . . The same is of course true in all complex civilizations” (5). Modern America, despite its relatively short history and high degree of economic and social homogenization, is an old state (older than most in today’s world). It encompasses diverse environments and values liberty and free speech, and its ideal of<italic>ex</italic>
                     </p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.12</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>173</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.13</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>197</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.14</book-part-id>
                  <title-group>
                     <title>Name Index</title>
                  </title-group>
                  <fpage>217</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt9qf8v1.15</book-part-id>
                  <title-group>
                     <title>Subject Index</title>
                  </title-group>
                  <fpage>221</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
