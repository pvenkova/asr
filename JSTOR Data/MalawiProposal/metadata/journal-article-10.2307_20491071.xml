<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">jvertpale</journal-id>
         <journal-id journal-id-type="jstor">j101365</journal-id>
         <journal-title-group>
            <journal-title>Journal of Vertebrate Paleontology</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Society of Vertebrate Paleontology</publisher-name>
         </publisher>
         <issn pub-type="ppub">02724634</issn>
         <issn pub-type="epub">19372809</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20491071</article-id>
         <title-group>
            <article-title>The Skull of Teleosaurus cadomensis (Crocodylomorpha; Thalattosuchia), and Phylogenetic Analysis of Thalattosuchia</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Stéphane</given-names>
                  <surname>Jouve</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>12</day>
            <month>3</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">29</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id>i20491062</issue-id>
         <fpage>88</fpage>
         <lpage>102</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 Society of Vertebrate Paleontology</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20491071"/>
         <abstract>
            <p>Several Teleosaurus skulls were described during the nineteenth century. Unfortunately, all skulls from this genus were destroyed during World War II. The only available skull is currently preserved in the MNHN. Thanks to a new preparation, new anatomical features can be seen, such as the morphology of the nasal cavity, the external otic recess, and the distribution of the foramina for the cranial nerves. A phylogenetic analysis is presented, including 14 thalattosuchian taxa. This analysis has generated four equally most parsimonious trees, where the thalattosuchians are closely related to the pholidosaurids and dyrosaurids, forming a longirostrine taxa. These relationships have been often considered to be based on homoplasies, related to the longirostrine morphology. This is also suggested herein, as the deletion of the longirostrine dependant characters or of the most longirostrine thalattosuchians in the analysis provide a consensus tree where thalattosuchians are basal crocodyliforms, a result more generally accepted. As the deletion of the most longirostrine thalattosuchians precludes the longirostrine problem in the phylogenetic analysis of Crocodyliformes, this deletion seems to be the less unsatisfactory solution to assess the crocodyliform relationships. The phylogenetic analysis also provides interesting information on the thalattosuchian relationships: Teleosaurus is the basal-most thalattosuchian, Teleosauridae is paraphyletic and Pelagosaurus is neither the basal-most thalattosuchian nor the basal-most metriorhynchid. The metriorhynchid relationships support previous works, as 'Teleidosaurus' is paraphyletic and the basal-most metriorhynchid, Metriorhynchus is more closely related to other metriorhynchid than 'Teleidosaurus,' and Enaliosuchus, for which the relationships are tested for the first time, is the sister taxon of Dakosaurus. Geosaurus is the sister taxon of the clade Dakosaurus + Enaliosuchus.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d2286e189a1310">
            <mixed-citation id="d2286e193" publication-type="other">
Andrews, C. W. 1913. A descriptive catalogue of the marine reptiles
of the Oxford Clay. British Museum of Natural History, London,
206 pp.</mixed-citation>
         </ref>
         <ref id="d2286e206a1310">
            <mixed-citation id="d2286e210" publication-type="other">
Benton, M. J., and J. M. Clark. 1988. Archosaur phylogeny and the
relationships of the Crocodilia, p. 295-338. In M. J. Benton (ed.),
The Phylogeny and Classification of the Tetrapods.Volume 1. Clar-
endon Press, Oxford.</mixed-citation>
         </ref>
         <ref id="d2286e226a1310">
            <mixed-citation id="d2286e230" publication-type="other">
Blainville, H. de. 1855. Ostéographie. Baillière, J.B., Paris, Atlas IV.</mixed-citation>
         </ref>
         <ref id="d2286e237a1310">
            <mixed-citation id="d2286e241" publication-type="other">
Bonaparte, J. F. 1971. Los tetrapodos del sector superior de la formacion
Los Colorados, La Rioja, Argentina. Opera Lilloana 22:1-183.</mixed-citation>
         </ref>
         <ref id="d2286e252a1310">
            <mixed-citation id="d2286e256" publication-type="other">
Bornet, T. 1866. Squelette de crocodile trouvé dans une carrière des
environs de Nevers. Bulletin de la Société Nivernaise des Lettres et
Sciences Naturelles 2:436-439.</mixed-citation>
         </ref>
         <ref id="d2286e269a1310">
            <mixed-citation id="d2286e273" publication-type="other">
Brochu, C. A., M. L. Bouaré, F. Sissoko, E. M. Roberts, and M. A.
O'leary. 2002. A dyrosaurid crocodyliform braincase from Mali.
Journal of Paleontology 76:1060-1071.</mixed-citation>
         </ref>
         <ref id="d2286e286a1310">
            <mixed-citation id="d2286e290" publication-type="other">
Broili, F. 1932. Weitere Beobachtungen an Geosaurus. Neues Jahrbuch
für Mineralogie und Paläontologie, Abhandlungen 68:128-148.</mixed-citation>
         </ref>
         <ref id="d2286e300a1310">
            <mixed-citation id="d2286e304" publication-type="other">
Broin, F. D. 1965. Etude paléontologique de quelques crocodiliens du
Sahara. Ph.D.dissertation, Paris, 140 pp.</mixed-citation>
         </ref>
         <ref id="d2286e314a1310">
            <mixed-citation id="d2286e318" publication-type="other">
Broili, F. 1932. Weitere Beobachtungen an Geosaurus. Neues Jahrbuch
für Mineralogie und Paläontologie, Abhandlungen 68:128-148.</mixed-citation>
         </ref>
         <ref id="d2286e328a1310">
            <mixed-citation id="d2286e332" publication-type="other">
Buckley, G. A., and C. A. Brochu. 1999. An enigmatic new crocodile
from the upper Cretaceous of Madagascar. Special Papers in
Palaeontology 60:149-175.</mixed-citation>
         </ref>
         <ref id="d2286e346a1310">
            <mixed-citation id="d2286e350" publication-type="other">
Buckley, G. A., C. A. Brochu, D. W. Krause, and D. Pol. 2000.
A pug-nose Crocodyliform from the Late Cretaceous of Madagas-
car. Nature 405:941-944.</mixed-citation>
         </ref>
         <ref id="d2286e363a1310">
            <mixed-citation id="d2286e367" publication-type="other">
Buffetaut, E. 1976. Ostéologie et affinités de Trematochampsa taquea
(Crocodylia, Mesosuchia) du Sénonien Inférieur d'In Beceten
(République du Niger). Geobios 9:143-198.</mixed-citation>
         </ref>
         <ref id="d2286e380a1310">
            <mixed-citation id="d2286e384" publication-type="other">
Buffetaut, E. 1980a. Position systématique et phylogénétique du genre
Pelagosaurus Bronn, 1841 (Crocodylia, Mesosuchia), du Toarcien
d'Europe. Geobios 13:783-786.</mixed-citation>
         </ref>
         <ref id="d2286e397a1310">
            <mixed-citation id="d2286e401" publication-type="other">
Buffetaut, E. 1980b. Teleosauridae et Metriorhynchidae: l'évolution de
deux Familles de crocodiliens Mésosuchiens marins du Mesozoique,
Comptes Rendus du 105e Congrès National des Sociétés Savantes,
Caen, 11-22.</mixed-citation>
         </ref>
         <ref id="d2286e417a1310">
            <mixed-citation id="d2286e421" publication-type="other">
Buscalioni, A. D., and J. L. Sanz. 1990. Montsecosuchus depereti (Croco-
dylomorpha, Atoposauridae), new denomination for Alligatorellus
depereti Vidal, 1915 (Early Cretaceous, Spain): redescription and
phylogenetic relationships. Journal of Vertebrate Paleontology
10:244-254.</mixed-citation>
         </ref>
         <ref id="d2286e440a1310">
            <mixed-citation id="d2286e444" publication-type="other">
Buscalioni, A. D., F. Ortega, D. B. Weishampel, and C M. Jianu. 2001.
A revision of the Crocodyliform Alio dap osuchus precedens from the
upper Cretaceous of the Hateg basin, Romania. Its relevance in the
phylogeny of Eusuchia. Journal of Vertebrate Paleontology
21:74-86.</mixed-citation>
         </ref>
         <ref id="d2286e464a1310">
            <mixed-citation id="d2286e468" publication-type="other">
Carvalho, I. S., and R. J. Bertini. 1999. Mariliasuchus: urn novo Crocody-
lomorpha (Notosuchia) do Cretaceo da Bacia Bauru, Brasil. Geolo-
gia Colombiana 24:83-105.</mixed-citation>
         </ref>
         <ref id="d2286e481a1310">
            <mixed-citation id="d2286e485" publication-type="other">
Carvalho, I. S., L. C. Borges Ribeiro, and L. D. Santos Avilla. 2004.
Uberabasuchus terrificus sp. nov., a new Crocodylomorpha from
the Bauru Basin (Upper Cretaceous), Brazil. Gondwana Research
7:975-1002.</mixed-citation>
         </ref>
         <ref id="d2286e501a1310">
            <mixed-citation id="d2286e505" publication-type="other">
Clark, J. M. 1994. Patterns of evolution in Mesozoic Crocodyliformes;
pp. 84-97 Jn N. C. Fraser and H. D. Sues (eds.), In the shadow of the
Dinosaurs, early Mesozoic Tetrapods. Cambridge University Press,
Cambridge and New York.</mixed-citation>
         </ref>
         <ref id="d2286e521a1310">
            <mixed-citation id="d2286e525" publication-type="other">
Collot, M. L. 1905. Reptile Jurassique (Teleidosaurus gaudryi) trouvé à
Saint-Seine-l'Abbaye (Côte-d'Or). Mémoires de l'Académie de
Dijon, série 4,10:3-7.</mixed-citation>
         </ref>
         <ref id="d2286e538a1310">
            <mixed-citation id="d2286e542" publication-type="other">
Cuvier, B. 1824. Recherches sur les ossemens fossiles, où l'on rétablit les
caractères de plusieurs animaux dont les révolutions du globe ont
détruit les espèces. Dufour, G. et D'Ocagne, E., Paris, 547 pp.</mixed-citation>
         </ref>
         <ref id="d2286e555a1310">
            <mixed-citation id="d2286e559" publication-type="other">
Erickson, B. R. 1976. Osteology of the early Eusuchian crocodile Lei-
dyosuchus formidabilis, sp. nov. Monograph of the Science Museum
of Minnesota 2:1-61.</mixed-citation>
         </ref>
         <ref id="d2286e573a1310">
            <mixed-citation id="d2286e577" publication-type="other">
Eudes-Deslongchamps, E. 1864. Observations sur une tête de Teleo-
saurus trouvée dans le Callovien du Mesnil de Bavet par M. Mor-
ière. Bulletin de la Société Linnéenne de Caen 8:287-288.</mixed-citation>
         </ref>
         <ref id="d2286e590a1310">
            <mixed-citation id="d2286e594" publication-type="other">
Eudes-Deslongchamps, E. 1867-1869. Prodrome des téléosauriens du
Calvados, p. 95-354, Notes Paléontologiques. Volume 1, Caen.</mixed-citation>
         </ref>
         <ref id="d2286e604a1310">
            <mixed-citation id="d2286e608" publication-type="other">
Eudes-Deslongchamps, E. 1868. Note sur trois espèces de téléosauriens
du calcaire de Caen, se rapprochant du 1er type créé par Geoffroy-
Saint-Hilaire, sous le nom de Téléosaurus cadomensis. Bulletin de la
Société Linnéenne de Caen, série 2, 1:326-353.</mixed-citation>
         </ref>
         <ref id="d2286e624a1310">
            <mixed-citation id="d2286e628" publication-type="other">
Eudes-Deslongchamps, E. 1869. Mémoire sur les Téléosauriens. Bulletin
de la Société Linnéenne de Normandie, série 2, 3:124-221.</mixed-citation>
         </ref>
         <ref id="d2286e638a1310">
            <mixed-citation id="d2286e642" publication-type="other">
Eudes-Deslongchamps, J. A. 1896. Découverte du premier individu du
Teleosaurus cadomensis. Bulletin de la Société Linnéenne de Nor-
mandie 410:26-49.</mixed-citation>
         </ref>
         <ref id="d2286e655a1310">
            <mixed-citation id="d2286e659" publication-type="other">
Fernandez, M., and Z. B. Gasparini. 2000. Sait glands in Tithonian
metriorhynchid crocodyliform and their physiological significance.
Lethaia 33:269-276.</mixed-citation>
         </ref>
         <ref id="d2286e673a1310">
            <mixed-citation id="d2286e677" publication-type="other">
Fraas, E. V. 1901. Die Meerkrokodile (Thalattosuchia n.g.) eine Saurier-
gruppe der Juraformation. Jahresheft der Vereinigung vaterlän-
discher Naturkundler Württembergs 57:409-418.</mixed-citation>
         </ref>
         <ref id="d2286e690a1310">
            <mixed-citation id="d2286e694" publication-type="other">
Fraas, E. V. 1902. Die Meer-Crocodilier (Thalattosuchia) des Oberen
Jura unter specieller Berücksichtigung von Dacosaurus und Geo-
saurus. Palaeontographica 49:1-72.</mixed-citation>
         </ref>
         <ref id="d2286e707a1310">
            <mixed-citation id="d2286e711" publication-type="other">
Gao, Y. H. 2001. A new species of Hsisosuchus from Dashanpu, Zigong,
Sichuan. Vertebrata PalAsiatica 39:177-184.</mixed-citation>
         </ref>
         <ref id="d2286e721a1310">
            <mixed-citation id="d2286e725" publication-type="other">
Gasparini, Z. B., and G. Chong Diaz. 1977. Metriorhynchus casamiquelai
n. sp. (Crocodilia, Thalattosuchia), a marine crocodile from the
Jurassic (Callovian) of Chile, South America. Neues Jahrbuch für
Geologie und Paläontologie, Abhandlungen 153:341-360.</mixed-citation>
         </ref>
         <ref id="d2286e741a1310">
            <mixed-citation id="d2286e745" publication-type="other">
Gasparini, Z. B., L. M. Chiappe, and M. Fernandez. 1991. A new Seno-
nian Peirosaurid (Crocodylomorpha) from Argentina and synopsis
of the South American Cretaceous crocodilians. Journal of Verte-
brate Paleontology 11:316-333.</mixed-citation>
         </ref>
         <ref id="d2286e761a1310">
            <mixed-citation id="d2286e765" publication-type="other">
Gasparini, Z. B., M. Fernandez, and J. Powell. 1993. New tertiary sebe-
cosuchians (Crocodylomorpha) from South America: phylogenetic
implications. Historical Biology 7:1-19.</mixed-citation>
         </ref>
         <ref id="d2286e779a1310">
            <mixed-citation id="d2286e783" publication-type="other">
Gasparini, Z. B., D. Pol, and L. A. Spalletti. 2005. An unusual marine
crocodyliform from the Jurassic-Cretaceous boundary of Patagonia.
Science 311:70-73.</mixed-citation>
         </ref>
         <ref id="d2286e796a1310">
            <mixed-citation id="d2286e800" publication-type="other">
Geoffroy Saint-Hilaire, E. 1825. Recherches sur l'organisation des Gavials,
sur leurs affinités naturelles desquelles résulte la necessité d'une autre
distribution générique: Gavialis, Teleosaurus, Steneosaurus. Mémoires
du Muséum National d'Histoire Naturelle 12:97-155.</mixed-citation>
         </ref>
         <ref id="d2286e816a1310">
            <mixed-citation id="d2286e820" publication-type="other">
Gervais, P. 1859. Zoologie et Paléontologie française. Bertrand, A.,
Paris, Atlas, 544 pp.</mixed-citation>
         </ref>
         <ref id="d2286e830a1310">
            <mixed-citation id="d2286e834" publication-type="other">
Goloboff, P. 1999. NONA (NO NAME) ver. 2 Published by the author,
Tucumán, Argentina.</mixed-citation>
         </ref>
         <ref id="d2286e844a1310">
            <mixed-citation id="d2286e848" publication-type="other">
Gomani, E. M. 1997. A crocodyliform from the early Cretaceous Dino-
saur beds, northern Malawi. Journal of Vertebrate Paleontology
17:280-294.</mixed-citation>
         </ref>
         <ref id="d2286e861a1310">
            <mixed-citation id="d2286e865" publication-type="other">
Hua, S., P. Vignaud, F. Atrops, and A. Clément. 2000. Enaliosuchus
macrospondylus Koken, 1883 (Crocodylia, Metriorhynchidae) du
Valanginien de Barret-le-Bas (Hautes Alpes, France): un cas unique
de remontée des narines externes parmi les crocodiliens. Geobios
33:467-474.</mixed-citation>
         </ref>
         <ref id="d2286e885a1310">
            <mixed-citation id="d2286e889" publication-type="other">
Jouve, S. 2004. Etude des Crocodyliformes fini Crétacé-Paléogène du
Bassin des Oulad Abdoun (Maroc) et comparaison avec les faunes
africaines contemporaines: systématique, phylogénie et paléobio-
géographie. Ph.D. dissertation, Muséum National d'Histoire Natur-
elle, Paris, 651 pp.</mixed-citation>
         </ref>
         <ref id="d2286e908a1310">
            <mixed-citation id="d2286e912" publication-type="other">
Jouve, S. 2005. A new description of the skull of Dyrosaurus phosphati-
cus (Thomas, 1893) (Mesoeucrocodylia: Dyrosauridae) from the
Lower Eocene of North Africa. Canadian Journal of Earth Sciences
42:323-337.</mixed-citation>
         </ref>
         <ref id="d2286e928a1310">
            <mixed-citation id="d2286e932" publication-type="other">
Jouve, S., M. Iarochène, B. Bouya, and M. Amaghzaz. 2006. A new
species of Dyrosaurus (Crocodylomorpha, Dyrosauridae) from the
Early Eocene of Morocco: phylogenetic implications. Zoological
Journal of the Linnean Society 148:603-656.</mixed-citation>
         </ref>
         <ref id="d2286e948a1310">
            <mixed-citation id="d2286e952" publication-type="other">
Jouve, S. 2007. Taxonomic revision of the dyrosaurid assemblage (Cro-
codyliformes, Mesoeucrocodylia) from the Palaeocene of the Iul-
lemmeden basin. Journal of Paleontology 81:163-175.</mixed-citation>
         </ref>
         <ref id="d2286e965a1310">
            <mixed-citation id="d2286e969" publication-type="other">
Lamouroux, M. 1820. Sur le crocodile fossile trouvé dans les carrières du
bourg d'Allemagne, à un quart de lieue de Cean. Annales Générales
des Sciences Physiques 3:160-164.</mixed-citation>
         </ref>
         <ref id="d2286e982a1310">
            <mixed-citation id="d2286e986" publication-type="other">
Larsson, H. C. E., and B. Gado. 2000. A new Early Cretaceous crocodyli-
form from Niger. Neues Jahrbuch für Geologie und Paläontologie,
Abhandlungen 217:131-141.</mixed-citation>
         </ref>
         <ref id="d2286e1000a1310">
            <mixed-citation id="d2286e1004" publication-type="other">
Li, J. 1985. Revision of Edentosuchus tienshanensis Young. Vertebrata
PalAsiatica 23:196-203.</mixed-citation>
         </ref>
         <ref id="d2286e1014a1310">
            <mixed-citation id="d2286e1018" publication-type="other">
Li, J. 1993. A new specimen of Peipehsuchus teleorhinus from Ziliujing
formation of Daxian, Sichuan. Vertebrata PalAsiatica 31:92-97.</mixed-citation>
         </ref>
         <ref id="d2286e1028a1310">
            <mixed-citation id="d2286e1032" publication-type="other">
Liu, H.-T. 1961. The discovery of Teleosaurus in China. Vertebrata
PalAsiatica 5:69-71 [Chinese 69; English 70].</mixed-citation>
         </ref>
         <ref id="d2286e1042a1310">
            <mixed-citation id="d2286e1046" publication-type="other">
Martinelli, A. G. 2003. New cranial remains of the bizarre notosuchid
Comahuesuchus brachybuccalis (Archosauria, Crocodyliformes)
from the Late Cretaceous of Rio Negro Province (Argentina). Ame-
ghiniana 40:559-572.</mixed-citation>
         </ref>
         <ref id="d2286e1062a1310">
            <mixed-citation id="d2286e1066" publication-type="other">
Mercier, J. 1933. Contribution à l'étude des Métriorhynchidés (crocodi-
liens). Annales de Paléontologie 22:99-119.</mixed-citation>
         </ref>
         <ref id="d2286e1076a1310">
            <mixed-citation id="d2286e1080" publication-type="other">
Morel De Glasville, M. 1876. Sur la cavité crânienne et la position du
trou optique dans le Steneosaurus heberti. Bulletin de la Société
Géologique de France 3 4:342-348.</mixed-citation>
         </ref>
         <ref id="d2286e1094a1310">
            <mixed-citation id="d2286e1098" publication-type="other">
Morel De Glasville, M. 1880. Note sur le Steneosaurus heberti. Bulletin
de la Société Géologique de France 3 8:318-330.</mixed-citation>
         </ref>
         <ref id="d2286e1108a1310">
            <mixed-citation id="d2286e1112" publication-type="other">
Mueller-Töwe, I. J. 2005. Phylogenetic relationships of the Thalattosu-
chia. Zitteliana A45:211-213.</mixed-citation>
         </ref>
         <ref id="d2286e1122a1310">
            <mixed-citation id="d2286e1126" publication-type="other">
Mueller-Töwe, I. J. 2006. Anatomy, phylogeny, and palaeoecology
of the basal thalattosuchians (Mesoeucrocodylia) from the Liassic
of Central Europe. Ph.D. thesis, University of Mainz, Mainz,
Germany.</mixed-citation>
         </ref>
         <ref id="d2286e1142a1310">
            <mixed-citation id="d2286e1146" publication-type="other">
Nixon, K. C. 1999-2002. WinClada ver. 1.0000 Published by the author,
Ithaca, NY, USA.</mixed-citation>
         </ref>
         <ref id="d2286e1156a1310">
            <mixed-citation id="d2286e1160" publication-type="other">
Ortega, F., Z. B. Gasparini, A. D. Buscalioni, and J. O. Calvo. 2000.
A new species of Araripesuchus (Crocodylomorpha, Mesoeucroco-
dylia) from the Lower Cretaceous of Patagonia (Argentina).
Journal of Vertebrate Paleontology 20:57-76.</mixed-citation>
         </ref>
         <ref id="d2286e1176a1310">
            <mixed-citation id="d2286e1180" publication-type="other">
Owen, R. D. 1841. Report on British fossils reptiles. Report of the
British Association for the Advancement of Science 11:60-204.</mixed-citation>
         </ref>
         <ref id="d2286e1191a1310">
            <mixed-citation id="d2286e1195" publication-type="other">
Phillips, J. 1871. The geology of Oxford and the Valley of the Thames.
Clarendon Press, Oxford, 523 pp.</mixed-citation>
         </ref>
         <ref id="d2286e1205a1310">
            <mixed-citation id="d2286e1209" publication-type="other">
Pierce, S., and M. Benton. 2006. Pelagosaurus typus Bronn, 1841
(Mesoeucrocodylia: Thalattosuchia) from the Upper Lias (Toarcian,
Lower Jurassic) of Somerset, England. Journal of Vertebrate Pale-
ontology 26:621-635.</mixed-citation>
         </ref>
         <ref id="d2286e1225a1310">
            <mixed-citation id="d2286e1229" publication-type="other">
Pol, D. 1999. Restos postcraneos de Notosuchus terrestris (Archosauria:
Crocodyliformes), caracteristicas e implicancias en la locomocion.
Ameghiniana 36:106-107.</mixed-citation>
         </ref>
         <ref id="d2286e1242a1310">
            <mixed-citation id="d2286e1246" publication-type="other">
Pol, D. 2003. New remains of Sphagesaurus huenei (Crocodylomorpha:
Mesoeucrocodylia) from the Late Cretaceous of Brazil. Journal of
Vertebrate Paleontology 23:817-831.</mixed-citation>
         </ref>
         <ref id="d2286e1259a1310">
            <mixed-citation id="d2286e1263" publication-type="other">
Pol, D., and S. Apesteguia. 2005. New Araripesuchus remains from the
Early Late Cretaceous (Cenomanian-Turonian) of Patagonia.
American Museum Novitates 3490:1-38.</mixed-citation>
         </ref>
         <ref id="d2286e1276a1310">
            <mixed-citation id="d2286e1280" publication-type="other">
Pol, D., S.-A. Ji, J. M. Clark, and L. M. Chiappe. 2004. Basal crocodyli-
forms from the Lower Cretaceous Tugulu Group (Xinjiang, China),
and the phylogenetic position of Edentosuchus. Cretaceous
Research 25:603-622.</mixed-citation>
         </ref>
         <ref id="d2286e1297a1310">
            <mixed-citation id="d2286e1301" publication-type="other">
Pol, D., and M. A. Norell. 2004a. A new Crocodyliform from Zos Can-
yon, Mongolia. American Museum Novitates: 1-36.</mixed-citation>
         </ref>
         <ref id="d2286e1311a1310">
            <mixed-citation id="d2286e1315" publication-type="other">
Pol, D., and M. A. Norell. 2004b. A new gobiosuchid crocodyliform
taxon from the Cretaceous of Mongolia. American Museum Novi-
tates 3458:1-31.</mixed-citation>
         </ref>
         <ref id="d2286e1328a1310">
            <mixed-citation id="d2286e1332" publication-type="other">
Rogers, J. V. 2003. Pachycheilosuchus trinquei, a new procelous crocody-
liform from the Lower Cretaceous (Albian) Glen Rose Formation
of Texas. Journal of Vertebrate Paleontology 23:128-145.</mixed-citation>
         </ref>
         <ref id="d2286e1345a1310">
            <mixed-citation id="d2286e1349" publication-type="other">
Salisbury, S. W., E. Frey, D. M. Martill, and M.-C. Buchy. 2003. A new
crocodilian from the Lower Cretaceous Crato Formation of north-
eastern Brazil. Palaeontographica Abteilung A 270:3-47.</mixed-citation>
         </ref>
         <ref id="d2286e1362a1310">
            <mixed-citation id="d2286e1366" publication-type="other">
Sauvage, H. E. 1874. Mémoire sur les dinosauriens et les crocodiliens des
terrains jurassiques de Boulogne-sur-mer. Mémoires de la Société
Géologique de France 210:1-55.</mixed-citation>
         </ref>
         <ref id="d2286e1379a1310">
            <mixed-citation id="d2286e1383" publication-type="other">
Schroeder, H. 1922. Ein Meereskrokodilier aus der Unteren Kreide
Norddeutschlands. Jahrbuch der Preussischen Geologischen Land-
esanstalt 42:352-364.</mixed-citation>
         </ref>
         <ref id="d2286e1397a1310">
            <mixed-citation id="d2286e1401" publication-type="other">
Schwarz, D., and S. W. Salisbury. 2005. A new species of Theriosuchus
(Atoposauridae, Crocodylomorpha) from the Late Jurassic (Kim-
meridgian) of Guimarota, Portugal. Geobios 38:779-802.</mixed-citation>
         </ref>
         <ref id="d2286e1414a1310">
            <mixed-citation id="d2286e1418" publication-type="other">
Sereno, P. C., H. C. E. Larsson, C. A. Sidor, and B. Gado. 2001. The
giant crocodyliform Sarcosuchus from the Cretaceous of Africa.
Science 294:1516-1519.</mixed-citation>
         </ref>
         <ref id="d2286e1431a1310">
            <mixed-citation id="d2286e1435" publication-type="other">
Sereno, P. C., C. A. Sidor, H. C. E. Larsson, and B. Gado. 2003. A new
Notosuchian from the Early Cretaceous of Niger. Journal of Verte-
brate Paleontology 23:477-482.</mixed-citation>
         </ref>
         <ref id="d2286e1448a1310">
            <mixed-citation id="d2286e1452" publication-type="other">
Sues, H. D., J. M. Clark, and F. A. J. Jenkins. 1984. A review of the Early
Jurassic tetrapods from the Glen Canyon Group of American
Southwest; p. 284-294. In N. C. Fraser and H. D. Sues (eds.), In the
shadow of the Dinosaurs, early Mesozoic Tetrapods. Cambridge
University Press, Cambridge and New York.</mixed-citation>
         </ref>
         <ref id="d2286e1471a1310">
            <mixed-citation id="d2286e1475" publication-type="other">
Swofford, D. L. 2002. PAUP: phylogenetic analysis using parsimony.
Sinauer Associates inc., Sunderland.</mixed-citation>
         </ref>
         <ref id="d2286e1485a1310">
            <mixed-citation id="d2286e1489" publication-type="other">
Telles-Antunes, M. 1967. Urn Mesosuquiano do Liasico de Tomar
(Portugal). Memorias dos Serviços Geologicos de Portugal new
serie 13:66p.</mixed-citation>
         </ref>
         <ref id="d2286e1503a1310">
            <mixed-citation id="d2286e1507" publication-type="other">
Turner, A. H., and J. O. Calvo. 2005. A new sebecosuchian crocodyli-
form from the Late Cretaceous of Patagonia. Journal of Vertebrate
Paleontology 25:87-98.</mixed-citation>
         </ref>
         <ref id="d2286e1520a1310">
            <mixed-citation id="d2286e1524" publication-type="other">
Tykoski, R. S., T. B. Rowe, R. A. Ketcham, and M. W. Colbert. 2002.
Calsoyasuchus valliceps, a new Crocodyliform from the Early Juras-
sic Kayenta Formation of Arizona. Journal of Vertebrate Paleontol-
ogy 22:593-611.</mixed-citation>
         </ref>
         <ref id="d2286e1540a1310">
            <mixed-citation id="d2286e1544" publication-type="other">
Vignaud, P. 1995. Les Thalattosuchia, crocodiles marins du Mesozoique:
systématique phylogénétique, paléoécologie, biochronologie et
implications paléogéographiques. Ph.D.dissertation, Université de
Poitiers, Paris, 271 pp.</mixed-citation>
         </ref>
         <ref id="d2286e1560a1310">
            <mixed-citation id="d2286e1564" publication-type="other">
Walker, A. D. 1970. A revision of Jurassic reptile Hallopus victor
(Marsh), with remarks on the classification of crocodiles. Philosoph-
ical Transactions of the Royal Society of London, B, Biological
Sciences 257:323-372.</mixed-citation>
         </ref>
         <ref id="d2286e1580a1310">
            <mixed-citation id="d2286e1584" publication-type="other">
Wellnhofer, P. 1971. Die Atoposauridae (Crocodylia, Mesosuchia) der
Oberjura-Plattenkalke Bayerns. Palaeontographica A 138:133-165.</mixed-citation>
         </ref>
         <ref id="d2286e1594a1310">
            <mixed-citation id="d2286e1598" publication-type="other">
Wenz, S. 1968. Contribution à l'étude du genre Metriorhynchus: crâne et
moulage endocranien de Metriorhynchus superciliosus. Annales de
Paléontologie 54:149-163.</mixed-citation>
         </ref>
         <ref id="d2286e1612a1310">
            <mixed-citation id="d2286e1616" publication-type="other">
Wenz, S. 1970. Sur un Metriorhynchus à museau court du Callovien des
Vaches Noires (Calvados). Bulletin de la Société Géologique de
France, série 7, 12:390-397.</mixed-citation>
         </ref>
         <ref id="d2286e1629a1310">
            <mixed-citation id="d2286e1633" publication-type="other">
Westphal, F. 1961. Zur Systematik der deutschen und englischen Lias-
Krokodilier. Neues Jahrbuch für Geologie und Paläontologie,
Abhandlungen 113:207-218.</mixed-citation>
         </ref>
         <ref id="d2286e1646a1310">
            <mixed-citation id="d2286e1650" publication-type="other">
Westphal, F. 1962. Die krokodilier des Deutschen und Englischen obe-
ren Lias. Palaeontographica A 116:23-118.</mixed-citation>
         </ref>
         <ref id="d2286e1660a1310">
            <mixed-citation id="d2286e1664" publication-type="other">
Witmer, L. M. 1995. Homology on facial structures in extant
Archosaurs (Birds and Crocodilians), with special reference to para-
nasal pneumaticity and nasal conchae. Journal of Morphology
225:269-327.</mixed-citation>
         </ref>
         <ref id="d2286e1680a1310">
            <mixed-citation id="d2286e1684" publication-type="other">
Witmer, L. M. 1997. The evolution of the antorbital cavity of archosaurs:
a study in soft-tissue reconstruction in the fossil record with an
analysis of the function of pneumaticity. Journal of Vertebrate Pale-
ontology 17:1-73.</mixed-citation>
         </ref>
         <ref id="d2286e1700a1310">
            <mixed-citation id="d2286e1704" publication-type="other">
Wu, X.-C., H. D. Sues, and Z. M. Dong. 1997. Sichuanosuchus shuhanen-
sis, a new? Early Cretaceous protosuchian (Archosauria: Crocodyli-
formes) from Sichuan (China), and the monophyly of Protosuchia.
Journal of Vertebrate Paleontology 17:89-103.</mixed-citation>
         </ref>
         <ref id="d2286e1721a1310">
            <mixed-citation id="d2286e1725" publication-type="other">
Wu, X.-C., A. P. Russell, and S. L. Cumbaa. 2001. Terminonaris
(Archosauria: Crocodyliformes): new material from Saskatchewan,
Canada, and comments on its phylogenetic relationships. Journal of
Vertebrate Paleontology 21:492-514.</mixed-citation>
         </ref>
         <ref id="d2286e1741a1310">
            <mixed-citation id="d2286e1745" publication-type="other">
Young, C. C. 1964. New fossil crocodiles from China. Vertebrata
PalAsiatica 8:199-208.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
