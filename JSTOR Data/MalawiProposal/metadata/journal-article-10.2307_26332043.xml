<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="eng">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">demorese</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50020442</journal-id>
         <journal-title-group>
            <journal-title>Demographic Research</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Max Planck Institute for Demographic Research</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">14359871</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">23637064</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26332043</article-id>
         <article-categories>
            <subj-group>
               <subject>Research Article</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Does grandparental help mediate the relationship between kin presence and fertility?</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Snopkowski</surname>
                  <given-names>Kristin</given-names>
               </string-name>
               <xref ref-type="aff" rid="af1">¹</xref>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Sear</surname>
                  <given-names>Rebecca</given-names>
               </string-name>
               <xref ref-type="aff" rid="af2">²</xref>
            </contrib>
            <aff id="af1">
               <label>¹</label>Boise State University, U.S.A.</aff>
            <aff id="af2">
               <label>²</label>London School of Hygiene and Tropical Medicine, U.K.</aff>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>1</month>
            <year>2016</year>
            <string-date>JANUARY - JUNE 2016</string-date>
         </pub-date>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <day>1</day>
            <month>6</month>
            <year>2016</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">34</volume>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">e26332026</issue-id>
         <fpage>467</fpage>
         <lpage>498</lpage>
         <permissions>
            <copyright-statement>© 2016 Kristin Snopkowski &amp; Rebecca Sear</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26332043"/>
         <abstract xml:lang="eng">
            <sec>
               <label>BACKGROUND</label>
               <p>Previous research suggests that kin availability may be correlated with reproductive outcomes, but it is not clear that a causal relationship underlies these findings. Further, there is substantial variation in how kin availability is measured.</p>
            </sec>
            <sec>
               <label>OBJECTIVE</label>
               <p>We attempt to identify whether different measures of kin availability influence how kin affect reproductive outcomes and whether the effect of kin on reproductive outcomes is driven by the help that they provide.</p>
            </sec>
            <sec>
               <label>METHODS</label>
               <p>Using data from the Indonesia Family Life Survey (1993, 1997, 2000, 2007), we compare the survival of parents and parents-in-law, their co-residence, geographic proximity, contact frequency, and helping behavior in predicting fertility outcomes, and test a hypothesized causal pathway linking kin availability to reproduction via helping behavior.</p>
            </sec>
            <sec>
               <label>RESULTS</label>
               <p>We find different results if we operationalize parental availability as survival or co-residence, suggesting that these measures cannot be used interchangeably. Receiving help from parents or parents-in-law has a positive effect on progression to birth when women have fewer than three living children. Path analyses show that geographic proximity is associated with contact frequency, which in turn influences helping behavior. Kin help has a positive effect on progression to giving birth for all parental categories, but the effects are strongest for mothers-in-law.</p>
            </sec>
            <sec>
               <label>CONCLUSION</label>
               <p>In Indonesia, kin availability has a positive effect on fertility<italic>only</italic>when kin provide help, suggesting that there is a causal relationship between kin availability and fertility which is mediated via the provision of help.</p>
            </sec>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="unparsed-citations">
         <title>References</title>
         <ref id="ref1">
            <mixed-citation publication-type="other">Beise, J. (2005). The Helping and the Helpful Grandmother: The Role of Maternal and Paternal Grandmothers in Child Mortality in the Seventeenth- and Eighteenth-Century Population of French Settlers in Quebec, Canada. In: Voland, E., Chasiotis, A., and Schiefenhovel, W. (eds.). Grandmotherhood: the Evolutionary Significance of the Second Half of Female Life. New Brunswick, New Jersey, and London: Rutgers University Press.</mixed-citation>
         </ref>
         <ref id="ref2">
            <mixed-citation publication-type="other">Beise, J. and Voland, E. (2002). A multilevel event history analysis of the effects of grandmothers on child mortality in a historical German population. Demographic Research 7(13): 469–498. doi:10.4054/DemRes.2002.7.13.</mixed-citation>
         </ref>
         <ref id="ref3">
            <mixed-citation publication-type="other">Berkman, L.F. (1984). Assessing the physical health effects of social networks and social support. Annual Review of Public Health 5: 413–432. doi:10.1146/annu rev.pu.05.050184.002213.</mixed-citation>
         </ref>
         <ref id="ref4">
            <mixed-citation publication-type="other">Borgerhoff Mulder, M. and Rauch, K.L. (2009). Sexual conflict in humans: Variations and solutions. Evolutionary Anthropology: Issues, News, and Reviews 18(5): 201–214.</mixed-citation>
         </ref>
         <ref id="ref5">
            <mixed-citation publication-type="other">BPS, Statistics Indonesia and Macro International (2008). Indonesia Young Adult Reproductive Health Survey 2007. Calverton, Maryland.</mixed-citation>
         </ref>
         <ref id="ref6">
            <mixed-citation publication-type="other">Cant, M.A. and Johnstone, R.A. (2008). Reproductive conflict and the separation of reproductive generations in humans. Proceedings of the National Academy of Sciences 105(14): 5332–5336. doi:10.1073/pnas.0711911105.</mixed-citation>
         </ref>
         <ref id="ref7">
            <mixed-citation publication-type="other">Chen, F., Short, S.E., and Entwisle, B. (2000). The impact of grandparental proximity on maternal childcare in China. Population Research and Policy Review 19(6): 571–590. doi:10.1023/A:1010618302144.</mixed-citation>
         </ref>
         <ref id="ref8">
            <mixed-citation publication-type="other">Frankenberg, E. and Karoly, L. (1995). The 1993 Indonesian Family Life Survey: Overview and Field Report. Santa Monica, CA.</mixed-citation>
         </ref>
         <ref id="ref9">
            <mixed-citation publication-type="other">Frankenberg, E. and Thomas, D. (2000). The Indonesia Family Life Survey (IFLS): Study Design and Results from Waves 1 and 2. Santa Monica, CA.</mixed-citation>
         </ref>
         <ref id="ref10">
            <mixed-citation publication-type="other">Gertler, P.J. and Molyneaux, J.W. (1994). How Economic Development and Family Planning Programs Combined to Reduce Indonesian Fertility. Demography 31(1): 33–63. doi:10.2307/2061907.</mixed-citation>
         </ref>
         <ref id="ref11">
            <mixed-citation publication-type="other">Gibson, M.A. and Mace, R. (2005). Helpful grandmothers in rural Ethiopia: A study of the effect of kin on child survival and growth. Evolution and Human Behavior 26(6): 469–482. doi:10.1016/j.evolhumbehav.2005.03.004.</mixed-citation>
         </ref>
         <ref id="ref12">
            <mixed-citation publication-type="other">Hadley, C. (2004). The Costs and Benefits of Kin: Kin Networks and Children’s Health among the Pimbwe of Tanzania. Human Nature 15(4): 377–395. doi:10.1007/s 12110-004-1015-7.</mixed-citation>
         </ref>
         <ref id="ref13">
            <mixed-citation publication-type="other">Hamilton, W.D. (1966). The moulding of senescence by natural selection. Journal of Theoretical Biology 12(1): 12–45. doi:10.1016/0022-5193(66)90184-6.</mixed-citation>
         </ref>
         <ref id="ref14">
            <mixed-citation publication-type="other">Havanan, N., Knodel, J., and Sittitrai, W. (1992). The Impact of Family Size on Wealth Accumulation in Rural Thailand. Population Studies 46(1): 37–51. doi:10.1080/0032472031000145996.</mixed-citation>
         </ref>
         <ref id="ref15">
            <mixed-citation publication-type="other">Hawkes, K., O’Connell, J.F., and Blurton Jones, N.G. (1997). Hadza Women’s Time Allocation, Offspring Provisioning, and the Evolution of Long Postmenopausal Life Spans. Current Anthropology 38(4): 551–577. doi:10.1086/204646.</mixed-citation>
         </ref>
         <ref id="ref16">
            <mixed-citation publication-type="other">Hill, K. and Hurtado, A.M. (1996). Ache Life History. Hawthorne, NY: Aldine de Gruyter.</mixed-citation>
         </ref>
         <ref id="ref17">
            <mixed-citation publication-type="other">Hrdy, S.B. (2005). Evolutionary Context of Human Development: The Cooperative Breeding Model. In: Carter, C.S., Ahnert, L., Grossmann, K.E., Hrdy, S.B., Lamb, M.E., Porges, S.W., and Sachser, N. (eds.). Attachment and Bonding: A New Synthesis. Cambridge, MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="ref18">
            <mixed-citation publication-type="other">Ivey, P.K. (2000). Cooperative Reproduction in Ituri Forest Hunter‐Gatherers: Who Cares for Efe Infants? Current Anthropology 41(5): 856–866. doi:10.1086/3174 14.</mixed-citation>
         </ref>
         <ref id="ref19">
            <mixed-citation publication-type="other">Jamison, C.S., Cornell, L.L., Jamison, P.L., and Nakazato, H. (2002). Are all grandmothers equal? A review and a preliminary test of the ‘grandmother hypothesis’ in Tokugawa Japan. American Journal of Physical Anthropology 119(1): 67–76. doi:10.1002/ajpa.10070.</mixed-citation>
         </ref>
         <ref id="ref20">
            <mixed-citation publication-type="other">Jensen, G.D. and Suryani, L.K. (1992). The Balinese People: A Reinvestigation of Character. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="ref21">
            <mixed-citation publication-type="other">Johnstone, R.A. and Cant, M.A. (2010). The evolution of menopause in cetaceans and humans: the role of demography. Proceedings of the Royal Society B: Biological Sciences 277(1701): 3765–3771. doi:10.1098/rspb.2010.0988.</mixed-citation>
         </ref>
         <ref id="ref22">
            <mixed-citation publication-type="other">Johow, J. and Voland, E. (2012). Conditional grandmother effects on age at marriage, age at first birth, and completed fertility of daughters and daughters-in-law in historical Krummhörn. Human Nature 23(3): 341–359. doi:10.1007/s12110-012-9147-7.</mixed-citation>
         </ref>
         <ref id="ref23">
            <mixed-citation publication-type="other">Kaptijn, R., Thomese, F., van Tilburg, T.G., and Liefbroer, A.C. (2010). How Grandparents Matter: Support for the Cooperative Breeding Hypothesis in a Contemporary Dutch Population. Human Nature 21(4): 393–405. doi:10.1007/s 12110-010-9098-9.</mixed-citation>
         </ref>
         <ref id="ref24">
            <mixed-citation publication-type="other">Kemkes-Grottenthalef, A. (2005). Of Grandmothers, Grandfathers and Wicked Step-Grandparents. Differential Impact of Paternal Grandparents on Grandoffspring Survival. Historical Social Research 30(3): 219–239.</mixed-citation>
         </ref>
         <ref id="ref25">
            <mixed-citation publication-type="other">Kramer, K.L. (2005). Children’s Help and the Pace of Reproduction: Cooperative Breeding in Humans. Evolutionary Anthropology 14(6): 224–237. doi:10.1002/evan.20082.</mixed-citation>
         </ref>
         <ref id="ref26">
            <mixed-citation publication-type="other">Kramer, K.L. (2010). Cooperative Breeding and its Significance to the Demographic Success of Humans. Annual Review of Anthropology 39(1): 417–436. doi:10.1146/annurev.anthro.012809.105054.</mixed-citation>
         </ref>
         <ref id="ref27">
            <mixed-citation publication-type="other">Lahdenperä, M., Lummaa, V., Helle, S., Tremblay, M., and Russell, A.F. (2004). Fitness benefits of prolonged post-reproductive lifespan in women. Nature 428(6979): 178–181. doi:10.1038/nature02367.</mixed-citation>
         </ref>
         <ref id="ref28">
            <mixed-citation publication-type="other">Leonetti, D.L., Nath, D.C., and Hemam, N.S. (2007). In-law Conflict: Women’s Reproductive Lives and the Roles of their Mothers and Husbands among the Matrilineal Khasi. Current Anthropology 48(6): 861–890. doi:10.1086/520976.</mixed-citation>
         </ref>
         <ref id="ref29">
            <mixed-citation publication-type="other">Mathews, P. and Sear, R. (2013). Family and fertility: kin influence on the progression to a second birth in the british household panel study. PloS one 8(3): e56941. doi:10.1371/journal.pone.0056941.</mixed-citation>
         </ref>
         <ref id="ref30">
            <mixed-citation publication-type="other">Meehan, C.L., Quinlan, R., and Malcom, C.D. (2013). Cooperative breeding and maternal energy expenditure among Aka foragers. American Journal of Human Biology 25: 42–57. doi:10.1002/ajhb.22336.</mixed-citation>
         </ref>
         <ref id="ref31">
            <mixed-citation publication-type="other">Molyneaux, J.W. and Gertler, P.J. (2000). The Impact of Targeted Family Planning Programs in Indonesia. Population and Development Review 26: 61–85.</mixed-citation>
         </ref>
         <ref id="ref32">
            <mixed-citation publication-type="other">Moya, C. and Sear, R. (2014). Intergenerational conflicts may help explain parental absence effects on reproductive timing: a model of age at first birth in humans. PeerJ 2: e512. doi:10.7717/peerj.512.</mixed-citation>
         </ref>
         <ref id="ref33">
            <mixed-citation publication-type="other">Moya, C., Snopkowski, K., and Sear, R. (2016). What Do Men Want? Re-examining whether men benefit from higher fertility than is optimal for women. Philosophical Transactions B.</mixed-citation>
         </ref>
         <ref id="ref34">
            <mixed-citation publication-type="other">Newson, L., Postmes, T., Lea, S.E.G., and Webley, P. (2005). Why are modern families small? Toward an evolutionary and cultural explanation for the demographic transition. Personality and Social Psychology Review 9(4): 360–375. doi:10.1207/s15327957pspr0904_5.</mixed-citation>
         </ref>
         <ref id="ref35">
            <mixed-citation publication-type="other">Newson, L., Postmes, T., Lea, S.E.G., Webley, P., Richerson, P.J., and Mcelreath, R. (2007). Influences on communication about reproduction: the cultural evolution of low fertility. Evolution and Human Behavior 28(3): 199–210. doi:10.1016/j.evolhumbehav.2007.01.003.</mixed-citation>
         </ref>
         <ref id="ref36">
            <mixed-citation publication-type="other">Penn, D.J. (1999). Explaining the human demographic transition. Trends in ecology &amp; evolution 14(1): 32. doi:10.1016/S0169-5347(98)01455-4.</mixed-citation>
         </ref>
         <ref id="ref37">
            <mixed-citation publication-type="other">Rabe-Hesketh, S., Skrondal, A., and Pickles, A. (2004). Generalized multilevel structural equation modeling. Psychometrika 69(2): 167–190. doi:10.1007/BF02 295939.</mixed-citation>
         </ref>
         <ref id="ref38">
            <mixed-citation publication-type="other">Rammohan, A. and Johar, M. (2009). The Determinants of Married Women’s Autonomy in Indonesia. Feminist Economics 15(4): 31–55. doi:10.1080/135457 00903153989.</mixed-citation>
         </ref>
         <ref id="ref39">
            <mixed-citation publication-type="other">Scelza, B. (2011). The place of proximity: social support in mother-adult daughter relationships. Human Nature 22(1-2): 108–127. doi:10.1007/s12110-011-9112-x.</mixed-citation>
         </ref>
         <ref id="ref40">
            <mixed-citation publication-type="other">Sear, R. (2008). Kin and Child Survival in Rural Malawi. Human Nature 19(3): 277–293. doi:10.1007/s12110-008-9042-4.</mixed-citation>
         </ref>
         <ref id="ref41">
            <mixed-citation publication-type="other">Sear, R. and Coall, D. (2011). How much does family matter? Cooperative breeding and the demographic transition. Population and development review 37(Suppl 1): 81–112. doi:10.1111/j.1728-4457.2011.00379.x.</mixed-citation>
         </ref>
         <ref id="ref42">
            <mixed-citation publication-type="other">Sear, R. and Mace, R. (2008). Who keeps children alive? A review of the effects of kin on child survival. Evolution and Human Behavior 29(1): 1–18. doi:10.1016/j. evolhumbehav.2007.10.001.</mixed-citation>
         </ref>
         <ref id="ref43">
            <mixed-citation publication-type="other">Sear, R., Mace, R., and McGregor, I.A. (2003). The effects of kin on female fertility in rural Gambia. Evolution and Human Behavior 24: 25–42. doi:10.1016/S1090-5138(02)00105-8.</mixed-citation>
         </ref>
         <ref id="ref44">
            <mixed-citation publication-type="other">Skinner, G.W. (2004). Grandparental effects on reproductive strategizing. Demographic Research 11(5): 111–148. doi:10.4054/DemRes.2004.11.5.</mixed-citation>
         </ref>
         <ref id="ref45">
            <mixed-citation publication-type="other">Snopkowski, K. and Sear, R. (2013). Kin influences on fertility in Thailand: Effects and mechanisms. Evolution and Human Behavior 34(2): 130–138. doi:10.1016/j. evolhumbehav.2012.11.004.</mixed-citation>
         </ref>
         <ref id="ref46">
            <mixed-citation publication-type="other">Snopkowski, K. and Sear, R. (2015). Grandparental help in Indonesia is directed preferentially towards needier descendants: A potential confounder when exploring grandparental influences on child health. Social Science &amp; Medicine 128: 105–114. doi:10.1016/j.socscimed.2015.01.012.</mixed-citation>
         </ref>
         <ref id="ref47">
            <mixed-citation publication-type="other">Sosa, R., Kennell, J., Klaus, M., Robertson, S., and Urrutia, J. (1980). The effect of a supportive companion on perinatal problems, length of labor, and mother-infant interactions. The New England Journal of Medicine 303(11): 597–600. doi:10.1056/NEJM198009113031101.</mixed-citation>
         </ref>
         <ref id="ref48">
            <mixed-citation publication-type="other">Strassmann, B.I. (2011). Cooperation and competition in a cliff-dwelling people. Proceedings of the National Academy of Sciences 108(suppl. 2): 10894–10901. doi:10.1073/pnas.1100306108.</mixed-citation>
         </ref>
         <ref id="ref49">
            <mixed-citation publication-type="other">Strauss, J., Beegle, K., Sikoki, B., Dwiyanto, A., Herawati, Y., and Witoelar, F. (2004). The Third Wave of the Indonesia Family Life Survey (IFLS): Overview and Field Report. Santa Monica, CA.</mixed-citation>
         </ref>
         <ref id="ref50">
            <mixed-citation publication-type="other">Strauss, J., Witoelar, F., Sikoki, B., and Wattie, A.M. (2009). The Fourth Wave of the Indonesian Family Life Survey (IFLS4): Overview and Field Report. Santa Monica, CA.</mixed-citation>
         </ref>
         <ref id="ref51">
            <mixed-citation publication-type="other">Thornton, A., Freedman, R., Sun, T.-H., and Chang, M.-C. (1986). Intergenerational Relations and Reproductive Behavior in Taiwan. Demography 23(2): 185–197. doi:10.2307/2061615.</mixed-citation>
         </ref>
         <ref id="ref52">
            <mixed-citation publication-type="other">Tsay, W.-J. and Chu, C.Y.C. (2005). The pattern of birth spacing during Taiwan’s demographic transition. Journal of Population Economics 18(2): 323–336. doi:10.1007/s00148-004-0200-7.</mixed-citation>
         </ref>
         <ref id="ref53">
            <mixed-citation publication-type="other">Tymicki, K. (2004). Kin influence on female reproductive behavior: the evidence from reconstitution of the Bejsce parish registers, 18th to 20th centuries, Poland. American Journal of Human Biology 16(5): 508–522. doi:10.1002/ajhb.20059.</mixed-citation>
         </ref>
         <ref id="ref54">
            <mixed-citation publication-type="other">UNICEF (2000). The State of the World’s Children. http://www.unicef.org/sowc00/stat3.htm.</mixed-citation>
         </ref>
         <ref id="ref55">
            <mixed-citation publication-type="other">UNICEF (2013). At A Glance: Indonesia. http://www.unicef.org/infobycountry/indonesia_statistics.html.</mixed-citation>
         </ref>
         <ref id="ref56">
            <mixed-citation publication-type="other">United Nations (2011). World Population Prospects, the 2010 Revision. http://esa.un.org/wpp/Excel-Data/fertility.htm.</mixed-citation>
         </ref>
         <ref id="ref57">
            <mixed-citation publication-type="other">Vandell, D.L., McCartney, K., Owen, M.T., Booth, C., and Clarke-Stewart, A. (2003). Variations in Child Care by Grandparents during the First Three Years. Journal of Marriage and Family 65(2): 375–381. doi:10.1111/j.1741-3737.2003. 00375.x.</mixed-citation>
         </ref>
         <ref id="ref58">
            <mixed-citation publication-type="other">Waynforth, D. (2012). Grandparental investment and reproductive decisions in the longitudinal 1970 British cohort study. Proceedings of the Royal Society B 279(1731): 1155–1160. doi:10.1098/rspb.2011.1424.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
