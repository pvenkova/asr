<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt13kh4v1</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt6wq0g9</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Zoology</subject>
         <subject>Biological Sciences</subject>
      </subj-group>
      <book-title-group>
         <book-title>A Field Guide to the Larger Mammals of Tanzania</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Foley</surname>
               <given-names>Charles</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>Foley</surname>
               <given-names>Lara</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib3">
            <name name-style="western">
               <surname>Lobora</surname>
               <given-names>Alex</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib4">
            <name name-style="western">
               <surname>De Luca</surname>
               <given-names>Daniela</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib5">
            <name name-style="western">
               <surname>Msuha</surname>
               <given-names>Maurus</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib6">
            <name name-style="western">
               <surname>Davenport</surname>
               <given-names>Tim R.B.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib7">
            <name name-style="western">
               <surname>Durant</surname>
               <given-names>Sarah</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>08</day>
         <month>06</month>
         <year>2014</year>
      </pub-date>
      <isbn content-type="epub">9781400852802</isbn>
      <isbn content-type="epub">1400852803</isbn>
      <publisher>
         <publisher-name>Princeton University Press</publisher-name>
         <publisher-loc>Princeton, New Jersey</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2014</copyright-year>
         <copyright-holder>Charles Foley</copyright-holder>
         <copyright-holder>Lara Foley</copyright-holder>
         <copyright-holder>Alex Lobora</copyright-holder>
         <copyright-holder>Daniela De Luca</copyright-holder>
         <copyright-holder>Maurus Msuha</copyright-holder>
         <copyright-holder>Tim R.B. Davenport</copyright-holder>
         <copyright-holder>Sarah Durant</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt6wq0g9"/>
      <abstract abstract-type="short">
         <p>Home to the Serengeti National Park, Ngorongoro Crater, and Mount Kilimanjaro, Tanzania offers some of the finest big game watching in the world, from elephants and rhinos to chimpanzees and lions. This field guide covers all the larger mammals of Tanzania, including marine mammals and some newly discovered species. Detailed accounts are provided for more than 135 species, along with color photos, color illustrations of marine mammals, and distribution maps. Accounts for land species give information on identification, subspecies, similar species, ecology, behavior, distribution, conservation status, and where best to see each species. The guide also features plates with side-by-side photographic comparisons of species that are easily confused, as well as first-time-ever species checklists for every national park.</p>
         <p>The definitive, most up-to-date field guide to the larger mammals of Tanzania, including marine mammalsFeatures detailed species accounts and numerous color photos throughoutProvides tips on where to see each speciesIncludes species checklists for every national park</p>
      </abstract>
      <counts>
         <page-count count="320"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>1</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>3</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Mduma</surname>
                           <given-names>Simon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>7</fpage>
                  <abstract>
                     <p>Tanzania is one of the most celebrated nations on earth for wildlife. From migrating wildebeest to famous chimpanzees, the country has become synonymous with concentrations of plains game and forest biodiversity. This has been reinforced by a long history of research in some of Africa’s most iconic National Parks and reserves, including Serengeti, Kilimanjaro, Selous, Gombe and Ngorongoro Crater. Recent high profile discoveries of large mammals have shown how much there is still to learn.</p>
                     <p>Against this background it gives me great pleasure to introduce this significant volume:<italic>A Field Guide to the Larger Mammals of Tanzania</italic>. Mammals are often</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>8</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.5</book-part-id>
                  <title-group>
                     <title>Acknowledgements</title>
                  </title-group>
                  <fpage>9</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.6</book-part-id>
                  <title-group>
                     <title>Conservation in Tanzania</title>
                  </title-group>
                  <fpage>10</fpage>
                  <abstract>
                     <p>Tanzania is one of the most important nations for wildlife conservation in mainland Africa, boasting an extraordinary wealth of flora and fauna, including many endemic species and subspecies. To date, over 10,000 species of trees and plants, 1,100 species of birds and at least 340 species of mammals have been recorded. Tanzania is a particularly important country for large mammals and supports the highest diversity of both ungulates and primates in continental Africa. It also hosts the largest numbers of many iconic African mammals, including Common Wildebeest, Plains Zebra, Giraffe, African Buffalo, Hippopotamus, Lion and African Wild Dog. The nutrient-rich</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.7</book-part-id>
                  <title-group>
                     <title>How to use this book</title>
                  </title-group>
                  <fpage>12</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.8</book-part-id>
                  <title-group>
                     <title>Watching mammals in Tanzania</title>
                  </title-group>
                  <fpage>16</fpage>
                  <abstract>
                     <p>The vast majority of people visiting Tanzania on safari use the services of a safari company or tour operator. This approach is highly recommended since these businesses are well placed to manage everything for you, including providing a vehicle with a driver-guide, booking accommodation and organizing entry permits for all the National Parks. The driver-guides tend to be very good at spotting wildlife, know the best places to go, and can obtain information about interesting mammal sightings from other guides. There are many different companies covering a wide price range. It is strongly recommended that your selected company is registered</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.9</book-part-id>
                  <title-group>
                     <title>Tanzania’s major vegetation types</title>
                  </title-group>
                  <fpage>20</fpage>
                  <abstract>
                     <p>Deciduous bushland and thicket of<italic>Acacia</italic>and/or<italic>Commiphora trees</italic>typically 3–5 m (10–16 ft) tall, the majority of which are bushy with branches near the base, interspersed with wooded grassland. Occasional tall trees such as<italic>Acacia tortilis</italic>and Baobabs may also occur throughout.</p>
                     <p>Note: The genus<italic>Acacia</italic>has recently been renamed<italic>Vachellia</italic>, however we continue to use<italic>Acacia</italic>throughout this text.</p>
                     <p>Semi-deciduous miombo woodland with annual rainfall less than 1,000 mm (39”). Canopy typically less than 15m (50 ft).<italic>Brachystegia</italic>and<italic>Julbernardia</italic>dominate on soils that are leached and acidic and often shallow and stony. Tree trunks are</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.10</book-part-id>
                  <title-group>
                     <title>Overview of mammalian families included in the book</title>
                  </title-group>
                  <fpage>24</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.11</book-part-id>
                  <title-group>
                     <title>AARDVARK: Tubulidentata</title>
                  </title-group>
                  <fpage>26</fpage>
                  <abstract>
                     <p>A thickset animal with a rounded body, a long, tapered tail, and a small, pig-like head with a very long nose and ears. The body colour ranges from light brown to pinkish-grey and the legs are black. Young animals are usually quite hairy, while adults may be mostly hairless on the back. There are four large claws on the front foot and five on the hind foot. Males and females are the same size.</p>
                     <p>Four subspecies are listed for Tanzania:</p>
                     <p>
                        <italic>O. a. lademanni</italic>,<italic>O. a. matschiei</italic>,</p>
                     <p>
                        <italic>O. a. observandus</italic>and<italic>O. a. ruvanensis</italic>,</p>
                     <p>although their validity is doubtful due to</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.12</book-part-id>
                  <title-group>
                     <title>ELEPHANT-SHREWS: Macroscelidea</title>
                  </title-group>
                  <fpage>28</fpage>
                  <abstract>
                     <p>A large elephant-shrew that ranges in colour and pattern from light brown with distinct spots to very dark with no spots. There may be significant variation within a single population. Melanistic individuals have also been recorded.</p>
                     <p>
                        <italic>R. c. reichardi</italic>: central montane areas and western Tanzania; light rufous-brown body with six black-and-white bands on the back and sides that run from shoulder to flank.</p>
                     <p>
                        <italic>R. c. macrurus</italic>: southeast Tanzania; rump usually dark purple, front light rufous, spots may be absent.</p>
                     <p>Subspecies<italic>macrurus</italic>and the Black-and-rufous Elephant-shrew look very similar but the known ranges of the two do not overlap.</p>
                     <p>The preferred</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.13</book-part-id>
                  <title-group>
                     <title>HYRAXES: Hyracoidea</title>
                  </title-group>
                  <fpage>32</fpage>
                  <abstract>
                     <p>A stocky hyrax with short legs, ears and face, giving it a snub-nosed appearance. The dorsal fur is brown and the underparts are cream-coloured in adults, slightly paler in juveniles. There is a patch of erectile hair covering a dorsal gland, which can be yellow, orange or brown. The dense fur is up to 2.5 cm (1”) long, and there are long, scattered guard hairs across the body.</p>
                     <p>
                        <italic>P. c. matschiei</italic>: only subspecies found in Tanzania.</p>
                     <p>The Bush Hyrax (<italic>page</italic>34 ) has a white, rather than yellow, belly and more pronounced white markings above the eyes than adult Rock</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.14</book-part-id>
                  <title-group>
                     <title>ELEPHANT: Proboscidea</title>
                  </title-group>
                  <fpage>38</fpage>
                  <abstract>
                     <p>The largest living land mammal, Savanna Elephants have huge bodies, a long, highly mobile trunk, and large ears. Their bodies are grey, with little hair, and the tail is long with a black tassel at the tip. Both sexes have tusks, although these are typically much larger in males. The largest pair of tusks ever recorded (103 kg and 97 kg | 227 lb and 213 lb) were from a bull shot on Mount Kilimanjaro at the end of the 19th century. Adult males are significantly larger than females.</p>
                     <p>Unmistakable.</p>
                     <p>Savanna Elephants are highly adaptable animals that live in a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.15</book-part-id>
                  <title-group>
                     <title>PRIMATES: Primates</title>
                  </title-group>
                  <fpage>40</fpage>
                  <abstract>
                     <p>A large, powerfully built ape with black fur, although older individuals are often partially bald and have white hair on their chin and lower back. The skin on the face and hands is mostly black, but can be pink or mottled. As with all great apes, Chimpanzees have no tail. They are quadrupedal and walk on their knuckles.</p>
                     <p>
                        <italic>P. t. schweinfurthii</italic>: the only subspecies found in Tanzania.</p>
                     <p>Unlikely to be mistaken for any other mammal.</p>
                     <p>The preferred habitat of Chimpanzees is forest, forest-woodland mosaic, and occasionally wooded grasslands. They live in multi-male and multi-female groups of 20 to over 100</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.16</book-part-id>
                  <title-group>
                     <title>RODENTS: Rodentia</title>
                  </title-group>
                  <fpage>84</fpage>
                  <abstract>
                     <p>This species resembles a small kangaroo, with long, powerful back legs, very short front legs, and a long tail. The body is yellowish or reddish-brown and the underparts are pale or white. The tail, which is used for balance, is the same colour as the body but has a long, bushy black tip. The ears are long and erect. There are four toes on the hind legs and five on the front legs. It moves by hopping on its hind legs, leaping as much as 2 metres (6 feet) with each bound.</p>
                     <p>Previously described as a subspecies of South African</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.17</book-part-id>
                  <title-group>
                     <title>HARES AND RABBITS: Lagomorpha</title>
                  </title-group>
                  <fpage>88</fpage>
                  <abstract>
                     <p>Both the African Savanna Hare and the Cape Hare have long ears, well-developed hind legs, small tails and flecked brown-grey backs with white underparts. Although they are extremely difficult to tell apart in the field, a range of morphological and ecological characteristics can help to identify them in East Africa.</p>
                     <p>The African Savanna Hare is relatively dark, has a distinct rufous nape and ears that are longer than its head. It also commonly has a white spot on its forehead. The shape of the head is gently convex from the forehead to the tip of the nose, and it has</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.18</book-part-id>
                  <title-group>
                     <title>HEDGEHOGS: Erinaceomorpha</title>
                  </title-group>
                  <fpage>92</fpage>
                  <abstract>
                     <p>A small animal with numerous short spines (1.5–2.5 cm | 0.6–1” long) covering the upperparts of the body. The underparts and front and sides of the face are white, while the nose and ears are black. The spines are white at the base, black or brown at the centre, and tipped with white. There are five toes on the front feet and four on the hind feet.</p>
                     <p>Unlikely to be mistaken for any other mammal.</p>
                     <p>White-bellied Hedgehogs are found in a wide variety of habitats including open grassland, wooded savanna, open forests, urban gardens, and agricultural land. They</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.19</book-part-id>
                  <title-group>
                     <title>PANGOLINS: Pholidota</title>
                  </title-group>
                  <fpage>94</fpage>
                  <abstract>
                     <p>Ground Pangolins are covered in large, broad, brown scales made of fused hair. There are 11–13 rows of scales between the back of the head and the base of the tail. The head is small and the nose is short and pointed. The tail is very muscular and broad at the base, with 11–13 rows of scales along its length and a rounded tip. All feet are covered in scales. The front feet are short with five strong claws, the central claw reaching up to 6 cm (2") long. The soles of the hind feet are rounded. Ground</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.20</book-part-id>
                  <title-group>
                     <title>CARNIVORES: Carnivora</title>
                  </title-group>
                  <fpage>100</fpage>
                  <abstract>
                     <p>The Side-striped Jackal is the largest jackal in Tanzania. The body is a dull grey colour, with a white or buff-coloured side stripe, of variable prominence, running from the shoulder to the hip with a black margin below. The legs are a light grey-brown, and the underside and throat are cream-coloured. It has a dog-like face with relatively small ears that are grey on the back. The tail is mostly black with a distinctive white tip, although the white tip is sometimes absent. Side-striped Jackals in southern Tanzania can be quite dark.</p>
                     <p>Can usually be easily distinguished from the Black-backed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.21</book-part-id>
                  <title-group>
                     <title>ODD-TOED UNGULATES: Perissodactyla</title>
                  </title-group>
                  <fpage>168</fpage>
                  <abstract>
                     <p>A very large, dark grey animal, with two horns on the face between the eyes and the nose. The front horn, near the nose, is generally longer (20–66 cm | 8–26" in Tanzania) than the rear horn. The skin is mostly hairless, and typically assumes the colour of the local soil as Black Rhinoceros frequently wallow in mud. The tail has a small fringe of black hair on the tip, and the edges of the ears are also lined with short, black hairs. The upper lip is pointed and used for grasping vegetation.</p>
                     <p>
                        <italic>D. b. michaeli</italic>: northern Tanzania; the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.22</book-part-id>
                  <title-group>
                     <title>EVEN-TOED UNGULATES: Artiodactyla</title>
                  </title-group>
                  <fpage>172</fpage>
                  <abstract>
                     <p>A medium-sized pig with a long muzzle. Coat colour is highly variable, ranging from light red to almost entirely black. Females and young typically have more red on the body, while older males are darker. There is a prominent dorsal crest running from the nape to the rump that is usually black-tipped with white hairs. The hairs on the body are long and coarse. The forehead is white with long white hairs on the side of the face, which are occasionally all-black in males. The lower tusks are long (up to 23 cm | 9”), although they do not protrude</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.23</book-part-id>
                  <title-group>
                     <title>DUGONG: Sirenia</title>
                  </title-group>
                  <fpage>246</fpage>
                  <abstract>
                     <p>Dugongs have large, rounded bodies, with two small, paddle-like flippers and a fluke-shaped tail. They have smooth, nearly naked skin, which is slate-grey above and paler below. The head is small and pig-like with a broad, flat muzzle and sensitive bristles covering the upper lip. The nostrils are positioned at the top of the nose and have flaps that close when the animal is submerged. Females are usually larger than males.</p>
                     <p>Unlikely to be mistaken for any other mammal.</p>
                     <p>Dugongs are found along the coastline in shallow marine waters, usually at depths of 1–12m (3–40 ft). They mostly</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.24</book-part-id>
                  <title-group>
                     <title>CETACEANS: Cetacea</title>
                  </title-group>
                  <fpage>248</fpage>
                  <abstract>
                     <p>A large whale, black-grey above and varying from dark to white below. The long flippers are almost one-third of the body length and usually all white or dark above and white below. The tail flukes, which have an irregular trailing edge, are dark on the upper side and patterned black-and-white on the underside. The dorsal fin is small and stubby and set two-thirds of the way along the back on a small hump. It often shows its back, dorsal fin, and fluke when it dives. The bushy blow is highly visible, up to 3 m (10 ft) tall.</p>
                     <p>Sperm Whales</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.25</book-part-id>
                  <title-group>
                     <title>CARNIVORES: Carnivora</title>
                  </title-group>
                  <fpage>261</fpage>
                  <abstract>
                     <p>A large seal with a short snout. Adults are dark brown on the back and sides and have a creamy-yellow chest and face. The top of the head is dark brown, contrasting with the yellow face and creating a mask-like effect. Pups are black and moult to the same colour as the adults when approximately three months old.</p>
                     <p>No other species of seal has been positively identified from Tanzania. However, it is possible that vagrant Afro-Australian Fur Seals<italic>Arctocephalus pusillus</italic>and Antarctic Fur Seals<italic>Arctocephalus gazella</italic>may also occasionally occur. The former have a longer snout than the Subantarctic Fur</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.26</book-part-id>
                  <title-group>
                     <title>Species comparison spreads</title>
                  </title-group>
                  <fpage>262</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.27</book-part-id>
                  <title-group>
                     <title>National Parks and major protected areas of Tanzania</title>
                  </title-group>
                  <fpage>269</fpage>
                  <abstract>
                     <p>The following pages provide summary information and species lists for the major protected areas in Tanzania. These fall under the jurisdiction of several different agencies, and are managed for different purposes. The mainland National Parks are operated by Tanzania National Parks (TANAPA), and are designated exclusively for wildlife conservation and photographic tourism. Jozani Chwaka Bay National Park in Zanzibar (Unguja Island) has similar status to the mainland national parks but is administered by the Zanzibar Department of Forestry and Nonrenewable Natural Resources. The Ngorongoro Conservation Area operates under the jurisdiction of the Ngorongoro Conservation Area Authority (NCAA), and allows multiple</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.28</book-part-id>
                  <title-group>
                     <title>Glossary</title>
                  </title-group>
                  <fpage>306</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.29</book-part-id>
                  <title-group>
                     <title>Photographic credits</title>
                  </title-group>
                  <fpage>309</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.30</book-part-id>
                  <title-group>
                     <title>Recommended further reading and references</title>
                  </title-group>
                  <fpage>312</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt6wq0g9.31</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>317</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
