<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="doi">10.2307/j.ctt1c84g3v</book-id>
      <subj-group subj-group-type="discipline">
         <subject>Religion</subject>
      </subj-group>
      <book-title-group>
         <book-title>Kenotic Ecclesiology</book-title>
         <subtitle>Select Writings of Donald M. MacKinnon</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="editor" id="contrib1">
            <name name-style="western">
               <surname>McDowell</surname>
               <given-names>John C.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib2">
            <name name-style="western">
               <surname>Kirkland</surname>
               <given-names>Scott A.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="editor" id="contrib3">
            <name name-style="western">
               <surname>Moyse</surname>
               <given-names>Ashley John</given-names>
            </name>
         </contrib>
         <role content-type="editor">editors</role>
      </contrib-group>
      <contrib-group>
         <contrib contrib-type="foreword-author" id="contrib4">
            <role>Foreword by</role>
            <name name-style="western">
               <surname>Williams</surname>
               <given-names>Rowan</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>11</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9781451496284</isbn>
      <isbn content-type="ppub">1451496281</isbn>
      <isbn content-type="epub">9781506418988</isbn>
      <isbn content-type="epub">1506418988</isbn>
      <publisher>
         <publisher-name>Augsburg Fortress</publisher-name>
         <publisher-loc>Minneapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2016</copyright-year>
         <copyright-holder>Fortress Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt1c84g3v"/>
      <abstract abstract-type="short">
         <p>Donald M. MacKinnon has been one of the most important and influential of the post-World War British theologians, significantly impacting the development and subsequent work of the likes of Rowan Williams, Nicholas Lash and John Milbank, among many other notable theologians. A younger generation largely emerging from Cambridge, but with influence elsewhere, has more recently brought MacKinnon’s eclectic and occasionalist work to a larger audience worldwide. In this collection, MacKinnon’s central writings on the major themes of ecclesiology, and especially the relationship of the church to theology, are gathered in one source. The volume will feature several of MacKinnon’s important early texts. These will include two short books published in the “Signposts" series during World War II, and a collection of later essays entitled “The Stripping of the Altars."</p>
      </abstract>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Williams</surname>
                           <given-names>Rowan</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>ix</fpage>
                  <abstract>
                     <p>Donald MacKinnon’s reputation remains both powerful and controversial. In recent years, he has begun to receive some of the attention he deserves from younger researchers, and we have seen a more balanced and three-dimensional picture emerging—rather than what was earlier the conventional focus on his supposedly “near-Manichaean” view of tragedy (and on his undoubtedly manifold personal eccentricities). There have been detailed intellectual biographies, anthologies of his mature work, and dissertations on his very complex philosophical sensibility.</p>
                     <p>All this is most welcome. But perhaps one area that still has to be fully examined is his passionate engagement with the life</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.4</book-part-id>
                  <title-group>
                     <title>Editors' Acknowledgements</title>
                  </title-group>
                  <fpage>xiii</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.5</book-part-id>
                  <title-group>
                     <title>Volume Introduction:</title>
                     <subtitle>Donald MacKinnon, Speaking Honestly to Ecclesial Power</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>McDowell</surname>
                           <given-names>John C.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Introducing the moral philosopher and philosophical theologian Donald Mackenzie MacKinnon (1913–1994) is not an easy business. Of course, those who are already familiar with the work of the Scottish Episcopalian from Oban in the Highlands will know this but regard it as a necessary difficulty. It is<italic>necessary</italic>for two main reasons. Firstly, despite the fact that he is arguably the most influential and important postwar British philosophical theologian, (although many would want to place T. F. Torrance in this category), he remains relatively unknown to theological scholarship outside of those who have in some way had connections with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.6</book-part-id>
                  <title-group>
                     <title>Part 1 Introduction:</title>
                     <subtitle>The Terrible Occasion and Particular Paradox of the Gospel</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Moyse</surname>
                           <given-names>Ashley John</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>The 1940 Signposts series, for which the following two texts of part 1 were written, offered its readers twelve small books released monthly that intended to illuminate the vital questions of human life, answerable by way of Thomist philosophy and of the Anglo-Catholic theology representative of the series contributors. Rather, as the publisher’s note reads in the front matter of each volume, this series is one “dealing with the relevance of the Christian Faith to the contemporary crisis of civilisation.”¹ Although intended to provide such answers as part of the series, MacKinnon’s particular contributions, some have declared, failed to have</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.7</book-part-id>
                  <title-group>
                     <title>God the Living and True</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MacKinnon</surname>
                           <given-names>Donald M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>41</fpage>
                  <abstract>
                     <p>This work claims to be no more than an introduction to the study of the subject with which it deals. It seeks to exhibit some characteristic emphases of Christian theology, and to suggest some of the consequences involved in admitting it to be an autonomous science. If it stimulates further thought concerning the fundamental problems it raises, it will have served its purpose.</p>
                     <p>I should like to thank many friends with whom I have discussed these matters for the light they have thrown on them. But specially I must acknowledge my debt to the Rev. T. F. Torrance of New</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.8</book-part-id>
                  <title-group>
                     <title>The Church of God</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MacKinnon</surname>
                           <given-names>Donald M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>In the preparation of this book, my main debt has undoubtedly been to the late Sir Edwyn Hoskyns’s writings. More perhaps than any other English theologian, he has helped many of my generation to understand the character of Christianity as a revealed religion. In the later parts of the book, I owe a great deal to the more recent writings of Mr. Christopher Dawson, especially, perhaps, to the chapter “Christianity and Polities,” in his book<italic>Beyond Politics</italic>(Sheed and Ward, 1939).</p>
                     <p>My colleague here, the Rev. J. D. M. Stuart, has helped me considerably with the preparation of the manuscript,</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.9</book-part-id>
                  <title-group>
                     <title>Part 2 Introduction:</title>
                     <subtitle>“That One Man Should Die for the People”: Ecclesiological Fundamentalism, Kenosis and the Tragic</subtitle>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Kirkland</surname>
                           <given-names>Scott A.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>159</fpage>
                  <abstract>
                     <p>Throughout the Signposts texts, MacKinnon often had occasion to speak of the church as the extension of the incarnation. Yet when he had spoken in this way, it had always been tensely related to a rather more Protestant proclivity—informed by Karl Barth, Emil Brunner and others—toward a more interrogative mood. If the church is the extension of the incarnation, it is only in its witness to the incarnate one who has entered into the precariousness of the wilderness. The texts in this particular volume,<italic>The Stripping of the Altars</italic>, are all, in one way or another, concerned with</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.10</book-part-id>
                  <title-group>
                     <title>The Stripping of the Altars</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>MacKinnon</surname>
                           <given-names>Donald M.</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>169</fpage>
                  <abstract>
                     <p>The lecture that introduces this volume was delivered as the Gore Memorial Lecture in Westminster Abbey on November 5, 1968. The version spoken in the Abbey was somewhat shorter than the one that is now published.¹</p>
                     <p>It may seem improper for one whose primary concern is the philosophy of religion to venture into the field of ecclesiology. But the crisis of belief through which we are living, and of which I am professionally aware literally every day, inevitably affects and is affected by the actual situation of the churches. Thus the weary archaism of the present establishment of the Church</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.11</book-part-id>
                  <title-group>
                     <title>Select Bibliography of MacKinnon’s Works</title>
                  </title-group>
                  <fpage>239</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.12</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>243</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="doi">10.2307/j.ctt1c84g3v.13</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>246</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
