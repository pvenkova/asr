<?xml version="1.0" encoding="UTF-8"?>
<article article-type="research-article" dtd-version="1.0" xml:lang="en">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="publisher-id">israelstudies</journal-id>
         <journal-title-group>
            <journal-title content-type="full">Israel Studies</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Indiana University Press</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">10849513</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">1527201X</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="publisher-id">ISR.15.3.121</article-id>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="doi">10.2979/ISR.2010.15.3.121</article-id>
         <article-categories>
            <subj-group subj-group-type="heading">
               <subject>
                  <bold>Articles</bold>
               </subject>
            </subj-group>
            <subj-group subj-group-type="subject">
               <subject>History, Jewish studies</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>Africa in Israeli Foreign Policy—Expectations and Disenchantment: Historical and Diplomatic Aspects</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author" equal-contrib="yes">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <surname>Oded</surname>
                  <given-names>Arye</given-names>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">
            <month>October</month>
            <year>2010</year>
            <string-date>Fall 2010</string-date>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink">15</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">isr.2010.15.issue-3</issue-id>
         <fpage>121</fpage>
         <lpage>142</lpage>
         <permissions>
            <copyright-statement>©2010 Ben-Gurion University of the Negev and Ben-Gurion Research Institute for the Study of Israel and Zionism</copyright-statement>
            <copyright-year>2010</copyright-year>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/10.2979/isr.2010.15.3.121"/>
         <abstract>
            <title>ABSTRACT</title>
            <p>In the 1960s, when most African states south of the Sahara achieved independence, Israel was one of the first countries to establish diplomatic relations with them and to offer them assistance. Israel was a major “donor” in Africa, and Israeli ambassadors operated in 33 countries. Africa had an important place in Israel's foreign policy. However, in 1973, during and after the Yom Kippur War, in an unexpected and severely disappointing move, almost all African countries severed their diplomatic relations with Israel. Ten years later, Israel returned, gradually, to Africa. The article examines the factors that influenced these changes and to what extent Israel drew the necessary conclusions after returning to Africa. Special emphasis is given to the making of Israel-African policy in the 1960s, which still remains the basis of Israel's present activities on the continent, although with significant changes.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>en</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list content-type="parsed-citations">
         <title>Notes</title>
         <ref id="ref1">
            <label>1.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Samuel Decalo</string-name>
               </person-group>,<article-title>“Africa and the U.N. Anti-Zionism Resolution,”</article-title>
               <source>
                  <italic>Cultures et Development</italic>
               </source>,<volume>8</volume>(<year>1976</year>)<fpage>89</fpage>–<lpage>117</lpage>, especially<fpage>92</fpage>.</mixed-citation>
         </ref>
         <ref id="ref2">
            <label>2.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Moshe Yegar</string-name>
               </person-group>,<source>
                  <italic>The Long Journey to Asia</italic>
               </source>(,<year>2004</year>)<fpage>84</fpage>[<comment>Hebrew</comment>].</mixed-citation>
         </ref>
         <ref id="ref3">
            <label>3.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Ehud Avriel</string-name>
               </person-group>,<article-title>“Some Minute Circumstances (Memoir),”</article-title>
               <source>
                  <italic>The Jerusalem Quarterly</italic>
               </source>,<volume>14</volume>(<year>1980</year>)<fpage>28</fpage>–<lpage>40</lpage>.</mixed-citation>
         </ref>
         <ref id="ref4">
            <label>4.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Colin Legum</string-name>
               </person-group>,<source>
                  <italic>Pan-Africansim</italic>
               </source>(,<year>1962</year>)<fpage>188</fpage>.</mixed-citation>
         </ref>
         <ref id="ref5">
            <label>5.</label>
            <mixed-citation publication-type="other">
               <italic>Ibid.</italic>,<fpage>182</fpage>.</mixed-citation>
         </ref>
         <ref id="ref6">
            <label>6.</label>
            <mixed-citation publication-type="book">Quoted in<person-group person-group-type="author">
                  <string-name>Golda Meir</string-name>
               </person-group>,<source>
                  <italic>My Life</italic>
               </source>(,<year>1976</year>)<fpage>266</fpage>.</mixed-citation>
         </ref>
         <ref id="ref7">
            <label>7.</label>
            <mixed-citation publication-type="other">
               <italic>Ibid.</italic>,<fpage>264</fpage>.</mixed-citation>
         </ref>
         <ref id="ref8">
            <label>8.</label>
            <mixed-citation publication-type="other">
               <italic>Ibid.</italic>,<fpage>265</fpage>.</mixed-citation>
         </ref>
         <ref id="ref9">
            <label>9.</label>
            <mixed-citation publication-type="journal">
               <person-group person-group-type="author">
                  <string-name>Benjamin Neuberger</string-name>
               </person-group>,<article-title>“Early African Nationalism, Judaism and Zionism,”</article-title>
               <source>
                  <italic>Africa Bulletin</italic>
               </source>,<volume>15</volume>(<year>1989</year>)<fpage>3</fpage>–<lpage>5</lpage>.</mixed-citation>
         </ref>
         <ref id="ref10">
            <label>10.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Rafael Ruppin</string-name>
               </person-group>,<source>
                  <italic>A Mission to Tanganyika</italic>
               </source>(,<year>1986</year>) [<comment>Hebrew</comment>].</mixed-citation>
         </ref>
         <ref id="ref11">
            <label>11.</label>
            <mixed-citation publication-type="other">Israel State Archives (ISA), HZ 7225/4, Ben-Horin, to Eytan, 20 May 1959.</mixed-citation>
         </ref>
         <ref id="ref12">
            <label>12.</label>
            <mixed-citation publication-type="other">ISA, HZ 6382/16, 25 August 1966, p.m. Eshkol's visit.</mixed-citation>
         </ref>
         <ref id="ref13">
            <label>13.</label>
            <mixed-citation publication-type="other">ISA, HZ 3401/10, Israel Embassy in Dar as-Salaam, 31 December 1963.</mixed-citation>
         </ref>
         <ref id="ref14">
            <label>14.</label>
            <mixed-citation publication-type="other">ISA, HZ 5575/19, Kollek to Meir, 8 October 1958.</mixed-citation>
         </ref>
         <ref id="ref15">
            <label>15.</label>
            <mixed-citation publication-type="other">
               <collab>MASHAV</collab>, Annual Reports,<year>1958–1971</year>.</mixed-citation>
         </ref>
         <ref id="ref16">
            <label>16.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Moshe Alpan</string-name>
               </person-group>,<article-title>“Israel Trade and Economic Relations with Africa,”</article-title>in<source>
                  <italic>Israel in the Third World</italic>
               </source>, ed.<person-group person-group-type="editor">
                  <string-name>Michael Curtis</string-name>
               </person-group>and<person-group person-group-type="editor">
                  <string-name>Susan Gitelson</string-name>
               </person-group>(,<year>1971</year>)<fpage>100</fpage>–<lpage>10</lpage>.</mixed-citation>
         </ref>
         <ref id="ref17">
            <label>17.</label>
            <mixed-citation publication-type="book">On the “President's Mission” see<person-group person-group-type="author">
                  <string-name>Colin Legum</string-name>
               </person-group>,<source>
                  <italic>Africa Contemporary Record</italic>
               </source>(,<year>1972–73</year>)<fpage>A132</fpage>;<person-group person-group-type="author">
                  <string-name>Moshe Liba</string-name>
               </person-group>,<source>
                  <italic>La Mission des Sages Africain</italic>
               </source>(,<year>1999</year>); ISA, HZ 5309/13, Shimoni to Eban.</mixed-citation>
         </ref>
         <ref id="ref18">
            <label>18.</label>
            <mixed-citation publication-type="other">OAU document, AH/RES 67 (IX) Rabat, June 1972.</mixed-citation>
         </ref>
         <ref id="ref19">
            <label>19.</label>
            <mixed-citation publication-type="book">
               <person-group person-group-type="author">
                  <string-name>Arye Oded</string-name>
               </person-group>,<source>
                  <italic>Uganda and Israel—The History of a Complex Relationship</italic>
               </source>(,<year>2002</year>)<fpage>86</fpage>–<lpage>89</lpage>[<comment>Hebrew</comment>].</mixed-citation>
         </ref>
         <ref id="ref20">
            <label>20.</label>
            <mixed-citation publication-type="other">
               <source>
                  <italic>Haʾaretz</italic>
               </source>,<day>31</day>
               <month>May</month>
               <year>1973</year>.</mixed-citation>
         </ref>
         <ref id="ref21">
            <label>21.</label>
            <mixed-citation publication-type="other">
               <source>
                  <italic>U.N. Chronicle</italic>
               </source>,<month>December</month>
               <year>1975</year>,<fpage>38</fpage>,<fpage>39</fpage>.</mixed-citation>
         </ref>
         <ref id="ref22">
            <label>22.</label>
            <mixed-citation publication-type="other">
               <source>
                  <italic>LʾAgence Zaire—Presse</italic>
               </source>,<day>21</day>
               <month>May</month>
               <year>1982</year>.</mixed-citation>
         </ref>
         <ref id="ref23">
            <label>23.</label>
            <mixed-citation publication-type="other">Interviews with President Fredrick Chiluba of Zambia, 30 December 1991, Lusaka; and Sally Kosgei, permanent secretary in the President's Office, Nairobi, October 1993.</mixed-citation>
         </ref>
         <ref id="ref24">
            <label>24.</label>
            <mixed-citation publication-type="other">
               <collab>MASHAV</collab>Annual Report<year>2008</year>.</mixed-citation>
         </ref>
         <ref id="ref25">
            <label>25.</label>
            <mixed-citation publication-type="other">The figures were collected from the Central Bureau of Statistics and the Economics Department of the MFA. They do not include military transactions.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
