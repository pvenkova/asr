<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">plansystevol</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50009192</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Plant Systematics and Evolution</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Springer-Verlag</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">03782697</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">16156110</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23674568</article-id>
         <title-group>
            <article-title>Molecular heterogeneity of Cowpea ( Vigna unguiculata Fabaceae) seed storage proteins</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Martin</given-names>
                  <surname>Fotso</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jean-Louis</given-names>
                  <surname>Azanza</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Remy</given-names>
                  <surname>Pasquet</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jacques</given-names>
                  <surname>Raymond</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>1994</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">191</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1/2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23674461</issue-id>
         <fpage>39</fpage>
         <lpage>56</lpage>
         <permissions>
            <copyright-statement>© Springer-Verlag 1994</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23674568"/>
         <abstract>
            <p>81 wild forms and 110 cultivated cowpea, Vigna unguiculata, accessions from 21 countries of Africa were screened for variability in seed storage proteins. Total seed proteins, albumin and globulin fractions were investigated by means of sodium dodecyl sulfate polyacrylamide gel electrophoresis (SDS-PAGE) and isoelectric focusing (IEF) of nonreduced and/or reduced samples in one- and two-dimensional procedures. The globulin fraction is heterogeneous in molecular weight and contains both legumin-like components and three to six nondisulfide-linked subunits. Three globulin subunits, with molecular weights 110, 76, and 41 kD were found to be composed of disulfide-linked polypeptides. In the nondisulfide-linked fraction, both cultivated and wild forms exhibited patterns of four types (A—D). This fraction contains polypeptide subunits of molecular weights 62, 56, and 52 kD for A type, 62, 56, 54, and 52 kD for B type, 62, 56, 52, and 50 kD for C type, and at least 62, 56, 54, 52, 50, and 49 kD for D type. These subunits present similar multiple charge forms but C and D types possess more basic specific 50 and 49 kD nondisulfide linked components. Major albumin fraction contains subunits of 94, 86, 32, and 24 kD. No infraspecific variation was observed in albumin or legumin-like fractions. The discussion is focussed on the relations between genetic variability assessed by storage protein coding genes and phenotypic variability.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d15e226a1310">
            <mixed-citation id="d15e230" publication-type="other">
Bhatty, R. S., 1982: Albumin proteins of eight edible grain legume species: electrophoretic
patterns and amino acid composition. - J. Agrie. Food Chem. 30: 620-622.</mixed-citation>
         </ref>
         <ref id="d15e240a1310">
            <mixed-citation id="d15e244" publication-type="other">
Blagrove, R. J., Gillepsie, J. M., 1978: Variability of the seed globulins of winged bean
Psophocarpus tetragonolobus (L.) DC. - Austral. J. PI. Physiol. 5: 371-375.</mixed-citation>
         </ref>
         <ref id="d15e254a1310">
            <mixed-citation id="d15e258" publication-type="other">
Bressani, R., 1985: Nutritive value of cowpea. - In Singh, S. R., Rachie, K. O., (Eds):
Cowpea research, production, and utilization, pp. 353-359. - Chichester: Wiley.</mixed-citation>
         </ref>
         <ref id="d15e268a1310">
            <mixed-citation id="d15e272" publication-type="other">
Brown, J. W. S., Ma, Y., Bliss, F. A., Hall, T. C., 1981: Genetic variation in the subunit
of globulin-1 storage protein of french bean. - Theor. Appl. Genet. 59: 83-88.</mixed-citation>
         </ref>
         <ref id="d15e283a1310">
            <mixed-citation id="d15e287" publication-type="other">
Carasco, J. F., Croy, R., Derbyshire, E., Boulter, D., 1978: The isolation and char-
acterization of the major polypeptides of the seed globulin of Cowpea [ Vigna unguiculata
(L.) Walp.] and their sequential synthesis in developing seeds. - J. Exper. Bot. 29:
309-323.</mixed-citation>
         </ref>
         <ref id="d15e303a1310">
            <mixed-citation id="d15e307" publication-type="other">
Chevalier, A., 1944: La dolique de Chine en Afrique. — Rev. Bot. Appl. Agrie. Trop.
24: 128-152.</mixed-citation>
         </ref>
         <ref id="d15e317a1310">
            <mixed-citation id="d15e321" publication-type="other">
Dalgalarrondo, M., Raymond, J., Azanza, J. L., 1984: Sunflower seed proteins: char-
acterization and subunit composition of the globulin fraction. - J. Exper. Bot. 35:
1618-1628.</mixed-citation>
         </ref>
         <ref id="d15e334a1310">
            <mixed-citation id="d15e338" publication-type="other">
- - - 1985: Sunflower seed protein: size and charge heterogeneity in subunits of the
globulin fraction. — Biochimie 67: 629-632.</mixed-citation>
         </ref>
         <ref id="d15e348a1310">
            <mixed-citation id="d15e352" publication-type="other">
Davies, C. S., Coates, J. B., Nielsen, N. C, 1985: Inheritance and biochemical analysis
of four electrophoretic variants of beta-conglycinin from soybean. - Theor. Appl.
Genet. 71: 351-358.</mixed-citation>
         </ref>
         <ref id="d15e365a1310">
            <mixed-citation id="d15e369" publication-type="other">
Debouck, D. G., Maquet, A., Posso, C. E., 1989: Biochemical evidence for two different
gene pool in lima beans, Phaseolus lunatus L. - Annu. Rept. Bean Improvement Coop.
32: 58-59.</mixed-citation>
         </ref>
         <ref id="d15e383a1310">
            <mixed-citation id="d15e387" publication-type="other">
Derbyshire, E., Wright, D. J., Boulter, D., 1976: Legumin and vicilin, storage proteins
of legume seeds. - Phytochemistry 15: 3-24.</mixed-citation>
         </ref>
         <ref id="d15e397a1310">
            <mixed-citation id="d15e401" publication-type="other">
Derchef-Hamey, S., Mimouni, B., Raymond, J., Azanza, J. L., 1990: Partial charac-
terization of polypeptide components of sunflower (Helianthus annuus L.) seed albumin
fraction. - Die Nahrung 34: 387-398.</mixed-citation>
         </ref>
         <ref id="d15e414a1310">
            <mixed-citation id="d15e418" publication-type="other">
Evans, I. M., Boulter, D., 1974: Chemical methods suitable for screening for protein
content and quality in cowpea (Vigna unguiculata) meals. - J. Sci. Food Agrie. 30:
948-958.</mixed-citation>
         </ref>
         <ref id="d15e431a1310">
            <mixed-citation id="d15e435" publication-type="other">
Gepts, P., Bliss, F. A., 1985: F 1 hybrids weakness in the common bean. - J. Fleredity
76; 447-450.</mixed-citation>
         </ref>
         <ref id="d15e445a1310">
            <mixed-citation id="d15e449" publication-type="other">
- - 1986: Phaseolin variability among wild and cultivated common beans (Phaseolus
vulgaris) from Columbia. - Econ. Bot. 40: 469-478.</mixed-citation>
         </ref>
         <ref id="d15e459a1310">
            <mixed-citation id="d15e463" publication-type="other">
- Osborn, T. C., Rashka, K., Bliss, F. A., 1986: Phaseolin-protein variability in wild
forms and landraces of the common bean (Phaseolus vulgaris): evidence for multiple
center of domestication. - Econ. Bot. 40: 451-468.</mixed-citation>
         </ref>
         <ref id="d15e477a1310">
            <mixed-citation id="d15e481" publication-type="other">
- Kmiecik, K., Pereira, P., Bliss, F. A., 1988: Dissemination pathways of common
bean (Phaseolus vulgaris, Fabaceae) deduced from Phaseolin electrophoretic variability.
The Americas. - Econ. Bot. 42: 73-85.</mixed-citation>
         </ref>
         <ref id="d15e494a1310">
            <mixed-citation id="d15e498" publication-type="other">
Hernandez, I. M., Truong, V. D., Mendoza, E. M. T., 1983: Characterization of seed
proteins of Vigna unguiculata (L.) Walp. Kalikasan, Philip. - J. Biol. 12: 115-126.</mixed-citation>
         </ref>
         <ref id="d15e508a1310">
            <mixed-citation id="d15e512" publication-type="other">
Hymowitz, T., Kaisuma, N., 1979: Dissemination of soybeans (Glycine max.): seed protein
electrophoresis profiles among japanese cultivars. - Econ. Bot. 33: 311-319.</mixed-citation>
         </ref>
         <ref id="d15e522a1310">
            <mixed-citation id="d15e526" publication-type="other">
- - 1981: Soybean seed protein electrophoresis profiles from 15 Asian countries or
regions: hypothesis on paths of dissemination of soybeans from China. - Econ. Bot.
35: 10-23.</mixed-citation>
         </ref>
         <ref id="d15e539a1310">
            <mixed-citation id="d15e543" publication-type="other">
Khan, M. R. I., Gatehouse, J. A., Boulter, D., 1980: The seed proteins of cowpea
[Vigna unguiculata (L.) Walp.]. - J. Exper. Bot. 31: 1599-1611.</mixed-citation>
         </ref>
         <ref id="d15e553a1310">
            <mixed-citation id="d15e557" publication-type="other">
Kitamura, K., Davies, C. S., Nielsen, N. C., 1984: Inheritance of alleles for CGY 1 and
Gy4 storage protein genes in soybean. - Theor. Appl. Genet. 68: 253-257.</mixed-citation>
         </ref>
         <ref id="d15e568a1310">
            <mixed-citation id="d15e572" publication-type="other">
Kortt, A., 1983: Comparative studies on the storage proteins and anti-nutritional factors
from seeds of Psophocarpus tetragonolobus (L.) DC. from five south-east asian countries.
- Quai. PI. Foods Human Nutr. 33: 29-40.</mixed-citation>
         </ref>
         <ref id="d15e585a1310">
            <mixed-citation id="d15e589" publication-type="other">
Krishna, T. G., Mitra, R., 1988: The probable genome donors to Arachis hypogea L.
based on arachin seed storage protein. — Euphytica 37: 47-52.</mixed-citation>
         </ref>
         <ref id="d15e599a1310">
            <mixed-citation id="d15e603" publication-type="other">
Krochko, J. E., Bowley, S. R., Bewley, J. D., 1990: A comparison of seed storage
proteins in subspecies and cultivars of Medicago sativa. — Cañad. J. Bot. 68: 940-948.</mixed-citation>
         </ref>
         <ref id="d15e613a1310">
            <mixed-citation id="d15e617" publication-type="other">
Ladizinsky, G., Hymowitz, T., 1979: Seed protein electrophoresis in taxonomic and
evolutionary studies. — Theor. Appl. Genet. 54: 145-151.</mixed-citation>
         </ref>
         <ref id="d15e627a1310">
            <mixed-citation id="d15e631" publication-type="other">
- Hamel, A., 1980: Seed protein profiles of pigeon pea (Cajanus cajan) and some Atylosia
species. - Euphytica 29: 313-317.</mixed-citation>
         </ref>
         <ref id="d15e641a1310">
            <mixed-citation id="d15e645" publication-type="other">
Laemmli, U. K., 1970: Cleavage of structural proteins during the assembly of the head of
bacteriophage T4. — Nature 227: 681-685.</mixed-citation>
         </ref>
         <ref id="d15e656a1310">
            <mixed-citation id="d15e660" publication-type="other">
Loveless, M. D., Hamrick, J. L., 1984: Ecological determinants of genetic structures in
plant populations. — Annu. Rev. Ecol. Syst. 15: 65-95.</mixed-citation>
         </ref>
         <ref id="d15e670a1310">
            <mixed-citation id="d15e674" publication-type="other">
Maquet, A., Gutierrez, A., Debouck, D. G., 1990: Further evidence for the existence
of two gene pools in lima beans. - Annu. Rept. Bean Improvement Coop. 33: 128—
129.</mixed-citation>
         </ref>
         <ref id="d15e687a1310">
            <mixed-citation id="d15e691" publication-type="other">
Mohamed, M. A., Lerro, K. A., Prestwich, G. D., 1989: Polyacrylamide gel miniatur-
ization improves protein visualization and autoradiographic detection. - Anal.
Biochem. 177: 187-290.</mixed-citation>
         </ref>
         <ref id="d15e704a1310">
            <mixed-citation id="d15e708" publication-type="other">
Murray, D. R., Mackenzie, K. F., Vairinhos, F., Peoples, M. B., Atkins, C. A., 1983:
Electrophoresis studies of the seed proteins of cowpea [ Vigna unguiculata (L.) Walp.]
- Z. Pflanzenphysiol. 109: 363-370.</mixed-citation>
         </ref>
         <ref id="d15e721a1310">
            <mixed-citation id="d15e725" publication-type="other">
Ologhoho, A. D., Fetuga, B. L., 1982: Chemical composition of promising cowpea ( Vigna
unguiculata) varieties. - Nutr. Rep. Intern. 25: 913-916.</mixed-citation>
         </ref>
         <ref id="d15e735a1310">
            <mixed-citation id="d15e739" publication-type="other">
Paino D'Urzo, M., Pedalino, M., Grillo, S., Rao, R., Tucci, M., 1990: Variability in
major seed proteins in different Vigna species. - In Ng, N. Q., Monti, L. M., (Eds):
Cowpea genetic resources, pp. 90-100. - Ibadan: UTA.</mixed-citation>
         </ref>
         <ref id="d15e753a1310">
            <mixed-citation id="d15e757" publication-type="other">
Pasqualini, S., Lluch, C., Antonielli, M., 1991: Seed storage proteins in several genetic
lines of Vicia faba. - PI. Physiol. Biochem. 29: 507-515.</mixed-citation>
         </ref>
         <ref id="d15e767a1310">
            <mixed-citation id="d15e771" publication-type="other">
Pasquet, R. S., 1992: Classification infraspécifique des formes spontanées de Vigna un-
guiculata (L.) Walp. (Fabaceae) à partir de données morphologiques. - Bull. Jard.
Bot. Nat. Belg. 62: 127-173.</mixed-citation>
         </ref>
         <ref id="d15e784a1310">
            <mixed-citation id="d15e788" publication-type="other">
- 1993: Variation at isoenzyme loci in wild Vigna unguiculata (L.) Walp. (Fabaceae,
Phaseoleae). - Pl. Syst. Evol. 186: 157-173.</mixed-citation>
         </ref>
         <ref id="d15e798a1310">
            <mixed-citation id="d15e802" publication-type="other">
Pedalino, M., Paino D'Urzo, M., Costa, A., Grillo, S., Rao, R., 1990: Biochemical
characterization of cowpea seed proteins. - In Ng, N. Q., Monti, L. M., (Eds): Cowpea
genetic resources, pp. 81-89. - Ibadan: UTA.</mixed-citation>
         </ref>
         <ref id="d15e815a1310">
            <mixed-citation id="d15e819" publication-type="other">
Raymond, J., Inquello, V., Azanza, J. L., 1991: The seed proteins of sunflower: com-
parative studies of cultivars. - Phytochemistry 30: 2849-2856.</mixed-citation>
         </ref>
         <ref id="d15e829a1310">
            <mixed-citation id="d15e833" publication-type="other">
Romero-Andreas, J., Bliss, F. A., 1985: Heritable variation in the phaseolin protein of
non-domesticated common bean, Phaseolus vulgaris. - Theor. Appl. Genet. 71: 478-
480.</mixed-citation>
         </ref>
         <ref id="d15e847a1310">
            <mixed-citation id="d15e851" publication-type="other">
Schinkel, C., Gepts, P., 1988: Phaseolin diversity in the tepary bean, Phaseolus acutifolius
A. Gray. - PI. Breed. 101: 292-301.</mixed-citation>
         </ref>
         <ref id="d15e861a1310">
            <mixed-citation id="d15e865" publication-type="other">
Schmit, V., Debouck, D. G., 1991: Observations on the origin of Phaseolus polyanthus
Greenman. - Econ. Bot. 45: 345-364.</mixed-citation>
         </ref>
         <ref id="d15e875a1310">
            <mixed-citation id="d15e879" publication-type="other">
Sefah-Debeh, S., Stanley, D., 1979: Cowpea proteins. Characterization of water ex-
tractable proteins. - J. Agr. Food Chem. 27: 1244-1247.</mixed-citation>
         </ref>
         <ref id="d15e889a1310">
            <mixed-citation id="d15e893" publication-type="other">
Tomooka, N., Lairungreang, C., Nakeeraks, P., Egawa, Y., Thavarasook, C., 1992:
Center of genetic diversity and dissemination pathways in mung bean deduced from
seed protein electrophoresis. - Theor. Appl. Genet. 83: 289-293.</mixed-citation>
         </ref>
         <ref id="d15e906a1310">
            <mixed-citation id="d15e910" publication-type="other">
Towbin, H., Staehelin, T., Gordon, J., 1979: Electrophoretic transfer of proteins from
polyacrylamide gels to nitrocellulose sheets: procedure and some applications. - Proc.
Natl. Acad. Sci. USA 76: 4350-4354.</mixed-citation>
         </ref>
         <ref id="d15e923a1310">
            <mixed-citation id="d15e927" publication-type="other">
Tsukada, Y., Kitamura, K., Harada, K., Kaizuma, N., 1986: Genetic analysis of subunits
of two major storage proteins (beta-conglycinin) of soybean seeds. - Japan J. PI. Breed.
36: 390-400.</mixed-citation>
         </ref>
         <ref id="d15e941a1310">
            <mixed-citation id="d15e945" publication-type="other">
Tucci, M., Capparelli, R., Costa, A., Rao, R., 1991: Molecular heterogeneity and genetics
of Vicia faba seed storage proteins. - Theor. Appl. Genet. 81: 50-58.</mixed-citation>
         </ref>
         <ref id="d15e955a1310">
            <mixed-citation id="d15e959" publication-type="other">
Vidovic, M., Murray, D. R., 1984: Mobilization of nitrogen during imbibition and
germination of seeds of Phaseolus vulgaris and Vigna unguiculata. - Z. Pñanzenphysiol.
113: 117-128.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
