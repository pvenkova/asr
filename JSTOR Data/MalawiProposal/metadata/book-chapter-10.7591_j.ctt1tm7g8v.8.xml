<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">j.ctt7mkgg</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1tm7g8v</book-id>
      <subj-group>
         <subject content-type="call-number">HV640L48 2005</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Refugees</subject>
         <subj-group>
            <subject content-type="lcsh">Services for</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Humanitarian assistance</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Dangerous Sanctuaries</book-title>
         <subtitle>Refugee Camps, Civil War, and the Dilemmas of Humanitarian Aid</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Lischer</surname>
               <given-names>Sarah Kanyon</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>01</day>
         <month>05</month>
         <year>2015</year>
      </pub-date>
      <isbn content-type="epub">9781501700408</isbn>
      <isbn content-type="epub">1501700405</isbn>
      <publisher>
         <publisher-name>Cornell University Press</publisher-name>
         <publisher-loc>Ithaca; London</publisher-loc>
      </publisher>
      <edition>1</edition>
      <permissions>
         <copyright-year>2005</copyright-year>
         <copyright-holder>Cornell University</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7591/j.ctt1tm7g8v"/>
      <abstract abstract-type="short">
         <p>Since the early 1990s, refugee crises in the Balkans, Central Africa, the Middle East, and West Africa have led to the international spread of civil war. In Central Africa alone, more than three million people have died in wars fueled, at least in part, by internationally supported refugee populations. The recurring pattern of violent refugee crises prompts the following questions: Under what conditions do refugee crises lead to the spread of civil war across borders? How can refugee relief organizations respond when militants use humanitarian assistance as a tool of war? What government actions can prevent or reduce conflict?</p>
         <p>To understand the role of refugees in the spread of conflict, Sarah Kenyon Lischer systematically compares violent and nonviolent crises involving Afghan, Bosnian, and Rwandan refugees. Lischer argues against the conventional socioeconomic explanations for refugee-related violence-abysmal living conditions, proximity to the homeland, and the presence of large numbers of bored young men. Lischer instead focuses on the often-ignored political context of the refugee crisis. She suggests that three factors are crucial: the level of the refugees' political cohesion before exile, the ability and willingness of the host state to prevent military activity, and the contribution, by aid agencies and outside parties, of resources that exacerbate conflict.</p>
         <p>Lischer's political explanation leads to policy prescriptions that are sure to be controversial: using private security forces in refugee camps or closing certain camps altogether. With no end in sight to the brutal wars that create refugee crises,<italic>Dangerous Sanctuaries</italic>is vital reading for anyone concerned with how refugee flows affect the dynamics of conflicts around the world.</p>
      </abstract>
      <counts>
         <page-count count="222"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.3</book-part-id>
                  <title-group>
                     <title>List of Tables and Maps</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Lischer</surname>
                           <given-names>Sarah Kanyon</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.5</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Refugee Crises as Catalysts of Conflict</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>After organizing the mass killing of hundreds of thousands of Rwandan Tutsi in 1994, the Rwandan Hutu leadership forced over a million Hutu civilians into eastern Congo (then Zaire). During the refugee crisis from 1994 to 1996, perpetrators of the Rwandan genocide established military training bases adjacent to the refugee camps. The militants stockpiled weapons, recruited and trained refugee fighters, and launched crossborder attacks against Rwanda. The militant leaders openly gloated about their manipulation of the Hutu refugees and their plan to complete the genocide of the Tutsi. From the camps, the genocidal leader Jean Bosco Barayagwiza boasted that “even</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.6</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Political Incentives for the Spread Of Civil War</title>
                  </title-group>
                  <fpage>18</fpage>
                  <abstract>
                     <p>Although scholars and policymakers have expressed increasing concern about refugee militarization, there is no existing systematic explanation for the phenomenon of refugee-related violence. The ad hoc explanations offered by the humanitarian assistance literature and the policy community generally focus on socioeconomic factors as both cause and cure. These socioeconomic propositions do not explicitly address the spread of civil war, but rather lump all types of refugeerelated violence together. According to these explanations, proximity to the border, large camp populations, the presence of bored young men, and poor living conditions cause cross-border violence. Reliance on these explanations leads to policy prescriptions</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.7</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Afghan Refugees:</title>
                     <subtitle>Three Decades of Political Violence</subtitle>
                  </title-group>
                  <fpage>44</fpage>
                  <abstract>
                     <p>After the Soviet invasion of Afghanistan in 1979, nearly six million Afghans fled their homes—around three million went east to Pakistan and two million moved west into Iran. In Pakistan, Afghan resistance parties created a powerful state in exile that waged a relentless guerrilla war against the Soviet-backed regime in Kabul. In contrast, the potentially militant refugees in Iran lacked the capability to mobilize against the Afghan government. The two receiving states had fundamentally different foreign policy goals, domestic politics, and attitudes toward the refugees. The refugee crises occurred in the political context of the Cold War, with Afghanistan</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.8</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>From Refugees to Regional war in Central Africa</title>
                  </title-group>
                  <fpage>73</fpage>
                  <abstract>
                     <p>Since the early 1990 s, internal conflicts have caused millions of deaths in the Democratic Republic of Congo (formerly Zaire), Rwanda, Burundi, and Uganda. In Congo alone, over three million people died from the effects of war between 1998 and 2003.¹ These many internal conflicts did not remain isolated from each other, but rather spread across national borders, escalating the costs of war. The potent mix of millions of refugees, thousands of rebels, and abundant humanitarian assistance acted as a catalyst for the spread of conflict in the Great Lakes region of Africa.² A locus of this regional destabilization was</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.9</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Demilitarizing a Refugee Army:</title>
                     <subtitle>Bosnian Muslim Renegade Refugees</subtitle>
                  </title-group>
                  <fpage>118</fpage>
                  <abstract>
                     <p>While ensconced in fetid chicken coops and burned-out buildings, a group of Bosnian Muslim refugees drilled for war in fall 1994 . Their target was to recapture their hometown—not from the Serbs but from the Bosnian Muslim government. In fact, the breakaway Muslim faction relied on Serb artillery to defeat the Bosnian government forces. The militant refugees also relied on food, shelter, and medical care provided by international humanitarian organizations.</p>
                     <p>The story of that ultimately unsuccessful Muslim rebellion against the Sarajevo government is often considered a footnote in the overall history of the war in the former Yugoslavia. In</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.10</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Collateral Damage:</title>
                     <subtitle>The Risks of Humanitarian Responses to Militarized Refugee Crises</subtitle>
                  </title-group>
                  <fpage>141</fpage>
                  <abstract>
                     <p>The unintended negative consequences of humanitarian aid indicate an urgent need to alter traditional responses to militarized or potentially militarized refugee crises. Most reactions ignore the militarization. Governments avoid political and military action by treating the crisis as purely humanitarian, especially if the situation does not present a global security threat. For example, western governments did not regard war in eastern Zaire as a threat to international security and thus refused to disarm the genocidal killers among the refugees. Fiona Terry argues that “the international response to the Rwandan crisis . . . suggests that humanitarian action in the post</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.11</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>167</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.12</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>197</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1tm7g8v.13</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>205</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
