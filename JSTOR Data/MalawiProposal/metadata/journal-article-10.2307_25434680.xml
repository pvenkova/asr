<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">popudeverevi</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100511</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Population and Development Review</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Blackwell Publishing</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00987921</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">17284457</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">25434680</article-id>
         <title-group>
            <article-title>Has the HIV Epidemic Peaked?</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>John</given-names>
                  <surname>Bongaarts</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Thomas</given-names>
                  <surname>Buettner</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gerhard</given-names>
                  <surname>Heilig</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>François</given-names>
                  <surname>Pelletier</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>6</month>
            <year>2008</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">34</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">2</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i25434678</issue-id>
         <fpage>199</fpage>
         <lpage>224</lpage>
         <permissions>
            <copyright-statement>Copyright 2008 The Population Council, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/25434680"/>
         <abstract>
            <p>This study reviews the highly diverse regional and country patterns of HIV epidemics and discusses possible causes of the geographic variation in epidemic sizes. Past trends and projections of the epidemics are presented and the peak years of epidemics are estimated. The potential future impact of new prevention technologies is briefly assessed. A final section summarizes the future impact of the epidemic on key demographic variables. The main finding of this analysis is that the HIV epidemic reached a major turning point over the past decade. The peak years of HIV incidence rates are past for all regions, and the peaks of prevalence rates are mostly in the past except in Eastern Europe, where they are expected to peak in 2008. But owing in part to the life-prolonging effect of antiretroviral therapy and to sustained population growth, the absolute number of infected individuals is expected to keep growing slowly in sub-Saharan Africa and to remain near current levels worldwide, thus posing a continuing challenge to public health programs. No country is expected to see a decline in its population size between 2005 and 2050 that is attributable to high mortality related to AIDS.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Notes</title>
         <ref id="d1457e278a1310">
            <label>1</label>
            <mixed-citation id="d1457e285" publication-type="other">
Halperin and Epstein
2007</mixed-citation>
            <mixed-citation id="d1457e294" publication-type="other">
Shiboshki and Padian 1998</mixed-citation>
            <mixed-citation id="d1457e300" publication-type="other">
Wawer et
al. 2005</mixed-citation>
         </ref>
         <ref id="d1457e310a1310">
            <label>2</label>
            <mixed-citation id="d1457e317" publication-type="other">
endnote 1</mixed-citation>
         </ref>
         <ref id="d1457e324a1310">
            <label>3</label>
            <mixed-citation id="d1457e331" publication-type="other">
Bongaarts 2007</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>References</title>
         <ref id="d1457e347a1310">
            <mixed-citation id="d1457e351" publication-type="other">
Anderson, R. M. 1996. "The spread of HTV and sexual mixing patterns," in J. Mann and D.
Tarantola (eds.), AIDS in the World II. New York: Oxford University Press, pp. 71-86.</mixed-citation>
         </ref>
         <ref id="d1457e361a1310">
            <mixed-citation id="d1457e365" publication-type="other">
-. 1999. "Transmission dynamics of sexually transmitted infections," in K. K. Holmes,
P. F. Sparling, P.-A. Mardh et al. (eds.), Sexually Transmitted Diseases, 3rd ed. New York:
McGraw-Hill, pp. 25-37.</mixed-citation>
         </ref>
         <ref id="d1457e378a1310">
            <mixed-citation id="d1457e382" publication-type="other">
Auvert, B., A. Buvé et al. 2001. "Ecological and individual level analysis of risk factors for HIV
infection in four urban populations in sub-Saharan Africa with different levels of HIV
infection," AIDS 15 (Suppl 4): S127-S131.</mixed-citation>
         </ref>
         <ref id="d1457e395a1310">
            <mixed-citation id="d1457e399" publication-type="other">
Auvert, B., D. Taljaard, E. Lagarde et al. 2005. "Randomized, controlled intervention trial of
male circumcision for reduction of HTV infection risk: The ANRS 1265 trial," PloSMedicine
2(11): e298. DOI: 10.1371/journal.pmed.0020298.</mixed-citation>
         </ref>
         <ref id="d1457e413a1310">
            <mixed-citation id="d1457e417" publication-type="other">
Bailey, R. C, S. Moses, C B. Parker et al. 2007. "Male circumcision in young men in Kisumu,
Kenya: A randomized controlled trial," Lancet 369: 343-356.</mixed-citation>
         </ref>
         <ref id="d1457e427a1310">
            <mixed-citation id="d1457e431" publication-type="other">
Bailey, R. C, F. A. Plummer, and S. Moses. 2001. "Male circumcision and HIV prevention: Cur-
rent knowledge and future research directions," Lancet Infectious Diseases 1: 223-231.</mixed-citation>
         </ref>
         <ref id="d1457e441a1310">
            <mixed-citation id="d1457e445" publication-type="other">
Bessinger, Ruth, Priscilla Akwara, and Daniel Halperin. 2003. "Sexual behavior, HIV, and fertil-
ity trends: A comparative analysis of six countries," Phase 1 of the ABC Study. US AID and
MEASURE Evaluation, a project managed by the University of North Carolina at Chapel
Hill in collaboration with ORC Macro, Tulane University, and John Snow Inc.</mixed-citation>
         </ref>
         <ref id="d1457e461a1310">
            <mixed-citation id="d1457e465" publication-type="other">
Bongaarts, John. 2007. "Late marriage and the HIV epidemic in sub-Saharan Africa," Popula-
tion Studies 61(1): 73-83.</mixed-citation>
         </ref>
         <ref id="d1457e475a1310">
            <mixed-citation id="d1457e479" publication-type="other">
Bongaarts, John, Prescilla Reining, Peter Way, and Francis Conant. 1989. "The relation-
ship between male circumcision and HIV infection in African populations," AIDS 3(6):
373-377.</mixed-citation>
         </ref>
         <ref id="d1457e492a1310">
            <mixed-citation id="d1457e496" publication-type="other">
Boerma, J. T., K. A. Stanecki, M. L. Newell et al. 2006. "Monitoring the scale-up of antiretro-
viral therapy programmes: Methods to estimate coverage," Bulletin of the World Health
Organization 84(2): 145-150 «http://www.who.int/bulletin/volumes/84/2/145.pdf».</mixed-citation>
         </ref>
         <ref id="d1457e510a1310">
            <mixed-citation id="d1457e514" publication-type="other">
Brown, Joelle M., Anna Wald, Alan Hubbard et al. 2007. "Incident and prevalent herpes sim-
plex virus type 2 infection increases risk of HIV acquisition among women in Uganda
and Zimbabwe," AIDS 21 (12): 1515-1524.</mixed-citation>
         </ref>
         <ref id="d1457e527a1310">
            <mixed-citation id="d1457e531" publication-type="other">
Caldwell, John C. 2000. "Rethinking the African AIDS epidemic," Population and Development
Review 26(1): 117-135.</mixed-citation>
         </ref>
         <ref id="d1457e541a1310">
            <mixed-citation id="d1457e545" publication-type="other">
Cameron, D. W. et al. 1989. "Female to male transmission of human immunodeficiency virus
type 1: Risk factors for seroconversion in men," Lancet 2: 403-407.</mixed-citation>
         </ref>
         <ref id="d1457e555a1310">
            <mixed-citation id="d1457e559" publication-type="other">
Chin, James. 2007. The AIDS Pandemic: The Collision of Epidemiology with Political Correctness.
Oxford: Radcliffe Publishing.</mixed-citation>
         </ref>
         <ref id="d1457e569a1310">
            <mixed-citation id="d1457e573" publication-type="other">
Cleland, J., J. T. Boerma, M. Caraël, and S. S. Weir. 2004. "Monitoring sexual behavior in
general populations: A synthesis of lessons from the past decade," Sexually Transmitted
Infections 80 (Suppl: Measurement of sexual behavior) II: ii1-ii7.</mixed-citation>
         </ref>
         <ref id="d1457e586a1310">
            <mixed-citation id="d1457e590" publication-type="other">
DHS. 2007. Tabulation created with STATCOMPILER, using data from Cameroon, Ethiopia,
Ghana, Kenya, Malawi, Mozambique, Nigeria, Tanzania, Zambia, Zimbabwe, «http://
www.measuredhs.com/hivdata/data/table_builder.cfm?survey_type_id=&amp;amp;survey_pop_
based=&amp;amp;userid=48147&amp;amp;usertabid=54161».</mixed-citation>
         </ref>
         <ref id="d1457e607a1310">
            <mixed-citation id="d1457e611" publication-type="other">
Dorrington, R. E., L. F. Johnson, D. Bradshaw, and T. Daniel. 2006. The Demographic Impact of
HIV/AIDS in South Africa: National and Provincial Indicators for 2006. Cape Town: Centre for
Actuarial Research, South African Medical Research Council and Actuarial Society of
South Africa «http://www.commerce.uct.ac.za/Research_Units/ CARE/RE SEARCH/PA-
PERS/ASSA2003Indicators.pdf».</mixed-citation>
         </ref>
         <ref id="d1457e630a1310">
            <mixed-citation id="d1457e634" publication-type="other">
Epstein, Helen. 2007. The Invisible Cure: Africa, the West, and the Fight Against AIDS. New York:
Farrar, Straus and Giroux.</mixed-citation>
         </ref>
         <ref id="d1457e644a1310">
            <mixed-citation id="d1457e650" publication-type="other">
Fleming, D. T. and J. N. Wasserheit. 1999. "From epidemiological synergy to public health policy
and practice: The contribution to public health policy and practice: The contribution of
other sexually transmitted diseases to sexual transmission of HIV infection/ Sexually
Transmitted Infections 75: 3-17.</mixed-citation>
         </ref>
         <ref id="d1457e666a1310">
            <mixed-citation id="d1457e670" publication-type="other">
Gray, R. H., G. Kigozi, D. Serwadda et al. 2007. "Male circumcision for HIV prevention in men
in Rakai, Uganda: A randomised trial," Lancet 369: 657-666.</mixed-citation>
         </ref>
         <ref id="d1457e680a1310">
            <mixed-citation id="d1457e684" publication-type="other">
Gray, R., M. J. Wawer, R. Brookmeyer et al. 2001. "Probability of HIV-1 transmission per coital
act in monogamous, heterosexual, HIV-1 discordant couples in Rakai, Uganda," Lancet
357(9263): 1149-1153.</mixed-citation>
         </ref>
         <ref id="d1457e697a1310">
            <mixed-citation id="d1457e701" publication-type="other">
Gregson, Simon, Geoffrey Garnett, Constance A. Nyamukapa et al. 2006. "HIV decline associ-
ated with behavior change in Eastern Zimbabwe," Science 311: 664-666.</mixed-citation>
         </ref>
         <ref id="d1457e712a1310">
            <mixed-citation id="d1457e716" publication-type="other">
Halperin, D. and R. C. Bailey. 1999. "Male circumcision and HIV infection: Ten years and count-
ing," Lancet 354(9192): 1813-1815.</mixed-citation>
         </ref>
         <ref id="d1457e726a1310">
            <mixed-citation id="d1457e730" publication-type="other">
Halperin, D. and H. Epstein. 2004. "Concurrent sexual partnerships help to explain Africa's high
HIV prevalence: Implications for intervention," Lancet 364: 4-6.</mixed-citation>
         </ref>
         <ref id="d1457e740a1310">
            <mixed-citation id="d1457e744" publication-type="other">
-. 2007. "Why is HIV prevalence so severe in southern Africa?," The Southern African
Journal of HIV Medicine (March): 19-24.</mixed-citation>
         </ref>
         <ref id="d1457e754a1310">
            <mixed-citation id="d1457e758" publication-type="other">
Hearst, Norman and Sanny Chen. 2004. "Condom promotion for AIDS prevention in the de-
veloping world: Is it working?," Studies in Family Planning 35(1): 39-47.</mixed-citation>
         </ref>
         <ref id="d1457e768a1310">
            <mixed-citation id="d1457e772" publication-type="other">
Johnson, M. I. and A. S. Fauci. 2007. "An HIV vaccine—evolving concepts," New England Journal
of Medicine 356: 2073-2081.</mixed-citation>
         </ref>
         <ref id="d1457e782a1310">
            <mixed-citation id="d1457e786" publication-type="other">
Kilian, Albert H. D., Simon Gregson, Bannet Ndyanabangi et al. 1999. "Reductions in risk
behaviour provide the most consistent explanation for declining HIV-1 prevalence in
Uganda," AIDS 13: 391-398.</mixed-citation>
         </ref>
         <ref id="d1457e800a1310">
            <mixed-citation id="d1457e804" publication-type="other">
Merson, Michael. 2006. "Uganda's HIV/AIDS epidemic: Guest editorial," AIDS Behavior 10(4):
333-334.</mixed-citation>
         </ref>
         <ref id="d1457e814a1310">
            <mixed-citation id="d1457e818" publication-type="other">
Morris, M. and M. Kretzchmar. 1997. "Concurrent partnerships and the spread of HIV," AIDS
11:681-683.</mixed-citation>
         </ref>
         <ref id="d1457e828a1310">
            <mixed-citation id="d1457e832" publication-type="other">
Moses, S., N. J. D. Bradley, J. O. Ndinya-Achla et al. 1990. "Geographical patterns of male cir-
cumcision practices in Africa: Association with HIV seroprevalence," International Journal
of Epidemiology 19: 693-697.</mixed-citation>
         </ref>
         <ref id="d1457e845a1310">
            <mixed-citation id="d1457e849" publication-type="other">
Nuttall, Jeremy, Joseph Romano, Karen Douville et al. 2007. "The future of HIV prevention:
Prospects for an effective anti-HIV microbicide," Infectious Disease Clinics of North America
21:219-239.</mixed-citation>
         </ref>
         <ref id="d1457e862a1310">
            <mixed-citation id="d1457e866" publication-type="other">
Orubuloye, I. O., John C. Caldwell, Pat Caldwell, and Gigi Santow. 1994. "Sexual networking
and AIDS in sub-Saharan Africa," in I. O. Orubuloye, John C. Caldwell, and Gigi Santow
(eds.), Sexual Networking and AIDS in Sub-Saharan Africa: Behavioural Research and the Social
Context. Health Transition Series No. 4. Canberra: Australian National University.</mixed-citation>
         </ref>
         <ref id="d1457e882a1310">
            <mixed-citation id="d1457e886" publication-type="other">
Quinn, T. C, M. Wawer, N. Sewankambo et al. 2000. "Viral load and heterosexual transmis-
sion of human immunodeficiency virus type 1," New England Journal of Medicine 342:
921-929.</mixed-citation>
         </ref>
         <ref id="d1457e900a1310">
            <mixed-citation id="d1457e904" publication-type="other">
Sawires, S. R. et al. 2007. "Male circumcision and HIV/AIDS: Challenges and opportunities,"
Lancet 369: 708-713.</mixed-citation>
         </ref>
         <ref id="d1457e914a1310">
            <mixed-citation id="d1457e918" publication-type="other">
Shapiro, R. 2002. "Drawing lines in the sand: The boundaries of the HIV pandemic in perspec-
tive," Social Science and Medicine 55: 107-110.</mixed-citation>
         </ref>
         <ref id="d1457e928a1310">
            <mixed-citation id="d1457e932" publication-type="other">
Shelton, James D., Daniel T. Halperin, and David Wilson. 2006. "Has global HIV incidence
peaked?," Lancet 367(9517): 1120-1121.</mixed-citation>
         </ref>
         <ref id="d1457e942a1310">
            <mixed-citation id="d1457e946" publication-type="other">
Shiboski, Stephen C. and Nancy S. Padian. 1998. "Epidemiological evidence for time variation
in HIV infectivity," Journal of Acquired Immune Deficiency Syndromes and Human Retrovirology
19: 527-535.</mixed-citation>
         </ref>
         <ref id="d1457e959a1310">
            <mixed-citation id="d1457e963" publication-type="other">
Singh, Susheela, Jacqueline E. Darroch, and Akinrinola Bankole. 2003. "A, B and C in Uganda:
The roles of abstinence, monogamy and condom use in HIV decline," Occasional Report No.
9. New York: The Alan Guttmacher Institute.</mixed-citation>
         </ref>
         <ref id="d1457e976a1310">
            <mixed-citation id="d1457e980" publication-type="other">
Stoneburner, Rand L. and Daniel Low-Beer. 2004. "Population-level HIV declines and behavioral
risk avoidance in Uganda," Science 304: 714-718 (30 April), «www.science mag.org».</mixed-citation>
         </ref>
         <ref id="d1457e991a1310">
            <mixed-citation id="d1457e997" publication-type="other">
Stover, J., N. Walker, N. C. Grassly, and M. Marston. 2006. "Projecting the demographic impact
of AIDS and the number of people in need of treatment: Updates to the Spectrum projec-
tion package," Sexually Transmitted Infections 82 (Supplement 3): iii45-iii50 «http://sti.bmj.
com/cgi/content/abstraa/82/suppl_3/iii45 ».</mixed-citation>
         </ref>
         <ref id="d1457e1013a1310">
            <mixed-citation id="d1457e1017" publication-type="other">
Swidler, Ann and Susan Cotts Watkins. 2007. "Ties of dependence: AIDS and transactional sex
in rural Malawi," Studies in Family Planning 38(3): 147-162.</mixed-citation>
         </ref>
         <ref id="d1457e1027a1310">
            <mixed-citation id="d1457e1031" publication-type="other">
UNAIDS Reference Group on Estimates, Modelling and Projections. 2002. "Improved methods
and assumptions for estimation of the HIV/AIDS epidemic and its impact: Recommenda-
tions of the UNAIDS Reference Group on Estimates, Modelling and Projections," AIDS 16:
W1-W14 «http://www.epidem.org».</mixed-citation>
         </ref>
         <ref id="d1457e1047a1310">
            <mixed-citation id="d1457e1051" publication-type="other">
UNAIDS. 2006. Report on the Global AIDS Epidemic. Joint United Nations Programme on HIV/
AIDS. Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d1457e1061a1310">
            <mixed-citation id="d1457e1065" publication-type="other">
-. 2007. AIDS Epidemic Update: December 2007. Joint United Nations Programme on HIV/
AIDS (UNAIDS) and World Health Organization (WHO). Geneva: UNAIDS.</mixed-citation>
         </ref>
         <ref id="d1457e1075a1310">
            <mixed-citation id="d1457e1079" publication-type="other">
UNAIDS, UNICEF, WHO. 2007. Children and AIDS—A Stocktaking Report. Actions and Progress dur-
ing the First Year of Unite for Children, Unite against AIDS (with Statistical Annexes), «www.
unicef.org/uniteforchildren».</mixed-citation>
         </ref>
         <ref id="d1457e1093a1310">
            <mixed-citation id="d1457e1097" publication-type="other">
United Nations. 2002. HIV/AIDS and Fertility in sub-Saharan Africa: A Review of the Research Litera-
ture. Population Division, Department of Economic and Social Affairs. New York: United
Nations. ESA/P/WP. 174.</mixed-citation>
         </ref>
         <ref id="d1457e1110a1310">
            <mixed-citation id="d1457e1114" publication-type="other">
-. 2005. "Population, Development and HTV IAIDS with Particular Emphasis on Poverty. Popu-
lation Division, Department of Economic and Social Affairs. New York: United Nations
(ST/ESA/SER.A/247).</mixed-citation>
         </ref>
         <ref id="d1457e1127a1310">
            <mixed-citation id="d1457e1131" publication-type="other">
-. 2006. Levels and Trends of Contraceptive Use as Assessed in 2002. Population Division, Depart-
ment of Economic and Social Affairs. New York: United Nations (ST/ESA/SER.A/239).</mixed-citation>
         </ref>
         <ref id="d1457e1141a1310">
            <mixed-citation id="d1457e1145" publication-type="other">
-. 2007. World Population Prospects: The 2006 Revision, CD-ROM Edition—Extended Dataset
in Excel and ASCII formats. Population Division, Department of Economic and Social Af-
fairs. New York: United Nations. E.07.XIII.7. (Special tabulations were prepared for this
study.)</mixed-citation>
         </ref>
         <ref id="d1457e1161a1310">
            <mixed-citation id="d1457e1165" publication-type="other">
Vachot, Laurence et al. 2006. "Sleeping with the enemy: The insidious relationship between
dendritic cells and immunodeficiency viruses," in Manfred B. Lutz, Nikolaus Romani, and
Alexander Steinkasserer (eds.), Handbook of Dendritic Cells: Biology, Diseases and Therapies.
Weinheim: Wiley-VCH, pp. 773-797.</mixed-citation>
         </ref>
         <ref id="d1457e1181a1310">
            <mixed-citation id="d1457e1185" publication-type="other">
Wawer, Maria J., Ronald H. Gray, Nelson K. Sewankambo et al. 2005. "Rates of HIV-1 transmis-
sion per coital act by stage of HIV-1 infection, in Rakai, Uganda," The Journal of Infectious
Diseases 191 (1 May): 1403-1409.</mixed-citation>
         </ref>
         <ref id="d1457e1199a1310">
            <mixed-citation id="d1457e1203" publication-type="other">
Weiss, H. A., M. A. Quigley, and R. J. Hayes. 2000. "Male circumcision and risk of HIV infection
in sub-Saharan Africa: A systematic review and meta-analysis," AIDS 14: 2361-2370.</mixed-citation>
         </ref>
         <ref id="d1457e1213a1310">
            <mixed-citation id="d1457e1217" publication-type="other">
Williams, B. G., J. O. Lloyd-Smith, E. Gouws et al. 2006. "The potential impact of male circum-
cision on HIV in sub-Saharan Africa," PloS Medicine 3(7): e262. DOI: 10.1371 /journal.
pmed.0030262</mixed-citation>
         </ref>
         <ref id="d1457e1230a1310">
            <mixed-citation id="d1457e1234" publication-type="other">
WHO. 1999. "Global prevalence and incidence of selected curable sexually transmitted infec-
tions," «www.who.int/hiv/pub/sti/who_hiv_aids_2001.02.pdf ».</mixed-citation>
         </ref>
         <ref id="d1457e1244a1310">
            <mixed-citation id="d1457e1248" publication-type="other">
WHO and UNAIDS. 2006. Progress on global access to HIV antiretroviral therapy. A report on
"3 by 5" and beyond. See Annex 1. Estimated number of people receiving antiretroviral
therapy, people needing antiretroviral therapy, percentage coverage and numbers of anti-
retroviral therapy sites in low- and middle-income countries (pp. 71-76). «http:// www.
who.int/hiv/fullreport_en_highres.pdf».</mixed-citation>
         </ref>
         <ref id="d1457e1267a1310">
            <mixed-citation id="d1457e1271" publication-type="other">
-. 2007. "New data on male circumcision and HIV prevention: Policy and programme
implications," WHO/UNAIDS Technical Consultation, Male Circumcision and HIV Pre-
vention: Research Implications for Policy and Programming, Montreux, March 2007
«http://data.unaids.org/pub/Report/2007/mc_recommendations_en.pdf».</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
