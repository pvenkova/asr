<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">regioncohesion</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50005810</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Regions &amp; Cohesion / Regiones y Cohesión / Régions et Cohésion</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>berghahn</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">2152906X</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">21529078</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">26452275</article-id>
         <title-group>
            <article-title>Linking social policy, migration, and development in a regional context: The case of sub-Saharan Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Katja</given-names>
                  <surname>Hujo</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>12</month>
            <year>2013</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i26452271</issue-id>
         <fpage>30</fpage>
         <lpage>55</lpage>
         <permissions>
            <copyright-statement>© 2013 Regions and Cohesion</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/26452275"/>
         <abstract>
            <p>International migration is driven by development processes and, at the same time, it impacts development through labor market effects, remittance flows, knowledge transfers, social change in households and communities and responses at the policy and institutional levels. Although the development potential of migration is now widely recognized, we still observe that migration, and in particular, the free movement of people and the access of migrants to sociopolitical rights, remains a highly contested and sensitive political issue. This is not only the case with regard to migration from developing countries to industrialized countries in the North, but also for migration at a regional level and within regional integration projects such as common markets or political and monetary unions. This article discusses the linkages between migration, development, social policy and regional integration. The focus is on migration in sub-Saharan Africa, its impact on development and migrants' rights and implications for public policies including new forms of migration governance. La migration internationale est pilotée par les processus de développement et, dans un même temps, impacte sur le développement à travers ses effets sur le marché du travail, les transferts de fonds des migrants, les transferts de connaissances, le changement social dans les ménages et les communautés, ainsi que les réponses qu'elle occasionne au niveau politique et institutionnel. Bien que le potentiel de développement des migrations soit désormais largement reconnu, nous observons encore que la migration, et en particulier la libre circulation des personnes et l'accès des migrants aux droits socio-politiques, reste une question politique très controversée et sensible. Cela ne concerne pas seulement le cas des flux migratoires des pays en développement vers les pays industrialisés du Nord, mais également les flux migratoires générés au niveau régional et dans les contextes d'intégration régionale tels que les marchés communs ou les unions politiques et monétaires. Cet article examine les liens entre la migration, le développement, la politique sociale et l'intégration régionale. L'accent est mis sur la migration en Afrique sub-saharienne, son impact sur le développement et les droits des migrants, ainsi que leurs impacts sur les politiques publiques, y compris les nouvelles formes de gouvernance migratoires. La migración internacional es impulsada por los procesos de desarrollo y, al mismo tiempo, tiene un impacto en el desarrollo a través de sus efectos en el mercado de trabajo, los flujos de remesas, las transferencias de conocimientos, el cambio social en los hogares y en las comunidades, así como las respuestas a nivel político e institucional. Aunque actualmente el potencial de desarrollo de la migración es ampliamente reconocido, todavía observamos que la migración y, en particular, la libre circulación de personas y el acceso de los migrantes a más derechos sociopolíticos, sigue siendo una cuestión política muy controvertida y sensible. Este no es sólo el caso con respecto a la migración de los países en desarrollo a los países industrializados del Norte, también ocurre en la migración a nivel regional y en los proyectos de integración regional tales como los mercados comunes o uniones políticas y monetarias. Este artículo analiza los vínculos entre la migración, el desarrollo, la política social y la integración regional. La atención se centra en la migración en el África Subsahariana, su impacto sobre el desarrollo y los derechos de los migrantes, así como sus implicaciones en las políticas públicas, incluyendo nuevas formas de gobernanza de la migración.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>NOTES</title>
         <ref id="d755e152a1310">
            <label>1</label>
            <mixed-citation id="d755e159" publication-type="other">
Arrighi (1973)</mixed-citation>
            <mixed-citation id="d755e165" publication-type="other">
Burawoy (1976)</mixed-citation>
            <mixed-citation id="d755e171" publication-type="other">
Crush, Jeeves, and Yudelman (1992)</mixed-citation>
            <mixed-citation id="d755e178" publication-type="other">
Cooper (1989)</mixed-citation>
            <mixed-citation id="d755e184" publication-type="other">
Kayenze (2004)</mixed-citation>
         </ref>
         <ref id="d755e191a1310">
            <label>4</label>
            <mixed-citation id="d755e200" publication-type="other">
UNRISD (2006, 2010)</mixed-citation>
            <mixed-citation id="d755e206" publication-type="other">
Mkandawire (2004)</mixed-citation>
            <mixed-citation id="d755e213" publication-type="other">
Townsend (2007)</mixed-citation>
         </ref>
         <ref id="d755e220a1310">
            <label>7</label>
            <mixed-citation id="d755e227" publication-type="other">
Mkandawire (2009)</mixed-citation>
         </ref>
         <ref id="d755e234a1310">
            <label>8</label>
            <mixed-citation id="d755e241" publication-type="other">
Morissens (2008)</mixed-citation>
            <mixed-citation id="d755e247" publication-type="other">
Morissens and Sainsbury (2005)</mixed-citation>
            <mixed-citation id="d755e253" publication-type="other">
Faist (1996)</mixed-citation>
            <mixed-citation id="d755e260" publication-type="other">
Bommes
and Geddes (2000)</mixed-citation>
            <mixed-citation id="d755e269" publication-type="other">
Geddes (2003)</mixed-citation>
            <mixed-citation id="d755e275" publication-type="other">
Sainsbury (2006)</mixed-citation>
         </ref>
         <ref id="d755e283a1310">
            <label>9</label>
            <mixed-citation id="d755e290" publication-type="other">
Mhone (2004)</mixed-citation>
         </ref>
         <ref id="d755e297a1310">
            <label>10</label>
            <mixed-citation id="d755e304" publication-type="other">
Grimson and Jelin (2006)</mixed-citation>
            <mixed-citation id="d755e310" publication-type="other">
Olivier (2011)</mixed-citation>
         </ref>
         <ref id="d755e317a1310">
            <label>11</label>
            <mixed-citation id="d755e324" publication-type="other">
Adésinà (2007)</mixed-citation>
            <mixed-citation id="d755e330" publication-type="other">
Lund (2009)</mixed-citation>
            <mixed-citation id="d755e336" publication-type="other">
Mhone (2004)</mixed-citation>
         </ref>
         <ref id="d755e343a1310">
            <label>12</label>
            <mixed-citation id="d755e350" publication-type="other">
UNRISD (2010), chapters 5
and 6</mixed-citation>
            <mixed-citation id="d755e359" publication-type="other">
Arrighi, 1973</mixed-citation>
         </ref>
         <ref id="d755e366a1310">
            <label>13</label>
            <mixed-citation id="d755e373" publication-type="other">
(Ratha et al., 2011, p. 22</mixed-citation>
         </ref>
         <ref id="d755e380a1310">
            <label>14</label>
            <mixed-citation id="d755e387" publication-type="other">
Ratha et al. (2011, p. 27)</mixed-citation>
            <mixed-citation id="d755e393" publication-type="other">
Sabates-Wheeler (2011)</mixed-citation>
         </ref>
         <ref id="d755e401a1310">
            <label>15</label>
            <mixed-citation id="d755e410" publication-type="other">
Kayenze (2004, p. 2)</mixed-citation>
            <mixed-citation id="d755e416" publication-type="other">
Cooper (1989)</mixed-citation>
            <mixed-citation id="d755e423" publication-type="other">
Crush et al. (1991)</mixed-citation>
            <mixed-citation id="d755e429" publication-type="other">
Burawoy (1976)</mixed-citation>
         </ref>
         <ref id="d755e436a1310">
            <label>17</label>
            <mixed-citation id="d755e443" publication-type="other">
Ratha et al. (2011)</mixed-citation>
         </ref>
         <ref id="d755e450a1310">
            <label>18</label>
            <mixed-citation id="d755e457" publication-type="other">
McConnell (2009)</mixed-citation>
            <mixed-citation id="d755e463" publication-type="other">
Dodson (2008)</mixed-citation>
            <mixed-citation id="d755e469" publication-type="other">
Dugbazah (2012)</mixed-citation>
            <mixed-citation id="d755e476" publication-type="other">
"Gender, migration and socioeconomic devel-
opment in Africa," held in Cairo, Egypt, 2010 (papers available at http://www
.codesria.org/spip.php7article980)</mixed-citation>
            <mixed-citation id="d755e488" publication-type="other">
Bond,
2007, p. 201; Burawoy, 1976; Kayenze, 2004)</mixed-citation>
         </ref>
         <ref id="d755e498a1310">
            <label>19</label>
            <mixed-citation id="d755e505" publication-type="other">
Hujo and Piper (2010)</mixed-citation>
         </ref>
         <ref id="d755e512a1310">
            <label>20</label>
            <mixed-citation id="d755e519" publication-type="other">
Aleinikoff &amp; Chetail, 2003, p. 307</mixed-citation>
         </ref>
         <ref id="d755e526a1310">
            <label>21</label>
            <mixed-citation id="d755e533" publication-type="other">
ILO (2004, chapter
4)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d755e552a1310">
            <mixed-citation id="d755e556" publication-type="other">
Adams, R. Jr., &amp; Page, J. (2005). Do international migration and remittances reduce
poverty in developing countries? World Development, 33(10), 1645-1669.</mixed-citation>
         </ref>
         <ref id="d755e566a1310">
            <mixed-citation id="d755e570" publication-type="other">
Adepoju, A. (2008). Migration and social policy in sub-Saharan Africa, unpublished
background paper commissioned for the project on Social Policy and Mi-
gration in Developing Countries. Geneva: UNRISD. Retrieved from http://
unrisd.org/unrisd/website/document.nsf/ab82a6805797760f80256b4f005dal
ab/ac96a6e2d6c0ff9ccl25751200354ddb/$FILE/draft_adepoju.pdf</mixed-citation>
         </ref>
         <ref id="d755e589a1310">
            <mixed-citation id="d755e593" publication-type="other">
Adepoju, A., &amp; Appleyard, R. (1996). The relevance of research on emigration dy-
namics for policy makers in sub-Saharan Africa. International Migration, 34(2),
321-333.</mixed-citation>
         </ref>
         <ref id="d755e606a1310">
            <mixed-citation id="d755e610" publication-type="other">
Adésinà, J. (Ed.). (2007). Social policy in sub-Saharan African context: In search of inclu-
sive development. Basingstoke: Palgrave McMillan/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e621a1310">
            <mixed-citation id="d755e625" publication-type="other">
Aleinikoff. A., &amp; Chetail, V. (Eds.). (2003). Migration and international legal norms.
The Hague: T.M.C. Asser Press.</mixed-citation>
         </ref>
         <ref id="d755e635a1310">
            <mixed-citation id="d755e639" publication-type="other">
Arrighi, G. (1973). International corporations, labor aristocracies, and economic
development in tropical Africa. In G. Arrighi &amp; J. Saul (Eds.), Essays on the
political economy of Africa (pp. 105-151). New York: Monthly Review Press.</mixed-citation>
         </ref>
         <ref id="d755e652a1310">
            <mixed-citation id="d755e656" publication-type="other">
Bakewell, O., de Haas, H., Castles, S., Vezzoli, S., &amp; Jonsson, G. (2009). South-South
migration and human development: Reflections on African experiences (IMI work-
ing papers, No. 15). Oxford: Oxford University.</mixed-citation>
         </ref>
         <ref id="d755e669a1310">
            <mixed-citation id="d755e673" publication-type="other">
Betts, A. (2010). Migration governance: Alternative futures (background paper for
WMR 2010). Geneva: International Organization for Migration.</mixed-citation>
         </ref>
         <ref id="d755e683a1310">
            <mixed-citation id="d755e687" publication-type="other">
Betts, A. (2011). Global migration governance. Oxford: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d755e694a1310">
            <mixed-citation id="d755e698" publication-type="other">
Black, R., Crush, J., Peberdy, S., &amp; Ammassari, S. (Eds.). (2006). Migration and develop-
ment in Africa: An overview. Pretoria: Institute for Democracy in SAMP Project.</mixed-citation>
         </ref>
         <ref id="d755e709a1310">
            <mixed-citation id="d755e713" publication-type="other">
Bolderson, H. (2011). The ethics of welfare provision for migrants: A case for
equal treatment and the repositioning of welfare. Journal of Social Policy, 40(2),
219-236.</mixed-citation>
         </ref>
         <ref id="d755e726a1310">
            <mixed-citation id="d755e730" publication-type="other">
Bommes, M., &amp; Geddes, A. (2000). Immigration and welfare: Challenging the borders of
the welfare state. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d755e740a1310">
            <mixed-citation id="d755e744" publication-type="other">
Bond, P. (2007). The sociopolitical structure of accumulation and social policy in
Southern Africa. In J. Adésinà (Ed.), Social policy in sub-Saharan African con-
text: In search of inclusive development (pp. 198-223). Basingstoke: Palgrave
McMillan/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e760a1310">
            <mixed-citation id="d755e764" publication-type="other">
Burawoy, M. (1976). The functions and reproduction of migrant labor: Compara-
tive material from Southern Africa and the United States. American Journal of
Sociology, 81(5), 1050-1087.</mixed-citation>
         </ref>
         <ref id="d755e777a1310">
            <mixed-citation id="d755e781" publication-type="other">
Cooper, F. (1989). From free labor to family allowances: Labor and African society
in colonial discourse. American Ethnologist, 16(4), 745-765.</mixed-citation>
         </ref>
         <ref id="d755e791a1310">
            <mixed-citation id="d755e795" publication-type="other">
Cross, C, &amp; Omoluabi, E. (2006). Introduction. In C. Cross, D. Gelderblom, N.
Roux, &amp; J. Mafukidz (Eds.), Views on migration in sub-Saharan Africa. Cape
Town: HSRC Press.</mixed-citation>
         </ref>
         <ref id="d755e809a1310">
            <mixed-citation id="d755e813" publication-type="other">
Crush, J., Jeeves, A.H., &amp; Yudelman, D. (1991). South Africa's labor empire: A history
of black migrancy to the gold mines. Boulder, CO: Westview Press.</mixed-citation>
         </ref>
         <ref id="d755e823a1310">
            <mixed-citation id="d755e827" publication-type="other">
Deacon, B., Macovei, M., Van Langenhove, L., &amp; Yeates, N. (2010). World-regional
social policy and global governance. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d755e837a1310">
            <mixed-citation id="d755e841" publication-type="other">
de Haan, A. &amp; Yaqub, S. (2010). Migration and poverty: Linkages, knowledge gaps
and policy implications. In K. Hujo &amp; N. Piper (Eds.), South-South migra-
tion: Implications for social policy and development (pp. 190-219). Basingstoke:
Palgra ve/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e857a1310">
            <mixed-citation id="d755e861" publication-type="other">
de Haas, H. (2007). Remittances, migration and social development: A conceptual re-
view of the literature (Social Policy and Development paper, No. 34). Geneva:
UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e874a1310">
            <mixed-citation id="d755e878" publication-type="other">
Dodson, B. (2008). Gender, migration and livelihoods: Migrant women in South-
ern Africa. In N. Piper (Ed.), New perspectives on gender and migration (pp. 137-
158). London: Routledge.</mixed-citation>
         </ref>
         <ref id="d755e891a1310">
            <mixed-citation id="d755e895" publication-type="other">
Dugbazah, J. (2012). Gender, livelihoods and migration in Africa. Bloomington, IN:
Xlibris Publishing.</mixed-citation>
         </ref>
         <ref id="d755e906a1310">
            <mixed-citation id="d755e910" publication-type="other">
Ellis, F. (2003.) A livelihoods approach to migration and poverty reduction. London:
DFID. Retrieved from http://www.uea.ac.uk/polopoly_fs/l-5341712003%20
livelihoods%20migration.pdf</mixed-citation>
         </ref>
         <ref id="d755e923a1310">
            <mixed-citation id="d755e927" publication-type="other">
Faist, T. (1996). Immigration, integration and the welfare state: Germany and the
USA in a comparative perspective. In R. Bauböck, A. Heller &amp; A.R. Zolberg
(Eds.), The challenge of diversity: Integration and pluralism in societies of immigra-
tion (pp. 227-258). Aldershot: Avebury.</mixed-citation>
         </ref>
         <ref id="d755e943a1310">
            <mixed-citation id="d755e947" publication-type="other">
Gagnon, J., &amp; Khoudour-Castéras, D. (2012). South-South migration in West Africa:
Addressing the challenge of immigrant integration (OECD Development Centre
Working Paper, No. 312). Paris: Organization for Economic Cooperation and
Development.</mixed-citation>
         </ref>
         <ref id="d755e963a1310">
            <mixed-citation id="d755e967" publication-type="other">
Geddes, A. (2003). The politics of migration and immigration in Europe. London:
Sage.</mixed-citation>
         </ref>
         <ref id="d755e977a1310">
            <mixed-citation id="d755e981" publication-type="other">
Geiger, M., &amp; Pécoud, A. (2012). The politics of international migration manage-
ment. In M. Geiger &amp; A. Pécoud (Eds.), The politics of international migration
management (pp. 1-20). Basingstoke: Palgrave.</mixed-citation>
         </ref>
         <ref id="d755e994a1310">
            <mixed-citation id="d755e998" publication-type="other">
Grimson, A., &amp; Jelin, E. (Eds.). (2006). Migraciones regionales hacia la Argentina. Dife-
rencia, desigualdad y derechos. Buenos Aires: Prometeo.</mixed-citation>
         </ref>
         <ref id="d755e1009a1310">
            <mixed-citation id="d755e1013" publication-type="other">
Grugel, J.B., &amp; Piper, N. (2007). Critical perspectives on global governance: Rights and
regulation in governing regimes. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d755e1023a1310">
            <mixed-citation id="d755e1027" publication-type="other">
Hagen-Zanker, J., &amp; Leon Himmelstine, C. (2012). How does access to social pro-
tection programmes affect the decision to migrate? (ODI Background Note,
April 2012). London: Overseas Development Institute.</mixed-citation>
         </ref>
         <ref id="d755e1040a1310">
            <mixed-citation id="d755e1044" publication-type="other">
Holzmann, R., Koettl, J., &amp; Chernetsky, T. (2005). Portability regimes of pension and
health care benefits for international migrants: An analysis of issues and good prac-
tices (Social Protection Discussion paper, Series No. 0519). Washington, DC:
World Bank.</mixed-citation>
         </ref>
         <ref id="d755e1060a1310">
            <mixed-citation id="d755e1064" publication-type="other">
Hujo, K., &amp; Piper, N. (2010). Linking migration, social development and policy
in the South: An introduction. In K. Hujo &amp; N. Piper (Eds.), South-South mi-
gration: Implications for social policy and development (pp. 1—45). Basingstoke:
Palgrave/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e1080a1310">
            <mixed-citation id="d755e1084" publication-type="other">
International Labor Organization. (2004). Towards a fair deal for migrant workers in
the global economy, International Labor Conference, 92nd session, Report VI,
Sixth item on the agenda. Geneva: ILO. Retrieved from http://www.ilo.org/
public/english/standards/relm/ilc/ilc92/pdf/rep-vi.pdf</mixed-citation>
         </ref>
         <ref id="d755e1100a1310">
            <mixed-citation id="d755e1104" publication-type="other">
International Labor Organization (2006). ILO multilateral framework on labor migra-
tion. Geneva: ILO.</mixed-citation>
         </ref>
         <ref id="d755e1115a1310">
            <mixed-citation id="d755e1119" publication-type="other">
International Labor Organization. (2010). Faire des migrations un facteur de dévelop-
pement: Une étude sur l'Afrique du Nord et l'Afrique d l'Ouest. Geneva: Institut
International d'Etudes Sociales.</mixed-citation>
         </ref>
         <ref id="d755e1132a1310">
            <mixed-citation id="d755e1136" publication-type="other">
International Labor Organization. (2011). World social security report 2010-11. Ge-
neva: ILO.</mixed-citation>
         </ref>
         <ref id="d755e1146a1310">
            <mixed-citation id="d755e1150" publication-type="other">
International Labor Organization. (2012). Social protection floors recommendation,
2012, No. 202. Retrieved from http://www.social-protection.org/gimi/gess/
RessShowRessource.do?ressourceId=31088</mixed-citation>
         </ref>
         <ref id="d755e1163a1310">
            <mixed-citation id="d755e1167" publication-type="other">
International Organization for Migration. (2008). World migration report 2008. Ge-
neva: IOM.</mixed-citation>
         </ref>
         <ref id="d755e1177a1310">
            <mixed-citation id="d755e1181" publication-type="other">
Iredale, R., Piper, N., &amp; Ancog, A. (2005). Impact of ratifying the 1990 UN Convention
on the Rights of All Migrant Workers and Members of Their Family: Case studies of
the Philippines and Sri Lanka (Working Paper No. 15, prepared for UNESCO,
Asia Pacific Migration Research Network (APMRN)). Canberra: Australian
National University.</mixed-citation>
         </ref>
         <ref id="d755e1200a1310">
            <mixed-citation id="d755e1204" publication-type="other">
Kayenze, G. (2004, March). African migrant labor situation in Southern Africa. Pa-
per presented at the ICFTU-AFRO Conference on Migrant Labor, Nairobi.
Retrieved from http://www.gurn.info/en/topics/migration/research-and-
trends-in-labor-migration/africa/african-migrant-labor-situation-in-southern-
africa</mixed-citation>
         </ref>
         <ref id="d755e1224a1310">
            <mixed-citation id="d755e1228" publication-type="other">
Kofman, E., &amp; Raghuram, P. (2012). The implications of migration for gender and
care regimes in the South. In K. Hujo &amp; N. Piper (Eds.), South-South migra-
tion: Implications for social policy and development (pp. 46-83). Basingstoke:
Palgrave/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e1244a1310">
            <mixed-citation id="d755e1248" publication-type="other">
Landau, L. B., &amp; Kazadi Wa Kabwe-Segatti, A. (2009). Human development impacts
of migration: South Africa case study (Human Development Research Paper
2009/5). New York: United Nations Development Programme.</mixed-citation>
         </ref>
         <ref id="d755e1261a1310">
            <mixed-citation id="d755e1265" publication-type="other">
Lund, F. (2009). South Africa: Transition under pressure. In P. Alcock &amp; G. Craig
(Eds.), International social policy: Welfare regimes in the developed world (pp. 267-
286). Basingstoke: Palgrave McMillan.</mixed-citation>
         </ref>
         <ref id="d755e1278a1310">
            <mixed-citation id="d755e1282" publication-type="other">
Mafukidze, J. (2006). A discussion of migration and migration patterns and flows
in Africa. In C. Cross, D. Gelderblom, N. Roux, &amp; J. Mafukidz (Eds.), Views on
migration in sub-Saharan Africa (pp. 103-129). Cape Town: HSRC Press.</mixed-citation>
         </ref>
         <ref id="d755e1295a1310">
            <mixed-citation id="d755e1299" publication-type="other">
Massey, D.S., Arango, J., Hugo, G., Kouaouci, A., Pellegrino, A., &amp; Taylor, J.E.
(1993). Theories of international migration: A review and appraisal. Popula-
tion and Development Review, 19(3), 431-466.</mixed-citation>
         </ref>
         <ref id="d755e1312a1310">
            <mixed-citation id="d755e1316" publication-type="other">
McConnell, C. (2009). Migration and xenophobia in South Africa. Conflict Trends,
1, 34-40.</mixed-citation>
         </ref>
         <ref id="d755e1327a1310">
            <mixed-citation id="d755e1333" publication-type="other">
McDonald, D., &amp; Crush, J. (2002). Introduction. Thinking about the brain drain
in Southern Africa. In D. McDonald &amp; J. Crush (Eds.), Destinations unknown:
Perspectives on the brain drain in Southern Africa. Pretoria: Africa Institute and
SAMP.</mixed-citation>
         </ref>
         <ref id="d755e1349a1310">
            <mixed-citation id="d755e1353" publication-type="other">
Meyer, J.B. (2010). Human resource flows from and between developing coun-
tries: Implications for social and public policies. In K. Hujo &amp; N. Piper (Eds.),
South-South migration: Implications for social policy and development (pp. 84-119).
Basingstoke: Palgrave/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e1369a1310">
            <mixed-citation id="d755e1373" publication-type="other">
Mhone, G. (2004). Historical trajectories of social policy in post-colonial Africa:
The case of Zambia. In T. Mkandawire (Ed.), Social policy in a development con-
text (pp. 308-338). Basingstoke: Palgrave/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e1386a1310">
            <mixed-citation id="d755e1390" publication-type="other">
Mkandawire, T. (Ed.). (2004). Social policy in a development context. Basingstoke:
Palgrave/UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e1400a1310">
            <mixed-citation id="d755e1404" publication-type="other">
Mkandwire, T. (2009). From the national question to the social question. Transfor-
mation, 69,130-160.</mixed-citation>
         </ref>
         <ref id="d755e1414a1310">
            <mixed-citation id="d755e1418" publication-type="other">
Morissens, A. (2008). Migration, Welfare States and the Incorporation of Migrants in
Different Welfare Regimes (mimeo paper prepared for the UNRISD Programme
on Identities, Conflict and Cohesion). Geneva: UNRISD.</mixed-citation>
         </ref>
         <ref id="d755e1432a1310">
            <mixed-citation id="d755e1436" publication-type="other">
Morissens, A., &amp; Sainsbury, D. (2005). Migrants' social rights, ethnicity and welfare
regimes. Journal of Social Policy, 34(4), 637-660.</mixed-citation>
         </ref>
         <ref id="d755e1446a1310">
            <mixed-citation id="d755e1450" publication-type="other">
Newland, K. (2010). The governance of international migration: Mechanisms, pro-
cesses, and institutions. Global Governance, 16(2010), 331-343.</mixed-citation>
         </ref>
         <ref id="d755e1460a1310">
            <mixed-citation id="d755e1464" publication-type="other">
OECD. (2012). Development aid at a glance: Africa. Paris: Organization for Eco-
nomic Cooperation and Development. Retrieved from http://www.oecd.org/
investment/aidstatistics/42139250.pdf</mixed-citation>
         </ref>
         <ref id="d755e1477a1310">
            <mixed-citation id="d755e1481" publication-type="other">
Olivier, M. (2011). Political and regulatory dimensions of access, portability and
exclusion: Social security for migrants, with an emphasis on migrants in
Southern Africa. In R. Sabates-Wheeler, &amp; R. Feldman (Eds.), Migration and
social protection: Claiming social rights beyond borders (pp. 117-142). Basingstoke:
Palgrave.</mixed-citation>
         </ref>
         <ref id="d755e1500a1310">
            <mixed-citation id="d755e1504" publication-type="other">
Ratha, D., Sänket, M., Caglar, O., Plaza, S., Shaw, W., &amp; Shimeles, A. (Eds.). (2011).
Leveraging migration for Africa: Remittances, skills, and investment. Washington,
DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d755e1517a1310">
            <mixed-citation id="d755e1521" publication-type="other">
Ratha, D., &amp; Shaw, W. (2007). South-South migration and remittances (Working Paper
No. 102). Washington DC: World Bank.</mixed-citation>
         </ref>
         <ref id="d755e1532a1310">
            <mixed-citation id="d755e1536" publication-type="other">
Sabates-Wheeler, R. (2011). Coping and investment strategies of migrants in the
South: Malawian migrants in South Africa. In R. Sabates-Wheeler &amp; R. Feld-
man (Eds.), Migration and social protection: Claiming social rights beyond borders
(pp. 232-261). Basingstoke: Palgrave.</mixed-citation>
         </ref>
         <ref id="d755e1552a1310">
            <mixed-citation id="d755e1556" publication-type="other">
Sabates-Wheeler, R., &amp; Feldman, R. (Eds.). (2011). Migration and social protection:
Claiming social rights beyond borders. Basingstoke: Palgrave.</mixed-citation>
         </ref>
         <ref id="d755e1566a1310">
            <mixed-citation id="d755e1570" publication-type="other">
Sabates-Wheeler, R., Koettl, J., &amp; Avato, J. (2011). Social security for migrants: A
global overview of portability arrangements. In R. Sabates-Wheeler &amp; R. Feld-
man (Eds.), Migration and social protection: Claiming social rights beyond borders
(pp. 91-116). Basingstoke: Palgrave.</mixed-citation>
         </ref>
         <ref id="d755e1586a1310">
            <mixed-citation id="d755e1590" publication-type="other">
Sabates-Wheeler, R., &amp; Waite, M. (2003). Migration and social protection: A concept
paper (Working Paper T2). Brighton: Development Research Centre on Migra-
tion, Globalisation and Poverty, University of Sussex.</mixed-citation>
         </ref>
         <ref id="d755e1603a1310">
            <mixed-citation id="d755e1607" publication-type="other">
Sainsbury, D. (2006). Immigrants' social rights in comparative perspective: Welfare
regimes, forms of immigration and immigration policy regimes. Journal of Eu-
ropean Social Policy, 16(3), 229-244.</mixed-citation>
         </ref>
         <ref id="d755e1620a1310">
            <mixed-citation id="d755e1624" publication-type="other">
Todaro, M. P. (1976). International migration in developing countries: A review of the-
ory, evidence, methodology and research priorities. Geneva: International Labor
Office.</mixed-citation>
         </ref>
         <ref id="d755e1638a1310">
            <mixed-citation id="d755e1642" publication-type="other">
Townsend, P. (2007). The right to social security and national development: Lessons from
OECD experience for low-income countries (ILO discussion paper, No. 18). Ge-
neva: ILO, Social Security Department.</mixed-citation>
         </ref>
         <ref id="d755e1655a1310">
            <mixed-citation id="d755e1659" publication-type="other">
UNCTAD. (2012). Statistics database. Geneva: United Nations Conference on Trade
and Development. Retrieved from http://unctadstat.unctad.org/TableViewer/
table View.aspx?ReportId=88</mixed-citation>
         </ref>
         <ref id="d755e1672a1310">
            <mixed-citation id="d755e1676" publication-type="other">
United Nations. (2006). World migrant stock: The 2005 revision population database.
New York: United Nations Population Division.</mixed-citation>
         </ref>
         <ref id="d755e1686a1310">
            <mixed-citation id="d755e1690" publication-type="other">
UNRISD. (2006). Transformative social policy: Lessons from UNRISD research (Re-
search and Policy Brief No. 5). Geneva: United Nations Research Institute for
Social Development.</mixed-citation>
         </ref>
         <ref id="d755e1703a1310">
            <mixed-citation id="d755e1707" publication-type="other">
UNRISD. (2010). Combating poverty and inequality. Geneva: United Nations Re-
search Institute for Social Development.</mixed-citation>
         </ref>
         <ref id="d755e1717a1310">
            <mixed-citation id="d755e1721" publication-type="other">
Williams, V., &amp; Carr, L. (2006). The Draft Protocol on the Facilitation of Movement of
Persons in SADC: Implications for State Parties (SAMP Migration Policy Brief
No. 18). Cape Town: Southern African Migration Project.</mixed-citation>
         </ref>
         <ref id="d755e1735a1310">
            <mixed-citation id="d755e1739" publication-type="other">
World Bank. (2006). Global economic prospects 2006: Economic implications of remit-
tances and migration. Washington, DC: World Bank.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
