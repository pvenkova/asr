<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">taxon</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100387</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Taxon</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>International Bureau for Plant Taxonomy</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00400262</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">27757051</article-id>
         <article-categories>
            <subj-group>
               <subject>Molecular Phylogenetics and Biogeography</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>A Multi-Locus Phylogeny of Euryops (Asteraceae, Senecioneae) Augments Support for the "Cape to Cairo" Hypothesis of Floral Migrations in Africa</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nicolas</given-names>
                  <surname>Devos</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nigel P.</given-names>
                  <surname>Barker</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Bertil</given-names>
                  <surname>Nordenstam</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ladislav</given-names>
                  <surname>Mucina</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>2</month>
            <year>2010</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">59</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i27757042</issue-id>
         <fpage>57</fpage>
         <lpage>67</lpage>
         <permissions>
            <copyright-statement>Copyright 2010 International Association for Plant Taxonomy</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/27757051"/>
         <abstract>
            <p>With about 100 species, Euryops (Cass.) Cass. ranks among the most speciose genera of the tribe Senecioneae (Asteraceae). The genus has its greatest diversity in South Africa, and displays an interesting disjunct distribution with most of the taxa found in southern Africa and a group of eight endemic species confined to the mountains of tropical East Africa and northeastern Africa. Molecular phylogenetic analyses of DNA sequence data from three chloroplast fragments and the nuclear ITS region were used to reconstruct the evolutionary history of 41 Euryops species in order to unravel species relationships and to determine the origin of the disjunct Afromontane taxa. Our results show a lack of support and resolution in the internal structure of the trees, but also reveal strong incongruence between the ITS and cpDNA datasets as assessed by Bayes Factors. We hypothesise that this is a consequence of the isolation and divergence of many populations over a short time period at some point in the history of the genus. Molecular dating based on our phylogenetic tree suggests that the genus diversified in South Africa around four million years ago. The origin of the East African species, dated at 1.9 Ma, well after the uplift of the East African mountains, is consistent with a scenario of a single dispersal event from South Africa northwards into the tropical East African mountains where diversification occurred, creating a monophyletic group of regional Afromontane endemics.</p>
         </abstract>
         <kwd-group>
            <kwd>Asteraceae</kwd>
            <kwd>Bayes Factors</kwd>
            <kwd>chloroplast DNA</kwd>
            <kwd>disjunct distribution</kwd>
            <kwd>Euryops</kwd>
            <kwd>ITS</kwd>
            <kwd>lineage sorting: migration routes</kwd>
            <kwd>molecular dating</kwd>
            <kwd>radiation</kwd>
            <kwd>topological incongruence</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>Literature Cited</title>
         <ref id="d148e220a1310">
            <mixed-citation id="d148e224" publication-type="other">
Adamson, R.S. 1958. The Cape as an ancient African flora. Advancem.
Sci. 58: 1–10.</mixed-citation>
         </ref>
         <ref id="d148e234a1310">
            <mixed-citation id="d148e238" publication-type="other">
Akaike, H. 1974. A new look at the statistical model identification.
IEEE Trans. Autom. Control 19: 716–723.</mixed-citation>
         </ref>
         <ref id="d148e248a1310">
            <mixed-citation id="d148e252" publication-type="other">
Alvarez, I. &amp;amp; Wendel, J.F. 2003. Ribosomal ITS sequences and plant
phylogenetic inference. Molec. Phylog. Evol. 29: 417–434.</mixed-citation>
         </ref>
         <ref id="d148e262a1310">
            <mixed-citation id="d148e266" publication-type="other">
Andreasen, K. &amp;amp; Baldwin, B.G. 2001. Unequal evolutionary rates be-
tween annual and perennial lineages of checker mallows (Sidalcea,
Malvaceae): Evidence from 18S-26S rDNA internal and external
transcribed spacers. Molec. Biol. Evol. 18: 936–944.</mixed-citation>
         </ref>
         <ref id="d148e283a1310">
            <mixed-citation id="d148e287" publication-type="other">
Axelrod, D.I. &amp;amp; Raven, P.H. 1978. Late Cretaceous and Tertiary
vegetation history of Africa. Pp. 79–130 in: Werger, M.J.A. (ed.),
Biogeography and ecology of southern Africa. The Hague: Dr.
W. Junk.</mixed-citation>
         </ref>
         <ref id="d148e303a1310">
            <mixed-citation id="d148e307" publication-type="other">
Baldwin, B.G. &amp;amp; Sanderson, M. J. 1998. Age and rate of diversifica-
tion of the Hawaiian silversword alliance (Compositae). Proc. Natl.
Acad. Sci. U.S.A. 95: 9402–9406.</mixed-citation>
         </ref>
         <ref id="d148e320a1310">
            <mixed-citation id="d148e324" publication-type="other">
Barker, F.K. &amp;amp; Lutzoni, F. 2002. The utility of the incongruence
length difference test. Syst. Biol. 51: 625–637.</mixed-citation>
         </ref>
         <ref id="d148e334a1310">
            <mixed-citation id="d148e338" publication-type="other">
Bellstedt, D.U., van Zyl, L., Marais, E.M., Bytebier, B., de Vil-
liers, C.A., Makwarela, A.M. &amp;amp; Dreyer, L.L. 2008. Phylogenetic
relationships, character evolution and biogeography of southern
African members of Zygophyllum (Zygophyllaceae) based on three
plastid regions. Molec. Phylog. Evol. 47: 932–949.</mixed-citation>
         </ref>
         <ref id="d148e357a1310">
            <mixed-citation id="d148e361" publication-type="other">
Carbutt, C. &amp;amp; Edwards, T.J. 2004. Cape elements on high-altitude
corridors and edaphic islands: Historical aspects and preliminary
phytogeography. Syst. Geogr. Pl. 71: 1033–1061.</mixed-citation>
         </ref>
         <ref id="d148e374a1310">
            <mixed-citation id="d148e378" publication-type="other">
Chase, M.W. &amp;amp; Hills, H.H. 1991. Silica gel: An ideal material for field
preservation of leaf samples for DNA studies. Taxon 40: 215–220.</mixed-citation>
         </ref>
         <ref id="d148e389a1310">
            <mixed-citation id="d148e393" publication-type="other">
Chorowicz, J. 2005. The East African rift system. J. Afr. Earth Sci.
43: 379–410.</mixed-citation>
         </ref>
         <ref id="d148e403a1310">
            <mixed-citation id="d148e407" publication-type="other">
Clark, V.R., Barker, N.P. &amp;amp; Mucina, L. 2009. The Sneeuberg: A
new centre of floristic endemism on the Great Escarpment, South
Africa. S. African J. Bot. 75: 196–238.</mixed-citation>
         </ref>
         <ref id="d148e420a1310">
            <mixed-citation id="d148e424" publication-type="other">
Cunningham, C.W. 1997. Is congruence between data partitions a
reliable predictor of phylogenetic accuracy? Empirically testing
an iterative procedure for choosing among phylogenetic methods.
Syst. Biol. 46: 464–478.</mixed-citation>
         </ref>
         <ref id="d148e440a1310">
            <mixed-citation id="d148e444" publication-type="other">
De Menocal, P.B. 2004. African climate change and faunal evolution
during the Pliocene-Pleistocene. Earth Planet. Sci. Lett. 220: 3–24.</mixed-citation>
         </ref>
         <ref id="d148e454a1310">
            <mixed-citation id="d148e458" publication-type="other">
DeSalle, R., Absher, R. &amp;amp; Amato, G. 1994. Speciation and phyloge-
netic resolution. Trends Ecol. Evol. 9: 297–298.</mixed-citation>
         </ref>
         <ref id="d148e468a1310">
            <mixed-citation id="d148e472" publication-type="other">
Dolphin, K., Beishaw, R., Orme, C.D.L. &amp;amp; Quicke, D.L.J. 2000.
Noise and incongruence: Interpreting results of the incongruence
length difference test. Molec. Phylog. Evol. 17: 401–406.</mixed-citation>
         </ref>
         <ref id="d148e486a1310">
            <mixed-citation id="d148e490" publication-type="other">
Doyle, J. J. &amp;amp; Doyle, J.L. 1987. Preservation of plant samples for DNA
restriction endonuclease analysis. Taxon 36: 715–722.</mixed-citation>
         </ref>
         <ref id="d148e500a1310">
            <mixed-citation id="d148e506" publication-type="other">
Drummond, A. J., Ho, S.Y.W., Phillips, M. J. &amp;amp; Rambaut, A. 2006.
Relaxed phylogenetics and dating with confidence. PLoS Biol 4:
e88. doi:10.1371/journal.pbio.0040088.</mixed-citation>
         </ref>
         <ref id="d148e519a1310">
            <mixed-citation id="d148e523" publication-type="other">
Drummond, A.J., Nicholls, G.K., Rodrigo, A.G. &amp;amp; Solomon, W.
2002. Estimating mutation parameters, population history and ge-
nealogy simultaneously from temporally spaced sequence data.
Genetics 161: 1307–1320.</mixed-citation>
         </ref>
         <ref id="d148e539a1310">
            <mixed-citation id="d148e543" publication-type="other">
Drummond, A.J. &amp;amp; Rambaut, A. 2007. BEAST: Bayesian evolutionary
analysis by sampling trees. BMCEvol. Biol 7: 214. doi:10.1186/1471-
2148-7-214.</mixed-citation>
         </ref>
         <ref id="d148e556a1310">
            <mixed-citation id="d148e560" publication-type="other">
Farris, J.S, Källersjö, M., Kluge, A.G. &amp;amp; Bult, C. 1994. Testing
significance of incongruence. Cladistics 10: 315–319.</mixed-citation>
         </ref>
         <ref id="d148e570a1310">
            <mixed-citation id="d148e574" publication-type="other">
Galley, C., Bytebier, B., Sellstedt, D.U. &amp;amp; Linder, H.P. 2007. The
Cape element in the Afrotemperate flora: From Cape to Cairo?
Proc. Roy. Soc. London, Ser. B, Biol. Sci. 274: 535–543.</mixed-citation>
         </ref>
         <ref id="d148e588a1310">
            <mixed-citation id="d148e592" publication-type="other">
Galley, C. &amp;amp; Linder, H.R 2006. Geographical affinities of the Cape
flora, South Africa. J. Biogeogr. 33: 236–250.</mixed-citation>
         </ref>
         <ref id="d148e602a1310">
            <mixed-citation id="d148e606" publication-type="other">
Gene Codes Corporation. 1998. Sequencher 4.01 reference: Advanced,
user friendly software tools for DNA sequencing. Madison, Wis-
consin: Gene Codes Corporation.</mixed-citation>
         </ref>
         <ref id="d148e619a1310">
            <mixed-citation id="d148e623" publication-type="other">
Holder, M.T., Anderson, J.A. &amp;amp; Holloway, A.K. 2001. Difficulties
in detecting hybridization. Syst. Biol. 50: 978–982.</mixed-citation>
         </ref>
         <ref id="d148e633a1310">
            <mixed-citation id="d148e637" publication-type="other">
Holland, P.G. 1978. An evolutionary biogeography of the genus Aloe.
J. Biogeogr. 5: 213–226.</mixed-citation>
         </ref>
         <ref id="d148e647a1310">
            <mixed-citation id="d148e651" publication-type="other">
Howis, S., Barker, N.P. &amp;amp; Mucina, L. 2009. Globally grown, but
poorly known: Species limits and biogeography of Gazania Gaert.
(Asteraceae) inferred from chloroplast and nuclear DNA sequence
data. Taxon 58: 871–882.</mixed-citation>
         </ref>
         <ref id="d148e667a1310">
            <mixed-citation id="d148e671" publication-type="other">
Hughes, C. &amp;amp; Eastwood, R. 2006. Island radiation on a continental
scale: Exceptional rates of plant diversification after uplift of the
Andes. Proc. Natl. Acad. Sci. U.S.A. 103: 10334–10339.</mixed-citation>
         </ref>
         <ref id="d148e685a1310">
            <mixed-citation id="d148e689" publication-type="other">
Hughes, J. &amp;amp; Vogler, A.P. 2004. The phylogeny of acorn weevils
(genus Curculio) from mitochondrial and nuclear DNA sequences:
The problem of incomplete data. Molec. Phylog. Evol. 32: 601–615.</mixed-citation>
         </ref>
         <ref id="d148e702a1310">
            <mixed-citation id="d148e706" publication-type="other">
Irestedt, M., Fjeldså, J, Nylander, J.A.A. &amp;amp; Ericson, P.G.P. 2004.
Phylogenetic relationships of typical antbirds (Thamnophilidae)
and test of incongruence based on Bayes factors. BMC Evol. Biol.
4: 23. doi:10.1186/1471-2148-4-23.</mixed-citation>
         </ref>
         <ref id="d148e722a1310">
            <mixed-citation id="d148e726" publication-type="other">
Kass, R.E. &amp;amp; Raftery, A.E. 1995. Bayes factors. J. Amer. Statist.
Assoc. 90: 773–795.</mixed-citation>
         </ref>
         <ref id="d148e736a1310">
            <mixed-citation id="d148e740" publication-type="other">
Killick, D.J.B. 1963. An account of the plant ecology of the Cathedral
peak area on the Natal Drakensberg. Mem. Bot. Surv. South Africa
31: 1–178.</mixed-citation>
         </ref>
         <ref id="d148e753a1310">
            <mixed-citation id="d148e757" publication-type="other">
Killick, D.J.B. 1978. The Afro-alpine region. Pp. 515–560 in: Werger,
M.J.A. (ed.), Biogeography and ecology of southern Africa. The
Hague: Dr. W. Junk.</mixed-citation>
         </ref>
         <ref id="d148e770a1310">
            <mixed-citation id="d148e774" publication-type="other">
Klak, C., Reeves, G. &amp;amp; Hedderson, T. 2004. Unmatched tempo of evo-
lution in Southern African semi-desert ice plants. Nature 427: 63–65.</mixed-citation>
         </ref>
         <ref id="d148e785a1310">
            <mixed-citation id="d148e789" publication-type="other">
Koekemoer, M. 1996. An overview of the Asteraceae of southern
Africa. Pp. 95–110 in: Hind, D.J.N. &amp;amp; Beentje, H.J. (eds.), Proceed-
ings of the International Compositae Conference, Kew, 1994, vol.
1, Compositae: Systematics. Kew: Royal Botanic Gardens.</mixed-citation>
         </ref>
         <ref id="d148e805a1310">
            <mixed-citation id="d148e809" publication-type="other">
Levyns, M.R. 1938. Some evidence bearing on the past history of the
Cape flora. Trans. Roy. Soc. South Africa 26: 404–424.</mixed-citation>
         </ref>
         <ref id="d148e819a1310">
            <mixed-citation id="d148e823" publication-type="other">
Levyns, M.R. 1952. Clues to the past in the Cape flora of today.
S. African J. Sci. 49: 155–164.</mixed-citation>
         </ref>
         <ref id="d148e833a1310">
            <mixed-citation id="d148e837" publication-type="other">
Levyns, M.R. 1964. Presidential address, migrations and origin of the
Cape flora. Trans. Roy. Soc. South Africa 37: 85–107.</mixed-citation>
         </ref>
         <ref id="d148e847a1310">
            <mixed-citation id="d148e851" publication-type="other">
Linder, H.P. 1990. On the relationship between the vegetation and
floras of the Afromontane and the Cape regions of Africa. Mitt.
Inst. Allg. Bot. Hamburg 23b: 777–790.</mixed-citation>
         </ref>
         <ref id="d148e864a1310">
            <mixed-citation id="d148e868" publication-type="other">
Linder, H.P. 1994. Afrotemperate phytogeography: Implications of
cladistic biogeographical analysis. Pp. 913–930 in: Seyani, J.H. &amp;amp;
Chikuni, A.C. (eds.), Proceedings of the 13th Plenary Meeting of
AETFAT, Zomba, Malawi, 2–11 April 1991. Zomba: National Her-
barium and Botanic Gardens of Malawi.</mixed-citation>
         </ref>
         <ref id="d148e888a1310">
            <mixed-citation id="d148e892" publication-type="other">
Linder, H.P. 2003. The radiation of the Cape flora, southern Africa.
Biol. Rev. 78: 597–638.</mixed-citation>
         </ref>
         <ref id="d148e902a1310">
            <mixed-citation id="d148e906" publication-type="other">
Linder, H.P. 2008. Plant species radiations: Where, when, why? Philos.
Trans., Ser. B. 363: 3097–3105.</mixed-citation>
         </ref>
         <ref id="d148e916a1310">
            <mixed-citation id="d148e920" publication-type="other">
Linder, H.P., Meadows, M.E. &amp;amp; Cowling, R.M. 1992. History of the
Cape Flora. Pp. 113–134 in: Cowling, R.M. (ed.), Fynbos: Nutri-
ents, fire and diversity. Cape Town: Oxford Univ. Press.</mixed-citation>
         </ref>
         <ref id="d148e933a1310">
            <mixed-citation id="d148e937" publication-type="other">
Maddison, W. 1989. Reconstructing character evolution on polyto-
mous cladograms. Cladistics 5: 365–377.</mixed-citation>
         </ref>
         <ref id="d148e947a1310">
            <mixed-citation id="d148e951" publication-type="other">
Maddison, W.P. 1997. Gene trees in species trees. Syst. Biol. 46: 523–536.</mixed-citation>
         </ref>
         <ref id="d148e958a1310">
            <mixed-citation id="d148e962" publication-type="other">
Maddison, W.P. &amp;amp; Maddison, D.R. 1992. MacClade: Analysis of
phylogeny and character evolution, version 4.0. Sunderland, Mas-
sachusetts: Sinauer.</mixed-citation>
         </ref>
         <ref id="d148e976a1310">
            <mixed-citation id="d148e980" publication-type="other">
Marlow, J.R., Lange, C.B., Walter, G. &amp;amp; Rosell-Mele, A. 2000. Up-
welling intensification as part of the Pliocene-Pleistocene climate
transition. Science 290: 2288–2291.</mixed-citation>
         </ref>
         <ref id="d148e993a1310">
            <mixed-citation id="d148e997" publication-type="other">
McCracken, K.G. &amp;amp; Sorensen, M.D. 2005. Is homoplasy or lineage
sorting the source of incongruent mtDNA and nuclear gene trees
in the stiff-tailed ducks (Nomonyx oxyura)? Syst. Biol. 54: 35–55.</mixed-citation>
         </ref>
         <ref id="d148e1010a1310">
            <mixed-citation id="d148e1014" publication-type="other">
Moore, M.J. &amp;amp; Jansen, R.K. 2006. Molecular evidence for the age,
origin, and evolutionary history of the American desert plant genus
Tiquilia (Boraginaceae). Molec. Phylog. Evol. 39: 668–687.</mixed-citation>
         </ref>
         <ref id="d148e1027a1310">
            <mixed-citation id="d148e1031" publication-type="other">
Newton, M.A. &amp;amp; Raftery, A.E. 1994. Approximate Bayesian infer-
ence by the weighted likelihood bootstrap (with discussion). J. Roy.
Statist. Soc, Ser. B 56: 3–48.</mixed-citation>
         </ref>
         <ref id="d148e1044a1310">
            <mixed-citation id="d148e1048" publication-type="other">
Nichols, R. 2001. Gene trees and species trees are not the same. Trends
Ecol. Evol. 16: 358–364.</mixed-citation>
         </ref>
         <ref id="d148e1058a1310">
            <mixed-citation id="d148e1062" publication-type="other">
Nordenstam, B. 1968a. The genus Euryops. Part I. Taxonomy. Opera
Bot. 20: 1–409.</mixed-citation>
         </ref>
         <ref id="d148e1073a1310">
            <mixed-citation id="d148e1077" publication-type="other">
Nordenstam, B. 1968b. The genus Euryops. Part II. Aspects of mor-
phology and cytology. Bot. Not. 121: 209–232.</mixed-citation>
         </ref>
         <ref id="d148e1087a1310">
            <mixed-citation id="d148e1091" publication-type="other">
Nordenstam, B. 1969. Phytogeography of the genus Euryops (Com-
positae). A contribution to the phytogeography of southern Africa.
Opera Bot. 23: 1–77.</mixed-citation>
         </ref>
         <ref id="d148e1104a1310">
            <mixed-citation id="d148e1108" publication-type="other">
Nordenstam, B., Clark, V.R., Devos, N. &amp;amp; Barker, N.P. 2009. Two new
species of Euryops (Asteraceae: Senecioneae) from the Sneeuberg,
Eastern Cape Province, South Africa. S. African J. Bot. 75: 144–152.</mixed-citation>
         </ref>
         <ref id="d148e1121a1310">
            <mixed-citation id="d148e1125" publication-type="other">
Nylander, J.A.A. 2004. MrModeltest, version 2. Evolutionary Biology
Centre, Uppsala University: Program distributed by the author.</mixed-citation>
         </ref>
         <ref id="d148e1135a1310">
            <mixed-citation id="d148e1139" publication-type="other">
Nylander, J.A.A., Ronquist, F., Huelsenbeck, J.P. &amp;amp; Nieves-Aldrey,
J.L. 2004. Bayesian phylogenetic analysis of combined data. Syst.
Biol. 53: 47–67.</mixed-citation>
         </ref>
         <ref id="d148e1152a1310">
            <mixed-citation id="d148e1156" publication-type="other">
Oxelman, B., Liden, M. &amp;amp; Berglund, D. 1997. Chloroplast rpsl6
intron phylogeny of the tribe Sileneae (Caryophyllaceae). Pl. Syst.
Evol. 206: 393–410.</mixed-citation>
         </ref>
         <ref id="d148e1170a1310">
            <mixed-citation id="d148e1174" publication-type="other">
Palmer, J.D. 1991. Plastid chromosome: Structure and evolution. Pp.
5–53 in: Bogorad, L. &amp;amp; Vasil, I.K. (eds.), The molecular biology
of plastids. San Diego: Academic Press.</mixed-citation>
         </ref>
         <ref id="d148e1187a1310">
            <mixed-citation id="d148e1191" publication-type="other">
Pamilo, P. &amp;amp; Nei, M. 1988. Relationships between gene trees and
species trees. Molec. Biol. Evol. 5: 568–583.</mixed-citation>
         </ref>
         <ref id="d148e1201a1310">
            <mixed-citation id="d148e1205" publication-type="other">
Pelser, P.B., Nordenstam, B., Kadereit, J.W. &amp;amp; Watson, L.E. 2007.
An ITS phylogeny of tribe Senecioneae (Asteraceae) and a new
delimitation of Senecio L. Taxon 56: 1077–1104.</mixed-citation>
         </ref>
         <ref id="d148e1218a1310">
            <mixed-citation id="d148e1222" publication-type="other">
Rambaut, A. &amp;amp; Drummond, A.J. 2007. Tracer, version 1.4. http://
tree.bio.ed.ac.uk/software/tracer/.</mixed-citation>
         </ref>
         <ref id="d148e1232a1310">
            <mixed-citation id="d148e1236" publication-type="other">
Richardson, J..E, Pennington, R.T., Pennington, T.D. &amp;amp; Holling-
sworth, P.M. 2001a. Rapid diversification of a species-rich genus
of neotropical rain forest trees. Science 293: 2242–2245.</mixed-citation>
         </ref>
         <ref id="d148e1249a1310">
            <mixed-citation id="d148e1253" publication-type="other">
Richardson, J.E., Weitz, F.M., Fay, M.F., Cronk, Q.C.B., Linder,
H.P., Reeves, G. &amp;amp; Chase, M.W. 2001b. Rapid and recent origin
of species richness in the Cape flora of South Africa. Nature 412:
181–183.</mixed-citation>
         </ref>
         <ref id="d148e1270a1310">
            <mixed-citation id="d148e1274" publication-type="other">
Ronquist, F. &amp;amp; Huelsenbeck, J.P. 2003. MrBayes 3: Bayesian phyloge-
netic inference under mixed models. Bioinformatics 19: 1572–1574.</mixed-citation>
         </ref>
         <ref id="d148e1284a1310">
            <mixed-citation id="d148e1288" publication-type="other">
Sang, T., Crawford, D.J., Stuessy, T.F. &amp;amp; Silva-O, M. 1995. ITS
sequences and the phylogeny of the genus Robinsonia (Asteraceae).
Syst. Bot. 20: 55–64.</mixed-citation>
         </ref>
         <ref id="d148e1301a1310">
            <mixed-citation id="d148e1305" publication-type="other">
Seehausen, O. 2004. Hybridization and adaptive radiation. Trends
Ecol. Evol. 19: 198–207.</mixed-citation>
         </ref>
         <ref id="d148e1315a1310">
            <mixed-citation id="d148e1319" publication-type="other">
Shaw, K.L. 2002. Conflict between nuclear and mitochondrial DNA
phylogenies of a recent species radiation: What mtDNA reveals
and conceals about modes of speciation in Hawaiian crickets. Proc.
Natl. Acad. Sci. U.S.A. 99: 16122–16127.</mixed-citation>
         </ref>
         <ref id="d148e1335a1310">
            <mixed-citation id="d148e1339" publication-type="other">
Stanford, A.M., Harden, R. &amp;amp; Parks, C.R. 2000. Phylogeny and
biogeography of Juglans (Juglandaceae) based on matK and ITS
sequence data. Amer. J. Bot. 87: 872–882.</mixed-citation>
         </ref>
         <ref id="d148e1352a1310">
            <mixed-citation id="d148e1356" publication-type="other">
Swofford, D.L. 2002. PAUP*: Phylogenetic analysis using parsimony
(*and other methods), version 4. Sunderland, Massachusetts: Sinauer.</mixed-citation>
         </ref>
         <ref id="d148e1367a1310">
            <mixed-citation id="d148e1371" publication-type="other">
Taberlet, P., Gielly, L., Pautou, G. &amp;amp; Bouvet, J. 1991. Universal prim-
ers for amplification of three non-coding regions of chloroplast
DNA. Pl. Molec. Biol. 17: 1105–1109.</mixed-citation>
         </ref>
         <ref id="d148e1384a1310">
            <mixed-citation id="d148e1388" publication-type="other">
Vargas, P., Baldwin, B.G. &amp;amp; Constance, L. 1998. Nuclear ribosomal
DNA evidence for a western North American origin of Hawaiian
and South American species of Sanicula (Apiaceae). Proc. Natl.
Acad. Sci. U.S.A. 95: 235–240.</mixed-citation>
         </ref>
         <ref id="d148e1404a1310">
            <mixed-citation id="d148e1408" publication-type="other">
Verboom, G.A., Linder, H.P. &amp;amp; Stock, W.D. 2003. Phylogenetics of the
grass genus Ehrharta Thunb.: Evidence for radiation in the summer-
arid zone of the South African Cape. Evolution 57: 1008–1021.</mixed-citation>
         </ref>
         <ref id="d148e1421a1310">
            <mixed-citation id="d148e1425" publication-type="other">
Weimarck, H. 1933. Die Verbreitung einiger afrikanisch-montanen
Pflanzengruppen. I–II. SvenskBoL Tidskr. 27: 400–419.</mixed-citation>
         </ref>
         <ref id="d148e1435a1310">
            <mixed-citation id="d148e1439" publication-type="other">
Weimarck, H. 1936. Die Verbreitung einiger afrikanisch-montanen
Pflanzengruppen. III–IV. SvenskBot. Tidskr. 30: 36–56.</mixed-citation>
         </ref>
         <ref id="d148e1449a1310">
            <mixed-citation id="d148e1453" publication-type="other">
Weimarck, H. 1941. Phytogeographical groups, centres and intervals
within the Cape flora. Lunds Univ. Arsskrift 37: 1–143.</mixed-citation>
         </ref>
         <ref id="d148e1464a1310">
            <mixed-citation id="d148e1468" publication-type="other">
White, F. 1978. The Afromontane region. Pp. 463–513 in: Werger,
M.J.A. (ed.), Biogeography and ecology of southern Africa. The
Hague: Dr. W. Junk.</mixed-citation>
         </ref>
         <ref id="d148e1481a1310">
            <mixed-citation id="d148e1485" publication-type="other">
White, T.J., Bruns, T., Lee, S. &amp;amp; Taylor, J. 1990. Amplification
and direct sequencing of fungal ribosomal RNA genes for phy-
logenetics. Pp. 315–324 in: Innis, M.A., Gelfand, D.H., Sninsky,
J.J. &amp;amp; White, T.J. (eds.), PCR protocols: A guide to methods and
applications. San Diego: Academic Press.</mixed-citation>
         </ref>
         <ref id="d148e1504a1310">
            <mixed-citation id="d148e1508" publication-type="other">
Wild, H. 1968. Phytogeography in South Central Africa. Kirkia 6:
197–222.</mixed-citation>
         </ref>
         <ref id="d148e1518a1310">
            <mixed-citation id="d148e1522" publication-type="other">
Yoder, A.D., Irwin, J.A. &amp;amp; Payseur, B.A. 2001. Failure of the ILD
to determine data combinability for slow loris phylogeny. Syst.
Biol. 50: 408–424.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
