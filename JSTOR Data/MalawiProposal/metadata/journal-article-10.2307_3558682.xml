<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">procbiolscie</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100837</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Proceedings: Biological Sciences</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Royal Society</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">09628452</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">3558682</article-id>
         <title-group>
            <article-title>Sex-Biased Dispersal in a Salmonid Fish</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jeffrey A.</given-names>
                  <surname>Hutchings</surname>
               </string-name>
            </contrib>
            <contrib contrib-type="author">
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Leah</given-names>
                  <surname>Gerber</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>07</day>
            <month>12</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">269</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1508</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i369459</issue-id>
         <fpage>2487</fpage>
         <lpage>2493</lpage>
         <page-range>2487-2493</page-range>
         <permissions>
            <copyright-statement>Copyright 2002 The Royal Society</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/3558682"/>
         <abstract>
            <p>We tested the hypothesis that dispersal is sex biased in an unexploited population of brook trout, Salvelinus fontinalis, on Cape Race, Newfoundland, Canada. Based on the assumptions that trout are promiscuous and that reproductive success is limited primarily by either number of mates (males) or fecundity (females), we predicted that males would disperse greater distances than females. We also tested the hypothesis that trout populations comprise stationary and mobile individuals, predicting that males have greater mobility than females. Based on a mark-recapture study of 943 individually tagged fishes, 191 of which were recaptured over 5 years, we find strong support for our hypothesis of male-biased dispersal in brook trout. Averaged among all 11 resampling periods, males dispersed 2.5 times as far as females; during the spawning period only, male dispersal exceeded that by females almost fourfold. Both sexes were heterogeneous with respect to movement, with a lower incidence of mobility among females (29.6%) than males (41.1%); mobile males dispersed six times further than mobile females. We conclude that this sex bias reduces mate competition among male kin and decreases the probability that males will reproduce with related females.</p>
         </abstract>
         <kwd-group>
            <kwd>Sex-Biased Dispersal</kwd>
            <kwd>Brook Trout</kwd>
            <kwd>Stationary and Mobile Individuals</kwd>
            <kwd>Newfoundland</kwd>
            <kwd>Salvelinus fontinalis</kwd>
         </kwd-group>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d701e232a1310">
            <mixed-citation id="d701e236" publication-type="journal">
Attwood, C. G. &amp; Bennett, B. A. 1994 Variation in dispersal
of galjoen (Coracinus capensis) (Teleostei, Coracinidae) from
a marine reserve. Can. J. Fish. Aquat. Sci. 51, 1247-1257.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Attwood</surname>
                  </string-name>
               </person-group>
               <fpage>1247</fpage>
               <volume>51</volume>
               <source>Can. J. Fish. Aquat. Sci.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d701e271a1310">
            <mixed-citation id="d701e275" publication-type="journal">
Biro, P. A. &amp; Ridgway, M. S. 1995 Individual variation in for-
aging movements in a lake population of young-of-the-year
brook charr (Salvelinus fontinalis). Behaviour 132, 57-74.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Biro</surname>
                  </string-name>
               </person-group>
               <fpage>57</fpage>
               <volume>132</volume>
               <source>Behaviour</source>
               <year>1995</year>
            </mixed-citation>
         </ref>
         <ref id="d701e310a1310">
            <mixed-citation id="d701e316" publication-type="journal">
Blundell, G. M., Ben-David, M., Groves, P., Bowyers, R. T. &amp;
Geffen, E. 2002 Characteristics of sex-biased dispersal in
coastal river otters: implications for natural recolonization of
extirpated populations. Mol. Ecol. 11, 289-303.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Blundell</surname>
                  </string-name>
               </person-group>
               <fpage>289</fpage>
               <volume>11</volume>
               <source>Mol. Ecol.</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d701e354a1310">
            <mixed-citation id="d701e358" publication-type="book">
Burnham, K. P. &amp; Anderson, D. R. 1998 Model selection and
inference. New York: Springer.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Burnham</surname>
                  </string-name>
               </person-group>
               <source>Model selection and inference</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d701e384a1310">
            <mixed-citation id="d701e388" publication-type="journal">
Clarke, A. B. 1978 Sex ratio and local resource competition in
a prosimian primate. Science 201, 163-165.<object-id pub-id-type="jstor">10.2307/1746703</object-id>
               <fpage>163</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e404a1310">
            <mixed-citation id="d701e408" publication-type="book">
COSEWIC (Committee on the Status of Endangered Wildlife
in Canada) 2002 Canadian species at risk. Ottawa: Canadian
Wildlife Service, Environment Canada.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>COSEWIC (Committee on the Status of Endangered Wildlife in Canada)</surname>
                  </string-name>
               </person-group>
               <source>Canadian species at risk</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d701e437a1310">
            <mixed-citation id="d701e441" publication-type="journal">
Dobson, F. S. 1982 Competition for mates and predominant
juvenile dispersal in mammals. Anim. Behav. 30, 1183-
1192.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Dobson</surname>
                  </string-name>
               </person-group>
               <fpage>1183</fpage>
               <volume>30</volume>
               <source>Anim. Behav.</source>
               <year>1982</year>
            </mixed-citation>
         </ref>
         <ref id="d701e476a1310">
            <mixed-citation id="d701e480" publication-type="journal">
Ferguson, M. M., Danzmann, R. G. &amp; Hutchings, J. A. 1991
Incongruent estimates of population differentiation among
brook charr, Salvelinus fontinalis, from Cape Race, New-
foundland, Canada, based upon allozyme and mitochondrial
DNA variation. J. Fish Biol. 39, 79-85.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Ferguson</surname>
                  </string-name>
               </person-group>
               <fpage>79</fpage>
               <volume>39</volume>
               <source>J. Fish Biol.</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d701e521a1310">
            <mixed-citation id="d701e525" publication-type="journal">
Fleming, I. A. 1998 Pattern and variability in the breeding sys-
tem of Atlantic salmon (Salmo salar) comparisons to other
salmonids. Can. J. Fish. Aquat. Sci. 55(Suppl. A), 59-76.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Fleming</surname>
                  </string-name>
               </person-group>
               <fpage>59</fpage>
               <volume>55</volume>
               <source>Can. J. Fish. Aquat. Sci.</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d701e560a1310">
            <mixed-citation id="d701e564" publication-type="journal">
Garant, D., Dodson, J. J. &amp; Bernatchez, L. 2001 A genetic
evaluation of mating system and determinants of individuals
reproductive success in Atlantic salmon (Salmo salar L.).
J. Hered. 92, 137-145.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Garant</surname>
                  </string-name>
               </person-group>
               <fpage>137</fpage>
               <volume>92</volume>
               <source>J. Hered</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d701e603a1310">
            <mixed-citation id="d701e607" publication-type="journal">
Gilliam, J. F. &amp; Fraser, D. F. 2001 Movement in corridors:
enhancement by predation threat, disturbance, and habitat
structure. Ecology 82, 258-273.<object-id pub-id-type="doi">10.2307/2680101</object-id>
               <fpage>258</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e626a1310">
            <mixed-citation id="d701e630" publication-type="journal">
Greenwood, P. J. 1980 Mating systems, philopatry and disper-
sal in birds and mammals. Anim. Behav. 28, 1140-1162.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Greenwood</surname>
                  </string-name>
               </person-group>
               <fpage>1140</fpage>
               <volume>28</volume>
               <source>Anim. Behav.</source>
               <year>1980</year>
            </mixed-citation>
         </ref>
         <ref id="d701e662a1310">
            <mixed-citation id="d701e666" publication-type="book">
Greenwood, P. J. 1983 Mating systems and the evolutionary
consequences of dispersal. In The ecology of animal movement
(ed. I. R. Swingland &amp; P. J. Greenwood), pp. 116-131.
Oxford: Clarendon<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Greenwood</surname>
                  </string-name>
               </person-group>
               <comment content-type="section">Mating systems and the evolutionary consequences of dispersal</comment>
               <fpage>116</fpage>
               <source>In The ecology of animal movement</source>
               <year>1983</year>
            </mixed-citation>
         </ref>
         <ref id="d701e704a1310">
            <mixed-citation id="d701e708" publication-type="journal">
Greenwood, P. J. &amp; Harvey, P. H. 1982 The natal and breed-
ing dispersal of birds. A. Rev. Ecol. Syst. 13, 1-21.<object-id pub-id-type="jstor">10.2307/2097060</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e724a1310">
            <mixed-citation id="d701e728" publication-type="journal">
Griffiths, S. W. &amp; Magurran, A. E. 1998 Sex and schooling
behaviour in the Trinidadian guppy. Anim. Behav. 56,
689-693.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Griffiths</surname>
                  </string-name>
               </person-group>
               <fpage>689</fpage>
               <volume>56</volume>
               <source>Anim. Behav.</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d701e763a1310">
            <mixed-citation id="d701e767" publication-type="journal">
Hamilton, W. D. 1967 Extraordinary sex ratios. Science 156,
477-488.<object-id pub-id-type="jstor">10.2307/1721222</object-id>
               <fpage>477</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e784a1310">
            <mixed-citation id="d701e788" publication-type="journal">
Hard, J. J. &amp; Heard, W. R. 1999 Analysis of straying variation
in Alaskan hatchery chinook salmon (Oncorhynchus
tshawytscha) following transplantation. Can. J. Fish. Aquat.
Sci. 56, 578-589.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hard</surname>
                  </string-name>
               </person-group>
               <fpage>578</fpage>
               <volume>56</volume>
               <source>Can. J. Fish. Aquat. Sci.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d701e826a1310">
            <mixed-citation id="d701e830" publication-type="journal">
Holtby, L. B. &amp; Healey, M. C. 1990 Sex-specific life history
tactics and risk-taking in coho salmon. Ecology 71, 678-690.<object-id pub-id-type="doi">10.2307/1940322</object-id>
               <fpage>678</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e846a1310">
            <mixed-citation id="d701e850" publication-type="journal">
Howard, W. E. 1960 Innate and environmental dispersal of
individual vertebrates. Am. Midl. Nat. 63, 152-161.<object-id pub-id-type="doi">10.2307/2422936</object-id>
               <fpage>152</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e866a1310">
            <mixed-citation id="d701e870" publication-type="journal">
Hurvich, C. M. &amp; Tsai, C.-L. 1989 Regression and time series
model selection in small samples. Biometrika 76, 297-307.<object-id pub-id-type="doi">10.2307/2336663</object-id>
               <fpage>297</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e886a1310">
            <mixed-citation id="d701e890" publication-type="journal">
Hutchings, J. A. 1991 Fitness consequences of variation in egg
size and food abundance in brook trout, Salvelinus fontinalis.
Evolution 45, 1162-1168.<object-id pub-id-type="doi">10.2307/2409723</object-id>
               <fpage>1162</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e909a1310">
            <mixed-citation id="d701e913" publication-type="journal">
Hutchings, J. A. 1993 Adaptive life histories effected by age-
specific survival and growth rate. Ecology 74, 673-684.<object-id pub-id-type="doi">10.2307/1940795</object-id>
               <fpage>673</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e930a1310">
            <mixed-citation id="d701e934" publication-type="journal">
Hutchings, J. A. 1994 Age- and size-specific costs of repro-
duction within populations of brook trout, Salvelinus
fontinalis. Oikos 70, 12-20.<object-id pub-id-type="doi">10.2307/3545693</object-id>
               <fpage>12</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e953a1310">
            <mixed-citation id="d701e957" publication-type="journal">
Hutchings, J. A. 1996 Adaptive phenotypic plasticity in brook
trout, Salvelinus fontinalis, life histories. Ecoscience 3, 25-32.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Hutchings</surname>
                  </string-name>
               </person-group>
               <fpage>25</fpage>
               <volume>3</volume>
               <source>Ecoscience</source>
               <year>1996</year>
            </mixed-citation>
         </ref>
         <ref id="d701e989a1310">
            <mixed-citation id="d701e993" publication-type="journal">
Johnson, M. L. &amp; Gaines, M. S. 1990 Evolution of dispersal:
theoretical models and empirical tests using birds and mam-
mals. A. Rev. Ecol. Syst. 21, 449-480.<object-id pub-id-type="jstor">10.2307/2097033</object-id>
               <fpage>449</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e1012a1310">
            <mixed-citation id="d701e1016" publication-type="journal">
Jones, M. W. &amp; Hutchings, J. A. 2002 Individual variation in
Atlantic salmon fertilization success: implications for effec-
tive population size. Ecol. Appl. 12, 184-193.<object-id pub-id-type="doi">10.2307/3061145</object-id>
               <fpage>184</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e1035a1310">
            <mixed-citation id="d701e1041" publication-type="journal">
Knight, M. E., Van Oppen, M. J. H., Smith, H. L., Rico, C.,
Hewitt, G. M. &amp; Turner, G. F. 1999 Evidence of male-
biased dispersal in Lake Malawi cichlids from microsatel-
lites. Mol. Ecol. 8, 1521-1527.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Knight</surname>
                  </string-name>
               </person-group>
               <fpage>1521</fpage>
               <volume>8</volume>
               <source>Mol. Ecol.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1079a1310">
            <mixed-citation id="d701e1083" publication-type="journal">
McLaughlin, R. L., Grant, J. W. A. &amp; Kramer, D. L. 1994
Foraging movements in relation to morphology, water col-
umn-use, and diet for recently emerged brook trout
(Salvelinus fontinalis) in still-water pools. Can. J. Fish. Aquat.
Sci. 51, 268-279.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>McLaughlin</surname>
                  </string-name>
               </person-group>
               <fpage>268</fpage>
               <volume>51</volume>
               <source>Can. J. Fish. Aquat. Sci.</source>
               <year>1994</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1125a1310">
            <mixed-citation id="d701e1129" publication-type="journal">
McLaughlin, R. L., Ferguson, M. M. &amp; Noakes, D. L. 1999
Adaptive peaks and alternative foraging tactics in brook
charr: evidence of short-term divergent selection for sitting-
and-waiting and actively searching. Behav. Ecol. Sociobiol.
45, 386-395.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>McLaughlin</surname>
                  </string-name>
               </person-group>
               <fpage>386</fpage>
               <volume>45</volume>
               <source>Behav. Ecol. Sociobiol.</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1170a1310">
            <mixed-citation id="d701e1174" publication-type="book">
Manly, B. F. J. 1991 Randomization and Monte Carlo methods
in biology. London: Chapman &amp; Hall.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Manly</surname>
                  </string-name>
               </person-group>
               <source>Randomization and Monte Carlo methods in biology</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1199a1310">
            <mixed-citation id="d701e1203" publication-type="journal">
Marschall, E. A., Quinn, T. P., Roff, D. A., Hutchings, J. A.,
Metcalfe, N. B., Bakke, T. A., Saunders, R. L. &amp; Poff, N. L.
1998 A framework for understanding Atlantic salmon (Salmo
salar) life history. Can. J. Fish. Aquat. Sci. 55(Suppl. A),
48-58.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Marschall</surname>
                  </string-name>
               </person-group>
               <fpage>48</fpage>
               <volume>55</volume>
               <source>Can. J. Fish. Aquat. Sci.</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1244a1310">
            <mixed-citation id="d701e1248" publication-type="journal">
Nehlsen, W., Williams, J. E. &amp; Lichatowich, J. A. 1991 Pacific
salmon at the crossroads at risk from California, Oregon,
Idaho, and Washington. Fisheries 16, 4-21.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Nehlsen</surname>
                  </string-name>
               </person-group>
               <fpage>4</fpage>
               <volume>16</volume>
               <source>Fisheries</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1283a1310">
            <mixed-citation id="d701e1287" publication-type="book">
NRC (National Research Council) 2002 Genetic status of Atlan-
tic salmon in Maine: interim report. Washington, DC: National
Academy of Sciences.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>NRC (National Research Council)</surname>
                  </string-name>
               </person-group>
               <source>Genetic status of Atlantic salmon in Maine: interim report</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1316a1310">
            <mixed-citation id="d701e1320" publication-type="journal">
Pardini, A. T. (and 11 others) 2001 Sex-biased dispersal of
great white sharks. Nature 412, 139-140.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pardini</surname>
                  </string-name>
               </person-group>
               <fpage>139</fpage>
               <volume>412</volume>
               <source>Nature</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1353a1310">
            <mixed-citation id="d701e1357" publication-type="journal">
Perrin, N. &amp; Mazalov, V. 1999 Dispersal and inbreeding avoid-
ance. Am. Nat. 154, 282-292.<object-id pub-id-type="jstor">10.2307/2463651</object-id>
               <fpage>282</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e1373a1310">
            <mixed-citation id="d701e1377" publication-type="journal">
Perrin, N. &amp; Mazalov, V. 2000 Local competition, inbreeding,
and the evolution of sex-biased dispersal. Am. Nat. 155,
116-127.<object-id pub-id-type="jstor">10.2307/3079020</object-id>
               <fpage>116</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e1396a1310">
            <mixed-citation id="d701e1400" publication-type="journal">
Pusey, A. E. 1987 Sex-biased dispersal and inbreeding avoid-
ance in birds and mammals. Trends Ecol. Evol. 2, 295-299.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Pusey</surname>
                  </string-name>
               </person-group>
               <fpage>295</fpage>
               <volume>2</volume>
               <source>Trends Ecol. Evol.</source>
               <year>1987</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1432a1310">
            <mixed-citation id="d701e1436" publication-type="journal">
Rodriguez, M. A. 2002 Restricted movement in stream fish:
the paradigm is incomplete, not lost. Ecology 83, 1-13.<object-id pub-id-type="doi">10.2307/2680115</object-id>
               <fpage>1</fpage>
            </mixed-citation>
         </ref>
         <ref id="d701e1452a1310">
            <mixed-citation id="d701e1456" publication-type="book">
Roff, D.A. 2002 Life history evolution. Sunderland, MA:
Sinauer Associates.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Roff</surname>
                  </string-name>
               </person-group>
               <source>Life history evolution</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1481a1310">
            <mixed-citation id="d701e1485" publication-type="book">
Stearns, S. C. 1992 The evolution of life histories. Oxford Univer-
sity Press.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Stearns</surname>
                  </string-name>
               </person-group>
               <source>The evolution of life histories</source>
               <year>1992</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1511a1310">
            <mixed-citation id="d701e1515" publication-type="journal">
Thedinga, J. F., Wertheimer, A. C., Heintz, R. A., Maselko,
J. M. &amp; Rice, S. D. 2000 Effects of stock, coded-wire tag-
ging, and transplant on straying of pink salmon
(Oncorhynchus gorbuscha) in southeastern Alaska. Can. J.
Fish. Aquat. Sci. 57, 2076-2085.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Thedinga</surname>
                  </string-name>
               </person-group>
               <fpage>2076</fpage>
               <volume>57</volume>
               <source>Can. J. Fish. Aquat. Sci.</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1556a1310">
            <mixed-citation id="d701e1560" publication-type="book">
Venables, W. N. &amp; Ripley, B. D. 1998 Modern applied statistics
with S-PLUS. New York: Springer.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Venables</surname>
                  </string-name>
               </person-group>
               <source>Modern applied statistics with S-Plus</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d701e1585a1310">
            <mixed-citation id="d701e1589" publication-type="book">
Wootton, R. J. 1998 The ecology of teleost fishes. New York:
Chapman &amp; Hall.<person-group>
                  <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                     <surname>Wootton</surname>
                  </string-name>
               </person-group>
               <source>The ecology of teleost fishes</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
