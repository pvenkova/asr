<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">revuetiersmonde</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50008830</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Revue Tiers Monde</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>l'Institut d'Étude du Développement économique et social</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">12938882</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19631359</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">23593770</article-id>
         <article-categories>
            <subj-group>
               <subject>DOSSIER: LA MESURE DU DEVELOPPEMENT</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>LES ONG ET LA MESURE DU DÉVELOPPEMENT : ENTRE PERFORMANCE ET COMMUNICATION</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Marc-Antoine Pérouse</given-names>
                  <surname>de Montclos</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>1</month>
            <year>2013</year>
      
            <day>1</day>
            <month>3</month>
            <year>2013</year>
         </pub-date>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">213</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i23593764</issue-id>
         <fpage>71</fpage>
         <lpage>86</lpage>
         <permissions>
            <copyright-statement>© Armand Colin/Université</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/23593770"/>
         <abstract>
            <p>Les ONG et les bailleurs de fonds de la coopération internationale pour le développement utilisent couramment des outils quantitatifs pour mesurer la pauvreté et l'impact de leurs programmes. Le procédé puise ses racines chez les économistes et les militaires qui mettent en place l'architecture moderne de l'aide au sortir de la seconde Guerre mondiale, mais il s'impose avec plus de vigueur au cours des années 1980 lorsque le triomphe du modèle libéral oblige les opérateurs à adopter une démarche entrepreneuriale et à démontrer leur performance avec des arguments financiers. Aussi convient-il d'engager une réflexion plus approfondie sur le « fétichisme » des chiffres et la standardisation des procédures d'aide, en parallèle avec la montée en puissance des ONG, du « tout-humanitaire » et du marketing compassionnel. NGOs and institutional funders currently use quantitative tools to measure poverty and the impact of their development programmes. Such a statistical evaluation started with economists and the military at the end of the Second World War, but it became a paradigm in the 1980s, when the operators of international aid had to follow a liberal model to demonstrate their performance. This article thus introduces a general discussion on the standardisation and the (mis)use of numbers to evaluate NGOs in a context where humanitarian rhetoric is often confused with marketing and fundraising. Las ONG y los organismos financieros de la cooperación internacional para el desarrollo suelen emplear herramientas cuantitativas para medir la pobreza y el impacto de sus programas. El proceso hunde sus raíces en la arquitectura moderna de la ayuda desarrollada por economistas y militares al finalizar la segunda guerra mundial, pero se impone con más vigor en el transcurso de los años 80 cuando el triunfo del modelo liberal obliga a los operadores a adoptar un modelo empresario de gestión y a justificar su eficiencia con argumentos financieros. Por ello hace falta emprender una reflexión profunda sobre el "fetichismo" de las cifras y la estandarización de los procedimientos de ayuda, paralelamente al fortalecimiento de las ONG, del "todo-humanitario" y del márketing compasivo.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>fre</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d855e155a1310">
            <label>1</label>
            <p>
               <mixed-citation id="d855e162" publication-type="other">
Bonnecase, 2011.</mixed-citation>
            </p>
         </fn>
         <fn id="d855e169a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d855e176" publication-type="other">
Riddell (2007, p. 248)</mixed-citation>
            </p>
         </fn>
         <fn id="d855e183a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d855e190" publication-type="other">
Luetchford et Burns (2003)</mixed-citation>
            </p>
         </fn>
         <fn id="d855e197a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d855e204" publication-type="other">
Pérouse de Montclos (2009)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d855e210" publication-type="other">
Sogge (2003).</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d855e216" publication-type="other">
Siméant (2005)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d855e223" publication-type="other">
Blood (2005).</mixed-citation>
            </p>
         </fn>
         <fn id="d855e231a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d855e238" publication-type="other">
Murungi (2008, p. 38)</mixed-citation>
            </p>
         </fn>
         <fn id="d855e245a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d855e252" publication-type="other">
Hansch etalii, 1994, pp. 25, 30</mixed-citation>
            </p>
         </fn>
         <fn id="d855e259a1310">
            <label>7</label>
            <p>
               <mixed-citation id="d855e266" publication-type="other">
Cavelier et alii, 2007,</mixed-citation>
            </p>
         </fn>
         <fn id="d855e273a1310">
            <label>8</label>
            <p>
               <mixed-citation id="d855e280" publication-type="other">
Small, Loewenstein et Slovic, 2007.</mixed-citation>
            </p>
         </fn>
         <fn id="d855e287a1310">
            <label>9</label>
            <p>
               <mixed-citation id="d855e294" publication-type="other">
http://www.charitynavigator.org/</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d855e300" publication-type="other">
http://www.bbb.org/us/charity/</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d855e306" publication-type="other">
http://www2.guidestar.org/</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>BIBLIOGRAPHIE</title>
         <ref id="d855e322a1310">
            <mixed-citation id="d855e326" publication-type="other">
Alexander R., 2006, Tsunami: Build Back Better. Man-
tra Aside, An Aid Gone Wrong Story?, Bangalore,
Development Consultancy Group.</mixed-citation>
         </ref>
         <ref id="d855e339a1310">
            <mixed-citation id="d855e343" publication-type="other">
Arts B., 1998, The Political Influence of Global NGOs:
Case Studies on the Climate and Biodiversity
Conventions, Utrecht, International Books.</mixed-citation>
         </ref>
         <ref id="d855e356a1310">
            <mixed-citation id="d855e360" publication-type="other">
Beck B., 2006, Evaluating Humanitarian Action Using
the OECD-DAC Criteria, Londres, Overseas Deve-
lopment Institute.</mixed-citation>
         </ref>
         <ref id="d855e373a1310">
            <mixed-citation id="d855e377" publication-type="other">
Berlage L., Stokke 0. (dir.), 1992, Evaluating Deve-
lopment Assistance: Approaches and Methods,
Londres, Frank Cass.</mixed-citation>
         </ref>
         <ref id="d855e391a1310">
            <mixed-citation id="d855e395" publication-type="other">
Blodgett Bermeo S., 2006, « Donors and Develop-
ment: The Use of Sector Allocation as a Tool in
Foreign Aid Policy », San Diego, Annual Metting
of the International Studies Association, polyco-
pié, 36 p.</mixed-citation>
         </ref>
         <ref id="d855e414a1310">
            <mixed-citation id="d855e418" publication-type="other">
Blood R., 2005, « Should NGOs Be Viewed as "Politi-
cal Corporations"? », Journal of Communication
Management, vol. 9, n° 2, pp. 120-133.</mixed-citation>
         </ref>
         <ref id="d855e431a1310">
            <mixed-citation id="d855e435" publication-type="other">
Bolton G., 2008, Aid and Other Dirty Business: How
Good Intentions Have Failed the World's Poor,
Londres, Ebury.</mixed-citation>
         </ref>
         <ref id="d855e448a1310">
            <mixed-citation id="d855e452" publication-type="other">
Bornstein L., 2006, « Systems of Accountability,
Webs of Deceit? Monitoring and Evaluation in
South African NGOs », Development, vol. 49,
n° 2, pp. 52-61.</mixed-citation>
         </ref>
         <ref id="d855e468a1310">
            <mixed-citation id="d855e472" publication-type="other">
Bonnecase V., 2011, La pauvreté au SäheI : du savoir
colonial à la mesure internationale, Paris, Kar-
thala.</mixed-citation>
         </ref>
         <ref id="d855e485a1310">
            <mixed-citation id="d855e489" publication-type="other">
Bradshaw Y., Schäfer M., 2000, « Urbanization and
Development: The Emergence of International
Nongovernmental Organizations Amid Declining
States », Sociological Perspectives, vol. 43,
n° 1, pp. 97-116.</mixed-citation>
         </ref>
         <ref id="d855e509a1310">
            <mixed-citation id="d855e513" publication-type="other">
Cavelier 8., du Buysson A., Fandre C., Réquéna R.,
Ruleta M., Voizot D., 2007, Guide de l'évaluation
2007, Nouvelle édition complétée et révisée,
Paris, Ministère des Affaires étrangères, 109 p.</mixed-citation>
         </ref>
         <ref id="d855e529a1310">
            <mixed-citation id="d855e533" publication-type="other">
Clark J., 2003, Worlds Apart: Civil Society and the
Battle for Ethical Globalization, Londres, Earths-
can.</mixed-citation>
         </ref>
         <ref id="d855e546a1310">
            <mixed-citation id="d855e550" publication-type="other">
Cracknell B. E., 2000, Evaluating Development Aid,
Londres, Sage Publications.</mixed-citation>
         </ref>
         <ref id="d855e560a1310">
            <mixed-citation id="d855e564" publication-type="other">
Easterly W., 2002, « The Cartel of Good Intentions:
The Problem of Bureaucracy in Foreign Aid », The
Journal of Policy Reform, vol. 5, n° 4, pp. 223-
250.</mixed-citation>
         </ref>
         <ref id="d855e580a1310">
            <mixed-citation id="d855e584" publication-type="other">
Ebrahim A., 2003, NGOs and Organizational Change:
Discourse, Reporting and Learning, Cambridge,
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d855e597a1310">
            <mixed-citation id="d855e601" publication-type="other">
Fuertes-Fuertes l„ Maset-Llaudes A., 2007, « Explo-
ring Spanish Nongovernmental Organizations for
Development: An Empirical Approach », Nonpro-
fit and Voluntary Sector Quarterly, vol. 36, n° 4,
pp. 695-706.</mixed-citation>
         </ref>
         <ref id="d855e621a1310">
            <mixed-citation id="d855e625" publication-type="other">
Gauri V., Galef J., 2005, « NGOs in Bangladesh: Acti-
vities, Resources, and Governance », World Deve-
lopment, vol. 33, n° 12, pp. 2045-2065.</mixed-citation>
         </ref>
         <ref id="d855e638a1310">
            <mixed-citation id="d855e642" publication-type="other">
GiveWell, 2007, The Case for the Clear Fund, New
York, GiveWell, 67 p.</mixed-citation>
         </ref>
         <ref id="d855e652a1310">
            <mixed-citation id="d855e656" publication-type="other">
Greenlee J., Fischer M., Gordon T., Keating E., 2007,
« An Investigation of Fraud in Nonprofit Organiza-
tions: Occurrences and Deterrents », Nonprofit
and Voluntary Sector Quarterly, vol. 36, n° 4,
pp. 676-694.</mixed-citation>
         </ref>
         <ref id="d855e675a1310">
            <mixed-citation id="d855e679" publication-type="other">
Griekspoor A., Collins S., 2001, « Raising Standards
in Emergency Relief: Flow Useful Are Sphere Mini-
mum Standards for Humanitarian Assistance? »,
British Medical Journal, vol. 323, 29 septembre,
pp. 740-742.</mixed-citation>
         </ref>
         <ref id="d855e698a1310">
            <mixed-citation id="d855e702" publication-type="other">
Grünewald F., 2000, « L'argent, l'urgence et
la reconstruction», Mouvements, n° 12,
novembre.</mixed-citation>
         </ref>
         <ref id="d855e715a1310">
            <mixed-citation id="d855e719" publication-type="other">
Hanlon H., 1991, Mozambique: Who Calls the Shots?,
Londres, James Currey.</mixed-citation>
         </ref>
         <ref id="d855e730a1310">
            <mixed-citation id="d855e736" publication-type="other">
Hansch S., Lillibridge S., Egeland G., Teller C.,
Toole M., 1994, Lives Lost, Lives Saved: Excess
Mortality and the Impact of Health Interventions
in the Somalia Emergency Washington D. C.,
Refugee Policy Group.</mixed-citation>
         </ref>
         <ref id="d855e755a1310">
            <mixed-citation id="d855e759" publication-type="other">
Hansen H., Tarp F., 2000, « Aid Effectiveness Dis-
puted » in Tarp F. (dir.), Foreign Aid and Deve-
lopment: Lessons Learnt and Directions for the
Future, Londres, Routledge, pp. 78-98.</mixed-citation>
         </ref>
         <ref id="d855e775a1310">
            <mixed-citation id="d855e779" publication-type="other">
Krueger A., Michaelopoulos C., Ruttan V. (dir.), 1989,
Aid and Development, Baltimore, Johns Hopkins
University Press.</mixed-citation>
         </ref>
         <ref id="d855e792a1310">
            <mixed-citation id="d855e796" publication-type="other">
Lappé F. M., Collins J., Kinley D., 1980, Aidas Obs-
tacle: Twenty Questions about our Foreign Aid
and the Hungry, San Francisco, Institute for Food
and Development Policy.</mixed-citation>
         </ref>
         <ref id="d855e812a1310">
            <mixed-citation id="d855e816" publication-type="other">
Luetchford M., Burns P., 2003, Waging the War on
Want: 50 Years of Campaigning Against World
Poverty: An Authorised History, Londres, War on
Want, 192 p.</mixed-citation>
         </ref>
         <ref id="d855e832a1310">
            <mixed-citation id="d855e836" publication-type="other">
Mercier M., 2004, Le Comité international de la Croix-
Rouge : l'action humanitaire dans le nouveau
contexte mondial, Lausanne, Presses polytech-
niques et universitaires romandes.</mixed-citation>
         </ref>
         <ref id="d855e853a1310">
            <mixed-citation id="d855e857" publication-type="other">
Murungi B. K., 2008, « To Whom, for What, and
About What? The Legitimacy of Human Rights
NGOs in Kenya » //rMutua M. (dir.), Human Rights
Ngos In East Africa: Political and Normative Ten-
sions, Philadelphie, University Of Pennsylvania
Press, pp. 37-49.</mixed-citation>
         </ref>
         <ref id="d855e880a1310">
            <mixed-citation id="d855e884" publication-type="other">
Oakley P., Bonde B., Dahl-0stergaard T., Herrera R.,
Lacayo N., Lipson B., Martinez J., Odum R.,
2001, Evaluation of the Public Support to the
Norwegian NGOs Working in Nicaragua 1994-
1999, Oslo, Ministry of Foreign Affairs.</mixed-citation>
         </ref>
         <ref id="d855e903a1310">
            <mixed-citation id="d855e907" publication-type="other">
Perouse de Montclos M.-A., 2000, « Villes en guerre
en Somalie : Mogadiscio et Hargeisa », Les dos-
siers du Ceped, n° 59, avril, 65 p.</mixed-citation>
         </ref>
         <ref id="d855e920a1310">
            <mixed-citation id="d855e924" publication-type="other">
Pérouse de Montclos M.-A., 2005, « Les ONG sur la
sellette », Études, n° 4036, décembre, pp. 607-
616.</mixed-citation>
         </ref>
         <ref id="d855e937a1310">
            <mixed-citation id="d855e941" publication-type="other">
Pérouse de Montclos M.-A., 2009, « Du développe-
ment à l'humanitaire, ou le triomphe de la corn' »,
Revue Tiers Monde, n° 200, pp. 751 -766.</mixed-citation>
         </ref>
         <ref id="d855e954a1310">
            <mixed-citation id="d855e958" publication-type="other">
Pérouse de Montclos M.-A., 2010, « Notation des
ONG et évaluation des Objectifs du Millénaire
pour le Développement : quelques pistes de
réflexions », Statéco, n° 105, pp. 49-60.</mixed-citation>
         </ref>
         <ref id="d855e975a1310">
            <mixed-citation id="d855e979" publication-type="other">
Perouse de Montclos M.-A., 2012, « Humanitarian
Action in Developing Countries: Who Evaluates
Who? », Evaluation and Program Planning,
vol. 35, n° 1, pp. 154-160.</mixed-citation>
         </ref>
         <ref id="d855e995a1310">
            <mixed-citation id="d855e999" publication-type="other">
Porter T., 1995, Trust in Numbers: The Pursuit of
Objectivity in Science and Public Life, New Jersey,
Princeton University Press.</mixed-citation>
         </ref>
         <ref id="d855e1012a1310">
            <mixed-citation id="d855e1016" publication-type="other">
Ridded R., 2007, Does Foreign Aid Really Work?,
Oxford, Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d855e1026a1310">
            <mixed-citation id="d855e1030" publication-type="other">
Sachs J., 2005, The End of Poverty: Economic Pos-
sibilities for our Time, New York, Penguin Press.</mixed-citation>
         </ref>
         <ref id="d855e1040a1310">
            <mixed-citation id="d855e1044" publication-type="other">
Siméant J., 2005, « What Is Going Global? The Inter-
nationalization of French NGOs "Without Bor-
ders" », Review of International Political Economy,
vol. 12, n° 5, pp. 851-883.</mixed-citation>
         </ref>
         <ref id="d855e1060a1310">
            <mixed-citation id="d855e1064" publication-type="other">
Similon A., 2009, « La concurrence : source de non-
coordination entre ONG du Nord? » in Rémon M.
(dir.), ONG et acteurs locaux : l'ultime alterna-
tive? Les limites du modèle participatif au Sud
et la concurrence des ONG dans le Nord, Namur,
Presses universitaires de Namur, pp. 83-105.</mixed-citation>
         </ref>
         <ref id="d855e1088a1310">
            <mixed-citation id="d855e1092" publication-type="other">
Small D„ Loewenstein G., Slovic P., 2007, « Sym-
pathy and Callousness: The Impact of Deli-
berative Thought on Donations to Identifiable
and Statistical Victims », Organizational Behavior
and Human Decision Processes, vol. 102, n° 2,
pp. 143-153.</mixed-citation>
         </ref>
         <ref id="d855e1115a1310">
            <mixed-citation id="d855e1119" publication-type="other">
Sogge D., 2003, Les mirages de l'aide internationale :
quand le calcul l'emporte sur la solidarité, Paris,
éditions de l'Atelier.</mixed-citation>
         </ref>
         <ref id="d855e1132a1310">
            <mixed-citation id="d855e1136" publication-type="other">
Spar D., Dail J., 2002, « Of Measurement and
Mission: Accounting for Performance in
Non-Governmental Organizations », Chicago
Journal of International Law, vol.3, n°1,
pp. 171-181.</mixed-citation>
         </ref>
         <ref id="d855e1155a1310">
            <mixed-citation id="d855e1159" publication-type="other">
Telford J., Cosgrave J., Houghton R., 2006, Joint
Evaluation of the International Response to
the Indian Ocean Tsunami: Synthesis Report,
Londres, Tsunami Evaluation Coalition.</mixed-citation>
         </ref>
         <ref id="d855e1175a1310">
            <mixed-citation id="d855e1179" publication-type="other">
Terry F., 2002, Condemned To Repeat? The Paradox
Of Humanitarian Action, Ithaca, Cornell University
Press.</mixed-citation>
         </ref>
         <ref id="d855e1192a1310">
            <mixed-citation id="d855e1196" publication-type="other">
Troubé T., 2010, L'Humanitaire, un business comme
les autres? Paris, Larousse.</mixed-citation>
         </ref>
         <ref id="d855e1207a1310">
            <mixed-citation id="d855e1211" publication-type="other">
Werker E., Ahmed F., 2008, « What Do Nongovern-
mental Organizations Do? », Journal of Economic
Perspectives, vol. 22, n° 2, pp. 73-92.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
