<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2jbr4m</book-id>
      <subj-group>
         <subject content-type="call-number">CB161.G67 2011</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Two thousand, A.D.</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Millennium (Eschatology)</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Calendar</subject>
         <subj-group>
            <subject content-type="lcsh">Social aspects</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Physics</subject>
         <subject>History</subject>
         <subject>Religion</subject>
      </subj-group>
      <book-title-group>
         <book-title>Questioning the Millennium</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Gould</surname>
               <given-names>Stephen Jay</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>29</day>
         <month>11</month>
         <year>2011</year>
      </pub-date>
      <isbn content-type="ppub">9780674061644</isbn>
      <isbn content-type="epub">9780674063341</isbn>
      <publisher>
         <publisher-name>Harvard University Press</publisher-name>
         <publisher-loc>Cambridge, Massachusetts; London, England</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>1999</copyright-year>
         <copyright-holder>Stephen Jay Gould</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt2jbr4m"/>
      <abstract abstract-type="short">
         <p>Gould addresses three questions about the millennium with his typical erudition, warmth, and whimsy: What is the concept of a millennium and how has its meaning shifted over time? How did the projection of Christ’s 1,000-year reign become a secular measure? And when exactly does the millennium begin—January 1, 2000, or January 2, 2001?</p>
      </abstract>
      <counts>
         <page-count count="224"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>1</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>7</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.3</book-part-id>
                  <title-group>
                     <title>[Illustration]</title>
                  </title-group>
                  <fpage>11</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.4</book-part-id>
                  <title-group>
                     <title>PREFACE TO THE REVISED EDITION</title>
                     <subtitle>Predicting: The Biggest Millennial Fallacy</subtitle>
                  </title-group>
                  <fpage>13</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.5</book-part-id>
                  <title-group>
                     <title>PREFACE TO THE ORIGINAL EDITION</title>
                     <subtitle>Our Precisely Arbitrary Millennium</subtitle>
                  </title-group>
                  <fpage>39</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.6</book-part-id>
                  <title-group>
                     <title>Redefining the Millennium:</title>
                     <subtitle>From Sacred Showdowns to Current Countdowns</subtitle>
                  </title-group>
                  <fpage>53</fpage>
                  <abstract>
                     <p>We inhabit a world of infinite and wondrous variety, a source of potential joy, especially if we can recapture childhood’s fresh delight for “splendor in the grass” and “glory in the flower.” Robert Louis Stevenson caught the essence of such naive pleasure in a single couplet—this “Happy Thought” from<italic>A Child’s Garden of Verses:</italic>
                     </p>
                     <p>
                        <italic>The world is so full of a number of things,</italic>
                     </p>
                     <p>
                        <italic>I’m sure we should all be as happy as kings.</italic>
                     </p>
                     <p>But sheer variety can also be overwhelming and frightening, especially when, as responsible adults, we must face the slings and arrows of (sometimes) outrageous fortune.</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.7</book-part-id>
                  <title-group>
                     <title>Dousing Diminutive Dennis’s Debate (DDDD = 2000)</title>
                  </title-group>
                  <fpage>127</fpage>
                  <abstract>
                     <p>In 1697, on the day appointed for repenting mistakes in judgment at Salem, Samuel Sewall of Boston stood silently in old South Church, as the rector read his confession of error aloud and to the public. He alone among judges of the falsely accused “witches” of Salem had the courage to undergo such public chastisement. Four years later, the same Samuel Sewall made a most joyful noise unto the Lord—and at a particularly auspicious moment. He hired four trumpeters to herald, as he wrote, the “entrance of the 18th century” by sounding a blast on Boston Common right at</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.8</book-part-id>
                  <title-group>
                     <label>Part One:</label>
                     <title>Bloody-Minded Nature</title>
                  </title-group>
                  <fpage>157</fpage>
                  <abstract>
                     <p>We have a false impression, buttressed by some famously exaggerated testimony, that the universe runs with the regularity of an ideal clock, and that God must therefore be a consummate mathematician. In his most famous aphorism, Galileo described the cosmos as “a grand book written in the language of mathematics, and its characters are triangles, circles, and other geometrical figures.” The Scottish biologist D’Arcy Thompson, one of my earliest intellectual heroes and author of the incomparably well-written<italic>Growth and Form</italic>(first published 1917 and still vigorously in print, the latest edition with a preface by yours truly), stated that “the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.9</book-part-id>
                  <title-group>
                     <label>Part Two:</label>
                     <title>Five Weeks</title>
                  </title-group>
                  <fpage>189</fpage>
                  <abstract>
                     <p>Poets, extolling the connectedness of all things, have said that the fall of a flower’s petal must disturb a distant star. Let us all be thankful that universal integration is not so tight, for we would not even exist in a cosmos of such intricate binding.</p>
                     <p>Georges Cuvier, the greatest French naturalist of the early nineteenth century, argued that evolution could not occur because all parts of the body are too highly integrated. If one part changed, absolutely every other part would have to alter in a corresponding manner to produce a new but equally elegant configuration for some different</p>
                  </abstract>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.10</book-part-id>
                  <title-group>
                     <title>Illustration Credits</title>
                  </title-group>
                  <fpage>207</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
      <book-part indexed="true" xlink:type="simple">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2jbr4m.11</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>211</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
