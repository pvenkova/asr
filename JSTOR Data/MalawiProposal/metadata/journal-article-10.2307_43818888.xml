<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">canajeconrevucan</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100116</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>The Canadian Journal of Economics / Revue canadienne d'Economique</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Wiley-Blackwell</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00084085</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15405982</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43818888</article-id>
         <title-group>
            <article-title>Core competencies, matching and the structure of foreign direct investment</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Federico J.</given-names>
                  <surname>Díez</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Alan C.</given-names>
                  <surname>Spearot</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>8</month>
            <year>2014</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">47</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">3</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40156545</issue-id>
         <fpage>813</fpage>
         <lpage>855</lpage>
         <permissions>
            <copyright-statement>© 2014 Canadian Economics Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43818888"/>
         <abstract>
            <p>We develop a matching model of foreign direct investment to study how multinational firms choose between greenfield investment, acquisitions and joint ownership. Firms must invest in a continuum of tasks to bring a product to market. Each firm possesses a core competency in the task space, but the firms are otherwise identical. For acquisitions and joint ownership, a multinational enterprise (MNE) must match with a local partner that may provide complementary expertise within the task space. However, under joint ownership, investment in tasks is shared by multiple owners and, hence, is subject to a holdup problem that varies with contract intensity. In equilibrium, ex ante identical multinationals enter the local matching market, and, ex post, three different types of heterogeneous firms arise. Specifically, the worst matches are forgone and the MNEs invest greenfield; the middle matches operate under joint ownership; and the best matches integrate via full acquisition. We link the firm-level model to cross-country and industry predictions and find that a greater share of full acquisitions occur between more proximate markets, in hosts with greater revenue potential and within contract-intensive industries. Using data on partial and full acquisitions across industries and countries, we find robust support for these predictions. Compétences de base, arrimage, et structure de l'investissement direct à l'étranger. On développe un modèle d'arrimage de l'investissement direct à l'étranger pour étudier le comportement des plurinationales dans le choix de diverses formes d'activités—construction de nouvelles installations, acquisitions, ou propriété conjointe. Les firmes doivent investir dans un continuum de tâches pour apporter un produit au marché. Chaque firme a une compétence de base dans cet espace de tâches, mais, pour ce qui est du reste, elles sont identiques. Dans le cas d'acquisitions et de propriété conjointe, les firmes plurinationales doivent s'arrimer avec un partenaire local qui puisse fournir l'expertise complémentaire dans l'espace des tâches. Cependant, dans le cas de propriété conjointe, l'investissement dans les tâches est partagé entre plusieurs propriétaires, et donc susceptible de se prêter à un problème de braquage qui varie selon les détails du contrat. En équilibre, des plurinationales identiques ex ante entrent dans le marché de l'arrimage, et ex post, trois types différents de firmes hétérogènes émergent. Spécifiquement, les pires arrimages sont évités, et la plurinationale construit des installations nouvelles; des arrimages moyens engendrent la propriété conjointe; et les meilleurs arrimages mènent à des acquisitions. En reliant le modèle au niveau de la firme aux prédictions entre pays et au niveau de l'industrie, on découvre qu'une plus grande portion des pleines acquisitions sont réalisées entre marchés proches, dans les pays hôtes qui promettent le plus grand potentiel de revenus, et à l'intérieur d'industries où les contrats sont les plus intensifs. À l'aide de données sur les acquisitions partielles et complètes, à travers les industries et les pays, il y a un support robuste pour ces prédictions.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <fn-group xmlns:xlink="http://www.w3.org/1999/xlink">
         <title>[Footnotes]</title>
         <fn id="d1247e242a1310">
            <label>2</label>
            <p>
               <mixed-citation id="d1247e249" publication-type="other">
McCalman (2008),</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e256a1310">
            <label>3</label>
            <p>
               <mixed-citation id="d1247e263" publication-type="other">
Desai, Foley, and Hines (2004),</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e270a1310">
            <label>4</label>
            <p>
               <mixed-citation id="d1247e277" publication-type="other">
Horn and Persson (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e283" publication-type="other">
Norbäck and Persson
(2004).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e293a1310">
            <label>5</label>
            <p>
               <mixed-citation id="d1247e300" publication-type="other">
Killing (1982),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e306" publication-type="other">
Gomes-Casseres (1987),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e312" publication-type="other">
Hamel, Doz and Prahalad (1989),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e319" publication-type="other">
Kogut (1989),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e325" publication-type="other">
Inkpen and Beamish
(1997),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e334" publication-type="other">
Miller, Glen, Jaspersen and Karmokolias (1997)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e340" publication-type="other">
Sinha (2001),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e346" publication-type="other">
Inkpen and Ross (2001)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e352" publication-type="other">
Roy Chowdhury and Roy Chowdhury (2001).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e360a1310">
            <label>6</label>
            <p>
               <mixed-citation id="d1247e367" publication-type="other">
Portes and Rey (2005)</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e374a1310">
            <label>13</label>
            <p>
               <mixed-citation id="d1247e381" publication-type="other">
Helpman, Melitz and Yeaple (2004),</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e388a1310">
            <label>16</label>
            <p>
               <mixed-citation id="d1247e395" publication-type="other">
Gugler, Mueller, Yurtoglu
and Zulehner (2003)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e404" publication-type="other">
Di Giovanni (2005),</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e410" publication-type="other">
Breinlich (2008)</mixed-citation>
            </p>
            <p>
               <mixed-citation id="d1247e417" publication-type="other">
Hijzen, Görg
and Manchin (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e427a1310">
            <label>17</label>
            <p>
               <mixed-citation id="d1247e434" publication-type="other">
Hijzen, Görg and Manchin (2008).</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e441a1310">
            <label>20</label>
            <p>
               <mixed-citation id="d1247e448" publication-type="other">
Fillat, Garetto and Oldenski (2013)</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e455a1310">
            <label>21</label>
            <p>
               <mixed-citation id="d1247e462" publication-type="other">
Blonigen and Piger (2012)</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e470a1310">
            <label>26</label>
            <p>
               <mixed-citation id="d1247e477" publication-type="other">
Chhibber and Majumdar (1999)</mixed-citation>
            </p>
         </fn>
         <fn id="d1247e484a1310">
            <label>27</label>
            <p>
               <mixed-citation id="d1247e491" publication-type="other">
Raff, Ryan and Stähler (2012)</mixed-citation>
            </p>
         </fn>
      </fn-group>
      <ref-list>
         <title>References</title>
         <ref id="d1247e507a1310">
            <mixed-citation id="d1247e511" publication-type="other">
Antràs, P., and E. Helpman (2008) "Contractual frictions and global sourcing," in The
Organization of Firms in a Global Economy, ed. E. Helpman, D. Marin and T.
Verdier. Cambridge: Harvard University Press</mixed-citation>
         </ref>
         <ref id="d1247e524a1310">
            <mixed-citation id="d1247e528" publication-type="other">
Arnold, J., and B. Javorcik (2005) "Gifted kids or pushy parents? Foreign acquisitions
and plant performance in Indonesia," World Bank Policy Research Working Paper
No. 3597</mixed-citation>
         </ref>
         <ref id="d1247e541a1310">
            <mixed-citation id="d1247e545" publication-type="other">
Bircan, C. (2011) "Optimal degree of foreign ownership under uncertainty," Working
Paper, Research Seminar in International Economics, University of Michigan</mixed-citation>
         </ref>
         <ref id="d1247e555a1310">
            <mixed-citation id="d1247e559" publication-type="other">
Blonigen, B., and J. Piger (2012) "Determinants of foreign direct investment," Working
Paper, Originally NBER WP No. 16704</mixed-citation>
         </ref>
         <ref id="d1247e570a1310">
            <mixed-citation id="d1247e574" publication-type="other">
Blonigen, B. A., L. Fontagne, N. Sly and F. Toubal (2012) Cherries for Sale: The
Incidence of Cross-Border M&amp;A," Working paper</mixed-citation>
         </ref>
         <ref id="d1247e584a1310">
            <mixed-citation id="d1247e588" publication-type="other">
Breinlich, H. (2008) "Trade liberalization and industrial restructuring through mergers
and acquisitions," Journal of International Economics 76(2), 254-66</mixed-citation>
         </ref>
         <ref id="d1247e598a1310">
            <mixed-citation id="d1247e602" publication-type="other">
Chhibber, P. K., and S. K. Majumdar (1999) "Foreign ownership and profitability:
Property rights, control, and the performance of firms in Indian industry," Journal of
Law and Economics 42(1), 209-38</mixed-citation>
         </ref>
         <ref id="d1247e615a1310">
            <mixed-citation id="d1247e619" publication-type="other">
Desai, M. A., C. F. Foley and J. R. Hines (2004) "The costs of shared ownership:
Evidence from international joint ventures," Journal of Financial Economics 73(2),
323-74</mixed-citation>
         </ref>
         <ref id="d1247e632a1310">
            <mixed-citation id="d1247e636" publication-type="other">
Di Giovanni, J. (2005) "What drives capital flows? The case of cross-border M&amp;A
activity and financial deepening," Journal of International Economics 65(1), 127-49</mixed-citation>
         </ref>
         <ref id="d1247e646a1310">
            <mixed-citation id="d1247e650" publication-type="other">
Ederington, J., and P. McCalman (2008) Endogenous firm heterogeneity and the
dynamics of trade liberalization," Journal of International Economics 74(2), 422-40</mixed-citation>
         </ref>
         <ref id="d1247e661a1310">
            <mixed-citation id="d1247e665" publication-type="other">
Fillat, J. L., S. Garetto and L. Oldenski (2013) "Diversification, cost structure, and the
stock returns of multinational corporations," 2013 Meeting Papers 1179, Society for
Economic Dynamics</mixed-citation>
         </ref>
         <ref id="d1247e678a1310">
            <mixed-citation id="d1247e682" publication-type="other">
Gomes-Casseres, B. (1987) "Joint venture instability: Is it a problem?" Columbia Journal
of World Business 22(2), 97-102</mixed-citation>
         </ref>
         <ref id="d1247e692a1310">
            <mixed-citation id="d1247e696" publication-type="other">
Grossman, G., and E. Helpman (2005) "Outsourcing in a Global Economy," Review of
Economic Studies 72(1), 135-59</mixed-citation>
         </ref>
         <ref id="d1247e706a1310">
            <mixed-citation id="d1247e710" publication-type="other">
Gugler, K., D. C. Mueller, B. B. Yurtoglu and C. Zulehner (2003) "The effects of
mergers: An international comparison," International Journal of Industrial
Organization 21(5), 625-53</mixed-citation>
         </ref>
         <ref id="d1247e723a1310">
            <mixed-citation id="d1247e727" publication-type="other">
Hamel, G., Y. L. Doz and C. K. Prahalad (1989) "Collaborate with your
competitors -and win," Harvard Business Review 67(1), 226-32</mixed-citation>
         </ref>
         <ref id="d1247e737a1310">
            <mixed-citation id="d1247e741" publication-type="other">
Head, K., and J. Ries (2008) "FDI as an outcome of the market for corporate control:
Theory and evidence," Journal of International Economics 74(2), 2-20</mixed-citation>
         </ref>
         <ref id="d1247e752a1310">
            <mixed-citation id="d1247e756" publication-type="other">
Helpman, E., M. Mehtz and S. Yeaple (2004) "Export versus FDI with Heterogenous
Firms," American Economic Review 94(1), 300-16</mixed-citation>
         </ref>
         <ref id="d1247e766a1310">
            <mixed-citation id="d1247e770" publication-type="other">
Hijzen, A., H. Görg and M. Manchin (2008) "Cross-border mergers and acquisitions
and the role of trade costs," European Economic Review 52(5), 849-66</mixed-citation>
         </ref>
         <ref id="d1247e780a1310">
            <mixed-citation id="d1247e784" publication-type="other">
Horn, H., and L. Persson (2001) "The equilibrium ownership of an international
oligopoly," Journal of International Economics 53(2), 307-33</mixed-citation>
         </ref>
         <ref id="d1247e794a1310">
            <mixed-citation id="d1247e798" publication-type="other">
Inkpen, A., and P. W. Beamish (1997) Knowledge, bargaining power, and the instability
of international joint ventures," Academy of Management Review 22(1), 177-202</mixed-citation>
         </ref>
         <ref id="d1247e808a1310">
            <mixed-citation id="d1247e812" publication-type="other">
Inkpen, A., and J. Ross (2001): Why Do Some Strategic Alliances Persist Beyond Their
Useful Life?," California Management Review , 44(1), 132-48</mixed-citation>
         </ref>
         <ref id="d1247e822a1310">
            <mixed-citation id="d1247e826" publication-type="other">
Killing, J. (1982) "How to make a global joint venture wórk," Harvard Business Review
60(3), 120-27</mixed-citation>
         </ref>
         <ref id="d1247e837a1310">
            <mixed-citation id="d1247e841" publication-type="other">
Kogut, B. (1989) The stability of joint ventures: Reciprocity and competitive rivalry,"
Journal of Industrial Economics 38(2), 183-98</mixed-citation>
         </ref>
         <ref id="d1247e851a1310">
            <mixed-citation id="d1247e855" publication-type="other">
Lichtenberg, F. R., and D. Siegel (1987) "Productivity and changes in ownership of
manufacturing plants," Brookings Papers on Economic Activity 1987(3), 643-83</mixed-citation>
         </ref>
         <ref id="d1247e865a1310">
            <mixed-citation id="d1247e869" publication-type="other">
Mayer, T., and S. Zignago (201 1) "Notes on CEPII's distances measures: The GeoDist
database," CEPII Working Paper 2011-25</mixed-citation>
         </ref>
         <ref id="d1247e879a1310">
            <mixed-citation id="d1247e883" publication-type="other">
Melitz, M. (2003) "The impact of trade on aggregate industry productivity and
intra-industry reallocations," Econometrica 71(6), 1695-1725</mixed-citation>
         </ref>
         <ref id="d1247e893a1310">
            <mixed-citation id="d1247e897" publication-type="other">
Miller, R., J. Glen, F. Jaspersen and Y. Karmokolias (1997) "International joint ventures
in developing countries," Finance and Development 34(1), 26-29</mixed-citation>
         </ref>
         <ref id="d1247e907a1310">
            <mixed-citation id="d1247e911" publication-type="other">
Nocke, V., and S. Yeaple (2007) "Cross-border mergers and acquisitions vs. greenfield
foreign direct investment: The role of firm heterogeneity," Journal of International
Economics 72(2), 336-65</mixed-citation>
         </ref>
         <ref id="d1247e925a1310">
            <mixed-citation id="d1247e929" publication-type="other">
---(2008) "An Assignment Theory of Foreign Direct Investment," Review of
Economic Studies 75(2), 529-57</mixed-citation>
         </ref>
         <ref id="d1247e939a1310">
            <mixed-citation id="d1247e943" publication-type="other">
Norbäck, P.-J., and L. Persson (2004) "Privatization and foreign competition," Journal
of International Economics 62(2), 409-16</mixed-citation>
         </ref>
         <ref id="d1247e953a1310">
            <mixed-citation id="d1247e957" publication-type="other">
Nunn, N. (2007) "Relationship-specificity, incomplete contracts, and the pattern of
trade," The Quarterly Journal of Economics 122(2), 569-600</mixed-citation>
         </ref>
         <ref id="d1247e967a1310">
            <mixed-citation id="d1247e971" publication-type="other">
Portes, R., and H. Rey (2005) "The determinants of cross-border equity flows," Journal
of International Economics 65(2), 269-96</mixed-citation>
         </ref>
         <ref id="d1247e981a1310">
            <mixed-citation id="d1247e985" publication-type="other">
Raff, H., M. Ryan and F. Stähler (2009) "The choice of market entry mode: Greenfield
investment, M&amp;A and joint venture," International Review of Economics &amp; Finance
18(1), 3-10</mixed-citation>
         </ref>
         <ref id="d1247e998a1310">
            <mixed-citation id="d1247e1002" publication-type="other">
---(2012) Firm productivity and the foreign-market entry decision, Journal of
Economics &amp; Management Strategy 21(3), 849-71</mixed-citation>
         </ref>
         <ref id="d1247e1013a1310">
            <mixed-citation id="d1247e1017" publication-type="other">
Rauch, J. E., and V. Trindade (2003) "Information, international substitutability, and
globalization," American Economic Review 93(3), 775-91</mixed-citation>
         </ref>
         <ref id="d1247e1027a1310">
            <mixed-citation id="d1247e1031" publication-type="other">
Roy Chowdhury, I., and P Roy Chowdhury (2001) "A theory of joint venture
life-cycles," International Journal of Industrial Organization 19(3-4), 319-43</mixed-citation>
         </ref>
         <ref id="d1247e1041a1310">
            <mixed-citation id="d1247e1045" publication-type="other">
Sinha, U. B. (2001) "International joint venture, licensing and buy-out under asymmetric
information," Journal of Development Economics 66(1), 127-51</mixed-citation>
         </ref>
         <ref id="d1247e1055a1310">
            <mixed-citation id="d1247e1059" publication-type="other">
Spearot, A. C. (2012) "Firm heterogeneity, new investment and acquisitions," The
Journal of Industrial Economics 60(1), 1-45</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
