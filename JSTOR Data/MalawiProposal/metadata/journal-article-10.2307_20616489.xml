<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">demography</journal-id>
         <journal-id journal-id-type="jstor">j100446</journal-id>
         <journal-title-group>
            <journal-title>Demography</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Population Association of America</publisher-name>
         </publisher>
         <issn pub-type="ppub">00703370</issn>
         <issn pub-type="epub">15337790</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">20616489</article-id>
         <title-group>
            <article-title>Adult Height and Childhood Disease</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name>
                  <given-names>Carlos</given-names>
                  <surname>Bozzoli</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Angus</given-names>
                  <surname>Deaton</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name>
                  <given-names>Climent</given-names>
                  <surname>Quintana-Domeque</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>11</month>
            <year>2009</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">46</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id>i20616486</issue-id>
         <fpage>647</fpage>
         <lpage>669</lpage>
         <permissions>
            <copyright-statement>Copyright Population Association of America</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/20616489"/>
         <abstract>
            <p>Taller populations are typically richer populations, and taller individuals live longer and earn more. In consequence, adult height has recently become a focus in understanding the relationship between health and wealth. We investigate the childhood determinants of population adult height, focusing on the respective roles of income and of disease. Across a range of European countries and the United States, we find a strong inverse relationship between postneonatal (ages 1 month to 1 year) mortality, interpreted as a measure of the disease and nutritional burden in childhood, and the mean height of those children as adults. Consistent with these findings, we develop a model of selection and stunting in which the early-life burden of undernutrition and disease not only is responsible for mortality in childhood but also leaves a residue of long-term health risks for survivors, risks that express themselves in adult height and in late-life disease. The model predicts that at sufficiently high mortality levels, selection can dominate scarring, leaving a taller population of survivors. We find evidence of this effect in the poorest and highest-mortality countries of the world, supplementing recent findings on the effects of the Great Chinese Famine.</p>
         </abstract>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d1023e186a1310">
            <mixed-citation id="d1023e190" publication-type="other">
Bogin, B. 2001. The Growth of Humanity. New York: Wiley-Liss.</mixed-citation>
         </ref>
         <ref id="d1023e197a1310">
            <mixed-citation id="d1023e201" publication-type="other">
Case, A. and C. Paxson. 2008. "Stature and Status: Height, Ability, and Labor Market Outcomes."
Journal of Political Economy 116:499-532.</mixed-citation>
         </ref>
         <ref id="d1023e211a1310">
            <mixed-citation id="d1023e215" publication-type="other">
Cole, T.J. 2000. "Secular Trends in Growth." Proceedings of the Nutrition Society 59:317-24.</mixed-citation>
         </ref>
         <ref id="d1023e222a1310">
            <mixed-citation id="d1023e226" publication-type="other">
Costa, D.L. 2002. "Changing Chronic Disease Rates and Long-Term Declines in Functional Limita-
tions Among Older Men." Demography 39:119-37.</mixed-citation>
         </ref>
         <ref id="d1023e237a1310">
            <mixed-citation id="d1023e241" publication-type="other">
—. 2004. "The Measure of Man and Older Age Mortality: Evidence From the Gould Sample."
Journal of Economic History 64:1-23.</mixed-citation>
         </ref>
         <ref id="d1023e251a1310">
            <mixed-citation id="d1023e255" publication-type="other">
Costa, D.L., L. Helmchen, and S. Wilson. 2007. "Race, Infection and Arteriosclerosis in the Past."
Proceedings of the National Academy of Sciences 104:13219-24.</mixed-citation>
         </ref>
         <ref id="d1023e265a1310">
            <mixed-citation id="d1023e269" publication-type="other">
Crimmins, E. and C.E. Finch. 2006. "Infection, Inflammation, Height, and Longevity." Proceedings
of the National Academy of Sciences 103:498-503.</mixed-citation>
         </ref>
         <ref id="d1023e279a1310">
            <mixed-citation id="d1023e283" publication-type="other">
Deaton, A. 2007. "Height, Health and Development." Proceedings of the National Academy of
Sciences 104:13232-37.</mixed-citation>
         </ref>
         <ref id="d1023e293a1310">
            <mixed-citation id="d1023e297" publication-type="other">
—. 2008. "Height, Health, and Inequality: The Distribution of Adult Heights in India." American
Economic Review 98:468-74.</mixed-citation>
         </ref>
         <ref id="d1023e307a1310">
            <mixed-citation id="d1023e311" publication-type="other">
Doblhammer, G. and J.W. Vaupel. 2001. "Lifespan Depends on Month of Birth." Proceedings of the
National Academy of Sciences 98:2934-39.</mixed-citation>
         </ref>
         <ref id="d1023e322a1310">
            <mixed-citation id="d1023e326" publication-type="other">
Elo, I. and S.H. Preston. 1992. "Effects of Early Life Conditions on Adult Mortality: A Review."
Population Index 58:186-212.</mixed-citation>
         </ref>
         <ref id="d1023e336a1310">
            <mixed-citation id="d1023e340" publication-type="other">
Eveleth, P.B. and J.M. Tanner. 1990. World-Wide Variation in Human Growth. 2nd ed. Cambridge.
Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1023e350a1310">
            <mixed-citation id="d1023e354" publication-type="other">
Ezzati, M., H. Martin, S. Skjold, S. Vander Hoorn, and C.J.L. Murray. 2006. "Trends in National
and State-Level Obesity in the USA After Correction for Self-Report Bias: Analysis of Health
Surveys." Journal of the Royal Society of Medicine 99:250-57.</mixed-citation>
         </ref>
         <ref id="d1023e367a1310">
            <mixed-citation id="d1023e371" publication-type="other">
Finch, C.E. and E. Crimmins. 2004. "Inflammatory Exposure and Historical Changes in Human Life-
Spans." Science 305:1736-39.</mixed-citation>
         </ref>
         <ref id="d1023e381a1310">
            <mixed-citation id="d1023e385" publication-type="other">
Floud, R., K. Wachter, and A. Gregory. 1990. Height, Health and History: Nutritional Status in the
UK, 1750-1980. Cambridge: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1023e395a1310">
            <mixed-citation id="d1023e399" publication-type="other">
Fogel, R.W 2004. The Escape From Hunger and Premature Death, 1700-2100. Cambridge and New
York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d1023e410a1310">
            <mixed-citation id="d1023e414" publication-type="other">
Garcia, J. and C. Quintana-Domeque, 2007. "The Evolution of Adult Height in Europe: A Brief
Note." Economics and Human Biology 5:340-49.</mixed-citation>
         </ref>
         <ref id="d1023e424a1310">
            <mixed-citation id="d1023e428" publication-type="other">
Gorgens, T., X. Meng, and R. Vaithianathan. 2007. "Stunting and Selection Effects of Famine: A Case
Study of the Great Chinese Famine." IZA Discussion Paper No. 2543. Institute for the Study of
Labor, Bonn, Germany.</mixed-citation>
         </ref>
         <ref id="d1023e441a1310">
            <mixed-citation id="d1023e445" publication-type="other">
Heston, A., R. Summers, and B. Aten. 2006. Penn World Table Version 6.2. Center for Inter-
national Comparisons of Production, Income, and Prices at the University of Pennsylvania
(September).</mixed-citation>
         </ref>
         <ref id="d1023e458a1310">
            <mixed-citation id="d1023e462" publication-type="other">
Jousilahti, P., J. Tuomilehto, E. Vartiainen, J. Eriksson, and P. Puska. 2000. "Relation of Adult Height
to Cause-Specific and Total Mortality: A Prospective Follow-up Study of 31,199 Middle-Aged
Men and Women in Finland." American Journal of Epidemiology 151:1112-20.</mixed-citation>
         </ref>
         <ref id="d1023e475a1310">
            <mixed-citation id="d1023e479" publication-type="other">
Klasen, S. 2006. "Poverty, Undernutrition, and Child Mortality: Some Interregional Puzzles and Their
Implications for Research and Policy." IZA Discussion Paper No. 2509. Institute for the Study of
Labor, Bonn, Germany.</mixed-citation>
         </ref>
         <ref id="d1023e492a1310">
            <mixed-citation id="d1023e496" publication-type="other">
Leon, D.A., G. Davey Smith, M. Shipley, and D. Strachan. 1995. "Adult Height and Mortality in
London: Early Life, Socioeconomic Status, or Shrinkage." Journal of Epidemiology and Com-
munity Health 49:5-9.</mixed-citation>
         </ref>
         <ref id="d1023e510a1310">
            <mixed-citation id="d1023e514" publication-type="other">
Mansfield, C., S. Hopfer, and T. Marteau. 1999. "Termination Rates After Prenatal Diagnosis of Down
Syndrome, Spina Bifida, Anencephaly, and Turner and Klinefelter Syndromes: A Systematic Lit-
erature Review." Prenatal Diagnosis 19:808-12.</mixed-citation>
         </ref>
         <ref id="d1023e527a1310">
            <mixed-citation id="d1023e531" publication-type="other">
Mokyr, J. and C. Ó Gráda. 1996. "Height and Health in the United Kingdom 1815-60: Evidence From
the East India Company Army." Explorations in Economic History 33:141-68.</mixed-citation>
         </ref>
         <ref id="d1023e541a1310">
            <mixed-citation id="d1023e545" publication-type="other">
Peracchi, F. 2002. "The European Community Household Panel: A Review." Empirical Economics
27:63-90.</mixed-citation>
         </ref>
         <ref id="d1023e555a1310">
            <mixed-citation id="d1023e559" publication-type="other">
Preston, S.H., M.E. Hill, and G.L. Drevenstedt. 1998. "Childhood Conditions That Predict Survival
to Advanced Ages Among African-Americans." Social Science and Medicine 47:1231-36.</mixed-citation>
         </ref>
         <ref id="d1023e569a1310">
            <mixed-citation id="d1023e573" publication-type="other">
Rosano, A., L.D. Botto, B. Botting, and P. Mastroiacovo. 2000. "Infant Mortality and Congenital
Anomalies From 1950 to 1994: An International Perspective." Journal of Epidemiology and Com-
munity Health 54:660-66.</mixed-citation>
         </ref>
         <ref id="d1023e586a1310">
            <mixed-citation id="d1023e590" publication-type="other">
Schmidt, I., M. Jorgensen, and K. Michaelsen. 1995. "Height of Conscripts in Europe: Is Postneonatal
Mortality a Predictor?" Annals of Human Biology 22:57-67.</mixed-citation>
         </ref>
         <ref id="d1023e601a1310">
            <mixed-citation id="d1023e605" publication-type="other">
Schultz, T.P. 2001. "Productive Benefits of Improving Health: Evidence From Low-Income Coun-
tries." Available online at http://www.econ.yale.edu/~pschultz/productivebenefits.</mixed-citation>
         </ref>
         <ref id="d1023e615a1310">
            <mixed-citation id="d1023e619" publication-type="other">
Scrimshaw, N.S., C.E. Taylor, and J.E. Gordon. 1968. Interactions of Nutrition and Infection. Geneva:
World Health Organization.</mixed-citation>
         </ref>
         <ref id="d1023e629a1310">
            <mixed-citation id="d1023e633" publication-type="other">
Silventoinen, K. 2003. "Determinants of Variation in Adult Height." Journal of Biosocial Science
35:263-85.</mixed-citation>
         </ref>
         <ref id="d1023e643a1310">
            <mixed-citation id="d1023e647" publication-type="other">
Steckel, R.H. 1995. "Stature and the Standard of Living." Journal of Economic Literature 33:
1903-40.</mixed-citation>
         </ref>
         <ref id="d1023e657a1310">
            <mixed-citation id="d1023e661" publication-type="other">
—. 2004. "New Light on the Dark Ages: The Remarkably Tall Stature of Northern European
Men During the Medieval Era." Social Science History 28:211-19.</mixed-citation>
         </ref>
         <ref id="d1023e671a1310">
            <mixed-citation id="d1023e675" publication-type="other">
—. 2008. "Biological Measures of the Standard of Living." Journal of Economic Perspectives
22:129-52.</mixed-citation>
         </ref>
         <ref id="d1023e686a1310">
            <mixed-citation id="d1023e690" publication-type="other">
Thomas, D. and E. Frankenberg. 2002. "The Measurement and Interpretation of Health in Social
Surveys," Pp. 387-420 in Summary Measures of Population Health, edited by C.J. Murray,
J.A. Salomon, C.D. Mathers, and A.D. Lopez. Geneva: World Health Organization.</mixed-citation>
         </ref>
         <ref id="d1023e703a1310">
            <mixed-citation id="d1023e707" publication-type="other">
United Nations. 1986. "Age Structure of Mortality in Developing Countries: A Data Base for
Cross-Sectional and Time Series Research." Report. Department of International Economic and
Social Affairs, United Nations, New York. Available online at http://www.un.org/esa/population/
publications/Age_Structure of_Mortality/AgeStruct_Preface_TOC.pdf.</mixed-citation>
         </ref>
         <ref id="d1023e723a1310">
            <mixed-citation id="d1023e727" publication-type="other">
United Nations Development Program. 2006. Human Development Report 2006. Beyond Scarcity:
Power, Poverty and the Global Water Crisis. United Nations, New York.</mixed-citation>
         </ref>
         <ref id="d1023e737a1310">
            <mixed-citation id="d1023e741" publication-type="other">
van den Berg, G., M. Lindeboom, and F. Portrait. 2006. "Economic Conditions Early in Life and
Individual Mortality." American Economic Review 96:290-302.</mixed-citation>
         </ref>
         <ref id="d1023e751a1310">
            <mixed-citation id="d1023e755" publication-type="other">
Waaler, H.T. 1984. "Height, Weight and Mortality: The Norwegian Experience." Acta Medica Scan-
dinavica Supplementum 679:1-51.</mixed-citation>
         </ref>
         <ref id="d1023e765a1310">
            <mixed-citation id="d1023e769" publication-type="other">
World Bank. 2007. World Development Indicators CD-ROM. Washington, DC.</mixed-citation>
         </ref>
         <ref id="d1023e777a1310">
            <mixed-citation id="d1023e781" publication-type="other">
World Health Organization (WHO). Various editions (1953-1964). Annual Epidemiological and Vital
Statistics. Part I: Vital Statistics and Causes of Deaths. WHO, Geneva.</mixed-citation>
         </ref>
         <ref id="d1023e791a1310">
            <mixed-citation id="d1023e795" publication-type="other">
—. Various editions (1965-1982). World Health Statistics Annual. Vol. I: Vital Statistics and
Causes of Deaths. WHO, Geneva.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
