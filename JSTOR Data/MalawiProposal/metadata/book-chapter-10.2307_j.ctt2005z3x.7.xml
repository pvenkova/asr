<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt2005z3x</book-id>
      <subj-group>
         <subject content-type="call-number">HD1538.Z55 R88 2017</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agricultural laborers</subject>
         <subj-group>
            <subject content-type="lcsh">Political activity</subject>
            <subj-group>
               <subject content-type="lcsh">Zimbabwe</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agricultural laborers</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
            <subj-group>
               <subject content-type="lcsh">Economic conditions</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Land reform</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Land use</subject>
         <subj-group>
            <subject content-type="lcsh">Government policy</subject>
            <subj-group>
               <subject content-type="lcsh">Zimbabwe</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Agriculture and state</subject>
         <subj-group>
            <subject content-type="lcsh">Zimbabwe</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Zimbabwe</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
            <subj-group>
               <subject content-type="lcsh">1980-</subject>
            </subj-group>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Sociology</subject>
      </subj-group>
      <book-title-group>
         <book-title>Farm Labor Struggles in Zimbabwe</book-title>
         <subtitle>The Ground of Politics</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>RUTHERFORD</surname>
               <given-names>BLAIR</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>19</day>
         <month>12</month>
         <year>2016</year>
      </pub-date>
      <isbn content-type="ppub">9780253023995</isbn>
      <isbn content-type="epub">9780253024077</isbn>
      <isbn content-type="epub">0253024072</isbn>
      <publisher>
         <publisher-name>Indiana University Press</publisher-name>
         <publisher-loc>Bloomington, Indianapolis</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2017</copyright-year>
         <copyright-holder>Blair Rutherford</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt2005z3x"/>
      <abstract abstract-type="short">
         <p>In the early twentieth-first century, white-owned farms in Zimbabwe were subject to large-scale occupations by black urban dwellers in an increasingly violent struggle between national electoral politics, land reform, and contestations over democracy. Were the black occupiers being freed from racist bondage as cheap laborers by the state-supported massive land redistribution, or were they victims of state violence who had been denied access to their homes, social services, and jobs? Blair Rutherford examines the unequal social and power relations shaping the lives, livelihoods, and struggles of some of the farm workers during this momentous period in Zimbabwean history. His analysis is anchored in the time he spent on a horticultural farm just east of Harare, the capital of Zimbabwe, that was embroiled in the tumult of political violence associated with jambanja, the democratization movement. Rutherford complicates this analysis by showing that there was far more in play than political oppression by a corrupt and authoritarian regime and a movement to rectify racial and colonial land imbalances, as dominant narratives would have it. Instead, he reveals, farm worker livelihoods, access to land, gendered violence, and conflicting promises of rights and sovereignty played a more important role in the political economy of citizenship and labor than had been imagined.</p>
      </abstract>
      <counts>
         <page-count count="294"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.3</book-part-id>
                  <title-group>
                     <title>ABBREVIATIONS</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.4</book-part-id>
                  <title-group>
                     <title>PREFACE</title>
                  </title-group>
                  <fpage>ix</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.5</book-part-id>
                  <title-group>
                     <title>INTRODUCTION</title>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>“And, your book project . . . ?” I cautiously asked Tawonga,¹ knowing that in one of his infrequent e-mail messages from the previous year he alluded to a “story” about it.</p>
                     <p>“Ahh,” Tawonga sighed with a wan smile. “After you so abruptly left that day ten years ago, there were so many anxieties and worries about being associated with you that the friend I was living with took all my papers and documents that were in any way tied to you, including everything I was accumulating for my book, and tore them up and threw them in the toilet.”</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.6</book-part-id>
                  <title-group>
                     <label>CHAPTER 1</label>
                     <title>OPPRESSION, MARAITI, AND FARM WORKER LIVELIHOODS:</title>
                     <subtitle>SHIFTING GROUNDS IN THE 1990S</subtitle>
                  </title-group>
                  <fpage>27</fpage>
                  <abstract>
                     <p>I met with Mr. Chapunga at his company’s headquarters on the edge of the manufacturing district in south-central Harare. It was not until near the end of this interview that he came closest to the topic he had told me he would not broach—the ongoing labor dispute on Upfumi farm, which was the very topic I had arranged to meet Mr. Chapunga about in his capacity as acting group human resources manager of ZimfarmEast. The farm’s pack-shed and agricultural operations were owned and run by ZimfarmEast, which was then the agro-based division of Zimfarm Limited, the highly profitable Zimbabwean</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.7</book-part-id>
                  <title-group>
                     <label>CHAPTER 2</label>
                     <title>THE TRACTION OF RIGHTS, THE ART OF POLITICS:</title>
                     <subtitle>THE LABOR “WAR” AT UPFUMI</subtitle>
                  </title-group>
                  <fpage>74</fpage>
                  <abstract>
                     <p>“Pasi naMugabe! ” (“Down with Mugabe!”). The loud words hung in the air, silencing the murmurs and whispering that had been growing in the packed office; a room that had been heating up in temperature and in temperament. Crowded around me, a few of the fired Upfumi workers audibly drew in their breaths. Many looked furtively around, checking the reactions of others and, more importantly, to see if there were any strangers in the room. A General Agriculture and Plantation Workers’ Union of Zimbabwe (GAPWUZ) official seated next to me laughed nervously. And Chenjerai, the person who cried out the</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.8</book-part-id>
                  <title-group>
                     <label>CHAPTER 3</label>
                     <title>THE DRAMA OF POLITICS:</title>
                     <subtitle>DISSENSION, SUFFERING, AND VIOLENCE</subtitle>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>A few days after the Labor Tribunal hearing of October 27, 1999, I stopped by the<italic>musososo</italic>in the late afternoon. When I went to park the car by the side of the Upfumi driveway next to the thatch and pole shelters, there already were two other vehicles parked on the edge: a Mercedes Benz and a Goromonzi Rural District Council (RDC) truck. There were only a few women sitting at the camp. They told me that everyone else was at a meeting, pointing to the grassy field on the other side of the road leading to Upfumi. As I</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.9</book-part-id>
                  <title-group>
                     <label>CHAPTER 4</label>
                     <title>POLITICS AND PRECARIOUS LIVELIHOODS DURING THE TIME OF JAMBANJA</title>
                  </title-group>
                  <fpage>184</fpage>
                  <abstract>
                     <p>“In our ward, there is no violence. Our war vets and the [white] farmers, we work together and when we get these strangers invading our area, I am able to control them, unlike south of us by Seke [communal land] where [war veteran] Mugwagwa is a bit violent. [Councillor] Chishiri and [ZANU (PF) leader] Mherera knew the war vets were coming down to Sky Farm today but they didn’t go there because they were afraid of Mugwagwa. They have no control.” So spoke Councillor Banda as I was driving him around on Friday morning July 28, 2000, as he performed</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.10</book-part-id>
                  <title-group>
                     <title>CONCLUSION:</title>
                     <subtitle>REPRESENTING LABOR STRUGGLES</subtitle>
                  </title-group>
                  <fpage>241</fpage>
                  <abstract>
                     <p>What are some of the implications of my analysis of this momentous farm worker struggle for understanding current and possibly future dynamics in Zimbabwe, for labor struggles elsewhere, and the politics of representation when conducting ethnographic research during unsettled times? Examining the key components of the ground of politics, my brief thoughts focus on the questions of sovereignty, livelihoods, and belonging. Let me begin with perhaps the most iconic figure of Zimbabwe: Robert Mugabe.</p>
                     <p>When discussing Zimbabwean politics, Robert Mugabe quickly emerges as a key object of attention and subject of conversation. Regardless if one is speaking inside or outside</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.11</book-part-id>
                  <title-group>
                     <title>NOTES</title>
                  </title-group>
                  <fpage>255</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.12</book-part-id>
                  <title-group>
                     <title>BIBLIOGRAPHY</title>
                  </title-group>
                  <fpage>261</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.13</book-part-id>
                  <title-group>
                     <title>INDEX</title>
                  </title-group>
                  <fpage>277</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt2005z3x.14</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>279</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
