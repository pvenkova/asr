<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">natitaxjour</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50005631</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>National Tax Journal</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>National Tax Association</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00280283</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19447477</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">41789308</article-id>
         <title-group>
            <article-title>THE ECONOMIC CASE FOR FOREIGN TAX CREDITS FOR CASH FLOW TAXES</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>CHARLES E.</given-names>
                  <surname>McLURE,</surname>
                  <suffix>JR.</suffix>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>GEORGE R.</given-names>
                  <surname>ZODROW</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>3</month>
            <year>1998</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40084006</issue-id>
         <fpage>1</fpage>
         <lpage>22</lpage>
         <permissions>
            <copyright-statement>Copyright 1998 National Tax Association—Tax Institute of America</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/41789308"/>
         <abstract>
            <p>Since consumption-based direct taxes typically include a businesslevel cash flow tax (CFT), any foreign country considering such a tax system faces the risk that a CFT might not be eligible for foreign tax credits in the United States. In this paper, we present the economic case for granting creditability for CFTs, arguing that such treatment is appropriate under current U.S. law, regardless of whether one applies the fairly general wording of the statute or the highly mechanical tests of the regulations. Moreover, we argue that, if this position is rejected, U.S. law and/or regulations should be changed to make CFTs creditable.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>ENDNOTES</title>
         <ref id="d92e202a1310">
            <label>1</label>
            <mixed-citation id="d92e209" publication-type="other">
U.S. Department of the Treasury (1977),</mixed-citation>
            <mixed-citation id="d92e215" publication-type="other">
Aaron
and Galper (1985),</mixed-citation>
            <mixed-citation id="d92e224" publication-type="other">
Bradford (1986),</mixed-citation>
            <mixed-citation id="d92e231" publication-type="other">
Hall and
Rabushka (1995),</mixed-citation>
            <mixed-citation id="d92e240" publication-type="other">
McLure and Zodrow (1996a),</mixed-citation>
            <mixed-citation id="d92e246" publication-type="other">
Weidenbaum (1996).</mixed-citation>
         </ref>
         <ref id="d92e253a1310">
            <label>2</label>
            <mixed-citation id="d92e260" publication-type="other">
McLure and Zodrow (1996b),</mixed-citation>
            <mixed-citation id="d92e266" publication-type="other">
McLure, et al.
(1990),</mixed-citation>
            <mixed-citation id="d92e275" publication-type="other">
Shalizi and Squire (1988),</mixed-citation>
            <mixed-citation id="d92e282" publication-type="other">
Lodin (1978),</mixed-citation>
            <mixed-citation id="d92e288" publication-type="other">
Institute for Fiscal Studies (1978),</mixed-citation>
            <mixed-citation id="d92e294" publication-type="other">
McLure
(1992a, b).</mixed-citation>
         </ref>
         <ref id="d92e304a1310">
            <label>3</label>
            <mixed-citation id="d92e311" publication-type="other">
Zodrow (1997)</mixed-citation>
            <mixed-citation id="d92e317" publication-type="other">
Bradford (1986)</mixed-citation>
            <mixed-citation id="d92e323" publication-type="other">
McLure and Zodrow (1990)</mixed-citation>
            <mixed-citation id="d92e330" publication-type="other">
McLure and Zodrow
(1996b).</mixed-citation>
         </ref>
         <ref id="d92e340a1310">
            <label>5</label>
            <mixed-citation id="d92e347" publication-type="other">
McLure et al. (1990).</mixed-citation>
         </ref>
         <ref id="d92e355a1310">
            <label>6</label>
            <mixed-citation id="d92e362" publication-type="other">
Abbin, Gordon, and Renfroe (1985,
p. 1135)</mixed-citation>
         </ref>
         <ref id="d92e372a1310">
            <label>8</label>
            <mixed-citation id="d92e379" publication-type="other">
McLure and
Zodrow (1996b).</mixed-citation>
         </ref>
         <ref id="d92e389a1310">
            <label>11</label>
            <mixed-citation id="d92e396" publication-type="other">
McLure et al. (1990)</mixed-citation>
         </ref>
         <ref id="d92e403a1310">
            <label>12</label>
            <mixed-citation id="d92e410" publication-type="other">
Institute for Fiscal Studies,
1978</mixed-citation>
            <mixed-citation id="d92e419" publication-type="other">
U.S. Treasury
Department (1977)</mixed-citation>
            <mixed-citation id="d92e428" publication-type="other">
Aaron and Galper (1985)</mixed-citation>
            <mixed-citation id="d92e435" publication-type="other">
McLure and Zodrow (1996a, b).</mixed-citation>
         </ref>
         <ref id="d92e442a1310">
            <label>14</label>
            <mixed-citation id="d92e449" publication-type="other">
Gentry and Hubbard (1997, p.
6),</mixed-citation>
            <mixed-citation id="d92e458" publication-type="other">
Bradford, 1996</mixed-citation>
         </ref>
         <ref id="d92e465a1310">
            <label>15</label>
            <mixed-citation id="d92e472" publication-type="other">
Institute for Fiscal Studies,
1978</mixed-citation>
            <mixed-citation id="d92e481" publication-type="other">
U.S. Treasury Department
(1977),</mixed-citation>
            <mixed-citation id="d92e490" publication-type="other">
Hall and Rabushka (1995)</mixed-citation>
            <mixed-citation id="d92e497" publication-type="other">
Alliance
USA (1995)</mixed-citation>
            <mixed-citation id="d92e506" publication-type="other">
Weidenbaum (1996),</mixed-citation>
            <mixed-citation id="d92e512" publication-type="other">
Bradford (1986),</mixed-citation>
            <mixed-citation id="d92e518" publication-type="other">
McLure
et al. (1990),</mixed-citation>
            <mixed-citation id="d92e527" publication-type="other">
McLure
and Zodrow (1996a, b).</mixed-citation>
         </ref>
         <ref id="d92e538a1310">
            <label>16</label>
            <mixed-citation id="d92e545" publication-type="other">
McLure, 1993</mixed-citation>
            <mixed-citation id="d92e551" publication-type="other">
Grubert and Newlon
(1995)</mixed-citation>
         </ref>
         <ref id="d92e561a1310">
            <label>18</label>
            <mixed-citation id="d92e568" publication-type="other">
Hufbauer (1992, p. 49),</mixed-citation>
         </ref>
         <ref id="d92e575a1310">
            <label>19</label>
            <mixed-citation id="d92e582" publication-type="other">
Hufbauer
(1992).</mixed-citation>
         </ref>
         <ref id="d92e592a1310">
            <label>23</label>
            <mixed-citation id="d92e599" publication-type="other">
Dolan (1984, p. 167)</mixed-citation>
            <mixed-citation id="d92e605" publication-type="other">
Lieberman (1984, p. 99)</mixed-citation>
         </ref>
         <ref id="d92e612a1310">
            <label>25</label>
            <mixed-citation id="d92e621" publication-type="other">
Grubert and Newlon (1995)</mixed-citation>
         </ref>
         <ref id="d92e628a1310">
            <label>26</label>
            <mixed-citation id="d92e635" publication-type="other">
Isenbergh (1984).</mixed-citation>
         </ref>
         <ref id="d92e643a1310">
            <label>28</label>
            <mixed-citation id="d92e650" publication-type="other">
Dolan,
1984</mixed-citation>
         </ref>
         <ref id="d92e660a1310">
            <label>31</label>
            <mixed-citation id="d92e667" publication-type="other">
McLure and Zodrow (1996b).</mixed-citation>
         </ref>
         <ref id="d92e674a1310">
            <label>35</label>
            <mixed-citation id="d92e681" publication-type="other">
Levey (1983, p. 240)</mixed-citation>
         </ref>
         <ref id="d92e688a1310">
            <label>36</label>
            <mixed-citation id="d92e695" publication-type="other">
Andrews (1974),</mixed-citation>
            <mixed-citation id="d92e701" publication-type="other">
Boskin (1996),</mixed-citation>
         </ref>
         <ref id="d92e708a1310">
            <label>37</label>
            <mixed-citation id="d92e715" publication-type="other">
Boskin (1996).</mixed-citation>
         </ref>
         <ref id="d92e722a1310">
            <label>38</label>
            <mixed-citation id="d92e729" publication-type="other">
Hufbauer
(1995)</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d92e748a1310">
            <mixed-citation id="d92e752" publication-type="other">
Aaron, Henry J., and Harvey Galper.
Assessing Tax Reform. Washington, D.C.: The
Brookings Institution, 1985.</mixed-citation>
         </ref>
         <ref id="d92e765a1310">
            <mixed-citation id="d92e769" publication-type="other">
Abbin, Byrle M., Richard A. Gordon, and
Dianne L. Renfroe. "International Implications
of a Cash Flow Consumption Tax." Tax Notes 28
No. 10 (September 2, 1985): 1127-35.</mixed-citation>
         </ref>
         <ref id="d92e785a1310">
            <mixed-citation id="d92e789" publication-type="other">
Alliance USA. "Unlimited Savings Allowance
(USA) Tax System." Tax Notes 66 No. 11 (March
10, 1995): 1485-575.</mixed-citation>
         </ref>
         <ref id="d92e802a1310">
            <mixed-citation id="d92e806" publication-type="other">
Andrews, William. "A Consumption-Type or
Cash Flow Personal Income Tax." Harvard Law
Review 87 No.6 (April, 1974): 309-85.</mixed-citation>
         </ref>
         <ref id="d92e820a1310">
            <mixed-citation id="d92e824" publication-type="other">
Auerbach, Alan J., and Dale W. Jorgenson.
"The First Year Capital Recovery System" Tax
Note, 10 No. 15 (April 14, 1980): 515-23.</mixed-citation>
         </ref>
         <ref id="d92e837a1310">
            <mixed-citation id="d92e841" publication-type="other">
Boskin, Michael J. Frontiers of Tax Reform. Palo
Alto: Hoover Institution Press, 1996.</mixed-citation>
         </ref>
         <ref id="d92e851a1310">
            <mixed-citation id="d92e855" publication-type="other">
Bradford, David F. Untangling the Income Tax.
Cambridge, MA: Harvard University Press, 1986.</mixed-citation>
         </ref>
         <ref id="d92e865a1310">
            <mixed-citation id="d92e869" publication-type="other">
Bradford, David F. "Consumption Taxes:
Some Fundamental Transition Issues." In
Frontiers of Tax Reform, edited by Michael J.
Boskin, 123-50. Palo Alto: Hoover Institution
Press, 1996.</mixed-citation>
         </ref>
         <ref id="d92e888a1310">
            <mixed-citation id="d92e892" publication-type="other">
Dolan, Kevin D. "General Standards of
Credibility Under the §§901 and 903 Final
Regulations: New Words, Old Concepts." Tax
Management International Journal 13 No. 6
(June, 1984): 167-80.</mixed-citation>
         </ref>
         <ref id="d92e911a1310">
            <mixed-citation id="d92e915" publication-type="other">
Gentry, William M., and R. Glenn Hubbard.
"Distributional Implications of Introducing a
Broad-Based Consumption Tax." In Tax Policy
and the Economy, Volume 11, edited by James
M. Poterba, 1-47. Cambridge MA: MIT Press.</mixed-citation>
         </ref>
         <ref id="d92e935a1310">
            <mixed-citation id="d92e939" publication-type="other">
Grubert, Harry, and Scott Newlon. "The
International Implications of Consumption Tax
Proposals." National Tax Journal 48 No. 4
(December, 1995): 619-47.</mixed-citation>
         </ref>
         <ref id="d92e955a1310">
            <mixed-citation id="d92e959" publication-type="other">
Hall, Robert E., and Alvin Rabushka. The Flat
Tax. 2nd ed. Stanford: Hoover Institution Press,
1995.</mixed-citation>
         </ref>
         <ref id="d92e972a1310">
            <mixed-citation id="d92e976" publication-type="other">
Hufbauer, Gary Clyde. U.S. Taxation of
International Income: Blueprint for Reform.
Washington, D.C.: Institute for International
Economics, 1992.</mixed-citation>
         </ref>
         <ref id="d92e992a1310">
            <mixed-citation id="d92e996" publication-type="other">
Hufbauer, Gary Clyde. Fundamental Tax
Reform and Border Tax Adjustments. Washington,
D.C.: Institute for International Economics, 1995.</mixed-citation>
         </ref>
         <ref id="d92e1009a1310">
            <mixed-citation id="d92e1013" publication-type="other">
Institute for Fiscal Studies. The Structure and
Reform of Direct Taxation. London: Allen and
Unwin, 1978.</mixed-citation>
         </ref>
         <ref id="d92e1026a1310">
            <mixed-citation id="d92e1030" publication-type="other">
Isenbergh, Joseph. "The Foreign Tax Credit:
Royalties, Subsidies and Creditable Taxes." Tax
Law Review 39 No. 3 (Spring 1984): 227-95.</mixed-citation>
         </ref>
         <ref id="d92e1044a1310">
            <mixed-citation id="d92e1048" publication-type="other">
King, Mervyn, and Don Fullerton. The
Taxation of Income from Capital. Chicago:
University of Chicago Press, 1984.</mixed-citation>
         </ref>
         <ref id="d92e1061a1310">
            <mixed-citation id="d92e1065" publication-type="other">
Levey, Marc M. "Cred¡tability of a Foreign Tax:
The Principles, the Regulations and the
Complexity." Journal of Law and Commerce 3
(1983): 193-241.</mixed-citation>
         </ref>
         <ref id="d92e1081a1310">
            <mixed-citation id="d92e1085" publication-type="other">
Lieberman, Edward H. "Whether and to What
Extent a Foreign Tax is Creditable Under Final
Regulations." Journal of Taxation 60 No. 2
(February, 1984): 98-102.</mixed-citation>
         </ref>
         <ref id="d92e1101a1310">
            <mixed-citation id="d92e1105" publication-type="other">
Lodin, Sven-Olof. Progressive Expenditure
Tax—An Alternative? Report of the 1972
Government Commission on Taxation.
Stockholm: LiberForlag, 1978.</mixed-citation>
         </ref>
         <ref id="d92e1121a1310">
            <mixed-citation id="d92e1125" publication-type="other">
McLure, Charles E., Jr. "Income Tax Policy for
the Russian Republic." Communist Economies
and Economic Transformation 4 No. 3 (Septem-
ber, 1992a): 425-36.</mixed-citation>
         </ref>
         <ref id="d92e1141a1310">
            <mixed-citation id="d92e1145" publication-type="other">
McLure, Charles E., Jr. "A Simpler Consump-
tion-Based Alternative to the Income Tax for
Socialist Economies in Transition." World Bank
Research Observer 7 No. 2 (July, 1992b): 221-37.</mixed-citation>
         </ref>
         <ref id="d92e1162a1310">
            <mixed-citation id="d92e1166" publication-type="other">
McLure, Charles E., Jr. "Economic, Administra-
tive, and Political Factors in Choosing a General
Consumption Tax." National Tax Journal 46 No. 3
(September, 1993): 345-58.</mixed-citation>
         </ref>
         <ref id="d92e1182a1310">
            <mixed-citation id="d92e1186" publication-type="other">
McLure, Charles E., Jr., and George R.
Zodrow. "Administrative Advantages of the
Individual Tax Prepayment Approach to the Direct
Taxation of Consumption." In Heidelberg
Congress on Taxing Consumption, edited by
Manfred Rose, 335-82. New York: Springer-
Verlag, 1990.</mixed-citation>
         </ref>
         <ref id="d92e1212a1310">
            <mixed-citation id="d92e1216" publication-type="other">
McLure, Charles E., Jr., and George R.
Zodrow. "A Hybrid Approach to the Direct
Taxation of Consumption." In Frontiers of Tax
Reform, edited by Michael Boskin, 70-90. Palo
Alto: Hoover Institution Press, 1996a.</mixed-citation>
         </ref>
         <ref id="d92e1235a1310">
            <mixed-citation id="d92e1239" publication-type="other">
McLure, Charles E., Jr., and George R.
Zodrow. "A Hybrid Consumption-Based Direct
Tax Proposed for Bolivia." International Tax and
Public Finance 3 No. 1 (January, 1996b): 97-112.</mixed-citation>
         </ref>
         <ref id="d92e1255a1310">
            <mixed-citation id="d92e1259" publication-type="other">
McLure, Charles E., Jr., Jack Mutti, Victor
Thuronyi, and George Zodrow. The Taxation
of Income from Business and Capital in
Colombia. Durham: Duke University Press,
1990.</mixed-citation>
         </ref>
         <ref id="d92e1278a1310">
            <mixed-citation id="d92e1282" publication-type="other">
Musgrave, Peggy B. "International Coordina-
tion Problems of Substituting Consumption for
Income Taxation." In Heidelberg Congress on
Taxing Consumption, edited by Manfred Rose,
453-89. New York: Springer-Verlag, 1990.</mixed-citation>
         </ref>
         <ref id="d92e1302a1310">
            <mixed-citation id="d92e1306" publication-type="other">
Shalizi, Zmarak, and Lyn Squire. Tax Policy in
Sub-Saharan Africa: A Framework for Analysis.
Washington, D.C.: World Bank, 1988.</mixed-citation>
         </ref>
         <ref id="d92e1319a1310">
            <mixed-citation id="d92e1325" publication-type="other">
Slemrod, Joel. "Effect of Taxation with Inter-
national Capital Mobility." In Uneasy Compro-
mise: Problems of a Hybrid Income-Consumption
Tax, edited by Henry J. Aaron, Harvey Galper, and
Joseph A. Pechman, 115-48. Washington, D.C.:
The Brookings Institution, 1988.</mixed-citation>
         </ref>
         <ref id="d92e1348a1310">
            <mixed-citation id="d92e1352" publication-type="other">
U.S. Department of the Treasury. Blueprints
for Basic Tax Reform. Washington, D.C.:
Government Printing Office, 1977. Also
available as: Bradford, David F., and the U.S.
Treasury Tax Policy Staff, Blueprints for Basic Tax
Reform, Arlington: Tax Analysts, 1984.</mixed-citation>
         </ref>
         <ref id="d92e1375a1310">
            <mixed-citation id="d92e1379" publication-type="other">
Weidenbaum, Murray. "The Nunn-Domenici
USA Tax: Analysis and Comparisons." In Frontiers
of Tax Reform, edited by Michael Boskin, 54-69.
Palo Alto: Hoover Institution Press, 1996.</mixed-citation>
         </ref>
         <ref id="d92e1395a1310">
            <mixed-citation id="d92e1399" publication-type="other">
Zodrow, George R. "Reflections on the
Consumption Tax Option." In Taxation Towards
2000, edited by John G. Head and Richard
Krever, 46-79. Sydney: Australian Tax
Foundation, 1997.</mixed-citation>
         </ref>
         <ref id="d92e1418a1310">
            <mixed-citation id="d92e1422" publication-type="other">
Zodrow, George R., and Charles E. McLure, Jr.
"Implementing Direct Consumption Taxes in
Developing Countries" Tax Law Review 46 No.4
(Summer, 1991): 405-87.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
