<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">urbaanthstudcult</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50000723</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Urban Anthropology and Studies of Cultural Systems and World Economic Development</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>The Institute, Inc.</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">08946019</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">40553613</article-id>
         <title-group>
            <article-title>The Politics Of Exclusion: Place And The Legislation Of The Environment In The Florida Everglades</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Max</given-names>
                  <surname>Kirsch</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>4</month>
            <year>2003</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">32</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40024201</issue-id>
         <fpage>99</fpage>
         <lpage>131</lpage>
         <permissions>
            <copyright-statement>© 2003 The Institute, Inc.</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/40553613"/>
         <abstract>
            <p>Despite the longstanding recognition of the importance of the Everglades in the nation's ecosystem, and the potential impact of the Everglades Restoration Project, few social scientists have worked in this area, and none have explored the impact of the Restoration project on the region and its communities. This paper seeks to integrate the Everglades on local, national and global levels, emphasizing the role of politics and community organizations on the development of the current multi-billion dollar plan for environmental restoration. The paper explores concerns of sectors of the regional population and their experiences in attempts to participate in current processes; establishes levels of integration on which analysis can take place; poses questions about the disjuncture among various sectors, including industry, development interests and community concerns on the planning process; and situates the role of sugar and the Cuban revolution on the current status of decision making on Florida and national policy developers. The paper argues that current discussions of globalization are inadequate for a careful analysis of local-level interactions with larger social fields, and that these discussions follow a more politicized strategy of capital fluidity than a holistic analysis of rapid social change. Finally, it argues that while the effect of localized organizing on the planning and implementation process remains to be seen, the result will be no less than the fate of the Everglades, and South Florida.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>NOTES</title>
         <ref id="d36e114a1310">
            <label>1</label>
            <mixed-citation id="d36e121" publication-type="other">
Alexander Cockburn's (1995: 201)</mixed-citation>
         </ref>
         <ref id="d36e128a1310">
            <label>2</label>
            <mixed-citation id="d36e135" publication-type="other">
Appadurai (1990) quoted in Nash (2001b).</mixed-citation>
            <mixed-citation id="d36e141" publication-type="other">
(Nash 2001b:16).</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>REFERENCES CITED</title>
         <ref id="d36e157a1310">
            <mixed-citation id="d36e161" publication-type="other">
Appadurai, Arjun (1986). Introduction: Communities and the Politics of
Value. IN The Social Life of Things: Commodities in Cultural Per-
spective, Arjun Appaduari (ed.). Cambridge: Cambridge Univer-
sity Press, pp. 3-63.</mixed-citation>
         </ref>
         <ref id="d36e177a1310">
            <mixed-citation id="d36e181" publication-type="other">
Appadurai, Arjun (1990). Disjuncture and Difference in the Global Cul-
tural Economy. Public Culture 2: 1-24.</mixed-citation>
         </ref>
         <ref id="d36e191a1310">
            <mixed-citation id="d36e195" publication-type="other">
Appadurai, Arjun (1991). Global Ethnoscapes: Notes and Queries for a
Transnational Anthropology. IN Recapturing Anthropology: Work-
ing in the Present, R. G. Fox (ed.). Santa Fe: School of American
Research Press, pp. 191-210.</mixed-citation>
         </ref>
         <ref id="d36e211a1310">
            <mixed-citation id="d36e215" publication-type="other">
Army Corps of Engineers and South Florida Water Management District
(2001). Public Outreach Program Management Plan, Comprehen-
sive Everglades Restoration Plan. (Xerox)</mixed-citation>
         </ref>
         <ref id="d36e229a1310">
            <mixed-citation id="d36e233" publication-type="other">
Barboza, J. (2002). Everglades Plan Defies Free Trade Logic. New York Times,
May 6. p. 1</mixed-citation>
         </ref>
         <ref id="d36e243a1310">
            <mixed-citation id="d36e247" publication-type="other">
Bourdeiu, Pierre, (1977). Outline of a Theory of Practice. Cambridge: Cam-
bridge University Press.</mixed-citation>
         </ref>
         <ref id="d36e257a1310">
            <mixed-citation id="d36e261" publication-type="other">
Brown, L. G (1993). Totch: A Life in the Everglades (with a forward by
Peter Matthiessen). Gainesville: University Press of Florida</mixed-citation>
         </ref>
         <ref id="d36e271a1310">
            <mixed-citation id="d36e275" publication-type="other">
Burns, Alan (1993). Maya in Exile: Guatemalans in Florida. Philadelphia:
Temple University Press.</mixed-citation>
         </ref>
         <ref id="d36e285a1310">
            <mixed-citation id="d36e289" publication-type="other">
Castells, Manuel, Sherjro Yazawa, and Emma Kiselyova (1995-1996). In-
surgents Against the New World Order: A Comparative Analysis
of the Zapatistas in Mexico, the American Militia and Japan's Aum
Shino. Berkeley Journal of Sociology 40.</mixed-citation>
         </ref>
         <ref id="d36e305a1310">
            <mixed-citation id="d36e309" publication-type="other">
Cockburn, Alexander (1995). "Win-Win" with Bruce Babbitt: The Clinton
Administration Meets the Environment. New Left Review 201: 43-
55.</mixed-citation>
         </ref>
         <ref id="d36e323a1310">
            <mixed-citation id="d36e327" publication-type="other">
Douglas, M. S. 1947 (1997). The Everglades: River of Grass. Sarasota FL:
Pineapple Press</mixed-citation>
         </ref>
         <ref id="d36e337a1310">
            <mixed-citation id="d36e341" publication-type="other">
Englund, Harry (2002). Ethnography After Globalism: Migration and Em-
placement in Malawi. American Ethnologist 29(2): 2261-2286.</mixed-citation>
         </ref>
         <ref id="d36e351a1310">
            <mixed-citation id="d36e355" publication-type="other">
Flesher, A. (2002). Mining the Everglades. Sun-Sentinel, p. NA</mixed-citation>
         </ref>
         <ref id="d36e362a1310">
            <mixed-citation id="d36e366" publication-type="other">
Friedman, J.F. (1995). COMMENT on Scheper-Hughes, N. (1995). "The Pri-
macy of the Ethical: Propositions for a Militant Anthropology."
Current Anthropology 36(3): 421.</mixed-citation>
         </ref>
         <ref id="d36e379a1310">
            <mixed-citation id="d36e383" publication-type="other">
Glades Community Development Corporation (1998). FINAL REPORT:
Glades Vision to Action Forums. September 1998. Copy</mixed-citation>
         </ref>
         <ref id="d36e393a1310">
            <mixed-citation id="d36e397" publication-type="other">
Glades Community Development Corporation (2002). Town Hall Meetings
Project: Final Report. Copy, January 30, 2002.</mixed-citation>
         </ref>
         <ref id="d36e408a1310">
            <mixed-citation id="d36e412" publication-type="other">
Griffith, David (2000). Work and Immigration: Winter Vegetable Produc-
tion in South Florida. IN Poverty or Development, Richard
Tardancio and Mark B. Rosenberg (eds.). New York: Routledge,
pp. 139-178.</mixed-citation>
         </ref>
         <ref id="d36e428a1310">
            <mixed-citation id="d36e432" publication-type="other">
Grunwald, Michael (2000). How Corps Turned Doubt into a Lock. Wash-
ington Post, Sunday, February 13, Page AO1.</mixed-citation>
         </ref>
         <ref id="d36e442a1310">
            <mixed-citation id="d36e446" publication-type="other">
Grunwald, Michael (2001). Plan to Revive Everglades Brings Renewed Dis-
pute. Washington Post, Saturday, December 29, Page AO3.</mixed-citation>
         </ref>
         <ref id="d36e456a1310">
            <mixed-citation id="d36e460" publication-type="other">
Grunwald, Michael (2001). Norton Closes Everglades Renewal Office. Wash-
ington Post, Wednesday, November 7, Page AO3.</mixed-citation>
         </ref>
         <ref id="d36e470a1310">
            <mixed-citation id="d36e474" publication-type="other">
Grunwald, Michael (2002). White House Relaxes Rules on Protection of
Wetland. Washington Post, Tuesday, January 15, Page AO2.</mixed-citation>
         </ref>
         <ref id="d36e484a1310">
            <mixed-citation id="d36e488" publication-type="other">
Grunwald, Michael (2002). A Rescue Plan, Bold and Uncertain. Washing-
ton Post, Sunday June 23, Page AO1.</mixed-citation>
         </ref>
         <ref id="d36e499a1310">
            <mixed-citation id="d36e503" publication-type="other">
Grunwald, Michael (2002). Among Environmentalists, The Great Divide.
Washington Post, Wednesday, June 26, Page A13.</mixed-citation>
         </ref>
         <ref id="d36e513a1310">
            <mixed-citation id="d36e517" publication-type="other">
Grunwald, Michael (2002). An Environmental Reversal of Fortune. Wash-
ington Post, Wednesday, June 26, Page AO1.</mixed-citation>
         </ref>
         <ref id="d36e527a1310">
            <mixed-citation id="d36e531" publication-type="other">
Grunwald, Michael (2002). Growing Pains in Southwest Florida. Washing-
ton Post Tuesday, Tune 25, Paee AO1.</mixed-citation>
         </ref>
         <ref id="d36e541a1310">
            <mixed-citation id="d36e545" publication-type="other">
Grunwald, Michael (2002). How Enron Sought to Tap the Everglades. Wash-
ington Post, Friday, February 8, Page A12.</mixed-citation>
         </ref>
         <ref id="d36e555a1310">
            <mixed-citation id="d36e559" publication-type="other">
Grunwald, Michael (2002). Interior's Silence on Corps Plan Questioned.
Washington Post, Monday, January 14, Page AO5.</mixed-citation>
         </ref>
         <ref id="d36e569a1310">
            <mixed-citation id="d36e573" publication-type="other">
Grunwald, Michael (2002). Oversight Favored for Corps Projects. Wash-
ington Post, Friday, July 26, Page A31.</mixed-citation>
         </ref>
         <ref id="d36e584a1310">
            <mixed-citation id="d36e588" publication-type="other">
Grunwald, Michael (2002). The Everglades. Washington Post, Monday, June
24, Washington Post Online Discussion.</mixed-citation>
         </ref>
         <ref id="d36e598a1310">
            <mixed-citation id="d36e602" publication-type="other">
Grunwald, Michael (2002). To the White House, by Way of the Everglades.
Washington Post, Sunday June 23, Page A16</mixed-citation>
         </ref>
         <ref id="d36e612a1310">
            <mixed-citation id="d36e616" publication-type="other">
Grunwald, Michael (2002). Water Quality is Long-Standing Issue for Tribe.
Washington Post, Monday June 24, Page All.</mixed-citation>
         </ref>
         <ref id="d36e626a1310">
            <mixed-citation id="d36e630" publication-type="other">
Grunwald, Michael (2002). When In Doubt, Blame Big Sugar. Washington
Post, Tuesday June 25, Page AO9.</mixed-citation>
         </ref>
         <ref id="d36e640a1310">
            <mixed-citation id="d36e644" publication-type="other">
Grunwald, Michael (2002). Between Rock and a Hard Place. Washington
Post, Monday, June 24, Page AO1.</mixed-citation>
         </ref>
         <ref id="d36e654a1310">
            <mixed-citation id="d36e658" publication-type="other">
Grunwald, Michael (2000). Engineers of Power: An Agency of Unchecked
Clout. Washington Post, Sunday, September 10, Paee A01</mixed-citation>
         </ref>
         <ref id="d36e669a1310">
            <mixed-citation id="d36e673" publication-type="other">
Grunwald, Michael (2000). Pentagon Rebukes Army Corps. Washington
Post, Thursday, December 7, Paee AO1.</mixed-citation>
         </ref>
         <ref id="d36e683a1310">
            <mixed-citation id="d36e687" publication-type="other">
Harvey, David (1990). The Condition of Postmodernity. Maiden, MA:
Blackwell</mixed-citation>
         </ref>
         <ref id="d36e697a1310">
            <mixed-citation id="d36e701" publication-type="other">
Harvey, David (1998). What's Green and Makes the Environment Go Round.
IN The Cultures of Globalization, Frederic Jameson and Masao
Miyoshi (eds.). Durham: Duke University Press, pp. 327-355.</mixed-citation>
         </ref>
         <ref id="d36e714a1310">
            <mixed-citation id="d36e718" publication-type="other">
Inda, Jonathan Xavier, and Renato Rosaldo (eds.) (2002). Introduction. IN
The Anthropology of Globalization: A Reader, Jonathan Xavier Inda
and Renato Rosaldo (eds.). Maiden MA: Blackwell Publishers, pp.
1-36.</mixed-citation>
         </ref>
         <ref id="d36e734a1310">
            <mixed-citation id="d36e738" publication-type="other">
Ingold, T. (1992). Culture and the Perception of the Environment. IN Bush
Base: Forest Farm, E. Croll and D. Parkin (eds.). London: Routledge.</mixed-citation>
         </ref>
         <ref id="d36e748a1310">
            <mixed-citation id="d36e752" publication-type="other">
Jefferson, Jon (1993). Cane Workers Accord Means Big Job Loss. The Na-
tional Law Journal. 16: 10, November 8.</mixed-citation>
         </ref>
         <ref id="d36e763a1310">
            <mixed-citation id="d36e767" publication-type="other">
King, R. P. (2001). Restoration Plan Threatened. Palm Beach Post, October
29, p. 1.</mixed-citation>
         </ref>
         <ref id="d36e777a1310">
            <mixed-citation id="d36e781" publication-type="other">
Kirsch, Max (2002). Fieldnotes.</mixed-citation>
         </ref>
         <ref id="d36e788a1310">
            <mixed-citation id="d36e792" publication-type="other">
Kirsch, Max (1998). In the Wake of the Giant: Multinational Restructuring
and Uneven Development in a New England Community. Albany:
SUNY Press.</mixed-citation>
         </ref>
         <ref id="d36e805a1310">
            <mixed-citation id="d36e809" publication-type="other">
Lipman, C. (2002). "Report": Everglades Science Needs More Money. Palm
Beach Post, Thursday, December 17.</mixed-citation>
         </ref>
         <ref id="d36e819a1310">
            <mixed-citation id="d36e823" publication-type="other">
Maanen, J. V. (ed.) (1995). Representation and Ethnography. Thousand Oaks,
CA: Sage Press</mixed-citation>
         </ref>
         <ref id="d36e833a1310">
            <mixed-citation id="d36e837" publication-type="other">
McCally, D. (1999). The Everglades: An Environmental History; foreword
by Raymond Arsenault and Gary R. Mormino. Gainesville: Uni-
versity Press of Florida</mixed-citation>
         </ref>
         <ref id="d36e851a1310">
            <mixed-citation id="d36e855" publication-type="other">
Mintz, Sidney (1985). Sweetness and Power: The Place of Sugar in Modern
History. New York: Penguin</mixed-citation>
         </ref>
         <ref id="d36e865a1310">
            <mixed-citation id="d36e869" publication-type="other">
Mintz, Sidney (1998). The Localization of Anthropological Practice: From
Area Studies to Transnationalism. Critique of Anthropology 18(2):
117-133.</mixed-citation>
         </ref>
         <ref id="d36e882a1310">
            <mixed-citation id="d36e886" publication-type="other">
Murphy, Martha Celeste (1997). An Empirical Study of Farm Workers in
South Florida: Environmental Injustice in the Fields? Ph.D. Disser-
tation, Florida Atlantic University</mixed-citation>
         </ref>
         <ref id="d36e899a1310">
            <mixed-citation id="d36e903" publication-type="other">
Nash, June (2001a). Mayan Visions: The Quest for Autonomy in an Age of
Globalization. New York: Routledee.</mixed-citation>
         </ref>
         <ref id="d36e913a1310">
            <mixed-citation id="d36e917" publication-type="other">
Nash, June (2001b). Globalization and the Cultivation of Peripheral Vision.
Anthropology Today 17(4): 15-22.</mixed-citation>
         </ref>
         <ref id="d36e927a1310">
            <mixed-citation id="d36e931" publication-type="other">
Nash June, and Max Kirsch (1986). Polychlorinated Biphenyls in the Elec-
trical Machinery Industry: An Ethnological Study of Community
Action and Corporate Responsibility. Social Science and Medicine
2(1): 131-138</mixed-citation>
         </ref>
         <ref id="d36e948a1310">
            <mixed-citation id="d36e952" publication-type="other">
Nash, June, and Max Kirsch (1988). The Discourse of Medical Science in
the Construction of Consensus Between Corporation and Com-
munity. Medical Anthropology Quarterly 2(2): 158-171.</mixed-citation>
         </ref>
         <ref id="d36e965a1310">
            <mixed-citation id="d36e969" publication-type="other">
Nash, June, and Max Kirsch (1994). Corporate Culture and Social Respon-
sibility: The Case of Toxic Wastes in a New England Community.
IN Anthropological Perspectives on Organizational Culture, T,
Hamada and W.S. Sibley (eds.). Latham, MA: University Press of
America.</mixed-citation>
         </ref>
         <ref id="d36e988a1310">
            <mixed-citation id="d36e992" publication-type="other">
Portes, Alejandro, and Alex Stepick (1985). Unwelcome Immigrants: The
Labor Market Experiences of 1980 (Mariel) Cuban and Haitian
Refugees in South Florida. American Sociological Review 50: 493-
513.</mixed-citation>
         </ref>
         <ref id="d36e1008a1310">
            <mixed-citation id="d36e1012" publication-type="other">
Report to Congressional Requestors 1992. Labor Action Needed to Protect
Florida Sugar Cane Workers. Washington D.C.</mixed-citation>
         </ref>
         <ref id="d36e1022a1310">
            <mixed-citation id="d36e1026" publication-type="other">
Resnick, Rosalind (1992). $50 Million Win for Cane Cutters. (Bygrave v.
Sugar Cane Growers Cooperative). The National Law Journal 14:
43, no. 3., July 13.</mixed-citation>
         </ref>
         <ref id="d36e1039a1310">
            <mixed-citation id="d36e1043" publication-type="other">
Resnick, Rosalind (1991). Cane Cleanup. (Florida Sugar-Cane Industry
Labor Policies). The National Law Journal 13: 36.</mixed-citation>
         </ref>
         <ref id="d36e1054a1310">
            <mixed-citation id="d36e1058" publication-type="other">
Roberts, Paul (1999). The Sweet Hereafter. Harper's 299: 1794: 54-68.</mixed-citation>
         </ref>
         <ref id="d36e1065a1310">
            <mixed-citation id="d36e1069" publication-type="other">
Rose, J. (1991). Environmental Concepts, Policies and Strategies. Philadel-
phia: Gordon and Breach.</mixed-citation>
         </ref>
         <ref id="d36e1079a1310">
            <mixed-citation id="d36e1083" publication-type="other">
Sassen, Saskia, (1998). Globalization and its Discontents. New York: The
Free Press.</mixed-citation>
         </ref>
         <ref id="d36e1093a1310">
            <mixed-citation id="d36e1097" publication-type="other">
Scheper-Hughes, N. (1995). The Primacy of the Ethical: Propositions for a
Militant Anthropology. Current Anthropology 36(3): 409-439.</mixed-citation>
         </ref>
         <ref id="d36e1107a1310">
            <mixed-citation id="d36e1111" publication-type="other">
Scully, J. (2001). Restoring the Fragile Everglades, Evermore. The Chronicle
Review, January 12, 2001, pp. B13-14.</mixed-citation>
         </ref>
         <ref id="d36e1121a1310">
            <mixed-citation id="d36e1125" publication-type="other">
Simmons, G.and L. O. (1998). Gladesmen. Gainesville: University Press of
Florida</mixed-citation>
         </ref>
         <ref id="d36e1136a1310">
            <mixed-citation id="d36e1140" publication-type="other">
Sinclair, M. Thea (ed.) (2000). Gender Work, Tourism. New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d36e1147a1310">
            <mixed-citation id="d36e1151" publication-type="other">
Sugar y Azúcar (1992). Has Sugar Harmed the Everglades? March.</mixed-citation>
         </ref>
         <ref id="d36e1158a1310">
            <mixed-citation id="d36e1162" publication-type="other">
Tardanico R. and M. B. Rosenberg (eds.) (2000). Poverty or Development:
Global Restructuring and Regional Transformations in the U.S.
South and the Mexican South. New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d36e1175a1310">
            <mixed-citation id="d36e1179" publication-type="other">
United States Congress, 106th Congress, 2nd Session, H.R. 5121, (House Plan),
September 7, 2000.</mixed-citation>
         </ref>
         <ref id="d36e1189a1310">
            <mixed-citation id="d36e1193" publication-type="other">
United States Congress, September 7, 2000, Comprehensive Everglades
Restoration Plan, H.R. 5121; 106 H.R. 5121.</mixed-citation>
         </ref>
         <ref id="d36e1203a1310">
            <mixed-citation id="d36e1207" publication-type="other">
United States General Accounting Office (1992). Foreign Farm Workers in
U.S.: Department of Labor Action Needed to Protect Florida Sugar
Cane Workers. Report to Congressional Requesters. Washington
DC.</mixed-citation>
         </ref>
         <ref id="d36e1224a1310">
            <mixed-citation id="d36e1228" publication-type="other">
Wallerstein, Immanuel (1974). The Modern World System: Capitalist Agri-
culture and the Originals of the European World Economy in the
Sixteenth Century. New York: Academic</mixed-citation>
         </ref>
         <ref id="d36e1241a1310">
            <mixed-citation id="d36e1245" publication-type="other">
Wilkinson, Alec (1990). Big Sugar: Seasons in the Cane Fields of Florida.
New York: Vintage.</mixed-citation>
         </ref>
         <ref id="d36e1255a1310">
            <mixed-citation id="d36e1259" publication-type="other">
Williams, Joy (1999). Ill Nature: Rants and Reflections on Humanity and
Other Animals. Delray Beach, FL :The Lyons Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
