<?xml version="1.0" encoding="UTF-8"?>
<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:mml="http://www.w3.org/1998/Math/MathML"
         dtd-version="1.0"
         article-type="research-article">
   <front>
      <journal-meta>
         <journal-id journal-id-type="jstor">joperresesoci</journal-id>
         <journal-id journal-id-type="jstor">j100539</journal-id>
         <journal-title-group>
            <journal-title>The Journal of the Operational Research Society</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Palgrave Macmillan Press</publisher-name>
         </publisher>
         <issn pub-type="ppub">01605682</issn>
         <issn pub-type="epub">14769360</issn>
         <custom-meta-group/>
      </journal-meta>
      <article-meta>
         <article-id pub-id-type="jstor">4102069</article-id>
         <article-categories>
            <subj-group>
               <subject>Theoretical Papers</subject>
            </subj-group>
         </article-categories>
         <title-group>
            <article-title>A Reassessment of the Human Development Index Via Data Envelopment Analysis</article-title>
         </title-group>
         <contrib-group>
            <contrib contrib-type="author">
               <string-name>
                  <given-names>D. K.</given-names>
                  <surname>Despotis</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date>
            <day>1</day>
            <month>8</month>
            <year>2005</year>
         </pub-date>
         <volume xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">56</volume>
         <issue xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">8</issue>
         <issue-id>i379836</issue-id>
         <fpage>969</fpage>
         <lpage>980</lpage>
         <page-range>969-980</page-range>
         <permissions>
            <copyright-statement>Copyright 2005 Operational Research Society Ltd</copyright-statement>
         </permissions>
         <self-uri xlink:href="https://www.jstor.org/stable/4102069"/>
         <abstract>
            <p>To consider different aspects of life when measuring human development, the United Nations Development Program introduced the Human Development Index (HDI). The HDI is a composite index of socioeconomic indicators that reflect three major dimensions of human development: longevity, knowledge and standard of living. In this paper, the assessment of the HDI is reconsidered in the light of data envelopment analysis (DEA). Instead of a simple rank of the countries, human development is benchmarked on the basis of empirical observations of best practice countries. First, on the same line as HDI, we develop a DEA-like model to assess the relative performance of the countries in human development. Then we extend our calculations with a post-DEA model to derive global estimates of a new development index by using common weights for the socioeconomic indicators. Finally, we introduce the transformation paradigm in the assessment of human development. We develop a DEA model to estimate the relative efficiency of the countries in converting income to knowledge and life opportunities.</p>
         </abstract>
         <kwd-group>
            <kwd>Human development index</kwd>
            <kwd>Data envelopment analysis</kwd>
            <kwd>Development</kwd>
         </kwd-group>
         <custom-meta-group>
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d533e257a1310">
            <label>1</label>
            <mixed-citation id="d533e264" publication-type="journal">
Hicks N and Streeten P (1979). Indicators of development: the
search for a basic needs yardstic. World Dev 7: 567-580.<person-group>
                  <string-name>
                     <surname>Hicks</surname>
                  </string-name>
               </person-group>
               <fpage>567</fpage>
               <volume>7</volume>
               <source>World Dev</source>
               <year>1979</year>
            </mixed-citation>
         </ref>
         <ref id="d533e296a1310">
            <label>2</label>
            <mixed-citation id="d533e303" publication-type="book">
Streeten P et al. (1981). First Things First: Meeting Basic Needs
in Developing Countries. Oxford University Press: New York.<person-group>
                  <string-name>
                     <surname>Streeten</surname>
                  </string-name>
               </person-group>
               <source>First Things First: Meeting Basic Needs in Developing Countries</source>
               <year>1981</year>
            </mixed-citation>
         </ref>
         <ref id="d533e328a1310">
            <label>3</label>
            <mixed-citation id="d533e335" publication-type="book">
UNDP (2000). Human Development Report 2000. United
Nations Development Program, Oxford University Press:
New York.<person-group>
                  <string-name>
                     <surname>UNDP</surname>
                  </string-name>
               </person-group>
               <source>Human Development Report 2000</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d533e364a1310">
            <label>4</label>
            <mixed-citation id="d533e371" publication-type="journal">
Noorbakhsh F (1998). A modified Human Development Index.
World Dev 26: 517-528.<person-group>
                  <string-name>
                     <surname>Noorbakhsh</surname>
                  </string-name>
               </person-group>
               <fpage>517</fpage>
               <volume>26</volume>
               <source>World Dev</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d533e404a1310">
            <label>5</label>
            <mixed-citation id="d533e411" publication-type="journal">
Sagar AD and Najam A (1998). The human development index:
a critical review. Ecol Econ 25: 249-264.<person-group>
                  <string-name>
                     <surname>Sagar</surname>
                  </string-name>
               </person-group>
               <fpage>249</fpage>
               <volume>25</volume>
               <source>Ecol Econ</source>
               <year>1998</year>
            </mixed-citation>
         </ref>
         <ref id="d533e443a1310">
            <label>6</label>
            <mixed-citation id="d533e450" publication-type="journal">
Neumayer E (2001). The human development index-a
constructive proposal. Ecol Econ 39: 101-114.<person-group>
                  <string-name>
                     <surname>Neumayer</surname>
                  </string-name>
               </person-group>
               <fpage>101</fpage>
               <volume>39</volume>
               <source>Ecol Econ</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d533e482a1310">
            <label>7</label>
            <mixed-citation id="d533e489" publication-type="journal">
Desai M (1991). Human development: concepts and measure-
ment. Eur Econ Rev 35: 350-357.<person-group>
                  <string-name>
                     <surname>Desai</surname>
                  </string-name>
               </person-group>
               <fpage>350</fpage>
               <volume>35</volume>
               <source>Eur Econ Rev</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d533e521a1310">
            <label>8</label>
            <mixed-citation id="d533e528" publication-type="journal">
Kelly AC (1991). The human development index: 'handle with
care'. Population Dev Rev 17: 315-324.<person-group>
                  <string-name>
                     <surname>Kelly</surname>
                  </string-name>
               </person-group>
               <fpage>315</fpage>
               <volume>17</volume>
               <source>Population Dev Rev</source>
               <year>1991</year>
            </mixed-citation>
         </ref>
         <ref id="d533e560a1310">
            <label>9</label>
            <mixed-citation id="d533e567" publication-type="book">
Mahlberg B and Obersteiner M (2001). Remeasuring the HDI by
data envelopment analysis. International Institute for Applied
Systems Analysis (IIASA), Interim Report IR-01-069, Laxem-
burg, Austria.<person-group>
                  <string-name>
                     <surname>Mahlberg</surname>
                  </string-name>
               </person-group>
               <source>Remeasuring the HDI by data envelopment analysis</source>
               <year>2001</year>
            </mixed-citation>
         </ref>
         <ref id="d533e599a1310">
            <label>10</label>
            <mixed-citation id="d533e606" publication-type="journal">
Charnes A, Cooper WW and Rhodes E (1978). Measuring the
efficiency of decision making units. Eur J Opl Res 2: 429-444.<person-group>
                  <string-name>
                     <surname>Charnes</surname>
                  </string-name>
               </person-group>
               <fpage>429</fpage>
               <volume>2</volume>
               <source>Eur J Opl Res</source>
               <year>1978</year>
            </mixed-citation>
         </ref>
         <ref id="d533e639a1310">
            <label>11</label>
            <mixed-citation id="d533e646" publication-type="journal">
Banker RD, Charnes A and Cooper WW (1984). Some models
for estimating technical and scale inefficiencies in data envelop-
ment analysis. Mngt Sci 30: 1078-1092.<person-group>
                  <string-name>
                     <surname>Banker</surname>
                  </string-name>
               </person-group>
               <fpage>1078</fpage>
               <volume>30</volume>
               <source>Mngt Sci</source>
               <year>1984</year>
            </mixed-citation>
         </ref>
         <ref id="d533e681a1310">
            <label>12</label>
            <mixed-citation id="d533e688" publication-type="book">
Cooper WW, Seiford LM and Tone K (1999). Data Envelop-
ment Analysis. Kluwer Academic Publishers: Boston.<person-group>
                  <string-name>
                     <surname>Cooper</surname>
                  </string-name>
               </person-group>
               <source>Envelopment Analysis</source>
               <year>1999</year>
            </mixed-citation>
         </ref>
         <ref id="d533e713a1310">
            <label>13</label>
            <mixed-citation id="d533e720" publication-type="journal">
Raab R, Kotamraju P and Haag S (2000). Efficient provision of
child quality of life in less developed countries: conventional
development indexes versus a programming approach to
development indexes. Socio-Econ Plann Sci 34: 51-67.<person-group>
                  <string-name>
                     <surname>Raab</surname>
                  </string-name>
               </person-group>
               <fpage>51</fpage>
               <volume>34</volume>
               <source>Socio-Econ Plann Sci</source>
               <year>2000</year>
            </mixed-citation>
         </ref>
         <ref id="d533e758a1310">
            <label>14</label>
            <mixed-citation id="d533e765" publication-type="journal">
Revilla E, Sarkis J and Modrego A (2003). Evaluating
performance of public-private research collaborations: a DEA
analysis. J Opl Res Soc 54: 165-174.<person-group>
                  <string-name>
                     <surname>Revilla</surname>
                  </string-name>
               </person-group>
               <fpage>165</fpage>
               <volume>54</volume>
               <source>J Opl Res Soc</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d533e800a1310">
            <label>15</label>
            <mixed-citation id="d533e807" publication-type="journal">
Mayston DJ (2003). Measuring and managing educational
performance. J Opl Res Soc 54: 679-691.<person-group>
                  <string-name>
                     <surname>Mayston</surname>
                  </string-name>
               </person-group>
               <fpage>679</fpage>
               <volume>54</volume>
               <source>J Opl Res Soc</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d533e839a1310">
            <label>16</label>
            <mixed-citation id="d533e846" publication-type="journal">
Mukherjee A, Nath P and Pal M (2003). Resource, service
quality and performance triad: a framework for measuring
efficiency of banking services. J Opl Res Soc 54: 723-735.<person-group>
                  <string-name>
                     <surname>Mukherjee</surname>
                  </string-name>
               </person-group>
               <fpage>723</fpage>
               <volume>54</volume>
               <source>J Opl Res Soc</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d533e882a1310">
            <label>17</label>
            <mixed-citation id="d533e889" publication-type="journal">
Bowlin WF, Renner CJ and Rives JM (2003). A DEA study of
gender equity in executive compensation. J Opl Res Soc 54:
751-757.<person-group>
                  <string-name>
                     <surname>Bowlin</surname>
                  </string-name>
               </person-group>
               <fpage>751</fpage>
               <volume>54</volume>
               <source>J Opl Res Soc</source>
               <year>2003</year>
            </mixed-citation>
         </ref>
         <ref id="d533e924a1310">
            <label>18</label>
            <mixed-citation id="d533e931" publication-type="journal">
Shao BBM and Shu WS (2004). Productivity breakdown of the
information and computing technology industries across
countries. J Opl Res Soc 55: 23-33.<person-group>
                  <string-name>
                     <surname>Shao</surname>
                  </string-name>
               </person-group>
               <fpage>23</fpage>
               <volume>55</volume>
               <source>J Opl Res Soc</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d533e966a1310">
            <label>19</label>
            <mixed-citation id="d533e973" publication-type="journal">
Despotis DK (2004). Measuring human development via data
envelopment analysis: The case of Asia and The Pacific. Omega.
Int J Mngt Sci (in press).<person-group>
                  <string-name>
                     <surname>Despotis</surname>
                  </string-name>
               </person-group>
               <source>Int J Mngt Sci</source>
               <year>2004</year>
            </mixed-citation>
         </ref>
         <ref id="d533e1002a1310">
            <label>20</label>
            <mixed-citation id="d533e1009" publication-type="journal">
Despotis DK (2002). Improving the discriminating power of
DEA: focus on globally efficient units. J Opl Res Soc 53:
314-323.<person-group>
                  <string-name>
                     <surname>Despotis</surname>
                  </string-name>
               </person-group>
               <fpage>314</fpage>
               <volume>53</volume>
               <source>J Opl Res Soc</source>
               <year>2002</year>
            </mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
