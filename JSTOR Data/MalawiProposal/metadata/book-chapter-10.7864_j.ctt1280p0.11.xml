<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt1280p0</book-id>
      <subj-group>
         <subject content-type="call-number">HG1325.M377 2004</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Monetary policy</subject>
         <subj-group>
            <subject content-type="lcsh">Africa</subject>
         </subj-group>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Business</subject>
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>The Monetary Geography of Africa</book-title>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>MASSON</surname>
               <given-names>PAUL R.</given-names>
            </name>
         </contrib>
         <contrib contrib-type="author" id="contrib2">
            <name name-style="western">
               <surname>PATTILLO</surname>
               <given-names>CATHERINE</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>30</day>
         <month>11</month>
         <year>2004</year>
      </pub-date>
      <isbn content-type="epub">9780815797531</isbn>
      <isbn content-type="epub">0815797532</isbn>
      <publisher>
         <publisher-name>Brookings Institution Press</publisher-name>
         <publisher-loc>Washington, D.C.</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2005</copyright-year>
         <copyright-holder>THE BROOKINGS INSTITUTION</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/10.7864/j.ctt1280p0"/>
      <abstract abstract-type="short">
         <p>Africa is working toward the goal of creating a common currency that would serve as a symbol of African unity. The advantages of a common currency include lower transaction costs, increased stability, and greater insulation of central banks from pressures to provide monetary financing. Disadvantages relate to asymmetries among countries, especially in their terms of trade and in the degree of fiscal discipline. More disciplined countries will not want to form a union with countries whose excessive spending puts upward pressure on the central bank's monetary expansion. In T<italic>he Monetary Geography of Africa,</italic>Paul Masson and Catherine Pattillo review the history of monetary arrangements on the continent and analyze the current situation and prospects for further integration. They apply lessons from both experience and theory that lead to a number of conclusions. To begin with, West Africa faces a major problem because Nigeria has both asymmetric terms of trade -it is a large oil exporter while its potential partners are oil importers -and most important, large fiscal imbalances. Secondly, a monetary union among all eastern or southern African countries seems infeasible at this stage, since a number of countries suffer from the effects of civil conflicts and drought and are far from achieving the macroeconomic stability of South Africa. Lastly, the plan by Kenya, Tanzania, and Uganda to create a common currency seems to be generally compatible with other initiatives that could contribute to greater regional solidarity. However, economic gains would likely favor Kenya, which, unlike the other two countries, has substantial exports to its neighbors, and this may constrain the political will needed to proceed. A more promising strategy for monetary integration would be to build on existing monetary unions -the CFA franc zone in western and central Africa and the Common Monetary Area in southern Africa. Masson and Pattillo argue that the goal of a creating a single African currency is probably beyond reach. Economic realities suggest that grand new projects for African monetary unions are unlikely to be successful. More important for Africa's economic well-being will be to attack the more fundamental problems of corruption and governance.</p>
      </abstract>
      <counts>
         <page-count count="217"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>I</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>VII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.3</book-part-id>
                  <title-group>
                     <title>Foreword</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Talbott</surname>
                           <given-names>Strobe</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>IX</fpage>
                  <abstract>
                     <p>What has taken the place of the cold war as the defining division in world politics? One answer is the chasm between, on the one hand, those who feel that they are benefiting from globalization and, on the other, those who feel left behind. Today the split is, very roughly, 50-50; but the gap between the two is widening, and because of unsustainable population growth in poorer regions of the world the ratio of globalization’s self-perceived winners and losers is shifting in the wrong direction.</p>
                     <p>Africa is a continent especially afflicted by this trend. Hence the importance of this book</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.4</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>XIII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.5</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <fpage>XV</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.6</book-part-id>
                  <title-group>
                     <title>Abbreviations and Acronyms</title>
                  </title-group>
                  <fpage>XVII</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.7</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Monetary Union in Africa:</title>
                     <subtitle>Past, Present, and Future</subtitle>
                  </title-group>
                  <fpage>1</fpage>
                  <abstract>
                     <p>Africa finds itself at an important juncture in its history as the twenty-first century gets under way. There is widespread consensus that Africans must take responsibility for their destiny. Nearly fifty years have passed since the beginning of decolonization and early hopes of rapid development have faded. In recent decades, the continent has suffered from abysmal economic performance. Africa has failed to benefit from the increase in prosperity experienced by the rest of the world, prosperity resulting from expansion of trade and other aspects of globalization. Instead, African countries have become increasingly marginalized, with their share of world exports falling</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.8</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>African Currency Regimes since World War II</title>
                  </title-group>
                  <fpage>12</fpage>
                  <abstract>
                     <p>Providing historical context for the current constellation of currency areas throws light on the potential success of initiatives toward greater monetary integration. Indeed the proposals to create monetary unions encompassing the countries of ECOWAS in West Africa and of EAC and of COMESA in East and southern Africa, to reinforce or enlarge the CFA franc zone, or to extend the rand area to SADC, to say nothing of the plan to create a single currency for Africa, need to be evaluated in light of past experiences with monetary integration. However, this is not the place for an exhaustive survey of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.9</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Criteria for Currency Unions or the Adoption of Another Currency</title>
                  </title-group>
                  <fpage>33</fpage>
                  <abstract>
                     <p>Currency regimes, in particular the adoption of a common currency in the context of regional integration, have been in the spotlight for more than four decades. In recent years, a more intense interest in developing-country monetary unions, especially in Africa, can be attributed to the EU’s successful launch of the euro zone. Despite extensive economic analysis and some agreement on the factors that would produce costs and benefits, economists often cannot reach a consensus on whether the benefits of a currency regime in a particular region outweigh the costs, much less a general presumption that monetary unions are necessarily a</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.10</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>African Monetary Integration in Practice:</title>
                     <subtitle>CFA Franc Zone and South African CMA</subtitle>
                  </title-group>
                  <fpage>45</fpage>
                  <abstract>
                     <p>The prospects for monetary integration in Africa are best analyzed by first studying the two existing monetary or formal exchange rate unions on the continent. These are the CFA franc zone (or, to be more precise, the two regions composing it, namely, WAEMU and CAEMC)¹ and the CMA based on South Africa’s rand. These examples of monetary integration also have the advantage (for our purposes) of having been in place for a long time, albeit having evolved somewhat over the course of their existence. Monetary union in southern Africa dates back to the early years of the twentieth century, becoming</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.11</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Experiences of Countries in Managing Independent Currencies</title>
                  </title-group>
                  <fpage>77</fpage>
                  <abstract>
                     <p>What are the individual country experiences with independent currency regimes? We focus in this chapter on those sub-Saharan African nations that are not currently members of a monetary union (either the CFA franc or CMA zone), covering the period from the 1970s to the beginning of the current decade. Countries are grouped into two broad categories: those that have moved to some form of flexible exchange rate system (the majority) and those with continued unilateral pegged (fixed or adjustable) exchange rate regimes. The objective of the chapter’s analysis is twofold. First, and most important, countries currently involved in proposals either</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.12</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Proposed Single Currency for West Africa</title>
                  </title-group>
                  <fpage>95</fpage>
                  <abstract>
                     <p>On April 20, 2000, in Accra, Ghana, the leaders of six West African countries declared their intention to proceed, by January 2003, to a monetary union to be known as WAMZ.¹ This would be a first step toward a wider monetary union in 2004, which would include all ECOWAS countries. The leaders committed themselves to lowering central bank financing of budget deficits to 10 percent of the previous year’s government revenue, reducing budget deficits to 4 percent of GDP by 2003, creating a convergence council to help coordinate macroeconomic policies, and setting up a common central bank. The declaration states</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.13</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Regional Integration in SADC</title>
                  </title-group>
                  <fpage>113</fpage>
                  <abstract>
                     <p>SADC, a grouping of countries in southern Africa, emerged from the Southern African Development Coordination Conference (SADCC). In existence from 1980 until 1992, SADCC excluded South Africa and aimed to contain the apartheid regime and minimize its unfavorable effects on neighboring countries. Following the formation of the African National Congress (ANC) government and its dismantling of apartheid, in 1994 South Africa joined a revamped organization whose purposes became to foster harmonization and rationalization of policies and strategies for sustainable development in the region, achieve peace and security, and evolve common political values, systems, institutions, and other links among the region’s</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.14</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>EAC and COMESA</title>
                  </title-group>
                  <fpage>129</fpage>
                  <abstract>
                     <p>Overlapping membership of countries in a number of regional organizations complicates plans for monetary integration in East Africa and southern Africa. There is a project under way to revive economic cooperation in a small, three-country group, the EAC, but two of these countries are also members of a much larger, twenty-country group, COMESA, which stretches from Egypt in the north to Namibia in the south. COMESA has its own agenda for regional integration and is a rival to SADC in these initiatives. Further complicating the situation, there are nine countries with membership in both SADC and COMESA.</p>
                     <p>The treaty establishing</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.15</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>A Single Currency for Africa?</title>
                  </title-group>
                  <fpage>147</fpage>
                  <abstract>
                     <p>The creation of a common African currency has long been a pillar of African unity, a symbol of the strength that its backers hope will emerge from efforts to integrate the continent. A common currency was an objective of the OAU, created in 1963, and the AEC, established in 1991. The project is intimately associated with the newly formed AU, whose constitutive act (which was signed by twenty-seven governments at the OAU/ AEC assembly of heads of state and government in Lomé, Togo, on July 11, 2000, and which entered into force on May 26, 2001) has superseded the OAU</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.16</book-part-id>
                  <title-group>
                     <label>10</label>
                     <title>Africa’s Monetary Geography in the Coming Decades</title>
                  </title-group>
                  <fpage>162</fpage>
                  <abstract>
                     <p>In the more than forty years since independence, Africa’s monetary geography has been transformed. In part this is due to changes to the international environment, which make keeping fixed exchange rates against an external anchor more difficult for African nations (though the CFA franc is still rigidly fixed against the euro). In particular, the generalized move to floating currencies in the early 1970s has meant that now the major currencies (dollar, euro, and yen) fluctuate in value against each other. A peg to one of them means fluctuating against the others, which may make the peg fragile.</p>
                     <p>We conclude this</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.17</book-part-id>
                  <title-group>
                     <title>APPENDIX A</title>
                     <subtitle>Calibration of the Model</subtitle>
                  </title-group>
                  <fpage>171</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.18</book-part-id>
                  <title-group>
                     <title>APPENDIX B</title>
                     <subtitle>Country Vignettes</subtitle>
                  </title-group>
                  <fpage>182</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.19</book-part-id>
                  <title-group>
                     <title>References</title>
                  </title-group>
                  <fpage>197</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.20</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>207</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt1280p0.21</book-part-id>
                  <title-group>
                     <title>Back Matter</title>
                  </title-group>
                  <fpage>219</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
