<?xml version="1.0" encoding="UTF-8"?>
<book xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:mml="http://www.w3.org/1998/Math/MathML"
      dtd-version="0.2"
      xml:lang="eng">
   <collection-meta>
      <collection-id collection-id-type="jstor">books</collection-id>
   </collection-meta>
   <book-meta>
      <book-id book-id-type="jstor">j.ctt7zq30</book-id>
      <subj-group>
         <subject content-type="call-number">HC59.7.B294 2010</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Islands</subject>
         <subj-group>
            <subject content-type="lcsh">Economic conditions</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Islands</subject>
         <subj-group>
            <subject content-type="lcsh">Politics and government</subject>
         </subj-group>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Jurisdiction, Territorial</subject>
      </subj-group>
      <subj-group>
         <subject content-type="lcsh">Economic development</subject>
      </subj-group>
      <subj-group subj-group-type="discipline">
         <subject>Political Science</subject>
      </subj-group>
      <book-title-group>
         <book-title>Island Enclaves</book-title>
         <subtitle>Offshoring Strategies, Creative Governance, and Subnational Island Jurisdictions</subtitle>
      </book-title-group>
      <contrib-group>
         <contrib contrib-type="author" id="contrib1">
            <name name-style="western">
               <surname>Baldacchino</surname>
               <given-names>Godfrey</given-names>
            </name>
         </contrib>
      </contrib-group>
      <pub-date>
         <day>27</day>
         <month>07</month>
         <year>2010</year>
      </pub-date>
      <isbn content-type="ppub">9780773537439</isbn>
      <isbn content-type="epub">9780773586581</isbn>
      <publisher>
         <publisher-name>MQUP</publisher-name>
         <publisher-loc>Montreal; Kingston; London; Ithaca</publisher-loc>
      </publisher>
      <permissions>
         <copyright-year>2010</copyright-year>
         <copyright-holder>McGill-Queen’s University Press</copyright-holder>
      </permissions>
      <self-uri xlink:href="https://www.jstor.org/stable/j.ctt7zq30"/>
      <abstract abstract-type="short">
         <p>Examining subnational island jurisdictions such as Guantánamo Bay, Macau, Aruba, the Isle of Man, and Prince Edward Island, Godfrey Baldacchino shows how these distinct locales arrange special relationships with larger metropolitan powers. He also deals with the politics, economics, and diplomacy of islands that have been engineered as detention camps, offshore finance centres, military bases, heritage parks, or otherwise autonomous regions. More than a study of how detached regions are governed, Island Enclaves displays the ways in which these jurisdictions are pioneering some of the modern world’s most creative – and shadowy – forms of sovereignty and government.</p>
      </abstract>
      <counts>
         <page-count count="301"/>
      </counts>
      <custom-meta-group>
         <custom-meta>
            <meta-name>
                    lang
                </meta-name>
            <meta-value>eng</meta-value>
         </custom-meta>
      </custom-meta-group>
   </book-meta>
   <body>
      <book-part book-part-type="book-toc-page-order" indexed="yes">
         <body>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.1</book-part-id>
                  <title-group>
                     <title>Front Matter</title>
                  </title-group>
                  <fpage>i</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.2</book-part-id>
                  <title-group>
                     <title>Table of Contents</title>
                  </title-group>
                  <fpage>v</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.3</book-part-id>
                  <title-group>
                     <title>Abbreviations</title>
                  </title-group>
                  <fpage>vii</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.4</book-part-id>
                  <title-group>
                     <title>Acknowledgments</title>
                  </title-group>
                  <contrib-group>
                     <contrib contrib-type="author" id="contrib1" xlink:type="simple">
                        <name name-style="western">
                           <surname>Baldacchino</surname>
                           <given-names>Godfrey</given-names>
                        </name>
                     </contrib>
                  </contrib-group>
                  <fpage>xi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.5</book-part-id>
                  <title-group>
                     <title>Preface</title>
                  </title-group>
                  <fpage>xxi</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.6</book-part-id>
                  <title-group>
                     <label>1</label>
                     <title>Autonomy without Sovereignty:</title>
                     <subtitle>An “Island Studies” Approach</subtitle>
                  </title-group>
                  <fpage>3</fpage>
                  <abstract>
                     <p>What goes around, comes around. Back in the winter of 1990–91, I was a freshly minted doctoral student at the University of Warwick, reading feverishly in order to come to better grips with what I wanted to concentrate on as my thesis topic, without wanting to admit to others or to myself that I was then hopelessly adrift. In any case, I found very welcome support from, among others, sociology professor Robin Cohen, for whose work I had developed an appreciation in my earlier forays in development studies. Cohen was kind enough to allow me free and unencumbered access</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.7</book-part-id>
                  <title-group>
                     <label>2</label>
                     <title>Borders within Borders and the New Offshoring</title>
                  </title-group>
                  <fpage>22</fpage>
                  <abstract>
                     <p>There are only two countries in the world that share a border stretching over 8,890 km, are both members of the Organization for Economic Co-operation and Development (OECD), share the same international telephone prefix, trade extensively in goods and services, and whose majority of citizens speak the same language, watch similar TV programs, and get to play in each other’s major sport leagues. Those countries, of course, are Canada and the United States.</p>
                     <p>One would imagine that such a relationship could only broaden in the context of a “borderless world.” But the contrary has happened: in April 2005 the United</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.8</book-part-id>
                  <title-group>
                     <label>3</label>
                     <title>Island Subnationalism and “Upside Down” Decolonization</title>
                  </title-group>
                  <fpage>42</fpage>
                  <abstract>
                     <p>Subnational island jurisdictions are spaces that illustrate some interesting departures from the conventional realms of political possibility. The case of the “tributary state” is one example, described in the preceding chapter. This chapter will identify at least one other example of such an idiosyncrasy: a decolonization experience that is stalled by virtue of the colonized party stubbornly preferring the status quo and refusing independence. In so doing, and with a sensitivity to both historical context and post-colonial theory, it elaborates more thoroughly upon the characteristics of “island subnationalism” and presents models for its patterning and categorization, drawn from studies of</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.9</book-part-id>
                  <title-group>
                     <label>4</label>
                     <title>A Review of Economic Performance:</title>
                     <subtitle>Tapping the Hinterland Beyond</subtitle>
                  </title-group>
                  <fpage>65</fpage>
                  <abstract>
                     <p>A small territory is especially obliged to use extraterritorial resources as its “hinterland” for economic success. Yet it is rare to come across a critical analysis which looks at islands as the strategic and candid promoters of a role as political and economic usufructuaries over external resources. Adopting a “jurisdiction as resource” perspective, this chapter notes how subnational island jurisdictions tend to perform economically better than sovereign island states, precisely by crafting themselves as efficient siphons for attracting rent surpluses from other jurisdictions, and especially (but not exclusively) from their patron metropolitan power and its citizens. The resources thus attracted</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.10</book-part-id>
                  <title-group>
                     <label>5</label>
                     <title>Patterns of Creative Governance for Development</title>
                  </title-group>
                  <fpage>89</fpage>
                  <abstract>
                     <p>The “development” agenda of smaller island states continues to be gripped by a paradigm of vulnerability, though a recognition of resilience now seems to be sneaking in (e.g., Briguglio and Cordina, 2004; Briguglio and Kisanga, 2004). Such disarming discourse vindicates the concerns of many observers and scholars about the presumed non-viability of smaller island polities and economies, expressed both before and after the wave of smaller state sovereignty took off in the 1960s (Cyprus – 1960; Samoa – 1962; Malta – 1964; Maldives – 1965), also propelled by the collapse of the West Indies Federation (starting with the independence of Jamaica in 1962). Independence</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.11</book-part-id>
                  <title-group>
                     <label>6</label>
                     <title>Offshoring Strategies:</title>
                     <subtitle>Lessons from Subnational Island Jurisdictions</subtitle>
                  </title-group>
                  <fpage>110</fpage>
                  <abstract>
                     <p>Saint-Pierre and Miquelon is the smallest jurisdiction in North America, off the southern coast of Newfoundland, a<italic>collectivité d’outre mer</italic>of France, with a population of around 6,500. It used to thrive on the cod fishery, before the Canadian government – which controls fish quotas – imposed its cod moratorium on the Grand Banks in 1992. The islands have since fallen into a sullen dependency on transfers from France. Their Exclusive Economic Zone is already quite unique and is completely engulfed by the Canadian one. It is 200 nautical miles deep, but just 10 miles wide – and is known, because of its</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.12</book-part-id>
                  <title-group>
                     <label>7</label>
                     <title>Island Paradiplomacy and Smaller Island State Diplomacy Compared</title>
                  </title-group>
                  <fpage>142</fpage>
                  <abstract>
                     <p>The international community may be coming to terms with small, mainly island, subnational jurisdictions where the majority of resident populations is generally content to maintain and strengthen legally and economically dependent relationships with other countries, and which are still typically allowed considerable autonomy and latitude, even in relations with the outside world beyond their metropolitan centre. Indeed, the capacity of non-state units to engage the outside world through varying patterns of communication and representation, a phenomenon now widely termed<italic>paradiplomacy</italic>, is one development that invites a sober reassessment of these small islands’ once conventional constitutional path towards full sovereignty.</p>
                     <p>The</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.13</book-part-id>
                  <title-group>
                     <label>8</label>
                     <title>Environmental Policies and Politics:</title>
                     <subtitle>Economic versus Ecological Development Strategies</subtitle>
                  </title-group>
                  <fpage>166</fpage>
                  <abstract>
                     <p>Much of the literature on the development prospects of smaller, often island, jurisdictions is steeped in pessimism, driven by a serious concern as to the ability of such players to exploit the opportunities of an increasingly globalized world and its emergent liberalized trade rules. It is common to argue that small size, islandness, low-governance capacity, and “vulnerability” – that is, a “proneness of susceptibility to damage or injury” (Wisner et al., 2004: 11) – conspire to exacerbate the existing marginalization of smaller economies and therefore create a condition that justifies ongoing, special treatment. These arguments, however, “are by no means uncontentious, and</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.14</book-part-id>
                  <title-group>
                     <label>9</label>
                     <title>An Enduring “Rich Seam” of Jurisdictional Capacity</title>
                  </title-group>
                  <fpage>188</fpage>
                  <abstract>
                     <p>They may all have their quirks and tendencies – for patronage in particular – yet most of today’s populated subnational island jurisdictions are functioning democracies and have been so for decades.¹ This suggests that their political economy is working, and that the majority of the population must be benefitting sufficiently from current arrangements so as not to opt for radical change. They are, and remain, as Connell (2003) puts it crisply, in the grips of “an infinite pause.”² This observation has three main components.</p>
                     <p>First, it is a widely held axiom that the middle classes in capitalist democracies normally vote their pocketbooks:</p>
                  </abstract>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.15</book-part-id>
                  <title-group>
                     <title>Appendix</title>
                     <subtitle>Listing and Maps of Subnational Island Jurisdictions</subtitle>
                  </title-group>
                  <fpage>203</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.16</book-part-id>
                  <title-group>
                     <title>Notes</title>
                  </title-group>
                  <fpage>215</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.17</book-part-id>
                  <title-group>
                     <title>Bibliography</title>
                  </title-group>
                  <fpage>223</fpage>
               </book-part-meta>
            </book-part>
            <book-part>
               <book-part-meta>
                  <book-part-id book-part-id-type="jstor">j.ctt7zq30.18</book-part-id>
                  <title-group>
                     <title>Index</title>
                  </title-group>
                  <fpage>281</fpage>
               </book-part-meta>
            </book-part>
         </body>
      </book-part>
   </body>
</book>
