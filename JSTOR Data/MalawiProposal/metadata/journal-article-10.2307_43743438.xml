<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">socieduc</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j100369</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Sociology of Education</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>American Sociological Association; Sage</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">00380407</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">19398573</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">43743438</article-id>
         <title-group>
            <article-title>Instrumental and Expressive Education: College Planning in the Face of Poverty</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Nicole M.</given-names>
                  <surname>Deterding</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>1</day>
            <month>10</month>
            <year>2015</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">88</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">4</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40151077</issue-id>
         <fpage>284</fpage>
         <lpage>301</lpage>
         <permissions>
            <copyright-statement>Copyright ©2015 American Sociological Association</copyright-statement>
         </permissions>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/43743438"/>
         <abstract>
            <p>Nearly all young people in the United States aspire to a college degree, but many fail to complete college in a timely manner. Does this lack of attainment reflect abandoned college plans? I analyze mixed-methods data from a five-year study of 700 low-income mothers at two Louisiana community colleges. Hurricane Katrina displaced respondents and interrupted their college educations; respondents had to decide whether, how, and why to return to school. Few women earned degrees during the study, but survey data indicate that the rate of reenrollment and intentions to complete were high. Interview data reveal the cultural logics supporting continued plans for a return to college. Instrumentally, respondents believed education would result in better employment. Expressively, the moral status afforded students supported respondents' narratives of upward mobility despite the difficulties they faced. The logic of human capital investment dominates policy and academic discussions of education's value, but I find the symbolic meaning of a college degree also shapes plans for college return and college decision making long into adulthood. Plans to return persist long beyond the objective probability of earning a degree, and despite respondents' difficult experiences, due to the expressive value college plans add to these young women's lives.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>NOTES</title>
         <ref id="d36e134a1310">
            <label>1</label>
            <mixed-citation id="d36e141" publication-type="other">
Richburg-
Hayes and colleagues (2009)</mixed-citation>
            <mixed-citation id="d36e150" publication-type="other">
Barrow and col-
leagues (2014).</mixed-citation>
         </ref>
         <ref id="d36e160a1310">
            <label>2</label>
            <mixed-citation id="d36e167" publication-type="other">
Havighurst 1964;</mixed-citation>
            <mixed-citation id="d36e173" publication-type="other">
Hiemstra 1972, 1976;</mixed-citation>
            <mixed-citation id="d36e179" publication-type="other">
Londoner
1990;</mixed-citation>
            <mixed-citation id="d36e189" publication-type="other">
Peterson 1983</mixed-citation>
            <mixed-citation id="d36e195" publication-type="other">
Havighurst
1964</mixed-citation>
            <mixed-citation id="d36e204" publication-type="other">
Havighurst's (1964)</mixed-citation>
            <mixed-citation id="d36e210" publication-type="other">
Londoner (1990)</mixed-citation>
            <mixed-citation id="d36e216" publication-type="other">
Parsons ([1951] 1962),</mixed-citation>
         </ref>
      </ref-list>
      <ref-list>
         <title>REFERENCES</title>
         <ref id="d36e232a1310">
            <mixed-citation id="d36e236" publication-type="other">
Alba, Richard D. and David E. Lavin. 1981. "Commu-
nity Colleges and Tracking in Higher Education."
Sociology of Education 54(4):223-37.</mixed-citation>
         </ref>
         <ref id="d36e249a1310">
            <mixed-citation id="d36e253" publication-type="other">
Alexander, Karl, Robert Bozick, and Doris Entwisle.
2008. "Warming Up, Cooling Out, or Holding
Steady? Persistence and Change in Educational
Expectations after High School." Sociology of Edu-
cation 81(4):371-96.</mixed-citation>
         </ref>
         <ref id="d36e272a1310">
            <mixed-citation id="d36e276" publication-type="other">
Alfonso, Mariana. 2006. "The Impact of Community
College Attendance on Baccalaureate Attainment."
Research in Higher Education 47(8):873-903.</mixed-citation>
         </ref>
         <ref id="d36e289a1310">
            <mixed-citation id="d36e293" publication-type="other">
Antikainen, Ari, Jarmo Houtsonen, Juha Kauppila, Katja
Komonen, and Leena Koski. 1999. "Construction of
Identity and Culture through Education." Interna-
tional Journal of Contemporary Sociology 36(2):
204-28.</mixed-citation>
         </ref>
         <ref id="d36e313a1310">
            <mixed-citation id="d36e317" publication-type="other">
Bahr, Peter Riley. 2008. "Cooling Out in the Community
College: What Is the Effect of Academic Advising
on Students' Chances of Success." Research in
Higher Education 49:704-32.</mixed-citation>
         </ref>
         <ref id="d36e333a1310">
            <mixed-citation id="d36e337" publication-type="other">
Baird, Chardie L., Stephanie W. Bürge, and John R. Rey-
nolds. 2008. "Absurdly Ambitious: Teenagers'
Expectations for the Future and the Realities of
Social Structure." Sociology Compass 2/3:944-62.</mixed-citation>
         </ref>
         <ref id="d36e353a1310">
            <mixed-citation id="d36e357" publication-type="other">
Barrow, Lisa, Lashawn Richburg-Hayes, Cecilia Elena
Rouse, and Thomas Brock. 2014. "Paying for Perfor-
mance: The Education Impacts of a Community Col-
lege Scholarship Program for Low-income Adults."
Journal of Labor Economics 32(2):563-99.</mixed-citation>
         </ref>
         <ref id="d36e376a1310">
            <mixed-citation id="d36e380" publication-type="other">
Berkner, Lutz. 2003. Descriptive Summary of 1995-1996
Beginning Postsecondary Students: Six Years Later.
Washington, DC: US Department of Education,
Institute of Education Sciences.</mixed-citation>
         </ref>
         <ref id="d36e396a1310">
            <mixed-citation id="d36e400" publication-type="other">
Bohon, Stephanie A., Monica K. Johnson, and Bridget
K. Gorman. 2006. "College Aspirations and Expect-
ations among Latino Adolescents in the United
States." Social Problems 53:207-25.</mixed-citation>
         </ref>
         <ref id="d36e416a1310">
            <mixed-citation id="d36e420" publication-type="other">
Bozick, Robert. 2007. "Making It through the First Year
of College: The Role of Students' Economic Resour-
ces, Employment, and Living Arrangements." Soci-
ology of Education 80(3):261-84.</mixed-citation>
         </ref>
         <ref id="d36e437a1310">
            <mixed-citation id="d36e441" publication-type="other">
Bnnt, Steven G. and Jerome Karabel. 1989. The Diverted
Dream: Community Colleges and the Promise of
Educational Opportunity in America, 1900-1985.
New York: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d36e457a1310">
            <mixed-citation id="d36e461" publication-type="other">
Campbell, Courtney and Regina Deil-Amen. 2012.
"Role Reversal in the College Admissions Process:
How For-profits Sell Themselves." Paper presented
as the ASHE Annual Conference, November 17,
Las Vegas, NV.</mixed-citation>
         </ref>
         <ref id="d36e480a1310">
            <mixed-citation id="d36e484" publication-type="other">
Carroll, C. Dennis. 1989. College Persistence and
Degree Attainment for 1980 High School Graduates:
Hazards for Transfers, Stopouts and Part-timers.
Washington, DC: Office of Education Research and
Improvement.</mixed-citation>
         </ref>
         <ref id="d36e503a1310">
            <mixed-citation id="d36e507" publication-type="other">
Clark, Burton. 1960a. "The Cooling-out Function in
Higher Education." American Journal of Sociology
65:569-76.</mixed-citation>
         </ref>
         <ref id="d36e520a1310">
            <mixed-citation id="d36e524" publication-type="other">
Clark, Burton. 1960b. The Open Door College: A Case
Study. New York: McGraw-Hill.</mixed-citation>
         </ref>
         <ref id="d36e534a1310">
            <mixed-citation id="d36e538" publication-type="other">
Clark, Burton. 1980. "The Cooling-out Function Revis-
ited." New Directions for Community Colleges 32:
15-31.</mixed-citation>
         </ref>
         <ref id="d36e552a1310">
            <mixed-citation id="d36e556" publication-type="other">
Conley, Dalton. 2001. "Capital for College: Parental
Assets and Postsecondary Schooling." Sociology of
Education 74(l):59-72.</mixed-citation>
         </ref>
         <ref id="d36e569a1310">
            <mixed-citation id="d36e573" publication-type="other">
Deil-Amen, Regina and James E. Rosenbaum. 2002.
"The Unintended Consequences of Stigma-free
Remediation." Sociology of Education 75(3):249-68.</mixed-citation>
         </ref>
         <ref id="d36e586a1310">
            <mixed-citation id="d36e590" publication-type="other">
Deil-Amen, Regina and Ruth Lopez Turley. 2007. "A
Review of the Transition to College Literature in
Sociology." Teachers College Record 109(10):
2324-66.</mixed-citation>
         </ref>
         <ref id="d36e606a1310">
            <mixed-citation id="d36e610" publication-type="other">
Deming, David J., Claudia Goldin, and Lawrence F.
Katz. 2012. "The For-profit Postsecondary School
Sector: Nimble Critters or Agile Predators?" Journal
of Economic Perspectives 26(1): 139-64.</mixed-citation>
         </ref>
         <ref id="d36e626a1310">
            <mixed-citation id="d36e630" publication-type="other">
Dougherty, Kevin J. 1987. "The Effects of Community
Colleges: Aid or Hindrance to Socioeconomic
Attainment?" Sociology of Education 60(2):86-103.</mixed-citation>
         </ref>
         <ref id="d36e643a1310">
            <mixed-citation id="d36e647" publication-type="other">
Dougherty, Kevin J. 1992. "Community Colleges and
Baccalaureate Attainment." Journal of Higher Edu-
cation 63(2): 188.</mixed-citation>
         </ref>
         <ref id="d36e661a1310">
            <mixed-citation id="d36e665" publication-type="other">
Dougherty, Kevin J. 1994. The Contradictory College:
The Conflicting Origins, Impacts, and Futures of
the Community College. Albany: State University
of New York Press.</mixed-citation>
         </ref>
         <ref id="d36e681a1310">
            <mixed-citation id="d36e685" publication-type="other">
Doyle, William R. 2009. "The Effect of Community
College Enrollment on Bachelor's Degree Comple-
tion." Economics of Education Review 28(2):
199-206.</mixed-citation>
         </ref>
         <ref id="d36e701a1310">
            <mixed-citation id="d36e705" publication-type="other">
Eliason, Scott R., Jeylan T. Mortimer, and Mike Vuolo.
2015. "The Transition to Adulthood: Life Course
Structures and Subjective Perceptions." Social Psy-
chology Quarterly. Advance online publication.</mixed-citation>
         </ref>
         <ref id="d36e721a1310">
            <mixed-citation id="d36e725" publication-type="other">
Frye, Margaret, 2012. "Bright Futures in Malawi's New
Dawn: Educational Aspirations as Assertions of
Identity." American Journal of Sociology 117(6):
1565-1624.</mixed-citation>
         </ref>
         <ref id="d36e741a1310">
            <mixed-citation id="d36e745" publication-type="other">
Giudici, Francesco and Aaron M. Pallas. 2014. "Social
Origins and Post-high school Institutional Pathways:
A Cumulative Dis/advantage Approach." Social Sci-
ence Research 44:103-13.</mixed-citation>
         </ref>
         <ref id="d36e761a1310">
            <mixed-citation id="d36e765" publication-type="other">
Goldrick-Rab, Sara. 2006. "Following Their Every
Move: An Investigation of Social-class Differences
in College Pathways." Sociology of Education
79(1):61-79.</mixed-citation>
         </ref>
         <ref id="d36e782a1310">
            <mixed-citation id="d36e786" publication-type="other">
Government Accountability Office. 2010. For-profit
Colleges: Undercover Testing Finds Colleges
Encouraged Fraud and Engaged in Deceptive and
Questionable Marketing Practices. GAO 10-948T.
Washington, DC: Author.</mixed-citation>
         </ref>
         <ref id="d36e805a1310">
            <mixed-citation id="d36e809" publication-type="other">
Goyette, Kimberly A. 2008. "College for Some to Col-
lege for All: Social Background, Occupational
Expectations, and Educational Expectations over
Time." Social Science Research 37(2):461-84.</mixed-citation>
         </ref>
         <ref id="d36e825a1310">
            <mixed-citation id="d36e829" publication-type="other">
Hamilton, Peter. 1991. Max Weber, Critical Assessments
2. London: Routledge.</mixed-citation>
         </ref>
         <ref id="d36e839a1310">
            <mixed-citation id="d36e843" publication-type="other">
Hancock, Agne-Marie. 2004. The Politics of Disgust:
The Public Identity of the Welfare Queen. New
York: New York University Press.</mixed-citation>
         </ref>
         <ref id="d36e856a1310">
            <mixed-citation id="d36e860" publication-type="other">
Hanson, Sandra L. 1994. "Lost Talent: Unrealized Edu-
cational Aspirations and Expectations among U.S.
Youths." Sociology of Education 67(3): 159-83.</mixed-citation>
         </ref>
         <ref id="d36e873a1310">
            <mixed-citation id="d36e877" publication-type="other">
Hauser, Robert H. and Douglas K. Anderson. 1991.
"Post-high school Plans and Aspirations of Black
and White High School Seniors: 1976-1986." Sociol-
ogy of Education 64:263-77.</mixed-citation>
         </ref>
         <ref id="d36e894a1310">
            <mixed-citation id="d36e898" publication-type="other">
Havighurst, Richard. 1964. "Changing Status and Roles
during the Adult Life Cycle: Significance for Adult
Education." Pp. 17-38 in Sociological Backgrounds
of Adult Education, edited by H. Burns. Chicago:
Center for the Liberal Education of Adults.</mixed-citation>
         </ref>
         <ref id="d36e917a1310">
            <mixed-citation id="d36e921" publication-type="other">
Hiemstra, Roger. 1972. "Continuing Education for the
Aged: A Survey of Needs and Interests of Older Peo-
ple." Adult Education Quarterly 22(2): 100-109.</mixed-citation>
         </ref>
         <ref id="d36e934a1310">
            <mixed-citation id="d36e938" publication-type="other">
Hiemstra, Roger. 1976. "Older Adult Learning: Instru-
mental and Expressive Categories." Educational
Gerontology 1(3):227-36.</mixed-citation>
         </ref>
         <ref id="d36e951a1310">
            <mixed-citation id="d36e955" publication-type="other">
Holland, Megan. 2015. "College for All and Community
College for None: Stigma in High Achieving
Schools." Teacher's College Record 117(5): 1-52.</mixed-citation>
         </ref>
         <ref id="d36e968a1310">
            <mixed-citation id="d36e972" publication-type="other">
Kalogrides, Demetra and Eric Grodsky. 2011. "Some-
thing to Fall Back On: Community Colleges as
a Safety Net." Social Forces 89(3):853-77.</mixed-citation>
         </ref>
         <ref id="d36e985a1310">
            <mixed-citation id="d36e989" publication-type="other">
Kao, Grace and Jennifer S. Thompson. 2003. "Racial and
Ethnic Stratification in Educational Achievement and
Attainment." Annual Review of Sociology 29:417-42.</mixed-citation>
         </ref>
         <ref id="d36e1003a1310">
            <mixed-citation id="d36e1007" publication-type="other">
Katz, Michael B. 2013. The Undeserving Poor: Ameri-
ca's Enduring Confrontation with Poverty, 2nd ed.
Oxford, UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d36e1020a1310">
            <mixed-citation id="d36e1024" publication-type="other">
Kerckhoff, Alan C. 2002. "The Transition from School
to Work." Pp. 52-87 in The Changing Adolescent
Experience: Societal Trends and the Transition to
Adulthood , edited by J. T. Mortimer and R. W. Lar-
son. New York: Cambridge University Press.</mixed-citation>
         </ref>
         <ref id="d36e1043a1310">
            <mixed-citation id="d36e1047" publication-type="other">
Kerckhoff, Alan C. 2003. "From Student to Worker."
Pp. 251-68 in Handbook of the Life Course, edited
by J. T. Mortimer and M. J. Shanahan. New York:
Kluwer Academic/Plenum.</mixed-citation>
         </ref>
         <ref id="d36e1063a1310">
            <mixed-citation id="d36e1067" publication-type="other">
Leigh, D. E. and A. M. Gill. 2003. "Do Community Col-
leges Really Divert Students from Earning Bache-
lor's Degrees?" Economics of Education Review
22(1):23-30.</mixed-citation>
         </ref>
         <ref id="d36e1083a1310">
            <mixed-citation id="d36e1087" publication-type="other">
Londoner, Carroll. 1990. "Instrumental and Expressive
Education: From Needs to Goals Assessment for
Educational Planning." Pp. 85-107 in Introduction
to Educational Gerontology , edited by R. H. Sherron
and D. B. Lumsden. Hoboken, NJ: Taylor and
Francis.</mixed-citation>
         </ref>
         <ref id="d36e1110a1310">
            <mixed-citation id="d36e1114" publication-type="other">
Luttrell, Wendy. 1997. School-smart and Mother-wise:
Working-class Women 's Identity and Schooling.
New York: Routledge.</mixed-citation>
         </ref>
         <ref id="d36e1128a1310">
            <mixed-citation id="d36e1132" publication-type="other">
McCormick, Alexander C. 2003. "Swirling and Double-
dipping: New Patterns of Student Attendance in
Higher Education." New Directions for Higher Edu-
cation 121:13-24.</mixed-citation>
         </ref>
         <ref id="d36e1148a1310">
            <mixed-citation id="d36e1152" publication-type="other">
Mettler, Susan. 2014. Degrees of Inequality: How the
Politics of Higher Education Sabotaged the Ameri-
can Dream. New York: Basic Books.</mixed-citation>
         </ref>
         <ref id="d36e1165a1310">
            <mixed-citation id="d36e1169" publication-type="other">
National Center for Education Statistics, Institute of
Education Sciences. 2014. Digest of Education Sta-
tistics. Washington, DC: Author.</mixed-citation>
         </ref>
         <ref id="d36e1182a1310">
            <mixed-citation id="d36e1186" publication-type="other">
Pallas, Aaron M., Matthew Boulay, and Melinda Mechur
Karp. 2003. "On What Is Learned in School: A Ver-
stehen Approach." Pp. 17-40 In Stability and
Change in Education: Structure, Processes and
Outcomes, edited by M. T. Hallinan, A. Gamoran,
W. Kubitschek, and T. Loveless. New York: Per-
cheron Press.</mixed-citation>
         </ref>
         <ref id="d36e1212a1310">
            <mixed-citation id="d36e1216" publication-type="other">
Park, Toby, J. 2013. "Stopout and Time for Work: An
Analysis of Degree Trajectories for Community Col-
lege Students." Paper presented at the Annual Meet-
ing of the Association for the Study of Higher Edu-
cation, November, St. Louis, MO.</mixed-citation>
         </ref>
         <ref id="d36e1235a1310">
            <mixed-citation id="d36e1239" publication-type="other">
Parsons, Talcott. [1951] 1962. The Social System. New
York: Free Press.</mixed-citation>
         </ref>
         <ref id="d36e1250a1310">
            <mixed-citation id="d36e1254" publication-type="other">
Pascarella, Ernest T., Marcia Edison, Amaury Nora,
Linda Serra Hagedorn, and Patrick T. Terenzini.
1998. "Does Community College versus Four-year
College Attendance Influence Students' Educational
Plans?" Journal of College Student Development
39(2):179.</mixed-citation>
         </ref>
         <ref id="d36e1277a1310">
            <mixed-citation id="d36e1281" publication-type="other">
Peterson, David A. 1983. Facilitating Education for
Older Learners. San Francisco: Jossey-Bass.</mixed-citation>
         </ref>
         <ref id="d36e1291a1310">
            <mixed-citation id="d36e1295" publication-type="other">
Reynolds, John and Chardie Baird. 2010. "Is There
a Downside to Shooting for the Stars? Unrealized
Educational Expectations and Symptoms of
Depression." American Sociological Review
75(1): 151-72.</mixed-citation>
         </ref>
         <ref id="d36e1314a1310">
            <mixed-citation id="d36e1318" publication-type="other">
Reynolds, John, Michael Steward, Ryan Macdonald, and
Lacey Sischo. 2006. "Have Adolescents Become
Too Ambitious? High School Seniors' Educational
and Occupational Plans, 1976-2000." Social Prob-
lems 53(2): 186-206.</mixed-citation>
         </ref>
         <ref id="d36e1337a1310">
            <mixed-citation id="d36e1341" publication-type="other">
Richburg-Hayes, Lashawn, Thomas Brock, Allen LeB-
lanc, Christina Paxson, Cecilia Elena Rouse, and
Lisa Barrow. 2009. "Rewarding Persistence: Effects
of a Performance-based Scholarship Program for
Young Parents." New York: MDRC.</mixed-citation>
         </ref>
         <ref id="d36e1360a1310">
            <mixed-citation id="d36e1364" publication-type="other">
Roksa, Josipa and Melissa Velez. 2010. "When Studying
Schooling Is not Enough: Incorporating Employment
in Models of Educational Transitions." Research in
Social Stratification and Mobility 28(1):5-21.</mixed-citation>
         </ref>
         <ref id="d36e1381a1310">
            <mixed-citation id="d36e1385" publication-type="other">
Roksa, Josipa and Melissa Velez. 2012. "A Late Start:
Delayed Entry, Life Course Transitions and Bache-
lor's Degree Completion." Social Forces 90(3):
769-94.</mixed-citation>
         </ref>
         <ref id="d36e1401a1310">
            <mixed-citation id="d36e1405" publication-type="other">
Rosenbaum, James. 1998. "College-for-all: Do Students
Understand What College Demands?" Social Psy-
chology of Education 2:55-80.</mixed-citation>
         </ref>
         <ref id="d36e1418a1310">
            <mixed-citation id="d36e1422" publication-type="other">
Rosenbaum, James E. 2001. Beyond College for All:
Career Paths for the Forgotten Half New York:
Russell Sage Foundation.</mixed-citation>
         </ref>
         <ref id="d36e1435a1310">
            <mixed-citation id="d36e1439" publication-type="other">
Rosenbaum, James E., Regina Deil-Amen, and Ann E. Per-
son. 2006. After Admission: From College Access to
College Success. New York: Russell Sage Foundation.</mixed-citation>
         </ref>
         <ref id="d36e1452a1310">
            <mixed-citation id="d36e1456" publication-type="other">
Rouse, Cecilia hiena. 1995. Democratization or Diver-
sion? The Effect of Community Colleges on Educa-
tional Attainment." Journal of Business &amp; Economic
Statistics 13(2):2 17.</mixed-citation>
         </ref>
         <ref id="d36e1472a1310">
            <mixed-citation id="d36e1476" publication-type="other">
Schneider, Barbara L. and David Stevenson. 1999. The
Ambitious Generation: America's Teenagers, Moti-
vated but Directionless. New Haven, CT: Yale Uni-
versity Press.</mixed-citation>
         </ref>
         <ref id="d36e1493a1310">
            <mixed-citation id="d36e1497" publication-type="other">
Shaw, Kathleen M., Sara Goldrick-Rab, Christopher
Mazzeo, and Jerry A. Jacobs. 2009. Putting Poor
People to Work: How the Work-first Idea Eroded
College Access for the Poor. New York: Russell
Sage Foundation.</mixed-citation>
         </ref>
         <ref id="d36e1516a1310">
            <mixed-citation id="d36e1520" publication-type="other">
Silva, Jennifer M. 2012. "Constructing Adulthood in an
Age of Uncertainty." American Sociological Review
78(4):505-22.</mixed-citation>
         </ref>
         <ref id="d36e1533a1310">
            <mixed-citation id="d36e1537" publication-type="other">
Silva, Jennifer M. 2013. Coming Up Short: Working-
class Adulthood in an Age of Uncertainty. Oxford,
UK: Oxford University Press.</mixed-citation>
         </ref>
         <ref id="d36e1550a1310">
            <mixed-citation id="d36e1554" publication-type="other">
Silva, Jennifer M. and Allison J. Pugh. 2010. "Beyond
the Depleting Model of Parenting: Narratives of
Childrearing and Change." Sociological Inquiry
80(4):605-27.</mixed-citation>
         </ref>
         <ref id="d36e1570a1310">
            <mixed-citation id="d36e1574" publication-type="other">
Turner, Ralph H. 1960. "Sponsored and Contest Mobil-
ity and the School System." American Sociological
Review 25(6):855-862.</mixed-citation>
         </ref>
         <ref id="d36e1587a1310">
            <mixed-citation id="d36e1591" publication-type="other">
U.S. Census Bureau. 2014. Educational Attainment in
the United States: 2014. Detailed Tables. Retrieved
July 10, 2015 (https://www.census.gov/hhes/
socdemo/education/data/cps/2014/tables.html).</mixed-citation>
         </ref>
         <ref id="d36e1608a1310">
            <mixed-citation id="d36e1612" publication-type="other">
Vaisey, Stephen. 2010. "What People Want: Rethinking
Poverty, Culture and Educational Attainment."
Annals of the American Academy of Political and
Social Science 629(1):75-101.</mixed-citation>
         </ref>
         <ref id="d36e1628a1310">
            <mixed-citation id="d36e1632" publication-type="other">
Weber, Max. [1922] 1978. Economy and Society: An
Outline of Interpretive Sociology. Berkeley: Univer-
sity of California Press.</mixed-citation>
         </ref>
         <ref id="d36e1645a1310">
            <mixed-citation id="d36e1649" publication-type="other">
Young, Alford A. 2004. The Minds of Marginalized
Black Men: Making Sense of Mobility, Opportunity,
and Future Life Chances. Princeton, NJ: Princeton
University Press.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
