<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mmwrsurvsumm</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50008370</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Morbidity and Mortality Weekly Report: Surveillance Summaries</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Centers for Disease Control and Prevention</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">15460738</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15458636</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42568132</article-id>
         <title-group>
            <article-title>Malaria Surveillance — United States, 2000</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Louise M.</given-names>
                  <surname>Causer</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert D.</given-names>
                  <surname>Newman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ann M.</given-names>
                  <surname>Barber</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jacquelin M.</given-names>
                  <surname>Roberts</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Gail</given-names>
                  <surname>Stennies</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Peter B.</given-names>
                  <surname>Bloland</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Monica E.</given-names>
                  <surname>Parise</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Richard W.</given-names>
                  <surname>Steketee</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>12</day>
            <month>7</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">SS-5</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40095085</issue-id>
         <fpage>9</fpage>
         <lpage>23</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42568132"/>
         <abstract>
            <p>Problem/Condition: Malaria is caused by four species of intraerythrocytic protozoa of the genus Plasmodium (i.e., P. falciparum, P. vivax, P. ovale, or P. mahriae). Malaria is transmitted by the bite of an infective female Anopheles sp. mosquito. The majority of malaria infections in the United States occur among persons who have traveled to areas with ongoing transmission. In the United States, cases can occur through exposure to infected blood products, by congenital transmission, or locally through mosquitoborne transmission. Malaria surveillance is conducted to identify episodes of local transmission and to guide prevention recommendations for travelers. Period Covered: Cases with onset of illness during 2000. Description of System: Malaria cases confirmed by blood smear are reported to local and state health departments by health-care providers or laboratory staff. Case investigations are conducted by local and state health departments, and reports are transmitted to CDC through the National Malaria Surveillance System (NMSS). Data from NMSS serve as the basis for this report. Results: CDC received reports of 1,402 cases of malaria with an onset of symptoms during 2000 among persons in the United States or one of its territories. This number represents a decrease of 9.0% from the 1,540 cases reported for 1999. P. falciparum, P. vivax, P. malariae, and P. ovale were identified in 43.6%, 37.2%, 4.8%, and 2.3% of cases, respectively. Nine patients (0.6% of total) were infected by ≥2 species. The infecting species was unreported or undetermined in 161 (11.5%) cases. Compared with 1999, the number of reported malaria cases acquired in Africa decreased by 13.1% (n = 783), and a decrease of 3.3% (n = 238) occurred in cases acquired in Asia. Cases from the Americas decreased by 1.1% (n = 271) from 1999. Of 825 U.S. civilians who acquired malaria abroad, 190 (23.0%) reported that they had followed a chemoprophylactic drug regimen recommended by CDC for the area to which they had traveled. Four patients became infected in the United States, two through congenital transmission and two through probable induced transmission. Six deaths were attributed to malaria, four caused by P. falciparum, one caused by P. malariae, and one by P. ovale. Interpretation: The 9.0% decrease in malaria cases in 2000, compared with 1999, resulted primarily from decreases in cases acquired in Africa and Asia. This decrease could have resulted from local changes in disease transmission, decreased travel to these regions, fluctuation in reporting to state and local health departments, or an increased use of effective antimalarial chemoprophylaxis. In the majority of reported cases, U.S. civilians who acquired infection abroad were not on an appropriate chemoprophylaxis regimen for the country in which they acquired malaria. Public Health Actions: Additional information was obtained concerning the six fatal cases and the four infections acquired in the United States. Persons traveling to a malarious area should take one of the recommended chemoprophylaxis regimens appropriate for the region of travel, and travelers should use personal protection measures to prevent mosquito bites. Any person who has been to a malarious area and who subsequently develops a fever or influenza-like symptoms should seek medical care immediately and report their travel history to the clinician; investigation should include a blood-film test for malaria. Malaria infections can be fatal if not diagnosed and treated promptly. Recommendations concerning malaria prevention can be obtained from CDC by calling the Malaria Hotline at 770-488-7788 or by accessing CDC's Internet site at http://www.cdc.gov/travel.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d267e288a1310">
            <label>1</label>
            <mixed-citation id="d267e295" publication-type="other">
World Health Organization. World malaria situation in 1994. Wkly
Epidemiol Rec 1997;72:269-76.</mixed-citation>
         </ref>
         <ref id="d267e305a1310">
            <label>2</label>
            <mixed-citation id="d267e312" publication-type="other">
Bremen JG. Ears of the hippopotamus: manifestations, determinants,
and estimates of the malaria burden. Am J Trop Med Hyg
2001;64(S1):1-11.</mixed-citation>
         </ref>
         <ref id="d267e325a1310">
            <label>3</label>
            <mixed-citation id="d267e332" publication-type="other">
Pan American Health Organization. Report for registration of malaria
eradication from United States of America. Washington, DC: Pan
American Health Organization, 1969.</mixed-citation>
         </ref>
         <ref id="d267e345a1310">
            <label>4</label>
            <mixed-citation id="d267e352" publication-type="other">
Zucker JR. Changing patterns of autochthonous malaria transmission
in the United States: a review of recent outbreaks. Emerg Infect Dis
1996;2:37-43.</mixed-citation>
         </ref>
         <ref id="d267e366a1310">
            <label>5</label>
            <mixed-citation id="d267e373" publication-type="other">
Lackritz EM, Lobel HO, Howell J, Bioland P, Campbell CC.
Imported Plasmodium falciparum malaria in American travelers to
Africa: implications for prevention strategies. JAMA 1991;265:383-5.</mixed-citation>
         </ref>
         <ref id="d267e386a1310">
            <label>6</label>
            <mixed-citation id="d267e393" publication-type="other">
Stroup DE Special analytic issues. In: Teutsch SM, Churchill RE, ed.
Principles and practice of public health surveillance. New York, NY:
Oxford University Press, 1994; 143-5.</mixed-citation>
         </ref>
         <ref id="d267e406a1310">
            <label>7</label>
            <mixed-citation id="d267e413" publication-type="other">
World Health Organization. Terminology of malaria and of malaria
eradication: report of a drafting committee. Geneva, Switzerland: World
Health Organization, 1963;32.</mixed-citation>
         </ref>
         <ref id="d267e426a1310">
            <label>8</label>
            <mixed-citation id="d267e433" publication-type="other">
Newman RD, Barber AM, Roberts J, Holtz TH, Steketee RW, Parise
ME. Malaria surveillance—United States, 1999. In: CDC Surveillance
Summaries. MMWR 2002; 51 (No. SS-1):15-28.</mixed-citation>
         </ref>
         <ref id="d267e446a1310">
            <label>9</label>
            <mixed-citation id="d267e453" publication-type="other">
CDC. Health information for international travel, 2001-2002.
Atlanta, GA: US Department of Health and Human Services, Public
Health Service, CDC, National Center for Infectious Diseases, 2001.</mixed-citation>
         </ref>
         <ref id="d267e466a1310">
            <label>10</label>
            <mixed-citation id="d267e473" publication-type="other">
CDC. Congenital malaria as a result of Plasmodium malariae—North
Carolina, 2000. MMWR 2002;51:164-5.</mixed-citation>
         </ref>
         <ref id="d267e484a1310">
            <label>11</label>
            <mixed-citation id="d267e491" publication-type="other">
Greenberg AE, Lobel HO. Mortality from Plasmodium falciparum
malaria in travelers from the United States, 1959 to 1987. Ann Intern
Med 1990;113:326-7.</mixed-citation>
         </ref>
         <ref id="d267e504a1310">
            <label>12</label>
            <mixed-citation id="d267e511" publication-type="other">
Luxemburger C, Ricci F, Nosten F, Raimond D, Bathet S, White NJ.
Epidemiology of severe malaria in an area of low transmission in Thai-
land. Trans R Soc Trop Med Hyg 1997;91:256-62.</mixed-citation>
         </ref>
         <ref id="d267e524a1310">
            <label>13</label>
            <mixed-citation id="d267e531" publication-type="other">
Nosten F, ter Kuile F, Maelankirri L, Decludt B, White NJ. Malaria
during pregnancy in an area of unstable endemicity. Trans R Soc Trop
Med Hyg 1991;85:424-9.</mixed-citation>
         </ref>
         <ref id="d267e544a1310">
            <label>14</label>
            <mixed-citation id="d267e551" publication-type="other">
Zucker JR, Campbell CC. Malaria: principles of prevention and treat-
ment. Infect Dis Clin North Am 1993;7:547-67.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
