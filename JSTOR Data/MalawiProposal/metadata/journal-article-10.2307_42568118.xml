<?xml version="1.0" encoding="UTF-8"?>
<article dtd-version="1.0" article-type="research-article">
   <front>
      <journal-meta>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">mmwrsurvsumm</journal-id>
         <journal-id xmlns:xlink="http://www.w3.org/1999/xlink" journal-id-type="jstor">j50008370</journal-id>
         <journal-title-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <journal-title>Morbidity and Mortality Weekly Report: Surveillance Summaries</journal-title>
         </journal-title-group>
         <publisher>
            <publisher-name>Centers for Disease Control and Prevention</publisher-name>
         </publisher>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="ppub">15460738</issn>
         <issn xmlns:xlink="http://www.w3.org/1999/xlink" pub-type="epub">15458636</issn>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink"/>
      </journal-meta>
      <article-meta>
         <article-id xmlns:xlink="http://www.w3.org/1999/xlink" pub-id-type="jstor">42568118</article-id>
         <title-group>
            <article-title>Malaria Surveillance — United States, 1999</article-title>
         </title-group>
         <contrib-group>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Robert D.</given-names>
                  <surname>Newman</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Ann M.</given-names>
                  <surname>Barber</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Jacquelin</given-names>
                  <surname>Roberts</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Timothy</given-names>
                  <surname>Holtz</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Richard W.</given-names>
                  <surname>Steketee</surname>
               </string-name>
            </contrib>
            <contrib>
               <string-name xmlns:xlink="http://www.w3.org/1999/xlink">
                  <given-names>Monica E.</given-names>
                  <surname>Parise</surname>
               </string-name>
            </contrib>
         </contrib-group>
         <pub-date xmlns:xlink="http://www.w3.org/1999/xlink">
            <day>29</day>
            <month>3</month>
            <year>2002</year>
         </pub-date>
         <volume xmlns:xlink="http://www.w3.org/1999/xlink"
                 xmlns:mml="http://www.w3.org/1998/Math/MathML"
                 xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">51</volume>
         <issue xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table">SS-1</issue>
         <issue-id xmlns:xlink="http://www.w3.org/1999/xlink">i40095079</issue-id>
         <fpage>15</fpage>
         <lpage>28</lpage>
         <self-uri xmlns:xlink="http://www.w3.org/1999/xlink"
                   xlink:href="https://www.jstor.org/stable/42568118"/>
         <abstract>
            <p>Problem/Condition: Malaria is caused by four species of intraerythrocytic protozoa of the genus Plasmodium (i.e., P. falciparum, P. vivax, P. ovale, or P. malariae). Malaria is transmitted by the bite of an infective female Anopheles sp. mosquito. The majority of malaria infections in the United States occur in persons who have traveled to areas with ongoing transmission. In the United States, cases can occur through exposure to infected blood products, by congenital transmission, or locally through mosquitoborne transmission. Malaria surveillance is conducted to identify episodes of local transmission and to guide prevention recommendations for travelers. Period Covered: Cases with onset of illness during 1999. Description of System: Malaria cases confirmed by blood films are reported to local and state health departments by health-care providers or laboratory staff. Case investigations are conducted by local and state health departments, and reports are transmitted to CDC through the National Malaria Surveillance System (NMSS). Data from NMSS serve as the basis for this report. Results: CDC received reports of 1,540 cases of malaria with an onset of symptoms during 1999 among persons in the United States or one of its territories. This number represents an increase of 25.5% from the 1,227 cases reported for 1998. P. falciparum, P. vivax, P. malariae, and P. ovale were identified in 46.0%, 30.7%, 4.6%, and 3.6% of cases, respectively. More than one species was present in 12 patients (0.8% of total). The infecting species was unreported or undetermined in 223 (14.5%) cases. The number of reported malaria cases acquired in Africa increased 27.6% (n = 901), compared with 1998, and an increase of 2.9% (n = 246) occurred in cases acquired in Asia, compared with 1998. Cases from the Americas increased by 19.7% (n = 274) from 1998. Of 831 U.S. civilians who acquired malaria abroad, 159 (19.1%) reported that they had followed a chemoprophylactic drug regimen recommended by CDC for the area to which they had traveled. Three patients became infected in the United States, all through probable local mosquitoborne transmission. Five deaths were attributed to malaria, all caused by P. falciparum. Interpretation: The 25.5% increase in malaria cases in 1999, compared with 1998, resulted primarily from increases in cases acquired in Africa and the Americas. This increase is possibly related to a change in the system by which states report to CDC, but it could also have resulted from local changes in disease transmission, increased travel to these regions, improved reporting to state and local health departments, or a decreased use of effective antimalarial chemoprophylaxis. In the majority of reported cases, U.S. civilians who acquired infection abroad were not on an appropriate chemoprophylaxis regimen for the country where they acquired malaria. Public Health Actions: Additional information was obtained concerning the five fatal cases and the three infections acquired in the United States. The NMSS surveillance form was modified to gather more detailed information regarding compliance with prescribed chemoprophylaxis regimens. Persons traveling to a malarious area should take one of the recommended chemoprophylaxis regimens appropriate to the region of travel, and travelers should use personal protection measures to prevent mosquito bites. Any person who has been to a malarious area and who subsequently develops a fever or influenza-like symptoms should seek medical care immediately; investigation should include a blood-film test for malaria. Malaria infections can be fatal if not diagnosed and treated promptly. Recommendations concerning prevention of malaria can be obtained from CDC.</p>
         </abstract>
         <custom-meta-group xmlns:xlink="http://www.w3.org/1999/xlink">
            <custom-meta>
               <meta-name>lang</meta-name>
               <meta-value>eng</meta-value>
            </custom-meta>
         </custom-meta-group>
      </article-meta>
   </front>
   <back>
      <ref-list>
         <title>References</title>
         <ref id="d442e262a1310">
            <label>1</label>
            <mixed-citation id="d442e269" publication-type="other">
World Health Organization. World malaria situation in 1994. Wkly
Epidemiol Rec 1997;72:269-76.</mixed-citation>
         </ref>
         <ref id="d442e279a1310">
            <label>2</label>
            <mixed-citation id="d442e286" publication-type="other">
Bremen JG. The ears of the hippopotamus: manifestations, determi-
nants, and estimates of the malaria burden. Am J Trop Med Hyg
2001;64(S1):1-11.</mixed-citation>
         </ref>
         <ref id="d442e299a1310">
            <label>3</label>
            <mixed-citation id="d442e306" publication-type="other">
Pan American Health Organization. Report for registration of malaria
eradication from United States of America. Washington, DC: Pan
American Health Organization, 1969.</mixed-citation>
         </ref>
         <ref id="d442e319a1310">
            <label>4</label>
            <mixed-citation id="d442e326" publication-type="other">
Zucker, JR. Changing patterns of autochthonous malaria transmission
in the United States: a review of recent outbreaks. Emerg Infect Dis
1996;2:37-43.</mixed-citation>
         </ref>
         <ref id="d442e340a1310">
            <label>5</label>
            <mixed-citation id="d442e347" publication-type="other">
Lackritz EM, Lobel HO, Howell J, Bioland P, Campbell CC. Im-
ported Plasmodium falciparum malaria in American travelers to Africa:
implications for prevention strategies. JAMA 1991;265:383-5.</mixed-citation>
         </ref>
         <ref id="d442e360a1310">
            <label>6</label>
            <mixed-citation id="d442e367" publication-type="other">
Stroup DE Special analytic issues. In: Teutsch SM, Churchill RE, ed.
Principles and practice of public health surveillance. New York, NY:
Oxford University Press: 1994; 143-5.</mixed-citation>
         </ref>
         <ref id="d442e380a1310">
            <label>7</label>
            <mixed-citation id="d442e387" publication-type="other">
World Health Organization. Terminology of malaria and of malaria
eradication: report of a drafting committee. Geneva, Switzerland: World
Health Organization, 1963:32.</mixed-citation>
         </ref>
         <ref id="d442e400a1310">
            <label>8</label>
            <mixed-citation id="d442e407" publication-type="other">
Holtz TH, MacArthur JR, Roberts JM, et al. Malaria surveillance—
United States, 1998. In: CDC Surveillance Summaries. MMWR
2001;50(No. SS-5):1-20.</mixed-citation>
         </ref>
         <ref id="d442e420a1310">
            <label>9</label>
            <mixed-citation id="d442e427" publication-type="other">
CDC. Health information for international travel, 2001-2002. At-
lanta, GA: US Department of Health and Human Services, Public
Health Service, CDC, National Center for Infectious Diseases, 2001.</mixed-citation>
         </ref>
         <ref id="d442e440a1310">
            <label>10</label>
            <mixed-citation id="d442e447" publication-type="other">
MacArthur JR, Holtz TH, Jenkins J, et al. Probable locally acquired
mosquito-transmitted malaria in Georgia, 1999. Clin Infect Dis
2001;32:e124-8.</mixed-citation>
         </ref>
         <ref id="d442e461a1310">
            <label>11</label>
            <mixed-citation id="d442e468" publication-type="other">
CDC. Probable locally acquired mosquito-transmitted Plasmodium
vivax infection—Suffolk County, New York, 1999. MMWR
2000;49:495-8.</mixed-citation>
         </ref>
         <ref id="d442e481a1310">
            <label>12</label>
            <mixed-citation id="d442e488" publication-type="other">
Greenberg AE, Lobel HO. Mortality from Plasmodium falciparum
malaria in travelers from the United States, 1959 to 1987. Ann Intern
Med 1990;113:326-7.</mixed-citation>
         </ref>
         <ref id="d442e501a1310">
            <label>13</label>
            <mixed-citation id="d442e508" publication-type="other">
Nosten F, ter Kuile F, Maelankirri L, Decludt B, White NJ. Malaria
during pregnancy in an area of unstable endemicity. Trans R Soc Trop
Med Hyg 1991;85:424-9.</mixed-citation>
         </ref>
         <ref id="d442e521a1310">
            <label>14</label>
            <mixed-citation id="d442e528" publication-type="other">
Luxemburger C, Ricci F, Nosten F, Raimond F, Bathet S, White NJ.
Epidemiology of severe malaria in an area of low transmission in Thai-
land. Trans R Soc Trop Med Hyg 1997;91:266-62.</mixed-citation>
         </ref>
         <ref id="d442e541a1310">
            <label>15</label>
            <mixed-citation id="d442e548" publication-type="other">
Zucker JR, Campbell CC. Malaria: principles of prevention and treat-
ment. Infect Dis Clin North Am 1993;7:547-67.</mixed-citation>
         </ref>
      </ref-list>
   </back>
</article>
